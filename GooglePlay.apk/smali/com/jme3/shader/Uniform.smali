.class public Lcom/jme3/shader/Uniform;
.super Lcom/jme3/shader/ShaderVariable;
.source "Uniform.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jme3/shader/Uniform$1;
    }
.end annotation


# static fields
.field private static final ZERO_BUF:Ljava/nio/FloatBuffer;

.field private static final ZERO_FLT:Ljava/lang/Float;

.field private static final ZERO_INT:Ljava/lang/Integer;


# instance fields
.field protected binding:Lcom/jme3/shader/UniformBinding;

.field protected multiData:Ljava/nio/FloatBuffer;

.field protected setByCurrentMaterial:Z

.field protected value:Ljava/lang/Object;

.field protected varType:Lcom/jme3/shader/VarType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/jme3/shader/Uniform;->ZERO_INT:Ljava/lang/Integer;

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    sput-object v0, Lcom/jme3/shader/Uniform;->ZERO_FLT:Ljava/lang/Float;

    const/16 v0, 0x10

    invoke-static {v0}, Lcom/jme3/util/BufferUtils;->createFloatBuffer(I)Ljava/nio/FloatBuffer;

    move-result-object v0

    sput-object v0, Lcom/jme3/shader/Uniform;->ZERO_BUF:Ljava/nio/FloatBuffer;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/jme3/shader/ShaderVariable;-><init>()V

    iput-object v0, p0, Lcom/jme3/shader/Uniform;->value:Ljava/lang/Object;

    iput-object v0, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jme3/shader/Uniform;->setByCurrentMaterial:Z

    return-void
.end method


# virtual methods
.method public clearSetByCurrentMaterial()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jme3/shader/Uniform;->setByCurrentMaterial:Z

    return-void
.end method

.method public clearUpdateNeeded()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jme3/shader/Uniform;->updateNeeded:Z

    return-void
.end method

.method public clearValue()V
    .locals 3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jme3/shader/Uniform;->updateNeeded:Z

    iget-object v0, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/jme3/shader/Uniform;->ZERO_BUF:Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    iget-object v0, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    :goto_0
    iget-object v0, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->remaining()I

    move-result v0

    if-lez v0, :cond_0

    sget-object v0, Lcom/jme3/shader/Uniform;->ZERO_BUF:Ljava/nio/FloatBuffer;

    iget-object v1, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    invoke-virtual {v1}, Ljava/nio/FloatBuffer;->remaining()I

    move-result v1

    const/16 v2, 0x10

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->limit(I)Ljava/nio/Buffer;

    iget-object v0, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    sget-object v1, Lcom/jme3/shader/Uniform;->ZERO_BUF:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put(Ljava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/jme3/shader/Uniform;->varType:Lcom/jme3/shader/VarType;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/jme3/shader/Uniform$1;->$SwitchMap$com$jme3$shader$VarType:[I

    iget-object v1, p0, Lcom/jme3/shader/Uniform;->varType:Lcom/jme3/shader/VarType;

    invoke-virtual {v1}, Lcom/jme3/shader/VarType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_1

    :pswitch_1
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/jme3/shader/Uniform;->value:Ljava/lang/Object;

    goto :goto_1

    :pswitch_2
    sget-object v0, Lcom/jme3/shader/Uniform;->ZERO_INT:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/jme3/shader/Uniform;->value:Ljava/lang/Object;

    goto :goto_1

    :pswitch_3
    sget-object v0, Lcom/jme3/shader/Uniform;->ZERO_FLT:Ljava/lang/Float;

    iput-object v0, p0, Lcom/jme3/shader/Uniform;->value:Ljava/lang/Object;

    goto :goto_1

    :pswitch_4
    sget-object v0, Lcom/jme3/math/Vector2f;->ZERO:Lcom/jme3/math/Vector2f;

    iput-object v0, p0, Lcom/jme3/shader/Uniform;->value:Ljava/lang/Object;

    goto :goto_1

    :pswitch_5
    sget-object v0, Lcom/jme3/math/Vector3f;->ZERO:Lcom/jme3/math/Vector3f;

    iput-object v0, p0, Lcom/jme3/shader/Uniform;->value:Ljava/lang/Object;

    goto :goto_1

    :pswitch_6
    iget-object v0, p0, Lcom/jme3/shader/Uniform;->value:Ljava/lang/Object;

    instance-of v0, v0, Lcom/jme3/math/ColorRGBA;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/jme3/math/ColorRGBA;->BlackNoAlpha:Lcom/jme3/math/ColorRGBA;

    iput-object v0, p0, Lcom/jme3/shader/Uniform;->value:Ljava/lang/Object;

    goto :goto_1

    :cond_3
    sget-object v0, Lcom/jme3/math/Quaternion;->ZERO:Lcom/jme3/math/Quaternion;

    iput-object v0, p0, Lcom/jme3/shader/Uniform;->value:Ljava/lang/Object;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method public getBinding()Lcom/jme3/shader/UniformBinding;
    .locals 1

    iget-object v0, p0, Lcom/jme3/shader/Uniform;->binding:Lcom/jme3/shader/UniformBinding;

    return-object v0
.end method

.method public getValue()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/jme3/shader/Uniform;->value:Ljava/lang/Object;

    return-object v0
.end method

.method public getVarType()Lcom/jme3/shader/VarType;
    .locals 1

    iget-object v0, p0, Lcom/jme3/shader/Uniform;->varType:Lcom/jme3/shader/VarType;

    return-object v0
.end method

.method public isSetByCurrentMaterial()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jme3/shader/Uniform;->setByCurrentMaterial:Z

    return v0
.end method

.method public isUpdateNeeded()Z
    .locals 1

    iget-boolean v0, p0, Lcom/jme3/shader/Uniform;->updateNeeded:Z

    return v0
.end method

.method public read(Lcom/jme3/export/JmeImporter;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lcom/jme3/shader/ShaderVariable;->read(Lcom/jme3/export/JmeImporter;)V

    invoke-interface {p1, p0}, Lcom/jme3/export/JmeImporter;->getCapsule(Lcom/jme3/export/Savable;)Lcom/jme3/export/InputCapsule;

    move-result-object v1

    const-string v0, "varType"

    const-class v2, Lcom/jme3/shader/VarType;

    invoke-interface {v1, v0, v2, v3}, Lcom/jme3/export/InputCapsule;->readEnum(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/jme3/shader/VarType;

    iput-object v0, p0, Lcom/jme3/shader/Uniform;->varType:Lcom/jme3/shader/VarType;

    const-string v0, "binding"

    const-class v2, Lcom/jme3/shader/UniformBinding;

    invoke-interface {v1, v0, v2, v3}, Lcom/jme3/export/InputCapsule;->readEnum(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/jme3/shader/UniformBinding;

    iput-object v0, p0, Lcom/jme3/shader/Uniform;->binding:Lcom/jme3/shader/UniformBinding;

    sget-object v0, Lcom/jme3/shader/Uniform$1;->$SwitchMap$com$jme3$shader$VarType:[I

    iget-object v2, p0, Lcom/jme3/shader/Uniform;->varType:Lcom/jme3/shader/VarType;

    invoke-virtual {v2}, Lcom/jme3/shader/VarType;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    const-string v0, "valueBoolean"

    invoke-interface {v1, v0, v4}, Lcom/jme3/export/InputCapsule;->readBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/shader/Uniform;->value:Ljava/lang/Object;

    goto :goto_0

    :pswitch_2
    const-string v0, "valueFloat"

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/jme3/export/InputCapsule;->readFloat(Ljava/lang/String;F)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/shader/Uniform;->value:Ljava/lang/Object;

    goto :goto_0

    :pswitch_3
    const-string v0, "valueFloatArray"

    invoke-interface {v1, v0, v3}, Lcom/jme3/export/InputCapsule;->readFloatBuffer(Ljava/lang/String;Ljava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/shader/Uniform;->value:Ljava/lang/Object;

    goto :goto_0

    :pswitch_4
    const-string v0, "valueInt"

    invoke-interface {v1, v0, v4}, Lcom/jme3/export/InputCapsule;->readInt(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/shader/Uniform;->value:Ljava/lang/Object;

    goto :goto_0

    :pswitch_5
    const-string v0, "valueMatrix3"

    invoke-interface {v1, v0, v3}, Lcom/jme3/export/InputCapsule;->readFloatBuffer(Ljava/lang/String;Ljava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    iget-object v0, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    iput-object v0, p0, Lcom/jme3/shader/Uniform;->value:Ljava/lang/Object;

    goto :goto_0

    :pswitch_6
    const-string v0, "valueMatrix4"

    invoke-interface {v1, v0, v3}, Lcom/jme3/export/InputCapsule;->readFloatBuffer(Ljava/lang/String;Ljava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    iget-object v0, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    iput-object v0, p0, Lcom/jme3/shader/Uniform;->value:Ljava/lang/Object;

    goto :goto_0

    :pswitch_7
    const-string v0, "valueVector2"

    invoke-interface {v1, v0, v3}, Lcom/jme3/export/InputCapsule;->readSavable(Ljava/lang/String;Lcom/jme3/export/Savable;)Lcom/jme3/export/Savable;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/shader/Uniform;->value:Ljava/lang/Object;

    goto :goto_0

    :pswitch_8
    const-string v0, "valueVector3"

    invoke-interface {v1, v0, v3}, Lcom/jme3/export/InputCapsule;->readSavable(Ljava/lang/String;Lcom/jme3/export/Savable;)Lcom/jme3/export/Savable;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/shader/Uniform;->value:Ljava/lang/Object;

    goto :goto_0

    :pswitch_9
    const-string v0, "valueVector3Array"

    invoke-interface {v1, v0, v3}, Lcom/jme3/export/InputCapsule;->readFloatBuffer(Ljava/lang/String;Ljava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/shader/Uniform;->value:Ljava/lang/Object;

    goto :goto_0

    :pswitch_a
    const-string v0, "valueVector4"

    invoke-interface {v1, v0, v3}, Lcom/jme3/export/InputCapsule;->readSavable(Ljava/lang/String;Lcom/jme3/export/Savable;)Lcom/jme3/export/Savable;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/shader/Uniform;->value:Ljava/lang/Object;

    goto :goto_0

    :pswitch_b
    const-string v0, "valueVector4Array"

    invoke-interface {v1, v0, v3}, Lcom/jme3/export/InputCapsule;->readFloatBuffer(Ljava/lang/String;Ljava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/jme3/shader/Uniform;->value:Ljava/lang/Object;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public reset()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/jme3/shader/Uniform;->setByCurrentMaterial:Z

    const/4 v0, -0x2

    iput v0, p0, Lcom/jme3/shader/Uniform;->location:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/jme3/shader/Uniform;->updateNeeded:Z

    return-void
.end method

.method public setBinding(Lcom/jme3/shader/UniformBinding;)V
    .locals 0
    .param p1    # Lcom/jme3/shader/UniformBinding;

    iput-object p1, p0, Lcom/jme3/shader/Uniform;->binding:Lcom/jme3/shader/UniformBinding;

    return-void
.end method

.method public setValue(Lcom/jme3/shader/VarType;Ljava/lang/Object;)V
    .locals 12
    .param p1    # Lcom/jme3/shader/VarType;
    .param p2    # Ljava/lang/Object;

    const/4 v11, 0x1

    iget v9, p0, Lcom/jme3/shader/Uniform;->location:I

    const/4 v10, -0x1

    if-ne v9, v10, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v9, p0, Lcom/jme3/shader/Uniform;->varType:Lcom/jme3/shader/VarType;

    if-eqz v9, :cond_2

    iget-object v9, p0, Lcom/jme3/shader/Uniform;->varType:Lcom/jme3/shader/VarType;

    if-eq v9, p1, :cond_2

    new-instance v9, Ljava/lang/IllegalArgumentException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Expected a "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/jme3/shader/Uniform;->varType:Lcom/jme3/shader/VarType;

    invoke-virtual {v11}, Lcom/jme3/shader/VarType;->name()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " value!"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v9

    :cond_2
    if-nez p2, :cond_3

    new-instance v9, Ljava/lang/NullPointerException;

    invoke-direct {v9}, Ljava/lang/NullPointerException;-><init>()V

    throw v9

    :cond_3
    iput-boolean v11, p0, Lcom/jme3/shader/Uniform;->setByCurrentMaterial:Z

    sget-object v9, Lcom/jme3/shader/Uniform$1;->$SwitchMap$com$jme3$shader$VarType:[I

    invoke-virtual {p1}, Lcom/jme3/shader/VarType;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    :pswitch_0
    iput-object p2, p0, Lcom/jme3/shader/Uniform;->value:Ljava/lang/Object;

    :goto_1
    iget-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    if-eqz v9, :cond_4

    iget-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    iput-object v9, p0, Lcom/jme3/shader/Uniform;->value:Ljava/lang/Object;

    :cond_4
    iput-object p1, p0, Lcom/jme3/shader/Uniform;->varType:Lcom/jme3/shader/VarType;

    iput-boolean v11, p0, Lcom/jme3/shader/Uniform;->updateNeeded:Z

    goto :goto_0

    :pswitch_1
    move-object v2, p2

    check-cast v2, Lcom/jme3/math/Matrix3f;

    iget-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    if-nez v9, :cond_5

    const/16 v9, 0x9

    invoke-static {v9}, Lcom/jme3/util/BufferUtils;->createFloatBuffer(I)Ljava/nio/FloatBuffer;

    move-result-object v9

    iput-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    :cond_5
    iget-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    invoke-virtual {v2, v9, v11}, Lcom/jme3/math/Matrix3f;->fillFloatBuffer(Ljava/nio/FloatBuffer;Z)Ljava/nio/FloatBuffer;

    iget-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    invoke-virtual {v9}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    goto :goto_1

    :pswitch_2
    move-object v4, p2

    check-cast v4, Lcom/jme3/math/Matrix4f;

    iget-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    if-nez v9, :cond_6

    const/16 v9, 0x10

    invoke-static {v9}, Lcom/jme3/util/BufferUtils;->createFloatBuffer(I)Ljava/nio/FloatBuffer;

    move-result-object v9

    iput-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    :cond_6
    iget-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    invoke-virtual {v4, v9, v11}, Lcom/jme3/math/Matrix4f;->fillFloatBuffer(Ljava/nio/FloatBuffer;Z)Ljava/nio/FloatBuffer;

    iget-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    invoke-virtual {v9}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    goto :goto_1

    :pswitch_3
    check-cast p2, [F

    move-object v0, p2

    check-cast v0, [F

    iget-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    if-nez v9, :cond_7

    invoke-static {v0}, Lcom/jme3/util/BufferUtils;->createFloatBuffer([F)Ljava/nio/FloatBuffer;

    move-result-object v9

    iput-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    :goto_2
    iget-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    invoke-virtual {v9, v0}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    iget-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    invoke-virtual {v9}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    goto :goto_1

    :cond_7
    iget-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    array-length v10, v0

    invoke-static {v9, v10}, Lcom/jme3/util/BufferUtils;->ensureLargeEnough(Ljava/nio/FloatBuffer;I)Ljava/nio/FloatBuffer;

    move-result-object v9

    iput-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    goto :goto_2

    :pswitch_4
    check-cast p2, [Lcom/jme3/math/Vector2f;

    move-object v6, p2

    check-cast v6, [Lcom/jme3/math/Vector2f;

    iget-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    if-nez v9, :cond_8

    invoke-static {v6}, Lcom/jme3/util/BufferUtils;->createFloatBuffer([Lcom/jme3/math/Vector2f;)Ljava/nio/FloatBuffer;

    move-result-object v9

    iput-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    :goto_3
    const/4 v1, 0x0

    :goto_4
    array-length v9, v6

    if-ge v1, v9, :cond_9

    aget-object v9, v6, v1

    iget-object v10, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    invoke-static {v9, v10, v1}, Lcom/jme3/util/BufferUtils;->setInBuffer(Lcom/jme3/math/Vector2f;Ljava/nio/FloatBuffer;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_8
    iget-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    array-length v10, v6

    mul-int/lit8 v10, v10, 0x2

    invoke-static {v9, v10}, Lcom/jme3/util/BufferUtils;->ensureLargeEnough(Ljava/nio/FloatBuffer;I)Ljava/nio/FloatBuffer;

    move-result-object v9

    iput-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    goto :goto_3

    :cond_9
    iget-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    invoke-virtual {v9}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    goto/16 :goto_1

    :pswitch_5
    check-cast p2, [Lcom/jme3/math/Vector3f;

    move-object v7, p2

    check-cast v7, [Lcom/jme3/math/Vector3f;

    iget-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    if-nez v9, :cond_a

    invoke-static {v7}, Lcom/jme3/util/BufferUtils;->createFloatBuffer([Lcom/jme3/math/Vector3f;)Ljava/nio/FloatBuffer;

    move-result-object v9

    iput-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    :goto_5
    const/4 v1, 0x0

    :goto_6
    array-length v9, v7

    if-ge v1, v9, :cond_b

    aget-object v9, v7, v1

    iget-object v10, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    invoke-static {v9, v10, v1}, Lcom/jme3/util/BufferUtils;->setInBuffer(Lcom/jme3/math/Vector3f;Ljava/nio/FloatBuffer;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_a
    iget-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    array-length v10, v7

    mul-int/lit8 v10, v10, 0x3

    invoke-static {v9, v10}, Lcom/jme3/util/BufferUtils;->ensureLargeEnough(Ljava/nio/FloatBuffer;I)Ljava/nio/FloatBuffer;

    move-result-object v9

    iput-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    goto :goto_5

    :cond_b
    iget-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    invoke-virtual {v9}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    goto/16 :goto_1

    :pswitch_6
    check-cast p2, [Lcom/jme3/math/Quaternion;

    move-object v8, p2

    check-cast v8, [Lcom/jme3/math/Quaternion;

    iget-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    if-nez v9, :cond_c

    invoke-static {v8}, Lcom/jme3/util/BufferUtils;->createFloatBuffer([Lcom/jme3/math/Quaternion;)Ljava/nio/FloatBuffer;

    move-result-object v9

    iput-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    :goto_7
    const/4 v1, 0x0

    :goto_8
    array-length v9, v8

    if-ge v1, v9, :cond_d

    aget-object v9, v8, v1

    iget-object v10, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    invoke-static {v9, v10, v1}, Lcom/jme3/util/BufferUtils;->setInBuffer(Lcom/jme3/math/Quaternion;Ljava/nio/FloatBuffer;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    :cond_c
    iget-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    array-length v10, v8

    mul-int/lit8 v10, v10, 0x4

    invoke-static {v9, v10}, Lcom/jme3/util/BufferUtils;->ensureLargeEnough(Ljava/nio/FloatBuffer;I)Ljava/nio/FloatBuffer;

    move-result-object v9

    iput-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    goto :goto_7

    :cond_d
    iget-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    invoke-virtual {v9}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    goto/16 :goto_1

    :pswitch_7
    check-cast p2, [Lcom/jme3/math/Matrix3f;

    move-object v3, p2

    check-cast v3, [Lcom/jme3/math/Matrix3f;

    iget-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    if-nez v9, :cond_e

    array-length v9, v3

    mul-int/lit8 v9, v9, 0x9

    invoke-static {v9}, Lcom/jme3/util/BufferUtils;->createFloatBuffer(I)Ljava/nio/FloatBuffer;

    move-result-object v9

    iput-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    :goto_9
    const/4 v1, 0x0

    :goto_a
    array-length v9, v3

    if-ge v1, v9, :cond_f

    aget-object v9, v3, v1

    iget-object v10, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    invoke-virtual {v9, v10, v11}, Lcom/jme3/math/Matrix3f;->fillFloatBuffer(Ljava/nio/FloatBuffer;Z)Ljava/nio/FloatBuffer;

    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    :cond_e
    iget-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    array-length v10, v3

    mul-int/lit8 v10, v10, 0x9

    invoke-static {v9, v10}, Lcom/jme3/util/BufferUtils;->ensureLargeEnough(Ljava/nio/FloatBuffer;I)Ljava/nio/FloatBuffer;

    move-result-object v9

    iput-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    goto :goto_9

    :cond_f
    iget-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    invoke-virtual {v9}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    goto/16 :goto_1

    :pswitch_8
    check-cast p2, [Lcom/jme3/math/Matrix4f;

    move-object v5, p2

    check-cast v5, [Lcom/jme3/math/Matrix4f;

    iget-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    if-nez v9, :cond_10

    array-length v9, v5

    mul-int/lit8 v9, v9, 0x10

    invoke-static {v9}, Lcom/jme3/util/BufferUtils;->createFloatBuffer(I)Ljava/nio/FloatBuffer;

    move-result-object v9

    iput-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    :goto_b
    const/4 v1, 0x0

    :goto_c
    array-length v9, v5

    if-ge v1, v9, :cond_11

    aget-object v9, v5, v1

    iget-object v10, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    invoke-virtual {v9, v10, v11}, Lcom/jme3/math/Matrix4f;->fillFloatBuffer(Ljava/nio/FloatBuffer;Z)Ljava/nio/FloatBuffer;

    add-int/lit8 v1, v1, 0x1

    goto :goto_c

    :cond_10
    iget-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    array-length v10, v5

    mul-int/lit8 v10, v10, 0x10

    invoke-static {v9, v10}, Lcom/jme3/util/BufferUtils;->ensureLargeEnough(Ljava/nio/FloatBuffer;I)Ljava/nio/FloatBuffer;

    move-result-object v9

    iput-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    goto :goto_b

    :cond_11
    iget-object v9, p0, Lcom/jme3/shader/Uniform;->multiData:Ljava/nio/FloatBuffer;

    invoke-virtual {v9}, Ljava/nio/FloatBuffer;->clear()Ljava/nio/Buffer;

    goto/16 :goto_1

    :pswitch_9
    iget-object v9, p0, Lcom/jme3/shader/Uniform;->value:Ljava/lang/Object;

    if-eqz v9, :cond_12

    iget-object v9, p0, Lcom/jme3/shader/Uniform;->value:Ljava/lang/Object;

    invoke-virtual {v9, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    :cond_12
    iput-object p2, p0, Lcom/jme3/shader/Uniform;->value:Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_9
        :pswitch_3
        :pswitch_9
        :pswitch_1
        :pswitch_7
        :pswitch_8
        :pswitch_4
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method

.method public setVector4InArray(FFFFI)V
    .locals 4
    .param p1    # F
    .param p2    # F
    .param p3    # F
    .param p4    # F
    .param p5    # I

    const/4 v3, 0x1

    iget v1, p0, Lcom/jme3/shader/Uniform;->location:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/jme3/shader/Uniform;->varType:Lcom/jme3/shader/VarType;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/jme3/shader/Uniform;->varType:Lcom/jme3/shader/VarType;

    sget-object v2, Lcom/jme3/shader/VarType;->Vector4Array:Lcom/jme3/shader/VarType;

    if-eq v1, v2, :cond_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Expected a "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/jme3/shader/Uniform;->varType:Lcom/jme3/shader/VarType;

    invoke-virtual {v3}, Lcom/jme3/shader/VarType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " value!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    iget-object v0, p0, Lcom/jme3/shader/Uniform;->value:Ljava/lang/Object;

    check-cast v0, Ljava/nio/FloatBuffer;

    mul-int/lit8 v1, p5, 0x4

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    invoke-virtual {v0, p1}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->rewind()Ljava/nio/Buffer;

    iput-boolean v3, p0, Lcom/jme3/shader/Uniform;->updateNeeded:Z

    iput-boolean v3, p0, Lcom/jme3/shader/Uniform;->setByCurrentMaterial:Z

    goto :goto_0
.end method

.method public setVector4Length(I)V
    .locals 4
    .param p1    # I

    const/4 v3, 0x1

    iget v1, p0, Lcom/jme3/shader/Uniform;->location:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jme3/shader/Uniform;->value:Ljava/lang/Object;

    check-cast v0, Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/nio/FloatBuffer;->capacity()I

    move-result v1

    if-ge v1, p1, :cond_2

    :cond_1
    mul-int/lit8 v1, p1, 0x4

    invoke-static {v1}, Lcom/jme3/util/BufferUtils;->createFloatBuffer(I)Ljava/nio/FloatBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/jme3/shader/Uniform;->value:Ljava/lang/Object;

    :cond_2
    sget-object v1, Lcom/jme3/shader/VarType;->Vector4Array:Lcom/jme3/shader/VarType;

    iput-object v1, p0, Lcom/jme3/shader/Uniform;->varType:Lcom/jme3/shader/VarType;

    iput-boolean v3, p0, Lcom/jme3/shader/Uniform;->updateNeeded:Z

    iput-boolean v3, p0, Lcom/jme3/shader/Uniform;->setByCurrentMaterial:Z

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/jme3/shader/Uniform;->name:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v1, "Uniform[name="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jme3/shader/Uniform;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jme3/shader/Uniform;->varType:Lcom/jme3/shader/VarType;

    if-eqz v1, :cond_1

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jme3/shader/Uniform;->varType:Lcom/jme3/shader/VarType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/jme3/shader/Uniform;->value:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    :goto_0
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_1
    const-string v1, ", value=<not set>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
