.class public Lcom/jme3/system/android/OGLESContext;
.super Ljava/lang/Object;
.source "OGLESContext.java"

# interfaces
.implements Landroid/opengl/GLSurfaceView$Renderer;
.implements Lcom/jme3/input/SoftTextDialogInput;
.implements Lcom/jme3/system/JmeContext;


# static fields
.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field private final ESCAPE_EVENT:Ljava/lang/String;

.field protected autoFlush:Z

.field protected clientOpenGLESVersion:I

.field protected final created:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private firstDrawFrame:Z

.field protected listener:Lcom/jme3/system/SystemListener;

.field protected minFrameDuration:I

.field protected final needClose:Ljava/util/concurrent/atomic/AtomicBoolean;

.field protected final renderable:Ljava/util/concurrent/atomic/AtomicBoolean;

.field protected renderer:Lcom/jme3/renderer/android/OGLESShaderRenderer;

.field protected final settings:Lcom/jme3/system/AppSettings;

.field protected timer:Lcom/jme3/system/Timer;

.field protected verboseLogging:Z

.field protected view:Lcom/jme3/input/android/AndroidInput;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/jme3/system/android/OGLESContext;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/jme3/system/android/OGLESContext;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/jme3/system/android/OGLESContext;->created:Ljava/util/concurrent/atomic/AtomicBoolean;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/jme3/system/android/OGLESContext;->renderable:Ljava/util/concurrent/atomic/AtomicBoolean;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/jme3/system/android/OGLESContext;->needClose:Ljava/util/concurrent/atomic/AtomicBoolean;

    new-instance v0, Lcom/jme3/system/AppSettings;

    invoke-direct {v0, v2}, Lcom/jme3/system/AppSettings;-><init>(Z)V

    iput-object v0, p0, Lcom/jme3/system/android/OGLESContext;->settings:Lcom/jme3/system/AppSettings;

    iput-boolean v2, p0, Lcom/jme3/system/android/OGLESContext;->autoFlush:Z

    iput-boolean v2, p0, Lcom/jme3/system/android/OGLESContext;->firstDrawFrame:Z

    iput v1, p0, Lcom/jme3/system/android/OGLESContext;->minFrameDuration:I

    iput v2, p0, Lcom/jme3/system/android/OGLESContext;->clientOpenGLESVersion:I

    iput-boolean v1, p0, Lcom/jme3/system/android/OGLESContext;->verboseLogging:Z

    const-string v0, "TouchEscape"

    iput-object v0, p0, Lcom/jme3/system/android/OGLESContext;->ESCAPE_EVENT:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public create(Z)V
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/jme3/system/android/OGLESContext;->waitFor(Z)V

    :cond_0
    return-void
.end method

.method public createView(Lcom/jme3/input/android/AndroidInput;Lcom/jme3/system/android/AndroidConfigChooser$ConfigType;Z)Landroid/opengl/GLSurfaceView;
    .locals 11
    .param p1    # Lcom/jme3/input/android/AndroidInput;
    .param p2    # Lcom/jme3/system/android/AndroidConfigChooser$ConfigType;
    .param p3    # Z

    iput-object p1, p0, Lcom/jme3/system/android/OGLESContext;->view:Lcom/jme3/input/android/AndroidInput;

    iput-boolean p3, p0, Lcom/jme3/system/android/OGLESContext;->verboseLogging:Z

    sget-object v0, Lcom/jme3/system/android/AndroidConfigChooser$ConfigType;->LEGACY:Lcom/jme3/system/android/AndroidConfigChooser$ConfigType;

    if-ne p2, v0, :cond_2

    const/4 v0, 0x2

    iput v0, p0, Lcom/jme3/system/android/OGLESContext;->clientOpenGLESVersion:I

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/jme3/input/android/AndroidInput;->setEGLContextClientVersion(I)V

    const/4 v1, 0x5

    const/4 v2, 0x6

    const/4 v3, 0x5

    const/4 v4, 0x0

    const/16 v5, 0x10

    const/4 v6, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v6}, Lcom/jme3/input/android/AndroidInput;->setEGLConfigChooser(IIIIII)V

    sget-object v0, Lcom/jme3/system/android/OGLESContext;->logger:Ljava/util/logging/Logger;

    const-string v1, "ConfigType.LEGACY using RGB565"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_0
    :goto_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/jme3/input/android/AndroidInput;->setFocusableInTouchMode(Z)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/jme3/input/android/AndroidInput;->setFocusable(Z)V

    invoke-virtual {p1}, Lcom/jme3/input/android/AndroidInput;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    invoke-virtual {p1, p0}, Lcom/jme3/input/android/AndroidInput;->setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V

    :cond_1
    :goto_1
    return-object p1

    :cond_2
    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v9

    check-cast v9, Ljavax/microedition/khronos/egl/EGL10;

    sget-object v0, Ljavax/microedition/khronos/egl/EGL10;->EGL_DEFAULT_DISPLAY:Ljava/lang/Object;

    invoke-interface {v9, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetDisplay(Ljava/lang/Object;)Ljavax/microedition/khronos/egl/EGLDisplay;

    move-result-object v8

    const/4 v0, 0x2

    new-array v10, v0, [I

    invoke-interface {v9, v8, v10}, Ljavax/microedition/khronos/egl/EGL10;->eglInitialize(Ljavax/microedition/khronos/egl/EGLDisplay;[I)Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    sget-object v0, Lcom/jme3/system/android/OGLESContext;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->INFO:Ljava/util/logging/Level;

    const-string v2, "Display EGL Version: {0}.{1}"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const/4 v5, 0x0

    aget v5, v10, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const/4 v5, 0x1

    aget v5, v10, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_3
    :try_start_0
    new-instance v7, Lcom/jme3/system/android/AndroidConfigChooser;

    invoke-direct {v7, p2}, Lcom/jme3/system/android/AndroidConfigChooser;-><init>(Lcom/jme3/system/android/AndroidConfigChooser$ConfigType;)V

    invoke-virtual {v7, v9, v8}, Lcom/jme3/system/android/AndroidConfigChooser;->findConfig(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/jme3/system/android/OGLESContext;->listener:Lcom/jme3/system/SystemListener;

    const-string v1, "Unable to find suitable EGL config"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/jme3/system/SystemListener;->handleError(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 p1, 0x0

    if-eqz v8, :cond_1

    invoke-interface {v9, v8}, Ljavax/microedition/khronos/egl/EGL10;->eglTerminate(Ljavax/microedition/khronos/egl/EGLDisplay;)Z

    goto :goto_1

    :cond_4
    :try_start_1
    invoke-virtual {v7}, Lcom/jme3/system/android/AndroidConfigChooser;->getClientOpenGLESVersion()I

    move-result v0

    iput v0, p0, Lcom/jme3/system/android/OGLESContext;->clientOpenGLESVersion:I

    iget v0, p0, Lcom/jme3/system/android/OGLESContext;->clientOpenGLESVersion:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_5

    iget-object v0, p0, Lcom/jme3/system/android/OGLESContext;->listener:Lcom/jme3/system/SystemListener;

    const-string v1, "OpenGL ES 2.0 is not supported on this device"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/jme3/system/SystemListener;->handleError(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 p1, 0x0

    if-eqz v8, :cond_1

    invoke-interface {v9, v8}, Ljavax/microedition/khronos/egl/EGL10;->eglTerminate(Ljavax/microedition/khronos/egl/EGLDisplay;)Z

    goto :goto_1

    :cond_5
    :try_start_2
    iget v0, p0, Lcom/jme3/system/android/OGLESContext;->clientOpenGLESVersion:I

    invoke-virtual {p1, v0}, Lcom/jme3/input/android/AndroidInput;->setEGLContextClientVersion(I)V

    invoke-virtual {p1, v7}, Lcom/jme3/input/android/AndroidInput;->setEGLConfigChooser(Landroid/opengl/GLSurfaceView$EGLConfigChooser;)V

    invoke-virtual {p1}, Lcom/jme3/input/android/AndroidInput;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-virtual {v7}, Lcom/jme3/system/android/AndroidConfigChooser;->getPixelFormat()I

    move-result v1

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setFormat(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v8, :cond_0

    invoke-interface {v9, v8}, Ljavax/microedition/khronos/egl/EGL10;->eglTerminate(Ljavax/microedition/khronos/egl/EGLDisplay;)Z

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    if-eqz v8, :cond_6

    invoke-interface {v9, v8}, Ljavax/microedition/khronos/egl/EGL10;->eglTerminate(Ljavax/microedition/khronos/egl/EGLDisplay;)Z

    :cond_6
    throw v0
.end method

.method protected deinitInThread()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/jme3/system/android/OGLESContext;->renderable:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/jme3/system/android/OGLESContext;->created:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v2, p0, Lcom/jme3/system/android/OGLESContext;->renderer:Lcom/jme3/renderer/android/OGLESShaderRenderer;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/jme3/system/android/OGLESContext;->renderer:Lcom/jme3/renderer/android/OGLESShaderRenderer;

    invoke-virtual {v2}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->cleanup()V

    :cond_0
    iget-object v2, p0, Lcom/jme3/system/android/OGLESContext;->listener:Lcom/jme3/system/SystemListener;

    invoke-interface {v2}, Lcom/jme3/system/SystemListener;->destroy()V

    iput-object v3, p0, Lcom/jme3/system/android/OGLESContext;->listener:Lcom/jme3/system/SystemListener;

    iput-object v3, p0, Lcom/jme3/system/android/OGLESContext;->renderer:Lcom/jme3/renderer/android/OGLESShaderRenderer;

    iput-object v3, p0, Lcom/jme3/system/android/OGLESContext;->timer:Lcom/jme3/system/Timer;

    sget-object v2, Lcom/jme3/system/android/OGLESContext;->logger:Ljava/util/logging/Logger;

    const-string v3, "Display destroyed."

    invoke-virtual {v2, v3}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/jme3/system/android/OGLESContext;->renderable:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v2, p0, Lcom/jme3/system/android/OGLESContext;->view:Lcom/jme3/input/android/AndroidInput;

    invoke-virtual {v2}, Lcom/jme3/input/android/AndroidInput;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v2, v0, Lcom/jme3/app/AndroidHarness;

    if-eqz v2, :cond_1

    move-object v1, v0

    check-cast v1, Lcom/jme3/app/AndroidHarness;

    invoke-virtual {v1}, Lcom/jme3/app/AndroidHarness;->isFinishOnAppStop()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/jme3/app/AndroidHarness;->finish()V

    :cond_1
    return-void
.end method

.method public destroy(Z)V
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/jme3/system/android/OGLESContext;->needClose:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/jme3/system/android/OGLESContext;->waitFor(Z)V

    :cond_0
    return-void
.end method

.method public getJoyInput()Lcom/jme3/input/JoyInput;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getKeyInput()Lcom/jme3/input/KeyInput;
    .locals 1

    new-instance v0, Lcom/jme3/input/dummy/DummyKeyInput;

    invoke-direct {v0}, Lcom/jme3/input/dummy/DummyKeyInput;-><init>()V

    return-object v0
.end method

.method public getMouseInput()Lcom/jme3/input/MouseInput;
    .locals 1

    new-instance v0, Lcom/jme3/input/dummy/DummyMouseInput;

    invoke-direct {v0}, Lcom/jme3/input/dummy/DummyMouseInput;-><init>()V

    return-object v0
.end method

.method public getRenderer()Lcom/jme3/renderer/Renderer;
    .locals 1

    iget-object v0, p0, Lcom/jme3/system/android/OGLESContext;->renderer:Lcom/jme3/renderer/android/OGLESShaderRenderer;

    return-object v0
.end method

.method public getSettings()Lcom/jme3/system/AppSettings;
    .locals 1

    iget-object v0, p0, Lcom/jme3/system/android/OGLESContext;->settings:Lcom/jme3/system/AppSettings;

    return-object v0
.end method

.method public getTimer()Lcom/jme3/system/Timer;
    .locals 1

    iget-object v0, p0, Lcom/jme3/system/android/OGLESContext;->timer:Lcom/jme3/system/Timer;

    return-object v0
.end method

.method public getTouchInput()Lcom/jme3/input/TouchInput;
    .locals 1

    iget-object v0, p0, Lcom/jme3/system/android/OGLESContext;->view:Lcom/jme3/input/android/AndroidInput;

    return-object v0
.end method

.method public getType()Lcom/jme3/system/JmeContext$Type;
    .locals 1

    sget-object v0, Lcom/jme3/system/JmeContext$Type;->Display:Lcom/jme3/system/JmeContext$Type;

    return-object v0
.end method

.method protected initInThread()V
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x1

    iget-object v2, p0, Lcom/jme3/system/android/OGLESContext;->created:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2, v7}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    sget-object v2, Lcom/jme3/system/android/OGLESContext;->logger:Ljava/util/logging/Logger;

    const-string v3, "OGLESContext create"

    invoke-virtual {v2, v3}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    sget-object v2, Lcom/jme3/system/android/OGLESContext;->logger:Ljava/util/logging/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Running on thread: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/jme3/system/android/OGLESContext;->view:Lcom/jme3/input/android/AndroidInput;

    invoke-virtual {v2}, Lcom/jme3/input/android/AndroidInput;->getContext()Landroid/content/Context;

    move-result-object v1

    instance-of v2, v1, Lcom/jme3/app/AndroidHarness;

    if-eqz v2, :cond_0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    new-instance v3, Lcom/jme3/system/android/OGLESContext$1;

    invoke-direct {v3, p0, v1}, Lcom/jme3/system/android/OGLESContext$1;-><init>(Lcom/jme3/system/android/OGLESContext;Landroid/content/Context;)V

    invoke-virtual {v2, v3}, Ljava/lang/Thread;->setUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    :goto_0
    iget v2, p0, Lcom/jme3/system/android/OGLESContext;->clientOpenGLESVersion:I

    const/4 v3, 0x2

    if-ge v2, v3, :cond_1

    new-instance v2, Ljava/lang/UnsupportedOperationException;

    const-string v3, "OpenGL ES 2.0 is not supported on this device"

    invoke-direct {v2, v3}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    new-instance v3, Lcom/jme3/system/android/OGLESContext$2;

    invoke-direct {v3, p0}, Lcom/jme3/system/android/OGLESContext$2;-><init>(Lcom/jme3/system/android/OGLESContext;)V

    invoke-virtual {v2, v3}, Ljava/lang/Thread;->setUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    goto :goto_0

    :cond_1
    new-instance v2, Lcom/jme3/system/android/AndroidTimer;

    invoke-direct {v2}, Lcom/jme3/system/android/AndroidTimer;-><init>()V

    iput-object v2, p0, Lcom/jme3/system/android/OGLESContext;->timer:Lcom/jme3/system/Timer;

    new-instance v2, Lcom/jme3/renderer/android/OGLESShaderRenderer;

    invoke-direct {v2}, Lcom/jme3/renderer/android/OGLESShaderRenderer;-><init>()V

    iput-object v2, p0, Lcom/jme3/system/android/OGLESContext;->renderer:Lcom/jme3/renderer/android/OGLESShaderRenderer;

    iget-object v2, p0, Lcom/jme3/system/android/OGLESContext;->renderer:Lcom/jme3/renderer/android/OGLESShaderRenderer;

    invoke-virtual {v2, v7}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->setUseVA(Z)V

    iget-object v2, p0, Lcom/jme3/system/android/OGLESContext;->renderer:Lcom/jme3/renderer/android/OGLESShaderRenderer;

    iget-boolean v3, p0, Lcom/jme3/system/android/OGLESContext;->verboseLogging:Z

    invoke-virtual {v2, v3}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->setVerboseLogging(Z)V

    iget-object v2, p0, Lcom/jme3/system/android/OGLESContext;->renderer:Lcom/jme3/renderer/android/OGLESShaderRenderer;

    invoke-virtual {v2}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->initialize()V

    iget-object v2, p0, Lcom/jme3/system/android/OGLESContext;->listener:Lcom/jme3/system/SystemListener;

    invoke-interface {v2}, Lcom/jme3/system/SystemListener;->initialize()V

    instance-of v2, v1, Lcom/jme3/app/AndroidHarness;

    if-eqz v2, :cond_2

    move-object v2, v1

    check-cast v2, Lcom/jme3/app/AndroidHarness;

    invoke-virtual {v2}, Lcom/jme3/app/AndroidHarness;->getJmeApplication()Lcom/jme3/app/Application;

    move-result-object v0

    invoke-virtual {v0}, Lcom/jme3/app/Application;->getInputManager()Lcom/jme3/input/InputManager;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/jme3/app/Application;->getInputManager()Lcom/jme3/input/InputManager;

    move-result-object v2

    const-string v3, "TouchEscape"

    new-array v4, v7, [Lcom/jme3/input/controls/Trigger;

    new-instance v5, Lcom/jme3/input/controls/TouchTrigger;

    const/4 v6, 0x4

    invoke-direct {v5, v6}, Lcom/jme3/input/controls/TouchTrigger;-><init>(I)V

    aput-object v5, v4, v8

    invoke-virtual {v2, v3, v4}, Lcom/jme3/input/InputManager;->addMapping(Ljava/lang/String;[Lcom/jme3/input/controls/Trigger;)V

    invoke-virtual {v0}, Lcom/jme3/app/Application;->getInputManager()Lcom/jme3/input/InputManager;

    move-result-object v2

    check-cast v1, Lcom/jme3/app/AndroidHarness;

    new-array v3, v7, [Ljava/lang/String;

    const-string v4, "TouchEscape"

    aput-object v4, v3, v8

    invoke-virtual {v2, v1, v3}, Lcom/jme3/input/InputManager;->addListener(Lcom/jme3/input/controls/InputListener;[Ljava/lang/String;)V

    :cond_2
    invoke-static {p0}, Lcom/jme3/system/JmeSystem;->setSoftTextDialogInput(Lcom/jme3/input/SoftTextDialogInput;)V

    iget-object v2, p0, Lcom/jme3/system/android/OGLESContext;->needClose:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2, v8}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v2, p0, Lcom/jme3/system/android/OGLESContext;->renderable:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2, v7}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    return-void
.end method

.method public isCreated()Z
    .locals 1

    iget-object v0, p0, Lcom/jme3/system/android/OGLESContext;->created:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public isRenderable()Z
    .locals 1

    iget-object v0, p0, Lcom/jme3/system/android/OGLESContext;->renderable:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 7
    .param p1    # Ljavax/microedition/khronos/opengles/GL10;

    iget-object v5, p0, Lcom/jme3/system/android/OGLESContext;->needClose:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {p0}, Lcom/jme3/system/android/OGLESContext;->deinitInThread()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v5, p0, Lcom/jme3/system/android/OGLESContext;->renderable:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/jme3/system/android/OGLESContext;->created:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v5

    if-nez v5, :cond_2

    new-instance v5, Ljava/lang/IllegalStateException;

    const-string v6, "onDrawFrame without create"

    invoke-direct {v5, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    iget-object v5, p0, Lcom/jme3/system/android/OGLESContext;->listener:Lcom/jme3/system/SystemListener;

    invoke-interface {v5}, Lcom/jme3/system/SystemListener;->update()V

    iget-boolean v5, p0, Lcom/jme3/system/android/OGLESContext;->firstDrawFrame:Z

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/jme3/system/android/OGLESContext;->view:Lcom/jme3/input/android/AndroidInput;

    invoke-virtual {v5}, Lcom/jme3/input/android/AndroidInput;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v5, v0, Lcom/jme3/app/AndroidHarness;

    if-eqz v5, :cond_3

    check-cast v0, Lcom/jme3/app/AndroidHarness;

    invoke-virtual {v0}, Lcom/jme3/app/AndroidHarness;->removeSplashScreen()V

    :cond_3
    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/jme3/system/android/OGLESContext;->firstDrawFrame:Z

    :cond_4
    iget-boolean v5, p0, Lcom/jme3/system/android/OGLESContext;->autoFlush:Z

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/jme3/system/android/OGLESContext;->renderer:Lcom/jme3/renderer/android/OGLESShaderRenderer;

    invoke-virtual {v5}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->onFrame()V

    :cond_5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sub-long v1, v5, v3

    iget v5, p0, Lcom/jme3/system/android/OGLESContext;->minFrameDuration:I

    int-to-long v5, v5

    cmp-long v5, v1, v5

    if-gez v5, :cond_0

    :try_start_0
    iget v5, p0, Lcom/jme3/system/android/OGLESContext;->minFrameDuration:I

    int-to-long v5, v5

    sub-long/2addr v5, v1

    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v5

    goto :goto_0
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 3
    .param p1    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2    # I
    .param p3    # I

    sget-object v0, Lcom/jme3/system/android/OGLESContext;->logger:Ljava/util/logging/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GL Surface changed, width: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " height: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/jme3/system/android/OGLESContext;->settings:Lcom/jme3/system/AppSettings;

    invoke-virtual {v0, p2, p3}, Lcom/jme3/system/AppSettings;->setResolution(II)V

    iget-object v0, p0, Lcom/jme3/system/android/OGLESContext;->listener:Lcom/jme3/system/SystemListener;

    invoke-interface {v0, p2, p3}, Lcom/jme3/system/SystemListener;->reshape(II)V

    return-void
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 2
    .param p1    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2    # Ljavax/microedition/khronos/egl/EGLConfig;

    iget-object v0, p0, Lcom/jme3/system/android/OGLESContext;->created:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jme3/system/android/OGLESContext;->renderer:Lcom/jme3/renderer/android/OGLESShaderRenderer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/jme3/system/android/OGLESContext;->renderer:Lcom/jme3/renderer/android/OGLESShaderRenderer;

    invoke-virtual {v0}, Lcom/jme3/renderer/android/OGLESShaderRenderer;->resetGLObjects()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/jme3/system/android/OGLESContext;->created:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/jme3/system/android/OGLESContext;->logger:Ljava/util/logging/Logger;

    const-string v1, "GL Surface created, doing JME3 init"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/jme3/system/android/OGLESContext;->initInThread()V

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/jme3/system/android/OGLESContext;->logger:Ljava/util/logging/Logger;

    const-string v1, "GL Surface already created"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public restart()V
    .locals 0

    return-void
.end method

.method public setSettings(Lcom/jme3/system/AppSettings;)V
    .locals 1
    .param p1    # Lcom/jme3/system/AppSettings;

    iget-object v0, p0, Lcom/jme3/system/android/OGLESContext;->settings:Lcom/jme3/system/AppSettings;

    invoke-virtual {v0, p1}, Lcom/jme3/system/AppSettings;->copyFrom(Lcom/jme3/system/AppSettings;)V

    return-void
.end method

.method public setSystemListener(Lcom/jme3/system/SystemListener;)V
    .locals 0
    .param p1    # Lcom/jme3/system/SystemListener;

    iput-object p1, p0, Lcom/jme3/system/android/OGLESContext;->listener:Lcom/jme3/system/SystemListener;

    return-void
.end method

.method protected waitFor(Z)V
    .locals 2
    .param p1    # Z

    :goto_0
    iget-object v0, p0, Lcom/jme3/system/android/OGLESContext;->renderable:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eq v0, p1, :cond_0

    const-wide/16 v0, 0xa

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_0
    return-void
.end method
