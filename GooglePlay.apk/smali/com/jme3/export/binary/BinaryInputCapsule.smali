.class final Lcom/jme3/export/binary/BinaryInputCapsule;
.super Ljava/lang/Object;
.source "BinaryInputCapsule.java"

# interfaces
.implements Lcom/jme3/export/InputCapsule;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jme3/export/binary/BinaryInputCapsule$1;,
        Lcom/jme3/export/binary/BinaryInputCapsule$IntIDMap;,
        Lcom/jme3/export/binary/BinaryInputCapsule$StringIDMap;,
        Lcom/jme3/export/binary/BinaryInputCapsule$ID;
    }
.end annotation


# static fields
.field private static final logger:Ljava/util/logging/Logger;


# instance fields
.field protected cObj:Lcom/jme3/export/binary/BinaryClassObject;

.field protected fieldData:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Byte;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field protected importer:Lcom/jme3/export/binary/BinaryImporter;

.field protected index:I

.field protected savable:Lcom/jme3/export/Savable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/jme3/export/binary/BinaryInputCapsule;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/jme3/export/binary/BinaryInputCapsule;->logger:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/jme3/export/binary/BinaryImporter;Lcom/jme3/export/Savable;Lcom/jme3/export/binary/BinaryClassObject;)V
    .locals 1
    .param p1    # Lcom/jme3/export/binary/BinaryImporter;
    .param p2    # Lcom/jme3/export/Savable;
    .param p3    # Lcom/jme3/export/binary/BinaryClassObject;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    iput-object p1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->importer:Lcom/jme3/export/binary/BinaryImporter;

    iput-object p3, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->cObj:Lcom/jme3/export/binary/BinaryClassObject;

    iput-object p2, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->savable:Lcom/jme3/export/Savable;

    return-void
.end method

.method protected static inflateFrom([BI)[B
    .locals 5
    .param p0    # [B
    .param p1    # I

    const/4 v4, -0x1

    const/4 v3, -0x2

    aget-byte v0, p0, p1

    if-ne v0, v4, :cond_1

    invoke-static {v4}, Lcom/jme3/export/binary/ByteUtils;->convertToBytes(I)[B

    move-result-object v1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    if-ne v0, v3, :cond_2

    invoke-static {v3}, Lcom/jme3/export/binary/ByteUtils;->convertToBytes(I)[B

    move-result-object v1

    goto :goto_0

    :cond_2
    if-nez v0, :cond_3

    const/4 v3, 0x0

    new-array v1, v3, [B

    goto :goto_0

    :cond_3
    new-array v1, v0, [B

    const/4 v2, 0x0

    :goto_1
    array-length v3, v1

    if-ge v2, v3, :cond_0

    add-int/lit8 v3, v2, 0x1

    add-int/2addr v3, p1

    aget-byte v3, p0, v3

    aput-byte v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private intSavableMapFromKV([I[Lcom/jme3/export/Savable;)Lcom/jme3/util/IntMap;
    .locals 4
    .param p1    # [I
    .param p2    # [Lcom/jme3/export/Savable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I[",
            "Lcom/jme3/export/Savable;",
            ")",
            "Lcom/jme3/util/IntMap",
            "<",
            "Lcom/jme3/export/Savable;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    :cond_0
    const/4 v0, 0x0

    :cond_1
    return-object v0

    :cond_2
    new-instance v0, Lcom/jme3/util/IntMap;

    array-length v2, p1

    invoke-direct {v0, v2}, Lcom/jme3/util/IntMap;-><init>(I)V

    const/4 v1, 0x0

    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_1

    aget v2, p1, v1

    aget-object v3, p2, v1

    invoke-virtual {v0, v2, v3}, Lcom/jme3/util/IntMap;->put(ILjava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private resolveIDs([Ljava/lang/Object;)[Lcom/jme3/export/Savable;
    .locals 6
    .param p1    # [Ljava/lang/Object;

    const/4 v4, 0x0

    if-eqz p1, :cond_1

    array-length v3, p1

    new-array v2, v3, [Lcom/jme3/export/Savable;

    const/4 v0, 0x0

    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_2

    aget-object v1, p1, v0

    check-cast v1, Lcom/jme3/export/binary/BinaryInputCapsule$ID;

    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->importer:Lcom/jme3/export/binary/BinaryImporter;

    iget v5, v1, Lcom/jme3/export/binary/BinaryInputCapsule$ID;->id:I

    invoke-virtual {v3, v5}, Lcom/jme3/export/binary/BinaryImporter;->readObject(I)Lcom/jme3/export/Savable;

    move-result-object v3

    :goto_1
    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move-object v3, v4

    goto :goto_1

    :cond_1
    move-object v2, v4

    :cond_2
    return-object v2
.end method

.method private savableArrayListFromArray([Lcom/jme3/export/Savable;)Ljava/util/ArrayList;
    .locals 3
    .param p1    # [Lcom/jme3/export/Savable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/jme3/export/Savable;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/jme3/export/Savable;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_1

    const/4 v0, 0x0

    :cond_0
    return-object v0

    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    array-length v2, p1

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v1, 0x0

    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_0

    aget-object v2, p1, v1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private stringSavableMapFromKV([Ljava/lang/String;[Lcom/jme3/export/Savable;)Ljava/util/Map;
    .locals 4
    .param p1    # [Ljava/lang/String;
    .param p2    # [Lcom/jme3/export/Savable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "[",
            "Lcom/jme3/export/Savable;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/jme3/export/Savable;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    :cond_0
    const/4 v0, 0x0

    :cond_1
    return-object v0

    :cond_2
    new-instance v0, Ljava/util/HashMap;

    array-length v2, p1

    invoke-direct {v0, v2}, Ljava/util/HashMap;-><init>(I)V

    const/4 v1, 0x0

    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_1

    aget-object v2, p1, v1

    aget-object v3, p2, v1

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getSavableVersion(Ljava/lang/Class;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/jme3/export/Savable;",
            ">;)I"
        }
    .end annotation

    iget-object v0, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->savable:Lcom/jme3/export/Savable;

    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->cObj:Lcom/jme3/export/binary/BinaryClassObject;

    iget-object v1, v1, Lcom/jme3/export/binary/BinaryClassObject;->classHierarchyVersions:[I

    iget-object v2, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->importer:Lcom/jme3/export/binary/BinaryImporter;

    invoke-virtual {v2}, Lcom/jme3/export/binary/BinaryImporter;->getFormatVersion()I

    move-result v2

    invoke-static {v0, p1, v1, v2}, Lcom/jme3/export/SavableClassUtil;->getSavedSavableVersion(Ljava/lang/Object;Ljava/lang/Class;[II)I

    move-result v0

    return v0
.end method

.method protected readBitSet([B)Ljava/util/BitSet;
    .locals 4
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readInt([B)I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_1

    const/4 v1, 0x0

    :cond_0
    return-object v1

    :cond_1
    new-instance v1, Ljava/util/BitSet;

    invoke-direct {v1, v0}, Ljava/util/BitSet;-><init>(I)V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readBoolean([B)Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Ljava/util/BitSet;->set(IZ)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public readBoolean(Ljava/lang/String;Z)Z
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->cObj:Lcom/jme3/export/binary/BinaryClassObject;

    iget-object v1, v1, Lcom/jme3/export/binary/BinaryClassObject;->nameFields:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/export/binary/BinaryClassField;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v2, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return p2

    :cond_1
    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v2, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    goto :goto_0
.end method

.method protected readBoolean([B)Z
    .locals 2
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    invoke-static {p1, v1}, Lcom/jme3/export/binary/ByteUtils;->convertBooleanFromBytes([BI)Z

    move-result v0

    iget v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    return v0
.end method

.method protected readBooleanArray([B)[Z
    .locals 4
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readInt([B)I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_1

    const/4 v1, 0x0

    :cond_0
    return-object v1

    :cond_1
    new-array v1, v0, [Z

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readBoolean([B)Z

    move-result v3

    aput-boolean v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method protected readBooleanArray2D([B)[[Z
    .locals 4
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readInt([B)I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    const/4 v3, 0x0

    check-cast v3, [[Z

    :goto_0
    return-object v3

    :cond_0
    new-array v1, v0, [[Z

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readBooleanArray([B)[Z

    move-result-object v3

    aput-object v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    move-object v3, v1

    goto :goto_0
.end method

.method public readByte(Ljava/lang/String;B)B
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->cObj:Lcom/jme3/export/binary/BinaryClassObject;

    iget-object v1, v1, Lcom/jme3/export/binary/BinaryClassObject;->nameFields:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/export/binary/BinaryClassField;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v2, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return p2

    :cond_1
    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v2, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Byte;

    invoke-virtual {v1}, Ljava/lang/Byte;->byteValue()B

    move-result p2

    goto :goto_0
.end method

.method protected readByte([B)B
    .locals 2
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    aget-byte v0, p1, v1

    iget v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    return v0
.end method

.method protected readByteArray([B)[B
    .locals 4
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readInt([B)I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_1

    const/4 v1, 0x0

    :cond_0
    return-object v1

    :cond_1
    new-array v1, v0, [B

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readByte([B)B

    move-result v3

    aput-byte v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method protected readByteArray2D([B)[[B
    .locals 4
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readInt([B)I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    const/4 v3, 0x0

    check-cast v3, [[B

    :goto_0
    return-object v3

    :cond_0
    new-array v1, v0, [[B

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readByteArray([B)[B

    move-result-object v3

    aput-object v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    move-object v3, v1

    goto :goto_0
.end method

.method public readByteBuffer(Ljava/lang/String;Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/nio/ByteBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->cObj:Lcom/jme3/export/binary/BinaryClassObject;

    iget-object v1, v1, Lcom/jme3/export/binary/BinaryClassObject;->nameFields:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/export/binary/BinaryClassField;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v2, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    move-object v1, p2

    :goto_0
    return-object v1

    :cond_1
    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v2, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/nio/ByteBuffer;

    goto :goto_0
.end method

.method protected readByteBuffer([B)Ljava/nio/ByteBuffer;
    .locals 4
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readInt([B)I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-static {}, Lcom/jme3/export/binary/BinaryImporter;->canUseFastBuffers()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {v0}, Lcom/jme3/util/BufferUtils;->createByteBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    iget v3, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    invoke-virtual {v1, p1, v3, v0}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    iget v3, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    add-int/2addr v3, v0

    iput v3, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    goto :goto_0

    :cond_1
    invoke-static {v0}, Lcom/jme3/util/BufferUtils;->createByteBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v0, :cond_2

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readByteForBuffer([B)B

    move-result v3

    invoke-virtual {v1, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    goto :goto_0
.end method

.method public readByteBufferArrayList(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 3
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/nio/ByteBuffer;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/nio/ByteBuffer;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->cObj:Lcom/jme3/export/binary/BinaryClassObject;

    iget-object v1, v1, Lcom/jme3/export/binary/BinaryClassObject;->nameFields:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/export/binary/BinaryClassField;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v2, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    move-object v1, p2

    :goto_0
    return-object v1

    :cond_1
    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v2, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    goto :goto_0
.end method

.method protected readByteBufferArrayList([B)Ljava/util/ArrayList;
    .locals 4
    .param p1    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/nio/ByteBuffer;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readInt([B)I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_1

    const/4 v1, 0x0

    :cond_0
    return-object v1

    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readByteBuffer([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method protected readByteForBuffer([B)B
    .locals 2
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    aget-byte v0, p1, v1

    iget v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    return v0
.end method

.method protected readDouble([B)D
    .locals 3
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v2, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    invoke-static {p1, v2}, Lcom/jme3/export/binary/ByteUtils;->convertDoubleFromBytes([BI)D

    move-result-wide v0

    iget v2, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    add-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    return-wide v0
.end method

.method protected readDoubleArray([B)[D
    .locals 5
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readInt([B)I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_1

    const/4 v1, 0x0

    :cond_0
    return-object v1

    :cond_1
    new-array v1, v0, [D

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readDouble([B)D

    move-result-wide v3

    aput-wide v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method protected readDoubleArray2D([B)[[D
    .locals 4
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readInt([B)I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    const/4 v3, 0x0

    check-cast v3, [[D

    :goto_0
    return-object v3

    :cond_0
    new-array v1, v0, [[D

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readDoubleArray([B)[D

    move-result-object v3

    aput-object v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    move-object v3, v1

    goto :goto_0
.end method

.method public readEnum(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;
    .locals 3
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum",
            "<TT;>;>(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;TT;)TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x0

    if-eqz p3, :cond_1

    invoke-virtual {p3}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {p0, p1, v1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p2, v0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v2

    :cond_0
    return-object v2

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public readFloat(Ljava/lang/String;F)F
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->cObj:Lcom/jme3/export/binary/BinaryClassObject;

    iget-object v1, v1, Lcom/jme3/export/binary/BinaryClassObject;->nameFields:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/export/binary/BinaryClassField;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v2, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return p2

    :cond_1
    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v2, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result p2

    goto :goto_0
.end method

.method protected readFloat([B)F
    .locals 2
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    invoke-static {p1, v1}, Lcom/jme3/export/binary/ByteUtils;->convertFloatFromBytes([BI)F

    move-result v0

    iget v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    add-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    return v0
.end method

.method public readFloatArray(Ljava/lang/String;[F)[F
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # [F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->cObj:Lcom/jme3/export/binary/BinaryClassObject;

    iget-object v1, v1, Lcom/jme3/export/binary/BinaryClassObject;->nameFields:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/export/binary/BinaryClassField;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v2, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    move-object v1, p2

    :goto_0
    return-object v1

    :cond_1
    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v2, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [F

    check-cast v1, [F

    goto :goto_0
.end method

.method protected readFloatArray([B)[F
    .locals 4
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readInt([B)I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_1

    const/4 v1, 0x0

    :cond_0
    return-object v1

    :cond_1
    new-array v1, v0, [F

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readFloat([B)F

    move-result v3

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method protected readFloatArray2D([B)[[F
    .locals 4
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readInt([B)I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    const/4 v3, 0x0

    check-cast v3, [[F

    :goto_0
    return-object v3

    :cond_0
    new-array v1, v0, [[F

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readFloatArray([B)[F

    move-result-object v3

    aput-object v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    move-object v3, v1

    goto :goto_0
.end method

.method public readFloatBuffer(Ljava/lang/String;Ljava/nio/FloatBuffer;)Ljava/nio/FloatBuffer;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/nio/FloatBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->cObj:Lcom/jme3/export/binary/BinaryClassObject;

    iget-object v1, v1, Lcom/jme3/export/binary/BinaryClassObject;->nameFields:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/export/binary/BinaryClassField;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v2, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    move-object v1, p2

    :goto_0
    return-object v1

    :cond_1
    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v2, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/nio/FloatBuffer;

    goto :goto_0
.end method

.method protected readFloatBuffer([B)Ljava/nio/FloatBuffer;
    .locals 5
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readInt([B)I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-static {}, Lcom/jme3/export/binary/BinaryImporter;->canUseFastBuffers()Z

    move-result v3

    if-eqz v3, :cond_1

    mul-int/lit8 v3, v0, 0x4

    invoke-static {v3}, Lcom/jme3/util/BufferUtils;->createByteBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    iget v3, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    mul-int/lit8 v4, v0, 0x4

    invoke-virtual {v1, p1, v3, v4}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    iget v3, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    mul-int/lit8 v4, v0, 0x4

    add-int/2addr v3, v4

    iput v3, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-static {v0}, Lcom/jme3/util/BufferUtils;->createFloatBuffer(I)Ljava/nio/FloatBuffer;

    move-result-object v1

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v0, :cond_2

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readFloatForBuffer([B)F

    move-result v3

    invoke-virtual {v1, v3}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v1}, Ljava/nio/FloatBuffer;->rewind()Ljava/nio/Buffer;

    goto :goto_0
.end method

.method protected readFloatBufferArrayList([B)Ljava/util/ArrayList;
    .locals 4
    .param p1    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/nio/FloatBuffer;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readInt([B)I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_1

    const/4 v1, 0x0

    :cond_0
    return-object v1

    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readFloatBuffer([B)Ljava/nio/FloatBuffer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method protected readFloatForBuffer([B)F
    .locals 2
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readIntForBuffer([B)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v1

    return v1
.end method

.method public readInt(Ljava/lang/String;I)I
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->cObj:Lcom/jme3/export/binary/BinaryClassObject;

    iget-object v1, v1, Lcom/jme3/export/binary/BinaryClassObject;->nameFields:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/export/binary/BinaryClassField;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v2, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return p2

    :cond_1
    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v2, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result p2

    goto :goto_0
.end method

.method protected readInt([B)I
    .locals 4
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v2, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    invoke-static {p1, v2}, Lcom/jme3/export/binary/BinaryInputCapsule;->inflateFrom([BI)[B

    move-result-object v0

    iget v2, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    array-length v3, v0

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v2, v3

    iput v2, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    const/4 v2, 0x4

    invoke-static {v0, v2}, Lcom/jme3/export/binary/ByteUtils;->rightAlignBytes([BI)[B

    move-result-object v0

    invoke-static {v0}, Lcom/jme3/export/binary/ByteUtils;->convertIntFromBytes([B)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    const/4 v2, -0x2

    if-ne v1, v2, :cond_1

    :cond_0
    iget v2, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    add-int/lit8 v2, v2, -0x4

    iput v2, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    :cond_1
    return v1
.end method

.method public readIntArray(Ljava/lang/String;[I)[I
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->cObj:Lcom/jme3/export/binary/BinaryClassObject;

    iget-object v1, v1, Lcom/jme3/export/binary/BinaryClassObject;->nameFields:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/export/binary/BinaryClassField;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v2, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    move-object v1, p2

    :goto_0
    return-object v1

    :cond_1
    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v2, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    check-cast v1, [I

    goto :goto_0
.end method

.method protected readIntArray([B)[I
    .locals 4
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readInt([B)I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_1

    const/4 v1, 0x0

    :cond_0
    return-object v1

    :cond_1
    new-array v1, v0, [I

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readInt([B)I

    move-result v3

    aput v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method protected readIntArray2D([B)[[I
    .locals 4
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readInt([B)I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    const/4 v3, 0x0

    check-cast v3, [[I

    :goto_0
    return-object v3

    :cond_0
    new-array v1, v0, [[I

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readIntArray([B)[I

    move-result-object v3

    aput-object v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    move-object v3, v1

    goto :goto_0
.end method

.method public readIntBuffer(Ljava/lang/String;Ljava/nio/IntBuffer;)Ljava/nio/IntBuffer;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/nio/IntBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->cObj:Lcom/jme3/export/binary/BinaryClassObject;

    iget-object v1, v1, Lcom/jme3/export/binary/BinaryClassObject;->nameFields:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/export/binary/BinaryClassField;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v2, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    move-object v1, p2

    :goto_0
    return-object v1

    :cond_1
    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v2, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/nio/IntBuffer;

    goto :goto_0
.end method

.method protected readIntBuffer([B)Ljava/nio/IntBuffer;
    .locals 5
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readInt([B)I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-static {}, Lcom/jme3/export/binary/BinaryImporter;->canUseFastBuffers()Z

    move-result v3

    if-eqz v3, :cond_1

    mul-int/lit8 v3, v0, 0x4

    invoke-static {v3}, Lcom/jme3/util/BufferUtils;->createByteBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    iget v3, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    mul-int/lit8 v4, v0, 0x4

    invoke-virtual {v1, p1, v3, v4}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    iget v3, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    mul-int/lit8 v4, v0, 0x4

    add-int/2addr v3, v4

    iput v3, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-static {v0}, Lcom/jme3/util/BufferUtils;->createIntBuffer(I)Ljava/nio/IntBuffer;

    move-result-object v1

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v0, :cond_2

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readIntForBuffer([B)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/nio/IntBuffer;->put(I)Ljava/nio/IntBuffer;

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v1}, Ljava/nio/IntBuffer;->rewind()Ljava/nio/Buffer;

    goto :goto_0
.end method

.method protected readIntForBuffer([B)I
    .locals 3
    .param p1    # [B

    iget v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    add-int/lit8 v1, v1, 0x3

    aget-byte v1, p1, v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x18

    iget v2, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    add-int/lit8 v2, v2, 0x2

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x10

    add-int/2addr v1, v2

    iget v2, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    add-int/lit8 v2, v2, 0x1

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    add-int/2addr v1, v2

    iget v2, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    add-int v0, v1, v2

    iget v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    add-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    return v0
.end method

.method protected readIntSavableMap([B)Lcom/jme3/export/binary/BinaryInputCapsule$IntIDMap;
    .locals 6
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x0

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readInt([B)I

    move-result v0

    const/4 v5, -0x1

    if-ne v0, v5, :cond_0

    move-object v2, v4

    :goto_0
    return-object v2

    :cond_0
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readIntArray([B)[I

    move-result-object v1

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readSavableArray([B)[Lcom/jme3/export/binary/BinaryInputCapsule$ID;

    move-result-object v3

    new-instance v2, Lcom/jme3/export/binary/BinaryInputCapsule$IntIDMap;

    invoke-direct {v2, v4}, Lcom/jme3/export/binary/BinaryInputCapsule$IntIDMap;-><init>(Lcom/jme3/export/binary/BinaryInputCapsule$1;)V

    iput-object v1, v2, Lcom/jme3/export/binary/BinaryInputCapsule$IntIDMap;->keys:[I

    iput-object v3, v2, Lcom/jme3/export/binary/BinaryInputCapsule$IntIDMap;->values:[Lcom/jme3/export/binary/BinaryInputCapsule$ID;

    goto :goto_0
.end method

.method public readIntSavableMap(Ljava/lang/String;Lcom/jme3/util/IntMap;)Lcom/jme3/util/IntMap;
    .locals 6
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/jme3/util/IntMap",
            "<+",
            "Lcom/jme3/export/Savable;",
            ">;)",
            "Lcom/jme3/util/IntMap",
            "<+",
            "Lcom/jme3/export/Savable;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v4, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->cObj:Lcom/jme3/export/binary/BinaryClassObject;

    iget-object v4, v4, Lcom/jme3/export/binary/BinaryClassObject;->nameFields:Ljava/util/HashMap;

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/export/binary/BinaryClassField;

    if-eqz v0, :cond_0

    iget-object v4, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v5, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    move-object v2, p2

    :goto_0
    return-object v2

    :cond_1
    iget-object v4, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v5, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    instance-of v4, v2, Lcom/jme3/export/binary/BinaryInputCapsule$IntIDMap;

    if-eqz v4, :cond_2

    move-object v1, v2

    check-cast v1, Lcom/jme3/export/binary/BinaryInputCapsule$IntIDMap;

    iget-object v4, v1, Lcom/jme3/export/binary/BinaryInputCapsule$IntIDMap;->values:[Lcom/jme3/export/binary/BinaryInputCapsule$ID;

    invoke-direct {p0, v4}, Lcom/jme3/export/binary/BinaryInputCapsule;->resolveIDs([Ljava/lang/Object;)[Lcom/jme3/export/Savable;

    move-result-object v3

    iget-object v4, v1, Lcom/jme3/export/binary/BinaryInputCapsule$IntIDMap;->keys:[I

    invoke-direct {p0, v4, v3}, Lcom/jme3/export/binary/BinaryInputCapsule;->intSavableMapFromKV([I[Lcom/jme3/export/Savable;)Lcom/jme3/util/IntMap;

    move-result-object v2

    iget-object v4, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v5, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    invoke-virtual {v4, v5, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    check-cast v2, Lcom/jme3/util/IntMap;

    goto :goto_0
.end method

.method public readLong(Ljava/lang/String;J)J
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->cObj:Lcom/jme3/export/binary/BinaryClassObject;

    iget-object v1, v1, Lcom/jme3/export/binary/BinaryClassObject;->nameFields:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/export/binary/BinaryClassField;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v2, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-wide p2

    :cond_1
    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v2, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide p2

    goto :goto_0
.end method

.method protected readLong([B)J
    .locals 5
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v3, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    invoke-static {p1, v3}, Lcom/jme3/export/binary/BinaryInputCapsule;->inflateFrom([BI)[B

    move-result-object v0

    iget v3, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    array-length v4, v0

    add-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    iput v3, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    const/16 v3, 0x8

    invoke-static {v0, v3}, Lcom/jme3/export/binary/ByteUtils;->rightAlignBytes([BI)[B

    move-result-object v0

    invoke-static {v0}, Lcom/jme3/export/binary/ByteUtils;->convertLongFromBytes([B)J

    move-result-wide v1

    return-wide v1
.end method

.method protected readLongArray([B)[J
    .locals 5
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readInt([B)I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_1

    const/4 v1, 0x0

    :cond_0
    return-object v1

    :cond_1
    new-array v1, v0, [J

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readLong([B)J

    move-result-wide v3

    aput-wide v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method protected readLongArray2D([B)[[J
    .locals 4
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readInt([B)I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    const/4 v3, 0x0

    check-cast v3, [[J

    :goto_0
    return-object v3

    :cond_0
    new-array v1, v0, [[J

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readLongArray([B)[J

    move-result-object v3

    aput-object v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    move-object v3, v1

    goto :goto_0
.end method

.method public readSavable(Ljava/lang/String;Lcom/jme3/export/Savable;)Lcom/jme3/export/Savable;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/jme3/export/Savable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v2, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->cObj:Lcom/jme3/export/binary/BinaryClassObject;

    iget-object v2, v2, Lcom/jme3/export/binary/BinaryClassObject;->nameFields:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/export/binary/BinaryClassField;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v3, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-object p2

    :cond_1
    iget-object v2, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v3, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_2

    const/4 p2, 0x0

    goto :goto_0

    :cond_2
    instance-of v2, v1, Lcom/jme3/export/binary/BinaryInputCapsule$ID;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->importer:Lcom/jme3/export/binary/BinaryImporter;

    check-cast v1, Lcom/jme3/export/binary/BinaryInputCapsule$ID;

    iget v3, v1, Lcom/jme3/export/binary/BinaryInputCapsule$ID;->id:I

    invoke-virtual {v2, v3}, Lcom/jme3/export/binary/BinaryImporter;->readObject(I)Lcom/jme3/export/Savable;

    move-result-object v1

    iget-object v2, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v3, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v1, Lcom/jme3/export/Savable;

    move-object p2, v1

    goto :goto_0
.end method

.method protected readSavable([B)Lcom/jme3/export/binary/BinaryInputCapsule$ID;
    .locals 2
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readInt([B)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/jme3/export/binary/BinaryInputCapsule$ID;

    invoke-direct {v1, v0}, Lcom/jme3/export/binary/BinaryInputCapsule$ID;-><init>(I)V

    goto :goto_0
.end method

.method public readSavableArray(Ljava/lang/String;[Lcom/jme3/export/Savable;)[Lcom/jme3/export/Savable;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # [Lcom/jme3/export/Savable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v2, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->cObj:Lcom/jme3/export/binary/BinaryClassObject;

    iget-object v2, v2, Lcom/jme3/export/binary/BinaryClassObject;->nameFields:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/export/binary/BinaryClassField;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v3, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-object p2

    :cond_1
    iget-object v2, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v3, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/Object;

    move-object v1, v2

    check-cast v1, [Ljava/lang/Object;

    instance-of v2, v1, [Lcom/jme3/export/binary/BinaryInputCapsule$ID;

    if-eqz v2, :cond_0

    invoke-direct {p0, v1}, Lcom/jme3/export/binary/BinaryInputCapsule;->resolveIDs([Ljava/lang/Object;)[Lcom/jme3/export/Savable;

    move-result-object v1

    iget-object v2, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v3, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v1, [Lcom/jme3/export/Savable;

    check-cast v1, [Lcom/jme3/export/Savable;

    move-object p2, v1

    goto :goto_0
.end method

.method protected readSavableArray([B)[Lcom/jme3/export/binary/BinaryInputCapsule$ID;
    .locals 4
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readInt([B)I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_1

    const/4 v1, 0x0

    :cond_0
    return-object v1

    :cond_1
    new-array v1, v0, [Lcom/jme3/export/binary/BinaryInputCapsule$ID;

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readSavable([B)Lcom/jme3/export/binary/BinaryInputCapsule$ID;

    move-result-object v3

    aput-object v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method protected readSavableArray2D([B)[[Lcom/jme3/export/binary/BinaryInputCapsule$ID;
    .locals 4
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readInt([B)I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    const/4 v3, 0x0

    check-cast v3, [[Lcom/jme3/export/binary/BinaryInputCapsule$ID;

    :goto_0
    return-object v3

    :cond_0
    new-array v1, v0, [[Lcom/jme3/export/binary/BinaryInputCapsule$ID;

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readSavableArray([B)[Lcom/jme3/export/binary/BinaryInputCapsule$ID;

    move-result-object v3

    aput-object v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    move-object v3, v1

    goto :goto_0
.end method

.method protected readSavableArray3D([B)[[[Lcom/jme3/export/binary/BinaryInputCapsule$ID;
    .locals 4
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readInt([B)I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    const/4 v3, 0x0

    check-cast v3, [[[Lcom/jme3/export/binary/BinaryInputCapsule$ID;

    :goto_0
    return-object v3

    :cond_0
    new-array v1, v0, [[[Lcom/jme3/export/binary/BinaryInputCapsule$ID;

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readSavableArray2D([B)[[Lcom/jme3/export/binary/BinaryInputCapsule$ID;

    move-result-object v3

    aput-object v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    move-object v3, v1

    goto :goto_0
.end method

.method public readSavableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v3, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->cObj:Lcom/jme3/export/binary/BinaryClassObject;

    iget-object v3, v3, Lcom/jme3/export/binary/BinaryClassObject;->nameFields:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/export/binary/BinaryClassField;

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v4, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    move-object v2, p2

    :goto_0
    return-object v2

    :cond_1
    iget-object v3, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v4, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    instance-of v3, v2, [Lcom/jme3/export/binary/BinaryInputCapsule$ID;

    if-eqz v3, :cond_2

    const/4 v3, 0x0

    invoke-virtual {p0, p1, v3}, Lcom/jme3/export/binary/BinaryInputCapsule;->readSavableArray(Ljava/lang/String;[Lcom/jme3/export/Savable;)[Lcom/jme3/export/Savable;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/jme3/export/binary/BinaryInputCapsule;->savableArrayListFromArray([Lcom/jme3/export/Savable;)Ljava/util/ArrayList;

    move-result-object v2

    iget-object v3, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v4, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    check-cast v2, Ljava/util/ArrayList;

    goto :goto_0
.end method

.method protected readSavableMap([B)[[Lcom/jme3/export/binary/BinaryInputCapsule$ID;
    .locals 4
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readInt([B)I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    const/4 v3, 0x0

    check-cast v3, [[Lcom/jme3/export/binary/BinaryInputCapsule$ID;

    :goto_0
    return-object v3

    :cond_0
    new-array v1, v0, [[Lcom/jme3/export/binary/BinaryInputCapsule$ID;

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readSavableArray([B)[Lcom/jme3/export/binary/BinaryInputCapsule$ID;

    move-result-object v3

    aput-object v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    move-object v3, v1

    goto :goto_0
.end method

.method protected readShort([B)S
    .locals 2
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    invoke-static {p1, v1}, Lcom/jme3/export/binary/ByteUtils;->convertShortFromBytes([BI)S

    move-result v0

    iget v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    return v0
.end method

.method public readShortArray(Ljava/lang/String;[S)[S
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # [S
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->cObj:Lcom/jme3/export/binary/BinaryClassObject;

    iget-object v1, v1, Lcom/jme3/export/binary/BinaryClassObject;->nameFields:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/export/binary/BinaryClassField;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v2, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    move-object v1, p2

    :goto_0
    return-object v1

    :cond_1
    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v2, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [S

    check-cast v1, [S

    goto :goto_0
.end method

.method protected readShortArray([B)[S
    .locals 4
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readInt([B)I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_1

    const/4 v1, 0x0

    :cond_0
    return-object v1

    :cond_1
    new-array v1, v0, [S

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readShort([B)S

    move-result v3

    aput-short v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method protected readShortArray2D([B)[[S
    .locals 4
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readInt([B)I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    const/4 v3, 0x0

    check-cast v3, [[S

    :goto_0
    return-object v3

    :cond_0
    new-array v1, v0, [[S

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readShortArray([B)[S

    move-result-object v3

    aput-object v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    move-object v3, v1

    goto :goto_0
.end method

.method public readShortBuffer(Ljava/lang/String;Ljava/nio/ShortBuffer;)Ljava/nio/ShortBuffer;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/nio/ShortBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->cObj:Lcom/jme3/export/binary/BinaryClassObject;

    iget-object v1, v1, Lcom/jme3/export/binary/BinaryClassObject;->nameFields:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/export/binary/BinaryClassField;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v2, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    move-object v1, p2

    :goto_0
    return-object v1

    :cond_1
    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v2, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/nio/ShortBuffer;

    goto :goto_0
.end method

.method protected readShortBuffer([B)Ljava/nio/ShortBuffer;
    .locals 5
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readInt([B)I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-static {}, Lcom/jme3/export/binary/BinaryImporter;->canUseFastBuffers()Z

    move-result v3

    if-eqz v3, :cond_1

    mul-int/lit8 v3, v0, 0x2

    invoke-static {v3}, Lcom/jme3/util/BufferUtils;->createByteBuffer(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    iget v3, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    mul-int/lit8 v4, v0, 0x2

    invoke-virtual {v1, p1, v3, v4}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    iget v3, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    mul-int/lit8 v4, v0, 0x2

    add-int/2addr v3, v4

    iput v3, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-static {v0}, Lcom/jme3/util/BufferUtils;->createShortBuffer(I)Ljava/nio/ShortBuffer;

    move-result-object v1

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v0, :cond_2

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readShortForBuffer([B)S

    move-result v3

    invoke-virtual {v1, v3}, Ljava/nio/ShortBuffer;->put(S)Ljava/nio/ShortBuffer;

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v1}, Ljava/nio/ShortBuffer;->rewind()Ljava/nio/Buffer;

    goto :goto_0
.end method

.method protected readShortForBuffer([B)S
    .locals 3
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    add-int/lit8 v1, v1, 0x0

    aget-byte v1, p1, v1

    and-int/lit16 v1, v1, 0xff

    iget v2, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    add-int/lit8 v2, v2, 0x1

    aget-byte v2, p1, v2

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    add-int/2addr v1, v2

    int-to-short v0, v1

    iget v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    return v0
.end method

.method public readString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->cObj:Lcom/jme3/export/binary/BinaryClassObject;

    iget-object v1, v1, Lcom/jme3/export/binary/BinaryClassObject;->nameFields:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/export/binary/BinaryClassField;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v2, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    move-object v1, p2

    :goto_0
    return-object v1

    :cond_1
    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v2, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    goto :goto_0
.end method

.method protected readString([B)Ljava/lang/String;
    .locals 9
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v8, 0x80

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readInt([B)I

    move-result v2

    const/4 v6, -0x1

    if-ne v2, v6, :cond_0

    const/4 v6, 0x0

    :goto_0
    return-object v6

    :cond_0
    new-array v1, v2, [B

    const/4 v4, 0x0

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v2, :cond_6

    iget v6, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    aget-byte v6, p1, v6

    aput-byte v6, v1, v5

    aget-byte v6, v1, v5

    and-int/lit16 v0, v6, 0xff

    packed-switch v4, :pswitch_data_0

    :cond_1
    :goto_2
    :pswitch_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :pswitch_1
    if-lt v0, v8, :cond_1

    and-int/lit16 v6, v0, 0xc0

    const/16 v7, 0xc0

    if-ne v6, v7, :cond_2

    const/4 v4, 0x2

    goto :goto_2

    :cond_2
    and-int/lit16 v6, v0, 0xe0

    const/16 v7, 0xe0

    if-ne v6, v7, :cond_3

    const/4 v4, 0x3

    goto :goto_2

    :cond_3
    const/16 v4, 0xa

    goto :goto_2

    :pswitch_2
    and-int/lit16 v6, v0, 0x80

    if-ne v6, v8, :cond_5

    const/4 v6, 0x3

    if-ne v4, v6, :cond_4

    const/4 v4, 0x4

    :goto_3
    goto :goto_2

    :cond_4
    const/4 v4, 0x0

    goto :goto_3

    :cond_5
    const/16 v4, 0xa

    goto :goto_2

    :cond_6
    if-nez v4, :cond_7

    :try_start_0
    new-instance v6, Ljava/lang/String;

    const-string v7, "UTF8"

    invoke-direct {v6, v1, v7}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v3

    sget-object v6, Lcom/jme3/export/binary/BinaryInputCapsule;->logger:Ljava/util/logging/Logger;

    sget-object v7, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    const-string v8, "Your export has been saved with an incorrect encoding or your version of Java is unable to decode the stored string. While your export may load correctly by falling back, using it on different platforms or java versions might lead to very strange inconsitenties. You should probably re-export your work. See ISSUE 276 in the jME issue tracker."

    invoke-virtual {v6, v7, v8}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v1}, Ljava/lang/String;-><init>([B)V

    goto :goto_0

    :cond_7
    :try_start_1
    sget-object v6, Lcom/jme3/export/binary/BinaryInputCapsule;->logger:Ljava/util/logging/Logger;

    sget-object v7, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v8, "Your export has been saved with an incorrect encoding for it\'s String fields which means it might not load correctly due to encoding issues. You should probably re-export your work. See ISSUE 276 in the jME issue tracker."

    invoke-virtual {v6, v7, v8}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;)V

    new-instance v6, Ljava/lang/String;

    const-string v7, "ISO8859_1"

    invoke-direct {v6, v1, v7}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public readStringArray(Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->cObj:Lcom/jme3/export/binary/BinaryClassObject;

    iget-object v1, v1, Lcom/jme3/export/binary/BinaryClassObject;->nameFields:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/export/binary/BinaryClassField;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v2, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    move-object v1, p2

    :goto_0
    return-object v1

    :cond_1
    iget-object v1, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v2, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/String;

    check-cast v1, [Ljava/lang/String;

    goto :goto_0
.end method

.method protected readStringArray([B)[Ljava/lang/String;
    .locals 4
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readInt([B)I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_1

    const/4 v1, 0x0

    :cond_0
    return-object v1

    :cond_1
    new-array v1, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readString([B)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method protected readStringArray2D([B)[[Ljava/lang/String;
    .locals 4
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readInt([B)I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    const/4 v3, 0x0

    check-cast v3, [[Ljava/lang/String;

    :goto_0
    return-object v3

    :cond_0
    new-array v1, v0, [[Ljava/lang/String;

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readStringArray([B)[Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    move-object v3, v1

    goto :goto_0
.end method

.method protected readStringSavableMap([B)Lcom/jme3/export/binary/BinaryInputCapsule$StringIDMap;
    .locals 6
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x0

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readInt([B)I

    move-result v0

    const/4 v5, -0x1

    if-ne v0, v5, :cond_0

    move-object v2, v4

    :goto_0
    return-object v2

    :cond_0
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readStringArray([B)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readSavableArray([B)[Lcom/jme3/export/binary/BinaryInputCapsule$ID;

    move-result-object v3

    new-instance v2, Lcom/jme3/export/binary/BinaryInputCapsule$StringIDMap;

    invoke-direct {v2, v4}, Lcom/jme3/export/binary/BinaryInputCapsule$StringIDMap;-><init>(Lcom/jme3/export/binary/BinaryInputCapsule$1;)V

    iput-object v1, v2, Lcom/jme3/export/binary/BinaryInputCapsule$StringIDMap;->keys:[Ljava/lang/String;

    iput-object v3, v2, Lcom/jme3/export/binary/BinaryInputCapsule$StringIDMap;->values:[Lcom/jme3/export/binary/BinaryInputCapsule$ID;

    goto :goto_0
.end method

.method public readStringSavableMap(Ljava/lang/String;Ljava/util/Map;)Ljava/util/Map;
    .locals 6
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "+",
            "Lcom/jme3/export/Savable;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "+",
            "Lcom/jme3/export/Savable;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v4, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->cObj:Lcom/jme3/export/binary/BinaryClassObject;

    iget-object v4, v4, Lcom/jme3/export/binary/BinaryClassObject;->nameFields:Ljava/util/HashMap;

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/export/binary/BinaryClassField;

    if-eqz v0, :cond_0

    iget-object v4, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v5, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    move-object v2, p2

    :goto_0
    return-object v2

    :cond_1
    iget-object v4, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v5, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    instance-of v4, v2, Lcom/jme3/export/binary/BinaryInputCapsule$StringIDMap;

    if-eqz v4, :cond_2

    move-object v1, v2

    check-cast v1, Lcom/jme3/export/binary/BinaryInputCapsule$StringIDMap;

    iget-object v4, v1, Lcom/jme3/export/binary/BinaryInputCapsule$StringIDMap;->values:[Lcom/jme3/export/binary/BinaryInputCapsule$ID;

    invoke-direct {p0, v4}, Lcom/jme3/export/binary/BinaryInputCapsule;->resolveIDs([Ljava/lang/Object;)[Lcom/jme3/export/Savable;

    move-result-object v3

    iget-object v4, v1, Lcom/jme3/export/binary/BinaryInputCapsule$StringIDMap;->keys:[Ljava/lang/String;

    invoke-direct {p0, v4, v3}, Lcom/jme3/export/binary/BinaryInputCapsule;->stringSavableMapFromKV([Ljava/lang/String;[Lcom/jme3/export/Savable;)Ljava/util/Map;

    move-result-object v2

    iget-object v4, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iget-byte v5, v0, Lcom/jme3/export/binary/BinaryClassField;->alias:B

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    invoke-virtual {v4, v5, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    check-cast v2, Ljava/util/Map;

    goto :goto_0
.end method

.method public setContent([BII)V
    .locals 9
    .param p1    # [B
    .param p2    # I
    .param p3    # I

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    iput p2, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    :goto_0
    iget v0, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    if-ge v0, p3, :cond_0

    iget v0, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    aget-byte v6, p1, v0

    iget v0, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->index:I

    :try_start_0
    iget-object v0, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->cObj:Lcom/jme3/export/binary/BinaryClassObject;

    iget-object v0, v0, Lcom/jme3/export/binary/BinaryClassObject;->aliasFields:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/jme3/export/binary/BinaryClassField;

    iget-byte v7, v0, Lcom/jme3/export/binary/BinaryClassField;->type:B

    const/4 v8, 0x0

    sparse-switch v7, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readByte([B)B

    move-result v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v8

    :goto_1
    iget-object v0, p0, Lcom/jme3/export/binary/BinaryInputCapsule;->fieldData:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    invoke-virtual {v0, v1, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v5

    sget-object v0, Lcom/jme3/export/binary/BinaryInputCapsule;->logger:Ljava/util/logging/Logger;

    sget-object v1, Ljava/util/logging/Level;->SEVERE:Ljava/util/logging/Level;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "setContent(byte[] content)"

    const-string v4, "Exception"

    invoke-virtual/range {v0 .. v5}, Ljava/util/logging/Logger;->logp(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :sswitch_1
    :try_start_1
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readBitSet([B)Ljava/util/BitSet;

    move-result-object v8

    goto :goto_1

    :sswitch_2
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readBoolean([B)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    goto :goto_1

    :sswitch_3
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readBooleanArray([B)[Z

    move-result-object v8

    goto :goto_1

    :sswitch_4
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readBooleanArray2D([B)[[Z

    move-result-object v8

    goto :goto_1

    :sswitch_5
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readByteArray([B)[B

    move-result-object v8

    goto :goto_1

    :sswitch_6
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readByteArray2D([B)[[B

    move-result-object v8

    goto :goto_1

    :sswitch_7
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readByteBuffer([B)Ljava/nio/ByteBuffer;

    move-result-object v8

    goto :goto_1

    :sswitch_8
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readDouble([B)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    goto :goto_1

    :sswitch_9
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readDoubleArray([B)[D

    move-result-object v8

    goto :goto_1

    :sswitch_a
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readDoubleArray2D([B)[[D

    move-result-object v8

    goto :goto_1

    :sswitch_b
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readFloat([B)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    goto :goto_1

    :sswitch_c
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readFloatArray([B)[F

    move-result-object v8

    goto :goto_1

    :sswitch_d
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readFloatArray2D([B)[[F

    move-result-object v8

    goto :goto_1

    :sswitch_e
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readFloatBuffer([B)Ljava/nio/FloatBuffer;

    move-result-object v8

    goto :goto_1

    :sswitch_f
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readFloatBufferArrayList([B)Ljava/util/ArrayList;

    move-result-object v8

    goto :goto_1

    :sswitch_10
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readByteBufferArrayList([B)Ljava/util/ArrayList;

    move-result-object v8

    goto :goto_1

    :sswitch_11
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readInt([B)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    goto/16 :goto_1

    :sswitch_12
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readIntArray([B)[I

    move-result-object v8

    goto/16 :goto_1

    :sswitch_13
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readIntArray2D([B)[[I

    move-result-object v8

    goto/16 :goto_1

    :sswitch_14
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readIntBuffer([B)Ljava/nio/IntBuffer;

    move-result-object v8

    goto/16 :goto_1

    :sswitch_15
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readLong([B)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    goto/16 :goto_1

    :sswitch_16
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readLongArray([B)[J

    move-result-object v8

    goto/16 :goto_1

    :sswitch_17
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readLongArray2D([B)[[J

    move-result-object v8

    goto/16 :goto_1

    :sswitch_18
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readSavable([B)Lcom/jme3/export/binary/BinaryInputCapsule$ID;

    move-result-object v8

    goto/16 :goto_1

    :sswitch_19
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readSavableArray([B)[Lcom/jme3/export/binary/BinaryInputCapsule$ID;

    move-result-object v8

    goto/16 :goto_1

    :sswitch_1a
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readSavableArray2D([B)[[Lcom/jme3/export/binary/BinaryInputCapsule$ID;

    move-result-object v8

    goto/16 :goto_1

    :sswitch_1b
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readSavableArray([B)[Lcom/jme3/export/binary/BinaryInputCapsule$ID;

    move-result-object v8

    goto/16 :goto_1

    :sswitch_1c
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readSavableArray2D([B)[[Lcom/jme3/export/binary/BinaryInputCapsule$ID;

    move-result-object v8

    goto/16 :goto_1

    :sswitch_1d
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readSavableArray3D([B)[[[Lcom/jme3/export/binary/BinaryInputCapsule$ID;

    move-result-object v8

    goto/16 :goto_1

    :sswitch_1e
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readSavableMap([B)[[Lcom/jme3/export/binary/BinaryInputCapsule$ID;

    move-result-object v8

    goto/16 :goto_1

    :sswitch_1f
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readStringSavableMap([B)Lcom/jme3/export/binary/BinaryInputCapsule$StringIDMap;

    move-result-object v8

    goto/16 :goto_1

    :sswitch_20
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readIntSavableMap([B)Lcom/jme3/export/binary/BinaryInputCapsule$IntIDMap;

    move-result-object v8

    goto/16 :goto_1

    :sswitch_21
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readShort([B)S

    move-result v0

    invoke-static {v0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    goto/16 :goto_1

    :sswitch_22
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readShortArray([B)[S

    move-result-object v8

    goto/16 :goto_1

    :sswitch_23
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readShortArray2D([B)[[S

    move-result-object v8

    goto/16 :goto_1

    :sswitch_24
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readShortBuffer([B)Ljava/nio/ShortBuffer;

    move-result-object v8

    goto/16 :goto_1

    :sswitch_25
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readString([B)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_1

    :sswitch_26
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readStringArray([B)[Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_1

    :sswitch_27
    invoke-virtual {p0, p1}, Lcom/jme3/export/binary/BinaryInputCapsule;->readStringArray2D([B)[[Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v8

    goto/16 :goto_1

    :cond_0
    return-void

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_5
        0x2 -> :sswitch_6
        0xa -> :sswitch_11
        0xb -> :sswitch_12
        0xc -> :sswitch_13
        0x14 -> :sswitch_b
        0x15 -> :sswitch_c
        0x16 -> :sswitch_d
        0x1e -> :sswitch_8
        0x1f -> :sswitch_9
        0x20 -> :sswitch_a
        0x28 -> :sswitch_15
        0x29 -> :sswitch_16
        0x2a -> :sswitch_17
        0x32 -> :sswitch_21
        0x33 -> :sswitch_22
        0x34 -> :sswitch_23
        0x3c -> :sswitch_2
        0x3d -> :sswitch_3
        0x3e -> :sswitch_4
        0x46 -> :sswitch_25
        0x47 -> :sswitch_26
        0x48 -> :sswitch_27
        0x50 -> :sswitch_1
        0x5a -> :sswitch_18
        0x5b -> :sswitch_19
        0x5c -> :sswitch_1a
        0x64 -> :sswitch_1b
        0x65 -> :sswitch_1c
        0x66 -> :sswitch_1d
        0x69 -> :sswitch_1e
        0x6a -> :sswitch_1f
        0x6b -> :sswitch_20
        0x6e -> :sswitch_f
        0x6f -> :sswitch_10
        0x78 -> :sswitch_e
        0x79 -> :sswitch_14
        0x7a -> :sswitch_7
        0x7b -> :sswitch_24
    .end sparse-switch
.end method
