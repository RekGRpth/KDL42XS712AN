.class public final Lcom/jme3/math/Ray;
.super Ljava/lang/Object;
.source "Ray.java"

# interfaces
.implements Lcom/jme3/collision/Collidable;
.implements Lcom/jme3/export/Savable;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field static final synthetic $assertionsDisabled:Z

.field static final serialVersionUID:J = 0x1L


# instance fields
.field public direction:Lcom/jme3/math/Vector3f;

.field public limit:F

.field public origin:Lcom/jme3/math/Vector3f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/jme3/math/Ray;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/jme3/math/Ray;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/jme3/math/Vector3f;

    invoke-direct {v0}, Lcom/jme3/math/Vector3f;-><init>()V

    iput-object v0, p0, Lcom/jme3/math/Ray;->origin:Lcom/jme3/math/Vector3f;

    new-instance v0, Lcom/jme3/math/Vector3f;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, v2, v2, v1}, Lcom/jme3/math/Vector3f;-><init>(FFF)V

    iput-object v0, p0, Lcom/jme3/math/Ray;->direction:Lcom/jme3/math/Vector3f;

    const/high16 v0, 0x7f800000    # Float.POSITIVE_INFINITY

    iput v0, p0, Lcom/jme3/math/Ray;->limit:F

    return-void
.end method

.method public constructor <init>(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)V
    .locals 3
    .param p1    # Lcom/jme3/math/Vector3f;
    .param p2    # Lcom/jme3/math/Vector3f;

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/jme3/math/Vector3f;

    invoke-direct {v0}, Lcom/jme3/math/Vector3f;-><init>()V

    iput-object v0, p0, Lcom/jme3/math/Ray;->origin:Lcom/jme3/math/Vector3f;

    new-instance v0, Lcom/jme3/math/Vector3f;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, v2, v2, v1}, Lcom/jme3/math/Vector3f;-><init>(FFF)V

    iput-object v0, p0, Lcom/jme3/math/Ray;->direction:Lcom/jme3/math/Vector3f;

    const/high16 v0, 0x7f800000    # Float.POSITIVE_INFINITY

    iput v0, p0, Lcom/jme3/math/Ray;->limit:F

    invoke-virtual {p0, p1}, Lcom/jme3/math/Ray;->setOrigin(Lcom/jme3/math/Vector3f;)V

    invoke-virtual {p0, p2}, Lcom/jme3/math/Ray;->setDirection(Lcom/jme3/math/Vector3f;)V

    return-void
.end method


# virtual methods
.method public clone()Lcom/jme3/math/Ray;
    .locals 3

    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/jme3/math/Ray;

    iget-object v2, p0, Lcom/jme3/math/Ray;->direction:Lcom/jme3/math/Vector3f;

    invoke-virtual {v2}, Lcom/jme3/math/Vector3f;->clone()Lcom/jme3/math/Vector3f;

    move-result-object v2

    iput-object v2, v1, Lcom/jme3/math/Ray;->direction:Lcom/jme3/math/Vector3f;

    iget-object v2, p0, Lcom/jme3/math/Ray;->origin:Lcom/jme3/math/Vector3f;

    invoke-virtual {v2}, Lcom/jme3/math/Vector3f;->clone()Lcom/jme3/math/Vector3f;

    move-result-object v2

    iput-object v2, v1, Lcom/jme3/math/Ray;->origin:Lcom/jme3/math/Vector3f;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2}, Ljava/lang/AssertionError;-><init>()V

    throw v2
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jme3/math/Ray;->clone()Lcom/jme3/math/Ray;

    move-result-object v0

    return-object v0
.end method

.method public collideWith(Lcom/jme3/collision/Collidable;Lcom/jme3/collision/CollisionResults;)I
    .locals 7
    .param p1    # Lcom/jme3/collision/Collidable;
    .param p2    # Lcom/jme3/collision/CollisionResults;

    instance-of v4, p1, Lcom/jme3/bounding/BoundingVolume;

    if-eqz v4, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/jme3/bounding/BoundingVolume;

    invoke-virtual {v0, p0, p2}, Lcom/jme3/bounding/BoundingVolume;->collideWith(Lcom/jme3/collision/Collidable;Lcom/jme3/collision/CollisionResults;)I

    move-result v4

    :goto_0
    return v4

    :cond_0
    instance-of v4, p1, Lcom/jme3/math/AbstractTriangle;

    if-eqz v4, :cond_3

    move-object v3, p1

    check-cast v3, Lcom/jme3/math/AbstractTriangle;

    invoke-virtual {v3}, Lcom/jme3/math/AbstractTriangle;->get1()Lcom/jme3/math/Vector3f;

    move-result-object v4

    invoke-virtual {v3}, Lcom/jme3/math/AbstractTriangle;->get2()Lcom/jme3/math/Vector3f;

    move-result-object v5

    invoke-virtual {v3}, Lcom/jme3/math/AbstractTriangle;->get3()Lcom/jme3/math/Vector3f;

    move-result-object v6

    invoke-virtual {p0, v4, v5, v6}, Lcom/jme3/math/Ray;->intersects(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    :cond_2
    new-instance v4, Lcom/jme3/math/Vector3f;

    iget-object v5, p0, Lcom/jme3/math/Ray;->direction:Lcom/jme3/math/Vector3f;

    invoke-direct {v4, v5}, Lcom/jme3/math/Vector3f;-><init>(Lcom/jme3/math/Vector3f;)V

    invoke-virtual {v4, v1}, Lcom/jme3/math/Vector3f;->multLocal(F)Lcom/jme3/math/Vector3f;

    move-result-object v4

    iget-object v5, p0, Lcom/jme3/math/Ray;->origin:Lcom/jme3/math/Vector3f;

    invoke-virtual {v4, v5}, Lcom/jme3/math/Vector3f;->addLocal(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v2

    new-instance v4, Lcom/jme3/collision/CollisionResult;

    invoke-direct {v4, v2, v1}, Lcom/jme3/collision/CollisionResult;-><init>(Lcom/jme3/math/Vector3f;F)V

    invoke-virtual {p2, v4}, Lcom/jme3/collision/CollisionResults;->addCollision(Lcom/jme3/collision/CollisionResult;)V

    const/4 v4, 0x1

    goto :goto_0

    :cond_3
    new-instance v4, Lcom/jme3/collision/UnsupportedCollisionException;

    invoke-direct {v4}, Lcom/jme3/collision/UnsupportedCollisionException;-><init>()V

    throw v4
.end method

.method public getDirection()Lcom/jme3/math/Vector3f;
    .locals 1

    iget-object v0, p0, Lcom/jme3/math/Ray;->direction:Lcom/jme3/math/Vector3f;

    return-object v0
.end method

.method public getLimit()F
    .locals 1

    iget v0, p0, Lcom/jme3/math/Ray;->limit:F

    return v0
.end method

.method public getOrigin()Lcom/jme3/math/Vector3f;
    .locals 1

    iget-object v0, p0, Lcom/jme3/math/Ray;->origin:Lcom/jme3/math/Vector3f;

    return-object v0
.end method

.method public intersects(Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;Lcom/jme3/math/Vector3f;)F
    .locals 26
    .param p1    # Lcom/jme3/math/Vector3f;
    .param p2    # Lcom/jme3/math/Vector3f;
    .param p3    # Lcom/jme3/math/Vector3f;

    move-object/from16 v0, p2

    iget v0, v0, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v23, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v24, v0

    sub-float v11, v23, v24

    move-object/from16 v0, p2

    iget v0, v0, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v23, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v24, v0

    sub-float v12, v23, v24

    move-object/from16 v0, p2

    iget v0, v0, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v23, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v24, v0

    sub-float v13, v23, v24

    move-object/from16 v0, p3

    iget v0, v0, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v23, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v24, v0

    sub-float v14, v23, v24

    move-object/from16 v0, p3

    iget v0, v0, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v23, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v24, v0

    sub-float v15, v23, v24

    move-object/from16 v0, p3

    iget v0, v0, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v23, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v24, v0

    sub-float v16, v23, v24

    mul-float v23, v12, v16

    mul-float v24, v13, v15

    sub-float v18, v23, v24

    mul-float v23, v13, v14

    mul-float v24, v11, v16

    sub-float v19, v23, v24

    mul-float v23, v11, v15

    mul-float v24, v12, v14

    sub-float v20, v23, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jme3/math/Ray;->direction:Lcom/jme3/math/Vector3f;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v23, v0

    mul-float v23, v23, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jme3/math/Ray;->direction:Lcom/jme3/math/Vector3f;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v24, v0

    mul-float v24, v24, v19

    add-float v23, v23, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jme3/math/Ray;->direction:Lcom/jme3/math/Vector3f;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v24, v0

    mul-float v24, v24, v20

    add-float v10, v23, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jme3/math/Ray;->origin:Lcom/jme3/math/Vector3f;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v23, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v24, v0

    sub-float v5, v23, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jme3/math/Ray;->origin:Lcom/jme3/math/Vector3f;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v23, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v24, v0

    sub-float v6, v23, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jme3/math/Ray;->origin:Lcom/jme3/math/Vector3f;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v23, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v24, v0

    sub-float v7, v23, v24

    const/high16 v23, 0x34000000

    cmpl-float v23, v10, v23

    if-lez v23, :cond_0

    const/high16 v21, 0x3f800000    # 1.0f

    :goto_0
    mul-float v23, v6, v16

    mul-float v24, v7, v15

    sub-float v2, v23, v24

    mul-float v23, v7, v14

    mul-float v24, v5, v16

    sub-float v3, v23, v24

    mul-float v23, v5, v15

    mul-float v24, v6, v14

    sub-float v4, v23, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jme3/math/Ray;->direction:Lcom/jme3/math/Vector3f;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v23, v0

    mul-float v23, v23, v2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jme3/math/Ray;->direction:Lcom/jme3/math/Vector3f;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v24, v0

    mul-float v24, v24, v3

    add-float v23, v23, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jme3/math/Ray;->direction:Lcom/jme3/math/Vector3f;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v24, v0

    mul-float v24, v24, v4

    add-float v23, v23, v24

    mul-float v8, v21, v23

    const/16 v23, 0x0

    cmpl-float v23, v8, v23

    if-ltz v23, :cond_2

    mul-float v23, v12, v7

    mul-float v24, v13, v6

    sub-float v2, v23, v24

    mul-float v23, v13, v5

    mul-float v24, v11, v7

    sub-float v3, v23, v24

    mul-float v23, v11, v6

    mul-float v24, v12, v5

    sub-float v4, v23, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jme3/math/Ray;->direction:Lcom/jme3/math/Vector3f;

    move-object/from16 v23, v0

    move-object/from16 v0, v23

    iget v0, v0, Lcom/jme3/math/Vector3f;->x:F

    move/from16 v23, v0

    mul-float v23, v23, v2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jme3/math/Ray;->direction:Lcom/jme3/math/Vector3f;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/jme3/math/Vector3f;->y:F

    move/from16 v24, v0

    mul-float v24, v24, v3

    add-float v23, v23, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/jme3/math/Ray;->direction:Lcom/jme3/math/Vector3f;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/jme3/math/Vector3f;->z:F

    move/from16 v24, v0

    mul-float v24, v24, v4

    add-float v23, v23, v24

    mul-float v9, v21, v23

    const/16 v23, 0x0

    cmpl-float v23, v9, v23

    if-ltz v23, :cond_2

    add-float v23, v8, v9

    cmpg-float v23, v23, v10

    if-gtz v23, :cond_2

    move/from16 v0, v21

    neg-float v0, v0

    move/from16 v23, v0

    mul-float v24, v5, v18

    mul-float v25, v6, v19

    add-float v24, v24, v25

    mul-float v25, v7, v20

    add-float v24, v24, v25

    mul-float v1, v23, v24

    const/16 v23, 0x0

    cmpl-float v23, v1, v23

    if-ltz v23, :cond_2

    const/high16 v23, 0x3f800000    # 1.0f

    div-float v17, v23, v10

    mul-float v22, v1, v17

    :goto_1
    return v22

    :cond_0
    const/high16 v23, -0x4c000000

    cmpg-float v23, v10, v23

    if-gez v23, :cond_1

    const/high16 v21, -0x40800000    # -1.0f

    neg-float v10, v10

    goto/16 :goto_0

    :cond_1
    const/high16 v22, 0x7f800000    # Float.POSITIVE_INFINITY

    goto :goto_1

    :cond_2
    const/high16 v22, 0x7f800000    # Float.POSITIVE_INFINITY

    goto :goto_1
.end method

.method public intersectsWherePlane(Lcom/jme3/math/Plane;Lcom/jme3/math/Vector3f;)Z
    .locals 7
    .param p1    # Lcom/jme3/math/Plane;
    .param p2    # Lcom/jme3/math/Vector3f;

    const/4 v3, 0x0

    const/high16 v6, 0x34000000

    invoke-virtual {p1}, Lcom/jme3/math/Plane;->getNormal()Lcom/jme3/math/Vector3f;

    move-result-object v4

    iget-object v5, p0, Lcom/jme3/math/Ray;->direction:Lcom/jme3/math/Vector3f;

    invoke-virtual {v4, v5}, Lcom/jme3/math/Vector3f;->dot(Lcom/jme3/math/Vector3f;)F

    move-result v0

    const/high16 v4, -0x4c000000

    cmpl-float v4, v0, v4

    if-lez v4, :cond_1

    cmpg-float v4, v0, v6

    if-gez v4, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    invoke-virtual {p1}, Lcom/jme3/math/Plane;->getNormal()Lcom/jme3/math/Vector3f;

    move-result-object v4

    iget-object v5, p0, Lcom/jme3/math/Ray;->origin:Lcom/jme3/math/Vector3f;

    invoke-virtual {v4, v5}, Lcom/jme3/math/Vector3f;->dot(Lcom/jme3/math/Vector3f;)F

    move-result v4

    invoke-virtual {p1}, Lcom/jme3/math/Plane;->getConstant()F

    move-result v5

    sub-float/2addr v4, v5

    neg-float v1, v4

    div-float v2, v1, v0

    cmpg-float v4, v2, v6

    if-ltz v4, :cond_0

    iget-object v3, p0, Lcom/jme3/math/Ray;->direction:Lcom/jme3/math/Vector3f;

    invoke-virtual {p2, v3}, Lcom/jme3/math/Vector3f;->set(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/jme3/math/Vector3f;->multLocal(F)Lcom/jme3/math/Vector3f;

    move-result-object v3

    iget-object v4, p0, Lcom/jme3/math/Ray;->origin:Lcom/jme3/math/Vector3f;

    invoke-virtual {v3, v4}, Lcom/jme3/math/Vector3f;->addLocal(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    const/4 v3, 0x1

    goto :goto_0
.end method

.method public read(Lcom/jme3/export/JmeImporter;)V
    .locals 3
    .param p1    # Lcom/jme3/export/JmeImporter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-interface {p1, p0}, Lcom/jme3/export/JmeImporter;->getCapsule(Lcom/jme3/export/Savable;)Lcom/jme3/export/InputCapsule;

    move-result-object v0

    const-string v1, "origin"

    sget-object v2, Lcom/jme3/math/Vector3f;->ZERO:Lcom/jme3/math/Vector3f;

    invoke-virtual {v2}, Lcom/jme3/math/Vector3f;->clone()Lcom/jme3/math/Vector3f;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readSavable(Ljava/lang/String;Lcom/jme3/export/Savable;)Lcom/jme3/export/Savable;

    move-result-object v1

    check-cast v1, Lcom/jme3/math/Vector3f;

    iput-object v1, p0, Lcom/jme3/math/Ray;->origin:Lcom/jme3/math/Vector3f;

    const-string v1, "direction"

    sget-object v2, Lcom/jme3/math/Vector3f;->ZERO:Lcom/jme3/math/Vector3f;

    invoke-virtual {v2}, Lcom/jme3/math/Vector3f;->clone()Lcom/jme3/math/Vector3f;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/jme3/export/InputCapsule;->readSavable(Ljava/lang/String;Lcom/jme3/export/Savable;)Lcom/jme3/export/Savable;

    move-result-object v1

    check-cast v1, Lcom/jme3/math/Vector3f;

    iput-object v1, p0, Lcom/jme3/math/Ray;->direction:Lcom/jme3/math/Vector3f;

    return-void
.end method

.method public setDirection(Lcom/jme3/math/Vector3f;)V
    .locals 1
    .param p1    # Lcom/jme3/math/Vector3f;

    sget-boolean v0, Lcom/jme3/math/Ray;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/jme3/math/Vector3f;->isUnitVector()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/jme3/math/Ray;->direction:Lcom/jme3/math/Vector3f;

    invoke-virtual {v0, p1}, Lcom/jme3/math/Vector3f;->set(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    return-void
.end method

.method public setOrigin(Lcom/jme3/math/Vector3f;)V
    .locals 1
    .param p1    # Lcom/jme3/math/Vector3f;

    iget-object v0, p0, Lcom/jme3/math/Ray;->origin:Lcom/jme3/math/Vector3f;

    invoke-virtual {v0, p1}, Lcom/jme3/math/Vector3f;->set(Lcom/jme3/math/Vector3f;)Lcom/jme3/math/Vector3f;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " [Origin: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/jme3/math/Ray;->origin:Lcom/jme3/math/Vector3f;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Direction: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/jme3/math/Ray;->direction:Lcom/jme3/math/Vector3f;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
