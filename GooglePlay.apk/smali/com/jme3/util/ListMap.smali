.class public final Lcom/jme3/util/ListMap;
.super Ljava/lang/Object;
.source "ListMap.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;
.implements Ljava/util/Map;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jme3/util/ListMap$ListMapEntry;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Map",
        "<TK;TV;>;",
        "Ljava/lang/Cloneable;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private final backingMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private entries:[Lcom/jme3/util/ListMap$ListMapEntry;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lcom/jme3/util/ListMap$ListMapEntry",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/jme3/util/ListMap;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/jme3/util/ListMap;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, v1, [Lcom/jme3/util/ListMap$ListMapEntry;

    iput-object v0, p0, Lcom/jme3/util/ListMap;->entries:[Lcom/jme3/util/ListMap$ListMapEntry;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/jme3/util/ListMap;->backingMap:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, p1, [Lcom/jme3/util/ListMap$ListMapEntry;

    iput-object v0, p0, Lcom/jme3/util/ListMap;->entries:[Lcom/jme3/util/ListMap$ListMapEntry;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/jme3/util/ListMap;->backingMap:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<+TK;+TV;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v0

    new-array v0, v0, [Lcom/jme3/util/ListMap$ListMapEntry;

    iput-object v0, p0, Lcom/jme3/util/ListMap;->entries:[Lcom/jme3/util/ListMap$ListMapEntry;

    new-instance v0, Ljava/util/HashMap;

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/jme3/util/ListMap;->backingMap:Ljava/util/HashMap;

    invoke-virtual {p0, p1}, Lcom/jme3/util/ListMap;->putAll(Ljava/util/Map;)V

    return-void
.end method

.method private static keyEq(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 3
    .param p0    # Ljava/lang/Object;
    .param p1    # Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v2

    if-ne v1, v2, :cond_1

    if-eq p0, p1, :cond_0

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method


# virtual methods
.method public clear()V
    .locals 1

    iget-object v0, p0, Lcom/jme3/util/ListMap;->backingMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    return-void
.end method

.method public clone()Lcom/jme3/util/ListMap;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/jme3/util/ListMap",
            "<TK;TV;>;"
        }
    .end annotation

    new-instance v0, Lcom/jme3/util/ListMap;

    invoke-virtual {p0}, Lcom/jme3/util/ListMap;->size()I

    move-result v1

    invoke-direct {v0, v1}, Lcom/jme3/util/ListMap;-><init>(I)V

    invoke-virtual {v0, p0}, Lcom/jme3/util/ListMap;->putAll(Ljava/util/Map;)V

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/jme3/util/ListMap;->clone()Lcom/jme3/util/ListMap;

    move-result-object v0

    return-object v0
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/jme3/util/ListMap;->backingMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    iget-object v0, p0, Lcom/jme3/util/ListMap;->backingMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public entrySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jme3/util/ListMap;->backingMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jme3/util/ListMap;->backingMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getEntry(I)Ljava/util/Map$Entry;
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jme3/util/ListMap;->entries:[Lcom/jme3/util/ListMap$ListMapEntry;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getValue(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TV;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jme3/util/ListMap;->entries:[Lcom/jme3/util/ListMap$ListMapEntry;

    aget-object v0, v0, p1

    # getter for: Lcom/jme3/util/ListMap$ListMapEntry;->value:Ljava/lang/Object;
    invoke-static {v0}, Lcom/jme3/util/ListMap$ListMapEntry;->access$000(Lcom/jme3/util/ListMap$ListMapEntry;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    invoke-virtual {p0}, Lcom/jme3/util/ListMap;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public keySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jme3/util/ListMap;->backingMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    const/4 v5, 0x0

    iget-object v4, p0, Lcom/jme3/util/ListMap;->backingMap:Ljava/util/HashMap;

    invoke-virtual {v4, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {p0}, Lcom/jme3/util/ListMap;->size()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v4, p0, Lcom/jme3/util/ListMap;->entries:[Lcom/jme3/util/ListMap$ListMapEntry;

    aget-object v0, v4, v1

    # getter for: Lcom/jme3/util/ListMap$ListMapEntry;->key:Ljava/lang/Object;
    invoke-static {v0}, Lcom/jme3/util/ListMap$ListMapEntry;->access$100(Lcom/jme3/util/ListMap$ListMapEntry;)Ljava/lang/Object;

    move-result-object v4

    invoke-static {v4, p1}, Lcom/jme3/util/ListMap;->keyEq(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    # setter for: Lcom/jme3/util/ListMap$ListMapEntry;->value:Ljava/lang/Object;
    invoke-static {v0, p2}, Lcom/jme3/util/ListMap$ListMapEntry;->access$002(Lcom/jme3/util/ListMap$ListMapEntry;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    :goto_1
    iget-object v4, p0, Lcom/jme3/util/ListMap;->backingMap:Ljava/util/HashMap;

    invoke-virtual {v4, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    return-object v4

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/jme3/util/ListMap;->size()I

    move-result v2

    iget-object v4, p0, Lcom/jme3/util/ListMap;->entries:[Lcom/jme3/util/ListMap$ListMapEntry;

    array-length v4, v4

    if-ne v2, v4, :cond_3

    iget-object v3, p0, Lcom/jme3/util/ListMap;->entries:[Lcom/jme3/util/ListMap$ListMapEntry;

    mul-int/lit8 v4, v2, 0x2

    new-array v4, v4, [Lcom/jme3/util/ListMap$ListMapEntry;

    iput-object v4, p0, Lcom/jme3/util/ListMap;->entries:[Lcom/jme3/util/ListMap$ListMapEntry;

    iget-object v4, p0, Lcom/jme3/util/ListMap;->entries:[Lcom/jme3/util/ListMap$ListMapEntry;

    invoke-static {v3, v5, v4, v5, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    iget-object v4, p0, Lcom/jme3/util/ListMap;->entries:[Lcom/jme3/util/ListMap$ListMapEntry;

    new-instance v5, Lcom/jme3/util/ListMap$ListMapEntry;

    invoke-direct {v5, p1, p2}, Lcom/jme3/util/ListMap$ListMapEntry;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v5, v4, v2

    goto :goto_1
.end method

.method public putAll(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<+TK;+TV;>;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lcom/jme3/util/ListMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8
    .param p1    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    iget-object v5, p0, Lcom/jme3/util/ListMap;->backingMap:Ljava/util/HashMap;

    invoke-virtual {v5, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/jme3/util/ListMap;->size()I

    move-result v5

    add-int/lit8 v4, v5, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v4, :cond_0

    iget-object v5, p0, Lcom/jme3/util/ListMap;->entries:[Lcom/jme3/util/ListMap$ListMapEntry;

    aget-object v1, v5, v2

    # getter for: Lcom/jme3/util/ListMap$ListMapEntry;->key:Ljava/lang/Object;
    invoke-static {v1}, Lcom/jme3/util/ListMap$ListMapEntry;->access$100(Lcom/jme3/util/ListMap$ListMapEntry;)Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5, p1}, Lcom/jme3/util/ListMap;->keyEq(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    move v3, v2

    :cond_0
    sget-boolean v5, Lcom/jme3/util/ListMap;->$assertionsDisabled:Z

    if-nez v5, :cond_2

    if-gez v3, :cond_2

    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5}, Ljava/lang/AssertionError;-><init>()V

    throw v5

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v4, v4, -0x1

    move v2, v3

    :goto_1
    if-ge v2, v4, :cond_3

    iget-object v5, p0, Lcom/jme3/util/ListMap;->entries:[Lcom/jme3/util/ListMap$ListMapEntry;

    iget-object v6, p0, Lcom/jme3/util/ListMap;->entries:[Lcom/jme3/util/ListMap$ListMapEntry;

    add-int/lit8 v7, v2, 0x1

    aget-object v6, v6, v7

    aput-object v6, v5, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    return-object v0
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lcom/jme3/util/ListMap;->backingMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    return v0
.end method

.method public values()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<TV;>;"
        }
    .end annotation

    iget-object v0, p0, Lcom/jme3/util/ListMap;->backingMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method
