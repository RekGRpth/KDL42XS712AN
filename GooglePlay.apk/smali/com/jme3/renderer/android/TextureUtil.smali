.class public Lcom/jme3/renderer/android/TextureUtil;
.super Ljava/lang/Object;
.source "TextureUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jme3/renderer/android/TextureUtil$1;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static buildMipmap(Landroid/graphics/Bitmap;)V
    .locals 7
    .param p0    # Landroid/graphics/Bitmap;

    const/4 v6, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    :goto_0
    if-ge v1, v6, :cond_0

    if-lt v3, v6, :cond_1

    :cond_0
    const/16 v4, 0xde1

    const/4 v5, 0x0

    invoke-static {v4, v2, p0, v5}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    if-eq v1, v6, :cond_1

    if-ne v3, v6, :cond_2

    :cond_1
    return-void

    :cond_2
    add-int/lit8 v2, v2, 0x1

    div-int/lit8 v1, v1, 0x2

    div-int/lit8 v3, v3, 0x2

    invoke-static {p0, v3, v1, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object p0, v0

    goto :goto_0
.end method

.method public static uploadTexture(Lcom/jme3/texture/Image;IIIZZZ)V
    .locals 29
    .param p0    # Lcom/jme3/texture/Image;
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Z
    .param p5    # Z
    .param p6    # Z

    invoke-virtual/range {p0 .. p0}, Lcom/jme3/texture/Image;->getEfficentData()Ljava/lang/Object;

    move-result-object v3

    instance-of v3, v3, Lcom/jme3/asset/AndroidImageInfo;

    if-eqz v3, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/jme3/texture/Image;->getEfficentData()Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Lcom/jme3/asset/AndroidImageInfo;

    invoke-virtual/range {v24 .. v24}, Lcom/jme3/asset/AndroidImageInfo;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    move/from16 v0, p1

    move/from16 v1, p5

    move/from16 v2, p6

    invoke-static {v0, v3, v1, v2}, Lcom/jme3/renderer/android/TextureUtil;->uploadTextureBitmap(ILandroid/graphics/Bitmap;ZZ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/jme3/texture/Image;->getFormat()Lcom/jme3/texture/Image$Format;

    move-result-object v23

    if-gez p2, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/jme3/texture/Image;->getData()Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/jme3/texture/Image;->getData()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_3

    :cond_2
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/jme3/texture/Image;->getData(I)Ljava/nio/ByteBuffer;

    move-result-object v10

    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/jme3/texture/Image;->getWidth()I

    move-result v6

    invoke-virtual/range {p0 .. p0}, Lcom/jme3/texture/Image;->getHeight()I

    move-result v7

    invoke-virtual/range {p0 .. p0}, Lcom/jme3/texture/Image;->getDepth()I

    move-result v22

    const/16 v20, 0x0

    const/16 v25, -0x1

    const/4 v5, -0x1

    const/16 v21, -0x1

    sget-object v3, Lcom/jme3/renderer/android/TextureUtil$1;->$SwitchMap$com$jme3$texture$Image$Format:[I

    invoke-virtual/range {v23 .. v23}, Lcom/jme3/texture/Image$Format;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    new-instance v3, Ljava/lang/UnsupportedOperationException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unrecognized format: "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_3
    const/4 v10, 0x0

    goto :goto_1

    :pswitch_0
    const/16 v5, 0x1906

    const/16 v21, 0x1401

    :goto_2
    const/4 v3, -0x1

    move/from16 v0, v25

    if-ne v0, v3, :cond_4

    move/from16 v25, v5

    :cond_4
    if-eqz v10, :cond_5

    const/16 v3, 0xcf5

    const/4 v4, 0x1

    invoke-static {v3, v4}, Landroid/opengl/GLES20;->glPixelStorei(II)V

    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/jme3/texture/Image;->getMipMapSizes()[I

    move-result-object v27

    const/16 v28, 0x0

    if-nez v27, :cond_6

    if-eqz v10, :cond_7

    const/4 v3, 0x1

    new-array v0, v3, [I

    move-object/from16 v27, v0

    const/4 v3, 0x0

    invoke-virtual {v10}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v4

    aput v4, v27, v3

    :cond_6
    :goto_3
    if-eqz v20, :cond_8

    invoke-virtual {v10}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    const/16 v3, 0xde1

    move-object/from16 v0, v27

    array-length v4, v0

    rsub-int/lit8 v4, v4, 0x1

    const/4 v8, 0x0

    invoke-virtual {v10}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v9

    invoke-static/range {v3 .. v10}, Landroid/opengl/GLES20;->glCompressedTexImage2D(IIIIIIILjava/nio/Buffer;)V

    goto/16 :goto_0

    :pswitch_1
    const/16 v5, 0x1909

    const/16 v21, 0x1401

    goto :goto_2

    :pswitch_2
    const/16 v5, 0x190a

    const/16 v21, 0x1401

    goto :goto_2

    :pswitch_3
    const/16 v5, 0x190a

    const/16 v21, 0x1401

    goto :goto_2

    :pswitch_4
    const/16 v5, 0x1909

    const/16 v21, 0x1401

    goto :goto_2

    :pswitch_5
    const/16 v5, 0x1907

    const v25, 0x8d62

    const v21, 0x8363

    goto :goto_2

    :pswitch_6
    const/16 v5, 0x1908

    const v21, 0x8033

    goto :goto_2

    :pswitch_7
    const/16 v5, 0x1907

    const/16 v21, 0x1401

    goto :goto_2

    :pswitch_8
    const/16 v5, 0x1907

    const/16 v21, 0x1401

    goto :goto_2

    :pswitch_9
    const/16 v5, 0x1908

    const v25, 0x8057

    const v21, 0x8034

    goto :goto_2

    :pswitch_a
    const/16 v5, 0x1907

    const/16 v21, 0x1401

    goto :goto_2

    :pswitch_b
    const/16 v5, 0x1907

    const/16 v21, 0x1401

    goto :goto_2

    :pswitch_c
    const/16 v5, 0x1908

    const v25, 0x8056

    const/16 v21, 0x1401

    goto/16 :goto_2

    :pswitch_d
    const/16 v5, 0x1908

    const/16 v21, 0x1401

    goto/16 :goto_2

    :pswitch_e
    const v5, 0x86a3

    const/16 v21, 0x1401

    :pswitch_f
    const/16 v5, 0x1902

    const/16 v21, 0x1401

    goto/16 :goto_2

    :pswitch_10
    const/16 v5, 0x1902

    const v25, 0x81a5

    const/16 v21, 0x1401

    goto/16 :goto_2

    :pswitch_11
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Unsupported depth format: "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v23

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_7
    const/4 v3, 0x1

    new-array v0, v3, [I

    move-object/from16 v27, v0

    const/4 v3, 0x0

    mul-int v4, v6, v7

    invoke-virtual/range {v23 .. v23}, Lcom/jme3/texture/Image$Format;->getBitsPerPixel()I

    move-result v8

    mul-int/2addr v4, v8

    div-int/lit8 v4, v4, 0x8

    aput v4, v27, v3

    goto/16 :goto_3

    :cond_8
    const/4 v12, 0x0

    :goto_4
    move-object/from16 v0, v27

    array-length v3, v0

    if-ge v12, v3, :cond_0

    const/4 v3, 0x1

    shr-int v4, v6, v12

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v14

    const/4 v3, 0x1

    shr-int v4, v7, v12

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v15

    const/4 v3, 0x1

    shr-int v4, v22, v12

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v26

    if-eqz v10, :cond_9

    move/from16 v0, v28

    invoke-virtual {v10, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    aget v3, v27, v12

    add-int v3, v3, v28

    invoke-virtual {v10, v3}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    :cond_9
    if-eqz v20, :cond_a

    if-eqz v10, :cond_a

    const/16 v11, 0xde1

    const/16 v16, 0x0

    invoke-virtual {v10}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v17

    move v13, v5

    move-object/from16 v18, v10

    invoke-static/range {v11 .. v18}, Landroid/opengl/GLES20;->glCompressedTexImage2D(IIIIIIILjava/nio/Buffer;)V

    :goto_5
    aget v3, v27, v12

    add-int v28, v28, v3

    add-int/lit8 v12, v12, 0x1

    goto :goto_4

    :cond_a
    const/16 v11, 0xde1

    const/16 v16, 0x0

    move/from16 v13, v25

    move/from16 v17, v5

    move/from16 v18, v21

    move-object/from16 v19, v10

    invoke-static/range {v11 .. v19}, Landroid/opengl/GLES20;->glTexImage2D(IIIIIIIILjava/nio/Buffer;)V

    goto :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_7
        :pswitch_8
        :pswitch_b
        :pswitch_a
        :pswitch_5
        :pswitch_9
        :pswitch_c
        :pswitch_d
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_e
        :pswitch_6
    .end packed-switch
.end method

.method public static uploadTextureBitmap(ILandroid/graphics/Bitmap;ZZ)V
    .locals 5
    .param p0    # I
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # Z
    .param p3    # Z

    const/4 v4, 0x0

    if-nez p3, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-static {v2}, Lcom/jme3/math/FastMath;->isPowerOfTwo(I)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v1}, Lcom/jme3/math/FastMath;->isPowerOfTwo(I)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    invoke-static {v2}, Lcom/jme3/math/FastMath;->nearestPowerOfTwo(I)I

    move-result v2

    invoke-static {v1}, Lcom/jme3/math/FastMath;->nearestPowerOfTwo(I)I

    move-result v1

    const/4 v3, 0x1

    invoke-static {p1, v2, v1, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object p1, v0

    :cond_1
    if-eqz p2, :cond_2

    invoke-static {p1}, Lcom/jme3/renderer/android/TextureUtil;->buildMipmap(Landroid/graphics/Bitmap;)V

    :goto_0
    return-void

    :cond_2
    invoke-static {p0, v4, p1, v4}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    goto :goto_0
.end method
