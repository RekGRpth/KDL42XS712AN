.class public Lcom/jme3/app/state/AppStateManager;
.super Ljava/lang/Object;
.source "AppStateManager.java"


# instance fields
.field private final app:Lcom/jme3/app/Application;

.field private final initializing:Lcom/jme3/util/SafeArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jme3/util/SafeArrayList",
            "<",
            "Lcom/jme3/app/state/AppState;",
            ">;"
        }
    .end annotation
.end field

.field private final states:Lcom/jme3/util/SafeArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jme3/util/SafeArrayList",
            "<",
            "Lcom/jme3/app/state/AppState;",
            ">;"
        }
    .end annotation
.end field

.field private final terminating:Lcom/jme3/util/SafeArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/jme3/util/SafeArrayList",
            "<",
            "Lcom/jme3/app/state/AppState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/jme3/app/Application;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/jme3/util/SafeArrayList;

    const-class v1, Lcom/jme3/app/state/AppState;

    invoke-direct {v0, v1}, Lcom/jme3/util/SafeArrayList;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/jme3/app/state/AppStateManager;->initializing:Lcom/jme3/util/SafeArrayList;

    new-instance v0, Lcom/jme3/util/SafeArrayList;

    const-class v1, Lcom/jme3/app/state/AppState;

    invoke-direct {v0, v1}, Lcom/jme3/util/SafeArrayList;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/jme3/app/state/AppStateManager;->states:Lcom/jme3/util/SafeArrayList;

    new-instance v0, Lcom/jme3/util/SafeArrayList;

    const-class v1, Lcom/jme3/app/state/AppState;

    invoke-direct {v0, v1}, Lcom/jme3/util/SafeArrayList;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/jme3/app/state/AppStateManager;->terminating:Lcom/jme3/util/SafeArrayList;

    iput-object p1, p0, Lcom/jme3/app/state/AppStateManager;->app:Lcom/jme3/app/Application;

    return-void
.end method


# virtual methods
.method public attach(Lcom/jme3/app/state/AppState;)Z
    .locals 2
    .param p1    # Lcom/jme3/app/state/AppState;

    iget-object v1, p0, Lcom/jme3/app/state/AppStateManager;->states:Lcom/jme3/util/SafeArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/jme3/app/state/AppStateManager;->states:Lcom/jme3/util/SafeArrayList;

    invoke-virtual {v0, p1}, Lcom/jme3/util/SafeArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/jme3/app/state/AppStateManager;->initializing:Lcom/jme3/util/SafeArrayList;

    invoke-virtual {v0, p1}, Lcom/jme3/util/SafeArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1, p0}, Lcom/jme3/app/state/AppState;->stateAttached(Lcom/jme3/app/state/AppStateManager;)V

    iget-object v0, p0, Lcom/jme3/app/state/AppStateManager;->initializing:Lcom/jme3/util/SafeArrayList;

    invoke-virtual {v0, p1}, Lcom/jme3/util/SafeArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    monitor-exit v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public cleanup()V
    .locals 5

    invoke-virtual {p0}, Lcom/jme3/app/state/AppStateManager;->getStates()[Lcom/jme3/app/state/AppState;

    move-result-object v1

    move-object v0, v1

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    invoke-interface {v4}, Lcom/jme3/app/state/AppState;->cleanup()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public detach(Lcom/jme3/app/state/AppState;)Z
    .locals 3
    .param p1    # Lcom/jme3/app/state/AppState;

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/jme3/app/state/AppStateManager;->states:Lcom/jme3/util/SafeArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/jme3/app/state/AppStateManager;->states:Lcom/jme3/util/SafeArrayList;

    invoke-virtual {v2, p1}, Lcom/jme3/util/SafeArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1, p0}, Lcom/jme3/app/state/AppState;->stateDetached(Lcom/jme3/app/state/AppStateManager;)V

    iget-object v2, p0, Lcom/jme3/app/state/AppStateManager;->states:Lcom/jme3/util/SafeArrayList;

    invoke-virtual {v2, p1}, Lcom/jme3/util/SafeArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/jme3/app/state/AppStateManager;->terminating:Lcom/jme3/util/SafeArrayList;

    invoke-virtual {v2, p1}, Lcom/jme3/util/SafeArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v1

    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, Lcom/jme3/app/state/AppStateManager;->initializing:Lcom/jme3/util/SafeArrayList;

    invoke-virtual {v2, p1}, Lcom/jme3/util/SafeArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1, p0}, Lcom/jme3/app/state/AppState;->stateDetached(Lcom/jme3/app/state/AppStateManager;)V

    iget-object v2, p0, Lcom/jme3/app/state/AppStateManager;->initializing:Lcom/jme3/util/SafeArrayList;

    invoke-virtual {v2, p1}, Lcom/jme3/util/SafeArrayList;->remove(Ljava/lang/Object;)Z

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    const/4 v0, 0x0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method protected getInitializing()[Lcom/jme3/app/state/AppState;
    .locals 2

    iget-object v1, p0, Lcom/jme3/app/state/AppStateManager;->states:Lcom/jme3/util/SafeArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/jme3/app/state/AppStateManager;->initializing:Lcom/jme3/util/SafeArrayList;

    invoke-virtual {v0}, Lcom/jme3/util/SafeArrayList;->getArray()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jme3/app/state/AppState;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getState(Ljava/lang/Class;)Lcom/jme3/app/state/AppState;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/jme3/app/state/AppState;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    iget-object v6, p0, Lcom/jme3/app/state/AppStateManager;->states:Lcom/jme3/util/SafeArrayList;

    monitor-enter v6

    :try_start_0
    invoke-virtual {p0}, Lcom/jme3/app/state/AppStateManager;->getStates()[Lcom/jme3/app/state/AppState;

    move-result-object v1

    move-object v0, v1

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v5

    if-eqz v5, :cond_0

    monitor-exit v6

    :goto_1
    return-object v4

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/jme3/app/state/AppStateManager;->getInitializing()[Lcom/jme3/app/state/AppState;

    move-result-object v1

    move-object v0, v1

    array-length v3, v0

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v3, :cond_3

    aget-object v4, v0, v2

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v5

    if-eqz v5, :cond_2

    monitor-exit v6

    goto :goto_1

    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v4, 0x0

    goto :goto_1
.end method

.method protected getStates()[Lcom/jme3/app/state/AppState;
    .locals 2

    iget-object v1, p0, Lcom/jme3/app/state/AppStateManager;->states:Lcom/jme3/util/SafeArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/jme3/app/state/AppStateManager;->states:Lcom/jme3/util/SafeArrayList;

    invoke-virtual {v0}, Lcom/jme3/util/SafeArrayList;->getArray()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jme3/app/state/AppState;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected getTerminating()[Lcom/jme3/app/state/AppState;
    .locals 2

    iget-object v1, p0, Lcom/jme3/app/state/AppStateManager;->states:Lcom/jme3/util/SafeArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/jme3/app/state/AppStateManager;->terminating:Lcom/jme3/util/SafeArrayList;

    invoke-virtual {v0}, Lcom/jme3/util/SafeArrayList;->getArray()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/jme3/app/state/AppState;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected initializePending()V
    .locals 8

    invoke-virtual {p0}, Lcom/jme3/app/state/AppStateManager;->getInitializing()[Lcom/jme3/app/state/AppState;

    move-result-object v1

    array-length v6, v1

    if-nez v6, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v7, p0, Lcom/jme3/app/state/AppStateManager;->states:Lcom/jme3/util/SafeArrayList;

    monitor-enter v7

    :try_start_0
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    iget-object v6, p0, Lcom/jme3/app/state/AppStateManager;->states:Lcom/jme3/util/SafeArrayList;

    invoke-virtual {v6, v5}, Lcom/jme3/util/SafeArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object v6, p0, Lcom/jme3/app/state/AppStateManager;->initializing:Lcom/jme3/util/SafeArrayList;

    invoke-virtual {v6, v5}, Lcom/jme3/util/SafeArrayList;->removeAll(Ljava/util/Collection;)Z

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v0, v1

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    iget-object v6, p0, Lcom/jme3/app/state/AppStateManager;->app:Lcom/jme3/app/Application;

    invoke-interface {v4, p0, v6}, Lcom/jme3/app/state/AppState;->initialize(Lcom/jme3/app/state/AppStateManager;Lcom/jme3/app/Application;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catchall_0
    move-exception v6

    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v6
.end method

.method public postRender()V
    .locals 6

    invoke-virtual {p0}, Lcom/jme3/app/state/AppStateManager;->getStates()[Lcom/jme3/app/state/AppState;

    move-result-object v1

    move-object v0, v1

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    invoke-interface {v4}, Lcom/jme3/app/state/AppState;->isEnabled()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Lcom/jme3/app/state/AppState;->postRender()V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public render(Lcom/jme3/renderer/RenderManager;)V
    .locals 6
    .param p1    # Lcom/jme3/renderer/RenderManager;

    invoke-virtual {p0}, Lcom/jme3/app/state/AppStateManager;->getStates()[Lcom/jme3/app/state/AppState;

    move-result-object v1

    move-object v0, v1

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    invoke-interface {v4}, Lcom/jme3/app/state/AppState;->isEnabled()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4, p1}, Lcom/jme3/app/state/AppState;->render(Lcom/jme3/renderer/RenderManager;)V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected terminatePending()V
    .locals 8

    invoke-virtual {p0}, Lcom/jme3/app/state/AppStateManager;->getTerminating()[Lcom/jme3/app/state/AppState;

    move-result-object v1

    array-length v5, v1

    if-nez v5, :cond_0

    :goto_0
    return-void

    :cond_0
    move-object v0, v1

    array-length v3, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    invoke-interface {v4}, Lcom/jme3/app/state/AppState;->cleanup()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    iget-object v6, p0, Lcom/jme3/app/state/AppStateManager;->states:Lcom/jme3/util/SafeArrayList;

    monitor-enter v6

    :try_start_0
    iget-object v5, p0, Lcom/jme3/app/state/AppStateManager;->terminating:Lcom/jme3/util/SafeArrayList;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/jme3/util/SafeArrayList;->removeAll(Ljava/util/Collection;)Z

    monitor-exit v6

    goto :goto_0

    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5
.end method

.method public update(F)V
    .locals 6
    .param p1    # F

    invoke-virtual {p0}, Lcom/jme3/app/state/AppStateManager;->terminatePending()V

    invoke-virtual {p0}, Lcom/jme3/app/state/AppStateManager;->initializePending()V

    invoke-virtual {p0}, Lcom/jme3/app/state/AppStateManager;->getStates()[Lcom/jme3/app/state/AppState;

    move-result-object v1

    move-object v0, v1

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    invoke-interface {v4}, Lcom/jme3/app/state/AppState;->isEnabled()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4, p1}, Lcom/jme3/app/state/AppState;->update(F)V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method
