.class public Lcom/jme3/input/event/TouchEvent;
.super Lcom/jme3/input/event/InputEvent;
.source "TouchEvent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/jme3/input/event/TouchEvent$Type;
    }
.end annotation


# instance fields
.field private characters:Ljava/lang/String;

.field private deltaX:F

.field private deltaY:F

.field private keyCode:I

.field private pointerId:I

.field private posX:F

.field private posY:F

.field private pressure:F

.field private scaleFactor:F

.field private scaleSpan:F

.field private type:Lcom/jme3/input/event/TouchEvent$Type;


# direct methods
.method public constructor <init>()V
    .locals 6

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/jme3/input/event/InputEvent;-><init>()V

    sget-object v0, Lcom/jme3/input/event/TouchEvent$Type;->IDLE:Lcom/jme3/input/event/TouchEvent$Type;

    iput-object v0, p0, Lcom/jme3/input/event/TouchEvent;->type:Lcom/jme3/input/event/TouchEvent$Type;

    sget-object v1, Lcom/jme3/input/event/TouchEvent$Type;->IDLE:Lcom/jme3/input/event/TouchEvent$Type;

    move-object v0, p0

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/jme3/input/event/TouchEvent;->set(Lcom/jme3/input/event/TouchEvent$Type;FFFF)V

    return-void
.end method


# virtual methods
.method public getDeltaY()F
    .locals 1

    iget v0, p0, Lcom/jme3/input/event/TouchEvent;->deltaY:F

    return v0
.end method

.method public getKeyCode()I
    .locals 1

    iget v0, p0, Lcom/jme3/input/event/TouchEvent;->keyCode:I

    return v0
.end method

.method public getPointerId()I
    .locals 1

    iget v0, p0, Lcom/jme3/input/event/TouchEvent;->pointerId:I

    return v0
.end method

.method public getType()Lcom/jme3/input/event/TouchEvent$Type;
    .locals 1

    iget-object v0, p0, Lcom/jme3/input/event/TouchEvent;->type:Lcom/jme3/input/event/TouchEvent$Type;

    return-object v0
.end method

.method public getX()F
    .locals 1

    iget v0, p0, Lcom/jme3/input/event/TouchEvent;->posX:F

    return v0
.end method

.method public getY()F
    .locals 1

    iget v0, p0, Lcom/jme3/input/event/TouchEvent;->posY:F

    return v0
.end method

.method public set(Lcom/jme3/input/event/TouchEvent$Type;)V
    .locals 6
    .param p1    # Lcom/jme3/input/event/TouchEvent$Type;

    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/jme3/input/event/TouchEvent;->set(Lcom/jme3/input/event/TouchEvent$Type;FFFF)V

    return-void
.end method

.method public set(Lcom/jme3/input/event/TouchEvent$Type;FFFF)V
    .locals 2
    .param p1    # Lcom/jme3/input/event/TouchEvent$Type;
    .param p2    # F
    .param p3    # F
    .param p4    # F
    .param p5    # F

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/jme3/input/event/TouchEvent;->type:Lcom/jme3/input/event/TouchEvent$Type;

    iput p2, p0, Lcom/jme3/input/event/TouchEvent;->posX:F

    iput p3, p0, Lcom/jme3/input/event/TouchEvent;->posY:F

    iput p4, p0, Lcom/jme3/input/event/TouchEvent;->deltaX:F

    iput p5, p0, Lcom/jme3/input/event/TouchEvent;->deltaY:F

    iput v1, p0, Lcom/jme3/input/event/TouchEvent;->pointerId:I

    iput v0, p0, Lcom/jme3/input/event/TouchEvent;->pressure:F

    iput v1, p0, Lcom/jme3/input/event/TouchEvent;->keyCode:I

    iput v0, p0, Lcom/jme3/input/event/TouchEvent;->scaleFactor:F

    iput v0, p0, Lcom/jme3/input/event/TouchEvent;->scaleSpan:F

    const-string v0, ""

    iput-object v0, p0, Lcom/jme3/input/event/TouchEvent;->characters:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/jme3/input/event/TouchEvent;->consumed:Z

    return-void
.end method

.method public setCharacters(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/jme3/input/event/TouchEvent;->characters:Ljava/lang/String;

    return-void
.end method

.method public setKeyCode(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/jme3/input/event/TouchEvent;->keyCode:I

    return-void
.end method

.method public setPointerId(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/jme3/input/event/TouchEvent;->pointerId:I

    return-void
.end method

.method public setPressure(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/jme3/input/event/TouchEvent;->pressure:F

    return-void
.end method

.method public setScaleFactor(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/jme3/input/event/TouchEvent;->scaleFactor:F

    return-void
.end method

.method public setScaleSpan(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/jme3/input/event/TouchEvent;->scaleSpan:F

    return-void
.end method
