.class public Lcom/android/emailcommon/service/LegacyPolicySet;
.super Ljava/lang/Object;
.source "LegacyPolicySet.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static flagsToPolicy(J)Lcom/android/emailcommon/provider/Policy;
    .locals 8
    .param p0    # J

    const-wide/16 v6, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/android/emailcommon/provider/Policy;

    invoke-direct {v0}, Lcom/android/emailcommon/provider/Policy;-><init>()V

    const-wide/16 v4, 0x1e0

    and-long/2addr v4, p0

    long-to-int v1, v4

    shr-int/lit8 v1, v1, 0x5

    iput v1, v0, Lcom/android/emailcommon/provider/Policy;->mPasswordMode:I

    const-wide/16 v4, 0x1f

    and-long/2addr v4, p0

    shr-long/2addr v4, v3

    long-to-int v1, v4

    iput v1, v0, Lcom/android/emailcommon/provider/Policy;->mPasswordMinLength:I

    const-wide/16 v4, 0x3e00

    and-long/2addr v4, p0

    const/16 v1, 0x9

    shr-long/2addr v4, v1

    long-to-int v1, v4

    iput v1, v0, Lcom/android/emailcommon/provider/Policy;->mPasswordMaxFails:I

    const-wide v4, 0x1f00000000000L

    and-long/2addr v4, p0

    const/16 v1, 0x2c

    shr-long/2addr v4, v1

    long-to-int v1, v4

    iput v1, v0, Lcom/android/emailcommon/provider/Policy;->mPasswordComplexChars:I

    const-wide v4, 0xff000000000L

    and-long/2addr v4, p0

    const/16 v1, 0x24

    shr-long/2addr v4, v1

    long-to-int v1, v4

    iput v1, v0, Lcom/android/emailcommon/provider/Policy;->mPasswordHistory:I

    const-wide v4, 0xffc000000L

    and-long/2addr v4, p0

    const/16 v1, 0x1a

    shr-long/2addr v4, v1

    long-to-int v1, v4

    iput v1, v0, Lcom/android/emailcommon/provider/Policy;->mPasswordExpirationDays:I

    const-wide/32 v4, 0x1ffc000    # 1.65699973E-316

    and-long/2addr v4, p0

    const/16 v1, 0xe

    shr-long/2addr v4, v1

    long-to-int v1, v4

    iput v1, v0, Lcom/android/emailcommon/provider/Policy;->mMaxScreenLockTime:I

    const-wide/32 v4, 0x2000000

    and-long/2addr v4, p0

    cmp-long v1, v6, v4

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    iput-boolean v1, v0, Lcom/android/emailcommon/provider/Policy;->mRequireRemoteWipe:Z

    const-wide/high16 v4, 0x2000000000000L

    and-long/2addr v4, p0

    cmp-long v1, v6, v4

    if-eqz v1, :cond_1

    move v1, v2

    :goto_1
    iput-boolean v1, v0, Lcom/android/emailcommon/provider/Policy;->mRequireEncryption:Z

    const-wide/high16 v4, 0x4000000000000L

    and-long/2addr v4, p0

    cmp-long v1, v6, v4

    if-eqz v1, :cond_2

    :goto_2
    iput-boolean v2, v0, Lcom/android/emailcommon/provider/Policy;->mRequireEncryptionExternal:Z

    return-object v0

    :cond_0
    move v1, v3

    goto :goto_0

    :cond_1
    move v1, v3

    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_2
.end method
