.class public Lcom/android/email/activity/MessageListItem;
.super Landroid/view/View;
.source "MessageListItem.java"


# static fields
.field private static ACTIVATED_TEXT_COLOR:I

.field private static DATE_TEXT_COLOR_READ:I

.field private static DATE_TEXT_COLOR_UNREAD:I

.field private static DEFAULT_TEXT_COLOR:I

.field private static SENDERS_TEXT_COLOR_READ:I

.field private static SENDERS_TEXT_COLOR_UNREAD:I

.field private static SNIPPET_TEXT_COLOR_READ:I

.field private static SNIPPET_TEXT_COLOR_UNREAD:I

.field private static SUBJECT_TEXT_COLOR_READ:I

.field private static SUBJECT_TEXT_COLOR_UNREAD:I

.field private static sAttachmentIcon:Landroid/graphics/Bitmap;

.field private static sBadgeMargin:I

.field private static final sBoldPaint:Landroid/text/TextPaint;

.field private static final sDatePaint:Landroid/text/TextPaint;

.field private static final sDefaultPaint:Landroid/text/TextPaint;

.field private static sFavoriteIconOff:Landroid/graphics/Bitmap;

.field private static sFavoriteIconOn:Landroid/graphics/Bitmap;

.field private static sInit:Z

.field private static sInviteIcon:Landroid/graphics/Bitmap;

.field private static sItemHeightNormal:I

.field private static sItemHeightWide:I

.field private static sScaledTouchSlop:I

.field private static sSelectedIconOff:Landroid/graphics/Bitmap;

.field private static sSelectedIconOn:Landroid/graphics/Bitmap;

.field private static sStateForwarded:Landroid/graphics/Bitmap;

.field private static sStateReplied:Landroid/graphics/Bitmap;

.field private static sStateRepliedAndForwarded:Landroid/graphics/Bitmap;

.field private static sSubjectDescription:Ljava/lang/String;

.field private static sSubjectEmptyDescription:Ljava/lang/String;

.field private static sSubjectSnippetDivider:Ljava/lang/String;


# instance fields
.field mAccountId:J

.field private mAdapter:Lcom/android/email/activity/MessagesAdapter;

.field public mColorChipPaint:Landroid/graphics/Paint;

.field private mContext:Landroid/content/Context;

.field private mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

.field private mCurentBackground:Landroid/graphics/drawable/Drawable;

.field private mDownEvent:Z

.field private mFormattedDate:Ljava/lang/CharSequence;

.field private mFormattedSender:Ljava/lang/CharSequence;

.field public mHasAttachment:Z

.field public mHasBeenForwarded:Z

.field public mHasBeenRepliedTo:Z

.field public mHasInvite:Z

.field public mIsFavorite:Z

.field private mIsSearchResult:Z

.field private mLayout:Lcom/android/email/activity/ThreePaneLayout;

.field mMailboxId:J

.field mMessageId:J

.field private mMode:I

.field public mRead:Z

.field private mReadSelector:Landroid/graphics/drawable/Drawable;

.field public mSender:Ljava/lang/String;

.field public mSnippet:Ljava/lang/CharSequence;

.field private mSubject:Ljava/lang/String;

.field private mSubjectLayout:Landroid/text/StaticLayout;

.field public mText:Landroid/text/SpannableStringBuilder;

.field mTimeFormatted:J

.field private mUnreadSelector:Landroid/graphics/drawable/Drawable;

.field private mViewHeight:I

.field private mViewWidth:I

.field private mWideReadSelector:Landroid/graphics/drawable/Drawable;

.field private mWideUnreadSelector:Landroid/graphics/drawable/Drawable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/email/activity/MessageListItem;->sInit:Z

    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    sput-object v0, Lcom/android/email/activity/MessageListItem;->sDefaultPaint:Landroid/text/TextPaint;

    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    sput-object v0, Lcom/android/email/activity/MessageListItem;->sBoldPaint:Landroid/text/TextPaint;

    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    sput-object v0, Lcom/android/email/activity/MessageListItem;->sDatePaint:Landroid/text/TextPaint;

    const/4 v0, -0x1

    sput v0, Lcom/android/email/activity/MessageListItem;->sScaledTouchSlop:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-boolean v1, p0, Lcom/android/email/activity/MessageListItem;->mIsSearchResult:Z

    iput-boolean v1, p0, Lcom/android/email/activity/MessageListItem;->mHasAttachment:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/email/activity/MessageListItem;->mHasInvite:Z

    iput-boolean v1, p0, Lcom/android/email/activity/MessageListItem;->mIsFavorite:Z

    iput-boolean v1, p0, Lcom/android/email/activity/MessageListItem;->mHasBeenRepliedTo:Z

    iput-boolean v1, p0, Lcom/android/email/activity/MessageListItem;->mHasBeenForwarded:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/email/activity/MessageListItem;->mMode:I

    iput v1, p0, Lcom/android/email/activity/MessageListItem;->mViewWidth:I

    iput v1, p0, Lcom/android/email/activity/MessageListItem;->mViewHeight:I

    const-string v0, ""

    iput-object v0, p0, Lcom/android/email/activity/MessageListItem;->mFormattedDate:Ljava/lang/CharSequence;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/email/activity/MessageListItem;->mTimeFormatted:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/email/activity/MessageListItem;->mCurentBackground:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, p1}, Lcom/android/email/activity/MessageListItem;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v1, p0, Lcom/android/email/activity/MessageListItem;->mIsSearchResult:Z

    iput-boolean v1, p0, Lcom/android/email/activity/MessageListItem;->mHasAttachment:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/email/activity/MessageListItem;->mHasInvite:Z

    iput-boolean v1, p0, Lcom/android/email/activity/MessageListItem;->mIsFavorite:Z

    iput-boolean v1, p0, Lcom/android/email/activity/MessageListItem;->mHasBeenRepliedTo:Z

    iput-boolean v1, p0, Lcom/android/email/activity/MessageListItem;->mHasBeenForwarded:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/email/activity/MessageListItem;->mMode:I

    iput v1, p0, Lcom/android/email/activity/MessageListItem;->mViewWidth:I

    iput v1, p0, Lcom/android/email/activity/MessageListItem;->mViewHeight:I

    const-string v0, ""

    iput-object v0, p0, Lcom/android/email/activity/MessageListItem;->mFormattedDate:Ljava/lang/CharSequence;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/email/activity/MessageListItem;->mTimeFormatted:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/email/activity/MessageListItem;->mCurentBackground:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, p1}, Lcom/android/email/activity/MessageListItem;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-boolean v1, p0, Lcom/android/email/activity/MessageListItem;->mIsSearchResult:Z

    iput-boolean v1, p0, Lcom/android/email/activity/MessageListItem;->mHasAttachment:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/email/activity/MessageListItem;->mHasInvite:Z

    iput-boolean v1, p0, Lcom/android/email/activity/MessageListItem;->mIsFavorite:Z

    iput-boolean v1, p0, Lcom/android/email/activity/MessageListItem;->mHasBeenRepliedTo:Z

    iput-boolean v1, p0, Lcom/android/email/activity/MessageListItem;->mHasBeenForwarded:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/email/activity/MessageListItem;->mMode:I

    iput v1, p0, Lcom/android/email/activity/MessageListItem;->mViewWidth:I

    iput v1, p0, Lcom/android/email/activity/MessageListItem;->mViewHeight:I

    const-string v0, ""

    iput-object v0, p0, Lcom/android/email/activity/MessageListItem;->mFormattedDate:Ljava/lang/CharSequence;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/email/activity/MessageListItem;->mTimeFormatted:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/email/activity/MessageListItem;->mCurentBackground:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, p1}, Lcom/android/email/activity/MessageListItem;->init(Landroid/content/Context;)V

    return-void
.end method

.method private calculateDrawingData()V
    .locals 11

    const/4 v7, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    sget-object v0, Lcom/android/email/activity/MessageListItem;->sDefaultPaint:Landroid/text/TextPaint;

    iget-object v1, p0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v1, v1, Lcom/android/email/activity/MessageListItemCoordinates;->subjectFontSize:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    invoke-direct {p0}, Lcom/android/email/activity/MessageListItem;->calculateSubjectText()V

    new-instance v0, Landroid/text/StaticLayout;

    iget-object v1, p0, Lcom/android/email/activity/MessageListItem;->mText:Landroid/text/SpannableStringBuilder;

    sget-object v2, Lcom/android/email/activity/MessageListItem;->sDefaultPaint:Landroid/text/TextPaint;

    iget-object v3, p0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v3, v3, Lcom/android/email/activity/MessageListItemCoordinates;->subjectWidth:I

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lcom/android/email/activity/MessageListItem;->mSubjectLayout:Landroid/text/StaticLayout;

    iget-object v0, p0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v0, v0, Lcom/android/email/activity/MessageListItemCoordinates;->subjectLineCount:I

    iget-object v1, p0, Lcom/android/email/activity/MessageListItem;->mSubjectLayout:Landroid/text/StaticLayout;

    invoke-virtual {v1}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/MessageListItem;->mSubjectLayout:Landroid/text/StaticLayout;

    iget-object v1, p0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v1, v1, Lcom/android/email/activity/MessageListItemCoordinates;->subjectLineCount:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/text/StaticLayout;->getLineEnd(I)I

    move-result v8

    new-instance v0, Landroid/text/StaticLayout;

    iget-object v1, p0, Lcom/android/email/activity/MessageListItem;->mText:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1, v7, v8}, Landroid/text/SpannableStringBuilder;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    sget-object v2, Lcom/android/email/activity/MessageListItem;->sDefaultPaint:Landroid/text/TextPaint;

    iget-object v3, p0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v3, v3, Lcom/android/email/activity/MessageListItemCoordinates;->subjectWidth:I

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/4 v7, 0x1

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lcom/android/email/activity/MessageListItem;->mSubjectLayout:Landroid/text/StaticLayout;

    :cond_0
    iget-boolean v0, p0, Lcom/android/email/activity/MessageListItem;->mRead:Z

    if-eqz v0, :cond_1

    sget-object v9, Lcom/android/email/activity/MessageListItem;->sDefaultPaint:Landroid/text/TextPaint;

    :goto_0
    iget-object v0, p0, Lcom/android/email/activity/MessageListItem;->mSender:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, ""

    iput-object v0, p0, Lcom/android/email/activity/MessageListItem;->mFormattedSender:Ljava/lang/CharSequence;

    :goto_1
    return-void

    :cond_1
    sget-object v9, Lcom/android/email/activity/MessageListItem;->sBoldPaint:Landroid/text/TextPaint;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v10, v0, Lcom/android/email/activity/MessageListItemCoordinates;->sendersWidth:I

    iget-object v0, p0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v0, v0, Lcom/android/email/activity/MessageListItemCoordinates;->sendersFontSize:I

    int-to-float v0, v0

    invoke-virtual {v9, v0}, Landroid/text/TextPaint;->setTextSize(F)V

    iget-boolean v0, p0, Lcom/android/email/activity/MessageListItem;->mRead:Z

    if-eqz v0, :cond_3

    sget v0, Lcom/android/email/activity/MessageListItem;->SENDERS_TEXT_COLOR_READ:I

    :goto_2
    invoke-direct {p0, v0}, Lcom/android/email/activity/MessageListItem;->getFontColor(I)I

    move-result v0

    invoke-virtual {v9, v0}, Landroid/text/TextPaint;->setColor(I)V

    iget-object v0, p0, Lcom/android/email/activity/MessageListItem;->mSender:Ljava/lang/String;

    int-to-float v1, v10

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v0, v9, v1, v2}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/activity/MessageListItem;->mFormattedSender:Ljava/lang/CharSequence;

    goto :goto_1

    :cond_3
    sget v0, Lcom/android/email/activity/MessageListItem;->SENDERS_TEXT_COLOR_UNREAD:I

    goto :goto_2
.end method

.method private calculateSubjectText()V
    .locals 9

    const/16 v8, 0x21

    iget-object v4, p0, Lcom/android/email/activity/MessageListItem;->mText:Landroid/text/SpannableStringBuilder;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/email/activity/MessageListItem;->mText:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/android/email/activity/MessageListItem;->mSubject:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    iget-boolean v4, p0, Lcom/android/email/activity/MessageListItem;->mRead:Z

    if-eqz v4, :cond_3

    sget v4, Lcom/android/email/activity/MessageListItem;->SUBJECT_TEXT_COLOR_READ:I

    :goto_1
    invoke-direct {p0, v4}, Lcom/android/email/activity/MessageListItem;->getFontColor(I)I

    move-result v3

    iget-object v4, p0, Lcom/android/email/activity/MessageListItem;->mText:Landroid/text/SpannableStringBuilder;

    new-instance v5, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v5, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/android/email/activity/MessageListItem;->mSubject:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    iget-object v4, p0, Lcom/android/email/activity/MessageListItem;->mSubject:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v2, v4, 0x1

    :cond_2
    iget-object v4, p0, Lcom/android/email/activity/MessageListItem;->mSnippet:Ljava/lang/CharSequence;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-boolean v4, p0, Lcom/android/email/activity/MessageListItem;->mRead:Z

    if-eqz v4, :cond_4

    sget v4, Lcom/android/email/activity/MessageListItem;->SNIPPET_TEXT_COLOR_READ:I

    :goto_2
    invoke-direct {p0, v4}, Lcom/android/email/activity/MessageListItem;->getFontColor(I)I

    move-result v1

    iget-object v4, p0, Lcom/android/email/activity/MessageListItem;->mText:Landroid/text/SpannableStringBuilder;

    new-instance v5, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v5, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iget-object v6, p0, Lcom/android/email/activity/MessageListItem;->mText:Landroid/text/SpannableStringBuilder;

    invoke-virtual {v6}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    invoke-virtual {v4, v5, v2, v6, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    :cond_3
    sget v4, Lcom/android/email/activity/MessageListItem;->SUBJECT_TEXT_COLOR_UNREAD:I

    goto :goto_1

    :cond_4
    sget v4, Lcom/android/email/activity/MessageListItem;->SNIPPET_TEXT_COLOR_UNREAD:I

    goto :goto_2
.end method

.method private getFontColor(I)I
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListItem;->isActivated()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/MessageListItem;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/android/email/activity/MessageListItemCoordinates;->isMultiPane(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget p1, Lcom/android/email/activity/MessageListItem;->ACTIVATED_TEXT_COLOR:I

    :cond_0
    return p1
.end method

.method private getViewMode(I)I
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/android/email/activity/MessageListItem;->mContext:Landroid/content/Context;

    iget-boolean v1, p0, Lcom/android/email/activity/MessageListItem;->mIsSearchResult:Z

    invoke-static {v0, p1, v1}, Lcom/android/email/activity/MessageListItemCoordinates;->getMode(Landroid/content/Context;IZ)I

    move-result v0

    return v0
.end method

.method private init(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    const/4 v3, 0x1

    iput-object p1, p0, Lcom/android/email/activity/MessageListItem;->mContext:Landroid/content/Context;

    sget-boolean v1, Lcom/android/email/activity/MessageListItem;->sInit:Z

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080058    # com.android.email.R.string.message_subject_description

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/email/activity/MessageListItem;->sSubjectDescription:Ljava/lang/String;

    const v1, 0x7f080059    # com.android.email.R.string.message_is_empty_description

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/email/activity/MessageListItem;->sSubjectEmptyDescription:Ljava/lang/String;

    const v1, 0x7f0800ab    # com.android.email.R.string.message_list_subject_snippet_divider

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/email/activity/MessageListItem;->sSubjectSnippetDivider:Ljava/lang/String;

    const v1, 0x7f0a0007    # com.android.email.R.dimen.message_list_item_height_wide

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/android/email/activity/MessageListItem;->sItemHeightWide:I

    const v1, 0x7f0a0008    # com.android.email.R.dimen.message_list_item_height_normal

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/android/email/activity/MessageListItem;->sItemHeightNormal:I

    sget-object v1, Lcom/android/email/activity/MessageListItem;->sDefaultPaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sget-object v1, Lcom/android/email/activity/MessageListItem;->sDefaultPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/android/email/activity/MessageListItem;->sDatePaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sget-object v1, Lcom/android/email/activity/MessageListItem;->sDatePaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    sget-object v1, Lcom/android/email/activity/MessageListItem;->sBoldPaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sget-object v1, Lcom/android/email/activity/MessageListItem;->sBoldPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    const v1, 0x7f02001e    # com.android.email.R.drawable.ic_badge_attachment

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/android/email/activity/MessageListItem;->sAttachmentIcon:Landroid/graphics/Bitmap;

    const v1, 0x7f020020    # com.android.email.R.drawable.ic_badge_invite_holo_light

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/android/email/activity/MessageListItem;->sInviteIcon:Landroid/graphics/Bitmap;

    const v1, 0x7f0a000b    # com.android.email.R.dimen.message_list_badge_margin

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/android/email/activity/MessageListItem;->sBadgeMargin:I

    const v1, 0x7f020009    # com.android.email.R.drawable.btn_star_off_normal_email_holo_light

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/android/email/activity/MessageListItem;->sFavoriteIconOff:Landroid/graphics/Bitmap;

    const v1, 0x7f02000b    # com.android.email.R.drawable.btn_star_on_normal_email_holo_light

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/android/email/activity/MessageListItem;->sFavoriteIconOn:Landroid/graphics/Bitmap;

    const v1, 0x7f020004    # com.android.email.R.drawable.btn_check_off_normal_holo_light

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/android/email/activity/MessageListItem;->sSelectedIconOff:Landroid/graphics/Bitmap;

    const v1, 0x7f020005    # com.android.email.R.drawable.btn_check_on_normal_holo_light

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/android/email/activity/MessageListItem;->sSelectedIconOn:Landroid/graphics/Bitmap;

    const v1, 0x7f020022    # com.android.email.R.drawable.ic_badge_reply_holo_light

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/android/email/activity/MessageListItem;->sStateReplied:Landroid/graphics/Bitmap;

    const v1, 0x7f02001f    # com.android.email.R.drawable.ic_badge_forward_holo_light

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/android/email/activity/MessageListItem;->sStateForwarded:Landroid/graphics/Bitmap;

    const v1, 0x7f020021    # com.android.email.R.drawable.ic_badge_reply_forward_holo_light

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/android/email/activity/MessageListItem;->sStateRepliedAndForwarded:Landroid/graphics/Bitmap;

    const v1, 0x7f090027    # com.android.email.R.color.default_text_color

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/android/email/activity/MessageListItem;->DEFAULT_TEXT_COLOR:I

    const v1, 0x106000b    # android.R.color.white

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/android/email/activity/MessageListItem;->ACTIVATED_TEXT_COLOR:I

    const v1, 0x7f09002b    # com.android.email.R.color.subject_text_color_read

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/android/email/activity/MessageListItem;->SUBJECT_TEXT_COLOR_READ:I

    const v1, 0x7f090028    # com.android.email.R.color.subject_text_color_unread

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/android/email/activity/MessageListItem;->SUBJECT_TEXT_COLOR_UNREAD:I

    const v1, 0x7f09002c    # com.android.email.R.color.snippet_text_color_read

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/android/email/activity/MessageListItem;->SNIPPET_TEXT_COLOR_READ:I

    const v1, 0x7f090029    # com.android.email.R.color.snippet_text_color_unread

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/android/email/activity/MessageListItem;->SNIPPET_TEXT_COLOR_UNREAD:I

    const v1, 0x7f09002d    # com.android.email.R.color.senders_text_color_read

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/android/email/activity/MessageListItem;->SENDERS_TEXT_COLOR_READ:I

    const v1, 0x7f09002a    # com.android.email.R.color.senders_text_color_unread

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/android/email/activity/MessageListItem;->SENDERS_TEXT_COLOR_UNREAD:I

    const v1, 0x7f09002f    # com.android.email.R.color.date_text_color_read

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/android/email/activity/MessageListItem;->DATE_TEXT_COLOR_READ:I

    const v1, 0x7f09002e    # com.android.email.R.color.date_text_color_unread

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/android/email/activity/MessageListItem;->DATE_TEXT_COLOR_UNREAD:I

    sput-boolean v3, Lcom/android/email/activity/MessageListItem;->sInit:Z

    :cond_0
    return-void
.end method

.method private initializeSlop(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;

    sget v4, Lcom/android/email/activity/MessageListItem;->sScaledTouchSlop:I

    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v1, v4, Landroid/util/DisplayMetrics;->density:F

    const/4 v4, 0x4

    invoke-virtual {v0, v4}, Landroid/content/res/Configuration;->isLayoutSizeAtLeast(I)Z

    move-result v4

    if-eqz v4, :cond_1

    const/high16 v4, 0x3fc00000    # 1.5f

    mul-float v3, v1, v4

    :goto_0
    const/high16 v4, 0x41c00000    # 24.0f

    mul-float/2addr v4, v3

    const/high16 v5, 0x3f000000    # 0.5f

    add-float/2addr v4, v5

    float-to-int v4, v4

    sput v4, Lcom/android/email/activity/MessageListItem;->sScaledTouchSlop:I

    :cond_0
    return-void

    :cond_1
    move v3, v1

    goto :goto_0
.end method

.method private measureHeight(II)I
    .locals 4
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    if-ne v1, v3, :cond_1

    move v0, v2

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v3, p0, Lcom/android/email/activity/MessageListItem;->mMode:I

    if-nez v3, :cond_2

    sget v0, Lcom/android/email/activity/MessageListItem;->sItemHeightWide:I

    :goto_1
    const/high16 v3, -0x80000000

    if-ne v1, v3, :cond_0

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    :cond_2
    sget v0, Lcom/android/email/activity/MessageListItem;->sItemHeightNormal:I

    goto :goto_1
.end method

.method private populateContentDescription()V
    .locals 2

    iget-object v0, p0, Lcom/android/email/activity/MessageListItem;->mSubject:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/android/email/activity/MessageListItem;->sSubjectDescription:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/email/activity/MessageListItem;->mSubject:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/email/activity/MessageListItem;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/android/email/activity/MessageListItem;->sSubjectEmptyDescription:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/android/email/activity/MessageListItem;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static resetDrawingCaches()V
    .locals 1

    invoke-static {}, Lcom/android/email/activity/MessageListItemCoordinates;->resetCaches()V

    const/4 v0, 0x0

    sput-boolean v0, Lcom/android/email/activity/MessageListItem;->sInit:Z

    return-void
.end method

.method private updateBackground()V
    .locals 4

    iget-object v2, p0, Lcom/android/email/activity/MessageListItem;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/email/activity/MessageListItemCoordinates;->isMultiPane(Landroid/content/Context;)Z

    move-result v0

    iget-boolean v2, p0, Lcom/android/email/activity/MessageListItem;->mRead:Z

    if-eqz v2, :cond_4

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/android/email/activity/MessageListItem;->mLayout:Lcom/android/email/activity/ThreePaneLayout;

    invoke-virtual {v2}, Lcom/android/email/activity/ThreePaneLayout;->isLeftPaneVisible()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/android/email/activity/MessageListItem;->mWideReadSelector:Landroid/graphics/drawable/Drawable;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListItem;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020014    # com.android.email.R.drawable.conversation_wide_read_selector

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/android/email/activity/MessageListItem;->mWideReadSelector:Landroid/graphics/drawable/Drawable;

    :cond_0
    iget-object v1, p0, Lcom/android/email/activity/MessageListItem;->mWideReadSelector:Landroid/graphics/drawable/Drawable;

    :goto_0
    iget-object v2, p0, Lcom/android/email/activity/MessageListItem;->mCurentBackground:Landroid/graphics/drawable/Drawable;

    if-eq v1, v2, :cond_1

    invoke-virtual {p0, v1}, Lcom/android/email/activity/MessageListItem;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iput-object v1, p0, Lcom/android/email/activity/MessageListItem;->mCurentBackground:Landroid/graphics/drawable/Drawable;

    :cond_1
    return-void

    :cond_2
    iget-object v2, p0, Lcom/android/email/activity/MessageListItem;->mReadSelector:Landroid/graphics/drawable/Drawable;

    if-nez v2, :cond_3

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListItem;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020012    # com.android.email.R.drawable.conversation_read_selector

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/android/email/activity/MessageListItem;->mReadSelector:Landroid/graphics/drawable/Drawable;

    :cond_3
    iget-object v1, p0, Lcom/android/email/activity/MessageListItem;->mReadSelector:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :cond_4
    if-eqz v0, :cond_6

    iget-object v2, p0, Lcom/android/email/activity/MessageListItem;->mLayout:Lcom/android/email/activity/ThreePaneLayout;

    invoke-virtual {v2}, Lcom/android/email/activity/ThreePaneLayout;->isLeftPaneVisible()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/android/email/activity/MessageListItem;->mWideUnreadSelector:Landroid/graphics/drawable/Drawable;

    if-nez v2, :cond_5

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListItem;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020015    # com.android.email.R.drawable.conversation_wide_unread_selector

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/android/email/activity/MessageListItem;->mWideUnreadSelector:Landroid/graphics/drawable/Drawable;

    :cond_5
    iget-object v1, p0, Lcom/android/email/activity/MessageListItem;->mWideUnreadSelector:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lcom/android/email/activity/MessageListItem;->mUnreadSelector:Landroid/graphics/drawable/Drawable;

    if-nez v2, :cond_7

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListItem;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020013    # com.android.email.R.drawable.conversation_unread_selector

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/android/email/activity/MessageListItem;->mUnreadSelector:Landroid/graphics/drawable/Drawable;

    :cond_7
    iget-object v1, p0, Lcom/android/email/activity/MessageListItem;->mUnreadSelector:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method


# virtual methods
.method public bindViewInit(Lcom/android/email/activity/MessagesAdapter;Lcom/android/email/activity/ThreePaneLayout;Z)V
    .locals 0
    .param p1    # Lcom/android/email/activity/MessagesAdapter;
    .param p2    # Lcom/android/email/activity/ThreePaneLayout;
    .param p3    # Z

    iput-object p2, p0, Lcom/android/email/activity/MessageListItem;->mLayout:Lcom/android/email/activity/ThreePaneLayout;

    iput-object p1, p0, Lcom/android/email/activity/MessageListItem;->mAdapter:Lcom/android/email/activity/MessagesAdapter;

    iput-boolean p3, p0, Lcom/android/email/activity/MessageListItem;->mIsSearchResult:Z

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListItem;->requestLayout()V

    return-void
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 2
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    const/4 v1, 0x1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListItem;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V

    invoke-virtual {p1, v1}, Landroid/view/accessibility/AccessibilityEvent;->setEnabled(Z)V

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListItem;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    return v1
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1    # Landroid/graphics/Canvas;

    iget-object v0, p0, Lcom/android/email/activity/MessageListItem;->mAdapter:Lcom/android/email/activity/MessagesAdapter;

    invoke-virtual {v0, p0}, Lcom/android/email/activity/MessagesAdapter;->isSelected(Lcom/android/email/activity/MessageListItem;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/email/activity/MessageListItem;->setSelected(Z)V

    invoke-direct {p0}, Lcom/android/email/activity/MessageListItem;->updateBackground()V

    invoke-super {p0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 17
    .param p1    # Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/email/activity/MessageListItem;->mColorChipPaint:Landroid/graphics/Paint;

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v1, v1, Lcom/android/email/activity/MessageListItemCoordinates;->chipX:I

    int-to-float v2, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v1, v1, Lcom/android/email/activity/MessageListItemCoordinates;->chipY:I

    int-to-float v3, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v1, v1, Lcom/android/email/activity/MessageListItemCoordinates;->chipX:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v4, v4, Lcom/android/email/activity/MessageListItemCoordinates;->chipWidth:I

    add-int/2addr v1, v4

    int-to-float v4, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v1, v1, Lcom/android/email/activity/MessageListItemCoordinates;->chipY:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v5, v5, Lcom/android/email/activity/MessageListItemCoordinates;->chipHeight:I

    add-int/2addr v1, v5

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/email/activity/MessageListItem;->mColorChipPaint:Landroid/graphics/Paint;

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/email/activity/MessageListItem;->mAdapter:Lcom/android/email/activity/MessagesAdapter;

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Lcom/android/email/activity/MessagesAdapter;->isSelected(Lcom/android/email/activity/MessageListItem;)Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v1, Lcom/android/email/activity/MessageListItem;->sSelectedIconOn:Landroid/graphics/Bitmap;

    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v2, v2, Lcom/android/email/activity/MessageListItemCoordinates;->checkmarkX:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v3, v3, Lcom/android/email/activity/MessageListItemCoordinates;->checkmarkY:I

    int-to-float v3, v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/email/activity/MessageListItem;->mRead:Z

    if-eqz v1, :cond_5

    sget-object v7, Lcom/android/email/activity/MessageListItem;->sDefaultPaint:Landroid/text/TextPaint;

    :goto_1
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/email/activity/MessageListItem;->mRead:Z

    if-eqz v1, :cond_6

    sget v1, Lcom/android/email/activity/MessageListItem;->SENDERS_TEXT_COLOR_READ:I

    :goto_2
    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/android/email/activity/MessageListItem;->getFontColor(I)I

    move-result v1

    invoke-virtual {v7, v1}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v1, v1, Lcom/android/email/activity/MessageListItemCoordinates;->sendersFontSize:I

    int-to-float v1, v1

    invoke-virtual {v7, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/activity/MessageListItem;->mFormattedSender:Ljava/lang/CharSequence;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/email/activity/MessageListItem;->mFormattedSender:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v1, v1, Lcom/android/email/activity/MessageListItemCoordinates;->sendersX:I

    int-to-float v5, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v1, v1, Lcom/android/email/activity/MessageListItemCoordinates;->sendersY:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v6, v6, Lcom/android/email/activity/MessageListItemCoordinates;->sendersAscent:I

    sub-int/2addr v1, v6

    int-to-float v6, v1

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/email/activity/MessageListItem;->mHasBeenRepliedTo:Z

    if-eqz v1, :cond_7

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/email/activity/MessageListItem;->mHasBeenForwarded:Z

    if-eqz v1, :cond_7

    sget-object v1, Lcom/android/email/activity/MessageListItem;->sStateRepliedAndForwarded:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v2, v2, Lcom/android/email/activity/MessageListItemCoordinates;->stateX:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v3, v3, Lcom/android/email/activity/MessageListItemCoordinates;->stateY:I

    int-to-float v3, v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_1
    :goto_3
    sget-object v1, Lcom/android/email/activity/MessageListItem;->sDefaultPaint:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v2, v2, Lcom/android/email/activity/MessageListItemCoordinates;->subjectFontSize:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v1, v1, Lcom/android/email/activity/MessageListItemCoordinates;->subjectX:I

    int-to-float v1, v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v2, v2, Lcom/android/email/activity/MessageListItemCoordinates;->subjectY:I

    int-to-float v2, v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/email/activity/MessageListItem;->mSubjectLayout:Landroid/text/StaticLayout;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    sget-object v1, Lcom/android/email/activity/MessageListItem;->sDatePaint:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v2, v2, Lcom/android/email/activity/MessageListItemCoordinates;->dateFontSize:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    sget-object v2, Lcom/android/email/activity/MessageListItem;->sDatePaint:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/email/activity/MessageListItem;->mRead:Z

    if-eqz v1, :cond_9

    sget v1, Lcom/android/email/activity/MessageListItem;->DATE_TEXT_COLOR_READ:I

    :goto_4
    invoke-virtual {v2, v1}, Landroid/text/TextPaint;->setColor(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v1, v1, Lcom/android/email/activity/MessageListItemCoordinates;->dateXEnd:I

    sget-object v2, Lcom/android/email/activity/MessageListItem;->sDatePaint:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/email/activity/MessageListItem;->mFormattedDate:Ljava/lang/CharSequence;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/android/email/activity/MessageListItem;->mFormattedDate:Ljava/lang/CharSequence;

    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    move-result v5

    invoke-virtual {v2, v3, v4, v5}, Landroid/text/TextPaint;->measureText(Ljava/lang/CharSequence;II)F

    move-result v2

    float-to-int v2, v2

    sub-int v15, v1, v2

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/android/email/activity/MessageListItem;->mFormattedDate:Ljava/lang/CharSequence;

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/email/activity/MessageListItem;->mFormattedDate:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v11

    int-to-float v12, v15

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v1, v1, Lcom/android/email/activity/MessageListItemCoordinates;->dateY:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v2, v2, Lcom/android/email/activity/MessageListItemCoordinates;->dateAscent:I

    sub-int/2addr v1, v2

    int-to-float v13, v1

    sget-object v14, Lcom/android/email/activity/MessageListItem;->sDatePaint:Landroid/text/TextPaint;

    move-object/from16 v8, p1

    invoke-virtual/range {v8 .. v14}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/email/activity/MessageListItem;->mIsFavorite:Z

    if-eqz v1, :cond_a

    sget-object v1, Lcom/android/email/activity/MessageListItem;->sFavoriteIconOn:Landroid/graphics/Bitmap;

    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v2, v2, Lcom/android/email/activity/MessageListItemCoordinates;->starX:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v3, v3, Lcom/android/email/activity/MessageListItemCoordinates;->starY:I

    int-to-float v3, v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    sget v1, Lcom/android/email/activity/MessageListItem;->sBadgeMargin:I

    sub-int v16, v15, v1

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/email/activity/MessageListItem;->mHasAttachment:Z

    if-eqz v1, :cond_2

    sget-object v1, Lcom/android/email/activity/MessageListItem;->sAttachmentIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int v16, v16, v1

    sget-object v1, Lcom/android/email/activity/MessageListItem;->sAttachmentIcon:Landroid/graphics/Bitmap;

    move/from16 v0, v16

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v3, v3, Lcom/android/email/activity/MessageListItemCoordinates;->paperclipY:I

    int-to-float v3, v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_2
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/email/activity/MessageListItem;->mHasInvite:Z

    if-eqz v1, :cond_3

    sget-object v1, Lcom/android/email/activity/MessageListItem;->sInviteIcon:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int v16, v16, v1

    sget-object v1, Lcom/android/email/activity/MessageListItem;->sInviteIcon:Landroid/graphics/Bitmap;

    move/from16 v0, v16

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v3, v3, Lcom/android/email/activity/MessageListItemCoordinates;->paperclipY:I

    int-to-float v3, v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_3
    return-void

    :cond_4
    sget-object v1, Lcom/android/email/activity/MessageListItem;->sSelectedIconOff:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    :cond_5
    sget-object v7, Lcom/android/email/activity/MessageListItem;->sBoldPaint:Landroid/text/TextPaint;

    goto/16 :goto_1

    :cond_6
    sget v1, Lcom/android/email/activity/MessageListItem;->SENDERS_TEXT_COLOR_UNREAD:I

    goto/16 :goto_2

    :cond_7
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/email/activity/MessageListItem;->mHasBeenRepliedTo:Z

    if-eqz v1, :cond_8

    sget-object v1, Lcom/android/email/activity/MessageListItem;->sStateReplied:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v2, v2, Lcom/android/email/activity/MessageListItemCoordinates;->stateX:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v3, v3, Lcom/android/email/activity/MessageListItemCoordinates;->stateY:I

    int-to-float v3, v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_3

    :cond_8
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/email/activity/MessageListItem;->mHasBeenForwarded:Z

    if-eqz v1, :cond_1

    sget-object v1, Lcom/android/email/activity/MessageListItem;->sStateForwarded:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v2, v2, Lcom/android/email/activity/MessageListItemCoordinates;->stateX:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v3, v3, Lcom/android/email/activity/MessageListItemCoordinates;->stateY:I

    int-to-float v3, v3

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_3

    :cond_9
    sget v1, Lcom/android/email/activity/MessageListItem;->DATE_TEXT_COLOR_UNREAD:I

    goto/16 :goto_4

    :cond_a
    sget-object v1, Lcom/android/email/activity/MessageListItem;->sFavoriteIconOff:Landroid/graphics/Bitmap;

    goto/16 :goto_5
.end method

.method protected onLayout(ZIIII)V
    .locals 3
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    iget-object v0, p0, Lcom/android/email/activity/MessageListItem;->mContext:Landroid/content/Context;

    iget v1, p0, Lcom/android/email/activity/MessageListItem;->mViewWidth:I

    iget-boolean v2, p0, Lcom/android/email/activity/MessageListItem;->mIsSearchResult:Z

    invoke-static {v0, v1, v2}, Lcom/android/email/activity/MessageListItemCoordinates;->forWidth(Landroid/content/Context;IZ)Lcom/android/email/activity/MessageListItemCoordinates;

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    invoke-direct {p0}, Lcom/android/email/activity/MessageListItem;->calculateDrawingData()V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    if-nez p1, :cond_0

    iget v1, p0, Lcom/android/email/activity/MessageListItem;->mViewWidth:I

    if-nez v1, :cond_2

    :cond_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/email/activity/MessageListItem;->mViewWidth:I

    iget v1, p0, Lcom/android/email/activity/MessageListItem;->mViewWidth:I

    invoke-direct {p0, v1}, Lcom/android/email/activity/MessageListItem;->getViewMode(I)I

    move-result v0

    iget v1, p0, Lcom/android/email/activity/MessageListItem;->mMode:I

    if-eq v0, v1, :cond_1

    iput v0, p0, Lcom/android/email/activity/MessageListItem;->mMode:I

    :cond_1
    iget v1, p0, Lcom/android/email/activity/MessageListItem;->mMode:I

    invoke-direct {p0, p2, v1}, Lcom/android/email/activity/MessageListItem;->measureHeight(II)I

    move-result v1

    iput v1, p0, Lcom/android/email/activity/MessageListItem;->mViewHeight:I

    :cond_2
    iget v1, p0, Lcom/android/email/activity/MessageListItem;->mViewWidth:I

    iget v2, p0, Lcom/android/email/activity/MessageListItem;->mViewHeight:I

    invoke-virtual {p0, v1, v2}, Lcom/android/email/activity/MessageListItem;->setMeasuredDimension(II)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1    # Landroid/view/MotionEvent;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListItem;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/android/email/activity/MessageListItem;->initializeSlop(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    float-to-int v3, v6

    iget-object v6, p0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v6, v6, Lcom/android/email/activity/MessageListItemCoordinates;->checkmarkX:I

    iget-object v7, p0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v7, v7, Lcom/android/email/activity/MessageListItemCoordinates;->checkmarkWidthIncludingMargins:I

    add-int/2addr v6, v7

    sget v7, Lcom/android/email/activity/MessageListItem;->sScaledTouchSlop:I

    add-int v0, v6, v7

    iget-object v6, p0, Lcom/android/email/activity/MessageListItem;->mCoordinates:Lcom/android/email/activity/MessageListItemCoordinates;

    iget v6, v6, Lcom/android/email/activity/MessageListItemCoordinates;->starX:I

    sget v7, Lcom/android/email/activity/MessageListItem;->sScaledTouchSlop:I

    sub-int v2, v6, v7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    if-eqz v1, :cond_5

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListItem;->invalidate()V

    :goto_1
    return v1

    :pswitch_1
    if-lt v3, v0, :cond_1

    if-le v3, v2, :cond_0

    :cond_1
    iput-boolean v4, p0, Lcom/android/email/activity/MessageListItem;->mDownEvent:Z

    if-lt v3, v0, :cond_2

    if-le v3, v2, :cond_0

    :cond_2
    const/4 v1, 0x1

    goto :goto_0

    :pswitch_2
    iput-boolean v5, p0, Lcom/android/email/activity/MessageListItem;->mDownEvent:Z

    goto :goto_0

    :pswitch_3
    iget-boolean v6, p0, Lcom/android/email/activity/MessageListItem;->mDownEvent:Z

    if-eqz v6, :cond_0

    if-ge v3, v0, :cond_3

    iget-object v4, p0, Lcom/android/email/activity/MessageListItem;->mAdapter:Lcom/android/email/activity/MessagesAdapter;

    invoke-virtual {v4, p0}, Lcom/android/email/activity/MessagesAdapter;->toggleSelected(Lcom/android/email/activity/MessageListItem;)V

    const/4 v1, 0x1

    goto :goto_0

    :cond_3
    if-le v3, v2, :cond_0

    iget-boolean v6, p0, Lcom/android/email/activity/MessageListItem;->mIsFavorite:Z

    if-nez v6, :cond_4

    :goto_2
    iput-boolean v4, p0, Lcom/android/email/activity/MessageListItem;->mIsFavorite:Z

    iget-object v4, p0, Lcom/android/email/activity/MessageListItem;->mAdapter:Lcom/android/email/activity/MessagesAdapter;

    iget-boolean v5, p0, Lcom/android/email/activity/MessageListItem;->mIsFavorite:Z

    invoke-virtual {v4, p0, v5}, Lcom/android/email/activity/MessagesAdapter;->updateFavorite(Lcom/android/email/activity/MessageListItem;Z)V

    const/4 v1, 0x1

    goto :goto_0

    :cond_4
    move v4, v5

    goto :goto_2

    :cond_5
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public setText(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    const/4 v5, 0x0

    const/4 v0, 0x0

    iget-object v4, p0, Lcom/android/email/activity/MessageListItem;->mSubject:Ljava/lang/String;

    invoke-static {v4, p1}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    iput-object p1, p0, Lcom/android/email/activity/MessageListItem;->mSubject:Ljava/lang/String;

    const/4 v0, 0x1

    invoke-direct {p0}, Lcom/android/email/activity/MessageListItem;->populateContentDescription()V

    :cond_0
    iget-object v4, p0, Lcom/android/email/activity/MessageListItem;->mSnippet:Ljava/lang/CharSequence;

    invoke-static {v4, p2}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    iput-object p2, p0, Lcom/android/email/activity/MessageListItem;->mSnippet:Ljava/lang/CharSequence;

    const/4 v0, 0x1

    :cond_1
    if-nez p3, :cond_2

    if-nez v0, :cond_2

    iget-object v4, p0, Lcom/android/email/activity/MessageListItem;->mSubject:Ljava/lang/String;

    if-nez v4, :cond_6

    iget-object v4, p0, Lcom/android/email/activity/MessageListItem;->mSnippet:Ljava/lang/CharSequence;

    if-nez v4, :cond_6

    :cond_2
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3}, Landroid/text/SpannableStringBuilder;-><init>()V

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/android/email/activity/MessageListItem;->mSubject:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    new-instance v2, Landroid/text/SpannableString;

    iget-object v4, p0, Lcom/android/email/activity/MessageListItem;->mSubject:Ljava/lang/String;

    invoke-direct {v2, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    new-instance v6, Landroid/text/style/StyleSpan;

    iget-boolean v4, p0, Lcom/android/email/activity/MessageListItem;->mRead:Z

    if-eqz v4, :cond_7

    move v4, v5

    :goto_0
    invoke-direct {v6, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v2}, Landroid/text/SpannableString;->length()I

    move-result v4

    const/16 v7, 0x21

    invoke-virtual {v2, v6, v5, v4, v7}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v3, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const/4 v1, 0x1

    :cond_3
    iget-object v4, p0, Lcom/android/email/activity/MessageListItem;->mSnippet:Ljava/lang/CharSequence;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    if-eqz v1, :cond_4

    sget-object v4, Lcom/android/email/activity/MessageListItem;->sSubjectSnippetDivider:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_4
    iget-object v4, p0, Lcom/android/email/activity/MessageListItem;->mSnippet:Ljava/lang/CharSequence;

    invoke-virtual {v3, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_5
    iput-object v3, p0, Lcom/android/email/activity/MessageListItem;->mText:Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0}, Lcom/android/email/activity/MessageListItem;->requestLayout()V

    :cond_6
    return-void

    :cond_7
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public setTimestamp(J)V
    .locals 2
    .param p1    # J

    iget-wide v0, p0, Lcom/android/email/activity/MessageListItem;->mTimeFormatted:J

    cmp-long v0, v0, p1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/MessageListItem;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Landroid/text/format/DateUtils;->getRelativeTimeSpanString(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/activity/MessageListItem;->mFormattedDate:Ljava/lang/CharSequence;

    iput-wide p1, p0, Lcom/android/email/activity/MessageListItem;->mTimeFormatted:J

    :cond_0
    return-void
.end method
