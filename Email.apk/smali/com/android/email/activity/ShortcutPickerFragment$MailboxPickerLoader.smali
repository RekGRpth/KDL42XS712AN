.class final Lcom/android/email/activity/ShortcutPickerFragment$MailboxPickerLoader;
.super Landroid/content/CursorLoader;
.source "ShortcutPickerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/email/activity/ShortcutPickerFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "MailboxPickerLoader"
.end annotation


# instance fields
.field private final mAccountId:J

.field private final mAllowUnread:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;JZ)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/net/Uri;
    .param p3    # [Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # [Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # J
    .param p9    # Z

    invoke-direct/range {p0 .. p6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    iput-wide p7, p0, Lcom/android/email/activity/ShortcutPickerFragment$MailboxPickerLoader;->mAccountId:J

    iput-boolean p9, p0, Lcom/android/email/activity/ShortcutPickerFragment$MailboxPickerLoader;->mAllowUnread:Z

    return-void
.end method


# virtual methods
.method public loadInBackground()Landroid/database/Cursor;
    .locals 12

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    new-instance v3, Landroid/database/MatrixCursor;

    # getter for: Lcom/android/email/activity/ShortcutPickerFragment$MailboxShortcutPickerFragment;->MATRIX_PROJECTION:[Ljava/lang/String;
    invoke-static {}, Lcom/android/email/activity/ShortcutPickerFragment$MailboxShortcutPickerFragment;->access$000()[Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/email/activity/ShortcutPickerFragment$MailboxPickerLoader;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-boolean v4, p0, Lcom/android/email/activity/ShortcutPickerFragment$MailboxPickerLoader;->mAllowUnread:Z

    if-eqz v4, :cond_0

    const v4, 0x7f080043    # com.android.email.R.string.picker_mailbox_name_all_unread

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v4, v11, [Ljava/lang/Object;

    const-wide/32 v5, 0x7ffffffc

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v8

    const-wide/16 v5, -0x3

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v9

    aput-object v2, v4, v10

    invoke-virtual {v3, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    :cond_0
    iget-wide v4, p0, Lcom/android/email/activity/ShortcutPickerFragment$MailboxPickerLoader;->mAccountId:J

    const-wide/high16 v6, 0x1000000000000000L

    cmp-long v4, v4, v6

    if-nez v4, :cond_1

    new-instance v0, Landroid/database/MatrixCursor;

    # getter for: Lcom/android/email/activity/ShortcutPickerFragment$MailboxShortcutPickerFragment;->MATRIX_PROJECTION:[Ljava/lang/String;
    invoke-static {}, Lcom/android/email/activity/ShortcutPickerFragment$MailboxShortcutPickerFragment;->access$000()[Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    const v4, 0x7f080044    # com.android.email.R.string.picker_mailbox_name_all_inbox

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v4, v11, [Ljava/lang/Object;

    const-wide/32 v5, 0x7ffffffd

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v8

    const-wide/16 v5, -0x2

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v9

    aput-object v2, v4, v10

    invoke-virtual {v0, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    new-instance v4, Landroid/database/MergeCursor;

    new-array v5, v10, [Landroid/database/Cursor;

    aput-object v0, v5, v8

    aput-object v3, v5, v9

    invoke-direct {v4, v5}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    :goto_0
    return-object v4

    :cond_1
    new-instance v4, Landroid/database/MergeCursor;

    new-array v5, v10, [Landroid/database/Cursor;

    invoke-super {p0}, Landroid/content/CursorLoader;->loadInBackground()Landroid/database/Cursor;

    move-result-object v6

    aput-object v6, v5, v8

    aput-object v3, v5, v9

    invoke-direct {v4, v5}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method public bridge synthetic loadInBackground()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/android/email/activity/ShortcutPickerFragment$MailboxPickerLoader;->loadInBackground()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
