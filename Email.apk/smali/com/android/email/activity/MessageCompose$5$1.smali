.class Lcom/android/email/activity/MessageCompose$5$1;
.super Ljava/lang/Object;
.source "MessageCompose.java"

# interfaces
.implements Lcom/android/email/activity/MessageCompose$AttachmentLoadedCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/email/activity/MessageCompose$5;->onMessageLoaded(Lcom/android/emailcommon/provider/EmailContent$Message;Lcom/android/emailcommon/provider/EmailContent$Body;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/email/activity/MessageCompose$5;


# direct methods
.method constructor <init>(Lcom/android/email/activity/MessageCompose$5;)V
    .locals 0

    iput-object p1, p0, Lcom/android/email/activity/MessageCompose$5$1;->this$1:Lcom/android/email/activity/MessageCompose$5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAttachmentLoaded([Lcom/android/emailcommon/provider/EmailContent$Attachment;)V
    .locals 9
    .param p1    # [Lcom/android/emailcommon/provider/EmailContent$Attachment;

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/android/email/activity/MessageCompose$5$1;->this$1:Lcom/android/email/activity/MessageCompose$5;

    iget-object v6, v6, Lcom/android/email/activity/MessageCompose$5;->this$0:Lcom/android/email/activity/MessageCompose;

    # getter for: Lcom/android/email/activity/MessageCompose;->mAccount:Lcom/android/emailcommon/provider/Account;
    invoke-static {v6}, Lcom/android/email/activity/MessageCompose;->access$100(Lcom/android/email/activity/MessageCompose;)Lcom/android/emailcommon/provider/Account;

    move-result-object v6

    iget v6, v6, Lcom/android/emailcommon/provider/Account;->mFlags:I

    and-int/lit16 v6, v6, 0x80

    if-eqz v6, :cond_1

    move v4, v5

    :goto_0
    move-object v0, p1

    array-length v3, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_2

    aget-object v1, v0, v2

    if-eqz v4, :cond_0

    iget v6, v1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFlags:I

    or-int/lit16 v6, v6, 0x100

    iput v6, v1, Lcom/android/emailcommon/provider/EmailContent$Attachment;->mFlags:I

    :cond_0
    iget-object v6, p0, Lcom/android/email/activity/MessageCompose$5$1;->this$1:Lcom/android/email/activity/MessageCompose$5;

    iget-object v6, v6, Lcom/android/email/activity/MessageCompose$5;->this$0:Lcom/android/email/activity/MessageCompose;

    # getter for: Lcom/android/email/activity/MessageCompose;->mSourceAttachments:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/android/email/activity/MessageCompose;->access$800(Lcom/android/email/activity/MessageCompose;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    :cond_2
    iget-object v6, p0, Lcom/android/email/activity/MessageCompose$5$1;->this$1:Lcom/android/email/activity/MessageCompose$5;

    iget-object v6, v6, Lcom/android/email/activity/MessageCompose$5;->this$0:Lcom/android/email/activity/MessageCompose;

    # invokes: Lcom/android/email/activity/MessageCompose;->isForward()Z
    invoke-static {v6}, Lcom/android/email/activity/MessageCompose;->access$1000(Lcom/android/email/activity/MessageCompose;)Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/android/email/activity/MessageCompose$5$1;->this$1:Lcom/android/email/activity/MessageCompose$5;

    iget-boolean v6, v6, Lcom/android/email/activity/MessageCompose$5;->val$restoreViews:Z

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/android/email/activity/MessageCompose$5$1;->this$1:Lcom/android/email/activity/MessageCompose$5;

    iget-object v6, v6, Lcom/android/email/activity/MessageCompose$5;->this$0:Lcom/android/email/activity/MessageCompose;

    iget-object v7, p0, Lcom/android/email/activity/MessageCompose$5$1;->this$1:Lcom/android/email/activity/MessageCompose$5;

    iget-object v7, v7, Lcom/android/email/activity/MessageCompose$5;->this$0:Lcom/android/email/activity/MessageCompose;

    # getter for: Lcom/android/email/activity/MessageCompose;->mAttachments:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/android/email/activity/MessageCompose;->access$1100(Lcom/android/email/activity/MessageCompose;)Ljava/util/ArrayList;

    move-result-object v7

    iget-object v8, p0, Lcom/android/email/activity/MessageCompose$5$1;->this$1:Lcom/android/email/activity/MessageCompose$5;

    iget-object v8, v8, Lcom/android/email/activity/MessageCompose$5;->this$0:Lcom/android/email/activity/MessageCompose;

    # getter for: Lcom/android/email/activity/MessageCompose;->mSourceAttachments:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/android/email/activity/MessageCompose;->access$800(Lcom/android/email/activity/MessageCompose;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v6, v7, v8, v5}, Lcom/android/email/activity/MessageCompose;->processSourceMessageAttachments(Ljava/util/List;Ljava/util/List;Z)Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/android/email/activity/MessageCompose$5$1;->this$1:Lcom/android/email/activity/MessageCompose$5;

    iget-object v6, v6, Lcom/android/email/activity/MessageCompose$5;->this$0:Lcom/android/email/activity/MessageCompose;

    # invokes: Lcom/android/email/activity/MessageCompose;->updateAttachmentUi()V
    invoke-static {v6}, Lcom/android/email/activity/MessageCompose;->access$1200(Lcom/android/email/activity/MessageCompose;)V

    iget-object v6, p0, Lcom/android/email/activity/MessageCompose$5$1;->this$1:Lcom/android/email/activity/MessageCompose$5;

    iget-object v6, v6, Lcom/android/email/activity/MessageCompose$5;->this$0:Lcom/android/email/activity/MessageCompose;

    # invokes: Lcom/android/email/activity/MessageCompose;->setMessageChanged(Z)V
    invoke-static {v6, v5}, Lcom/android/email/activity/MessageCompose;->access$000(Lcom/android/email/activity/MessageCompose;Z)V

    :cond_3
    return-void
.end method
