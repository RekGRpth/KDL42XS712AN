.class Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;
.super Lcom/android/email/AttachmentInfo;
.source "MessageViewFragmentBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/email/activity/MessageViewFragmentBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MessageViewAttachmentInfo"
.end annotation


# static fields
.field private static final sSavedFileInfos:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/android/email/AttachmentInfo;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private cancelButton:Landroid/widget/Button;

.field private iconView:Landroid/widget/ImageView;

.field private infoButton:Landroid/widget/Button;

.field private loadButton:Landroid/widget/Button;

.field private loaded:Z

.field private final mProgressView:Landroid/widget/ProgressBar;

.field private openButton:Landroid/widget/Button;

.field private saveButton:Landroid/widget/Button;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/google/common/collect/Maps;->newHashMap()Ljava/util/HashMap;

    move-result-object v0

    sput-object v0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->sSavedFileInfos:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;

    invoke-direct {p0, p1, p2}, Lcom/android/email/AttachmentInfo;-><init>(Landroid/content/Context;Lcom/android/email/AttachmentInfo;)V

    iget-object v0, p2, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->openButton:Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->openButton:Landroid/widget/Button;

    iget-object v0, p2, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->saveButton:Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->saveButton:Landroid/widget/Button;

    iget-object v0, p2, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->loadButton:Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->loadButton:Landroid/widget/Button;

    iget-object v0, p2, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->infoButton:Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->infoButton:Landroid/widget/Button;

    iget-object v0, p2, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->cancelButton:Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->cancelButton:Landroid/widget/Button;

    iget-object v0, p2, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->iconView:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->iconView:Landroid/widget/ImageView;

    iget-object v0, p2, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->mProgressView:Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->mProgressView:Landroid/widget/ProgressBar;

    iget-boolean v0, p2, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->loaded:Z

    iput-boolean v0, p0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->loaded:Z

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;Lcom/android/email/activity/MessageViewFragmentBase$1;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;
    .param p3    # Lcom/android/email/activity/MessageViewFragmentBase$1;

    invoke-direct {p0, p1, p2}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;-><init>(Landroid/content/Context;Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Attachment;Landroid/widget/ProgressBar;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .param p3    # Landroid/widget/ProgressBar;

    invoke-direct {p0, p1, p2}, Lcom/android/email/AttachmentInfo;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Attachment;)V

    iput-object p3, p0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->mProgressView:Landroid/widget/ProgressBar;

    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Attachment;Landroid/widget/ProgressBar;Lcom/android/email/activity/MessageViewFragmentBase$1;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/emailcommon/provider/EmailContent$Attachment;
    .param p3    # Landroid/widget/ProgressBar;
    .param p4    # Lcom/android/email/activity/MessageViewFragmentBase$1;

    invoke-direct {p0, p1, p2, p3}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/EmailContent$Attachment;Landroid/widget/ProgressBar;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)Z
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;

    iget-boolean v0, p0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->loaded:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;Z)Z
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->loaded:Z

    return p1
.end method

.method static synthetic access$2500(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)Landroid/widget/ImageView;
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->iconView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$2502(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;
    .param p1    # Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->iconView:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$2600(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->openButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$2602(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;Landroid/widget/Button;)Landroid/widget/Button;
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;
    .param p1    # Landroid/widget/Button;

    iput-object p1, p0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->openButton:Landroid/widget/Button;

    return-object p1
.end method

.method static synthetic access$2700(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->saveButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$2702(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;Landroid/widget/Button;)Landroid/widget/Button;
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;
    .param p1    # Landroid/widget/Button;

    iput-object p1, p0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->saveButton:Landroid/widget/Button;

    return-object p1
.end method

.method static synthetic access$2800(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->infoButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$2802(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;Landroid/widget/Button;)Landroid/widget/Button;
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;
    .param p1    # Landroid/widget/Button;

    iput-object p1, p0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->infoButton:Landroid/widget/Button;

    return-object p1
.end method

.method static synthetic access$700(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->setSavedPath(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$800(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->loadButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$802(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;Landroid/widget/Button;)Landroid/widget/Button;
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;
    .param p1    # Landroid/widget/Button;

    iput-object p1, p0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->loadButton:Landroid/widget/Button;

    return-object p1
.end method

.method static synthetic access$900(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->cancelButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$902(Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;Landroid/widget/Button;)Landroid/widget/Button;
    .locals 0
    .param p0    # Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;
    .param p1    # Landroid/widget/Button;

    iput-object p1, p0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->cancelButton:Landroid/widget/Button;

    return-object p1
.end method

.method private getSavedPath()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->sSavedFileInfos:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method private setSavedPath(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    sget-object v0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->sSavedFileInfos:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->sSavedFileInfos:Ljava/util/Map;

    invoke-interface {v0, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method


# virtual methods
.method protected getUriForIntent(Landroid/content/Context;J)Landroid/net/Uri;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # J

    invoke-direct {p0}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->getSavedPath()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "file://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->getSavedPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/android/email/AttachmentInfo;->getUriForIntent(Landroid/content/Context;J)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0
.end method

.method public hideProgress()V
    .locals 2

    const/4 v1, 0x4

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->mProgressView:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result v0

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->mProgressView:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public isFileSaved()Z
    .locals 3

    invoke-direct {p0}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->getSavedPath()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v1, 0x0

    :cond_0
    :goto_0
    return v1

    :cond_1
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->setSavedPath(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public showProgress(I)V
    .locals 2
    .param p1    # I

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->mProgressView:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->mProgressView:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->mProgressView:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->isIndeterminate()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->mProgressView:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    :cond_1
    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->mProgressView:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    const/16 v0, 0x64

    if-ne p1, v0, :cond_2

    invoke-virtual {p0}, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->hideProgress()V

    :cond_2
    return-void
.end method

.method public showProgressIndeterminate()V
    .locals 2

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->mProgressView:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->mProgressView:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->mProgressView:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->isIndeterminate()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/email/activity/MessageViewFragmentBase$MessageViewAttachmentInfo;->mProgressView:Landroid/widget/ProgressBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    :cond_1
    return-void
.end method
