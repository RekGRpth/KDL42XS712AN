.class Lcom/android/email/activity/MessagesAdapter;
.super Landroid/widget/CursorAdapter;
.source "MessagesAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/email/activity/MessagesAdapter$1;,
        Lcom/android/email/activity/MessagesAdapter$SearchCursorLoader;,
        Lcom/android/email/activity/MessagesAdapter$SearchResultsCursor;,
        Lcom/android/email/activity/MessagesAdapter$MessagesCursorLoader;,
        Lcom/android/email/activity/MessagesAdapter$MessagesCursor;,
        Lcom/android/email/activity/MessagesAdapter$Callback;
    }
.end annotation


# static fields
.field static final MESSAGE_PROJECTION:[Ljava/lang/String;


# instance fields
.field private final mCallback:Lcom/android/email/activity/MessagesAdapter$Callback;

.field private mIsSearchResult:Z

.field private mLayout:Lcom/android/email/activity/ThreePaneLayout;

.field private mQuery:Ljava/lang/String;

.field private final mResourceHelper:Lcom/android/email/ResourceHelper;

.field private final mSelectedSet:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mShowColorChips:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "mailboxKey"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "accountKey"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "displayName"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "subject"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "timeStamp"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "flagRead"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "flagFavorite"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "flagAttachment"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "flags"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "snippet"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/email/activity/MessagesAdapter;->MESSAGE_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/email/activity/MessagesAdapter$Callback;Z)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/email/activity/MessagesAdapter$Callback;
    .param p3    # Z

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, v2}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/email/activity/MessagesAdapter;->mSelectedSet:Ljava/util/HashSet;

    iput-boolean v2, p0, Lcom/android/email/activity/MessagesAdapter;->mIsSearchResult:Z

    invoke-static {p1}, Lcom/android/email/ResourceHelper;->getInstance(Landroid/content/Context;)Lcom/android/email/ResourceHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/activity/MessagesAdapter;->mResourceHelper:Lcom/android/email/ResourceHelper;

    iput-object p2, p0, Lcom/android/email/activity/MessagesAdapter;->mCallback:Lcom/android/email/activity/MessagesAdapter$Callback;

    iput-boolean p3, p0, Lcom/android/email/activity/MessagesAdapter;->mIsSearchResult:Z

    return-void
.end method

.method private changeFavoriteIcon(Lcom/android/email/activity/MessageListItem;Z)V
    .locals 0
    .param p1    # Lcom/android/email/activity/MessageListItem;
    .param p2    # Z

    invoke-virtual {p1}, Lcom/android/email/activity/MessageListItem;->invalidate()V

    return-void
.end method

.method public static createLoader(Landroid/content/Context;Lcom/android/email/MessageListContext;)Landroid/content/Loader;
    .locals 3
    .param p0    # Landroid/content/Context;
    .param p1    # Lcom/android/email/MessageListContext;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/android/email/MessageListContext;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MessagesAdapter createLoader listContext="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p1}, Lcom/android/email/MessageListContext;->isSearch()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/android/email/activity/MessagesAdapter$SearchCursorLoader;

    invoke-direct {v0, p0, p1}, Lcom/android/email/activity/MessagesAdapter$SearchCursorLoader;-><init>(Landroid/content/Context;Lcom/android/email/MessageListContext;)V

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/android/email/activity/MessagesAdapter$MessagesCursorLoader;

    invoke-direct {v0, p0, p1}, Lcom/android/email/activity/MessagesAdapter$MessagesCursorLoader;-><init>(Landroid/content/Context;Lcom/android/email/MessageListContext;)V

    goto :goto_0
.end method

.method private updateSelected(Lcom/android/email/activity/MessageListItem;Z)V
    .locals 3
    .param p1    # Lcom/android/email/activity/MessageListItem;
    .param p2    # Z

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/android/email/activity/MessagesAdapter;->mSelectedSet:Ljava/util/HashSet;

    iget-wide v1, p1, Lcom/android/email/activity/MessageListItem;->mMessageId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :goto_0
    iget-object v0, p0, Lcom/android/email/activity/MessagesAdapter;->mCallback:Lcom/android/email/activity/MessagesAdapter$Callback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/MessagesAdapter;->mCallback:Lcom/android/email/activity/MessagesAdapter$Callback;

    iget-object v1, p0, Lcom/android/email/activity/MessagesAdapter;->mSelectedSet:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v1

    invoke-interface {v0, p1, p2, v1}, Lcom/android/email/activity/MessagesAdapter$Callback;->onAdapterSelectedChanged(Lcom/android/email/activity/MessageListItem;ZI)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/email/activity/MessagesAdapter;->mSelectedSet:Ljava/util/HashSet;

    iget-wide v1, p1, Lcom/android/email/activity/MessageListItem;->mMessageId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 11
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    const/16 v10, 0xa

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object v4, p1

    check-cast v4, Lcom/android/email/activity/MessageListItem;

    iget-object v8, p0, Lcom/android/email/activity/MessagesAdapter;->mLayout:Lcom/android/email/activity/ThreePaneLayout;

    iget-boolean v9, p0, Lcom/android/email/activity/MessagesAdapter;->mIsSearchResult:Z

    invoke-virtual {v4, p0, v8, v9}, Lcom/android/email/activity/MessageListItem;->bindViewInit(Lcom/android/email/activity/MessagesAdapter;Lcom/android/email/activity/ThreePaneLayout;Z)V

    invoke-interface {p3, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    iput-wide v8, v4, Lcom/android/email/activity/MessageListItem;->mMessageId:J

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    iput-wide v8, v4, Lcom/android/email/activity/MessageListItem;->mMailboxId:J

    const/4 v8, 0x2

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v4, Lcom/android/email/activity/MessageListItem;->mAccountId:J

    const/4 v8, 0x6

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    if-eqz v8, :cond_1

    move v3, v6

    :goto_0
    iget-boolean v8, v4, Lcom/android/email/activity/MessageListItem;->mRead:Z

    if-eq v3, v8, :cond_2

    move v5, v6

    :goto_1
    iput-boolean v3, v4, Lcom/android/email/activity/MessageListItem;->mRead:Z

    const/4 v8, 0x7

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    if-eqz v8, :cond_3

    move v8, v6

    :goto_2
    iput-boolean v8, v4, Lcom/android/email/activity/MessageListItem;->mIsFavorite:Z

    const/16 v8, 0x9

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    and-int/lit8 v8, v2, 0x4

    if-eqz v8, :cond_4

    move v8, v6

    :goto_3
    iput-boolean v8, v4, Lcom/android/email/activity/MessageListItem;->mHasInvite:Z

    const/high16 v8, 0x40000

    and-int/2addr v8, v2

    if-eqz v8, :cond_5

    move v8, v6

    :goto_4
    iput-boolean v8, v4, Lcom/android/email/activity/MessageListItem;->mHasBeenRepliedTo:Z

    const/high16 v8, 0x80000

    and-int/2addr v8, v2

    if-eqz v8, :cond_6

    move v8, v6

    :goto_5
    iput-boolean v8, v4, Lcom/android/email/activity/MessageListItem;->mHasBeenForwarded:Z

    const/16 v8, 0x8

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    if-eqz v8, :cond_7

    :goto_6
    iput-boolean v6, v4, Lcom/android/email/activity/MessageListItem;->mHasAttachment:Z

    const/4 v6, 0x5

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/android/email/activity/MessageListItem;->setTimestamp(J)V

    const/4 v6, 0x3

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v4, Lcom/android/email/activity/MessageListItem;->mSender:Ljava/lang/String;

    const/4 v6, 0x4

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {p3, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v6, v7, v5}, Lcom/android/email/activity/MessageListItem;->setText(Ljava/lang/String;Ljava/lang/String;Z)V

    iget-boolean v6, p0, Lcom/android/email/activity/MessagesAdapter;->mShowColorChips:Z

    if-eqz v6, :cond_8

    iget-object v6, p0, Lcom/android/email/activity/MessagesAdapter;->mResourceHelper:Lcom/android/email/ResourceHelper;

    invoke-virtual {v6, v0, v1}, Lcom/android/email/ResourceHelper;->getAccountColorPaint(J)Landroid/graphics/Paint;

    move-result-object v6

    :goto_7
    iput-object v6, v4, Lcom/android/email/activity/MessageListItem;->mColorChipPaint:Landroid/graphics/Paint;

    iget-object v6, p0, Lcom/android/email/activity/MessagesAdapter;->mQuery:Ljava/lang/String;

    if-eqz v6, :cond_0

    iget-object v6, v4, Lcom/android/email/activity/MessageListItem;->mSnippet:Ljava/lang/CharSequence;

    if-eqz v6, :cond_0

    invoke-interface {p3, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/android/email/activity/MessagesAdapter;->mQuery:Ljava/lang/String;

    invoke-static {v6, v7}, Lcom/android/emailcommon/utility/TextUtilities;->highlightTermsInText(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v6

    iput-object v6, v4, Lcom/android/email/activity/MessageListItem;->mSnippet:Ljava/lang/CharSequence;

    :cond_0
    return-void

    :cond_1
    move v3, v7

    goto :goto_0

    :cond_2
    move v5, v7

    goto :goto_1

    :cond_3
    move v8, v7

    goto :goto_2

    :cond_4
    move v8, v7

    goto :goto_3

    :cond_5
    move v8, v7

    goto :goto_4

    :cond_6
    move v8, v7

    goto :goto_5

    :cond_7
    move v6, v7

    goto :goto_6

    :cond_8
    const/4 v6, 0x0

    goto :goto_7
.end method

.method public clearSelection()V
    .locals 2

    invoke-virtual {p0}, Lcom/android/email/activity/MessagesAdapter;->getSelectedSet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    if-lez v1, :cond_0

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    invoke-virtual {p0}, Lcom/android/email/activity/MessagesAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public getSelectedSet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/email/activity/MessagesAdapter;->mSelectedSet:Ljava/util/HashSet;

    return-object v0
.end method

.method public isSelected(Lcom/android/email/activity/MessageListItem;)Z
    .locals 3
    .param p1    # Lcom/android/email/activity/MessageListItem;

    invoke-virtual {p0}, Lcom/android/email/activity/MessagesAdapter;->getSelectedSet()Ljava/util/Set;

    move-result-object v0

    iget-wide v1, p1, Lcom/android/email/activity/MessageListItem;->mMessageId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public loadState(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/android/email/activity/MessagesAdapter;->getSelectedSet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    const-string v6, "com.android.email.activity.MessagesAdapter.checkedItems"

    invoke-virtual {p1, v6}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v0

    array-length v5, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v5, :cond_0

    aget-wide v3, v0, v2

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/android/email/activity/MessagesAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;
    .param p3    # Landroid/view/ViewGroup;

    new-instance v0, Lcom/android/email/activity/MessageListItem;

    invoke-direct {v0, p1}, Lcom/android/email/activity/MessageListItem;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/email/activity/MessageListItem;->setVisibility(I)V

    return-object v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const-string v0, "com.android.email.activity.MessagesAdapter.checkedItems"

    invoke-virtual {p0}, Lcom/android/email/activity/MessagesAdapter;->getSelectedSet()Ljava/util/Set;

    move-result-object v1

    invoke-static {v1}, Lcom/android/emailcommon/utility/Utility;->toPrimitiveLongArray(Ljava/util/Collection;)[J

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    return-void
.end method

.method public setLayout(Lcom/android/email/activity/ThreePaneLayout;)V
    .locals 0
    .param p1    # Lcom/android/email/activity/ThreePaneLayout;

    iput-object p1, p0, Lcom/android/email/activity/MessagesAdapter;->mLayout:Lcom/android/email/activity/ThreePaneLayout;

    return-void
.end method

.method public setQuery(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/email/activity/MessagesAdapter;->mQuery:Ljava/lang/String;

    return-void
.end method

.method public setShowColorChips(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/email/activity/MessagesAdapter;->mShowColorChips:Z

    return-void
.end method

.method public toggleSelected(Lcom/android/email/activity/MessageListItem;)V
    .locals 1
    .param p1    # Lcom/android/email/activity/MessageListItem;

    invoke-virtual {p0, p1}, Lcom/android/email/activity/MessagesAdapter;->isSelected(Lcom/android/email/activity/MessageListItem;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/android/email/activity/MessagesAdapter;->updateSelected(Lcom/android/email/activity/MessageListItem;Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updateFavorite(Lcom/android/email/activity/MessageListItem;Z)V
    .locals 1
    .param p1    # Lcom/android/email/activity/MessageListItem;
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Lcom/android/email/activity/MessagesAdapter;->changeFavoriteIcon(Lcom/android/email/activity/MessageListItem;Z)V

    iget-object v0, p0, Lcom/android/email/activity/MessagesAdapter;->mCallback:Lcom/android/email/activity/MessagesAdapter$Callback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/MessagesAdapter;->mCallback:Lcom/android/email/activity/MessagesAdapter$Callback;

    invoke-interface {v0, p1, p2}, Lcom/android/email/activity/MessagesAdapter$Callback;->onAdapterFavoriteChanged(Lcom/android/email/activity/MessageListItem;Z)V

    :cond_0
    return-void
.end method
