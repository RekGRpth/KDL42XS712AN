.class Lcom/android/email/activity/ChipsAddressTextView;
.super Lcom/android/ex/chips/RecipientEditTextView;
.source "ChipsAddressTextView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/email/activity/ChipsAddressTextView$1;,
        Lcom/android/email/activity/ChipsAddressTextView$ForwardValidator;
    }
.end annotation


# instance fields
.field private mInputManager:Landroid/view/inputmethod/InputMethodManager;

.field private final mInternalValidator:Lcom/android/email/activity/ChipsAddressTextView$ForwardValidator;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/ex/chips/RecipientEditTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Lcom/android/email/activity/ChipsAddressTextView$ForwardValidator;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/email/activity/ChipsAddressTextView$ForwardValidator;-><init>(Lcom/android/email/activity/ChipsAddressTextView;Lcom/android/email/activity/ChipsAddressTextView$1;)V

    iput-object v0, p0, Lcom/android/email/activity/ChipsAddressTextView;->mInternalValidator:Lcom/android/email/activity/ChipsAddressTextView$ForwardValidator;

    iget-object v0, p0, Lcom/android/email/activity/ChipsAddressTextView;->mInternalValidator:Lcom/android/email/activity/ChipsAddressTextView$ForwardValidator;

    invoke-super {p0, v0}, Lcom/android/ex/chips/RecipientEditTextView;->setValidator(Landroid/widget/AutoCompleteTextView$Validator;)V

    const-string v0, "input_method"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/android/email/activity/ChipsAddressTextView;->mInputManager:Landroid/view/inputmethod/InputMethodManager;

    return-void
.end method


# virtual methods
.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v0, 0x42

    if-ne p1, v0, :cond_0

    const-string v0, "Email"

    const-string v1, "Enter key down......"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/email/activity/ChipsAddressTextView;->showIME()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/android/ex/chips/RecipientEditTextView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public setValidator(Landroid/widget/AutoCompleteTextView$Validator;)V
    .locals 1
    .param p1    # Landroid/widget/AutoCompleteTextView$Validator;

    iget-object v0, p0, Lcom/android/email/activity/ChipsAddressTextView;->mInternalValidator:Lcom/android/email/activity/ChipsAddressTextView$ForwardValidator;

    invoke-virtual {v0, p1}, Lcom/android/email/activity/ChipsAddressTextView$ForwardValidator;->setValidator(Landroid/widget/AutoCompleteTextView$Validator;)V

    return-void
.end method

.method showIME()V
    .locals 2

    iget-object v0, p0, Lcom/android/email/activity/ChipsAddressTextView;->mInputManager:Landroid/view/inputmethod/InputMethodManager;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    return-void
.end method
