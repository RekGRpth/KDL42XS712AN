.class abstract Lcom/android/email/activity/UIControllerBase;
.super Ljava/lang/Object;
.source "UIControllerBase.java"

# interfaces
.implements Lcom/android/email/activity/MailboxListFragment$Callback;
.implements Lcom/android/email/activity/MessageListFragment$Callback;
.implements Lcom/android/email/activity/MessageViewFragment$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/email/activity/UIControllerBase$1;,
        Lcom/android/email/activity/UIControllerBase$MessageOrderManagerCallback;,
        Lcom/android/email/activity/UIControllerBase$RefreshListener;
    }
.end annotation


# instance fields
.field protected final mActionBarController:Lcom/android/email/activity/ActionBarController;

.field final mActivity:Lcom/android/email/activity/EmailActivity;

.field final mFragmentManager:Landroid/app/FragmentManager;

.field protected mListContext:Lcom/android/email/MessageListContext;

.field private mMailboxListFragment:Lcom/android/email/activity/MailboxListFragment;

.field private mMessageListFragment:Lcom/android/email/activity/MessageListFragment;

.field private final mMessageOrderManagerCallback:Lcom/android/email/activity/UIControllerBase$MessageOrderManagerCallback;

.field private mMessageViewFragment:Lcom/android/email/activity/MessageViewFragment;

.field private mNfcHandler:Lcom/android/email/activity/NfcHandler;

.field private mOrderManager:Lcom/android/email/activity/MessageOrderManager;

.field protected final mRefreshListener:Lcom/android/email/activity/UIControllerBase$RefreshListener;

.field final mRefreshManager:Lcom/android/email/RefreshManager;

.field private final mRemovedFragments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field final mTaskTracker:Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;


# direct methods
.method public constructor <init>(Lcom/android/email/activity/EmailActivity;)V
    .locals 2
    .param p1    # Lcom/android/email/activity/EmailActivity;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/email/activity/UIControllerBase$MessageOrderManagerCallback;

    invoke-direct {v0, p0, v1}, Lcom/android/email/activity/UIControllerBase$MessageOrderManagerCallback;-><init>(Lcom/android/email/activity/UIControllerBase;Lcom/android/email/activity/UIControllerBase$1;)V

    iput-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mMessageOrderManagerCallback:Lcom/android/email/activity/UIControllerBase$MessageOrderManagerCallback;

    new-instance v0, Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

    invoke-direct {v0}, Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;-><init>()V

    iput-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mTaskTracker:Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mRemovedFragments:Ljava/util/List;

    new-instance v0, Lcom/android/email/activity/UIControllerBase$RefreshListener;

    invoke-direct {v0, p0, v1}, Lcom/android/email/activity/UIControllerBase$RefreshListener;-><init>(Lcom/android/email/activity/UIControllerBase;Lcom/android/email/activity/UIControllerBase$1;)V

    iput-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mRefreshListener:Lcom/android/email/activity/UIControllerBase$RefreshListener;

    iput-object p1, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-virtual {p1}, Lcom/android/email/activity/EmailActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mFragmentManager:Landroid/app/FragmentManager;

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-static {v0}, Lcom/android/email/RefreshManager;->getInstance(Landroid/content/Context;)Lcom/android/email/RefreshManager;

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mRefreshManager:Lcom/android/email/RefreshManager;

    invoke-virtual {p0, p1}, Lcom/android/email/activity/UIControllerBase;->createActionBarController(Landroid/app/Activity;)Lcom/android/email/activity/ActionBarController;

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mActionBarController:Lcom/android/email/activity/ActionBarController;

    return-void
.end method

.method private getSearchableMailbox()Lcom/android/emailcommon/provider/Mailbox;
    .locals 2

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isMessageListReady()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getMessageListFragment()Lcom/android/email/activity/MessageListFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/android/email/activity/UIControllerBase;->mListContext:Lcom/android/email/MessageListContext;

    invoke-virtual {v1}, Lcom/android/email/MessageListContext;->isSearch()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/android/email/activity/MessageListFragment;->getSearchedMailbox()Lcom/android/emailcommon/provider/Mailbox;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/android/email/activity/MessageListFragment;->getMailbox()Lcom/android/emailcommon/provider/Mailbox;

    move-result-object v1

    goto :goto_0
.end method

.method private onAccountSettings()Z
    .locals 3

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getActualAccountId()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/android/email/activity/setup/AccountSettings;->actionSettings(Landroid/app/Activity;J)V

    const/4 v0, 0x1

    return v0
.end method

.method private onCompose()Z
    .locals 3

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isAccountSelected()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getActualAccountId()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/android/email/activity/MessageCompose;->actionCompose(Landroid/content/Context;J)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static shouldDoGlobalSearch(Lcom/android/emailcommon/provider/Account;Lcom/android/emailcommon/provider/Mailbox;)Z
    .locals 1
    .param p0    # Lcom/android/emailcommon/provider/Account;
    .param p1    # Lcom/android/emailcommon/provider/Mailbox;

    iget v0, p0, Lcom/android/emailcommon/provider/Account;->mFlags:I

    and-int/lit16 v0, v0, 0x1000

    if-eqz v0, :cond_0

    iget v0, p1, Lcom/android/emailcommon/provider/Mailbox;->mType:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showAccountSpecificWarning(J)V
    .locals 4
    .param p1    # J

    const-wide/16 v2, -0x1

    cmp-long v1, p1, v2

    if-eqz v1, :cond_0

    cmp-long v1, p1, v2

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-static {v1, p1, p2}, Lcom/android/emailcommon/provider/Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-static {v1}, Lcom/android/email/Preferences;->getPreferences(Landroid/content/Context;)Lcom/android/email/Preferences;

    move-result-object v1

    iget-object v2, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-virtual {v1, v2, v0}, Lcom/android/email/Preferences;->shouldShowRequireManualSync(Landroid/content/Context;Lcom/android/emailcommon/provider/Account;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lcom/android/email/RequireManualSyncDialog;

    iget-object v2, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-direct {v1, v2, v0}, Lcom/android/email/RequireManualSyncDialog;-><init>(Landroid/content/Context;Lcom/android/emailcommon/provider/Account;)V

    invoke-virtual {v1}, Lcom/android/email/RequireManualSyncDialog;->show()V

    :cond_0
    return-void
.end method

.method private updateMessageOrderManager()V
    .locals 4

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isMessageViewInstalled()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mListContext:Lcom/android/email/MessageListContext;

    invoke-static {v0}, Lcom/google/common/base/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mOrderManager:Lcom/android/email/activity/MessageOrderManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mOrderManager:Lcom/android/email/activity/MessageOrderManager;

    invoke-virtual {v0}, Lcom/android/email/activity/MessageOrderManager;->getListContext()Lcom/android/email/MessageListContext;

    move-result-object v0

    iget-object v1, p0, Lcom/android/email/activity/UIControllerBase;->mListContext:Lcom/android/email/MessageListContext;

    invoke-virtual {v0, v1}, Lcom/android/email/MessageListContext;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->stopMessageOrderManager()V

    new-instance v0, Lcom/android/email/activity/MessageOrderManager;

    iget-object v1, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    iget-object v2, p0, Lcom/android/email/activity/UIControllerBase;->mListContext:Lcom/android/email/MessageListContext;

    iget-object v3, p0, Lcom/android/email/activity/UIControllerBase;->mMessageOrderManagerCallback:Lcom/android/email/activity/UIControllerBase$MessageOrderManagerCallback;

    invoke-direct {v0, v1, v2, v3}, Lcom/android/email/activity/MessageOrderManager;-><init>(Landroid/content/Context;Lcom/android/email/MessageListContext;Lcom/android/email/activity/MessageOrderManager$Callback;)V

    iput-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mOrderManager:Lcom/android/email/activity/MessageOrderManager;

    :cond_2
    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mOrderManager:Lcom/android/email/activity/MessageOrderManager;

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getMessageId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/android/email/activity/MessageOrderManager;->moveTo(J)V

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->updateNavigationArrows()V

    goto :goto_0
.end method


# virtual methods
.method protected addFragmentToRemovalList(Landroid/app/Fragment;)V
    .locals 1
    .param p1    # Landroid/app/Fragment;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mRemovedFragments:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method protected commitFragmentTransaction(Landroid/app/FragmentTransaction;)V
    .locals 1
    .param p1    # Landroid/app/FragmentTransaction;

    invoke-virtual {p1}, Landroid/app/FragmentTransaction;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v0}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    :cond_0
    return-void
.end method

.method protected abstract createActionBarController(Landroid/app/Activity;)Lcom/android/email/activity/ActionBarController;
.end method

.method protected final doAutoAdvance()V
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-static {v0}, Lcom/android/email/Preferences;->getPreferences(Landroid/content/Context;)Lcom/android/email/Preferences;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/email/Preferences;->getAutoAdvanceDirection()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isMessageViewInstalled()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/email/activity/UIControllerBase;->onBackPressed(Z)Z

    :cond_1
    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->moveToNewer()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->moveToOlder()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final getActualAccountId()J
    .locals 2

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isActualAccountSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getUIAccountId()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public abstract getLayoutId()I
.end method

.method protected final getMailboxListFragment()Lcom/android/email/activity/MailboxListFragment;
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mMailboxListFragment:Lcom/android/email/activity/MailboxListFragment;

    return-object v0
.end method

.method protected getMailboxListMailboxId()J
    .locals 2

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isMailboxListInstalled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getMailboxListFragment()Lcom/android/email/activity/MailboxListFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/email/activity/MailboxListFragment;->getSelectedMailboxId()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method protected abstract getMailboxSettingsMailboxId()J
.end method

.method protected getMessageId()J
    .locals 2

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isMessageViewInstalled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getMessageViewFragment()Lcom/android/email/activity/MessageViewFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/email/activity/MessageViewFragment;->getMessageId()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method protected final getMessageListFragment()Lcom/android/email/activity/MessageListFragment;
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mMessageListFragment:Lcom/android/email/activity/MessageListFragment;

    return-object v0
.end method

.method protected getMessageListMailboxId()J
    .locals 2

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isMessageListInstalled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getMessageListFragment()Lcom/android/email/activity/MessageListFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/email/activity/MessageListFragment;->getMailboxId()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method protected final getMessageOrderManager()Lcom/android/email/activity/MessageOrderManager;
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mOrderManager:Lcom/android/email/activity/MessageOrderManager;

    return-object v0
.end method

.method protected final getMessageViewFragment()Lcom/android/email/activity/MessageViewFragment;
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mMessageViewFragment:Lcom/android/email/activity/MessageViewFragment;

    return-object v0
.end method

.method protected getSearchHint()Ljava/lang/String;
    .locals 6

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isMessageListReady()Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, ""

    :goto_0
    return-object v3

    :cond_0
    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getMessageListFragment()Lcom/android/email/activity/MessageListFragment;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/email/activity/MessageListFragment;->getAccount()Lcom/android/emailcommon/provider/Account;

    move-result-object v0

    invoke-direct {p0}, Lcom/android/email/activity/UIControllerBase;->getSearchableMailbox()Lcom/android/emailcommon/provider/Mailbox;

    move-result-object v1

    if-nez v1, :cond_1

    const-string v3, ""

    goto :goto_0

    :cond_1
    invoke-static {v0, v1}, Lcom/android/email/activity/UIControllerBase;->shouldDoGlobalSearch(Lcom/android/emailcommon/provider/Account;Lcom/android/emailcommon/provider/Mailbox;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    const v4, 0x7f08018b    # com.android.email.R.string.search_hint

    invoke-virtual {v3, v4}, Lcom/android/email/activity/EmailActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-static {v3}, Lcom/android/email/FolderProperties;->getInstance(Landroid/content/Context;)Lcom/android/email/FolderProperties;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/android/email/FolderProperties;->getDisplayName(Lcom/android/emailcommon/provider/Mailbox;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    const v4, 0x7f08018c    # com.android.email.R.string.search_mailbox_hint

    invoke-virtual {v3, v4}, Lcom/android/email/activity/EmailActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public abstract getUIAccountId()J
.end method

.method protected installMailboxListFragment(Lcom/android/email/activity/MailboxListFragment;)V
    .locals 1
    .param p1    # Lcom/android/email/activity/MailboxListFragment;

    iput-object p1, p0, Lcom/android/email/activity/UIControllerBase;->mMailboxListFragment:Lcom/android/email/activity/MailboxListFragment;

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mMailboxListFragment:Lcom/android/email/activity/MailboxListFragment;

    invoke-virtual {v0, p0}, Lcom/android/email/activity/MailboxListFragment;->setCallback(Lcom/android/email/activity/MailboxListFragment$Callback;)V

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->refreshActionBar()V

    return-void
.end method

.method protected installMessageListFragment(Lcom/android/email/activity/MessageListFragment;)V
    .locals 1
    .param p1    # Lcom/android/email/activity/MessageListFragment;

    iput-object p1, p0, Lcom/android/email/activity/UIControllerBase;->mMessageListFragment:Lcom/android/email/activity/MessageListFragment;

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mMessageListFragment:Lcom/android/email/activity/MessageListFragment;

    invoke-virtual {v0, p0}, Lcom/android/email/activity/MessageListFragment;->setCallback(Lcom/android/email/activity/MessageListFragment$Callback;)V

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->refreshActionBar()V

    return-void
.end method

.method protected installMessageViewFragment(Lcom/android/email/activity/MessageViewFragment;)V
    .locals 1
    .param p1    # Lcom/android/email/activity/MessageViewFragment;

    iput-object p1, p0, Lcom/android/email/activity/UIControllerBase;->mMessageViewFragment:Lcom/android/email/activity/MessageViewFragment;

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mMessageViewFragment:Lcom/android/email/activity/MessageViewFragment;

    invoke-virtual {v0, p0}, Lcom/android/email/activity/MessageViewFragment;->setCallback(Lcom/android/email/activity/MessageViewFragment$Callback;)V

    invoke-direct {p0}, Lcom/android/email/activity/UIControllerBase;->updateMessageOrderManager()V

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->refreshActionBar()V

    return-void
.end method

.method public final isAccountSelected()Z
    .locals 4

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getUIAccountId()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isActualAccountSelected()Z
    .locals 4

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isAccountSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getUIAccountId()J

    move-result-wide v0

    const-wide/high16 v2, 0x1000000000000000L

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final isMailboxListInstalled()Z
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mMailboxListFragment:Lcom/android/email/activity/MailboxListFragment;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final isMessageListInstalled()Z
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mMessageListFragment:Lcom/android/email/activity/MessageListFragment;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isMessageListReady()Z
    .locals 1

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isMessageListInstalled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getMessageListFragment()Lcom/android/email/activity/MessageListFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/email/activity/MessageListFragment;->hasDataLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final isMessageViewInstalled()Z
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mMessageViewFragment:Lcom/android/email/activity/MessageViewFragment;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract isRefreshEnabled()Z
.end method

.method protected abstract isRefreshInProgress()Z
.end method

.method protected final moveToNewer()Z
    .locals 2

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mOrderManager:Lcom/android/email/activity/MessageOrderManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mOrderManager:Lcom/android/email/activity/MessageOrderManager;

    invoke-virtual {v0}, Lcom/android/email/activity/MessageOrderManager;->moveToNewer()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mOrderManager:Lcom/android/email/activity/MessageOrderManager;

    invoke-virtual {v0}, Lcom/android/email/activity/MessageOrderManager;->getCurrentMessageId()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/android/email/activity/UIControllerBase;->navigateToMessage(J)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final moveToOlder()Z
    .locals 2

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mOrderManager:Lcom/android/email/activity/MessageOrderManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mOrderManager:Lcom/android/email/activity/MessageOrderManager;

    invoke-virtual {v0}, Lcom/android/email/activity/MessageOrderManager;->moveToOlder()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mOrderManager:Lcom/android/email/activity/MessageOrderManager;

    invoke-virtual {v0}, Lcom/android/email/activity/MessageOrderManager;->getCurrentMessageId()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/android/email/activity/UIControllerBase;->navigateToMessage(J)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract navigateToMessage(J)V
.end method

.method public onActivityCreated()V
    .locals 3

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onActivityCreated"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mRefreshManager:Lcom/android/email/RefreshManager;

    iget-object v1, p0, Lcom/android/email/activity/UIControllerBase;->mRefreshListener:Lcom/android/email/activity/UIControllerBase$RefreshListener;

    invoke-virtual {v0, v1}, Lcom/android/email/RefreshManager;->registerListener(Lcom/android/email/RefreshManager$Listener;)V

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mActionBarController:Lcom/android/email/activity/ActionBarController;

    invoke-virtual {v0}, Lcom/android/email/activity/ActionBarController;->onActivityCreated()V

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-static {p0, v0}, Lcom/android/email/activity/NfcHandler;->register(Lcom/android/email/activity/UIControllerBase;Landroid/app/Activity;)Lcom/android/email/activity/NfcHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mNfcHandler:Lcom/android/email/activity/NfcHandler;

    return-void
.end method

.method public onActivityDestroy()V
    .locals 3

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onActivityDestroy"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mActionBarController:Lcom/android/email/activity/ActionBarController;

    invoke-virtual {v0}, Lcom/android/email/activity/ActionBarController;->onActivityDestroy()V

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mRefreshManager:Lcom/android/email/RefreshManager;

    iget-object v1, p0, Lcom/android/email/activity/UIControllerBase;->mRefreshListener:Lcom/android/email/activity/UIControllerBase$RefreshListener;

    invoke-virtual {v0, v1}, Lcom/android/email/RefreshManager;->unregisterListener(Lcom/android/email/RefreshManager$Listener;)V

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mTaskTracker:Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

    invoke-virtual {v0}, Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;->cancellAllInterrupt()V

    return-void
.end method

.method public onActivityPause()V
    .locals 3

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onActivityPause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public onActivityResume()V
    .locals 5

    sget-boolean v2, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v2, :cond_0

    sget-boolean v2, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v2, :cond_0

    const-string v2, "Email"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " onActivityResume"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->refreshActionBar()V

    iget-object v2, p0, Lcom/android/email/activity/UIControllerBase;->mNfcHandler:Lcom/android/email/activity/NfcHandler;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/email/activity/UIControllerBase;->mNfcHandler:Lcom/android/email/activity/NfcHandler;

    invoke-virtual {v2}, Lcom/android/email/activity/NfcHandler;->onAccountChanged()V

    :cond_1
    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getUIAccountId()J

    move-result-wide v0

    iget-object v2, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-static {v2}, Lcom/android/email/Preferences;->getPreferences(Landroid/content/Context;)Lcom/android/email/Preferences;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/android/email/Preferences;->setLastUsedAccountId(J)V

    invoke-direct {p0, v0, v1}, Lcom/android/email/activity/UIControllerBase;->showAccountSpecificWarning(J)V

    return-void
.end method

.method public onActivityStart()V
    .locals 3

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onActivityStart"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isMessageViewInstalled()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/email/activity/UIControllerBase;->updateMessageOrderManager()V

    :cond_1
    return-void
.end method

.method public onActivityStop()V
    .locals 3

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onActivityStop"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->stopMessageOrderManager()V

    return-void
.end method

.method public onActivityViewReady()V
    .locals 3

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onActivityViewReady"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public abstract onBackPressed(Z)Z
.end method

.method public onBeforeMessageGone()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->doAutoAdvance()V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/MenuInflater;Landroid/view/Menu;)Z
    .locals 1
    .param p1    # Landroid/view/MenuInflater;
    .param p2    # Landroid/view/Menu;

    const v0, 0x7f0e0001    # com.android.email.R.menu.email_activity_options

    invoke-virtual {p1, v0, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 v0, 0x1

    return v0
.end method

.method public final onInstallFragment(Landroid/app/Fragment;)V
    .locals 3
    .param p1    # Landroid/app/Fragment;

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onInstallFragment  fragment="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    instance-of v0, p1, Lcom/android/email/activity/MailboxListFragment;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/android/email/activity/MailboxListFragment;

    invoke-virtual {p0, p1}, Lcom/android/email/activity/UIControllerBase;->installMailboxListFragment(Lcom/android/email/activity/MailboxListFragment;)V

    :goto_0
    return-void

    :cond_1
    instance-of v0, p1, Lcom/android/email/activity/MessageListFragment;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/android/email/activity/MessageListFragment;

    invoke-virtual {p0, p1}, Lcom/android/email/activity/UIControllerBase;->installMessageListFragment(Lcom/android/email/activity/MessageListFragment;)V

    goto :goto_0

    :cond_2
    instance-of v0, p1, Lcom/android/email/activity/MessageViewFragment;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/android/email/activity/MessageViewFragment;

    invoke-virtual {p0, p1}, Lcom/android/email/activity/UIControllerBase;->installMessageViewFragment(Lcom/android/email/activity/MessageViewFragment;)V

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Tried to install unknown fragment"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onMailboxNotFound(Z)V
    .locals 4
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    const v3, 0x7f080161    # com.android.email.R.string.toast_mailbox_not_found

    invoke-static {v2, v3}, Lcom/android/emailcommon/utility/Utility;->showToast(Landroid/content/Context;I)V

    :cond_0
    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getUIAccountId()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    iget-object v3, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-static {v3, v0, v1}, Lcom/android/email/activity/Welcome;->createOpenAccountInboxIntent(Landroid/content/Context;J)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/email/activity/EmailActivity;->startActivity(Landroid/content/Intent;)V

    :goto_0
    iget-object v2, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-virtual {v2}, Lcom/android/email/activity/EmailActivity;->finish()V

    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-static {v2}, Lcom/android/email/activity/Welcome;->actionStart(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method public onMessageNotExists()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->doAutoAdvance()V

    return-void
.end method

.method public onMessageSetUnread()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->doAutoAdvance()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1    # Landroid/view/MenuItem;

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    move v2, v3

    :cond_0
    :goto_0
    return v2

    :sswitch_0
    invoke-virtual {p0, v3}, Lcom/android/email/activity/UIControllerBase;->onBackPressed(Z)Z

    move-result v2

    goto :goto_0

    :sswitch_1
    invoke-direct {p0}, Lcom/android/email/activity/UIControllerBase;->onCompose()Z

    move-result v2

    goto :goto_0

    :sswitch_2
    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->onRefresh()V

    goto :goto_0

    :sswitch_3
    invoke-direct {p0}, Lcom/android/email/activity/UIControllerBase;->onAccountSettings()Z

    move-result v2

    goto :goto_0

    :sswitch_4
    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->onSearchRequested()V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getMailboxSettingsMailboxId()J

    move-result-wide v0

    const-wide/16 v3, -0x1

    cmp-long v3, v0, v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-static {v3, v0, v1}, Lcom/android/email/activity/setup/MailboxSettings;->start(Landroid/app/Activity;J)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0    # android.R.id.home
        0x7f0f00df -> :sswitch_1    # com.android.email.R.id.compose
        0x7f0f00e0 -> :sswitch_4    # com.android.email.R.id.search
        0x7f0f00e2 -> :sswitch_2    # com.android.email.R.id.refresh
        0x7f0f00e5 -> :sswitch_5    # com.android.email.R.id.mailbox_settings
        0x7f0f00e6 -> :sswitch_3    # com.android.email.R.id.account_settings
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/MenuInflater;Landroid/view/Menu;)Z
    .locals 15
    .param p1    # Landroid/view/MenuInflater;
    .param p2    # Landroid/view/Menu;

    const v13, 0x7f0f00e2    # com.android.email.R.id.refresh

    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isRefreshEnabled()Z

    move-result v13

    if-eqz v13, :cond_4

    const/4 v13, 0x1

    invoke-interface {v6, v13}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v13, p0, Lcom/android/email/activity/UIControllerBase;->mRefreshListener:Lcom/android/email/activity/UIControllerBase$RefreshListener;

    invoke-virtual {v13, v6}, Lcom/android/email/activity/UIControllerBase$RefreshListener;->setRefreshIcon(Landroid/view/MenuItem;)V

    :cond_0
    :goto_0
    const/4 v8, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isMessageListReady()Z

    move-result v13

    if-eqz v13, :cond_1

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getActualAccountId()J

    move-result-wide v2

    const-wide/16 v13, 0x0

    cmp-long v13, v2, v13

    if-lez v13, :cond_1

    iget-object v13, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-static {v13, v2, v3}, Lcom/android/emailcommon/provider/Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/Account;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v13, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-virtual {v1, v13}, Lcom/android/emailcommon/provider/Account;->getProtocol(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    const-string v13, "eas"

    invoke-virtual {v13, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getMessageListFragment()Lcom/android/email/activity/MessageListFragment;

    move-result-object v13

    invoke-virtual {v13}, Lcom/android/email/activity/MessageListFragment;->getMailbox()Lcom/android/emailcommon/provider/Mailbox;

    move-result-object v7

    if-eqz v7, :cond_5

    invoke-virtual {v7, v9}, Lcom/android/emailcommon/provider/Mailbox;->loadsFromServer(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_5

    const/4 v8, 0x1

    :goto_1
    iget v13, v1, Lcom/android/emailcommon/provider/Account;->mFlags:I

    and-int/lit16 v13, v13, 0x800

    if-eqz v13, :cond_6

    const/4 v4, 0x1

    :cond_1
    :goto_2
    iget-object v13, p0, Lcom/android/email/activity/UIControllerBase;->mActionBarController:Lcom/android/email/activity/ActionBarController;

    invoke-virtual {v13}, Lcom/android/email/activity/ActionBarController;->isInSearchMode()Z

    move-result v13

    if-nez v13, :cond_7

    if-eqz v4, :cond_7

    if-eqz v8, :cond_7

    const/4 v12, 0x1

    :goto_3
    const v13, 0x7f0f00e0    # com.android.email.R.id.search

    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v10

    if-eqz v10, :cond_2

    invoke-interface {v10, v12}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_2
    const v13, 0x7f0f00e5    # com.android.email.R.id.mailbox_settings

    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v11

    if-eqz v11, :cond_3

    if-eqz v5, :cond_8

    if-eqz v8, :cond_8

    const/4 v13, 0x1

    :goto_4
    invoke-interface {v11, v13}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    :cond_3
    const/4 v13, 0x1

    return v13

    :cond_4
    const/4 v13, 0x0

    invoke-interface {v6, v13}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object v13, p0, Lcom/android/email/activity/UIControllerBase;->mRefreshListener:Lcom/android/email/activity/UIControllerBase$RefreshListener;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Lcom/android/email/activity/UIControllerBase$RefreshListener;->setRefreshIcon(Landroid/view/MenuItem;)V

    goto :goto_0

    :cond_5
    const/4 v8, 0x0

    goto :goto_1

    :cond_6
    const/4 v4, 0x0

    goto :goto_2

    :cond_7
    const/4 v12, 0x0

    goto :goto_3

    :cond_8
    const/4 v13, 0x0

    goto :goto_4
.end method

.method protected abstract onRefresh()V
.end method

.method public onRespondedToInvite(I)V
    .locals 0
    .param p1    # I

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->doAutoAdvance()V

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " restoreInstanceState"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mActionBarController:Lcom/android/email/activity/ActionBarController;

    invoke-virtual {v0, p1}, Lcom/android/email/activity/ActionBarController;->onRestoreInstanceState(Landroid/os/Bundle;)V

    const-string v0, "UIControllerBase.listContext"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/android/email/MessageListContext;

    iput-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mListContext:Lcom/android/email/MessageListContext;

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onSaveInstanceState"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mActionBarController:Lcom/android/email/activity/ActionBarController;

    invoke-virtual {v0, p1}, Lcom/android/email/activity/ActionBarController;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "UIControllerBase.listContext"

    iget-object v1, p0, Lcom/android/email/activity/UIControllerBase;->mListContext:Lcom/android/email/MessageListContext;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method

.method protected onSearchExit()V
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mListContext:Lcom/android/email/MessageListContext;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mListContext:Lcom/android/email/MessageListContext;

    invoke-virtual {v0}, Lcom/android/email/MessageListContext;->isSearch()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-virtual {v0}, Lcom/android/email/activity/EmailActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-virtual {v0}, Lcom/android/email/activity/EmailActivity;->invalidateOptionsMenu()V

    goto :goto_0
.end method

.method public onSearchRequested()V
    .locals 7

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getActualAccountId()J

    move-result-wide v1

    const/4 v3, 0x0

    const-wide/16 v5, 0x0

    cmp-long v5, v1, v5

    if-lez v5, :cond_0

    iget-object v5, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-static {v5, v1, v2}, Lcom/android/emailcommon/provider/Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v5, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-virtual {v0, v5}, Lcom/android/emailcommon/provider/Account;->getProtocol(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    iget v5, v0, Lcom/android/emailcommon/provider/Account;->mFlags:I

    and-int/lit16 v5, v5, 0x800

    if-eqz v5, :cond_2

    const/4 v3, 0x1

    :cond_0
    :goto_0
    if-nez v3, :cond_3

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v3, 0x0

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->isMessageListReady()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/android/email/activity/UIControllerBase;->mActionBarController:Lcom/android/email/activity/ActionBarController;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/android/email/activity/ActionBarController;->enterSearchMode(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onSearchStarted()V
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-virtual {v0}, Lcom/android/email/activity/EmailActivity;->invalidateOptionsMenu()V

    return-void
.end method

.method protected onSearchSubmit(Ljava/lang/String;)V
    .locals 8
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getUIAccountId()J

    move-result-wide v1

    invoke-static {v1, v2}, Lcom/android/emailcommon/provider/Account;->isNormalAccount(J)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/android/email/activity/UIControllerBase;->getSearchableMailbox()Lcom/android/emailcommon/provider/Mailbox;

    move-result-object v6

    if-eqz v6, :cond_0

    iget-wide v3, v6, Lcom/android/emailcommon/provider/Mailbox;->mId:J

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_2

    const-string v0, "Email"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Submitting search: ["

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "] in mailboxId="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v7, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    move-object v5, p1

    invoke-static/range {v0 .. v5}, Lcom/android/email/activity/EmailActivity;->createSearchIntent(Landroid/app/Activity;JJLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/android/email/activity/EmailActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mActionBarController:Lcom/android/email/activity/ActionBarController;

    invoke-virtual {v0}, Lcom/android/email/activity/ActionBarController;->exitSearchMode()V

    goto :goto_0
.end method

.method public final onUninstallFragment(Landroid/app/Fragment;)V
    .locals 3
    .param p1    # Landroid/app/Fragment;

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onUninstallFragment  fragment="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mRemovedFragments:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mMailboxListFragment:Lcom/android/email/activity/MailboxListFragment;

    if-ne p1, v0, :cond_1

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->uninstallMailboxListFragment()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mMessageListFragment:Lcom/android/email/activity/MessageListFragment;

    if-ne p1, v0, :cond_2

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->uninstallMessageListFragment()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mMessageViewFragment:Lcom/android/email/activity/MessageViewFragment;

    if-ne p1, v0, :cond_3

    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->uninstallMessageViewFragment()V

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Tried to uninstall unknown fragment"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final open(Lcom/android/email/MessageListContext;J)V
    .locals 2
    .param p1    # Lcom/android/email/MessageListContext;
    .param p2    # J

    invoke-virtual {p0, p1}, Lcom/android/email/activity/UIControllerBase;->setListContext(Lcom/android/email/MessageListContext;)V

    invoke-virtual {p0, p1, p2, p3}, Lcom/android/email/activity/UIControllerBase;->openInternal(Lcom/android/email/MessageListContext;J)V

    invoke-virtual {p1}, Lcom/android/email/MessageListContext;->isSearch()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mActionBarController:Lcom/android/email/activity/ActionBarController;

    invoke-virtual {p1}, Lcom/android/email/MessageListContext;->getSearchParams()Lcom/android/emailcommon/service/SearchParams;

    move-result-object v1

    iget-object v1, v1, Lcom/android/emailcommon/service/SearchParams;->mFilter:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/email/activity/ActionBarController;->enterSearchMode(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected abstract openInternal(Lcom/android/email/MessageListContext;J)V
.end method

.method protected final openMailbox(JJ)V
    .locals 3
    .param p1    # J
    .param p3    # J

    invoke-static {p1, p2, p3, p4}, Lcom/android/email/MessageListContext;->forMailbox(JJ)Lcom/android/email/MessageListContext;

    move-result-object v0

    const-wide/16 v1, -0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/email/activity/UIControllerBase;->open(Lcom/android/email/MessageListContext;J)V

    return-void
.end method

.method protected refreshActionBar()V
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mActionBarController:Lcom/android/email/activity/ActionBarController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mActionBarController:Lcom/android/email/activity/ActionBarController;

    invoke-virtual {v0}, Lcom/android/email/activity/ActionBarController;->refresh()V

    :cond_0
    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-virtual {v0}, Lcom/android/email/activity/EmailActivity;->invalidateOptionsMenu()V

    return-void
.end method

.method protected final removeFragment(Landroid/app/FragmentTransaction;Landroid/app/Fragment;)V
    .locals 4
    .param p1    # Landroid/app/FragmentTransaction;
    .param p2    # Landroid/app/Fragment;

    sget-boolean v1, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v1, :cond_0

    sget-boolean v1, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v1, :cond_0

    const-string v1, "Email"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " removeFragment fragment="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    if-nez p2, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v1, p0, Lcom/android/email/activity/UIControllerBase;->mRemovedFragments:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :try_start_0
    invoke-virtual {p1, p2}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    invoke-virtual {p0, p2}, Lcom/android/email/activity/UIControllerBase;->addFragmentToRemovalList(Landroid/app/Fragment;)V

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Email"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Swalling IllegalStateException due to known bug for  fragment: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string v1, "Email"

    invoke-static {p2}, Lcom/android/emailcommon/utility/Utility;->dumpFragment(Landroid/app/Fragment;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method protected removeMailboxListFragment(Landroid/app/FragmentTransaction;)Landroid/app/FragmentTransaction;
    .locals 1
    .param p1    # Landroid/app/FragmentTransaction;

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mMailboxListFragment:Lcom/android/email/activity/MailboxListFragment;

    invoke-virtual {p0, p1, v0}, Lcom/android/email/activity/UIControllerBase;->removeFragment(Landroid/app/FragmentTransaction;Landroid/app/Fragment;)V

    return-object p1
.end method

.method protected removeMessageListFragment(Landroid/app/FragmentTransaction;)Landroid/app/FragmentTransaction;
    .locals 1
    .param p1    # Landroid/app/FragmentTransaction;

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mMessageListFragment:Lcom/android/email/activity/MessageListFragment;

    invoke-virtual {p0, p1, v0}, Lcom/android/email/activity/UIControllerBase;->removeFragment(Landroid/app/FragmentTransaction;Landroid/app/Fragment;)V

    return-object p1
.end method

.method protected removeMessageViewFragment(Landroid/app/FragmentTransaction;)Landroid/app/FragmentTransaction;
    .locals 1
    .param p1    # Landroid/app/FragmentTransaction;

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mMessageViewFragment:Lcom/android/email/activity/MessageViewFragment;

    invoke-virtual {p0, p1, v0}, Lcom/android/email/activity/UIControllerBase;->removeFragment(Landroid/app/FragmentTransaction;Landroid/app/Fragment;)V

    return-object p1
.end method

.method protected setListContext(Lcom/android/email/MessageListContext;)V
    .locals 3
    .param p1    # Lcom/android/email/MessageListContext;

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mListContext:Lcom/android/email/MessageListContext;

    invoke-static {p1, v0}, Lcom/google/common/base/Objects;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_1

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_1

    const-string v0, "Email"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " setListContext: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iput-object p1, p0, Lcom/android/email/activity/UIControllerBase;->mListContext:Lcom/android/email/MessageListContext;

    goto :goto_0
.end method

.method protected final stopMessageOrderManager()V
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mOrderManager:Lcom/android/email/activity/MessageOrderManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mOrderManager:Lcom/android/email/activity/MessageOrderManager;

    invoke-virtual {v0}, Lcom/android/email/activity/MessageOrderManager;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mOrderManager:Lcom/android/email/activity/MessageOrderManager;

    :cond_0
    return-void
.end method

.method public final switchAccount(JZ)V
    .locals 5
    .param p1    # J
    .param p3    # Z

    iget-object v2, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-static {v2, p1, p2}, Lcom/android/emailcommon/provider/Account;->isSecurityHold(Landroid/content/Context;J)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-static {v2, p1, p2}, Lcom/android/email/activity/ActivityHelper;->showSecurityHoldDialog(Landroid/app/Activity;J)V

    iget-object v2, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-virtual {v2}, Lcom/android/email/activity/EmailActivity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/email/activity/UIControllerBase;->getUIAccountId()J

    move-result-wide v2

    cmp-long v2, p1, v2

    if-nez v2, :cond_2

    if-eqz p3, :cond_0

    :cond_2
    const-wide/high16 v2, 0x1000000000000000L

    cmp-long v2, p1, v2

    if-nez v2, :cond_4

    const-wide/16 v2, -0x2

    invoke-virtual {p0, p1, p2, v2, v3}, Lcom/android/email/activity/UIControllerBase;->openMailbox(JJ)V

    :goto_1
    iget-object v2, p0, Lcom/android/email/activity/UIControllerBase;->mNfcHandler:Lcom/android/email/activity/NfcHandler;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/android/email/activity/UIControllerBase;->mNfcHandler:Lcom/android/email/activity/NfcHandler;

    invoke-virtual {v2}, Lcom/android/email/activity/NfcHandler;->onAccountChanged()V

    :cond_3
    iget-object v2, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-static {v2}, Lcom/android/email/Preferences;->getPreferences(Landroid/content/Context;)Lcom/android/email/Preferences;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Lcom/android/email/Preferences;->setLastUsedAccountId(J)V

    invoke-direct {p0, p1, p2}, Lcom/android/email/activity/UIControllerBase;->showAccountSpecificWarning(J)V

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    const/4 v3, 0x0

    invoke-static {v2, p1, p2, v3}, Lcom/android/emailcommon/provider/Mailbox;->findMailboxOfType(Landroid/content/Context;JI)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_5

    const-string v2, "Email"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Account "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " doesn\'t have Inbox.  Redirecting"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " to Welcome..."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-static {v2, p1, p2}, Lcom/android/email/activity/Welcome;->actionOpenAccountInbox(Landroid/app/Activity;J)V

    iget-object v2, p0, Lcom/android/email/activity/UIControllerBase;->mActivity:Lcom/android/email/activity/EmailActivity;

    invoke-virtual {v2}, Lcom/android/email/activity/EmailActivity;->finish()V

    goto :goto_1

    :cond_5
    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/android/email/activity/UIControllerBase;->openMailbox(JJ)V

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected uninstallMailboxListFragment()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mMailboxListFragment:Lcom/android/email/activity/MailboxListFragment;

    invoke-virtual {v0, v1}, Lcom/android/email/activity/MailboxListFragment;->setCallback(Lcom/android/email/activity/MailboxListFragment$Callback;)V

    iput-object v1, p0, Lcom/android/email/activity/UIControllerBase;->mMailboxListFragment:Lcom/android/email/activity/MailboxListFragment;

    return-void
.end method

.method protected uninstallMessageListFragment()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mMessageListFragment:Lcom/android/email/activity/MessageListFragment;

    invoke-virtual {v0, v1}, Lcom/android/email/activity/MessageListFragment;->setCallback(Lcom/android/email/activity/MessageListFragment$Callback;)V

    iput-object v1, p0, Lcom/android/email/activity/UIControllerBase;->mMessageListFragment:Lcom/android/email/activity/MessageListFragment;

    return-void
.end method

.method protected uninstallMessageViewFragment()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/email/activity/UIControllerBase;->mMessageViewFragment:Lcom/android/email/activity/MessageViewFragment;

    invoke-virtual {v0, v1}, Lcom/android/email/activity/MessageViewFragment;->setCallback(Lcom/android/email/activity/MessageViewFragment$Callback;)V

    iput-object v1, p0, Lcom/android/email/activity/UIControllerBase;->mMessageViewFragment:Lcom/android/email/activity/MessageViewFragment;

    return-void
.end method

.method protected abstract updateNavigationArrows()V
.end method
