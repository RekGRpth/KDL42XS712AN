.class public Lcom/android/email/activity/MessagesAdapter$MessagesCursor;
.super Landroid/database/CursorWrapper;
.source "MessagesAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/email/activity/MessagesAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MessagesCursor"
.end annotation


# instance fields
.field public final mAccount:Lcom/android/emailcommon/provider/Account;

.field public final mCountTotalAccounts:I

.field public final mIsEasAccount:Z

.field public final mIsFound:Z

.field public final mIsRefreshable:Z

.field public final mMailbox:Lcom/android/emailcommon/provider/Mailbox;


# direct methods
.method private constructor <init>(Landroid/database/Cursor;ZLcom/android/emailcommon/provider/Account;Lcom/android/emailcommon/provider/Mailbox;ZZI)V
    .locals 0
    .param p1    # Landroid/database/Cursor;
    .param p2    # Z
    .param p3    # Lcom/android/emailcommon/provider/Account;
    .param p4    # Lcom/android/emailcommon/provider/Mailbox;
    .param p5    # Z
    .param p6    # Z
    .param p7    # I

    invoke-direct {p0, p1}, Landroid/database/CursorWrapper;-><init>(Landroid/database/Cursor;)V

    iput-boolean p2, p0, Lcom/android/email/activity/MessagesAdapter$MessagesCursor;->mIsFound:Z

    iput-object p3, p0, Lcom/android/email/activity/MessagesAdapter$MessagesCursor;->mAccount:Lcom/android/emailcommon/provider/Account;

    iput-object p4, p0, Lcom/android/email/activity/MessagesAdapter$MessagesCursor;->mMailbox:Lcom/android/emailcommon/provider/Mailbox;

    iput-boolean p5, p0, Lcom/android/email/activity/MessagesAdapter$MessagesCursor;->mIsEasAccount:Z

    iput-boolean p6, p0, Lcom/android/email/activity/MessagesAdapter$MessagesCursor;->mIsRefreshable:Z

    iput p7, p0, Lcom/android/email/activity/MessagesAdapter$MessagesCursor;->mCountTotalAccounts:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/database/Cursor;ZLcom/android/emailcommon/provider/Account;Lcom/android/emailcommon/provider/Mailbox;ZZILcom/android/email/activity/MessagesAdapter$1;)V
    .locals 0
    .param p1    # Landroid/database/Cursor;
    .param p2    # Z
    .param p3    # Lcom/android/emailcommon/provider/Account;
    .param p4    # Lcom/android/emailcommon/provider/Mailbox;
    .param p5    # Z
    .param p6    # Z
    .param p7    # I
    .param p8    # Lcom/android/email/activity/MessagesAdapter$1;

    invoke-direct/range {p0 .. p7}, Lcom/android/email/activity/MessagesAdapter$MessagesCursor;-><init>(Landroid/database/Cursor;ZLcom/android/emailcommon/provider/Account;Lcom/android/emailcommon/provider/Mailbox;ZZI)V

    return-void
.end method
