.class public Lcom/android/email/activity/Welcome;
.super Landroid/app/Activity;
.source "Welcome.java"


# instance fields
.field private mAccountId:J

.field private mAccountUuid:Ljava/lang/String;

.field private mInboxFinder:Lcom/android/email/activity/MailboxFinder;

.field private final mMailboxFinderCallback:Lcom/android/email/activity/MailboxFinder$Callback;

.field private mMailboxId:J

.field private mMessageId:J

.field private final mTaskTracker:Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

.field private mWaitingForSyncView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

    invoke-direct {v0}, Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;-><init>()V

    iput-object v0, p0, Lcom/android/email/activity/Welcome;->mTaskTracker:Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

    new-instance v0, Lcom/android/email/activity/Welcome$2;

    invoke-direct {v0, p0}, Lcom/android/email/activity/Welcome$2;-><init>(Lcom/android/email/activity/Welcome;)V

    iput-object v0, p0, Lcom/android/email/activity/Welcome;->mMailboxFinderCallback:Lcom/android/email/activity/MailboxFinder$Callback;

    return-void
.end method

.method static synthetic access$000(Lcom/android/email/activity/Welcome;)V
    .locals 0
    .param p0    # Lcom/android/email/activity/Welcome;

    invoke-direct {p0}, Lcom/android/email/activity/Welcome;->resolveAccount()V

    return-void
.end method

.method static synthetic access$102(Lcom/android/email/activity/Welcome;Lcom/android/email/activity/MailboxFinder;)Lcom/android/email/activity/MailboxFinder;
    .locals 0
    .param p0    # Lcom/android/email/activity/Welcome;
    .param p1    # Lcom/android/email/activity/MailboxFinder;

    iput-object p1, p0, Lcom/android/email/activity/Welcome;->mInboxFinder:Lcom/android/email/activity/MailboxFinder;

    return-object p1
.end method

.method static synthetic access$202(Lcom/android/email/activity/Welcome;J)J
    .locals 0
    .param p0    # Lcom/android/email/activity/Welcome;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/email/activity/Welcome;->mAccountId:J

    return-wide p1
.end method

.method static synthetic access$302(Lcom/android/email/activity/Welcome;J)J
    .locals 0
    .param p0    # Lcom/android/email/activity/Welcome;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/email/activity/Welcome;->mMailboxId:J

    return-wide p1
.end method

.method static synthetic access$402(Lcom/android/email/activity/Welcome;J)J
    .locals 0
    .param p0    # Lcom/android/email/activity/Welcome;
    .param p1    # J

    iput-wide p1, p0, Lcom/android/email/activity/Welcome;->mMessageId:J

    return-wide p1
.end method

.method static synthetic access$502(Lcom/android/email/activity/Welcome;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/android/email/activity/Welcome;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/android/email/activity/Welcome;->mAccountUuid:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$600(Lcom/android/email/activity/Welcome;)V
    .locals 0
    .param p0    # Lcom/android/email/activity/Welcome;

    invoke-direct {p0}, Lcom/android/email/activity/Welcome;->startEmailActivity()V

    return-void
.end method

.method public static actionOpenAccountInbox(Landroid/app/Activity;J)V
    .locals 1
    .param p0    # Landroid/app/Activity;
    .param p1    # J

    invoke-static {p0, p1, p2}, Lcom/android/email/activity/Welcome;->createOpenAccountInboxIntent(Landroid/content/Context;J)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public static actionStart(Landroid/app/Activity;)V
    .locals 1

    const-class v0, Lcom/android/email/activity/Welcome;

    invoke-static {p0, v0}, Lcom/android/emailcommon/utility/IntentUtilities;->createRestartAppIntent(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public static createAccountShortcutIntent(Landroid/content/Context;Ljava/lang/String;J)Landroid/content/Intent;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;
    .param p2    # J

    const-string v1, "/view/mailbox"

    invoke-static {v1}, Lcom/android/emailcommon/utility/IntentUtilities;->createActivityIntentUrlBuilder(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/android/emailcommon/utility/IntentUtilities;->setAccountUuid(Landroid/net/Uri$Builder;Ljava/lang/String;)V

    invoke-static {v0, p2, p3}, Lcom/android/emailcommon/utility/IntentUtilities;->setMailboxId(Landroid/net/Uri$Builder;J)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/android/emailcommon/utility/IntentUtilities;->createRestartAppIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method public static createOpenAccountInboxIntent(Landroid/content/Context;J)Landroid/content/Intent;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # J

    const-string v1, "/view/mailbox"

    invoke-static {v1}, Lcom/android/emailcommon/utility/IntentUtilities;->createActivityIntentUrlBuilder(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/android/emailcommon/utility/IntentUtilities;->setAccountId(Landroid/net/Uri$Builder;J)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/android/emailcommon/utility/IntentUtilities;->createRestartAppIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method public static createOpenMessageIntent(Landroid/content/Context;JJJ)Landroid/content/Intent;
    .locals 2
    .param p0    # Landroid/content/Context;
    .param p1    # J
    .param p3    # J
    .param p5    # J

    const-string v1, "/view/mailbox"

    invoke-static {v1}, Lcom/android/emailcommon/utility/IntentUtilities;->createActivityIntentUrlBuilder(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/android/emailcommon/utility/IntentUtilities;->setAccountId(Landroid/net/Uri$Builder;J)V

    invoke-static {v0, p3, p4}, Lcom/android/emailcommon/utility/IntentUtilities;->setMailboxId(Landroid/net/Uri$Builder;J)V

    invoke-static {v0, p5, p6}, Lcom/android/emailcommon/utility/IntentUtilities;->setMessageId(Landroid/net/Uri$Builder;J)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/android/emailcommon/utility/IntentUtilities;->createRestartAppIntent(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method private static getDebugPaneMode(Landroid/content/Intent;)I
    .locals 3
    .param p0    # Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v2, "DEBUG_PANE_MODE"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "1"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const-string v2, "2"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x2

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private resolveAccount()V
    .locals 5

    sget-object v1, Lcom/android/emailcommon/provider/Account;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {p0, v1}, Lcom/android/emailcommon/provider/EmailContent;->count(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/android/email/activity/setup/AccountSetupBasics;->actionNewAccount(Landroid/app/Activity;)V

    invoke-virtual {p0}, Lcom/android/email/activity/Welcome;->finish()V

    :goto_0
    return-void

    :cond_0
    iget-wide v1, p0, Lcom/android/email/activity/Welcome;->mAccountId:J

    iget-object v3, p0, Lcom/android/email/activity/Welcome;->mAccountUuid:Ljava/lang/String;

    invoke-static {p0, v1, v2, v3}, Lcom/android/email/activity/Welcome;->resolveAccountId(Landroid/content/Context;JLjava/lang/String;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/email/activity/Welcome;->mAccountId:J

    iget-wide v1, p0, Lcom/android/email/activity/Welcome;->mAccountId:J

    invoke-static {v1, v2}, Lcom/android/emailcommon/provider/Account;->isNormalAccount(J)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-wide v1, p0, Lcom/android/email/activity/Welcome;->mAccountId:J

    const/4 v3, 0x0

    invoke-static {p0, v1, v2, v3}, Lcom/android/emailcommon/provider/Mailbox;->findMailboxOfType(Landroid/content/Context;JI)J

    move-result-wide v1

    const-wide/16 v3, -0x1

    cmp-long v1, v1, v3

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/android/email/activity/Welcome;->startInboxLookup()V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/android/email/activity/Welcome;->startEmailActivity()V

    goto :goto_0
.end method

.method static resolveAccountId(Landroid/content/Context;JLjava/lang/String;)J
    .locals 8
    .param p0    # Landroid/content/Context;
    .param p1    # J
    .param p3    # Ljava/lang/String;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    const-wide/16 v6, -0x1

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {p0, p3}, Lcom/android/emailcommon/provider/Account;->getAccountIdFromUuid(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v0

    :goto_0
    cmp-long v4, v0, v6

    if-eqz v4, :cond_6

    :goto_1
    return-wide v0

    :cond_0
    cmp-long v4, p1, v6

    if-eqz v4, :cond_3

    const-wide/high16 v4, 0x1000000000000000L

    cmp-long v4, p1, v4

    if-eqz v4, :cond_1

    invoke-static {p0, p1, p2}, Lcom/android/emailcommon/provider/Account;->isValidId(Landroid/content/Context;J)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_1
    move-wide v0, p1

    goto :goto_0

    :cond_2
    const-wide/16 v0, -0x1

    goto :goto_0

    :cond_3
    invoke-static {p0}, Lcom/android/email/Preferences;->getPreferences(Landroid/content/Context;)Lcom/android/email/Preferences;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/email/Preferences;->getLastUsedAccountId()J

    move-result-wide v2

    cmp-long v4, v2, v6

    if-eqz v4, :cond_4

    invoke-static {p0, v2, v3}, Lcom/android/emailcommon/provider/Account;->isValidId(Landroid/content/Context;J)Z

    move-result v4

    if-nez v4, :cond_4

    const-wide/16 v2, -0x1

    invoke-static {p0}, Lcom/android/email/Preferences;->getPreferences(Landroid/content/Context;)Lcom/android/email/Preferences;

    move-result-object v4

    invoke-virtual {v4, v6, v7}, Lcom/android/email/Preferences;->setLastUsedAccountId(J)V

    :cond_4
    cmp-long v4, v2, v6

    if-nez v4, :cond_5

    invoke-static {p0}, Lcom/android/emailcommon/provider/Account;->getDefaultAccountId(Landroid/content/Context;)J

    move-result-wide v0

    :goto_2
    goto :goto_0

    :cond_5
    move-wide v0, v2

    goto :goto_2

    :cond_6
    const v4, 0x7f080160    # com.android.email.R.string.toast_account_not_found

    invoke-static {p0, v4}, Lcom/android/emailcommon/utility/Utility;->showToast(Landroid/content/Context;I)V

    invoke-static {p0}, Lcom/android/emailcommon/provider/Account;->getDefaultAccountId(Landroid/content/Context;)J

    move-result-wide v0

    goto :goto_1
.end method

.method private startEmailActivity()V
    .locals 8

    const-wide/16 v2, -0x1

    iget-wide v0, p0, Lcom/android/email/activity/Welcome;->mMessageId:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v1, p0, Lcom/android/email/activity/Welcome;->mAccountId:J

    iget-wide v3, p0, Lcom/android/email/activity/Welcome;->mMailboxId:J

    iget-wide v5, p0, Lcom/android/email/activity/Welcome;->mMessageId:J

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/android/email/activity/EmailActivity;->createOpenMessageIntent(Landroid/app/Activity;JJJ)Landroid/content/Intent;

    move-result-object v7

    :goto_0
    invoke-virtual {p0, v7}, Lcom/android/email/activity/Welcome;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/android/email/activity/Welcome;->finish()V

    return-void

    :cond_0
    iget-wide v0, p0, Lcom/android/email/activity/Welcome;->mMailboxId:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/android/email/activity/Welcome;->mAccountId:J

    iget-wide v2, p0, Lcom/android/email/activity/Welcome;->mMailboxId:J

    invoke-static {p0, v0, v1, v2, v3}, Lcom/android/email/activity/EmailActivity;->createOpenMailboxIntent(Landroid/app/Activity;JJ)Landroid/content/Intent;

    move-result-object v7

    goto :goto_0

    :cond_1
    iget-wide v0, p0, Lcom/android/email/activity/Welcome;->mAccountId:J

    invoke-static {p0, v0, v1}, Lcom/android/email/activity/EmailActivity;->createOpenAccountIntent(Landroid/app/Activity;J)Landroid/content/Intent;

    move-result-object v7

    goto :goto_0
.end method

.method private startInboxLookup()V
    .locals 7

    const/4 v6, -0x1

    const-string v0, "Email"

    const-string v1, "Inbox not found.  Starting mailbox finder..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/email/activity/Welcome;->stopInboxLookup()V

    new-instance v0, Lcom/android/email/activity/MailboxFinder;

    iget-wide v2, p0, Lcom/android/email/activity/Welcome;->mAccountId:J

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/android/email/activity/Welcome;->mMailboxFinderCallback:Lcom/android/email/activity/MailboxFinder$Callback;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/android/email/activity/MailboxFinder;-><init>(Landroid/content/Context;JILcom/android/email/activity/MailboxFinder$Callback;)V

    iput-object v0, p0, Lcom/android/email/activity/Welcome;->mInboxFinder:Lcom/android/email/activity/MailboxFinder;

    iget-object v0, p0, Lcom/android/email/activity/Welcome;->mInboxFinder:Lcom/android/email/activity/MailboxFinder;

    invoke-virtual {v0}, Lcom/android/email/activity/MailboxFinder;->startLookup()V

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040044    # com.android.email.R.layout.waiting_for_sync_message

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/android/email/activity/Welcome;->mWaitingForSyncView:Landroid/view/View;

    iget-object v0, p0, Lcom/android/email/activity/Welcome;->mWaitingForSyncView:Landroid/view/View;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v6, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/android/email/activity/Welcome;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Lcom/android/email/activity/Welcome;->invalidateOptionsMenu()V

    return-void
.end method

.method private stopInboxLookup()V
    .locals 1

    iget-object v0, p0, Lcom/android/email/activity/Welcome;->mInboxFinder:Lcom/android/email/activity/MailboxFinder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/email/activity/Welcome;->mInboxFinder:Lcom/android/email/activity/MailboxFinder;

    invoke-virtual {v0}, Lcom/android/email/activity/MailboxFinder;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/email/activity/Welcome;->mInboxFinder:Lcom/android/email/activity/MailboxFinder;

    :cond_0
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lcom/android/email/activity/ActivityHelper;->debugSetWindowFlags(Landroid/app/Activity;)V

    invoke-static {p0}, Lcom/android/email/service/EmailServiceUtils;->startExchangeService(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/android/email/activity/Welcome;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/android/emailcommon/utility/IntentUtilities;->getAccountIdFromIntent(Landroid/content/Intent;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/email/activity/Welcome;->mAccountId:J

    invoke-static {v0}, Lcom/android/emailcommon/utility/IntentUtilities;->getMailboxIdFromIntent(Landroid/content/Intent;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/email/activity/Welcome;->mMailboxId:J

    invoke-static {v0}, Lcom/android/emailcommon/utility/IntentUtilities;->getMessageIdFromIntent(Landroid/content/Intent;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/android/email/activity/Welcome;->mMessageId:J

    invoke-static {v0}, Lcom/android/emailcommon/utility/IntentUtilities;->getAccountUuidFromIntent(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/email/activity/Welcome;->mAccountUuid:Ljava/lang/String;

    invoke-static {v0}, Lcom/android/email/activity/Welcome;->getDebugPaneMode(Landroid/content/Intent;)I

    move-result v1

    invoke-static {v1}, Lcom/android/email/activity/UiUtilities;->setDebugPaneMode(I)V

    new-instance v1, Lcom/android/email/activity/Welcome$1;

    invoke-direct {v1, p0}, Lcom/android/email/activity/Welcome$1;-><init>(Lcom/android/email/activity/Welcome;)V

    invoke-static {v1}, Lcom/android/emailcommon/utility/EmailAsyncTask;->runAsyncParallel(Ljava/lang/Runnable;)Lcom/android/emailcommon/utility/EmailAsyncTask;

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/android/email/Email;->setNotifyUiAccountsChanged(Z)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    iget-object v0, p0, Lcom/android/email/activity/Welcome;->mInboxFinder:Lcom/android/email/activity/MailboxFinder;

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/email/activity/Welcome;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0e0009    # com.android.email.R.menu.welcome

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0f00e6    # com.android.email.R.id.account_settings

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/android/email/activity/Welcome;->mAccountId:J

    invoke-static {p0, v0, v1}, Lcom/android/email/activity/setup/AccountSettings;->actionSettings(Landroid/app/Activity;J)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onStop()V
    .locals 2

    invoke-direct {p0}, Lcom/android/email/activity/Welcome;->stopInboxLookup()V

    iget-object v0, p0, Lcom/android/email/activity/Welcome;->mTaskTracker:Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;

    invoke-virtual {v0}, Lcom/android/emailcommon/utility/EmailAsyncTask$Tracker;->cancellAllInterrupt()V

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    invoke-virtual {p0}, Lcom/android/email/activity/Welcome;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/android/email/Email;->DEBUG:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/android/emailcommon/Logging;->DEBUG_LIFECYCLE:Z

    if-eqz v0, :cond_0

    const-string v0, "Email"

    const-string v1, "Welcome: Closing self..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lcom/android/email/activity/Welcome;->finish()V

    :cond_1
    return-void
.end method

.method public startActivity(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/high16 v0, 0x2000000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    invoke-super {p0, p1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
