.class public Lcom/android/email/view/NonLockingScrollView;
.super Landroid/widget/ScrollView;
.source "NonLockingScrollView.java"


# static fields
.field private static final sHitFrame:Landroid/graphics/Rect;


# instance fields
.field private final mChildrenNeedingAllTouches:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mInCustomDrag:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/android/email/view/NonLockingScrollView;->sHitFrame:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/email/view/NonLockingScrollView;->mInCustomDrag:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/email/view/NonLockingScrollView;->mChildrenNeedingAllTouches:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/email/view/NonLockingScrollView;->mInCustomDrag:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/email/view/NonLockingScrollView;->mChildrenNeedingAllTouches:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/email/view/NonLockingScrollView;->mInCustomDrag:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/email/view/NonLockingScrollView;->mChildrenNeedingAllTouches:Ljava/util/ArrayList;

    return-void
.end method

.method private static canViewReceivePointerEvents(Landroid/view/View;)Z
    .locals 1
    .param p0    # Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private excludeChildrenFromInterceptions(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    instance-of v4, p1, Landroid/webkit/WebView;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/android/email/view/NonLockingScrollView;->mChildrenNeedingAllTouches:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void

    :cond_1
    instance-of v4, p1, Landroid/view/ViewGroup;

    if-eqz v4, :cond_0

    move-object v3, p1

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/email/view/NonLockingScrollView;->excludeChildrenFromInterceptions(Landroid/view/View;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private static isEventOverChild(Landroid/view/MotionEvent;Ljava/util/ArrayList;)Z
    .locals 8
    .param p0    # Landroid/view/MotionEvent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/MotionEvent;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)Z"
        }
    .end annotation

    invoke-virtual {p0}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    invoke-virtual {p0, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-static {v1}, Lcom/android/email/view/NonLockingScrollView;->canViewReceivePointerEvents(Landroid/view/View;)Z

    move-result v5

    if-eqz v5, :cond_0

    sget-object v5, Lcom/android/email/view/NonLockingScrollView;->sHitFrame:Landroid/graphics/Rect;

    invoke-virtual {v1, v5}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    sget-object v5, Lcom/android/email/view/NonLockingScrollView;->sHitFrame:Landroid/graphics/Rect;

    float-to-int v6, v3

    float-to-int v7, v4

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Rect;->contains(II)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x1

    :goto_0
    return v5

    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onFinishInflate()V
    .locals 0

    invoke-super {p0}, Landroid/widget/ScrollView;->onFinishInflate()V

    invoke-direct {p0, p0}, Lcom/android/email/view/NonLockingScrollView;->excludeChildrenFromInterceptions(Landroid/view/View;)V

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1    # Landroid/view/MotionEvent;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-ne v0, v2, :cond_0

    move v1, v2

    :goto_0
    if-eqz v1, :cond_1

    iget-boolean v4, p0, Lcom/android/email/view/NonLockingScrollView;->mInCustomDrag:Z

    if-eqz v4, :cond_1

    iput-boolean v3, p0, Lcom/android/email/view/NonLockingScrollView;->mInCustomDrag:Z

    invoke-virtual {p0, p1}, Lcom/android/email/view/NonLockingScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    :goto_1
    return v2

    :cond_0
    move v1, v3

    goto :goto_0

    :cond_1
    iget-boolean v2, p0, Lcom/android/email/view/NonLockingScrollView;->mInCustomDrag:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/android/email/view/NonLockingScrollView;->mChildrenNeedingAllTouches:Ljava/util/ArrayList;

    invoke-static {p1, v2}, Lcom/android/email/view/NonLockingScrollView;->isEventOverChild(Landroid/view/MotionEvent;Ljava/util/ArrayList;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    goto :goto_1

    :cond_2
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/android/email/view/NonLockingScrollView;->mInCustomDrag:Z

    iget-boolean v2, p0, Lcom/android/email/view/NonLockingScrollView;->mInCustomDrag:Z

    if-eqz v2, :cond_3

    invoke-virtual {p0, p1}, Lcom/android/email/view/NonLockingScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    :cond_3
    move v2, v3

    goto :goto_1
.end method
