.class Lcom/android/email/NotificationController$MessageContentObserver;
.super Landroid/database/ContentObserver;
.source "NotificationController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/email/NotificationController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MessageContentObserver"
.end annotation


# instance fields
.field private final mAccountId:J

.field private final mContext:Landroid/content/Context;

.field private final mMailboxId:J


# direct methods
.method public constructor <init>(Landroid/os/Handler;Landroid/content/Context;JJ)V
    .locals 0
    .param p1    # Landroid/os/Handler;
    .param p2    # Landroid/content/Context;
    .param p3    # J
    .param p5    # J

    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    iput-object p2, p0, Lcom/android/email/NotificationController$MessageContentObserver;->mContext:Landroid/content/Context;

    iput-wide p3, p0, Lcom/android/email/NotificationController$MessageContentObserver;->mMailboxId:J

    iput-wide p5, p0, Lcom/android/email/NotificationController$MessageContentObserver;->mAccountId:J

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 26
    .param p1    # Z

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/android/email/NotificationController$MessageContentObserver;->mAccountId:J

    # getter for: Lcom/android/email/NotificationController;->sInstance:Lcom/android/email/NotificationController;
    invoke-static {}, Lcom/android/email/NotificationController;->access$900()Lcom/android/email/NotificationController;

    move-result-object v4

    # getter for: Lcom/android/email/NotificationController;->mSuspendAccountId:J
    invoke-static {v4}, Lcom/android/email/NotificationController;->access$1000(Lcom/android/email/NotificationController;)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    # getter for: Lcom/android/email/NotificationController;->sInstance:Lcom/android/email/NotificationController;
    invoke-static {}, Lcom/android/email/NotificationController;->access$900()Lcom/android/email/NotificationController;

    move-result-object v2

    # getter for: Lcom/android/email/NotificationController;->mSuspendAccountId:J
    invoke-static {v2}, Lcom/android/email/NotificationController;->access$1000(Lcom/android/email/NotificationController;)J

    move-result-wide v2

    const-wide/high16 v4, 0x1000000000000000L

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    # getter for: Lcom/android/email/NotificationController;->sInstance:Lcom/android/email/NotificationController;
    invoke-static {}, Lcom/android/email/NotificationController;->access$900()Lcom/android/email/NotificationController;

    move-result-object v2

    # getter for: Lcom/android/email/NotificationController;->mNotificationMap:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/android/email/NotificationController;->access$600(Lcom/android/email/NotificationController;)Ljava/util/HashMap;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/android/email/NotificationController$MessageContentObserver;->mAccountId:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Landroid/database/ContentObserver;

    if-nez v20, :cond_2

    const-string v2, "Email"

    const-string v3, "Received notification when observer data was null"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/NotificationController$MessageContentObserver;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/android/email/NotificationController$MessageContentObserver;->mAccountId:J

    invoke-static {v2, v3, v4}, Lcom/android/emailcommon/provider/Account;->restoreAccountWithId(Landroid/content/Context;J)Lcom/android/emailcommon/provider/Account;

    move-result-object v12

    if-nez v12, :cond_3

    const-string v2, "Email"

    const-string v3, "Couldn\'t find account for changed message notification"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget-wide v0, v12, Lcom/android/emailcommon/provider/Account;->mNotifiedMessageId:J

    move-wide/from16 v22, v0

    iget v0, v12, Lcom/android/emailcommon/provider/Account;->mNotifiedMessageCount:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/NotificationController$MessageContentObserver;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/NotificationController$MessageContentObserver;->mContext:Landroid/content/Context;

    sget-object v3, Lcom/android/emailcommon/provider/Mailbox;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/email/NotificationController$MessageContentObserver;->mMailboxId:J

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "lastSeenMessageKey"

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lcom/android/emailcommon/utility/Utility;->getFirstRowLong(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/Long;

    move-result-object v15

    if-nez v15, :cond_4

    const-string v2, "Email"

    const-string v3, "Couldn\'t find mailbox for changed message notification"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    sget-object v3, Lcom/android/emailcommon/provider/EmailContent$Message;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/android/emailcommon/provider/EmailContent;->ID_PROJECTION:[Ljava/lang/String;

    const-string v5, "mailboxKey=? AND _id>? AND flagRead=0 AND flagLoaded IN (2,1)"

    const/4 v2, 0x2

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-wide v7, v0, Lcom/android/email/NotificationController$MessageContentObserver;->mMailboxId:J

    invoke-static {v7, v8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    const/4 v2, 0x1

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    const-string v7, "_id DESC"

    move-object/from16 v2, v24

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    if-nez v13, :cond_5

    const-string v2, "Email"

    const-string v3, "#onChange(); NULL response for message id query"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_5
    :try_start_0
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v10

    const-wide/16 v17, 0x0

    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x0

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v17

    :cond_6
    if-nez v10, :cond_8

    # getter for: Lcom/android/email/NotificationController;->sInstance:Lcom/android/email/NotificationController;
    invoke-static {}, Lcom/android/email/NotificationController;->access$900()Lcom/android/email/NotificationController;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/android/email/NotificationController$MessageContentObserver;->mAccountId:J

    # invokes: Lcom/android/email/NotificationController;->getNewMessageNotificationId(J)I
    invoke-static {v2, v3, v4}, Lcom/android/email/NotificationController;->access$700(Lcom/android/email/NotificationController;J)I

    move-result v19

    # getter for: Lcom/android/email/NotificationController;->sInstance:Lcom/android/email/NotificationController;
    invoke-static {}, Lcom/android/email/NotificationController;->access$900()Lcom/android/email/NotificationController;

    move-result-object v2

    # getter for: Lcom/android/email/NotificationController;->mNotificationManager:Landroid/app/NotificationManager;
    invoke-static {v2}, Lcom/android/email/NotificationController;->access$800(Lcom/android/email/NotificationController;)Landroid/app/NotificationManager;

    move-result-object v2

    move/from16 v0, v19

    invoke-virtual {v2, v0}, Landroid/app/NotificationManager;->cancel(I)V

    :cond_7
    :goto_1
    new-instance v14, Landroid/content/ContentValues;

    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "notifiedMessageId"

    invoke-static/range {v17 .. v18}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v14, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "notifiedMessageCount"

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v14, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v2, Lcom/android/emailcommon/provider/Account;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/android/email/NotificationController$MessageContentObserver;->mAccountId:J

    invoke-static {v2, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, v24

    invoke-virtual {v0, v2, v14, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    :cond_8
    move/from16 v0, v21

    if-ne v10, v0, :cond_9

    const-wide/16 v2, 0x0

    cmp-long v2, v17, v2

    if-eqz v2, :cond_7

    cmp-long v2, v17, v22

    if-eqz v2, :cond_7

    :cond_9
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/email/NotificationController$MessageContentObserver;->mContext:Landroid/content/Context;

    sget-object v3, Lcom/android/emailcommon/provider/Mailbox;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/email/NotificationController$MessageContentObserver;->mMailboxId:J

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "unreadCount"

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lcom/android/emailcommon/utility/Utility;->getFirstRowInt(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/Integer;

    move-result-object v25

    if-nez v25, :cond_a

    const-string v2, "Email"

    const-string v3, "Couldn\'t find unread count for mailbox"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    :cond_a
    :try_start_2
    # getter for: Lcom/android/email/NotificationController;->sInstance:Lcom/android/email/NotificationController;
    invoke-static {}, Lcom/android/email/NotificationController;->access$900()Lcom/android/email/NotificationController;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v3, v0, Lcom/android/email/NotificationController$MessageContentObserver;->mAccountId:J

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/android/email/NotificationController$MessageContentObserver;->mMailboxId:J

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Integer;->intValue()I

    move-result v11

    move-object v7, v13

    move-wide/from16 v8, v17

    invoke-virtual/range {v2 .. v11}, Lcom/android/email/NotificationController;->createNewMessageNotification(JJLandroid/database/Cursor;JII)Landroid/app/Notification;

    move-result-object v16

    if-eqz v16, :cond_7

    # getter for: Lcom/android/email/NotificationController;->sInstance:Lcom/android/email/NotificationController;
    invoke-static {}, Lcom/android/email/NotificationController;->access$900()Lcom/android/email/NotificationController;

    move-result-object v2

    # getter for: Lcom/android/email/NotificationController;->mNotificationManager:Landroid/app/NotificationManager;
    invoke-static {v2}, Lcom/android/email/NotificationController;->access$800(Lcom/android/email/NotificationController;)Landroid/app/NotificationManager;

    move-result-object v2

    # getter for: Lcom/android/email/NotificationController;->sInstance:Lcom/android/email/NotificationController;
    invoke-static {}, Lcom/android/email/NotificationController;->access$900()Lcom/android/email/NotificationController;

    move-result-object v3

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/android/email/NotificationController$MessageContentObserver;->mAccountId:J

    # invokes: Lcom/android/email/NotificationController;->getNewMessageNotificationId(J)I
    invoke-static {v3, v4, v5}, Lcom/android/email/NotificationController;->access$700(Lcom/android/email/NotificationController;J)I

    move-result v3

    move-object/from16 v0, v16

    invoke-virtual {v2, v3, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    :catchall_0
    move-exception v2

    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    throw v2
.end method
