.class public Lcom/android/mail/providers/UIProvider;
.super Ljava/lang/Object;
.source "UIProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/mail/providers/UIProvider$EditSettingsExtras;,
        Lcom/android/mail/providers/UIProvider$UpdateNotificationExtras;,
        Lcom/android/mail/providers/UIProvider$DefaultReplyBehavior;,
        Lcom/android/mail/providers/UIProvider$MessageTextSize;,
        Lcom/android/mail/providers/UIProvider$SnapHeaderValue;,
        Lcom/android/mail/providers/UIProvider$AutoAdvance;,
        Lcom/android/mail/providers/UIProvider$AttachmentColumns;,
        Lcom/android/mail/providers/UIProvider$AttachmentDestination;,
        Lcom/android/mail/providers/UIProvider$AttachmentState;,
        Lcom/android/mail/providers/UIProvider$MessageOperations;,
        Lcom/android/mail/providers/UIProvider$MessageColumns;,
        Lcom/android/mail/providers/UIProvider$MessageFlags;,
        Lcom/android/mail/providers/UIProvider$AccountCursorExtraKeys;,
        Lcom/android/mail/providers/UIProvider$CursorExtraKeys;,
        Lcom/android/mail/providers/UIProvider$CursorStatus;,
        Lcom/android/mail/providers/UIProvider$DraftType;,
        Lcom/android/mail/providers/UIProvider$ConversationOperations;,
        Lcom/android/mail/providers/UIProvider$ConversationCursorCommand;,
        Lcom/android/mail/providers/UIProvider$ConversationColumns;,
        Lcom/android/mail/providers/UIProvider$ConversationFlags;,
        Lcom/android/mail/providers/UIProvider$ConversationPersonalLevel;,
        Lcom/android/mail/providers/UIProvider$ConversationPriority;,
        Lcom/android/mail/providers/UIProvider$ConversationSendingState;,
        Lcom/android/mail/providers/UIProvider$FolderColumns;,
        Lcom/android/mail/providers/UIProvider$FolderCapabilities;,
        Lcom/android/mail/providers/UIProvider$FolderType;,
        Lcom/android/mail/providers/UIProvider$ConversationListQueryParameters;,
        Lcom/android/mail/providers/UIProvider$SearchQueryParameters;,
        Lcom/android/mail/providers/UIProvider$AccountColumns;,
        Lcom/android/mail/providers/UIProvider$AccountCapabilities;,
        Lcom/android/mail/providers/UIProvider$LastSyncResult;,
        Lcom/android/mail/providers/UIProvider$SyncStatus;
    }
.end annotation


# static fields
.field public static final ACCOUNTS_PROJECTION:[Ljava/lang/String;

.field public static final ATTACHMENT_PROJECTION:[Ljava/lang/String;

.field public static final CONVERSATION_PROJECTION:[Ljava/lang/String;

.field public static final FOLDERS_PROJECTION:[Ljava/lang/String;

.field public static final MESSAGE_PROJECTION:[Ljava/lang/String;

.field public static final UNDO_PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/16 v0, 0x1e

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "name"

    aput-object v1, v0, v4

    const-string v1, "providerVersion"

    aput-object v1, v0, v5

    const-string v1, "accountUri"

    aput-object v1, v0, v6

    const-string v1, "capabilities"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "folderListUri"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "searchUri"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "accountFromAddresses"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "saveDraftUri"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "sendMailUri"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "expungeMessageUri"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "undoUri"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/android/mail/providers/UIProvider$AccountColumns;->SETTINGS_INTENT_URI:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "syncStatus"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/android/mail/providers/UIProvider$AccountColumns;->HELP_INTENT_URI:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/android/mail/providers/UIProvider$AccountColumns;->SEND_FEEDBACK_INTENT_URI:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "composeUri"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "mimeType"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "recentFolderListUri"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "signature"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "auto_advance"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "message_text_size"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "snap_headers"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "reply_behavior"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "hide_checkboxes"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "confirm_delete"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "confirm_archive"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "confirm_send"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "default_inbox"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/android/mail/providers/UIProvider$AccountColumns$SettingsColumns;->FORCE_REPLY_FROM_DEFAULT:Ljava/lang/String;

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/mail/providers/UIProvider;->ACCOUNTS_PROJECTION:[Ljava/lang/String;

    const/16 v0, 0x12

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "folderUri"

    aput-object v1, v0, v4

    const-string v1, "name"

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/mail/providers/UIProvider$FolderColumns;->HAS_CHILDREN:Ljava/lang/String;

    aput-object v1, v0, v6

    sget-object v1, Lcom/android/mail/providers/UIProvider$FolderColumns;->CAPABILITIES:Ljava/lang/String;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/android/mail/providers/UIProvider$FolderColumns;->SYNC_WINDOW:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "conversationListUri"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "childFoldersListUri"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "unreadCount"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "totalCount"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "refreshUri"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "syncStatus"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "lastSyncResult"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "type"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "iconResId"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "bgColor"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "fgColor"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "loadMoreUri"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/mail/providers/UIProvider;->FOLDERS_PROJECTION:[Ljava/lang/String;

    const/16 v0, 0x14

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "conversationUri"

    aput-object v1, v0, v4

    const-string v1, "messageListUri"

    aput-object v1, v0, v5

    const-string v1, "subject"

    aput-object v1, v0, v6

    const-string v1, "snippet"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "senderInfo"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "dateReceivedMs"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "hasAttachments"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/android/mail/providers/UIProvider$ConversationColumns;->NUM_MESSAGES:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/android/mail/providers/UIProvider$ConversationColumns;->NUM_DRAFTS:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/android/mail/providers/UIProvider$ConversationColumns;->SENDING_STATE:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/android/mail/providers/UIProvider$ConversationColumns;->PRIORITY:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/android/mail/providers/UIProvider$ConversationColumns;->READ:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/android/mail/providers/UIProvider$ConversationColumns;->STARRED:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "folderList"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "rawFolders"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "conversationFlags"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "personalLevel"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "spam"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "muted"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/mail/providers/UIProvider;->CONVERSATION_PROJECTION:[Ljava/lang/String;

    const/16 v0, 0x20

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "serverMessageId"

    aput-object v1, v0, v4

    const-string v1, "messageUri"

    aput-object v1, v0, v5

    const-string v1, "conversationId"

    aput-object v1, v0, v6

    const-string v1, "subject"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "snippet"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "fromAddress"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "toAddresses"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "ccAddresses"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "bccAddresses"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "replyToAddress"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "dateReceivedMs"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "bodyHtml"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "bodyText"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "bodyEmbedsExternalResources"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "refMessageId"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "draftType"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "appendRefMessageContent"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "hasAttachments"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "attachmentListUri"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "messageFlags"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "joinedAttachmentInfos"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "saveMessageUri"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "sendMessageUri"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "alwaysShowImages"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/android/mail/providers/UIProvider$MessageColumns;->READ:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/android/mail/providers/UIProvider$MessageColumns;->STARRED:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "quotedTextStartPos"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "attachments"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "customFrom"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "messageAccountUri"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "eventIntentUri"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/mail/providers/UIProvider;->MESSAGE_PROJECTION:[Ljava/lang/String;

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_display_name"

    aput-object v1, v0, v3

    const-string v1, "_size"

    aput-object v1, v0, v4

    const-string v1, "uri"

    aput-object v1, v0, v5

    const-string v1, "contentType"

    aput-object v1, v0, v6

    const-string v1, "state"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "destination"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "downloadedSize"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "contentUri"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "thumbnailUri"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "previewIntent"

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/mail/providers/UIProvider;->ATTACHMENT_PROJECTION:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "messageListUri"

    aput-object v1, v0, v3

    sput-object v0, Lcom/android/mail/providers/UIProvider;->UNDO_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
