.class Lcom/konka/picasaweb/PicasaWebActivity$2;
.super Landroid/webkit/WebViewClient;
.source "PicasaWebActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/picasaweb/PicasaWebActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/picasaweb/PicasaWebActivity;


# direct methods
.method constructor <init>(Lcom/konka/picasaweb/PicasaWebActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/picasaweb/PicasaWebActivity$2;->this$0:Lcom/konka/picasaweb/PicasaWebActivity;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Ljava/lang/String;

    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/picasaweb/PicasaWebActivity$2;->this$0:Lcom/konka/picasaweb/PicasaWebActivity;

    # getter for: Lcom/konka/picasaweb/PicasaWebActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/konka/picasaweb/PicasaWebActivity;->access$0(Lcom/konka/picasaweb/PicasaWebActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/picasaweb/PicasaWebActivity$2;->this$0:Lcom/konka/picasaweb/PicasaWebActivity;

    # getter for: Lcom/konka/picasaweb/PicasaWebActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/konka/picasaweb/PicasaWebActivity;->access$0(Lcom/konka/picasaweb/PicasaWebActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/picasaweb/PicasaWebActivity$2;->this$0:Lcom/konka/picasaweb/PicasaWebActivity;

    # getter for: Lcom/konka/picasaweb/PicasaWebActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/konka/picasaweb/PicasaWebActivity;->access$0(Lcom/konka/picasaweb/PicasaWebActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    :cond_0
    return-void
.end method

.method public onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 5
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/graphics/Bitmap;

    const/4 v4, 0x1

    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/konka/picasaweb/PicasaWebActivity$2;->this$0:Lcom/konka/picasaweb/PicasaWebActivity;

    # getter for: Lcom/konka/picasaweb/PicasaWebActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/konka/picasaweb/PicasaWebActivity;->access$0(Lcom/konka/picasaweb/PicasaWebActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/picasaweb/PicasaWebActivity$2;->this$0:Lcom/konka/picasaweb/PicasaWebActivity;

    # getter for: Lcom/konka/picasaweb/PicasaWebActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/konka/picasaweb/PicasaWebActivity;->access$0(Lcom/konka/picasaweb/PicasaWebActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/konka/picasaweb/PicasaWebActivity$2;->this$0:Lcom/konka/picasaweb/PicasaWebActivity;

    iget-object v1, p0, Lcom/konka/picasaweb/PicasaWebActivity$2;->this$0:Lcom/konka/picasaweb/PicasaWebActivity;

    # getter for: Lcom/konka/picasaweb/PicasaWebActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/konka/picasaweb/PicasaWebActivity;->access$2(Lcom/konka/picasaweb/PicasaWebActivity;)Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x0

    const-string v3, "Loading... 0%"

    invoke-static {v1, v2, v3, v4, v4}, Landroid/app/ProgressDialog;->show(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/picasaweb/PicasaWebActivity;->access$3(Lcom/konka/picasaweb/PicasaWebActivity;Landroid/app/ProgressDialog;)V

    :cond_1
    return-void
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/webkit/WebView;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/picasaweb/PicasaWebActivity$2;->this$0:Lcom/konka/picasaweb/PicasaWebActivity;

    # getter for: Lcom/konka/picasaweb/PicasaWebActivity;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/konka/picasaweb/PicasaWebActivity;->access$2(Lcom/konka/picasaweb/PicasaWebActivity;)Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p3, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public onScaleChanged(Landroid/webkit/WebView;FF)V
    .locals 0
    .param p1    # Landroid/webkit/WebView;
    .param p2    # F
    .param p3    # F

    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onScaleChanged(Landroid/webkit/WebView;FF)V

    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 1
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/picasaweb/PicasaWebActivity$2;->this$0:Lcom/konka/picasaweb/PicasaWebActivity;

    # getter for: Lcom/konka/picasaweb/PicasaWebActivity;->mWebView:Landroid/webkit/WebView;
    invoke-static {v0}, Lcom/konka/picasaweb/PicasaWebActivity;->access$4(Lcom/konka/picasaweb/PicasaWebActivity;)Landroid/webkit/WebView;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0
.end method
