.class public Lcom/konka/picasaweb/PicasaWebActivity;
.super Landroid/app/Activity;
.source "PicasaWebActivity.java"


# static fields
.field private static final DOUBLE_PRESS_INTERVAL:J = 0x7d0L

.field private static final LOAD_URL:Ljava/lang/String; = "https://picasaweb.google.com/m/viewer"

.field private static final TAG:Ljava/lang/String;

.field private static final USERAGENT_LINUX_CHROME_STRING:Ljava/lang/String; = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.63 Safari/537.31"

.field private static mLastPressTime:J


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private mExitToast:Landroid/widget/Toast;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mWebView:Landroid/webkit/WebView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/konka/picasaweb/PicasaWebActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/konka/picasaweb/PicasaWebActivity;->TAG:Ljava/lang/String;

    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/konka/picasaweb/PicasaWebActivity;->mLastPressTime:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object p0, p0, Lcom/konka/picasaweb/PicasaWebActivity;->mActivity:Landroid/app/Activity;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/picasaweb/PicasaWebActivity;)Landroid/app/ProgressDialog;
    .locals 1

    iget-object v0, p0, Lcom/konka/picasaweb/PicasaWebActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/konka/picasaweb/PicasaWebActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/picasaweb/PicasaWebActivity;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/konka/picasaweb/PicasaWebActivity;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/picasaweb/PicasaWebActivity;Landroid/app/ProgressDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/picasaweb/PicasaWebActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    return-void
.end method

.method static synthetic access$4(Lcom/konka/picasaweb/PicasaWebActivity;)Landroid/webkit/WebView;
    .locals 1

    iget-object v0, p0, Lcom/konka/picasaweb/PicasaWebActivity;->mWebView:Landroid/webkit/WebView;

    return-object v0
.end method


# virtual methods
.method public onBackPressed()V
    .locals 4

    iget-object v0, p0, Lcom/konka/picasaweb/PicasaWebActivity;->mWebView:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/picasaweb/PicasaWebActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/picasaweb/PicasaWebActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sget-wide v2, Lcom/konka/picasaweb/PicasaWebActivity;->mLastPressTime:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x7d0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/konka/picasaweb/PicasaWebActivity;->mActivity:Landroid/app/Activity;

    const-string v1, "Press again to exit."

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/picasaweb/PicasaWebActivity;->mExitToast:Landroid/widget/Toast;

    iget-object v0, p0, Lcom/konka/picasaweb/PicasaWebActivity;->mExitToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/konka/picasaweb/PicasaWebActivity;->mLastPressTime:J

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/konka/picasaweb/PicasaWebActivity;->mExitToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    invoke-virtual {p0}, Lcom/konka/picasaweb/PicasaWebActivity;->finish()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    const/4 v2, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Landroid/webkit/WebView;

    invoke-direct {v0, p0}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/konka/picasaweb/PicasaWebActivity;->mWebView:Landroid/webkit/WebView;

    iget-object v0, p0, Lcom/konka/picasaweb/PicasaWebActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {p0, v0}, Lcom/konka/picasaweb/PicasaWebActivity;->setContentView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/konka/picasaweb/PicasaWebActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0xf

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/picasaweb/PicasaWebActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const-string v1, "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.63 Safari/537.31"

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    sget-object v0, Lcom/konka/picasaweb/PicasaWebActivity;->TAG:Ljava/lang/String;

    const-string v1, "xlarge screen"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/konka/picasaweb/PicasaWebActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    iget-object v0, p0, Lcom/konka/picasaweb/PicasaWebActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    iget-object v0, p0, Lcom/konka/picasaweb/PicasaWebActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    iget-object v0, p0, Lcom/konka/picasaweb/PicasaWebActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setSupportZoom(Z)V

    iget-object v0, p0, Lcom/konka/picasaweb/PicasaWebActivity;->mWebView:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    iget-object v0, p0, Lcom/konka/picasaweb/PicasaWebActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v1, Lcom/konka/picasaweb/PicasaWebActivity$1;

    invoke-direct {v1, p0}, Lcom/konka/picasaweb/PicasaWebActivity$1;-><init>(Lcom/konka/picasaweb/PicasaWebActivity;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    iget-object v0, p0, Lcom/konka/picasaweb/PicasaWebActivity;->mWebView:Landroid/webkit/WebView;

    new-instance v1, Lcom/konka/picasaweb/PicasaWebActivity$2;

    invoke-direct {v1, p0}, Lcom/konka/picasaweb/PicasaWebActivity$2;-><init>(Lcom/konka/picasaweb/PicasaWebActivity;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    iget-object v0, p0, Lcom/konka/picasaweb/PicasaWebActivity;->mWebView:Landroid/webkit/WebView;

    const-string v1, "https://picasaweb.google.com/m/viewer"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    invoke-virtual {p0}, Lcom/konka/picasaweb/PicasaWebActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f060000    # com.konka.picasaweb.R.menu.main

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/picasaweb/PicasaWebActivity;->mWebView:Landroid/webkit/WebView;

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method
