.class public Lcom/android/wallpaper/livepicker/LiveWallpaperActivity;
.super Landroid/app/Activity;
.source "LiveWallpaperActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/wallpaper/livepicker/LiveWallpaperActivity$WallpaperDialog;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    const/16 v0, 0x64

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    invoke-virtual {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity;->finish()V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/high16 v2, 0x7f030000    # com.android.wallpaper.livepicker.R.layout.live_wallpaper_base

    invoke-virtual {p0, v2}, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const/high16 v3, 0x7f090000    # com.android.wallpaper.livepicker.R.id.live_wallpaper_fragment

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity$WallpaperDialog;->newInstance()Lcom/android/wallpaper/livepicker/LiveWallpaperActivity$WallpaperDialog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/wallpaper/livepicker/LiveWallpaperActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "dialog"

    invoke-virtual {v0, v2, v3}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
