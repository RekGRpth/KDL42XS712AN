.class Lcom/konka/printscreen/PreviewActivity$1;
.super Ljava/lang/Object;
.source "PreviewActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/printscreen/PreviewActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/printscreen/PreviewActivity;


# direct methods
.method constructor <init>(Lcom/konka/printscreen/PreviewActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/printscreen/PreviewActivity$1;->this$0:Lcom/konka/printscreen/PreviewActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "android.intent.extra.TEXT"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "@@@@@"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/printscreen/PreviewActivity$1;->this$0:Lcom/konka/printscreen/PreviewActivity;

    # getter for: Lcom/konka/printscreen/PreviewActivity;->pathname:Ljava/lang/String;
    invoke-static {v3}, Lcom/konka/printscreen/PreviewActivity;->access$0(Lcom/konka/printscreen/PreviewActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const-string v1, "android.intent.extra.STREAM"

    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/konka/printscreen/PreviewActivity$1;->this$0:Lcom/konka/printscreen/PreviewActivity;

    # getter for: Lcom/konka/printscreen/PreviewActivity;->pathname:Ljava/lang/String;
    invoke-static {v3}, Lcom/konka/printscreen/PreviewActivity;->access$0(Lcom/konka/printscreen/PreviewActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "image/jpg"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/konka/printscreen/PreviewActivity$1;->this$0:Lcom/konka/printscreen/PreviewActivity;

    iget-object v2, p0, Lcom/konka/printscreen/PreviewActivity$1;->this$0:Lcom/konka/printscreen/PreviewActivity;

    const v3, 0x7f0a01a1    # com.konka.tvsettings.R.string.share_to_network

    invoke-virtual {v2, v3}, Lcom/konka/printscreen/PreviewActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/konka/printscreen/PreviewActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/konka/printscreen/PreviewActivity$1;->this$0:Lcom/konka/printscreen/PreviewActivity;

    invoke-virtual {v1}, Lcom/konka/printscreen/PreviewActivity;->finish()V

    return-void
.end method
