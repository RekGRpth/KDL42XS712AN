.class final Lcom/konka/printscreen/ScreenCapture$ScreenCaptureCallback;
.super Ljava/lang/Object;
.source "ScreenCapture.java"

# interfaces
.implements Landroid/hardware/Camera$PictureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/printscreen/ScreenCapture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "ScreenCaptureCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/printscreen/ScreenCapture;


# direct methods
.method constructor <init>(Lcom/konka/printscreen/ScreenCapture;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/printscreen/ScreenCapture$ScreenCaptureCallback;->this$0:Lcom/konka/printscreen/ScreenCapture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPictureTaken([BLandroid/hardware/Camera;)V
    .locals 13
    .param p1    # [B
    .param p2    # Landroid/hardware/Camera;

    const/4 v12, 0x0

    const/4 v11, 0x1

    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v8, "ScreenCaptureCallback,###start picture taken"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "###data.length="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v9, p1

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v4, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v7

    if-eqz v7, :cond_4

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/mstar/android/tvapi/common/PictureManager;->getPanelWidthHeight()Lcom/mstar/android/tvapi/common/vo/PanelProperty;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    :goto_0
    const/16 v6, 0x780

    const/16 v2, 0x438

    if-eqz v4, :cond_0

    iget v6, v4, Lcom/mstar/android/tvapi/common/vo/PanelProperty;->width:I

    iget v2, v4, Lcom/mstar/android/tvapi/common/vo/PanelProperty;->height:I

    :cond_0
    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "### creat bitmap...width="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " height="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v2, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    array-length v7, p1

    const v8, 0x12c000

    if-ge v7, v8, :cond_5

    const/4 v3, 0x0

    iget-object v7, p0, Lcom/konka/printscreen/ScreenCapture$ScreenCaptureCallback;->this$0:Lcom/konka/printscreen/ScreenCapture;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iput-wide v8, v7, Lcom/konka/printscreen/ScreenCapture;->etime2:J

    iget-object v7, p0, Lcom/konka/printscreen/ScreenCapture$ScreenCaptureCallback;->this$0:Lcom/konka/printscreen/ScreenCapture;

    iget-wide v7, v7, Lcom/konka/printscreen/ScreenCapture;->etime2:J

    iget-object v9, p0, Lcom/konka/printscreen/ScreenCapture$ScreenCaptureCallback;->this$0:Lcom/konka/printscreen/ScreenCapture;

    iget-wide v9, v9, Lcom/konka/printscreen/ScreenCapture;->etime1:J

    sub-long/2addr v7, v9

    const-wide/16 v9, 0x258

    cmp-long v7, v7, v9

    if-gez v7, :cond_1

    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v8, "^^^^^^^^^^^^^data too small^^^^^^^^^^^^^^^^^"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/konka/printscreen/ScreenCapture$ScreenCaptureCallback;->this$0:Lcom/konka/printscreen/ScreenCapture;

    # getter for: Lcom/konka/printscreen/ScreenCapture;->mCameraDevice:Landroid/hardware/Camera;
    invoke-static {v7}, Lcom/konka/printscreen/ScreenCapture;->access$0(Lcom/konka/printscreen/ScreenCapture;)Landroid/hardware/Camera;

    move-result-object v7

    iget-object v8, p0, Lcom/konka/printscreen/ScreenCapture$ScreenCaptureCallback;->this$0:Lcom/konka/printscreen/ScreenCapture;

    # getter for: Lcom/konka/printscreen/ScreenCapture;->mPictureCallback:Lcom/konka/printscreen/ScreenCapture$ScreenCaptureCallback;
    invoke-static {v8}, Lcom/konka/printscreen/ScreenCapture;->access$1(Lcom/konka/printscreen/ScreenCapture;)Lcom/konka/printscreen/ScreenCapture$ScreenCaptureCallback;

    move-result-object v8

    invoke-virtual {v7, v12, v12, v8}, Landroid/hardware/Camera;->takePicture(Landroid/hardware/Camera$ShutterCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;)V

    :cond_1
    :goto_1
    if-eqz v3, :cond_6

    iget-object v7, p0, Lcom/konka/printscreen/ScreenCapture$ScreenCaptureCallback;->this$0:Lcom/konka/printscreen/ScreenCapture;

    const-string v8, "screenCapture"

    # invokes: Lcom/konka/printscreen/ScreenCapture;->saveBitmap2File(Ljava/lang/String;Landroid/graphics/Bitmap;)I
    invoke-static {v7, v8, v3}, Lcom/konka/printscreen/ScreenCapture;->access$2(Lcom/konka/printscreen/ScreenCapture;Ljava/lang/String;Landroid/graphics/Bitmap;)I

    move-result v5

    if-ne v5, v11, :cond_2

    iget-object v7, p0, Lcom/konka/printscreen/ScreenCapture$ScreenCaptureCallback;->this$0:Lcom/konka/printscreen/ScreenCapture;

    invoke-static {v7, v11}, Lcom/konka/printscreen/ScreenCapture;->access$3(Lcom/konka/printscreen/ScreenCapture;Z)V

    :cond_2
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v7

    if-nez v7, :cond_3

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    :cond_3
    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v8, "ScreenCaptureCallback,###end picture taken"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/konka/printscreen/ScreenCapture$ScreenCaptureCallback;->this$0:Lcom/konka/printscreen/ScreenCapture;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iput-wide v8, v7, Lcom/konka/printscreen/ScreenCapture;->time2:J

    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "time2-time1="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v9, p0, Lcom/konka/printscreen/ScreenCapture$ScreenCaptureCallback;->this$0:Lcom/konka/printscreen/ScreenCapture;

    iget-wide v9, v9, Lcom/konka/printscreen/ScreenCapture;->time2:J

    iget-object v11, p0, Lcom/konka/printscreen/ScreenCapture$ScreenCaptureCallback;->this$0:Lcom/konka/printscreen/ScreenCapture;

    iget-wide v11, v11, Lcom/konka/printscreen/ScreenCapture;->time1:J

    sub-long/2addr v9, v11

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :goto_2
    return-void

    :cond_4
    :try_start_1
    const-string v7, "TvManger.getInstance() == null"

    invoke-static {v7}, Lcom/konka/debuginfo/logPrint;->Fatal(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0

    :cond_5
    invoke-static {p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/graphics/Bitmap;->copyPixelsFromBuffer(Ljava/nio/Buffer;)V

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    goto :goto_1

    :cond_6
    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v8, "ScreenCaptureCallback, mBitmap is null"

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_2
.end method
