.class public Lcom/konka/printscreen/Storage;
.super Ljava/lang/Object;
.source "Storage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/printscreen/Storage$UsbReceiver;
    }
.end annotation


# instance fields
.field private UsbVolumeIndex:I

.field private stm:Landroid/os/storage/StorageManager;

.field private usbReceiver:Lcom/konka/printscreen/Storage$UsbReceiver;

.field private volumes:[Landroid/os/storage/StorageVolume;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v1, 0x0

    iput v1, p0, Lcom/konka/printscreen/Storage;->UsbVolumeIndex:I

    new-instance v1, Lcom/konka/printscreen/Storage$UsbReceiver;

    invoke-direct {v1, p0}, Lcom/konka/printscreen/Storage$UsbReceiver;-><init>(Lcom/konka/printscreen/Storage;)V

    iput-object v1, p0, Lcom/konka/printscreen/Storage;->usbReceiver:Lcom/konka/printscreen/Storage$UsbReceiver;

    const-string v1, "storage"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/storage/StorageManager;

    iput-object v1, p0, Lcom/konka/printscreen/Storage;->stm:Landroid/os/storage/StorageManager;

    iget-object v1, p0, Lcom/konka/printscreen/Storage;->stm:Landroid/os/storage/StorageManager;

    invoke-virtual {v1}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/printscreen/Storage;->volumes:[Landroid/os/storage/StorageVolume;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/printscreen/Storage;->usbReceiver:Lcom/konka/printscreen/Storage$UsbReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/printscreen/Storage;->usbReceiver:Lcom/konka/printscreen/Storage$UsbReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method


# virtual methods
.method public IsUsbsInsert()Z
    .locals 5

    iget-object v3, p0, Lcom/konka/printscreen/Storage;->stm:Landroid/os/storage/StorageManager;

    invoke-virtual {v3}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/printscreen/Storage;->volumes:[Landroid/os/storage/StorageVolume;

    iget-object v3, p0, Lcom/konka/printscreen/Storage;->volumes:[Landroid/os/storage/StorageVolume;

    if-nez v3, :cond_0

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "1111111No usb exist!"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_1
    iget-object v3, p0, Lcom/konka/printscreen/Storage;->volumes:[Landroid/os/storage/StorageVolume;

    array-length v3, v3

    if-lt v1, v3, :cond_1

    if-eqz v0, :cond_4

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "2222222usb exist!"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/konka/printscreen/Storage;->stm:Landroid/os/storage/StorageManager;

    iget-object v4, p0, Lcom/konka/printscreen/Storage;->volumes:[Landroid/os/storage/StorageVolume;

    aget-object v4, v4, v1

    invoke-virtual {v4}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    const-string v3, "mounted"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    :cond_2
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lcom/konka/printscreen/Storage;->volumes:[Landroid/os/storage/StorageVolume;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v3

    const-string v4, "/mnt/usb/mmcblka1"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const/4 v0, 0x1

    goto :goto_2

    :cond_4
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "2222222No usb exist!"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public destoryStorage(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, Lcom/konka/printscreen/Storage;->usbReceiver:Lcom/konka/printscreen/Storage$UsbReceiver;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public getUsbPath()Ljava/lang/String;
    .locals 8

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/konka/printscreen/Storage;->stm:Landroid/os/storage/StorageManager;

    invoke-virtual {v5}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v5

    iput-object v5, p0, Lcom/konka/printscreen/Storage;->volumes:[Landroid/os/storage/StorageVolume;

    iget-object v5, p0, Lcom/konka/printscreen/Storage;->volumes:[Landroid/os/storage/StorageVolume;

    if-eqz v5, :cond_0

    const-string v2, "Usbs now exist:\n"

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    iget-object v5, p0, Lcom/konka/printscreen/Storage;->volumes:[Landroid/os/storage/StorageVolume;

    array-length v5, v5

    if-lt v1, v5, :cond_1

    :goto_1
    if-eqz v0, :cond_4

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v4, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/printscreen/Storage;->volumes:[Landroid/os/storage/StorageVolume;

    iget v5, p0, Lcom/konka/printscreen/Storage;->UsbVolumeIndex:I

    aget-object v4, v4, v5

    invoke-virtual {v4}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v4

    :cond_0
    :goto_2
    return-object v4

    :cond_1
    iget-object v5, p0, Lcom/konka/printscreen/Storage;->stm:Landroid/os/storage/StorageManager;

    iget-object v6, p0, Lcom/konka/printscreen/Storage;->volumes:[Landroid/os/storage/StorageVolume;

    aget-object v6, v6, v1

    invoke-virtual {v6}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "volumes[i].getPath()="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/konka/printscreen/Storage;->volumes:[Landroid/os/storage/StorageVolume;

    aget-object v7, v7, v1

    invoke-virtual {v7}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    if-eqz v3, :cond_2

    const-string v5, "mounted"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    iget-object v5, p0, Lcom/konka/printscreen/Storage;->volumes:[Landroid/os/storage/StorageVolume;

    aget-object v5, v5, v1

    invoke-virtual {v5}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v5

    const-string v6, "/mnt/usb/mmcblka1"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/konka/printscreen/Storage;->volumes:[Landroid/os/storage/StorageVolume;

    aget-object v6, v6, v1

    invoke-virtual {v6}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x1

    iput v1, p0, Lcom/konka/printscreen/Storage;->UsbVolumeIndex:I

    goto/16 :goto_1

    :cond_4
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "333333No usb exist!"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_2
.end method
