.class Lcom/konka/printscreen/doScreenCapture$1;
.super Landroid/os/Handler;
.source "doScreenCapture.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/printscreen/doScreenCapture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/printscreen/doScreenCapture;


# direct methods
.method constructor <init>(Lcom/konka/printscreen/doScreenCapture;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/printscreen/doScreenCapture$1;->this$0:Lcom/konka/printscreen/doScreenCapture;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 12
    .param p1    # Landroid/os/Message;

    const v11, 0x7f0a019c    # com.konka.tvsettings.R.string.capture_failed

    const/16 v10, 0xf5

    const/16 v9, 0x11

    const/4 v8, 0x0

    iget v6, p1, Landroid/os/Message;->what:I

    const/16 v7, 0x65

    if-ne v6, v7, :cond_0

    iget-object v6, p0, Lcom/konka/printscreen/doScreenCapture$1;->this$0:Lcom/konka/printscreen/doScreenCapture;

    # getter for: Lcom/konka/printscreen/doScreenCapture;->mScreenCapture:Lcom/konka/printscreen/ScreenCapture;
    invoke-static {v6}, Lcom/konka/printscreen/doScreenCapture;->access$0(Lcom/konka/printscreen/doScreenCapture;)Lcom/konka/printscreen/ScreenCapture;

    move-result-object v6

    if-nez v6, :cond_1

    iget-object v6, p0, Lcom/konka/printscreen/doScreenCapture$1;->this$0:Lcom/konka/printscreen/doScreenCapture;

    invoke-virtual {v6}, Lcom/konka/printscreen/doScreenCapture;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Lcom/konka/printscreen/doScreenCapture$1;->this$0:Lcom/konka/printscreen/doScreenCapture;

    invoke-virtual {v7, v11}, Lcom/konka/printscreen/doScreenCapture;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5, v9, v8, v10}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    iget-object v6, p0, Lcom/konka/printscreen/doScreenCapture$1;->this$0:Lcom/konka/printscreen/doScreenCapture;

    invoke-virtual {v6}, Lcom/konka/printscreen/doScreenCapture;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v6, p0, Lcom/konka/printscreen/doScreenCapture$1;->this$0:Lcom/konka/printscreen/doScreenCapture;

    # getter for: Lcom/konka/printscreen/doScreenCapture;->mScreenCapture:Lcom/konka/printscreen/ScreenCapture;
    invoke-static {v6}, Lcom/konka/printscreen/doScreenCapture;->access$0(Lcom/konka/printscreen/doScreenCapture;)Lcom/konka/printscreen/ScreenCapture;

    move-result-object v6

    invoke-virtual {v6}, Lcom/konka/printscreen/ScreenCapture;->getCaptrueImagePath()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v6, p0, Lcom/konka/printscreen/doScreenCapture$1;->this$0:Lcom/konka/printscreen/doScreenCapture;

    invoke-virtual {v6}, Lcom/konka/printscreen/doScreenCapture;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Lcom/konka/printscreen/doScreenCapture$1;->this$0:Lcom/konka/printscreen/doScreenCapture;

    invoke-virtual {v7, v11}, Lcom/konka/printscreen/doScreenCapture;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5, v9, v8, v10}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    iget-object v6, p0, Lcom/konka/printscreen/doScreenCapture$1;->this$0:Lcom/konka/printscreen/doScreenCapture;

    invoke-virtual {v6}, Lcom/konka/printscreen/doScreenCapture;->finish()V

    goto :goto_0

    :cond_2
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v6, "yyyyMMddHHmmss"

    invoke-direct {v2, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    new-instance v3, Landroid/content/Intent;

    iget-object v6, p0, Lcom/konka/printscreen/doScreenCapture$1;->this$0:Lcom/konka/printscreen/doScreenCapture;

    const-class v7, Lcom/konka/printscreen/PreviewActivity;

    invoke-direct {v3, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v6, "IMAGE_PATH"

    iget-object v7, p0, Lcom/konka/printscreen/doScreenCapture$1;->this$0:Lcom/konka/printscreen/doScreenCapture;

    # getter for: Lcom/konka/printscreen/doScreenCapture;->mScreenCapture:Lcom/konka/printscreen/ScreenCapture;
    invoke-static {v7}, Lcom/konka/printscreen/doScreenCapture;->access$0(Lcom/konka/printscreen/doScreenCapture;)Lcom/konka/printscreen/ScreenCapture;

    move-result-object v7

    invoke-virtual {v7}, Lcom/konka/printscreen/ScreenCapture;->getCaptrueImagePath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v6, "FILE_NAME"

    invoke-virtual {v3, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v6, p0, Lcom/konka/printscreen/doScreenCapture$1;->this$0:Lcom/konka/printscreen/doScreenCapture;

    invoke-virtual {v6, v3}, Lcom/konka/printscreen/doScreenCapture;->startActivity(Landroid/content/Intent;)V

    iget-object v6, p0, Lcom/konka/printscreen/doScreenCapture$1;->this$0:Lcom/konka/printscreen/doScreenCapture;

    invoke-virtual {v6}, Lcom/konka/printscreen/doScreenCapture;->finish()V

    goto :goto_0
.end method
