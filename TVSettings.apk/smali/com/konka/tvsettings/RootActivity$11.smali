.class Lcom/konka/tvsettings/RootActivity$11;
.super Ljava/lang/Object;
.source "RootActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/RootActivity;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/RootActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/RootActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/RootActivity$11;->this$0:Lcom/konka/tvsettings/RootActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v1, p0, Lcom/konka/tvsettings/RootActivity$11;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v1}, Lcom/konka/tvsettings/RootActivity;->access$0(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/RootActivity$11;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_myHandler:Lcom/konka/tvsettings/RootActivity$MyHandler;
    invoke-static {v1}, Lcom/konka/tvsettings/RootActivity;->access$5(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/tvsettings/RootActivity$MyHandler;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/tvsettings/RootActivity$11;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->subtitleRunnable:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/konka/tvsettings/RootActivity;->access$38(Lcom/konka/tvsettings/RootActivity;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/konka/tvsettings/RootActivity$MyHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/RootActivity$11;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_myHandler:Lcom/konka/tvsettings/RootActivity$MyHandler;
    invoke-static {v1}, Lcom/konka/tvsettings/RootActivity;->access$5(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/tvsettings/RootActivity$MyHandler;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/tvsettings/RootActivity$11;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->subtitleRunnable:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/konka/tvsettings/RootActivity;->access$38(Lcom/konka/tvsettings/RootActivity;)Ljava/lang/Runnable;

    move-result-object v2

    const-wide/16 v3, 0x5dc

    invoke-virtual {v1, v2, v3, v4}, Lcom/konka/tvsettings/RootActivity$MyHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method
