.class Lcom/konka/tvsettings/RootActivity$RootMenuEventReceiver;
.super Landroid/content/BroadcastReceiver;
.source "RootActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/RootActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RootMenuEventReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/RootActivity;


# direct methods
.method private constructor <init>(Lcom/konka/tvsettings/RootActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/RootActivity$RootMenuEventReceiver;->this$0:Lcom/konka/tvsettings/RootActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/tvsettings/RootActivity;Lcom/konka/tvsettings/RootActivity$RootMenuEventReceiver;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/RootActivity$RootMenuEventReceiver;-><init>(Lcom/konka/tvsettings/RootActivity;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v6, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "get action=="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const-string v4, "com.konka.tv.action.KEYDOWN"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "keyCode"

    invoke-virtual {p2, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    const-string v4, "keyevent"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/view/KeyEvent;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "receive keycode====="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ",event = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    if-eqz v3, :cond_0

    invoke-static {v6}, Lcom/konka/tvsettings/RootActivity;->access$32(Z)V

    iget-object v4, p0, Lcom/konka/tvsettings/RootActivity$RootMenuEventReceiver;->this$0:Lcom/konka/tvsettings/RootActivity;

    invoke-virtual {v4, v3, v1}, Lcom/konka/tvsettings/RootActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v4, "com.konka.tv.action.SYNCDB"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/konka/tvsettings/RootActivity$RootMenuEventReceiver;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v4}, Lcom/konka/tvsettings/RootActivity;->access$0(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getDataBaseManagerInstance()Lcom/konka/kkinterface/tv/DataBaseDesk;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/DataBaseDesk;->loadEssentialDataFromDB()V

    goto :goto_0

    :cond_2
    const-string v4, "com.konka.tv.action.SHOWINFO"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/konka/tvsettings/RootActivity$RootMenuEventReceiver;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_bRootActivityOnTop:Z
    invoke-static {v4}, Lcom/konka/tvsettings/RootActivity;->access$3(Lcom/konka/tvsettings/RootActivity;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/konka/tvsettings/RootActivity$RootMenuEventReceiver;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->settingManager:Lcom/konka/kkinterface/tv/SettingDesk;
    invoke-static {v4}, Lcom/konka/tvsettings/RootActivity;->access$4(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/SettingDesk;->GetInstallationFlag()Z

    move-result v4

    if-nez v4, :cond_0

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    iget-object v4, p0, Lcom/konka/tvsettings/RootActivity$RootMenuEventReceiver;->this$0:Lcom/konka/tvsettings/RootActivity;

    const-class v5, Lcom/konka/tvsettings/popup/SourceInfoActivity;

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v4, p0, Lcom/konka/tvsettings/RootActivity$RootMenuEventReceiver;->this$0:Lcom/konka/tvsettings/RootActivity;

    iget-object v5, p0, Lcom/konka/tvsettings/RootActivity$RootMenuEventReceiver;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->rootmenuHandler:Lcom/konka/tvsettings/RootActivity$RootMenuHandler;
    invoke-static {v5}, Lcom/konka/tvsettings/RootActivity;->access$33(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/tvsettings/RootActivity$RootMenuHandler;

    move-result-object v5

    const/16 v6, 0x1f4

    invoke-static {v4, v5, v2, v6}, Lcom/konka/tvsettings/SwitchMenuHelper;->delay2ShowMenu(Landroid/app/Activity;Landroid/os/Handler;Landroid/content/Intent;I)V

    goto :goto_0

    :cond_3
    const-string v4, "com.konka.tv.action.DTV_SEARCH_START"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/konka/tvsettings/RootActivity$RootMenuEventReceiver;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_RadioBgView:Landroid/widget/ImageView;
    invoke-static {v4}, Lcom/konka/tvsettings/RootActivity;->access$13(Lcom/konka/tvsettings/RootActivity;)Landroid/widget/ImageView;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_4
    const-string v4, "com.konka.tv.action.DTV_SEARCH_END"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/konka/tvsettings/RootActivity$RootMenuEventReceiver;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_RadioBgView:Landroid/widget/ImageView;
    invoke-static {v4}, Lcom/konka/tvsettings/RootActivity;->access$13(Lcom/konka/tvsettings/RootActivity;)Landroid/widget/ImageView;

    move-result-object v4

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_5
    const-string v4, "com.konka.stop.alwaytimeshiftthread"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    const-string v4, "Try to stop always time shift thread, receiving message - com.konka.stop.alwaytimeshiftthread!"

    invoke-static {v4}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/tvsettings/RootActivity$RootMenuEventReceiver;->this$0:Lcom/konka/tvsettings/RootActivity;

    invoke-virtual {v4}, Lcom/konka/tvsettings/RootActivity;->stopAlTimeShiftThread()V

    goto/16 :goto_0

    :cond_6
    const-string v4, "com.konka.start.alwaytimeshiftthread"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "Try to start always time shift thread, receiving message - com.konka.start.alwaytimeshiftthread!"

    invoke-static {v4}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/tvsettings/RootActivity$RootMenuEventReceiver;->this$0:Lcom/konka/tvsettings/RootActivity;

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/konka/tvsettings/RootActivity;->access$34(Lcom/konka/tvsettings/RootActivity;Z)V

    goto/16 :goto_0
.end method
