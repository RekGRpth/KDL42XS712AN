.class Lcom/konka/tvsettings/teletext/TeletextActivity$2;
.super Landroid/os/Handler;
.source "TeletextActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/teletext/TeletextActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/teletext/TeletextActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/teletext/TeletextActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/teletext/TeletextActivity$2;->this$0:Lcom/konka/tvsettings/teletext/TeletextActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1    # Landroid/os/Message;

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget v2, p1, Landroid/os/Message;->what:I

    sget-object v3, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_CHANGE_TTX_STATUS:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->ordinal()I

    move-result v3

    if-ne v2, v3, :cond_0

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity$2;->this$0:Lcom/konka/tvsettings/teletext/TeletextActivity;

    const-class v3, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity$2;->this$0:Lcom/konka/tvsettings/teletext/TeletextActivity;

    invoke-virtual {v2, v1}, Lcom/konka/tvsettings/teletext/TeletextActivity;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method
