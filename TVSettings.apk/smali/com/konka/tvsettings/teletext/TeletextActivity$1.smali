.class Lcom/konka/tvsettings/teletext/TeletextActivity$1;
.super Ljava/lang/Object;
.source "TeletextActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/teletext/TeletextActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/teletext/TeletextActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/teletext/TeletextActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/teletext/TeletextActivity$1;->this$0:Lcom/konka/tvsettings/teletext/TeletextActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v1, p0, Lcom/konka/tvsettings/teletext/TeletextActivity$1;->this$0:Lcom/konka/tvsettings/teletext/TeletextActivity;

    iget-object v1, v1, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->closeTeletext()Z

    iget-object v1, p0, Lcom/konka/tvsettings/teletext/TeletextActivity$1;->this$0:Lcom/konka/tvsettings/teletext/TeletextActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/teletext/TeletextActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/konka/tvsettings/TVRootApp;

    sget-object v2, Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;->TEXT_STATUS_NONE:Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

    invoke-virtual {v1, v2}, Lcom/konka/tvsettings/TVRootApp;->setTvTextMode(Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/konka/tvsettings/teletext/TeletextActivity$1;->this$0:Lcom/konka/tvsettings/teletext/TeletextActivity;

    const-class v2, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/konka/tvsettings/teletext/TeletextActivity$1;->this$0:Lcom/konka/tvsettings/teletext/TeletextActivity;

    invoke-virtual {v1, v0}, Lcom/konka/tvsettings/teletext/TeletextActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
