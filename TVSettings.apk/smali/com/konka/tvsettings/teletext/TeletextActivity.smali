.class public Lcom/konka/tvsettings/teletext/TeletextActivity;
.super Lcom/konka/tvsettings/teletext/MstarBaseActivity;
.source "TeletextActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TeletextActivity"


# instance fields
.field private killself:Ljava/lang/Runnable;

.field private mAudioManager:Landroid/media/AudioManager;

.field mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

.field protected myHandler:Landroid/os/Handler;

.field serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    new-instance v0, Lcom/konka/tvsettings/teletext/TeletextActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/teletext/TeletextActivity$1;-><init>(Lcom/konka/tvsettings/teletext/TeletextActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->killself:Ljava/lang/Runnable;

    new-instance v0, Lcom/konka/tvsettings/teletext/TeletextActivity$2;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/teletext/TeletextActivity$2;-><init>(Lcom/konka/tvsettings/teletext/TeletextActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->myHandler:Landroid/os/Handler;

    return-void
.end method

.method private closeTTX()V
    .locals 3

    invoke-virtual {p0}, Lcom/konka/tvsettings/teletext/TeletextActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/TVRootApp;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->closeTeletext()Z

    return-void
.end method


# virtual methods
.method public isTvKeyPadAction(Landroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # Landroid/view/KeyEvent;

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "Konka Smart TV Keypad"

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/InputDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1

    :pswitch_0
    iget-object v0, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mAudioManager:Landroid/media/AudioManager;

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v3, 0x16

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v2, v0, v1}, Landroid/media/AudioManager;->adjustMasterVolume(II)V

    goto :goto_0

    :cond_2
    const/4 v0, -0x1

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->PAGE_UP:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    invoke-interface {v0, v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->PAGE_DOWN:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    invoke-interface {v0, v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x0

    const-string v1, "TeletextActivity"

    const-string v2, "OnCreate"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/teletext/TeletextActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/TVRootApp;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v1, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-virtual {p0}, Lcom/konka/tvsettings/teletext/TeletextActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {p0}, Lcom/konka/tvsettings/teletext/TeletextActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {p0}, Lcom/konka/tvsettings/teletext/TeletextActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "TTX_MODE_CLOCK"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;->TTX_MODE_CLOCK:Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->openTeletext(Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "TeletextActivity"

    const-string v2, "open teletext false"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    const-string v1, "audio"

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/teletext/TeletextActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/media/AudioManager;

    iput-object v1, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mAudioManager:Landroid/media/AudioManager;

    return-void

    :cond_1
    iget-object v1, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->myHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->killself:Ljava/lang/Runnable;

    const-wide/16 v3, 0x1388

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/konka/tvsettings/teletext/TeletextActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/konka/tvsettings/teletext/TeletextActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/konka/tvsettings/teletext/TeletextActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "TTX_MODE_SUBTITLE"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;->TTX_MODE_SUBTITLE_NAVIGATION:Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->openTeletext(Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "TeletextActivity"

    const-string v2, "open teletext subtitle false"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const-string v1, "TeletextActivity"

    const-string v2, "open teletext subtitle successfully!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;->TTX_MODE_NORMAL:Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->openTeletext(Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "TeletextActivity"

    const-string v2, "open teletext false"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;->TTX_MODE_NORMAL:Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->openTeletext(Lcom/mstar/android/tvapi/common/vo/EnumTeletextMode;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "TeletextActivity"

    const-string v2, "open teletext false"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->myHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->killself:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 6
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v3, 0x1

    const-string v2, "Teletext Actvity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Fetcch current key:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, p2}, Lcom/konka/tvsettings/teletext/TeletextActivity;->isTvKeyPadAction(Landroid/view/KeyEvent;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v2, v3

    :goto_0
    return v2

    :cond_0
    sparse-switch p1, :sswitch_data_0

    const-string v2, "Teletext Actvity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Force consume key:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    goto :goto_0

    :sswitch_0
    invoke-super {p0, p1, p2}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    goto :goto_0

    :sswitch_1
    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->DIGIT_0:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    invoke-interface {v2, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z

    move v2, v3

    goto :goto_0

    :sswitch_2
    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->DIGIT_1:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    invoke-interface {v2, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z

    move v2, v3

    goto :goto_0

    :sswitch_3
    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->DIGIT_2:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    invoke-interface {v2, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z

    move v2, v3

    goto :goto_0

    :sswitch_4
    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->DIGIT_3:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    invoke-interface {v2, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z

    move v2, v3

    goto :goto_0

    :sswitch_5
    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->DIGIT_4:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    invoke-interface {v2, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z

    move v2, v3

    goto :goto_0

    :sswitch_6
    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->DIGIT_5:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    invoke-interface {v2, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z

    move v2, v3

    goto :goto_0

    :sswitch_7
    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->DIGIT_6:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    invoke-interface {v2, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z

    move v2, v3

    goto :goto_0

    :sswitch_8
    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->DIGIT_7:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    invoke-interface {v2, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z

    move v2, v3

    goto :goto_0

    :sswitch_9
    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->DIGIT_8:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    invoke-interface {v2, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z

    move v2, v3

    goto :goto_0

    :sswitch_a
    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->DIGIT_9:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    invoke-interface {v2, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z

    move v2, v3

    goto :goto_0

    :sswitch_b
    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->PAGE_UP:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    invoke-interface {v2, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z

    move v2, v3

    goto/16 :goto_0

    :sswitch_c
    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->PAGE_DOWN:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    invoke-interface {v2, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z

    move v2, v3

    goto/16 :goto_0

    :sswitch_d
    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->PAGE_LEFT:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    invoke-interface {v2, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z

    move v2, v3

    goto/16 :goto_0

    :sswitch_e
    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->PAGE_RIGHT:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    invoke-interface {v2, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z

    move v2, v3

    goto/16 :goto_0

    :sswitch_f
    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->NORMAL_MODE_NEXT_PHASE:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    invoke-interface {v2, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z

    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->isTeletextDisplayed()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/konka/tvsettings/teletext/TeletextActivity;->getApplication()Landroid/app/Application;

    move-result-object v2

    check-cast v2, Lcom/konka/tvsettings/TVRootApp;

    sget-object v4, Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;->TEXT_STATUS_NONE:Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

    invoke-virtual {v2, v4}, Lcom/konka/tvsettings/TVRootApp;->setTvTextMode(Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;)V

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/teletext/TeletextActivity;->startActivity(Landroid/content/Intent;)V

    :cond_1
    move v2, v3

    goto/16 :goto_0

    :sswitch_10
    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->closeTeletext()Z

    invoke-virtual {p0}, Lcom/konka/tvsettings/teletext/TeletextActivity;->getApplication()Landroid/app/Application;

    move-result-object v2

    check-cast v2, Lcom/konka/tvsettings/TVRootApp;

    sget-object v4, Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;->TEXT_STATUS_NONE:Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

    invoke-virtual {v2, v4}, Lcom/konka/tvsettings/TVRootApp;->setTvTextMode(Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;)V

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/teletext/TeletextActivity;->startActivity(Landroid/content/Intent;)V

    move v2, v3

    goto/16 :goto_0

    :sswitch_11
    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->CYAN:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    invoke-interface {v2, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z

    move v2, v3

    goto/16 :goto_0

    :sswitch_12
    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->YELLOW:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    invoke-interface {v2, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z

    move v2, v3

    goto/16 :goto_0

    :sswitch_13
    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->GREEN:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    invoke-interface {v2, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z

    move v2, v3

    goto/16 :goto_0

    :sswitch_14
    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->RED:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    invoke-interface {v2, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z

    move v2, v3

    goto/16 :goto_0

    :sswitch_15
    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->MIX:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    invoke-interface {v2, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z

    move v2, v3

    goto/16 :goto_0

    :sswitch_16
    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->UPDATE:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    invoke-interface {v2, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z

    move v2, v3

    goto/16 :goto_0

    :sswitch_17
    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->SIZE:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    invoke-interface {v2, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z

    move v2, v3

    goto/16 :goto_0

    :sswitch_18
    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->INDEX:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    invoke-interface {v2, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z

    move v2, v3

    goto/16 :goto_0

    :sswitch_19
    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->HOLD:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    invoke-interface {v2, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z

    move v2, v3

    goto/16 :goto_0

    :sswitch_1a
    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->REVEAL:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    invoke-interface {v2, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z

    move v2, v3

    goto/16 :goto_0

    :sswitch_1b
    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->LIST:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    invoke-interface {v2, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z

    move v2, v3

    goto/16 :goto_0

    :sswitch_1c
    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->CLOCK:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    invoke-interface {v2, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z

    move v2, v3

    goto/16 :goto_0

    :sswitch_1d
    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->SUBPAGE:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    invoke-interface {v2, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z

    move v2, v3

    goto/16 :goto_0

    :sswitch_1e
    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->isTeletextDisplayed()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;->SUBTITLE_NAVIGATION:Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;

    invoke-interface {v2, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->sendTeletextCommand(Lcom/mstar/android/tvapi/common/vo/EnumTeletextCommand;)Z

    :cond_2
    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->isTeletextDisplayed()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/konka/tvsettings/teletext/TeletextActivity;->mChannelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->closeTeletext()Z

    const-string v2, "Teletext Actvity"

    const-string v4, "SUBTITLE_NAVIGATION close txt"

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/konka/tvsettings/teletext/TeletextActivity;->getApplication()Landroid/app/Application;

    move-result-object v2

    check-cast v2, Lcom/konka/tvsettings/TVRootApp;

    sget-object v4, Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;->TEXT_STATUS_NONE:Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

    invoke-virtual {v2, v4}, Lcom/konka/tvsettings/TVRootApp;->setTvTextMode(Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;)V

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/teletext/TeletextActivity;->startActivity(Landroid/content/Intent;)V

    :cond_3
    move v2, v3

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_10
        0x7 -> :sswitch_1
        0x8 -> :sswitch_2
        0x9 -> :sswitch_3
        0xa -> :sswitch_4
        0xb -> :sswitch_5
        0xc -> :sswitch_6
        0xd -> :sswitch_7
        0xe -> :sswitch_8
        0xf -> :sswitch_9
        0x10 -> :sswitch_a
        0x15 -> :sswitch_d
        0x16 -> :sswitch_e
        0x18 -> :sswitch_0
        0x19 -> :sswitch_0
        0x5b -> :sswitch_0
        0x5c -> :sswitch_b
        0x5d -> :sswitch_c
        0xa4 -> :sswitch_0
        0xa6 -> :sswitch_b
        0xa7 -> :sswitch_c
        0xb7 -> :sswitch_14
        0xb8 -> :sswitch_13
        0xb9 -> :sswitch_12
        0xba -> :sswitch_11
        0x101 -> :sswitch_1b
        0x102 -> :sswitch_1e
        0x106 -> :sswitch_f
        0x12e -> :sswitch_18
        0x12f -> :sswitch_19
        0x130 -> :sswitch_16
        0x131 -> :sswitch_1a
        0x132 -> :sswitch_1d
        0x133 -> :sswitch_17
        0x134 -> :sswitch_1c
        0x203 -> :sswitch_15
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 0

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onPause()V

    invoke-direct {p0}, Lcom/konka/tvsettings/teletext/TeletextActivity;->closeTTX()V

    return-void
.end method
