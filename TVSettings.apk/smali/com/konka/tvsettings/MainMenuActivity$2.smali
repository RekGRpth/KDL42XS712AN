.class Lcom/konka/tvsettings/MainMenuActivity$2;
.super Ljava/lang/Object;
.source "MainMenuActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/MainMenuActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/MainMenuActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/MainMenuActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/MainMenuActivity$2;->this$0:Lcom/konka/tvsettings/MainMenuActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "move to==="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/tvsettings/MainMenuActivity$2;->this$0:Lcom/konka/tvsettings/MainMenuActivity;

    # getter for: Lcom/konka/tvsettings/MainMenuActivity;->gridView:Landroid/widget/GridView;
    invoke-static {v3}, Lcom/konka/tvsettings/MainMenuActivity;->access$2(Lcom/konka/tvsettings/MainMenuActivity;)Landroid/widget/GridView;

    move-result-object v3

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/konka/tvsettings/MainMenuActivity$2;->this$0:Lcom/konka/tvsettings/MainMenuActivity;

    # invokes: Lcom/konka/tvsettings/MainMenuActivity;->IsReadytoMove()Z
    invoke-static {v2}, Lcom/konka/tvsettings/MainMenuActivity;->access$3(Lcom/konka/tvsettings/MainMenuActivity;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v1, p3

    const/4 v0, -0x1

    iget-object v2, p0, Lcom/konka/tvsettings/MainMenuActivity$2;->this$0:Lcom/konka/tvsettings/MainMenuActivity;

    # getter for: Lcom/konka/tvsettings/MainMenuActivity;->mainPanel:Lcom/konka/tvsettings/MovingPanel;
    invoke-static {v2}, Lcom/konka/tvsettings/MainMenuActivity;->access$4(Lcom/konka/tvsettings/MainMenuActivity;)Lcom/konka/tvsettings/MovingPanel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/tvsettings/MovingPanel;->getFocusID()I

    move-result v0

    iget-object v2, p0, Lcom/konka/tvsettings/MainMenuActivity$2;->this$0:Lcom/konka/tvsettings/MainMenuActivity;

    # getter for: Lcom/konka/tvsettings/MainMenuActivity;->mainPanel:Lcom/konka/tvsettings/MovingPanel;
    invoke-static {v2}, Lcom/konka/tvsettings/MainMenuActivity;->access$4(Lcom/konka/tvsettings/MainMenuActivity;)Lcom/konka/tvsettings/MovingPanel;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/konka/tvsettings/MovingPanel;->focusToPostion(I)V

    iget-object v2, p0, Lcom/konka/tvsettings/MainMenuActivity$2;->this$0:Lcom/konka/tvsettings/MainMenuActivity;

    # getter for: Lcom/konka/tvsettings/MainMenuActivity;->b_focusOnWeibo:Z
    invoke-static {v2}, Lcom/konka/tvsettings/MainMenuActivity;->access$5(Lcom/konka/tvsettings/MainMenuActivity;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/MainMenuActivity$2;->this$0:Lcom/konka/tvsettings/MainMenuActivity;

    # invokes: Lcom/konka/tvsettings/MainMenuActivity;->focustoSettingMenu(I)V
    invoke-static {v2, v1}, Lcom/konka/tvsettings/MainMenuActivity;->access$6(Lcom/konka/tvsettings/MainMenuActivity;I)V

    :cond_0
    if-ne p3, v0, :cond_1

    iget-object v2, p0, Lcom/konka/tvsettings/MainMenuActivity$2;->this$0:Lcom/konka/tvsettings/MainMenuActivity;

    # invokes: Lcom/konka/tvsettings/MainMenuActivity;->startMenu(I)V
    invoke-static {v2, p3}, Lcom/konka/tvsettings/MainMenuActivity;->access$1(Lcom/konka/tvsettings/MainMenuActivity;I)V

    :cond_1
    return-void
.end method
