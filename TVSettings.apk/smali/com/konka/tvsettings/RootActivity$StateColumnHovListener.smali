.class Lcom/konka/tvsettings/RootActivity$StateColumnHovListener;
.super Ljava/lang/Object;
.source "RootActivity.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/RootActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "StateColumnHovListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/RootActivity;


# direct methods
.method private constructor <init>(Lcom/konka/tvsettings/RootActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/RootActivity$StateColumnHovListener;->this$0:Lcom/konka/tvsettings/RootActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/tvsettings/RootActivity;Lcom/konka/tvsettings/RootActivity$StateColumnHovListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/RootActivity$StateColumnHovListener;-><init>(Lcom/konka/tvsettings/RootActivity;)V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    const-string v1, "in the statecolumnHovListener"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "X======="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Y======"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/tvsettings/RootActivity$StateColumnHovListener;->this$0:Lcom/konka/tvsettings/RootActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/RootActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090020    # com.konka.tvsettings.R.dimen.TVSettingStateColumn_DetectHeight

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v0, v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    if-ge v1, v0, :cond_0

    const-string v1, "allow to show StateColumn"

    const-string v2, "Column show"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/tvsettings/RootActivity$StateColumnHovListener;->this$0:Lcom/konka/tvsettings/RootActivity;

    invoke-static {v1}, Lcom/konka/tvsettings/SwitchMenuHelper;->goToStateColumnPage(Landroid/app/Activity;)V

    :cond_0
    const/4 v1, 0x0

    return v1
.end method
