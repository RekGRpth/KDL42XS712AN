.class Lcom/konka/tvsettings/RootActivity$1;
.super Ljava/lang/Object;
.source "RootActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/RootActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/RootActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/RootActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/RootActivity$1;->this$0:Lcom/konka/tvsettings/RootActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    iget-object v4, p0, Lcom/konka/tvsettings/RootActivity$1;->this$0:Lcom/konka/tvsettings/RootActivity;

    invoke-virtual {v4}, Lcom/konka/tvsettings/RootActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->getLocalDimmingInfo()Lcom/konka/kkinterface/tv/CommonDesk$LocalDimmingInfo;

    move-result-object v3

    const-string v4, "RootActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "get localdimminginfo enable = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, v3, Lcom/konka/kkinterface/tv/CommonDesk$LocalDimmingInfo;->localdimmingenable:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v4, v3, Lcom/konka/kkinterface/tv/CommonDesk$LocalDimmingInfo;->localdimmingenable:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/konka/tvsettings/RootActivity$1;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v4}, Lcom/konka/tvsettings/RootActivity;->access$0(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/PictureDesk;->GetBacklight()S

    move-result v0

    const-string v4, "RootActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, " set backlight ["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/PictureManager;->getBacklight()I

    move-result v4

    if-eq v4, v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/mstar/android/tvapi/common/PictureManager;->setBacklight(I)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method
