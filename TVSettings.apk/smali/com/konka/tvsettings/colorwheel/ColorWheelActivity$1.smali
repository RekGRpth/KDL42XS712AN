.class Lcom/konka/tvsettings/colorwheel/ColorWheelActivity$1;
.super Landroid/os/Handler;
.source "ColorWheelActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity$1;->this$0:Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1    # Landroid/os/Message;

    const/4 v3, 0x1

    iget v4, p1, Landroid/os/Message;->what:I

    if-ne v4, v3, :cond_1

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "red"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v4

    float-to-int v2, v4

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "green"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v4

    float-to-int v1, v4

    rsub-int/lit8 v4, v2, 0x64

    sub-int v0, v4, v1

    iget-object v4, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity$1;->this$0:Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;

    # getter for: Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->pbRed:Landroid/widget/ProgressBar;
    invoke-static {v4}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->access$0(Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;)Landroid/widget/ProgressBar;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity$1;->this$0:Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;

    # getter for: Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->textViewRed:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->access$1(Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;)Landroid/widget/TextView;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity$1;->this$0:Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;

    # getter for: Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->pbGreen:Landroid/widget/ProgressBar;
    invoke-static {v4}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->access$2(Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;)Landroid/widget/ProgressBar;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity$1;->this$0:Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;

    # getter for: Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->textViewGreen:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->access$3(Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;)Landroid/widget/TextView;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity$1;->this$0:Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;

    # getter for: Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->pbBlue:Landroid/widget/ProgressBar;
    invoke-static {v4}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->access$4(Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;)Landroid/widget/ProgressBar;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity$1;->this$0:Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;

    # getter for: Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->textViewBlue:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->access$5(Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;)Landroid/widget/TextView;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "%"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v4, "DEBUG"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "refreshred: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; blue: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "; green: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity$1;->this$0:Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;

    iget-object v5, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity$1;->this$0:Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;

    # invokes: Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->TVCOLORWHEEL_CalculateColorMerge(III)I
    invoke-static {v5, v2, v1, v0}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->access$6(Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;III)I

    move-result v5

    invoke-static {v4, v5}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->access$7(Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;I)V

    iget-object v4, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity$1;->this$0:Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;

    iget-object v5, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity$1;->this$0:Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;

    # getter for: Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->temp:I
    invoke-static {v5}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->access$8(Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;)I

    move-result v5

    if-ne v5, v3, :cond_0

    const/4 v3, 0x0

    :cond_0
    invoke-static {v4, v3}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->access$9(Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;I)V

    iget-object v3, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity$1;->this$0:Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;

    # getter for: Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->textView:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->access$10(Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;)Landroid/widget/TextView;

    move-result-object v3

    # getter for: Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->STR_ID:[I
    invoke-static {}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->access$11()[I

    move-result-object v4

    iget-object v5, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity$1;->this$0:Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;

    # invokes: Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->getCurrentColor(III)I
    invoke-static {v5, v2, v1, v0}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->access$12(Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;III)I

    move-result v5

    aget v4, v4, v5

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    iget-object v3, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity$1;->this$0:Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;

    invoke-virtual {v3}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->setTextViewInverted()V

    iget-object v3, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity$1;->this$0:Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;

    # getter for: Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->imageView:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->access$13(Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;)Landroid/widget/ImageView;

    move-result-object v3

    # getter for: Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->IMG_ID:[[I
    invoke-static {}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->access$14()[[I

    move-result-object v4

    iget-object v5, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity$1;->this$0:Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;

    # getter for: Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->COUNT:I
    invoke-static {v5}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->access$15(Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;)I

    move-result v5

    aget-object v4, v4, v5

    iget-object v5, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity$1;->this$0:Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;

    # getter for: Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->temp:I
    invoke-static {v5}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->access$8(Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;)I

    move-result v5

    aget v4, v4, v5

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :cond_1
    return-void
.end method
