.class public Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;
.super Landroid/app/Activity;
.source "ColorWheelActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field private static final BLUE:I = 0x2

.field private static final GREEN:I = 0x1

.field private static final IMG_ID:[[I

.field private static final RED:I = 0x0

.field private static final REFRESH_IMG:I = 0x2

.field private static final REFRESH_NUM:I = 0x1

.field private static final STR_ID:[I


# instance fields
.field private COUNT:I

.field private bRun:Z

.field private broadcastReceiver:Landroid/content/BroadcastReceiver;

.field handler:Landroid/os/Handler;

.field private imageView:Landroid/widget/ImageView;

.field private layout:Landroid/widget/RelativeLayout;

.field private pbBlue:Landroid/widget/ProgressBar;

.field private pbGreen:Landroid/widget/ProgressBar;

.field private pbRed:Landroid/widget/ProgressBar;

.field private temp:I

.field private textView:Landroid/widget/TextView;

.field private textViewBlue:Landroid/widget/TextView;

.field private textViewGreen:Landroid/widget/TextView;

.field private textViewInverted:Landroid/widget/ImageView;

.field private textViewRed:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x3

    const/4 v3, 0x2

    new-array v0, v4, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->STR_ID:[I

    const/16 v0, 0xc

    new-array v0, v0, [[I

    const/4 v1, 0x0

    new-array v2, v3, [I

    fill-array-data v2, :array_1

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-array v2, v3, [I

    fill-array-data v2, :array_2

    aput-object v2, v0, v1

    new-array v1, v3, [I

    fill-array-data v1, :array_3

    aput-object v1, v0, v3

    new-array v1, v3, [I

    fill-array-data v1, :array_4

    aput-object v1, v0, v4

    const/4 v1, 0x4

    new-array v2, v3, [I

    fill-array-data v2, :array_5

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v3, [I

    fill-array-data v2, :array_6

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v3, [I

    fill-array-data v2, :array_7

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v3, [I

    fill-array-data v2, :array_8

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v3, [I

    fill-array-data v2, :array_9

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-array v2, v3, [I

    fill-array-data v2, :array_a

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v2, v3, [I

    fill-array-data v2, :array_b

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-array v2, v3, [I

    fill-array-data v2, :array_c

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->IMG_ID:[[I

    return-void

    :array_0
    .array-data 4
        0x7f0a0127    # com.konka.tvsettings.R.string.str_color_wheel_red_hight
        0x7f0a0128    # com.konka.tvsettings.R.string.str_color_wheel_green_hight
        0x7f0a0129    # com.konka.tvsettings.R.string.str_color_wheel_blue_hight
    .end array-data

    :array_1
    .array-data 4
        0x7f02000c    # com.konka.tvsettings.R.drawable.color_wheel_bg00_0
        0x7f02000d    # com.konka.tvsettings.R.drawable.color_wheel_bg00_1
    .end array-data

    :array_2
    .array-data 4
        0x7f02000e    # com.konka.tvsettings.R.drawable.color_wheel_bg01_0
        0x7f02000f    # com.konka.tvsettings.R.drawable.color_wheel_bg01_1
    .end array-data

    :array_3
    .array-data 4
        0x7f020010    # com.konka.tvsettings.R.drawable.color_wheel_bg02_0
        0x7f020011    # com.konka.tvsettings.R.drawable.color_wheel_bg02_1
    .end array-data

    :array_4
    .array-data 4
        0x7f020012    # com.konka.tvsettings.R.drawable.color_wheel_bg03_0
        0x7f020013    # com.konka.tvsettings.R.drawable.color_wheel_bg03_1
    .end array-data

    :array_5
    .array-data 4
        0x7f020014    # com.konka.tvsettings.R.drawable.color_wheel_bg04_0
        0x7f020015    # com.konka.tvsettings.R.drawable.color_wheel_bg04_1
    .end array-data

    :array_6
    .array-data 4
        0x7f020016    # com.konka.tvsettings.R.drawable.color_wheel_bg05_0
        0x7f020017    # com.konka.tvsettings.R.drawable.color_wheel_bg05_1
    .end array-data

    :array_7
    .array-data 4
        0x7f020018    # com.konka.tvsettings.R.drawable.color_wheel_bg06_0
        0x7f020019    # com.konka.tvsettings.R.drawable.color_wheel_bg06_1
    .end array-data

    :array_8
    .array-data 4
        0x7f02001a    # com.konka.tvsettings.R.drawable.color_wheel_bg07_0
        0x7f02001b    # com.konka.tvsettings.R.drawable.color_wheel_bg07_1
    .end array-data

    :array_9
    .array-data 4
        0x7f02001c    # com.konka.tvsettings.R.drawable.color_wheel_bg08_0
        0x7f02001d    # com.konka.tvsettings.R.drawable.color_wheel_bg08_1
    .end array-data

    :array_a
    .array-data 4
        0x7f02001e    # com.konka.tvsettings.R.drawable.color_wheel_bg09_0
        0x7f02001f    # com.konka.tvsettings.R.drawable.color_wheel_bg09_1
    .end array-data

    :array_b
    .array-data 4
        0x7f020020    # com.konka.tvsettings.R.drawable.color_wheel_bg10_0
        0x7f020021    # com.konka.tvsettings.R.drawable.color_wheel_bg10_1
    .end array-data

    :array_c
    .array-data 4
        0x7f020022    # com.konka.tvsettings.R.drawable.color_wheel_bg11_0
        0x7f020023    # com.konka.tvsettings.R.drawable.color_wheel_bg11_1
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->pbRed:Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->textViewRed:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->pbBlue:Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->textViewBlue:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->pbGreen:Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->textViewGreen:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->imageView:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->layout:Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->textView:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->textViewInverted:Landroid/widget/ImageView;

    iput v1, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->COUNT:I

    iput v1, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->temp:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->bRun:Z

    new-instance v0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity$1;-><init>(Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->handler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity$2;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity$2;-><init>(Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->broadcastReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private TVCOLORWHEEL_CalculateColorMerge(III)I
    .locals 10
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/16 v1, 0x64

    const/16 v6, 0x400

    rsub-int v0, p1, 0x400

    rsub-int v2, p2, 0x400

    rsub-int v7, p3, 0x400

    if-ge v0, v6, :cond_0

    move v6, v0

    :cond_0
    if-ge v2, v6, :cond_1

    move v6, v2

    :cond_1
    if-ge v7, v6, :cond_2

    move v6, v7

    :cond_2
    const/16 v8, 0x400

    if-ne v6, v8, :cond_5

    const/4 v0, 0x0

    const/4 v2, 0x0

    const/4 v7, 0x0

    :goto_0
    if-ge v0, v2, :cond_6

    move v5, v0

    move v3, v2

    :goto_1
    if-le v5, v7, :cond_7

    move v5, v7

    :cond_3
    :goto_2
    if-ne v0, v3, :cond_f

    if-eqz v0, :cond_f

    if-nez v7, :cond_a

    mul-int/lit8 v8, v2, 0x64

    div-int v2, v8, v0

    const/16 v0, 0x64

    const/16 v8, 0x32

    if-ge v2, v8, :cond_8

    const/4 v1, 0x7

    :cond_4
    :goto_3
    return v1

    :cond_5
    sub-int v8, v0, v6

    mul-int/lit8 v8, v8, 0x64

    rsub-int v9, v6, 0x400

    div-int v0, v8, v9

    sub-int v8, v2, v6

    mul-int/lit8 v8, v8, 0x64

    rsub-int v9, v6, 0x400

    div-int v2, v8, v9

    sub-int v8, v7, v6

    mul-int/lit8 v8, v8, 0x64

    rsub-int v9, v6, 0x400

    div-int v7, v8, v9

    goto :goto_0

    :cond_6
    move v5, v2

    move v3, v0

    goto :goto_1

    :cond_7
    if-ge v3, v7, :cond_3

    move v4, v3

    move v3, v7

    goto :goto_2

    :cond_8
    const/16 v8, 0x46

    if-ge v2, v8, :cond_9

    const/4 v1, 0x6

    goto :goto_3

    :cond_9
    const/4 v1, 0x6

    goto :goto_3

    :cond_a
    const/16 v8, 0x46

    if-le v2, v8, :cond_b

    const/4 v1, 0x2

    goto :goto_3

    :cond_b
    const/16 v8, 0x32

    if-le v2, v8, :cond_c

    const/4 v1, 0x1

    goto :goto_3

    :cond_c
    const/16 v8, 0x1e

    if-le v2, v8, :cond_d

    const/4 v1, 0x0

    goto :goto_3

    :cond_d
    const/16 v8, 0xf

    if-le v2, v8, :cond_e

    const/16 v1, 0xb

    goto :goto_3

    :cond_e
    const/16 v1, 0x9

    goto :goto_3

    :cond_f
    if-ne v2, v3, :cond_14

    if-eqz v2, :cond_14

    if-nez v7, :cond_12

    mul-int/lit8 v8, v0, 0x64

    div-int v0, v8, v2

    const/16 v2, 0x64

    const/16 v8, 0x28

    if-ge v0, v8, :cond_10

    const/4 v1, 0x4

    goto :goto_3

    :cond_10
    const/16 v8, 0x50

    if-ge v0, v8, :cond_11

    const/4 v1, 0x5

    goto :goto_3

    :cond_11
    const/4 v1, 0x6

    goto :goto_3

    :cond_12
    const/4 v1, 0x3

    const/16 v8, 0x50

    if-le v0, v8, :cond_13

    const/16 v1, 0x9

    goto :goto_3

    :cond_13
    const/16 v8, 0x28

    if-le v0, v8, :cond_4

    const/16 v1, 0xa

    goto :goto_3

    :cond_14
    if-eqz v7, :cond_4

    if-nez v0, :cond_19

    mul-int/lit8 v8, v2, 0x64

    div-int v2, v8, v7

    const/16 v7, 0x64

    const/16 v8, 0xf

    if-ge v2, v8, :cond_15

    const/16 v1, 0xb

    goto :goto_3

    :cond_15
    const/16 v8, 0x1e

    if-ge v2, v8, :cond_16

    const/4 v1, 0x0

    goto/16 :goto_3

    :cond_16
    const/16 v8, 0x32

    if-ge v2, v8, :cond_17

    const/4 v1, 0x1

    goto/16 :goto_3

    :cond_17
    const/16 v8, 0x46

    if-ge v2, v8, :cond_18

    const/4 v1, 0x2

    goto/16 :goto_3

    :cond_18
    const/4 v1, 0x3

    goto/16 :goto_3

    :cond_19
    if-nez v2, :cond_1c

    const/16 v8, 0x28

    if-ge v0, v8, :cond_1a

    const/16 v1, 0xa

    goto/16 :goto_3

    :cond_1a
    const/16 v8, 0x50

    if-ge v0, v8, :cond_1b

    const/16 v1, 0x9

    goto/16 :goto_3

    :cond_1b
    const/16 v1, 0x8

    goto/16 :goto_3

    :cond_1c
    const/16 v8, 0x28

    if-ge v0, v8, :cond_1d

    const/16 v1, 0xa

    :goto_4
    const/16 v8, 0xf

    if-lt v2, v8, :cond_4

    const/16 v8, 0x1e

    if-ge v2, v8, :cond_1f

    const/4 v1, 0x0

    goto/16 :goto_3

    :cond_1d
    const/16 v8, 0x50

    if-ge v0, v8, :cond_1e

    const/16 v1, 0x9

    goto :goto_4

    :cond_1e
    const/16 v1, 0x8

    goto :goto_4

    :cond_1f
    const/16 v8, 0x32

    if-ge v2, v8, :cond_20

    const/4 v1, 0x1

    goto/16 :goto_3

    :cond_20
    const/16 v8, 0x46

    if-ge v2, v8, :cond_21

    const/4 v1, 0x2

    goto/16 :goto_3

    :cond_21
    const/4 v1, 0x3

    goto/16 :goto_3
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;)Landroid/widget/ProgressBar;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->pbRed:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->textViewRed:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$10(Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->textView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$11()[I
    .locals 1

    sget-object v0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->STR_ID:[I

    return-object v0
.end method

.method static synthetic access$12(Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;III)I
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->getCurrentColor(III)I

    move-result v0

    return v0
.end method

.method static synthetic access$13(Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->imageView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$14()[[I
    .locals 1

    sget-object v0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->IMG_ID:[[I

    return-object v0
.end method

.method static synthetic access$15(Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->COUNT:I

    return v0
.end method

.method static synthetic access$16(Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;)Landroid/widget/RelativeLayout;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->layout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;)Landroid/widget/ProgressBar;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->pbGreen:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->textViewGreen:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;)Landroid/widget/ProgressBar;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->pbBlue:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->textViewBlue:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;III)I
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->TVCOLORWHEEL_CalculateColorMerge(III)I

    move-result v0

    return v0
.end method

.method static synthetic access$7(Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->COUNT:I

    return-void
.end method

.method static synthetic access$8(Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->temp:I

    return v0
.end method

.method static synthetic access$9(Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->temp:I

    return-void
.end method

.method private getCurrentColor(III)I
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x2

    if-le p1, p2, :cond_1

    if-le p1, p3, :cond_0

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-le p2, p3, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030011    # com.konka.tvsettings.R.layout.colorwheel_menu

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->setContentView(I)V

    const v0, 0x7f070071    # com.konka.tvsettings.R.id.color_wheel_layout_container

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->layout:Landroid/widget/RelativeLayout;

    const v0, 0x7f070074    # com.konka.tvsettings.R.id.color_wheel_pb_red

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->pbRed:Landroid/widget/ProgressBar;

    const v0, 0x7f070075    # com.konka.tvsettings.R.id.color_wheel_tv_red

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->textViewRed:Landroid/widget/TextView;

    const v0, 0x7f07007a    # com.konka.tvsettings.R.id.color_wheel_pb_blue

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->pbBlue:Landroid/widget/ProgressBar;

    const v0, 0x7f07007b    # com.konka.tvsettings.R.id.color_wheel_tv_blue

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->textViewBlue:Landroid/widget/TextView;

    const v0, 0x7f070077    # com.konka.tvsettings.R.id.color_wheel_pb_green

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->pbGreen:Landroid/widget/ProgressBar;

    const v0, 0x7f070078    # com.konka.tvsettings.R.id.color_wheel_tv_green

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->textViewGreen:Landroid/widget/TextView;

    const v0, 0x7f070072    # com.konka.tvsettings.R.id.color_wheel_imageView

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->imageView:Landroid/widget/ImageView;

    const v0, 0x7f07007c    # com.konka.tvsettings.R.id.color_wheel_textView

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->textView:Landroid/widget/TextView;

    const v0, 0x7f07007d    # com.konka.tvsettings.R.id.color_wheel_textView_inverted

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->textViewInverted:Landroid/widget/ImageView;

    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->bRun:Z

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const-string v1, "DEBUG"

    const-string v2, "Color Wheel Menu On Key Down."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.konka.tv.action.KEYDOWN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "keyCode"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->layout:Landroid/widget/RelativeLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->layout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->invalidate()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->finish()V

    const/4 v1, 0x1

    return v1
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    iget-object v0, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->broadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.konka.tv.action.SIGNAL_UNLOCK"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->broadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public run()V
    .locals 15

    :goto_0
    iget-boolean v12, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->bRun:Z

    if-nez v12, :cond_0

    return-void

    :cond_0
    const-wide/16 v12, 0x3e8

    :try_start_0
    invoke-static {v12, v13}, Ljava/lang/Thread;->sleep(J)V

    const/16 v10, 0x3c0

    const/16 v11, 0x190

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/GetPixelRgbStage$EnumGetPixelRgbStage;->E_GET_PIXEL_STAGE_AFTER_DLC:Lcom/mstar/android/tvapi/common/vo/GetPixelRgbStage$EnumGetPixelRgbStage;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    const/high16 v7, 0x42480000    # 50.0f

    const/high16 v5, 0x42480000    # 50.0f

    const/high16 v0, 0x42480000    # 50.0f

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v12

    if-eqz v12, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v12

    if-eqz v12, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v12

    invoke-virtual {v12, v3, v10, v11, v4}, Lcom/mstar/android/tvapi/common/PictureManager;->getPixelRgb(Lcom/mstar/android/tvapi/common/vo/GetPixelRgbStage$EnumGetPixelRgbStage;SSLcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)Lcom/mstar/android/tvapi/common/vo/Rgb_Data;

    move-result-object v8

    iget v12, v8, Lcom/mstar/android/tvapi/common/vo/Rgb_Data;->r:I

    int-to-float v7, v12

    iget v12, v8, Lcom/mstar/android/tvapi/common/vo/Rgb_Data;->g:I

    int-to-float v5, v12

    iget v12, v8, Lcom/mstar/android/tvapi/common/vo/Rgb_Data;->b:I

    int-to-float v0, v12

    :cond_1
    const-string v12, "DEBUG"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "R + G + B:"

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-float v14, v7, v5

    add-float/2addr v14, v0

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v12, "DEBUG"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "R:"

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v12, "DEBUG"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "G:"

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v12, "DEBUG"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "B:"

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    add-float v12, v7, v5

    add-float v9, v12, v0

    const/high16 v12, 0x42c80000    # 100.0f

    mul-float/2addr v12, v7

    div-float v7, v12, v9

    const/high16 v12, 0x42c80000    # 100.0f

    mul-float/2addr v12, v5

    div-float v5, v12, v9

    const/high16 v12, 0x42c80000    # 100.0f

    sub-float/2addr v12, v7

    sub-float v0, v12, v5

    new-instance v6, Landroid/os/Message;

    invoke-direct {v6}, Landroid/os/Message;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v12, "red"

    invoke-virtual {v1, v12, v7}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    const-string v12, "green"

    invoke-virtual {v1, v12, v5}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    const-string v12, "blue"

    invoke-virtual {v1, v12, v0}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    invoke-virtual {v6, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    const/4 v12, 0x1

    iput v12, v6, Landroid/os/Message;->what:I

    iget-object v12, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v12, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0

    :catch_1
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_0
.end method

.method protected setTextViewInverted()V
    .locals 9

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->textView:Landroid/widget/TextView;

    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setDrawingCacheEnabled(Z)V

    iget-object v2, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->textView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->buildDrawingCache()V

    iget-object v2, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->textView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v8, 0x7f080002    # com.konka.tvsettings.R.color.text_normal_col

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v2, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->textView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v6, -0x40800000    # -1.0f

    invoke-virtual {v5, v2, v6}, Landroid/graphics/Matrix;->preScale(FF)Z

    move v2, v1

    move v6, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v7, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v7, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->textViewInverted:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->textView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/colorwheel/ColorWheelActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f080001    # com.konka.tvsettings.R.color.text_select_col

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method
