.class public Lcom/konka/tvsettings/time/OnTimeActivity;
.super Lcom/konka/tvsettings/BaseKonkaActivity;
.source "OnTimeActivity.java"


# static fields
.field private static m_isBootSwitchOn:Z


# instance fields
.field private channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

.field private commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

.field private inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field private itemBootSource:Landroid/widget/TextView;

.field private itemBootTime:Landroid/widget/TextView;

.field private itemOnTimeSetTime:Landroid/widget/LinearLayout;

.field private itemOnTimeSource:Landroid/widget/LinearLayout;

.field private itemOnTimeSwitch:Lcom/konka/tvsettings/picture/PictureSettingItem1;

.field private leftarrow:Landroid/widget/ImageView;

.field private mArrCurrent:[Ljava/lang/CharSequence;

.field private mEnumOnTimeSource:[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

.field private mInputSourceName:[Ljava/lang/String;

.field private m_SourceNum:Ljava/lang/Integer;

.field private m_iBootChannelIndex:Ljava/lang/Integer;

.field private myHandler:Landroid/os/Handler;

.field private onBootTimeSetListener:Landroid/app/TimePickerDialog$OnTimeSetListener;

.field private rightarrow:Landroid/widget/ImageView;

.field private serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

.field private tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

.field private timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/konka/tvsettings/time/OnTimeActivity;->m_isBootSwitchOn:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;-><init>()V

    iput-object v1, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->itemOnTimeSwitch:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    iput-object v1, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->itemOnTimeSetTime:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->itemOnTimeSource:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->itemBootTime:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->itemBootSource:Landroid/widget/TextView;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->m_iBootChannelIndex:Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->m_SourceNum:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->mArrCurrent:[Ljava/lang/CharSequence;

    new-instance v0, Lcom/konka/tvsettings/time/OnTimeActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/time/OnTimeActivity$1;-><init>(Lcom/konka/tvsettings/time/OnTimeActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->myHandler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/tvsettings/time/OnTimeActivity$2;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/time/OnTimeActivity$2;-><init>(Lcom/konka/tvsettings/time/OnTimeActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->onBootTimeSetListener:Landroid/app/TimePickerDialog$OnTimeSetListener;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/time/OnTimeActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/time/OnTimeActivity;->updateBootTimeUI()V

    return-void
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/time/OnTimeActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->itemOnTimeSwitch:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-object v0
.end method

.method static synthetic access$2(Z)V
    .locals 0

    sput-boolean p0, Lcom/konka/tvsettings/time/OnTimeActivity;->m_isBootSwitchOn:Z

    return-void
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/time/OnTimeActivity;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->itemOnTimeSetTime:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$4()Z
    .locals 1

    sget-boolean v0, Lcom/konka/tvsettings/time/OnTimeActivity;->m_isBootSwitchOn:Z

    return v0
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/time/OnTimeActivity;Landroid/widget/LinearLayout;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/konka/tvsettings/time/OnTimeActivity;->setOptionItemEnabled(Landroid/widget/LinearLayout;Z)V

    return-void
.end method

.method static synthetic access$6(Lcom/konka/tvsettings/time/OnTimeActivity;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->itemOnTimeSource:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$7(Lcom/konka/tvsettings/time/OnTimeActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/time/OnTimeActivity;->showDialog()V

    return-void
.end method

.method private addItemOnTimeSetTime()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->itemOnTimeSetTime:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/tvsettings/time/OnTimeActivity$4;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/time/OnTimeActivity$4;-><init>(Lcom/konka/tvsettings/time/OnTimeActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private addItemOnTimeSource()V
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TimerManager;->getOnTime()Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->getTvSource()Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/konka/tvsettings/time/OnTimeActivity;->getIdxByInputSrc(Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->m_iBootChannelIndex:Ljava/lang/Integer;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-direct {p0}, Lcom/konka/tvsettings/time/OnTimeActivity;->updateBootChannelUI()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/time/OnTimeActivity;->updateBootChannel()V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private addItemOnTimeSwitch()V
    .locals 7

    const/4 v5, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TimerManager;->getOnTime()Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->getTimerPeriod()Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Off:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/tvsettings/time/OnTimeActivity;->m_isBootSwitchOn:Z

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TimerManager;->getOnTime()Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->getTimerPeriod()Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Once:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/konka/tvsettings/time/OnTimeActivity;->m_isBootSwitchOn:Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->itemOnTimeSetTime:Landroid/widget/LinearLayout;

    sget-boolean v1, Lcom/konka/tvsettings/time/OnTimeActivity;->m_isBootSwitchOn:Z

    invoke-direct {p0, v0, v1}, Lcom/konka/tvsettings/time/OnTimeActivity;->setOptionItemEnabled(Landroid/widget/LinearLayout;Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->itemOnTimeSource:Landroid/widget/LinearLayout;

    sget-boolean v1, Lcom/konka/tvsettings/time/OnTimeActivity;->m_isBootSwitchOn:Z

    invoke-direct {p0, v0, v1}, Lcom/konka/tvsettings/time/OnTimeActivity;->setOptionItemEnabled(Landroid/widget/LinearLayout;Z)V

    sget-boolean v0, Lcom/konka/tvsettings/time/OnTimeActivity;->m_isBootSwitchOn:Z

    if-eqz v0, :cond_2

    const/4 v5, 0x1

    :goto_1
    new-instance v0, Lcom/konka/tvsettings/time/OnTimeActivity$3;

    const v3, 0x7f07020d    # com.konka.tvsettings.R.id.ontime_menu_ontimeswitch

    const v4, 0x7f0b0026    # com.konka.tvsettings.R.array.str_arr_time_ontimeswitch_vals

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/time/OnTimeActivity$3;-><init>(Lcom/konka/tvsettings/time/OnTimeActivity;Landroid/app/Activity;III)V

    iput-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->itemOnTimeSwitch:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-void

    :catch_0
    move-exception v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_2
    const/4 v5, 0x0

    goto :goto_1
.end method

.method private dataConfigurate()V
    .locals 14

    const/4 v13, 0x1

    iget-object v10, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v10}, Lcom/konka/kkinterface/tv/CommonDesk;->getInputSourceInfo()Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    move-result-object v10

    iget v2, v10, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountDTV:I

    iget-object v10, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v10}, Lcom/konka/kkinterface/tv/CommonDesk;->getInputSourceInfo()Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    move-result-object v10

    iget v0, v10, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountATV:I

    iget-object v10, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v10}, Lcom/konka/kkinterface/tv/CommonDesk;->getInputSourceInfo()Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    move-result-object v10

    iget v1, v10, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountAV:I

    iget-object v10, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v10}, Lcom/konka/kkinterface/tv/CommonDesk;->getInputSourceInfo()Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    move-result-object v10

    iget v6, v10, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountYPbPr:I

    iget-object v10, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v10}, Lcom/konka/kkinterface/tv/CommonDesk;->getInputSourceInfo()Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    move-result-object v10

    iget v5, v10, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountVGA:I

    iget-object v10, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v10}, Lcom/konka/kkinterface/tv/CommonDesk;->getInputSourceInfo()Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    move-result-object v10

    iget v3, v10, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountHDMI:I

    add-int v10, v2, v0

    add-int/2addr v10, v1

    add-int/2addr v10, v6

    add-int/2addr v10, v5

    add-int v4, v10, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    iput-object v10, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->m_SourceNum:Ljava/lang/Integer;

    new-array v10, v4, [Ljava/lang/String;

    iput-object v10, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->mInputSourceName:[Ljava/lang/String;

    new-array v10, v4, [Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    iput-object v10, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->mEnumOnTimeSource:[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    const/4 v7, 0x0

    if-ne v2, v13, :cond_0

    iget-object v10, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->mInputSourceName:[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/konka/tvsettings/time/OnTimeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0a00c9    # com.konka.tvsettings.R.string.str_popup_input_menu_dtv

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v7

    iget-object v10, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->mEnumOnTimeSource:[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    sget-object v11, Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;->EN_Time_OnTimer_Source_DTV:Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    aput-object v11, v10, v7

    add-int/lit8 v7, v7, 0x1

    :cond_0
    if-ne v0, v13, :cond_1

    iget-object v10, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->mInputSourceName:[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/konka/tvsettings/time/OnTimeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0a00ca    # com.konka.tvsettings.R.string.str_popup_input_menu_atv

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v7

    iget-object v10, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->mEnumOnTimeSource:[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    sget-object v11, Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;->EN_Time_OnTimer_Source_ATV:Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    aput-object v11, v10, v7

    add-int/lit8 v7, v7, 0x1

    :cond_1
    const/4 v8, 0x0

    :goto_0
    if-lt v8, v1, :cond_3

    const/4 v8, 0x0

    :goto_1
    if-lt v8, v6, :cond_7

    if-ne v5, v13, :cond_2

    iget-object v10, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->mInputSourceName:[Ljava/lang/String;

    const-string v11, "VGA"

    aput-object v11, v10, v7

    iget-object v10, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->mEnumOnTimeSource:[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    sget-object v11, Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;->EN_Time_OnTimer_Source_RGB:Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    aput-object v11, v10, v7

    add-int/lit8 v7, v7, 0x1

    :cond_2
    const/4 v8, 0x0

    :goto_2
    if-lt v8, v3, :cond_9

    iget-object v10, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->mInputSourceName:[Ljava/lang/String;

    iput-object v10, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->mArrCurrent:[Ljava/lang/CharSequence;

    const-string v10, "===>individ==>enum_inputsources:"

    invoke-static {v10}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v11, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->mInputSourceName:[Ljava/lang/String;

    array-length v12, v11

    const/4 v10, 0x0

    :goto_3
    if-lt v10, v12, :cond_e

    return-void

    :cond_3
    if-nez v8, :cond_5

    if-le v1, v13, :cond_4

    iget-object v10, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->mInputSourceName:[Ljava/lang/String;

    const-string v11, "AV1"

    aput-object v11, v10, v7

    :goto_4
    iget-object v10, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->mEnumOnTimeSource:[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    sget-object v11, Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;->EN_Time_OnTimer_Source_AV:Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    aput-object v11, v10, v7

    :goto_5
    add-int/lit8 v8, v8, 0x1

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_4
    iget-object v10, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->mInputSourceName:[Ljava/lang/String;

    const-string v11, "AV"

    aput-object v11, v10, v7

    goto :goto_4

    :cond_5
    iget-object v10, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->mInputSourceName:[Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "AV"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v12, v8, 0x1

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v7

    iget-object v11, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->mEnumOnTimeSource:[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    if-ne v8, v13, :cond_6

    sget-object v10, Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;->EN_Time_OnTimer_Source_AV2:Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    :goto_6
    aput-object v10, v11, v7

    goto :goto_5

    :cond_6
    sget-object v10, Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;->EN_Time_OnTimer_Source_AV2:Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    goto :goto_6

    :cond_7
    if-nez v8, :cond_8

    iget-object v10, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->mInputSourceName:[Ljava/lang/String;

    const-string v11, "YPbPr"

    aput-object v11, v10, v7

    iget-object v10, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->mEnumOnTimeSource:[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    sget-object v11, Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;->EN_Time_OnTimer_Source_COMPONENT:Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    aput-object v11, v10, v7

    :goto_7
    add-int/lit8 v8, v8, 0x1

    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_8
    iget-object v10, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->mInputSourceName:[Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "YPbPr"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v12, v8, 0x1

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v7

    iget-object v10, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->mEnumOnTimeSource:[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    sget-object v11, Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;->EN_Time_OnTimer_Source_COMPONENT2:Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    aput-object v11, v10, v7

    goto :goto_7

    :cond_9
    if-nez v8, :cond_b

    if-le v3, v13, :cond_a

    iget-object v10, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->mInputSourceName:[Ljava/lang/String;

    const-string v11, "HDMI1"

    aput-object v11, v10, v7

    :goto_8
    iget-object v10, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->mEnumOnTimeSource:[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    sget-object v11, Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;->EN_Time_OnTimer_Source_HDMI:Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    aput-object v11, v10, v7

    :goto_9
    add-int/lit8 v8, v8, 0x1

    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_2

    :cond_a
    iget-object v10, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->mInputSourceName:[Ljava/lang/String;

    const-string v11, "HDMI"

    aput-object v11, v10, v7

    goto :goto_8

    :cond_b
    iget-object v10, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->mInputSourceName:[Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "HDMI"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v12, v8, 0x1

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v10, v7

    if-ne v8, v13, :cond_c

    iget-object v10, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->mEnumOnTimeSource:[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    sget-object v11, Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;->EN_Time_OnTimer_Source_HDMI2:Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    aput-object v11, v10, v7

    goto :goto_9

    :cond_c
    const/4 v10, 0x2

    if-ne v8, v10, :cond_d

    iget-object v10, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->mEnumOnTimeSource:[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    sget-object v11, Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;->EN_Time_OnTimer_Source_HDMI3:Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    aput-object v11, v10, v7

    goto :goto_9

    :cond_d
    iget-object v10, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->mEnumOnTimeSource:[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    sget-object v11, Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;->EN_Time_OnTimer_Source_HDMI4:Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    aput-object v11, v10, v7

    goto :goto_9

    :cond_e
    aget-object v9, v11, v10

    invoke-static {v9}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_3
.end method

.method private debug(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method private getIdxByInputSrc(Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;)I
    .locals 6
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    const/4 v2, 0x0

    const/4 v1, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "getIdxByInputSrc========the source is ===="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->mEnumOnTimeSource:[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    array-length v5, v4

    move v3, v2

    :goto_0
    if-lt v3, v5, :cond_1

    move v1, v2

    :cond_0
    return v1

    :cond_1
    aget-object v0, v4, v3

    if-eq v0, p1, :cond_0

    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private initDateTime()V
    .locals 3

    :try_start_0
    iget-object v1, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TimerManager;->getOnTime()Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "init_PowerOn_Time: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    invoke-direct {p0, v2}, Lcom/konka/tvsettings/time/OnTimeActivity;->timeToString(Landroid/text/format/Time;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/konka/tvsettings/time/OnTimeActivity;->debug(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private setOptionItemEnabled(Landroid/widget/LinearLayout;Z)V
    .locals 0
    .param p1    # Landroid/widget/LinearLayout;
    .param p2    # Z

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setClickable(Z)V

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    return-void
.end method

.method private showDialog()V
    .locals 14

    const/4 v6, 0x1

    new-instance v7, Landroid/text/format/Time;

    invoke-direct {v7}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v7}, Landroid/text/format/Time;->setToNow()V

    invoke-virtual {v7, v6}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    invoke-virtual {v7, v2, v3}, Landroid/text/format/Time;->set(J)V

    const/4 v1, 0x0

    new-instance v1, Landroid/app/TimePickerDialog;

    iget-object v3, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->onBootTimeSetListener:Landroid/app/TimePickerDialog$OnTimeSetListener;

    iget-object v2, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget v4, v2, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->hour:I

    iget-object v2, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget v5, v2, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->minute:I

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Landroid/app/TimePickerDialog;-><init>(Landroid/content/Context;Landroid/app/TimePickerDialog$OnTimeSetListener;IIZ)V

    new-instance v2, Lcom/konka/tvsettings/time/OnTimeActivity$5;

    invoke-direct {v2, p0}, Lcom/konka/tvsettings/time/OnTimeActivity$5;-><init>(Lcom/konka/tvsettings/time/OnTimeActivity;)V

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    :cond_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const/4 v12, 0x0

    :try_start_0
    const-string v2, "mTimePicker"

    invoke-virtual {v8, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v10

    const/4 v2, 0x1

    invoke-virtual {v10, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    invoke-virtual {v10, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TimePicker;

    move-object v12, v0
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    if-eqz v12, :cond_1

    invoke-virtual {v12}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    const/4 v13, 0x0

    :try_start_1
    const-string v2, "mHourSpinner"

    invoke-virtual {v11, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v10

    const/4 v2, 0x1

    invoke-virtual {v10, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    invoke-virtual {v10, v12}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v13

    :goto_1
    instance-of v2, v13, Landroid/widget/NumberPicker;

    if-eqz v2, :cond_1

    move-object v2, v13

    check-cast v2, Landroid/widget/NumberPicker;

    invoke-virtual {v2}, Landroid/widget/NumberPicker;->getChildCount()I

    move-result v2

    if-lez v2, :cond_1

    check-cast v13, Landroid/widget/NumberPicker;

    const/4 v2, 0x0

    invoke-virtual {v13, v2}, Landroid/widget/NumberPicker;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    :cond_1
    return-void

    :catch_0
    move-exception v9

    invoke-virtual {v9}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v9

    invoke-virtual {v9}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception v9

    invoke-virtual {v9}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception v9

    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private timeToString(Landroid/text/format/Time;)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/text/format/Time;

    new-instance v0, Ljava/lang/StringBuilder;

    iget v1, p1, Landroid/text/format/Time;->year:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/text/format/Time;->month:I

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/time/OnTimeActivity;->formatTimeField(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/time/OnTimeActivity;->formatTimeField(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/text/format/Time;->hour:I

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/time/OnTimeActivity;->formatTimeField(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/text/format/Time;->minute:I

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/time/OnTimeActivity;->formatTimeField(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/text/format/Time;->second:I

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/time/OnTimeActivity;->formatTimeField(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private updateBootChannelUI()V
    .locals 3

    iget-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->mArrCurrent:[Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    const v0, 0x7f070212    # com.konka.tvsettings.R.id.ontime_menu_source_option

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/time/OnTimeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->itemBootSource:Landroid/widget/TextView;

    const v0, 0x7f070211    # com.konka.tvsettings.R.id.ontime_menu_source_leftarrow

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/time/OnTimeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->leftarrow:Landroid/widget/ImageView;

    const v0, 0x7f070213    # com.konka.tvsettings.R.id.ontime_menu_source_rightarrow

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/time/OnTimeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->rightarrow:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->itemBootSource:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->mArrCurrent:[Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->m_iBootChannelIndex:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "OnTime channel"

    const-string v1, "updateBootChannelUI-- mArrCurrent is null!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private updateBootTimeUI()V
    .locals 3

    const v0, 0x7f07020f    # com.konka.tvsettings.R.id.ontime_menu_settime_option

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/time/OnTimeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->itemBootTime:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->itemBootTime:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget v2, v2, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->hour:I

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/time/OnTimeActivity;->formatTimeField(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget v2, v2, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->minute:I

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/time/OnTimeActivity;->formatTimeField(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public addView()V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/time/OnTimeActivity;->addItemOnTimeSwitch()V

    invoke-direct {p0}, Lcom/konka/tvsettings/time/OnTimeActivity;->addItemOnTimeSetTime()V

    invoke-direct {p0}, Lcom/konka/tvsettings/time/OnTimeActivity;->addItemOnTimeSource()V

    return-void
.end method

.method public formatTimeField(I)Ljava/lang/String;
    .locals 3
    .param p1    # I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "0"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/konka/tvsettings/BaseKonkaActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030053    # com.konka.tvsettings.R.layout.time_ontime

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/time/OnTimeActivity;->setContentView(I)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    :cond_0
    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    iget-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    iget-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    const v0, 0x7f07020e    # com.konka.tvsettings.R.id.ontime_menu_settime

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/time/OnTimeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->itemOnTimeSetTime:Landroid/widget/LinearLayout;

    const v0, 0x7f070210    # com.konka.tvsettings.R.id.ontime_menu_source

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/time/OnTimeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->itemOnTimeSource:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->itemOnTimeSource:Landroid/widget/LinearLayout;

    const v1, 0x7f07020d    # com.konka.tvsettings.R.id.ontime_menu_ontimeswitch

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusDownId(I)V

    invoke-direct {p0}, Lcom/konka/tvsettings/time/OnTimeActivity;->initDateTime()V

    invoke-direct {p0}, Lcom/konka/tvsettings/time/OnTimeActivity;->updateBootTimeUI()V

    invoke-direct {p0}, Lcom/konka/tvsettings/time/OnTimeActivity;->dataConfigurate()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/time/OnTimeActivity;->addView()V

    iget-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->myHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/konka/tvsettings/common/LittleDownTimer;->setHandler(Landroid/os/Handler;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const v7, 0x7f070210    # com.konka.tvsettings.R.id.ontime_menu_source

    const v6, 0x7f040005    # com.konka.tvsettings.R.anim.anim_menu_exit

    const/4 v4, 0x4

    const/4 v5, 0x0

    const-string v3, "onKeyDown"

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/time/OnTimeActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch p1, :sswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/konka/tvsettings/BaseKonkaActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v3

    :goto_1
    return v3

    :sswitch_0
    if-ne v0, v7, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->m_iBootChannelIndex:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v4, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->m_SourceNum:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne v3, v4, :cond_1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->m_iBootChannelIndex:Ljava/lang/Integer;

    :goto_2
    invoke-virtual {p0}, Lcom/konka/tvsettings/time/OnTimeActivity;->updateBootChannel()V

    invoke-direct {p0}, Lcom/konka/tvsettings/time/OnTimeActivity;->updateBootChannelUI()V

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->m_iBootChannelIndex:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->m_iBootChannelIndex:Ljava/lang/Integer;

    goto :goto_2

    :sswitch_1
    if-ne v0, v7, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->m_iBootChannelIndex:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->m_SourceNum:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->m_iBootChannelIndex:Ljava/lang/Integer;

    :goto_3
    invoke-virtual {p0}, Lcom/konka/tvsettings/time/OnTimeActivity;->updateBootChannel()V

    invoke-direct {p0}, Lcom/konka/tvsettings/time/OnTimeActivity;->updateBootChannelUI()V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->m_iBootChannelIndex:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->m_iBootChannelIndex:Ljava/lang/Integer;

    goto :goto_3

    :sswitch_2
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v3, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->leftarrow:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v3, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->rightarrow:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :sswitch_3
    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iget-object v3, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->leftarrow:Landroid/widget/ImageView;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v3, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->rightarrow:Landroid/widget/ImageView;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :sswitch_4
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/time/OnTimeActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/time/OnTimeActivity;->finish()V

    invoke-virtual {p0, v5, v6}, Lcom/konka/tvsettings/time/OnTimeActivity;->overridePendingTransition(II)V

    goto/16 :goto_0

    :sswitch_5
    const-string v3, "Exit"

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-class v3, Lcom/konka/tvsettings/time/TimeSettingActivity;

    invoke-virtual {v1, p0, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/time/OnTimeActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/time/OnTimeActivity;->finish()V

    invoke-virtual {p0, v5, v6}, Lcom/konka/tvsettings/time/OnTimeActivity;->overridePendingTransition(II)V

    goto/16 :goto_0

    :sswitch_6
    const/4 v3, 0x1

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_4
        0x13 -> :sswitch_3
        0x14 -> :sswitch_2
        0x15 -> :sswitch_1
        0x16 -> :sswitch_0
        0x52 -> :sswitch_5
        0xb2 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x7f07020e
        :pswitch_0    # com.konka.tvsettings.R.id.ontime_menu_settime
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x7f070210
        :pswitch_1    # com.konka.tvsettings.R.id.ontime_menu_source
    .end packed-switch
.end method

.method protected onPause()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->pauseMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resumeMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onResume()V

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onStart()V

    const v0, 0x7f040007    # com.konka.tvsettings.R.anim.anim_right_in

    const v1, 0x7f04000a    # com.konka.tvsettings.R.anim.anim_zoom_out

    invoke-virtual {p0, v0, v1}, Lcom/konka/tvsettings/time/OnTimeActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resetMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onUserInteraction()V

    return-void
.end method

.method updateBootChannel()V
    .locals 6

    :try_start_0
    iget-object v1, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TimerManager;->getOnTime()Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget-object v1, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTimerBootType;->EN_TIMER_BOOT_ON_TIMER:Lcom/mstar/android/tvapi/common/vo/EnumTimerBootType;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->setBootMode(Lcom/mstar/android/tvapi/common/vo/EnumTimerBootType;)V

    sget-boolean v1, Lcom/konka/tvsettings/time/OnTimeActivity;->m_isBootSwitchOn:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Once:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->setTimerPeriod(Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;)V

    :goto_0
    iget-object v1, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget-object v2, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->mEnumOnTimeSource:[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    iget-object v3, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->m_iBootChannelIndex:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->setTvSource(Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;)V

    iget-object v1, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    iget-object v2, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    const/4 v3, 0x1

    const/4 v4, 0x1

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/EnumEpgTimerActType;->EN_EPGTIMER_ACT_NONE:Lcom/mstar/android/tvapi/common/vo/EnumEpgTimerActType;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/mstar/android/tvapi/common/TimerManager;->setOnTime(Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;ZZLcom/mstar/android/tvapi/common/vo/EnumEpgTimerActType;)V

    :goto_1
    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Off:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->setTimerPeriod(Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method updateBootSwitch()V
    .locals 9

    :try_start_0
    iget-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TimerManager;->getOnTime()Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    sget-boolean v0, Lcom/konka/tvsettings/time/OnTimeActivity;->m_isBootSwitchOn:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Once:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->setTimerPeriod(Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;)V

    :goto_0
    new-instance v7, Landroid/text/format/Time;

    invoke-direct {v7}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v7}, Landroid/text/format/Time;->setToNow()V

    iget-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget-object v1, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget v1, v1, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->second:I

    iget-object v2, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget v2, v2, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->minute:I

    iget-object v3, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget v3, v3, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->hour:I

    iget v4, v7, Landroid/text/format/Time;->monthDay:I

    iget v5, v7, Landroid/text/format/Time;->month:I

    add-int/lit8 v5, v5, 0x1

    iget v6, v7, Landroid/text/format/Time;->year:I

    invoke-virtual/range {v0 .. v6}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->set(IIIIII)V

    iget-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    iget-object v1, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    const/4 v2, 0x1

    const/4 v3, 0x0

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumEpgTimerActType;->EN_EPGTIMER_ACT_NONE:Lcom/mstar/android/tvapi/common/vo/EnumEpgTimerActType;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/mstar/android/tvapi/common/TimerManager;->setOnTime(Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;ZZLcom/mstar/android/tvapi/common/vo/EnumEpgTimerActType;)V

    :goto_1
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Off:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->setTimerPeriod(Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method updateBootTime(II)V
    .locals 9
    .param p1    # I
    .param p2    # I

    :try_start_0
    iget-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TimerManager;->getOnTime()Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    new-instance v7, Landroid/text/format/Time;

    invoke-direct {v7}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v7}, Landroid/text/format/Time;->setToNow()V

    iget-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget-object v1, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget v1, v1, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->second:I

    iget v4, v7, Landroid/text/format/Time;->monthDay:I

    iget v2, v7, Landroid/text/format/Time;->month:I

    add-int/lit8 v5, v2, 0x1

    iget v6, v7, Landroid/text/format/Time;->year:I

    move v2, p2

    move v3, p1

    invoke-virtual/range {v0 .. v6}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->set(IIIIII)V

    iget-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTimerBootType;->EN_TIMER_BOOT_ON_TIMER:Lcom/mstar/android/tvapi/common/vo/EnumTimerBootType;

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->setBootMode(Lcom/mstar/android/tvapi/common/vo/EnumTimerBootType;)V

    sget-boolean v0, Lcom/konka/tvsettings/time/OnTimeActivity;->m_isBootSwitchOn:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Once:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->setTimerPeriod(Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;)V

    :goto_0
    iget-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    iget-object v1, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->mEnumOnTimeSource:[Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;

    iget-object v2, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->m_iBootChannelIndex:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->setTvSource(Lcom/mstar/android/tvapi/common/vo/EnumTimeOnTimerSource;)V

    iget-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    iget-object v1, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    const/4 v2, 0x1

    const/4 v3, 0x1

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumEpgTimerActType;->EN_EPGTIMER_ACT_NONE:Lcom/mstar/android/tvapi/common/vo/EnumEpgTimerActType;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/mstar/android/tvapi/common/TimerManager;->setOnTime(Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;ZZLcom/mstar/android/tvapi/common/vo/EnumEpgTimerActType;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "update_PowerOn_Time: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    invoke-direct {p0, v1}, Lcom/konka/tvsettings/time/OnTimeActivity;->timeToString(Landroid/text/format/Time;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/konka/tvsettings/time/OnTimeActivity;->debug(Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/time/OnTimeActivity;->timerOn:Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Off:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOn;->setTimerPeriod(Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method
