.class Lcom/konka/tvsettings/time/TimeClockActivity$2;
.super Ljava/lang/Object;
.source "TimeClockActivity.java"

# interfaces
.implements Landroid/app/DatePickerDialog$OnDateSetListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/time/TimeClockActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/time/TimeClockActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/time/TimeClockActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/time/TimeClockActivity$2;->this$0:Lcom/konka/tvsettings/time/TimeClockActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDateSet(Landroid/widget/DatePicker;III)V
    .locals 2
    .param p1    # Landroid/widget/DatePicker;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity$2;->this$0:Lcom/konka/tvsettings/time/TimeClockActivity;

    # invokes: Lcom/konka/tvsettings/time/TimeClockActivity;->setDate(III)V
    invoke-static {v0, p2, p3, p4}, Lcom/konka/tvsettings/time/TimeClockActivity;->access$0(Lcom/konka/tvsettings/time/TimeClockActivity;III)V

    iget-object v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity$2;->this$0:Lcom/konka/tvsettings/time/TimeClockActivity;

    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeClockActivity$2;->this$0:Lcom/konka/tvsettings/time/TimeClockActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/time/TimeClockActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/time/TimeClockActivity;->updateTimeAndDateDisplay(Landroid/content/Context;)V

    return-void
.end method
