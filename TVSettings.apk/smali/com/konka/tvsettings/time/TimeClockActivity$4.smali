.class Lcom/konka/tvsettings/time/TimeClockActivity$4;
.super Lcom/konka/tvsettings/picture/PictureSettingItem1;
.source "TimeClockActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/time/TimeClockActivity;->addItemAutoMode()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/time/TimeClockActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/time/TimeClockActivity;Landroid/app/Activity;III)V
    .locals 0
    .param p2    # Landroid/app/Activity;
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iput-object p1, p0, Lcom/konka/tvsettings/time/TimeClockActivity$4;->this$0:Lcom/konka/tvsettings/time/TimeClockActivity;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/konka/tvsettings/picture/PictureSettingItem1;-><init>(Landroid/app/Activity;III)V

    return-void
.end method


# virtual methods
.method public doUpdate()V
    .locals 4

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeClockActivity$4;->this$0:Lcom/konka/tvsettings/time/TimeClockActivity;

    iget-object v2, p0, Lcom/konka/tvsettings/time/TimeClockActivity$4;->this$0:Lcom/konka/tvsettings/time/TimeClockActivity;

    # getter for: Lcom/konka/tvsettings/time/TimeClockActivity;->itemAutoMode:Lcom/konka/tvsettings/picture/PictureSettingItem1;
    invoke-static {v2}, Lcom/konka/tvsettings/time/TimeClockActivity;->access$2(Lcom/konka/tvsettings/time/TimeClockActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/tvsettings/picture/PictureSettingItem1;->getCurrentState()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/tvsettings/time/TimeClockActivity;->access$3(Lcom/konka/tvsettings/time/TimeClockActivity;Ljava/lang/Integer;)V

    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeClockActivity$4;->this$0:Lcom/konka/tvsettings/time/TimeClockActivity;

    # getter for: Lcom/konka/tvsettings/time/TimeClockActivity;->settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/time/TimeClockActivity;->access$4(Lcom/konka/tvsettings/time/TimeClockActivity;)Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/tvsettings/time/TimeClockActivity$4;->this$0:Lcom/konka/tvsettings/time/TimeClockActivity;

    # getter for: Lcom/konka/tvsettings/time/TimeClockActivity;->m_iAutoTimeSelectIndex:Ljava/lang/Integer;
    invoke-static {v2}, Lcom/konka/tvsettings/time/TimeClockActivity;->access$5(Lcom/konka/tvsettings/time/TimeClockActivity;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/SettingDesk;->SetSystemAutoTimeType(I)Z

    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeClockActivity$4;->this$0:Lcom/konka/tvsettings/time/TimeClockActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/time/TimeClockActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "auto_time"

    iget-object v3, p0, Lcom/konka/tvsettings/time/TimeClockActivity$4;->this$0:Lcom/konka/tvsettings/time/TimeClockActivity;

    # getter for: Lcom/konka/tvsettings/time/TimeClockActivity;->m_iAutoTimeSelectIndex:Ljava/lang/Integer;
    invoke-static {v3}, Lcom/konka/tvsettings/time/TimeClockActivity;->access$5(Lcom/konka/tvsettings/time/TimeClockActivity;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v3, v0, :cond_0

    :goto_0
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    iget-object v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity$4;->this$0:Lcom/konka/tvsettings/time/TimeClockActivity;

    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeClockActivity$4;->this$0:Lcom/konka/tvsettings/time/TimeClockActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/time/TimeClockActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/time/TimeClockActivity;->updateTimeAndDateDisplay(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity$4;->this$0:Lcom/konka/tvsettings/time/TimeClockActivity;

    # invokes: Lcom/konka/tvsettings/time/TimeClockActivity;->updateAutoTimeUI()V
    invoke-static {v0}, Lcom/konka/tvsettings/time/TimeClockActivity;->access$6(Lcom/konka/tvsettings/time/TimeClockActivity;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
