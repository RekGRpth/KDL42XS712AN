.class Lcom/konka/tvsettings/time/OnTimeActivity$3;
.super Lcom/konka/tvsettings/picture/PictureSettingItem1;
.source "OnTimeActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/time/OnTimeActivity;->addItemOnTimeSwitch()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/time/OnTimeActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/time/OnTimeActivity;Landroid/app/Activity;III)V
    .locals 0
    .param p2    # Landroid/app/Activity;
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iput-object p1, p0, Lcom/konka/tvsettings/time/OnTimeActivity$3;->this$0:Lcom/konka/tvsettings/time/OnTimeActivity;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/konka/tvsettings/picture/PictureSettingItem1;-><init>(Landroid/app/Activity;III)V

    return-void
.end method


# virtual methods
.method public doUpdate()V
    .locals 4

    iget-object v1, p0, Lcom/konka/tvsettings/time/OnTimeActivity$3;->this$0:Lcom/konka/tvsettings/time/OnTimeActivity;

    # getter for: Lcom/konka/tvsettings/time/OnTimeActivity;->itemOnTimeSwitch:Lcom/konka/tvsettings/picture/PictureSettingItem1;
    invoke-static {v1}, Lcom/konka/tvsettings/time/OnTimeActivity;->access$1(Lcom/konka/tvsettings/time/OnTimeActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/tvsettings/picture/PictureSettingItem1;->getCurrentState()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/konka/tvsettings/time/OnTimeActivity;->access$2(Z)V

    :goto_0
    iget-object v1, p0, Lcom/konka/tvsettings/time/OnTimeActivity$3;->this$0:Lcom/konka/tvsettings/time/OnTimeActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/time/OnTimeActivity;->updateBootSwitch()V

    iget-object v1, p0, Lcom/konka/tvsettings/time/OnTimeActivity$3;->this$0:Lcom/konka/tvsettings/time/OnTimeActivity;

    iget-object v2, p0, Lcom/konka/tvsettings/time/OnTimeActivity$3;->this$0:Lcom/konka/tvsettings/time/OnTimeActivity;

    # getter for: Lcom/konka/tvsettings/time/OnTimeActivity;->itemOnTimeSetTime:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/konka/tvsettings/time/OnTimeActivity;->access$3(Lcom/konka/tvsettings/time/OnTimeActivity;)Landroid/widget/LinearLayout;

    move-result-object v2

    # getter for: Lcom/konka/tvsettings/time/OnTimeActivity;->m_isBootSwitchOn:Z
    invoke-static {}, Lcom/konka/tvsettings/time/OnTimeActivity;->access$4()Z

    move-result v3

    # invokes: Lcom/konka/tvsettings/time/OnTimeActivity;->setOptionItemEnabled(Landroid/widget/LinearLayout;Z)V
    invoke-static {v1, v2, v3}, Lcom/konka/tvsettings/time/OnTimeActivity;->access$5(Lcom/konka/tvsettings/time/OnTimeActivity;Landroid/widget/LinearLayout;Z)V

    iget-object v1, p0, Lcom/konka/tvsettings/time/OnTimeActivity$3;->this$0:Lcom/konka/tvsettings/time/OnTimeActivity;

    iget-object v2, p0, Lcom/konka/tvsettings/time/OnTimeActivity$3;->this$0:Lcom/konka/tvsettings/time/OnTimeActivity;

    # getter for: Lcom/konka/tvsettings/time/OnTimeActivity;->itemOnTimeSource:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/konka/tvsettings/time/OnTimeActivity;->access$6(Lcom/konka/tvsettings/time/OnTimeActivity;)Landroid/widget/LinearLayout;

    move-result-object v2

    # getter for: Lcom/konka/tvsettings/time/OnTimeActivity;->m_isBootSwitchOn:Z
    invoke-static {}, Lcom/konka/tvsettings/time/OnTimeActivity;->access$4()Z

    move-result v3

    # invokes: Lcom/konka/tvsettings/time/OnTimeActivity;->setOptionItemEnabled(Landroid/widget/LinearLayout;Z)V
    invoke-static {v1, v2, v3}, Lcom/konka/tvsettings/time/OnTimeActivity;->access$5(Lcom/konka/tvsettings/time/OnTimeActivity;Landroid/widget/LinearLayout;Z)V

    return-void

    :cond_0
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/konka/tvsettings/time/OnTimeActivity;->access$2(Z)V

    goto :goto_0
.end method
