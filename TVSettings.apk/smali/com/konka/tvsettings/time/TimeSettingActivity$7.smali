.class Lcom/konka/tvsettings/time/TimeSettingActivity$7;
.super Lcom/konka/tvsettings/picture/PictureSettingItem1;
.source "TimeSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/time/TimeSettingActivity;->addItemTimeZone()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/time/TimeSettingActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/time/TimeSettingActivity;Landroid/app/Activity;III)V
    .locals 0
    .param p2    # Landroid/app/Activity;
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iput-object p1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity$7;->this$0:Lcom/konka/tvsettings/time/TimeSettingActivity;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/konka/tvsettings/picture/PictureSettingItem1;-><init>(Landroid/app/Activity;III)V

    return-void
.end method


# virtual methods
.method public doUpdate()V
    .locals 4

    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity$7;->this$0:Lcom/konka/tvsettings/time/TimeSettingActivity;

    # getter for: Lcom/konka/tvsettings/time/TimeSettingActivity;->itemTimezone:Lcom/konka/tvsettings/picture/PictureSettingItem1;
    invoke-static {v1}, Lcom/konka/tvsettings/time/TimeSettingActivity;->access$4(Lcom/konka/tvsettings/time/TimeSettingActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/tvsettings/picture/PictureSettingItem1;->getCurrentState()I

    move-result v0

    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity$7;->this$0:Lcom/konka/tvsettings/time/TimeSettingActivity;

    # invokes: Lcom/konka/tvsettings/time/TimeSettingActivity;->setTimeZonebyItemIndex(I)V
    invoke-static {v1, v0}, Lcom/konka/tvsettings/time/TimeSettingActivity;->access$5(Lcom/konka/tvsettings/time/TimeSettingActivity;I)V

    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity$7;->this$0:Lcom/konka/tvsettings/time/TimeSettingActivity;

    # getter for: Lcom/konka/tvsettings/time/TimeSettingActivity;->txtTimeZoneOption:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/tvsettings/time/TimeSettingActivity;->access$6(Lcom/konka/tvsettings/time/TimeSettingActivity;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/tvsettings/time/TimeSettingActivity$7;->this$0:Lcom/konka/tvsettings/time/TimeSettingActivity;

    iget-object v3, p0, Lcom/konka/tvsettings/time/TimeSettingActivity$7;->this$0:Lcom/konka/tvsettings/time/TimeSettingActivity;

    # getter for: Lcom/konka/tvsettings/time/TimeSettingActivity;->timeZoneString:Ljava/lang/String;
    invoke-static {v3}, Lcom/konka/tvsettings/time/TimeSettingActivity;->access$7(Lcom/konka/tvsettings/time/TimeSettingActivity;)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/konka/tvsettings/time/TimeSettingActivity;->getGMTTxt(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/konka/tvsettings/time/TimeSettingActivity;->access$8(Lcom/konka/tvsettings/time/TimeSettingActivity;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
