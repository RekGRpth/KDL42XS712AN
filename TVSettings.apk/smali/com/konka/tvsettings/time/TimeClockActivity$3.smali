.class Lcom/konka/tvsettings/time/TimeClockActivity$3;
.super Ljava/lang/Object;
.source "TimeClockActivity.java"

# interfaces
.implements Landroid/app/TimePickerDialog$OnTimeSetListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/time/TimeClockActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/time/TimeClockActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/time/TimeClockActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/time/TimeClockActivity$3;->this$0:Lcom/konka/tvsettings/time/TimeClockActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTimeSet(Landroid/widget/TimePicker;II)V
    .locals 3
    .param p1    # Landroid/widget/TimePicker;
    .param p2    # I
    .param p3    # I

    const-string v0, "Trace setTVTime"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "in TimeSettingSetListener,Set time to:hourOfDay "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", minute "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity$3;->this$0:Lcom/konka/tvsettings/time/TimeClockActivity;

    # invokes: Lcom/konka/tvsettings/time/TimeClockActivity;->setTime(II)V
    invoke-static {v0, p2, p3}, Lcom/konka/tvsettings/time/TimeClockActivity;->access$1(Lcom/konka/tvsettings/time/TimeClockActivity;II)V

    iget-object v0, p0, Lcom/konka/tvsettings/time/TimeClockActivity$3;->this$0:Lcom/konka/tvsettings/time/TimeClockActivity;

    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeClockActivity$3;->this$0:Lcom/konka/tvsettings/time/TimeClockActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/time/TimeClockActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/time/TimeClockActivity;->updateTimeAndDateDisplay(Landroid/content/Context;)V

    return-void
.end method
