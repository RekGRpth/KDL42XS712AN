.class public Lcom/konka/tvsettings/time/OffTimeActivity;
.super Lcom/konka/tvsettings/BaseKonkaActivity;
.source "OffTimeActivity.java"


# static fields
.field private static m_isHaltSwitchOn:Z


# instance fields
.field private itemHaltTime:Landroid/widget/TextView;

.field private itemOffTimeSetTime:Landroid/widget/LinearLayout;

.field private itemOffTimeSwitch:Lcom/konka/tvsettings/picture/PictureSettingItem1;

.field private myHandler:Landroid/os/Handler;

.field private onHalTimeSetListener:Landroid/app/TimePickerDialog$OnTimeSetListener;

.field private tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

.field private timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/konka/tvsettings/time/OffTimeActivity;->m_isHaltSwitchOn:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->itemOffTimeSwitch:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    iput-object v0, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->itemOffTimeSetTime:Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->itemHaltTime:Landroid/widget/TextView;

    new-instance v0, Lcom/konka/tvsettings/time/OffTimeActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/time/OffTimeActivity$1;-><init>(Lcom/konka/tvsettings/time/OffTimeActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->myHandler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/tvsettings/time/OffTimeActivity$2;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/time/OffTimeActivity$2;-><init>(Lcom/konka/tvsettings/time/OffTimeActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->onHalTimeSetListener:Landroid/app/TimePickerDialog$OnTimeSetListener;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/time/OffTimeActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/time/OffTimeActivity;->updateHaltTimeUI()V

    return-void
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/time/OffTimeActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->itemOffTimeSwitch:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-object v0
.end method

.method static synthetic access$2(Z)V
    .locals 0

    sput-boolean p0, Lcom/konka/tvsettings/time/OffTimeActivity;->m_isHaltSwitchOn:Z

    return-void
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/time/OffTimeActivity;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->itemOffTimeSetTime:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$4()Z
    .locals 1

    sget-boolean v0, Lcom/konka/tvsettings/time/OffTimeActivity;->m_isHaltSwitchOn:Z

    return v0
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/time/OffTimeActivity;Landroid/widget/LinearLayout;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/konka/tvsettings/time/OffTimeActivity;->setOptionItemEnabled(Landroid/widget/LinearLayout;Z)V

    return-void
.end method

.method static synthetic access$6(Lcom/konka/tvsettings/time/OffTimeActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/time/OffTimeActivity;->showDialog()V

    return-void
.end method

.method private addItemOffTimeSetTime()V
    .locals 2

    const v0, 0x7f070209    # com.konka.tvsettings.R.id.offtime_menu_settime

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/time/OffTimeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->itemOffTimeSetTime:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->itemOffTimeSetTime:Landroid/widget/LinearLayout;

    const v1, 0x7f070208    # com.konka.tvsettings.R.id.offtime_menu_offtimeswitch

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusDownId(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->itemOffTimeSetTime:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/tvsettings/time/OffTimeActivity$4;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/time/OffTimeActivity$4;-><init>(Lcom/konka/tvsettings/time/OffTimeActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private addItemOffTimeSwitch()V
    .locals 7

    const/4 v5, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TimerManager;->getOffModeStatus()Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->getTimerPeriod()Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Off:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/tvsettings/time/OffTimeActivity;->m_isHaltSwitchOn:Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->itemOffTimeSetTime:Landroid/widget/LinearLayout;

    sget-boolean v1, Lcom/konka/tvsettings/time/OffTimeActivity;->m_isHaltSwitchOn:Z

    invoke-direct {p0, v0, v1}, Lcom/konka/tvsettings/time/OffTimeActivity;->setOptionItemEnabled(Landroid/widget/LinearLayout;Z)V

    sget-boolean v0, Lcom/konka/tvsettings/time/OffTimeActivity;->m_isHaltSwitchOn:Z

    if-eqz v0, :cond_1

    const/4 v5, 0x1

    :goto_1
    new-instance v0, Lcom/konka/tvsettings/time/OffTimeActivity$3;

    const v3, 0x7f070208    # com.konka.tvsettings.R.id.offtime_menu_offtimeswitch

    const v4, 0x7f0b0026    # com.konka.tvsettings.R.array.str_arr_time_ontimeswitch_vals

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/time/OffTimeActivity$3;-><init>(Lcom/konka/tvsettings/time/OffTimeActivity;Landroid/app/Activity;III)V

    iput-object v0, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->itemOffTimeSwitch:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-void

    :cond_0
    const/4 v0, 0x1

    :try_start_1
    sput-boolean v0, Lcom/konka/tvsettings/time/OffTimeActivity;->m_isHaltSwitchOn:Z
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method private debug(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method private initDateTime()V
    .locals 3

    :try_start_0
    iget-object v1, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TimerManager;->getOffModeStatus()Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "init_PowerOff_Time: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    invoke-direct {p0, v2}, Lcom/konka/tvsettings/time/OffTimeActivity;->timeToString(Landroid/text/format/Time;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/konka/tvsettings/time/OffTimeActivity;->debug(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private setOptionItemEnabled(Landroid/widget/LinearLayout;Z)V
    .locals 0
    .param p1    # Landroid/widget/LinearLayout;
    .param p2    # Z

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setClickable(Z)V

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    return-void
.end method

.method private showDialog()V
    .locals 14

    const/4 v6, 0x1

    new-instance v7, Landroid/text/format/Time;

    invoke-direct {v7}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v7}, Landroid/text/format/Time;->setToNow()V

    invoke-virtual {v7, v6}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    invoke-virtual {v7, v2, v3}, Landroid/text/format/Time;->set(J)V

    const/4 v1, 0x0

    new-instance v1, Landroid/app/TimePickerDialog;

    iget-object v3, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->onHalTimeSetListener:Landroid/app/TimePickerDialog$OnTimeSetListener;

    iget-object v2, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    iget v4, v2, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->hour:I

    iget-object v2, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    iget v5, v2, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->minute:I

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Landroid/app/TimePickerDialog;-><init>(Landroid/content/Context;Landroid/app/TimePickerDialog$OnTimeSetListener;IIZ)V

    new-instance v2, Lcom/konka/tvsettings/time/OffTimeActivity$5;

    invoke-direct {v2, p0}, Lcom/konka/tvsettings/time/OffTimeActivity$5;-><init>(Lcom/konka/tvsettings/time/OffTimeActivity;)V

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    :cond_0
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    const/4 v12, 0x0

    :try_start_0
    const-string v2, "mTimePicker"

    invoke-virtual {v8, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v10

    const/4 v2, 0x1

    invoke-virtual {v10, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    invoke-virtual {v10, v1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Landroid/widget/TimePicker;

    move-object v12, v0
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    if-eqz v12, :cond_1

    invoke-virtual {v12}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v11

    const/4 v13, 0x0

    :try_start_1
    const-string v2, "mHourSpinner"

    invoke-virtual {v11, v2}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v10

    const/4 v2, 0x1

    invoke-virtual {v10, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    invoke-virtual {v10, v12}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v13

    :goto_1
    instance-of v2, v13, Landroid/widget/NumberPicker;

    if-eqz v2, :cond_1

    move-object v2, v13

    check-cast v2, Landroid/widget/NumberPicker;

    invoke-virtual {v2}, Landroid/widget/NumberPicker;->getChildCount()I

    move-result v2

    if-lez v2, :cond_1

    check-cast v13, Landroid/widget/NumberPicker;

    const/4 v2, 0x0

    invoke-virtual {v13, v2}, Landroid/widget/NumberPicker;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    :cond_1
    return-void

    :catch_0
    move-exception v9

    invoke-virtual {v9}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v9

    invoke-virtual {v9}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception v9

    invoke-virtual {v9}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception v9

    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private timeToString(Landroid/text/format/Time;)Ljava/lang/String;
    .locals 2
    .param p1    # Landroid/text/format/Time;

    new-instance v0, Ljava/lang/StringBuilder;

    iget v1, p1, Landroid/text/format/Time;->year:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/text/format/Time;->month:I

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/time/OffTimeActivity;->formatTimeField(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/time/OffTimeActivity;->formatTimeField(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/text/format/Time;->hour:I

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/time/OffTimeActivity;->formatTimeField(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/text/format/Time;->minute:I

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/time/OffTimeActivity;->formatTimeField(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/text/format/Time;->second:I

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/time/OffTimeActivity;->formatTimeField(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private updateHaltTimeUI()V
    .locals 3

    const v0, 0x7f07020a    # com.konka.tvsettings.R.id.offtime_menu_settime_option

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/time/OffTimeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->itemHaltTime:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->itemHaltTime:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    iget v2, v2, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->hour:I

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/time/OffTimeActivity;->formatTimeField(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    iget v2, v2, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->minute:I

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/time/OffTimeActivity;->formatTimeField(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public addView()V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/time/OffTimeActivity;->addItemOffTimeSetTime()V

    invoke-direct {p0}, Lcom/konka/tvsettings/time/OffTimeActivity;->addItemOffTimeSwitch()V

    return-void
.end method

.method public formatTimeField(I)Ljava/lang/String;
    .locals 3
    .param p1    # I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "0"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    :cond_0
    invoke-super {p0, p1}, Lcom/konka/tvsettings/BaseKonkaActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030052    # com.konka.tvsettings.R.layout.time_offtime

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/time/OffTimeActivity;->setContentView(I)V

    invoke-direct {p0}, Lcom/konka/tvsettings/time/OffTimeActivity;->initDateTime()V

    invoke-direct {p0}, Lcom/konka/tvsettings/time/OffTimeActivity;->updateHaltTimeUI()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/time/OffTimeActivity;->addView()V

    iget-object v0, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->myHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/konka/tvsettings/common/LittleDownTimer;->setHandler(Landroid/os/Handler;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const v4, 0x7f040005    # com.konka.tvsettings.R.anim.anim_menu_exit

    const/4 v3, 0x0

    const-string v2, "onKeyDown"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    sparse-switch p1, :sswitch_data_0

    :goto_0
    invoke-super {p0, p1, p2}, Lcom/konka/tvsettings/BaseKonkaActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    :goto_1
    return v2

    :sswitch_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/time/OffTimeActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/time/OffTimeActivity;->finish()V

    invoke-virtual {p0, v3, v4}, Lcom/konka/tvsettings/time/OffTimeActivity;->overridePendingTransition(II)V

    goto :goto_0

    :sswitch_1
    const-string v2, "Exit"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v2, Lcom/konka/tvsettings/time/TimeSettingActivity;

    invoke-virtual {v0, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/time/OffTimeActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/time/OffTimeActivity;->finish()V

    invoke-virtual {p0, v3, v4}, Lcom/konka/tvsettings/time/OffTimeActivity;->overridePendingTransition(II)V

    goto :goto_0

    :sswitch_2
    const/4 v2, 0x1

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x52 -> :sswitch_1
        0xb2 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->pauseMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resumeMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onResume()V

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onStart()V

    const v0, 0x7f040007    # com.konka.tvsettings.R.anim.anim_right_in

    const v1, 0x7f04000a    # com.konka.tvsettings.R.anim.anim_zoom_out

    invoke-virtual {p0, v0, v1}, Lcom/konka/tvsettings/time/OffTimeActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resetMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onUserInteraction()V

    return-void
.end method

.method updateHaltSwitch()V
    .locals 9

    :try_start_0
    iget-object v0, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TimerManager;->getOffModeStatus()Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    sget-boolean v0, Lcom/konka/tvsettings/time/OffTimeActivity;->m_isHaltSwitchOn:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Once:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->setTimerPeriod(Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;)V

    :goto_0
    new-instance v7, Landroid/text/format/Time;

    invoke-direct {v7}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v7}, Landroid/text/format/Time;->setToNow()V

    iget-object v0, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    iget-object v1, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    iget v1, v1, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->second:I

    iget-object v2, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    iget v2, v2, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->minute:I

    iget-object v3, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    iget v3, v3, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->hour:I

    iget v4, v7, Landroid/text/format/Time;->monthDay:I

    iget v5, v7, Landroid/text/format/Time;->month:I

    add-int/lit8 v5, v5, 0x1

    iget v6, v7, Landroid/text/format/Time;->year:I

    invoke-virtual/range {v0 .. v6}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->set(IIIIII)V

    iget-object v0, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    iget-object v1, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/mstar/android/tvapi/common/TimerManager;->setOffModeStatus(Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;Z)V

    :goto_1
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Off:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->setTimerPeriod(Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method updateHaltTime(II)V
    .locals 9
    .param p1    # I
    .param p2    # I

    :try_start_0
    iget-object v0, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;->OFF_MODE_TIMER:Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/TimerManager;->disablePowerOffMode(Lcom/mstar/android/tvapi/common/vo/EnumOffTimerMode;)V

    iget-object v0, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TimerManager;->getOffModeStatus()Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    new-instance v7, Landroid/text/format/Time;

    invoke-direct {v7}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v7}, Landroid/text/format/Time;->setToNow()V

    iget-object v0, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    iget-object v1, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    iget v1, v1, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->second:I

    iget v4, v7, Landroid/text/format/Time;->monthDay:I

    iget v2, v7, Landroid/text/format/Time;->month:I

    add-int/lit8 v5, v2, 0x1

    iget v6, v7, Landroid/text/format/Time;->year:I

    move v2, p2

    move v3, p1

    invoke-virtual/range {v0 .. v6}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->set(IIIIII)V

    iget-object v0, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;->EN_Timer_Once:Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;->setTimerPeriod(Lcom/mstar/android/tvapi/common/vo/EnumTimerPeriod;)V

    iget-object v0, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->tMgr:Lcom/mstar/android/tvapi/common/TimerManager;

    iget-object v1, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/mstar/android/tvapi/common/TimerManager;->setOffModeStatus(Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;Z)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "update_PowerOff_Time: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/tvsettings/time/OffTimeActivity;->timerOff:Lcom/mstar/android/tvapi/common/vo/TimerPowerOffModeStatus;

    invoke-direct {p0, v1}, Lcom/konka/tvsettings/time/OffTimeActivity;->timeToString(Landroid/text/format/Time;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/konka/tvsettings/time/OffTimeActivity;->debug(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method
