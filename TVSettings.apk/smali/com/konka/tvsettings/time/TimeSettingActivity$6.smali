.class Lcom/konka/tvsettings/time/TimeSettingActivity$6;
.super Lcom/konka/tvsettings/picture/PictureSettingItem1;
.source "TimeSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/time/TimeSettingActivity;->addItemSleepTimer()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/time/TimeSettingActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/time/TimeSettingActivity;Landroid/app/Activity;III)V
    .locals 0
    .param p2    # Landroid/app/Activity;
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iput-object p1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity$6;->this$0:Lcom/konka/tvsettings/time/TimeSettingActivity;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/konka/tvsettings/picture/PictureSettingItem1;-><init>(Landroid/app/Activity;III)V

    return-void
.end method


# virtual methods
.method public doUpdate()V
    .locals 3

    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity$6;->this$0:Lcom/konka/tvsettings/time/TimeSettingActivity;

    # getter for: Lcom/konka/tvsettings/time/TimeSettingActivity;->itemSleepTimer:Lcom/konka/tvsettings/picture/PictureSettingItem1;
    invoke-static {v1}, Lcom/konka/tvsettings/time/TimeSettingActivity;->access$2(Lcom/konka/tvsettings/time/TimeSettingActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/tvsettings/picture/PictureSettingItem1;->getCurrentState()I

    move-result v0

    iget-object v1, p0, Lcom/konka/tvsettings/time/TimeSettingActivity$6;->this$0:Lcom/konka/tvsettings/time/TimeSettingActivity;

    # getter for: Lcom/konka/tvsettings/time/TimeSettingActivity;->tvTimerManager:Lcom/mstar/android/tv/TvTimerManager;
    invoke-static {v1}, Lcom/konka/tvsettings/time/TimeSettingActivity;->access$3(Lcom/konka/tvsettings/time/TimeSettingActivity;)Lcom/mstar/android/tv/TvTimerManager;

    move-result-object v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;->values()[Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;

    move-result-object v2

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Lcom/mstar/android/tv/TvTimerManager;->setSleepMode(Lcom/mstar/android/tvapi/common/vo/EnumSleepTimeState;)Z

    return-void
.end method
