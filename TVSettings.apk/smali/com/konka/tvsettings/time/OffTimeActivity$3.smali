.class Lcom/konka/tvsettings/time/OffTimeActivity$3;
.super Lcom/konka/tvsettings/picture/PictureSettingItem1;
.source "OffTimeActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/time/OffTimeActivity;->addItemOffTimeSwitch()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/time/OffTimeActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/time/OffTimeActivity;Landroid/app/Activity;III)V
    .locals 0
    .param p2    # Landroid/app/Activity;
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iput-object p1, p0, Lcom/konka/tvsettings/time/OffTimeActivity$3;->this$0:Lcom/konka/tvsettings/time/OffTimeActivity;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/konka/tvsettings/picture/PictureSettingItem1;-><init>(Landroid/app/Activity;III)V

    return-void
.end method


# virtual methods
.method public doUpdate()V
    .locals 4

    iget-object v1, p0, Lcom/konka/tvsettings/time/OffTimeActivity$3;->this$0:Lcom/konka/tvsettings/time/OffTimeActivity;

    # getter for: Lcom/konka/tvsettings/time/OffTimeActivity;->itemOffTimeSwitch:Lcom/konka/tvsettings/picture/PictureSettingItem1;
    invoke-static {v1}, Lcom/konka/tvsettings/time/OffTimeActivity;->access$1(Lcom/konka/tvsettings/time/OffTimeActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/tvsettings/picture/PictureSettingItem1;->getCurrentState()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/konka/tvsettings/time/OffTimeActivity;->access$2(Z)V

    :goto_0
    iget-object v1, p0, Lcom/konka/tvsettings/time/OffTimeActivity$3;->this$0:Lcom/konka/tvsettings/time/OffTimeActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/time/OffTimeActivity;->updateHaltSwitch()V

    iget-object v1, p0, Lcom/konka/tvsettings/time/OffTimeActivity$3;->this$0:Lcom/konka/tvsettings/time/OffTimeActivity;

    iget-object v2, p0, Lcom/konka/tvsettings/time/OffTimeActivity$3;->this$0:Lcom/konka/tvsettings/time/OffTimeActivity;

    # getter for: Lcom/konka/tvsettings/time/OffTimeActivity;->itemOffTimeSetTime:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/konka/tvsettings/time/OffTimeActivity;->access$3(Lcom/konka/tvsettings/time/OffTimeActivity;)Landroid/widget/LinearLayout;

    move-result-object v2

    # getter for: Lcom/konka/tvsettings/time/OffTimeActivity;->m_isHaltSwitchOn:Z
    invoke-static {}, Lcom/konka/tvsettings/time/OffTimeActivity;->access$4()Z

    move-result v3

    # invokes: Lcom/konka/tvsettings/time/OffTimeActivity;->setOptionItemEnabled(Landroid/widget/LinearLayout;Z)V
    invoke-static {v1, v2, v3}, Lcom/konka/tvsettings/time/OffTimeActivity;->access$5(Lcom/konka/tvsettings/time/OffTimeActivity;Landroid/widget/LinearLayout;Z)V

    return-void

    :cond_0
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/konka/tvsettings/time/OffTimeActivity;->access$2(Z)V

    goto :goto_0
.end method
