.class public Lcom/konka/tvsettings/view/S3dItem1;
.super Landroid/widget/RelativeLayout;
.source "S3dItem1.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/view/S3dItem1$ItemOnChangeEvent;
    }
.end annotation


# static fields
.field private static final NAMESPACE:Ljava/lang/String; = "http://schemas.android.com/apk/res/android"


# instance fields
.field private mCurrState:I

.field private final mItemContainer:Landroid/widget/RelativeLayout;

.field private mItemName:Landroid/widget/TextView;

.field private mItemTag:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object p0, p0, Lcom/konka/tvsettings/view/S3dItem1;->mItemContainer:Landroid/widget/RelativeLayout;

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/tvsettings/view/S3dItem1;->mCurrState:I

    invoke-direct {p0, p2}, Lcom/konka/tvsettings/view/S3dItem1;->init(I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object p0, p0, Lcom/konka/tvsettings/view/S3dItem1;->mItemContainer:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    iput v1, p0, Lcom/konka/tvsettings/view/S3dItem1;->mCurrState:I

    const-string v1, "http://schemas.android.com/apk/res/android"

    const-string v2, "text"

    const v3, 0x7f0a00a0    # com.konka.tvsettings.R.string.str_pic_setting_pic_mod

    invoke-interface {p2, v1, v2, v3}, Landroid/util/AttributeSet;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/konka/tvsettings/view/S3dItem1;->init(I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object p0, p0, Lcom/konka/tvsettings/view/S3dItem1;->mItemContainer:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    iput v1, p0, Lcom/konka/tvsettings/view/S3dItem1;->mCurrState:I

    const-string v1, "http://schemas.android.com/apk/res/android"

    const-string v2, "text"

    const v3, 0x7f0a00a0    # com.konka.tvsettings.R.string.str_pic_setting_pic_mod

    invoke-interface {p2, v1, v2, v3}, Landroid/util/AttributeSet;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/konka/tvsettings/view/S3dItem1;->init(I)V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/view/S3dItem1;)Landroid/widget/RelativeLayout;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/view/S3dItem1;->mItemContainer:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/view/S3dItem1;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/view/S3dItem1;->mItemName:Landroid/widget/TextView;

    return-object v0
.end method

.method private init(I)V
    .locals 2
    .param p1    # I

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/view/S3dItem1;->setClickable(Z)V

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/view/S3dItem1;->setFocusable(Z)V

    new-instance v0, Lcom/konka/tvsettings/view/S3dItem1$ItemOnChangeEvent;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/konka/tvsettings/view/S3dItem1$ItemOnChangeEvent;-><init>(Lcom/konka/tvsettings/view/S3dItem1;Lcom/konka/tvsettings/view/S3dItem1$ItemOnChangeEvent;)V

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/view/S3dItem1;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/view/S3dItem1;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030046    # com.konka.tvsettings.R.layout.s3d_item1

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    iget-object v0, p0, Lcom/konka/tvsettings/view/S3dItem1;->mItemContainer:Landroid/widget/RelativeLayout;

    const v1, 0x7f0701cb    # com.konka.tvsettings.R.id.s3d_item1_name

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/view/S3dItem1;->mItemName:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/view/S3dItem1;->mItemContainer:Landroid/widget/RelativeLayout;

    const v1, 0x7f0701cc    # com.konka.tvsettings.R.id.s3d_item1_tag

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/konka/tvsettings/view/S3dItem1;->mItemTag:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/konka/tvsettings/view/S3dItem1;->mItemName:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method


# virtual methods
.method public getStatus()I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/view/S3dItem1;->mCurrState:I

    return v0
.end method

.method public setStatus(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/tvsettings/view/S3dItem1;->mCurrState:I

    return-void
.end method

.method public setStatusFbd()V
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/view/S3dItem1;->setFocusable(Z)V

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/view/S3dItem1;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/S3dItem1;->mItemTag:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/S3dItem1;->mItemName:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/view/S3dItem1;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080003    # com.konka.tvsettings.R.color.text_forbidden_col

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public setStatusNor()V
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/view/S3dItem1;->setFocusable(Z)V

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/view/S3dItem1;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/S3dItem1;->mItemTag:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/S3dItem1;->mItemName:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/view/S3dItem1;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080002    # com.konka.tvsettings.R.color.text_normal_col

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public setStatusRun()V
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/view/S3dItem1;->setFocusable(Z)V

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/view/S3dItem1;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/S3dItem1;->mItemTag:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/S3dItem1;->mItemName:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/view/S3dItem1;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080003    # com.konka.tvsettings.R.color.text_forbidden_col

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public setStatusSel()V
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/view/S3dItem1;->setFocusable(Z)V

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/view/S3dItem1;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/S3dItem1;->mItemTag:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/S3dItem1;->mItemName:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/view/S3dItem1;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080001    # com.konka.tvsettings.R.color.text_select_col

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method
