.class public Lcom/konka/tvsettings/view/PictureSettingItem1;
.super Landroid/widget/LinearLayout;
.source "PictureSettingItem1.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/view/View$OnKeyListener;


# static fields
.field private static final NAMESPACE:Ljava/lang/String; = "http://schemas.android.com/apk/res/android"


# instance fields
.field private STATE_COUNT:I

.field private callback:Lcom/konka/tvsettings/view/IUpdateSysData;

.field private mArrowsLeft:Landroid/widget/ImageView;

.field private mArrowsRight:Landroid/widget/ImageView;

.field private mCurrentState:I

.field private mItemContainer:Landroid/widget/LinearLayout;

.field public mItemName:Landroid/widget/TextView;

.field public mItemValue:Landroid/widget/TextView;

.field private mPsi1:Landroid/widget/LinearLayout;

.field private mStates:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # I

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object p0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemName:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mArrowsLeft:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemValue:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mArrowsRight:Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mStates:[Ljava/lang/String;

    iput v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mCurrentState:I

    iput v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->STATE_COUNT:I

    iput-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->callback:Lcom/konka/tvsettings/view/IUpdateSysData;

    const-string v0, "Self define Component Contruction function with three params"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-direct {p0, p2}, Lcom/konka/tvsettings/view/PictureSettingItem1;->init(I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object p0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemName:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mArrowsLeft:Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemValue:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mArrowsRight:Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mStates:[Ljava/lang/String;

    iput v2, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mCurrentState:I

    iput v2, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->STATE_COUNT:I

    iput-object v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->callback:Lcom/konka/tvsettings/view/IUpdateSysData;

    const-string v1, "http://schemas.android.com/apk/res/android"

    const-string v2, "text"

    const v3, 0x7f0a00a0    # com.konka.tvsettings.R.string.str_pic_setting_pic_mod

    invoke-interface {p2, v1, v2, v3}, Landroid/util/AttributeSet;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    const-string v1, "Self define Component Contruction function with three params"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/konka/tvsettings/view/PictureSettingItem1;->init(I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object p0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemName:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mArrowsLeft:Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemValue:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mArrowsRight:Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mStates:[Ljava/lang/String;

    iput v2, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mCurrentState:I

    iput v2, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->STATE_COUNT:I

    iput-object v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->callback:Lcom/konka/tvsettings/view/IUpdateSysData;

    const-string v1, "http://schemas.android.com/apk/res/android"

    const-string v2, "text"

    const v3, 0x7f0a00a0    # com.konka.tvsettings.R.string.str_pic_setting_pic_mod

    invoke-interface {p2, v1, v2, v3}, Landroid/util/AttributeSet;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    const-string v1, "Self define Component Contruction function with three params"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/konka/tvsettings/view/PictureSettingItem1;->init(I)V

    return-void
.end method

.method private init(I)V
    .locals 2
    .param p1    # I

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/view/PictureSettingItem1;->setFocusable(Z)V

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/view/PictureSettingItem1;->setFocusableInTouchMode(Z)V

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/view/PictureSettingItem1;->setClickable(Z)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/view/PictureSettingItem1;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030027    # com.konka.tvsettings.R.layout.picture_setting_item1

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    const v1, 0x7f0700ff    # com.konka.tvsettings.R.id.picture_setting_item1_name

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemName:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    const v1, 0x7f070100    # com.konka.tvsettings.R.id.picture_setting_item1_left_arrow

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mArrowsLeft:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    const v1, 0x7f070101    # com.konka.tvsettings.R.id.picture_setting_item1_value

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemValue:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    const v1, 0x7f070102    # com.konka.tvsettings.R.id.picture_setting_item1_right_arrow

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mArrowsRight:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemName:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method private initArrowsBg(I)V
    .locals 3
    .param p1    # I

    const v2, 0x7f020004    # com.konka.tvsettings.R.drawable.arrows_sel_right

    const v1, 0x7f020003    # com.konka.tvsettings.R.drawable.arrows_sel_left

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mArrowsLeft:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mArrowsRight:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->STATE_COUNT:I

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mArrowsLeft:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mArrowsRight:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mArrowsLeft:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mArrowsRight:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method


# virtual methods
.method public getCurrentState()I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mCurrentState:I

    return v0
.end method

.method public initData(IILcom/konka/tvsettings/view/IUpdateSysData;Z)V
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # Lcom/konka/tvsettings/view/IUpdateSysData;
    .param p4    # Z

    const/high16 v5, 0x3f000000    # 0.5f

    if-eqz p4, :cond_0

    invoke-virtual {p0}, Lcom/konka/tvsettings/view/PictureSettingItem1;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v1, v3, Landroid/util/DisplayMetrics;->density:F

    const v3, 0x7f0700fe    # com.konka.tvsettings.R.id.picture_setting_item1

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/view/PictureSettingItem1;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mPsi1:Landroid/widget/LinearLayout;

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v3, 0x43b40000    # 360.0f

    mul-float/2addr v3, v1

    add-float/2addr v3, v5

    float-to-int v3, v3

    const/high16 v4, 0x42200000    # 40.0f

    mul-float/2addr v4, v1

    add-float/2addr v4, v5

    float-to-int v4, v4

    invoke-direct {v0, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v3, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mPsi1:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v3, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mPsi1:Landroid/widget/LinearLayout;

    const/16 v4, 0x11

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setGravity(I)V

    :cond_0
    iput-object p3, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->callback:Lcom/konka/tvsettings/view/IUpdateSysData;

    invoke-virtual {p0}, Lcom/konka/tvsettings/view/PictureSettingItem1;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    iput v3, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->STATE_COUNT:I

    iput-object v2, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mStates:[Ljava/lang/String;

    iget v3, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->STATE_COUNT:I

    if-lt p2, v3, :cond_1

    iget v3, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->STATE_COUNT:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mCurrentState:I

    :goto_0
    iget-object v3, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemValue:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mStates:[Ljava/lang/String;

    iget v5, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mCurrentState:I

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v3, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mCurrentState:I

    invoke-direct {p0, v3}, Lcom/konka/tvsettings/view/PictureSettingItem1;->initArrowsBg(I)V

    invoke-virtual {p0, p0}, Lcom/konka/tvsettings/view/PictureSettingItem1;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v3, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mArrowsLeft:Landroid/widget/ImageView;

    invoke-virtual {v3, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mArrowsRight:Landroid/widget/ImageView;

    invoke-virtual {v3, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, p0}, Lcom/konka/tvsettings/view/PictureSettingItem1;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void

    :cond_1
    if-gez p2, :cond_2

    const/4 v3, 0x0

    iput v3, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mCurrentState:I

    goto :goto_0

    :cond_2
    iput p2, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mCurrentState:I

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mArrowsLeft:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getId()I

    move-result v1

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/konka/tvsettings/view/PictureSettingItem1;->stateLeft()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mArrowsRight:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getId()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/konka/tvsettings/view/PictureSettingItem1;->stateRight()V

    goto :goto_0
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 5
    .param p1    # Landroid/view/View;
    .param p2    # Z

    const v4, 0x7f080002    # com.konka.tvsettings.R.color.text_normal_col

    const v3, 0x7f080001    # com.konka.tvsettings.R.color.text_select_col

    const/4 v1, 0x4

    const/4 v2, 0x0

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mArrowsLeft:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mArrowsRight:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemName:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/view/PictureSettingItem1;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemValue:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/view/PictureSettingItem1;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setSelected(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mArrowsLeft:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mArrowsRight:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemName:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/view/PictureSettingItem1;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemValue:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/view/PictureSettingItem1;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setSelected(Z)V

    goto :goto_0
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onKeyDown: keycode: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ;event: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getId()I

    move-result v1

    if-ne v0, v1, :cond_1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x15

    if-ne v0, p2, :cond_0

    invoke-virtual {p0}, Lcom/konka/tvsettings/view/PictureSettingItem1;->stateLeft()V

    :cond_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0x16

    if-ne v0, p2, :cond_1

    invoke-virtual {p0}, Lcom/konka/tvsettings/view/PictureSettingItem1;->stateRight()V

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public setCurrentState(I)V
    .locals 3
    .param p1    # I

    iput p1, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mCurrentState:I

    iget v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mCurrentState:I

    iget v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->STATE_COUNT:I

    add-int/lit8 v1, v1, -0x1

    if-gt v0, v1, :cond_0

    iget v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mCurrentState:I

    if-gez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mCurrentState:I

    :cond_1
    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemValue:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mStates:[Ljava/lang/String;

    iget v2, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mCurrentState:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mCurrentState:I

    invoke-direct {p0, v0}, Lcom/konka/tvsettings/view/PictureSettingItem1;->initArrowsBg(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->callback:Lcom/konka/tvsettings/view/IUpdateSysData;

    invoke-interface {v0}, Lcom/konka/tvsettings/view/IUpdateSysData;->doUpdate()V

    return-void
.end method

.method public setStatusFbd()V
    .locals 3

    const v2, 0x7f080003    # com.konka.tvsettings.R.color.text_forbidden_col

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mArrowsLeft:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mArrowsRight:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemName:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/view/PictureSettingItem1;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemValue:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/view/PictureSettingItem1;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public setStatusNor()V
    .locals 3

    const v2, 0x7f080002    # com.konka.tvsettings.R.color.text_normal_col

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mArrowsLeft:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mArrowsRight:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemName:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/view/PictureSettingItem1;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemValue:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/view/PictureSettingItem1;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public stateLeft()V
    .locals 3

    iget v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mCurrentState:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mCurrentState:I

    iget v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mCurrentState:I

    if-gez v0, :cond_0

    iget v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->STATE_COUNT:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mCurrentState:I

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemValue:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mStates:[Ljava/lang/String;

    iget v2, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mCurrentState:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mCurrentState:I

    invoke-direct {p0, v0}, Lcom/konka/tvsettings/view/PictureSettingItem1;->initArrowsBg(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->callback:Lcom/konka/tvsettings/view/IUpdateSysData;

    invoke-interface {v0}, Lcom/konka/tvsettings/view/IUpdateSysData;->doUpdate()V

    return-void
.end method

.method public stateRight()V
    .locals 3

    iget v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mCurrentState:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mCurrentState:I

    iget v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mCurrentState:I

    iget v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->STATE_COUNT:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mCurrentState:I

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemValue:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mStates:[Ljava/lang/String;

    iget v2, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mCurrentState:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->mCurrentState:I

    invoke-direct {p0, v0}, Lcom/konka/tvsettings/view/PictureSettingItem1;->initArrowsBg(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/PictureSettingItem1;->callback:Lcom/konka/tvsettings/view/IUpdateSysData;

    invoke-interface {v0}, Lcom/konka/tvsettings/view/IUpdateSysData;->doUpdate()V

    return-void
.end method
