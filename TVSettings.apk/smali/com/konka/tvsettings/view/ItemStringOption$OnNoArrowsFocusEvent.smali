.class public Lcom/konka/tvsettings/view/ItemStringOption$OnNoArrowsFocusEvent;
.super Ljava/lang/Object;
.source "ItemStringOption.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/view/ItemStringOption;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "OnNoArrowsFocusEvent"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/view/ItemStringOption;


# direct methods
.method public constructor <init>(Lcom/konka/tvsettings/view/ItemStringOption;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/view/ItemStringOption$OnNoArrowsFocusEvent;->this$0:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Z

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption$OnNoArrowsFocusEvent;->this$0:Lcom/konka/tvsettings/view/ItemStringOption;

    iget-object v0, v0, Lcom/konka/tvsettings/view/ItemStringOption;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption$OnNoArrowsFocusEvent;->this$0:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v0}, Lcom/konka/tvsettings/view/ItemStringOption;->noArrowsItemGainFocus()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption$OnNoArrowsFocusEvent;->this$0:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v0}, Lcom/konka/tvsettings/view/ItemStringOption;->noArrowsItemLoseFocus()V

    goto :goto_0
.end method
