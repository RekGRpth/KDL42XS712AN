.class Lcom/konka/tvsettings/view/S3dItem1$ItemOnChangeEvent;
.super Ljava/lang/Object;
.source "S3dItem1.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/view/S3dItem1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ItemOnChangeEvent"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/view/S3dItem1;


# direct methods
.method private constructor <init>(Lcom/konka/tvsettings/view/S3dItem1;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/view/S3dItem1$ItemOnChangeEvent;->this$0:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/tvsettings/view/S3dItem1;Lcom/konka/tvsettings/view/S3dItem1$ItemOnChangeEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/view/S3dItem1$ItemOnChangeEvent;-><init>(Lcom/konka/tvsettings/view/S3dItem1;)V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Z

    iget-object v0, p0, Lcom/konka/tvsettings/view/S3dItem1$ItemOnChangeEvent;->this$0:Lcom/konka/tvsettings/view/S3dItem1;

    # getter for: Lcom/konka/tvsettings/view/S3dItem1;->mItemContainer:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/konka/tvsettings/view/S3dItem1;->access$0(Lcom/konka/tvsettings/view/S3dItem1;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/view/S3dItem1$ItemOnChangeEvent;->this$0:Lcom/konka/tvsettings/view/S3dItem1;

    # getter for: Lcom/konka/tvsettings/view/S3dItem1;->mItemName:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/tvsettings/view/S3dItem1;->access$1(Lcom/konka/tvsettings/view/S3dItem1;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/view/S3dItem1$ItemOnChangeEvent;->this$0:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/S3dItem1;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080001    # com.konka.tvsettings.R.color.text_select_col

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/view/S3dItem1$ItemOnChangeEvent;->this$0:Lcom/konka/tvsettings/view/S3dItem1;

    # getter for: Lcom/konka/tvsettings/view/S3dItem1;->mItemName:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/tvsettings/view/S3dItem1;->access$1(Lcom/konka/tvsettings/view/S3dItem1;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/view/S3dItem1$ItemOnChangeEvent;->this$0:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/S3dItem1;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080002    # com.konka.tvsettings.R.color.text_normal_col

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method
