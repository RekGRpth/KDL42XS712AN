.class public Lcom/konka/tvsettings/view/ChooserView;
.super Landroid/widget/LinearLayout;
.source "ChooserView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/view/ChooserView$ChooserAdapter;
    }
.end annotation


# instance fields
.field private adapter:Lcom/konka/tvsettings/view/ChooserView$ChooserAdapter;

.field private chooserListView:Landroid/widget/ListView;

.field private m_iCurIndex:I

.field private m_itemName:[Ljava/lang/String;

.field private mlayout:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;ILandroid/view/View$OnKeyListener;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;
    .param p4    # I
    .param p5    # Landroid/view/View$OnKeyListener;

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object p3, p0, Lcom/konka/tvsettings/view/ChooserView;->m_itemName:[Ljava/lang/String;

    iput p4, p0, Lcom/konka/tvsettings/view/ChooserView;->m_iCurIndex:I

    const-string v2, "layout_inflater"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v2, 0x7f030021    # com.konka.tvsettings.R.layout.osd_language_list

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/tvsettings/view/ChooserView;->mlayout:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/16 v2, 0x190

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    iget-object v2, p0, Lcom/konka/tvsettings/view/ChooserView;->mlayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/konka/tvsettings/view/ChooserView;->mlayout:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/view/ChooserView;->addView(Landroid/view/View;)V

    const v2, 0x7f0700e7    # com.konka.tvsettings.R.id.osd_language_list_title

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/view/ChooserView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v2, 0x7f0700e8    # com.konka.tvsettings.R.id.osd_language_list_view

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/view/ChooserView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lcom/konka/tvsettings/view/ChooserView;->chooserListView:Landroid/widget/ListView;

    new-instance v2, Lcom/konka/tvsettings/view/ChooserView$ChooserAdapter;

    iget v3, p0, Lcom/konka/tvsettings/view/ChooserView;->m_iCurIndex:I

    invoke-direct {v2, p0, p1, v3}, Lcom/konka/tvsettings/view/ChooserView$ChooserAdapter;-><init>(Lcom/konka/tvsettings/view/ChooserView;Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/konka/tvsettings/view/ChooserView;->adapter:Lcom/konka/tvsettings/view/ChooserView$ChooserAdapter;

    iget-object v2, p0, Lcom/konka/tvsettings/view/ChooserView;->chooserListView:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/konka/tvsettings/view/ChooserView;->adapter:Lcom/konka/tvsettings/view/ChooserView$ChooserAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v2, p0, Lcom/konka/tvsettings/view/ChooserView;->chooserListView:Landroid/widget/ListView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setDividerHeight(I)V

    iget-object v2, p0, Lcom/konka/tvsettings/view/ChooserView;->chooserListView:Landroid/widget/ListView;

    iget v3, p0, Lcom/konka/tvsettings/view/ChooserView;->m_iCurIndex:I

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setSelection(I)V

    iget-object v2, p0, Lcom/konka/tvsettings/view/ChooserView;->chooserListView:Landroid/widget/ListView;

    invoke-virtual {v2, p5}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/view/ChooserView;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/view/ChooserView;->m_iCurIndex:I

    return-void
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/view/ChooserView;)[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/view/ChooserView;->m_itemName:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/view/ChooserView;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/view/ChooserView;->m_iCurIndex:I

    return v0
.end method


# virtual methods
.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    const/16 v1, 0x13

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/view/ChooserView;->chooserListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/view/ChooserView;->chooserListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/konka/tvsettings/view/ChooserView;->m_itemName:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setSelection(I)V

    :goto_0
    return v0

    :cond_0
    const/16 v1, 0x14

    if-ne p1, v1, :cond_1

    iget-object v1, p0, Lcom/konka/tvsettings/view/ChooserView;->chooserListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v1

    iget-object v2, p0, Lcom/konka/tvsettings/view/ChooserView;->m_itemName:[Ljava/lang/String;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/konka/tvsettings/view/ChooserView;->chooserListView:Landroid/widget/ListView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method
