.class Lcom/konka/tvsettings/view/ItemStringOption$ItemLeftArrowOnClickEvent;
.super Ljava/lang/Object;
.source "ItemStringOption.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/view/ItemStringOption;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ItemLeftArrowOnClickEvent"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/view/ItemStringOption;


# direct methods
.method private constructor <init>(Lcom/konka/tvsettings/view/ItemStringOption;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/view/ItemStringOption$ItemLeftArrowOnClickEvent;->this$0:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/tvsettings/view/ItemStringOption;Lcom/konka/tvsettings/view/ItemStringOption$ItemLeftArrowOnClickEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/view/ItemStringOption$ItemLeftArrowOnClickEvent;-><init>(Lcom/konka/tvsettings/view/ItemStringOption;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption$ItemLeftArrowOnClickEvent;->this$0:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v0}, Lcom/konka/tvsettings/view/ItemStringOption;->onKeyLeft()V

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption$ItemLeftArrowOnClickEvent;->this$0:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v0}, Lcom/konka/tvsettings/view/ItemStringOption;->doUpdate()V

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption$ItemLeftArrowOnClickEvent;->this$0:Lcom/konka/tvsettings/view/ItemStringOption;

    # invokes: Lcom/konka/tvsettings/view/ItemStringOption;->updateItemState()V
    invoke-static {v0}, Lcom/konka/tvsettings/view/ItemStringOption;->access$4(Lcom/konka/tvsettings/view/ItemStringOption;)V

    return-void
.end method
