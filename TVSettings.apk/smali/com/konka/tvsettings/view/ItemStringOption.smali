.class public Lcom/konka/tvsettings/view/ItemStringOption;
.super Landroid/widget/LinearLayout;
.source "ItemStringOption.java"

# interfaces
.implements Lcom/konka/tvsettings/view/IUpdateSysData;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/view/ItemStringOption$ItemLeftArrowOnClickEvent;,
        Lcom/konka/tvsettings/view/ItemStringOption$ItemOnChangeEvent;,
        Lcom/konka/tvsettings/view/ItemStringOption$ItemOnKeyEvent;,
        Lcom/konka/tvsettings/view/ItemStringOption$ItemRightArrowOnClickEvent;,
        Lcom/konka/tvsettings/view/ItemStringOption$OnNoArrowsFocusEvent;
    }
.end annotation


# instance fields
.field private DOWNT_KEY_CODE:I

.field public final IMG_ITEM_BG:I

.field public final IMG_ITEM_CONTAINER_BG:I

.field public final IMG_LEFT_ARROW_STATE:I

.field public final IMG_RIGHT_ARROW_STATE:I

.field private ITEM_HEIGHT:I

.field protected ITEM_WIDTH:I

.field private LEFT_KEY_CODE:I

.field private MAX_VALUE:I

.field private MIN_VALUE:I

.field private RIGHT_KEY_CODE:I

.field public final TXT_COLOR_FOCUSED:I

.field public final TXT_COLOR_NORMAL:I

.field public final TXT_COLOR_UNFOCUSED:I

.field public final TXT_SIZE_FOCUS:I

.field public TXT_SIZE_UNFOCUS:I

.field private bIsValueANumber:Z

.field protected container:Landroid/widget/LinearLayout;

.field private iValueIndex:I

.field private imgLeftArrow:Landroid/widget/ImageView;

.field private imgRightArrow:Landroid/widget/ImageView;

.field protected isCreateImageView:Z

.field private mContext:Landroid/content/Context;

.field private strItemValues:[Ljava/lang/CharSequence;

.field private txtOptionName:Landroid/widget/TextView;

.field private txtOptionValue:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # I

    const/4 v2, 0x0

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->MAX_VALUE:I

    iput v2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->MIN_VALUE:I

    iput v2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->ITEM_WIDTH:I

    iput v2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->ITEM_HEIGHT:I

    const/16 v0, 0x15

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->LEFT_KEY_CODE:I

    const/16 v0, 0x16

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->RIGHT_KEY_CODE:I

    const/16 v0, 0x14

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->DOWNT_KEY_CODE:I

    const v0, 0x7f020032    # com.konka.tvsettings.R.drawable.com_bg_popmenu2

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->IMG_ITEM_BG:I

    const v0, 0x7f020120    # com.konka.tvsettings.R.drawable.setting_menu_item_state

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->IMG_ITEM_CONTAINER_BG:I

    const v0, 0x7f020070    # com.konka.tvsettings.R.drawable.item_left_arrow_state

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->IMG_LEFT_ARROW_STATE:I

    const v0, 0x7f020071    # com.konka.tvsettings.R.drawable.item_right_arrow_state

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->IMG_RIGHT_ARROW_STATE:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->TXT_COLOR_FOCUSED:I

    const v0, -0x666667

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->TXT_COLOR_NORMAL:I

    const v0, -0x7c7c78

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->TXT_COLOR_UNFOCUSED:I

    const/16 v0, 0x21

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->TXT_SIZE_FOCUS:I

    const/16 v0, 0x1c

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->TXT_SIZE_UNFOCUS:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->isCreateImageView:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09000d    # com.konka.tvsettings.R.dimen.SettingStringOption_textSize

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->TXT_SIZE_UNFOCUS:I

    iput-object p1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->mContext:Landroid/content/Context;

    iput p2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->ITEM_WIDTH:I

    iput p3, p0, Lcom/konka/tvsettings/view/ItemStringOption;->ITEM_HEIGHT:I

    iput v2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->iValueIndex:I

    invoke-virtual {p0}, Lcom/konka/tvsettings/view/ItemStringOption;->initItem()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IIII)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v2, 0x0

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->MAX_VALUE:I

    iput v2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->MIN_VALUE:I

    iput v2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->ITEM_WIDTH:I

    iput v2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->ITEM_HEIGHT:I

    const/16 v0, 0x15

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->LEFT_KEY_CODE:I

    const/16 v0, 0x16

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->RIGHT_KEY_CODE:I

    const/16 v0, 0x14

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->DOWNT_KEY_CODE:I

    const v0, 0x7f020032    # com.konka.tvsettings.R.drawable.com_bg_popmenu2

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->IMG_ITEM_BG:I

    const v0, 0x7f020120    # com.konka.tvsettings.R.drawable.setting_menu_item_state

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->IMG_ITEM_CONTAINER_BG:I

    const v0, 0x7f020070    # com.konka.tvsettings.R.drawable.item_left_arrow_state

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->IMG_LEFT_ARROW_STATE:I

    const v0, 0x7f020071    # com.konka.tvsettings.R.drawable.item_right_arrow_state

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->IMG_RIGHT_ARROW_STATE:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->TXT_COLOR_FOCUSED:I

    const v0, -0x666667

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->TXT_COLOR_NORMAL:I

    const v0, -0x7c7c78

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->TXT_COLOR_UNFOCUSED:I

    const/16 v0, 0x21

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->TXT_SIZE_FOCUS:I

    const/16 v0, 0x1c

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->TXT_SIZE_UNFOCUS:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->isCreateImageView:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09000d    # com.konka.tvsettings.R.dimen.SettingStringOption_textSize

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->TXT_SIZE_UNFOCUS:I

    iput-object p1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->mContext:Landroid/content/Context;

    iput-boolean v2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->isCreateImageView:Z

    iput p4, p0, Lcom/konka/tvsettings/view/ItemStringOption;->ITEM_WIDTH:I

    iput p5, p0, Lcom/konka/tvsettings/view/ItemStringOption;->ITEM_HEIGHT:I

    invoke-virtual {p0}, Lcom/konka/tvsettings/view/ItemStringOption;->initItem()V

    iput-boolean v2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->bIsValueANumber:Z

    invoke-virtual {p0, p2}, Lcom/konka/tvsettings/view/ItemStringOption;->setItemName(I)Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {p0, p3, v2}, Lcom/konka/tvsettings/view/ItemStringOption;->setItemValue(IZ)Lcom/konka/tvsettings/view/ItemStringOption;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IIIII)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I

    const/4 v2, 0x0

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->MAX_VALUE:I

    iput v2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->MIN_VALUE:I

    iput v2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->ITEM_WIDTH:I

    iput v2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->ITEM_HEIGHT:I

    const/16 v0, 0x15

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->LEFT_KEY_CODE:I

    const/16 v0, 0x16

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->RIGHT_KEY_CODE:I

    const/16 v0, 0x14

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->DOWNT_KEY_CODE:I

    const v0, 0x7f020032    # com.konka.tvsettings.R.drawable.com_bg_popmenu2

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->IMG_ITEM_BG:I

    const v0, 0x7f020120    # com.konka.tvsettings.R.drawable.setting_menu_item_state

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->IMG_ITEM_CONTAINER_BG:I

    const v0, 0x7f020070    # com.konka.tvsettings.R.drawable.item_left_arrow_state

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->IMG_LEFT_ARROW_STATE:I

    const v0, 0x7f020071    # com.konka.tvsettings.R.drawable.item_right_arrow_state

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->IMG_RIGHT_ARROW_STATE:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->TXT_COLOR_FOCUSED:I

    const v0, -0x666667

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->TXT_COLOR_NORMAL:I

    const v0, -0x7c7c78

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->TXT_COLOR_UNFOCUSED:I

    const/16 v0, 0x21

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->TXT_SIZE_FOCUS:I

    const/16 v0, 0x1c

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->TXT_SIZE_UNFOCUS:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->isCreateImageView:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09000d    # com.konka.tvsettings.R.dimen.SettingStringOption_textSize

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->TXT_SIZE_UNFOCUS:I

    iput-object p1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->mContext:Landroid/content/Context;

    iput p5, p0, Lcom/konka/tvsettings/view/ItemStringOption;->ITEM_WIDTH:I

    iput p6, p0, Lcom/konka/tvsettings/view/ItemStringOption;->ITEM_HEIGHT:I

    invoke-virtual {p0}, Lcom/konka/tvsettings/view/ItemStringOption;->initItem()V

    iput p4, p0, Lcom/konka/tvsettings/view/ItemStringOption;->iValueIndex:I

    iput-boolean v2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->bIsValueANumber:Z

    invoke-virtual {p0, p2}, Lcom/konka/tvsettings/view/ItemStringOption;->setItemName(I)Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {p0, p3}, Lcom/konka/tvsettings/view/ItemStringOption;->setItemValueStrs(I)V

    invoke-virtual {p0, p4}, Lcom/konka/tvsettings/view/ItemStringOption;->setItemValueIndex(I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IIIIII)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->MAX_VALUE:I

    iput v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->MIN_VALUE:I

    iput v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->ITEM_WIDTH:I

    iput v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->ITEM_HEIGHT:I

    const/16 v0, 0x15

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->LEFT_KEY_CODE:I

    const/16 v0, 0x16

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->RIGHT_KEY_CODE:I

    const/16 v0, 0x14

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->DOWNT_KEY_CODE:I

    const v0, 0x7f020032    # com.konka.tvsettings.R.drawable.com_bg_popmenu2

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->IMG_ITEM_BG:I

    const v0, 0x7f020120    # com.konka.tvsettings.R.drawable.setting_menu_item_state

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->IMG_ITEM_CONTAINER_BG:I

    const v0, 0x7f020070    # com.konka.tvsettings.R.drawable.item_left_arrow_state

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->IMG_LEFT_ARROW_STATE:I

    const v0, 0x7f020071    # com.konka.tvsettings.R.drawable.item_right_arrow_state

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->IMG_RIGHT_ARROW_STATE:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->TXT_COLOR_FOCUSED:I

    const v0, -0x666667

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->TXT_COLOR_NORMAL:I

    const v0, -0x7c7c78

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->TXT_COLOR_UNFOCUSED:I

    const/16 v0, 0x21

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->TXT_SIZE_FOCUS:I

    const/16 v0, 0x1c

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->TXT_SIZE_UNFOCUS:I

    iput-boolean v2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->isCreateImageView:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09000d    # com.konka.tvsettings.R.dimen.SettingStringOption_textSize

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->TXT_SIZE_UNFOCUS:I

    iput-object p1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->mContext:Landroid/content/Context;

    iput p6, p0, Lcom/konka/tvsettings/view/ItemStringOption;->ITEM_WIDTH:I

    iput p7, p0, Lcom/konka/tvsettings/view/ItemStringOption;->ITEM_HEIGHT:I

    iput p4, p0, Lcom/konka/tvsettings/view/ItemStringOption;->MIN_VALUE:I

    iput p5, p0, Lcom/konka/tvsettings/view/ItemStringOption;->MAX_VALUE:I

    invoke-virtual {p0}, Lcom/konka/tvsettings/view/ItemStringOption;->initItem()V

    iput p3, p0, Lcom/konka/tvsettings/view/ItemStringOption;->iValueIndex:I

    iput-boolean v2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->bIsValueANumber:Z

    invoke-virtual {p0, p2}, Lcom/konka/tvsettings/view/ItemStringOption;->setItemName(I)Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {p0, p4, p5}, Lcom/konka/tvsettings/view/ItemStringOption;->setValueRange(II)V

    invoke-virtual {p0, p3, v2}, Lcom/konka/tvsettings/view/ItemStringOption;->setItemValue(IZ)Lcom/konka/tvsettings/view/ItemStringOption;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/view/ItemStringOption;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/view/ItemStringOption;->imageLeftRightVisibility(I)V

    return-void
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/view/ItemStringOption;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionName:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/view/ItemStringOption;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionValue:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/view/ItemStringOption;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->LEFT_KEY_CODE:I

    return v0
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/view/ItemStringOption;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/view/ItemStringOption;->updateItemState()V

    return-void
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/view/ItemStringOption;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->RIGHT_KEY_CODE:I

    return v0
.end method

.method private imageLeftRightVisibility(I)V
    .locals 1
    .param p1    # I

    iget-boolean v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->isCreateImageView:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->imgLeftArrow:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->imgRightArrow:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private updateItemState()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    iget-boolean v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->bIsValueANumber:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->iValueIndex:I

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionValue:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "+"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->iValueIndex:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->iValueIndex:I

    iget v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->MIN_VALUE:I

    if-ne v0, v1, :cond_2

    invoke-virtual {p0, v4, v3}, Lcom/konka/tvsettings/view/ItemStringOption;->enableArrows(ZZ)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionValue:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->iValueIndex:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->iValueIndex:I

    iget v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->MAX_VALUE:I

    if-ne v0, v1, :cond_4

    invoke-virtual {p0, v3, v4}, Lcom/konka/tvsettings/view/ItemStringOption;->enableArrows(ZZ)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->strItemValues:[Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionValue:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->strItemValues:[Ljava/lang/CharSequence;

    iget v2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->iValueIndex:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    invoke-virtual {p0, v3, v3}, Lcom/konka/tvsettings/view/ItemStringOption;->enableArrows(ZZ)V

    goto :goto_1
.end method


# virtual methods
.method public disabledArrows()V
    .locals 3

    const/4 v2, 0x4

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->isCreateImageView:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->imgLeftArrow:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->imgLeftArrow:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->imgLeftArrow:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->imgRightArrow:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->imgRightArrow:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->imgRightArrow:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public doUpdate()V
    .locals 0

    return-void
.end method

.method public enableArrows(ZZ)V
    .locals 1
    .param p1    # Z
    .param p2    # Z

    iget-boolean v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->isCreateImageView:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->imgLeftArrow:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->imgLeftArrow:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->imgRightArrow:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->imgRightArrow:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto :goto_0
.end method

.method public getIndex()I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->iValueIndex:I

    return v0
.end method

.method public getMenuItemContainer()Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->container:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public getOnNoArrowsFocusChangeListener()Lcom/konka/tvsettings/view/ItemStringOption$OnNoArrowsFocusEvent;
    .locals 1

    new-instance v0, Lcom/konka/tvsettings/view/ItemStringOption$OnNoArrowsFocusEvent;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/view/ItemStringOption$OnNoArrowsFocusEvent;-><init>(Lcom/konka/tvsettings/view/ItemStringOption;)V

    return-object v0
.end method

.method protected initItem()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, -0x1

    invoke-virtual {p0, v5}, Lcom/konka/tvsettings/view/ItemStringOption;->setOrientation(I)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    iget v2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->ITEM_HEIGHT:I

    invoke-direct {v1, v4, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/view/ItemStringOption;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v1, 0x7f020032    # com.konka.tvsettings.R.drawable.com_bg_popmenu2

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/view/ItemStringOption;->setBackgroundResource(I)V

    new-instance v1, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->container:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->container:Landroid/widget/LinearLayout;

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->container:Landroid/widget/LinearLayout;

    const v2, 0x7f020120    # com.konka.tvsettings.R.drawable.setting_menu_item_state

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v6}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionName:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionName:Landroid/widget/TextView;

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    iget v3, p0, Lcom/konka/tvsettings/view/ItemStringOption;->ITEM_WIDTH:I

    mul-int/lit16 v3, v3, 0xdd

    div-int/lit16 v3, v3, 0x1dc

    invoke-direct {v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionName:Landroid/widget/TextView;

    const v2, -0x7c7c78

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionName:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->TXT_SIZE_UNFOCUS:I

    int-to-float v2, v2

    invoke-virtual {v1, v5, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionName:Landroid/widget/TextView;

    const/16 v2, 0x15

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionName:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->setSingleLine()V

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->container:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionName:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->imgLeftArrow:Landroid/widget/ImageView;

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    iget v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->ITEM_WIDTH:I

    mul-int/lit8 v1, v1, 0x22

    div-int/lit16 v1, v1, 0x1dc

    invoke-direct {v0, v1, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->ITEM_WIDTH:I

    mul-int/lit8 v1, v1, 0x19

    div-int/lit16 v1, v1, 0x1dc

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->imgLeftArrow:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->imgLeftArrow:Landroid/widget/ImageView;

    const v2, 0x7f020070    # com.konka.tvsettings.R.drawable.item_left_arrow_state

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->imgLeftArrow:Landroid/widget/ImageView;

    invoke-virtual {v1, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->imgLeftArrow:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setClickable(Z)V

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->imgLeftArrow:Landroid/widget/ImageView;

    new-instance v2, Lcom/konka/tvsettings/view/ItemStringOption$ItemLeftArrowOnClickEvent;

    invoke-direct {v2, p0, v7}, Lcom/konka/tvsettings/view/ItemStringOption$ItemLeftArrowOnClickEvent;-><init>(Lcom/konka/tvsettings/view/ItemStringOption;Lcom/konka/tvsettings/view/ItemStringOption$ItemLeftArrowOnClickEvent;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->container:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->imgLeftArrow:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionValue:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionValue:Landroid/widget/TextView;

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    iget v3, p0, Lcom/konka/tvsettings/view/ItemStringOption;->ITEM_WIDTH:I

    mul-int/lit16 v3, v3, 0x8f

    div-int/lit16 v3, v3, 0x1dc

    invoke-direct {v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionValue:Landroid/widget/TextView;

    const v2, -0x7c7c78

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionValue:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->TXT_SIZE_UNFOCUS:I

    int-to-float v2, v2

    invoke-virtual {v1, v5, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionValue:Landroid/widget/TextView;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionValue:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->setSingleLine()V

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->container:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionValue:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->imgRightArrow:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->imgRightArrow:Landroid/widget/ImageView;

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    iget v3, p0, Lcom/konka/tvsettings/view/ItemStringOption;->ITEM_WIDTH:I

    mul-int/lit8 v3, v3, 0x39

    div-int/lit16 v3, v3, 0x1dc

    invoke-direct {v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->imgRightArrow:Landroid/widget/ImageView;

    const v2, 0x7f020071    # com.konka.tvsettings.R.drawable.item_right_arrow_state

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->imgRightArrow:Landroid/widget/ImageView;

    invoke-virtual {v1, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->imgRightArrow:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setClickable(Z)V

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->imgRightArrow:Landroid/widget/ImageView;

    new-instance v2, Lcom/konka/tvsettings/view/ItemStringOption$ItemRightArrowOnClickEvent;

    invoke-direct {v2, p0, v7}, Lcom/konka/tvsettings/view/ItemStringOption$ItemRightArrowOnClickEvent;-><init>(Lcom/konka/tvsettings/view/ItemStringOption;Lcom/konka/tvsettings/view/ItemStringOption$ItemRightArrowOnClickEvent;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->container:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->imgRightArrow:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iput v5, p0, Lcom/konka/tvsettings/view/ItemStringOption;->iValueIndex:I

    iput-boolean v5, p0, Lcom/konka/tvsettings/view/ItemStringOption;->bIsValueANumber:Z

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->container:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/view/ItemStringOption;->addView(Landroid/view/View;)V

    iget-boolean v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->isCreateImageView:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->container:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/konka/tvsettings/view/ItemStringOption$ItemOnKeyEvent;

    invoke-direct {v2, p0, v7}, Lcom/konka/tvsettings/view/ItemStringOption$ItemOnKeyEvent;-><init>(Lcom/konka/tvsettings/view/ItemStringOption;Lcom/konka/tvsettings/view/ItemStringOption$ItemOnKeyEvent;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    invoke-direct {p0, v8}, Lcom/konka/tvsettings/view/ItemStringOption;->imageLeftRightVisibility(I)V

    :cond_0
    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->container:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/konka/tvsettings/view/ItemStringOption$ItemOnChangeEvent;

    invoke-direct {v2, p0, v7}, Lcom/konka/tvsettings/view/ItemStringOption$ItemOnChangeEvent;-><init>(Lcom/konka/tvsettings/view/ItemStringOption;Lcom/konka/tvsettings/view/ItemStringOption$ItemOnChangeEvent;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method public noArrowsItemGainFocus()V
    .locals 2

    const/4 v1, -0x1

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionName:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionValue:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public noArrowsItemLoseFocus()V
    .locals 2

    const v1, -0x7c7c78

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionName:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionValue:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public onKeyLeft()V
    .locals 3

    iget-boolean v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->bIsValueANumber:Z

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->iValueIndex:I

    iget v2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->MIN_VALUE:I

    if-gt v1, v2, :cond_0

    iget v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->MIN_VALUE:I

    :goto_0
    iput v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->iValueIndex:I

    :goto_1
    return-void

    :cond_0
    iget v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->iValueIndex:I

    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->strItemValues:[Ljava/lang/CharSequence;

    array-length v1, v1

    add-int/lit8 v0, v1, -0x1

    iget v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->iValueIndex:I

    if-lez v1, :cond_2

    iget v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->iValueIndex:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->iValueIndex:I

    goto :goto_1

    :cond_2
    iput v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->iValueIndex:I

    goto :goto_1
.end method

.method public onKeyRight()V
    .locals 3

    iget-boolean v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->bIsValueANumber:Z

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->iValueIndex:I

    iget v2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->MAX_VALUE:I

    if-lt v1, v2, :cond_1

    iget v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->MAX_VALUE:I

    :goto_0
    iput v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->iValueIndex:I

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->iValueIndex:I

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->strItemValues:[Ljava/lang/CharSequence;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->strItemValues:[Ljava/lang/CharSequence;

    array-length v1, v1

    add-int/lit8 v0, v1, -0x1

    iget v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->iValueIndex:I

    if-ge v1, v0, :cond_3

    iget v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->iValueIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->iValueIndex:I

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    iput v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->iValueIndex:I

    goto :goto_1
.end method

.method public requestFocus(ILandroid/graphics/Rect;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/graphics/Rect;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/view/ItemStringOption;->playSoundEffect(I)V

    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method

.method public setCurrValue(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->iValueIndex:I

    invoke-direct {p0}, Lcom/konka/tvsettings/view/ItemStringOption;->updateItemState()V

    return-void
.end method

.method public setItemName(I)Lcom/konka/tvsettings/view/ItemStringOption;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionName:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-object p0
.end method

.method public setItemName(Ljava/lang/String;)Lcom/konka/tvsettings/view/ItemStringOption;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionName:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object p0
.end method

.method public setItemValue(IZ)Lcom/konka/tvsettings/view/ItemStringOption;
    .locals 3
    .param p1    # I
    .param p2    # Z

    iput-boolean p2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->bIsValueANumber:Z

    if-eqz p2, :cond_1

    iput p1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->iValueIndex:I

    iget v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->iValueIndex:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->iValueIndex:I

    if-lez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "+"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionValue:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->strItemValues:[Ljava/lang/CharSequence;

    :goto_0
    return-object p0

    :cond_1
    invoke-virtual {p0}, Lcom/konka/tvsettings/view/ItemStringOption;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/view/ItemStringOption;->setItemValue(Ljava/lang/String;)Lcom/konka/tvsettings/view/ItemStringOption;

    goto :goto_0
.end method

.method public setItemValue(Ljava/lang/String;)Lcom/konka/tvsettings/view/ItemStringOption;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionValue:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object p0
.end method

.method public setItemValueIndex(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->iValueIndex:I

    invoke-direct {p0}, Lcom/konka/tvsettings/view/ItemStringOption;->updateItemState()V

    return-void
.end method

.method public setItemValueStrs(I)V
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lcom/konka/tvsettings/view/ItemStringOption;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->strItemValues:[Ljava/lang/CharSequence;

    return-void
.end method

.method public setKeyValue(II)Lcom/konka/tvsettings/view/ItemStringOption;
    .locals 1
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionName:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionValue:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(I)V

    return-object p0
.end method

.method public setLeftKeyCode(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->LEFT_KEY_CODE:I

    return-void
.end method

.method public setRightKeyCode(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->RIGHT_KEY_CODE:I

    return-void
.end method

.method public setStatusAllowed()V
    .locals 3

    const v2, 0x7f080002    # com.konka.tvsettings.R.color.text_normal_col

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/view/ItemStringOption;->setClickable(Z)V

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/view/ItemStringOption;->setFocusable(Z)V

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/view/ItemStringOption;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionName:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionValue:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public setStatusFbd()V
    .locals 3

    const v2, 0x7f080003    # com.konka.tvsettings.R.color.text_forbidden_col

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/view/ItemStringOption;->setClickable(Z)V

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/view/ItemStringOption;->setFocusable(Z)V

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/view/ItemStringOption;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionName:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/view/ItemStringOption;->txtOptionValue:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public setValueRange(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    if-le p1, p2, :cond_0

    move v0, p1

    move p1, p2

    move p2, v0

    :cond_0
    if-ne p1, p2, :cond_1

    add-int/lit8 p2, p1, 0x2

    :cond_1
    iput p1, p0, Lcom/konka/tvsettings/view/ItemStringOption;->MIN_VALUE:I

    iput p2, p0, Lcom/konka/tvsettings/view/ItemStringOption;->MAX_VALUE:I

    invoke-direct {p0}, Lcom/konka/tvsettings/view/ItemStringOption;->updateItemState()V

    return-void
.end method
