.class Lcom/konka/tvsettings/RootActivity$MyHandler;
.super Landroid/os/Handler;
.source "RootActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/RootActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MyHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/RootActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/RootActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/RootActivity$MyHandler;->this$0:Lcom/konka/tvsettings/RootActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1    # Landroid/os/Message;

    const/16 v5, 0x2001

    const/4 v4, 0x0

    iget v2, p1, Landroid/os/Message;->what:I

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$MyHandler;->this$0:Lcom/konka/tvsettings/RootActivity;

    # invokes: Lcom/konka/tvsettings/RootActivity;->checkmsg(I)Z
    invoke-static {v3, v2}, Lcom/konka/tvsettings/RootActivity;->access$19(Lcom/konka/tvsettings/RootActivity;I)Z

    move-result v3

    if-nez v3, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v3, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_SIGNAL_LOCK:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->ordinal()I

    move-result v3

    if-eq v2, v3, :cond_1

    sget-object v3, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_SIGNAL_UNLOCK:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->ordinal()I

    move-result v3

    if-ne v2, v3, :cond_3

    :cond_1
    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$MyHandler;->this$0:Lcom/konka/tvsettings/RootActivity;

    # invokes: Lcom/konka/tvsettings/RootActivity;->signalLockHandle(Landroid/os/Message;)V
    invoke-static {v3, p1}, Lcom/konka/tvsettings/RootActivity;->access$20(Lcom/konka/tvsettings/RootActivity;Landroid/os/Message;)V

    :cond_2
    :goto_1
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0

    :cond_3
    sget-object v3, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_SCREEN_SAVER_MODE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->ordinal()I

    move-result v3

    if-ne v2, v3, :cond_4

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$MyHandler;->this$0:Lcom/konka/tvsettings/RootActivity;

    invoke-static {v3, v4}, Lcom/konka/tvsettings/RootActivity;->access$8(Lcom/konka/tvsettings/RootActivity;Z)V

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$MyHandler;->this$0:Lcom/konka/tvsettings/RootActivity;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "Status"

    const/4 v6, -0x1

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    invoke-static {v3, v4}, Lcom/konka/tvsettings/RootActivity;->access$21(Lcom/konka/tvsettings/RootActivity;I)V

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$MyHandler;->this$0:Lcom/konka/tvsettings/RootActivity;

    # invokes: Lcom/konka/tvsettings/RootActivity;->DTVScreenWarningHandle(Landroid/os/Message;)V
    invoke-static {v3, p1}, Lcom/konka/tvsettings/RootActivity;->access$22(Lcom/konka/tvsettings/RootActivity;Landroid/os/Message;)V

    goto :goto_1

    :cond_4
    const/16 v3, 0x1001

    if-ne v2, v3, :cond_2

    const-string v3, "get message of TIMER_SCREEN_SAVER_TIMEOUT"

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$MyHandler;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_ScreenSaverTimer:Lcom/konka/tvsettings/common/CountDownTimer;
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$14(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/tvsettings/common/CountDownTimer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/tvsettings/common/CountDownTimer;->stop()V

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$MyHandler;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_bRootActivityOnTop:Z
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$3(Lcom/konka/tvsettings/RootActivity;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$MyHandler;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_bIsFirstDoingAfterStart:Z
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$23(Lcom/konka/tvsettings/RootActivity;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$MyHandler;->this$0:Lcom/konka/tvsettings/RootActivity;

    invoke-static {v3, v4}, Lcom/konka/tvsettings/RootActivity;->access$24(Lcom/konka/tvsettings/RootActivity;Z)V

    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.konka.action.STATUSBAR_CONTROL"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "show"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$MyHandler;->this$0:Lcom/konka/tvsettings/RootActivity;

    invoke-virtual {v3, v1}, Lcom/konka/tvsettings/RootActivity;->sendBroadcast(Landroid/content/Intent;)V

    :cond_5
    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$MyHandler;->this$0:Lcom/konka/tvsettings/RootActivity;

    iget-object v4, p0, Lcom/konka/tvsettings/RootActivity$MyHandler;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v4}, Lcom/konka/tvsettings/RootActivity;->access$0(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/CommonDesk;->isSignalStable()Z

    move-result v4

    invoke-static {v3, v4}, Lcom/konka/tvsettings/RootActivity;->access$25(Lcom/konka/tvsettings/RootActivity;Z)V

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$MyHandler;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_bSignalLock:Z
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$26(Lcom/konka/tvsettings/RootActivity;)Z

    move-result v3

    if-eqz v3, :cond_a

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$MyHandler;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_bDTVScreenWarning:Z
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$27(Lcom/konka/tvsettings/RootActivity;)Z

    move-result v3

    if-eqz v3, :cond_6

    const-string v3, "RootActivity"

    const-string v4, "+++++++++++m_bDTVScreenWarning"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_6
    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$MyHandler;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_bColorWheelDemon:Z
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$28(Lcom/konka/tvsettings/RootActivity;)Z

    move-result v3

    if-eqz v3, :cond_7

    const-string v3, "start to demonstrate the color wheel"

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$MyHandler;->this$0:Lcom/konka/tvsettings/RootActivity;

    const/16 v4, 0x2000

    invoke-static {v3, v4}, Lcom/konka/tvsettings/SwitchMenuHelper;->goToMenuById(Landroid/content/Context;I)V

    goto/16 :goto_1

    :cond_7
    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$MyHandler;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_bEnergySavingDemon:Z
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$29(Lcom/konka/tvsettings/RootActivity;)Z

    move-result v3

    if-eqz v3, :cond_8

    const-string v3, "start to demonstrate the energy-saving"

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$MyHandler;->this$0:Lcom/konka/tvsettings/RootActivity;

    const-class v4, Lcom/konka/tvsettings/popup/IntellControlActivity;

    invoke-direct {v0, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$MyHandler;->this$0:Lcom/konka/tvsettings/RootActivity;

    invoke-virtual {v3, v0}, Lcom/konka/tvsettings/RootActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    :cond_8
    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$MyHandler;->this$0:Lcom/konka/tvsettings/RootActivity;

    # invokes: Lcom/konka/tvsettings/RootActivity;->isShowRadio()Z
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$15(Lcom/konka/tvsettings/RootActivity;)Z

    move-result v3

    if-eqz v3, :cond_9

    const-string v3, "RootActivity"

    const-string v4, "XXX+++++++++++isShowRadio"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$MyHandler;->this$0:Lcom/konka/tvsettings/RootActivity;

    const/16 v4, 0x2002

    invoke-static {v3, v4}, Lcom/konka/tvsettings/SwitchMenuHelper;->goToMenuById(Landroid/content/Context;I)V

    goto/16 :goto_1

    :cond_9
    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$MyHandler;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->vgaSupport:Z
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$30(Lcom/konka/tvsettings/RootActivity;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$MyHandler;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$0(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_VGA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v3, v4, :cond_2

    const-string v3, "VGA RootActivity"

    const-string v4, "+++++++++++UNSUPPORT MODE"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$MyHandler;->this$0:Lcom/konka/tvsettings/RootActivity;

    invoke-static {v3, v5}, Lcom/konka/tvsettings/SwitchMenuHelper;->goToMenuById(Landroid/content/Context;I)V

    goto/16 :goto_1

    :cond_a
    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$MyHandler;->this$0:Lcom/konka/tvsettings/RootActivity;

    # invokes: Lcom/konka/tvsettings/RootActivity;->isReadyShowScreenSaver()Z
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$31(Lcom/konka/tvsettings/RootActivity;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "RootActivity"

    const-string v4, "+++++++++++screensaver"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$MyHandler;->this$0:Lcom/konka/tvsettings/RootActivity;

    invoke-static {v3, v5}, Lcom/konka/tvsettings/SwitchMenuHelper;->goToMenuById(Landroid/content/Context;I)V

    goto/16 :goto_1
.end method
