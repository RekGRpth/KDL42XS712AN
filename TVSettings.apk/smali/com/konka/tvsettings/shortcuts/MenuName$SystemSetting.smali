.class public final Lcom/konka/tvsettings/shortcuts/MenuName$SystemSetting;
.super Ljava/lang/Object;
.source "MenuName.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/shortcuts/MenuName;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SystemSetting"
.end annotation


# static fields
.field public static final MENU_INDIVIDUAL_ENERGY:Ljava/lang/String; = "individual_energy"

.field public static final MENU_INDIVIDUAL_TIMING:Ljava/lang/String; = "individual_timing"

.field public static final MENU_NET_LAN:Ljava/lang/String; = "net_lan"

.field public static final MENU_NET_PPOE:Ljava/lang/String; = "net_PPOE"

.field public static final MENU_NET_WIRELESS:Ljava/lang/String; = "net_wireless"

.field public static final MENU_STORAGE_RAM:Ljava/lang/String; = "storage_ram"

.field public static final MENU_STORAGE_SD:Ljava/lang/String; = "storage_sd"

.field public static final MENU_SYSTEM_DISCLAIMER:Ljava/lang/String; = "system_disclaimer"

.field public static final MENU_SYSTEM_INFO:Ljava/lang/String; = "system_info"

.field public static final MENU_SYSTEM_INPUT:Ljava/lang/String; = "system_input"

.field public static final MENU_SYSTEM_LANGUAGE:Ljava/lang/String; = "system_language"

.field public static final MENU_SYSTEM_RESET:Ljava/lang/String; = "system_reset"

.field public static final MENU_SYSTEM_UPGRADE:Ljava/lang/String; = "system_upgrade"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
