.class Lcom/konka/tvsettings/MovingPanel$AsynMove;
.super Landroid/os/AsyncTask;
.source "MovingPanel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/MovingPanel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "AsynMove"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/MovingPanel;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/MovingPanel;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/MovingPanel$AsynMove;->this$0:Lcom/konka/tvsettings/MovingPanel;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/konka/tvsettings/MovingPanel$AsynMove;->doInBackground([Ljava/lang/Integer;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Integer;)Ljava/lang/Void;
    .locals 6
    .param p1    # [Ljava/lang/Integer;

    const/4 v5, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/konka/tvsettings/MovingPanel$AsynMove;->this$0:Lcom/konka/tvsettings/MovingPanel;

    # getter for: Lcom/konka/tvsettings/MovingPanel;->iMoveLength:I
    invoke-static {v3}, Lcom/konka/tvsettings/MovingPanel;->access$3(Lcom/konka/tvsettings/MovingPanel;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    aget-object v4, p1, v5

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    rem-int/2addr v3, v4

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/MovingPanel$AsynMove;->this$0:Lcom/konka/tvsettings/MovingPanel;

    # getter for: Lcom/konka/tvsettings/MovingPanel;->iMoveLength:I
    invoke-static {v3}, Lcom/konka/tvsettings/MovingPanel;->access$3(Lcom/konka/tvsettings/MovingPanel;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    aget-object v4, p1, v5

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    div-int v2, v3, v4

    :goto_0
    const/4 v1, 0x0

    :goto_1
    if-lt v1, v2, :cond_1

    const/4 v3, 0x0

    return-object v3

    :cond_0
    iget-object v3, p0, Lcom/konka/tvsettings/MovingPanel$AsynMove;->this$0:Lcom/konka/tvsettings/MovingPanel;

    # getter for: Lcom/konka/tvsettings/MovingPanel;->iMoveLength:I
    invoke-static {v3}, Lcom/konka/tvsettings/MovingPanel;->access$3(Lcom/konka/tvsettings/MovingPanel;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    aget-object v4, p1, v5

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    div-int/2addr v3, v4

    add-int/lit8 v2, v3, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/konka/tvsettings/MovingPanel$AsynMove;->publishProgress([Ljava/lang/Object;)V

    const/16 v3, 0x64

    :try_start_0
    iget-object v4, p0, Lcom/konka/tvsettings/MovingPanel$AsynMove;->this$0:Lcom/konka/tvsettings/MovingPanel;

    # getter for: Lcom/konka/tvsettings/MovingPanel;->MOVE_SPEED:I
    invoke-static {v4}, Lcom/konka/tvsettings/MovingPanel;->access$4(Lcom/konka/tvsettings/MovingPanel;)I

    move-result v4

    div-int/2addr v3, v4

    int-to-long v3, v3

    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/konka/tvsettings/MovingPanel$AsynMove;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2
    .param p1    # Ljava/lang/Void;

    iget-object v0, p0, Lcom/konka/tvsettings/MovingPanel$AsynMove;->this$0:Lcom/konka/tvsettings/MovingPanel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/konka/tvsettings/MovingPanel;->access$0(Lcom/konka/tvsettings/MovingPanel;Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/MovingPanel$AsynMove;->this$0:Lcom/konka/tvsettings/MovingPanel;

    # getter for: Lcom/konka/tvsettings/MovingPanel;->movingAdapter:Lcom/konka/tvsettings/ImageTextAdapter;
    invoke-static {v0}, Lcom/konka/tvsettings/MovingPanel;->access$1(Lcom/konka/tvsettings/MovingPanel;)Lcom/konka/tvsettings/ImageTextAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/MovingPanel$AsynMove;->this$0:Lcom/konka/tvsettings/MovingPanel;

    # getter for: Lcom/konka/tvsettings/MovingPanel;->iCurFocusItemID:I
    invoke-static {v1}, Lcom/konka/tvsettings/MovingPanel;->access$2(Lcom/konka/tvsettings/MovingPanel;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/ImageTextAdapter;->notifyDataSetChanged(I)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/MovingPanel$AsynMove;->this$0:Lcom/konka/tvsettings/MovingPanel;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/konka/tvsettings/MovingPanel;->access$0(Lcom/konka/tvsettings/MovingPanel;Z)V

    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 3
    .param p1    # [Ljava/lang/Integer;

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/konka/tvsettings/MovingPanel$AsynMove;->this$0:Lcom/konka/tvsettings/MovingPanel;

    # getter for: Lcom/konka/tvsettings/MovingPanel;->panelContainer:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/konka/tvsettings/MovingPanel;->access$5(Lcom/konka/tvsettings/MovingPanel;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    aget-object v1, p1, v2

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gez v1, :cond_0

    iget v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    aget-object v2, p1, v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/konka/tvsettings/MovingPanel$AsynMove;->this$0:Lcom/konka/tvsettings/MovingPanel;

    # getter for: Lcom/konka/tvsettings/MovingPanel;->iDestLeftMargin:I
    invoke-static {v2}, Lcom/konka/tvsettings/MovingPanel;->access$6(Lcom/konka/tvsettings/MovingPanel;)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    :goto_0
    iget-object v1, p0, Lcom/konka/tvsettings/MovingPanel$AsynMove;->this$0:Lcom/konka/tvsettings/MovingPanel;

    # getter for: Lcom/konka/tvsettings/MovingPanel;->panelContainer:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/konka/tvsettings/MovingPanel;->access$5(Lcom/konka/tvsettings/MovingPanel;)Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void

    :cond_0
    iget v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    aget-object v2, p1, v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/konka/tvsettings/MovingPanel$AsynMove;->this$0:Lcom/konka/tvsettings/MovingPanel;

    # getter for: Lcom/konka/tvsettings/MovingPanel;->iDestLeftMargin:I
    invoke-static {v2}, Lcom/konka/tvsettings/MovingPanel;->access$6(Lcom/konka/tvsettings/MovingPanel;)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    goto :goto_0
.end method

.method protected bridge varargs synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/konka/tvsettings/MovingPanel$AsynMove;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
