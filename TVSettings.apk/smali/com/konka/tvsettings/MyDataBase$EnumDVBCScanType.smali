.class public final enum Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;
.super Ljava/lang/Enum;
.source "MyDataBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/MyDataBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumDVBCScanType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;

.field public static final enum SCAN_FULL:Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;

.field public static final enum SCAN_NETTABLE:Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;

.field public static final enum SCAN_TYPE_NUM:Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;

    const-string v1, "SCAN_NETTABLE"

    invoke-direct {v0, v1, v2}, Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;->SCAN_NETTABLE:Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;

    new-instance v0, Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;

    const-string v1, "SCAN_FULL"

    invoke-direct {v0, v1, v3}, Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;->SCAN_FULL:Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;

    new-instance v0, Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;

    const-string v1, "SCAN_TYPE_NUM"

    invoke-direct {v0, v1, v4}, Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;->SCAN_TYPE_NUM:Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;

    sget-object v1, Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;->SCAN_NETTABLE:Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;->SCAN_FULL:Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;->SCAN_TYPE_NUM:Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;->ENUM$VALUES:[Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;
    .locals 1

    const-class v0, Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;

    return-object v0
.end method

.method public static values()[Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;->ENUM$VALUES:[Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/tvsettings/MyDataBase$EnumDVBCScanType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
