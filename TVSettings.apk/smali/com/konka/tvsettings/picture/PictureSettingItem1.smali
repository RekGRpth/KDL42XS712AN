.class public Lcom/konka/tvsettings/picture/PictureSettingItem1;
.super Ljava/lang/Object;
.source "PictureSettingItem1.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/view/View$OnKeyListener;
.implements Lcom/konka/tvsettings/view/IUpdateSysData;


# instance fields
.field private STATE_COUNT:I

.field private activity:Landroid/app/Activity;

.field private commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

.field private mArrowsLeft:Landroid/widget/ImageView;

.field private mArrowsRight:Landroid/widget/ImageView;

.field private mCurrentState:I

.field private mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field private mItemContainer:Landroid/widget/LinearLayout;

.field private mItemId:I

.field private mItemName:Landroid/widget/TextView;

.field private mItemState:Landroid/widget/TextView;

.field private mStates:[Ljava/lang/String;

.field private serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;


# direct methods
.method public constructor <init>(Landroid/app/Activity;III)V
    .locals 6
    .param p1    # Landroid/app/Activity;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const v5, 0x7f0700da    # com.konka.tvsettings.R.id.lock_adv_childlock

    const v4, 0x7f0700d6    # com.konka.tvsettings.R.id.lock_adv_locksystem

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemName:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mArrowsLeft:Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemState:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mArrowsRight:Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mStates:[Ljava/lang/String;

    iput v2, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mCurrentState:I

    iput v2, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->STATE_COUNT:I

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->activity:Landroid/app/Activity;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput p2, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemId:I

    iput-object p1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->activity:Landroid/app/Activity;

    invoke-virtual {v1, p2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemName:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mArrowsLeft:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemState:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mArrowsRight:Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    array-length v1, v0

    iput v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->STATE_COUNT:I

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mStates:[Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemState:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mStates:[Ljava/lang/String;

    aget-object v2, v2, p4

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iput p4, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mCurrentState:I

    iget v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mCurrentState:I

    invoke-direct {p0, v1}, Lcom/konka/tvsettings/picture/PictureSettingItem1;->initArrowsBg(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mArrowsLeft:Landroid/widget/ImageView;

    const-string v2, "left"

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mArrowsRight:Landroid/widget/ImageView;

    const-string v2, "right"

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    const v1, 0x7f070108    # com.konka.tvsettings.R.id.picture_setting_pic_mode

    if-ne p2, v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_VGA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v1, v2, :cond_7

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    const v2, 0x7f070110    # com.konka.tvsettings.R.id.picture_setting_backlight

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setNextFocusUpId(I)V

    :cond_0
    :goto_0
    const v1, 0x7f0700ee    # com.konka.tvsettings.R.id.osd_setting_language

    if-ne p2, v1, :cond_1

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    const v2, 0x7f0700ef    # com.konka.tvsettings.R.id.osd_setting_time

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setNextFocusUpId(I)V

    :cond_1
    const v1, 0x7f0700ef    # com.konka.tvsettings.R.id.osd_setting_time

    if-ne p2, v1, :cond_2

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    const v2, 0x7f0700ee    # com.konka.tvsettings.R.id.osd_setting_language

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setNextFocusDownId(I)V

    :cond_2
    const v1, 0x7f07020d    # com.konka.tvsettings.R.id.ontime_menu_ontimeswitch

    if-ne p2, v1, :cond_3

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    const v2, 0x7f070210    # com.konka.tvsettings.R.id.ontime_menu_source

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setNextFocusUpId(I)V

    :cond_3
    const v1, 0x7f070208    # com.konka.tvsettings.R.id.offtime_menu_offtimeswitch

    if-ne p2, v1, :cond_4

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    const v2, 0x7f070209    # com.konka.tvsettings.R.id.offtime_menu_settime

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setNextFocusUpId(I)V

    :cond_4
    if-ne p2, v4, :cond_5

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setNextFocusUpId(I)V

    :cond_5
    if-ne p2, v5, :cond_6

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setNextFocusDownId(I)V

    :cond_6
    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, p0}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mArrowsLeft:Landroid/widget/ImageView;

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mArrowsRight:Landroid/widget/ImageView;

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, p0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void

    :cond_7
    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    const v2, 0x7f070111    # com.konka.tvsettings.R.id.picture_setting_pc_adjust

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setNextFocusUpId(I)V

    goto :goto_0
.end method

.method private initArrowsBg(I)V
    .locals 3
    .param p1    # I

    const v2, 0x7f020004    # com.konka.tvsettings.R.drawable.arrows_sel_right

    const v1, 0x7f020003    # com.konka.tvsettings.R.drawable.arrows_sel_left

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mArrowsLeft:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mArrowsRight:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->STATE_COUNT:I

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mArrowsLeft:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mArrowsRight:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mArrowsLeft:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mArrowsRight:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0
.end method


# virtual methods
.method public doUpdate()V
    .locals 0

    return-void
.end method

.method public getCurrentState()I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mCurrentState:I

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    const v3, 0x7f0a0014    # com.konka.tvsettings.R.string.warning_dtvnotsupport

    const v2, 0x7f0700d9    # com.konka.tvsettings.R.id.lock_adv_parentalguidance

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "left"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemId:I

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->activity:Landroid/app/Activity;

    invoke-static {v1, v3}, Lcom/konka/tvsettings/common/LittleMenu;->popToast(Landroid/content/Context;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/konka/tvsettings/picture/PictureSettingItem1;->stateLeft()V

    goto :goto_0

    :cond_2
    const-string v1, "right"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemId:I

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->activity:Landroid/app/Activity;

    invoke-static {v1, v3}, Lcom/konka/tvsettings/common/LittleMenu;->popToast(Landroid/content/Context;I)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/konka/tvsettings/picture/PictureSettingItem1;->stateLeft()V

    goto :goto_0
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Z

    const/4 v2, 0x4

    const/4 v1, 0x0

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mArrowsLeft:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mArrowsRight:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mArrowsLeft:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mArrowsRight:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const v3, 0x7f0a0014    # com.konka.tvsettings.R.string.warning_dtvnotsupport

    const v2, 0x7f0700d9    # com.konka.tvsettings.R.id.lock_adv_parentalguidance

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getId()I

    move-result v1

    if-ne v0, v1, :cond_1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x15

    if-ne v0, p2, :cond_0

    iget v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemId:I

    if-ne v0, v2, :cond_2

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->activity:Landroid/app/Activity;

    invoke-static {v0, v3}, Lcom/konka/tvsettings/common/LittleMenu;->popToast(Landroid/content/Context;I)V

    :cond_0
    :goto_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0x16

    if-ne v0, p2, :cond_1

    iget v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemId:I

    if-ne v0, v2, :cond_3

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->activity:Landroid/app/Activity;

    invoke-static {v0, v3}, Lcom/konka/tvsettings/common/LittleMenu;->popToast(Landroid/content/Context;I)V

    :cond_1
    :goto_1
    const/4 v0, 0x0

    return v0

    :cond_2
    invoke-virtual {p0}, Lcom/konka/tvsettings/picture/PictureSettingItem1;->stateLeft()V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/konka/tvsettings/picture/PictureSettingItem1;->stateRight()V

    goto :goto_1
.end method

.method public setCurrentState(I)V
    .locals 3
    .param p1    # I

    iput p1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mCurrentState:I

    iget v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mCurrentState:I

    iget v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->STATE_COUNT:I

    add-int/lit8 v1, v1, -0x1

    if-gt v0, v1, :cond_0

    iget v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mCurrentState:I

    if-gez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mCurrentState:I

    :cond_1
    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemState:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mStates:[Ljava/lang/String;

    iget v2, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mCurrentState:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mCurrentState:I

    invoke-direct {p0, v0}, Lcom/konka/tvsettings/picture/PictureSettingItem1;->initArrowsBg(I)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/picture/PictureSettingItem1;->doUpdate()V

    return-void
.end method

.method public setItemEnabled(Z)V
    .locals 4
    .param p1    # Z

    const v3, 0x7f080003    # com.konka.tvsettings.R.color.text_forbidden_col

    const v2, 0x7f080002    # com.konka.tvsettings.R.color.text_normal_col

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mArrowsLeft:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mArrowsRight:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setClickable(Z)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemName:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemState:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemName:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemState:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public setStatusFbd()V
    .locals 3

    const v2, 0x7f080003    # com.konka.tvsettings.R.color.text_forbidden_col

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mArrowsLeft:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mArrowsRight:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemName:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemState:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public setVisibility(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    return-void
.end method

.method public stateLeft()V
    .locals 3

    iget v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mCurrentState:I

    if-nez v0, :cond_1

    iget v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->STATE_COUNT:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mCurrentState:I

    :goto_0
    iget v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mCurrentState:I

    if-gez v0, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mCurrentState:I

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemState:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mStates:[Ljava/lang/String;

    iget v2, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mCurrentState:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mCurrentState:I

    invoke-direct {p0, v0}, Lcom/konka/tvsettings/picture/PictureSettingItem1;->initArrowsBg(I)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/picture/PictureSettingItem1;->doUpdate()V

    return-void

    :cond_1
    iget v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mCurrentState:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mCurrentState:I

    goto :goto_0
.end method

.method public stateRight()V
    .locals 3

    iget v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mCurrentState:I

    iget v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->STATE_COUNT:I

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mCurrentState:I

    :goto_0
    iget v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mCurrentState:I

    iget v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->STATE_COUNT:I

    if-lt v0, v1, :cond_0

    iget v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->STATE_COUNT:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mCurrentState:I

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mItemState:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mStates:[Ljava/lang/String;

    iget v2, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mCurrentState:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mCurrentState:I

    invoke-direct {p0, v0}, Lcom/konka/tvsettings/picture/PictureSettingItem1;->initArrowsBg(I)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/picture/PictureSettingItem1;->doUpdate()V

    return-void

    :cond_1
    iget v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mCurrentState:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem1;->mCurrentState:I

    goto :goto_0
.end method
