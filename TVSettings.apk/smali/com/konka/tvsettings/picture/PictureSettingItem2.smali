.class public Lcom/konka/tvsettings/picture/PictureSettingItem2;
.super Ljava/lang/Object;
.source "PictureSettingItem2.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;
.implements Lcom/konka/tvsettings/view/IUpdateSysData;


# instance fields
.field private final START_VALUE:I

.field private activity:Landroid/app/Activity;

.field private commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

.field private mCurrentValue:I

.field private mDisplayValue:I

.field private mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field private mItemContainer:Landroid/widget/LinearLayout;

.field private mItemDownHalf:Landroid/widget/LinearLayout;

.field private mItemName:Landroid/widget/TextView;

.field private mItemUpHalf:Landroid/widget/LinearLayout;

.field private mItemValue:Landroid/widget/TextView;

.field protected mSeekBar:Landroid/widget/SeekBar;

.field private serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;


# direct methods
.method public constructor <init>(Landroid/app/Activity;III)V
    .locals 3
    .param p1    # Landroid/app/Activity;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemContainer:Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemUpHalf:Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemDownHalf:Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemName:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemValue:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mSeekBar:Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->activity:Landroid/app/Activity;

    iput v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mCurrentValue:I

    iput v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mDisplayValue:I

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object p1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->activity:Landroid/app/Activity;

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->activity:Landroid/app/Activity;

    invoke-virtual {v0, p2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemContainer:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemUpHalf:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemUpHalf:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemName:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemUpHalf:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemValue:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemDownHalf:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemDownHalf:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mSeekBar:Landroid/widget/SeekBar;

    iput p4, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->START_VALUE:I

    iput p3, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mCurrentValue:I

    iget v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->START_VALUE:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mDisplayValue:I

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemValue:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mDisplayValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p3}, Landroid/widget/SeekBar;->setProgress(I)V

    const v0, 0x7f070110    # com.konka.tvsettings.R.id.picture_setting_backlight

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_VGA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemContainer:Landroid/widget/LinearLayout;

    const v1, 0x7f070108    # com.konka.tvsettings.R.id.picture_setting_pic_mode

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusDownId(I)V

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    return-void
.end method


# virtual methods
.method public doUpdate()V
    .locals 0

    return-void
.end method

.method public getCurrentValue()I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mCurrentValue:I

    return v0
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Z

    const/4 v2, 0x0

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemContainer:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setSelected(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemDownHalf:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setSelected(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemDownHalf:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getId()I

    move-result v1

    if-ne v0, v1, :cond_1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x15

    if-ne v0, p2, :cond_0

    invoke-virtual {p0}, Lcom/konka/tvsettings/picture/PictureSettingItem2;->valueDec()V

    :cond_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0x16

    if-ne v0, p2, :cond_1

    invoke-virtual {p0}, Lcom/konka/tvsettings/picture/PictureSettingItem2;->valueInc()V

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 3
    .param p1    # Landroid/widget/SeekBar;
    .param p2    # I
    .param p3    # Z

    iput p2, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mCurrentValue:I

    iget v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mCurrentValue:I

    iget v1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->START_VALUE:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mDisplayValue:I

    iget v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->START_VALUE:I

    if-gez v0, :cond_1

    iget v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mDisplayValue:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemValue:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "+"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mDisplayValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onProgressChanged:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/picture/PictureSettingItem2;->doUpdate()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemValue:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mDisplayValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemValue:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mDisplayValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1    # Landroid/widget/SeekBar;

    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1    # Landroid/widget/SeekBar;

    return-void
.end method

.method public setCurrentValue(I)V
    .locals 3
    .param p1    # I

    iput p1, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mCurrentValue:I

    iget v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->START_VALUE:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mDisplayValue:I

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemValue:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mDisplayValue:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    return-void
.end method

.method public setStatusFbd()V
    .locals 3

    const/4 v2, 0x0

    const v1, -0xcdcdce

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemName:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemValue:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public setStatusNor()V
    .locals 3

    const/4 v2, 0x1

    const v1, -0x666667

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemName:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemValue:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method public setVisibility(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mItemContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    return-void
.end method

.method public valueDec()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mSeekBar:Landroid/widget/SeekBar;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->incrementProgressBy(I)V

    return-void
.end method

.method public valueInc()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingItem2;->mSeekBar:Landroid/widget/SeekBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->incrementProgressBy(I)V

    return-void
.end method
