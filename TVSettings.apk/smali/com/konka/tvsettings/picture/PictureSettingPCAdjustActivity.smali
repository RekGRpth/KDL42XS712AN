.class public Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;
.super Lcom/konka/tvsettings/BaseKonkaActivity;
.source "PictureSettingPCAdjustActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity$AutoAdjClickEvent;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I = null

.field private static final AUTOPC_END_SUCESSED:I = 0x2


# instance fields
.field private bAutoPCSucessed:Z

.field private handler:Landroid/os/Handler;

.field private inputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field private itemAutoAdjust:Landroid/widget/LinearLayout;

.field private itemAutoAdjust3dot:Landroid/widget/TextView;

.field private itemAutoAdjustText:Landroid/widget/TextView;

.field private itemHPos:Lcom/konka/tvsettings/picture/PictureSettingItem2;

.field private itemPhase:Lcom/konka/tvsettings/picture/PictureSettingItem2;

.field private itemPhaseLayout:Landroid/widget/LinearLayout;

.field private itemRate:Lcom/konka/tvsettings/picture/PictureSettingItem2;

.field private itemVPos:Lcom/konka/tvsettings/picture/PictureSettingItem2;

.field private pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

.field private serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;


# direct methods
.method static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource()[I
    .locals 3

    sget-object v0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_28

    :goto_1
    :try_start_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_27

    :goto_2
    :try_start_2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_26

    :goto_3
    :try_start_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_25

    :goto_4
    :try_start_4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_24

    :goto_5
    :try_start_5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS5:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_23

    :goto_6
    :try_start_6
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS6:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_22

    :goto_7
    :try_start_7
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS7:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_21

    :goto_8
    :try_start_8
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS8:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_20

    :goto_9
    :try_start_9
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_1f

    :goto_a
    :try_start_a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1d

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_1e

    :goto_b
    :try_start_b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x26

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_1d

    :goto_c
    :try_start_c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1e

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_1c

    :goto_d
    :try_start_d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1f

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_1b

    :goto_e
    :try_start_e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x20

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_1a

    :goto_f
    :try_start_f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x21

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_19

    :goto_10
    :try_start_10
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x22

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_18

    :goto_11
    :try_start_11
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x18

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_17

    :goto_12
    :try_start_12
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x19

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_16

    :goto_13
    :try_start_13
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1a

    aput v2, v0, v1
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_15

    :goto_14
    :try_start_14
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1b

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_14} :catch_14

    :goto_15
    :try_start_15
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1c

    aput v2, v0, v1
    :try_end_15
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15 .. :try_end_15} :catch_13

    :goto_16
    :try_start_16
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_JPEG:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x25

    aput v2, v0, v1
    :try_end_16
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16 .. :try_end_16} :catch_12

    :goto_17
    :try_start_17
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_KTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x24

    aput v2, v0, v1
    :try_end_17
    .catch Ljava/lang/NoSuchFieldError; {:try_start_17 .. :try_end_17} :catch_11

    :goto_18
    :try_start_18
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x29

    aput v2, v0, v1
    :try_end_18
    .catch Ljava/lang/NoSuchFieldError; {:try_start_18 .. :try_end_18} :catch_10

    :goto_19
    :try_start_19
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x28

    aput v2, v0, v1
    :try_end_19
    .catch Ljava/lang/NoSuchFieldError; {:try_start_19 .. :try_end_19} :catch_f

    :goto_1a
    :try_start_1a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SCART:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x15

    aput v2, v0, v1
    :try_end_1a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a .. :try_end_1a} :catch_e

    :goto_1b
    :try_start_1b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SCART2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x16

    aput v2, v0, v1
    :try_end_1b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1b .. :try_end_1b} :catch_d

    :goto_1c
    :try_start_1c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SCART_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x17

    aput v2, v0, v1
    :try_end_1c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1c .. :try_end_1c} :catch_c

    :goto_1d
    :try_start_1d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x23

    aput v2, v0, v1
    :try_end_1d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1d .. :try_end_1d} :catch_b

    :goto_1e
    :try_start_1e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x27

    aput v2, v0, v1
    :try_end_1e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1e .. :try_end_1e} :catch_a

    :goto_1f
    :try_start_1f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_1f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1f .. :try_end_1f} :catch_9

    :goto_20
    :try_start_20
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_20
    .catch Ljava/lang/NoSuchFieldError; {:try_start_20 .. :try_end_20} :catch_8

    :goto_21
    :try_start_21
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_21
    .catch Ljava/lang/NoSuchFieldError; {:try_start_21 .. :try_end_21} :catch_7

    :goto_22
    :try_start_22
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_22
    .catch Ljava/lang/NoSuchFieldError; {:try_start_22 .. :try_end_22} :catch_6

    :goto_23
    :try_start_23
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_23
    .catch Ljava/lang/NoSuchFieldError; {:try_start_23 .. :try_end_23} :catch_5

    :goto_24
    :try_start_24
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_VGA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_24
    .catch Ljava/lang/NoSuchFieldError; {:try_start_24 .. :try_end_24} :catch_4

    :goto_25
    :try_start_25
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_25
    .catch Ljava/lang/NoSuchFieldError; {:try_start_25 .. :try_end_25} :catch_3

    :goto_26
    :try_start_26
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x12

    aput v2, v0, v1
    :try_end_26
    .catch Ljava/lang/NoSuchFieldError; {:try_start_26 .. :try_end_26} :catch_2

    :goto_27
    :try_start_27
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x13

    aput v2, v0, v1
    :try_end_27
    .catch Ljava/lang/NoSuchFieldError; {:try_start_27 .. :try_end_27} :catch_1

    :goto_28
    :try_start_28
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x14

    aput v2, v0, v1
    :try_end_28
    .catch Ljava/lang/NoSuchFieldError; {:try_start_28 .. :try_end_28} :catch_0

    :goto_29
    sput-object v0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_29

    :catch_1
    move-exception v1

    goto :goto_28

    :catch_2
    move-exception v1

    goto :goto_27

    :catch_3
    move-exception v1

    goto :goto_26

    :catch_4
    move-exception v1

    goto :goto_25

    :catch_5
    move-exception v1

    goto :goto_24

    :catch_6
    move-exception v1

    goto :goto_23

    :catch_7
    move-exception v1

    goto :goto_22

    :catch_8
    move-exception v1

    goto :goto_21

    :catch_9
    move-exception v1

    goto :goto_20

    :catch_a
    move-exception v1

    goto :goto_1f

    :catch_b
    move-exception v1

    goto/16 :goto_1e

    :catch_c
    move-exception v1

    goto/16 :goto_1d

    :catch_d
    move-exception v1

    goto/16 :goto_1c

    :catch_e
    move-exception v1

    goto/16 :goto_1b

    :catch_f
    move-exception v1

    goto/16 :goto_1a

    :catch_10
    move-exception v1

    goto/16 :goto_19

    :catch_11
    move-exception v1

    goto/16 :goto_18

    :catch_12
    move-exception v1

    goto/16 :goto_17

    :catch_13
    move-exception v1

    goto/16 :goto_16

    :catch_14
    move-exception v1

    goto/16 :goto_15

    :catch_15
    move-exception v1

    goto/16 :goto_14

    :catch_16
    move-exception v1

    goto/16 :goto_13

    :catch_17
    move-exception v1

    goto/16 :goto_12

    :catch_18
    move-exception v1

    goto/16 :goto_11

    :catch_19
    move-exception v1

    goto/16 :goto_10

    :catch_1a
    move-exception v1

    goto/16 :goto_f

    :catch_1b
    move-exception v1

    goto/16 :goto_e

    :catch_1c
    move-exception v1

    goto/16 :goto_d

    :catch_1d
    move-exception v1

    goto/16 :goto_c

    :catch_1e
    move-exception v1

    goto/16 :goto_b

    :catch_1f
    move-exception v1

    goto/16 :goto_a

    :catch_20
    move-exception v1

    goto/16 :goto_9

    :catch_21
    move-exception v1

    goto/16 :goto_8

    :catch_22
    move-exception v1

    goto/16 :goto_7

    :catch_23
    move-exception v1

    goto/16 :goto_6

    :catch_24
    move-exception v1

    goto/16 :goto_5

    :catch_25
    move-exception v1

    goto/16 :goto_4

    :catch_26
    move-exception v1

    goto/16 :goto_3

    :catch_27
    move-exception v1

    goto/16 :goto_2

    :catch_28
    move-exception v1

    goto/16 :goto_1
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->bAutoPCSucessed:Z

    new-instance v0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity$1;-><init>(Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->handler:Landroid/os/Handler;

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemAutoAdjust:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemAutoAdjustText:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemAutoAdjust3dot:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemPhaseLayout:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemHPos:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemVPos:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemRate:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemPhase:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->updateItemValue()V

    return-void
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;)Lcom/konka/kkinterface/tv/PictureDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem2;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemHPos:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem2;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemVPos:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem2;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemRate:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->bAutoPCSucessed:Z

    return v0
.end method

.method static synthetic access$7(Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->bAutoPCSucessed:Z

    return-void
.end method

.method static synthetic access$8(Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem2;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemPhase:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    return-object v0
.end method

.method private addItemAutoAdjust()V
    .locals 4

    const v3, 0x7f070118    # com.konka.tvsettings.R.id.picture_setting_pcadj_phase

    const v0, 0x7f070113    # com.konka.tvsettings.R.id.linear_picture_setting_pcadj_auto_adjuct

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemAutoAdjust:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemPhaseLayout:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemAutoAdjust:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemAutoAdjustText:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemAutoAdjust:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemAutoAdjust3dot:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemAutoAdjust:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity$AutoAdjClickEvent;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity$AutoAdjClickEvent;-><init>(Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity$AutoAdjClickEvent;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemAutoAdjust:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setNextFocusUpId(I)V

    return-void
.end method

.method private addItemHPos()V
    .locals 9

    const/4 v7, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    const-string v1, "GetLogicalHPos"

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosCommonCommand(Ljava/lang/String;)[S
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    move-object v8, v7

    :goto_0
    if-eqz v8, :cond_0

    :try_start_1
    array-length v0, v8

    if-nez v0, :cond_1

    :cond_0
    const-string v0, "============GetLogicalVPos has data null"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v7, v0, [S

    const/4 v0, 0x0

    const/16 v1, 0x32

    aput-short v1, v7, v0
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    :try_start_2
    new-instance v0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity$2;

    const v3, 0x7f070115    # com.konka.tvsettings.R.id.picture_setting_pcadj_hpos

    const/4 v1, 0x0

    aget-short v4, v7, v1

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity$2;-><init>(Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;Landroid/app/Activity;III)V

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemHPos:Lcom/konka/tvsettings/picture/PictureSettingItem2;
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_0

    :goto_2
    return-void

    :cond_1
    :try_start_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "============GetLogicalVPos: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    aget-short v1, v8, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_1

    move-object v7, v8

    goto :goto_1

    :catch_0
    move-exception v6

    :goto_3
    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2

    :catch_1
    move-exception v6

    move-object v7, v8

    goto :goto_3

    :cond_2
    move-object v8, v7

    goto :goto_0
.end method

.method private addItemPhase()V
    .locals 9

    const/4 v7, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    const-string v1, "GetLogicalPCPhase"

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosCommonCommand(Ljava/lang/String;)[S
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    move-object v8, v7

    :goto_0
    if-eqz v8, :cond_0

    :try_start_1
    array-length v0, v8

    if-nez v0, :cond_1

    :cond_0
    const-string v0, "============GetLogicalVPos has data null"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v7, v0, [S

    const/4 v0, 0x0

    const/16 v1, 0x32

    aput-short v1, v7, v0
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    :try_start_2
    new-instance v0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity$5;

    const v3, 0x7f070118    # com.konka.tvsettings.R.id.picture_setting_pcadj_phase

    const/4 v1, 0x0

    aget-short v4, v7, v1

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity$5;-><init>(Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;Landroid/app/Activity;III)V

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemPhase:Lcom/konka/tvsettings/picture/PictureSettingItem2;
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_0

    :goto_2
    return-void

    :cond_1
    :try_start_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "============GetLogicalVPos: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    aget-short v1, v8, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_1

    move-object v7, v8

    goto :goto_1

    :catch_0
    move-exception v6

    :goto_3
    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2

    :catch_1
    move-exception v6

    move-object v7, v8

    goto :goto_3

    :cond_2
    move-object v8, v7

    goto :goto_0
.end method

.method private addItemRate()V
    .locals 9

    const/16 v2, 0x64

    const/4 v7, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    const-string v1, "GetLogicalPCClock"

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosCommonCommand(Ljava/lang/String;)[S
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    move-object v8, v7

    :goto_0
    if-eqz v8, :cond_0

    :try_start_1
    array-length v0, v8

    if-nez v0, :cond_2

    :cond_0
    const-string v0, "============GetLogicalVPos has data null"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v7, v0, [S

    const/4 v0, 0x0

    const/16 v1, 0x32

    aput-short v1, v7, v0
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    const/4 v0, 0x0

    :try_start_2
    aget-short v0, v7, v0

    if-gez v0, :cond_3

    const/4 v0, 0x0

    const/4 v1, 0x0

    aput-short v1, v7, v0

    :cond_1
    :goto_2
    new-instance v0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity$4;

    const v3, 0x7f070117    # com.konka.tvsettings.R.id.picture_setting_pcadj_rate

    const/4 v1, 0x0

    aget-short v4, v7, v1

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity$4;-><init>(Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;Landroid/app/Activity;III)V

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemRate:Lcom/konka/tvsettings/picture/PictureSettingItem2;
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    return-void

    :cond_2
    :try_start_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "============GetLogicalVPos: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    aget-short v1, v8, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_1

    move-object v7, v8

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    :try_start_4
    aget-short v0, v7, v0

    if-le v0, v2, :cond_1

    const/4 v0, 0x0

    const/16 v1, 0x64

    aput-short v1, v7, v0
    :try_end_4
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_2

    :catch_0
    move-exception v6

    :goto_4
    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_3

    :catch_1
    move-exception v6

    move-object v7, v8

    goto :goto_4

    :cond_4
    move-object v8, v7

    goto :goto_0
.end method

.method private addItemVPos()V
    .locals 9

    const/4 v7, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    const-string v1, "GetLogicalVPos"

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosCommonCommand(Ljava/lang/String;)[S
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    move-object v8, v7

    :goto_0
    if-eqz v8, :cond_0

    :try_start_1
    array-length v0, v8

    if-nez v0, :cond_1

    :cond_0
    const-string v0, "============GetLogicalVPos has data null"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/4 v0, 0x1

    new-array v7, v0, [S

    const/4 v0, 0x0

    const/16 v1, 0x32

    aput-short v1, v7, v0
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    :try_start_2
    new-instance v0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity$3;

    const v3, 0x7f070116    # com.konka.tvsettings.R.id.picture_setting_pcadj_vpos

    const/4 v1, 0x0

    aget-short v4, v7, v1

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity$3;-><init>(Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;Landroid/app/Activity;III)V

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemVPos:Lcom/konka/tvsettings/picture/PictureSettingItem2;
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_0

    :goto_2
    return-void

    :cond_1
    :try_start_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "============GetLogicalVPos: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    aget-short v1, v8, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_1

    move-object v7, v8

    goto :goto_1

    :catch_0
    move-exception v6

    :goto_3
    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2

    :catch_1
    move-exception v6

    move-object v7, v8

    goto :goto_3

    :cond_2
    move-object v8, v7

    goto :goto_0
.end method

.method private addView()V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->addItemAutoAdjust()V

    invoke-direct {p0}, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->addItemHPos()V

    invoke-direct {p0}, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->addItemVPos()V

    invoke-direct {p0}, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->addItemPhase()V

    invoke-direct {p0}, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->addItemRate()V

    return-void
.end method

.method private fbdItemBySource()V
    .locals 2

    invoke-static {}, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource()[I

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->inputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    aget v0, v0, v1

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    invoke-direct {p0}, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->setItemAutoStatusFbd()V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemRate:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    invoke-virtual {v0}, Lcom/konka/tvsettings/picture/PictureSettingItem2;->setStatusFbd()V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemPhase:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    invoke-virtual {v0}, Lcom/konka/tvsettings/picture/PictureSettingItem2;->setStatusFbd()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x11 -> :sswitch_0
        0x12 -> :sswitch_0
        0x13 -> :sswitch_0
        0x1d -> :sswitch_0
    .end sparse-switch
.end method

.method private setItemAutoStatusFbd()V
    .locals 3

    const v2, -0xcdcdce

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemAutoAdjust:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemAutoAdjust:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemAutoAdjust:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemAutoAdjustText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemAutoAdjust3dot:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method

.method private updateItemValue()V
    .locals 9

    const/4 v1, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v7

    if-eqz v7, :cond_8

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v7

    const-string v8, "GetLogicalHPos"

    invoke-virtual {v7, v8}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosCommonCommand(Ljava/lang/String;)[S

    move-result-object v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v7

    const-string v8, "GetLogicalVPos"

    invoke-virtual {v7, v8}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosCommonCommand(Ljava/lang/String;)[S

    move-result-object v5

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v7

    const-string v8, "GetLogicalPCPhase"

    invoke-virtual {v7, v8}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosCommonCommand(Ljava/lang/String;)[S

    move-result-object v4

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v7

    const-string v8, "GetLogicalPCClock"

    invoke-virtual {v7, v8}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosCommonCommand(Ljava/lang/String;)[S
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    move-object v6, v5

    move-object v2, v1

    :goto_0
    if-eqz v2, :cond_0

    :try_start_1
    array-length v7, v2

    if-nez v7, :cond_4

    :cond_0
    const-string v7, "============GetLogicalVPos has data null"

    invoke-static {v7}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/4 v7, 0x1

    new-array v1, v7, [S

    const/4 v7, 0x0

    const/16 v8, 0x32

    aput-short v8, v1, v7
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    if-eqz v6, :cond_1

    :try_start_2
    array-length v7, v6

    if-nez v7, :cond_5

    :cond_1
    const-string v7, "============GetLogicalVPos has data null"

    invoke-static {v7}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/4 v7, 0x1

    new-array v5, v7, [S

    const/4 v7, 0x0

    const/16 v8, 0x32

    aput-short v8, v5, v7
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    :goto_2
    if-eqz v4, :cond_2

    :try_start_3
    array-length v7, v4

    if-nez v7, :cond_6

    :cond_2
    const-string v7, "============GetLogicalVPos has data null"

    invoke-static {v7}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/4 v7, 0x1

    new-array v4, v7, [S

    :goto_3
    if-eqz v3, :cond_3

    array-length v7, v3

    if-nez v7, :cond_7

    :cond_3
    const-string v7, "============GetLogicalVPos has data null"

    invoke-static {v7}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/4 v7, 0x1

    new-array v3, v7, [S

    :goto_4
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->bAutoPCSucessed:Z

    iget-object v7, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemHPos:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    const/4 v8, 0x0

    aget-short v8, v1, v8

    invoke-virtual {v7, v8}, Lcom/konka/tvsettings/picture/PictureSettingItem2;->setCurrentValue(I)V

    iget-object v7, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemVPos:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    const/4 v8, 0x0

    aget-short v8, v5, v8

    invoke-virtual {v7, v8}, Lcom/konka/tvsettings/picture/PictureSettingItem2;->setCurrentValue(I)V

    iget-object v7, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemRate:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    const/4 v8, 0x0

    aget-short v8, v4, v8

    invoke-virtual {v7, v8}, Lcom/konka/tvsettings/picture/PictureSettingItem2;->setCurrentValue(I)V

    iget-object v7, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemPhase:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    const/4 v8, 0x0

    aget-short v8, v3, v8

    invoke-virtual {v7, v8}, Lcom/konka/tvsettings/picture/PictureSettingItem2;->setCurrentValue(I)V
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_0

    :goto_5
    return-void

    :cond_4
    :try_start_4
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "============GetLogicalVPos: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v8, 0x0

    aget-short v8, v2, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V
    :try_end_4
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_4 .. :try_end_4} :catch_1

    move-object v1, v2

    goto :goto_1

    :cond_5
    :try_start_5
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "============GetLogicalVPos: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v8, 0x0

    aget-short v8, v6, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V
    :try_end_5
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_5 .. :try_end_5} :catch_2

    move-object v5, v6

    goto :goto_2

    :cond_6
    :try_start_6
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "============GetLogicalVPos: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v8, 0x0

    aget-short v8, v4, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V
    :try_end_6
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_6 .. :try_end_6} :catch_0

    goto :goto_3

    :catch_0
    move-exception v0

    :goto_6
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_5

    :cond_7
    :try_start_7
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "============GetLogicalVPos: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v8, 0x0

    aget-short v8, v3, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V
    :try_end_7
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_7 .. :try_end_7} :catch_0

    goto/16 :goto_4

    :catch_1
    move-exception v0

    move-object v5, v6

    move-object v1, v2

    goto :goto_6

    :catch_2
    move-exception v0

    move-object v5, v6

    goto :goto_6

    :cond_8
    move-object v6, v5

    move-object v2, v1

    goto/16 :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/konka/tvsettings/BaseKonkaActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f03002a    # com.konka.tvsettings.R.layout.picture_setting_pc_adj_menu

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->setContentView(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->bAutoPCSucessed:Z

    invoke-virtual {p0}, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->handler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/PictureDesk;->setHandler(Landroid/os/Handler;I)Z

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->inputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0}, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->addView()V

    invoke-direct {p0}, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->fbdItemBySource()V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->handler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/konka/tvsettings/common/LittleDownTimer;->setHandler(Landroid/os/Handler;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/PictureDesk;->releaseHandler(I)V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v2, 0x1

    sparse-switch p1, :sswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/konka/tvsettings/BaseKonkaActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    :goto_1
    :sswitch_0
    return v2

    :sswitch_1
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->finish()V

    const/4 v2, 0x0

    const v3, 0x7f040005    # com.konka.tvsettings.R.anim.anim_menu_exit

    invoke-virtual {p0, v2, v3}, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->overridePendingTransition(II)V

    goto :goto_0

    :sswitch_2
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v2, Lcom/konka/tvsettings/picture/PictureSettingActivity;

    invoke-virtual {v0, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->finish()V

    goto :goto_0

    :sswitch_3
    iget-object v3, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemPhaseLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->itemAutoAdjust:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x14 -> :sswitch_3
        0x52 -> :sswitch_2
        0xb2 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->pauseMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resumeMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onResume()V

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onStart()V

    const v0, 0x7f040009    # com.konka.tvsettings.R.anim.anim_zoom_in

    const v1, 0x7f040008    # com.konka.tvsettings.R.anim.anim_right_out

    invoke-virtual {p0, v0, v1}, Lcom/konka/tvsettings/picture/PictureSettingPCAdjustActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resetMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onUserInteraction()V

    return-void
.end method
