.class Lcom/konka/tvsettings/picture/PictureSettingActivity$9;
.super Lcom/konka/tvsettings/picture/PictureSettingItem1;
.source "PictureSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/picture/PictureSettingActivity;->addItemColorTemp()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/picture/PictureSettingActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/picture/PictureSettingActivity;Landroid/app/Activity;III)V
    .locals 0
    .param p2    # Landroid/app/Activity;
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iput-object p1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity$9;->this$0:Lcom/konka/tvsettings/picture/PictureSettingActivity;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/konka/tvsettings/picture/PictureSettingItem1;-><init>(Landroid/app/Activity;III)V

    return-void
.end method


# virtual methods
.method public doUpdate()V
    .locals 3

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity$9;->this$0:Lcom/konka/tvsettings/picture/PictureSettingActivity;

    # getter for: Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemColorTemp:Lcom/konka/tvsettings/picture/PictureSettingItem1;
    invoke-static {v1}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->access$10(Lcom/konka/tvsettings/picture/PictureSettingActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/tvsettings/picture/PictureSettingItem1;->getCurrentState()I

    move-result v0

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity$9;->this$0:Lcom/konka/tvsettings/picture/PictureSettingActivity;

    # getter for: Lcom/konka/tvsettings/picture/PictureSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v1}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->access$11(Lcom/konka/tvsettings/picture/PictureSettingActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v1

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    move-result-object v2

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PictureDesk;->SetColorTempIdx(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;)Z

    return-void
.end method
