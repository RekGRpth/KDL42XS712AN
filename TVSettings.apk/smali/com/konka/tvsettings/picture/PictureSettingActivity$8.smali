.class Lcom/konka/tvsettings/picture/PictureSettingActivity$8;
.super Lcom/konka/tvsettings/picture/PictureSettingItem1;
.source "PictureSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/picture/PictureSettingActivity;->addItemZoomMode()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/picture/PictureSettingActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/picture/PictureSettingActivity;Landroid/app/Activity;III)V
    .locals 0
    .param p2    # Landroid/app/Activity;
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iput-object p1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity$8;->this$0:Lcom/konka/tvsettings/picture/PictureSettingActivity;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/konka/tvsettings/picture/PictureSettingItem1;-><init>(Landroid/app/Activity;III)V

    return-void
.end method


# virtual methods
.method public doUpdate()V
    .locals 4

    invoke-virtual {p0}, Lcom/konka/tvsettings/picture/PictureSettingActivity$8;->getCurrentState()I

    move-result v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.konka.tv.hotkey.service.UNFREEZE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity$8;->this$0:Lcom/konka/tvsettings/picture/PictureSettingActivity;

    invoke-virtual {v2, v1}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v2, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity$8;->this$0:Lcom/konka/tvsettings/picture/PictureSettingActivity;

    # getter for: Lcom/konka/tvsettings/picture/PictureSettingActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;
    invoke-static {v2}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->access$0(Lcom/konka/tvsettings/picture/PictureSettingActivity;)Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity$8;->this$0:Lcom/konka/tvsettings/picture/PictureSettingActivity;

    iget-object v3, v3, Lcom/konka/tvsettings/picture/PictureSettingActivity;->osdModesArray:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    aget-object v3, v3, v0

    invoke-interface {v2, v3}, Lcom/konka/kkinterface/tv/PictureDesk;->SetVideoArc(Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;)Z

    iget-object v2, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity$8;->this$0:Lcom/konka/tvsettings/picture/PictureSettingActivity;

    invoke-virtual {v2}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->fbdPMUserBySourceOrOsd()V

    return-void
.end method
