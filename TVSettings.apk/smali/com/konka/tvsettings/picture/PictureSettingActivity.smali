.class public Lcom/konka/tvsettings/picture/PictureSettingActivity;
.super Lcom/konka/tvsettings/BaseKonkaActivity;
.source "PictureSettingActivity.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_PICTURE:[I

.field private static synthetic $SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$MAPI_VIDEO_ARC_Type:[I

.field private static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I


# instance fields
.field protected bIsPicValueChanged:Z

.field private channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

.field private commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

.field private inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field private itemBacklight:Lcom/konka/tvsettings/picture/PictureSettingItem2;

.field private itemBrightness:Lcom/konka/tvsettings/picture/PictureSettingItem2;

.field private itemColorTemp:Lcom/konka/tvsettings/picture/PictureSettingItem1;

.field private itemColorWheel:Lcom/konka/tvsettings/picture/PictureSettingItem1;

.field private itemContrast:Lcom/konka/tvsettings/picture/PictureSettingItem2;

.field private itemHue:Lcom/konka/tvsettings/picture/PictureSettingItem2;

.field private itemPCAdjust:Landroid/widget/LinearLayout;

.field private itemPicMode:Lcom/konka/tvsettings/picture/PictureSettingItem1;

.field private itemSaturation:Lcom/konka/tvsettings/picture/PictureSettingItem2;

.field private itemSharpness:Lcom/konka/tvsettings/picture/PictureSettingItem2;

.field private itemZoomMode:Lcom/konka/tvsettings/picture/PictureSettingItem1;

.field private mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field private mOsdMod:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

.field private myHandler:Landroid/os/Handler;

.field osdModesArray:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

.field osdStrsId:I

.field private pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

.field private serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;


# direct methods
.method static synthetic $SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_PICTURE()[I
    .locals 3

    sget-object v0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_PICTURE:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->PICTURE_DYNAMIC:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_7

    :goto_1
    :try_start_1
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->PICTURE_NATURAL:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_6

    :goto_2
    :try_start_2
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->PICTURE_NORMAL:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_5

    :goto_3
    :try_start_3
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->PICTURE_NUMS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_4

    :goto_4
    :try_start_4
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->PICTURE_SOFT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_3

    :goto_5
    :try_start_5
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->PICTURE_SPORTS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_2

    :goto_6
    :try_start_6
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->PICTURE_USER:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_1

    :goto_7
    :try_start_7
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->PICTURE_VIVID:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_0

    :goto_8
    sput-object v0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_PICTURE:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_8

    :catch_1
    move-exception v1

    goto :goto_7

    :catch_2
    move-exception v1

    goto :goto_6

    :catch_3
    move-exception v1

    goto :goto_5

    :catch_4
    move-exception v1

    goto :goto_4

    :catch_5
    move-exception v1

    goto :goto_3

    :catch_6
    move-exception v1

    goto :goto_2

    :catch_7
    move-exception v1

    goto :goto_1
.end method

.method static synthetic $SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$MAPI_VIDEO_ARC_Type()[I
    .locals 3

    sget-object v0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$MAPI_VIDEO_ARC_Type:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_14x9:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_16

    :goto_1
    :try_start_1
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_16x9:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_15

    :goto_2
    :try_start_2
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_16x9_Combind:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/16 v2, 0x13

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_14

    :goto_3
    :try_start_3
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_16x9_PanScan:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_13

    :goto_4
    :try_start_4
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_16x9_PillarBox:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_12

    :goto_5
    :try_start_5
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_4x3:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_11

    :goto_6
    :try_start_6
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_4x3_Combind:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/16 v2, 0x12

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_10

    :goto_7
    :try_start_7
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_4x3_LetterBox:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_f

    :goto_8
    :try_start_8
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_4x3_PanScan:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_e

    :goto_9
    :try_start_9
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_AUTO:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_d

    :goto_a
    :try_start_a
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_DEFAULT:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_c

    :goto_b
    :try_start_b
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_DotByDot:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_b

    :goto_c
    :try_start_c
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_JustScan:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_a

    :goto_d
    :try_start_d
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/16 v2, 0x17

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_9

    :goto_e
    :try_start_e
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_Movie:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_8

    :goto_f
    :try_start_f
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_Panorama:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_7

    :goto_10
    :try_start_10
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_Personal:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_6

    :goto_11
    :try_start_11
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_Subtitle:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_5

    :goto_12
    :try_start_12
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_Zoom1:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_4

    :goto_13
    :try_start_13
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_Zoom2:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_3

    :goto_14
    :try_start_14
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_Zoom_2x:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/16 v2, 0x14

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_14} :catch_2

    :goto_15
    :try_start_15
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_Zoom_3x:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/16 v2, 0x15

    aput v2, v0, v1
    :try_end_15
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15 .. :try_end_15} :catch_1

    :goto_16
    :try_start_16
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_Zoom_4x:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    const/16 v2, 0x16

    aput v2, v0, v1
    :try_end_16
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16 .. :try_end_16} :catch_0

    :goto_17
    sput-object v0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$MAPI_VIDEO_ARC_Type:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_17

    :catch_1
    move-exception v1

    goto :goto_16

    :catch_2
    move-exception v1

    goto :goto_15

    :catch_3
    move-exception v1

    goto :goto_14

    :catch_4
    move-exception v1

    goto :goto_13

    :catch_5
    move-exception v1

    goto :goto_12

    :catch_6
    move-exception v1

    goto :goto_11

    :catch_7
    move-exception v1

    goto :goto_10

    :catch_8
    move-exception v1

    goto :goto_f

    :catch_9
    move-exception v1

    goto :goto_e

    :catch_a
    move-exception v1

    goto :goto_d

    :catch_b
    move-exception v1

    goto/16 :goto_c

    :catch_c
    move-exception v1

    goto/16 :goto_b

    :catch_d
    move-exception v1

    goto/16 :goto_a

    :catch_e
    move-exception v1

    goto/16 :goto_9

    :catch_f
    move-exception v1

    goto/16 :goto_8

    :catch_10
    move-exception v1

    goto/16 :goto_7

    :catch_11
    move-exception v1

    goto/16 :goto_6

    :catch_12
    move-exception v1

    goto/16 :goto_5

    :catch_13
    move-exception v1

    goto/16 :goto_4

    :catch_14
    move-exception v1

    goto/16 :goto_3

    :catch_15
    move-exception v1

    goto/16 :goto_2

    :catch_16
    move-exception v1

    goto/16 :goto_1
.end method

.method static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource()[I
    .locals 3

    sget-object v0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_28

    :goto_1
    :try_start_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_27

    :goto_2
    :try_start_2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_26

    :goto_3
    :try_start_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_25

    :goto_4
    :try_start_4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_24

    :goto_5
    :try_start_5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS5:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_23

    :goto_6
    :try_start_6
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS6:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_22

    :goto_7
    :try_start_7
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS7:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_21

    :goto_8
    :try_start_8
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS8:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_20

    :goto_9
    :try_start_9
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_1f

    :goto_a
    :try_start_a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1d

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_1e

    :goto_b
    :try_start_b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x26

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_1d

    :goto_c
    :try_start_c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1e

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_1c

    :goto_d
    :try_start_d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1f

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_1b

    :goto_e
    :try_start_e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x20

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_1a

    :goto_f
    :try_start_f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x21

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_19

    :goto_10
    :try_start_10
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x22

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_18

    :goto_11
    :try_start_11
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x18

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_17

    :goto_12
    :try_start_12
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x19

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_16

    :goto_13
    :try_start_13
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1a

    aput v2, v0, v1
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_15

    :goto_14
    :try_start_14
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1b

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_14} :catch_14

    :goto_15
    :try_start_15
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1c

    aput v2, v0, v1
    :try_end_15
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15 .. :try_end_15} :catch_13

    :goto_16
    :try_start_16
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_JPEG:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x25

    aput v2, v0, v1
    :try_end_16
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16 .. :try_end_16} :catch_12

    :goto_17
    :try_start_17
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_KTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x24

    aput v2, v0, v1
    :try_end_17
    .catch Ljava/lang/NoSuchFieldError; {:try_start_17 .. :try_end_17} :catch_11

    :goto_18
    :try_start_18
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x29

    aput v2, v0, v1
    :try_end_18
    .catch Ljava/lang/NoSuchFieldError; {:try_start_18 .. :try_end_18} :catch_10

    :goto_19
    :try_start_19
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x28

    aput v2, v0, v1
    :try_end_19
    .catch Ljava/lang/NoSuchFieldError; {:try_start_19 .. :try_end_19} :catch_f

    :goto_1a
    :try_start_1a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SCART:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x15

    aput v2, v0, v1
    :try_end_1a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a .. :try_end_1a} :catch_e

    :goto_1b
    :try_start_1b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SCART2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x16

    aput v2, v0, v1
    :try_end_1b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1b .. :try_end_1b} :catch_d

    :goto_1c
    :try_start_1c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SCART_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x17

    aput v2, v0, v1
    :try_end_1c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1c .. :try_end_1c} :catch_c

    :goto_1d
    :try_start_1d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x23

    aput v2, v0, v1
    :try_end_1d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1d .. :try_end_1d} :catch_b

    :goto_1e
    :try_start_1e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x27

    aput v2, v0, v1
    :try_end_1e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1e .. :try_end_1e} :catch_a

    :goto_1f
    :try_start_1f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_1f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1f .. :try_end_1f} :catch_9

    :goto_20
    :try_start_20
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_20
    .catch Ljava/lang/NoSuchFieldError; {:try_start_20 .. :try_end_20} :catch_8

    :goto_21
    :try_start_21
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_21
    .catch Ljava/lang/NoSuchFieldError; {:try_start_21 .. :try_end_21} :catch_7

    :goto_22
    :try_start_22
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_22
    .catch Ljava/lang/NoSuchFieldError; {:try_start_22 .. :try_end_22} :catch_6

    :goto_23
    :try_start_23
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_23
    .catch Ljava/lang/NoSuchFieldError; {:try_start_23 .. :try_end_23} :catch_5

    :goto_24
    :try_start_24
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_VGA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_24
    .catch Ljava/lang/NoSuchFieldError; {:try_start_24 .. :try_end_24} :catch_4

    :goto_25
    :try_start_25
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_25
    .catch Ljava/lang/NoSuchFieldError; {:try_start_25 .. :try_end_25} :catch_3

    :goto_26
    :try_start_26
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x12

    aput v2, v0, v1
    :try_end_26
    .catch Ljava/lang/NoSuchFieldError; {:try_start_26 .. :try_end_26} :catch_2

    :goto_27
    :try_start_27
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x13

    aput v2, v0, v1
    :try_end_27
    .catch Ljava/lang/NoSuchFieldError; {:try_start_27 .. :try_end_27} :catch_1

    :goto_28
    :try_start_28
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x14

    aput v2, v0, v1
    :try_end_28
    .catch Ljava/lang/NoSuchFieldError; {:try_start_28 .. :try_end_28} :catch_0

    :goto_29
    sput-object v0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_29

    :catch_1
    move-exception v1

    goto :goto_28

    :catch_2
    move-exception v1

    goto :goto_27

    :catch_3
    move-exception v1

    goto :goto_26

    :catch_4
    move-exception v1

    goto :goto_25

    :catch_5
    move-exception v1

    goto :goto_24

    :catch_6
    move-exception v1

    goto :goto_23

    :catch_7
    move-exception v1

    goto :goto_22

    :catch_8
    move-exception v1

    goto :goto_21

    :catch_9
    move-exception v1

    goto :goto_20

    :catch_a
    move-exception v1

    goto :goto_1f

    :catch_b
    move-exception v1

    goto/16 :goto_1e

    :catch_c
    move-exception v1

    goto/16 :goto_1d

    :catch_d
    move-exception v1

    goto/16 :goto_1c

    :catch_e
    move-exception v1

    goto/16 :goto_1b

    :catch_f
    move-exception v1

    goto/16 :goto_1a

    :catch_10
    move-exception v1

    goto/16 :goto_19

    :catch_11
    move-exception v1

    goto/16 :goto_18

    :catch_12
    move-exception v1

    goto/16 :goto_17

    :catch_13
    move-exception v1

    goto/16 :goto_16

    :catch_14
    move-exception v1

    goto/16 :goto_15

    :catch_15
    move-exception v1

    goto/16 :goto_14

    :catch_16
    move-exception v1

    goto/16 :goto_13

    :catch_17
    move-exception v1

    goto/16 :goto_12

    :catch_18
    move-exception v1

    goto/16 :goto_11

    :catch_19
    move-exception v1

    goto/16 :goto_10

    :catch_1a
    move-exception v1

    goto/16 :goto_f

    :catch_1b
    move-exception v1

    goto/16 :goto_e

    :catch_1c
    move-exception v1

    goto/16 :goto_d

    :catch_1d
    move-exception v1

    goto/16 :goto_c

    :catch_1e
    move-exception v1

    goto/16 :goto_b

    :catch_1f
    move-exception v1

    goto/16 :goto_a

    :catch_20
    move-exception v1

    goto/16 :goto_9

    :catch_21
    move-exception v1

    goto/16 :goto_8

    :catch_22
    move-exception v1

    goto/16 :goto_7

    :catch_23
    move-exception v1

    goto/16 :goto_6

    :catch_24
    move-exception v1

    goto/16 :goto_5

    :catch_25
    move-exception v1

    goto/16 :goto_4

    :catch_26
    move-exception v1

    goto/16 :goto_3

    :catch_27
    move-exception v1

    goto/16 :goto_2

    :catch_28
    move-exception v1

    goto/16 :goto_1
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;-><init>()V

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_16x9:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->mOsdMod:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->bIsPicValueChanged:Z

    new-instance v0, Lcom/konka/tvsettings/picture/PictureSettingActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/picture/PictureSettingActivity$1;-><init>(Lcom/konka/tvsettings/picture/PictureSettingActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->myHandler:Landroid/os/Handler;

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemPicMode:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemBrightness:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemContrast:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemSaturation:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemSharpness:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemHue:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemZoomMode:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemColorWheel:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemColorTemp:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemBacklight:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    iput-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemPCAdjust:Landroid/widget/LinearLayout;

    const v0, 0x7f0b0019    # com.konka.tvsettings.R.array.str_pic_setting_osd_mod_tv

    iput v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->osdStrsId:I

    sget-object v0, Lcom/konka/tvsettings/picture/PictureSettingGRuleEnum;->OSD_MODE_TV:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->osdModesArray:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/picture/PictureSettingActivity;)Lcom/konka/kkinterface/tv/PictureDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/picture/PictureSettingActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem2;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemBrightness:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    return-object v0
.end method

.method static synthetic access$10(Lcom/konka/tvsettings/picture/PictureSettingActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemColorTemp:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-object v0
.end method

.method static synthetic access$11(Lcom/konka/tvsettings/picture/PictureSettingActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    return-object v0
.end method

.method static synthetic access$12(Lcom/konka/tvsettings/picture/PictureSettingActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem2;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemBacklight:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/picture/PictureSettingActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem2;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemContrast:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/picture/PictureSettingActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem2;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemSaturation:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/picture/PictureSettingActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem2;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemSharpness:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/picture/PictureSettingActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem2;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemHue:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    return-object v0
.end method

.method static synthetic access$7(Lcom/konka/tvsettings/picture/PictureSettingActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemPicMode:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-object v0
.end method

.method private addItemBacklight()V
    .locals 6

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/PictureDesk;->GetBacklight()S

    move-result v4

    new-instance v0, Lcom/konka/tvsettings/picture/PictureSettingActivity$10;

    const v3, 0x7f070110    # com.konka.tvsettings.R.id.picture_setting_backlight

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/picture/PictureSettingActivity$10;-><init>(Lcom/konka/tvsettings/picture/PictureSettingActivity;Landroid/app/Activity;III)V

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemBacklight:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    return-void
.end method

.method private addItemColorTemp()V
    .locals 7

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/PictureDesk;->GetColorTempIdx()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    move-result-object v6

    const/4 v5, 0x0

    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_COOL:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    if-ne v6, v0, :cond_0

    const/4 v5, 0x0

    :goto_0
    new-instance v0, Lcom/konka/tvsettings/picture/PictureSettingActivity$9;

    const v3, 0x7f07010f    # com.konka.tvsettings.R.id.picture_setting_colortemp

    const v4, 0x7f0b0018    # com.konka.tvsettings.R.array.str_pic_setting_arr_color_temp

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/picture/PictureSettingActivity$9;-><init>(Lcom/konka/tvsettings/picture/PictureSettingActivity;Landroid/app/Activity;III)V

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemColorTemp:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-void

    :cond_0
    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_NATURE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    if-ne v6, v0, :cond_1

    const/4 v5, 0x1

    goto :goto_0

    :cond_1
    const/4 v5, 0x2

    goto :goto_0
.end method

.method private addItemPCAdjust()V
    .locals 2

    const v0, 0x7f070111    # com.konka.tvsettings.R.id.picture_setting_pc_adjust

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemPCAdjust:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemPCAdjust:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/tvsettings/picture/PictureSettingActivity$11;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/picture/PictureSettingActivity$11;-><init>(Lcom/konka/tvsettings/picture/PictureSettingActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemPCAdjust:Landroid/widget/LinearLayout;

    const v1, 0x7f070108    # com.konka.tvsettings.R.id.picture_setting_pic_mode

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusDownId(I)V

    return-void
.end method

.method private addItemPicMode()V
    .locals 7

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/PictureDesk;->GetPictureModeIdx()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    move-result-object v6

    invoke-static {}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_PICTURE()[I

    move-result-object v0

    invoke-virtual {v6}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v5, 0x0

    :goto_0
    new-instance v0, Lcom/konka/tvsettings/picture/PictureSettingActivity$7;

    const v3, 0x7f070108    # com.konka.tvsettings.R.id.picture_setting_pic_mode

    const v4, 0x7f0b001f    # com.konka.tvsettings.R.array.str_pic_setting_arr_pic_mod

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/picture/PictureSettingActivity$7;-><init>(Lcom/konka/tvsettings/picture/PictureSettingActivity;Landroid/app/Activity;III)V

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemPicMode:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    const/4 v0, 0x4

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemPicMode:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    invoke-virtual {v1}, Lcom/konka/tvsettings/picture/PictureSettingItem1;->getCurrentState()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->updatePicModUserItemVisible(I)V

    :goto_1
    return-void

    :pswitch_0
    const/4 v5, 0x0

    goto :goto_0

    :pswitch_1
    const/4 v5, 0x1

    goto :goto_0

    :pswitch_2
    const/4 v5, 0x2

    goto :goto_0

    :pswitch_3
    const/4 v5, 0x3

    goto :goto_0

    :pswitch_4
    const/4 v5, 0x4

    goto :goto_0

    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->updatePicModUserItemVisible(I)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method private addItemPicModeUser()V
    .locals 28

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_BRIGHTNESS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-interface {v2, v3}, Lcom/konka/kkinterface/tv/PictureDesk;->GetVideoItem(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;)S

    move-result v6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_CONTRAST:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-interface {v2, v3}, Lcom/konka/kkinterface/tv/PictureDesk;->GetVideoItem(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;)S

    move-result v11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_SATURATION:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-interface {v2, v3}, Lcom/konka/kkinterface/tv/PictureDesk;->GetVideoItem(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;)S

    move-result v16

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_SHARPNESS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-interface {v2, v3}, Lcom/konka/kkinterface/tv/PictureDesk;->GetVideoItem(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;)S

    move-result v21

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_HUE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-interface {v2, v3}, Lcom/konka/kkinterface/tv/PictureDesk;->GetVideoItem(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;)S

    move-result v26

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "It is usr pic mod initialize ,and the value is "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v21

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, v26

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v2, Lcom/konka/tvsettings/picture/PictureSettingActivity$2;

    const v5, 0x7f070109    # com.konka.tvsettings.R.id.picture_setting_brightness

    const/4 v7, 0x0

    move-object/from16 v3, p0

    move-object/from16 v4, p0

    invoke-direct/range {v2 .. v7}, Lcom/konka/tvsettings/picture/PictureSettingActivity$2;-><init>(Lcom/konka/tvsettings/picture/PictureSettingActivity;Landroid/app/Activity;III)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemBrightness:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    new-instance v7, Lcom/konka/tvsettings/picture/PictureSettingActivity$3;

    const v10, 0x7f07010a    # com.konka.tvsettings.R.id.picture_setting_contrast

    const/4 v12, 0x0

    move-object/from16 v8, p0

    move-object/from16 v9, p0

    invoke-direct/range {v7 .. v12}, Lcom/konka/tvsettings/picture/PictureSettingActivity$3;-><init>(Lcom/konka/tvsettings/picture/PictureSettingActivity;Landroid/app/Activity;III)V

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemContrast:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    new-instance v12, Lcom/konka/tvsettings/picture/PictureSettingActivity$4;

    const v15, 0x7f07010b    # com.konka.tvsettings.R.id.picture_setting_saturation

    const/16 v17, 0x0

    move-object/from16 v13, p0

    move-object/from16 v14, p0

    invoke-direct/range {v12 .. v17}, Lcom/konka/tvsettings/picture/PictureSettingActivity$4;-><init>(Lcom/konka/tvsettings/picture/PictureSettingActivity;Landroid/app/Activity;III)V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemSaturation:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    new-instance v17, Lcom/konka/tvsettings/picture/PictureSettingActivity$5;

    const v20, 0x7f07010c    # com.konka.tvsettings.R.id.picture_setting_sharpness

    const/16 v22, 0x0

    move-object/from16 v18, p0

    move-object/from16 v19, p0

    invoke-direct/range {v17 .. v22}, Lcom/konka/tvsettings/picture/PictureSettingActivity$5;-><init>(Lcom/konka/tvsettings/picture/PictureSettingActivity;Landroid/app/Activity;III)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemSharpness:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    new-instance v22, Lcom/konka/tvsettings/picture/PictureSettingActivity$6;

    const v25, 0x7f07010d    # com.konka.tvsettings.R.id.picture_setting_hue

    const/16 v27, 0x0

    move-object/from16 v23, p0

    move-object/from16 v24, p0

    invoke-direct/range {v22 .. v27}, Lcom/konka/tvsettings/picture/PictureSettingActivity$6;-><init>(Lcom/konka/tvsettings/picture/PictureSettingActivity;Landroid/app/Activity;III)V

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemHue:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    return-void
.end method

.method private addItemZoomMode()V
    .locals 6

    const v3, 0x7f0b0019    # com.konka.tvsettings.R.array.str_pic_setting_osd_mod_tv

    const/4 v5, 0x0

    const-string v0, "zoom"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mOsdMod==========="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->mOsdMod:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$MAPI_VIDEO_ARC_Type()[I

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->mOsdMod:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const/4 v5, 0x0

    :cond_0
    :goto_0
    invoke-static {}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource()[I

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    :pswitch_1
    iput v3, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->osdStrsId:I

    sget-object v0, Lcom/konka/tvsettings/picture/PictureSettingGRuleEnum;->OSD_MODE_TV:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->osdModesArray:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    :goto_1
    const-string v0, "zoom"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "index==========="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/konka/tvsettings/picture/PictureSettingActivity$8;

    const v3, 0x7f07010e    # com.konka.tvsettings.R.id.picture_setting_zoom_mode

    iget v4, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->osdStrsId:I

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/picture/PictureSettingActivity$8;-><init>(Lcom/konka/tvsettings/picture/PictureSettingActivity;Landroid/app/Activity;III)V

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemZoomMode:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-void

    :pswitch_2
    const/4 v5, 0x0

    goto :goto_0

    :pswitch_3
    const/4 v5, 0x1

    goto :goto_0

    :pswitch_4
    const/4 v5, 0x2

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_VGA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v0, v1, :cond_1

    const/4 v5, 0x2

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v0, v1, :cond_0

    :cond_2
    const/4 v5, 0x3

    goto :goto_0

    :pswitch_6
    const/4 v5, 0x3

    goto :goto_0

    :pswitch_7
    const/4 v5, 0x4

    goto :goto_0

    :pswitch_8
    iput v3, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->osdStrsId:I

    sget-object v0, Lcom/konka/tvsettings/picture/PictureSettingGRuleEnum;->OSD_MODE_TV:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->osdModesArray:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    goto :goto_1

    :pswitch_9
    const v0, 0x7f0b001a    # com.konka.tvsettings.R.array.str_pic_setting_osd_mod_ypbpr

    iput v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->osdStrsId:I

    sget-object v0, Lcom/konka/tvsettings/picture/PictureSettingGRuleEnum;->OSD_MODE_YPBPR:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->osdModesArray:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    goto :goto_1

    :pswitch_a
    const v0, 0x7f0b001b    # com.konka.tvsettings.R.array.str_pic_setting_osd_mod_vga

    iput v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->osdStrsId:I

    sget-object v0, Lcom/konka/tvsettings/picture/PictureSettingGRuleEnum;->OSD_MODE_VGA:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->osdModesArray:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    goto :goto_1

    :pswitch_b
    const v0, 0x7f0b001d    # com.konka.tvsettings.R.array.str_pic_setting_osd_mod_hdmi

    iput v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->osdStrsId:I

    sget-object v0, Lcom/konka/tvsettings/picture/PictureSettingGRuleEnum;->OSD_MODE_HDMI:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->osdModesArray:[Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_4
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_a
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_1
        :pswitch_8
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_8
    .end packed-switch
.end method

.method private fbdAdvAndPCAdjBlBySource()V
    .locals 3

    invoke-static {}, Lcom/konka/tvsettings/SwitchMenuHelper;->get4K2KMode()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->setItemPCAdjustFbd()V

    :goto_0
    :pswitch_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    invoke-static {}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource()[I

    move-result-object v1

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :pswitch_1
    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->setItemPCAdjustFbd()V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->setItemPCAdjustFbd()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private fbdZoomMode()V
    .locals 3

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->getCurrent3dFormat()Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-ne v1, v2, :cond_0

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getS3DManagerInstance()Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/S3DDesk;->getDisplay3DTo2DMode()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    if-ne v1, v2, :cond_0

    invoke-static {}, Lcom/konka/tvsettings/SwitchMenuHelper;->get4K2KMode()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemZoomMode:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    invoke-virtual {v1}, Lcom/konka/tvsettings/picture/PictureSettingItem1;->setStatusFbd()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private setItemPCAdjustFbd()V
    .locals 5

    const v4, -0xcdcdce

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemPCAdjust:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v2, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemPCAdjust:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    iget-object v2, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemPCAdjust:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v2, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemPCAdjust:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v2, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemPCAdjust:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    return-void
.end method


# virtual methods
.method public addView()V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->addItemPicModeUser()V

    invoke-direct {p0}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->addItemPicMode()V

    invoke-direct {p0}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->addItemZoomMode()V

    invoke-direct {p0}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->addItemColorTemp()V

    invoke-direct {p0}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->addItemBacklight()V

    invoke-direct {p0}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->addItemPCAdjust()V

    return-void
.end method

.method public fbdBacklightBy3DOrEnergySaving()V
    .locals 3

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->getCurrent3dFormat()Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->get3DImplType()Lcom/konka/kkinterface/tv/CommonDesk$TypeOf3DImpl;

    move-result-object v1

    iget v1, v1, Lcom/konka/kkinterface/tv/CommonDesk$TypeOf3DImpl;->m_typeOf3DImpl:I

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_3D_IMPL_TYPE;->MS_3DTYPE_SG:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_3D_IMPL_TYPE;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_3D_IMPL_TYPE;->ordinal()I

    move-result v2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemBacklight:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    invoke-virtual {v1}, Lcom/konka/tvsettings/picture/PictureSettingItem2;->setStatusFbd()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public fbdPMUserBySourceOrOsd()V
    .locals 2

    invoke-static {}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource()[I

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    aget v0, v0, v1

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->isSignalStabled()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->NTSC_M:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->atvGetVideoSystem()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    move-result-object v1

    if-eq v0, v1, :cond_0

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->NTSC_44:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->getVideoStandard()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    move-result-object v1

    if-eq v0, v1, :cond_0

    :cond_1
    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemHue:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    invoke-virtual {v0}, Lcom/konka/tvsettings/picture/PictureSettingItem2;->setStatusFbd()V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->isSignalStabled()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->NTSC_M:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->getVideoStandard()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    move-result-object v1

    if-eq v0, v1, :cond_0

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->NTSC_44:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->getVideoStandard()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    move-result-object v1

    if-eq v0, v1, :cond_0

    :cond_2
    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemHue:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    invoke-virtual {v0}, Lcom/konka/tvsettings/picture/PictureSettingItem2;->setStatusFbd()V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemSharpness:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    invoke-virtual {v0}, Lcom/konka/tvsettings/picture/PictureSettingItem2;->setStatusFbd()V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/PictureDesk;->GetVideoArc()Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    move-result-object v0

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_DotByDot:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemSaturation:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    invoke-virtual {v0}, Lcom/konka/tvsettings/picture/PictureSettingItem2;->setStatusFbd()V

    :goto_1
    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemHue:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    invoke-virtual {v0}, Lcom/konka/tvsettings/picture/PictureSettingItem2;->setStatusFbd()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemSaturation:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    invoke-virtual {v0}, Lcom/konka/tvsettings/picture/PictureSettingItem2;->setStatusNor()V

    goto :goto_1

    :sswitch_3
    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->isHdmiSignalMode()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemSharpness:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    invoke-virtual {v0}, Lcom/konka/tvsettings/picture/PictureSettingItem2;->setStatusFbd()V

    :cond_4
    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/PictureDesk;->GetVideoArc()Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    move-result-object v0

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_DotByDot:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemSaturation:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    invoke-virtual {v0}, Lcom/konka/tvsettings/picture/PictureSettingItem2;->setStatusFbd()V

    :goto_2
    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemHue:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    invoke-virtual {v0}, Lcom/konka/tvsettings/picture/PictureSettingItem2;->setStatusFbd()V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemSaturation:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    invoke-virtual {v0}, Lcom/konka/tvsettings/picture/PictureSettingItem2;->setStatusNor()V

    goto :goto_2

    :sswitch_4
    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemHue:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    invoke-virtual {v0}, Lcom/konka/tvsettings/picture/PictureSettingItem2;->setStatusFbd()V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x2 -> :sswitch_0
        0x3 -> :sswitch_1
        0x4 -> :sswitch_1
        0x11 -> :sswitch_4
        0x12 -> :sswitch_4
        0x18 -> :sswitch_3
        0x19 -> :sswitch_3
        0x1a -> :sswitch_3
        0x1b -> :sswitch_3
        0x1d -> :sswitch_4
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/konka/tvsettings/BaseKonkaActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030029    # com.konka.tvsettings.R.layout.picture_setting_menu

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->setContentView(I)V

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/PictureDesk;->GetVideoArc()Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->mOsdMod:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "=======input mode: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; osd mod: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->mOsdMod:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "====="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->addView()V

    invoke-direct {p0}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->fbdZoomMode()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->fbdPMUserBySourceOrOsd()V

    invoke-direct {p0}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->fbdAdvAndPCAdjBlBySource()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->fbdBacklightBy3DOrEnergySaving()V

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->myHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/konka/tvsettings/common/LittleDownTimer;->setHandler(Landroid/os/Handler;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const v4, 0x7f040005    # com.konka.tvsettings.R.anim.anim_menu_exit

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch p1, :sswitch_data_0

    :goto_0
    invoke-super {p0, p1, p2}, Lcom/konka/tvsettings/BaseKonkaActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    :goto_1
    return v2

    :sswitch_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->finish()V

    invoke-virtual {p0, v3, v4}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->overridePendingTransition(II)V

    :sswitch_1
    const-string v2, "Exit"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->finish()V

    invoke-virtual {p0, v3, v4}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->overridePendingTransition(II)V

    goto :goto_0

    :sswitch_2
    const/4 v2, 0x1

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x52 -> :sswitch_1
        0xb2 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 1

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->pauseMenu()V

    iget-boolean v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->bIsPicValueChanged:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/PictureDesk;->UpdateVideoItem()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->bIsPicValueChanged:Z

    const-string v0, "onPause()===>> is UpdateVideoItem database"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :cond_0
    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resumeMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onResume()V

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onStart()V

    const v0, 0x7f040007    # com.konka.tvsettings.R.anim.anim_right_in

    const v1, 0x7f04000a    # com.konka.tvsettings.R.anim.anim_zoom_out

    invoke-virtual {p0, v0, v1}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resetMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onUserInteraction()V

    return-void
.end method

.method protected updatePicModUserItemVisible(I)V
    .locals 7
    .param p1    # I

    if-nez p1, :cond_0

    iget-object v5, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    sget-object v6, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_BRIGHTNESS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-interface {v5, v6}, Lcom/konka/kkinterface/tv/PictureDesk;->GetVideoItem(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;)S

    move-result v0

    iget-object v5, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    sget-object v6, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_CONTRAST:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-interface {v5, v6}, Lcom/konka/kkinterface/tv/PictureDesk;->GetVideoItem(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;)S

    move-result v1

    iget-object v5, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    sget-object v6, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_SATURATION:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-interface {v5, v6}, Lcom/konka/kkinterface/tv/PictureDesk;->GetVideoItem(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;)S

    move-result v3

    iget-object v5, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    sget-object v6, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_SHARPNESS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-interface {v5, v6}, Lcom/konka/kkinterface/tv/PictureDesk;->GetVideoItem(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;)S

    move-result v4

    iget-object v5, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    sget-object v6, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_HUE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-interface {v5, v6}, Lcom/konka/kkinterface/tv/PictureDesk;->GetVideoItem(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_VIDEOITEM;)S

    move-result v2

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "It is usr pic mod initialize ,and the value is "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemBrightness:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    invoke-virtual {v5, v0}, Lcom/konka/tvsettings/picture/PictureSettingItem2;->setCurrentValue(I)V

    iget-object v5, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemContrast:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    invoke-virtual {v5, v1}, Lcom/konka/tvsettings/picture/PictureSettingItem2;->setCurrentValue(I)V

    iget-object v5, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemSaturation:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    invoke-virtual {v5, v3}, Lcom/konka/tvsettings/picture/PictureSettingItem2;->setCurrentValue(I)V

    iget-object v5, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemSharpness:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    invoke-virtual {v5, v4}, Lcom/konka/tvsettings/picture/PictureSettingItem2;->setCurrentValue(I)V

    iget-object v5, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemHue:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    invoke-virtual {v5, v2}, Lcom/konka/tvsettings/picture/PictureSettingItem2;->setCurrentValue(I)V

    :cond_0
    iget-object v5, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemBrightness:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    invoke-virtual {v5, p1}, Lcom/konka/tvsettings/picture/PictureSettingItem2;->setVisibility(I)V

    iget-object v5, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemContrast:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    invoke-virtual {v5, p1}, Lcom/konka/tvsettings/picture/PictureSettingItem2;->setVisibility(I)V

    iget-object v5, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemSaturation:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    invoke-virtual {v5, p1}, Lcom/konka/tvsettings/picture/PictureSettingItem2;->setVisibility(I)V

    iget-object v5, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemSharpness:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    invoke-virtual {v5, p1}, Lcom/konka/tvsettings/picture/PictureSettingItem2;->setVisibility(I)V

    iget-object v5, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemHue:Lcom/konka/tvsettings/picture/PictureSettingItem2;

    invoke-virtual {v5, p1}, Lcom/konka/tvsettings/picture/PictureSettingItem2;->setVisibility(I)V

    return-void
.end method
