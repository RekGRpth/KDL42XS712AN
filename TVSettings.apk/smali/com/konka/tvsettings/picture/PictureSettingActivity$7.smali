.class Lcom/konka/tvsettings/picture/PictureSettingActivity$7;
.super Lcom/konka/tvsettings/picture/PictureSettingItem1;
.source "PictureSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/picture/PictureSettingActivity;->addItemPicMode()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/picture/PictureSettingActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/picture/PictureSettingActivity;Landroid/app/Activity;III)V
    .locals 0
    .param p2    # Landroid/app/Activity;
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iput-object p1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity$7;->this$0:Lcom/konka/tvsettings/picture/PictureSettingActivity;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/konka/tvsettings/picture/PictureSettingItem1;-><init>(Landroid/app/Activity;III)V

    return-void
.end method


# virtual methods
.method public doUpdate()V
    .locals 4

    const/16 v3, 0x8

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity$7;->this$0:Lcom/konka/tvsettings/picture/PictureSettingActivity;

    # getter for: Lcom/konka/tvsettings/picture/PictureSettingActivity;->itemPicMode:Lcom/konka/tvsettings/picture/PictureSettingItem1;
    invoke-static {v1}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->access$7(Lcom/konka/tvsettings/picture/PictureSettingActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/tvsettings/picture/PictureSettingItem1;->getCurrentState()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "pic mode: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    packed-switch v0, :pswitch_data_0

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity$7;->this$0:Lcom/konka/tvsettings/picture/PictureSettingActivity;

    # getter for: Lcom/konka/tvsettings/picture/PictureSettingActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->access$0(Lcom/konka/tvsettings/picture/PictureSettingActivity;)Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->PICTURE_DYNAMIC:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PictureDesk;->SetPictureModeIdx(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;)Z

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity$7;->this$0:Lcom/konka/tvsettings/picture/PictureSettingActivity;

    invoke-virtual {v1, v3}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->updatePicModUserItemVisible(I)V

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity$7;->this$0:Lcom/konka/tvsettings/picture/PictureSettingActivity;

    # getter for: Lcom/konka/tvsettings/picture/PictureSettingActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->access$0(Lcom/konka/tvsettings/picture/PictureSettingActivity;)Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->PICTURE_NORMAL:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PictureDesk;->SetPictureModeIdx(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;)Z

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity$7;->this$0:Lcom/konka/tvsettings/picture/PictureSettingActivity;

    invoke-virtual {v1, v3}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->updatePicModUserItemVisible(I)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity$7;->this$0:Lcom/konka/tvsettings/picture/PictureSettingActivity;

    # getter for: Lcom/konka/tvsettings/picture/PictureSettingActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->access$0(Lcom/konka/tvsettings/picture/PictureSettingActivity;)Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->PICTURE_SOFT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PictureDesk;->SetPictureModeIdx(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;)Z

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity$7;->this$0:Lcom/konka/tvsettings/picture/PictureSettingActivity;

    invoke-virtual {v1, v3}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->updatePicModUserItemVisible(I)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity$7;->this$0:Lcom/konka/tvsettings/picture/PictureSettingActivity;

    # getter for: Lcom/konka/tvsettings/picture/PictureSettingActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->access$0(Lcom/konka/tvsettings/picture/PictureSettingActivity;)Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->PICTURE_VIVID:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PictureDesk;->SetPictureModeIdx(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;)Z

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity$7;->this$0:Lcom/konka/tvsettings/picture/PictureSettingActivity;

    invoke-virtual {v1, v3}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->updatePicModUserItemVisible(I)V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity$7;->this$0:Lcom/konka/tvsettings/picture/PictureSettingActivity;

    # getter for: Lcom/konka/tvsettings/picture/PictureSettingActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->access$0(Lcom/konka/tvsettings/picture/PictureSettingActivity;)Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;->PICTURE_USER:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PictureDesk;->SetPictureModeIdx(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_PICTURE;)Z

    iget-object v1, p0, Lcom/konka/tvsettings/picture/PictureSettingActivity$7;->this$0:Lcom/konka/tvsettings/picture/PictureSettingActivity;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/konka/tvsettings/picture/PictureSettingActivity;->updatePicModUserItemVisible(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
