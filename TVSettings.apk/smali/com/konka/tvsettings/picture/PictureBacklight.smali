.class public Lcom/konka/tvsettings/picture/PictureBacklight;
.super Lcom/konka/tvsettings/picture/PictureSettingItem2;
.source "PictureBacklight.java"


# direct methods
.method public constructor <init>(Landroid/app/Activity;III)V
    .locals 0
    .param p1    # Landroid/app/Activity;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/konka/tvsettings/picture/PictureSettingItem2;-><init>(Landroid/app/Activity;III)V

    return-void
.end method


# virtual methods
.method public valueDec()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureBacklight;->mSeekBar:Landroid/widget/SeekBar;

    const/16 v1, -0xa

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->incrementProgressBy(I)V

    return-void
.end method

.method public valueInc()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/picture/PictureBacklight;->mSeekBar:Landroid/widget/SeekBar;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->incrementProgressBy(I)V

    return-void
.end method
