.class Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;
.super Landroid/widget/RelativeLayout;
.source "InputSettingActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/input/InputSettingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "InputItem"
.end annotation


# instance fields
.field public mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field private mPos:I

.field private mText:Ljava/lang/String;

.field final synthetic this$0:Lcom/konka/tvsettings/input/InputSettingActivity;

.field private tv:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/konka/tvsettings/input/InputSettingActivity;Landroid/content/Context;Ljava/lang/String;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;I)V
    .locals 6
    .param p2    # Landroid/content/Context;
    .param p3    # Ljava/lang/String;
    .param p4    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .param p5    # I

    const/4 v5, 0x1

    const/4 v4, -0x2

    const/4 v3, 0x0

    iput-object p1, p0, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->this$0:Lcom/konka/tvsettings/input/InputSettingActivity;

    invoke-direct {p0, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    const/4 v2, 0x0

    iput v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->mPos:I

    iput-object v3, p0, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->mText:Ljava/lang/String;

    iput-object v3, p0, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->tv:Landroid/widget/TextView;

    iput-object p4, p0, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput p5, p0, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->mPos:I

    iput-object p3, p0, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->mText:Ljava/lang/String;

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    # getter for: Lcom/konka/tvsettings/input/InputSettingActivity;->WIDTH:I
    invoke-static {p1}, Lcom/konka/tvsettings/input/InputSettingActivity;->access$0(Lcom/konka/tvsettings/input/InputSettingActivity;)I

    move-result v2

    # getter for: Lcom/konka/tvsettings/input/InputSettingActivity;->HEIGHT:I
    invoke-static {p1}, Lcom/konka/tvsettings/input/InputSettingActivity;->access$1(Lcom/konka/tvsettings/input/InputSettingActivity;)I

    move-result v3

    invoke-direct {v0, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0, v5}, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->setFocusable(Z)V

    invoke-virtual {p0, v5}, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->setFocusableInTouchMode(Z)V

    invoke-virtual {p0, p0}, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, p0}, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    const v2, 0x7f02006f    # com.konka.tvsettings.R.drawable.input_menu_item_state

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->setBackgroundResource(I)V

    new-instance v2, Landroid/widget/TextView;

    invoke-direct {v2, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->tv:Landroid/widget/TextView;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0xd

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->tv:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->tv:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->mText:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->tv:Landroid/widget/TextView;

    const/4 v3, 0x2

    const/high16 v4, 0x41a00000    # 20.0f

    invoke-virtual {v2, v3, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->tv:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080002    # com.konka.tvsettings.R.color.text_normal_col

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->tv:Landroid/widget/TextView;

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    iget-object v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->this$0:Lcom/konka/tvsettings/input/InputSettingActivity;

    iget-object v3, p0, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-static {v2, v3}, Lcom/konka/tvsettings/input/InputSettingActivity;->access$2(Lcom/konka/tvsettings/input/InputSettingActivity;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V

    iget-object v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->this$0:Lcom/konka/tvsettings/input/InputSettingActivity;

    new-instance v3, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v3}, Lcom/konka/tvsettings/RootActivity;-><init>()V

    invoke-static {v2, v3}, Lcom/konka/tvsettings/input/InputSettingActivity;->access$3(Lcom/konka/tvsettings/input/InputSettingActivity;Lcom/konka/tvsettings/RootActivity;)V

    iget-object v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->this$0:Lcom/konka/tvsettings/input/InputSettingActivity;

    # getter for: Lcom/konka/tvsettings/input/InputSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;
    invoke-static {v2}, Lcom/konka/tvsettings/input/InputSettingActivity;->access$4(Lcom/konka/tvsettings/input/InputSettingActivity;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v2, v3, :cond_0

    :try_start_0
    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPvrManagerInstance()Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/PvrDesk;->isTimeShiftRecording()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPvrManagerInstance()Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/PvrDesk;->stopTimeShift()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->this$0:Lcom/konka/tvsettings/input/InputSettingActivity;

    # getter for: Lcom/konka/tvsettings/input/InputSettingActivity;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    invoke-static {v2}, Lcom/konka/tvsettings/input/InputSettingActivity;->access$5(Lcom/konka/tvsettings/input/InputSettingActivity;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v2, v3, :cond_1

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.konka.mm"

    const-string v3, "com.konka.mm.filemanager.FileDiskActivity"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->this$0:Lcom/konka/tvsettings/input/InputSettingActivity;

    invoke-virtual {v2, v1}, Lcom/konka/tvsettings/input/InputSettingActivity;->startActivity(Landroid/content/Intent;)V

    :goto_1
    iget-object v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->this$0:Lcom/konka/tvsettings/input/InputSettingActivity;

    invoke-virtual {v2}, Lcom/konka/tvsettings/input/InputSettingActivity;->finish()V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->this$0:Lcom/konka/tvsettings/input/InputSettingActivity;

    # getter for: Lcom/konka/tvsettings/input/InputSettingActivity;->rootActivity:Lcom/konka/tvsettings/RootActivity;
    invoke-static {v2}, Lcom/konka/tvsettings/input/InputSettingActivity;->access$6(Lcom/konka/tvsettings/input/InputSettingActivity;)Lcom/konka/tvsettings/RootActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->this$0:Lcom/konka/tvsettings/input/InputSettingActivity;

    # getter for: Lcom/konka/tvsettings/input/InputSettingActivity;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    invoke-static {v3}, Lcom/konka/tvsettings/input/InputSettingActivity;->access$5(Lcom/konka/tvsettings/input/InputSettingActivity;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->this$0:Lcom/konka/tvsettings/input/InputSettingActivity;

    invoke-virtual {v4}, Lcom/konka/tvsettings/input/InputSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/konka/tvsettings/RootActivity;->setTVInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Landroid/content/Context;)V

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->this$0:Lcom/konka/tvsettings/input/InputSettingActivity;

    const-class v3, Lcom/konka/tvsettings/popup/SourceInfoActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->this$0:Lcom/konka/tvsettings/input/InputSettingActivity;

    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    const/16 v4, 0x320

    invoke-static {v2, v3, v1, v4}, Lcom/konka/tvsettings/SwitchMenuHelper;->delay2ShowMenu(Landroid/app/Activity;Landroid/os/Handler;Landroid/content/Intent;I)V

    iget-object v2, p0, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->this$0:Lcom/konka/tvsettings/input/InputSettingActivity;

    invoke-static {v2}, Lcom/konka/tvsettings/SwitchMenuHelper;->unFfeeze(Landroid/app/Activity;)V

    goto :goto_1
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Z

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->tv:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080001    # com.konka.tvsettings.R.color.text_select_col

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->tv:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080002    # com.konka.tvsettings.R.color.text_normal_col

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public requestFocus(ILandroid/graphics/Rect;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/graphics/Rect;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/input/InputSettingActivity$InputItem;->playSoundEffect(I)V

    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method
