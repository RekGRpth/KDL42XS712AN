.class public final Lcom/konka/tvsettings/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final MainSettingItem_Width:I = 0x7f090002

.field public static final SCREEN_HEIGHT:I = 0x7f090001

.field public static final SCREEN_WIDTH:I = 0x7f090000

.field public static final SettingStringOption_Height:I = 0x7f09000a

.field public static final SettingStringOption_Width:I = 0x7f090009

.field public static final SettingStringOption_bottomHeight:I = 0x7f09000c

.field public static final SettingStringOption_textSize:I = 0x7f09000d

.field public static final SettingStringOption_topHeight:I = 0x7f09000b

.field public static final TVSettingInputItem_Height:I = 0x7f090008

.field public static final TVSettingInputItem_Width:I = 0x7f090007

.field public static final TVSettingIntellControl_Height:I = 0x7f09001e

.field public static final TVSettingIntellControl_LeftMargin:I = 0x7f09001f

.field public static final TVSettingIntellControl_Width:I = 0x7f09001d

.field public static final TVSettingItemPicAdjust_Height1:I = 0x7f09001b

.field public static final TVSettingItemPicAdjust_Height2:I = 0x7f09001c

.field public static final TVSettingItemPicAdjust_Width:I = 0x7f09001a

.field public static final TVSettingPicture_Width:I = 0x7f090019

.field public static final TVSettingS3dEnItemBottom_Height:I = 0x7f090017

.field public static final TVSettingS3dEnItemTop_Height:I = 0x7f090016

.field public static final TVSettingS3dEnItem_Height:I = 0x7f090015

.field public static final TVSettingS3dEnMenu_Height:I = 0x7f090014

.field public static final TVSettingS3dEnMenu_Width:I = 0x7f090013

.field public static final TVSettingS3dEnPaddingLeft:I = 0x7f090018

.field public static final TVSettingS3dItemBottom_Height:I = 0x7f090011

.field public static final TVSettingS3dItemTextWidth:I = 0x7f090012

.field public static final TVSettingS3dItemTop_Height:I = 0x7f090010

.field public static final TVSettingS3dItem_Height:I = 0x7f09000f

.field public static final TVSettingS3dMenu_Width:I = 0x7f09000e

.field public static final TVSettingStateColumn_DetectHeight:I = 0x7f090020

.field public static final TVWidget_Height:I = 0x7f090004

.field public static final TVWidget_Width:I = 0x7f090003

.field public static final TVWidget_rightPading:I = 0x7f090005

.field public static final TVWidget_topPading:I = 0x7f090006

.field public static final VirtualPadHeight:I = 0x7f090022

.field public static final VirtualPadPaddingRight:I = 0x7f090023

.field public static final VirtualPadPaddingTop:I = 0x7f090024

.field public static final VirtualPadWidth:I = 0x7f090021


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
