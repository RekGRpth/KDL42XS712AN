.class public Lcom/konka/tvsettings/AD;
.super Landroid/app/Activity;
.source "AD.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;
    }
.end annotation


# instance fields
.field private Off:Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;

.field private On:Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;

.field private serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/AD;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iput-object v0, p0, Lcom/konka/tvsettings/AD;->On:Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;

    iput-object v0, p0, Lcom/konka/tvsettings/AD;->Off:Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/AD;)Lcom/konka/kkinterface/tv/TvDeskProvider;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/AD;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/AD;)Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/AD;->On:Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/AD;)Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/AD;->Off:Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const/16 v6, 0x11

    const/4 v5, 0x0

    const/4 v4, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/high16 v2, 0x7f030000    # com.konka.tvsettings.R.layout.activity_ad

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/AD;->setContentView(I)V

    new-instance v2, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;

    const v3, 0x7f0a0004    # com.konka.tvsettings.R.string.common_true_on

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/AD;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p0, p0, v3}, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;-><init>(Lcom/konka/tvsettings/AD;Landroid/content/Context;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/konka/tvsettings/AD;->On:Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;

    new-instance v2, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;

    const v3, 0x7f0a0005    # com.konka.tvsettings.R.string.common_false_off

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/AD;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p0, p0, v3}, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;-><init>(Lcom/konka/tvsettings/AD;Landroid/content/Context;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/konka/tvsettings/AD;->Off:Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;

    const v2, 0x7f070004    # com.konka.tvsettings.R.id.ad_container

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/AD;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setGravity(I)V

    iget-object v2, p0, Lcom/konka/tvsettings/AD;->On:Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;

    invoke-virtual {v2, v6}, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->setGravity(I)V

    iget-object v2, p0, Lcom/konka/tvsettings/AD;->Off:Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;

    invoke-virtual {v2, v6}, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->setGravity(I)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0xc

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    iget-object v2, p0, Lcom/konka/tvsettings/AD;->On:Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;

    invoke-virtual {v0, v2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/konka/tvsettings/AD;->Off:Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;

    invoke-virtual {v0, v2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/tvsettings/AD;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v2, p0, Lcom/konka/tvsettings/AD;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/SoundDesk;->getADEnable()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/AD;->On:Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;

    invoke-virtual {v2, v5}, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->setEnabled(Z)V

    iget-object v2, p0, Lcom/konka/tvsettings/AD;->On:Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;

    invoke-virtual {v2, v5}, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->setFocusable(Z)V

    iget-object v2, p0, Lcom/konka/tvsettings/AD;->Off:Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;

    invoke-virtual {v2, v4}, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->setEnabled(Z)V

    iget-object v2, p0, Lcom/konka/tvsettings/AD;->Off:Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;

    invoke-virtual {v2, v4}, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->setFocusable(Z)V

    iget-object v2, p0, Lcom/konka/tvsettings/AD;->Off:Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;

    invoke-virtual {v2}, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->requestFocus()Z

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/konka/tvsettings/AD;->Off:Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;

    invoke-virtual {v2, v5}, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->setEnabled(Z)V

    iget-object v2, p0, Lcom/konka/tvsettings/AD;->Off:Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;

    invoke-virtual {v2, v5}, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->setFocusable(Z)V

    iget-object v2, p0, Lcom/konka/tvsettings/AD;->On:Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;

    invoke-virtual {v2, v4}, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->setEnabled(Z)V

    iget-object v2, p0, Lcom/konka/tvsettings/AD;->On:Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;

    invoke-virtual {v2, v4}, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->setFocusable(Z)V

    iget-object v2, p0, Lcom/konka/tvsettings/AD;->On:Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;

    invoke-virtual {v2}, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->requestFocus()Z

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1    # Landroid/view/Menu;

    invoke-virtual {p0}, Lcom/konka/tvsettings/AD;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const/high16 v1, 0x7f0c0000    # com.konka.tvsettings.R.menu.ad

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const/4 v0, 0x1

    return v0
.end method
