.class Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$2;
.super Landroid/content/BroadcastReceiver;
.source "ScreenSaverActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$2;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.konka.tv.action.SIGNAL_LOCK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$2;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->textView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$0(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$2;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->mRefreshTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$5(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)Ljava/util/Timer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$2;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->mRefreshTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$5(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$2;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->mCountDownTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$6(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)Ljava/util/Timer;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$2;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->mCountDownTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$6(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    :cond_1
    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$2;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    invoke-virtual {v0}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->finish()V

    :cond_2
    return-void
.end method
