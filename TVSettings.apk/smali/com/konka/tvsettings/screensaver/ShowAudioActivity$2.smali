.class Lcom/konka/tvsettings/screensaver/ShowAudioActivity$2;
.super Landroid/content/BroadcastReceiver;
.source "ShowAudioActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/screensaver/ShowAudioActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$2;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$2;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->textView:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$0(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;)Landroid/widget/TextView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$2;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->mRefreshTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$5(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;)Ljava/util/Timer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$2;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->mRefreshTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$5(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$2;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->mCountDownTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$6(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;)Ljava/util/Timer;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$2;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->mCountDownTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->access$6(Lcom/konka/tvsettings/screensaver/ShowAudioActivity;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    :cond_1
    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ShowAudioActivity$2;->this$0:Lcom/konka/tvsettings/screensaver/ShowAudioActivity;

    invoke-virtual {v0}, Lcom/konka/tvsettings/screensaver/ShowAudioActivity;->finish()V

    return-void
.end method
