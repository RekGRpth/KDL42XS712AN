.class Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$3;
.super Ljava/util/TimerTask;
.source "ScreenSaverActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, -0x2

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    iget-object v1, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->textView:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$0(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getWidth()I

    move-result v1

    invoke-static {v0, v1}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$7(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;I)V

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    iget-object v1, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->textView:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$0(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$8(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;I)V

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    iget-object v1, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->textView:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$0(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getLeft()I

    move-result v1

    invoke-static {v0, v1}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$9(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;I)V

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    iget-object v1, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->textView:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$0(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TextView;->getTop()I

    move-result v1

    invoke-static {v0, v1}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$10(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;I)V

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->posY:I
    invoke-static {v0}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$2(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)I

    move-result v1

    iget-object v2, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->Y_STEP:I
    invoke-static {v2}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$11(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)I

    move-result v2

    add-int/2addr v1, v2

    invoke-static {v0, v1}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$10(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;I)V

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->posX:I
    invoke-static {v0}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$1(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)I

    move-result v1

    iget-object v2, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->X_STEP:I
    invoke-static {v2}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$12(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)I

    move-result v2

    add-int/2addr v1, v2

    invoke-static {v0, v1}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$9(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;I)V

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->posX:I
    invoke-static {v0}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$1(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)I

    move-result v0

    iget-object v1, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->TEXT_WIDTH:I
    invoke-static {v1}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$3(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->SCREEN_WIDTH:I
    invoke-static {v1}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$13(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)I

    move-result v1

    if-le v0, v1, :cond_2

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    invoke-static {v0, v3}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$14(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;I)V

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->posX:I
    invoke-static {v0}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$1(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-static {v0, v1}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$9(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;I)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->posY:I
    invoke-static {v0}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$2(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)I

    move-result v0

    iget-object v1, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->TEXT_HEIGHT:I
    invoke-static {v1}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$4(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->SCREEN_HEIGHT:I
    invoke-static {v1}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$15(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)I

    move-result v1

    if-le v0, v1, :cond_3

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    invoke-static {v0, v3}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$16(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;I)V

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->posY:I
    invoke-static {v0}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$2(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-static {v0, v1}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$10(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;I)V

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$17(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x3e9

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void

    :cond_2
    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->posX:I
    invoke-static {v0}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$1(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)I

    move-result v0

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    invoke-static {v0, v4}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$14(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;I)V

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->posX:I
    invoke-static {v0}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$1(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$9(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->posY:I
    invoke-static {v0}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$2(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)I

    move-result v0

    if-gez v0, :cond_1

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    invoke-static {v0, v4}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$16(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;I)V

    iget-object v0, p0, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity$3;->this$0:Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;

    # getter for: Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->posY:I
    invoke-static {v0}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$2(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;)I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1}, Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;->access$10(Lcom/konka/tvsettings/screensaver/ScreenSaverActivity;I)V

    goto :goto_1
.end method
