.class public Lcom/konka/tvsettings/common/CountDownTimer;
.super Ljava/lang/Object;
.source "CountDownTimer.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final TICK_SECONDS:I

.field private m_bPause:Z

.field private m_bRunning:Z

.field private m_bStop:Z

.field private m_handler:Landroid/os/Handler;

.field private m_iBackMsg:I

.field private m_iCount:I

.field private m_iCountDownTime:I

.field private m_iTime:I

.field private m_sBackMsg:I

.field private m_sCountDownTime:I


# direct methods
.method public constructor <init>(IILandroid/os/Handler;II)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/os/Handler;
    .param p4    # I
    .param p5    # I

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_bStop:Z

    iput-boolean v1, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_bPause:Z

    iput v1, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_iBackMsg:I

    const/16 v0, 0x3e8

    iput v0, p0, Lcom/konka/tvsettings/common/CountDownTimer;->TICK_SECONDS:I

    iput v1, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_iTime:I

    iput v1, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_iCountDownTime:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_handler:Landroid/os/Handler;

    iput-boolean v1, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_bRunning:Z

    iput v1, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_sBackMsg:I

    iput v1, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_iCount:I

    iput v1, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_sCountDownTime:I

    iput p1, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_iTime:I

    iput p1, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_iCountDownTime:I

    iput-object p3, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_handler:Landroid/os/Handler;

    iput p4, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_iBackMsg:I

    iput p2, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_iCount:I

    iget v0, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_iCount:I

    iput v0, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_sCountDownTime:I

    iput p5, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_sBackMsg:I

    return-void
.end method


# virtual methods
.method public isRuning()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_bRunning:Z

    return v0
.end method

.method public pause()V
    .locals 1

    const-string v0, "count down pause"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_bPause:Z

    return-void
.end method

.method public reStart()V
    .locals 0

    invoke-virtual {p0}, Lcom/konka/tvsettings/common/CountDownTimer;->resetTimer()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/common/CountDownTimer;->start()V

    return-void
.end method

.method public resetTimer()V
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_iTime:I

    iput v0, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_iCountDownTime:I

    iget v0, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_iCount:I

    iput v0, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_sCountDownTime:I

    return-void
.end method

.method public resume()V
    .locals 1

    const-string v0, "count down resume"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_bPause:Z

    return-void
.end method

.method public run()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v1, "count down run"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iput-boolean v4, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_bRunning:Z

    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_bStop:Z

    if-eqz v1, :cond_1

    iput-boolean v3, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_bRunning:Z

    return-void

    :cond_1
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    const-wide/16 v1, 0x3e8

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-boolean v1, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_bPause:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_handler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_iCountDownTime:I

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_bStop:Z

    if-nez v1, :cond_2

    const-string v1, "SendMessage===COUNTDOWN_TIMER_TIMEOUT"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_handler:Landroid/os/Handler;

    iget v2, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_iBackMsg:I

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iput-boolean v4, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_bStop:Z

    iput v3, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_sCountDownTime:I

    :cond_2
    iget v1, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_iCountDownTime:I

    if-lez v1, :cond_3

    iget v1, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_iCountDownTime:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_iCountDownTime:I

    iget v1, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_sCountDownTime:I

    if-lez v1, :cond_3

    iget-boolean v1, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_bStop:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_handler:Landroid/os/Handler;

    iget v2, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_sBackMsg:I

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget v1, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_sCountDownTime:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_sCountDownTime:I

    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "main CountDownTimer: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_iCountDownTime:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1
.end method

.method public start()V
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_bStop:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_bStop:Z

    iget-boolean v0, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_bRunning:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_0
    iput-boolean v1, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_bStop:Z

    iput-boolean v1, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_bPause:Z

    return-void
.end method

.method public stop()V
    .locals 1

    const-string v0, "count down stop"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/tvsettings/common/CountDownTimer;->m_bStop:Z

    invoke-virtual {p0}, Lcom/konka/tvsettings/common/CountDownTimer;->resetTimer()V

    return-void
.end method
