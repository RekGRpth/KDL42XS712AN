.class public Lcom/konka/tvsettings/common/LittleDownTimer;
.super Ljava/lang/Object;
.source "LittleDownTimer.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field static final SELECT_RETURN_MSG:I = 0x66

.field public static final TIME_OUT_CHANGE_SOURCE:I = 0x2bf

.field public static final TIME_OUT_MSG:I = 0x65

.field private static final TOTAL_ITEM_TIMEOUT:I = 0x3

.field private static final TOTAL_SOURCE_CHANGE_TIMEOUT:I = 0x5

.field private static changeSourceCount:I

.field private static countItem:I

.field private static countMenu:I

.field private static enableItem:Z

.field private static enableMenu:Z

.field private static handler:Landroid/os/Handler;

.field private static instance:Lcom/konka/tvsettings/common/LittleDownTimer;

.field private static isStop:Z

.field private static isStopMenu:Z

.field private static totalMenuTime:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const/16 v0, 0xa

    sput v0, Lcom/konka/tvsettings/common/LittleDownTimer;->totalMenuTime:I

    sget v0, Lcom/konka/tvsettings/common/LittleDownTimer;->totalMenuTime:I

    sput v0, Lcom/konka/tvsettings/common/LittleDownTimer;->countMenu:I

    sput-boolean v1, Lcom/konka/tvsettings/common/LittleDownTimer;->enableMenu:Z

    sput-boolean v1, Lcom/konka/tvsettings/common/LittleDownTimer;->isStopMenu:Z

    const/4 v0, 0x1

    sput-boolean v0, Lcom/konka/tvsettings/common/LittleDownTimer;->isStop:Z

    const/4 v0, 0x3

    sput v0, Lcom/konka/tvsettings/common/LittleDownTimer;->countItem:I

    sput-boolean v1, Lcom/konka/tvsettings/common/LittleDownTimer;->enableItem:Z

    const/4 v0, 0x5

    sput v0, Lcom/konka/tvsettings/common/LittleDownTimer;->changeSourceCount:I

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sput-boolean v0, Lcom/konka/tvsettings/common/LittleDownTimer;->isStop:Z

    sput-boolean v0, Lcom/konka/tvsettings/common/LittleDownTimer;->enableMenu:Z

    sput-boolean v0, Lcom/konka/tvsettings/common/LittleDownTimer;->enableItem:Z

    return-void
.end method

.method private decreaseItem()V
    .locals 1

    sget v0, Lcom/konka/tvsettings/common/LittleDownTimer;->countItem:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/konka/tvsettings/common/LittleDownTimer;->countItem:I

    return-void
.end method

.method private decreaseMenu()V
    .locals 1

    sget v0, Lcom/konka/tvsettings/common/LittleDownTimer;->countMenu:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/konka/tvsettings/common/LittleDownTimer;->countMenu:I

    return-void
.end method

.method public static destroy()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/konka/tvsettings/common/LittleDownTimer;->isStop:Z

    return-void
.end method

.method public static getInstance()Lcom/konka/tvsettings/common/LittleDownTimer;
    .locals 1

    sget-object v0, Lcom/konka/tvsettings/common/LittleDownTimer;->instance:Lcom/konka/tvsettings/common/LittleDownTimer;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/tvsettings/common/LittleDownTimer;

    invoke-direct {v0}, Lcom/konka/tvsettings/common/LittleDownTimer;-><init>()V

    sput-object v0, Lcom/konka/tvsettings/common/LittleDownTimer;->instance:Lcom/konka/tvsettings/common/LittleDownTimer;

    :cond_0
    sget-object v0, Lcom/konka/tvsettings/common/LittleDownTimer;->instance:Lcom/konka/tvsettings/common/LittleDownTimer;

    return-object v0
.end method

.method public static pauseItem()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/tvsettings/common/LittleDownTimer;->enableItem:Z

    return-void
.end method

.method public static pauseMenu()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/tvsettings/common/LittleDownTimer;->enableMenu:Z

    return-void
.end method

.method public static resetItem()V
    .locals 1

    const/4 v0, 0x3

    sput v0, Lcom/konka/tvsettings/common/LittleDownTimer;->countItem:I

    return-void
.end method

.method public static resetMenu()V
    .locals 1

    sget v0, Lcom/konka/tvsettings/common/LittleDownTimer;->totalMenuTime:I

    sput v0, Lcom/konka/tvsettings/common/LittleDownTimer;->countMenu:I

    const/4 v0, 0x5

    sput v0, Lcom/konka/tvsettings/common/LittleDownTimer;->changeSourceCount:I

    return-void
.end method

.method public static resumeItem()V
    .locals 2

    sget-boolean v0, Lcom/konka/tvsettings/common/LittleDownTimer;->isStop:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/tvsettings/common/LittleDownTimer;->isStop:Z

    const-string v0, "LittleDownTimer"

    const-string v1, " new Thread"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/Thread;

    sget-object v1, Lcom/konka/tvsettings/common/LittleDownTimer;->instance:Lcom/konka/tvsettings/common/LittleDownTimer;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/konka/tvsettings/common/LittleDownTimer;->enableItem:Z

    const/4 v0, 0x3

    sput v0, Lcom/konka/tvsettings/common/LittleDownTimer;->countItem:I

    return-void
.end method

.method public static resumeMenu()V
    .locals 2

    sget-boolean v0, Lcom/konka/tvsettings/common/LittleDownTimer;->isStop:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/tvsettings/common/LittleDownTimer;->isStop:Z

    const-string v0, "LittleDownTimer"

    const-string v1, " new Thread"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/Thread;

    sget-object v1, Lcom/konka/tvsettings/common/LittleDownTimer;->instance:Lcom/konka/tvsettings/common/LittleDownTimer;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lcom/konka/tvsettings/common/LittleDownTimer;->enableMenu:Z

    sget v0, Lcom/konka/tvsettings/common/LittleDownTimer;->totalMenuTime:I

    sput v0, Lcom/konka/tvsettings/common/LittleDownTimer;->countMenu:I

    const/4 v0, 0x5

    sput v0, Lcom/konka/tvsettings/common/LittleDownTimer;->changeSourceCount:I

    return-void
.end method

.method public static setHandler(Landroid/os/Handler;)V
    .locals 0
    .param p0    # Landroid/os/Handler;

    sput-object p0, Lcom/konka/tvsettings/common/LittleDownTimer;->handler:Landroid/os/Handler;

    return-void
.end method

.method public static setMenuStatus(I)V
    .locals 1
    .param p0    # I

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/tvsettings/common/LittleDownTimer;->isStopMenu:Z

    sput p0, Lcom/konka/tvsettings/common/LittleDownTimer;->totalMenuTime:I

    return-void
.end method

.method public static stopMenu()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/konka/tvsettings/common/LittleDownTimer;->isStopMenu:Z

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    :cond_0
    :goto_0
    sget-boolean v1, Lcom/konka/tvsettings/common/LittleDownTimer;->isStop:Z

    if-eqz v1, :cond_1

    return-void

    :cond_1
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    const-wide/16 v1, 0x3e8

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    sget-boolean v1, Lcom/konka/tvsettings/common/LittleDownTimer;->isStopMenu:Z

    if-nez v1, :cond_3

    sget-boolean v1, Lcom/konka/tvsettings/common/LittleDownTimer;->enableMenu:Z

    if-eqz v1, :cond_3

    sget v1, Lcom/konka/tvsettings/common/LittleDownTimer;->countMenu:I

    if-nez v1, :cond_2

    sget-object v1, Lcom/konka/tvsettings/common/LittleDownTimer;->handler:Landroid/os/Handler;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/konka/tvsettings/common/LittleDownTimer;->handler:Landroid/os/Handler;

    const/16 v2, 0x65

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_2
    invoke-direct {p0}, Lcom/konka/tvsettings/common/LittleDownTimer;->decreaseMenu()V

    :cond_3
    sget-boolean v1, Lcom/konka/tvsettings/common/LittleDownTimer;->enableItem:Z

    if-eqz v1, :cond_5

    sget v1, Lcom/konka/tvsettings/common/LittleDownTimer;->countItem:I

    if-nez v1, :cond_4

    sget-object v1, Lcom/konka/tvsettings/common/LittleDownTimer;->handler:Landroid/os/Handler;

    if-eqz v1, :cond_4

    sget-object v1, Lcom/konka/tvsettings/common/LittleDownTimer;->handler:Landroid/os/Handler;

    const/16 v2, 0x66

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_4
    invoke-direct {p0}, Lcom/konka/tvsettings/common/LittleDownTimer;->decreaseItem()V

    :cond_5
    sget v1, Lcom/konka/tvsettings/common/LittleDownTimer;->changeSourceCount:I

    if-nez v1, :cond_6

    const/4 v1, 0x5

    sput v1, Lcom/konka/tvsettings/common/LittleDownTimer;->changeSourceCount:I

    sget-object v1, Lcom/konka/tvsettings/common/LittleDownTimer;->handler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/konka/tvsettings/common/LittleDownTimer;->handler:Landroid/os/Handler;

    const/16 v2, 0x2bf

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    :cond_6
    sget v1, Lcom/konka/tvsettings/common/LittleDownTimer;->changeSourceCount:I

    add-int/lit8 v1, v1, -0x1

    sput v1, Lcom/konka/tvsettings/common/LittleDownTimer;->changeSourceCount:I

    goto :goto_0
.end method

.method public start()V
    .locals 3

    const/4 v2, 0x0

    sget-boolean v0, Lcom/konka/tvsettings/common/LittleDownTimer;->isStop:Z

    if-eqz v0, :cond_0

    sput-boolean v2, Lcom/konka/tvsettings/common/LittleDownTimer;->isStop:Z

    const-string v0, "LittleDownTimer"

    const-string v1, " new Thread"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_0
    sput-boolean v2, Lcom/konka/tvsettings/common/LittleDownTimer;->isStopMenu:Z

    return-void
.end method
