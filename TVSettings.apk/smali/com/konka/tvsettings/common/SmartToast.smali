.class public Lcom/konka/tvsettings/common/SmartToast;
.super Ljava/lang/Object;
.source "SmartToast.java"


# static fields
.field public static b_isToastOff:Z

.field private static handler:Landroid/os/Handler;

.field private static mParamslayout:Landroid/view/WindowManager$LayoutParams;

.field private static runnable:Ljava/lang/Runnable;

.field private static toast:Landroid/view/WindowManager;

.field private static viewToast:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/tvsettings/common/SmartToast;->mParamslayout:Landroid/view/WindowManager$LayoutParams;

    const/4 v0, 0x1

    sput-boolean v0, Lcom/konka/tvsettings/common/SmartToast;->b_isToastOff:Z

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Lcom/konka/tvsettings/common/SmartToast;->handler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/tvsettings/common/SmartToast$1;

    invoke-direct {v0}, Lcom/konka/tvsettings/common/SmartToast$1;-><init>()V

    sput-object v0, Lcom/konka/tvsettings/common/SmartToast;->runnable:Ljava/lang/Runnable;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static SetSmartToast(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p0    # Landroid/content/Context;
    .param p1    # Ljava/lang/String;

    sget-object v2, Lcom/konka/tvsettings/common/SmartToast;->toast:Landroid/view/WindowManager;

    if-nez v2, :cond_0

    const-string v2, "window"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    sput-object v2, Lcom/konka/tvsettings/common/SmartToast;->toast:Landroid/view/WindowManager;

    :cond_0
    const-string v2, "layout_inflater"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    sget-object v2, Lcom/konka/tvsettings/common/SmartToast;->viewToast:Landroid/view/View;

    if-nez v2, :cond_1

    const v2, 0x7f03004b    # com.konka.tvsettings.R.layout.smart_toast

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    sput-object v2, Lcom/konka/tvsettings/common/SmartToast;->viewToast:Landroid/view/View;

    :cond_1
    sget-object v2, Lcom/konka/tvsettings/common/SmartToast;->viewToast:Landroid/view/View;

    const v3, 0x7f0701e2    # com.konka.tvsettings.R.id.smart_toast_txt

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic access$0()Landroid/view/WindowManager;
    .locals 1

    sget-object v0, Lcom/konka/tvsettings/common/SmartToast;->toast:Landroid/view/WindowManager;

    return-object v0
.end method

.method static synthetic access$1()Landroid/view/View;
    .locals 1

    sget-object v0, Lcom/konka/tvsettings/common/SmartToast;->viewToast:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2(Landroid/view/View;)V
    .locals 0

    sput-object p0, Lcom/konka/tvsettings/common/SmartToast;->viewToast:Landroid/view/View;

    return-void
.end method

.method public static show(I)V
    .locals 4
    .param p0    # I

    const/4 v2, -0x2

    sget-object v0, Lcom/konka/tvsettings/common/SmartToast;->toast:Landroid/view/WindowManager;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/konka/tvsettings/common/SmartToast;->mParamslayout:Landroid/view/WindowManager$LayoutParams;

    if-nez v0, :cond_0

    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    sput-object v0, Lcom/konka/tvsettings/common/SmartToast;->mParamslayout:Landroid/view/WindowManager$LayoutParams;

    sget-object v0, Lcom/konka/tvsettings/common/SmartToast;->mParamslayout:Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x7d6

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    sget-object v0, Lcom/konka/tvsettings/common/SmartToast;->mParamslayout:Landroid/view/WindowManager$LayoutParams;

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    sget-object v0, Lcom/konka/tvsettings/common/SmartToast;->mParamslayout:Landroid/view/WindowManager$LayoutParams;

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    sget-object v0, Lcom/konka/tvsettings/common/SmartToast;->mParamslayout:Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x11

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    :cond_0
    sget-boolean v0, Lcom/konka/tvsettings/common/SmartToast;->b_isToastOff:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/konka/tvsettings/common/SmartToast;->toast:Landroid/view/WindowManager;

    sget-object v1, Lcom/konka/tvsettings/common/SmartToast;->viewToast:Landroid/view/View;

    sget-object v2, Lcom/konka/tvsettings/common/SmartToast;->mParamslayout:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/tvsettings/common/SmartToast;->b_isToastOff:Z

    :cond_1
    sget-object v0, Lcom/konka/tvsettings/common/SmartToast;->handler:Landroid/os/Handler;

    sget-object v1, Lcom/konka/tvsettings/common/SmartToast;->runnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    sget-object v0, Lcom/konka/tvsettings/common/SmartToast;->handler:Landroid/os/Handler;

    sget-object v1, Lcom/konka/tvsettings/common/SmartToast;->runnable:Ljava/lang/Runnable;

    int-to-long v2, p0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_0
    return-void

    :cond_2
    const-string v0, "!!!!!!![muyan]===========toast is null"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0
.end method
