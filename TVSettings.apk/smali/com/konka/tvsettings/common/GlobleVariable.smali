.class public Lcom/konka/tvsettings/common/GlobleVariable;
.super Ljava/lang/Object;
.source "GlobleVariable.java"


# static fields
.field public static final ACTION_KEYDOWN:I = 0x1000

.field public static final ACTION_NAME:Ljava/lang/String; = "action_name"

.field public static final ACTION_VALUE:Ljava/lang/String; = "action_value"

.field public static final AIDL_CONNECTION_OK:I = 0x1002

.field public static final MENU_AUDIOONLY:I = 0x2002

.field public static final MENU_COLORWHEEL_DEMO:I = 0x2000

.field public static final MENU_SCREENSAVER:I = 0x2001

.field public static final PROGRAM_SEARCHING_EXIT:I = 0x1003

.field public static SRS_DEBUG:Z = false

.field public static final SRS_DEFAULT_VALUE_DCCONTROL:I = 0x7

.field public static final SRS_DEFAULT_VALUE_DEFINITIONCONTROL:I = 0x3

.field public static final SRS_DEFAULT_VALUE_INPUTGAIN:I = 0x6

.field public static final SRS_DEFAULT_VALUE_SPEAKERANALYSIS:I = 0x4

.field public static final SRS_DEFAULT_VALUE_SPEAKERAUDIO:I = 0x9

.field public static final SRS_DEFAULT_VALUE_SURRLEVELCONTROL:I = 0x7

.field public static final SRS_DEFAULT_VALUE_TRUBASSCONTROL:I = 0x5

.field public static final START_ACTIVITY:Ljava/lang/String; = "start_activity"

.field public static final START_EPG:Ljava/lang/String; = "action_start_epg"

.field public static final TEXT_COLOR_ITEM_CANNOTFOCUSE:I = -0x7c7c78

.field public static final TEXT_COLOR_ITEM_FOCUSED:I = -0x1

.field public static final TEXT_COLOR_ITEM_LASTFOCUSED:I = -0x87e2

.field public static final TIMER_SCREEN_SAVER_TIMEOUT:I = 0x1001


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/tvsettings/common/GlobleVariable;->SRS_DEBUG:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
