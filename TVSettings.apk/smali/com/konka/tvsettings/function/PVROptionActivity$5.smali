.class Lcom/konka/tvsettings/function/PVROptionActivity$5;
.super Ljava/lang/Object;
.source "PVROptionActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/function/PVROptionActivity;->startSpeedTest()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

.field private final synthetic val$handler:Landroid/os/Handler;

.field private final synthetic val$mpDialog:Landroid/app/ProgressDialog;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/function/PVROptionActivity;Landroid/os/Handler;Landroid/app/ProgressDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/function/PVROptionActivity$5;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    iput-object p2, p0, Lcom/konka/tvsettings/function/PVROptionActivity$5;->val$handler:Landroid/os/Handler;

    iput-object p3, p0, Lcom/konka/tvsettings/function/PVROptionActivity$5;->val$mpDialog:Landroid/app/ProgressDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/function/PVROptionActivity$5;)Lcom/konka/tvsettings/function/PVROptionActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$5;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 6

    const/4 v2, 0x0

    :try_start_0
    const-string v3, "USB speed ========>>>run check"

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/tvsettings/function/PVROptionActivity$5;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v3}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$4(Lcom/konka/tvsettings/function/PVROptionActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/tvsettings/function/PVROptionActivity$5;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->selectedDiskPath:Ljava/lang/String;
    invoke-static {v4}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$6(Lcom/konka/tvsettings/function/PVROptionActivity;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    invoke-interface {v3, v4, v5}, Lcom/konka/kkinterface/tv/PvrDesk;->setPVRParas(Ljava/lang/String;S)Z

    iget-object v3, p0, Lcom/konka/tvsettings/function/PVROptionActivity$5;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v3}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$4(Lcom/konka/tvsettings/function/PVROptionActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/PvrDesk;->checkUsbSpeed()I

    move-result v2

    const-string v3, "USB speed ========>>>run check 2"

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const-string v3, "PVROptionActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "USB check speed "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " KB/S"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " KB/S"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/konka/tvsettings/function/PVROptionActivity$5;->val$handler:Landroid/os/Handler;

    new-instance v4, Lcom/konka/tvsettings/function/PVROptionActivity$5$1;

    iget-object v5, p0, Lcom/konka/tvsettings/function/PVROptionActivity$5;->val$mpDialog:Landroid/app/ProgressDialog;

    invoke-direct {v4, p0, v0, v5}, Lcom/konka/tvsettings/function/PVROptionActivity$5$1;-><init>(Lcom/konka/tvsettings/function/PVROptionActivity$5;Ljava/lang/String;Landroid/app/ProgressDialog;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method
