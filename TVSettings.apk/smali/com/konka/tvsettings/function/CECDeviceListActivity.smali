.class public Lcom/konka/tvsettings/function/CECDeviceListActivity;
.super Landroid/app/Activity;
.source "CECDeviceListActivity.java"


# instance fields
.field DeviceListLa:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;",
            ">;"
        }
    .end annotation
.end field

.field private DevicesListView:Landroid/widget/ListView;

.field SelectDevice:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

.field private adapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field cecManager:Lcom/mstar/android/tvapi/common/CecManager;

.field private myHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v1, p0, Lcom/konka/tvsettings/function/CECDeviceListActivity;->adapter:Landroid/widget/ArrayAdapter;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getCecManager()Lcom/mstar/android/tvapi/common/CecManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/function/CECDeviceListActivity;->cecManager:Lcom/mstar/android/tvapi/common/CecManager;

    iput-object v1, p0, Lcom/konka/tvsettings/function/CECDeviceListActivity;->DevicesListView:Landroid/widget/ListView;

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_TV:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    iput-object v0, p0, Lcom/konka/tvsettings/function/CECDeviceListActivity;->SelectDevice:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    iput-object v1, p0, Lcom/konka/tvsettings/function/CECDeviceListActivity;->DeviceListLa:Ljava/util/ArrayList;

    new-instance v0, Lcom/konka/tvsettings/function/CECDeviceListActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/function/CECDeviceListActivity$1;-><init>(Lcom/konka/tvsettings/function/CECDeviceListActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/function/CECDeviceListActivity;->myHandler:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const/4 v6, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v4, 0x7f030008    # com.konka.tvsettings.R.layout.cec_devicelist

    invoke-virtual {p0, v4}, Lcom/konka/tvsettings/function/CECDeviceListActivity;->setContentView(I)V

    new-instance v4, Landroid/widget/ArrayAdapter;

    const v5, 0x7f030009    # com.konka.tvsettings.R.layout.cec_deviceslist_item

    invoke-direct {v4, p0, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v4, p0, Lcom/konka/tvsettings/function/CECDeviceListActivity;->adapter:Landroid/widget/ArrayAdapter;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/konka/tvsettings/function/CECDeviceListActivity;->DeviceListLa:Ljava/util/ArrayList;

    const/4 v2, 0x0

    :goto_0
    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->E_UNREGISTERED:Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->ordinal()I

    move-result v4

    if-lt v2, v4, :cond_0

    const v4, 0x7f07003a    # com.konka.tvsettings.R.id.cec_devicelist

    invoke-virtual {p0, v4}, Lcom/konka/tvsettings/function/CECDeviceListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ListView;

    iput-object v4, p0, Lcom/konka/tvsettings/function/CECDeviceListActivity;->DevicesListView:Landroid/widget/ListView;

    iget-object v4, p0, Lcom/konka/tvsettings/function/CECDeviceListActivity;->DevicesListView:Landroid/widget/ListView;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setChoiceMode(I)V

    iget-object v4, p0, Lcom/konka/tvsettings/function/CECDeviceListActivity;->DevicesListView:Landroid/widget/ListView;

    iget-object v5, p0, Lcom/konka/tvsettings/function/CECDeviceListActivity;->adapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v4, p0, Lcom/konka/tvsettings/function/CECDeviceListActivity;->adapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v4}, Landroid/widget/ArrayAdapter;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_2

    const v4, 0x7f0a01bb    # com.konka.tvsettings.R.string.str_function_cec_no_device

    invoke-virtual {p0, v4}, Lcom/konka/tvsettings/function/CECDeviceListActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/tvsettings/function/CECDeviceListActivity;->adapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v4, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    iget-object v4, p0, Lcom/konka/tvsettings/function/CECDeviceListActivity;->DevicesListView:Landroid/widget/ListView;

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setEnabled(Z)V

    iget-object v4, p0, Lcom/konka/tvsettings/function/CECDeviceListActivity;->DevicesListView:Landroid/widget/ListView;

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setClickable(Z)V

    iget-object v4, p0, Lcom/konka/tvsettings/function/CECDeviceListActivity;->DevicesListView:Landroid/widget/ListView;

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setFocusable(Z)V

    iget-object v4, p0, Lcom/konka/tvsettings/function/CECDeviceListActivity;->DevicesListView:Landroid/widget/ListView;

    invoke-virtual {v4, v6}, Landroid/widget/ListView;->setFocusableInTouchMode(Z)V

    :goto_1
    iget-object v4, p0, Lcom/konka/tvsettings/function/CECDeviceListActivity;->myHandler:Landroid/os/Handler;

    invoke-static {v4}, Lcom/konka/tvsettings/common/LittleDownTimer;->setHandler(Landroid/os/Handler;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    :try_start_0
    iget-object v4, p0, Lcom/konka/tvsettings/function/CECDeviceListActivity;->cecManager:Lcom/mstar/android/tvapi/common/CecManager;

    invoke-virtual {v4, v2}, Lcom/mstar/android/tvapi/common/CecManager;->getDeviceName(I)Ljava/lang/String;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/konka/tvsettings/function/CECDeviceListActivity;->adapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v4, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    iget-object v4, p0, Lcom/konka/tvsettings/function/CECDeviceListActivity;->DeviceListLa:Ljava/util/ArrayList;

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;->values()[Lcom/mstar/android/tvapi/common/vo/EnumCecDeviceLa;

    move-result-object v5

    aget-object v5, v5, v2

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2

    :cond_2
    iget-object v4, p0, Lcom/konka/tvsettings/function/CECDeviceListActivity;->DevicesListView:Landroid/widget/ListView;

    new-instance v5, Lcom/konka/tvsettings/function/CECDeviceListActivity$2;

    invoke-direct {v5, p0}, Lcom/konka/tvsettings/function/CECDeviceListActivity$2;-><init>(Lcom/konka/tvsettings/function/CECDeviceListActivity;)V

    invoke-virtual {v4, v5}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_1
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    sparse-switch p1, :sswitch_data_0

    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    :goto_1
    return v2

    :sswitch_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/function/CECDeviceListActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/function/CECDeviceListActivity;->finish()V

    const/4 v2, 0x0

    const v3, 0x7f040005    # com.konka.tvsettings.R.anim.anim_menu_exit

    invoke-virtual {p0, v2, v3}, Lcom/konka/tvsettings/function/CECDeviceListActivity;->overridePendingTransition(II)V

    goto :goto_0

    :sswitch_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v2, Lcom/konka/tvsettings/function/CECActivity;

    invoke-virtual {v0, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/function/CECDeviceListActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/function/CECDeviceListActivity;->finish()V

    goto :goto_0

    :sswitch_2
    const/4 v2, 0x1

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x52 -> :sswitch_1
        0xb2 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->pauseMenu()V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resumeMenu()V

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    const v0, 0x7f040009    # com.konka.tvsettings.R.anim.anim_zoom_in

    const v1, 0x7f040008    # com.konka.tvsettings.R.anim.anim_right_out

    invoke-virtual {p0, v0, v1}, Lcom/konka/tvsettings/function/CECDeviceListActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resetMenu()V

    invoke-super {p0}, Landroid/app/Activity;->onUserInteraction()V

    return-void
.end method
