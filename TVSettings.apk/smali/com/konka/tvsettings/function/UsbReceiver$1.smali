.class Lcom/konka/tvsettings/function/UsbReceiver$1;
.super Ljava/lang/Thread;
.source "UsbReceiver.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/function/UsbReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/function/UsbReceiver;

.field private final synthetic val$path:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/function/UsbReceiver;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/function/UsbReceiver$1;->this$0:Lcom/konka/tvsettings/function/UsbReceiver;

    iput-object p2, p0, Lcom/konka/tvsettings/function/UsbReceiver$1;->val$path:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    :try_start_0
    new-instance v2, Landroid/os/StatFs;

    iget-object v3, p0, Lcom/konka/tvsettings/function/UsbReceiver$1;->val$path:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2}, Landroid/os/StatFs;->getFreeBlocks()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2}, Landroid/os/StatFs;->getBlockCount()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v4, v5

    sub-float/2addr v3, v4

    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v3, v4

    float-to-int v1, v3

    iget-object v3, p0, Lcom/konka/tvsettings/function/UsbReceiver$1;->val$path:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/konka/tvsettings/function/UsbReceiver;->addDiskDesc(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
