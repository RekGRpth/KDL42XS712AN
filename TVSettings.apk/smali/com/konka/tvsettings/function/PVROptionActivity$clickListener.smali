.class Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;
.super Ljava/lang/Object;
.source "PVROptionActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/function/PVROptionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "clickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/function/PVROptionActivity;


# direct methods
.method private constructor <init>(Lcom/konka/tvsettings/function/PVROptionActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/tvsettings/function/PVROptionActivity;Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;-><init>(Lcom/konka/tvsettings/function/PVROptionActivity;)V

    return-void
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;)Lcom/konka/tvsettings/function/PVROptionActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10
    .param p1    # Landroid/view/View;

    const/4 v5, 0x4

    const v2, 0x7f0a00eb    # com.konka.tvsettings.R.string.str_pvr_insert_usb

    const/16 v9, 0x1f4

    const/4 v1, 0x1

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    invoke-static {v0, v4}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$0(Lcom/konka/tvsettings/function/PVROptionActivity;Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->usbSelecter:Lcom/konka/tvsettings/function/USBDiskSelecter;
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$1(Lcom/konka/tvsettings/function/PVROptionActivity;)Lcom/konka/tvsettings/function/USBDiskSelecter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/tvsettings/function/USBDiskSelecter;->getDriverCount()I

    move-result v8

    if-lez v8, :cond_0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->pauseMenu()V

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->usbSelecter:Lcom/konka/tvsettings/function/USBDiskSelecter;
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$1(Lcom/konka/tvsettings/function/PVROptionActivity;)Lcom/konka/tvsettings/function/USBDiskSelecter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/tvsettings/function/USBDiskSelecter;->start()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$2(Lcom/konka/tvsettings/function/PVROptionActivity;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :pswitch_2
    new-instance v6, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    invoke-direct {v6, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    new-array v7, v5, [Ljava/lang/String;

    const-string v0, "512M"

    aput-object v0, v7, v4

    const-string v0, "1G"

    aput-object v0, v7, v1

    const/4 v0, 0x2

    const-string v1, "2G"

    aput-object v1, v7, v0

    const/4 v0, 0x3

    const-string v1, "4G"

    aput-object v1, v7, v0

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    # invokes: Lcom/konka/tvsettings/function/PVROptionActivity;->getLastTimeShiftSize()I
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$3(Lcom/konka/tvsettings/function/PVROptionActivity;)I

    move-result v0

    new-instance v1, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener$1;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener$1;-><init>(Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;)V

    invoke-virtual {v6, v7, v0, v1}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    new-instance v0, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener$2;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener$2;-><init>(Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;)V

    invoke-virtual {v6, v0}, Landroid/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->pauseMenu()V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->selectedDiskPath:Ljava/lang/String;
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$6(Lcom/konka/tvsettings/function/PVROptionActivity;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "USB format ========>>>select disk first"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->usbSelecter:Lcom/konka/tvsettings/function/USBDiskSelecter;
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$1(Lcom/konka/tvsettings/function/PVROptionActivity;)Lcom/konka/tvsettings/function/USBDiskSelecter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/tvsettings/function/USBDiskSelecter;->getDriverCount()I

    move-result v8

    if-lez v8, :cond_1

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->usbSelecter:Lcom/konka/tvsettings/function/USBDiskSelecter;
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$1(Lcom/konka/tvsettings/function/PVROptionActivity;)Lcom/konka/tvsettings/function/USBDiskSelecter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/tvsettings/function/USBDiskSelecter;->start()V

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    invoke-static {v0, v1}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$0(Lcom/konka/tvsettings/function/PVROptionActivity;Z)V

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->pauseMenu()V

    goto/16 :goto_0

    :cond_1
    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$2(Lcom/konka/tvsettings/function/PVROptionActivity;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    invoke-static {v0, v4}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$0(Lcom/konka/tvsettings/function/PVROptionActivity;Z)V

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    # invokes: Lcom/konka/tvsettings/function/PVROptionActivity;->formatConfirm()V
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$7(Lcom/konka/tvsettings/function/PVROptionActivity;)V

    goto/16 :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->selectedDiskPath:Ljava/lang/String;
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$6(Lcom/konka/tvsettings/function/PVROptionActivity;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    const-string v0, "USB speed ========>>>select disk first"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->usbSelecter:Lcom/konka/tvsettings/function/USBDiskSelecter;
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$1(Lcom/konka/tvsettings/function/PVROptionActivity;)Lcom/konka/tvsettings/function/USBDiskSelecter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/tvsettings/function/USBDiskSelecter;->getDriverCount()I

    move-result v8

    if-lez v8, :cond_3

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->usbSelecter:Lcom/konka/tvsettings/function/USBDiskSelecter;
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$1(Lcom/konka/tvsettings/function/PVROptionActivity;)Lcom/konka/tvsettings/function/USBDiskSelecter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/tvsettings/function/USBDiskSelecter;->start()V

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    invoke-static {v0, v1}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$8(Lcom/konka/tvsettings/function/PVROptionActivity;Z)V

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->pauseMenu()V

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$2(Lcom/konka/tvsettings/function/PVROptionActivity;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v2, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "USB speed ========>>>select disk "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->selectedDiskLable:Ljava/lang/String;
    invoke-static {v2}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$9(Lcom/konka/tvsettings/function/PVROptionActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const-string v3, "NTFS"

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->selectedDiskLable:Ljava/lang/String;
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$9(Lcom/konka/tvsettings/function/PVROptionActivity;)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x6

    invoke-virtual/range {v0 .. v5}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$2(Lcom/konka/tvsettings/function/PVROptionActivity;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a018a    # com.konka.tvsettings.R.string.str_pvr_unsurpt_flsystem

    invoke-static {v0, v1, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    # invokes: Lcom/konka/tvsettings/function/PVROptionActivity;->startSpeedTest()V
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$10(Lcom/konka/tvsettings/function/PVROptionActivity;)V

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->pauseMenu()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f07017b
        :pswitch_1    # com.konka.tvsettings.R.id.pvr_file_system_select_disk
        :pswitch_2    # com.konka.tvsettings.R.id.pvr_file_system_time_shift_size
        :pswitch_3    # com.konka.tvsettings.R.id.pvr_file_system_format_layout
        :pswitch_0    # com.konka.tvsettings.R.id.pvr_file_system_format_start
        :pswitch_0    # com.konka.tvsettings.R.id.pvr_file_system_format_context
        :pswitch_4    # com.konka.tvsettings.R.id.pvr_file_system_speed_layout
    .end packed-switch
.end method
