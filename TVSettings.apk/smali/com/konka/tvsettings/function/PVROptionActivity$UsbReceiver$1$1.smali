.class Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1$1;
.super Ljava/lang/Object;
.source "PVROptionActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1$1;->this$2:Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1$1;->this$2:Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;->this$1:Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;->access$0(Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;)Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;

    move-result-object v0

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;->access$1(Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;)Lcom/konka/tvsettings/function/PVROptionActivity;

    move-result-object v0

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->diskFormatStatus:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$13(Lcom/konka/tvsettings/function/PVROptionActivity;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1$1;->this$2:Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;->this$1:Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;->access$0(Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;)Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;

    move-result-object v0

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;->access$1(Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;)Lcom/konka/tvsettings/function/PVROptionActivity;

    move-result-object v0

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->diskFormatStatus:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$13(Lcom/konka/tvsettings/function/PVROptionActivity;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f0a00db    # com.konka.tvsettings.R.string.str_pvr_file_system_format_context

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1$1;->this$2:Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;->this$1:Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;->access$0(Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;)Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;

    move-result-object v0

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;->access$1(Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;)Lcom/konka/tvsettings/function/PVROptionActivity;

    move-result-object v0

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->mpFormatDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$14(Lcom/konka/tvsettings/function/PVROptionActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resumeMenu()V

    return-void
.end method
