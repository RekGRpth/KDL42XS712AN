.class Lcom/konka/tvsettings/function/USBDiskSelecter$UpdateHandler;
.super Landroid/os/Handler;
.source "USBDiskSelecter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/function/USBDiskSelecter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UpdateHandler"
.end annotation


# instance fields
.field pb:Landroid/widget/ProgressBar;

.field tip:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/widget/ProgressBar;Landroid/widget/ProgressBar;)V
    .locals 0
    .param p1    # Landroid/widget/ProgressBar;
    .param p2    # Landroid/widget/ProgressBar;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    iput-object p1, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$UpdateHandler;->pb:Landroid/widget/ProgressBar;

    iput-object p2, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$UpdateHandler;->tip:Landroid/widget/ProgressBar;

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1    # Landroid/os/Message;

    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v0, p1, Landroid/os/Message;->arg2:I

    iget-object v2, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$UpdateHandler;->pb:Landroid/widget/ProgressBar;

    const/high16 v3, 0x3f800000    # 1.0f

    int-to-float v4, v1

    int-to-float v5, v0

    div-float/2addr v4, v5

    sub-float/2addr v3, v4

    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$UpdateHandler;->tip:Landroid/widget/ProgressBar;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void
.end method
