.class Lcom/konka/tvsettings/function/PVROptionActivity$2;
.super Lcom/konka/tvsettings/function/USBDiskSelecter;
.source "PVROptionActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/function/PVROptionActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/function/PVROptionActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/function/PVROptionActivity;Landroid/content/Context;)V
    .locals 0
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/konka/tvsettings/function/PVROptionActivity$2;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    invoke-direct {p0, p2}, Lcom/konka/tvsettings/function/USBDiskSelecter;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onItemCancel()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resumeMenu()V

    return-void
.end method

.method public onItemChosen(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    const/4 v5, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x6

    const/4 v4, 0x0

    const-string v7, "FAT"

    const-string v3, "NTFS"

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$2;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->selectDisk:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$15(Lcom/konka/tvsettings/function/PVROptionActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$2;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    invoke-static {v0, p3}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$16(Lcom/konka/tvsettings/function/PVROptionActivity;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$2;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    invoke-static {v0, p2}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$17(Lcom/konka/tvsettings/function/PVROptionActivity;Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "PVRFBActivity"

    const-string v8, "=============>>>>> USB Disk Path is NULL !!!"

    invoke-static {v0, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "PVRFBActivity"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "=============>>>>> USB Disk Path = "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v0, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x6

    const/4 v8, 0x0

    const/4 v9, 0x3

    invoke-virtual {p2, v0, v7, v8, v9}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$2;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$4(Lcom/konka/tvsettings/function/PVROptionActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v0

    const/4 v8, 0x2

    invoke-interface {v0, p3, v8}, Lcom/konka/kkinterface/tv/PvrDesk;->setPVRParas(Ljava/lang/String;S)Z

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$2;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    const/4 v8, 0x1

    # invokes: Lcom/konka/tvsettings/function/PVROptionActivity;->saveChooseDiskSettings(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v8, p3, p2, p4}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$18(Lcom/konka/tvsettings/function/PVROptionActivity;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resumeMenu()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$2;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->waitToFormat:Z
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$11(Lcom/konka/tvsettings/function/PVROptionActivity;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$2;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    # invokes: Lcom/konka/tvsettings/function/PVROptionActivity;->formatConfirm()V
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$7(Lcom/konka/tvsettings/function/PVROptionActivity;)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x6

    const/4 v8, 0x0

    const/4 v9, 0x4

    :try_start_1
    invoke-virtual {p2, v0, v3, v8, v9}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$2;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$4(Lcom/konka/tvsettings/function/PVROptionActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v0

    const/4 v8, 0x6

    invoke-interface {v0, p3, v8}, Lcom/konka/kkinterface/tv/PvrDesk;->setPVRParas(Ljava/lang/String;S)Z
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$2;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->waitToSpeedTest:Z
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$19(Lcom/konka/tvsettings/function/PVROptionActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$2;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    invoke-static {v0, v4}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$8(Lcom/konka/tvsettings/function/PVROptionActivity;Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$2;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->selectedDiskLable:Ljava/lang/String;
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$9(Lcom/konka/tvsettings/function/PVROptionActivity;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual/range {v0 .. v5}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$2;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$2(Lcom/konka/tvsettings/function/PVROptionActivity;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a018a    # com.konka.tvsettings.R.string.str_pvr_unsurpt_flsystem

    const/16 v2, 0x1f4

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$2;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;

    # invokes: Lcom/konka/tvsettings/function/PVROptionActivity;->startSpeedTest()V
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$10(Lcom/konka/tvsettings/function/PVROptionActivity;)V

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->pauseMenu()V

    goto/16 :goto_0
.end method
