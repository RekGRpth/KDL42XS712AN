.class public abstract Lcom/konka/tvsettings/function/USBDiskSelecter;
.super Ljava/lang/Object;
.source "USBDiskSelecter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/function/USBDiskSelecter$DirectoryFilter;,
        Lcom/konka/tvsettings/function/USBDiskSelecter$UpdateHandler;,
        Lcom/konka/tvsettings/function/USBDiskSelecter$UsbReceiver;,
        Lcom/konka/tvsettings/function/USBDiskSelecter$listAdapter;,
        Lcom/konka/tvsettings/function/USBDiskSelecter$usbDiskSelectedListener;,
        Lcom/konka/tvsettings/function/USBDiskSelecter$usbListener;
    }
.end annotation


# static fields
.field public static final CHOOSE_DISK:Ljava/lang/String; = "CHOOSE_DISK"

.field public static final NO_DISK:Ljava/lang/String; = "NO_DISK"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private context:Landroid/content/Context;

.field private currentDialog:Landroid/app/Dialog;

.field private final maxDiskperPage:I

.field protected noDismiss:Z

.field private storageManager:Lcom/mstar/android/storage/MStorageManager;

.field private usbDriverCount:I

.field private usbDriverLabel:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private usbDriverLabelPcStyle:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private usbDriverPath:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private usbReceiver:Lcom/konka/tvsettings/function/USBDiskSelecter$UsbReceiver;

.field private usblistener:Lcom/konka/tvsettings/function/USBDiskSelecter$usbListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "USBDiskSelecter"

    iput-object v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->TAG:Ljava/lang/String;

    const/4 v0, 0x4

    iput v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->maxDiskperPage:I

    new-instance v0, Lcom/konka/tvsettings/function/USBDiskSelecter$UsbReceiver;

    invoke-direct {v0, p0, v1}, Lcom/konka/tvsettings/function/USBDiskSelecter$UsbReceiver;-><init>(Lcom/konka/tvsettings/function/USBDiskSelecter;Lcom/konka/tvsettings/function/USBDiskSelecter$UsbReceiver;)V

    iput-object v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->usbReceiver:Lcom/konka/tvsettings/function/USBDiskSelecter$UsbReceiver;

    iput v2, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->usbDriverCount:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->usbDriverLabel:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->usbDriverPath:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->usbDriverLabelPcStyle:Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->currentDialog:Landroid/app/Dialog;

    iput-object v1, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->context:Landroid/content/Context;

    iput-object v1, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->usblistener:Lcom/konka/tvsettings/function/USBDiskSelecter$usbListener;

    iput-boolean v2, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->noDismiss:Z

    iput-object p1, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->context:Landroid/content/Context;

    invoke-static {p1}, Lcom/mstar/android/storage/MStorageManager;->getInstance(Landroid/content/Context;)Lcom/mstar/android/storage/MStorageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->storageManager:Lcom/mstar/android/storage/MStorageManager;

    invoke-direct {p0}, Lcom/konka/tvsettings/function/USBDiskSelecter;->updateUSBDriverInfo()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/function/USBDiskSelecter;->registerUSBDetector()V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/function/USBDiskSelecter;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/function/USBDiskSelecter;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->usbDriverLabelPcStyle:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$10(Lcom/konka/tvsettings/function/USBDiskSelecter;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->usbDriverLabel:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/function/USBDiskSelecter;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->usbDriverPath:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/function/USBDiskSelecter;F)I
    .locals 1

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/function/USBDiskSelecter;->dip2px(F)I

    move-result v0

    return v0
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/function/USBDiskSelecter;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/function/USBDiskSelecter;->updateUSBDriverInfo()V

    return-void
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/function/USBDiskSelecter;)Landroid/app/Dialog;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->currentDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/tvsettings/function/USBDiskSelecter;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->usbDriverCount:I

    return v0
.end method

.method static synthetic access$7(Lcom/konka/tvsettings/function/USBDiskSelecter;Landroid/app/Dialog;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->currentDialog:Landroid/app/Dialog;

    return-void
.end method

.method static synthetic access$8(Lcom/konka/tvsettings/function/USBDiskSelecter;I)Landroid/view/View;
    .locals 1

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/function/USBDiskSelecter;->getUSBSelecterView(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$9(Lcom/konka/tvsettings/function/USBDiskSelecter;)Lcom/konka/tvsettings/function/USBDiskSelecter$usbListener;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->usblistener:Lcom/konka/tvsettings/function/USBDiskSelecter$usbListener;

    return-object v0
.end method

.method private dip2px(F)I
    .locals 3
    .param p1    # F

    iget-object v1, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v0, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float v1, p1, v0

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v1, v1

    return v1
.end method

.method private getChooseDiskLable()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->context:Landroid/content/Context;

    const-string v2, "save_setting_select"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "DISK_LABEL"

    const-string v2, "unknown"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getChooseDiskPath()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->context:Landroid/content/Context;

    const-string v2, "save_setting_select"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "DISK_PATH"

    const-string v2, "unknown"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getChooseDiskSettings()Z
    .locals 4

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->context:Landroid/content/Context;

    const-string v2, "save_setting_select"

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "IS_ALREADY_CHOOSE_DISK"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method private getFileSystem(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/lang/String;

    if-nez p1, :cond_1

    const-string v5, ""

    :cond_0
    :goto_0
    return-object v5

    :cond_1
    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/FileReader;

    invoke-direct {v5, p1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_9
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v4

    :goto_1
    if-nez v4, :cond_3

    if-eqz v1, :cond_a

    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_8

    move-object v0, v1

    :cond_2
    :goto_2
    const-string v5, ""

    goto :goto_0

    :cond_3
    :try_start_3
    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x1

    aget-object v5, v3, v5

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    const/4 v5, 0x2

    aget-object v5, v3, v5

    const-string v6, "ntfs3g"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_a
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_9
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v5

    if-eqz v5, :cond_5

    if-eqz v1, :cond_4

    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    :cond_4
    :goto_3
    const-string v5, "NTFS"

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    :cond_5
    const/4 v5, 0x2

    :try_start_5
    aget-object v5, v3, v5

    const-string v6, "vfat"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_a
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_9
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result v5

    if-eqz v5, :cond_7

    if-eqz v1, :cond_6

    :try_start_6
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    :cond_6
    :goto_4
    const-string v5, "FAT"

    goto :goto_0

    :catch_1
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    :cond_7
    const/4 v5, 0x2

    :try_start_7
    aget-object v5, v3, v5
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_a
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_9
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    if-eqz v1, :cond_0

    :try_start_8
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    goto :goto_0

    :catch_2
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    :cond_8
    :try_start_9
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_9
    .catch Ljava/io/FileNotFoundException; {:try_start_9 .. :try_end_9} :catch_a
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    move-result-object v4

    goto :goto_1

    :catch_3
    move-exception v2

    :goto_5
    :try_start_a
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    if-eqz v0, :cond_2

    :try_start_b
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_4

    goto :goto_2

    :catch_4
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    :catch_5
    move-exception v2

    :goto_6
    :try_start_c
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    if-eqz v0, :cond_2

    :try_start_d
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_6

    goto :goto_2

    :catch_6
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    :catchall_0
    move-exception v5

    :goto_7
    if-eqz v0, :cond_9

    :try_start_e
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_7

    :cond_9
    :goto_8
    throw v5

    :catch_7
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    :catch_8
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    :cond_a
    move-object v0, v1

    goto/16 :goto_2

    :catchall_1
    move-exception v5

    move-object v0, v1

    goto :goto_7

    :catch_9
    move-exception v2

    move-object v0, v1

    goto :goto_6

    :catch_a
    move-exception v2

    move-object v0, v1

    goto :goto_5
.end method

.method private getFirstUseableDiskAtParentDir(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_0

    new-instance v1, Lcom/konka/tvsettings/function/USBDiskSelecter$DirectoryFilter;

    invoke-direct {v1, v4}, Lcom/konka/tvsettings/function/USBDiskSelecter$DirectoryFilter;-><init>(Lcom/konka/tvsettings/function/USBDiskSelecter$DirectoryFilter;)V

    invoke-virtual {v0, v1}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v2

    array-length v6, v2

    const/4 v5, 0x0

    :goto_0
    if-lt v5, v6, :cond_1

    :cond_0
    :goto_1
    return-object v4

    :cond_1
    aget-object v3, v2, v5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "/_MSTPVR/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/konka/tvsettings/function/USBDiskSelecter;->isFileExisted(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    new-instance v7, Ljava/lang/String;

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Lcom/konka/tvsettings/function/UsbReceiver;->isDiskExisted(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    :cond_2
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method

.method private getUSBSelecterView(I)Landroid/view/View;
    .locals 11
    .param p1    # I

    const/4 v10, -0x2

    iget-object v8, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->context:Landroid/content/Context;

    const-string v9, "layout_inflater"

    invoke-virtual {v8, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    const v8, 0x7f030059    # com.konka.tvsettings.R.layout.usb_driver_selecter

    const/4 v9, 0x0

    invoke-virtual {v3, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    const v8, 0x7f070221    # com.konka.tvsettings.R.id.usb_driver_selecter

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/GridView;

    invoke-virtual {v6, p1}, Landroid/widget/GridView;->setNumColumns(I)V

    const/high16 v8, 0x44020000    # 520.0f

    invoke-direct {p0, v8}, Lcom/konka/tvsettings/function/USBDiskSelecter;->dip2px(F)I

    move-result v5

    const/high16 v8, 0x40a00000    # 5.0f

    invoke-direct {p0, v8}, Lcom/konka/tvsettings/function/USBDiskSelecter;->dip2px(F)I

    move-result v2

    const/4 v8, 0x4

    if-gt p1, v8, :cond_0

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v4, v5, v10}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v6, v4}, Landroid/widget/GridView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    mul-int v8, p1, v2

    sub-int v8, v5, v8

    div-int/2addr v8, p1

    invoke-virtual {v6, v8}, Landroid/widget/GridView;->setColumnWidth(I)V

    :goto_0
    new-instance v8, Lcom/konka/tvsettings/function/USBDiskSelecter$listAdapter;

    invoke-direct {v8, p0, p1}, Lcom/konka/tvsettings/function/USBDiskSelecter$listAdapter;-><init>(Lcom/konka/tvsettings/function/USBDiskSelecter;I)V

    invoke-virtual {v6, v8}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v8, Lcom/konka/tvsettings/function/USBDiskSelecter$1;

    invoke-direct {v8, p0}, Lcom/konka/tvsettings/function/USBDiskSelecter$1;-><init>(Lcom/konka/tvsettings/function/USBDiskSelecter;)V

    invoke-virtual {v6, v8}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    new-instance v9, Lcom/konka/tvsettings/function/USBDiskSelecter$usbDiskSelectedListener;

    const v8, 0x7f070220    # com.konka.tvsettings.R.id.usb_driver_scroller

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/HorizontalScrollView;

    invoke-direct {v9, p0, v8}, Lcom/konka/tvsettings/function/USBDiskSelecter$usbDiskSelectedListener;-><init>(Lcom/konka/tvsettings/function/USBDiskSelecter;Landroid/widget/HorizontalScrollView;)V

    invoke-virtual {v6, v9}, Landroid/widget/GridView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    const v8, 0x7f070222    # com.konka.tvsettings.R.id.usb_driver_selecter_cancel

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v8, Lcom/konka/tvsettings/function/USBDiskSelecter$2;

    invoke-direct {v8, p0}, Lcom/konka/tvsettings/function/USBDiskSelecter$2;-><init>(Lcom/konka/tvsettings/function/USBDiskSelecter;)V

    invoke-virtual {v0, v8}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    return-object v7

    :cond_0
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v8, 0x42fa0000    # 125.0f

    invoke-direct {p0, v8}, Lcom/konka/tvsettings/function/USBDiskSelecter;->dip2px(F)I

    move-result v8

    mul-int/2addr v8, p1

    invoke-direct {v1, v8, v10}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v6, v1}, Landroid/widget/GridView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/high16 v8, 0x42f00000    # 120.0f

    invoke-direct {p0, v8}, Lcom/konka/tvsettings/function/USBDiskSelecter;->dip2px(F)I

    move-result v8

    invoke-virtual {v6, v8}, Landroid/widget/GridView;->setColumnWidth(I)V

    goto :goto_0
.end method

.method private isFileExisted(Ljava/lang/String;)Z
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    const-string v2, ""

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private linux2pc(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x4

    if-ge v1, v2, :cond_1

    :cond_0
    const-string v1, "unknown"

    :goto_0
    return-object v1

    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    add-int/lit8 v1, v1, 0x2

    int-to-char v0, v1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private updateUSBDriverInfo()V
    .locals 15

    const/4 v14, 0x0

    iget-object v12, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->storageManager:Lcom/mstar/android/storage/MStorageManager;

    invoke-virtual {v12}, Lcom/mstar/android/storage/MStorageManager;->getVolumePaths()[Ljava/lang/String;

    move-result-object v11

    const/16 v12, 0x10

    new-array v8, v12, [Ljava/lang/String;

    const-string v12, "C"

    aput-object v12, v8, v14

    const/4 v12, 0x1

    const-string v13, "D"

    aput-object v13, v8, v12

    const/4 v12, 0x2

    const-string v13, "E"

    aput-object v13, v8, v12

    const/4 v12, 0x3

    const-string v13, "F"

    aput-object v13, v8, v12

    const/4 v12, 0x4

    const-string v13, "G"

    aput-object v13, v8, v12

    const/4 v12, 0x5

    const-string v13, "H"

    aput-object v13, v8, v12

    const/4 v12, 0x6

    const-string v13, "I"

    aput-object v13, v8, v12

    const/4 v12, 0x7

    const-string v13, "J"

    aput-object v13, v8, v12

    const/16 v12, 0x8

    const-string v13, "K"

    aput-object v13, v8, v12

    const/16 v12, 0x9

    const-string v13, "L"

    aput-object v13, v8, v12

    const/16 v12, 0xa

    const-string v13, "M"

    aput-object v13, v8, v12

    const/16 v12, 0xb

    const-string v13, "N"

    aput-object v13, v8, v12

    const/16 v12, 0xc

    const-string v13, "O"

    aput-object v13, v8, v12

    const/16 v12, 0xd

    const-string v13, "P"

    aput-object v13, v8, v12

    const/16 v12, 0xe

    const-string v13, "Q"

    aput-object v13, v8, v12

    const/16 v12, 0xf

    const-string v13, "R"

    aput-object v13, v8, v12

    iput v14, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->usbDriverCount:I

    iget-object v12, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->usbDriverLabel:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->clear()V

    iget-object v12, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->usbDriverPath:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->clear()V

    iget-object v12, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->usbDriverLabelPcStyle:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->clear()V

    if-nez v11, :cond_1

    :cond_0
    return-void

    :cond_1
    new-instance v0, Ljava/io/File;

    const-string v12, "proc/mounts"

    invoke-direct {v0, v12}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v12

    if-eqz v12, :cond_3

    :cond_2
    const/4 v0, 0x0

    :cond_3
    const/4 v1, 0x1

    :goto_0
    array-length v12, v11

    if-ge v1, v12, :cond_0

    iget-object v12, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->storageManager:Lcom/mstar/android/storage/MStorageManager;

    aget-object v13, v11, v1

    invoke-virtual {v12, v13}, Lcom/mstar/android/storage/MStorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_4

    const-string v12, "mounted"

    invoke-virtual {v7, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_5

    :cond_4
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_5
    aget-object v5, v11, v1

    const-string v12, "/"

    invoke-virtual {v5, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    array-length v12, v6

    add-int/lit8 v12, v12, -0x1

    aget-object v3, v6, v12

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "updateUSBDriverInfo========>>>iiiiiiii=["

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "]"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v12, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->storageManager:Lcom/mstar/android/storage/MStorageManager;

    invoke-virtual {v12, v5}, Lcom/mstar/android/storage/MStorageManager;->getVolumeLabel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_6

    const-string v12, " "

    invoke-virtual {v10, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    const-string v10, ""

    const/4 v2, 0x0

    :goto_2
    array-length v12, v9

    if-lt v2, v12, :cond_7

    :cond_6
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v13, ": "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-direct {p0, v0, v5}, Lcom/konka/tvsettings/function/USBDiskSelecter;->getFileSystem(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    array-length v12, v8

    if-ge v1, v12, :cond_9

    if-lez v1, :cond_9

    add-int/lit8 v12, v1, -0x1

    aget-object v4, v8, v12

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v13, ": "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-direct {p0, v0, v5}, Lcom/konka/tvsettings/function/USBDiskSelecter;->getFileSystem(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v12, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->usbDriverLabelPcStyle:Ljava/util/ArrayList;

    iget v13, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->usbDriverCount:I

    invoke-virtual {v12, v13, v4}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    :goto_3
    iget-object v12, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->usbDriverLabel:Ljava/util/ArrayList;

    iget v13, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->usbDriverCount:I

    invoke-virtual {v12, v13, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    iget-object v12, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->usbDriverPath:Ljava/util/ArrayList;

    iget v13, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->usbDriverCount:I

    invoke-virtual {v12, v13, v5}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    iget v12, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->usbDriverCount:I

    add-int/lit8 v12, v12, 0x1

    iput v12, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->usbDriverCount:I

    goto/16 :goto_1

    :cond_7
    array-length v12, v9

    add-int/lit8 v12, v12, -0x1

    if-eq v2, v12, :cond_8

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v13, v9, v2

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_8
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v13, v9, v2

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto :goto_4

    :cond_9
    iget-object v12, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->usbDriverLabelPcStyle:Ljava/util/ArrayList;

    iget v13, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->usbDriverCount:I

    invoke-virtual {v12, v13, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_3
.end method


# virtual methods
.method public dismiss()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->usbReceiver:Lcom/konka/tvsettings/function/USBDiskSelecter$UsbReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->currentDialog:Landroid/app/Dialog;

    return-void
.end method

.method public getBestDiskPath()Ljava/lang/String;
    .locals 5

    invoke-direct {p0}, Lcom/konka/tvsettings/function/USBDiskSelecter;->getChooseDiskSettings()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-direct {p0}, Lcom/konka/tvsettings/function/USBDiskSelecter;->getChooseDiskPath()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/_MSTPVR"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/konka/tvsettings/function/USBDiskSelecter;->isFileExisted(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Lcom/konka/tvsettings/function/UsbReceiver;->isDiskExisted(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    move-object v0, v2

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    const-string v1, "/mnt/usb/"

    invoke-direct {p0, v1}, Lcom/konka/tvsettings/function/USBDiskSelecter;->getFirstUseableDiskAtParentDir(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "NO_DISK"

    goto :goto_0

    :cond_3
    const-string v0, "CHOOSE_DISK"

    goto :goto_0
.end method

.method public getDriverCount()I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->usbDriverCount:I

    return v0
.end method

.method public getUsbLabelByPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 19
    .param p1    # Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/function/USBDiskSelecter;->context:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/mstar/android/storage/MStorageManager;->getInstance(Landroid/content/Context;)Lcom/mstar/android/storage/MStorageManager;

    move-result-object v10

    invoke-virtual {v10}, Lcom/mstar/android/storage/MStorageManager;->getVolumePaths()[Ljava/lang/String;

    move-result-object v16

    const/4 v12, 0x0

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v13}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {v14}, Ljava/util/ArrayList;->clear()V

    if-nez v16, :cond_0

    const/16 v17, 0x0

    :goto_0
    return-object v17

    :cond_0
    new-instance v3, Ljava/io/File;

    const-string v17, "proc/mounts"

    move-object/from16 v0, v17

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v17

    if-eqz v17, :cond_1

    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v17

    if-eqz v17, :cond_2

    :cond_1
    const/4 v3, 0x0

    :cond_2
    const/4 v4, 0x0

    :goto_1
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    if-lt v4, v0, :cond_5

    const-string v17, "/"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_3

    const/16 v17, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    :cond_3
    const-string v17, "/"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_4

    const/16 v17, 0x0

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v18

    add-int/lit8 v18, v18, -0x1

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    :cond_4
    const/4 v4, 0x0

    :goto_2
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v17

    move/from16 v0, v17

    if-lt v4, v0, :cond_b

    const/16 v17, 0x0

    goto :goto_0

    :cond_5
    aget-object v17, v16, v4

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Lcom/mstar/android/storage/MStorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_6

    const-string v17, "mounted"

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_7

    :cond_6
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_7
    aget-object v7, v16, v4

    const-string v17, "/"

    move-object/from16 v0, v17

    invoke-virtual {v7, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    array-length v0, v8

    move/from16 v17, v0

    add-int/lit8 v17, v17, -0x1

    aget-object v6, v8, v17

    invoke-virtual {v10, v7}, Lcom/mstar/android/storage/MStorageManager;->getVolumeLabel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    if-eqz v15, :cond_8

    const-string v17, " "

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    const-string v15, ""

    const/4 v5, 0x0

    :goto_4
    array-length v0, v11

    move/from16 v17, v0

    move/from16 v0, v17

    if-lt v5, v0, :cond_9

    :cond_8
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v18, ": "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v7}, Lcom/konka/tvsettings/function/USBDiskSelecter;->getFileSystem(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "\n"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v13, v12, v6}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    invoke-virtual {v14, v12, v7}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    add-int/lit8 v12, v12, 0x1

    goto :goto_3

    :cond_9
    array-length v0, v11

    move/from16 v17, v0

    add-int/lit8 v17, v17, -0x1

    move/from16 v0, v17

    if-eq v5, v0, :cond_a

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-static {v15}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v18, v11, v5

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    :goto_5
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    :cond_a
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-static {v15}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v18, v11, v5

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    goto :goto_5

    :cond_b
    invoke-virtual {v14, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-eqz v17, :cond_c

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    goto/16 :goto_0

    :cond_c
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_2
.end method

.method public abstract onItemCancel()V
.end method

.method public abstract onItemChosen(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public registerUSBDetector()V
    .locals 3

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->usbReceiver:Lcom/konka/tvsettings/function/USBDiskSelecter$UsbReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->context:Landroid/content/Context;

    iget-object v2, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->usbReceiver:Lcom/konka/tvsettings/function/USBDiskSelecter$UsbReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public setUSBListener(Lcom/konka/tvsettings/function/USBDiskSelecter$usbListener;)V
    .locals 0
    .param p1    # Lcom/konka/tvsettings/function/USBDiskSelecter$usbListener;

    iput-object p1, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->usblistener:Lcom/konka/tvsettings/function/USBDiskSelecter$usbListener;

    return-void
.end method

.method public start()V
    .locals 5

    const/4 v4, 0x0

    iget v1, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->usbDriverCount:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->usbDriverLabel:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->usbDriverPath:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->usbDriverLabelPcStyle:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {p0, v4, v1, v2, v3}, Lcom/konka/tvsettings/function/USBDiskSelecter;->onItemChosen(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/app/Dialog;

    iget-object v1, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->context:Landroid/content/Context;

    const v2, 0x7f060026    # com.konka.tvsettings.R.style.UsbSelecterDialog

    invoke-direct {v0, v1, v2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    iget v1, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->usbDriverCount:I

    invoke-direct {p0, v1}, Lcom/konka/tvsettings/function/USBDiskSelecter;->getUSBSelecterView(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    iput-object v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter;->currentDialog:Landroid/app/Dialog;

    goto :goto_0
.end method
