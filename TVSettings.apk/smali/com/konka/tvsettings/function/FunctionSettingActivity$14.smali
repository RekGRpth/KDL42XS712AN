.class Lcom/konka/tvsettings/function/FunctionSettingActivity$14;
.super Lcom/konka/tvsettings/picture/PictureSettingItem1;
.source "FunctionSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/function/FunctionSettingActivity;->addItemGameMode()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

.field private final synthetic val$deskImpl:Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/function/FunctionSettingActivity;Landroid/app/Activity;IIILcom/konka/kkimplements/tv/mstar/SettingDeskImpl;)V
    .locals 0
    .param p2    # Landroid/app/Activity;
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iput-object p1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$14;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    iput-object p6, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$14;->val$deskImpl:Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/konka/tvsettings/picture/PictureSettingItem1;-><init>(Landroid/app/Activity;III)V

    return-void
.end method


# virtual methods
.method public doUpdate()V
    .locals 6

    iget-object v3, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$14;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    # getter for: Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemGameMode:Lcom/konka/tvsettings/picture/PictureSettingItem1;
    invoke-static {v3}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->access$11(Lcom/konka/tvsettings/function/FunctionSettingActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/tvsettings/picture/PictureSettingItem1;->getCurrentState()I

    move-result v3

    if-nez v3, :cond_0

    const/4 v2, 0x0

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "FunctionSetting::memc enable[ "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$14;->val$deskImpl:Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;

    invoke-virtual {v3, v2}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->setMemcEnable(Z)V

    iget-object v3, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$14;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    # getter for: Lcom/konka/tvsettings/function/FunctionSettingActivity;->tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v3}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->access$9(Lcom/konka/tvsettings/function/FunctionSettingActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    if-eqz v2, :cond_1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    const-string v4, "game_mode"

    const-string v5, "enable"

    invoke-virtual {v3, v4, v5}, Lcom/mstar/android/tvapi/common/TvManager;->setEnvironment(Ljava/lang/String;Ljava/lang/String;)Z

    :goto_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/PictureManager;->getInstance()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/mstar/android/tvapi/common/PictureManager;->setRGBBypass(Z)V

    :goto_2
    return-void

    :cond_0
    const/4 v2, 0x1

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    const-string v4, "game_mode"

    const-string v5, "disable"

    invoke-virtual {v3, v4, v5}, Lcom/mstar/android/tvapi/common/TvManager;->setEnvironment(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2
.end method
