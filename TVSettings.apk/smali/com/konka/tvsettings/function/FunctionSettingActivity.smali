.class public Lcom/konka/tvsettings/function/FunctionSettingActivity;
.super Lcom/konka/tvsettings/BaseKonkaActivity;
.source "FunctionSettingActivity.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_NR:[I

.field public static PersionCustomer:Ljava/lang/String;

.field private static mDynamicDenoiseStatus:I


# instance fields
.field CEC_Setting:Lcom/mstar/android/tvapi/common/vo/CecSetting;

.field private commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

.field public curCustomer:Ljava/lang/String;

.field private itemAlwaysTimeShift:Lcom/konka/tvsettings/picture/PictureSettingItem1;

.field private itemAlwaysTimeShiftlayout:Landroid/widget/LinearLayout;

.field private itemAudioOnly:Lcom/konka/tvsettings/picture/PictureSettingItem1;

.field private itemAudioOnlylayout:Landroid/widget/LinearLayout;

.field private itemCEC:Landroid/widget/LinearLayout;

.field private itemCECControl:Landroid/widget/LinearLayout;

.field private itemDemoMode:Lcom/konka/tvsettings/picture/PictureSettingItem1;

.field private itemDemoModelayout:Landroid/widget/LinearLayout;

.field private itemDynamicDenoise:Lcom/konka/tvsettings/picture/PictureSettingItem1;

.field private itemDynamicDenoiselayout:Landroid/widget/LinearLayout;

.field private itemGameMode:Lcom/konka/tvsettings/picture/PictureSettingItem1;

.field private itemGameModelayout:Landroid/widget/LinearLayout;

.field private itemLock:Landroid/widget/LinearLayout;

.field private itemOSDSetting:Landroid/widget/LinearLayout;

.field private itemPvr:Landroid/widget/LinearLayout;

.field private itemRGBRange:Lcom/konka/tvsettings/picture/PictureSettingItem1;

.field private itemReset:Landroid/widget/LinearLayout;

.field private itemRgbRangelayout:Landroid/widget/LinearLayout;

.field private itemSSU:Lcom/konka/tvsettings/picture/PictureSettingItem1;

.field private itemSSUlayout:Landroid/widget/LinearLayout;

.field private itemScreenSaver:Lcom/konka/tvsettings/picture/PictureSettingItem1;

.field private itemScreenSaverlayout:Landroid/widget/LinearLayout;

.field private itemStickerDemo:Lcom/konka/tvsettings/picture/PictureSettingItem1;

.field private itemStickerDemolayout:Landroid/widget/LinearLayout;

.field private myHandler:Landroid/os/Handler;

.field private pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

.field private settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;

.field private tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

.field private ursaType:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;


# direct methods
.method static synthetic $SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_NR()[I
    .locals 3

    sget-object v0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_NR:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->MS_NR_AUTO:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_5

    :goto_1
    :try_start_1
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->MS_NR_HIGH:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_4

    :goto_2
    :try_start_2
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->MS_NR_LOW:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_3

    :goto_3
    :try_start_3
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->MS_NR_MIDDLE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_2

    :goto_4
    :try_start_4
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->MS_NR_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_1

    :goto_5
    :try_start_5
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->MS_NR_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_0

    :goto_6
    sput-object v0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_NR:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_6

    :catch_1
    move-exception v1

    goto :goto_5

    :catch_2
    move-exception v1

    goto :goto_4

    :catch_3
    move-exception v1

    goto :goto_3

    :catch_4
    move-exception v1

    goto :goto_2

    :catch_5
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x3

    sput v0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->mDynamicDenoiseStatus:I

    const-string v0, "snowa"

    sput-object v0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->PersionCustomer:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;-><init>()V

    new-instance v0, Lcom/konka/tvsettings/function/FunctionSettingActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/function/FunctionSettingActivity$1;-><init>(Lcom/konka/tvsettings/function/FunctionSettingActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->myHandler:Landroid/os/Handler;

    iput-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemOSDSetting:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemCEC:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemCECControl:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemLock:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemPvr:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemScreenSaverlayout:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemStickerDemolayout:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemDemoModelayout:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemSSUlayout:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemDynamicDenoiselayout:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemRgbRangelayout:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemReset:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemGameModelayout:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemAlwaysTimeShiftlayout:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemAudioOnlylayout:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemScreenSaver:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    iput-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemStickerDemo:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    iput-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemDemoMode:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    iput-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemSSU:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    iput-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemDynamicDenoise:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    iput-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemRGBRange:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    iput-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemGameMode:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    iput-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemAlwaysTimeShift:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    iput-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemAudioOnly:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    iput-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    iput-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->CEC_Setting:Lcom/mstar/android/tvapi/common/vo/CecSetting;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/function/FunctionSettingActivity;)Lcom/konka/kkinterface/tv/CommonDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/function/FunctionSettingActivity;)Lcom/konka/kkinterface/tv/SettingDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;

    return-object v0
.end method

.method static synthetic access$10(Lcom/konka/tvsettings/function/FunctionSettingActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemRGBRange:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-object v0
.end method

.method static synthetic access$11(Lcom/konka/tvsettings/function/FunctionSettingActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemGameMode:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-object v0
.end method

.method static synthetic access$12(Lcom/konka/tvsettings/function/FunctionSettingActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemAlwaysTimeShift:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-object v0
.end method

.method static synthetic access$13(Lcom/konka/tvsettings/function/FunctionSettingActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemAudioOnly:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/function/FunctionSettingActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemScreenSaver:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/function/FunctionSettingActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemStickerDemo:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/function/FunctionSettingActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemDemoMode:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/function/FunctionSettingActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemSSU:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-object v0
.end method

.method static synthetic access$7(Lcom/konka/tvsettings/function/FunctionSettingActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemDynamicDenoise:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-object v0
.end method

.method static synthetic access$8(I)V
    .locals 0

    sput p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->mDynamicDenoiseStatus:I

    return-void
.end method

.method static synthetic access$9(Lcom/konka/tvsettings/function/FunctionSettingActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    return-object v0
.end method

.method private addItemAlwaysTimeShift()V
    .locals 7

    invoke-virtual {p0}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->getSettingMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;

    move-result-object v6

    invoke-virtual {v6}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->getAlTimeShift()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v5, 0x1

    :goto_0
    new-instance v0, Lcom/konka/tvsettings/function/FunctionSettingActivity$15;

    const v3, 0x7f0700c2    # com.konka.tvsettings.R.id.linearlayout_function_Always_time_shift

    const v4, 0x7f0b0032    # com.konka.tvsettings.R.array.str_arr_fun_AlwaysTimeShift_vals

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/konka/tvsettings/function/FunctionSettingActivity$15;-><init>(Lcom/konka/tvsettings/function/FunctionSettingActivity;Landroid/app/Activity;IIILcom/konka/kkimplements/tv/mstar/SettingDeskImpl;)V

    iput-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemAlwaysTimeShift:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-void

    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private addItemAudioOnly()V
    .locals 8

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->getSettingMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;

    move-result-object v6

    invoke-virtual {v6}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->getAudioCloseBacklight()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v5, 0x1

    :goto_0
    new-instance v0, Lcom/konka/tvsettings/function/FunctionSettingActivity$16;

    const v3, 0x7f0700c3    # com.konka.tvsettings.R.id.linearlayout_function_audio_only

    const v4, 0x7f0b0030    # com.konka.tvsettings.R.array.str_arr_fun_audioonly_vals

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/konka/tvsettings/function/FunctionSettingActivity$16;-><init>(Lcom/konka/tvsettings/function/FunctionSettingActivity;Landroid/app/Activity;IIILcom/konka/kkimplements/tv/mstar/SettingDeskImpl;)V

    iput-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemAudioOnly:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemAudioOnlylayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemAudioOnlylayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    :cond_0
    return-void

    :cond_1
    move v5, v7

    goto :goto_0
.end method

.method private addItemCEC()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemCEC:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/tvsettings/function/FunctionSettingActivity$5;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/function/FunctionSettingActivity$5;-><init>(Lcom/konka/tvsettings/function/FunctionSettingActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemOSDSetting:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemReset:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusUpId(I)V

    return-void
.end method

.method private addItemCECControl()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemCECControl:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/tvsettings/function/FunctionSettingActivity$6;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/function/FunctionSettingActivity$6;-><init>(Lcom/konka/tvsettings/function/FunctionSettingActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private addItemDemoMode()V
    .locals 7

    invoke-virtual {p0}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->getSettingMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;

    move-result-object v6

    invoke-virtual {v6}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->getDemoModeStatus()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v5, 0x1

    :goto_0
    new-instance v0, Lcom/konka/tvsettings/function/FunctionSettingActivity$9;

    const v3, 0x7f0700bb    # com.konka.tvsettings.R.id.linearlayout_function_DemoMode

    const v4, 0x7f0b002b    # com.konka.tvsettings.R.array.str_arr_fun_screensaver_vals

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/konka/tvsettings/function/FunctionSettingActivity$9;-><init>(Lcom/konka/tvsettings/function/FunctionSettingActivity;Landroid/app/Activity;IIILcom/konka/kkimplements/tv/mstar/SettingDeskImpl;)V

    iput-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemDemoMode:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-void

    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private addItemDynamicDenoise()V
    .locals 8

    invoke-static {}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->$SWITCH_TABLE$com$konka$kkinterface$tv$DataBaseDesk$EN_MS_NR()[I

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PictureDesk;->getDNR()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_NR;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x3

    sput v0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->mDynamicDenoiseStatus:I

    :goto_0
    new-instance v0, Lcom/konka/tvsettings/function/FunctionSettingActivity$11;

    const v3, 0x7f0700be    # com.konka.tvsettings.R.id.linearlayout_function_dynamic_denoise

    const v4, 0x7f0b0017    # com.konka.tvsettings.R.array.str_pic_setting_arr_dynamic_denoise

    sget v5, Lcom/konka/tvsettings/function/FunctionSettingActivity;->mDynamicDenoiseStatus:I

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/function/FunctionSettingActivity$11;-><init>(Lcom/konka/tvsettings/function/FunctionSettingActivity;Landroid/app/Activity;III)V

    iput-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemDynamicDenoise:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    :try_start_0
    sget-object v7, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->getCurrent3dFormat()Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v7

    :cond_0
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-eq v7, v0, :cond_1

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemDynamicDenoise:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    invoke-virtual {v0}, Lcom/konka/tvsettings/picture/PictureSettingItem1;->setStatusFbd()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    return-void

    :pswitch_0
    const/4 v0, 0x4

    sput v0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->mDynamicDenoiseStatus:I

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x0

    sput v0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->mDynamicDenoiseStatus:I

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x1

    sput v0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->mDynamicDenoiseStatus:I

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x2

    sput v0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->mDynamicDenoiseStatus:I

    goto :goto_0

    :catch_0
    move-exception v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private addItemGameMode()V
    .locals 10

    const/4 v9, 0x0

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->ursaType:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    sget-object v1, Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;->SIXM30_IC:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->ursaType:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    sget-object v1, Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;->SIXM40_IC:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    if-ne v0, v1, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->getSettingMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;

    move-result-object v6

    invoke-virtual {v6}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->getMemcEnable()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v5, 0x1

    :goto_0
    new-instance v0, Lcom/konka/tvsettings/function/FunctionSettingActivity$14;

    const v3, 0x7f0700c1    # com.konka.tvsettings.R.id.linearlayout_function_game_mode

    const v4, 0x7f0b002f    # com.konka.tvsettings.R.array.str_arr_fun_gamemode_vals

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/konka/tvsettings/function/FunctionSettingActivity$14;-><init>(Lcom/konka/tvsettings/function/FunctionSettingActivity;Landroid/app/Activity;IIILcom/konka/kkimplements/tv/mstar/SettingDeskImpl;)V

    iput-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemGameMode:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->ursaType:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    sget-object v1, Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;->SIXM40_IC:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_VGA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemGameModelayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemGameModelayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    move v5, v9

    goto :goto_0

    :cond_3
    :try_start_0
    sget-object v8, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->getCurrent3dFormat()Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v8

    :cond_4
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-eq v8, v0, :cond_1

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemGameModelayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemGameModelayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v7

    invoke-virtual {v7}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method private addItemLock()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    const v0, 0x7f0700b7    # com.konka.tvsettings.R.id.linearlayout_function_lock

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemLock:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->curCustomer:Ljava/lang/String;

    sget-object v1, Lcom/konka/tvsettings/function/FunctionSettingActivity;->PersionCustomer:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemLock:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemLock:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    :goto_0
    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemLock:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/tvsettings/function/FunctionSettingActivity$3;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/function/FunctionSettingActivity$3;-><init>(Lcom/konka/tvsettings/function/FunctionSettingActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v0, v1, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemLock:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemLock:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemLock:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemLock:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    goto :goto_0
.end method

.method private addItemOSDSetting()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemOSDSetting:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/tvsettings/function/FunctionSettingActivity$2;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/function/FunctionSettingActivity$2;-><init>(Lcom/konka/tvsettings/function/FunctionSettingActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemOSDSetting:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemReset:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusUpId(I)V

    return-void
.end method

.method private addItemPvrSystem()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemPvr:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/tvsettings/function/FunctionSettingActivity$4;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/function/FunctionSettingActivity$4;-><init>(Lcom/konka/tvsettings/function/FunctionSettingActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private addItemRGBRange()V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/SettingDesk;->GetColorRange()S

    move-result v5

    new-instance v0, Lcom/konka/tvsettings/function/FunctionSettingActivity$12;

    const v3, 0x7f0700bf    # com.konka.tvsettings.R.id.linearlayout_function_rgb_range

    const v4, 0x7f0b002c    # com.konka.tvsettings.R.array.str_arr_fun_rgbrange_vals

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/function/FunctionSettingActivity$12;-><init>(Lcom/konka/tvsettings/function/FunctionSettingActivity;Landroid/app/Activity;III)V

    iput-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemRGBRange:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-void
.end method

.method private addItemReset()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemReset:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/tvsettings/function/FunctionSettingActivity$13;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/function/FunctionSettingActivity$13;-><init>(Lcom/konka/tvsettings/function/FunctionSettingActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemReset:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemOSDSetting:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusDownId(I)V

    return-void
.end method

.method private addItemSSU()V
    .locals 7

    invoke-virtual {p0}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->getSettingMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;

    move-result-object v6

    invoke-virtual {v6}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->getSsuSaveModeStatus()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v5, 0x1

    :goto_0
    new-instance v0, Lcom/konka/tvsettings/function/FunctionSettingActivity$10;

    const v3, 0x7f0700c0    # com.konka.tvsettings.R.id.linearlayout_function_ssu

    const v4, 0x7f0b002b    # com.konka.tvsettings.R.array.str_arr_fun_screensaver_vals

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/konka/tvsettings/function/FunctionSettingActivity$10;-><init>(Lcom/konka/tvsettings/function/FunctionSettingActivity;Landroid/app/Activity;IIILcom/konka/kkimplements/tv/mstar/SettingDeskImpl;)V

    iput-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemSSU:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-void

    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private addItemScreenSaver()V
    .locals 7

    invoke-virtual {p0}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->getSettingMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;

    move-result-object v6

    invoke-virtual {v6}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->getScreenSaveModeStatus()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v5, 0x1

    :goto_0
    new-instance v0, Lcom/konka/tvsettings/function/FunctionSettingActivity$7;

    const v3, 0x7f0700b9    # com.konka.tvsettings.R.id.linearlayout_function_screensaver

    const v4, 0x7f0b002b    # com.konka.tvsettings.R.array.str_arr_fun_screensaver_vals

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/konka/tvsettings/function/FunctionSettingActivity$7;-><init>(Lcom/konka/tvsettings/function/FunctionSettingActivity;Landroid/app/Activity;IIILcom/konka/kkimplements/tv/mstar/SettingDeskImpl;)V

    iput-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemScreenSaver:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-void

    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private addItemStickerDemo()V
    .locals 7

    invoke-virtual {p0}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->getSettingMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;

    move-result-object v6

    invoke-virtual {v6}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->getStickerDemoStatus()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v5, 0x1

    :goto_0
    new-instance v0, Lcom/konka/tvsettings/function/FunctionSettingActivity$8;

    const v3, 0x7f0700ba    # com.konka.tvsettings.R.id.linearlayout_function_StickerDemo

    const v4, 0x7f0b002b    # com.konka.tvsettings.R.array.str_arr_fun_screensaver_vals

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/konka/tvsettings/function/FunctionSettingActivity$8;-><init>(Lcom/konka/tvsettings/function/FunctionSettingActivity;Landroid/app/Activity;IIILcom/konka/kkimplements/tv/mstar/SettingDeskImpl;)V

    iput-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemStickerDemo:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-void

    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addView()V
    .locals 2

    invoke-direct {p0}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->addItemOSDSetting()V

    invoke-direct {p0}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->addItemLock()V

    invoke-direct {p0}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->addItemPvrSystem()V

    invoke-direct {p0}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->addItemCEC()V

    invoke-direct {p0}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->addItemCECControl()V

    invoke-direct {p0}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->addItemScreenSaver()V

    invoke-direct {p0}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->addItemStickerDemo()V

    invoke-direct {p0}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->addItemDemoMode()V

    invoke-direct {p0}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->addItemDynamicDenoise()V

    invoke-direct {p0}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->addItemRGBRange()V

    invoke-direct {p0}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->addItemSSU()V

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->ursaType:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    sget-object v1, Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;->SIXM30_IC:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->ursaType:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    sget-object v1, Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;->SIXM40_IC:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    if-ne v0, v1, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->addItemGameMode()V

    :cond_1
    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableAlTimeShift()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->addItemAlwaysTimeShift()V

    :cond_2
    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->curCustomer:Ljava/lang/String;

    sget-object v1, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->PersionCustomer1:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->addItemAudioOnly()V

    :cond_3
    invoke-direct {p0}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->addItemReset()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const/4 v6, 0x1

    const/16 v5, 0x8

    const/4 v4, 0x0

    invoke-super {p0, p1}, Lcom/konka/tvsettings/BaseKonkaActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->getCustomerInfo()Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;

    move-result-object v2

    iget-object v2, v2, Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;->strCustomerName:Ljava/lang/String;

    iput-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->curCustomer:Ljava/lang/String;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->getUrsaSelect()Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->ursaType:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    const v2, 0x7f030018    # com.konka.tvsettings.R.layout.function_menu

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->setContentView(I)V

    const v2, 0x7f0700b6    # com.konka.tvsettings.R.id.linearlayout_function_osdsetting

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemOSDSetting:Landroid/widget/LinearLayout;

    const v2, 0x7f0700bf    # com.konka.tvsettings.R.id.linearlayout_function_rgb_range

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemRgbRangelayout:Landroid/widget/LinearLayout;

    const v2, 0x7f0700b9    # com.konka.tvsettings.R.id.linearlayout_function_screensaver

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemScreenSaverlayout:Landroid/widget/LinearLayout;

    const v2, 0x7f0700ba    # com.konka.tvsettings.R.id.linearlayout_function_StickerDemo

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemStickerDemolayout:Landroid/widget/LinearLayout;

    const v2, 0x7f0700bb    # com.konka.tvsettings.R.id.linearlayout_function_DemoMode

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemDemoModelayout:Landroid/widget/LinearLayout;

    const v2, 0x7f0700c0    # com.konka.tvsettings.R.id.linearlayout_function_ssu

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemSSUlayout:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->curCustomer:Ljava/lang/String;

    const-string v3, "Thai"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemSSUlayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_0
    const v2, 0x7f0700c1    # com.konka.tvsettings.R.id.linearlayout_function_game_mode

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemGameModelayout:Landroid/widget/LinearLayout;

    const v2, 0x7f0700c2    # com.konka.tvsettings.R.id.linearlayout_function_Always_time_shift

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemAlwaysTimeShiftlayout:Landroid/widget/LinearLayout;

    const v2, 0x7f0700c3    # com.konka.tvsettings.R.id.linearlayout_function_audio_only

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemAudioOnlylayout:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->ursaType:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    sget-object v3, Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;->SIXM30_IC:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->ursaType:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    sget-object v3, Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;->SIXM40_IC:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemGameModelayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_1
    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->curCustomer:Ljava/lang/String;

    sget-object v3, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->PersionCustomer1:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemAudioOnlylayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_2
    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableAlTimeShift()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemAlwaysTimeShiftlayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_3
    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableStickerDemo()Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemStickerDemolayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_4
    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableDemoMode()Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemDemoModelayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_5
    const v2, 0x7f0700be    # com.konka.tvsettings.R.id.linearlayout_function_dynamic_denoise

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemDynamicDenoiselayout:Landroid/widget/LinearLayout;

    const v2, 0x7f0700b8    # com.konka.tvsettings.R.id.linearlayout_function_pvr

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemPvr:Landroid/widget/LinearLayout;

    const v2, 0x7f0700bc    # com.konka.tvsettings.R.id.linearlayout_function_cec

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemCEC:Landroid/widget/LinearLayout;

    const v2, 0x7f0700bd    # com.konka.tvsettings.R.id.linearlayout_function_cec_control

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemCECControl:Landroid/widget/LinearLayout;

    const v2, 0x7f0700c4    # com.konka.tvsettings.R.id.linearlayout_function_reset

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemReset:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableCEC()Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemCEC:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemCECControl:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_6
    invoke-virtual {p0}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->addView()V

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v2, v3, :cond_7

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v2, v3, :cond_7

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v2, v3, :cond_7

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v2, v3, :cond_11

    :cond_7
    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->isHdmiSignalMode()Z

    move-result v2

    if-nez v2, :cond_e

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->isSignalStable()Z

    move-result v2

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemRgbRangelayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemRgbRangelayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    :goto_0
    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemCECControl:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemCECControl:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    :goto_1
    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v2, v3, :cond_12

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemPvr:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemPvr:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    :goto_2
    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v2, v3, :cond_13

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemScreenSaverlayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemScreenSaverlayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    :goto_3
    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableMonitor()Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemLock:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemPvr:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemScreenSaverlayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemAlwaysTimeShiftlayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_8
    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->isHdmiSignalMode()Z

    move-result v2

    if-nez v2, :cond_9

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v2, v3, :cond_a

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v2, v3, :cond_a

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v2, v3, :cond_a

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v2, v3, :cond_a

    :cond_9
    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_VGA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v2, v3, :cond_a

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/PictureDesk;->GetVideoArc()Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    move-result-object v2

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_DotByDot:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    if-ne v2, v3, :cond_b

    :cond_a
    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemDynamicDenoiselayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemDynamicDenoiselayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    :cond_b
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->getCurrent3dFormat()Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-eq v1, v2, :cond_c

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemGameModelayout:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemGameModelayout:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setEnabled(Z)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_c
    :goto_4
    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableAlTimeShift()Z

    move-result v2

    if-nez v2, :cond_d

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemAlwaysTimeShiftlayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemAlwaysTimeShiftlayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    :cond_d
    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->myHandler:Landroid/os/Handler;

    invoke-static {v2}, Lcom/konka/tvsettings/common/LittleDownTimer;->setHandler(Landroid/os/Handler;)V

    return-void

    :cond_e
    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/PictureDesk;->GetVideoArc()Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    move-result-object v2

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;->E_AR_DotByDot:Lcom/konka/kkinterface/tv/DataBaseDesk$MAPI_VIDEO_ARC_Type;

    if-eq v2, v3, :cond_f

    invoke-static {}, Lcom/konka/tvsettings/SwitchMenuHelper;->get4K2KMode()Z

    move-result v2

    if-eqz v2, :cond_10

    :cond_f
    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemRgbRangelayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemRgbRangelayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    goto/16 :goto_0

    :cond_10
    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemRgbRangelayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemRgbRangelayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    goto/16 :goto_0

    :cond_11
    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemRgbRangelayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemRgbRangelayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemCECControl:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemCECControl:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    goto/16 :goto_1

    :cond_12
    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemPvr:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemPvr:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemAlwaysTimeShiftlayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemAlwaysTimeShiftlayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    goto/16 :goto_2

    :cond_13
    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemScreenSaverlayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemScreenSaverlayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    goto/16 :goto_3

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_4
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const v3, 0x7f040005    # com.konka.tvsettings.R.anim.anim_menu_exit

    const/4 v2, 0x0

    const-string v1, "onKeyDown"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    sparse-switch p1, :sswitch_data_0

    :goto_0
    invoke-super {p0, p1, p2}, Lcom/konka/tvsettings/BaseKonkaActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    :goto_1
    return v1

    :sswitch_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->finish()V

    invoke-virtual {p0, v2, v3}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->overridePendingTransition(II)V

    :sswitch_1
    const-string v1, "Exit"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->finish()V

    invoke-virtual {p0, v2, v3}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->overridePendingTransition(II)V

    goto :goto_0

    :sswitch_2
    const/4 v1, 0x1

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x52 -> :sswitch_1
        0xb2 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->pauseMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resumeMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onResume()V

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onStart()V

    const v0, 0x7f040007    # com.konka.tvsettings.R.anim.anim_right_in

    const v1, 0x7f04000a    # com.konka.tvsettings.R.anim.anim_zoom_out

    invoke-virtual {p0, v0, v1}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resetMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onUserInteraction()V

    return-void
.end method
