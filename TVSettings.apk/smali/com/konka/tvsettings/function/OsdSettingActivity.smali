.class public Lcom/konka/tvsettings/function/OsdSettingActivity;
.super Lcom/konka/tvsettings/BaseKonkaActivity;
.source "OsdSettingActivity.java"


# static fields
.field protected static final Arabic:I = 0x1

.field protected static final Chinese:I = 0x9

.field protected static final ENGLISH:I = 0x0

.field protected static final Franch:I = 0x5

.field protected static final Hebrew:I = 0x8

.field protected static final Indonesia:I = 0x6

.field protected static final Kurdish:I = 0x3

.field protected static final Persian:I = 0x2

.field protected static final Russia:I = 0x4

.field protected static final Thai:I = 0xa

.field protected static final Turkish:I = 0x7


# instance fields
.field private OsdSettingLanguage:Landroid/widget/LinearLayout;

.field private itemOsdTime:Lcom/konka/tvsettings/picture/PictureSettingItem1;

.field private language_values:[Ljava/lang/CharSequence;

.field private m_iCurrLangIndex:I

.field private myHandler:Landroid/os/Handler;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HandlerLeak"
        }
    .end annotation
.end field

.field private settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;-><init>()V

    new-instance v0, Lcom/konka/tvsettings/function/OsdSettingActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/function/OsdSettingActivity$1;-><init>(Lcom/konka/tvsettings/function/OsdSettingActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/function/OsdSettingActivity;->myHandler:Landroid/os/Handler;

    iput-object v1, p0, Lcom/konka/tvsettings/function/OsdSettingActivity;->itemOsdTime:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    iput-object v1, p0, Lcom/konka/tvsettings/function/OsdSettingActivity;->OsdSettingLanguage:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/function/OsdSettingActivity;->language_values:[Ljava/lang/CharSequence;

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/tvsettings/function/OsdSettingActivity;->m_iCurrLangIndex:I

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/function/OsdSettingActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/OsdSettingActivity;->itemOsdTime:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/function/OsdSettingActivity;)Lcom/konka/kkinterface/tv/SettingDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/OsdSettingActivity;->settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;

    return-object v0
.end method

.method private addItemOsdLanguage()V
    .locals 2

    const v0, 0x7f0700ee    # com.konka.tvsettings.R.id.osd_setting_language

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/function/OsdSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/function/OsdSettingActivity;->OsdSettingLanguage:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Lcom/konka/tvsettings/function/OsdSettingActivity;->updateLanguageData()V

    invoke-direct {p0}, Lcom/konka/tvsettings/function/OsdSettingActivity;->updateCurrentLang()V

    iget-object v0, p0, Lcom/konka/tvsettings/function/OsdSettingActivity;->OsdSettingLanguage:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/tvsettings/function/OsdSettingActivity$2;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/function/OsdSettingActivity$2;-><init>(Lcom/konka/tvsettings/function/OsdSettingActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private addItemOsdTime()V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/konka/tvsettings/function/OsdSettingActivity;->settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/SettingDesk;->GetOsdDuration()S

    move-result v5

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "=============**********index time osd***********"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v0, Lcom/konka/tvsettings/function/OsdSettingActivity$3;

    const v3, 0x7f0700ef    # com.konka.tvsettings.R.id.osd_setting_time

    const v4, 0x7f0b002a    # com.konka.tvsettings.R.array.str_arr_osdtime_vals

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/function/OsdSettingActivity$3;-><init>(Lcom/konka/tvsettings/function/OsdSettingActivity;Landroid/app/Activity;III)V

    iput-object v0, p0, Lcom/konka/tvsettings/function/OsdSettingActivity;->itemOsdTime:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-void
.end method

.method private addView()V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/function/OsdSettingActivity;->addItemOsdLanguage()V

    invoke-direct {p0}, Lcom/konka/tvsettings/function/OsdSettingActivity;->addItemOsdTime()V

    return-void
.end method

.method private updateCurrentLang()V
    .locals 3

    iget-object v1, p0, Lcom/konka/tvsettings/function/OsdSettingActivity;->OsdSettingLanguage:Landroid/widget/LinearLayout;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/tvsettings/function/OsdSettingActivity;->OsdSettingLanguage:Landroid/widget/LinearLayout;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/function/OsdSettingActivity;->language_values:[Ljava/lang/CharSequence;

    iget v2, p0, Lcom/konka/tvsettings/function/OsdSettingActivity;->m_iCurrLangIndex:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateLanguageData()V
    .locals 4

    invoke-virtual {p0}, Lcom/konka/tvsettings/function/OsdSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v2, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "#the country is +++"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const-string v2, "US"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    iput v2, p0, Lcom/konka/tvsettings/function/OsdSettingActivity;->m_iCurrLangIndex:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v2, "EG"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    iput v2, p0, Lcom/konka/tvsettings/function/OsdSettingActivity;->m_iCurrLangIndex:I

    goto :goto_0

    :cond_2
    const-string v2, "IR"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x2

    iput v2, p0, Lcom/konka/tvsettings/function/OsdSettingActivity;->m_iCurrLangIndex:I

    goto :goto_0

    :cond_3
    const-string v2, "KD"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x3

    iput v2, p0, Lcom/konka/tvsettings/function/OsdSettingActivity;->m_iCurrLangIndex:I

    goto :goto_0

    :cond_4
    const-string v2, "RU"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x4

    iput v2, p0, Lcom/konka/tvsettings/function/OsdSettingActivity;->m_iCurrLangIndex:I

    goto :goto_0

    :cond_5
    const-string v2, "FR"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x5

    iput v2, p0, Lcom/konka/tvsettings/function/OsdSettingActivity;->m_iCurrLangIndex:I

    goto :goto_0

    :cond_6
    const-string v2, "ID"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    const/4 v2, 0x6

    iput v2, p0, Lcom/konka/tvsettings/function/OsdSettingActivity;->m_iCurrLangIndex:I

    goto :goto_0

    :cond_7
    const-string v2, "TR"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    const/4 v2, 0x7

    iput v2, p0, Lcom/konka/tvsettings/function/OsdSettingActivity;->m_iCurrLangIndex:I

    goto :goto_0

    :cond_8
    const-string v2, "IL"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v2, 0x8

    iput v2, p0, Lcom/konka/tvsettings/function/OsdSettingActivity;->m_iCurrLangIndex:I

    goto :goto_0

    :cond_9
    const-string v2, "CN"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0x9

    iput v2, p0, Lcom/konka/tvsettings/function/OsdSettingActivity;->m_iCurrLangIndex:I

    goto :goto_0

    :cond_a
    const-string v2, "TH"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0xa

    iput v2, p0, Lcom/konka/tvsettings/function/OsdSettingActivity;->m_iCurrLangIndex:I

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/konka/tvsettings/BaseKonkaActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030023    # com.konka.tvsettings.R.layout.osd_setting_menu

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/function/OsdSettingActivity;->setContentView(I)V

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/function/OsdSettingActivity;->settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;

    invoke-virtual {p0}, Lcom/konka/tvsettings/function/OsdSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0029    # com.konka.tvsettings.R.array.str_arr_osdlanguage_vals

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/function/OsdSettingActivity;->language_values:[Ljava/lang/CharSequence;

    invoke-direct {p0}, Lcom/konka/tvsettings/function/OsdSettingActivity;->addView()V

    iget-object v0, p0, Lcom/konka/tvsettings/function/OsdSettingActivity;->myHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/konka/tvsettings/common/LittleDownTimer;->setHandler(Landroid/os/Handler;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    sparse-switch p1, :sswitch_data_0

    :goto_0
    invoke-super {p0, p1, p2}, Lcom/konka/tvsettings/BaseKonkaActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    :goto_1
    return v2

    :sswitch_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/function/OsdSettingActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/function/OsdSettingActivity;->finish()V

    const/4 v2, 0x0

    const v3, 0x7f040005    # com.konka.tvsettings.R.anim.anim_menu_exit

    invoke-virtual {p0, v2, v3}, Lcom/konka/tvsettings/function/OsdSettingActivity;->overridePendingTransition(II)V

    goto :goto_0

    :sswitch_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v2, Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-virtual {v0, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/function/OsdSettingActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/function/OsdSettingActivity;->finish()V

    goto :goto_0

    :sswitch_2
    const/4 v2, 0x1

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x52 -> :sswitch_1
        0xb2 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->pauseMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resumeMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onResume()V

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onStart()V

    const v0, 0x7f040009    # com.konka.tvsettings.R.anim.anim_zoom_in

    const v1, 0x7f040008    # com.konka.tvsettings.R.anim.anim_right_out

    invoke-virtual {p0, v0, v1}, Lcom/konka/tvsettings/function/OsdSettingActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resetMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onUserInteraction()V

    return-void
.end method
