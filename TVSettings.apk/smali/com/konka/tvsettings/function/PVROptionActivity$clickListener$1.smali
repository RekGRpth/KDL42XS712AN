.class Lcom/konka/tvsettings/function/PVROptionActivity$clickListener$1;
.super Ljava/lang/Object;
.source "PVROptionActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener$1;->this$1:Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    packed-switch p2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    :try_start_0
    iget-object v1, p0, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener$1;->this$1:Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;
    invoke-static {v1}, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->access$1(Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;)Lcom/konka/tvsettings/function/PVROptionActivity;

    move-result-object v1

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$4(Lcom/konka/tvsettings/function/PVROptionActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v1

    const-wide/32 v2, 0x80000

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/PvrDesk;->setTimeShiftFileSize(J)V

    iget-object v1, p0, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener$1;->this$1:Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;
    invoke-static {v1}, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->access$1(Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;)Lcom/konka/tvsettings/function/PVROptionActivity;

    move-result-object v1

    const/4 v2, 0x0

    # invokes: Lcom/konka/tvsettings/function/PVROptionActivity;->saveLastTimeShiftSize(I)V
    invoke-static {v1, v2}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$5(Lcom/konka/tvsettings/function/PVROptionActivity;I)V

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/SettingDesk;->SetTimeShiftSize(S)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_1
    :try_start_1
    iget-object v1, p0, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener$1;->this$1:Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;
    invoke-static {v1}, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->access$1(Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;)Lcom/konka/tvsettings/function/PVROptionActivity;

    move-result-object v1

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$4(Lcom/konka/tvsettings/function/PVROptionActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v1

    const-wide/32 v2, 0x100000

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/PvrDesk;->setTimeShiftFileSize(J)V

    iget-object v1, p0, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener$1;->this$1:Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;
    invoke-static {v1}, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->access$1(Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;)Lcom/konka/tvsettings/function/PVROptionActivity;

    move-result-object v1

    const/4 v2, 0x1

    # invokes: Lcom/konka/tvsettings/function/PVROptionActivity;->saveLastTimeShiftSize(I)V
    invoke-static {v1, v2}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$5(Lcom/konka/tvsettings/function/PVROptionActivity;I)V

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/SettingDesk;->SetTimeShiftSize(S)Z

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener$1;->this$1:Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;
    invoke-static {v1}, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->access$1(Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;)Lcom/konka/tvsettings/function/PVROptionActivity;

    move-result-object v1

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$4(Lcom/konka/tvsettings/function/PVROptionActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v1

    const-wide/32 v2, 0x200000

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/PvrDesk;->setTimeShiftFileSize(J)V

    iget-object v1, p0, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener$1;->this$1:Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;
    invoke-static {v1}, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->access$1(Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;)Lcom/konka/tvsettings/function/PVROptionActivity;

    move-result-object v1

    const/4 v2, 0x2

    # invokes: Lcom/konka/tvsettings/function/PVROptionActivity;->saveLastTimeShiftSize(I)V
    invoke-static {v1, v2}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$5(Lcom/konka/tvsettings/function/PVROptionActivity;I)V

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/SettingDesk;->SetTimeShiftSize(S)Z

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener$1;->this$1:Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;
    invoke-static {v1}, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->access$1(Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;)Lcom/konka/tvsettings/function/PVROptionActivity;

    move-result-object v1

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$4(Lcom/konka/tvsettings/function/PVROptionActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v1

    const-wide/32 v2, 0x400000

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/PvrDesk;->setTimeShiftFileSize(J)V

    iget-object v1, p0, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener$1;->this$1:Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;
    invoke-static {v1}, Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;->access$1(Lcom/konka/tvsettings/function/PVROptionActivity$clickListener;)Lcom/konka/tvsettings/function/PVROptionActivity;

    move-result-object v1

    const/4 v2, 0x3

    # invokes: Lcom/konka/tvsettings/function/PVROptionActivity;->saveLastTimeShiftSize(I)V
    invoke-static {v1, v2}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$5(Lcom/konka/tvsettings/function/PVROptionActivity;I)V

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v1

    const/4 v2, 0x3

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/SettingDesk;->SetTimeShiftSize(S)Z
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
