.class Lcom/konka/tvsettings/function/FunctionSettingActivity$3;
.super Ljava/lang/Object;
.source "FunctionSettingActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/function/FunctionSettingActivity;->addItemLock()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/function/FunctionSettingActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$3;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    const/4 v4, 0x5

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$3;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    iget-object v2, v2, Lcom/konka/tvsettings/function/FunctionSettingActivity;->curCustomer:Ljava/lang/String;

    sget-object v3, Lcom/konka/tvsettings/function/FunctionSettingActivity;->PersionCustomer:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$3;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-virtual {v2}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->IsChildLocked()Z

    move-result v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getParentalcontrolManager()Lcom/mstar/android/tvapi/common/ParentalcontrolManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/ParentalcontrolManager;->isSystemLock()Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$3;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-virtual {v2}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->finish()V

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$3;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-virtual {v2}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/konka/tvsettings/popup/CheckParentalPwd;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v2, "lockType"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$3;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-virtual {v2, v0}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$3;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-virtual {v2}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/konka/tvsettings/function/LockSettingActivity;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$3;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-virtual {v2, v0}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$3;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-virtual {v2}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->finish()V

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getParentalcontrolManager()Lcom/mstar/android/tvapi/common/ParentalcontrolManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/ParentalcontrolManager;->isSystemLock()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$3;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-virtual {v2}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->finish()V

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$3;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-virtual {v2}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/konka/tvsettings/popup/CheckParentalPwd;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v2, "lockType"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$3;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-virtual {v2, v0}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$3;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-virtual {v2}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/konka/tvsettings/function/LockSettingActivity;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$3;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-virtual {v2, v0}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$3;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-virtual {v2}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->finish()V

    goto :goto_0
.end method
