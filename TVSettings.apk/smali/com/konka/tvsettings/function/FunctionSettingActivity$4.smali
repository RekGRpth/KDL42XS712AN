.class Lcom/konka/tvsettings/function/FunctionSettingActivity$4;
.super Ljava/lang/Object;
.source "FunctionSettingActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/function/FunctionSettingActivity;->addItemPvrSystem()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/function/FunctionSettingActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$4;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$4;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    # getter for: Lcom/konka/tvsettings/function/FunctionSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;
    invoke-static {v2}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->access$0(Lcom/konka/tvsettings/function/FunctionSettingActivity;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableAlTimeShift()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$4;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    # getter for: Lcom/konka/tvsettings/function/FunctionSettingActivity;->settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;
    invoke-static {v2}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->access$1(Lcom/konka/tvsettings/function/FunctionSettingActivity;)Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/SettingDesk;->getAlTimeShift()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$4;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    const v3, 0x7f0a0194    # com.konka.tvsettings.R.string.warning_stop_alwaystimeshift

    const/4 v4, 0x5

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$4;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-virtual {v2}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/konka/tvsettings/function/PVROptionActivity;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$4;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-virtual {v2, v0}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$4;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-virtual {v2}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->finish()V

    goto :goto_0
.end method
