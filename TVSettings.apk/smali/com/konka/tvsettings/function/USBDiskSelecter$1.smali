.class Lcom/konka/tvsettings/function/USBDiskSelecter$1;
.super Ljava/lang/Object;
.source "USBDiskSelecter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/function/USBDiskSelecter;->getUSBSelecterView(I)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/function/USBDiskSelecter;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/function/USBDiskSelecter;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$1;->this$0:Lcom/konka/tvsettings/function/USBDiskSelecter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v3, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$1;->this$0:Lcom/konka/tvsettings/function/USBDiskSelecter;

    iget-object v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$1;->this$0:Lcom/konka/tvsettings/function/USBDiskSelecter;

    # getter for: Lcom/konka/tvsettings/function/USBDiskSelecter;->usbDriverLabel:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/konka/tvsettings/function/USBDiskSelecter;->access$10(Lcom/konka/tvsettings/function/USBDiskSelecter;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$1;->this$0:Lcom/konka/tvsettings/function/USBDiskSelecter;

    # getter for: Lcom/konka/tvsettings/function/USBDiskSelecter;->usbDriverPath:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/konka/tvsettings/function/USBDiskSelecter;->access$2(Lcom/konka/tvsettings/function/USBDiskSelecter;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$1;->this$0:Lcom/konka/tvsettings/function/USBDiskSelecter;

    # getter for: Lcom/konka/tvsettings/function/USBDiskSelecter;->usbDriverLabelPcStyle:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/konka/tvsettings/function/USBDiskSelecter;->access$1(Lcom/konka/tvsettings/function/USBDiskSelecter;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, p3, v0, v1, v2}, Lcom/konka/tvsettings/function/USBDiskSelecter;->onItemChosen(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$1;->this$0:Lcom/konka/tvsettings/function/USBDiskSelecter;

    iget-boolean v0, v0, Lcom/konka/tvsettings/function/USBDiskSelecter;->noDismiss:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$1;->this$0:Lcom/konka/tvsettings/function/USBDiskSelecter;

    # getter for: Lcom/konka/tvsettings/function/USBDiskSelecter;->currentDialog:Landroid/app/Dialog;
    invoke-static {v0}, Lcom/konka/tvsettings/function/USBDiskSelecter;->access$5(Lcom/konka/tvsettings/function/USBDiskSelecter;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    iget-object v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$1;->this$0:Lcom/konka/tvsettings/function/USBDiskSelecter;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/konka/tvsettings/function/USBDiskSelecter;->access$7(Lcom/konka/tvsettings/function/USBDiskSelecter;Landroid/app/Dialog;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$1;->this$0:Lcom/konka/tvsettings/function/USBDiskSelecter;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/konka/tvsettings/function/USBDiskSelecter;->noDismiss:Z

    goto :goto_0
.end method
