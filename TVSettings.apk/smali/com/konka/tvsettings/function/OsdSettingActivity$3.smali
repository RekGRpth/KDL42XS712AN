.class Lcom/konka/tvsettings/function/OsdSettingActivity$3;
.super Lcom/konka/tvsettings/picture/PictureSettingItem1;
.source "OsdSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/function/OsdSettingActivity;->addItemOsdTime()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/function/OsdSettingActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/function/OsdSettingActivity;Landroid/app/Activity;III)V
    .locals 0
    .param p2    # Landroid/app/Activity;
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iput-object p1, p0, Lcom/konka/tvsettings/function/OsdSettingActivity$3;->this$0:Lcom/konka/tvsettings/function/OsdSettingActivity;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/konka/tvsettings/picture/PictureSettingItem1;-><init>(Landroid/app/Activity;III)V

    return-void
.end method


# virtual methods
.method public doUpdate()V
    .locals 3

    iget-object v1, p0, Lcom/konka/tvsettings/function/OsdSettingActivity$3;->this$0:Lcom/konka/tvsettings/function/OsdSettingActivity;

    # getter for: Lcom/konka/tvsettings/function/OsdSettingActivity;->itemOsdTime:Lcom/konka/tvsettings/picture/PictureSettingItem1;
    invoke-static {v1}, Lcom/konka/tvsettings/function/OsdSettingActivity;->access$0(Lcom/konka/tvsettings/function/OsdSettingActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/tvsettings/picture/PictureSettingItem1;->getCurrentState()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "========get now=====**********index time osd***********"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/tvsettings/function/OsdSettingActivity$3;->this$0:Lcom/konka/tvsettings/function/OsdSettingActivity;

    # getter for: Lcom/konka/tvsettings/function/OsdSettingActivity;->settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/function/OsdSettingActivity;->access$1(Lcom/konka/tvsettings/function/OsdSettingActivity;)Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v1

    int-to-short v2, v0

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/SettingDesk;->SetOsdDuration(S)Z

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->stopMenu()V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/tvsettings/function/OsdSettingActivity$3;->this$0:Lcom/konka/tvsettings/function/OsdSettingActivity;

    # getter for: Lcom/konka/tvsettings/function/OsdSettingActivity;->settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/function/OsdSettingActivity;->access$1(Lcom/konka/tvsettings/function/OsdSettingActivity;)Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/SettingDesk;->getOsdTimeoutSecond()I

    move-result v1

    invoke-static {v1}, Lcom/konka/tvsettings/common/LittleDownTimer;->setMenuStatus(I)V

    goto :goto_0
.end method
