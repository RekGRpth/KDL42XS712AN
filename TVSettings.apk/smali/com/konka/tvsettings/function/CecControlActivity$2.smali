.class Lcom/konka/tvsettings/function/CecControlActivity$2;
.super Ljava/lang/Object;
.source "CecControlActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/function/CecControlActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/function/CecControlActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/function/CecControlActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/function/CecControlActivity$2;->this$0:Lcom/konka/tvsettings/function/CecControlActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/konka/tvsettings/function/CecControlActivity$2;->this$0:Lcom/konka/tvsettings/function/CecControlActivity;

    # getter for: Lcom/konka/tvsettings/function/CecControlActivity;->adapter:Lcom/konka/tvsettings/function/CecControlActivity$AdListAdapter;
    invoke-static {v1}, Lcom/konka/tvsettings/function/CecControlActivity;->access$1(Lcom/konka/tvsettings/function/CecControlActivity;)Lcom/konka/tvsettings/function/CecControlActivity$AdListAdapter;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/konka/tvsettings/function/CecControlActivity$AdListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    const-string v1, "wangjian"

    const-string v2, "-----------Cec Control power ------------"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/tvsettings/function/CecControlActivity$2;->this$0:Lcom/konka/tvsettings/function/CecControlActivity;

    iget-object v1, v1, Lcom/konka/tvsettings/function/CecControlActivity;->cecmanager:Lcom/mstar/android/tvapi/common/CecManager;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/CecManager;->cecStandby(I)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/konka/tvsettings/function/CecControlActivity$2;->this$0:Lcom/konka/tvsettings/function/CecControlActivity;

    # getter for: Lcom/konka/tvsettings/function/CecControlActivity;->adapter:Lcom/konka/tvsettings/function/CecControlActivity$AdListAdapter;
    invoke-static {v1}, Lcom/konka/tvsettings/function/CecControlActivity;->access$1(Lcom/konka/tvsettings/function/CecControlActivity;)Lcom/konka/tvsettings/function/CecControlActivity$AdListAdapter;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/konka/tvsettings/function/CecControlActivity$AdListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :try_start_1
    const-string v1, "wangjian"

    const-string v2, "-----------Cec Control root menu------------"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/tvsettings/function/CecControlActivity$2;->this$0:Lcom/konka/tvsettings/function/CecControlActivity;

    iget-object v1, v1, Lcom/konka/tvsettings/function/CecControlActivity;->cecmanager:Lcom/mstar/android/tvapi/common/CecManager;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/CecManager;->cecRealIr(I)I

    iget-object v1, p0, Lcom/konka/tvsettings/function/CecControlActivity$2;->this$0:Lcom/konka/tvsettings/function/CecControlActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/function/CecControlActivity;->onBackPressed()V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/konka/tvsettings/function/CecControlActivity$2;->this$0:Lcom/konka/tvsettings/function/CecControlActivity;

    # getter for: Lcom/konka/tvsettings/function/CecControlActivity;->adapter:Lcom/konka/tvsettings/function/CecControlActivity$AdListAdapter;
    invoke-static {v1}, Lcom/konka/tvsettings/function/CecControlActivity;->access$1(Lcom/konka/tvsettings/function/CecControlActivity;)Lcom/konka/tvsettings/function/CecControlActivity$AdListAdapter;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/konka/tvsettings/function/CecControlActivity$AdListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :try_start_2
    const-string v1, "wangjian"

    const-string v2, "-----------Cec Control setup menu ------------"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/tvsettings/function/CecControlActivity$2;->this$0:Lcom/konka/tvsettings/function/CecControlActivity;

    iget-object v1, v1, Lcom/konka/tvsettings/function/CecControlActivity;->cecmanager:Lcom/mstar/android/tvapi/common/CecManager;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/CecManager;->cecRealIr(I)I

    iget-object v1, p0, Lcom/konka/tvsettings/function/CecControlActivity$2;->this$0:Lcom/konka/tvsettings/function/CecControlActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/function/CecControlActivity;->onBackPressed()V
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_2
    :goto_2
    iget-object v1, p0, Lcom/konka/tvsettings/function/CecControlActivity$2;->this$0:Lcom/konka/tvsettings/function/CecControlActivity;

    # getter for: Lcom/konka/tvsettings/function/CecControlActivity;->adapter:Lcom/konka/tvsettings/function/CecControlActivity$AdListAdapter;
    invoke-static {v1}, Lcom/konka/tvsettings/function/CecControlActivity;->access$1(Lcom/konka/tvsettings/function/CecControlActivity;)Lcom/konka/tvsettings/function/CecControlActivity$AdListAdapter;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/konka/tvsettings/function/CecControlActivity$AdListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    :try_start_3
    const-string v1, "wangjian"

    const-string v2, "-----------Cec Control menu------------"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/tvsettings/function/CecControlActivity$2;->this$0:Lcom/konka/tvsettings/function/CecControlActivity;

    iget-object v1, v1, Lcom/konka/tvsettings/function/CecControlActivity;->cecmanager:Lcom/mstar/android/tvapi/common/CecManager;

    const/16 v2, 0xb

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/CecManager;->cecRealIr(I)I

    iget-object v1, p0, Lcom/konka/tvsettings/function/CecControlActivity$2;->this$0:Lcom/konka/tvsettings/function/CecControlActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/function/CecControlActivity;->onBackPressed()V
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_3

    :cond_3
    :goto_3
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_3
.end method
