.class Lcom/konka/tvsettings/function/USBDiskSelecter$UsbReceiver;
.super Landroid/content/BroadcastReceiver;
.source "USBDiskSelecter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/function/USBDiskSelecter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UsbReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/function/USBDiskSelecter;


# direct methods
.method private constructor <init>(Lcom/konka/tvsettings/function/USBDiskSelecter;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$UsbReceiver;->this$0:Lcom/konka/tvsettings/function/USBDiskSelecter;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/tvsettings/function/USBDiskSelecter;Lcom/konka/tvsettings/function/USBDiskSelecter$UsbReceiver;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/function/USBDiskSelecter$UsbReceiver;-><init>(Lcom/konka/tvsettings/function/USBDiskSelecter;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v3, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_0
    iget-object v3, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$UsbReceiver;->this$0:Lcom/konka/tvsettings/function/USBDiskSelecter;

    # invokes: Lcom/konka/tvsettings/function/USBDiskSelecter;->updateUSBDriverInfo()V
    invoke-static {v3}, Lcom/konka/tvsettings/function/USBDiskSelecter;->access$4(Lcom/konka/tvsettings/function/USBDiskSelecter;)V

    iget-object v3, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$UsbReceiver;->this$0:Lcom/konka/tvsettings/function/USBDiskSelecter;

    # getter for: Lcom/konka/tvsettings/function/USBDiskSelecter;->currentDialog:Landroid/app/Dialog;
    invoke-static {v3}, Lcom/konka/tvsettings/function/USBDiskSelecter;->access$5(Lcom/konka/tvsettings/function/USBDiskSelecter;)Landroid/app/Dialog;

    move-result-object v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$UsbReceiver;->this$0:Lcom/konka/tvsettings/function/USBDiskSelecter;

    # getter for: Lcom/konka/tvsettings/function/USBDiskSelecter;->usbDriverCount:I
    invoke-static {v3}, Lcom/konka/tvsettings/function/USBDiskSelecter;->access$6(Lcom/konka/tvsettings/function/USBDiskSelecter;)I

    move-result v3

    const/4 v4, 0x1

    if-ge v3, v4, :cond_2

    iget-object v3, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$UsbReceiver;->this$0:Lcom/konka/tvsettings/function/USBDiskSelecter;

    # getter for: Lcom/konka/tvsettings/function/USBDiskSelecter;->currentDialog:Landroid/app/Dialog;
    invoke-static {v3}, Lcom/konka/tvsettings/function/USBDiskSelecter;->access$5(Lcom/konka/tvsettings/function/USBDiskSelecter;)Landroid/app/Dialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Dialog;->dismiss()V

    iget-object v3, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$UsbReceiver;->this$0:Lcom/konka/tvsettings/function/USBDiskSelecter;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/konka/tvsettings/function/USBDiskSelecter;->access$7(Lcom/konka/tvsettings/function/USBDiskSelecter;Landroid/app/Dialog;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v3, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$UsbReceiver;->this$0:Lcom/konka/tvsettings/function/USBDiskSelecter;

    # getter for: Lcom/konka/tvsettings/function/USBDiskSelecter;->currentDialog:Landroid/app/Dialog;
    invoke-static {v3}, Lcom/konka/tvsettings/function/USBDiskSelecter;->access$5(Lcom/konka/tvsettings/function/USBDiskSelecter;)Landroid/app/Dialog;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$UsbReceiver;->this$0:Lcom/konka/tvsettings/function/USBDiskSelecter;

    iget-object v5, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$UsbReceiver;->this$0:Lcom/konka/tvsettings/function/USBDiskSelecter;

    # getter for: Lcom/konka/tvsettings/function/USBDiskSelecter;->usbDriverCount:I
    invoke-static {v5}, Lcom/konka/tvsettings/function/USBDiskSelecter;->access$6(Lcom/konka/tvsettings/function/USBDiskSelecter;)I

    move-result v5

    # invokes: Lcom/konka/tvsettings/function/USBDiskSelecter;->getUSBSelecterView(I)Landroid/view/View;
    invoke-static {v4, v5}, Lcom/konka/tvsettings/function/USBDiskSelecter;->access$8(Lcom/konka/tvsettings/function/USBDiskSelecter;I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    :cond_3
    iget-object v3, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$UsbReceiver;->this$0:Lcom/konka/tvsettings/function/USBDiskSelecter;

    # getter for: Lcom/konka/tvsettings/function/USBDiskSelecter;->usblistener:Lcom/konka/tvsettings/function/USBDiskSelecter$usbListener;
    invoke-static {v3}, Lcom/konka/tvsettings/function/USBDiskSelecter;->access$9(Lcom/konka/tvsettings/function/USBDiskSelecter;)Lcom/konka/tvsettings/function/USBDiskSelecter$usbListener;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    const-string v3, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$UsbReceiver;->this$0:Lcom/konka/tvsettings/function/USBDiskSelecter;

    # getter for: Lcom/konka/tvsettings/function/USBDiskSelecter;->usblistener:Lcom/konka/tvsettings/function/USBDiskSelecter$usbListener;
    invoke-static {v3}, Lcom/konka/tvsettings/function/USBDiskSelecter;->access$9(Lcom/konka/tvsettings/function/USBDiskSelecter;)Lcom/konka/tvsettings/function/USBDiskSelecter$usbListener;

    move-result-object v3

    invoke-interface {v3, v1}, Lcom/konka/tvsettings/function/USBDiskSelecter$usbListener;->onUSBMounted(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v3, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$UsbReceiver;->this$0:Lcom/konka/tvsettings/function/USBDiskSelecter;

    # getter for: Lcom/konka/tvsettings/function/USBDiskSelecter;->usblistener:Lcom/konka/tvsettings/function/USBDiskSelecter$usbListener;
    invoke-static {v3}, Lcom/konka/tvsettings/function/USBDiskSelecter;->access$9(Lcom/konka/tvsettings/function/USBDiskSelecter;)Lcom/konka/tvsettings/function/USBDiskSelecter$usbListener;

    move-result-object v3

    invoke-interface {v3, v1}, Lcom/konka/tvsettings/function/USBDiskSelecter$usbListener;->onUSBUnmounted(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    const-string v3, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$UsbReceiver;->this$0:Lcom/konka/tvsettings/function/USBDiskSelecter;

    # getter for: Lcom/konka/tvsettings/function/USBDiskSelecter;->usblistener:Lcom/konka/tvsettings/function/USBDiskSelecter$usbListener;
    invoke-static {v3}, Lcom/konka/tvsettings/function/USBDiskSelecter;->access$9(Lcom/konka/tvsettings/function/USBDiskSelecter;)Lcom/konka/tvsettings/function/USBDiskSelecter$usbListener;

    move-result-object v3

    invoke-interface {v3, v1}, Lcom/konka/tvsettings/function/USBDiskSelecter$usbListener;->onUSBEject(Ljava/lang/String;)V

    goto :goto_0
.end method
