.class public Lcom/konka/tvsettings/function/LockSettingActivity;
.super Lcom/konka/tvsettings/BaseKonkaActivity;
.source "LockSettingActivity.java"


# static fields
.field public static CurrentPGRate:I

.field public static PersionCustomer:Ljava/lang/String;


# instance fields
.field private bCheckisAustralia:Z

.field private commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

.field public curCustomer:Ljava/lang/String;

.field private itemBlockSystem:Landroid/widget/LinearLayout;

.field private itemChildLock:Lcom/konka/tvsettings/picture/PictureSettingItem1;

.field private itemChildLocklayout:Landroid/widget/LinearLayout;

.field private itemLockSystem:Lcom/konka/tvsettings/picture/PictureSettingItem1;

.field private itemLockSystemlayout:Landroid/widget/LinearLayout;

.field private itemParentalGuidance:Lcom/konka/tvsettings/picture/PictureSettingItem1;

.field private itemParentalGuidancelayout:Landroid/widget/LinearLayout;

.field private itemSetPassword:Landroid/widget/LinearLayout;

.field private miCurCountry:I

.field private myHandler:Landroid/os/Handler;

.field private serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "snowa"

    sput-object v0, Lcom/konka/tvsettings/function/LockSettingActivity;->PersionCustomer:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;-><init>()V

    new-instance v0, Lcom/konka/tvsettings/function/LockSettingActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/function/LockSettingActivity$1;-><init>(Lcom/konka/tvsettings/function/LockSettingActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->myHandler:Landroid/os/Handler;

    iput-object v1, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->itemLockSystem:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    iput-object v1, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->itemLockSystemlayout:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->itemSetPassword:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->itemBlockSystem:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->itemChildLocklayout:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->itemParentalGuidancelayout:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->itemParentalGuidance:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    iput-object v1, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->itemChildLock:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    iput-object v1, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    iput v2, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->miCurCountry:I

    iput-boolean v2, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->bCheckisAustralia:Z

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/function/LockSettingActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->itemLockSystem:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/function/LockSettingActivity;Lcom/konka/kkinterface/tv/TvDeskProvider;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    return-void
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/function/LockSettingActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/function/LockSettingActivity;)Lcom/konka/kkinterface/tv/CommonDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/function/LockSettingActivity;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->itemParentalGuidancelayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/function/LockSettingActivity;Landroid/widget/LinearLayout;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/konka/tvsettings/function/LockSettingActivity;->setOptionItemEnabled(Landroid/widget/LinearLayout;Z)V

    return-void
.end method

.method static synthetic access$6(Lcom/konka/tvsettings/function/LockSettingActivity;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->itemSetPassword:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$7(Lcom/konka/tvsettings/function/LockSettingActivity;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->itemBlockSystem:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$8(Lcom/konka/tvsettings/function/LockSettingActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->itemChildLock:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-object v0
.end method

.method private addItemBlockSystem()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->itemBlockSystem:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/tvsettings/function/LockSettingActivity$4;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/function/LockSettingActivity$4;-><init>(Lcom/konka/tvsettings/function/LockSettingActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private addItemChildLock()V
    .locals 7

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/konka/tvsettings/function/LockSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->IsChildLocked()Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v5, 0x1

    :goto_0
    new-instance v0, Lcom/konka/tvsettings/function/LockSettingActivity$7;

    const v3, 0x7f0700da    # com.konka.tvsettings.R.id.lock_adv_childlock

    const v4, 0x7f0b0036    # com.konka.tvsettings.R.array.str_arr_fun_locksystem_vals

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/function/LockSettingActivity$7;-><init>(Lcom/konka/tvsettings/function/LockSettingActivity;Landroid/app/Activity;III)V

    iput-object v0, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->itemChildLock:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-void

    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private addItemLockSystem()V
    .locals 7

    const/4 v0, 0x0

    const/4 v5, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getParentalcontrolManager()Lcom/mstar/android/tvapi/common/ParentalcontrolManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/ParentalcontrolManager;->isSystemLock()Z

    move-result v6

    if-nez v6, :cond_1

    move v5, v0

    :goto_0
    iget-object v1, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->itemLockSystemlayout:Landroid/widget/LinearLayout;

    invoke-direct {p0, v1, v0}, Lcom/konka/tvsettings/function/LockSettingActivity;->setOptionItemEnabled(Landroid/widget/LinearLayout;Z)V

    iget-object v1, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->itemBlockSystem:Landroid/widget/LinearLayout;

    invoke-direct {p0, v1, v0}, Lcom/konka/tvsettings/function/LockSettingActivity;->setOptionItemEnabled(Landroid/widget/LinearLayout;Z)V

    iget-object v1, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->itemSetPassword:Landroid/widget/LinearLayout;

    invoke-direct {p0, v1, v0}, Lcom/konka/tvsettings/function/LockSettingActivity;->setOptionItemEnabled(Landroid/widget/LinearLayout;Z)V

    iget-object v1, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->itemParentalGuidancelayout:Landroid/widget/LinearLayout;

    invoke-direct {p0, v1, v0}, Lcom/konka/tvsettings/function/LockSettingActivity;->setOptionItemEnabled(Landroid/widget/LinearLayout;Z)V

    :cond_0
    :goto_1
    new-instance v0, Lcom/konka/tvsettings/function/LockSettingActivity$2;

    const v3, 0x7f0700d6    # com.konka.tvsettings.R.id.lock_adv_locksystem

    const v4, 0x7f0b0036    # com.konka.tvsettings.R.array.str_arr_fun_locksystem_vals

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/function/LockSettingActivity$2;-><init>(Lcom/konka/tvsettings/function/LockSettingActivity;Landroid/app/Activity;III)V

    iput-object v0, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->itemLockSystem:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-void

    :cond_1
    const/4 v5, 0x1

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->itemBlockSystem:Landroid/widget/LinearLayout;

    invoke-direct {p0, v1, v6}, Lcom/konka/tvsettings/function/LockSettingActivity;->setOptionItemEnabled(Landroid/widget/LinearLayout;Z)V

    iget-object v1, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->itemSetPassword:Landroid/widget/LinearLayout;

    invoke-direct {p0, v1, v6}, Lcom/konka/tvsettings/function/LockSettingActivity;->setOptionItemEnabled(Landroid/widget/LinearLayout;Z)V

    iget-object v1, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v1, v2, :cond_3

    iget-object v0, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->itemParentalGuidancelayout:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0, v6}, Lcom/konka/tvsettings/function/LockSettingActivity;->setOptionItemEnabled(Landroid/widget/LinearLayout;Z)V

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->itemParentalGuidancelayout:Landroid/widget/LinearLayout;

    invoke-direct {p0, v1, v0}, Lcom/konka/tvsettings/function/LockSettingActivity;->setOptionItemEnabled(Landroid/widget/LinearLayout;Z)V

    goto :goto_1
.end method

.method private addItemPG()V
    .locals 7

    const v3, 0x7f0700d9    # com.konka.tvsettings.R.id.lock_adv_parentalguidance

    const/4 v5, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getParentalcontrolManager()Lcom/mstar/android/tvapi/common/ParentalcontrolManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/ParentalcontrolManager;->GetParentalControlRating()I

    move-result v6

    const/16 v0, 0x13

    if-ne v6, v0, :cond_2

    const/4 v5, 0x0

    const/4 v0, 0x0

    sput v0, Lcom/konka/tvsettings/function/LockSettingActivity;->CurrentPGRate:I

    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->bCheckisAustralia:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    if-gt v0, v6, :cond_3

    const/16 v0, 0x8

    if-gt v6, v0, :cond_3

    const/4 v5, 0x1

    :cond_1
    :goto_1
    iget-boolean v0, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->bCheckisAustralia:Z

    if-eqz v0, :cond_a

    new-instance v0, Lcom/konka/tvsettings/function/LockSettingActivity$5;

    const v4, 0x7f0b003b    # com.konka.tvsettings.R.array.str_arr_Australian_parentalguidance__vals

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/function/LockSettingActivity$5;-><init>(Lcom/konka/tvsettings/function/LockSettingActivity;Landroid/app/Activity;III)V

    iput-object v0, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->itemParentalGuidance:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    :goto_2
    return-void

    :cond_2
    if-eqz v6, :cond_0

    add-int/lit8 v5, v6, -0x2

    sput v6, Lcom/konka/tvsettings/function/LockSettingActivity;->CurrentPGRate:I

    goto :goto_0

    :cond_3
    const/16 v0, 0x9

    if-gt v0, v6, :cond_4

    const/16 v0, 0xa

    if-gt v6, v0, :cond_4

    const/4 v5, 0x2

    goto :goto_1

    :cond_4
    const/16 v0, 0xb

    if-gt v0, v6, :cond_5

    const/16 v0, 0xc

    if-gt v6, v0, :cond_5

    const/4 v5, 0x3

    goto :goto_1

    :cond_5
    const/16 v0, 0xd

    if-gt v0, v6, :cond_6

    const/16 v0, 0xe

    if-gt v6, v0, :cond_6

    const/4 v5, 0x4

    goto :goto_1

    :cond_6
    const/16 v0, 0xf

    if-gt v0, v6, :cond_7

    const/16 v0, 0x10

    if-gt v6, v0, :cond_7

    const/4 v5, 0x5

    goto :goto_1

    :cond_7
    const/16 v0, 0x11

    if-ne v6, v0, :cond_8

    const/4 v5, 0x6

    goto :goto_1

    :cond_8
    const/16 v0, 0x12

    if-ne v6, v0, :cond_9

    const/4 v5, 0x7

    goto :goto_1

    :cond_9
    const/4 v5, 0x0

    goto :goto_1

    :cond_a
    new-instance v0, Lcom/konka/tvsettings/function/LockSettingActivity$6;

    const v4, 0x7f0b0039    # com.konka.tvsettings.R.array.str_arr_parentalguidance_vals

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/function/LockSettingActivity$6;-><init>(Lcom/konka/tvsettings/function/LockSettingActivity;Landroid/app/Activity;III)V

    iput-object v0, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->itemParentalGuidance:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    goto :goto_2
.end method

.method private addItemSetPassword()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->itemSetPassword:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/tvsettings/function/LockSettingActivity$3;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/function/LockSettingActivity$3;-><init>(Lcom/konka/tvsettings/function/LockSettingActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private addView()V
    .locals 2

    invoke-direct {p0}, Lcom/konka/tvsettings/function/LockSettingActivity;->addItemLockSystem()V

    invoke-direct {p0}, Lcom/konka/tvsettings/function/LockSettingActivity;->addItemSetPassword()V

    invoke-direct {p0}, Lcom/konka/tvsettings/function/LockSettingActivity;->addItemBlockSystem()V

    invoke-direct {p0}, Lcom/konka/tvsettings/function/LockSettingActivity;->addItemPG()V

    iget-object v0, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->curCustomer:Ljava/lang/String;

    sget-object v1, Lcom/konka/tvsettings/function/LockSettingActivity;->PersionCustomer:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/konka/tvsettings/function/LockSettingActivity;->addItemChildLock()V

    :cond_0
    return-void
.end method

.method private setOptionItemEnabled(Landroid/widget/LinearLayout;Z)V
    .locals 0
    .param p1    # Landroid/widget/LinearLayout;
    .param p2    # Z

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setClickable(Z)V

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/konka/tvsettings/BaseKonkaActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v0, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    iget-object v0, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->getCustomerInfo()Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;->strCustomerName:Ljava/lang/String;

    iput-object v0, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->curCustomer:Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getDataBaseManagerInstance()Lcom/konka/kkinterface/tv/DataBaseDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk;->queryCurCountry()I

    move-result v0

    iput v0, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->miCurCountry:I

    iget v0, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->miCurCountry:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->bCheckisAustralia:Z

    :cond_0
    const v0, 0x7f03001d    # com.konka.tvsettings.R.layout.lock_menu_setting

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/function/LockSettingActivity;->setContentView(I)V

    const v0, 0x7f0700d7    # com.konka.tvsettings.R.id.lock_adv_setpassword

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/function/LockSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->itemSetPassword:Landroid/widget/LinearLayout;

    const v0, 0x7f0700d8    # com.konka.tvsettings.R.id.lock_adv_blockprogram

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/function/LockSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->itemBlockSystem:Landroid/widget/LinearLayout;

    const v0, 0x7f0700d9    # com.konka.tvsettings.R.id.lock_adv_parentalguidance

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/function/LockSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->itemParentalGuidancelayout:Landroid/widget/LinearLayout;

    const v0, 0x7f0700d6    # com.konka.tvsettings.R.id.lock_adv_locksystem

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/function/LockSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->itemLockSystemlayout:Landroid/widget/LinearLayout;

    const v0, 0x7f0700da    # com.konka.tvsettings.R.id.lock_adv_childlock

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/function/LockSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->itemChildLocklayout:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->curCustomer:Ljava/lang/String;

    sget-object v1, Lcom/konka/tvsettings/function/LockSettingActivity;->PersionCustomer:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->itemChildLocklayout:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_1
    invoke-direct {p0}, Lcom/konka/tvsettings/function/LockSettingActivity;->addView()V

    iget-object v0, p0, Lcom/konka/tvsettings/function/LockSettingActivity;->myHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/konka/tvsettings/common/LittleDownTimer;->setHandler(Landroid/os/Handler;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    sparse-switch p1, :sswitch_data_0

    :goto_0
    invoke-super {p0, p1, p2}, Lcom/konka/tvsettings/BaseKonkaActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    :goto_1
    return v2

    :sswitch_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/function/LockSettingActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/function/LockSettingActivity;->finish()V

    const/4 v2, 0x0

    const v3, 0x7f040005    # com.konka.tvsettings.R.anim.anim_menu_exit

    invoke-virtual {p0, v2, v3}, Lcom/konka/tvsettings/function/LockSettingActivity;->overridePendingTransition(II)V

    goto :goto_0

    :sswitch_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v2, Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-virtual {v0, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/function/LockSettingActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/function/LockSettingActivity;->finish()V

    goto :goto_0

    :sswitch_2
    const/4 v2, 0x1

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x52 -> :sswitch_1
        0xb2 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->pauseMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resumeMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onResume()V

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onStart()V

    const v0, 0x7f040009    # com.konka.tvsettings.R.anim.anim_zoom_in

    const v1, 0x7f040008    # com.konka.tvsettings.R.anim.anim_right_out

    invoke-virtual {p0, v0, v1}, Lcom/konka/tvsettings/function/LockSettingActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resetMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onUserInteraction()V

    return-void
.end method
