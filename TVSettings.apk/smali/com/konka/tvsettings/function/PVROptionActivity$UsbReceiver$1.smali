.class Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;
.super Ljava/lang/Object;
.source "PVROptionActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;

.field private final synthetic val$handler:Landroid/os/Handler;

.field private final synthetic val$path:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;Ljava/lang/String;Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;->this$1:Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;

    iput-object p2, p0, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;->val$path:Ljava/lang/String;

    iput-object p3, p0, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;->val$handler:Landroid/os/Handler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;)Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;->this$1:Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;->this$1:Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;->access$1(Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;)Lcom/konka/tvsettings/function/PVROptionActivity;

    move-result-object v0

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->storageManager:Lcom/mstar/android/storage/MStorageManager;
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$12(Lcom/konka/tvsettings/function/PVROptionActivity;)Lcom/mstar/android/storage/MStorageManager;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;->val$path:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/mstar/android/storage/MStorageManager;->formatVolume(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "PVROptionActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Success to format "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;->val$path:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;->this$1:Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;->this$0:Lcom/konka/tvsettings/function/PVROptionActivity;
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;->access$1(Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver;)Lcom/konka/tvsettings/function/PVROptionActivity;

    move-result-object v0

    # getter for: Lcom/konka/tvsettings/function/PVROptionActivity;->storageManager:Lcom/mstar/android/storage/MStorageManager;
    invoke-static {v0}, Lcom/konka/tvsettings/function/PVROptionActivity;->access$12(Lcom/konka/tvsettings/function/PVROptionActivity;)Lcom/mstar/android/storage/MStorageManager;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;->val$path:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/mstar/android/storage/MStorageManager;->mountVolume(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PVROptionActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Success to mount "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;->val$path:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " again"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;->val$handler:Landroid/os/Handler;

    new-instance v1, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1$1;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1$1;-><init>(Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void

    :cond_0
    const-string v0, "PVROptionActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fail to mount "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;->val$path:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " again"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;->val$handler:Landroid/os/Handler;

    new-instance v1, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1$2;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1$2;-><init>(Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_1
    const-string v0, "PVROptionActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fail to format "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;->val$path:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;->val$handler:Landroid/os/Handler;

    new-instance v1, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1$3;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1$3;-><init>(Lcom/konka/tvsettings/function/PVROptionActivity$UsbReceiver$1;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
