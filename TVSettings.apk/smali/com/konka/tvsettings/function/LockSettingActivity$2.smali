.class Lcom/konka/tvsettings/function/LockSettingActivity$2;
.super Lcom/konka/tvsettings/picture/PictureSettingItem1;
.source "LockSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/function/LockSettingActivity;->addItemLockSystem()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/function/LockSettingActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/function/LockSettingActivity;Landroid/app/Activity;III)V
    .locals 0
    .param p2    # Landroid/app/Activity;
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iput-object p1, p0, Lcom/konka/tvsettings/function/LockSettingActivity$2;->this$0:Lcom/konka/tvsettings/function/LockSettingActivity;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/konka/tvsettings/picture/PictureSettingItem1;-><init>(Landroid/app/Activity;III)V

    return-void
.end method


# virtual methods
.method public doUpdate()V
    .locals 10

    const/4 v4, 0x0

    const/4 v5, 0x1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/konka/tvsettings/function/LockSettingActivity$2;->this$0:Lcom/konka/tvsettings/function/LockSettingActivity;

    # getter for: Lcom/konka/tvsettings/function/LockSettingActivity;->itemLockSystem:Lcom/konka/tvsettings/picture/PictureSettingItem1;
    invoke-static {v1}, Lcom/konka/tvsettings/function/LockSettingActivity;->access$0(Lcom/konka/tvsettings/function/LockSettingActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/tvsettings/picture/PictureSettingItem1;->getCurrentState()I

    move-result v1

    if-nez v1, :cond_4

    move v8, v4

    :goto_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getParentalcontrolManager()Lcom/mstar/android/tvapi/common/ParentalcontrolManager;

    move-result-object v1

    invoke-virtual {v1, v8}, Lcom/mstar/android/tvapi/common/ParentalcontrolManager;->setSystemLock(Z)V

    if-eqz v8, :cond_5

    sget v1, Lcom/konka/tvsettings/function/LockSettingActivity;->CurrentPGRate:I

    const/16 v2, 0xe

    if-gt v1, v2, :cond_0

    sget v1, Lcom/konka/tvsettings/function/LockSettingActivity;->CurrentPGRate:I

    if-lt v1, v5, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getParentalcontrolManager()Lcom/mstar/android/tvapi/common/ParentalcontrolManager;

    move-result-object v1

    sget v2, Lcom/konka/tvsettings/function/LockSettingActivity;->CurrentPGRate:I

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/ParentalcontrolManager;->setParentalControlRating(I)V

    :cond_0
    iget-object v1, p0, Lcom/konka/tvsettings/function/LockSettingActivity$2;->this$0:Lcom/konka/tvsettings/function/LockSettingActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/function/LockSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->getChannelMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v9

    const-string v1, "pinfo lock"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "===================="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, v9, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->isLock:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v1, v9, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->isLock:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/tvsettings/function/LockSettingActivity$2;->this$0:Lcom/konka/tvsettings/function/LockSettingActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/function/LockSettingActivity;->getApplication()Landroid/app/Application;

    move-result-object v6

    check-cast v6, Lcom/konka/tvsettings/TVRootApp;

    iget-object v1, p0, Lcom/konka/tvsettings/function/LockSettingActivity$2;->this$0:Lcom/konka/tvsettings/function/LockSettingActivity;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/tvsettings/function/LockSettingActivity;->access$1(Lcom/konka/tvsettings/function/LockSettingActivity;Lcom/konka/kkinterface/tv/TvDeskProvider;)V

    iget-object v1, p0, Lcom/konka/tvsettings/function/LockSettingActivity$2;->this$0:Lcom/konka/tvsettings/function/LockSettingActivity;

    # getter for: Lcom/konka/tvsettings/function/LockSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v1}, Lcom/konka/tvsettings/function/LockSettingActivity;->access$2(Lcom/konka/tvsettings/function/LockSettingActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;->E_LOCK:Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;

    iget v2, v9, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    iget-short v3, v9, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    invoke-interface/range {v0 .. v5}, Lcom/konka/kkinterface/tv/ChannelDesk;->setProgramAttribute(Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;ISIZ)V

    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/konka/tvsettings/function/LockSettingActivity$2;->this$0:Lcom/konka/tvsettings/function/LockSettingActivity;

    # getter for: Lcom/konka/tvsettings/function/LockSettingActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/function/LockSettingActivity;->access$3(Lcom/konka/tvsettings/function/LockSettingActivity;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/konka/tvsettings/function/LockSettingActivity$2;->this$0:Lcom/konka/tvsettings/function/LockSettingActivity;

    iget-object v2, p0, Lcom/konka/tvsettings/function/LockSettingActivity$2;->this$0:Lcom/konka/tvsettings/function/LockSettingActivity;

    # getter for: Lcom/konka/tvsettings/function/LockSettingActivity;->itemParentalGuidancelayout:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/konka/tvsettings/function/LockSettingActivity;->access$4(Lcom/konka/tvsettings/function/LockSettingActivity;)Landroid/widget/LinearLayout;

    move-result-object v2

    # invokes: Lcom/konka/tvsettings/function/LockSettingActivity;->setOptionItemEnabled(Landroid/widget/LinearLayout;Z)V
    invoke-static {v1, v2, v8}, Lcom/konka/tvsettings/function/LockSettingActivity;->access$5(Lcom/konka/tvsettings/function/LockSettingActivity;Landroid/widget/LinearLayout;Z)V

    :cond_2
    iget-object v1, p0, Lcom/konka/tvsettings/function/LockSettingActivity$2;->this$0:Lcom/konka/tvsettings/function/LockSettingActivity;

    iget-object v2, p0, Lcom/konka/tvsettings/function/LockSettingActivity$2;->this$0:Lcom/konka/tvsettings/function/LockSettingActivity;

    # getter for: Lcom/konka/tvsettings/function/LockSettingActivity;->itemSetPassword:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/konka/tvsettings/function/LockSettingActivity;->access$6(Lcom/konka/tvsettings/function/LockSettingActivity;)Landroid/widget/LinearLayout;

    move-result-object v2

    # invokes: Lcom/konka/tvsettings/function/LockSettingActivity;->setOptionItemEnabled(Landroid/widget/LinearLayout;Z)V
    invoke-static {v1, v2, v8}, Lcom/konka/tvsettings/function/LockSettingActivity;->access$5(Lcom/konka/tvsettings/function/LockSettingActivity;Landroid/widget/LinearLayout;Z)V

    iget-object v1, p0, Lcom/konka/tvsettings/function/LockSettingActivity$2;->this$0:Lcom/konka/tvsettings/function/LockSettingActivity;

    iget-object v2, p0, Lcom/konka/tvsettings/function/LockSettingActivity$2;->this$0:Lcom/konka/tvsettings/function/LockSettingActivity;

    # getter for: Lcom/konka/tvsettings/function/LockSettingActivity;->itemBlockSystem:Landroid/widget/LinearLayout;
    invoke-static {v2}, Lcom/konka/tvsettings/function/LockSettingActivity;->access$7(Lcom/konka/tvsettings/function/LockSettingActivity;)Landroid/widget/LinearLayout;

    move-result-object v2

    # invokes: Lcom/konka/tvsettings/function/LockSettingActivity;->setOptionItemEnabled(Landroid/widget/LinearLayout;Z)V
    invoke-static {v1, v2, v8}, Lcom/konka/tvsettings/function/LockSettingActivity;->access$5(Lcom/konka/tvsettings/function/LockSettingActivity;Landroid/widget/LinearLayout;Z)V

    :cond_3
    return-void

    :cond_4
    move v8, v5

    goto/16 :goto_0

    :cond_5
    iget-object v1, p0, Lcom/konka/tvsettings/function/LockSettingActivity$2;->this$0:Lcom/konka/tvsettings/function/LockSettingActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/function/LockSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->getChannelMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v9

    iget-boolean v1, v9, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->isLock:Z

    if-eqz v1, :cond_1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v1

    invoke-interface {v1}, Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;->unlockChannel()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v7

    invoke-virtual {v7}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method
