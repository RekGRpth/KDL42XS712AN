.class Lcom/konka/tvsettings/function/FunctionSettingActivity$7;
.super Lcom/konka/tvsettings/picture/PictureSettingItem1;
.source "FunctionSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/function/FunctionSettingActivity;->addItemScreenSaver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

.field private final synthetic val$deskImpl:Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/function/FunctionSettingActivity;Landroid/app/Activity;IIILcom/konka/kkimplements/tv/mstar/SettingDeskImpl;)V
    .locals 0
    .param p2    # Landroid/app/Activity;
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iput-object p1, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$7;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    iput-object p6, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$7;->val$deskImpl:Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/konka/tvsettings/picture/PictureSettingItem1;-><init>(Landroid/app/Activity;III)V

    return-void
.end method


# virtual methods
.method public doUpdate()V
    .locals 6

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_1

    :try_start_0
    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$7;->this$0:Lcom/konka/tvsettings/function/FunctionSettingActivity;

    # getter for: Lcom/konka/tvsettings/function/FunctionSettingActivity;->itemScreenSaver:Lcom/konka/tvsettings/picture/PictureSettingItem1;
    invoke-static {v2}, Lcom/konka/tvsettings/function/FunctionSettingActivity;->access$2(Lcom/konka/tvsettings/function/FunctionSettingActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/tvsettings/picture/PictureSettingItem1;->getCurrentState()I

    move-result v2

    if-nez v2, :cond_2

    const/4 v1, 0x0

    :goto_0
    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->isSignalStable()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    const/16 v4, 0x378

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v2, v1, v3, v4, v5}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z

    :cond_0
    iget-object v2, p0, Lcom/konka/tvsettings/function/FunctionSettingActivity$7;->val$deskImpl:Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;

    invoke-virtual {v2, v1}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->setScreenSaveModeStatus(Z)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method
