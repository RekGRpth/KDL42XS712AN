.class Lcom/konka/tvsettings/function/LockSettingActivity$5;
.super Lcom/konka/tvsettings/picture/PictureSettingItem1;
.source "LockSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/function/LockSettingActivity;->addItemPG()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/function/LockSettingActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/function/LockSettingActivity;Landroid/app/Activity;III)V
    .locals 0
    .param p2    # Landroid/app/Activity;
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iput-object p1, p0, Lcom/konka/tvsettings/function/LockSettingActivity$5;->this$0:Lcom/konka/tvsettings/function/LockSettingActivity;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/konka/tvsettings/picture/PictureSettingItem1;-><init>(Landroid/app/Activity;III)V

    return-void
.end method


# virtual methods
.method public doUpdate()V
    .locals 7

    const/16 v6, 0xd

    const/16 v5, 0xb

    const/16 v4, 0x9

    const/4 v3, 0x0

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/konka/tvsettings/function/LockSettingActivity$5;->getCurrentState()I

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getParentalcontrolManager()Lcom/mstar/android/tvapi/common/ParentalcontrolManager;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/mstar/android/tvapi/common/ParentalcontrolManager;->setParentalControlRating(I)V

    sput v3, Lcom/konka/tvsettings/function/LockSettingActivity;->CurrentPGRate:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getParentalcontrolManager()Lcom/mstar/android/tvapi/common/ParentalcontrolManager;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/ParentalcontrolManager;->setParentalControlRating(I)V

    sput v2, Lcom/konka/tvsettings/function/LockSettingActivity;->CurrentPGRate:I

    goto :goto_0

    :cond_2
    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getParentalcontrolManager()Lcom/mstar/android/tvapi/common/ParentalcontrolManager;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/mstar/android/tvapi/common/ParentalcontrolManager;->setParentalControlRating(I)V

    sput v4, Lcom/konka/tvsettings/function/LockSettingActivity;->CurrentPGRate:I

    goto :goto_0

    :cond_3
    if-ne v0, v2, :cond_4

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getParentalcontrolManager()Lcom/mstar/android/tvapi/common/ParentalcontrolManager;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/mstar/android/tvapi/common/ParentalcontrolManager;->setParentalControlRating(I)V

    sput v5, Lcom/konka/tvsettings/function/LockSettingActivity;->CurrentPGRate:I

    goto :goto_0

    :cond_4
    const/4 v1, 0x4

    if-ne v0, v1, :cond_5

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getParentalcontrolManager()Lcom/mstar/android/tvapi/common/ParentalcontrolManager;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/mstar/android/tvapi/common/ParentalcontrolManager;->setParentalControlRating(I)V

    sput v6, Lcom/konka/tvsettings/function/LockSettingActivity;->CurrentPGRate:I

    goto :goto_0

    :cond_5
    const/4 v1, 0x5

    if-ne v0, v1, :cond_6

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getParentalcontrolManager()Lcom/mstar/android/tvapi/common/ParentalcontrolManager;

    move-result-object v1

    const/16 v2, 0xf

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/ParentalcontrolManager;->setParentalControlRating(I)V

    const/16 v1, 0xf

    sput v1, Lcom/konka/tvsettings/function/LockSettingActivity;->CurrentPGRate:I

    goto :goto_0

    :cond_6
    const/4 v1, 0x6

    if-ne v0, v1, :cond_7

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getParentalcontrolManager()Lcom/mstar/android/tvapi/common/ParentalcontrolManager;

    move-result-object v1

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/ParentalcontrolManager;->setParentalControlRating(I)V

    const/16 v1, 0x11

    sput v1, Lcom/konka/tvsettings/function/LockSettingActivity;->CurrentPGRate:I

    goto :goto_0

    :cond_7
    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getParentalcontrolManager()Lcom/mstar/android/tvapi/common/ParentalcontrolManager;

    move-result-object v1

    const/16 v2, 0x12

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/ParentalcontrolManager;->setParentalControlRating(I)V

    const/16 v1, 0x12

    sput v1, Lcom/konka/tvsettings/function/LockSettingActivity;->CurrentPGRate:I

    goto/16 :goto_0
.end method
