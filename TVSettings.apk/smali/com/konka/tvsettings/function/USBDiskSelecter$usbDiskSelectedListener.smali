.class Lcom/konka/tvsettings/function/USBDiskSelecter$usbDiskSelectedListener;
.super Ljava/lang/Object;
.source "USBDiskSelecter.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/function/USBDiskSelecter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "usbDiskSelectedListener"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemSelectedListener;"
    }
.end annotation


# instance fields
.field private final Backward:I

.field private final Forward:I

.field private itemParent:Landroid/widget/GridView;

.field private pageLength:I

.field private scroller:Landroid/widget/HorizontalScrollView;

.field final synthetic this$0:Lcom/konka/tvsettings/function/USBDiskSelecter;


# direct methods
.method public constructor <init>(Lcom/konka/tvsettings/function/USBDiskSelecter;Landroid/widget/HorizontalScrollView;)V
    .locals 3
    .param p2    # Landroid/widget/HorizontalScrollView;

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$usbDiskSelectedListener;->this$0:Lcom/konka/tvsettings/function/USBDiskSelecter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$usbDiskSelectedListener;->scroller:Landroid/widget/HorizontalScrollView;

    iput-object v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$usbDiskSelectedListener;->itemParent:Landroid/widget/GridView;

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$usbDiskSelectedListener;->pageLength:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$usbDiskSelectedListener;->Forward:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$usbDiskSelectedListener;->Backward:I

    iput-object p2, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$usbDiskSelectedListener;->scroller:Landroid/widget/HorizontalScrollView;

    iget-object v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$usbDiskSelectedListener;->scroller:Landroid/widget/HorizontalScrollView;

    const v1, 0x7f070221    # com.konka.tvsettings.R.id.usb_driver_selecter

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$usbDiskSelectedListener;->itemParent:Landroid/widget/GridView;

    const/high16 v0, 0x43fa0000    # 500.0f

    # invokes: Lcom/konka/tvsettings/function/USBDiskSelecter;->dip2px(F)I
    invoke-static {p1, v0}, Lcom/konka/tvsettings/function/USBDiskSelecter;->access$3(Lcom/konka/tvsettings/function/USBDiskSelecter;F)I

    move-result v0

    iput v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$usbDiskSelectedListener;->pageLength:I

    const-string v0, "PVR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "============>>>>>>>>>>> pageLength = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$usbDiskSelectedListener;->pageLength:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private getScrollType(I)I
    .locals 5
    .param p1    # I

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$usbDiskSelectedListener;->itemParent:Landroid/widget/GridView;

    invoke-virtual {v4, p1}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v0

    iget-object v4, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$usbDiskSelectedListener;->itemParent:Landroid/widget/GridView;

    invoke-virtual {v4, p1}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v1

    iget-object v4, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$usbDiskSelectedListener;->scroller:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v4}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    move-result v2

    if-le v0, v2, :cond_1

    iget v4, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$usbDiskSelectedListener;->pageLength:I

    add-int/2addr v4, v2

    if-ge v1, v4, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    if-ge v0, v2, :cond_2

    const/4 v3, 0x2

    goto :goto_0

    :cond_2
    iget v4, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$usbDiskSelectedListener;->pageLength:I

    add-int/2addr v4, v2

    if-le v1, v4, :cond_0

    const/4 v3, 0x1

    goto :goto_0
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    const/4 v4, 0x0

    invoke-direct {p0, p3}, Lcom/konka/tvsettings/function/USBDiskSelecter$usbDiskSelectedListener;->getScrollType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$usbDiskSelectedListener;->scroller:Landroid/widget/HorizontalScrollView;

    iget-object v1, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$usbDiskSelectedListener;->itemParent:Landroid/widget/GridView;

    invoke-virtual {v1, p3}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    iget-object v2, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$usbDiskSelectedListener;->this$0:Lcom/konka/tvsettings/function/USBDiskSelecter;

    const/high16 v3, 0x41a00000    # 20.0f

    # invokes: Lcom/konka/tvsettings/function/USBDiskSelecter;->dip2px(F)I
    invoke-static {v2, v3}, Lcom/konka/tvsettings/function/USBDiskSelecter;->access$3(Lcom/konka/tvsettings/function/USBDiskSelecter;F)I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1, v4}, Landroid/widget/HorizontalScrollView;->scrollTo(II)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$usbDiskSelectedListener;->scroller:Landroid/widget/HorizontalScrollView;

    iget-object v1, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$usbDiskSelectedListener;->itemParent:Landroid/widget/GridView;

    invoke-virtual {v1, p3}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v1

    iget v2, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$usbDiskSelectedListener;->pageLength:I

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1, v4}, Landroid/widget/HorizontalScrollView;->scrollTo(II)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    return-void
.end method
