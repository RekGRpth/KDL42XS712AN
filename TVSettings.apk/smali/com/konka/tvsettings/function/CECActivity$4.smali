.class Lcom/konka/tvsettings/function/CECActivity$4;
.super Ljava/lang/Object;
.source "CECActivity.java"

# interfaces
.implements Lcom/konka/tvsettings/view/IUpdateSysData;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/function/CECActivity;->addItemCECAutoStandby()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/function/CECActivity;

.field private final synthetic val$deskImpl:Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/function/CECActivity;Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/function/CECActivity$4;->this$0:Lcom/konka/tvsettings/function/CECActivity;

    iput-object p2, p0, Lcom/konka/tvsettings/function/CECActivity$4;->val$deskImpl:Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public doUpdate()V
    .locals 6

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/konka/tvsettings/function/CECActivity$4;->this$0:Lcom/konka/tvsettings/function/CECActivity;

    # getter for: Lcom/konka/tvsettings/function/CECActivity;->itemCECAutoStandby:Lcom/konka/tvsettings/view/ComboSettingItem;
    invoke-static {v3}, Lcom/konka/tvsettings/function/CECActivity;->access$9(Lcom/konka/tvsettings/function/CECActivity;)Lcom/konka/tvsettings/view/ComboSettingItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/tvsettings/view/ComboSettingItem;->getCurrentState()I

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/konka/tvsettings/function/CECActivity$4;->val$deskImpl:Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;

    invoke-virtual {v3, v0}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->setCECAutoStandbyModeStatus(Z)Z

    const-string v3, "liying"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "autostandby "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/konka/tvsettings/function/CECActivity$4;->this$0:Lcom/konka/tvsettings/function/CECActivity;

    iget-object v3, v3, Lcom/konka/tvsettings/function/CECActivity;->CEC_Setting:Lcom/mstar/android/tvapi/common/vo/CecSetting;

    if-eqz v0, :cond_1

    :goto_1
    int-to-short v1, v2

    iput-short v1, v3, Lcom/mstar/android/tvapi/common/vo/CecSetting;->autoStandby:S

    iget-object v1, p0, Lcom/konka/tvsettings/function/CECActivity$4;->this$0:Lcom/konka/tvsettings/function/CECActivity;

    # getter for: Lcom/konka/tvsettings/function/CECActivity;->tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v1}, Lcom/konka/tvsettings/function/CECActivity;->access$7(Lcom/konka/tvsettings/function/CECActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/tvsettings/function/CECActivity$4;->this$0:Lcom/konka/tvsettings/function/CECActivity;

    iget-object v2, v2, Lcom/konka/tvsettings/function/CECActivity;->CEC_Setting:Lcom/mstar/android/tvapi/common/vo/CecSetting;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/SettingDesk;->SetCecStatus(Lcom/mstar/android/tvapi/common/vo/CecSetting;)Z

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1
.end method
