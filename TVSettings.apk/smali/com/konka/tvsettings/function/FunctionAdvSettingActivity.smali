.class public Lcom/konka/tvsettings/function/FunctionAdvSettingActivity;
.super Landroid/app/Activity;
.source "FunctionAdvSettingActivity.java"


# instance fields
.field private PriAudioLang:Lcom/konka/tvsettings/picture/PictureSettingItem1;

.field private PriSubtitleLang:Lcom/konka/tvsettings/picture/PictureSettingItem1;

.field private SecAudioLang:Lcom/konka/tvsettings/picture/PictureSettingItem1;

.field private SecSubtitleLang:Lcom/konka/tvsettings/picture/PictureSettingItem1;

.field private myHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/konka/tvsettings/function/FunctionAdvSettingActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/function/FunctionAdvSettingActivity$1;-><init>(Lcom/konka/tvsettings/function/FunctionAdvSettingActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/function/FunctionAdvSettingActivity;->myHandler:Landroid/os/Handler;

    iput-object v1, p0, Lcom/konka/tvsettings/function/FunctionAdvSettingActivity;->PriAudioLang:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    iput-object v1, p0, Lcom/konka/tvsettings/function/FunctionAdvSettingActivity;->SecAudioLang:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    iput-object v1, p0, Lcom/konka/tvsettings/function/FunctionAdvSettingActivity;->PriSubtitleLang:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    iput-object v1, p0, Lcom/konka/tvsettings/function/FunctionAdvSettingActivity;->SecSubtitleLang:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-void
.end method

.method private addItemPriAudioLang()V
    .locals 6

    const/4 v5, 0x0

    new-instance v0, Lcom/konka/tvsettings/function/FunctionAdvSettingActivity$2;

    const v3, 0x7f0700b1    # com.konka.tvsettings.R.id.function_adv_pri_audio_lang

    const v4, 0x7f0b0037    # com.konka.tvsettings.R.array.str_arr_fun_audio_lang_vals

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/function/FunctionAdvSettingActivity$2;-><init>(Lcom/konka/tvsettings/function/FunctionAdvSettingActivity;Landroid/app/Activity;III)V

    iput-object v0, p0, Lcom/konka/tvsettings/function/FunctionAdvSettingActivity;->PriAudioLang:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-void
.end method

.method private addItemPriSubtitleLang()V
    .locals 6

    const/4 v5, 0x0

    new-instance v0, Lcom/konka/tvsettings/function/FunctionAdvSettingActivity$4;

    const v3, 0x7f0700b3    # com.konka.tvsettings.R.id.function_adv_pri_subtitle_lang

    const v4, 0x7f0b0037    # com.konka.tvsettings.R.array.str_arr_fun_audio_lang_vals

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/function/FunctionAdvSettingActivity$4;-><init>(Lcom/konka/tvsettings/function/FunctionAdvSettingActivity;Landroid/app/Activity;III)V

    iput-object v0, p0, Lcom/konka/tvsettings/function/FunctionAdvSettingActivity;->PriSubtitleLang:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-void
.end method

.method private addItemSecAudioLang()V
    .locals 6

    const/4 v5, 0x0

    new-instance v0, Lcom/konka/tvsettings/function/FunctionAdvSettingActivity$3;

    const v3, 0x7f0700b2    # com.konka.tvsettings.R.id.function_adv_secondary_audio_lang

    const v4, 0x7f0b0037    # com.konka.tvsettings.R.array.str_arr_fun_audio_lang_vals

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/function/FunctionAdvSettingActivity$3;-><init>(Lcom/konka/tvsettings/function/FunctionAdvSettingActivity;Landroid/app/Activity;III)V

    iput-object v0, p0, Lcom/konka/tvsettings/function/FunctionAdvSettingActivity;->SecAudioLang:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-void
.end method

.method private addItemSecSubtitleLang()V
    .locals 6

    const/4 v5, 0x0

    new-instance v0, Lcom/konka/tvsettings/function/FunctionAdvSettingActivity$5;

    const v3, 0x7f0700b4    # com.konka.tvsettings.R.id.function_adv_sec_subtitle_lang

    const v4, 0x7f0b0037    # com.konka.tvsettings.R.array.str_arr_fun_audio_lang_vals

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/function/FunctionAdvSettingActivity$5;-><init>(Lcom/konka/tvsettings/function/FunctionAdvSettingActivity;Landroid/app/Activity;III)V

    iput-object v0, p0, Lcom/konka/tvsettings/function/FunctionAdvSettingActivity;->SecSubtitleLang:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-void
.end method

.method private addView()V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/function/FunctionAdvSettingActivity;->addItemPriAudioLang()V

    invoke-direct {p0}, Lcom/konka/tvsettings/function/FunctionAdvSettingActivity;->addItemSecAudioLang()V

    invoke-direct {p0}, Lcom/konka/tvsettings/function/FunctionAdvSettingActivity;->addItemPriSubtitleLang()V

    invoke-direct {p0}, Lcom/konka/tvsettings/function/FunctionAdvSettingActivity;->addItemSecSubtitleLang()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030017    # com.konka.tvsettings.R.layout.function_adv_setting

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/function/FunctionAdvSettingActivity;->setContentView(I)V

    invoke-direct {p0}, Lcom/konka/tvsettings/function/FunctionAdvSettingActivity;->addView()V

    iget-object v0, p0, Lcom/konka/tvsettings/function/FunctionAdvSettingActivity;->myHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/konka/tvsettings/common/LittleDownTimer;->setHandler(Landroid/os/Handler;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    sparse-switch p1, :sswitch_data_0

    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    :goto_1
    return v2

    :sswitch_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/function/FunctionAdvSettingActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/function/FunctionAdvSettingActivity;->finish()V

    const/4 v2, 0x0

    const v3, 0x7f040005    # com.konka.tvsettings.R.anim.anim_menu_exit

    invoke-virtual {p0, v2, v3}, Lcom/konka/tvsettings/function/FunctionAdvSettingActivity;->overridePendingTransition(II)V

    goto :goto_0

    :sswitch_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v2, Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-virtual {v0, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/function/FunctionAdvSettingActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/function/FunctionAdvSettingActivity;->finish()V

    goto :goto_0

    :sswitch_2
    const/4 v2, 0x1

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x52 -> :sswitch_1
        0xb2 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->pauseMenu()V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resumeMenu()V

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    const v0, 0x7f040009    # com.konka.tvsettings.R.anim.anim_zoom_in

    const v1, 0x7f040008    # com.konka.tvsettings.R.anim.anim_right_out

    invoke-virtual {p0, v0, v1}, Lcom/konka/tvsettings/function/FunctionAdvSettingActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resetMenu()V

    invoke-super {p0}, Landroid/app/Activity;->onUserInteraction()V

    return-void
.end method
