.class public Lcom/konka/tvsettings/function/CECActivity;
.super Landroid/app/Activity;
.source "CECActivity.java"


# instance fields
.field CEC_Setting:Lcom/mstar/android/tvapi/common/vo/CecSetting;

.field private arcindex:S

.field private cecstatus:S

.field private itemARC:Lcom/konka/tvsettings/picture/PictureSettingItem1;

.field private itemARClayout:Landroid/widget/LinearLayout;

.field private itemCECAutoPoweroff:Lcom/konka/tvsettings/view/ComboSettingItem;

.field private itemCECAutoStandby:Lcom/konka/tvsettings/view/ComboSettingItem;

.field private itemCECDeviceList:Landroid/widget/LinearLayout;

.field private itemCECFunction:Lcom/konka/tvsettings/view/ComboSettingItem;

.field private myHandler:Landroid/os/Handler;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HandlerLeak"
        }
    .end annotation
.end field

.field private poweroffindex:S

.field private settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;

.field private standbyindex:S

.field private tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v1, p0, Lcom/konka/tvsettings/function/CECActivity;->itemARClayout:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/function/CECActivity;->itemARC:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    iput-object v1, p0, Lcom/konka/tvsettings/function/CECActivity;->CEC_Setting:Lcom/mstar/android/tvapi/common/vo/CecSetting;

    iput-short v0, p0, Lcom/konka/tvsettings/function/CECActivity;->cecstatus:S

    iput-short v0, p0, Lcom/konka/tvsettings/function/CECActivity;->standbyindex:S

    iput-short v0, p0, Lcom/konka/tvsettings/function/CECActivity;->poweroffindex:S

    iput-short v0, p0, Lcom/konka/tvsettings/function/CECActivity;->arcindex:S

    new-instance v0, Lcom/konka/tvsettings/function/CECActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/function/CECActivity$1;-><init>(Lcom/konka/tvsettings/function/CECActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/function/CECActivity;->myHandler:Landroid/os/Handler;

    iput-object v1, p0, Lcom/konka/tvsettings/function/CECActivity;->itemCECFunction:Lcom/konka/tvsettings/view/ComboSettingItem;

    iput-object v1, p0, Lcom/konka/tvsettings/function/CECActivity;->itemCECAutoStandby:Lcom/konka/tvsettings/view/ComboSettingItem;

    iput-object v1, p0, Lcom/konka/tvsettings/function/CECActivity;->itemCECAutoPoweroff:Lcom/konka/tvsettings/view/ComboSettingItem;

    iput-object v1, p0, Lcom/konka/tvsettings/function/CECActivity;->itemCECDeviceList:Landroid/widget/LinearLayout;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/function/CECActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/CECActivity;->itemARC:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/function/CECActivity;S)V
    .locals 0

    iput-short p1, p0, Lcom/konka/tvsettings/function/CECActivity;->arcindex:S

    return-void
.end method

.method static synthetic access$10(Lcom/konka/tvsettings/function/CECActivity;)Lcom/konka/tvsettings/view/ComboSettingItem;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/CECActivity;->itemCECAutoPoweroff:Lcom/konka/tvsettings/view/ComboSettingItem;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/function/CECActivity;)S
    .locals 1

    iget-short v0, p0, Lcom/konka/tvsettings/function/CECActivity;->arcindex:S

    return v0
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/function/CECActivity;)Lcom/konka/kkinterface/tv/SettingDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/CECActivity;->settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/function/CECActivity;)Lcom/konka/tvsettings/view/ComboSettingItem;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/CECActivity;->itemCECFunction:Lcom/konka/tvsettings/view/ComboSettingItem;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/function/CECActivity;S)V
    .locals 0

    iput-short p1, p0, Lcom/konka/tvsettings/function/CECActivity;->cecstatus:S

    return-void
.end method

.method static synthetic access$6(Lcom/konka/tvsettings/function/CECActivity;)S
    .locals 1

    iget-short v0, p0, Lcom/konka/tvsettings/function/CECActivity;->cecstatus:S

    return v0
.end method

.method static synthetic access$7(Lcom/konka/tvsettings/function/CECActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/CECActivity;->tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    return-object v0
.end method

.method static synthetic access$8(Lcom/konka/tvsettings/function/CECActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/function/CECActivity;->setCECItemStatus()V

    return-void
.end method

.method static synthetic access$9(Lcom/konka/tvsettings/function/CECActivity;)Lcom/konka/tvsettings/view/ComboSettingItem;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/CECActivity;->itemCECAutoStandby:Lcom/konka/tvsettings/view/ComboSettingItem;

    return-object v0
.end method

.method private addItemARC()V
    .locals 7

    invoke-virtual {p0}, Lcom/konka/tvsettings/function/CECActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->getSettingMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;

    move-result-object v6

    iget-short v5, p0, Lcom/konka/tvsettings/function/CECActivity;->arcindex:S

    new-instance v0, Lcom/konka/tvsettings/function/CECActivity$2;

    const v3, 0x7f070041    # com.konka.tvsettings.R.id.linearlayout_function_ARC

    const v4, 0x7f0b0033    # com.konka.tvsettings.R.array.str_arr_fun_OnorOff_switcher

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/function/CECActivity$2;-><init>(Lcom/konka/tvsettings/function/CECActivity;Landroid/app/Activity;III)V

    iput-object v0, p0, Lcom/konka/tvsettings/function/CECActivity;->itemARC:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-void
.end method

.method private addItemCECAutoPoweroff()V
    .locals 6

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/konka/tvsettings/function/CECActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->getSettingMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->getCECAutoPoweroffModeStatus()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    const v2, 0x7f070040    # com.konka.tvsettings.R.id.linearlayout_function_cec_autopoweroff

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/function/CECActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/konka/tvsettings/view/ComboSettingItem;

    iput-object v2, p0, Lcom/konka/tvsettings/function/CECActivity;->itemCECAutoPoweroff:Lcom/konka/tvsettings/view/ComboSettingItem;

    iget-object v2, p0, Lcom/konka/tvsettings/function/CECActivity;->itemCECAutoPoweroff:Lcom/konka/tvsettings/view/ComboSettingItem;

    const v4, 0x7f0b002b    # com.konka.tvsettings.R.array.str_arr_fun_screensaver_vals

    new-instance v5, Lcom/konka/tvsettings/function/CECActivity$5;

    invoke-direct {v5, p0, v0}, Lcom/konka/tvsettings/function/CECActivity$5;-><init>(Lcom/konka/tvsettings/function/CECActivity;Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;)V

    invoke-virtual {v2, v4, v1, v5, v3}, Lcom/konka/tvsettings/view/ComboSettingItem;->initData(IILcom/konka/tvsettings/view/IUpdateSysData;Z)V

    return-void

    :cond_0
    move v1, v3

    goto :goto_0
.end method

.method private addItemCECAutoStandby()V
    .locals 6

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/konka/tvsettings/function/CECActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->getSettingMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->getCECAutoStandbyModeStatus()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    const v2, 0x7f07003f    # com.konka.tvsettings.R.id.linearlayout_function_cec_autostandby

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/function/CECActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/konka/tvsettings/view/ComboSettingItem;

    iput-object v2, p0, Lcom/konka/tvsettings/function/CECActivity;->itemCECAutoStandby:Lcom/konka/tvsettings/view/ComboSettingItem;

    iget-object v2, p0, Lcom/konka/tvsettings/function/CECActivity;->itemCECAutoStandby:Lcom/konka/tvsettings/view/ComboSettingItem;

    const v4, 0x7f0b002b    # com.konka.tvsettings.R.array.str_arr_fun_screensaver_vals

    new-instance v5, Lcom/konka/tvsettings/function/CECActivity$4;

    invoke-direct {v5, p0, v0}, Lcom/konka/tvsettings/function/CECActivity$4;-><init>(Lcom/konka/tvsettings/function/CECActivity;Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;)V

    invoke-virtual {v2, v4, v1, v5, v3}, Lcom/konka/tvsettings/view/ComboSettingItem;->initData(IILcom/konka/tvsettings/view/IUpdateSysData;Z)V

    return-void

    :cond_0
    move v1, v3

    goto :goto_0
.end method

.method private addItemCECDeviceList()V
    .locals 2

    const v0, 0x7f070042    # com.konka.tvsettings.R.id.linearlayout_cec_devicelist

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/function/CECActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/function/CECActivity;->itemCECDeviceList:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/function/CECActivity;->itemCECDeviceList:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/tvsettings/function/CECActivity$6;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/function/CECActivity$6;-><init>(Lcom/konka/tvsettings/function/CECActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private addItemCECFunction()V
    .locals 6

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/konka/tvsettings/function/CECActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->getSettingMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->getCECFunctionModeStatus()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v2, "liying"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "get cec function mode :"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const v2, 0x7f07003e    # com.konka.tvsettings.R.id.linearlayout_function_cec_function

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/function/CECActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/konka/tvsettings/view/ComboSettingItem;

    iput-object v2, p0, Lcom/konka/tvsettings/function/CECActivity;->itemCECFunction:Lcom/konka/tvsettings/view/ComboSettingItem;

    iget-object v2, p0, Lcom/konka/tvsettings/function/CECActivity;->itemCECFunction:Lcom/konka/tvsettings/view/ComboSettingItem;

    const v4, 0x7f0b002b    # com.konka.tvsettings.R.array.str_arr_fun_screensaver_vals

    new-instance v5, Lcom/konka/tvsettings/function/CECActivity$3;

    invoke-direct {v5, p0, v0}, Lcom/konka/tvsettings/function/CECActivity$3;-><init>(Lcom/konka/tvsettings/function/CECActivity;Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;)V

    invoke-virtual {v2, v4, v1, v5, v3}, Lcom/konka/tvsettings/view/ComboSettingItem;->initData(IILcom/konka/tvsettings/view/IUpdateSysData;Z)V

    return-void

    :cond_0
    move v1, v3

    goto :goto_0
.end method

.method private addView()V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/function/CECActivity;->addItemCECFunction()V

    invoke-direct {p0}, Lcom/konka/tvsettings/function/CECActivity;->addItemCECAutoStandby()V

    invoke-direct {p0}, Lcom/konka/tvsettings/function/CECActivity;->addItemCECAutoPoweroff()V

    invoke-direct {p0}, Lcom/konka/tvsettings/function/CECActivity;->addItemARC()V

    invoke-direct {p0}, Lcom/konka/tvsettings/function/CECActivity;->addItemCECDeviceList()V

    return-void
.end method

.method private setCECItemEnabled(Landroid/view/View;Z)V
    .locals 0
    .param p1    # Landroid/view/View;
    .param p2    # Z

    invoke-virtual {p1, p2}, Landroid/view/View;->setEnabled(Z)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setClickable(Z)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setFocusable(Z)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    return-void
.end method

.method private setCECItemStatus()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/konka/tvsettings/function/CECActivity;->itemCECFunction:Lcom/konka/tvsettings/view/ComboSettingItem;

    invoke-virtual {v0}, Lcom/konka/tvsettings/view/ComboSettingItem;->getCurrentState()I

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/function/CECActivity;->itemCECAutoStandby:Lcom/konka/tvsettings/view/ComboSettingItem;

    invoke-direct {p0, v0, v1}, Lcom/konka/tvsettings/function/CECActivity;->setCECItemEnabled(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/function/CECActivity;->itemCECAutoPoweroff:Lcom/konka/tvsettings/view/ComboSettingItem;

    invoke-direct {p0, v0, v1}, Lcom/konka/tvsettings/function/CECActivity;->setCECItemEnabled(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/function/CECActivity;->itemCECDeviceList:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0, v1}, Lcom/konka/tvsettings/function/CECActivity;->setCECItemEnabled(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/function/CECActivity;->itemARClayout:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0, v1}, Lcom/konka/tvsettings/function/CECActivity;->setCECItemEnabled(Landroid/view/View;Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/function/CECActivity;->itemCECAutoStandby:Lcom/konka/tvsettings/view/ComboSettingItem;

    invoke-direct {p0, v0, v2}, Lcom/konka/tvsettings/function/CECActivity;->setCECItemEnabled(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/function/CECActivity;->itemCECAutoPoweroff:Lcom/konka/tvsettings/view/ComboSettingItem;

    invoke-direct {p0, v0, v2}, Lcom/konka/tvsettings/function/CECActivity;->setCECItemEnabled(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/function/CECActivity;->itemCECDeviceList:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0, v2}, Lcom/konka/tvsettings/function/CECActivity;->setCECItemEnabled(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/function/CECActivity;->itemARClayout:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0, v2}, Lcom/konka/tvsettings/function/CECActivity;->setCECItemEnabled(Landroid/view/View;Z)V

    goto :goto_0
.end method

.method private setViewStatus()V
    .locals 2

    invoke-direct {p0}, Lcom/konka/tvsettings/function/CECActivity;->setCECItemStatus()V

    iget-object v0, p0, Lcom/konka/tvsettings/function/CECActivity;->itemCECFunction:Lcom/konka/tvsettings/view/ComboSettingItem;

    iget-object v1, p0, Lcom/konka/tvsettings/function/CECActivity;->itemCECDeviceList:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/view/ComboSettingItem;->setNextFocusUpId(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/function/CECActivity;->itemCECDeviceList:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/tvsettings/function/CECActivity;->itemCECFunction:Lcom/konka/tvsettings/view/ComboSettingItem;

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/ComboSettingItem;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusDownId(I)V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f03000a    # com.konka.tvsettings.R.layout.cec_menu

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/function/CECActivity;->setContentView(I)V

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/function/CECActivity;->tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/function/CECActivity;->settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;

    iget-object v0, p0, Lcom/konka/tvsettings/function/CECActivity;->tvManagerProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/SettingDesk;->GetCecStatus()Lcom/mstar/android/tvapi/common/vo/CecSetting;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/function/CECActivity;->CEC_Setting:Lcom/mstar/android/tvapi/common/vo/CecSetting;

    iget-object v0, p0, Lcom/konka/tvsettings/function/CECActivity;->CEC_Setting:Lcom/mstar/android/tvapi/common/vo/CecSetting;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/function/CECActivity;->CEC_Setting:Lcom/mstar/android/tvapi/common/vo/CecSetting;

    iget-short v0, v0, Lcom/mstar/android/tvapi/common/vo/CecSetting;->cecStatus:S

    iput-short v0, p0, Lcom/konka/tvsettings/function/CECActivity;->cecstatus:S

    const-string v0, "liying"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cecStatus:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-short v2, p0, Lcom/konka/tvsettings/function/CECActivity;->cecstatus:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/tvsettings/function/CECActivity;->CEC_Setting:Lcom/mstar/android/tvapi/common/vo/CecSetting;

    iget-short v0, v0, Lcom/mstar/android/tvapi/common/vo/CecSetting;->autoStandby:S

    iput-short v0, p0, Lcom/konka/tvsettings/function/CECActivity;->standbyindex:S

    iget-object v0, p0, Lcom/konka/tvsettings/function/CECActivity;->CEC_Setting:Lcom/mstar/android/tvapi/common/vo/CecSetting;

    iget-short v0, v0, Lcom/mstar/android/tvapi/common/vo/CecSetting;->autoPoweron:S

    iput-short v0, p0, Lcom/konka/tvsettings/function/CECActivity;->poweroffindex:S

    iget-object v0, p0, Lcom/konka/tvsettings/function/CECActivity;->CEC_Setting:Lcom/mstar/android/tvapi/common/vo/CecSetting;

    iget-short v0, v0, Lcom/mstar/android/tvapi/common/vo/CecSetting;->arcStatus:S

    iput-short v0, p0, Lcom/konka/tvsettings/function/CECActivity;->arcindex:S

    :cond_0
    const v0, 0x7f070041    # com.konka.tvsettings.R.id.linearlayout_function_ARC

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/function/CECActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/function/CECActivity;->itemARClayout:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Lcom/konka/tvsettings/function/CECActivity;->addView()V

    invoke-direct {p0}, Lcom/konka/tvsettings/function/CECActivity;->setViewStatus()V

    iget-object v0, p0, Lcom/konka/tvsettings/function/CECActivity;->myHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/konka/tvsettings/common/LittleDownTimer;->setHandler(Landroid/os/Handler;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    sparse-switch p1, :sswitch_data_0

    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    :goto_1
    return v2

    :sswitch_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/function/CECActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/function/CECActivity;->finish()V

    const/4 v2, 0x0

    const v3, 0x7f040005    # com.konka.tvsettings.R.anim.anim_menu_exit

    invoke-virtual {p0, v2, v3}, Lcom/konka/tvsettings/function/CECActivity;->overridePendingTransition(II)V

    goto :goto_0

    :sswitch_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v2, Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-virtual {v0, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/function/CECActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/function/CECActivity;->finish()V

    goto :goto_0

    :sswitch_2
    const/4 v2, 0x1

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x52 -> :sswitch_1
        0xb2 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->pauseMenu()V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resumeMenu()V

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    const v0, 0x7f040009    # com.konka.tvsettings.R.anim.anim_zoom_in

    const v1, 0x7f040008    # com.konka.tvsettings.R.anim.anim_right_out

    invoke-virtual {p0, v0, v1}, Lcom/konka/tvsettings/function/CECActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resetMenu()V

    invoke-super {p0}, Landroid/app/Activity;->onUserInteraction()V

    return-void
.end method
