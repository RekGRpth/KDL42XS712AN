.class public Lcom/konka/tvsettings/function/CecControlActivity;
.super Landroid/app/Activity;
.source "CecControlActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/function/CecControlActivity$AdListAdapter;
    }
.end annotation


# instance fields
.field private CecControlListView:Landroid/widget/ListView;

.field private adapter:Lcom/konka/tvsettings/function/CecControlActivity$AdListAdapter;

.field cecmanager:Lcom/mstar/android/tvapi/common/CecManager;

.field private chooses:[Ljava/lang/String;

.field private myHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/tvsettings/function/CecControlActivity;->CecControlListView:Landroid/widget/ListView;

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/konka/tvsettings/function/CecControlActivity;->chooses:[Ljava/lang/String;

    new-instance v0, Lcom/konka/tvsettings/function/CecControlActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/function/CecControlActivity$1;-><init>(Lcom/konka/tvsettings/function/CecControlActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/function/CecControlActivity;->myHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/function/CecControlActivity;)[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/CecControlActivity;->chooses:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/function/CecControlActivity;)Lcom/konka/tvsettings/function/CecControlActivity$AdListAdapter;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/CecControlActivity;->adapter:Lcom/konka/tvsettings/function/CecControlActivity$AdListAdapter;

    return-object v0
.end method


# virtual methods
.method public initChooses()V
    .locals 3

    iget-object v0, p0, Lcom/konka/tvsettings/function/CecControlActivity;->chooses:[Ljava/lang/String;

    const/4 v1, 0x0

    const v2, 0x7f0a015e    # com.konka.tvsettings.R.string.str_cec_control_power

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/function/CecControlActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/konka/tvsettings/function/CecControlActivity;->chooses:[Ljava/lang/String;

    const/4 v1, 0x1

    const v2, 0x7f0a015f    # com.konka.tvsettings.R.string.str_cec_control_root_menu

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/function/CecControlActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/konka/tvsettings/function/CecControlActivity;->chooses:[Ljava/lang/String;

    const/4 v1, 0x2

    const v2, 0x7f0a0160    # com.konka.tvsettings.R.string.str_cec_control_setup_menu

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/function/CecControlActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/konka/tvsettings/function/CecControlActivity;->chooses:[Ljava/lang/String;

    const/4 v1, 0x3

    const v2, 0x7f0a0161    # com.konka.tvsettings.R.string.str_cec_control_menu

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/function/CecControlActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    return-void
.end method

.method public onBackPressed()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/function/CecControlActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/function/CecControlActivity;->finish()V

    const/4 v1, 0x0

    const v2, 0x7f040005    # com.konka.tvsettings.R.anim.anim_menu_exit

    invoke-virtual {p0, v1, v2}, Lcom/konka/tvsettings/function/CecControlActivity;->overridePendingTransition(II)V

    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030007    # com.konka.tvsettings.R.layout.cec_control_menu

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/function/CecControlActivity;->setContentView(I)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getCecManager()Lcom/mstar/android/tvapi/common/CecManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/function/CecControlActivity;->cecmanager:Lcom/mstar/android/tvapi/common/CecManager;

    invoke-virtual {p0}, Lcom/konka/tvsettings/function/CecControlActivity;->initChooses()V

    const v0, 0x7f070037    # com.konka.tvsettings.R.id.cec_control_list_view

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/function/CecControlActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/konka/tvsettings/function/CecControlActivity;->CecControlListView:Landroid/widget/ListView;

    new-instance v0, Lcom/konka/tvsettings/function/CecControlActivity$AdListAdapter;

    invoke-direct {v0, p0, p0}, Lcom/konka/tvsettings/function/CecControlActivity$AdListAdapter;-><init>(Lcom/konka/tvsettings/function/CecControlActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/konka/tvsettings/function/CecControlActivity;->adapter:Lcom/konka/tvsettings/function/CecControlActivity$AdListAdapter;

    iget-object v0, p0, Lcom/konka/tvsettings/function/CecControlActivity;->CecControlListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/konka/tvsettings/function/CecControlActivity;->adapter:Lcom/konka/tvsettings/function/CecControlActivity$AdListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/konka/tvsettings/function/CecControlActivity;->CecControlListView:Landroid/widget/ListView;

    new-instance v1, Lcom/konka/tvsettings/function/CecControlActivity$2;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/function/CecControlActivity$2;-><init>(Lcom/konka/tvsettings/function/CecControlActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/function/CecControlActivity;->myHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/konka/tvsettings/common/LittleDownTimer;->setHandler(Landroid/os/Handler;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    sparse-switch p1, :sswitch_data_0

    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    :goto_1
    return v1

    :sswitch_0
    invoke-virtual {p0}, Lcom/konka/tvsettings/function/CecControlActivity;->onBackPressed()V

    goto :goto_0

    :sswitch_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v1, Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/function/CecControlActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/function/CecControlActivity;->finish()V

    goto :goto_0

    :sswitch_2
    const/4 v1, 0x1

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x52 -> :sswitch_1
        0xb2 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->pauseMenu()V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resumeMenu()V

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    const v0, 0x7f040009    # com.konka.tvsettings.R.anim.anim_zoom_in

    const v1, 0x7f040008    # com.konka.tvsettings.R.anim.anim_right_out

    invoke-virtual {p0, v0, v1}, Lcom/konka/tvsettings/function/CecControlActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resetMenu()V

    invoke-super {p0}, Landroid/app/Activity;->onUserInteraction()V

    return-void
.end method
