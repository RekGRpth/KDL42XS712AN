.class Lcom/konka/tvsettings/function/USBDiskSelecter$listAdapter;
.super Landroid/widget/BaseAdapter;
.source "USBDiskSelecter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/function/USBDiskSelecter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "listAdapter"
.end annotation


# instance fields
.field private itemCount:I

.field final synthetic this$0:Lcom/konka/tvsettings/function/USBDiskSelecter;


# direct methods
.method public constructor <init>(Lcom/konka/tvsettings/function/USBDiskSelecter;I)V
    .locals 1
    .param p2    # I

    iput-object p1, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$listAdapter;->this$0:Lcom/konka/tvsettings/function/USBDiskSelecter;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$listAdapter;->itemCount:I

    iput p2, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$listAdapter;->itemCount:I

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/function/USBDiskSelecter$listAdapter;)Lcom/konka/tvsettings/function/USBDiskSelecter;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$listAdapter;->this$0:Lcom/konka/tvsettings/function/USBDiskSelecter;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$listAdapter;->itemCount:I

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const v8, 0x7f07021e    # com.konka.tvsettings.R.id.usbItemSpace

    const/4 v7, 0x0

    if-nez p2, :cond_0

    iget-object v5, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$listAdapter;->this$0:Lcom/konka/tvsettings/function/USBDiskSelecter;

    # getter for: Lcom/konka/tvsettings/function/USBDiskSelecter;->context:Landroid/content/Context;
    invoke-static {v5}, Lcom/konka/tvsettings/function/USBDiskSelecter;->access$0(Lcom/konka/tvsettings/function/USBDiskSelecter;)Landroid/content/Context;

    move-result-object v5

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    const v5, 0x7f030058    # com.konka.tvsettings.R.layout.usb_driver_item

    invoke-virtual {v3, v5, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_0
    const v5, 0x7f07021c    # com.konka.tvsettings.R.id.usbItemName

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v5, p0, Lcom/konka/tvsettings/function/USBDiskSelecter$listAdapter;->this$0:Lcom/konka/tvsettings/function/USBDiskSelecter;

    # getter for: Lcom/konka/tvsettings/function/USBDiskSelecter;->usbDriverLabelPcStyle:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/konka/tvsettings/function/USBDiskSelecter;->access$1(Lcom/konka/tvsettings/function/USBDiskSelecter;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ProgressBar;

    const/16 v5, 0x64

    invoke-virtual {v0, v5}, Landroid/widget/ProgressBar;->setMax(I)V

    invoke-virtual {v4, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    new-instance v1, Lcom/konka/tvsettings/function/USBDiskSelecter$UpdateHandler;

    invoke-direct {v1, v0, v4}, Lcom/konka/tvsettings/function/USBDiskSelecter$UpdateHandler;-><init>(Landroid/widget/ProgressBar;Landroid/widget/ProgressBar;)V

    new-instance v5, Lcom/konka/tvsettings/function/USBDiskSelecter$listAdapter$1;

    invoke-direct {v5, p0, p1, v1}, Lcom/konka/tvsettings/function/USBDiskSelecter$listAdapter$1;-><init>(Lcom/konka/tvsettings/function/USBDiskSelecter$listAdapter;ILcom/konka/tvsettings/function/USBDiskSelecter$UpdateHandler;)V

    invoke-virtual {v5}, Lcom/konka/tvsettings/function/USBDiskSelecter$listAdapter$1;->start()V

    return-object p2
.end method
