.class public Lcom/konka/tvsettings/MovingPanelParameter;
.super Ljava/lang/Object;
.source "MovingPanelParameter.java"


# instance fields
.field public iCenterOffset:I

.field public iItemCounts:I

.field public iItemWidth:I

.field public iMarginSpace:I

.field public iMoveSpeed:I


# direct methods
.method public constructor <init>(IIIII)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/konka/tvsettings/MovingPanelParameter;->iItemWidth:I

    iput p2, p0, Lcom/konka/tvsettings/MovingPanelParameter;->iMarginSpace:I

    iput p3, p0, Lcom/konka/tvsettings/MovingPanelParameter;->iItemCounts:I

    iput p4, p0, Lcom/konka/tvsettings/MovingPanelParameter;->iCenterOffset:I

    iput p5, p0, Lcom/konka/tvsettings/MovingPanelParameter;->iMoveSpeed:I

    return-void
.end method
