.class public Lcom/konka/tvsettings/video3d/S3dEngineActivity;
.super Landroid/app/Activity;
.source "S3dEngineActivity.java"


# static fields
.field private static final STATUS_OFF:I = 0x0

.field private static final STATUS_ON:I = 0x1

.field private static final THREE_D_2TO3:I = 0x2

.field private static final THREE_D_CLAR:I = 0x3

.field private static final THREE_D_LR:I = 0x0

.field private static final THREE_D_TB:I = 0x1

.field private static bCheckS3dEngineFirstCreate:Z

.field private static isADV:Z


# instance fields
.field private DEPTH_VALUE:I

.field private OFFSET_VALUE:I

.field private i3dDepth:Lcom/konka/tvsettings/view/IUpdateSysData;

.field private i3dEffect:Lcom/konka/tvsettings/view/IUpdateSysData;

.field private i3dOffset:Lcom/konka/tvsettings/view/IUpdateSysData;

.field private i3dSequence:Lcom/konka/tvsettings/view/IUpdateSysData;

.field private iClar:[S

.field private iEffect:[S

.field private iPq:[S

.field private iSequence:I

.field private mHandler:Landroid/os/Handler;

.field private mItem3DDepth:Lcom/konka/tvsettings/view/PictureSettingItem2;

.field private mItem3DEffect:Lcom/konka/tvsettings/view/PictureSettingItem1;

.field private mItem3DOffset:Lcom/konka/tvsettings/view/PictureSettingItem2;

.field private mItem3DSequence:Lcom/konka/tvsettings/view/PictureSettingItem1;

.field private ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->isADV:Z

    sput-boolean v0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->bCheckS3dEngineFirstCreate:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x5

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lcom/konka/tvsettings/video3d/S3dEngineActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/video3d/S3dEngineActivity$1;-><init>(Lcom/konka/tvsettings/video3d/S3dEngineActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mHandler:Landroid/os/Handler;

    iput-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->iEffect:[S

    iput-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->iPq:[S

    iput-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->iClar:[S

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->iSequence:I

    iput v2, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->DEPTH_VALUE:I

    iput v2, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->OFFSET_VALUE:I

    new-instance v0, Lcom/konka/tvsettings/video3d/S3dEngineActivity$2;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/video3d/S3dEngineActivity$2;-><init>(Lcom/konka/tvsettings/video3d/S3dEngineActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->i3dEffect:Lcom/konka/tvsettings/view/IUpdateSysData;

    new-instance v0, Lcom/konka/tvsettings/video3d/S3dEngineActivity$3;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/video3d/S3dEngineActivity$3;-><init>(Lcom/konka/tvsettings/video3d/S3dEngineActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->i3dDepth:Lcom/konka/tvsettings/view/IUpdateSysData;

    new-instance v0, Lcom/konka/tvsettings/video3d/S3dEngineActivity$4;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/video3d/S3dEngineActivity$4;-><init>(Lcom/konka/tvsettings/video3d/S3dEngineActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->i3dOffset:Lcom/konka/tvsettings/view/IUpdateSysData;

    new-instance v0, Lcom/konka/tvsettings/video3d/S3dEngineActivity$5;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/video3d/S3dEngineActivity$5;-><init>(Lcom/konka/tvsettings/video3d/S3dEngineActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->i3dSequence:Lcom/konka/tvsettings/view/IUpdateSysData;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/video3d/S3dEngineActivity;)Lcom/konka/tvsettings/view/PictureSettingItem1;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DEffect:Lcom/konka/tvsettings/view/PictureSettingItem1;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/video3d/S3dEngineActivity;)Lcom/konka/kkinterface/tv/S3DDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/video3d/S3dEngineActivity;)Lcom/konka/tvsettings/view/PictureSettingItem2;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DDepth:Lcom/konka/tvsettings/view/PictureSettingItem2;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/video3d/S3dEngineActivity;)Lcom/konka/tvsettings/view/PictureSettingItem2;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DOffset:Lcom/konka/tvsettings/view/PictureSettingItem2;

    return-object v0
.end method

.method static synthetic access$4()Z
    .locals 1

    sget-boolean v0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->bCheckS3dEngineFirstCreate:Z

    return v0
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/video3d/S3dEngineActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->DEPTH_VALUE:I

    return-void
.end method

.method static synthetic access$6(Lcom/konka/tvsettings/video3d/S3dEngineActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->OFFSET_VALUE:I

    return-void
.end method

.method static synthetic access$7(Lcom/konka/tvsettings/video3d/S3dEngineActivity;)Lcom/konka/tvsettings/view/PictureSettingItem1;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DSequence:Lcom/konka/tvsettings/view/PictureSettingItem1;

    return-object v0
.end method

.method private findFocus()V
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "Which3DMode"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "findFocus :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    packed-switch v1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DEffect:Lcom/konka/tvsettings/view/PictureSettingItem1;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/PictureSettingItem1;->setStatusFbd()V

    invoke-virtual {p0, v4}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->showItemDepthOffset(Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private initData()V
    .locals 10

    const/16 v9, 0xa

    const/4 v8, 0x1

    const/high16 v7, 0x41800000    # 16.0f

    const/4 v6, 0x0

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/S3DDesk;->get3DDepthMode()I

    move-result v1

    div-int/lit8 v1, v1, 0x3

    iput v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->DEPTH_VALUE:I

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/S3DDesk;->get3DOffsetMode()I

    move-result v1

    div-int/lit8 v1, v1, 0x3

    iput v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->OFFSET_VALUE:I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    const-string v2, "GetPicQualityImproved"

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosCommonCommand(Ljava/lang/String;)[S

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->iPq:[S

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    const-string v2, "GetGainAndOffsetMode"

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosCommonCommand(Ljava/lang/String;)[S

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->iEffect:[S

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    const-string v2, "GetLOW3dQualityStatus"

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosCommonCommand(Ljava/lang/String;)[S

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->iClar:[S

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Effect: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->iEffect:[S

    const/4 v3, 0x0

    aget-short v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; Pq: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->iPq:[S

    const/4 v3, 0x0

    aget-short v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; Clar: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->iClar:[S

    const/4 v3, 0x0

    aget-short v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->iPq:[S

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->iPq:[S

    array-length v1, v1

    if-nez v1, :cond_2

    :cond_1
    const/4 v1, 0x1

    new-array v1, v1, [S

    iput-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->iPq:[S

    :cond_2
    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->iEffect:[S

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->iEffect:[S

    array-length v1, v1

    if-nez v1, :cond_4

    :cond_3
    const/4 v1, 0x1

    new-array v1, v1, [S

    iput-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->iEffect:[S

    :cond_4
    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->iClar:[S

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->iClar:[S

    array-length v1, v1

    if-nez v1, :cond_6

    :cond_5
    const/4 v1, 0x1

    new-array v1, v1, [S

    iput-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->iClar:[S
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_6
    :goto_0
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;->DB_ThreeD_Video_LRVIEWSWITCH_EXCHANGE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/S3DDesk;->getLRViewSwitch()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_LRVIEWSWITCH;

    move-result-object v2

    if-ne v1, v2, :cond_7

    iput v8, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->iSequence:I

    :goto_1
    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DEffect:Lcom/konka/tvsettings/view/PictureSettingItem1;

    const v2, 0x7f0b0020    # com.konka.tvsettings.R.array.str_arr_v3d_effect_vals

    iget-object v3, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->iEffect:[S

    aget-short v3, v3, v6

    iget-object v4, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->i3dEffect:Lcom/konka/tvsettings/view/IUpdateSysData;

    sget-boolean v5, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->isADV:Z

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/konka/tvsettings/view/PictureSettingItem1;->initData(IILcom/konka/tvsettings/view/IUpdateSysData;Z)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DEffect:Lcom/konka/tvsettings/view/PictureSettingItem1;

    iget-object v1, v1, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemName:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DEffect:Lcom/konka/tvsettings/view/PictureSettingItem1;

    iget-object v1, v1, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemValue:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DDepth:Lcom/konka/tvsettings/view/PictureSettingItem2;

    iget v2, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->DEPTH_VALUE:I

    iget-object v3, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->i3dDepth:Lcom/konka/tvsettings/view/IUpdateSysData;

    invoke-virtual {v1, v2, v6, v3}, Lcom/konka/tvsettings/view/PictureSettingItem2;->initData(IILcom/konka/tvsettings/view/IUpdateSysData;)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DDepth:Lcom/konka/tvsettings/view/PictureSettingItem2;

    iget-object v1, v1, Lcom/konka/tvsettings/view/PictureSettingItem2;->mItemName:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DDepth:Lcom/konka/tvsettings/view/PictureSettingItem2;

    iget-object v1, v1, Lcom/konka/tvsettings/view/PictureSettingItem2;->mItemValue:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DDepth:Lcom/konka/tvsettings/view/PictureSettingItem2;

    invoke-virtual {v1, v9}, Lcom/konka/tvsettings/view/PictureSettingItem2;->setMax(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DOffset:Lcom/konka/tvsettings/view/PictureSettingItem2;

    iget v2, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->OFFSET_VALUE:I

    iget-object v3, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->i3dOffset:Lcom/konka/tvsettings/view/IUpdateSysData;

    invoke-virtual {v1, v2, v6, v3}, Lcom/konka/tvsettings/view/PictureSettingItem2;->initData(IILcom/konka/tvsettings/view/IUpdateSysData;)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DOffset:Lcom/konka/tvsettings/view/PictureSettingItem2;

    invoke-virtual {v1, v9}, Lcom/konka/tvsettings/view/PictureSettingItem2;->setMax(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DEffect:Lcom/konka/tvsettings/view/PictureSettingItem1;

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/PictureSettingItem1;->getCurrentState()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_8

    invoke-virtual {p0, v8}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->showItemDepthOffset(Z)V

    :goto_2
    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DSequence:Lcom/konka/tvsettings/view/PictureSettingItem1;

    const v2, 0x7f0b0021    # com.konka.tvsettings.R.array.str_arr_v3d_sequence_vals

    iget v3, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->iSequence:I

    iget-object v4, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->i3dSequence:Lcom/konka/tvsettings/view/IUpdateSysData;

    sget-boolean v5, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->isADV:Z

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/konka/tvsettings/view/PictureSettingItem1;->initData(IILcom/konka/tvsettings/view/IUpdateSysData;Z)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DSequence:Lcom/konka/tvsettings/view/PictureSettingItem1;

    iget-object v1, v1, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemName:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DSequence:Lcom/konka/tvsettings/view/PictureSettingItem1;

    iget-object v1, v1, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemValue:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setTextSize(F)V

    sput-boolean v6, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->bCheckS3dEngineFirstCreate:Z

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0

    :cond_7
    iput v6, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->iSequence:I

    goto :goto_1

    :cond_8
    invoke-virtual {p0, v6}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->showItemDepthOffset(Z)V

    goto :goto_2
.end method

.method private initListener()V
    .locals 0

    return-void
.end method

.method private setupView()V
    .locals 1

    const v0, 0x7f0701c7    # com.konka.tvsettings.R.id.s3d_engine_3d_effect

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/view/PictureSettingItem1;

    iput-object v0, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DEffect:Lcom/konka/tvsettings/view/PictureSettingItem1;

    const v0, 0x7f0701c8    # com.konka.tvsettings.R.id.s3d_engine_depth

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/view/PictureSettingItem2;

    iput-object v0, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DDepth:Lcom/konka/tvsettings/view/PictureSettingItem2;

    const v0, 0x7f0701c9    # com.konka.tvsettings.R.id.s3d_engine_offset

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/view/PictureSettingItem2;

    iput-object v0, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DOffset:Lcom/konka/tvsettings/view/PictureSettingItem2;

    const v0, 0x7f0701ca    # com.konka.tvsettings.R.id.s3d_engine_sequence

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/view/PictureSettingItem1;

    iput-object v0, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DSequence:Lcom/konka/tvsettings/view/PictureSettingItem1;

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030045    # com.konka.tvsettings.R.layout.s3d_engine_menu

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->setContentView(I)V

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getS3DManagerInstance()Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    const/4 v0, 0x1

    sput-boolean v0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->bCheckS3dEngineFirstCreate:Z

    invoke-direct {p0}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->setupView()V

    invoke-direct {p0}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->initData()V

    invoke-direct {p0}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->findFocus()V

    invoke-direct {p0}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->initListener()V

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/konka/tvsettings/common/LittleDownTimer;->setHandler(Landroid/os/Handler;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 6
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v5, 0x0

    const/4 v3, 0x1

    sparse-switch p1, :sswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v3

    :goto_1
    :sswitch_0
    return v3

    :sswitch_1
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->finish()V

    const v3, 0x7f040005    # com.konka.tvsettings.R.anim.anim_menu_exit

    invoke-virtual {p0, v5, v3}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->overridePendingTransition(II)V

    goto :goto_0

    :sswitch_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v4, "isFromMain"

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v4, "focusItem"

    const-string v5, "mItemEngine"

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-class v4, Lcom/konka/tvsettings/video3d/S3dSettingActivity;

    invoke-virtual {v1, p0, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->finish()V

    :sswitch_3
    iget-object v4, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DEffect:Lcom/konka/tvsettings/view/PictureSettingItem1;

    invoke-virtual {v4}, Lcom/konka/tvsettings/view/PictureSettingItem1;->isSelected()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DSequence:Lcom/konka/tvsettings/view/PictureSettingItem1;

    invoke-virtual {v4}, Lcom/konka/tvsettings/view/PictureSettingItem1;->requestFocus()Z

    goto :goto_1

    :sswitch_4
    iget-object v4, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DSequence:Lcom/konka/tvsettings/view/PictureSettingItem1;

    invoke-virtual {v4}, Lcom/konka/tvsettings/view/PictureSettingItem1;->isSelected()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DEffect:Lcom/konka/tvsettings/view/PictureSettingItem1;

    invoke-virtual {v4}, Lcom/konka/tvsettings/view/PictureSettingItem1;->requestFocus()Z

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x13 -> :sswitch_3
        0x14 -> :sswitch_4
        0x52 -> :sswitch_2
        0xb2 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 1

    const-string v0, "S3dEngineActivity==========>>>5"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->pauseMenu()V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 1

    const-string v0, "S3dEngineActivity==========>>>3"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resumeMenu()V

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method

.method public onUserInteraction()V
    .locals 1

    const-string v0, "S3dEngineActivity==========>>>4"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resetMenu()V

    invoke-super {p0}, Landroid/app/Activity;->onUserInteraction()V

    return-void
.end method

.method public showItemDepthOffset(Z)V
    .locals 3
    .param p1    # Z

    const/16 v2, 0x8

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DDepth:Lcom/konka/tvsettings/view/PictureSettingItem2;

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/view/PictureSettingItem2;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DOffset:Lcom/konka/tvsettings/view/PictureSettingItem2;

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/view/PictureSettingItem2;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DDepth:Lcom/konka/tvsettings/view/PictureSettingItem2;

    invoke-virtual {v0, v2}, Lcom/konka/tvsettings/view/PictureSettingItem2;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DOffset:Lcom/konka/tvsettings/view/PictureSettingItem2;

    invoke-virtual {v0, v2}, Lcom/konka/tvsettings/view/PictureSettingItem2;->setVisibility(I)V

    goto :goto_0
.end method
