.class Lcom/konka/tvsettings/video3d/S3dSettingActivity$2;
.super Ljava/lang/Object;
.source "S3dSettingActivity.java"

# interfaces
.implements Lcom/konka/tvsettings/view/IUpdateSysData;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/video3d/S3dSettingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/video3d/S3dSettingActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/video3d/S3dSettingActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity$2;->this$0:Lcom/konka/tvsettings/video3d/S3dSettingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/video3d/S3dSettingActivity$2;)Lcom/konka/tvsettings/video3d/S3dSettingActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity$2;->this$0:Lcom/konka/tvsettings/video3d/S3dSettingActivity;

    return-object v0
.end method


# virtual methods
.method public doUpdate()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity$2;->this$0:Lcom/konka/tvsettings/video3d/S3dSettingActivity;

    # getter for: Lcom/konka/tvsettings/video3d/S3dSettingActivity;->pd:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->access$0(Lcom/konka/tvsettings/video3d/S3dSettingActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity$2;->this$0:Lcom/konka/tvsettings/video3d/S3dSettingActivity;

    # getter for: Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dAuto:Lcom/konka/tvsettings/view/PictureSettingItem1;
    invoke-static {v0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->access$2(Lcom/konka/tvsettings/video3d/S3dSettingActivity;)Lcom/konka/tvsettings/view/PictureSettingItem1;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/tvsettings/view/PictureSettingItem1;->getCurrentState()I

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "item auto doupdate off"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/tvsettings/video3d/S3dSettingActivity$2$1;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity$2$1;-><init>(Lcom/konka/tvsettings/video3d/S3dSettingActivity$2;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity$2;->this$0:Lcom/konka/tvsettings/video3d/S3dSettingActivity;

    # getter for: Lcom/konka/tvsettings/video3d/S3dSettingActivity;->pd:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->access$0(Lcom/konka/tvsettings/video3d/S3dSettingActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity$2;->this$0:Lcom/konka/tvsettings/video3d/S3dSettingActivity;

    # getter for: Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dAuto:Lcom/konka/tvsettings/view/PictureSettingItem1;
    invoke-static {v1}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->access$2(Lcom/konka/tvsettings/video3d/S3dSettingActivity;)Lcom/konka/tvsettings/view/PictureSettingItem1;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/PictureSettingItem1;->getCurrentState()I

    move-result v1

    if-ne v0, v1, :cond_0

    const-string v0, "item auto doupdate on"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/tvsettings/video3d/S3dSettingActivity$2$2;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity$2$2;-><init>(Lcom/konka/tvsettings/video3d/S3dSettingActivity$2;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method
