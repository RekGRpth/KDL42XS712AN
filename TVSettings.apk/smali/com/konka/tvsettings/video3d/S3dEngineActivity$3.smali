.class Lcom/konka/tvsettings/video3d/S3dEngineActivity$3;
.super Ljava/lang/Object;
.source "S3dEngineActivity.java"

# interfaces
.implements Lcom/konka/tvsettings/view/IUpdateSysData;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/video3d/S3dEngineActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/video3d/S3dEngineActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/video3d/S3dEngineActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity$3;->this$0:Lcom/konka/tvsettings/video3d/S3dEngineActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public doUpdate()V
    .locals 2

    # getter for: Lcom/konka/tvsettings/video3d/S3dEngineActivity;->bCheckS3dEngineFirstCreate:Z
    invoke-static {}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->access$4()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity$3;->this$0:Lcom/konka/tvsettings/video3d/S3dEngineActivity;

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity$3;->this$0:Lcom/konka/tvsettings/video3d/S3dEngineActivity;

    # getter for: Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DDepth:Lcom/konka/tvsettings/view/PictureSettingItem2;
    invoke-static {v1}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->access$2(Lcom/konka/tvsettings/video3d/S3dEngineActivity;)Lcom/konka/tvsettings/view/PictureSettingItem2;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/PictureSettingItem2;->getCurrentValue()I

    move-result v1

    invoke-static {v0, v1}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->access$5(Lcom/konka/tvsettings/video3d/S3dEngineActivity;I)V

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity$3;->this$0:Lcom/konka/tvsettings/video3d/S3dEngineActivity;

    # getter for: Lcom/konka/tvsettings/video3d/S3dEngineActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;
    invoke-static {v0}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->access$1(Lcom/konka/tvsettings/video3d/S3dEngineActivity;)Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity$3;->this$0:Lcom/konka/tvsettings/video3d/S3dEngineActivity;

    # getter for: Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DDepth:Lcom/konka/tvsettings/view/PictureSettingItem2;
    invoke-static {v1}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->access$2(Lcom/konka/tvsettings/video3d/S3dEngineActivity;)Lcom/konka/tvsettings/view/PictureSettingItem2;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/PictureSettingItem2;->getCurrentValue()I

    move-result v1

    mul-int/lit8 v1, v1, 0x3

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DDepthMode(I)Z

    :cond_0
    return-void
.end method
