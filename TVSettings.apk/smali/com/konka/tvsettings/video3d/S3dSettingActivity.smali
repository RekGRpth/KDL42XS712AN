.class public Lcom/konka/tvsettings/video3d/S3dSettingActivity;
.super Lcom/konka/tvsettings/BaseKonkaActivity;
.source "S3dSettingActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$Enum3dType:[I = null

.field private static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I = null

.field private static final MSG_DISMISS_PROGRESSDIALOG:I = 0x0

.field private static final STATUS_OFF:I = 0x0

.field private static final STATUS_ON:I = 0x1

.field private static final THREE_D_2TO3:I = 0x2

.field private static final THREE_D_CLAR:I = 0x3

.field private static final THREE_D_LR:I = 0x0

.field private static final THREE_D_TB:I = 0x1

.field private static isADV:Z


# instance fields
.field private final STATUS_FBD:I

.field private final STATUS_NOR:I

.field private final STATUS_RUN:I

.field private final STATUS_SEL:I

.field private is3DClar:Z

.field private mHandler:Landroid/os/Handler;

.field private mItem2dTo3d:Lcom/konka/tvsettings/view/S3dItem1;

.field private mItem3dAuto:Lcom/konka/tvsettings/view/PictureSettingItem1;

.field private mItem3dClar:Lcom/konka/tvsettings/view/S3dItem1;

.field private mItem3dLr:Lcom/konka/tvsettings/view/S3dItem1;

.field private mItem3dOff:Lcom/konka/tvsettings/view/S3dItem1;

.field private mItem3dTb:Lcom/konka/tvsettings/view/S3dItem1;

.field private mItem3dTo2d:Lcom/konka/tvsettings/view/S3dItem1;

.field private mItemEngine:Landroid/widget/LinearLayout;

.field private ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

.field private pd:Landroid/app/ProgressDialog;

.field private updateSysData3dAuto:Lcom/konka/tvsettings/view/IUpdateSysData;


# direct methods
.method static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$Enum3dType()[I
    .locals 3

    sget-object v0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$Enum3dType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->values()[Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_2DTO3D:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_c

    :goto_1
    :try_start_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_CHECK_BORAD:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_b

    :goto_2
    :try_start_2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_DUALVIEW:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_a

    :goto_3
    :try_start_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FIELD_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_9

    :goto_4
    :try_start_4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_8

    :goto_5
    :try_start_5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_PACKING_1080P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_7

    :goto_6
    :try_start_6
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_PACKING_720P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :goto_7
    :try_start_7
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_LINE_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_5

    :goto_8
    :try_start_8
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_4

    :goto_9
    :try_start_9
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_PIXEL_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_3

    :goto_a
    :try_start_a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_SIDE_BY_SIDE_HALF:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_2

    :goto_b
    :try_start_b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_TOP_BOTTOM:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_1

    :goto_c
    :try_start_c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_TYPE_NUM:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_0

    :goto_d
    sput-object v0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$Enum3dType:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_d

    :catch_1
    move-exception v1

    goto :goto_c

    :catch_2
    move-exception v1

    goto :goto_b

    :catch_3
    move-exception v1

    goto :goto_a

    :catch_4
    move-exception v1

    goto :goto_9

    :catch_5
    move-exception v1

    goto :goto_8

    :catch_6
    move-exception v1

    goto :goto_7

    :catch_7
    move-exception v1

    goto :goto_6

    :catch_8
    move-exception v1

    goto :goto_5

    :catch_9
    move-exception v1

    goto :goto_4

    :catch_a
    move-exception v1

    goto :goto_3

    :catch_b
    move-exception v1

    goto/16 :goto_2

    :catch_c
    move-exception v1

    goto/16 :goto_1
.end method

.method static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource()[I
    .locals 3

    sget-object v0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_28

    :goto_1
    :try_start_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_27

    :goto_2
    :try_start_2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_26

    :goto_3
    :try_start_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_25

    :goto_4
    :try_start_4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_24

    :goto_5
    :try_start_5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS5:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_23

    :goto_6
    :try_start_6
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS6:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_22

    :goto_7
    :try_start_7
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS7:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_21

    :goto_8
    :try_start_8
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS8:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_20

    :goto_9
    :try_start_9
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_1f

    :goto_a
    :try_start_a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1d

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_1e

    :goto_b
    :try_start_b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x26

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_1d

    :goto_c
    :try_start_c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1e

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_1c

    :goto_d
    :try_start_d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1f

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_1b

    :goto_e
    :try_start_e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x20

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_1a

    :goto_f
    :try_start_f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x21

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_19

    :goto_10
    :try_start_10
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x22

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_18

    :goto_11
    :try_start_11
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x18

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_17

    :goto_12
    :try_start_12
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x19

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_16

    :goto_13
    :try_start_13
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1a

    aput v2, v0, v1
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_15

    :goto_14
    :try_start_14
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1b

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_14} :catch_14

    :goto_15
    :try_start_15
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1c

    aput v2, v0, v1
    :try_end_15
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15 .. :try_end_15} :catch_13

    :goto_16
    :try_start_16
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_JPEG:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x25

    aput v2, v0, v1
    :try_end_16
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16 .. :try_end_16} :catch_12

    :goto_17
    :try_start_17
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_KTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x24

    aput v2, v0, v1
    :try_end_17
    .catch Ljava/lang/NoSuchFieldError; {:try_start_17 .. :try_end_17} :catch_11

    :goto_18
    :try_start_18
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x29

    aput v2, v0, v1
    :try_end_18
    .catch Ljava/lang/NoSuchFieldError; {:try_start_18 .. :try_end_18} :catch_10

    :goto_19
    :try_start_19
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x28

    aput v2, v0, v1
    :try_end_19
    .catch Ljava/lang/NoSuchFieldError; {:try_start_19 .. :try_end_19} :catch_f

    :goto_1a
    :try_start_1a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SCART:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x15

    aput v2, v0, v1
    :try_end_1a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a .. :try_end_1a} :catch_e

    :goto_1b
    :try_start_1b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SCART2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x16

    aput v2, v0, v1
    :try_end_1b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1b .. :try_end_1b} :catch_d

    :goto_1c
    :try_start_1c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SCART_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x17

    aput v2, v0, v1
    :try_end_1c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1c .. :try_end_1c} :catch_c

    :goto_1d
    :try_start_1d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x23

    aput v2, v0, v1
    :try_end_1d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1d .. :try_end_1d} :catch_b

    :goto_1e
    :try_start_1e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x27

    aput v2, v0, v1
    :try_end_1e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1e .. :try_end_1e} :catch_a

    :goto_1f
    :try_start_1f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_1f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1f .. :try_end_1f} :catch_9

    :goto_20
    :try_start_20
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_20
    .catch Ljava/lang/NoSuchFieldError; {:try_start_20 .. :try_end_20} :catch_8

    :goto_21
    :try_start_21
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_21
    .catch Ljava/lang/NoSuchFieldError; {:try_start_21 .. :try_end_21} :catch_7

    :goto_22
    :try_start_22
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_22
    .catch Ljava/lang/NoSuchFieldError; {:try_start_22 .. :try_end_22} :catch_6

    :goto_23
    :try_start_23
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_23
    .catch Ljava/lang/NoSuchFieldError; {:try_start_23 .. :try_end_23} :catch_5

    :goto_24
    :try_start_24
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_VGA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_24
    .catch Ljava/lang/NoSuchFieldError; {:try_start_24 .. :try_end_24} :catch_4

    :goto_25
    :try_start_25
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_25
    .catch Ljava/lang/NoSuchFieldError; {:try_start_25 .. :try_end_25} :catch_3

    :goto_26
    :try_start_26
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x12

    aput v2, v0, v1
    :try_end_26
    .catch Ljava/lang/NoSuchFieldError; {:try_start_26 .. :try_end_26} :catch_2

    :goto_27
    :try_start_27
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x13

    aput v2, v0, v1
    :try_end_27
    .catch Ljava/lang/NoSuchFieldError; {:try_start_27 .. :try_end_27} :catch_1

    :goto_28
    :try_start_28
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x14

    aput v2, v0, v1
    :try_end_28
    .catch Ljava/lang/NoSuchFieldError; {:try_start_28 .. :try_end_28} :catch_0

    :goto_29
    sput-object v0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_29

    :catch_1
    move-exception v1

    goto :goto_28

    :catch_2
    move-exception v1

    goto :goto_27

    :catch_3
    move-exception v1

    goto :goto_26

    :catch_4
    move-exception v1

    goto :goto_25

    :catch_5
    move-exception v1

    goto :goto_24

    :catch_6
    move-exception v1

    goto :goto_23

    :catch_7
    move-exception v1

    goto :goto_22

    :catch_8
    move-exception v1

    goto :goto_21

    :catch_9
    move-exception v1

    goto :goto_20

    :catch_a
    move-exception v1

    goto :goto_1f

    :catch_b
    move-exception v1

    goto/16 :goto_1e

    :catch_c
    move-exception v1

    goto/16 :goto_1d

    :catch_d
    move-exception v1

    goto/16 :goto_1c

    :catch_e
    move-exception v1

    goto/16 :goto_1b

    :catch_f
    move-exception v1

    goto/16 :goto_1a

    :catch_10
    move-exception v1

    goto/16 :goto_19

    :catch_11
    move-exception v1

    goto/16 :goto_18

    :catch_12
    move-exception v1

    goto/16 :goto_17

    :catch_13
    move-exception v1

    goto/16 :goto_16

    :catch_14
    move-exception v1

    goto/16 :goto_15

    :catch_15
    move-exception v1

    goto/16 :goto_14

    :catch_16
    move-exception v1

    goto/16 :goto_13

    :catch_17
    move-exception v1

    goto/16 :goto_12

    :catch_18
    move-exception v1

    goto/16 :goto_11

    :catch_19
    move-exception v1

    goto/16 :goto_10

    :catch_1a
    move-exception v1

    goto/16 :goto_f

    :catch_1b
    move-exception v1

    goto/16 :goto_e

    :catch_1c
    move-exception v1

    goto/16 :goto_d

    :catch_1d
    move-exception v1

    goto/16 :goto_c

    :catch_1e
    move-exception v1

    goto/16 :goto_b

    :catch_1f
    move-exception v1

    goto/16 :goto_a

    :catch_20
    move-exception v1

    goto/16 :goto_9

    :catch_21
    move-exception v1

    goto/16 :goto_8

    :catch_22
    move-exception v1

    goto/16 :goto_7

    :catch_23
    move-exception v1

    goto/16 :goto_6

    :catch_24
    move-exception v1

    goto/16 :goto_5

    :catch_25
    move-exception v1

    goto/16 :goto_4

    :catch_26
    move-exception v1

    goto/16 :goto_3

    :catch_27
    move-exception v1

    goto/16 :goto_2

    :catch_28
    move-exception v1

    goto/16 :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->isADV:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;-><init>()V

    iput-boolean v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->is3DClar:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->pd:Landroid/app/ProgressDialog;

    new-instance v0, Lcom/konka/tvsettings/video3d/S3dSettingActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity$1;-><init>(Lcom/konka/tvsettings/video3d/S3dSettingActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/tvsettings/video3d/S3dSettingActivity$2;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity$2;-><init>(Lcom/konka/tvsettings/video3d/S3dSettingActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->updateSysData3dAuto:Lcom/konka/tvsettings/view/IUpdateSysData;

    iput v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->STATUS_NOR:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->STATUS_SEL:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->STATUS_RUN:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->STATUS_FBD:I

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/video3d/S3dSettingActivity;)Landroid/app/ProgressDialog;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->pd:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/video3d/S3dSettingActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->findFocus()V

    return-void
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/video3d/S3dSettingActivity;)Lcom/konka/tvsettings/view/PictureSettingItem1;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dAuto:Lcom/konka/tvsettings/view/PictureSettingItem1;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/video3d/S3dSettingActivity;)Lcom/konka/kkinterface/tv/S3DDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/video3d/S3dSettingActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private findFocus()V
    .locals 11

    const/4 v7, 0x1

    const/4 v10, 0x3

    const/4 v8, 0x0

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mstar/android/tv/TvPipPopManager;->isPipModeEnabled()Z

    move-result v9

    if-eqz v9, :cond_1

    iget-object v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dLr:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v7, v10}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTb:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v7, v10}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dOff:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v7, v10}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTo2d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v7, v10}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem2dTo3d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v7, v10}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dClar:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v7, v10}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dLr:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-direct {p0, v7}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->setBgByStatus(Lcom/konka/tvsettings/view/S3dItem1;)V

    iget-object v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTb:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-direct {p0, v7}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->setBgByStatus(Lcom/konka/tvsettings/view/S3dItem1;)V

    iget-object v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dOff:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-direct {p0, v7}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->setBgByStatus(Lcom/konka/tvsettings/view/S3dItem1;)V

    iget-object v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTo2d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-direct {p0, v7}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->setBgByStatus(Lcom/konka/tvsettings/view/S3dItem1;)V

    iget-object v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem2dTo3d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-direct {p0, v7}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->setBgByStatus(Lcom/konka/tvsettings/view/S3dItem1;)V

    iget-object v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dClar:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-direct {p0, v7}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->setBgByStatus(Lcom/konka/tvsettings/view/S3dItem1;)V

    iget-object v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dAuto:Lcom/konka/tvsettings/view/PictureSettingItem1;

    invoke-virtual {v7}, Lcom/konka/tvsettings/view/PictureSettingItem1;->setStatusFbd()V

    invoke-direct {p0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->setItemEngineFbd()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    iget-object v9, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    invoke-interface {v9}, Lcom/konka/kkinterface/tv/S3DDesk;->getDisplay3DTo2DMode()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    move-result-object v0

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "==============================================get display 3d to 2d: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v9

    if-eqz v9, :cond_8

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v9

    invoke-virtual {v9}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->getCurrent3dFormat()Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v3

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "====================================get current 3d mode: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v9, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    invoke-interface {v9}, Lcom/konka/kkinterface/tv/S3DDesk;->getDisplay3DTo2DMode()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    move-result-object v1

    const/4 v6, 0x0

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_DUALVIEW:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-eq v3, v9, :cond_2

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_TYPE_NUM:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-eq v3, v9, :cond_2

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-ne v3, v9, :cond_5

    sget-object v9, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    if-ne v1, v9, :cond_5

    :cond_2
    move v6, v8

    :goto_1
    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/mstar/android/tv/TvPipPopManager;->isDualViewEnabled()Z

    move-result v7

    if-nez v7, :cond_3

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/mstar/android/tv/TvPipPopManager;->isPopEnabled()Z

    move-result v7

    if-nez v7, :cond_3

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/mstar/android/tv/TvPipPopManager;->isPipEnabled()Z

    move-result v7

    if-eqz v7, :cond_4

    :cond_3
    const/4 v6, 0x0

    :cond_4
    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, " __________________________________  "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-static {}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$Enum3dType()[I

    move-result-object v7

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v9

    aget v7, v7, v9

    packed-switch v7, :pswitch_data_0

    :pswitch_0
    sget-object v7, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    if-ne v1, v7, :cond_6

    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->is3DClar:Z

    iget-object v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dOff:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v7}, Lcom/konka/tvsettings/view/S3dItem1;->getId()I

    move-result v7

    invoke-virtual {p0, v7}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->refreshStatusBgByRunningItem(I)V

    :goto_2
    invoke-direct {p0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->setItemEngineFbd()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_3
    invoke-virtual {p0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v7, "3DEngine"

    invoke-virtual {v4, v7, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItemEngine:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto/16 :goto_0

    :cond_5
    move v6, v7

    goto :goto_1

    :pswitch_1
    const/4 v7, 0x0

    :try_start_1
    iput-boolean v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->is3DClar:Z

    iget-object v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dLr:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v7}, Lcom/konka/tvsettings/view/S3dItem1;->getId()I

    move-result v7

    invoke-virtual {p0, v7}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->refreshStatusBgByRunningItem(I)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_3

    :pswitch_2
    const/4 v7, 0x0

    :try_start_2
    iput-boolean v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->is3DClar:Z

    iget-object v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTb:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v7}, Lcom/konka/tvsettings/view/S3dItem1;->getId()I

    move-result v7

    invoke-virtual {p0, v7}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->refreshStatusBgByRunningItem(I)V

    goto :goto_3

    :pswitch_3
    iget-object v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem2dTo3d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v7}, Lcom/konka/tvsettings/view/S3dItem1;->getId()I

    move-result v7

    invoke-virtual {p0, v7}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->refreshStatusBgByRunningItem(I)V

    goto :goto_3

    :pswitch_4
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->is3DClar:Z

    iget-object v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dClar:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v7}, Lcom/konka/tvsettings/view/S3dItem1;->getId()I

    move-result v7

    invoke-virtual {p0, v7}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->refreshStatusBgByRunningItem(I)V

    goto :goto_3

    :cond_6
    sget-object v7, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_FRAME_PACKING:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    if-ne v1, v7, :cond_7

    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->is3DClar:Z

    iget-object v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTo2d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v7}, Lcom/konka/tvsettings/view/S3dItem1;->getId()I

    move-result v7

    invoke-virtual {p0, v7}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->refreshStatusBgByRunningItem(I)V

    goto :goto_2

    :cond_7
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->is3DClar:Z

    iget-object v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTo2d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v7}, Lcom/konka/tvsettings/view/S3dItem1;->getId()I

    move-result v7

    invoke-virtual {p0, v7}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->refreshStatusBgByRunningItem(I)V

    goto :goto_2

    :cond_8
    const-string v7, "TvManger.getInstance() == null"

    invoke-static {v7}, Lcom/konka/debuginfo/logPrint;->Fatal(Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private initData()V
    .locals 7

    const/high16 v6, 0x41800000    # 16.0f

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/S3DDesk;->getSelfAdaptiveDetect()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    move-result-object v1

    const/4 v0, 0x0

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;->DB_ThreeD_Video_SELF_ADAPTIVE_DETECT_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    if-ne v2, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dAuto:Lcom/konka/tvsettings/view/PictureSettingItem1;

    const v3, 0x7f0b0022    # com.konka.tvsettings.R.array.str_arr_v3d_switch_vals

    iget-object v4, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->updateSysData3dAuto:Lcom/konka/tvsettings/view/IUpdateSysData;

    sget-boolean v5, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->isADV:Z

    invoke-virtual {v2, v3, v0, v4, v5}, Lcom/konka/tvsettings/view/PictureSettingItem1;->initData(IILcom/konka/tvsettings/view/IUpdateSysData;Z)V

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dAuto:Lcom/konka/tvsettings/view/PictureSettingItem1;

    iget-object v2, v2, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemName:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setTextSize(F)V

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dAuto:Lcom/konka/tvsettings/view/PictureSettingItem1;

    iget-object v2, v2, Lcom/konka/tvsettings/view/PictureSettingItem1;->mItemValue:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setTextSize(F)V

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private initListener()V
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dLr:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v0, p0}, Lcom/konka/tvsettings/view/S3dItem1;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTb:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v0, p0}, Lcom/konka/tvsettings/view/S3dItem1;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dOff:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v0, p0}, Lcom/konka/tvsettings/view/S3dItem1;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTo2d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v0, p0}, Lcom/konka/tvsettings/view/S3dItem1;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem2dTo3d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v0, p0}, Lcom/konka/tvsettings/view/S3dItem1;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dClar:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v0, p0}, Lcom/konka/tvsettings/view/S3dItem1;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItemEngine:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private setBgByStatus(Lcom/konka/tvsettings/view/S3dItem1;)V
    .locals 1
    .param p1    # Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {p1}, Lcom/konka/tvsettings/view/S3dItem1;->getStatus()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p1}, Lcom/konka/tvsettings/view/S3dItem1;->setStatusNor()V

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p1}, Lcom/konka/tvsettings/view/S3dItem1;->setStatusSel()V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1}, Lcom/konka/tvsettings/view/S3dItem1;->setStatusRun()V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p1}, Lcom/konka/tvsettings/view/S3dItem1;->setStatusFbd()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private setItemEngineFbd()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItemEngine:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItemEngine:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItemEngine:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    return-void
.end method

.method private setItemEngineNor()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItemEngine:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItemEngine:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItemEngine:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    return-void
.end method

.method private setupView()V
    .locals 3

    const v0, 0x7f0701cd    # com.konka.tvsettings.R.id.s3d_settings_3dlr

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/view/S3dItem1;

    iput-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dLr:Lcom/konka/tvsettings/view/S3dItem1;

    const v0, 0x7f0701ce    # com.konka.tvsettings.R.id.s3d_settings_3dud

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/view/S3dItem1;

    iput-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTb:Lcom/konka/tvsettings/view/S3dItem1;

    const v0, 0x7f0701cf    # com.konka.tvsettings.R.id.s3d_settings_3doff

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/view/S3dItem1;

    iput-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dOff:Lcom/konka/tvsettings/view/S3dItem1;

    const v0, 0x7f0701d0    # com.konka.tvsettings.R.id.s3d_settings_3dto2d

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/view/S3dItem1;

    iput-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTo2d:Lcom/konka/tvsettings/view/S3dItem1;

    const v0, 0x7f0701d1    # com.konka.tvsettings.R.id.s3d_settings_2dto3d

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/view/S3dItem1;

    iput-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem2dTo3d:Lcom/konka/tvsettings/view/S3dItem1;

    const v0, 0x7f0701d2    # com.konka.tvsettings.R.id.s3d_settings_3dclar

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/view/S3dItem1;

    iput-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dClar:Lcom/konka/tvsettings/view/S3dItem1;

    const v0, 0x7f0701d3    # com.konka.tvsettings.R.id.s3d_settings_3dauto

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/view/PictureSettingItem1;

    iput-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dAuto:Lcom/konka/tvsettings/view/PictureSettingItem1;

    const v0, 0x7f0701d4    # com.konka.tvsettings.R.id.s3d_settings_3dengine

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItemEngine:Landroid/widget/LinearLayout;

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->pd:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->pd:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00c7    # com.konka.tvsettings.R.string.str_s3d_auto_wait

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 13
    .param p1    # Landroid/view/View;

    const/16 v12, 0x12

    const/16 v11, 0xc

    const/4 v10, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v6

    iget-object v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dLr:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v7}, Lcom/konka/tvsettings/view/S3dItem1;->getId()I

    move-result v7

    if-ne v6, v7, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v6

    invoke-virtual {p0, v6}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->refreshStatusBgByRunningItem(I)V

    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.konka.tv.hotkey.service.UNFREEZE"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v5}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v6, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    sget-object v7, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_SIDE_BY_SIDE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-interface {v6, v7}, Lcom/konka/kkinterface/tv/S3DDesk;->setDisplayFormat(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v6

    iget-object v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTb:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v7}, Lcom/konka/tvsettings/view/S3dItem1;->getId()I

    move-result v7

    if-ne v6, v7, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v6

    invoke-virtual {p0, v6}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->refreshStatusBgByRunningItem(I)V

    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.konka.tv.hotkey.service.UNFREEZE"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v5}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v6, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    sget-object v7, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_TOP_BOTTOM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-interface {v6, v7}, Lcom/konka/kkinterface/tv/S3DDesk;->setDisplayFormat(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;)Z

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v6

    iget-object v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dOff:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v7}, Lcom/konka/tvsettings/view/S3dItem1;->getId()I

    move-result v7

    if-ne v6, v7, :cond_3

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v6

    invoke-virtual {p0, v6}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->refreshStatusBgByRunningItem(I)V

    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.konka.tv.hotkey.service.UNFREEZE"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v5}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v6, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    sget-object v7, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-interface {v6, v7}, Lcom/konka/kkinterface/tv/S3DDesk;->setDisplayFormat(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;)Z

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v6

    iget-object v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTo2d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v7}, Lcom/konka/tvsettings/view/S3dItem1;->getId()I

    move-result v7

    if-ne v6, v7, :cond_4

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v6

    invoke-virtual {p0, v6}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->refreshStatusBgByRunningItem(I)V

    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.konka.tv.hotkey.service.UNFREEZE"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v5}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->sendBroadcast(Landroid/content/Intent;)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->getCurrent3dFormat()Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v2

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "set 3Dto2D"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "guan"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$Enum3dType()[I

    move-result-object v6

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    iget-object v6, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    sget-object v7, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_SIDE_BY_SIDE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    invoke-interface {v6, v7}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DTo2D(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;)Z

    iget-object v6, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dLr:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v6}, Lcom/konka/tvsettings/view/S3dItem1;->requestFocus()Z

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "set 3Dto2D guan side by side"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "guan"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0

    :pswitch_1
    :try_start_1
    iget-object v6, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    sget-object v7, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_TOP_BOTTOM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    invoke-interface {v6, v7}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DTo2D(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;)Z

    iget-object v6, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dLr:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v6}, Lcom/konka/tvsettings/view/S3dItem1;->requestFocus()Z

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "set 3Dto2D guan top bottom"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "guan"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_2
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->is3DClar:Z

    iget-object v6, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    sget-object v7, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_FRAME_PACKING:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    invoke-interface {v6, v7}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DTo2D(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;)Z

    iget-object v6, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dClar:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v6}, Lcom/konka/tvsettings/view/S3dItem1;->requestFocus()Z

    invoke-direct {p0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->setItemEngineFbd()V

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "set 3Dto2D guan frame_packing"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "guan"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_3
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->is3DClar:Z

    iget-object v6, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    sget-object v7, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_FRAME_PACKING:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    invoke-interface {v6, v7}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DTo2D(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;)Z

    iget-object v6, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dClar:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v6}, Lcom/konka/tvsettings/view/S3dItem1;->requestFocus()Z

    invoke-direct {p0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->setItemEngineFbd()V

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "set 3Dto2D guan frame_packing"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, "guan"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v6

    iget-object v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem2dTo3d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v7}, Lcom/konka/tvsettings/view/S3dItem1;->getId()I

    move-result v7

    if-ne v6, v7, :cond_8

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v6

    invoke-virtual {p0, v6}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->refreshStatusBgByRunningItem(I)V

    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.konka.tv.hotkey.service.UNFREEZE"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v5}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v6, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    sget-object v7, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_2DTO3D:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-interface {v6, v7}, Lcom/konka/kkinterface/tv/S3DDesk;->setDisplayFormat(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;)Z

    const/4 v3, 0x0

    :try_start_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    if-eqz v6, :cond_5

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    const-string v7, "GetGainAndOffsetMode"

    invoke-virtual {v6, v7}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosCommonCommand(Ljava/lang/String;)[S
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v3

    :cond_5
    :goto_1
    if-eqz v3, :cond_6

    array-length v6, v3

    if-nez v6, :cond_7

    :cond_6
    new-array v3, v8, [S

    :cond_7
    aget-short v6, v3, v10

    packed-switch v6, :pswitch_data_1

    goto/16 :goto_0

    :pswitch_4
    iget-object v6, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    invoke-interface {v6, v12}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DDepthMode(I)Z

    iget-object v6, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    invoke-interface {v6, v12}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DOffsetMode(I)Z

    goto/16 :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :pswitch_5
    iget-object v6, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    const/16 v7, 0x18

    invoke-interface {v6, v7}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DDepthMode(I)Z

    iget-object v6, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    const/16 v7, 0x18

    invoke-interface {v6, v7}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DOffsetMode(I)Z

    goto/16 :goto_0

    :pswitch_6
    iget-object v6, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    invoke-interface {v6, v11}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DDepthMode(I)Z

    iget-object v6, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    invoke-interface {v6, v11}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DOffsetMode(I)Z

    goto/16 :goto_0

    :pswitch_7
    iget-object v6, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    iget-object v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    invoke-interface {v7}, Lcom/konka/kkinterface/tv/S3DDesk;->get3DDepthMode()I

    move-result v7

    invoke-interface {v6, v7}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DDepthMode(I)Z

    iget-object v6, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    iget-object v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    invoke-interface {v7}, Lcom/konka/kkinterface/tv/S3DDesk;->get3DOffsetMode()I

    move-result v7

    invoke-interface {v6, v7}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DOffsetMode(I)Z

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v6

    iget-object v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dClar:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v7}, Lcom/konka/tvsettings/view/S3dItem1;->getId()I

    move-result v7

    if-ne v6, v7, :cond_9

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v6

    invoke-virtual {p0, v6}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->refreshStatusBgByRunningItem(I)V

    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.konka.tv.hotkey.service.UNFREEZE"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v5}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->sendBroadcast(Landroid/content/Intent;)V

    iput-boolean v8, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->is3DClar:Z

    iget-object v6, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    sget-object v7, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_FRAME_PACKING:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-interface {v6, v7}, Lcom/konka/kkinterface/tv/S3DDesk;->setDisplayFormat(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;)Z

    goto/16 :goto_0

    :cond_9
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v6

    iget-object v7, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItemEngine:Landroid/widget/LinearLayout;

    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getId()I

    move-result v7

    if-ne v6, v7, :cond_0

    new-instance v4, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const-class v7, Lcom/konka/tvsettings/video3d/S3dEngineActivity;

    invoke-direct {v4, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v6, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem2dTo3d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v6}, Lcom/konka/tvsettings/view/S3dItem1;->getStatus()I

    move-result v6

    if-ne v9, v6, :cond_b

    const-string v6, "Which3DMode"

    invoke-virtual {v4, v6, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_a
    :goto_2
    invoke-virtual {p0, v4}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->finish()V

    goto/16 :goto_0

    :cond_b
    iget-object v6, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dClar:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v6}, Lcom/konka/tvsettings/view/S3dItem1;->getStatus()I

    move-result v6

    if-ne v9, v6, :cond_c

    const-string v6, "Which3DMode"

    const/4 v7, 0x3

    invoke-virtual {v4, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_2

    :cond_c
    iget-object v6, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTb:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v6}, Lcom/konka/tvsettings/view/S3dItem1;->getStatus()I

    move-result v6

    if-ne v9, v6, :cond_d

    const-string v6, "Which3DMode"

    invoke-virtual {v4, v6, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_2

    :cond_d
    iget-object v6, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dLr:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v6}, Lcom/konka/tvsettings/view/S3dItem1;->getStatus()I

    move-result v6

    if-ne v9, v6, :cond_a

    const-string v6, "Which3DMode"

    invoke-virtual {v4, v6, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/konka/tvsettings/BaseKonkaActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030047    # com.konka.tvsettings.R.layout.s3d_setting_menu

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->setContentView(I)V

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getS3DManagerInstance()Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    invoke-direct {p0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->setupView()V

    invoke-direct {p0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->initData()V

    invoke-direct {p0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->initListener()V

    invoke-direct {p0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->findFocus()V

    iget-object v0, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/konka/tvsettings/common/LittleDownTimer;->setHandler(Landroid/os/Handler;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const v2, 0x7f040005    # com.konka.tvsettings.R.anim.anim_menu_exit

    const/4 v1, 0x1

    const/4 v3, 0x0

    sparse-switch p1, :sswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/konka/tvsettings/BaseKonkaActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    :goto_1
    :sswitch_0
    return v1

    :sswitch_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->finish()V

    invoke-virtual {p0, v3, v2}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->overridePendingTransition(II)V

    goto :goto_0

    :sswitch_2
    const-string v1, "Exit"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->finish()V

    invoke-virtual {p0, v3, v2}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->overridePendingTransition(II)V

    goto :goto_0

    :sswitch_3
    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dLr:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/S3dItem1;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItemEngine:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->isFocusable()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItemEngine:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->requestFocus()Z

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItemEngine:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->playSoundEffect(I)V

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItemEngine:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->isFocusable()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dAuto:Lcom/konka/tvsettings/view/PictureSettingItem1;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/PictureSettingItem1;->isFocusable()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dAuto:Lcom/konka/tvsettings/view/PictureSettingItem1;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/PictureSettingItem1;->requestFocus()Z

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dAuto:Lcom/konka/tvsettings/view/PictureSettingItem1;

    invoke-virtual {v2, v3}, Lcom/konka/tvsettings/view/PictureSettingItem1;->playSoundEffect(I)V

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItemEngine:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->isFocusable()Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem2dTo3d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/S3dItem1;->isFocusable()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem2dTo3d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/S3dItem1;->requestFocus()Z

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem2dTo3d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v2, v3}, Lcom/konka/tvsettings/view/S3dItem1;->playSoundEffect(I)V

    goto :goto_1

    :cond_3
    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dOff:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/S3dItem1;->requestFocus()Z

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dOff:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v2, v3}, Lcom/konka/tvsettings/view/S3dItem1;->playSoundEffect(I)V

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTb:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/S3dItem1;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dLr:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/S3dItem1;->isFocusable()Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItemEngine:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->requestFocus()Z

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItemEngine:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->playSoundEffect(I)V

    goto/16 :goto_1

    :cond_5
    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dOff:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/S3dItem1;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dLr:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/S3dItem1;->isFocusable()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTb:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/S3dItem1;->isFocusable()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItemEngine:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->requestFocus()Z

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItemEngine:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->playSoundEffect(I)V

    goto/16 :goto_1

    :sswitch_4
    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItemEngine:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dLr:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/S3dItem1;->isFocusable()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dLr:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/S3dItem1;->requestFocus()Z

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dLr:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v2, v3}, Lcom/konka/tvsettings/view/S3dItem1;->playSoundEffect(I)V

    goto/16 :goto_1

    :cond_6
    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dLr:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/S3dItem1;->isFocusable()Z

    move-result v2

    if-nez v2, :cond_7

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTb:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/S3dItem1;->isFocusable()Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTb:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/S3dItem1;->requestFocus()Z

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTb:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v2, v3}, Lcom/konka/tvsettings/view/S3dItem1;->playSoundEffect(I)V

    goto/16 :goto_1

    :cond_7
    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dOff:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/S3dItem1;->requestFocus()Z

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dOff:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v2, v3}, Lcom/konka/tvsettings/view/S3dItem1;->playSoundEffect(I)V

    goto/16 :goto_1

    :cond_8
    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dAuto:Lcom/konka/tvsettings/view/PictureSettingItem1;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/PictureSettingItem1;->isSelected()Z

    move-result v2

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItemEngine:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->isFocusable()Z

    move-result v2

    if-nez v2, :cond_9

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dLr:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/S3dItem1;->requestFocus()Z

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dLr:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v2, v3}, Lcom/konka/tvsettings/view/S3dItem1;->playSoundEffect(I)V

    goto/16 :goto_1

    :cond_9
    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItemEngine:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->isFocusable()Z

    move-result v2

    if-nez v2, :cond_a

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dAuto:Lcom/konka/tvsettings/view/PictureSettingItem1;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/PictureSettingItem1;->isFocusable()Z

    move-result v2

    if-nez v2, :cond_a

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dClar:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/S3dItem1;->isFocusable()Z

    move-result v2

    if-nez v2, :cond_a

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem2dTo3d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/S3dItem1;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dLr:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/S3dItem1;->requestFocus()Z

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dLr:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v2, v3}, Lcom/konka/tvsettings/view/S3dItem1;->playSoundEffect(I)V

    goto/16 :goto_1

    :cond_a
    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItemEngine:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->isFocusable()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dAuto:Lcom/konka/tvsettings/view/PictureSettingItem1;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/PictureSettingItem1;->isFocusable()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dClar:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/S3dItem1;->isFocusable()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem2dTo3d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/S3dItem1;->isFocusable()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTo2d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/S3dItem1;->isFocusable()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dOff:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/S3dItem1;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dLr:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/S3dItem1;->requestFocus()Z

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dLr:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v2, v3}, Lcom/konka/tvsettings/view/S3dItem1;->playSoundEffect(I)V

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x13 -> :sswitch_3
        0x14 -> :sswitch_4
        0x52 -> :sswitch_2
        0xb2 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->pauseMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resumeMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onResume()V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resetMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onUserInteraction()V

    return-void
.end method

.method public refreshStatusBgByRunningItem(I)V
    .locals 7
    .param p1    # I

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "refresh status bg pos:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getInstance()Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tv/TvCommonManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    invoke-static {}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource()[I

    move-result-object v1

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v2

    aget v1, v1, v2

    iget-boolean v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->is3DClar:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dAuto:Lcom/konka/tvsettings/view/PictureSettingItem1;

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/PictureSettingItem1;->setStatusFbd()V

    :cond_1
    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dLr:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-direct {p0, v1}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->setBgByStatus(Lcom/konka/tvsettings/view/S3dItem1;)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTb:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-direct {p0, v1}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->setBgByStatus(Lcom/konka/tvsettings/view/S3dItem1;)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dOff:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-direct {p0, v1}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->setBgByStatus(Lcom/konka/tvsettings/view/S3dItem1;)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTo2d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-direct {p0, v1}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->setBgByStatus(Lcom/konka/tvsettings/view/S3dItem1;)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem2dTo3d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-direct {p0, v1}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->setBgByStatus(Lcom/konka/tvsettings/view/S3dItem1;)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dClar:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-direct {p0, v1}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->setBgByStatus(Lcom/konka/tvsettings/view/S3dItem1;)V

    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dLr:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v6}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTb:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v5}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dOff:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v4}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTo2d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v4}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem2dTo3d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v3}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dClar:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v3}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dAuto:Lcom/konka/tvsettings/view/PictureSettingItem1;

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/PictureSettingItem1;->setStatusFbd()V

    invoke-direct {p0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->setItemEngineNor()V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dLr:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v5}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTb:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v6}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dOff:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v4}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTo2d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v4}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem2dTo3d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v3}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dClar:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v3}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dAuto:Lcom/konka/tvsettings/view/PictureSettingItem1;

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/PictureSettingItem1;->setStatusFbd()V

    invoke-direct {p0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->setItemEngineNor()V

    goto/16 :goto_0

    :pswitch_2
    iget-boolean v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->is3DClar:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dLr:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v3}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTb:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v3}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dOff:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v3}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTo2d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v6}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem2dTo3d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v3}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dClar:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v5}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dAuto:Lcom/konka/tvsettings/view/PictureSettingItem1;

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/PictureSettingItem1;->setStatusFbd()V

    :goto_1
    invoke-direct {p0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->setItemEngineFbd()V

    goto/16 :goto_0

    :cond_2
    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dLr:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v5}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTb:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v4}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dOff:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v4}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTo2d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v6}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem2dTo3d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v3}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dClar:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v3}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dAuto:Lcom/konka/tvsettings/view/PictureSettingItem1;

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/PictureSettingItem1;->setStatusFbd()V

    goto :goto_1

    :pswitch_3
    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dLr:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v3}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTb:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v3}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dOff:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v5}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTo2d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v3}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem2dTo3d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v6}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dClar:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v3}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dAuto:Lcom/konka/tvsettings/view/PictureSettingItem1;

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/PictureSettingItem1;->setStatusNor()V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dAuto:Lcom/konka/tvsettings/view/PictureSettingItem1;

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/PictureSettingItem1;->setStatusFbd()V

    invoke-direct {p0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->setItemEngineNor()V

    goto/16 :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dLr:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v3}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTb:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v3}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dOff:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v3}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTo2d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v5}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem2dTo3d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v3}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dClar:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v6}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    invoke-direct {p0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->setItemEngineNor()V

    goto/16 :goto_0

    :pswitch_5
    iget-boolean v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->is3DClar:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dLr:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v5}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTb:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v4}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dOff:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v6}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTo2d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v3}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem2dTo3d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v4}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dClar:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v3}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dAuto:Lcom/konka/tvsettings/view/PictureSettingItem1;

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/PictureSettingItem1;->setStatusFbd()V

    invoke-direct {p0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->setItemEngineFbd()V

    goto/16 :goto_0

    :cond_3
    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dLr:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v5}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTb:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v4}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dOff:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v6}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dTo2d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v3}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem2dTo3d:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v4}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dClar:Lcom/konka/tvsettings/view/S3dItem1;

    invoke-virtual {v1, v3}, Lcom/konka/tvsettings/view/S3dItem1;->setStatus(I)V

    invoke-direct {p0}, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->setItemEngineFbd()V

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dAuto:Lcom/konka/tvsettings/view/PictureSettingItem1;

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/PictureSettingItem1;->isFocused()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dSettingActivity;->mItem3dAuto:Lcom/konka/tvsettings/view/PictureSettingItem1;

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/PictureSettingItem1;->setStatusNor()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0701cd
        :pswitch_0    # com.konka.tvsettings.R.id.s3d_settings_3dlr
        :pswitch_1    # com.konka.tvsettings.R.id.s3d_settings_3dud
        :pswitch_5    # com.konka.tvsettings.R.id.s3d_settings_3doff
        :pswitch_2    # com.konka.tvsettings.R.id.s3d_settings_3dto2d
        :pswitch_3    # com.konka.tvsettings.R.id.s3d_settings_2dto3d
        :pswitch_4    # com.konka.tvsettings.R.id.s3d_settings_3dclar
    .end packed-switch
.end method
