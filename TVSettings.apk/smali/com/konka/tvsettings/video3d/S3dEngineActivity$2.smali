.class Lcom/konka/tvsettings/video3d/S3dEngineActivity$2;
.super Ljava/lang/Object;
.source "S3dEngineActivity.java"

# interfaces
.implements Lcom/konka/tvsettings/view/IUpdateSysData;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/video3d/S3dEngineActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/video3d/S3dEngineActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/video3d/S3dEngineActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity$2;->this$0:Lcom/konka/tvsettings/video3d/S3dEngineActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public doUpdate()V
    .locals 7

    const/16 v6, 0x18

    const/16 v5, 0x12

    const/16 v4, 0xc

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity$2;->this$0:Lcom/konka/tvsettings/video3d/S3dEngineActivity;

    # getter for: Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DEffect:Lcom/konka/tvsettings/view/PictureSettingItem1;
    invoke-static {v1}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->access$0(Lcom/konka/tvsettings/video3d/S3dEngineActivity;)Lcom/konka/tvsettings/view/PictureSettingItem1;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/PictureSettingItem1;->getCurrentState()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    const-string v2, "SetGainAndOffsetMode_Normal"

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosCommonCommand(Ljava/lang/String;)[S
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity$2;->this$0:Lcom/konka/tvsettings/video3d/S3dEngineActivity;

    # getter for: Lcom/konka/tvsettings/video3d/S3dEngineActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->access$1(Lcom/konka/tvsettings/video3d/S3dEngineActivity;)Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v1

    invoke-interface {v1, v5}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DDepthMode(I)Z

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity$2;->this$0:Lcom/konka/tvsettings/video3d/S3dEngineActivity;

    # getter for: Lcom/konka/tvsettings/video3d/S3dEngineActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->access$1(Lcom/konka/tvsettings/video3d/S3dEngineActivity;)Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v1

    invoke-interface {v1, v5}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DOffsetMode(I)Z

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity$2;->this$0:Lcom/konka/tvsettings/video3d/S3dEngineActivity;

    invoke-virtual {v1, v3}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->showItemDepthOffset(Z)V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :pswitch_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    const-string v2, "SetGainAndOffsetMode_Strong"

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosCommonCommand(Ljava/lang/String;)[S
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_2
    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity$2;->this$0:Lcom/konka/tvsettings/video3d/S3dEngineActivity;

    # getter for: Lcom/konka/tvsettings/video3d/S3dEngineActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->access$1(Lcom/konka/tvsettings/video3d/S3dEngineActivity;)Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v1

    invoke-interface {v1, v6}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DDepthMode(I)Z

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity$2;->this$0:Lcom/konka/tvsettings/video3d/S3dEngineActivity;

    # getter for: Lcom/konka/tvsettings/video3d/S3dEngineActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->access$1(Lcom/konka/tvsettings/video3d/S3dEngineActivity;)Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v1

    invoke-interface {v1, v6}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DOffsetMode(I)Z

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity$2;->this$0:Lcom/konka/tvsettings/video3d/S3dEngineActivity;

    invoke-virtual {v1, v3}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->showItemDepthOffset(Z)V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2

    :pswitch_2
    :try_start_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    const-string v2, "SetGainAndOffsetMode_Weak"

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosCommonCommand(Ljava/lang/String;)[S
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_2
    :goto_3
    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity$2;->this$0:Lcom/konka/tvsettings/video3d/S3dEngineActivity;

    # getter for: Lcom/konka/tvsettings/video3d/S3dEngineActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->access$1(Lcom/konka/tvsettings/video3d/S3dEngineActivity;)Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v1

    invoke-interface {v1, v4}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DDepthMode(I)Z

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity$2;->this$0:Lcom/konka/tvsettings/video3d/S3dEngineActivity;

    # getter for: Lcom/konka/tvsettings/video3d/S3dEngineActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->access$1(Lcom/konka/tvsettings/video3d/S3dEngineActivity;)Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v1

    invoke-interface {v1, v4}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DOffsetMode(I)Z

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity$2;->this$0:Lcom/konka/tvsettings/video3d/S3dEngineActivity;

    invoke-virtual {v1, v3}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->showItemDepthOffset(Z)V

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_3

    :pswitch_3
    :try_start_3
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    const-string v2, "SetGainAndOffsetMode_User"

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosCommonCommand(Ljava/lang/String;)[S
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_3

    :cond_3
    :goto_4
    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity$2;->this$0:Lcom/konka/tvsettings/video3d/S3dEngineActivity;

    # getter for: Lcom/konka/tvsettings/video3d/S3dEngineActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->access$1(Lcom/konka/tvsettings/video3d/S3dEngineActivity;)Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity$2;->this$0:Lcom/konka/tvsettings/video3d/S3dEngineActivity;

    # getter for: Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DDepth:Lcom/konka/tvsettings/view/PictureSettingItem2;
    invoke-static {v2}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->access$2(Lcom/konka/tvsettings/video3d/S3dEngineActivity;)Lcom/konka/tvsettings/view/PictureSettingItem2;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/PictureSettingItem2;->getCurrentValue()I

    move-result v2

    mul-int/lit8 v2, v2, 0x3

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DDepthMode(I)Z

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity$2;->this$0:Lcom/konka/tvsettings/video3d/S3dEngineActivity;

    # getter for: Lcom/konka/tvsettings/video3d/S3dEngineActivity;->ms3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->access$1(Lcom/konka/tvsettings/video3d/S3dEngineActivity;)Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity$2;->this$0:Lcom/konka/tvsettings/video3d/S3dEngineActivity;

    # getter for: Lcom/konka/tvsettings/video3d/S3dEngineActivity;->mItem3DOffset:Lcom/konka/tvsettings/view/PictureSettingItem2;
    invoke-static {v2}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->access$3(Lcom/konka/tvsettings/video3d/S3dEngineActivity;)Lcom/konka/tvsettings/view/PictureSettingItem2;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/PictureSettingItem2;->getCurrentValue()I

    move-result v2

    mul-int/lit8 v2, v2, 0x3

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/S3DDesk;->set3DOffsetMode(I)Z

    iget-object v1, p0, Lcom/konka/tvsettings/video3d/S3dEngineActivity$2;->this$0:Lcom/konka/tvsettings/video3d/S3dEngineActivity;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/konka/tvsettings/video3d/S3dEngineActivity;->showItemDepthOffset(Z)V

    goto/16 :goto_0

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
