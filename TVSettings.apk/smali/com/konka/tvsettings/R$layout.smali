.class public final Lcom/konka/tvsettings/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final activity_ad:I = 0x7f030000

.field public static final ad_list_item:I = 0x7f030001

.field public static final atvmanualtuning:I = 0x7f030002

.field public static final audio_language_list_view:I = 0x7f030003

.field public static final audioonly_menu:I = 0x7f030004

.field public static final autotuning:I = 0x7f030005

.field public static final cec_control_list_item:I = 0x7f030006

.field public static final cec_control_menu:I = 0x7f030007

.field public static final cec_devicelist:I = 0x7f030008

.field public static final cec_deviceslist_item:I = 0x7f030009

.field public static final cec_menu:I = 0x7f03000a

.field public static final channeltuning:I = 0x7f03000b

.field public static final check_parental_pwd:I = 0x7f03000c

.field public static final ci_menu:I = 0x7f03000d

.field public static final ci_singlestring:I = 0x7f03000e

.field public static final cimmi_list_view:I = 0x7f03000f

.field public static final cimmi_pwd_dialog:I = 0x7f030010

.field public static final colorwheel_menu:I = 0x7f030011

.field public static final dtv_signal_information_menu:I = 0x7f030012

.field public static final dtvmanualtuning:I = 0x7f030013

.field public static final elc_page_tag:I = 0x7f030014

.field public static final equalizer_dialog:I = 0x7f030015

.field public static final exittuninginfo_dialog:I = 0x7f030016

.field public static final function_adv_setting:I = 0x7f030017

.field public static final function_menu:I = 0x7f030018

.field public static final hint_popup:I = 0x7f030019

.field public static final image_text_item:I = 0x7f03001a

.field public static final input_menu:I = 0x7f03001b

.field public static final installation_guide_menu:I = 0x7f03001c

.field public static final lock_menu_setting:I = 0x7f03001d

.field public static final main:I = 0x7f03001e

.field public static final mts_info_list_view_item:I = 0x7f03001f

.field public static final mtsinfo:I = 0x7f030020

.field public static final osd_language_list:I = 0x7f030021

.field public static final osd_language_list_item:I = 0x7f030022

.field public static final osd_setting_menu:I = 0x7f030023

.field public static final pic_adjust_item:I = 0x7f030024

.field public static final pic_mod_item:I = 0x7f030025

.field public static final picture_setting_adv_setting:I = 0x7f030026

.field public static final picture_setting_item1:I = 0x7f030027

.field public static final picture_setting_item2:I = 0x7f030028

.field public static final picture_setting_menu:I = 0x7f030029

.field public static final picture_setting_pc_adj_menu:I = 0x7f03002a

.field public static final popup_channel_info_menu:I = 0x7f03002b

.field public static final popup_img_item:I = 0x7f03002c

.field public static final popup_input_menu:I = 0x7f03002d

.field public static final popup_intell_control_menu:I = 0x7f03002e

.field public static final popup_no_signal_prompt_menu:I = 0x7f03002f

.field public static final popup_program_menu:I = 0x7f030030

.field public static final popup_source_info_menu:I = 0x7f030031

.field public static final popwin_eventinfo:I = 0x7f030032

.field public static final print_screen:I = 0x7f030033

.field public static final print_screen_preview:I = 0x7f030034

.field public static final program_dialog_edit_text:I = 0x7f030035

.field public static final program_favorite_list:I = 0x7f030036

.field public static final program_favorite_list_item:I = 0x7f030037

.field public static final program_list_view:I = 0x7f030038

.field public static final program_list_view_item:I = 0x7f030039

.field public static final program_lock_list_view:I = 0x7f03003a

.field public static final program_menu:I = 0x7f03003b

.field public static final pvr_file_system:I = 0x7f03003c

.field public static final pvr_full_page_browser:I = 0x7f03003d

.field public static final pvr_listview_item:I = 0x7f03003e

.field public static final pvr_menu:I = 0x7f03003f

.field public static final pvr_menu_dialog:I = 0x7f030040

.field public static final pvr_menu_info_list_view_item:I = 0x7f030041

.field public static final pvr_option:I = 0x7f030042

.field public static final reset_usersetting_dialog:I = 0x7f030043

.field public static final root:I = 0x7f030044

.field public static final s3d_engine_menu:I = 0x7f030045

.field public static final s3d_item1:I = 0x7f030046

.field public static final s3d_setting_menu:I = 0x7f030047

.field public static final screensaver_menu:I = 0x7f030048

.field public static final set_parental_pwd:I = 0x7f030049

.field public static final short_cut_view:I = 0x7f03004a

.field public static final smart_toast:I = 0x7f03004b

.field public static final sound_menu:I = 0x7f03004c

.field public static final sound_srs_menu:I = 0x7f03004d

.field public static final statecolumn:I = 0x7f03004e

.field public static final subtitle_language:I = 0x7f03004f

.field public static final time_clock_menu:I = 0x7f030050

.field public static final time_menu:I = 0x7f030051

.field public static final time_offtime:I = 0x7f030052

.field public static final time_ontime:I = 0x7f030053

.field public static final tip:I = 0x7f030054

.field public static final toast_warning:I = 0x7f030055

.field public static final translucent_enter:I = 0x7f030056

.field public static final translucent_exit:I = 0x7f030057

.field public static final usb_driver_item:I = 0x7f030058

.field public static final usb_driver_selecter:I = 0x7f030059

.field public static final video3d_keyvalue_item:I = 0x7f03005a

.field public static final video3d_main_menu:I = 0x7f03005b

.field public static final video3d_tick_item:I = 0x7f03005c

.field public static final virtual_pad_channel_btn:I = 0x7f03005d

.field public static final virtual_pad_divider:I = 0x7f03005e

.field public static final virtual_pad_edit:I = 0x7f03005f

.field public static final virtual_pad_main:I = 0x7f030060

.field public static final virtual_pad_volume_btn:I = 0x7f030061

.field public static final voide3d_engine_menu:I = 0x7f030062


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
