.class public Lcom/konka/tvsettings/MovingPanel;
.super Ljava/lang/Object;
.source "MovingPanel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/MovingPanel$AsynMove;
    }
.end annotation


# instance fields
.field private ITEM_COUNTS:I

.field private ITEM_WIDTH:I

.field private MOVE_SPEED:I

.field private bIsReadytoMove:Z

.field private iCurFocusItemID:I

.field private iDestLeftMargin:I

.field private iMoveLength:I

.field private mContext:Landroid/app/Activity;

.field private movingAdapter:Lcom/konka/tvsettings/ImageTextAdapter;

.field private panelContainer:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;I)V
    .locals 6
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;
    .param p3    # I

    const/16 v5, 0x64

    const/16 v4, 0x10

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x10e

    iput v0, p0, Lcom/konka/tvsettings/MovingPanel;->ITEM_WIDTH:I

    iput v4, p0, Lcom/konka/tvsettings/MovingPanel;->MOVE_SPEED:I

    const/4 v0, 0x6

    iput v0, p0, Lcom/konka/tvsettings/MovingPanel;->ITEM_COUNTS:I

    iput-boolean v3, p0, Lcom/konka/tvsettings/MovingPanel;->bIsReadytoMove:Z

    iput v2, p0, Lcom/konka/tvsettings/MovingPanel;->iDestLeftMargin:I

    iput v2, p0, Lcom/konka/tvsettings/MovingPanel;->iMoveLength:I

    iput v2, p0, Lcom/konka/tvsettings/MovingPanel;->iCurFocusItemID:I

    iput-object p1, p0, Lcom/konka/tvsettings/MovingPanel;->mContext:Landroid/app/Activity;

    iget-object v0, p0, Lcom/konka/tvsettings/MovingPanel;->mContext:Landroid/app/Activity;

    const v1, 0x7f0700de    # com.konka.tvsettings.R.id.layout_movingpanel

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/MovingPanel;->panelContainer:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/MovingPanel;->mContext:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090002    # com.konka.tvsettings.R.dimen.MainSettingItem_Width

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/konka/tvsettings/MovingPanel;->ITEM_WIDTH:I

    iput v4, p0, Lcom/konka/tvsettings/MovingPanel;->MOVE_SPEED:I

    iput p3, p0, Lcom/konka/tvsettings/MovingPanel;->ITEM_COUNTS:I

    iget v0, p0, Lcom/konka/tvsettings/MovingPanel;->MOVE_SPEED:I

    if-le v0, v5, :cond_0

    iput v5, p0, Lcom/konka/tvsettings/MovingPanel;->MOVE_SPEED:I

    :cond_0
    iput v2, p0, Lcom/konka/tvsettings/MovingPanel;->iDestLeftMargin:I

    iput v3, p0, Lcom/konka/tvsettings/MovingPanel;->iCurFocusItemID:I

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/MovingPanel;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/tvsettings/MovingPanel;->bIsReadytoMove:Z

    return-void
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/MovingPanel;)Lcom/konka/tvsettings/ImageTextAdapter;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/MovingPanel;->movingAdapter:Lcom/konka/tvsettings/ImageTextAdapter;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/MovingPanel;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/MovingPanel;->iCurFocusItemID:I

    return v0
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/MovingPanel;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/MovingPanel;->iMoveLength:I

    return v0
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/MovingPanel;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/MovingPanel;->MOVE_SPEED:I

    return v0
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/MovingPanel;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/MovingPanel;->panelContainer:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/tvsettings/MovingPanel;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/MovingPanel;->iDestLeftMargin:I

    return v0
.end method


# virtual methods
.method public focusToPostion(I)V
    .locals 7
    .param p1    # I

    const/4 v6, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "move left to the id===="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " The curFocusId=="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/konka/tvsettings/MovingPanel;->iCurFocusItemID:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/konka/tvsettings/MovingPanel;->bIsReadytoMove:Z

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/konka/tvsettings/MovingPanel;->iCurFocusItemID:I

    if-eq p1, v3, :cond_0

    iget v3, p0, Lcom/konka/tvsettings/MovingPanel;->ITEM_COUNTS:I

    if-ge p1, v3, :cond_0

    if-ltz p1, :cond_0

    iput-boolean v6, p0, Lcom/konka/tvsettings/MovingPanel;->bIsReadytoMove:Z

    iget v3, p0, Lcom/konka/tvsettings/MovingPanel;->iCurFocusItemID:I

    sub-int v1, v3, p1

    iget v3, p0, Lcom/konka/tvsettings/MovingPanel;->ITEM_WIDTH:I

    mul-int/2addr v3, v1

    iput v3, p0, Lcom/konka/tvsettings/MovingPanel;->iMoveLength:I

    iget-object v3, p0, Lcom/konka/tvsettings/MovingPanel;->panelContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    iget v3, v2, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    iget v4, p0, Lcom/konka/tvsettings/MovingPanel;->iMoveLength:I

    add-int/2addr v3, v4

    iput v3, p0, Lcom/konka/tvsettings/MovingPanel;->iDestLeftMargin:I

    iget v3, p0, Lcom/konka/tvsettings/MovingPanel;->MOVE_SPEED:I

    mul-int v0, v3, v1

    new-instance v3, Lcom/konka/tvsettings/MovingPanel$AsynMove;

    invoke-direct {v3, p0}, Lcom/konka/tvsettings/MovingPanel$AsynMove;-><init>(Lcom/konka/tvsettings/MovingPanel;)V

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Integer;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v3, v4}, Lcom/konka/tvsettings/MovingPanel$AsynMove;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    iput p1, p0, Lcom/konka/tvsettings/MovingPanel;->iCurFocusItemID:I

    :cond_0
    return-void
.end method

.method public getFocusID()I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/MovingPanel;->iCurFocusItemID:I

    return v0
.end method

.method public isReadytoMove()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/MovingPanel;->bIsReadytoMove:Z

    return v0
.end method

.method public moveLeftOneStep()V
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/MovingPanel;->iCurFocusItemID:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/MovingPanel;->focusToPostion(I)V

    return-void
.end method

.method public moveLeftSteps(I)V
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/konka/tvsettings/MovingPanel;->iCurFocusItemID:I

    sub-int/2addr v0, p1

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/MovingPanel;->focusToPostion(I)V

    return-void
.end method

.method public moveRightOneStep()V
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/MovingPanel;->iCurFocusItemID:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/MovingPanel;->focusToPostion(I)V

    return-void
.end method

.method public moveRightSteps(I)V
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/konka/tvsettings/MovingPanel;->iCurFocusItemID:I

    add-int/2addr v0, p1

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/MovingPanel;->focusToPostion(I)V

    return-void
.end method

.method public setAdapter(Lcom/konka/tvsettings/ImageTextAdapter;)V
    .locals 0
    .param p1    # Lcom/konka/tvsettings/ImageTextAdapter;

    iput-object p1, p0, Lcom/konka/tvsettings/MovingPanel;->movingAdapter:Lcom/konka/tvsettings/ImageTextAdapter;

    return-void
.end method
