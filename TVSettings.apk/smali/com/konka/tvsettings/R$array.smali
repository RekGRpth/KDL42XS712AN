.class public final Lcom/konka/tvsettings/R$array;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "array"
.end annotation


# static fields
.field public static final arr_searching_signalquality_mode:I = 0x7f0b0000

.field public static final dynamicbl_mod_select:I = 0x7f0b0040

.field public static final inputsource_array:I = 0x7f0b0025

.field public static final str_arr_Australian_parentalguidance__vals:I = 0x7f0b003b

.field public static final str_arr_antennatype:I = 0x7f0b003d

.field public static final str_arr_autotuning_country:I = 0x7f0b0004

.field public static final str_arr_autotuning_country_snowa:I = 0x7f0b0006

.field public static final str_arr_autotuning_country_xvision:I = 0x7f0b0005

.field public static final str_arr_color_wheel_status:I = 0x7f0b0023

.field public static final str_arr_common_on_and_off:I = 0x7f0b003c

.field public static final str_arr_default_audio_lan_value:I = 0x7f0b003f

.field public static final str_arr_fun_AlwaysTimeShift_vals:I = 0x7f0b0032

.field public static final str_arr_fun_OnorOff_switcher:I = 0x7f0b0033

.field public static final str_arr_fun_audio_lang_vals:I = 0x7f0b0037

.field public static final str_arr_fun_audioonly_vals:I = 0x7f0b0030

.field public static final str_arr_fun_gamemode_vals:I = 0x7f0b002f

.field public static final str_arr_fun_hearingimp_vals:I = 0x7f0b0035

.field public static final str_arr_fun_locksystem_vals:I = 0x7f0b0036

.field public static final str_arr_fun_mhlautoswitch_vals:I = 0x7f0b002e

.field public static final str_arr_fun_rgbrange_vals:I = 0x7f0b002c

.field public static final str_arr_fun_screensaver_vals:I = 0x7f0b002b

.field public static final str_arr_fun_ssu_vals:I = 0x7f0b002d

.field public static final str_arr_fun_ttxlanguage_vals:I = 0x7f0b0034

.field public static final str_arr_mfc_status:I = 0x7f0b0041

.field public static final str_arr_osdlanguage_vals:I = 0x7f0b0029

.field public static final str_arr_osdtime_vals:I = 0x7f0b002a

.field public static final str_arr_parentalguidance_vals:I = 0x7f0b0039

.field public static final str_arr_pro_lcnarrange_vals:I = 0x7f0b0031

.field public static final str_arr_program_city:I = 0x7f0b0002

.field public static final str_arr_program_modulationmode:I = 0x7f0b0003

.field public static final str_arr_program_searchtype_values:I = 0x7f0b0001

.field public static final str_arr_sound_audio_choice_vals:I = 0x7f0b0011

.field public static final str_arr_sound_audio_language_vals:I = 0x7f0b0010

.field public static final str_arr_sound_hdmi_vals:I = 0x7f0b0012

.field public static final str_arr_sound_mode_vals:I = 0x7f0b000b

.field public static final str_arr_sound_spdifoutput_vals:I = 0x7f0b000f

.field public static final str_arr_sound_srscomvals:I = 0x7f0b000c

.field public static final str_arr_sound_surround_vals:I = 0x7f0b000d

.field public static final str_arr_sound_voice_ctrl_vals:I = 0x7f0b000e

.field public static final str_arr_time_ontimeswitch_vals:I = 0x7f0b0026

.field public static final str_arr_time_sleeptimer_vals:I = 0x7f0b0027

.field public static final str_arr_timezone_vals:I = 0x7f0b0028

.field public static final str_arr_v3d_effect_vals:I = 0x7f0b0020

.field public static final str_arr_v3d_sequence_vals:I = 0x7f0b0021

.field public static final str_arr_v3d_switch_vals:I = 0x7f0b0022

.field public static final str_array_atv_colorformat:I = 0x7f0b0009

.field public static final str_array_atv_soundformat:I = 0x7f0b000a

.field public static final str_pic_setting_arr_col_skin:I = 0x7f0b0013

.field public static final str_pic_setting_arr_color_temp:I = 0x7f0b0018

.field public static final str_pic_setting_arr_denoise:I = 0x7f0b0015

.field public static final str_pic_setting_arr_dynamic_denoise:I = 0x7f0b0017

.field public static final str_pic_setting_arr_pic_mod:I = 0x7f0b001f

.field public static final str_pic_setting_arr_skin_adjust:I = 0x7f0b0016

.field public static final str_pic_setting_open_and_close:I = 0x7f0b0014

.field public static final str_pic_setting_osd_mod_hdmi:I = 0x7f0b001d

.field public static final str_pic_setting_osd_mod_hdmi_interlaced:I = 0x7f0b001c

.field public static final str_pic_setting_osd_mod_tv:I = 0x7f0b0019

.field public static final str_pic_setting_osd_mod_usb:I = 0x7f0b001e

.field public static final str_pic_setting_osd_mod_vga:I = 0x7f0b001b

.field public static final str_pic_setting_osd_mod_ypbpr:I = 0x7f0b001a

.field public static final str_popup_state_open_close:I = 0x7f0b0024

.field public static final str_pvr_error_state:I = 0x7f0b003a

.field public static final str_time_setting_mode_option:I = 0x7f0b0038

.field public static final strs_cha_dtvautotuning_full:I = 0x7f0b0008

.field public static final strs_cha_dtvautotuning_full_or_net:I = 0x7f0b0007

.field public static final tv_options:I = 0x7f0b003e


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
