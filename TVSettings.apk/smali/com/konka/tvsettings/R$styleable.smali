.class public final Lcom/konka/tvsettings/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final Gallery:[I

.field public static final Gallery_android_galleryItemBackground:I = 0x0

.field public static final TimeChooser:[I

.field public static final TimeChooser_chooseType:I = 0x1

.field public static final TimeChooser_hourLimit:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x101004c    # android.R.attr.galleryItemBackground

    aput v2, v0, v1

    sput-object v0, Lcom/konka/tvsettings/R$styleable;->Gallery:[I

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/konka/tvsettings/R$styleable;->TimeChooser:[I

    return-void

    :array_0
    .array-data 4
        0x7f010000    # com.konka.tvsettings.R.attr.hourLimit
        0x7f010001    # com.konka.tvsettings.R.attr.chooseType
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
