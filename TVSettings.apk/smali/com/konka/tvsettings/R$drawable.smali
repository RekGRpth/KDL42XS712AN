.class public final Lcom/konka/tvsettings/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final app_icon_tv:I = 0x7f020000

.field public static final arrows_nor_left:I = 0x7f020001

.field public static final arrows_nor_right:I = 0x7f020002

.field public static final arrows_sel_left:I = 0x7f020003

.field public static final arrows_sel_right:I = 0x7f020004

.field public static final auto_option_left_arrow_state:I = 0x7f020005

.field public static final auto_option_right_arrow_state:I = 0x7f020006

.field public static final button_ok:I = 0x7f020007

.field public static final button_state:I = 0x7f020008

.field public static final channel_add_selector:I = 0x7f020009

.field public static final channel_sub_selector:I = 0x7f02000a

.field public static final ci_item_state:I = 0x7f02000b

.field public static final color_wheel_bg00_0:I = 0x7f02000c

.field public static final color_wheel_bg00_1:I = 0x7f02000d

.field public static final color_wheel_bg01_0:I = 0x7f02000e

.field public static final color_wheel_bg01_1:I = 0x7f02000f

.field public static final color_wheel_bg02_0:I = 0x7f020010

.field public static final color_wheel_bg02_1:I = 0x7f020011

.field public static final color_wheel_bg03_0:I = 0x7f020012

.field public static final color_wheel_bg03_1:I = 0x7f020013

.field public static final color_wheel_bg04_0:I = 0x7f020014

.field public static final color_wheel_bg04_1:I = 0x7f020015

.field public static final color_wheel_bg05_0:I = 0x7f020016

.field public static final color_wheel_bg05_1:I = 0x7f020017

.field public static final color_wheel_bg06_0:I = 0x7f020018

.field public static final color_wheel_bg06_1:I = 0x7f020019

.field public static final color_wheel_bg07_0:I = 0x7f02001a

.field public static final color_wheel_bg07_1:I = 0x7f02001b

.field public static final color_wheel_bg08_0:I = 0x7f02001c

.field public static final color_wheel_bg08_1:I = 0x7f02001d

.field public static final color_wheel_bg09_0:I = 0x7f02001e

.field public static final color_wheel_bg09_1:I = 0x7f02001f

.field public static final color_wheel_bg10_0:I = 0x7f020020

.field public static final color_wheel_bg10_1:I = 0x7f020021

.field public static final color_wheel_bg11_0:I = 0x7f020022

.field public static final color_wheel_bg11_1:I = 0x7f020023

.field public static final color_wheel_bg_left:I = 0x7f020024

.field public static final color_wheel_bg_right:I = 0x7f020025

.field public static final color_wheel_blue_back:I = 0x7f020026

.field public static final color_wheel_blue_progress:I = 0x7f020027

.field public static final color_wheel_frame:I = 0x7f020028

.field public static final color_wheel_green_back:I = 0x7f020029

.field public static final color_wheel_green_progress:I = 0x7f02002a

.field public static final color_wheel_progress_style_blue:I = 0x7f02002b

.field public static final color_wheel_progress_style_green:I = 0x7f02002c

.field public static final color_wheel_progress_style_red:I = 0x7f02002d

.field public static final color_wheel_red_back:I = 0x7f02002e

.field public static final color_wheel_red_progress:I = 0x7f02002f

.field public static final com_bg:I = 0x7f020030

.field public static final com_bg_popmenu1:I = 0x7f020031

.field public static final com_bg_popmenu2:I = 0x7f020032

.field public static final com_bg_popmenu3:I = 0x7f020033

.field public static final com_button_select:I = 0x7f020034

.field public static final com_button_select_s1:I = 0x7f020035

.field public static final com_button_select_s2:I = 0x7f020036

.field public static final com_button_select_s3:I = 0x7f020037

.field public static final com_button_select_uns1:I = 0x7f020038

.field public static final com_button_select_uns2:I = 0x7f020039

.field public static final com_button_select_uns3:I = 0x7f02003a

.field public static final com_icon_bg_s:I = 0x7f02003b

.field public static final com_icon_button_3d_s:I = 0x7f02003c

.field public static final com_icon_button_3d_uns:I = 0x7f02003d

.field public static final com_icon_button_cicard_s:I = 0x7f02003e

.field public static final com_icon_button_cicard_uns:I = 0x7f02003f

.field public static final com_icon_button_function_s:I = 0x7f020040

.field public static final com_icon_button_function_uns:I = 0x7f020041

.field public static final com_icon_button_images_s:I = 0x7f020042

.field public static final com_icon_button_images_uns:I = 0x7f020043

.field public static final com_icon_button_program_s:I = 0x7f020044

.field public static final com_icon_button_program_uns:I = 0x7f020045

.field public static final com_icon_button_sound_s:I = 0x7f020046

.field public static final com_icon_button_sound_uns:I = 0x7f020047

.field public static final com_icon_button_sources_s:I = 0x7f020048

.field public static final com_icon_button_sources_uns:I = 0x7f020049

.field public static final com_icon_button_time_s:I = 0x7f02004a

.field public static final com_icon_button_time_uns:I = 0x7f02004b

.field public static final com_icon_button_weibo_s:I = 0x7f02004c

.field public static final com_icon_button_weibo_uns:I = 0x7f02004d

.field public static final com_menu_bg:I = 0x7f02004e

.field public static final com_progressbar_arrived1:I = 0x7f02004f

.field public static final com_progressbar_arrived_all:I = 0x7f020050

.field public static final com_progressbar_arrived_button_s:I = 0x7f020051

.field public static final com_progressbar_arrived_button_uns:I = 0x7f020052

.field public static final com_select_bar:I = 0x7f020053

.field public static final common_img_arrow_l:I = 0x7f020054

.field public static final common_img_arrow_r:I = 0x7f020055

.field public static final common_img_ok:I = 0x7f020056

.field public static final common_img_pagepoint_disable:I = 0x7f020057

.field public static final common_img_pagepoint_enable:I = 0x7f020058

.field public static final dialog_button_state:I = 0x7f020059

.field public static final dialog_item_img_bg_nor:I = 0x7f02005a

.field public static final division:I = 0x7f02005b

.field public static final epg_info_bg:I = 0x7f02005c

.field public static final exitdialog_button_style:I = 0x7f02005d

.field public static final fav_list_menu_img_bg:I = 0x7f02005e

.field public static final history_statecolumn:I = 0x7f02005f

.field public static final home_default_tv:I = 0x7f020060

.field public static final homepage_statecolumn:I = 0x7f020061

.field public static final icon:I = 0x7f020062

.field public static final icon_tips_blue:I = 0x7f020063

.field public static final icon_tips_green:I = 0x7f020064

.field public static final icon_tips_red:I = 0x7f020065

.field public static final icon_tips_yellow:I = 0x7f020066

.field public static final idle_image_press_status_pvr1:I = 0x7f020067

.field public static final idle_img_press_ststus_bg_m:I = 0x7f020068

.field public static final idle_img_press_ststus_pvr:I = 0x7f020069

.field public static final image_weibo_state:I = 0x7f02006a

.field public static final input_item_foot:I = 0x7f02006b

.field public static final input_item_head:I = 0x7f02006c

.field public static final input_item_nor:I = 0x7f02006d

.field public static final input_item_sel:I = 0x7f02006e

.field public static final input_menu_item_state:I = 0x7f02006f

.field public static final item_left_arrow_state:I = 0x7f020070

.field public static final item_right_arrow_state:I = 0x7f020071

.field public static final item_weibo_state:I = 0x7f020072

.field public static final keypad_statecolumn:I = 0x7f020073

.field public static final list_menu_img_atv_foucus:I = 0x7f020074

.field public static final list_menu_img_bg:I = 0x7f020075

.field public static final list_menu_img_dtv_foucus:I = 0x7f020076

.field public static final list_menu_img_favorite_focus:I = 0x7f020077

.field public static final list_menu_img_hint_b:I = 0x7f020078

.field public static final list_menu_img_hint_g:I = 0x7f020079

.field public static final list_menu_img_hint_lb:I = 0x7f02007a

.field public static final list_menu_img_hint_m:I = 0x7f02007b

.field public static final list_menu_img_hint_r:I = 0x7f02007c

.field public static final list_menu_img_hint_y:I = 0x7f02007d

.field public static final list_menu_img_lock_focus:I = 0x7f02007e

.field public static final list_menu_img_radio_foucus:I = 0x7f02007f

.field public static final list_menu_img_skip_focus:I = 0x7f020080

.field public static final list_menu_img_ssl_focus:I = 0x7f020081

.field public static final loopab_progressbar_style:I = 0x7f020082

.field public static final mts_audio_aac:I = 0x7f020083

.field public static final mts_audio_dolby:I = 0x7f020084

.field public static final mts_audio_dolbyp:I = 0x7f020085

.field public static final mts_audio_left:I = 0x7f020086

.field public static final mts_audio_mpeg:I = 0x7f020087

.field public static final mts_audio_right:I = 0x7f020088

.field public static final mts_audio_text:I = 0x7f020089

.field public static final mts_audio_vi:I = 0x7f02008a

.field public static final mts_img_icon_aac_focus:I = 0x7f02008b

.field public static final mts_img_icon_aac_unfocus:I = 0x7f02008c

.field public static final mts_img_icon_dolby_focus:I = 0x7f02008d

.field public static final mts_img_icon_dolby_unfocus:I = 0x7f02008e

.field public static final mts_img_icon_dolbyp_focus:I = 0x7f02008f

.field public static final mts_img_icon_dolbyp_unfocus:I = 0x7f020090

.field public static final mts_img_icon_left_focus:I = 0x7f020091

.field public static final mts_img_icon_left_unfocus:I = 0x7f020092

.field public static final mts_img_icon_mpeg_focus:I = 0x7f020093

.field public static final mts_img_icon_mpeg_unfocus:I = 0x7f020094

.field public static final mts_img_icon_right_focus:I = 0x7f020095

.field public static final mts_img_icon_right_unfocus:I = 0x7f020096

.field public static final mts_img_icon_vi_focus:I = 0x7f020097

.field public static final mts_img_icon_vi_unfocus:I = 0x7f020098

.field public static final normal_btn_selector:I = 0x7f020099

.field public static final osd_lang_bg:I = 0x7f02009a

.field public static final osd_lang_icon_s:I = 0x7f02009b

.field public static final osd_lang_icon_uns:I = 0x7f02009c

.field public static final osd_lang_list_body:I = 0x7f02009d

.field public static final osd_lang_list_bottom:I = 0x7f02009e

.field public static final osd_lang_list_head:I = 0x7f02009f

.field public static final pic_adj_item_state:I = 0x7f0200a0

.field public static final pic_adj_seekbar_state:I = 0x7f0200a1

.field public static final pic_item_sel_bg:I = 0x7f0200a2

.field public static final pic_mode_focus_state:I = 0x7f0200a3

.field public static final picture_cha_autotuneoption_bg:I = 0x7f0200a4

.field public static final picture_item_bg_selector:I = 0x7f0200a5

.field public static final picture_mode_img_bar:I = 0x7f0200a6

.field public static final picture_mode_img_bg:I = 0x7f0200a7

.field public static final picture_mode_img_focus:I = 0x7f0200a8

.field public static final picture_serchprogressbar_empty:I = 0x7f0200a9

.field public static final picture_serchprogressbar_solid:I = 0x7f0200aa

.field public static final picture_setting_body_nor:I = 0x7f0200ab

.field public static final picture_setting_body_sel1:I = 0x7f0200ac

.field public static final picture_setting_body_sel2:I = 0x7f0200ad

.field public static final picture_setting_foot:I = 0x7f0200ae

.field public static final picture_setting_head:I = 0x7f0200af

.field public static final picture_setting_item_selector1:I = 0x7f0200b0

.field public static final picture_setting_item_selector2:I = 0x7f0200b1

.field public static final player_backward:I = 0x7f0200b2

.field public static final player_backward_focus:I = 0x7f0200b3

.field public static final player_bg:I = 0x7f0200b4

.field public static final player_capture:I = 0x7f0200b5

.field public static final player_capture_focus:I = 0x7f0200b6

.field public static final player_ff:I = 0x7f0200b7

.field public static final player_ff_disable:I = 0x7f0200b8

.field public static final player_ff_focus:I = 0x7f0200b9

.field public static final player_forward:I = 0x7f0200ba

.field public static final player_forward_focus:I = 0x7f0200bb

.field public static final player_pause:I = 0x7f0200bc

.field public static final player_pause_disable:I = 0x7f0200bd

.field public static final player_pause_focus:I = 0x7f0200be

.field public static final player_play:I = 0x7f0200bf

.field public static final player_play_disable:I = 0x7f0200c0

.field public static final player_play_focus:I = 0x7f0200c1

.field public static final player_recorder:I = 0x7f0200c2

.field public static final player_recorder_disable:I = 0x7f0200c3

.field public static final player_recorder_focus:I = 0x7f0200c4

.field public static final player_rev:I = 0x7f0200c5

.field public static final player_rev_disable:I = 0x7f0200c6

.field public static final player_rev_focus:I = 0x7f0200c7

.field public static final player_slow:I = 0x7f0200c8

.field public static final player_slow_disable:I = 0x7f0200c9

.field public static final player_slow_focus:I = 0x7f0200ca

.field public static final player_stop:I = 0x7f0200cb

.field public static final player_stop_disable:I = 0x7f0200cc

.field public static final player_stop_focus:I = 0x7f0200cd

.field public static final player_time_disable:I = 0x7f0200ce

.field public static final player_time_focus:I = 0x7f0200cf

.field public static final playertime:I = 0x7f0200d0

.field public static final popup_bg:I = 0x7f0200d1

.field public static final popup_buton_focus:I = 0x7f0200d2

.field public static final popup_button_unfocus:I = 0x7f0200d3

.field public static final popup_channel_info_menu_bg:I = 0x7f0200d4

.field public static final popup_icon_exclamation:I = 0x7f0200d5

.field public static final popup_input_menu_bg:I = 0x7f0200d6

.field public static final popup_input_menu_body_bg:I = 0x7f0200d7

.field public static final popup_input_menu_hdmi_run:I = 0x7f0200d8

.field public static final popup_input_menu_hdmi_s:I = 0x7f0200d9

.field public static final popup_input_menu_hdmi_uns:I = 0x7f0200da

.field public static final popup_input_menu_l_bg:I = 0x7f0200db

.field public static final popup_input_menu_r_bg:I = 0x7f0200dc

.field public static final popup_input_menu_tv_run:I = 0x7f0200dd

.field public static final popup_input_menu_tv_s:I = 0x7f0200de

.field public static final popup_input_menu_tv_uns:I = 0x7f0200df

.field public static final popup_input_menu_usb_run:I = 0x7f0200e0

.field public static final popup_input_menu_usb_s:I = 0x7f0200e1

.field public static final popup_input_menu_usb_uns:I = 0x7f0200e2

.field public static final popup_input_menu_vga_run:I = 0x7f0200e3

.field public static final popup_input_menu_vga_s:I = 0x7f0200e4

.field public static final popup_input_menu_vga_uns:I = 0x7f0200e5

.field public static final popup_input_menu_ypbpr_run:I = 0x7f0200e6

.field public static final popup_input_menu_ypbpr_s:I = 0x7f0200e7

.field public static final popup_input_menu_ypbpr_uns:I = 0x7f0200e8

.field public static final popup_intell_control_menu_bg:I = 0x7f0200e9

.field public static final popup_intell_control_menu_deep:I = 0x7f0200ea

.field public static final popup_intell_control_menu_low:I = 0x7f0200eb

.field public static final popup_program_menu_bg_body:I = 0x7f0200ec

.field public static final popup_program_menu_bg_foot:I = 0x7f0200ed

.field public static final popup_program_menu_bg_head:I = 0x7f0200ee

.field public static final popup_program_menu_item_bg:I = 0x7f0200ef

.field public static final popup_source_info_dtv_bg:I = 0x7f0200f0

.field public static final popup_source_info_menu_bg:I = 0x7f0200f1

.field public static final preview_button:I = 0x7f0200f2

.field public static final preview_button_logo:I = 0x7f0200f3

.field public static final preview_button_txt:I = 0x7f0200f4

.field public static final programme_epg_img_focus:I = 0x7f0200f5

.field public static final programme_epg_selcet:I = 0x7f0200f6

.field public static final progressbar:I = 0x7f0200f7

.field public static final pvr_ff_image_selector:I = 0x7f0200f8

.field public static final pvr_pause_image_selector:I = 0x7f0200f9

.field public static final pvr_play_image_selector:I = 0x7f0200fa

.field public static final pvr_recorder_image_selector:I = 0x7f0200fb

.field public static final pvr_rev_image_selector:I = 0x7f0200fc

.field public static final pvr_slow_image_selector:I = 0x7f0200fd

.field public static final pvr_stop_image_selector:I = 0x7f0200fe

.field public static final pvr_time_image_selector:I = 0x7f0200ff

.field public static final radio_bg:I = 0x7f020100

.field public static final record_img_hint_r:I = 0x7f020101

.field public static final s3d_item1_tag:I = 0x7f020102

.field public static final schedule_list_img_time:I = 0x7f020103

.field public static final screen_capture_bg:I = 0x7f020104

.field public static final screen_capture_btn_clk_bg:I = 0x7f020105

.field public static final screen_capture_btn_logo_clk:I = 0x7f020106

.field public static final screen_capture_btn_logo_s:I = 0x7f020107

.field public static final screen_capture_btn_s_bg:I = 0x7f020108

.field public static final screensaver_no_signal_bg:I = 0x7f020109

.field public static final scrubber_primary_holo:I = 0x7f02010a

.field public static final scrubber_track_holo_dark:I = 0x7f02010b

.field public static final search_statecolumn:I = 0x7f02010c

.field public static final seekbar_progress:I = 0x7f02010d

.field public static final seekbar_style:I = 0x7f02010e

.field public static final seekbar_thumb_style:I = 0x7f02010f

.field public static final set_3d_icon_disadled:I = 0x7f020110

.field public static final set_3d_icon_s:I = 0x7f020111

.field public static final set_3d_icon_uns:I = 0x7f020112

.field public static final set_cicard_bgbottom:I = 0x7f020113

.field public static final set_cicard_bgmiddle:I = 0x7f020114

.field public static final set_cicard_bgtop:I = 0x7f020115

.field public static final set_cicard_select_bar:I = 0x7f020116

.field public static final set_program_batton__popmenu_uns:I = 0x7f020117

.field public static final set_program_button__popmenu_s:I = 0x7f020118

.field public static final set_program_button_caf:I = 0x7f020119

.field public static final set_program_button_s:I = 0x7f02011a

.field public static final set_program_button_s1:I = 0x7f02011b

.field public static final set_program_button_uns:I = 0x7f02011c

.field public static final set_program_popmenu_bg:I = 0x7f02011d

.field public static final set_select_bar_images22:I = 0x7f02011e

.field public static final setting_button_style:I = 0x7f02011f

.field public static final setting_menu_item_state:I = 0x7f020120

.field public static final setting_statecolumn:I = 0x7f020121

.field public static final shortcut_icon_3d:I = 0x7f020122

.field public static final shortcut_icon_picture_setting:I = 0x7f020123

.field public static final shortcut_icon_program:I = 0x7f020124

.field public static final shortcut_icon_sound_setting:I = 0x7f020125

.field public static final shortcut_icon_source:I = 0x7f020126

.field public static final smart_toast_msg_bg:I = 0x7f020127

.field public static final symbol_warning:I = 0x7f020128

.field public static final text_color_selector:I = 0x7f020129

.field public static final translucent:I = 0x7f02012a

.field public static final transparent:I = 0x7f02012b

.field public static final tuning_img_hint_exit:I = 0x7f02012c

.field public static final tuning_img_hint_menu:I = 0x7f02012d

.field public static final tv_bj:I = 0x7f02012e

.field public static final tv_button_bj1_s:I = 0x7f02012f

.field public static final tv_button_bj_s:I = 0x7f020130

.field public static final tv_button_bj_uns:I = 0x7f020131

.field public static final tv_button_channel1_s:I = 0x7f020132

.field public static final tv_button_channel21_uns:I = 0x7f020133

.field public static final tv_button_channel2_s:I = 0x7f020134

.field public static final tv_button_channel2_uns:I = 0x7f020135

.field public static final tv_button_channel_s:I = 0x7f020136

.field public static final tv_button_channel_uns:I = 0x7f020137

.field public static final tv_button_delete1_s:I = 0x7f020138

.field public static final tv_button_delete2_s:I = 0x7f020139

.field public static final tv_button_delete_uns:I = 0x7f02013a

.field public static final tv_button_middle:I = 0x7f02013b

.field public static final tv_button_sound1_s:I = 0x7f02013c

.field public static final tv_button_sound21_s:I = 0x7f02013d

.field public static final tv_button_sound2_s:I = 0x7f02013e

.field public static final tv_button_sound2_uns:I = 0x7f02013f

.field public static final tv_button_sound_s:I = 0x7f020140

.field public static final tv_button_sound_uns:I = 0x7f020141

.field public static final tv_icon_home_s:I = 0x7f020142

.field public static final tv_icon_home_uns:I = 0x7f020143

.field public static final tv_icon_recentlyused_s:I = 0x7f020144

.field public static final tv_icon_recentlyused_uns:I = 0x7f020145

.field public static final tv_icon_remote_s:I = 0x7f020146

.field public static final tv_icon_remote_uns:I = 0x7f020147

.field public static final tv_icon_search_s:I = 0x7f020148

.field public static final tv_icon_search_uns:I = 0x7f020149

.field public static final tv_icon_set_s:I = 0x7f02014a

.field public static final tv_icon_set_uns:I = 0x7f02014b

.field public static final tv_icon_usb1_s:I = 0x7f02014c

.field public static final tv_icon_usb1_uns:I = 0x7f02014d

.field public static final tv_icon_usb_s:I = 0x7f02014e

.field public static final tv_icon_usb_uns:I = 0x7f02014f

.field public static final tv_icon_wifi_s:I = 0x7f020150

.field public static final tv_icon_wifi_uns:I = 0x7f020151

.field public static final tv_icon_wired1_s:I = 0x7f020152

.field public static final tv_icon_wired1_uns:I = 0x7f020153

.field public static final tv_icon_wired2_s:I = 0x7f020154

.field public static final tv_icon_wired2_uns:I = 0x7f020155

.field public static final tv_icon_wired_s:I = 0x7f020156

.field public static final tv_icon_wired_uns:I = 0x7f020157

.field public static final tv_icon_wireless1_s:I = 0x7f020158

.field public static final tv_icon_wireless1_uns:I = 0x7f020159

.field public static final tv_icon_wireless2_s:I = 0x7f02015a

.field public static final tv_icon_wireless2_uns:I = 0x7f02015b

.field public static final tv_icon_wireless3_s:I = 0x7f02015c

.field public static final tv_icon_wireless3_uns:I = 0x7f02015d

.field public static final tv_icon_wireless_s:I = 0x7f02015e

.field public static final tv_icon_wireless_uns:I = 0x7f02015f

.field public static final tv_input_bj:I = 0x7f020160

.field public static final tv_line:I = 0x7f020161

.field public static final tv_record_list_pic_bg:I = 0x7f020162

.field public static final tv_record_list_pic_s_bg:I = 0x7f020163

.field public static final tv_record_list_r_bg:I = 0x7f020164

.field public static final tv_record_list_timeline_bg:I = 0x7f020165

.field public static final tv_top_bj1:I = 0x7f020166

.field public static final tv_top_bj2:I = 0x7f020167

.field public static final tvwidgetlogo:I = 0x7f020168

.field public static final usb_drive:I = 0x7f020169

.field public static final usb_statecolumn:I = 0x7f02016a

.field public static final volume_add_selector:I = 0x7f02016b

.field public static final volume_sub_selector:I = 0x7f02016c

.field public static final wifi_statecolumn:I = 0x7f02016d


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
