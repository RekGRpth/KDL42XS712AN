.class public Lcom/konka/tvsettings/TVRootApp;
.super Landroid/app/Application;
.source "TVRootApp.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;,
        Lcom/konka/tvsettings/TVRootApp$MonitorThread;,
        Lcom/konka/tvsettings/TVRootApp$RootBroadCastReceiver;
    }
.end annotation


# static fields
.field public static final SP_CHOOSED_COUNTRY:Ljava/lang/String; = "CHOOSED_COUNTRY"

.field public static final SP_FIRST_RUN_GUIDE:Ljava/lang/String; = "FIRST_RUN_GUIDE"

.field public static final SP_TUNING_COUNTRY:Ljava/lang/String; = "tuningCountry"

.field public static final SP_TVSETTING:Ljava/lang/String; = "TVSETTING"

.field private static monitorThread:Lcom/konka/tvsettings/TVRootApp$MonitorThread;

.field private static textStatus:Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

.field private static tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;


# instance fields
.field databasePath:Ljava/lang/String;

.field private errProgNums:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field protected handler:Landroid/os/Handler;

.field private isPVREnable:Z

.field private isTTXEnable:Z

.field private isToUpdataDB:Z

.field private mainmenuHandler:Lcom/konka/tvsettings/MainMenuActivity$MainMenuHandler;

.field private rootBroadCastReceiver:Lcom/konka/tvsettings/TVRootApp$RootBroadCastReceiver;

.field private rootmenuHandler:Lcom/konka/tvsettings/RootActivity$RootMenuHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/tvsettings/TVRootApp;->monitorThread:Lcom/konka/tvsettings/TVRootApp$MonitorThread;

    sget-object v0, Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;->TEXT_STATUS_NONE:Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

    sput-object v0, Lcom/konka/tvsettings/TVRootApp;->textStatus:Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/TVRootApp;->mainmenuHandler:Lcom/konka/tvsettings/MainMenuActivity$MainMenuHandler;

    iput-object v0, p0, Lcom/konka/tvsettings/TVRootApp;->rootmenuHandler:Lcom/konka/tvsettings/RootActivity$RootMenuHandler;

    iput-object v0, p0, Lcom/konka/tvsettings/TVRootApp;->databasePath:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/TVRootApp;->errProgNums:Ljava/util/ArrayList;

    iput-boolean v1, p0, Lcom/konka/tvsettings/TVRootApp;->isPVREnable:Z

    iput-boolean v1, p0, Lcom/konka/tvsettings/TVRootApp;->isTTXEnable:Z

    new-instance v0, Lcom/konka/tvsettings/TVRootApp$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/TVRootApp$1;-><init>(Lcom/konka/tvsettings/TVRootApp;)V

    iput-object v0, p0, Lcom/konka/tvsettings/TVRootApp;->handler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$0()Lcom/konka/kkinterface/tv/TvDeskProvider;
    .locals 1

    sget-object v0, Lcom/konka/tvsettings/TVRootApp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    return-object v0
.end method

.method private createDatabaseFile(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/konka/tvsettings/TVRootApp;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/high16 v5, 0x7f050000    # com.konka.tvsettings.R.raw.tvsettings

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v3

    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    const/16 v4, 0x2000

    new-array v0, v4, [B

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v3, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    if-gtz v1, :cond_0

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    return-void

    :cond_0
    const/4 v4, 0x0

    invoke-virtual {v2, v0, v4, v1}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_0
.end method

.method public static getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;
    .locals 1

    sget-object v0, Lcom/konka/tvsettings/TVRootApp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    return-object v0
.end method

.method private initConfigData()V
    .locals 9

    const/4 v8, 0x0

    const/4 v2, 0x0

    const/4 v7, 0x1

    invoke-virtual {p0}, Lcom/konka/tvsettings/TVRootApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/snconfig"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PVR_ENABLE"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v7, :cond_1

    move v0, v7

    :goto_0
    iput-boolean v0, p0, Lcom/konka/tvsettings/TVRootApp;->isPVREnable:Z

    const-string v0, "TTX_ENABLE"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v7, :cond_2

    :goto_1
    iput-boolean v7, p0, Lcom/konka/tvsettings/TVRootApp;->isTTXEnable:Z

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :goto_2
    return-void

    :cond_1
    move v0, v8

    goto :goto_0

    :cond_2
    move v7, v8

    goto :goto_1

    :cond_3
    iput-boolean v7, p0, Lcom/konka/tvsettings/TVRootApp;->isPVREnable:Z

    iput-boolean v7, p0, Lcom/konka/tvsettings/TVRootApp;->isTTXEnable:Z

    goto :goto_2
.end method

.method private initLittleDownCounter()V
    .locals 4

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->getInstance()Lcom/konka/tvsettings/common/LittleDownTimer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/tvsettings/common/LittleDownTimer;->start()V

    const/4 v0, 0x0

    sget-object v1, Lcom/konka/tvsettings/TVRootApp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/konka/tvsettings/TVRootApp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/SettingDesk;->getOsdTimeoutSecond()I

    move-result v0

    :cond_0
    const-string v1, "[!!!!!!!!!!!!!!!!!!!!!]time out data problem"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "the unit is ms in db~~"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    const-string v1, "time out data problem"

    const-string v2, "the unit is ms in db"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x5

    :cond_1
    const/16 v1, 0x1e

    if-le v0, v1, :cond_2

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->stopMenu()V

    :goto_0
    return-void

    :cond_2
    invoke-static {v0}, Lcom/konka/tvsettings/common/LittleDownTimer;->setMenuStatus(I)V

    goto :goto_0
.end method

.method private initMyDB()V
    .locals 5

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/konka/tvsettings/TVRootApp;->databasePath:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/tvsettings.db"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    new-instance v1, Ljava/io/File;

    iget-object v3, p0, Lcom/konka/tvsettings/TVRootApp;->databasePath:Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/Exception;

    const-string v4, "create dir fail!"

    invoke-direct {v3, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v2

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "initMyDB Fail!"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-direct {p0, v0}, Lcom/konka/tvsettings/TVRootApp;->createDatabaseFile(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :cond_1
    :try_start_2
    invoke-static {}, Lcom/konka/tvsettings/MyDataBase;->openDB()V

    invoke-static {}, Lcom/konka/tvsettings/MyDataBase;->checkDBVersion()Z

    move-result v3

    if-nez v3, :cond_2

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "checkDBVersion! >>>createDatabaseFile!"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/konka/tvsettings/TVRootApp;->createDatabaseFile(Ljava/lang/String;)V

    :cond_2
    invoke-static {}, Lcom/konka/tvsettings/MyDataBase;->closeDB()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_1
    move-exception v2

    :try_start_3
    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Exception! >>>createDatabaseFile!"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/konka/tvsettings/TVRootApp;->createDatabaseFile(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0
.end method

.method private initTvDeskProvider()V
    .locals 1

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    sput-object v0, Lcom/konka/tvsettings/TVRootApp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    return-void
.end method


# virtual methods
.method public getMainmenuHandler()Lcom/konka/tvsettings/MainMenuActivity$MainMenuHandler;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/TVRootApp;->mainmenuHandler:Lcom/konka/tvsettings/MainMenuActivity$MainMenuHandler;

    return-object v0
.end method

.method public getRootmenuHandler()Lcom/konka/tvsettings/RootActivity$RootMenuHandler;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/TVRootApp;->rootmenuHandler:Lcom/konka/tvsettings/RootActivity$RootMenuHandler;

    return-object v0
.end method

.method public getTvTextMode()Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;
    .locals 1

    sget-object v0, Lcom/konka/tvsettings/TVRootApp;->textStatus:Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

    return-object v0
.end method

.method public isPVREnable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/TVRootApp;->isPVREnable:Z

    return v0
.end method

.method public isTTXEnable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/TVRootApp;->isTTXEnable:Z

    return v0
.end method

.method public isToUpdataDB()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/TVRootApp;->isToUpdataDB:Z

    return v0
.end method

.method public onCreate()V
    .locals 4

    invoke-direct {p0}, Lcom/konka/tvsettings/TVRootApp;->initTvDeskProvider()V

    invoke-direct {p0}, Lcom/konka/tvsettings/TVRootApp;->initLittleDownCounter()V

    const-string v2, "Create the TVRootApp for TVSettings"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const-string v2, "all connection is over"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/konka/tvsettings/TVRootApp;->initConfigData()V

    new-instance v2, Lcom/konka/tvsettings/TimeZoneSetter;

    invoke-direct {v2, p0}, Lcom/konka/tvsettings/TimeZoneSetter;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Lcom/konka/tvsettings/TimeZoneSetter;->updateTimeZone()V

    new-instance v2, Lcom/konka/tvsettings/TVRootApp$RootBroadCastReceiver;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/konka/tvsettings/TVRootApp$RootBroadCastReceiver;-><init>(Lcom/konka/tvsettings/TVRootApp;Lcom/konka/tvsettings/TVRootApp$RootBroadCastReceiver;)V

    iput-object v2, p0, Lcom/konka/tvsettings/TVRootApp;->rootBroadCastReceiver:Lcom/konka/tvsettings/TVRootApp$RootBroadCastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "com.mstar.tv.service.COMMON_EVENT_SIGNAL_STATUS_UPDATE"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/tvsettings/TVRootApp;->rootBroadCastReceiver:Lcom/konka/tvsettings/TVRootApp$RootBroadCastReceiver;

    invoke-virtual {p0, v2, v1}, Lcom/konka/tvsettings/TVRootApp;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "/data/data/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/TVRootApp;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/databases"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/tvsettings/TVRootApp;->databasePath:Ljava/lang/String;

    iget-object v2, p0, Lcom/konka/tvsettings/TVRootApp;->databasePath:Ljava/lang/String;

    invoke-static {v2}, Lcom/konka/tvsettings/MyDataBase;->setDBFileDir(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/konka/tvsettings/TVRootApp;->initMyDB()V

    invoke-static {}, Lcom/konka/tvsettings/MyDataBase;->openDB()V

    sget-object v2, Lcom/konka/tvsettings/TVRootApp;->monitorThread:Lcom/konka/tvsettings/TVRootApp$MonitorThread;

    if-nez v2, :cond_0

    const-string v2, "TVRootApp"

    const-string v3, "MonitorThread"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Lcom/konka/tvsettings/TVRootApp$MonitorThread;

    invoke-direct {v2, p0}, Lcom/konka/tvsettings/TVRootApp$MonitorThread;-><init>(Lcom/konka/tvsettings/TVRootApp;)V

    sput-object v2, Lcom/konka/tvsettings/TVRootApp;->monitorThread:Lcom/konka/tvsettings/TVRootApp$MonitorThread;

    sget-object v2, Lcom/konka/tvsettings/TVRootApp;->monitorThread:Lcom/konka/tvsettings/TVRootApp$MonitorThread;

    invoke-virtual {v2}, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->start()V

    :cond_0
    sget-object v2, Lcom/konka/tvsettings/TVRootApp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "the On source is ===="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    return-void
.end method

.method public onTerminate()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/MyDataBase;->closeDB()V

    invoke-super {p0}, Landroid/app/Application;->onTerminate()V

    return-void
.end method

.method public setMainmenuHandler(Lcom/konka/tvsettings/MainMenuActivity$MainMenuHandler;)V
    .locals 0
    .param p1    # Lcom/konka/tvsettings/MainMenuActivity$MainMenuHandler;

    iput-object p1, p0, Lcom/konka/tvsettings/TVRootApp;->mainmenuHandler:Lcom/konka/tvsettings/MainMenuActivity$MainMenuHandler;

    return-void
.end method

.method public setRootmenuHandler(Lcom/konka/tvsettings/RootActivity$RootMenuHandler;)V
    .locals 0
    .param p1    # Lcom/konka/tvsettings/RootActivity$RootMenuHandler;

    iput-object p1, p0, Lcom/konka/tvsettings/TVRootApp;->rootmenuHandler:Lcom/konka/tvsettings/RootActivity$RootMenuHandler;

    return-void
.end method

.method public setToUpdataDB(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/tvsettings/TVRootApp;->isToUpdataDB:Z

    return-void
.end method

.method public setTvTextMode(Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;)V
    .locals 0
    .param p1    # Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

    sput-object p1, Lcom/konka/tvsettings/TVRootApp;->textStatus:Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

    return-void
.end method
