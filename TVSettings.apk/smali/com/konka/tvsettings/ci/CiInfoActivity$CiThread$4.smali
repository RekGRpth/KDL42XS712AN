.class Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread$4;
.super Ljava/lang/Object;
.source "CiInfoActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread$4;->this$1:Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread$4;->this$1:Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;
    invoke-static {v0}, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->access$2(Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;)Lcom/konka/tvsettings/ci/CiInfoActivity;

    move-result-object v0

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity;->m_CiHelper:Lcom/konka/tvsettings/ci/CiHelper;
    invoke-static {v0}, Lcom/konka/tvsettings/ci/CiInfoActivity;->access$1(Lcom/konka/tvsettings/ci/CiInfoActivity;)Lcom/konka/tvsettings/ci/CiHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/tvsettings/ci/CiHelper;->isCiMenuOn()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread$4;->this$1:Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;
    invoke-static {v0}, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->access$2(Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;)Lcom/konka/tvsettings/ci/CiInfoActivity;

    move-result-object v0

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity;->m_CiHelper:Lcom/konka/tvsettings/ci/CiHelper;
    invoke-static {v0}, Lcom/konka/tvsettings/ci/CiInfoActivity;->access$1(Lcom/konka/tvsettings/ci/CiInfoActivity;)Lcom/konka/tvsettings/ci/CiHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/tvsettings/ci/CiHelper;->enterMenu()V

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread$4;->this$1:Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;
    invoke-static {v0}, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->access$2(Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;)Lcom/konka/tvsettings/ci/CiInfoActivity;

    move-result-object v0

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity;->m_SubTitle:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/tvsettings/ci/CiInfoActivity;->access$4(Lcom/konka/tvsettings/ci/CiInfoActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread$4;->this$1:Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;
    invoke-static {v0}, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->access$2(Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;)Lcom/konka/tvsettings/ci/CiInfoActivity;

    move-result-object v0

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity;->m_ListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/konka/tvsettings/ci/CiInfoActivity;->access$6(Lcom/konka/tvsettings/ci/CiInfoActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread$4;->this$1:Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;
    invoke-static {v0}, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->access$2(Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;)Lcom/konka/tvsettings/ci/CiInfoActivity;

    move-result-object v0

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity;->m_BtmTitle:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/tvsettings/ci/CiInfoActivity;->access$5(Lcom/konka/tvsettings/ci/CiInfoActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread$4;->this$1:Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;
    invoke-static {v0}, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->access$2(Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;)Lcom/konka/tvsettings/ci/CiInfoActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread$4;->this$1:Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;
    invoke-static {v1}, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->access$2(Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;)Lcom/konka/tvsettings/ci/CiInfoActivity;

    move-result-object v1

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity;->m_CiHelper:Lcom/konka/tvsettings/ci/CiHelper;
    invoke-static {v1}, Lcom/konka/tvsettings/ci/CiInfoActivity;->access$1(Lcom/konka/tvsettings/ci/CiInfoActivity;)Lcom/konka/tvsettings/ci/CiHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/tvsettings/ci/CiHelper;->getMmiType()Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;

    move-result-object v1

    # invokes: Lcom/konka/tvsettings/ci/CiInfoActivity;->refreshData(Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;)V
    invoke-static {v0, v1}, Lcom/konka/tvsettings/ci/CiInfoActivity;->access$7(Lcom/konka/tvsettings/ci/CiInfoActivity;Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;)V

    :cond_0
    return-void
.end method
