.class Lcom/konka/tvsettings/ci/CiInfoActivity$CiHandler;
.super Landroid/os/Handler;
.source "CiInfoActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/ci/CiInfoActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CiHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;


# direct methods
.method private constructor <init>(Lcom/konka/tvsettings/ci/CiInfoActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiHandler;->this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/tvsettings/ci/CiInfoActivity;Lcom/konka/tvsettings/ci/CiInfoActivity$CiHandler;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/ci/CiInfoActivity$CiHandler;-><init>(Lcom/konka/tvsettings/ci/CiInfoActivity;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1    # Landroid/os/Message;

    const/4 v1, 0x0

    const/16 v2, 0x8

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiHandler;->this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity;->m_SubTitle:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/tvsettings/ci/CiInfoActivity;->access$4(Lcom/konka/tvsettings/ci/CiInfoActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiHandler;->this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;

    iget-object v1, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiHandler;->this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity;->m_CiHelper:Lcom/konka/tvsettings/ci/CiHelper;
    invoke-static {v1}, Lcom/konka/tvsettings/ci/CiInfoActivity;->access$1(Lcom/konka/tvsettings/ci/CiInfoActivity;)Lcom/konka/tvsettings/ci/CiHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/tvsettings/ci/CiHelper;->getMmiType()Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;

    move-result-object v1

    # invokes: Lcom/konka/tvsettings/ci/CiInfoActivity;->refreshData(Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;)V
    invoke-static {v0, v1}, Lcom/konka/tvsettings/ci/CiInfoActivity;->access$7(Lcom/konka/tvsettings/ci/CiInfoActivity;Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiHandler;->this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity;->m_CiHelper:Lcom/konka/tvsettings/ci/CiHelper;
    invoke-static {v0}, Lcom/konka/tvsettings/ci/CiInfoActivity;->access$1(Lcom/konka/tvsettings/ci/CiInfoActivity;)Lcom/konka/tvsettings/ci/CiHelper;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/ci/CiHelper;->setMenuNum(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiHandler;->this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity;->m_CiHelper:Lcom/konka/tvsettings/ci/CiHelper;
    invoke-static {v0}, Lcom/konka/tvsettings/ci/CiInfoActivity;->access$1(Lcom/konka/tvsettings/ci/CiInfoActivity;)Lcom/konka/tvsettings/ci/CiHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/tvsettings/ci/CiHelper;->close()V

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiHandler;->this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity;->m_Title:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/tvsettings/ci/CiInfoActivity;->access$3(Lcom/konka/tvsettings/ci/CiInfoActivity;)Landroid/widget/TextView;

    move-result-object v0

    const v1, 0x7f0a0103    # com.konka.tvsettings.R.string.str_ci_info_nocard

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiHandler;->this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity;->m_SubTitle:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/tvsettings/ci/CiInfoActivity;->access$4(Lcom/konka/tvsettings/ci/CiInfoActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiHandler;->this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity;->m_BtmTitle:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/tvsettings/ci/CiInfoActivity;->access$5(Lcom/konka/tvsettings/ci/CiInfoActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiHandler;->this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity;->m_ListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/konka/tvsettings/ci/CiInfoActivity;->access$6(Lcom/konka/tvsettings/ci/CiInfoActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
