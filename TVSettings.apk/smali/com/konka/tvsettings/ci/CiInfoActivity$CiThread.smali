.class Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;
.super Ljava/lang/Thread;
.source "CiInfoActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/ci/CiInfoActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CiThread"
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$dtv$vo$EnumCardState:[I


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;


# direct methods
.method static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$dtv$vo$EnumCardState()[I
    .locals 3

    sget-object v0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->$SWITCH_TABLE$com$mstar$android$tvapi$dtv$vo$EnumCardState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;->values()[Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;->E_INITIALIZING:Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_1
    :try_start_1
    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;->E_MAX:Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_2
    :try_start_2
    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;->E_NO:Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    :try_start_3
    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;->E_READY:Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_4
    :try_start_4
    sget-object v1, Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;->E_RESET:Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_5
    sput-object v0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->$SWITCH_TABLE$com$mstar$android$tvapi$dtv$vo$EnumCardState:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method

.method private constructor <init>(Lcom/konka/tvsettings/ci/CiInfoActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/tvsettings/ci/CiInfoActivity;Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;-><init>(Lcom/konka/tvsettings/ci/CiInfoActivity;)V

    return-void
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;)Lcom/konka/tvsettings/ci/CiInfoActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 5

    :goto_0
    iget-object v1, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity;->bIsCiAct:Z
    invoke-static {v1}, Lcom/konka/tvsettings/ci/CiInfoActivity;->access$0(Lcom/konka/tvsettings/ci/CiInfoActivity;)Z

    move-result v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "card state ====== "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity;->m_CiHelper:Lcom/konka/tvsettings/ci/CiHelper;
    invoke-static {v2}, Lcom/konka/tvsettings/ci/CiInfoActivity;->access$1(Lcom/konka/tvsettings/ci/CiInfoActivity;)Lcom/konka/tvsettings/ci/CiHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/tvsettings/ci/CiHelper;->getCardState()Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->$SWITCH_TABLE$com$mstar$android$tvapi$dtv$vo$EnumCardState()[I

    move-result-object v1

    iget-object v2, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity;->m_CiHelper:Lcom/konka/tvsettings/ci/CiHelper;
    invoke-static {v2}, Lcom/konka/tvsettings/ci/CiInfoActivity;->access$1(Lcom/konka/tvsettings/ci/CiInfoActivity;)Lcom/konka/tvsettings/ci/CiHelper;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/tvsettings/ci/CiHelper;->getCardState()Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_1
    const-wide/16 v1, 0x1f40

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    :pswitch_0
    iget-object v1, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity;->m_Handler:Lcom/konka/tvsettings/ci/CiInfoActivity$CiHandler;
    invoke-static {v1}, Lcom/konka/tvsettings/ci/CiInfoActivity;->access$2(Lcom/konka/tvsettings/ci/CiInfoActivity;)Lcom/konka/tvsettings/ci/CiInfoActivity$CiHandler;

    move-result-object v1

    new-instance v2, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread$1;

    invoke-direct {v2, p0}, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread$1;-><init>(Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;)V

    invoke-virtual {v1, v2}, Lcom/konka/tvsettings/ci/CiInfoActivity$CiHandler;->post(Ljava/lang/Runnable;)Z

    iget-object v1, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity;->m_Handler:Lcom/konka/tvsettings/ci/CiInfoActivity$CiHandler;
    invoke-static {v1}, Lcom/konka/tvsettings/ci/CiInfoActivity;->access$2(Lcom/konka/tvsettings/ci/CiInfoActivity;)Lcom/konka/tvsettings/ci/CiInfoActivity$CiHandler;

    move-result-object v1

    new-instance v2, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread$2;

    invoke-direct {v2, p0}, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread$2;-><init>(Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;)V

    const-wide/16 v3, 0x3e8

    invoke-virtual {v1, v2, v3, v4}, Lcom/konka/tvsettings/ci/CiInfoActivity$CiHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    :pswitch_1
    iget-object v1, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity;->m_Handler:Lcom/konka/tvsettings/ci/CiInfoActivity$CiHandler;
    invoke-static {v1}, Lcom/konka/tvsettings/ci/CiInfoActivity;->access$2(Lcom/konka/tvsettings/ci/CiInfoActivity;)Lcom/konka/tvsettings/ci/CiInfoActivity$CiHandler;

    move-result-object v1

    new-instance v2, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread$3;

    invoke-direct {v2, p0}, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread$3;-><init>(Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;)V

    invoke-virtual {v1, v2}, Lcom/konka/tvsettings/ci/CiInfoActivity$CiHandler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    :pswitch_2
    iget-object v1, p0, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;->this$0:Lcom/konka/tvsettings/ci/CiInfoActivity;

    # getter for: Lcom/konka/tvsettings/ci/CiInfoActivity;->m_Handler:Lcom/konka/tvsettings/ci/CiInfoActivity$CiHandler;
    invoke-static {v1}, Lcom/konka/tvsettings/ci/CiInfoActivity;->access$2(Lcom/konka/tvsettings/ci/CiInfoActivity;)Lcom/konka/tvsettings/ci/CiInfoActivity$CiHandler;

    move-result-object v1

    new-instance v2, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread$4;

    invoke-direct {v2, p0}, Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread$4;-><init>(Lcom/konka/tvsettings/ci/CiInfoActivity$CiThread;)V

    invoke-virtual {v1, v2}, Lcom/konka/tvsettings/ci/CiInfoActivity$CiHandler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
