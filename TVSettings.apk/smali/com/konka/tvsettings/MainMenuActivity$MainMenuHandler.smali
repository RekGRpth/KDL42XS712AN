.class public final Lcom/konka/tvsettings/MainMenuActivity$MainMenuHandler;
.super Landroid/os/Handler;
.source "MainMenuActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/MainMenuActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "MainMenuHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/MainMenuActivity;


# direct methods
.method public constructor <init>(Lcom/konka/tvsettings/MainMenuActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/MainMenuActivity$MainMenuHandler;->this$0:Lcom/konka/tvsettings/MainMenuActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1    # Landroid/os/Message;

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x10

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/konka/tvsettings/MainMenuActivity$MainMenuHandler;->this$0:Lcom/konka/tvsettings/MainMenuActivity;

    invoke-virtual {v0}, Lcom/konka/tvsettings/MainMenuActivity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x20

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/MainMenuActivity$MainMenuHandler;->this$0:Lcom/konka/tvsettings/MainMenuActivity;

    iget v1, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/konka/tvsettings/MainMenuActivity;->startMenu(I)V
    invoke-static {v0, v1}, Lcom/konka/tvsettings/MainMenuActivity;->access$1(Lcom/konka/tvsettings/MainMenuActivity;I)V

    goto :goto_0
.end method
