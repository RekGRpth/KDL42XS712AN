.class Lcom/konka/tvsettings/sound/SoundSettingActivity$5;
.super Lcom/konka/tvsettings/view/ItemStringOption;
.source "SoundSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/sound/SoundSettingActivity;->findViews()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/sound/SoundSettingActivity;Landroid/content/Context;IIIII)V
    .locals 7
    .param p2    # Landroid/content/Context;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I

    iput-object p1, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity$5;->this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;

    move-object v0, p0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    move v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/konka/tvsettings/view/ItemStringOption;-><init>(Landroid/content/Context;IIIII)V

    return-void
.end method


# virtual methods
.method public doUpdate()V
    .locals 5

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity$5;->this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;

    # getter for: Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSmartSoundCtrl:Lcom/konka/tvsettings/view/ItemStringOption;
    invoke-static {v3}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->access$4(Lcom/konka/tvsettings/sound/SoundSettingActivity;)Lcom/konka/tvsettings/view/ItemStringOption;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/tvsettings/view/ItemStringOption;->getIndex()I

    move-result v1

    if-ne v1, v2, :cond_0

    move v0, v2

    :goto_0
    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity$5;->this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;

    # getter for: Lcom/konka/tvsettings/sound/SoundSettingActivity;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v3}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->access$5(Lcom/konka/tvsettings/sound/SoundSettingActivity;)Landroid/media/AudioManager;

    move-result-object v3

    const/16 v4, -0xa

    invoke-virtual {v3, v2, v4}, Landroid/media/AudioManager;->setMasterMute(ZI)V

    iget-object v2, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity$5;->this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;

    # getter for: Lcom/konka/tvsettings/sound/SoundSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v2}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->access$1(Lcom/konka/tvsettings/sound/SoundSettingActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/konka/kkinterface/tv/SoundDesk;->setAVCMode(Z)Z

    iget-object v2, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity$5;->this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;

    invoke-virtual {v2}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->unmute()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
