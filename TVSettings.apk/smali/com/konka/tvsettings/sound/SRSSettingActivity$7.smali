.class Lcom/konka/tvsettings/sound/SRSSettingActivity$7;
.super Lcom/konka/tvsettings/view/ItemStringOption;
.source "SRSSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/sound/SRSSettingActivity;->initMenu()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/sound/SRSSettingActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/sound/SRSSettingActivity;Landroid/content/Context;IIIIII)V
    .locals 8
    .param p2    # Landroid/content/Context;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # I

    iput-object p1, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity$7;->this$0:Lcom/konka/tvsettings/sound/SRSSettingActivity;

    move-object v0, p0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    move v6, p7

    move/from16 v7, p8

    invoke-direct/range {v0 .. v7}, Lcom/konka/tvsettings/view/ItemStringOption;-><init>(Landroid/content/Context;IIIIII)V

    return-void
.end method


# virtual methods
.method public doUpdate()V
    .locals 3

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity$7;->this$0:Lcom/konka/tvsettings/sound/SRSSettingActivity;

    iget-object v1, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity$7;->this$0:Lcom/konka/tvsettings/sound/SRSSettingActivity;

    # getter for: Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemSpeakAudio:Lcom/konka/tvsettings/view/ItemStringOption;
    invoke-static {v1}, Lcom/konka/tvsettings/sound/SRSSettingActivity;->access$15(Lcom/konka/tvsettings/sound/SRSSettingActivity;)Lcom/konka/tvsettings/view/ItemStringOption;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/ItemStringOption;->getIndex()I

    move-result v1

    invoke-static {v0, v1}, Lcom/konka/tvsettings/sound/SRSSettingActivity;->access$16(Lcom/konka/tvsettings/sound/SRSSettingActivity;I)V

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity$7;->this$0:Lcom/konka/tvsettings/sound/SRSSettingActivity;

    # getter for: Lcom/konka/tvsettings/sound/SRSSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v0}, Lcom/konka/tvsettings/sound/SRSSettingActivity;->access$10(Lcom/konka/tvsettings/sound/SRSSettingActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v0

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;->E_SRS_SPEAKERAUDIO:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;

    iget-object v2, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity$7;->this$0:Lcom/konka/tvsettings/sound/SRSSettingActivity;

    # getter for: Lcom/konka/tvsettings/sound/SRSSettingActivity;->m_iSpeakAudio:I
    invoke-static {v2}, Lcom/konka/tvsettings/sound/SRSSettingActivity;->access$17(Lcom/konka/tvsettings/sound/SRSSettingActivity;)I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/SoundDesk;->setSRSPara(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SRS_SET_TYPE;I)V

    return-void
.end method
