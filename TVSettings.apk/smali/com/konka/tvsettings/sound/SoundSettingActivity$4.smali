.class Lcom/konka/tvsettings/sound/SoundSettingActivity$4;
.super Lcom/konka/tvsettings/view/ItemStringOption;
.source "SoundSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/sound/SoundSettingActivity;->findViews()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/sound/SoundSettingActivity;Landroid/content/Context;IIIIII)V
    .locals 8
    .param p2    # Landroid/content/Context;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # I

    iput-object p1, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity$4;->this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;

    move-object v0, p0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    move v6, p7

    move/from16 v7, p8

    invoke-direct/range {v0 .. v7}, Lcom/konka/tvsettings/view/ItemStringOption;-><init>(Landroid/content/Context;IIIIII)V

    return-void
.end method


# virtual methods
.method public doUpdate()V
    .locals 2

    iget-object v1, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity$4;->this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;

    # getter for: Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemBalance:Lcom/konka/tvsettings/view/ItemStringOption;
    invoke-static {v1}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->access$3(Lcom/konka/tvsettings/sound/SoundSettingActivity;)Lcom/konka/tvsettings/view/ItemStringOption;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/ItemStringOption;->getIndex()I

    move-result v1

    add-int/lit8 v1, v1, 0x32

    int-to-short v0, v1

    iget-object v1, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity$4;->this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;

    # getter for: Lcom/konka/tvsettings/sound/SoundSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v1}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->access$1(Lcom/konka/tvsettings/sound/SoundSettingActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/konka/kkinterface/tv/SoundDesk;->setBalance(S)Z

    iget-object v1, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity$4;->this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->unmute()V

    return-void
.end method
