.class Lcom/konka/tvsettings/sound/SoundSettingActivity$Ress;
.super Ljava/lang/Object;
.source "SoundSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/sound/SoundSettingActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Ress"
.end annotation


# static fields
.field public static final AD:I = 0x6

.field public static final ADVolume:I = 0x7

.field public static final BALANCE:I = 0x1

.field public static final HDMIS:I = 0x5

.field public static final MENU_BOTTOM_BACKGROUND:I = 0x7f020033

.field public static final MENU_TOP_BACKGROUND:I = 0x7f020031

.field public static final NameOf:[I

.field public static final SMOD:I = 0x0

.field public static final SPDIF:I = 0x4

.field public static final SSCTRL:I = 0x2

.field public static final SURRND:I = 0x3

.field public static final ValArrOf:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/konka/tvsettings/sound/SoundSettingActivity$Ress;->ValArrOf:[I

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/konka/tvsettings/sound/SoundSettingActivity$Ress;->NameOf:[I

    return-void

    :array_0
    .array-data 4
        0x7f0b000b    # com.konka.tvsettings.R.array.str_arr_sound_mode_vals
        0x7f0b000b    # com.konka.tvsettings.R.array.str_arr_sound_mode_vals
        0x7f0b000e    # com.konka.tvsettings.R.array.str_arr_sound_voice_ctrl_vals
        0x7f0b000d    # com.konka.tvsettings.R.array.str_arr_sound_surround_vals
        0x7f0b000f    # com.konka.tvsettings.R.array.str_arr_sound_spdifoutput_vals
        0x7f0b0012    # com.konka.tvsettings.R.array.str_arr_sound_hdmi_vals
        0x7f0b003c    # com.konka.tvsettings.R.array.str_arr_common_on_and_off
    .end array-data

    :array_1
    .array-data 4
        0x7f0a0088    # com.konka.tvsettings.R.string.str_sound_soundmode
        0x7f0a0091    # com.konka.tvsettings.R.string.str_sound_balance
        0x7f0a0093    # com.konka.tvsettings.R.string.str_sound_voice_ctrl
        0x7f0a0092    # com.konka.tvsettings.R.string.str_sound_surround
        0x7f0a0094    # com.konka.tvsettings.R.string.str_sound_spdifoutput
        0x7f0a009e    # com.konka.tvsettings.R.string.str_sound_hdmi
        0x7f0a01af    # com.konka.tvsettings.R.string.str_ad
        0x7f0a009f    # com.konka.tvsettings.R.string.str_sound_ADVolume
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
