.class public Lcom/konka/tvsettings/sound/EqualizerDialogActivity;
.super Lcom/konka/tvsettings/teletext/MstarBaseActivity;
.source "EqualizerDialogActivity.java"


# static fields
.field protected static final STEP:I = 0x1


# instance fields
.field protected bIsValueChanged:Z

.field private handler:Landroid/os/Handler;

.field private mAudioManager:Landroid/media/AudioManager;

.field protected seekBarBtn10KHz:Lcom/konka/tvsettings/statebar/SeekBarButton;

.field protected seekBarBtn120Hz:Lcom/konka/tvsettings/statebar/SeekBarButton;

.field protected seekBarBtn1_5KHz:Lcom/konka/tvsettings/statebar/SeekBarButton;

.field protected seekBarBtn500Hz:Lcom/konka/tvsettings/statebar/SeekBarButton;

.field protected seekBarBtn5KHz:Lcom/konka/tvsettings/statebar/SeekBarButton;

.field protected serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->bIsValueChanged:Z

    new-instance v0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/sound/EqualizerDialogActivity$1;-><init>(Lcom/konka/tvsettings/sound/EqualizerDialogActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->handler:Landroid/os/Handler;

    return-void
.end method

.method private LoadDataToDialog()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->seekBarBtn120Hz:Lcom/konka/tvsettings/statebar/SeekBarButton;

    iget-object v1, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/SoundDesk;->getEqBand120()S

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    int-to-short v1, v1

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/statebar/SeekBarButton;->setProgress(S)V

    iget-object v0, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->seekBarBtn500Hz:Lcom/konka/tvsettings/statebar/SeekBarButton;

    iget-object v1, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/SoundDesk;->getEqBand500()S

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    int-to-short v1, v1

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/statebar/SeekBarButton;->setProgress(S)V

    iget-object v0, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->seekBarBtn1_5KHz:Lcom/konka/tvsettings/statebar/SeekBarButton;

    iget-object v1, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/SoundDesk;->getEqBand1500()S

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    int-to-short v1, v1

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/statebar/SeekBarButton;->setProgress(S)V

    iget-object v0, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->seekBarBtn5KHz:Lcom/konka/tvsettings/statebar/SeekBarButton;

    iget-object v1, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/SoundDesk;->getEqBand5k()S

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    int-to-short v1, v1

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/statebar/SeekBarButton;->setProgress(S)V

    iget-object v0, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->seekBarBtn10KHz:Lcom/konka/tvsettings/statebar/SeekBarButton;

    iget-object v1, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/SoundDesk;->getEqBand10k()S

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    int-to-short v1, v1

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/statebar/SeekBarButton;->setProgress(S)V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/sound/EqualizerDialogActivity;)Landroid/media/AudioManager;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method private setOnFocusChangeListeners()V
    .locals 3

    new-instance v0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity$7;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/sound/EqualizerDialogActivity$7;-><init>(Lcom/konka/tvsettings/sound/EqualizerDialogActivity;)V

    iget-object v2, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->seekBarBtn120Hz:Lcom/konka/tvsettings/statebar/SeekBarButton;

    invoke-virtual {v2, v0}, Lcom/konka/tvsettings/statebar/SeekBarButton;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v2, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->seekBarBtn500Hz:Lcom/konka/tvsettings/statebar/SeekBarButton;

    invoke-virtual {v2, v0}, Lcom/konka/tvsettings/statebar/SeekBarButton;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v2, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->seekBarBtn1_5KHz:Lcom/konka/tvsettings/statebar/SeekBarButton;

    invoke-virtual {v2, v0}, Lcom/konka/tvsettings/statebar/SeekBarButton;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v2, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->seekBarBtn5KHz:Lcom/konka/tvsettings/statebar/SeekBarButton;

    invoke-virtual {v2, v0}, Lcom/konka/tvsettings/statebar/SeekBarButton;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v2, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->seekBarBtn10KHz:Lcom/konka/tvsettings/statebar/SeekBarButton;

    invoke-virtual {v2, v0}, Lcom/konka/tvsettings/statebar/SeekBarButton;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    new-instance v1, Lcom/konka/tvsettings/sound/EqualizerDialogActivity$8;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/sound/EqualizerDialogActivity$8;-><init>(Lcom/konka/tvsettings/sound/EqualizerDialogActivity;)V

    iget-object v2, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->seekBarBtn120Hz:Lcom/konka/tvsettings/statebar/SeekBarButton;

    invoke-virtual {v2, v1}, Lcom/konka/tvsettings/statebar/SeekBarButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->seekBarBtn500Hz:Lcom/konka/tvsettings/statebar/SeekBarButton;

    invoke-virtual {v2, v1}, Lcom/konka/tvsettings/statebar/SeekBarButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->seekBarBtn1_5KHz:Lcom/konka/tvsettings/statebar/SeekBarButton;

    invoke-virtual {v2, v1}, Lcom/konka/tvsettings/statebar/SeekBarButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->seekBarBtn5KHz:Lcom/konka/tvsettings/statebar/SeekBarButton;

    invoke-virtual {v2, v1}, Lcom/konka/tvsettings/statebar/SeekBarButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->seekBarBtn10KHz:Lcom/konka/tvsettings/statebar/SeekBarButton;

    invoke-virtual {v2, v1}, Lcom/konka/tvsettings/statebar/SeekBarButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method getSoundEffect()Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;
    .locals 3

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;-><init>()V

    iget-object v1, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->seekBarBtn120Hz:Lcom/konka/tvsettings/statebar/SeekBarButton;

    invoke-virtual {v2}, Lcom/konka/tvsettings/statebar/SeekBarButton;->getProgress()S

    move-result v2

    div-int/lit8 v2, v2, 0x2

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v1, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->seekBarBtn500Hz:Lcom/konka/tvsettings/statebar/SeekBarButton;

    invoke-virtual {v2}, Lcom/konka/tvsettings/statebar/SeekBarButton;->getProgress()S

    move-result v2

    div-int/lit8 v2, v2, 0x2

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v1, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->seekBarBtn1_5KHz:Lcom/konka/tvsettings/statebar/SeekBarButton;

    invoke-virtual {v2}, Lcom/konka/tvsettings/statebar/SeekBarButton;->getProgress()S

    move-result v2

    div-int/lit8 v2, v2, 0x2

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v1, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->seekBarBtn5KHz:Lcom/konka/tvsettings/statebar/SeekBarButton;

    invoke-virtual {v2}, Lcom/konka/tvsettings/statebar/SeekBarButton;->getProgress()S

    move-result v2

    div-int/lit8 v2, v2, 0x2

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v1, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->seekBarBtn10KHz:Lcom/konka/tvsettings/statebar/SeekBarButton;

    invoke-virtual {v2}, Lcom/konka/tvsettings/statebar/SeekBarButton;->getProgress()S

    move-result v2

    div-int/lit8 v2, v2, 0x2

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    const/4 v1, 0x5

    iput-short v1, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->eqBandNumber:S

    return-object v0
.end method

.method public onBackPressed()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/konka/tvsettings/MainMenuActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->finish()V

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onBackPressed()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-super {p0, p1}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030015    # com.konka.tvsettings.R.layout.equalizer_dialog

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->getApplication()Landroid/app/Application;

    move-result-object v6

    check-cast v6, Lcom/konka/tvsettings/TVRootApp;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->mAudioManager:Landroid/media/AudioManager;

    new-instance v0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity$2;

    const v3, 0x7f0700a6    # com.konka.tvsettings.R.id.linearlayout_sound_equalizer120hz

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/sound/EqualizerDialogActivity$2;-><init>(Lcom/konka/tvsettings/sound/EqualizerDialogActivity;Landroid/app/Activity;IIZ)V

    iput-object v0, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->seekBarBtn120Hz:Lcom/konka/tvsettings/statebar/SeekBarButton;

    new-instance v0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity$3;

    const v3, 0x7f0700a7    # com.konka.tvsettings.R.id.linearlayout_sound_equalizer500hz

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/sound/EqualizerDialogActivity$3;-><init>(Lcom/konka/tvsettings/sound/EqualizerDialogActivity;Landroid/app/Activity;IIZ)V

    iput-object v0, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->seekBarBtn500Hz:Lcom/konka/tvsettings/statebar/SeekBarButton;

    new-instance v0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity$4;

    const v3, 0x7f0700a8    # com.konka.tvsettings.R.id.linearlayout_sound_equalizer1_5khz

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/sound/EqualizerDialogActivity$4;-><init>(Lcom/konka/tvsettings/sound/EqualizerDialogActivity;Landroid/app/Activity;IIZ)V

    iput-object v0, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->seekBarBtn1_5KHz:Lcom/konka/tvsettings/statebar/SeekBarButton;

    new-instance v0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity$5;

    const v3, 0x7f0700a9    # com.konka.tvsettings.R.id.linearlayout_sound_equalizer5khz

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/sound/EqualizerDialogActivity$5;-><init>(Lcom/konka/tvsettings/sound/EqualizerDialogActivity;Landroid/app/Activity;IIZ)V

    iput-object v0, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->seekBarBtn5KHz:Lcom/konka/tvsettings/statebar/SeekBarButton;

    new-instance v0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity$6;

    const v3, 0x7f0700aa    # com.konka.tvsettings.R.id.linearlayout_sound_equalizer10khz

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/sound/EqualizerDialogActivity$6;-><init>(Lcom/konka/tvsettings/sound/EqualizerDialogActivity;Landroid/app/Activity;IIZ)V

    iput-object v0, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->seekBarBtn10KHz:Lcom/konka/tvsettings/statebar/SeekBarButton;

    invoke-direct {p0}, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->LoadDataToDialog()V

    invoke-direct {p0}, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->setOnFocusChangeListeners()V

    iget-object v0, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->seekBarBtn120Hz:Lcom/konka/tvsettings/statebar/SeekBarButton;

    invoke-virtual {v0}, Lcom/konka/tvsettings/statebar/SeekBarButton;->setFocused()V

    iget-object v0, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->handler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/konka/tvsettings/common/LittleDownTimer;->setHandler(Landroid/os/Handler;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch p1, :sswitch_data_0

    :goto_0
    invoke-super {p0, p1, p2}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v3

    :goto_1
    return v3

    :sswitch_0
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->finish()V

    const v3, 0x7f040005    # com.konka.tvsettings.R.anim.anim_menu_exit

    invoke-virtual {p0, v4, v3}, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->overridePendingTransition(II)V

    goto :goto_0

    :sswitch_1
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-class v3, Lcom/konka/tvsettings/sound/SoundSettingActivity;

    invoke-virtual {v1, p0, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v3, "currentPage"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->finish()V

    goto :goto_0

    :sswitch_2
    const/4 v3, 0x1

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x52 -> :sswitch_1
        0xb2 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 3

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->pauseMenu()V

    iget-boolean v1, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->bIsValueChanged:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->getSoundEffect()Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, Lcom/konka/kkinterface/tv/SoundDesk;->adjustSoundMode(Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;Z)Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;->bIsValueChanged:Z

    :cond_0
    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resumeMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onResume()V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resetMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onUserInteraction()V

    return-void
.end method
