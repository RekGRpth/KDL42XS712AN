.class Lcom/konka/tvsettings/sound/SoundSettingActivity$2;
.super Lcom/konka/tvsettings/view/ItemStringOption;
.source "SoundSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/sound/SoundSettingActivity;->findViews()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/sound/SoundSettingActivity;Landroid/content/Context;IIIII)V
    .locals 7
    .param p2    # Landroid/content/Context;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I

    iput-object p1, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity$2;->this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;

    move-object v0, p0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    move v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/konka/tvsettings/view/ItemStringOption;-><init>(Landroid/content/Context;IIIII)V

    return-void
.end method


# virtual methods
.method public doUpdate()V
    .locals 3

    iget-object v2, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity$2;->this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;

    # getter for: Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSoundMod:Lcom/konka/tvsettings/view/ItemStringOption;
    invoke-static {v2}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->access$0(Lcom/konka/tvsettings/sound/SoundSettingActivity;)Lcom/konka/tvsettings/view/ItemStringOption;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/ItemStringOption;->getIndex()I

    move-result v1

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    move-result-object v2

    aget-object v0, v2, v1

    iget-object v2, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity$2;->this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;

    # getter for: Lcom/konka/tvsettings/sound/SoundSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v2}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->access$1(Lcom/konka/tvsettings/sound/SoundSettingActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/konka/kkinterface/tv/SoundDesk;->setSoundMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;)Z

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity$2;->this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;

    # getter for: Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemEqualizer:Lcom/konka/tvsettings/view/ItemStringOption;
    invoke-static {v2}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->access$2(Lcom/konka/tvsettings/sound/SoundSettingActivity;)Lcom/konka/tvsettings/view/ItemStringOption;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusAllowed()V

    :goto_0
    iget-object v2, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity$2;->this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;

    invoke-virtual {v2}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->unmute()V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity$2;->this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;

    # getter for: Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemEqualizer:Lcom/konka/tvsettings/view/ItemStringOption;
    invoke-static {v2}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->access$2(Lcom/konka/tvsettings/sound/SoundSettingActivity;)Lcom/konka/tvsettings/view/ItemStringOption;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusFbd()V

    goto :goto_0
.end method
