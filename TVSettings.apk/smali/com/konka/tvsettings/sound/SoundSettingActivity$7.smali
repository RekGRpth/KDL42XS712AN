.class Lcom/konka/tvsettings/sound/SoundSettingActivity$7;
.super Lcom/konka/tvsettings/view/ItemStringOption;
.source "SoundSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/sound/SoundSettingActivity;->findViews()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/sound/SoundSettingActivity;Landroid/content/Context;IIIII)V
    .locals 7
    .param p2    # Landroid/content/Context;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I

    iput-object p1, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity$7;->this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;

    move-object v0, p0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    move v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/konka/tvsettings/view/ItemStringOption;-><init>(Landroid/content/Context;IIIII)V

    return-void
.end method


# virtual methods
.method public doUpdate()V
    .locals 4

    iget-object v2, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity$7;->this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;

    # getter for: Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemSoundSpdifOutput:Lcom/konka/tvsettings/view/ItemStringOption;
    invoke-static {v2}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->access$7(Lcom/konka/tvsettings/sound/SoundSettingActivity;)Lcom/konka/tvsettings/view/ItemStringOption;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/tvsettings/view/ItemStringOption;->getIndex()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SPDIF_MODE;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SPDIF_MODE;

    move-result-object v2

    aget-object v1, v2, v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SPDIF XXX=====>>>spdifoutMode["

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity$7;->this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;

    # getter for: Lcom/konka/tvsettings/sound/SoundSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v2}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->access$1(Lcom/konka/tvsettings/sound/SoundSettingActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/konka/kkinterface/tv/SoundDesk;->setSpdifOutMode(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SPDIF_MODE;)Z

    return-void
.end method
