.class Lcom/konka/tvsettings/sound/SoundSettingActivity$9;
.super Lcom/konka/tvsettings/view/ItemStringOption;
.source "SoundSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/sound/SoundSettingActivity;->findViews()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/sound/SoundSettingActivity;Landroid/content/Context;IIIII)V
    .locals 7
    .param p2    # Landroid/content/Context;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I

    iput-object p1, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity$9;->this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;

    move-object v0, p0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    move v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/konka/tvsettings/view/ItemStringOption;-><init>(Landroid/content/Context;IIIII)V

    return-void
.end method


# virtual methods
.method public doUpdate()V
    .locals 7

    const/4 v2, 0x1

    iget-object v4, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity$9;->this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;

    # getter for: Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemAD:Lcom/konka/tvsettings/view/ItemStringOption;
    invoke-static {v4}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->access$9(Lcom/konka/tvsettings/sound/SoundSettingActivity;)Lcom/konka/tvsettings/view/ItemStringOption;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/tvsettings/view/ItemStringOption;->getIndex()I

    move-result v0

    const-string v4, "liying"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "\n itemAD get index ----"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-ne v0, v2, :cond_0

    :goto_0
    iget-object v4, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity$9;->this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;

    # getter for: Lcom/konka/tvsettings/sound/SoundSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v4}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->access$1(Lcom/konka/tvsettings/sound/SoundSettingActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/SoundDesk;->getADAbsoluteVolume()I

    move-result v1

    iget-object v4, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity$9;->this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;

    # getter for: Lcom/konka/tvsettings/sound/SoundSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v4}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->access$1(Lcom/konka/tvsettings/sound/SoundSettingActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/SoundDesk;->getVolume()S

    move-result v3

    const-string v4, "qhc"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "adAbsoluteVolume"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "qhc"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "volume"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity$9;->this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;

    # getter for: Lcom/konka/tvsettings/sound/SoundSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v4}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->access$1(Lcom/konka/tvsettings/sound/SoundSettingActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v4

    invoke-interface {v4, v2}, Lcom/konka/kkinterface/tv/SoundDesk;->setADEnable(Z)V

    iget-object v4, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity$9;->this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;

    # getter for: Lcom/konka/tvsettings/sound/SoundSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v4}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->access$1(Lcom/konka/tvsettings/sound/SoundSettingActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v4

    invoke-interface {v4, v1}, Lcom/konka/kkinterface/tv/SoundDesk;->setADAbsoluteVolume(I)V

    invoke-super {p0}, Lcom/konka/tvsettings/view/ItemStringOption;->doUpdate()V

    return-void

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
