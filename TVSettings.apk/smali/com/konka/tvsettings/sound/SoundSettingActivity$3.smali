.class Lcom/konka/tvsettings/sound/SoundSettingActivity$3;
.super Lcom/konka/tvsettings/view/SingleItemStringOption;
.source "SoundSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/sound/SoundSettingActivity;->findViews()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/sound/SoundSettingActivity;Landroid/content/Context;IIII)V
    .locals 6
    .param p2    # Landroid/content/Context;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I

    iput-object p1, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity$3;->this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;

    move-object v0, p0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/view/SingleItemStringOption;-><init>(Landroid/content/Context;IIII)V

    return-void
.end method


# virtual methods
.method public doUpdate()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity$3;->this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/konka/tvsettings/sound/EqualizerDialogActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity$3;->this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;

    invoke-virtual {v1, v0}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity$3;->this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->finish()V

    return-void
.end method
