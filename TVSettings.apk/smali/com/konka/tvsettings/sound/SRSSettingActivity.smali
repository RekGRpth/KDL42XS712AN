.class public Lcom/konka/tvsettings/sound/SRSSettingActivity;
.super Landroid/app/Activity;
.source "SRSSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/sound/SRSSettingActivity$Ress;
    }
.end annotation


# instance fields
.field private iItemHeight:I

.field private iItemWidth:I

.field private itemDCControl:Lcom/konka/tvsettings/view/ItemStringOption;

.field private itemDefinationControl:Lcom/konka/tvsettings/view/ItemStringOption;

.field private itemDefinition:Lcom/konka/tvsettings/view/ItemStringOption;

.field private itemDynamicClarity:Lcom/konka/tvsettings/view/ItemStringOption;

.field private itemInputGain:Lcom/konka/tvsettings/view/ItemStringOption;

.field private itemSpeakAudio:Lcom/konka/tvsettings/view/ItemStringOption;

.field private itemSpeakerAnalysis:Lcom/konka/tvsettings/view/ItemStringOption;

.field private itemSurrLevelControl:Lcom/konka/tvsettings/view/ItemStringOption;

.field private itemTSHD:Lcom/konka/tvsettings/view/ItemStringOption;

.field private itemTrubassControl:Lcom/konka/tvsettings/view/ItemStringOption;

.field private itemTruebass:Lcom/konka/tvsettings/view/ItemStringOption;

.field private layoutMenuContainer:Landroid/widget/LinearLayout;

.field private m_iDCControl:I

.field private m_iDefinationControl:I

.field private m_iInputGain:I

.field private m_iSpeakAudio:I

.field private m_iSpeakerAnalysis:I

.field private m_iSurrLevelControl:I

.field private m_iTrubassControl:I

.field preTools:Lcom/konka/tvsettings/common/PreferancesTools;

.field private serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

.field stPara:Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/sound/SRSSettingActivity;)Lcom/konka/tvsettings/view/ItemStringOption;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemTSHD:Lcom/konka/tvsettings/view/ItemStringOption;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/sound/SRSSettingActivity;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/sound/SRSSettingActivity;->setSRSTSHD(Z)V

    return-void
.end method

.method static synthetic access$10(Lcom/konka/tvsettings/sound/SRSSettingActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    return-object v0
.end method

.method static synthetic access$11(Lcom/konka/tvsettings/sound/SRSSettingActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->m_iInputGain:I

    return v0
.end method

.method static synthetic access$12(Lcom/konka/tvsettings/sound/SRSSettingActivity;)Lcom/konka/tvsettings/view/ItemStringOption;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemSurrLevelControl:Lcom/konka/tvsettings/view/ItemStringOption;

    return-object v0
.end method

.method static synthetic access$13(Lcom/konka/tvsettings/sound/SRSSettingActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->m_iSurrLevelControl:I

    return-void
.end method

.method static synthetic access$14(Lcom/konka/tvsettings/sound/SRSSettingActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->m_iSurrLevelControl:I

    return v0
.end method

.method static synthetic access$15(Lcom/konka/tvsettings/sound/SRSSettingActivity;)Lcom/konka/tvsettings/view/ItemStringOption;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemSpeakAudio:Lcom/konka/tvsettings/view/ItemStringOption;

    return-object v0
.end method

.method static synthetic access$16(Lcom/konka/tvsettings/sound/SRSSettingActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->m_iSpeakAudio:I

    return-void
.end method

.method static synthetic access$17(Lcom/konka/tvsettings/sound/SRSSettingActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->m_iSpeakAudio:I

    return v0
.end method

.method static synthetic access$18(Lcom/konka/tvsettings/sound/SRSSettingActivity;)Lcom/konka/tvsettings/view/ItemStringOption;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemSpeakerAnalysis:Lcom/konka/tvsettings/view/ItemStringOption;

    return-object v0
.end method

.method static synthetic access$19(Lcom/konka/tvsettings/sound/SRSSettingActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->m_iSpeakerAnalysis:I

    return-void
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/sound/SRSSettingActivity;)Lcom/konka/tvsettings/view/ItemStringOption;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemTruebass:Lcom/konka/tvsettings/view/ItemStringOption;

    return-object v0
.end method

.method static synthetic access$20(Lcom/konka/tvsettings/sound/SRSSettingActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->m_iSpeakerAnalysis:I

    return v0
.end method

.method static synthetic access$21(Lcom/konka/tvsettings/sound/SRSSettingActivity;)Lcom/konka/tvsettings/view/ItemStringOption;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemTrubassControl:Lcom/konka/tvsettings/view/ItemStringOption;

    return-object v0
.end method

.method static synthetic access$22(Lcom/konka/tvsettings/sound/SRSSettingActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->m_iTrubassControl:I

    return-void
.end method

.method static synthetic access$23(Lcom/konka/tvsettings/sound/SRSSettingActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->m_iTrubassControl:I

    return v0
.end method

.method static synthetic access$24(Lcom/konka/tvsettings/sound/SRSSettingActivity;)Lcom/konka/tvsettings/view/ItemStringOption;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemDCControl:Lcom/konka/tvsettings/view/ItemStringOption;

    return-object v0
.end method

.method static synthetic access$25(Lcom/konka/tvsettings/sound/SRSSettingActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->m_iDCControl:I

    return-void
.end method

.method static synthetic access$26(Lcom/konka/tvsettings/sound/SRSSettingActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->m_iDCControl:I

    return v0
.end method

.method static synthetic access$27(Lcom/konka/tvsettings/sound/SRSSettingActivity;)Lcom/konka/tvsettings/view/ItemStringOption;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemDefinationControl:Lcom/konka/tvsettings/view/ItemStringOption;

    return-object v0
.end method

.method static synthetic access$28(Lcom/konka/tvsettings/sound/SRSSettingActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->m_iDefinationControl:I

    return-void
.end method

.method static synthetic access$29(Lcom/konka/tvsettings/sound/SRSSettingActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->m_iDefinationControl:I

    return v0
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/sound/SRSSettingActivity;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/sound/SRSSettingActivity;->setSRSTrueBass(Z)V

    return-void
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/sound/SRSSettingActivity;)Lcom/konka/tvsettings/view/ItemStringOption;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemDynamicClarity:Lcom/konka/tvsettings/view/ItemStringOption;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/sound/SRSSettingActivity;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/sound/SRSSettingActivity;->setSRSDynamicClarity(Z)V

    return-void
.end method

.method static synthetic access$6(Lcom/konka/tvsettings/sound/SRSSettingActivity;)Lcom/konka/tvsettings/view/ItemStringOption;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemDefinition:Lcom/konka/tvsettings/view/ItemStringOption;

    return-object v0
.end method

.method static synthetic access$7(Lcom/konka/tvsettings/sound/SRSSettingActivity;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/sound/SRSSettingActivity;->setSRSDefination(Z)V

    return-void
.end method

.method static synthetic access$8(Lcom/konka/tvsettings/sound/SRSSettingActivity;)Lcom/konka/tvsettings/view/ItemStringOption;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemInputGain:Lcom/konka/tvsettings/view/ItemStringOption;

    return-object v0
.end method

.method static synthetic access$9(Lcom/konka/tvsettings/sound/SRSSettingActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->m_iInputGain:I

    return-void
.end method

.method private initMenu()V
    .locals 23

    const v1, 0x7f0701e4    # com.konka.tvsettings.R.id.linearlayout_sound_srsmenu

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/sound/SRSSettingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->layoutMenuContainer:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getDataBaseManagerInstance()Lcom/konka/kkinterface/tv/DataBaseDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk;->getSRSOnOff()Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;->ordinal()I

    move-result v6

    new-instance v1, Lcom/konka/tvsettings/sound/SRSSettingActivity$1;

    const v4, 0x7f0a008c    # com.konka.tvsettings.R.string.str_sound_srs_tshd

    const v5, 0x7f0b000c    # com.konka.tvsettings.R.array.str_arr_sound_srscomvals

    move-object/from16 v0, p0

    iget v7, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->iItemWidth:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->iItemHeight:I

    move-object/from16 v2, p0

    move-object/from16 v3, p0

    invoke-direct/range {v1 .. v8}, Lcom/konka/tvsettings/sound/SRSSettingActivity$1;-><init>(Lcom/konka/tvsettings/sound/SRSSettingActivity;Landroid/content/Context;IIIII)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemTSHD:Lcom/konka/tvsettings/view/ItemStringOption;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getDataBaseManagerInstance()Lcom/konka/kkinterface/tv/DataBaseDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk;->getTruebaseOnOff()Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;->ordinal()I

    move-result v12

    new-instance v7, Lcom/konka/tvsettings/sound/SRSSettingActivity$2;

    const v10, 0x7f0a008d    # com.konka.tvsettings.R.string.str_sound_srs_truebass

    const v11, 0x7f0b000c    # com.konka.tvsettings.R.array.str_arr_sound_srscomvals

    move-object/from16 v0, p0

    iget v13, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->iItemWidth:I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->iItemHeight:I

    move-object/from16 v8, p0

    move-object/from16 v9, p0

    invoke-direct/range {v7 .. v14}, Lcom/konka/tvsettings/sound/SRSSettingActivity$2;-><init>(Lcom/konka/tvsettings/sound/SRSSettingActivity;Landroid/content/Context;IIIII)V

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemTruebass:Lcom/konka/tvsettings/view/ItemStringOption;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getDataBaseManagerInstance()Lcom/konka/kkinterface/tv/DataBaseDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk;->getDialogClarityOnOff()Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;->ordinal()I

    move-result v12

    new-instance v7, Lcom/konka/tvsettings/sound/SRSSettingActivity$3;

    const v10, 0x7f0a008e    # com.konka.tvsettings.R.string.str_sound_srs_dynamicclarity

    const v11, 0x7f0b000c    # com.konka.tvsettings.R.array.str_arr_sound_srscomvals

    move-object/from16 v0, p0

    iget v13, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->iItemWidth:I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->iItemHeight:I

    move-object/from16 v8, p0

    move-object/from16 v9, p0

    invoke-direct/range {v7 .. v14}, Lcom/konka/tvsettings/sound/SRSSettingActivity$3;-><init>(Lcom/konka/tvsettings/sound/SRSSettingActivity;Landroid/content/Context;IIIII)V

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemDynamicClarity:Lcom/konka/tvsettings/view/ItemStringOption;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getDataBaseManagerInstance()Lcom/konka/kkinterface/tv/DataBaseDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk;->getDefinitionOnOff()Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EnumSwitchOnOff;->ordinal()I

    move-result v12

    new-instance v7, Lcom/konka/tvsettings/sound/SRSSettingActivity$4;

    const v10, 0x7f0a008f    # com.konka.tvsettings.R.string.str_sound_srs_definition

    const v11, 0x7f0b000c    # com.konka.tvsettings.R.array.str_arr_sound_srscomvals

    move-object/from16 v0, p0

    iget v13, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->iItemWidth:I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->iItemHeight:I

    move-object/from16 v8, p0

    move-object/from16 v9, p0

    invoke-direct/range {v7 .. v14}, Lcom/konka/tvsettings/sound/SRSSettingActivity$4;-><init>(Lcom/konka/tvsettings/sound/SRSSettingActivity;Landroid/content/Context;IIIII)V

    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemDefinition:Lcom/konka/tvsettings/view/ItemStringOption;

    new-instance v22, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    invoke-direct/range {v22 .. v22}, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;-><init>()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getDataBaseManagerInstance()Lcom/konka/kkinterface/tv/DataBaseDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk;->getSRSSet()Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;

    move-result-object v22

    move-object/from16 v0, v22

    iget v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_InputGain:I

    move-object/from16 v0, p0

    iput v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->m_iInputGain:I

    move-object/from16 v0, v22

    iget v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_SurrLevelControl:I

    move-object/from16 v0, p0

    iput v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->m_iSurrLevelControl:I

    move-object/from16 v0, v22

    iget v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_SpeakerAudio:I

    move-object/from16 v0, p0

    iput v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->m_iSpeakAudio:I

    move-object/from16 v0, v22

    iget v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_SpeakerAnalysis:I

    move-object/from16 v0, p0

    iput v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->m_iSpeakerAnalysis:I

    move-object/from16 v0, v22

    iget v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_TrubassControl:I

    move-object/from16 v0, p0

    iput v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->m_iTrubassControl:I

    move-object/from16 v0, v22

    iget v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_DCControl:I

    move-object/from16 v0, p0

    iput v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->m_iDCControl:I

    move-object/from16 v0, v22

    iget v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_DefinitionControl:I

    move-object/from16 v0, p0

    iput v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->m_iDefinationControl:I

    sget-boolean v1, Lcom/konka/tvsettings/common/GlobleVariable;->SRS_DEBUG:Z

    if-eqz v1, :cond_0

    new-instance v13, Lcom/konka/tvsettings/sound/SRSSettingActivity$5;

    const v16, 0x7f0a0108    # com.konka.tvsettings.R.string.srs_inputgain

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->m_iInputGain:I

    move/from16 v17, v0

    const/16 v18, 0x0

    const/16 v19, 0xa

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->iItemWidth:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->iItemHeight:I

    move/from16 v21, v0

    move-object/from16 v14, p0

    move-object/from16 v15, p0

    invoke-direct/range {v13 .. v21}, Lcom/konka/tvsettings/sound/SRSSettingActivity$5;-><init>(Lcom/konka/tvsettings/sound/SRSSettingActivity;Landroid/content/Context;IIIIII)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemInputGain:Lcom/konka/tvsettings/view/ItemStringOption;

    new-instance v13, Lcom/konka/tvsettings/sound/SRSSettingActivity$6;

    const v16, 0x7f0a0109    # com.konka.tvsettings.R.string.srs_surrlevelcontrol

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->m_iSurrLevelControl:I

    move/from16 v17, v0

    const/16 v18, 0x0

    const/16 v19, 0xa

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->iItemWidth:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->iItemHeight:I

    move/from16 v21, v0

    move-object/from16 v14, p0

    move-object/from16 v15, p0

    invoke-direct/range {v13 .. v21}, Lcom/konka/tvsettings/sound/SRSSettingActivity$6;-><init>(Lcom/konka/tvsettings/sound/SRSSettingActivity;Landroid/content/Context;IIIIII)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemSurrLevelControl:Lcom/konka/tvsettings/view/ItemStringOption;

    new-instance v13, Lcom/konka/tvsettings/sound/SRSSettingActivity$7;

    const v16, 0x7f0a010a    # com.konka.tvsettings.R.string.srs_speakeraudio

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->m_iSpeakAudio:I

    move/from16 v17, v0

    const/16 v18, 0x0

    const/16 v19, 0x26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->iItemWidth:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->iItemHeight:I

    move/from16 v21, v0

    move-object/from16 v14, p0

    move-object/from16 v15, p0

    invoke-direct/range {v13 .. v21}, Lcom/konka/tvsettings/sound/SRSSettingActivity$7;-><init>(Lcom/konka/tvsettings/sound/SRSSettingActivity;Landroid/content/Context;IIIIII)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemSpeakAudio:Lcom/konka/tvsettings/view/ItemStringOption;

    new-instance v13, Lcom/konka/tvsettings/sound/SRSSettingActivity$8;

    const v16, 0x7f0a010b    # com.konka.tvsettings.R.string.srs_speakeranalysis

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->m_iSpeakerAnalysis:I

    move/from16 v17, v0

    const/16 v18, 0x0

    const/16 v19, 0x26

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->iItemWidth:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->iItemHeight:I

    move/from16 v21, v0

    move-object/from16 v14, p0

    move-object/from16 v15, p0

    invoke-direct/range {v13 .. v21}, Lcom/konka/tvsettings/sound/SRSSettingActivity$8;-><init>(Lcom/konka/tvsettings/sound/SRSSettingActivity;Landroid/content/Context;IIIIII)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemSpeakerAnalysis:Lcom/konka/tvsettings/view/ItemStringOption;

    new-instance v13, Lcom/konka/tvsettings/sound/SRSSettingActivity$9;

    const v16, 0x7f0a010c    # com.konka.tvsettings.R.string.srs_trubasscontrol

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->m_iTrubassControl:I

    move/from16 v17, v0

    const/16 v18, 0x0

    const/16 v19, 0xa

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->iItemWidth:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->iItemHeight:I

    move/from16 v21, v0

    move-object/from16 v14, p0

    move-object/from16 v15, p0

    invoke-direct/range {v13 .. v21}, Lcom/konka/tvsettings/sound/SRSSettingActivity$9;-><init>(Lcom/konka/tvsettings/sound/SRSSettingActivity;Landroid/content/Context;IIIIII)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemTrubassControl:Lcom/konka/tvsettings/view/ItemStringOption;

    new-instance v13, Lcom/konka/tvsettings/sound/SRSSettingActivity$10;

    const v16, 0x7f0a010d    # com.konka.tvsettings.R.string.srs_dccontrol

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->m_iDCControl:I

    move/from16 v17, v0

    const/16 v18, 0x0

    const/16 v19, 0xa

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->iItemWidth:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->iItemHeight:I

    move/from16 v21, v0

    move-object/from16 v14, p0

    move-object/from16 v15, p0

    invoke-direct/range {v13 .. v21}, Lcom/konka/tvsettings/sound/SRSSettingActivity$10;-><init>(Lcom/konka/tvsettings/sound/SRSSettingActivity;Landroid/content/Context;IIIIII)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemDCControl:Lcom/konka/tvsettings/view/ItemStringOption;

    new-instance v13, Lcom/konka/tvsettings/sound/SRSSettingActivity$11;

    const v16, 0x7f0a010e    # com.konka.tvsettings.R.string.srs_definitioncontrol

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->m_iDefinationControl:I

    move/from16 v17, v0

    const/16 v18, 0x0

    const/16 v19, 0xa

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->iItemWidth:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->iItemHeight:I

    move/from16 v21, v0

    move-object/from16 v14, p0

    move-object/from16 v15, p0

    invoke-direct/range {v13 .. v21}, Lcom/konka/tvsettings/sound/SRSSettingActivity$11;-><init>(Lcom/konka/tvsettings/sound/SRSSettingActivity;Landroid/content/Context;IIIIII)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemDefinationControl:Lcom/konka/tvsettings/view/ItemStringOption;

    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->layoutMenuContainer:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemTSHD:Lcom/konka/tvsettings/view/ItemStringOption;

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->layoutMenuContainer:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemTruebass:Lcom/konka/tvsettings/view/ItemStringOption;

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->layoutMenuContainer:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemDynamicClarity:Lcom/konka/tvsettings/view/ItemStringOption;

    const/4 v3, 0x4

    invoke-virtual {v1, v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->layoutMenuContainer:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemDefinition:Lcom/konka/tvsettings/view/ItemStringOption;

    const/4 v3, 0x5

    invoke-virtual {v1, v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    sget-boolean v1, Lcom/konka/tvsettings/common/GlobleVariable;->SRS_DEBUG:Z

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->layoutMenuContainer:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemInputGain:Lcom/konka/tvsettings/view/ItemStringOption;

    const/4 v3, 0x6

    invoke-virtual {v1, v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->layoutMenuContainer:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemSurrLevelControl:Lcom/konka/tvsettings/view/ItemStringOption;

    const/4 v3, 0x7

    invoke-virtual {v1, v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->layoutMenuContainer:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemSpeakAudio:Lcom/konka/tvsettings/view/ItemStringOption;

    const/16 v3, 0x8

    invoke-virtual {v1, v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->layoutMenuContainer:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemSpeakerAnalysis:Lcom/konka/tvsettings/view/ItemStringOption;

    const/16 v3, 0x9

    invoke-virtual {v1, v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->layoutMenuContainer:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemTrubassControl:Lcom/konka/tvsettings/view/ItemStringOption;

    const/16 v3, 0xa

    invoke-virtual {v1, v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->layoutMenuContainer:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemDCControl:Lcom/konka/tvsettings/view/ItemStringOption;

    const/16 v3, 0xb

    invoke-virtual {v1, v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->layoutMenuContainer:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemDefinationControl:Lcom/konka/tvsettings/view/ItemStringOption;

    const/16 v3, 0xc

    invoke-virtual {v1, v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    :cond_1
    if-nez v6, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemTruebass:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusFbd()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemDynamicClarity:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusFbd()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemDefinition:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusFbd()V

    sget-boolean v1, Lcom/konka/tvsettings/common/GlobleVariable;->SRS_DEBUG:Z

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemInputGain:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusAllowed()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemSurrLevelControl:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusAllowed()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemSpeakAudio:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusAllowed()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemSpeakerAnalysis:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusAllowed()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemTrubassControl:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusAllowed()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemDCControl:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusAllowed()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemDefinationControl:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusAllowed()V

    :cond_2
    :goto_0
    return-void

    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemTruebass:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusAllowed()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemDynamicClarity:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusAllowed()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemDefinition:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusAllowed()V

    sget-boolean v1, Lcom/konka/tvsettings/common/GlobleVariable;->SRS_DEBUG:Z

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemInputGain:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusFbd()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemSurrLevelControl:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusFbd()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemSpeakAudio:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusFbd()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemSpeakerAnalysis:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusFbd()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemTrubassControl:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusFbd()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemDCControl:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusFbd()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemDefinationControl:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v1}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusFbd()V

    goto :goto_0
.end method

.method private setSRSDefination(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/konka/kkinterface/tv/SoundDesk;->setSRSDefination(Z)V

    return-void
.end method

.method private setSRSDynamicClarity(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/konka/kkinterface/tv/SoundDesk;->setSRSDynamicClarity(Z)V

    return-void
.end method

.method private setSRSTSHD(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/konka/kkinterface/tv/SoundDesk;->setSRSTSHD(Z)V

    if-eqz p1, :cond_1

    const-string v0, "set SRS TSHD on"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemTruebass:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v0}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusAllowed()V

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemDynamicClarity:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v0}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusAllowed()V

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemDefinition:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v0}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusAllowed()V

    sget-boolean v0, Lcom/konka/tvsettings/common/GlobleVariable;->SRS_DEBUG:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemInputGain:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v0}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusFbd()V

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemSurrLevelControl:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v0}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusFbd()V

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemSpeakAudio:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v0}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusFbd()V

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemSpeakerAnalysis:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v0}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusFbd()V

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemTrubassControl:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v0}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusFbd()V

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemDCControl:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v0}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusFbd()V

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemDefinationControl:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v0}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusFbd()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "set SRS TSHD off"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemTruebass:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v0}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusFbd()V

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemDynamicClarity:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v0}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusFbd()V

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemDefinition:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v0}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusFbd()V

    sget-boolean v0, Lcom/konka/tvsettings/common/GlobleVariable;->SRS_DEBUG:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemInputGain:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v0}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusAllowed()V

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemSurrLevelControl:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v0}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusAllowed()V

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemSpeakAudio:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v0}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusAllowed()V

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemSpeakerAnalysis:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v0}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusAllowed()V

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemTrubassControl:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v0}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusAllowed()V

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemDCControl:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v0}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusAllowed()V

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->itemDefinationControl:Lcom/konka/tvsettings/view/ItemStringOption;

    invoke-virtual {v0}, Lcom/konka/tvsettings/view/ItemStringOption;->setStatusAllowed()V

    goto :goto_0
.end method

.method private setSRSTrueBass(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/konka/kkinterface/tv/SoundDesk;->setSRSTrueBass(Z)V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f03004d    # com.konka.tvsettings.R.layout.sound_srs_menu

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/sound/SRSSettingActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/sound/SRSSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090009    # com.konka.tvsettings.R.dimen.SettingStringOption_Width

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->iItemWidth:I

    invoke-virtual {p0}, Lcom/konka/tvsettings/sound/SRSSettingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09000a    # com.konka.tvsettings.R.dimen.SettingStringOption_Height

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->iItemHeight:I

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->stPara:Lcom/mstar/android/tvapi/common/vo/AdvancedSoundParameter;

    new-instance v0, Lcom/konka/tvsettings/common/PreferancesTools;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/common/PreferancesTools;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/konka/tvsettings/sound/SRSSettingActivity;->preTools:Lcom/konka/tvsettings/common/PreferancesTools;

    invoke-direct {p0}, Lcom/konka/tvsettings/sound/SRSSettingActivity;->initMenu()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    sparse-switch p1, :sswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    return v2

    :sswitch_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/sound/SRSSettingActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/sound/SRSSettingActivity;->finish()V

    const/4 v2, 0x0

    const v3, 0x7f040005    # com.konka.tvsettings.R.anim.anim_menu_exit

    invoke-virtual {p0, v2, v3}, Lcom/konka/tvsettings/sound/SRSSettingActivity;->overridePendingTransition(II)V

    goto :goto_0

    :sswitch_1
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v2, Lcom/konka/tvsettings/sound/SoundSettingActivity;

    invoke-virtual {v0, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/sound/SRSSettingActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/sound/SRSSettingActivity;->finish()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x52 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    const v0, 0x7f040009    # com.konka.tvsettings.R.anim.anim_zoom_in

    const v1, 0x7f040008    # com.konka.tvsettings.R.anim.anim_right_out

    invoke-virtual {p0, v0, v1}, Lcom/konka/tvsettings/sound/SRSSettingActivity;->overridePendingTransition(II)V

    return-void
.end method
