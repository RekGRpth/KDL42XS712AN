.class Lcom/konka/tvsettings/sound/SoundSettingActivity$8;
.super Lcom/konka/tvsettings/view/ItemStringOption;
.source "SoundSettingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/sound/SoundSettingActivity;->findViews()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/sound/SoundSettingActivity;Landroid/content/Context;IIIII)V
    .locals 7
    .param p2    # Landroid/content/Context;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I

    iput-object p1, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity$8;->this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;

    move-object v0, p0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    move v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/konka/tvsettings/view/ItemStringOption;-><init>(Landroid/content/Context;IIIII)V

    return-void
.end method


# virtual methods
.method public doUpdate()V
    .locals 4

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity$8;->this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;

    # getter for: Lcom/konka/tvsettings/sound/SoundSettingActivity;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v0}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->access$5(Lcom/konka/tvsettings/sound/SoundSettingActivity;)Landroid/media/AudioManager;

    move-result-object v0

    const/4 v1, 0x1

    const/16 v2, -0xa

    invoke-virtual {v0, v1, v2}, Landroid/media/AudioManager;->setMasterMute(ZI)V

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity$8;->this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;

    # getter for: Lcom/konka/tvsettings/sound/SoundSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v0}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->access$1(Lcom/konka/tvsettings/sound/SoundSettingActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity$8;->this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;

    # getter for: Lcom/konka/tvsettings/sound/SoundSettingActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v1}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->access$1(Lcom/konka/tvsettings/sound/SoundSettingActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity$8;->this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;

    # getter for: Lcom/konka/tvsettings/sound/SoundSettingActivity;->itemHDMISound:Lcom/konka/tvsettings/view/ItemStringOption;
    invoke-static {v3}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->access$8(Lcom/konka/tvsettings/sound/SoundSettingActivity;)Lcom/konka/tvsettings/view/ItemStringOption;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/tvsettings/view/ItemStringOption;->getIndex()I

    move-result v3

    aget-object v2, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/SoundDesk;->setHdmiAudioSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/konka/kkinterface/tv/DataBaseDesk$HdmiAudioSource;)Z

    iget-object v0, p0, Lcom/konka/tvsettings/sound/SoundSettingActivity$8;->this$0:Lcom/konka/tvsettings/sound/SoundSettingActivity;

    invoke-virtual {v0}, Lcom/konka/tvsettings/sound/SoundSettingActivity;->unmute()V

    return-void
.end method
