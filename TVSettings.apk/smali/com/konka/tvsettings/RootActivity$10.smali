.class Lcom/konka/tvsettings/RootActivity$10;
.super Ljava/lang/Object;
.source "RootActivity.java"

# interfaces
.implements Lcom/konka/tvsettings/function/USBDiskSelecter$usbListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/RootActivity;->setUsbEjectListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/RootActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/RootActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/RootActivity$10;->this$0:Lcom/konka/tvsettings/RootActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onUSBEject(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    const-string v2, "PVR ========>>>onUSBEject"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/tvsettings/RootActivity$10;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;
    invoke-static {v2}, Lcom/konka/tvsettings/RootActivity;->access$36(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v2, v3, :cond_1

    const-string v2, "RootActivity=====>>>Usbselect return2\n"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/konka/tvsettings/RootActivity$10;->this$0:Lcom/konka/tvsettings/RootActivity;

    invoke-virtual {v2}, Lcom/konka/tvsettings/RootActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/konka/tvsettings/SwitchMenuHelper;->getBestDiskPath(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/konka/tvsettings/RootActivity$10;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v2}, Lcom/konka/tvsettings/RootActivity;->access$37(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/PvrDesk;->isTimeShiftRecording()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/RootActivity$10;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v2}, Lcom/konka/tvsettings/RootActivity;->access$37(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/PvrDesk;->stopTimeShift()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public onUSBMounted(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$10;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$36(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableAlTimeShift()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$10;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$36(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v3, v4, :cond_1

    const-string v3, "RootActivity=====>>>Usbselect return1 \n"

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$10;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->settingManager:Lcom/konka/kkinterface/tv/SettingDesk;
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$4(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/SettingDesk;->getAlTimeShift()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$10;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$37(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/PvrDesk;->isTimeShiftRecording()Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "NO_DISK"

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$10;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$37(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {v3, p1, v4}, Lcom/konka/kkinterface/tv/PvrDesk;->setPVRParas(Ljava/lang/String;S)Z

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$10;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$37(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/PvrDesk;->startTimeShiftRecord()Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "RootActivity=====>>>Start 444...startTimeShiftRecord...>>>["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_SUCCESS:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    if-eq v2, v3, :cond_0

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_SUCCESS_CI_PLUS_COPY_PROTECTION:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    if-eq v2, v3, :cond_0

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->ordinal()I

    move-result v1

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$10;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$37(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/PvrDesk;->stopTimeShift()V

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$10;->this$0:Lcom/konka/tvsettings/RootActivity;

    const-string v4, "PVR Record failed!"

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    const-string v3, "RootActivity=====>>>start timeshift failed \n"

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public onUSBUnmounted(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method
