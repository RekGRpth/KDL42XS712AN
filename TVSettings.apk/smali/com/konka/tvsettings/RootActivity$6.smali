.class Lcom/konka/tvsettings/RootActivity$6;
.super Landroid/os/Handler;
.source "RootActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/RootActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/RootActivity;


# direct methods
.method static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource()[I
    .locals 3

    sget-object v0, Lcom/konka/tvsettings/RootActivity$6;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_28

    :goto_1
    :try_start_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_27

    :goto_2
    :try_start_2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_26

    :goto_3
    :try_start_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_25

    :goto_4
    :try_start_4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_24

    :goto_5
    :try_start_5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS5:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_23

    :goto_6
    :try_start_6
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS6:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_22

    :goto_7
    :try_start_7
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS7:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_21

    :goto_8
    :try_start_8
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS8:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_20

    :goto_9
    :try_start_9
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_1f

    :goto_a
    :try_start_a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1d

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_1e

    :goto_b
    :try_start_b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x26

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_1d

    :goto_c
    :try_start_c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1e

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_1c

    :goto_d
    :try_start_d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1f

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_1b

    :goto_e
    :try_start_e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x20

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_1a

    :goto_f
    :try_start_f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x21

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_19

    :goto_10
    :try_start_10
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x22

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_18

    :goto_11
    :try_start_11
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x18

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_17

    :goto_12
    :try_start_12
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x19

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_16

    :goto_13
    :try_start_13
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1a

    aput v2, v0, v1
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_15

    :goto_14
    :try_start_14
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1b

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_14} :catch_14

    :goto_15
    :try_start_15
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1c

    aput v2, v0, v1
    :try_end_15
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15 .. :try_end_15} :catch_13

    :goto_16
    :try_start_16
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_JPEG:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x25

    aput v2, v0, v1
    :try_end_16
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16 .. :try_end_16} :catch_12

    :goto_17
    :try_start_17
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_KTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x24

    aput v2, v0, v1
    :try_end_17
    .catch Ljava/lang/NoSuchFieldError; {:try_start_17 .. :try_end_17} :catch_11

    :goto_18
    :try_start_18
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x29

    aput v2, v0, v1
    :try_end_18
    .catch Ljava/lang/NoSuchFieldError; {:try_start_18 .. :try_end_18} :catch_10

    :goto_19
    :try_start_19
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x28

    aput v2, v0, v1
    :try_end_19
    .catch Ljava/lang/NoSuchFieldError; {:try_start_19 .. :try_end_19} :catch_f

    :goto_1a
    :try_start_1a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SCART:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x15

    aput v2, v0, v1
    :try_end_1a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a .. :try_end_1a} :catch_e

    :goto_1b
    :try_start_1b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SCART2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x16

    aput v2, v0, v1
    :try_end_1b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1b .. :try_end_1b} :catch_d

    :goto_1c
    :try_start_1c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SCART_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x17

    aput v2, v0, v1
    :try_end_1c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1c .. :try_end_1c} :catch_c

    :goto_1d
    :try_start_1d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x23

    aput v2, v0, v1
    :try_end_1d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1d .. :try_end_1d} :catch_b

    :goto_1e
    :try_start_1e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x27

    aput v2, v0, v1
    :try_end_1e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1e .. :try_end_1e} :catch_a

    :goto_1f
    :try_start_1f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_1f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1f .. :try_end_1f} :catch_9

    :goto_20
    :try_start_20
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_20
    .catch Ljava/lang/NoSuchFieldError; {:try_start_20 .. :try_end_20} :catch_8

    :goto_21
    :try_start_21
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_21
    .catch Ljava/lang/NoSuchFieldError; {:try_start_21 .. :try_end_21} :catch_7

    :goto_22
    :try_start_22
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_22
    .catch Ljava/lang/NoSuchFieldError; {:try_start_22 .. :try_end_22} :catch_6

    :goto_23
    :try_start_23
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_23
    .catch Ljava/lang/NoSuchFieldError; {:try_start_23 .. :try_end_23} :catch_5

    :goto_24
    :try_start_24
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_VGA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_24
    .catch Ljava/lang/NoSuchFieldError; {:try_start_24 .. :try_end_24} :catch_4

    :goto_25
    :try_start_25
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_25
    .catch Ljava/lang/NoSuchFieldError; {:try_start_25 .. :try_end_25} :catch_3

    :goto_26
    :try_start_26
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x12

    aput v2, v0, v1
    :try_end_26
    .catch Ljava/lang/NoSuchFieldError; {:try_start_26 .. :try_end_26} :catch_2

    :goto_27
    :try_start_27
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x13

    aput v2, v0, v1
    :try_end_27
    .catch Ljava/lang/NoSuchFieldError; {:try_start_27 .. :try_end_27} :catch_1

    :goto_28
    :try_start_28
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x14

    aput v2, v0, v1
    :try_end_28
    .catch Ljava/lang/NoSuchFieldError; {:try_start_28 .. :try_end_28} :catch_0

    :goto_29
    sput-object v0, Lcom/konka/tvsettings/RootActivity$6;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_29

    :catch_1
    move-exception v1

    goto :goto_28

    :catch_2
    move-exception v1

    goto :goto_27

    :catch_3
    move-exception v1

    goto :goto_26

    :catch_4
    move-exception v1

    goto :goto_25

    :catch_5
    move-exception v1

    goto :goto_24

    :catch_6
    move-exception v1

    goto :goto_23

    :catch_7
    move-exception v1

    goto :goto_22

    :catch_8
    move-exception v1

    goto :goto_21

    :catch_9
    move-exception v1

    goto :goto_20

    :catch_a
    move-exception v1

    goto :goto_1f

    :catch_b
    move-exception v1

    goto/16 :goto_1e

    :catch_c
    move-exception v1

    goto/16 :goto_1d

    :catch_d
    move-exception v1

    goto/16 :goto_1c

    :catch_e
    move-exception v1

    goto/16 :goto_1b

    :catch_f
    move-exception v1

    goto/16 :goto_1a

    :catch_10
    move-exception v1

    goto/16 :goto_19

    :catch_11
    move-exception v1

    goto/16 :goto_18

    :catch_12
    move-exception v1

    goto/16 :goto_17

    :catch_13
    move-exception v1

    goto/16 :goto_16

    :catch_14
    move-exception v1

    goto/16 :goto_15

    :catch_15
    move-exception v1

    goto/16 :goto_14

    :catch_16
    move-exception v1

    goto/16 :goto_13

    :catch_17
    move-exception v1

    goto/16 :goto_12

    :catch_18
    move-exception v1

    goto/16 :goto_11

    :catch_19
    move-exception v1

    goto/16 :goto_10

    :catch_1a
    move-exception v1

    goto/16 :goto_f

    :catch_1b
    move-exception v1

    goto/16 :goto_e

    :catch_1c
    move-exception v1

    goto/16 :goto_d

    :catch_1d
    move-exception v1

    goto/16 :goto_c

    :catch_1e
    move-exception v1

    goto/16 :goto_b

    :catch_1f
    move-exception v1

    goto/16 :goto_a

    :catch_20
    move-exception v1

    goto/16 :goto_9

    :catch_21
    move-exception v1

    goto/16 :goto_8

    :catch_22
    move-exception v1

    goto/16 :goto_7

    :catch_23
    move-exception v1

    goto/16 :goto_6

    :catch_24
    move-exception v1

    goto/16 :goto_5

    :catch_25
    move-exception v1

    goto/16 :goto_4

    :catch_26
    move-exception v1

    goto/16 :goto_3

    :catch_27
    move-exception v1

    goto/16 :goto_2

    :catch_28
    move-exception v1

    goto/16 :goto_1
.end method

.method constructor <init>(Lcom/konka/tvsettings/RootActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/RootActivity$6;->this$0:Lcom/konka/tvsettings/RootActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1    # Landroid/os/Message;

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$6;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$0(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    invoke-static {}, Lcom/konka/tvsettings/RootActivity$6;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource()[I

    move-result-object v3

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v4

    aget v3, v3, v4

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$6;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_iDTVScreenStatus:I
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$7(Lcom/konka/tvsettings/RootActivity;)I

    move-result v3

    sget-object v4, Lcom/konka/kkinterface/tv/CommonDesk$EnumSignalProgSyncStatus;->E_SIGNALPROC_STABLE_UN_SUPPORT_MODE:Lcom/konka/kkinterface/tv/CommonDesk$EnumSignalProgSyncStatus;

    invoke-virtual {v4}, Lcom/konka/kkinterface/tv/CommonDesk$EnumSignalProgSyncStatus;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$6;->this$0:Lcom/konka/tvsettings/RootActivity;

    invoke-static {v3, v6}, Lcom/konka/tvsettings/RootActivity;->access$8(Lcom/konka/tvsettings/RootActivity;Z)V

    goto :goto_0

    :sswitch_1
    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$6;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_iDTVScreenStatus:I
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$7(Lcom/konka/tvsettings/RootActivity;)I

    move-result v3

    sget-object v4, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_SCRAMBLED_PROGRAM:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    invoke-virtual {v4}, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_1

    sput-boolean v5, Lcom/konka/tvsettings/RootActivity;->CurProBlock:Z

    const-string v3, "m_iDTVScreenStatus: MSRV_DTV_SS_SCRAMBLED_PROGRAM"

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$6;->this$0:Lcom/konka/tvsettings/RootActivity;

    invoke-static {v3, v6}, Lcom/konka/tvsettings/RootActivity;->access$9(Lcom/konka/tvsettings/RootActivity;Z)V

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$6;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_bRootActivityOnTop:Z
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$3(Lcom/konka/tvsettings/RootActivity;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$6;->this$0:Lcom/konka/tvsettings/RootActivity;

    const v4, 0x7f0a0049    # com.konka.tvsettings.R.string.warning_encryptedprogram

    # invokes: Lcom/konka/tvsettings/RootActivity;->ToastWarning(I)V
    invoke-static {v3, v4}, Lcom/konka/tvsettings/RootActivity;->access$10(Lcom/konka/tvsettings/RootActivity;I)V

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$6;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_iDTVScreenStatus:I
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$7(Lcom/konka/tvsettings/RootActivity;)I

    move-result v3

    sget-object v4, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_NO_CI_MODULE:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    invoke-virtual {v4}, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_2

    sput-boolean v5, Lcom/konka/tvsettings/RootActivity;->CurProBlock:Z

    const-string v3, "m_iDTVScreenStatus: MSRV_DTV_SS_NO_CI_MODULE"

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$6;->this$0:Lcom/konka/tvsettings/RootActivity;

    invoke-static {v3, v6}, Lcom/konka/tvsettings/RootActivity;->access$9(Lcom/konka/tvsettings/RootActivity;Z)V

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$6;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_bRootActivityOnTop:Z
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$3(Lcom/konka/tvsettings/RootActivity;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$6;->this$0:Lcom/konka/tvsettings/RootActivity;

    const v4, 0x7f0a0048    # com.konka.tvsettings.R.string.warning_nocicard

    # invokes: Lcom/konka/tvsettings/RootActivity;->ToastWarning(I)V
    invoke-static {v3, v4}, Lcom/konka/tvsettings/RootActivity;->access$10(Lcom/konka/tvsettings/RootActivity;I)V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$6;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_iDTVScreenStatus:I
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$7(Lcom/konka/tvsettings/RootActivity;)I

    move-result v3

    sget-object v4, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_CH_BLOCK:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    invoke-virtual {v4}, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_4

    sput-boolean v6, Lcom/konka/tvsettings/RootActivity;->CurProBlock:Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getParentalcontrolManager()Lcom/mstar/android/tvapi/common/ParentalcontrolManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/ParentalcontrolManager;->isSystemLock()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$6;->this$0:Lcom/konka/tvsettings/RootActivity;

    # invokes: Lcom/konka/tvsettings/RootActivity;->stopPVRByLock()V
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$11(Lcom/konka/tvsettings/RootActivity;)V

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$6;->this$0:Lcom/konka/tvsettings/RootActivity;

    # invokes: Lcom/konka/tvsettings/RootActivity;->startCheckPwdActivity()V
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$12(Lcom/konka/tvsettings/RootActivity;)V

    goto/16 :goto_0

    :cond_3
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v3

    invoke-interface {v3}, Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;->unlockChannel()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0

    :cond_4
    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$6;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_iDTVScreenStatus:I
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$7(Lcom/konka/tvsettings/RootActivity;)I

    move-result v3

    sget-object v4, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_PARENTAL_BLOCK:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    invoke-virtual {v4}, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_6

    sput-boolean v6, Lcom/konka/tvsettings/RootActivity;->CurProBlock:Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getParentalcontrolManager()Lcom/mstar/android/tvapi/common/ParentalcontrolManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/ParentalcontrolManager;->isSystemLock()Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$6;->this$0:Lcom/konka/tvsettings/RootActivity;

    # invokes: Lcom/konka/tvsettings/RootActivity;->stopPVRByLock()V
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$11(Lcom/konka/tvsettings/RootActivity;)V

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$6;->this$0:Lcom/konka/tvsettings/RootActivity;

    # invokes: Lcom/konka/tvsettings/RootActivity;->startCheckPwdActivity()V
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$12(Lcom/konka/tvsettings/RootActivity;)V

    goto/16 :goto_0

    :cond_5
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v3

    invoke-interface {v3}, Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;->unlockChannel()Z
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0

    :cond_6
    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$6;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_iDTVScreenStatus:I
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$7(Lcom/konka/tvsettings/RootActivity;)I

    move-result v3

    sget-object v4, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_AUDIO_ONLY:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    invoke-virtual {v4}, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_7

    sput-boolean v5, Lcom/konka/tvsettings/RootActivity;->CurProBlock:Z

    const-string v3, "m_iDTVScreenStatus: MSRV_DTV_SS_AUDIO_ONLY"

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$6;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_RadioBgView:Landroid/widget/ImageView;
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$13(Lcom/konka/tvsettings/RootActivity;)Landroid/widget/ImageView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ImageView;->getVisibility()I

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$6;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_ScreenSaverTimer:Lcom/konka/tvsettings/common/CountDownTimer;
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$14(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/tvsettings/common/CountDownTimer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/tvsettings/common/CountDownTimer;->isRuning()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$6;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_bRootActivityOnTop:Z
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$3(Lcom/konka/tvsettings/RootActivity;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$6;->this$0:Lcom/konka/tvsettings/RootActivity;

    # invokes: Lcom/konka/tvsettings/RootActivity;->isShowRadio()Z
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$15(Lcom/konka/tvsettings/RootActivity;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$6;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_ScreenSaverTimer:Lcom/konka/tvsettings/common/CountDownTimer;
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$14(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/tvsettings/common/CountDownTimer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/tvsettings/common/CountDownTimer;->stop()V

    const-string v3, "RootActivity"

    const-string v4, "+++++++++++isShowRadio"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$6;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_myHandler:Lcom/konka/tvsettings/RootActivity$MyHandler;
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$5(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/tvsettings/RootActivity$MyHandler;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/tvsettings/RootActivity$6;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->rShowAudioOnlyMenu:Ljava/lang/Runnable;
    invoke-static {v4}, Lcom/konka/tvsettings/RootActivity;->access$16(Lcom/konka/tvsettings/RootActivity;)Ljava/lang/Runnable;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/konka/tvsettings/RootActivity$MyHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$6;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_myHandler:Lcom/konka/tvsettings/RootActivity$MyHandler;
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$5(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/tvsettings/RootActivity$MyHandler;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/tvsettings/RootActivity$6;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->rShowAudioOnlyMenu:Ljava/lang/Runnable;
    invoke-static {v4}, Lcom/konka/tvsettings/RootActivity;->access$16(Lcom/konka/tvsettings/RootActivity;)Ljava/lang/Runnable;

    move-result-object v4

    const-wide/16 v5, 0x3e8

    invoke-virtual {v3, v4, v5, v6}, Lcom/konka/tvsettings/RootActivity$MyHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    :cond_7
    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$6;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_iDTVScreenStatus:I
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$7(Lcom/konka/tvsettings/RootActivity;)I

    move-result v3

    sget-object v4, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_COMMON_VIDEO:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    invoke-virtual {v4}, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_8

    sput-boolean v5, Lcom/konka/tvsettings/RootActivity;->CurProBlock:Z

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$6;->this$0:Lcom/konka/tvsettings/RootActivity;

    invoke-static {v3, v5}, Lcom/konka/tvsettings/RootActivity;->access$9(Lcom/konka/tvsettings/RootActivity;Z)V

    goto/16 :goto_0

    :cond_8
    sput-boolean v5, Lcom/konka/tvsettings/RootActivity;->CurProBlock:Z

    goto/16 :goto_0

    :sswitch_2
    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$6;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_iDTVScreenStatus:I
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$7(Lcom/konka/tvsettings/RootActivity;)I

    move-result v3

    sget-object v4, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->MSRV_DTV_SS_CH_BLOCK:Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;

    invoke-virtual {v4}, Lcom/konka/kkinterface/tv/CommonDesk$EnumScreenMode;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_a

    sput-boolean v6, Lcom/konka/tvsettings/RootActivity;->CurProBlock:Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getParentalcontrolManager()Lcom/mstar/android/tvapi/common/ParentalcontrolManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/ParentalcontrolManager;->isSystemLock()Z

    move-result v3

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/konka/tvsettings/RootActivity$6;->this$0:Lcom/konka/tvsettings/RootActivity;

    # invokes: Lcom/konka/tvsettings/RootActivity;->startCheckPwdActivity()V
    invoke-static {v3}, Lcom/konka/tvsettings/RootActivity;->access$12(Lcom/konka/tvsettings/RootActivity;)V

    goto/16 :goto_0

    :cond_9
    :try_start_2
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v3

    invoke-interface {v3}, Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;->unlockChannel()Z
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0

    :cond_a
    sput-boolean v5, Lcom/konka/tvsettings/RootActivity;->CurProBlock:Z

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x1d -> :sswitch_1
    .end sparse-switch
.end method
