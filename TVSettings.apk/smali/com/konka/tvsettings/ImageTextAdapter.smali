.class public Lcom/konka/tvsettings/ImageTextAdapter;
.super Landroid/widget/BaseAdapter;
.source "ImageTextAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/ImageTextAdapter$Item;,
        Lcom/konka/tvsettings/ImageTextAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field inflater:Landroid/view/LayoutInflater;

.field private mContext:Landroid/content/Context;

.field private mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/tvsettings/ImageTextAdapter$Item;",
            ">;"
        }
    .end annotation
.end field

.field private select:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/ImageTextAdapter;->mItems:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/tvsettings/ImageTextAdapter;->select:I

    iput-object p1, p0, Lcom/konka/tvsettings/ImageTextAdapter;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/konka/tvsettings/ImageTextAdapter;->mContext:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/konka/tvsettings/ImageTextAdapter;->inflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public addItem(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 2
    .param p1    # Ljava/lang/Integer;
    .param p2    # Ljava/lang/Integer;
    .param p3    # Ljava/lang/Integer;

    iget-object v0, p0, Lcom/konka/tvsettings/ImageTextAdapter;->mItems:Ljava/util/ArrayList;

    new-instance v1, Lcom/konka/tvsettings/ImageTextAdapter$Item;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/konka/tvsettings/ImageTextAdapter$Item;-><init>(Lcom/konka/tvsettings/ImageTextAdapter;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/ImageTextAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/tvsettings/ImageTextAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    if-nez p2, :cond_0

    new-instance v0, Lcom/konka/tvsettings/ImageTextAdapter$ViewHolder;

    invoke-direct {v0}, Lcom/konka/tvsettings/ImageTextAdapter$ViewHolder;-><init>()V

    iget-object v1, p0, Lcom/konka/tvsettings/ImageTextAdapter;->inflater:Landroid/view/LayoutInflater;

    const v2, 0x7f03001a    # com.konka.tvsettings.R.layout.image_text_item

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    const v1, 0x7f0700c6    # com.konka.tvsettings.R.id.ItemText

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/konka/tvsettings/ImageTextAdapter$ViewHolder;->text:Landroid/widget/TextView;

    const v1, 0x7f0700c5    # com.konka.tvsettings.R.id.ItemImage

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v0, Lcom/konka/tvsettings/ImageTextAdapter$ViewHolder;->icon:Landroid/widget/ImageView;

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    iget v1, p0, Lcom/konka/tvsettings/ImageTextAdapter;->select:I

    if-ne p1, v1, :cond_1

    iget-object v2, v0, Lcom/konka/tvsettings/ImageTextAdapter$ViewHolder;->text:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/ImageTextAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/tvsettings/ImageTextAdapter$Item;

    iget-object v1, v1, Lcom/konka/tvsettings/ImageTextAdapter$Item;->ItemText:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, v0, Lcom/konka/tvsettings/ImageTextAdapter$ViewHolder;->text:Landroid/widget/TextView;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v2, v0, Lcom/konka/tvsettings/ImageTextAdapter$ViewHolder;->icon:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/konka/tvsettings/ImageTextAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/tvsettings/ImageTextAdapter$Item;

    iget-object v1, v1, Lcom/konka/tvsettings/ImageTextAdapter$Item;->ItemFocusImage:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    :goto_1
    return-object p2

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/ImageTextAdapter$ViewHolder;

    goto :goto_0

    :cond_1
    iget-object v2, v0, Lcom/konka/tvsettings/ImageTextAdapter$ViewHolder;->text:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/ImageTextAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/tvsettings/ImageTextAdapter$Item;

    iget-object v1, v1, Lcom/konka/tvsettings/ImageTextAdapter$Item;->ItemText:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, v0, Lcom/konka/tvsettings/ImageTextAdapter$ViewHolder;->text:Landroid/widget/TextView;

    const v2, -0x777778

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v2, v0, Lcom/konka/tvsettings/ImageTextAdapter$ViewHolder;->icon:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/konka/tvsettings/ImageTextAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/tvsettings/ImageTextAdapter$Item;

    iget-object v1, v1, Lcom/konka/tvsettings/ImageTextAdapter$Item;->ItemUnfocusImage:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_1
.end method

.method public notifyDataSetChanged(I)V
    .locals 2
    .param p1    # I

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "notifyDataSetChanged=="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iput p1, p0, Lcom/konka/tvsettings/ImageTextAdapter;->select:I

    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method
