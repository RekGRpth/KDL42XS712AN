.class public final enum Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;
.super Ljava/lang/Enum;
.source "TVRootApp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/TVRootApp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumTextStatus"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

.field public static final enum TEXT_STATUS_GINGA:Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

.field public static final enum TEXT_STATUS_HBBTV:Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

.field public static final enum TEXT_STATUS_MHEG5:Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

.field public static final enum TEXT_STATUS_NONE:Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

.field public static final enum TEXT_STATUS_TTX:Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

    const-string v1, "TEXT_STATUS_TTX"

    invoke-direct {v0, v1, v2}, Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;->TEXT_STATUS_TTX:Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

    new-instance v0, Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

    const-string v1, "TEXT_STATUS_MHEG5"

    invoke-direct {v0, v1, v3}, Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;->TEXT_STATUS_MHEG5:Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

    new-instance v0, Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

    const-string v1, "TEXT_STATUS_HBBTV"

    invoke-direct {v0, v1, v4}, Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;->TEXT_STATUS_HBBTV:Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

    new-instance v0, Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

    const-string v1, "TEXT_STATUS_GINGA"

    invoke-direct {v0, v1, v5}, Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;->TEXT_STATUS_GINGA:Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

    new-instance v0, Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

    const-string v1, "TEXT_STATUS_NONE"

    invoke-direct {v0, v1, v6}, Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;->TEXT_STATUS_NONE:Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

    sget-object v1, Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;->TEXT_STATUS_TTX:Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;->TEXT_STATUS_MHEG5:Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;->TEXT_STATUS_HBBTV:Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;->TEXT_STATUS_GINGA:Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

    aput-object v1, v0, v5

    sget-object v1, Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;->TEXT_STATUS_NONE:Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

    aput-object v1, v0, v6

    sput-object v0, Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;->ENUM$VALUES:[Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;
    .locals 1

    const-class v0, Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

    return-object v0
.end method

.method public static values()[Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;->ENUM$VALUES:[Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/tvsettings/TVRootApp$EnumTextStatus;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
