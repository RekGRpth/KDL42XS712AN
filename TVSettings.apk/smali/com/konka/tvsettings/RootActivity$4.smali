.class Lcom/konka/tvsettings/RootActivity$4;
.super Ljava/lang/Object;
.source "RootActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/RootActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/RootActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/RootActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/RootActivity$4;->this$0:Lcom/konka/tvsettings/RootActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity$4;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_bRootActivityOnTop:Z
    invoke-static {v0}, Lcom/konka/tvsettings/RootActivity;->access$3(Lcom/konka/tvsettings/RootActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity$4;->this$0:Lcom/konka/tvsettings/RootActivity;

    const/16 v1, 0x2002

    invoke-static {v0, v1}, Lcom/konka/tvsettings/SwitchMenuHelper;->goToMenuById(Landroid/content/Context;I)V

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity$4;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->settingManager:Lcom/konka/kkinterface/tv/SettingDesk;
    invoke-static {v0}, Lcom/konka/tvsettings/RootActivity;->access$4(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/SettingDesk;->getAudioCloseBacklight()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity$4;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_myHandler:Lcom/konka/tvsettings/RootActivity$MyHandler;
    invoke-static {v0}, Lcom/konka/tvsettings/RootActivity;->access$5(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/tvsettings/RootActivity$MyHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/RootActivity$4;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->disableBacklightRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/konka/tvsettings/RootActivity;->access$6(Lcom/konka/tvsettings/RootActivity;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/RootActivity$MyHandler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/konka/tvsettings/RootActivity$4;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->m_myHandler:Lcom/konka/tvsettings/RootActivity$MyHandler;
    invoke-static {v0}, Lcom/konka/tvsettings/RootActivity;->access$5(Lcom/konka/tvsettings/RootActivity;)Lcom/konka/tvsettings/RootActivity$MyHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/RootActivity$4;->this$0:Lcom/konka/tvsettings/RootActivity;

    # getter for: Lcom/konka/tvsettings/RootActivity;->disableBacklightRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/konka/tvsettings/RootActivity;->access$6(Lcom/konka/tvsettings/RootActivity;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v1, v2, v3}, Lcom/konka/tvsettings/RootActivity$MyHandler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method
