.class public Lcom/konka/tvsettings/MainMenuActivity;
.super Landroid/app/Activity;
.source "MainMenuActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/MainMenuActivity$FallAnimationListener;,
        Lcom/konka/tvsettings/MainMenuActivity$MainMenuHandler;
    }
.end annotation


# static fields
.field private static final EXIT_MENU:I = 0x10

.field private static LockChannel:I = 0x0

.field private static final POST_TASK:I = 0x20

.field private static final SETTING_3D:I = 0x4

.field private static final SETTING_CIINFO:I = 0x7

.field private static final SETTING_FUNCTION:I = 0x5

.field private static final SETTING_IMAGE:I = 0x1

.field private static final SETTING_INPUTSOURCE:I = 0x3

.field private static final SETTING_PROGRAM:I = 0x2

.field private static final SETTING_SOUND:I = 0x0

.field private static final SETTING_TIME:I = 0x6

.field private static final SETTING_WEIBO:I = 0x63


# instance fields
.field private b_focusOnWeibo:Z

.field cd:Lcom/konka/kkinterface/tv/ChannelDesk;

.field private gridView:Landroid/widget/GridView;

.field private hotelEnable:Z

.field private ll_focusBg:Landroid/widget/LinearLayout;

.field private mFocusImageIDs:[Ljava/lang/Integer;

.field private mImageIDs:[Ljava/lang/Integer;

.field private mStringIDs:[Ljava/lang/Integer;

.field public m_adapter:Lcom/konka/tvsettings/ImageTextAdapter;

.field private m_bIsFirstShow:Z

.field private m_bRiseUpEnable:Z

.field private m_iBackdoorCode:I

.field private mainPanel:Lcom/konka/tvsettings/MovingPanel;

.field private main_container:Landroid/widget/LinearLayout;

.field private mainmenuHandler:Lcom/konka/tvsettings/MainMenuActivity$MainMenuHandler;

.field private myHandler:Landroid/os/Handler;

.field private rootApp:Lcom/konka/tvsettings/TVRootApp;

.field serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/konka/tvsettings/MainMenuActivity;->LockChannel:I

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Integer;

    const v1, 0x7f020046    # com.konka.tvsettings.R.drawable.com_icon_button_sound_s

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    const v1, 0x7f020042    # com.konka.tvsettings.R.drawable.com_icon_button_images_s

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    const v1, 0x7f020044    # com.konka.tvsettings.R.drawable.com_icon_button_program_s

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v5

    const v1, 0x7f020048    # com.konka.tvsettings.R.drawable.com_icon_button_sources_s

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    const v1, 0x7f02003c    # com.konka.tvsettings.R.drawable.com_icon_button_3d_s

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const v2, 0x7f020040    # com.konka.tvsettings.R.drawable.com_icon_button_function_s

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const v2, 0x7f02004a    # com.konka.tvsettings.R.drawable.com_icon_button_time_s

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/konka/tvsettings/MainMenuActivity;->mFocusImageIDs:[Ljava/lang/Integer;

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Integer;

    const v1, 0x7f020047    # com.konka.tvsettings.R.drawable.com_icon_button_sound_uns

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    const v1, 0x7f020043    # com.konka.tvsettings.R.drawable.com_icon_button_images_uns

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    const v1, 0x7f020045    # com.konka.tvsettings.R.drawable.com_icon_button_program_uns

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v5

    const v1, 0x7f020049    # com.konka.tvsettings.R.drawable.com_icon_button_sources_uns

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    const v1, 0x7f02003d    # com.konka.tvsettings.R.drawable.com_icon_button_3d_uns

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const v2, 0x7f020041    # com.konka.tvsettings.R.drawable.com_icon_button_function_uns

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const v2, 0x7f02004b    # com.konka.tvsettings.R.drawable.com_icon_button_time_uns

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/konka/tvsettings/MainMenuActivity;->mImageIDs:[Ljava/lang/Integer;

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Integer;

    const v1, 0x7f0a0008    # com.konka.tvsettings.R.string.set_sound

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    const v1, 0x7f0a0009    # com.konka.tvsettings.R.string.set_image

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    const v1, 0x7f0a000a    # com.konka.tvsettings.R.string.set_program

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v5

    const v1, 0x7f0a000b    # com.konka.tvsettings.R.string.set_source

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    const v1, 0x7f0a000c    # com.konka.tvsettings.R.string.set_3d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const v2, 0x7f0a0010    # com.konka.tvsettings.R.string.set_function

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const v2, 0x7f0a0011    # com.konka.tvsettings.R.string.set_time

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/konka/tvsettings/MainMenuActivity;->mStringIDs:[Ljava/lang/Integer;

    iput-boolean v3, p0, Lcom/konka/tvsettings/MainMenuActivity;->b_focusOnWeibo:Z

    iput v3, p0, Lcom/konka/tvsettings/MainMenuActivity;->m_iBackdoorCode:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/tvsettings/MainMenuActivity;->mainmenuHandler:Lcom/konka/tvsettings/MainMenuActivity$MainMenuHandler;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/tvsettings/MainMenuActivity;->rootApp:Lcom/konka/tvsettings/TVRootApp;

    iput-boolean v3, p0, Lcom/konka/tvsettings/MainMenuActivity;->m_bIsFirstShow:Z

    iput-boolean v3, p0, Lcom/konka/tvsettings/MainMenuActivity;->m_bRiseUpEnable:Z

    iput-boolean v3, p0, Lcom/konka/tvsettings/MainMenuActivity;->hotelEnable:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/tvsettings/MainMenuActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    new-instance v0, Lcom/konka/tvsettings/MainMenuActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/MainMenuActivity$1;-><init>(Lcom/konka/tvsettings/MainMenuActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/MainMenuActivity;->myHandler:Landroid/os/Handler;

    return-void
.end method

.method private CreatePanel()V
    .locals 9

    const/4 v7, 0x1

    const/4 v6, 0x0

    new-instance v1, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v1}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v4, "/customercfg/panel/panel.ini"

    invoke-virtual {v1, v4}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    const-string v4, "panel:b3DPanel"

    const-string v5, "0"

    invoke-virtual {v1, v4, v5}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x7

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableMonitor()Z

    move-result v4

    if-eqz v4, :cond_0

    sput v7, Lcom/konka/tvsettings/MainMenuActivity;->LockChannel:I

    :cond_0
    sget v4, Lcom/konka/tvsettings/MainMenuActivity;->LockChannel:I

    if-ne v4, v7, :cond_1

    add-int/lit8 v3, v3, -0x1

    :cond_1
    const-string v4, "0"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    add-int/lit8 v3, v3, -0x1

    :cond_2
    new-instance v4, Lcom/konka/tvsettings/MovingPanel;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5, v3}, Lcom/konka/tvsettings/MovingPanel;-><init>(Landroid/app/Activity;Landroid/view/View;I)V

    iput-object v4, p0, Lcom/konka/tvsettings/MainMenuActivity;->mainPanel:Lcom/konka/tvsettings/MovingPanel;

    const v4, 0x7f0700df    # com.konka.tvsettings.R.id.gridview_mainpanel

    invoke-virtual {p0, v4}, Lcom/konka/tvsettings/MainMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/GridView;

    iput-object v4, p0, Lcom/konka/tvsettings/MainMenuActivity;->gridView:Landroid/widget/GridView;

    iget-object v4, p0, Lcom/konka/tvsettings/MainMenuActivity;->gridView:Landroid/widget/GridView;

    invoke-virtual {v4, v6}, Landroid/widget/GridView;->setFocusable(Z)V

    iget-object v4, p0, Lcom/konka/tvsettings/MainMenuActivity;->gridView:Landroid/widget/GridView;

    new-instance v5, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v5, v6}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v4, v5}, Landroid/widget/GridView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    new-instance v4, Lcom/konka/tvsettings/ImageTextAdapter;

    invoke-direct {v4, p0}, Lcom/konka/tvsettings/ImageTextAdapter;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/konka/tvsettings/MainMenuActivity;->m_adapter:Lcom/konka/tvsettings/ImageTextAdapter;

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v3, :cond_3

    iget-object v4, p0, Lcom/konka/tvsettings/MainMenuActivity;->gridView:Landroid/widget/GridView;

    iget-object v5, p0, Lcom/konka/tvsettings/MainMenuActivity;->m_adapter:Lcom/konka/tvsettings/ImageTextAdapter;

    invoke-virtual {v4, v5}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void

    :cond_3
    sget v4, Lcom/konka/tvsettings/MainMenuActivity;->LockChannel:I

    if-nez v4, :cond_6

    const/4 v4, 0x4

    if-lt v0, v4, :cond_5

    const-string v4, "0"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/konka/tvsettings/MainMenuActivity;->m_adapter:Lcom/konka/tvsettings/ImageTextAdapter;

    iget-object v5, p0, Lcom/konka/tvsettings/MainMenuActivity;->mImageIDs:[Ljava/lang/Integer;

    add-int/lit8 v6, v0, 0x1

    aget-object v5, v5, v6

    iget-object v6, p0, Lcom/konka/tvsettings/MainMenuActivity;->mFocusImageIDs:[Ljava/lang/Integer;

    add-int/lit8 v7, v0, 0x1

    aget-object v6, v6, v7

    iget-object v7, p0, Lcom/konka/tvsettings/MainMenuActivity;->mStringIDs:[Ljava/lang/Integer;

    add-int/lit8 v8, v0, 0x1

    aget-object v7, v7, v8

    invoke-virtual {v4, v5, v6, v7}, Lcom/konka/tvsettings/ImageTextAdapter;->addItem(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    iget-object v4, p0, Lcom/konka/tvsettings/MainMenuActivity;->m_adapter:Lcom/konka/tvsettings/ImageTextAdapter;

    iget-object v5, p0, Lcom/konka/tvsettings/MainMenuActivity;->mImageIDs:[Ljava/lang/Integer;

    aget-object v5, v5, v0

    iget-object v6, p0, Lcom/konka/tvsettings/MainMenuActivity;->mFocusImageIDs:[Ljava/lang/Integer;

    aget-object v6, v6, v0

    iget-object v7, p0, Lcom/konka/tvsettings/MainMenuActivity;->mStringIDs:[Ljava/lang/Integer;

    aget-object v7, v7, v0

    invoke-virtual {v4, v5, v6, v7}, Lcom/konka/tvsettings/ImageTextAdapter;->addItem(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    goto :goto_1

    :cond_5
    iget-object v4, p0, Lcom/konka/tvsettings/MainMenuActivity;->m_adapter:Lcom/konka/tvsettings/ImageTextAdapter;

    iget-object v5, p0, Lcom/konka/tvsettings/MainMenuActivity;->mImageIDs:[Ljava/lang/Integer;

    aget-object v5, v5, v0

    iget-object v6, p0, Lcom/konka/tvsettings/MainMenuActivity;->mFocusImageIDs:[Ljava/lang/Integer;

    aget-object v6, v6, v0

    iget-object v7, p0, Lcom/konka/tvsettings/MainMenuActivity;->mStringIDs:[Ljava/lang/Integer;

    aget-object v7, v7, v0

    invoke-virtual {v4, v5, v6, v7}, Lcom/konka/tvsettings/ImageTextAdapter;->addItem(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    goto :goto_1

    :cond_6
    const/4 v4, 0x3

    if-lt v0, v4, :cond_8

    const-string v4, "0"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/konka/tvsettings/MainMenuActivity;->m_adapter:Lcom/konka/tvsettings/ImageTextAdapter;

    iget-object v5, p0, Lcom/konka/tvsettings/MainMenuActivity;->mImageIDs:[Ljava/lang/Integer;

    add-int/lit8 v6, v0, 0x2

    aget-object v5, v5, v6

    iget-object v6, p0, Lcom/konka/tvsettings/MainMenuActivity;->mFocusImageIDs:[Ljava/lang/Integer;

    add-int/lit8 v7, v0, 0x2

    aget-object v6, v6, v7

    iget-object v7, p0, Lcom/konka/tvsettings/MainMenuActivity;->mStringIDs:[Ljava/lang/Integer;

    add-int/lit8 v8, v0, 0x2

    aget-object v7, v7, v8

    invoke-virtual {v4, v5, v6, v7}, Lcom/konka/tvsettings/ImageTextAdapter;->addItem(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    goto :goto_1

    :cond_7
    iget-object v4, p0, Lcom/konka/tvsettings/MainMenuActivity;->m_adapter:Lcom/konka/tvsettings/ImageTextAdapter;

    iget-object v5, p0, Lcom/konka/tvsettings/MainMenuActivity;->mImageIDs:[Ljava/lang/Integer;

    add-int/lit8 v6, v0, 0x1

    aget-object v5, v5, v6

    iget-object v6, p0, Lcom/konka/tvsettings/MainMenuActivity;->mFocusImageIDs:[Ljava/lang/Integer;

    add-int/lit8 v7, v0, 0x1

    aget-object v6, v6, v7

    iget-object v7, p0, Lcom/konka/tvsettings/MainMenuActivity;->mStringIDs:[Ljava/lang/Integer;

    add-int/lit8 v8, v0, 0x1

    aget-object v7, v7, v8

    invoke-virtual {v4, v5, v6, v7}, Lcom/konka/tvsettings/ImageTextAdapter;->addItem(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    goto :goto_1

    :cond_8
    const/4 v4, 0x2

    if-lt v0, v4, :cond_9

    iget-object v4, p0, Lcom/konka/tvsettings/MainMenuActivity;->m_adapter:Lcom/konka/tvsettings/ImageTextAdapter;

    iget-object v5, p0, Lcom/konka/tvsettings/MainMenuActivity;->mImageIDs:[Ljava/lang/Integer;

    add-int/lit8 v6, v0, 0x1

    aget-object v5, v5, v6

    iget-object v6, p0, Lcom/konka/tvsettings/MainMenuActivity;->mFocusImageIDs:[Ljava/lang/Integer;

    add-int/lit8 v7, v0, 0x1

    aget-object v6, v6, v7

    iget-object v7, p0, Lcom/konka/tvsettings/MainMenuActivity;->mStringIDs:[Ljava/lang/Integer;

    add-int/lit8 v8, v0, 0x1

    aget-object v7, v7, v8

    invoke-virtual {v4, v5, v6, v7}, Lcom/konka/tvsettings/ImageTextAdapter;->addItem(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    goto :goto_1

    :cond_9
    iget-object v4, p0, Lcom/konka/tvsettings/MainMenuActivity;->m_adapter:Lcom/konka/tvsettings/ImageTextAdapter;

    iget-object v5, p0, Lcom/konka/tvsettings/MainMenuActivity;->mImageIDs:[Ljava/lang/Integer;

    aget-object v5, v5, v0

    iget-object v6, p0, Lcom/konka/tvsettings/MainMenuActivity;->mFocusImageIDs:[Ljava/lang/Integer;

    aget-object v6, v6, v0

    iget-object v7, p0, Lcom/konka/tvsettings/MainMenuActivity;->mStringIDs:[Ljava/lang/Integer;

    aget-object v7, v7, v0

    invoke-virtual {v4, v5, v6, v7}, Lcom/konka/tvsettings/ImageTextAdapter;->addItem(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;)V

    goto/16 :goto_1
.end method

.method private IsPipModeEnabled()Z
    .locals 5

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/CommonDesk;->getPipInfo()Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;

    move-result-object v1

    iget v4, v1, Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;->enablePip:I

    if-eqz v4, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tv/TvPipPopManager;->isPipModeEnabled()Z

    move-result v4

    if-eqz v4, :cond_1

    :goto_1
    return v2

    :cond_0
    move v0, v3

    goto :goto_0

    :cond_1
    move v2, v3

    goto :goto_1
.end method

.method private IsReadytoMove()Z
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/MainMenuActivity;->mainPanel:Lcom/konka/tvsettings/MovingPanel;

    invoke-virtual {v0}, Lcom/konka/tvsettings/MovingPanel;->isReadytoMove()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const-string v0, "The menu is not ready to move"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/MainMenuActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/tvsettings/MainMenuActivity;->m_bRiseUpEnable:Z

    return-void
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/MainMenuActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/MainMenuActivity;->startMenu(I)V

    return-void
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/MainMenuActivity;)Landroid/widget/GridView;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/MainMenuActivity;->gridView:Landroid/widget/GridView;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/MainMenuActivity;)Z
    .locals 1

    invoke-direct {p0}, Lcom/konka/tvsettings/MainMenuActivity;->IsReadytoMove()Z

    move-result v0

    return v0
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/MainMenuActivity;)Lcom/konka/tvsettings/MovingPanel;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/MainMenuActivity;->mainPanel:Lcom/konka/tvsettings/MovingPanel;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/MainMenuActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/MainMenuActivity;->b_focusOnWeibo:Z

    return v0
.end method

.method static synthetic access$6(Lcom/konka/tvsettings/MainMenuActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/MainMenuActivity;->focustoSettingMenu(I)V

    return-void
.end method

.method private delayToDo(I)V
    .locals 4
    .param p1    # I

    const/16 v2, 0x20

    iget-object v1, p0, Lcom/konka/tvsettings/MainMenuActivity;->mainmenuHandler:Lcom/konka/tvsettings/MainMenuActivity$MainMenuHandler;

    invoke-virtual {v1}, Lcom/konka/tvsettings/MainMenuActivity$MainMenuHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput v2, v0, Landroid/os/Message;->what:I

    iput p1, v0, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lcom/konka/tvsettings/MainMenuActivity;->mainmenuHandler:Lcom/konka/tvsettings/MainMenuActivity$MainMenuHandler;

    invoke-virtual {v1, v2}, Lcom/konka/tvsettings/MainMenuActivity$MainMenuHandler;->removeMessages(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/MainMenuActivity;->mainmenuHandler:Lcom/konka/tvsettings/MainMenuActivity$MainMenuHandler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v0, v2, v3}, Lcom/konka/tvsettings/MainMenuActivity$MainMenuHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method private focustoSettingMenu(I)V
    .locals 3
    .param p1    # I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/tvsettings/MainMenuActivity;->b_focusOnWeibo:Z

    iget-object v0, p0, Lcom/konka/tvsettings/MainMenuActivity;->ll_focusBg:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const v2, -0x87e2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/konka/tvsettings/MainMenuActivity;->m_adapter:Lcom/konka/tvsettings/ImageTextAdapter;

    invoke-virtual {v0, p1}, Lcom/konka/tvsettings/ImageTextAdapter;->notifyDataSetChanged(I)V

    return-void
.end method

.method private showSettingMenu(Landroid/content/Intent;)V
    .locals 9
    .param p1    # Landroid/content/Intent;

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-string v1, "menu_name"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v1, "menu_name"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "menuName = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const-string v1, "menu_3d"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/tvsettings/MainMenuActivity;->mainPanel:Lcom/konka/tvsettings/MovingPanel;

    invoke-virtual {v1, v8}, Lcom/konka/tvsettings/MovingPanel;->focusToPostion(I)V

    invoke-direct {p0, v8}, Lcom/konka/tvsettings/MainMenuActivity;->delayToDo(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "menu_channel"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/konka/tvsettings/MainMenuActivity;->mainPanel:Lcom/konka/tvsettings/MovingPanel;

    invoke-virtual {v1, v6}, Lcom/konka/tvsettings/MovingPanel;->focusToPostion(I)V

    invoke-direct {p0, v6}, Lcom/konka/tvsettings/MainMenuActivity;->delayToDo(I)V

    goto :goto_0

    :cond_2
    const-string v1, "menu_picture"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/konka/tvsettings/MainMenuActivity;->mainPanel:Lcom/konka/tvsettings/MovingPanel;

    invoke-virtual {v1, v5}, Lcom/konka/tvsettings/MovingPanel;->focusToPostion(I)V

    invoke-direct {p0, v5}, Lcom/konka/tvsettings/MainMenuActivity;->delayToDo(I)V

    goto :goto_0

    :cond_3
    const-string v1, "menu_sound"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/konka/tvsettings/MainMenuActivity;->mainPanel:Lcom/konka/tvsettings/MovingPanel;

    invoke-virtual {v1, v4}, Lcom/konka/tvsettings/MovingPanel;->focusToPostion(I)V

    invoke-direct {p0, v4}, Lcom/konka/tvsettings/MainMenuActivity;->delayToDo(I)V

    goto :goto_0

    :cond_4
    const-string v1, "menu_source"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/konka/tvsettings/MainMenuActivity;->mainPanel:Lcom/konka/tvsettings/MovingPanel;

    invoke-virtual {v1, v7}, Lcom/konka/tvsettings/MovingPanel;->focusToPostion(I)V

    invoke-direct {p0, v7}, Lcom/konka/tvsettings/MainMenuActivity;->delayToDo(I)V

    goto :goto_0

    :cond_5
    const-string v1, "menu_function"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/konka/tvsettings/MainMenuActivity;->mainPanel:Lcom/konka/tvsettings/MovingPanel;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/konka/tvsettings/MovingPanel;->focusToPostion(I)V

    const/4 v1, 0x5

    invoke-direct {p0, v1}, Lcom/konka/tvsettings/MainMenuActivity;->delayToDo(I)V

    goto :goto_0

    :cond_6
    const-string v1, "menu_time"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/MainMenuActivity;->mainPanel:Lcom/konka/tvsettings/MovingPanel;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lcom/konka/tvsettings/MovingPanel;->focusToPostion(I)V

    const/4 v1, 0x6

    invoke-direct {p0, v1}, Lcom/konka/tvsettings/MainMenuActivity;->delayToDo(I)V

    goto :goto_0
.end method

.method private startFallAnimation()V
    .locals 3

    const v1, 0x7f040002    # com.konka.tvsettings.R.anim.anim_bottom_fall

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    new-instance v1, Lcom/konka/tvsettings/MainMenuActivity$FallAnimationListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/konka/tvsettings/MainMenuActivity$FallAnimationListener;-><init>(Lcom/konka/tvsettings/MainMenuActivity;Lcom/konka/tvsettings/MainMenuActivity$FallAnimationListener;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    iget-object v1, p0, Lcom/konka/tvsettings/MainMenuActivity;->main_container:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method private startMenu(I)V
    .locals 8
    .param p1    # I

    const v7, 0x7f0a00b0    # com.konka.tvsettings.R.string.str_zoom_warm

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    new-instance v2, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v2}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v5, "/customercfg/panel/panel.ini"

    invoke-virtual {v2, v5}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    const-string v5, "panel:b3DPanel"

    const-string v6, "0"

    invoke-virtual {v2, v5, v6}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/konka/tvsettings/MainMenuActivity;->LockChannel:I

    if-nez v5, :cond_1

    const/4 v5, 0x4

    if-lt p1, v5, :cond_0

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    add-int/lit8 p1, p1, 0x1

    :cond_0
    :goto_0
    sparse-switch p1, :sswitch_data_0

    :goto_1
    return-void

    :cond_1
    const/4 v5, 0x3

    if-lt p1, v5, :cond_2

    add-int/lit8 p1, p1, 0x1

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_2
    const/4 v5, 0x2

    if-lt p1, v5, :cond_0

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :sswitch_0
    const-class v5, Lcom/konka/tvsettings/sound/SoundSettingActivity;

    invoke-virtual {v3, p0, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/MainMenuActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    :sswitch_1
    const-class v5, Lcom/konka/tvsettings/picture/PictureSettingActivity;

    invoke-virtual {v3, p0, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/MainMenuActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    :sswitch_2
    const-class v5, Lcom/konka/tvsettings/channel/ProgramSettingMain;

    invoke-virtual {v3, p0, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/MainMenuActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    :sswitch_3
    const-class v5, Lcom/konka/tvsettings/input/InputSettingActivity;

    invoke-virtual {v3, p0, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/MainMenuActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    :sswitch_4
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "MainmenuActivity RootActivity curproblock ===="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v6, Lcom/konka/tvsettings/RootActivity;->CurProBlock:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v5

    sget-object v6, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v5, v6, :cond_3

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v5

    sget-object v6, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v5, v6, :cond_4

    :cond_3
    iget-object v5, p0, Lcom/konka/tvsettings/MainMenuActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_4
    :goto_2
    invoke-direct {p0}, Lcom/konka/tvsettings/MainMenuActivity;->IsPipModeEnabled()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-static {p0, v7}, Lcom/konka/tvsettings/common/LittleMenu;->popToast(Landroid/content/Context;I)V

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2

    :cond_5
    invoke-static {}, Lcom/konka/tvsettings/SwitchMenuHelper;->get4K2KMode()Z

    move-result v5

    if-eqz v5, :cond_6

    const v5, 0x7f0a0198    # com.konka.tvsettings.R.string.warning_4k2knotsupport

    invoke-static {p0, v5}, Lcom/konka/tvsettings/common/LittleMenu;->popToast(Landroid/content/Context;I)V

    goto/16 :goto_1

    :cond_6
    if-eqz v0, :cond_7

    iget-boolean v5, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->isLock:Z

    if-eqz v5, :cond_7

    invoke-static {p0, v7}, Lcom/konka/tvsettings/common/LittleMenu;->popToast(Landroid/content/Context;I)V

    goto/16 :goto_1

    :cond_7
    const-class v5, Lcom/konka/tvsettings/video3d/S3dSettingActivity;

    invoke-virtual {v3, p0, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/MainMenuActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    :sswitch_5
    const-class v5, Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-virtual {v3, p0, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/MainMenuActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    :sswitch_6
    const-class v5, Lcom/konka/tvsettings/time/TimeSettingActivity;

    invoke-virtual {v3, p0, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/MainMenuActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    :sswitch_7
    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v5

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v5

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v5

    sget-object v6, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v5, v6, :cond_8

    const-class v5, Lcom/konka/tvsettings/ci/CiInfoActivity;

    invoke-virtual {v3, p0, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/MainMenuActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    :cond_8
    const v5, 0x7f0a0014    # com.konka.tvsettings.R.string.warning_dtvnotsupport

    invoke-static {p0, v5}, Lcom/konka/tvsettings/common/LittleMenu;->popToast(Landroid/content/Context;I)V

    goto/16 :goto_1

    :sswitch_8
    const/4 v5, 0x0

    invoke-static {v5}, Lcom/konka/tvsettings/RootActivity;->setIsShowSourceInfo(Z)V

    new-instance v5, Landroid/content/ComponentName;

    const-string v6, "com.konka.weibo"

    const-string v7, "com.konka.weibo.WelcomeActivity"

    invoke-direct {v5, v6, v7}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    const-string v5, "COM_KONKA_WEIBO_EXTRA_ISPOPUP"

    const/4 v6, 0x1

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/MainMenuActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/MainMenuActivity;->finish()V

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x63 -> :sswitch_8
    .end sparse-switch
.end method

.method private startRiseAnimation()V
    .locals 2

    const v1, 0x7f040003    # com.konka.tvsettings.R.anim.anim_bottom_rise

    invoke-static {p0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    iget-object v1, p0, Lcom/konka/tvsettings/MainMenuActivity;->main_container:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 10
    .param p1    # Landroid/os/Bundle;

    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f03001e    # com.konka.tvsettings.R.layout.main

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/MainMenuActivity;->setContentView(I)V

    const v0, 0x7f0700db    # com.konka.tvsettings.R.id.tv_main

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/MainMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/MainMenuActivity;->main_container:Landroid/widget/LinearLayout;

    const-string v0, "onCreate"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/MainMenuActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/TVRootApp;

    iput-object v0, p0, Lcom/konka/tvsettings/MainMenuActivity;->rootApp:Lcom/konka/tvsettings/TVRootApp;

    new-instance v0, Lcom/konka/tvsettings/MainMenuActivity$MainMenuHandler;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/MainMenuActivity$MainMenuHandler;-><init>(Lcom/konka/tvsettings/MainMenuActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/MainMenuActivity;->mainmenuHandler:Lcom/konka/tvsettings/MainMenuActivity$MainMenuHandler;

    iget-object v0, p0, Lcom/konka/tvsettings/MainMenuActivity;->rootApp:Lcom/konka/tvsettings/TVRootApp;

    iget-object v1, p0, Lcom/konka/tvsettings/MainMenuActivity;->mainmenuHandler:Lcom/konka/tvsettings/MainMenuActivity$MainMenuHandler;

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/TVRootApp;->setMainmenuHandler(Lcom/konka/tvsettings/MainMenuActivity$MainMenuHandler;)V

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/MainMenuActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v0, p0, Lcom/konka/tvsettings/MainMenuActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/MainMenuActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-virtual {p0}, Lcom/konka/tvsettings/MainMenuActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://com.konka.hotelmenu/"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "OnOff"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v8, v0, :cond_2

    move v0, v8

    :goto_0
    iput-boolean v0, p0, Lcom/konka/tvsettings/MainMenuActivity;->hotelEnable:Z

    iget-boolean v0, p0, Lcom/konka/tvsettings/MainMenuActivity;->hotelEnable:Z

    if-eqz v0, :cond_0

    const-string v0, "LockChannel"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    sput v0, Lcom/konka/tvsettings/MainMenuActivity;->LockChannel:I

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    iput v9, p0, Lcom/konka/tvsettings/MainMenuActivity;->m_iBackdoorCode:I

    const v0, 0x7f0700dc    # com.konka.tvsettings.R.id.layout_focus_bg

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/MainMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/MainMenuActivity;->ll_focusBg:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Lcom/konka/tvsettings/MainMenuActivity;->CreatePanel()V

    iget-object v0, p0, Lcom/konka/tvsettings/MainMenuActivity;->mainPanel:Lcom/konka/tvsettings/MovingPanel;

    iget-object v1, p0, Lcom/konka/tvsettings/MainMenuActivity;->m_adapter:Lcom/konka/tvsettings/ImageTextAdapter;

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/MovingPanel;->setAdapter(Lcom/konka/tvsettings/ImageTextAdapter;)V

    iget-object v0, p0, Lcom/konka/tvsettings/MainMenuActivity;->gridView:Landroid/widget/GridView;

    new-instance v1, Lcom/konka/tvsettings/MainMenuActivity$2;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/MainMenuActivity$2;-><init>(Lcom/konka/tvsettings/MainMenuActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-direct {p0, v8}, Lcom/konka/tvsettings/MainMenuActivity;->focustoSettingMenu(I)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/MainMenuActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/konka/tvsettings/MainMenuActivity;->showSettingMenu(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/konka/tvsettings/MainMenuActivity;->myHandler:Landroid/os/Handler;

    invoke-static {v0}, Lcom/konka/tvsettings/common/LittleDownTimer;->setHandler(Landroid/os/Handler;)V

    return-void

    :cond_2
    move v0, v9

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 3

    const-string v1, "onDestroy"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/konka/tvsettings/MainMenuActivity;->m_bIsFirstShow:Z

    if-eqz v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.konka.action.STATUSBAR_CONTROL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "show"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/MainMenuActivity;->sendBroadcast(Landroid/content/Intent;)V

    :cond_0
    invoke-direct {p0}, Lcom/konka/tvsettings/MainMenuActivity;->startFallAnimation()V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 9
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/high16 v8, 0x10200000

    const/4 v4, 0x1

    const/4 v7, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "onKeyDown======="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v4

    :cond_0
    :goto_0
    return v4

    :sswitch_0
    invoke-virtual {p0}, Lcom/konka/tvsettings/MainMenuActivity;->onBackPressed()V

    goto :goto_0

    :sswitch_1
    invoke-direct {p0}, Lcom/konka/tvsettings/MainMenuActivity;->IsReadytoMove()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/konka/tvsettings/MainMenuActivity;->mainPanel:Lcom/konka/tvsettings/MovingPanel;

    invoke-virtual {v5}, Lcom/konka/tvsettings/MovingPanel;->getFocusID()I

    move-result v2

    if-lez v2, :cond_1

    iget-object v5, p0, Lcom/konka/tvsettings/MainMenuActivity;->mainPanel:Lcom/konka/tvsettings/MovingPanel;

    invoke-virtual {v5}, Lcom/konka/tvsettings/MovingPanel;->moveLeftOneStep()V

    goto :goto_0

    :cond_1
    invoke-direct {p0, v7}, Lcom/konka/tvsettings/MainMenuActivity;->focustoSettingMenu(I)V

    goto :goto_0

    :sswitch_2
    iget-boolean v5, p0, Lcom/konka/tvsettings/MainMenuActivity;->b_focusOnWeibo:Z

    if-eqz v5, :cond_2

    invoke-direct {p0, v7}, Lcom/konka/tvsettings/MainMenuActivity;->focustoSettingMenu(I)V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/konka/tvsettings/MainMenuActivity;->IsReadytoMove()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/konka/tvsettings/MainMenuActivity;->mainPanel:Lcom/konka/tvsettings/MovingPanel;

    invoke-virtual {v5}, Lcom/konka/tvsettings/MovingPanel;->moveRightOneStep()V

    goto :goto_0

    :sswitch_3
    iget-boolean v5, p0, Lcom/konka/tvsettings/MainMenuActivity;->b_focusOnWeibo:Z

    if-eqz v5, :cond_3

    const/16 v5, 0x63

    invoke-direct {p0, v5}, Lcom/konka/tvsettings/MainMenuActivity;->startMenu(I)V

    goto :goto_0

    :cond_3
    iget-object v5, p0, Lcom/konka/tvsettings/MainMenuActivity;->mainPanel:Lcom/konka/tvsettings/MovingPanel;

    invoke-virtual {v5}, Lcom/konka/tvsettings/MovingPanel;->getFocusID()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/konka/tvsettings/MainMenuActivity;->startMenu(I)V

    goto :goto_0

    :sswitch_4
    iget v5, p0, Lcom/konka/tvsettings/MainMenuActivity;->m_iBackdoorCode:I

    mul-int/lit8 v5, v5, 0xa

    add-int/lit8 v6, p1, -0x7

    add-int/2addr v5, v6

    iput v5, p0, Lcom/konka/tvsettings/MainMenuActivity;->m_iBackdoorCode:I

    iget v5, p0, Lcom/konka/tvsettings/MainMenuActivity;->m_iBackdoorCode:I

    const/16 v6, 0x7d8

    if-ne v5, v6, :cond_4

    const-string v5, "start the activity=====com.konka.factory.MainmenuActivity"

    invoke-static {v5}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const-string v5, "com.konka.factory"

    const-string v6, "com.konka.factory.MainmenuActivity"

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v3, v8}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/MainMenuActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/MainMenuActivity;->finish()V

    goto :goto_0

    :cond_4
    iget v5, p0, Lcom/konka/tvsettings/MainMenuActivity;->m_iBackdoorCode:I

    const/16 v6, 0x7dc

    if-ne v5, v6, :cond_6

    sget-boolean v5, Lcom/konka/tvsettings/common/GlobleVariable;->SRS_DEBUG:Z

    if-eqz v5, :cond_5

    sput-boolean v7, Lcom/konka/tvsettings/common/GlobleVariable;->SRS_DEBUG:Z

    const v5, 0x7f0a0110    # com.konka.tvsettings.R.string.srs_debug_close

    invoke-static {p0, v5}, Lcom/konka/tvsettings/common/LittleMenu;->popToast(Landroid/content/Context;I)V

    :goto_1
    iput v7, p0, Lcom/konka/tvsettings/MainMenuActivity;->m_iBackdoorCode:I

    goto/16 :goto_0

    :cond_5
    sput-boolean v4, Lcom/konka/tvsettings/common/GlobleVariable;->SRS_DEBUG:Z

    const v5, 0x7f0a010f    # com.konka.tvsettings.R.string.srs_debug_open

    invoke-static {p0, v5}, Lcom/konka/tvsettings/common/LittleMenu;->popToast(Landroid/content/Context;I)V

    goto :goto_1

    :cond_6
    const/16 v5, 0x22b8

    iget v6, p0, Lcom/konka/tvsettings/MainMenuActivity;->m_iBackdoorCode:I

    if-ne v5, v6, :cond_7

    const-string v5, "start the activity=====com.konka.hotelmenu"

    invoke-static {v5}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :try_start_0
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const-string v5, "com.konka.hotelmenu"

    const-string v6, "com.konka.hotelmenu.MainActivity"

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v5, 0x10200000

    invoke-virtual {v3, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/MainMenuActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/MainMenuActivity;->finish()V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    invoke-virtual {p0}, Lcom/konka/tvsettings/MainMenuActivity;->finish()V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    const-string v5, "no such activity"

    invoke-static {v5}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_2

    :cond_7
    iget v5, p0, Lcom/konka/tvsettings/MainMenuActivity;->m_iBackdoorCode:I

    const/16 v6, 0x3e8

    if-le v5, v6, :cond_0

    iput v7, p0, Lcom/konka/tvsettings/MainMenuActivity;->m_iBackdoorCode:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x7 -> :sswitch_4
        0x8 -> :sswitch_4
        0x9 -> :sswitch_4
        0xa -> :sswitch_4
        0xb -> :sswitch_4
        0xc -> :sswitch_4
        0xd -> :sswitch_4
        0xe -> :sswitch_4
        0xf -> :sswitch_4
        0x10 -> :sswitch_4
        0x15 -> :sswitch_1
        0x16 -> :sswitch_2
        0x17 -> :sswitch_3
        0x42 -> :sswitch_3
        0x52 -> :sswitch_0
        0xb2 -> :sswitch_3
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 3

    const/4 v2, 0x0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->pauseMenu()V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    const-string v0, "onPause"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const v0, 0x7f040006    # com.konka.tvsettings.R.anim.anim_menu_popup

    invoke-virtual {p0, v0, v2}, Lcom/konka/tvsettings/MainMenuActivity;->overridePendingTransition(II)V

    invoke-direct {p0}, Lcom/konka/tvsettings/MainMenuActivity;->startFallAnimation()V

    const v0, 0x7f0700dc    # com.konka.tvsettings.R.id.layout_focus_bg

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/MainMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method protected onResume()V
    .locals 3

    const-string v0, "onResume!!!"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/4 v0, 0x0

    const v1, 0x7f040005    # com.konka.tvsettings.R.anim.anim_menu_exit

    invoke-virtual {p0, v0, v1}, Lcom/konka/tvsettings/MainMenuActivity;->overridePendingTransition(II)V

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resumeMenu()V

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const-string v0, "OnResume"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const v0, 0x7f0700dc    # com.konka.tvsettings.R.id.layout_focus_bg

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/MainMenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const v2, -0x87e2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-boolean v0, p0, Lcom/konka/tvsettings/MainMenuActivity;->m_bRiseUpEnable:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/konka/tvsettings/MainMenuActivity;->startRiseAnimation()V

    :cond_0
    const-string v0, "onResume id end!!!"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public onUserInteraction()V
    .locals 1

    const-string v0, "onUserInteraction"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resetMenu()V

    invoke-super {p0}, Landroid/app/Activity;->onUserInteraction()V

    return-void
.end method
