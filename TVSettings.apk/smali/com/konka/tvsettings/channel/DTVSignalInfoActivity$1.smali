.class Lcom/konka/tvsettings/channel/DTVSignalInfoActivity$1;
.super Landroid/os/Handler;
.source "DTVSignalInfoActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity$1;->this$0:Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1    # Landroid/os/Message;

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity$1;->this$0:Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrentSignalInformation()Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity$1;->this$0:Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;

    # invokes: Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->updateDtvSignalInfoComponents()V
    invoke-static {v1}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->access$0(Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;)V

    iget v1, v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->quality:I

    if-gtz v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity$1;->this$0:Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;

    # invokes: Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->setProgressValueForSignalQuality(I)V
    invoke-static {v1, v2}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->access$1(Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;I)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity$1;->this$0:Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;

    # invokes: Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->setProgressValueForSignalStrengh(I)V
    invoke-static {v1, v2}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->access$2(Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;I)V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity$1;->this$0:Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;

    iget v2, v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->quality:I

    div-int/lit8 v2, v2, 0xa

    # invokes: Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->setProgressValueForSignalQuality(I)V
    invoke-static {v1, v2}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->access$1(Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;I)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity$1;->this$0:Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;

    iget v2, v0, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->strength:I

    div-int/lit8 v2, v2, 0xa

    # invokes: Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->setProgressValueForSignalStrengh(I)V
    invoke-static {v1, v2}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->access$2(Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
