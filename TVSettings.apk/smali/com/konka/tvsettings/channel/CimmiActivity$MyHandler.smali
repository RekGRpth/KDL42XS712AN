.class Lcom/konka/tvsettings/channel/CimmiActivity$MyHandler;
.super Landroid/os/Handler;
.source "CimmiActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/channel/CimmiActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MyHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/channel/CimmiActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/channel/CimmiActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/channel/CimmiActivity$MyHandler;->this$0:Lcom/konka/tvsettings/channel/CimmiActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1    # Landroid/os/Message;

    const/4 v6, 0x3

    iget v2, p1, Landroid/os/Message;->what:I

    const-string v3, "liying"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "handle message "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v3, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->EV_CIMMI_UI_DATA_READY:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    invoke-virtual {v3}, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->ordinal()I

    move-result v3

    if-ne v2, v3, :cond_1

    :try_start_0
    iget-object v3, p0, Lcom/konka/tvsettings/channel/CimmiActivity$MyHandler;->this$0:Lcom/konka/tvsettings/channel/CimmiActivity;

    invoke-virtual {v3}, Lcom/konka/tvsettings/channel/CimmiActivity;->cimmiUiDataReady()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v3, p0, Lcom/konka/tvsettings/channel/CimmiActivity$MyHandler;->this$0:Lcom/konka/tvsettings/channel/CimmiActivity;

    invoke-virtual {v3}, Lcom/konka/tvsettings/channel/CimmiActivity;->resetCimmiTimeCounter()V

    :cond_0
    :goto_1
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_1
    sget-object v3, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->EV_CIMMI_UI_CLOSEMMI:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    invoke-virtual {v3}, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->ordinal()I

    move-result v3

    if-ne v2, v3, :cond_2

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "\n----------EV_CIMMI_UI_CLOSEMMI\n"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/tvsettings/channel/CimmiActivity$MyHandler;->this$0:Lcom/konka/tvsettings/channel/CimmiActivity;

    invoke-virtual {v3}, Lcom/konka/tvsettings/channel/CimmiActivity;->finish()V

    goto :goto_1

    :cond_2
    sget-object v3, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->EV_CIMMI_UI_CARD_INSERTED:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    invoke-virtual {v3}, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->ordinal()I

    move-result v3

    if-ne v2, v3, :cond_3

    iget-object v3, p0, Lcom/konka/tvsettings/channel/CimmiActivity$MyHandler;->this$0:Lcom/konka/tvsettings/channel/CimmiActivity;

    const v4, 0x7f0a01a3    # com.konka.tvsettings.R.string.str_cimmi_hint_ci_inserted

    invoke-static {v3, v4, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_1

    :cond_3
    sget-object v3, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->EV_CIMMI_UI_CARD_REMOVED:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    invoke-virtual {v3}, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->ordinal()I

    move-result v3

    if-ne v2, v3, :cond_4

    iget-object v3, p0, Lcom/konka/tvsettings/channel/CimmiActivity$MyHandler;->this$0:Lcom/konka/tvsettings/channel/CimmiActivity;

    const v4, 0x7f0a01a4    # com.konka.tvsettings.R.string.str_cimmi_hint_ci_removed

    invoke-static {v3, v4, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_1

    :cond_4
    sget-object v3, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->EV_CIMMI_UI_AUTOTEST_MESSAGE_SHOWN:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    invoke-virtual {v3}, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->ordinal()I

    move-result v3

    if-ne v2, v3, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/channel/CimmiActivity$MyHandler;->this$0:Lcom/konka/tvsettings/channel/CimmiActivity;

    const v4, 0x7f0a01a5    # com.konka.tvsettings.R.string.str_cimmi_hint_ci_auto_test

    invoke-static {v3, v4, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method
