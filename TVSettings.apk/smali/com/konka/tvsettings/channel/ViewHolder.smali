.class public Lcom/konka/tvsettings/channel/ViewHolder;
.super Ljava/lang/Object;
.source "ViewHolder.java"


# instance fields
.field private atvmanualtune:Lcom/konka/tvsettings/channel/Atvmanualtuning;

.field private autotune:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

.field protected button_cha_country_1:Landroid/widget/Button;

.field protected button_cha_country_2:Landroid/widget/Button;

.field protected button_cha_country_3:Landroid/widget/Button;

.field protected button_cha_country_4:Landroid/widget/Button;

.field protected button_cha_country_5:Landroid/widget/Button;

.field protected button_cha_country_6:Landroid/widget/Button;

.field protected button_cha_country_7:Landroid/widget/Button;

.field protected button_cha_country_8:Landroid/widget/Button;

.field protected button_cha_country_9:Landroid/widget/Button;

.field private channeltune:Lcom/konka/tvsettings/channel/Channeltuning;

.field private dtvmanualtune:Lcom/konka/tvsettings/channel/DTVManualTuning;

.field private exittune:Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;

.field protected linear_atvmanualtuning_finetune:Landroid/widget/LinearLayout;

.field protected linear_atvmanualtuning_skip:Landroid/widget/LinearLayout;

.field protected linear_atvmanualtuning_starttuning:Landroid/widget/LinearLayout;

.field protected linear_cha_autotuning_tuningtype:Landroid/widget/LinearLayout;

.field protected linear_cha_dtvmanualtuning_channelnum:Landroid/widget/LinearLayout;

.field protected linear_cha_dtvmanualtuning_signalquality_val:Landroid/widget/LinearLayout;

.field protected linear_cha_dtvmanualtuning_signalstrength_val:Landroid/widget/LinearLayout;

.field protected linear_cha_mainlinear:Landroid/widget/LinearLayout;

.field protected linear_time_schedule:Landroid/widget/LinearLayout;

.field protected linearlayout_cha_antennatype:Landroid/widget/LinearLayout;

.field protected linearlayout_cha_atvmanualtuning:Landroid/widget/LinearLayout;

.field protected linearlayout_cha_autotuning:Landroid/widget/LinearLayout;

.field protected linearlayout_cha_ciinformation:Landroid/widget/LinearLayout;

.field protected linearlayout_cha_dtvmanualtuning:Landroid/widget/LinearLayout;

.field protected linearlayout_cha_programedit:Landroid/widget/LinearLayout;

.field protected progressbar_cha_program_ch:Landroid/widget/TextView;

.field protected progressbar_cha_program_val:Landroid/widget/TextView;

.field protected progressbar_cha_tuneprogress:Landroid/widget/ProgressBar;

.field protected text_cha_antennatype_val:Landroid/widget/TextView;

.field protected text_cha_atvmanualtuning_channelnum_val:Landroid/widget/TextView;

.field protected text_cha_atvmanualtuning_colorsystem_val:Landroid/widget/TextView;

.field protected text_cha_atvmanualtuning_freqency_val:Landroid/widget/TextView;

.field protected text_cha_atvmanualtuning_skip_val:Landroid/widget/TextView;

.field protected text_cha_atvmanualtuning_soundsystem_val:Landroid/widget/TextView;

.field protected text_cha_autotuning_tuningtype_val:Landroid/widget/TextView;

.field protected text_cha_dataprogram_val:Landroid/widget/TextView;

.field protected text_cha_dtvmanualtuning_channelnum_val:Landroid/widget/TextView;

.field protected text_cha_dtvmanualtuning_frequency_val:Landroid/widget/TextView;

.field protected text_cha_dtvmanualtuning_modulation_val:Landroid/widget/TextView;

.field protected text_cha_dtvmanualtuning_symbol_val:Landroid/widget/TextView;

.field protected text_cha_dtvmanualtuning_tuningresult_data_val:Landroid/widget/TextView;

.field protected text_cha_dtvmanualtuning_tuningresult_dtv_val:Landroid/widget/TextView;

.field protected text_cha_dtvmanualtuning_tuningresult_radio_val:Landroid/widget/TextView;

.field protected text_cha_dtvprogram_val:Landroid/widget/TextView;

.field protected text_cha_hint_currentpage_val:Landroid/widget/TextView;

.field protected text_cha_hint_totalpage_val:Landroid/widget/TextView;

.field protected text_cha_program_vhf:Landroid/widget/TextView;

.field protected text_cha_radioprogram_val:Landroid/widget/TextView;

.field protected text_cha_tuningprogress_type:Landroid/widget/TextView;

.field protected text_cha_tuningprogress_val:Landroid/widget/TextView;

.field protected text_cha_tvprogram_val:Landroid/widget/TextView;

.field protected text_time_autotime_val:Landroid/widget/TextView;

.field protected text_time_schedule_channel_val:Landroid/widget/TextView;

.field protected text_time_schedule_hour_val:Landroid/widget/TextView;

.field protected text_time_schedule_minute_val:Landroid/widget/TextView;

.field protected text_time_schedule_ontime_val:Landroid/widget/TextView;

.field protected text_time_schedule_source_val:Landroid/widget/TextView;

.field protected text_time_schedule_volume_val:Landroid/widget/TextView;

.field protected text_time_sleep_val:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/konka/tvsettings/channel/Atvmanualtuning;)V
    .locals 0
    .param p1    # Lcom/konka/tvsettings/channel/Atvmanualtuning;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/konka/tvsettings/channel/ViewHolder;->atvmanualtune:Lcom/konka/tvsettings/channel/Atvmanualtuning;

    return-void
.end method

.method public constructor <init>(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)V
    .locals 0
    .param p1    # Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/konka/tvsettings/channel/ViewHolder;->autotune:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    return-void
.end method

.method public constructor <init>(Lcom/konka/tvsettings/channel/Channeltuning;)V
    .locals 0
    .param p1    # Lcom/konka/tvsettings/channel/Channeltuning;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/konka/tvsettings/channel/ViewHolder;->channeltune:Lcom/konka/tvsettings/channel/Channeltuning;

    return-void
.end method

.method public constructor <init>(Lcom/konka/tvsettings/channel/DTVManualTuning;)V
    .locals 0
    .param p1    # Lcom/konka/tvsettings/channel/DTVManualTuning;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/konka/tvsettings/channel/ViewHolder;->dtvmanualtune:Lcom/konka/tvsettings/channel/DTVManualTuning;

    return-void
.end method

.method public constructor <init>(Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;)V
    .locals 0
    .param p1    # Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/konka/tvsettings/channel/ViewHolder;->exittune:Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;

    return-void
.end method


# virtual methods
.method findViewForAtvManualTuning()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->atvmanualtune:Lcom/konka/tvsettings/channel/Atvmanualtuning;

    const v1, 0x7f07000c    # com.konka.tvsettings.R.id.textview_cha_atvmanualtuning_channelnum_val

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_atvmanualtuning_channelnum_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->atvmanualtune:Lcom/konka/tvsettings/channel/Atvmanualtuning;

    const v1, 0x7f07000f    # com.konka.tvsettings.R.id.textview_cha_atvmanualtuning_colorsystem_val

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_atvmanualtuning_colorsystem_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->atvmanualtune:Lcom/konka/tvsettings/channel/Atvmanualtuning;

    const v1, 0x7f070012    # com.konka.tvsettings.R.id.textview_cha_atvmanualtuning_soundsystem_val

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_atvmanualtuning_soundsystem_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->atvmanualtune:Lcom/konka/tvsettings/channel/Atvmanualtuning;

    const v1, 0x7f070015    # com.konka.tvsettings.R.id.textview_cha_atvmanualtuning_skip_val

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_atvmanualtuning_skip_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->atvmanualtune:Lcom/konka/tvsettings/channel/Atvmanualtuning;

    const v1, 0x7f07001c    # com.konka.tvsettings.R.id.textview_cha_atvmanualtuning_frequency_val

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_atvmanualtuning_freqency_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->atvmanualtune:Lcom/konka/tvsettings/channel/Atvmanualtuning;

    const v1, 0x7f070013    # com.konka.tvsettings.R.id.linearlayout_cha_atvmanualtuning_skip

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->linear_atvmanualtuning_skip:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->atvmanualtune:Lcom/konka/tvsettings/channel/Atvmanualtuning;

    const v1, 0x7f070016    # com.konka.tvsettings.R.id.linearlayout_cha_atvmanualtuning_starttuning

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->linear_atvmanualtuning_starttuning:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->atvmanualtune:Lcom/konka/tvsettings/channel/Atvmanualtuning;

    const v1, 0x7f070018    # com.konka.tvsettings.R.id.linearlayout_cha_atvmanualtuning_finetune

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->linear_atvmanualtuning_finetune:Landroid/widget/LinearLayout;

    return-void
.end method

.method findViewForChannelTuning()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->channeltune:Lcom/konka/tvsettings/channel/Channeltuning;

    const v1, 0x7f070043    # com.konka.tvsettings.R.id.linearlayout_cha_mainlinear

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/Channeltuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->linear_cha_mainlinear:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->channeltune:Lcom/konka/tvsettings/channel/Channeltuning;

    const v1, 0x7f070047    # com.konka.tvsettings.R.id.textview_cha_tvprogram_val

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/Channeltuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_tvprogram_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->channeltune:Lcom/konka/tvsettings/channel/Channeltuning;

    const v1, 0x7f07004b    # com.konka.tvsettings.R.id.textview_cha_dtvprogram_val

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/Channeltuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_dtvprogram_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->channeltune:Lcom/konka/tvsettings/channel/Channeltuning;

    const v1, 0x7f07004f    # com.konka.tvsettings.R.id.textview_cha_radioprogram_val

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/Channeltuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_radioprogram_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->channeltune:Lcom/konka/tvsettings/channel/Channeltuning;

    const v1, 0x7f070053    # com.konka.tvsettings.R.id.textview_cha_dataprogram_val

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/Channeltuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_dataprogram_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->channeltune:Lcom/konka/tvsettings/channel/Channeltuning;

    const v1, 0x7f070058    # com.konka.tvsettings.R.id.textview_cha_tuningprogress_vhf

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/Channeltuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_program_vhf:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->channeltune:Lcom/konka/tvsettings/channel/Channeltuning;

    const v1, 0x7f070057    # com.konka.tvsettings.R.id.textview_cha_tuningprogress_percent

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/Channeltuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_tuningprogress_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->channeltune:Lcom/konka/tvsettings/channel/Channeltuning;

    const v1, 0x7f07005b    # com.konka.tvsettings.R.id.textview_cha_tuningprogress_type

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/Channeltuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_tuningprogress_type:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->channeltune:Lcom/konka/tvsettings/channel/Channeltuning;

    const v1, 0x7f07005c    # com.konka.tvsettings.R.id.progressbar_cha_tuningprogress

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/Channeltuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->progressbar_cha_tuneprogress:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->channeltune:Lcom/konka/tvsettings/channel/Channeltuning;

    const v1, 0x7f07005a    # com.konka.tvsettings.R.id.textview_cha_tuningprogress_num

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/Channeltuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->progressbar_cha_program_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->channeltune:Lcom/konka/tvsettings/channel/Channeltuning;

    const v1, 0x7f070059    # com.konka.tvsettings.R.id.textview_cha_tuningprogress_ch

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/Channeltuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->progressbar_cha_program_ch:Landroid/widget/TextView;

    return-void
.end method

.method findViewForDtvManualTuning()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->dtvmanualtune:Lcom/konka/tvsettings/channel/DTVManualTuning;

    const v1, 0x7f07008a    # com.konka.tvsettings.R.id.linearlayout_cha_dtvmanualtuning_channelnum

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/DTVManualTuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->linear_cha_dtvmanualtuning_channelnum:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->dtvmanualtune:Lcom/konka/tvsettings/channel/DTVManualTuning;

    const v1, 0x7f07008b    # com.konka.tvsettings.R.id.textview_cha_dtvmanualtuning_channelnum_val

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/DTVManualTuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_dtvmanualtuning_channelnum_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->dtvmanualtune:Lcom/konka/tvsettings/channel/DTVManualTuning;

    const v1, 0x7f070091    # com.konka.tvsettings.R.id.textview_cha_dtvmanualtuning_modulation_val

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/DTVManualTuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_dtvmanualtuning_modulation_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->dtvmanualtune:Lcom/konka/tvsettings/channel/DTVManualTuning;

    const v1, 0x7f070099    # com.konka.tvsettings.R.id.linearlayout_cha_dtvmanualtuning_signalstrength_val

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/DTVManualTuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->linear_cha_dtvmanualtuning_signalstrength_val:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->dtvmanualtune:Lcom/konka/tvsettings/channel/DTVManualTuning;

    const v1, 0x7f07009c    # com.konka.tvsettings.R.id.linearlayout_cha_dtvmanualtuning_signalquality_val

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/DTVManualTuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->linear_cha_dtvmanualtuning_signalquality_val:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->dtvmanualtune:Lcom/konka/tvsettings/channel/DTVManualTuning;

    const v1, 0x7f07009f    # com.konka.tvsettings.R.id.textview_cha_dtvmanualtuning_tuningresult_dtv_val

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/DTVManualTuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_dtvmanualtuning_tuningresult_dtv_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->dtvmanualtune:Lcom/konka/tvsettings/channel/DTVManualTuning;

    const v1, 0x7f0700a1    # com.konka.tvsettings.R.id.textview_cha_dtvmanualtuning_tuningresult_data_val

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/DTVManualTuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_dtvmanualtuning_tuningresult_data_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->dtvmanualtune:Lcom/konka/tvsettings/channel/DTVManualTuning;

    const v1, 0x7f0700a3    # com.konka.tvsettings.R.id.textview_cha_dtvmanualtuning_tuningresult_radio_val

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/DTVManualTuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_dtvmanualtuning_tuningresult_radio_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->dtvmanualtune:Lcom/konka/tvsettings/channel/DTVManualTuning;

    const v1, 0x7f070094    # com.konka.tvsettings.R.id.textview_cha_dtvmanualtuning_symbol_val

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/DTVManualTuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_dtvmanualtuning_symbol_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->dtvmanualtune:Lcom/konka/tvsettings/channel/DTVManualTuning;

    const v1, 0x7f07008e    # com.konka.tvsettings.R.id.textview_cha_dtvmanualtuning_frequency_val

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/DTVManualTuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_dtvmanualtuning_frequency_val:Landroid/widget/TextView;

    return-void
.end method

.method findViewsForAutoTuning()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->autotune:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    const v1, 0x7f070022    # com.konka.tvsettings.R.id.linearlayout_cha_autotuning_tuningtype

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->linear_cha_autotuning_tuningtype:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->autotune:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    const v1, 0x7f070024    # com.konka.tvsettings.R.id.textview_cha_autotuning_tuningtype_val

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_autotuning_tuningtype_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->autotune:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    const v1, 0x7f070028    # com.konka.tvsettings.R.id.button_cha_autotuning_choosecountry_australia

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_1:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->autotune:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    const v1, 0x7f070029    # com.konka.tvsettings.R.id.button_cha_autotuning_choosecountry_austria

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_2:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->autotune:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    const v1, 0x7f07002a    # com.konka.tvsettings.R.id.button_cha_autotuning_choosecountry_beligum

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_3:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->autotune:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    const v1, 0x7f07002b    # com.konka.tvsettings.R.id.button_cha_autotuning_choosecountry_bulgaral

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_4:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->autotune:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    const v1, 0x7f07002c    # com.konka.tvsettings.R.id.button_cha_autotuning_choosecountry_croatia

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_5:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->autotune:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    const v1, 0x7f07002d    # com.konka.tvsettings.R.id.button_cha_autotuning_choosecountry_czech

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_6:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->autotune:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    const v1, 0x7f07002e    # com.konka.tvsettings.R.id.button_cha_autotuning_choosecountry_denmark

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_7:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->autotune:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    const v1, 0x7f07002f    # com.konka.tvsettings.R.id.button_cha_autotuning_choosecountry_finland

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_8:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->autotune:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    const v1, 0x7f070030    # com.konka.tvsettings.R.id.button_cha_autotuning_choosecountry_france

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_9:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->autotune:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    const v1, 0x7f070032    # com.konka.tvsettings.R.id.textview_cha_hint_currentpage

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_hint_currentpage_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->autotune:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    const v1, 0x7f070033    # com.konka.tvsettings.R.id.textview_cha_hint_totalpage

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_hint_totalpage_val:Landroid/widget/TextView;

    return-void
.end method
