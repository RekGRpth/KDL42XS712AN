.class Lcom/konka/tvsettings/channel/DTVSignalInfoActivity$MyHandler;
.super Landroid/os/Handler;
.source "DTVSignalInfoActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MyHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity$MyHandler;->this$0:Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .param p1    # Landroid/os/Message;

    iget v4, p1, Landroid/os/Message;->what:I

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v5

    if-ne v4, v5, :cond_0

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    const-string v5, "quality"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const-string v5, "quality"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iget-object v5, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity$MyHandler;->this$0:Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;

    div-int/lit8 v6, v2, 0xa

    # invokes: Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->setProgressValueForSignalQuality(I)V
    invoke-static {v5, v6}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->access$1(Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;I)V

    iget-object v5, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity$MyHandler;->this$0:Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;

    div-int/lit8 v6, v3, 0xa

    # invokes: Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->setProgressValueForSignalStrengh(I)V
    invoke-static {v5, v6}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->access$2(Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;I)V

    :cond_0
    const/16 v5, 0x65

    iget v6, p1, Landroid/os/Message;->what:I

    if-ne v5, v6, :cond_1

    new-instance v1, Landroid/content/Intent;

    iget-object v5, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity$MyHandler;->this$0:Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;

    const-class v6, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v1, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v5, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity$MyHandler;->this$0:Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;

    invoke-virtual {v5, v1}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v5, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity$MyHandler;->this$0:Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;

    invoke-virtual {v5}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->finish()V

    :cond_1
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    return-void
.end method
