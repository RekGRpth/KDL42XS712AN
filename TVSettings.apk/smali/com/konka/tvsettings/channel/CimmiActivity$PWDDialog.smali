.class Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;
.super Landroid/app/Dialog;
.source "CimmiActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/channel/CimmiActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PWDDialog"
.end annotation


# instance fields
.field private editText:Landroid/widget/EditText;

.field final synthetic this$0:Lcom/konka/tvsettings/channel/CimmiActivity;


# direct methods
.method public constructor <init>(Lcom/konka/tvsettings/channel/CimmiActivity;Landroid/content/Context;I)V
    .locals 1
    .param p2    # Landroid/content/Context;
    .param p3    # I

    iput-object p1, p0, Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;->this$0:Lcom/konka/tvsettings/channel/CimmiActivity;

    invoke-direct {p0, p2, p3}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;->editText:Landroid/widget/EditText;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;->editText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;)Lcom/konka/tvsettings/channel/CimmiActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;->this$0:Lcom/konka/tvsettings/channel/CimmiActivity;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const v0, 0x7f030010    # com.konka.tvsettings.R.layout.cimmi_pwd_dialog

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;->setContentView(I)V

    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f070070    # com.konka.tvsettings.R.id.editText

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;->editText:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;->editText:Landroid/widget/EditText;

    new-instance v1, Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog$1;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog$1;-><init>(Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void
.end method
