.class Lcom/konka/tvsettings/channel/Channeltuning$2;
.super Ljava/lang/Object;
.source "Channeltuning.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/channel/Channeltuning;->onKeyDown(ILandroid/view/KeyEvent;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/channel/Channeltuning;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/channel/Channeltuning;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/channel/Channeltuning$2;->this$0:Lcom/konka/tvsettings/channel/Channeltuning;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning$2;->this$0:Lcom/konka/tvsettings/channel/Channeltuning;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/Channeltuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->GetTsStatus()Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_NONE:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning$2;->this$0:Lcom/konka/tvsettings/channel/Channeltuning;

    invoke-virtual {v1}, Lcom/konka/tvsettings/channel/Channeltuning;->finish()V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning$2;->this$0:Lcom/konka/tvsettings/channel/Channeltuning;

    # getter for: Lcom/konka/tvsettings/channel/Channeltuning;->viewholder_channeltune:Lcom/konka/tvsettings/channel/ViewHolder;
    invoke-static {v1}, Lcom/konka/tvsettings/channel/Channeltuning;->access$0(Lcom/konka/tvsettings/channel/Channeltuning;)Lcom/konka/tvsettings/channel/ViewHolder;

    move-result-object v1

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->linear_cha_mainlinear:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method
