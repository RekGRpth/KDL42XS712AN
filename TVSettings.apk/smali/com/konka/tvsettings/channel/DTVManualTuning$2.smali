.class Lcom/konka/tvsettings/channel/DTVManualTuning$2;
.super Ljava/lang/Object;
.source "DTVManualTuning.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/channel/DTVManualTuning;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/channel/DTVManualTuning;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$2;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$2;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    const/4 v2, -0x1

    invoke-static {v1, v2}, Lcom/konka/tvsettings/channel/DTVManualTuning;->access$7(Lcom/konka/tvsettings/channel/DTVManualTuning;I)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$2;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    # invokes: Lcom/konka/tvsettings/channel/DTVManualTuning;->getTurningCountry()Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;
    invoke-static {v1}, Lcom/konka/tvsettings/channel/DTVManualTuning;->access$8(Lcom/konka/tvsettings/channel/DTVManualTuning;)Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUSSIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$2;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    # getter for: Lcom/konka/tvsettings/channel/DTVManualTuning;->channelno:I
    invoke-static {v1}, Lcom/konka/tvsettings/channel/DTVManualTuning;->access$9(Lcom/konka/tvsettings/channel/DTVManualTuning;)I

    move-result v1

    const/16 v2, 0x46

    if-gt v1, v2, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$2;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    # invokes: Lcom/konka/tvsettings/channel/DTVManualTuning;->getTurningCountry()Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;
    invoke-static {v1}, Lcom/konka/tvsettings/channel/DTVManualTuning;->access$8(Lcom/konka/tvsettings/channel/DTVManualTuning;)Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUSSIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$2;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    # getter for: Lcom/konka/tvsettings/channel/DTVManualTuning;->channelno:I
    invoke-static {v1}, Lcom/konka/tvsettings/channel/DTVManualTuning;->access$9(Lcom/konka/tvsettings/channel/DTVManualTuning;)I

    move-result v1

    const/16 v2, 0x45

    if-le v1, v2, :cond_3

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$2;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    # getter for: Lcom/konka/tvsettings/channel/DTVManualTuning;->rfInfo:Lcom/mstar/android/tvapi/dtv/vo/RfInfo;
    invoke-static {v1}, Lcom/konka/tvsettings/channel/DTVManualTuning;->access$10(Lcom/konka/tvsettings/channel/DTVManualTuning;)Lcom/mstar/android/tvapi/dtv/vo/RfInfo;

    move-result-object v1

    iget-object v0, v1, Lcom/mstar/android/tvapi/dtv/vo/RfInfo;->rfName:Ljava/lang/String;

    const-string v1, "5A"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "9A"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_2
    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$2;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    # getter for: Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;
    invoke-static {v1}, Lcom/konka/tvsettings/channel/DTVManualTuning;->access$0(Lcom/konka/tvsettings/channel/DTVManualTuning;)Lcom/konka/tvsettings/channel/ViewHolder;

    move-result-object v1

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_dtvmanualtuning_channelnum_val:Landroid/widget/TextView;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    :goto_0
    const-string v1, "TvApp"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "#########handle.channelno:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$2;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    # getter for: Lcom/konka/tvsettings/channel/DTVManualTuning;->channelno:I
    invoke-static {v3}, Lcom/konka/tvsettings/channel/DTVManualTuning;->access$9(Lcom/konka/tvsettings/channel/DTVManualTuning;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "handle.prev channelno:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$2;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    # getter for: Lcom/konka/tvsettings/channel/DTVManualTuning;->previousChannelNumber:I
    invoke-static {v3}, Lcom/konka/tvsettings/channel/DTVManualTuning;->access$11(Lcom/konka/tvsettings/channel/DTVManualTuning;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$2;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    # getter for: Lcom/konka/tvsettings/channel/DTVManualTuning;->channelno:I
    invoke-static {v1}, Lcom/konka/tvsettings/channel/DTVManualTuning;->access$9(Lcom/konka/tvsettings/channel/DTVManualTuning;)I

    move-result v1

    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$2;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    # getter for: Lcom/konka/tvsettings/channel/DTVManualTuning;->previousChannelNumber:I
    invoke-static {v2}, Lcom/konka/tvsettings/channel/DTVManualTuning;->access$11(Lcom/konka/tvsettings/channel/DTVManualTuning;)I

    move-result v2

    if-eq v1, v2, :cond_5

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$2;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    # invokes: Lcom/konka/tvsettings/channel/DTVManualTuning;->initScanResult()V
    invoke-static {v1}, Lcom/konka/tvsettings/channel/DTVManualTuning;->access$13(Lcom/konka/tvsettings/channel/DTVManualTuning;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$2;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/DTVManualTuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$2;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    # getter for: Lcom/konka/tvsettings/channel/DTVManualTuning;->channelno:I
    invoke-static {v2}, Lcom/konka/tvsettings/channel/DTVManualTuning;->access$9(Lcom/konka/tvsettings/channel/DTVManualTuning;)I

    move-result v2

    int-to-short v2, v2

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvManualScanRF(S)Z

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$2;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$2;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    # getter for: Lcom/konka/tvsettings/channel/DTVManualTuning;->channelno:I
    invoke-static {v2}, Lcom/konka/tvsettings/channel/DTVManualTuning;->access$9(Lcom/konka/tvsettings/channel/DTVManualTuning;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/konka/tvsettings/channel/DTVManualTuning;->access$14(Lcom/konka/tvsettings/channel/DTVManualTuning;I)V

    :goto_1
    return-void

    :cond_4
    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$2;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$2;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    # getter for: Lcom/konka/tvsettings/channel/DTVManualTuning;->previousChannelNumber:I
    invoke-static {v2}, Lcom/konka/tvsettings/channel/DTVManualTuning;->access$11(Lcom/konka/tvsettings/channel/DTVManualTuning;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/konka/tvsettings/channel/DTVManualTuning;->access$12(Lcom/konka/tvsettings/channel/DTVManualTuning;I)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$2;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    # getter for: Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;
    invoke-static {v1}, Lcom/konka/tvsettings/channel/DTVManualTuning;->access$0(Lcom/konka/tvsettings/channel/DTVManualTuning;)Lcom/konka/tvsettings/channel/ViewHolder;

    move-result-object v1

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_dtvmanualtuning_channelnum_val:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$2;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    # getter for: Lcom/konka/tvsettings/channel/DTVManualTuning;->channelno:I
    invoke-static {v2}, Lcom/konka/tvsettings/channel/DTVManualTuning;->access$9(Lcom/konka/tvsettings/channel/DTVManualTuning;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$2;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning$2;->this$0:Lcom/konka/tvsettings/channel/DTVManualTuning;

    # getter for: Lcom/konka/tvsettings/channel/DTVManualTuning;->channelno:I
    invoke-static {v2}, Lcom/konka/tvsettings/channel/DTVManualTuning;->access$9(Lcom/konka/tvsettings/channel/DTVManualTuning;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/konka/tvsettings/channel/DTVManualTuning;->access$14(Lcom/konka/tvsettings/channel/DTVManualTuning;I)V

    goto :goto_1
.end method
