.class public Lcom/konka/tvsettings/channel/CimmiActivity;
.super Lcom/konka/tvsettings/teletext/MstarBaseActivity;
.source "CimmiActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/channel/CimmiActivity$MyHandler;,
        Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;
    }
.end annotation


# static fields
.field private static CIMMI_DELAY_TIME_COUNTER_MAX:I


# instance fields
.field private adapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private bottom:Landroid/widget/TextView;

.field private ci:Lcom/konka/kkinterface/tv/CiDesk;

.field private ciHandler:Lcom/konka/tvsettings/channel/CimmiActivity$MyHandler;

.field private cimMiHasDestory:Z

.field private cimmiListView:Landroid/widget/ListView;

.field private cimmiTimeCounter:I

.field private data:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private subTitle:Landroid/widget/TextView;

.field private timerHandler:Landroid/os/Handler;

.field private title:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x78

    sput v0, Lcom/konka/tvsettings/channel/CimmiActivity;->CIMMI_DELAY_TIME_COUNTER_MAX:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->adapter:Landroid/widget/ArrayAdapter;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->cimmiListView:Landroid/widget/ListView;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->title:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->subTitle:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->bottom:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->ci:Lcom/konka/kkinterface/tv/CiDesk;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->ciHandler:Lcom/konka/tvsettings/channel/CimmiActivity$MyHandler;

    iput v1, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->cimmiTimeCounter:I

    iput-boolean v1, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->cimMiHasDestory:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->data:Ljava/util/ArrayList;

    new-instance v0, Lcom/konka/tvsettings/channel/CimmiActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/channel/CimmiActivity$1;-><init>(Lcom/konka/tvsettings/channel/CimmiActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->timerHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/channel/CimmiActivity;)Lcom/konka/kkinterface/tv/CiDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->ci:Lcom/konka/kkinterface/tv/CiDesk;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/channel/CimmiActivity;)Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->cimmiListView:Landroid/widget/ListView;

    return-object v0
.end method


# virtual methods
.method public cimmiUiDataReady()V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v0, 0x0

    sget-object v2, Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;->E_NONE:Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;

    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->ci:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/CiDesk;->isDataExisted()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->ci:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/CiDesk;->getMmiType()Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;

    move-result-object v2

    sget-object v8, Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;->E_MENU:Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;

    if-ne v2, v8, :cond_6

    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->ci:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/CiDesk;->getMenuTitleLength()I

    move-result v8

    if-lez v8, :cond_2

    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->ci:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/CiDesk;->getMenuTitleString()Ljava/lang/String;

    move-result-object v7

    :goto_1
    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->title:Landroid/widget/TextView;

    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->ci:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/CiDesk;->getMenuSubtitleLength()I

    move-result v8

    if-lez v8, :cond_3

    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->ci:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/CiDesk;->getMenuSubtitleString()Ljava/lang/String;

    move-result-object v6

    :goto_2
    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->subTitle:Landroid/widget/TextView;

    invoke-virtual {v8, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->ci:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/CiDesk;->getMenuBottomLength()I

    move-result v8

    if-lez v8, :cond_4

    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->ci:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/CiDesk;->getMenuBottomString()Ljava/lang/String;

    move-result-object v4

    :goto_3
    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->bottom:Landroid/widget/TextView;

    invoke-virtual {v8, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->data:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    const/4 v1, 0x0

    :goto_4
    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->ci:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/CiDesk;->getMenuChoiceNumber()S

    move-result v8

    if-lt v1, v8, :cond_5

    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->adapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v8}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->cimmiListView:Landroid/widget/ListView;

    invoke-virtual {v8}, Landroid/widget/ListView;->invalidate()V

    goto :goto_0

    :cond_2
    const-string v7, " "

    goto :goto_1

    :cond_3
    const-string v6, " "

    goto :goto_2

    :cond_4
    const-string v4, " "

    goto :goto_3

    :cond_5
    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->ci:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v8, v1}, Lcom/konka/kkinterface/tv/CiDesk;->getMenuSelectionString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->data:Ljava/util/ArrayList;

    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_6
    sget-object v8, Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;->E_LIST:Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;

    if-ne v2, v8, :cond_b

    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->ci:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/CiDesk;->getListTitleLength()I

    move-result v8

    if-lez v8, :cond_7

    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->ci:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/CiDesk;->getListTitleString()Ljava/lang/String;

    move-result-object v7

    :goto_5
    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->title:Landroid/widget/TextView;

    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->ci:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/CiDesk;->getListSubtitleLength()I

    move-result v8

    if-lez v8, :cond_8

    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->ci:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/CiDesk;->getListSubtitleString()Ljava/lang/String;

    move-result-object v6

    :goto_6
    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->subTitle:Landroid/widget/TextView;

    invoke-virtual {v8, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->ci:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/CiDesk;->getListBottomLength()I

    move-result v8

    if-lez v8, :cond_9

    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->ci:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/CiDesk;->getListBottomString()Ljava/lang/String;

    move-result-object v4

    :goto_7
    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->bottom:Landroid/widget/TextView;

    invoke-virtual {v8, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->adapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v8}, Landroid/widget/ArrayAdapter;->clear()V

    const/4 v1, 0x0

    :goto_8
    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->ci:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/CiDesk;->getListChoiceNumber()S

    move-result v8

    if-lt v1, v8, :cond_a

    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->adapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v8}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->cimmiListView:Landroid/widget/ListView;

    invoke-virtual {v8}, Landroid/widget/ListView;->invalidate()V

    goto/16 :goto_0

    :cond_7
    const-string v7, " "

    goto :goto_5

    :cond_8
    const-string v6, " "

    goto :goto_6

    :cond_9
    const-string v4, " "

    goto :goto_7

    :cond_a
    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->ci:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v8, v1}, Lcom/konka/kkinterface/tv/CiDesk;->getListSelectionString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->adapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v8, v5}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    :cond_b
    sget-object v8, Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;->E_ENQ:Lcom/mstar/android/tvapi/dtv/vo/EnumMmiType;

    if-ne v2, v8, :cond_0

    new-instance v3, Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;

    const v8, 0x1030059    # android.R.style.Theme_Panel

    invoke-direct {v3, p0, p0, v8}, Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;-><init>(Lcom/konka/tvsettings/channel/CimmiActivity;Landroid/content/Context;I)V

    invoke-virtual {v3}, Lcom/konka/tvsettings/channel/CimmiActivity$PWDDialog;->show()V

    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->ci:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/CiDesk;->getEnqLength()S

    move-result v8

    if-lez v8, :cond_c

    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->ci:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/CiDesk;->getEnqString()Ljava/lang/String;

    move-result-object v7

    :goto_9
    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->title:Landroid/widget/TextView;

    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v6, " "

    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->subTitle:Landroid/widget/TextView;

    invoke-virtual {v8, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v4, " "

    iget-object v8, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->bottom:Landroid/widget/TextView;

    invoke-virtual {v8, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_c
    const-string v7, " "

    goto :goto_9
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onCreate(Landroid/os/Bundle;)V

    const-string v3, "liying"

    const-string v4, "CimmiActivity---------oncreate "

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const v3, 0x7f03000f    # com.konka.tvsettings.R.layout.cimmi_list_view

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/channel/CimmiActivity;->setContentView(I)V

    const v3, 0x7f07006e    # com.konka.tvsettings.R.id.cimmi_list_view

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/channel/CimmiActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    iput-object v3, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->cimmiListView:Landroid/widget/ListView;

    const v3, 0x7f07006c    # com.konka.tvsettings.R.id.cimmi_subtitle_one

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/channel/CimmiActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->title:Landroid/widget/TextView;

    const v3, 0x7f07006d    # com.konka.tvsettings.R.id.cimmi_subtitle_two

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/channel/CimmiActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->subTitle:Landroid/widget/TextView;

    const v3, 0x7f07006f    # com.konka.tvsettings.R.id.cimmi_text_end

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/channel/CimmiActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->bottom:Landroid/widget/TextView;

    new-instance v3, Landroid/widget/ArrayAdapter;

    const v4, 0x7f030041    # com.konka.tvsettings.R.layout.pvr_menu_info_list_view_item

    iget-object v5, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->data:Ljava/util/ArrayList;

    invoke-direct {v3, p0, v4, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v3, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->adapter:Landroid/widget/ArrayAdapter;

    iget-object v3, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->cimmiListView:Landroid/widget/ListView;

    iget-object v4, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->adapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v3, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->cimmiListView:Landroid/widget/ListView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setDividerHeight(I)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/CimmiActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/TVRootApp;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCiManagerInstance()Lcom/konka/kkinterface/tv/CiDesk;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->ci:Lcom/konka/kkinterface/tv/CiDesk;

    new-instance v3, Lcom/konka/tvsettings/channel/CimmiActivity$MyHandler;

    invoke-direct {v3, p0}, Lcom/konka/tvsettings/channel/CimmiActivity$MyHandler;-><init>(Lcom/konka/tvsettings/channel/CimmiActivity;)V

    iput-object v3, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->ciHandler:Lcom/konka/tvsettings/channel/CimmiActivity$MyHandler;

    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;->getInstance()Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->ciHandler:Lcom/konka/tvsettings/channel/CimmiActivity$MyHandler;

    invoke-virtual {v3, v4}, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;->attachHandler(Landroid/os/Handler;)V

    iget-object v3, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->cimmiListView:Landroid/widget/ListView;

    new-instance v4, Lcom/konka/tvsettings/channel/CimmiActivity$2;

    invoke-direct {v4, p0}, Lcom/konka/tvsettings/channel/CimmiActivity$2;-><init>(Lcom/konka/tvsettings/channel/CimmiActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    :try_start_0
    const-string v3, "liying"

    const-string v4, "oncreate:cimmiUiDataReady"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/CimmiActivity;->cimmiUiDataReady()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v3, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->cimmiListView:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->requestFocus()Z

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/CimmiActivity;->resetCimmiTimeCounter()V

    iget-object v3, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->timerHandler:Landroid/os/Handler;

    invoke-static {v3}, Lcom/konka/tvsettings/common/LittleDownTimer;->setHandler(Landroid/os/Handler;)V

    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->cimMiHasDestory:Z

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v1, 0x1

    invoke-static {}, Lcom/konka/kkinterface/tv/TranslateIrKeyDesk;->getInstance()Lcom/konka/kkinterface/tv/TranslateIrKeyDesk;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/konka/kkinterface/tv/TranslateIrKeyDesk;->translateIRKey(I)I

    move-result p1

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    :goto_0
    return v1

    :sswitch_0
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n----------KEYCODE_MENU\n"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :try_start_0
    iget-object v2, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->ci:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CiDesk;->backMenu()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :sswitch_1
    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/CimmiActivity;->resetCimmiTimeCounter()V

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n----------KEYCODE_BACK\n"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :try_start_1
    iget-object v2, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->ci:Lcom/konka/kkinterface/tv/CiDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CiDesk;->close()V

    sget v2, Lcom/konka/tvsettings/channel/CimmiActivity;->CIMMI_DELAY_TIME_COUNTER_MAX:I

    add-int/lit8 v2, v2, -0x3

    iput v2, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->cimmiTimeCounter:I
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x1c -> :sswitch_1
        0x29 -> :sswitch_0
        0x52 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->pauseMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resumeMenu()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/CimmiActivity;->resetCimmiTimeCounter()V

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onResume()V

    return-void
.end method

.method protected onStart()V
    .locals 0

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/CimmiActivity;->resetCimmiTimeCounter()V

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onStart()V

    return-void
.end method

.method protected onStop()V
    .locals 0

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/CimmiActivity;->resetCimmiTimeCounter()V

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onStop()V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resetMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onUserInteraction()V

    return-void
.end method

.method public resetCimmiTimeCounter()V
    .locals 3

    const-string v0, "liying"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "cimmitimercounter "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->cimmiTimeCounter:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/tvsettings/channel/CimmiActivity;->cimmiTimeCounter:I

    return-void
.end method
