.class public Lcom/konka/tvsettings/channel/Atvmanualtuning;
.super Lcom/konka/tvsettings/teletext/MstarBaseActivity;
.source "Atvmanualtuning.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/channel/Atvmanualtuning$MyHandler;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumAvdVideoStandardType:[I = null

.field public static final max_atv_count:I = 0x64

.field public static final min_atv_count:I = 0x1


# instance fields
.field public bProListItemIndex:I

.field private colorsystem:[Ljava/lang/String;

.field private colorsystemindex:I

.field public curChannelNumber:I

.field private miChannelNum:I

.field private myHandler:Lcom/konka/tvsettings/channel/Atvmanualtuning$MyHandler;

.field private s3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

.field serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

.field private soundsystem:[Ljava/lang/String;

.field private soundsystemindex:I

.field ts:Lcom/konka/kkinterface/tv/ChannelDesk;

.field private viewholder_atvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;


# direct methods
.method static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumAvdVideoStandardType()[I
    .locals 3

    sget-object v0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumAvdVideoStandardType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->AUTO:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_9

    :goto_1
    :try_start_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->MAX:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_8

    :goto_2
    :try_start_2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->NOTSTANDARD:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_7

    :goto_3
    :try_start_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->NTSC_44:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_6

    :goto_4
    :try_start_4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->NTSC_M:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_5

    :goto_5
    :try_start_5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->PAL_60:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_4

    :goto_6
    :try_start_6
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->PAL_BGHI:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_3

    :goto_7
    :try_start_7
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->PAL_M:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_2

    :goto_8
    :try_start_8
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->PAL_N:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_1

    :goto_9
    :try_start_9
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->SECAM:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_0

    :goto_a
    sput-object v0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumAvdVideoStandardType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_a

    :catch_1
    move-exception v1

    goto :goto_9

    :catch_2
    move-exception v1

    goto :goto_8

    :catch_3
    move-exception v1

    goto :goto_7

    :catch_4
    move-exception v1

    goto :goto_6

    :catch_5
    move-exception v1

    goto :goto_5

    :catch_6
    move-exception v1

    goto :goto_4

    :catch_7
    move-exception v1

    goto :goto_3

    :catch_8
    move-exception v1

    goto :goto_2

    :catch_9
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;-><init>()V

    iput v0, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->miChannelNum:I

    iput v0, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->colorsystemindex:I

    iput v0, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->soundsystemindex:I

    sget-object v0, Lcom/konka/kkinterface/tv/ChannelDesk;->atvsoundsystem:[Ljava/lang/String;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->soundsystem:[Ljava/lang/String;

    sget-object v0, Lcom/konka/kkinterface/tv/ChannelDesk;->atvcolorsystem:[Ljava/lang/String;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->colorsystem:[Ljava/lang/String;

    iput-object v1, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    iput-object v1, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->s3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    return-void
.end method

.method private SetAtvManualSkipStatus()V
    .locals 8

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    iget v1, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->bProListItemIndex:I

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->getProgramInfoByIndex(I)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v6

    iget-boolean v5, v6, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->isSkip:Z

    if-eqz v5, :cond_0

    move v5, v4

    :goto_0
    iput-boolean v5, v6, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->isSkip:Z

    iget-object v0, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;->E_SKIP:Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;

    iget v2, v6, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    iget-short v3, v6, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    invoke-interface/range {v0 .. v5}, Lcom/konka/kkinterface/tv/ChannelDesk;->setProgramAttribute(Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;ISIZ)V

    if-eqz v5, :cond_1

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0004    # com.konka.tvsettings.R.string.common_true_on

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    :goto_1
    iget-object v0, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->viewholder_atvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_atvmanualtuning_skip_val:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    const/4 v5, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0005    # com.konka.tvsettings.R.string.common_false_off

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    goto :goto_1
.end method

.method private SetOnFocuseChangeListener()V
    .locals 2

    new-instance v0, Lcom/konka/tvsettings/channel/Atvmanualtuning$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/channel/Atvmanualtuning$1;-><init>(Lcom/konka/tvsettings/channel/Atvmanualtuning;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->viewholder_atvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->linear_atvmanualtuning_starttuning:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->viewholder_atvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->linear_atvmanualtuning_finetune:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/channel/Atvmanualtuning;)Lcom/konka/tvsettings/channel/ViewHolder;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->viewholder_atvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/channel/Atvmanualtuning;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->updateAtvManualtuningComponents()V

    return-void
.end method

.method private getColorSystem(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->colorsystem:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method private getColorSystemIndexByType(Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;)I
    .locals 2
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    invoke-static {}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumAvdVideoStandardType()[I

    move-result-object v0

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x3

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getColorSystemType(I)Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;
    .locals 1
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->AUTO:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->PAL_BGHI:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->NTSC_M:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->SECAM:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private inputCurChannelNumber(I)I
    .locals 4
    .param p1    # I

    const/16 v3, 0x64

    const/4 v1, 0x0

    iget v2, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->miChannelNum:I

    mul-int/lit8 v2, v2, 0xa

    if-le v2, v3, :cond_0

    iput v1, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->miChannelNum:I

    :cond_0
    iget v2, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->miChannelNum:I

    mul-int/lit8 v2, v2, 0xa

    add-int/2addr v2, p1

    iput v2, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->miChannelNum:I

    iget v2, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->miChannelNum:I

    if-nez v2, :cond_1

    :goto_0
    return v1

    :cond_1
    iget v2, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->miChannelNum:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->curChannelNumber:I

    iget v2, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->curChannelNumber:I

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iget v2, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->curChannelNumber:I

    if-lt v2, v3, :cond_2

    iput v1, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->miChannelNum:I

    iput v3, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->curChannelNumber:I

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->viewholder_atvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_atvmanualtuning_channelnum_val:Landroid/widget/TextView;

    const-string v2, "100"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v1, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->curChannelNumber:I

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->viewholder_atvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_atvmanualtuning_channelnum_val:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v1, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->curChannelNumber:I

    goto :goto_0
.end method

.method private updateAtvManualSkipStatus()V
    .locals 8

    const v7, 0x7f0a0004    # com.konka.tvsettings.R.string.common_true_on

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v2, 0x0

    :goto_0
    const/16 v3, 0x64

    if-lt v2, v3, :cond_1

    :cond_0
    iget-object v3, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    iget v4, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->bProListItemIndex:I

    invoke-interface {v3, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->getProgramInfoByIndex(I)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v0

    iget-short v3, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->viewholder_atvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_atvmanualtuning_skip_val:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->viewholder_atvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ViewHolder;->linear_atvmanualtuning_skip:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v3, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->viewholder_atvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ViewHolder;->linear_atvmanualtuning_skip:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    :goto_1
    return-void

    :cond_1
    iget-object v3, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_DTV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-interface {v3, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->getProgramCount(Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;)I

    move-result v3

    add-int/2addr v3, v2

    iput v3, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->bProListItemIndex:I

    iget-object v3, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    iget v4, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->bProListItemIndex:I

    invoke-interface {v3, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->getProgramInfoByIndex(I)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v3

    iget v3, v3, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    iget v4, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->curChannelNumber:I

    if-eq v3, v4, :cond_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->viewholder_atvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ViewHolder;->linear_atvmanualtuning_skip:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v3, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->viewholder_atvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ViewHolder;->linear_atvmanualtuning_skip:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    iget-boolean v1, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->isSkip:Z

    if-eqz v1, :cond_3

    iget-object v3, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->viewholder_atvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_atvmanualtuning_skip_val:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->viewholder_atvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_atvmanualtuning_skip_val:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0005    # com.konka.tvsettings.R.string.common_false_off

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private updateAtvManualtuningComponents()V
    .locals 5

    iget v2, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->curChannelNumber:I

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->atvGetVideoSystem()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->getColorSystemIndexByType(Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;)I

    move-result v2

    iput v2, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->colorsystemindex:I

    iget-object v2, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->viewholder_atvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_atvmanualtuning_colorsystem_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->colorsystemindex:I

    invoke-direct {p0, v3}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->getColorSystem(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->atvGetSoundSystem()Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->ordinal()I

    move-result v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->E_M:Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->ordinal()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    rem-int/2addr v2, v3

    iput v2, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->soundsystemindex:I

    iget-object v2, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->viewholder_atvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_atvmanualtuning_channelnum_val:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->viewholder_atvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_atvmanualtuning_soundsystem_val:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->soundsystem:[Ljava/lang/String;

    iget v4, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->soundsystemindex:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->updateAtvManualSkipStatus()V

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->updateAtvManualtuningfreq()V

    return-void
.end method

.method private updateAtvManualtuningComponentsWhenInputChannelNumber()V
    .locals 5

    iget v2, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->curChannelNumber:I

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->atvGetVideoSystem()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->getColorSystemIndexByType(Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;)I

    move-result v2

    iput v2, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->colorsystemindex:I

    iget-object v2, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->viewholder_atvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_atvmanualtuning_colorsystem_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->colorsystemindex:I

    invoke-direct {p0, v3}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->getColorSystem(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->atvGetSoundSystem()Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->ordinal()I

    move-result v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->E_L:Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->ordinal()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    rem-int/2addr v2, v3

    iput v2, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->soundsystemindex:I

    iget-object v2, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->viewholder_atvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_atvmanualtuning_channelnum_val:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->viewholder_atvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_atvmanualtuning_soundsystem_val:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->soundsystem:[Ljava/lang/String;

    iget v4, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->soundsystemindex:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->updateAtvManualSkipStatus()V

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->updateAtvManualtuningfreq()V

    return-void
.end method

.method private updateAtvManualtuningfreq()V
    .locals 7

    iget-object v4, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->atvGetCurrentFrequency()I

    move-result v0

    div-int/lit16 v2, v0, 0x3e8

    rem-int/lit16 v4, v0, 0x3e8

    div-int/lit8 v1, v4, 0xa

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v4, 0xa

    if-ge v1, v4, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "0"

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    :goto_0
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->viewholder_atvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v4, v4, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_atvmanualtuning_freqency_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f030002    # com.konka.tvsettings.R.layout.atvmanualtuning

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->setContentView(I)V

    new-instance v1, Lcom/konka/tvsettings/channel/Atvmanualtuning$MyHandler;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/channel/Atvmanualtuning$MyHandler;-><init>(Lcom/konka/tvsettings/channel/Atvmanualtuning;)V

    iput-object v1, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->myHandler:Lcom/konka/tvsettings/channel/Atvmanualtuning$MyHandler;

    new-instance v1, Lcom/konka/tvsettings/channel/ViewHolder;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/channel/ViewHolder;-><init>(Lcom/konka/tvsettings/channel/Atvmanualtuning;)V

    iput-object v1, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->viewholder_atvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->viewholder_atvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    invoke-virtual {v1}, Lcom/konka/tvsettings/channel/ViewHolder;->findViewForAtvManualTuning()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/TVRootApp;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->myHandler:Lcom/konka/tvsettings/channel/Atvmanualtuning$MyHandler;

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setHandler(Landroid/os/Handler;I)Z

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->makeSourceAtv()V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;->E_FIRST_SERVICE_ATV:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;->E_DEFAULT:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/ChannelDesk;->changeToFirstService(Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;)Z

    :cond_0
    iget-object v1, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrentChannelNumber()I

    move-result v1

    iput v1, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->curChannelNumber:I

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->SetOnFocuseChangeListener()V

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->updateAtvManualtuningComponents()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 11
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    invoke-static {}, Lcom/konka/kkinterface/tv/TranslateIrKeyDesk;->getInstance()Lcom/konka/kkinterface/tv/TranslateIrKeyDesk;

    move-result-object v8

    invoke-virtual {v8, p1}, Lcom/konka/kkinterface/tv/TranslateIrKeyDesk;->translateIRKey(I)I

    move-result p1

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->getCurrentFocus()Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getId()I

    move-result v0

    iget-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->colorsystem:[Ljava/lang/String;

    array-length v6, v8

    sget-object v8, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->E_M:Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->ordinal()I

    move-result v8

    add-int/lit8 v5, v8, 0x1

    iget-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/ChannelDesk;->atvGetCurrentFrequency()I

    move-result v7

    sparse-switch p1, :sswitch_data_0

    :goto_0
    invoke-super {p0, p1, p2}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v8

    :goto_1
    return v8

    :sswitch_0
    sparse-switch v0, :sswitch_data_1

    :goto_2
    invoke-direct {p0}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->updateAtvManualtuningComponents()V

    goto :goto_0

    :sswitch_1
    iget-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    const/16 v9, 0xbb8

    sget-object v10, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;->E_MANUAL_TUNE_MODE_FINE_TUNE_UP:Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

    invoke-interface {v8, v9, v7, v10}, Lcom/konka/kkinterface/tv/ChannelDesk;->atvSetManualTuningStart(IILcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;)Z

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->updateAtvManualtuningfreq()V

    iget-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    iget v9, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->curChannelNumber:I

    invoke-interface {v8, v9}, Lcom/konka/kkinterface/tv/ChannelDesk;->saveAtvProgram(I)Z

    goto :goto_2

    :sswitch_2
    iget-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/ChannelDesk;->GetTsStatus()Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    move-result-object v8

    sget-object v9, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_ATV_MANU_TUNING_LEFT:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    if-ne v8, v9, :cond_0

    iget-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/ChannelDesk;->atvSetManualTuningEnd()V

    :cond_0
    new-instance v3, Landroid/content/Intent;

    const-string v8, "com.konka.tv.hotkey.service.UNFREEZE"

    invoke-direct {v3, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getS3DManagerInstance()Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v8

    iput-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->s3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    iget-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->s3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    sget-object v9, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-interface {v8, v9}, Lcom/konka/kkinterface/tv/S3DDesk;->setDisplayFormat(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;)Z

    iget-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    const/16 v9, 0x1388

    sget-object v10, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;->E_MANUAL_TUNE_MODE_SEARCH_ONE_TO_UP:Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

    invoke-interface {v8, v9, v7, v10}, Lcom/konka/kkinterface/tv/ChannelDesk;->atvSetManualTuningStart(IILcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;)Z

    goto :goto_2

    :sswitch_3
    new-instance v4, Landroid/content/Intent;

    const-string v8, "com.konka.tv.hotkey.service.UNFREEZE"

    invoke-direct {v4, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->sendBroadcast(Landroid/content/Intent;)V

    iget v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->curChannelNumber:I

    const/16 v9, 0x63

    if-ge v8, v9, :cond_2

    iget v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->curChannelNumber:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->curChannelNumber:I

    :cond_1
    :goto_3
    iget-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    iget v9, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->curChannelNumber:I

    sget-object v10, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->E_SERVICETYPE_ATV:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    invoke-interface {v8, v9, v10}, Lcom/konka/kkinterface/tv/ChannelDesk;->programSel(ILcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;)Z

    goto :goto_2

    :cond_2
    iget v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->curChannelNumber:I

    const/16 v9, 0x63

    if-ne v8, v9, :cond_1

    const/4 v8, 0x0

    iput v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->curChannelNumber:I

    goto :goto_3

    :sswitch_4
    iget v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->colorsystemindex:I

    add-int/lit8 v8, v8, 0x1

    rem-int/2addr v8, v6

    iput v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->colorsystemindex:I

    iget-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    iget v9, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->colorsystemindex:I

    invoke-direct {p0, v9}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->getColorSystemType(I)Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    move-result-object v9

    invoke-interface {v8, v9}, Lcom/konka/kkinterface/tv/ChannelDesk;->atvSetForceVedioSystem(Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;)V

    iget-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->viewholder_atvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v8, v8, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_atvmanualtuning_colorsystem_val:Landroid/widget/TextView;

    iget v9, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->colorsystemindex:I

    invoke-direct {p0, v9}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->getColorSystem(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    iget v9, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->curChannelNumber:I

    invoke-interface {v8, v9}, Lcom/konka/kkinterface/tv/ChannelDesk;->saveAtvProgram(I)Z

    goto/16 :goto_2

    :sswitch_5
    iget v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->soundsystemindex:I

    add-int/lit8 v8, v8, 0x1

    rem-int/2addr v8, v5

    iput v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->soundsystemindex:I

    iget-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->values()[Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;

    move-result-object v9

    iget v10, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->soundsystemindex:I

    aget-object v9, v9, v10

    invoke-interface {v8, v9}, Lcom/konka/kkinterface/tv/ChannelDesk;->atvSetForceSoundSystem(Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;)Z

    iget-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->viewholder_atvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v8, v8, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_atvmanualtuning_soundsystem_val:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->soundsystem:[Ljava/lang/String;

    iget v10, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->soundsystemindex:I

    aget-object v9, v9, v10

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    iget v9, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->curChannelNumber:I

    invoke-interface {v8, v9}, Lcom/konka/kkinterface/tv/ChannelDesk;->saveAtvProgram(I)Z

    goto/16 :goto_2

    :sswitch_6
    invoke-direct {p0}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->SetAtvManualSkipStatus()V

    goto/16 :goto_2

    :sswitch_7
    sparse-switch v0, :sswitch_data_2

    :goto_4
    invoke-direct {p0}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->updateAtvManualtuningComponents()V

    goto/16 :goto_0

    :sswitch_8
    iget-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    const/16 v9, 0xbb8

    sget-object v10, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;->E_MANUAL_TUNE_MODE_FINE_TUNE_DOWN:Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

    invoke-interface {v8, v9, v7, v10}, Lcom/konka/kkinterface/tv/ChannelDesk;->atvSetManualTuningStart(IILcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;)Z

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->updateAtvManualtuningfreq()V

    iget-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    iget v9, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->curChannelNumber:I

    invoke-interface {v8, v9}, Lcom/konka/kkinterface/tv/ChannelDesk;->saveAtvProgram(I)Z

    goto :goto_4

    :sswitch_9
    iget-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/ChannelDesk;->GetTsStatus()Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    move-result-object v8

    sget-object v9, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_ATV_MANU_TUNING_RIGHT:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    if-ne v8, v9, :cond_3

    iget-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/ChannelDesk;->atvSetManualTuningEnd()V

    :cond_3
    new-instance v3, Landroid/content/Intent;

    const-string v8, "com.konka.tv.hotkey.service.UNFREEZE"

    invoke-direct {v3, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getS3DManagerInstance()Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v8

    iput-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->s3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    iget-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->s3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    sget-object v9, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-interface {v8, v9}, Lcom/konka/kkinterface/tv/S3DDesk;->setDisplayFormat(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;)Z

    iget-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    const/16 v9, 0x1388

    sget-object v10, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;->E_MANUAL_TUNE_MODE_SEARCH_ONE_TO_DOWN:Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

    invoke-interface {v8, v9, v7, v10}, Lcom/konka/kkinterface/tv/ChannelDesk;->atvSetManualTuningStart(IILcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;)Z

    goto :goto_4

    :sswitch_a
    new-instance v4, Landroid/content/Intent;

    const-string v8, "com.konka.tv.hotkey.service.UNFREEZE"

    invoke-direct {v4, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->sendBroadcast(Landroid/content/Intent;)V

    iget v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->curChannelNumber:I

    if-lez v8, :cond_5

    iget v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->curChannelNumber:I

    add-int/lit8 v8, v8, -0x1

    iput v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->curChannelNumber:I

    :cond_4
    :goto_5
    iget-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    iget v9, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->curChannelNumber:I

    sget-object v10, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->E_SERVICETYPE_ATV:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    invoke-interface {v8, v9, v10}, Lcom/konka/kkinterface/tv/ChannelDesk;->programSel(ILcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;)Z

    goto :goto_4

    :cond_5
    iget v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->curChannelNumber:I

    if-nez v8, :cond_4

    const/16 v8, 0x63

    iput v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->curChannelNumber:I

    goto :goto_5

    :sswitch_b
    iget v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->colorsystemindex:I

    add-int/2addr v8, v6

    add-int/lit8 v8, v8, -0x1

    rem-int/2addr v8, v6

    iput v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->colorsystemindex:I

    iget-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    iget v9, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->colorsystemindex:I

    invoke-direct {p0, v9}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->getColorSystemType(I)Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    move-result-object v9

    invoke-interface {v8, v9}, Lcom/konka/kkinterface/tv/ChannelDesk;->atvSetForceVedioSystem(Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;)V

    iget-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->viewholder_atvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v8, v8, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_atvmanualtuning_colorsystem_val:Landroid/widget/TextView;

    iget v9, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->colorsystemindex:I

    invoke-direct {p0, v9}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->getColorSystem(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    iget v9, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->curChannelNumber:I

    invoke-interface {v8, v9}, Lcom/konka/kkinterface/tv/ChannelDesk;->saveAtvProgram(I)Z

    goto/16 :goto_4

    :sswitch_c
    iget v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->soundsystemindex:I

    add-int/2addr v8, v5

    add-int/lit8 v8, v8, -0x1

    rem-int/2addr v8, v5

    iput v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->soundsystemindex:I

    iget-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->values()[Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;

    move-result-object v9

    iget v10, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->soundsystemindex:I

    aget-object v9, v9, v10

    invoke-interface {v8, v9}, Lcom/konka/kkinterface/tv/ChannelDesk;->atvSetForceSoundSystem(Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;)Z

    iget-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->viewholder_atvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v8, v8, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_atvmanualtuning_soundsystem_val:Landroid/widget/TextView;

    iget-object v9, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->soundsystem:[Ljava/lang/String;

    iget v10, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->soundsystemindex:I

    aget-object v9, v9, v10

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    iget v9, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->curChannelNumber:I

    invoke-interface {v8, v9}, Lcom/konka/kkinterface/tv/ChannelDesk;->saveAtvProgram(I)Z

    goto/16 :goto_4

    :sswitch_d
    invoke-direct {p0}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->SetAtvManualSkipStatus()V

    goto/16 :goto_4

    :sswitch_e
    iget-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/ChannelDesk;->GetTsStatus()Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    move-result-object v8

    sget-object v9, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_NONE:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    if-eq v8, v9, :cond_6

    iget-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/ChannelDesk;->atvSetManualTuningEnd()V

    :cond_6
    new-instance v2, Landroid/content/Intent;

    const-class v8, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v2, p0, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->finish()V

    const/4 v8, 0x0

    const v9, 0x7f040005    # com.konka.tvsettings.R.anim.anim_menu_exit

    invoke-virtual {p0, v8, v9}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->overridePendingTransition(II)V

    goto/16 :goto_0

    :sswitch_f
    iget-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/ChannelDesk;->GetTsStatus()Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    move-result-object v8

    sget-object v9, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_NONE:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    if-eq v8, v9, :cond_7

    iget-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/ChannelDesk;->atvSetManualTuningEnd()V

    :cond_7
    const-class v8, Lcom/konka/tvsettings/channel/ProgramSettingMain;

    invoke-virtual {v1, p0, v8}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v8, "currentPage"

    const/4 v9, 0x2

    invoke-virtual {v1, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->finish()V

    goto/16 :goto_0

    :sswitch_10
    const/4 v8, 0x1

    goto/16 :goto_1

    :sswitch_11
    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    new-instance v3, Landroid/content/Intent;

    const-string v8, "com.konka.tv.hotkey.service.UNFREEZE"

    invoke-direct {v3, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    iget v9, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->curChannelNumber:I

    sget-object v10, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->E_SERVICETYPE_ATV:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    invoke-interface {v8, v9, v10}, Lcom/konka/kkinterface/tv/ChannelDesk;->programSel(ILcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;)Z

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->updateAtvManualtuningComponentsWhenInputChannelNumber()V

    const/4 v8, 0x0

    iput v8, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->miChannelNum:I

    goto/16 :goto_0

    :sswitch_12
    const-string v8, "manualsearch"

    const-string v9, "ATVManualSearch KEYCODE_0 OnKeyDownAction"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_0

    :pswitch_1
    const-string v8, "manualsearch"

    const-string v9, "ATVManualSearch KEYCODE_0 OnKeyDownAction 00"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x0

    invoke-direct {p0, v8}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->inputCurChannelNumber(I)I

    goto/16 :goto_0

    :sswitch_13
    const-string v8, "manualsearch"

    const-string v9, "ATVManualSearch KEYCODE_1 OnKeyDownAction"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch v0, :pswitch_data_2

    goto/16 :goto_0

    :pswitch_2
    const-string v8, "manualsearch"

    const-string v9, "ATVManualSearch KEYCODE_1 OnKeyDownAction 11"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x1

    invoke-direct {p0, v8}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->inputCurChannelNumber(I)I

    goto/16 :goto_0

    :sswitch_14
    const-string v8, "manualsearch"

    const-string v9, "ATVManualSearch KEYCODE_2 OnKeyDownAction"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch v0, :pswitch_data_3

    goto/16 :goto_0

    :pswitch_3
    const-string v8, "manualsearch"

    const-string v9, "ATVManualSearch KEYCODE_2 OnKeyDownAction 22"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v8, 0x2

    invoke-direct {p0, v8}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->inputCurChannelNumber(I)I

    goto/16 :goto_0

    :sswitch_15
    packed-switch v0, :pswitch_data_4

    goto/16 :goto_0

    :pswitch_4
    const/4 v8, 0x3

    invoke-direct {p0, v8}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->inputCurChannelNumber(I)I

    goto/16 :goto_0

    :sswitch_16
    packed-switch v0, :pswitch_data_5

    goto/16 :goto_0

    :pswitch_5
    const/4 v8, 0x4

    invoke-direct {p0, v8}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->inputCurChannelNumber(I)I

    goto/16 :goto_0

    :sswitch_17
    packed-switch v0, :pswitch_data_6

    goto/16 :goto_0

    :pswitch_6
    const/4 v8, 0x5

    invoke-direct {p0, v8}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->inputCurChannelNumber(I)I

    goto/16 :goto_0

    :sswitch_18
    packed-switch v0, :pswitch_data_7

    goto/16 :goto_0

    :pswitch_7
    const/4 v8, 0x6

    invoke-direct {p0, v8}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->inputCurChannelNumber(I)I

    goto/16 :goto_0

    :sswitch_19
    packed-switch v0, :pswitch_data_8

    goto/16 :goto_0

    :pswitch_8
    const/4 v8, 0x7

    invoke-direct {p0, v8}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->inputCurChannelNumber(I)I

    goto/16 :goto_0

    :sswitch_1a
    packed-switch v0, :pswitch_data_9

    goto/16 :goto_0

    :pswitch_9
    const/16 v8, 0x8

    invoke-direct {p0, v8}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->inputCurChannelNumber(I)I

    goto/16 :goto_0

    :sswitch_1b
    packed-switch v0, :pswitch_data_a

    goto/16 :goto_0

    :pswitch_a
    const/16 v8, 0x9

    invoke-direct {p0, v8}, Lcom/konka/tvsettings/channel/Atvmanualtuning;->inputCurChannelNumber(I)I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_e
        0x7 -> :sswitch_12
        0x8 -> :sswitch_13
        0x9 -> :sswitch_14
        0xa -> :sswitch_15
        0xb -> :sswitch_16
        0xc -> :sswitch_17
        0xd -> :sswitch_18
        0xe -> :sswitch_19
        0xf -> :sswitch_1a
        0x10 -> :sswitch_1b
        0x15 -> :sswitch_7
        0x16 -> :sswitch_0
        0x42 -> :sswitch_11
        0x52 -> :sswitch_f
        0xb2 -> :sswitch_10
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x7f07000a -> :sswitch_3    # com.konka.tvsettings.R.id.linearlayout_cha_atvmanualtuning_channelnum
        0x7f07000d -> :sswitch_4    # com.konka.tvsettings.R.id.linearlayout_cha_atvmanualtuning_colorsystem
        0x7f070010 -> :sswitch_5    # com.konka.tvsettings.R.id.linearlayout_cha_atvmanualtuning_soundsystem
        0x7f070013 -> :sswitch_6    # com.konka.tvsettings.R.id.linearlayout_cha_atvmanualtuning_skip
        0x7f070016 -> :sswitch_2    # com.konka.tvsettings.R.id.linearlayout_cha_atvmanualtuning_starttuning
        0x7f070018 -> :sswitch_1    # com.konka.tvsettings.R.id.linearlayout_cha_atvmanualtuning_finetune
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x7f07000a -> :sswitch_a    # com.konka.tvsettings.R.id.linearlayout_cha_atvmanualtuning_channelnum
        0x7f07000d -> :sswitch_b    # com.konka.tvsettings.R.id.linearlayout_cha_atvmanualtuning_colorsystem
        0x7f070010 -> :sswitch_c    # com.konka.tvsettings.R.id.linearlayout_cha_atvmanualtuning_soundsystem
        0x7f070013 -> :sswitch_d    # com.konka.tvsettings.R.id.linearlayout_cha_atvmanualtuning_skip
        0x7f070016 -> :sswitch_9    # com.konka.tvsettings.R.id.linearlayout_cha_atvmanualtuning_starttuning
        0x7f070018 -> :sswitch_8    # com.konka.tvsettings.R.id.linearlayout_cha_atvmanualtuning_finetune
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x7f07000a
        :pswitch_0    # com.konka.tvsettings.R.id.linearlayout_cha_atvmanualtuning_channelnum
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x7f07000a
        :pswitch_1    # com.konka.tvsettings.R.id.linearlayout_cha_atvmanualtuning_channelnum
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x7f07000a
        :pswitch_2    # com.konka.tvsettings.R.id.linearlayout_cha_atvmanualtuning_channelnum
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x7f07000a
        :pswitch_3    # com.konka.tvsettings.R.id.linearlayout_cha_atvmanualtuning_channelnum
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x7f07000a
        :pswitch_4    # com.konka.tvsettings.R.id.linearlayout_cha_atvmanualtuning_channelnum
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x7f07000a
        :pswitch_5    # com.konka.tvsettings.R.id.linearlayout_cha_atvmanualtuning_channelnum
    .end packed-switch

    :pswitch_data_6
    .packed-switch 0x7f07000a
        :pswitch_6    # com.konka.tvsettings.R.id.linearlayout_cha_atvmanualtuning_channelnum
    .end packed-switch

    :pswitch_data_7
    .packed-switch 0x7f07000a
        :pswitch_7    # com.konka.tvsettings.R.id.linearlayout_cha_atvmanualtuning_channelnum
    .end packed-switch

    :pswitch_data_8
    .packed-switch 0x7f07000a
        :pswitch_8    # com.konka.tvsettings.R.id.linearlayout_cha_atvmanualtuning_channelnum
    .end packed-switch

    :pswitch_data_9
    .packed-switch 0x7f07000a
        :pswitch_9    # com.konka.tvsettings.R.id.linearlayout_cha_atvmanualtuning_channelnum
    .end packed-switch

    :pswitch_data_a
    .packed-switch 0x7f07000a
        :pswitch_a    # com.konka.tvsettings.R.id.linearlayout_cha_atvmanualtuning_channelnum
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/channel/Atvmanualtuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/CommonDesk;->releaseHandler(I)V

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onResume()V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onUserInteraction()V

    return-void
.end method
