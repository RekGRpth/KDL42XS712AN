.class public Lcom/konka/tvsettings/channel/ProgramListViewActivity;
.super Lcom/konka/tvsettings/teletext/MstarBaseActivity;
.source "ProgramListViewActivity.java"


# instance fields
.field private ImgDelete:Landroid/widget/TextView;

.field private ImgFavorite:Landroid/widget/TextView;

.field private ImgMove:Landroid/widget/TextView;

.field private ImgSkip:Landroid/widget/TextView;

.field private adapter:Lcom/konka/tvsettings/channel/ProgramEditAdapter;

.field cd:Lcom/konka/kkinterface/tv/ChannelDesk;

.field private currutPage:I

.field private flag:Z

.field private input:Landroid/widget/EditText;

.field private mCommonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

.field private m_nServiceNum:I

.field private m_u32Source:I

.field private m_u32Target:I

.field private moveFlag:Z

.field private moveKeyCount:I

.field private moveble:Z

.field private myHandler:Landroid/os/Handler;

.field private pageSize:I

.field private plvioTmp:Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

.field private plvios:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/tvsettings/channel/ProgramListViewItemObject;",
            ">;"
        }
    .end annotation
.end field

.field private position:I

.field private proListView:Landroid/widget/ListView;

.field private progInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/ProgramInfo;",
            ">;"
        }
    .end annotation
.end field

.field private serviceProv:Lcom/konka/kkinterface/tv/TvDeskProvider;

.field serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;-><init>()V

    iput-boolean v1, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->flag:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvios:Ljava/util/ArrayList;

    new-instance v0, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    invoke-direct {v0}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvioTmp:Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->progInfoList:Ljava/util/ArrayList;

    iput-object v2, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->adapter:Lcom/konka/tvsettings/channel/ProgramEditAdapter;

    iput-object v2, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->input:Landroid/widget/EditText;

    iput-boolean v1, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->moveFlag:Z

    iput-boolean v1, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->moveble:Z

    iput v1, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->moveKeyCount:I

    const/16 v0, 0xa

    iput v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->pageSize:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->currutPage:I

    iput v1, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->m_u32Source:I

    iput v1, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->m_u32Target:I

    iput v1, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->m_nServiceNum:I

    iput-object v2, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    new-instance v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/channel/ProgramListViewActivity$1;-><init>(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->myHandler:Landroid/os/Handler;

    return-void
.end method

.method private RefreshContent()V
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvios:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->progInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->getProgList()V

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->adapter:Lcom/konka/tvsettings/channel/ProgramEditAdapter;

    invoke-virtual {v0}, Lcom/konka/tvsettings/channel/ProgramEditAdapter;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->proListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidate()V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvios:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->flag:Z

    return v0
.end method

.method static synthetic access$10(Lcom/konka/tvsettings/channel/ProgramListViewActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->position:I

    return-void
.end method

.method static synthetic access$11(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Lcom/konka/tvsettings/channel/ProgramEditAdapter;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->adapter:Lcom/konka/tvsettings/channel/ProgramEditAdapter;

    return-object v0
.end method

.method static synthetic access$12(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->progInfoList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->ImgMove:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->proListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->moveFlag:Z

    return v0
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/channel/ProgramListViewActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->moveble:Z

    return-void
.end method

.method static synthetic access$6(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->moveble:Z

    return v0
.end method

.method static synthetic access$7(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->position:I

    return v0
.end method

.method static synthetic access$8(Lcom/konka/tvsettings/channel/ProgramListViewActivity;Lcom/konka/tvsettings/channel/ProgramListViewItemObject;Lcom/konka/tvsettings/channel/ProgramListViewItemObject;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->swapObject(Lcom/konka/tvsettings/channel/ProgramListViewItemObject;Lcom/konka/tvsettings/channel/ProgramListViewItemObject;)V

    return-void
.end method

.method static synthetic access$9(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)Lcom/konka/tvsettings/channel/ProgramListViewItemObject;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvioTmp:Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    return-object v0
.end method

.method private addOneListViewItem(Lcom/mstar/android/tvapi/common/vo/ProgramInfo;)V
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    new-instance v1, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    invoke-direct {v1}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;-><init>()V

    iget-object v2, p1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setTvName(Ljava/lang/String;)V

    iget-short v2, p1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    sget-object v3, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->E_SERVICETYPE_ATV:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->ordinal()I

    move-result v3

    if-ne v2, v3, :cond_2

    iget v2, p1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setTvNumber(Ljava/lang/String;)V

    :goto_0
    const/4 v0, 0x0

    iget-short v2, p1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->favorite:S

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v1, v0}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setFavoriteImg(Z)V

    iget-boolean v0, p1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->isSkip:Z

    invoke-virtual {v1, v0}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setSkipImg(Z)V

    iget-boolean v0, p1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->isScramble:Z

    invoke-virtual {v1, v0}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setSslImg(Z)V

    iget-short v2, p1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    invoke-virtual {v1, v2}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setServiceType(S)V

    iget-short v2, p1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    sget-object v3, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->E_SERVICETYPE_ATV:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->ordinal()I

    move-result v3

    if-ne v2, v3, :cond_3

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setSourceImg(Z)V

    :goto_1
    iget-object v2, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvios:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void

    :cond_2
    iget v2, p1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setTvNumber(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setSourceImg(Z)V

    goto :goto_1
.end method

.method private getProgList()V
    .locals 4

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_ATV_DTV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-interface {v2, v3}, Lcom/konka/kkinterface/tv/ChannelDesk;->getProgramCount(Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;)I

    move-result v2

    iput v2, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->m_nServiceNum:I

    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->m_nServiceNum:I

    if-lt v0, v2, :cond_0

    return-void

    :cond_0
    iget-object v2, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v2, v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->getProgramInfoByIndex(I)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-boolean v2, v1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->isDelete:Z

    if-nez v2, :cond_1

    iget-boolean v2, v1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->isVisible:Z

    if-nez v2, :cond_2

    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->progInfoList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, v1}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->addOneListViewItem(Lcom/mstar/android/tvapi/common/vo/ProgramInfo;)V

    goto :goto_1
.end method

.method private getfocusIndex()I
    .locals 5

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v0

    const/4 v2, 0x0

    :goto_0
    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->progInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v2, v3, :cond_0

    return v1

    :cond_0
    iget v4, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->progInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    iget v3, v3, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    if-ne v4, v3, :cond_1

    iget-short v4, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->progInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    iget-short v3, v3, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    if-ne v4, v3, :cond_1

    move v1, v2

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private setMoveTip(Z)V
    .locals 3
    .param p1    # Z

    const/4 v2, 0x4

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->ImgDelete:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->ImgFavorite:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->ImgMove:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->ImgSkip:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->ImgDelete:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->ImgFavorite:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->ImgMove:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->ImgSkip:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private swapObject(Lcom/konka/tvsettings/channel/ProgramListViewItemObject;Lcom/konka/tvsettings/channel/ProgramListViewItemObject;)V
    .locals 1
    .param p1    # Lcom/konka/tvsettings/channel/ProgramListViewItemObject;
    .param p2    # Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    invoke-virtual {p2}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->getTvName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setTvName(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->getTvNumber()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setTvNumber(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->isFavoriteImg()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setFavoriteImg(Z)V

    invoke-virtual {p2}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->isMoveImg()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setMoveImg(Z)V

    invoke-virtual {p2}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->isSkipImg()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setSkipImg(Z)V

    invoke-virtual {p2}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->isSslImg()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setSslImg(Z)V

    invoke-virtual {p2}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->isSourceImg()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setSourceImg(Z)V

    return-void
.end method


# virtual methods
.method checkChmoveble(II)Z
    .locals 5
    .param p1    # I
    .param p2    # I

    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/16 v3, 0x14

    if-ne p1, v3, :cond_3

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->progInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-lt p2, v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->progInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->progInfoList:Ljava/util/ArrayList;

    add-int/lit8 v4, p2, 0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    :cond_2
    :goto_1
    iget-short v3, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    iget-short v4, v1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    if-ne v3, v4, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :cond_3
    const/16 v3, 0x13

    if-ne p1, v3, :cond_2

    if-eqz p2, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->progInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->progInfoList:Ljava/util/ArrayList;

    add-int/lit8 v4, p2, -0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f030038    # com.konka.tvsettings.R.layout.program_list_view

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->setContentView(I)V

    const v1, 0x7f07015d    # com.konka.tvsettings.R.id.program_edit_img_skip

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->ImgSkip:Landroid/widget/TextView;

    const v1, 0x7f070161    # com.konka.tvsettings.R.id.program_edit_img_favorite

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->ImgFavorite:Landroid/widget/TextView;

    const v1, 0x7f07015f    # com.konka.tvsettings.R.id.program_edit_img_delete

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->ImgDelete:Landroid/widget/TextView;

    const v1, 0x7f070160    # com.konka.tvsettings.R.id.program_edit_img_move

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->ImgMove:Landroid/widget/TextView;

    const v1, 0x7f07015c    # com.konka.tvsettings.R.id.program_edit_list_view

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->proListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/TVRootApp;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->getProgList()V

    new-instance v1, Lcom/konka/tvsettings/channel/ProgramEditAdapter;

    iget-object v2, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvios:Ljava/util/ArrayList;

    invoke-direct {v1, p0, v2}, Lcom/konka/tvsettings/channel/ProgramEditAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v1, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->adapter:Lcom/konka/tvsettings/channel/ProgramEditAdapter;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->proListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->adapter:Lcom/konka/tvsettings/channel/ProgramEditAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->proListView:Landroid/widget/ListView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setDividerHeight(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->proListView:Landroid/widget/ListView;

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->getfocusIndex()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setSelection(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->proListView:Landroid/widget/ListView;

    new-instance v2, Lcom/konka/tvsettings/channel/ProgramListViewActivity$2;

    invoke-direct {v2, p0}, Lcom/konka/tvsettings/channel/ProgramListViewActivity$2;-><init>(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->proListView:Landroid/widget/ListView;

    new-instance v2, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;

    invoke-direct {v2, p0}, Lcom/konka/tvsettings/channel/ProgramListViewActivity$3;-><init>(Lcom/konka/tvsettings/channel/ProgramListViewActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->myHandler:Landroid/os/Handler;

    invoke-static {v1}, Lcom/konka/tvsettings/common/LittleDownTimer;->setHandler(Landroid/os/Handler;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 16
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->proListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getSelectedItemId()J

    move-result-wide v1

    long-to-int v9, v1

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->progInfoList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvios:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-gt v9, v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvios:Ljava/util/ArrayList;

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    :cond_0
    const-string v1, "onkeydown"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "listview index  = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0xb7

    move/from16 v0, p1

    if-ne v0, v1, :cond_4

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->moveFlag:Z

    if-nez v1, :cond_4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvios:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->proListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getSelectedItemId()J

    move-result-wide v1

    long-to-int v13, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->progInfoList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v13, v1, :cond_1

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->progInfoList:Ljava/util/ArrayList;

    invoke-virtual {v1, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;->E_DELETE:Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;

    iget v3, v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    iget-short v4, v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-interface/range {v1 .. v6}, Lcom/konka/kkinterface/tv/ChannelDesk;->setProgramAttribute(Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;ISIZ)V

    iget v1, v8, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    iget v2, v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    if-ne v1, v2, :cond_2

    iget-short v1, v8, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    iget-short v2, v14, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    if-ne v1, v2, :cond_2

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_ATV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    iget-short v2, v8, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    if-ne v1, v2, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;->E_FIRST_SERVICE_ATV:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;->E_DEFAULT:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/ChannelDesk;->changeToFirstService(Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;)Z

    :cond_2
    :goto_1
    invoke-direct/range {p0 .. p0}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->RefreshContent()V

    const/4 v1, 0x1

    goto :goto_0

    :cond_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_DTV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    iget-short v2, v8, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    if-ne v1, v2, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;->E_FIRST_SERVICE_DTV:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;->E_DEFAULT:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/ChannelDesk;->changeToFirstService(Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;)Z

    goto :goto_1

    :cond_4
    const/16 v1, 0xb9

    move/from16 v0, p1

    if-ne v0, v1, :cond_e

    if-eqz v12, :cond_e

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->flag:Z

    if-nez v1, :cond_5

    invoke-virtual {v12}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->isSourceImg()Z

    move-result v1

    if-eqz v1, :cond_e

    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvios:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_6

    const/4 v1, 0x1

    goto :goto_0

    :cond_6
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->moveFlag:Z

    if-eqz v1, :cond_7

    const/4 v1, 0x0

    :goto_2
    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->moveFlag:Z

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->moveFlag:Z

    move-object/from16 v0, p0

    invoke-direct {v0, v1}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->setMoveTip(Z)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->proListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getSelectedView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f07016a    # com.konka.tvsettings.R.id.program_edit_mov_img

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->moveFlag:Z

    if-eqz v1, :cond_8

    const/4 v1, 0x0

    invoke-virtual {v15, v1}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvios:Ljava/util/ArrayList;

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setMoveImg(Z)V

    :goto_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->proListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getSelectedItemId()J

    move-result-wide v1

    long-to-int v1, v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->position:I

    move-object/from16 v0, p0

    iget v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->position:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvios:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_9

    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_7
    const/4 v1, 0x1

    goto :goto_2

    :cond_8
    const/4 v1, 0x4

    invoke-virtual {v15, v1}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvios:Ljava/util/ArrayList;

    invoke-virtual {v1, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setMoveImg(Z)V

    goto :goto_3

    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvioTmp:Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvios:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->position:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v1}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->swapObject(Lcom/konka/tvsettings/channel/ProgramListViewItemObject;Lcom/konka/tvsettings/channel/ProgramListViewItemObject;)V

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->moveFlag:Z

    if-eqz v1, :cond_b

    move-object/from16 v0, p0

    iget v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->position:I

    move-object/from16 v0, p0

    iput v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->m_u32Source:I

    :cond_a
    :goto_4
    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_b
    move-object/from16 v0, p0

    iget v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->position:I

    move-object/from16 v0, p0

    iput v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->m_u32Target:I

    move-object/from16 v0, p0

    iget v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->m_u32Source:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->m_u32Target:I

    if-eq v1, v2, :cond_c

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->m_u32Source:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->m_u32Target:I

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/ChannelDesk;->moveProgram(II)V

    invoke-direct/range {p0 .. p0}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->RefreshContent()V

    :cond_c
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->progInfoList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_a

    move-object/from16 v0, p0

    iget v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->m_u32Target:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->progInfoList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v1, v2, :cond_d

    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_d
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->progInfoList:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->m_u32Target:I

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    iget v2, v7, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    invoke-static {}, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->values()[Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    move-result-object v3

    iget-short v4, v7, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    aget-object v3, v3, v4

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/ChannelDesk;->programSel(ILcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;)Z

    goto :goto_4

    :cond_e
    const/16 v1, 0x134

    move/from16 v0, p1

    if-ne v0, v1, :cond_10

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->proListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getSelectedItemId()J

    move-result-wide v1

    long-to-int v1, v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->position:I

    move-object/from16 v0, p0

    iget v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->position:I

    const/16 v2, 0xa

    if-lt v1, v2, :cond_f

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->proListView:Landroid/widget/ListView;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->position:I

    add-int/lit8 v2, v2, -0xa

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setSelection(I)V

    :goto_5
    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_f
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->proListView:Landroid/widget/ListView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_5

    :cond_10
    const/16 v1, 0x132

    move/from16 v0, p1

    if-ne v0, v1, :cond_12

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->proListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getSelectedItemId()J

    move-result-wide v1

    long-to-int v1, v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->position:I

    move-object/from16 v0, p0

    iget v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->position:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvios:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0xb

    if-gt v1, v2, :cond_11

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->proListView:Landroid/widget/ListView;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->position:I

    add-int/lit8 v2, v2, 0xa

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setSelection(I)V

    :goto_6
    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_11
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->proListView:Landroid/widget/ListView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvios:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_6

    :cond_12
    const/16 v1, 0x13

    move/from16 v0, p1

    if-ne v0, v1, :cond_13

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->proListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v1

    if-nez v1, :cond_13

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->proListView:Landroid/widget/ListView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvios:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setSelection(I)V

    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_13
    const/16 v1, 0x14

    move/from16 v0, p1

    if-ne v0, v1, :cond_14

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->proListView:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->plvios:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_14

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->proListView:Landroid/widget/ListView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setSelection(I)V

    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_14
    const/16 v1, 0x52

    move/from16 v0, p1

    if-ne v0, v1, :cond_15

    new-instance v10, Landroid/content/Intent;

    invoke-direct {v10}, Landroid/content/Intent;-><init>()V

    const-class v1, Lcom/konka/tvsettings/channel/ProgramSettingMain;

    move-object/from16 v0, p0

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual/range {p0 .. p0}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->finish()V

    :cond_15
    const/4 v1, 0x4

    move/from16 v0, p1

    if-ne v0, v1, :cond_16

    new-instance v11, Landroid/content/Intent;

    const-class v1, Lcom/konka/tvsettings/RootActivity;

    move-object/from16 v0, p0

    invoke-direct {v11, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual/range {p0 .. p0}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->finish()V

    :cond_16
    const/16 v1, 0xb2

    move/from16 v0, p1

    if-ne v0, v1, :cond_17

    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_17
    invoke-super/range {p0 .. p2}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto/16 :goto_0
.end method

.method protected onPause()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->pauseMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 2

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resumeMenu()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->serviceProv:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->serviceProv:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->mCommonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->mCommonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->getCustomerInfo()Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;->strCustomerName:Ljava/lang/String;

    const-string v1, "snowa"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->mCommonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->getCustomerInfo()Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;->strCustomerName:Ljava/lang/String;

    const-string v1, "MLS_SD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->mCommonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->getCustomerInfo()Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;->strCustomerName:Ljava/lang/String;

    const-string v1, "MLS_HD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->flag:Z

    :goto_0
    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onResume()V

    return-void

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->flag:Z

    goto :goto_0
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onStart()V

    const v0, 0x7f040009    # com.konka.tvsettings.R.anim.anim_zoom_in

    const v1, 0x7f040008    # com.konka.tvsettings.R.anim.anim_right_out

    invoke-virtual {p0, v0, v1}, Lcom/konka/tvsettings/channel/ProgramListViewActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resetMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onUserInteraction()V

    return-void
.end method

.method public splitString(Ljava/lang/String;I)Ljava/lang/String;
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # I

    if-nez p1, :cond_1

    const-string v3, ""

    :cond_0
    return-object v3

    :cond_1
    const/4 v2, 0x0

    const-string v3, ""

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v1, v4, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    array-length v4, v0

    add-int/2addr v2, v4

    if-gt v2, p2, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
