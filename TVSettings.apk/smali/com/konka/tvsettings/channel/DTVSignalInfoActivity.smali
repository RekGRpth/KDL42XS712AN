.class public Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;
.super Landroid/app/Activity;
.source "DTVSignalInfoActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/channel/DTVSignalInfoActivity$MyHandler;
    }
.end annotation


# static fields
.field private static final DTV_SIGNAL_REFRESH_UI:S = 0x1s

.field private static enableEpgDataThread:Z


# instance fields
.field private channelno:I

.field private dtvSignalHandler:Landroid/os/Handler;

.field private getDtvSignalThread:Ljava/lang/Thread;

.field private m_viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

.field private modulationindex:I

.field private modulationtype:[Ljava/lang/String;

.field private myHandler:Lcom/konka/tvsettings/channel/DTVSignalInfoActivity$MyHandler;

.field serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

.field ts:Lcom/konka/kkinterface/tv/ChannelDesk;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->enableEpgDataThread:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput v2, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->channelno:I

    iput v3, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->modulationindex:I

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "QPSK"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, "16 QAM"

    aput-object v2, v0, v1

    const-string v1, "64 QAM"

    aput-object v1, v0, v3

    const/4 v1, 0x3

    const-string v2, "256 QAM"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->modulationtype:[Ljava/lang/String;

    iput-object v4, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    iput-object v4, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->getDtvSignalThread:Ljava/lang/Thread;

    new-instance v0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity$1;-><init>(Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->dtvSignalHandler:Landroid/os/Handler;

    return-void
.end method

.method private InitialProgressValueForSignalQuality()V
    .locals 4

    const/4 v0, 0x0

    :goto_0
    const/16 v2, 0x9

    if-le v0, v2, :cond_0

    return-void

    :cond_0
    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->m_viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linear_dtv_signalquality_val:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200a9    # com.konka.tvsettings.R.drawable.picture_serchprogressbar_empty

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private InitialProgressValueForSignalStrengh()V
    .locals 4

    const/4 v0, 0x0

    :goto_0
    const/16 v2, 0x9

    if-le v0, v2, :cond_0

    return-void

    :cond_0
    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->m_viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linear_dtv_signalstrength_val:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200a9    # com.konka.tvsettings.R.drawable.picture_serchprogressbar_empty

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->updateDtvSignalInfoComponents()V

    return-void
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->setProgressValueForSignalQuality(I)V

    return-void
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->setProgressValueForSignalStrengh(I)V

    return-void
.end method

.method static synthetic access$3()Z
    .locals 1

    sget-boolean v0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->enableEpgDataThread:Z

    return v0
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->dtvSignalHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private setProgressValueForSignalQuality(I)V
    .locals 6
    .param p1    # I

    const v5, 0x7f0200aa    # com.konka.tvsettings.R.drawable.picture_serchprogressbar_solid

    const v4, 0x7f0200a9    # com.konka.tvsettings.R.drawable.picture_serchprogressbar_empty

    const/16 v2, 0xa

    const/16 v3, 0x9

    if-gt p1, v2, :cond_3

    if-lez p1, :cond_3

    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v2, p1, -0x1

    if-le v0, v2, :cond_1

    move v0, p1

    :goto_1
    if-le v0, v3, :cond_2

    :cond_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->m_viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linear_dtv_signalquality_val:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->m_viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linear_dtv_signalquality_val:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    if-le p1, v2, :cond_4

    const/4 v0, 0x0

    :goto_2
    if-gt v0, v3, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->m_viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linear_dtv_signalquality_val:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_3
    if-gt v0, v3, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->m_viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linear_dtv_signalquality_val:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_3
.end method

.method private setProgressValueForSignalStrengh(I)V
    .locals 6
    .param p1    # I

    const v5, 0x7f0200aa    # com.konka.tvsettings.R.drawable.picture_serchprogressbar_solid

    const v4, 0x7f0200a9    # com.konka.tvsettings.R.drawable.picture_serchprogressbar_empty

    const/16 v2, 0xa

    const/16 v3, 0x9

    if-gt p1, v2, :cond_3

    if-lez p1, :cond_3

    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v2, p1, -0x1

    if-le v0, v2, :cond_1

    move v0, p1

    :goto_1
    if-le v0, v3, :cond_2

    :cond_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->m_viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linear_dtv_signalstrength_val:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->m_viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linear_dtv_signalstrength_val:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    if-le p1, v2, :cond_4

    const/4 v0, 0x0

    :goto_2
    if-gt v0, v3, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->m_viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linear_dtv_signalstrength_val:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_3
    if-gt v0, v3, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->m_viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linear_dtv_signalstrength_val:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_3
.end method

.method private updateDtvSignalInfoComponents()V
    .locals 8

    iget-object v5, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrentSignalInformation()Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;

    move-result-object v2

    const/4 v1, 0x0

    iget-object v5, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v6, Lcom/mstar/android/tvapi/dtv/vo/RfInfo$EnumInfoType;->E_FIRST_TO_SHOW_RF:Lcom/mstar/android/tvapi/dtv/vo/RfInfo$EnumInfoType;

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvGetRFInfo(Lcom/mstar/android/tvapi/dtv/vo/RfInfo$EnumInfoType;I)Lcom/mstar/android/tvapi/dtv/vo/RfInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v5, "updateDtvSignalInfoComponents"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "channelno:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->channelno:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-short v5, v1, Lcom/mstar/android/tvapi/dtv/vo/RfInfo;->rfPhyNum:S

    iput v5, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->channelno:I

    iget-object v5, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->m_viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v5, v5, Lcom/konka/tvsettings/channel/ProgramViewHolder;->textview_dtvsignalinfo_curchannel:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget v7, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->channelno:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, v2, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->networkName:Ljava/lang/String;

    iget-object v5, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->m_viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v5, v5, Lcom/konka/tvsettings/channel/ProgramViewHolder;->textview_dtvsignalinfo_network:Landroid/widget/TextView;

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-short v5, v2, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->amMode:S

    iput v5, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->modulationindex:I

    iget-object v5, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->m_viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v5, v5, Lcom/konka/tvsettings/channel/ProgramViewHolder;->textview_ddtvsignalinfo_modulation:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->modulationtype:[Ljava/lang/String;

    iget v7, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->modulationindex:I

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->InitialProgressValueForSignalQuality()V

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->InitialProgressValueForSignalStrengh()V

    iget v4, v2, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->strength:I

    iget v3, v2, Lcom/mstar/android/tvapi/common/vo/DtvProgramSignalInfo;->quality:I

    iget-object v5, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->m_viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v5, v5, Lcom/konka/tvsettings/channel/ProgramViewHolder;->textview_ddtvsignalinfo_strength_percent:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "%"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v5, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->m_viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v5, v5, Lcom/konka/tvsettings/channel/ProgramViewHolder;->textview_ddtvsignalinfo_quality_percent:Landroid/widget/TextView;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "%"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f030012    # com.konka.tvsettings.R.layout.dtv_signal_information_menu

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->setContentView(I)V

    new-instance v1, Lcom/konka/tvsettings/channel/ProgramViewHolder;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/channel/ProgramViewHolder;-><init>(Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;)V

    iput-object v1, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->m_viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->m_viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    invoke-virtual {v1}, Lcom/konka/tvsettings/channel/ProgramViewHolder;->findViewforDTVSignalInfo()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/TVRootApp;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->updateDtvSignalInfoComponents()V

    new-instance v1, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity$MyHandler;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity$MyHandler;-><init>(Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;)V

    iput-object v1, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->myHandler:Lcom/konka/tvsettings/channel/DTVSignalInfoActivity$MyHandler;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->myHandler:Lcom/konka/tvsettings/channel/DTVSignalInfoActivity$MyHandler;

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setHandler(Landroid/os/Handler;I)Z

    sput-boolean v3, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->enableEpgDataThread:Z

    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity$2;

    invoke-direct {v2, p0}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity$2;-><init>(Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v1, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->getDtvSignalThread:Ljava/lang/Thread;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->getDtvSignalThread:Ljava/lang/Thread;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->getDtvSignalThread:Ljava/lang/Thread;

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    :cond_0
    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->myHandler:Lcom/konka/tvsettings/channel/DTVSignalInfoActivity$MyHandler;

    invoke-static {v1}, Lcom/konka/tvsettings/common/LittleDownTimer;->setHandler(Landroid/os/Handler;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->enableEpgDataThread:Z

    iget-object v0, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->getDtvSignalThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->run()V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    sparse-switch p1, :sswitch_data_0

    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    :goto_1
    return v2

    :sswitch_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->finish()V

    const/4 v2, 0x0

    const v3, 0x7f040005    # com.konka.tvsettings.R.anim.anim_menu_exit

    invoke-virtual {p0, v2, v3}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->overridePendingTransition(II)V

    goto :goto_0

    :sswitch_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v2, Lcom/konka/tvsettings/channel/ProgramSettingMain;

    invoke-virtual {v0, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->finish()V

    goto :goto_0

    :sswitch_2
    const/4 v2, 0x1

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x52 -> :sswitch_1
        0xb2 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 2

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->pauseMenu()V

    iget-object v0, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/CommonDesk;->releaseHandler(I)V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 3

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resumeMenu()V

    iget-object v0, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->myHandler:Lcom/konka/tvsettings/channel/DTVSignalInfoActivity$MyHandler;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->setHandler(Landroid/os/Handler;I)Z

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    const v0, 0x7f040009    # com.konka.tvsettings.R.anim.anim_zoom_in

    const v1, 0x7f040008    # com.konka.tvsettings.R.anim.anim_right_out

    invoke-virtual {p0, v0, v1}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resetMenu()V

    invoke-super {p0}, Landroid/app/Activity;->onUserInteraction()V

    return-void
.end method
