.class Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;
.super Ljava/lang/Object;
.source "ProgramLockListViewActivity.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 10
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/16 v3, 0x14

    const/16 v2, 0x13

    const/4 v4, 0x0

    const/4 v9, 0x1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->proListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->access$0(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getSelectedItemId()J

    move-result-wide v0

    long-to-int v8, v0

    if-ne p2, v2, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->moveFlag:Z
    invoke-static {v0}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->access$1(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-eq v0, v9, :cond_1

    :cond_0
    if-ne p2, v3, :cond_2

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->moveFlag:Z
    invoke-static {v0}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->access$1(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v9, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->progInfoList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->access$2(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    :cond_2
    if-ne p2, v2, :cond_3

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->moveFlag:Z
    invoke-static {v0}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->access$1(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    if-ne p2, v3, :cond_5

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->moveFlag:Z
    invoke-static {v0}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->access$1(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_5

    :cond_4
    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    invoke-virtual {v0, p2, v8}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->checkChmoveble(II)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    invoke-static {v0, v9}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->access$3(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;Z)V

    :cond_5
    if-ne p2, v2, :cond_6

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->moveFlag:Z
    invoke-static {v0}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->access$1(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-eq v0, v9, :cond_7

    :cond_6
    if-ne p2, v3, :cond_c

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->moveble:Z
    invoke-static {v0}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->access$4(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v9, :cond_c

    :cond_7
    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->moveFlag:Z
    invoke-static {v0}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->access$1(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->position:I
    invoke-static {v0}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->access$5(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)I

    move-result v0

    iget-object v1, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->plvios:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->access$6(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_8

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->plvios:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->access$6(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v8, v0, :cond_b

    :cond_8
    move v9, v4

    :cond_9
    :goto_0
    return v9

    :cond_a
    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    invoke-static {v0, v4}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->access$3(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;Z)V

    goto :goto_0

    :cond_b
    iget-object v2, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->plvios:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->access$6(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->position:I
    invoke-static {v1}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->access$5(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->plvios:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->access$6(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    # invokes: Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->swapObject(Lcom/konka/tvsettings/channel/ProgramListViewItemObject;Lcom/konka/tvsettings/channel/ProgramListViewItemObject;)V
    invoke-static {v2, v0, v1}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->access$7(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;Lcom/konka/tvsettings/channel/ProgramListViewItemObject;Lcom/konka/tvsettings/channel/ProgramListViewItemObject;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->plvios:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->access$6(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    iget-object v2, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->plvioTmp:Lcom/konka/tvsettings/channel/ProgramListViewItemObject;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->access$8(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    move-result-object v2

    # invokes: Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->swapObject(Lcom/konka/tvsettings/channel/ProgramListViewItemObject;Lcom/konka/tvsettings/channel/ProgramListViewItemObject;)V
    invoke-static {v1, v0, v2}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->access$7(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;Lcom/konka/tvsettings/channel/ProgramListViewItemObject;Lcom/konka/tvsettings/channel/ProgramListViewItemObject;)V

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    invoke-static {v0, v8}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->access$9(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;I)V

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->adapter:Lcom/konka/tvsettings/channel/ProgramEditAdapter;
    invoke-static {v0}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->access$10(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)Lcom/konka/tvsettings/channel/ProgramEditAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/tvsettings/channel/ProgramEditAdapter;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->proListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->access$0(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidate()V

    goto :goto_0

    :cond_c
    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->moveFlag:Z
    invoke-static {v0}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->access$1(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)Z

    move-result v0

    if-eqz v0, :cond_d

    move v9, v4

    goto :goto_0

    :cond_d
    const/16 v0, 0xba

    if-ne p2, v0, :cond_11

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v9, :cond_11

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->progInfoList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->access$2(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v8, v0, :cond_e

    move v9, v4

    goto :goto_0

    :cond_e
    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->progInfoList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->access$2(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    iget-boolean v5, v6, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->isLock:Z

    if-eqz v5, :cond_f

    move v5, v4

    :goto_1
    iput-boolean v5, v6, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->isLock:Z

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;->E_LOCK:Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;

    iget v2, v6, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    iget-short v3, v6, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    invoke-interface/range {v0 .. v5}, Lcom/konka/kkinterface/tv/ChannelDesk;->setProgramAttribute(Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;ISIZ)V

    if-eqz v5, :cond_10

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->plvios:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->access$6(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    invoke-virtual {v0, v9}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setLockImg(Z)V

    :goto_2
    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->adapter:Lcom/konka/tvsettings/channel/ProgramEditAdapter;
    invoke-static {v0}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->access$10(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)Lcom/konka/tvsettings/channel/ProgramEditAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/tvsettings/channel/ProgramEditAdapter;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->proListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->access$0(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidate()V

    goto/16 :goto_0

    :cond_f
    move v5, v9

    goto :goto_1

    :cond_10
    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->plvios:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->access$6(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    invoke-virtual {v0, v4}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setLockImg(Z)V

    goto :goto_2

    :cond_11
    const/16 v0, 0x42

    if-ne p2, v0, :cond_15

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v9, :cond_15

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->progInfoList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->access$2(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v8, v0, :cond_12

    move v9, v4

    goto/16 :goto_0

    :cond_12
    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    # getter for: Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->progInfoList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->access$2(Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v0

    iget v0, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    iget v1, v6, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    if-ne v0, v1, :cond_13

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v0

    iget-short v0, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    iget-short v1, v6, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    if-ne v0, v1, :cond_13

    const-string v0, "TuningService"

    const-string v1, "ProList:Select the same channel!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_13
    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvPipPopManager;->getSubInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v0, v1, :cond_14

    iget-short v0, v6, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    sget-object v1, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->E_SERVICETYPE_ATV:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_14

    const-string v0, "ProgramLockListViewActivity"

    const-string v1, "ATV selected, close PIP..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvPipPopManager;->isPipEnabled()Z

    move-result v0

    if-eqz v0, :cond_14

    new-instance v7, Landroid/content/Intent;

    const-string v0, "com.konka.hotkey.disablePip"

    invoke-direct {v7, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    invoke-virtual {v0, v7}, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->sendBroadcast(Landroid/content/Intent;)V

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvPipPopManager;->disablePip()Z

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/mstar/android/tv/TvPipPopManager;->setPipOnFlag(Z)Z

    :cond_14
    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity$1;->this$0:Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ProgramLockListViewActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    iget v1, v6, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    invoke-static {}, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->values()[Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    move-result-object v2

    iget-short v3, v6, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    aget-object v2, v2, v3

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->programSel(ILcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;)Z

    goto/16 :goto_0

    :cond_15
    move v9, v4

    goto/16 :goto_0
.end method
