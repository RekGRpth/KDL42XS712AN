.class public Lcom/konka/tvsettings/channel/DTVManualTuning;
.super Lcom/konka/tvsettings/teletext/MstarBaseActivity;
.source "DTVManualTuning.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/channel/DTVManualTuning$MyHandler;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$Enum3dType:[I


# instance fields
.field private final FREQUENCY_MAX:I

.field private QAM_Type:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

.field private bDvbcMode:Z

.field private bManualScanByUser:Z

.field private channelno:I

.field private dvbcfreq:D

.field private dvbcsymbol:S

.field private inputChannelNumber:I

.field private inputdvbcfreq:I

.field private inputfreq:I

.field private inputsymbol:I

.field private modulationindex:I

.field private modulationtype:[Ljava/lang/String;

.field private myHandler:Lcom/konka/tvsettings/channel/DTVManualTuning$MyHandler;

.field private previousChannelNumber:I

.field pvr:Lcom/mstar/android/tvapi/common/PvrManager;

.field private rfInfo:Lcom/mstar/android/tvapi/dtv/vo/RfInfo;

.field private s3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

.field private searchtype:I

.field serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

.field private strfreq:Ljava/lang/String;

.field private strsymbol:Ljava/lang/String;

.field private timerHandler:Landroid/os/Handler;

.field private timerRunnable:Ljava/lang/Runnable;

.field ts:Lcom/konka/kkinterface/tv/ChannelDesk;

.field private viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

.field private final waitExpireTime:I


# direct methods
.method static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$Enum3dType()[I
    .locals 3

    sget-object v0, Lcom/konka/tvsettings/channel/DTVManualTuning;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$Enum3dType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->values()[Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_2DTO3D:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_c

    :goto_1
    :try_start_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_CHECK_BORAD:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_b

    :goto_2
    :try_start_2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_DUALVIEW:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_a

    :goto_3
    :try_start_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FIELD_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_9

    :goto_4
    :try_start_4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_8

    :goto_5
    :try_start_5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_PACKING_1080P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_7

    :goto_6
    :try_start_6
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_PACKING_720P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :goto_7
    :try_start_7
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_LINE_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_5

    :goto_8
    :try_start_8
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_4

    :goto_9
    :try_start_9
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_PIXEL_ALTERNATIVE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_3

    :goto_a
    :try_start_a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_SIDE_BY_SIDE_HALF:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_2

    :goto_b
    :try_start_b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_TOP_BOTTOM:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_1

    :goto_c
    :try_start_c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_TYPE_NUM:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_0

    :goto_d
    sput-object v0, Lcom/konka/tvsettings/channel/DTVManualTuning;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$Enum3dType:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_d

    :catch_1
    move-exception v1

    goto :goto_c

    :catch_2
    move-exception v1

    goto :goto_b

    :catch_3
    move-exception v1

    goto :goto_a

    :catch_4
    move-exception v1

    goto :goto_9

    :catch_5
    move-exception v1

    goto :goto_8

    :catch_6
    move-exception v1

    goto :goto_7

    :catch_7
    move-exception v1

    goto :goto_6

    :catch_8
    move-exception v1

    goto :goto_5

    :catch_9
    move-exception v1

    goto :goto_4

    :catch_a
    move-exception v1

    goto :goto_3

    :catch_b
    move-exception v1

    goto/16 :goto_2

    :catch_c
    move-exception v1

    goto/16 :goto_1
.end method

.method public constructor <init>()V
    .locals 7

    const/4 v6, 0x2

    const/4 v5, -0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;-><init>()V

    iput v3, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->channelno:I

    iput v6, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->modulationindex:I

    const/16 v0, 0x1adb

    iput-short v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->dvbcsymbol:S

    const-wide v0, 0x407da00000000000L    # 474.0

    iput-wide v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->dvbcfreq:D

    iput v3, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->inputfreq:I

    iput v3, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->inputsymbol:I

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->strfreq:Ljava/lang/String;

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->strsymbol:Ljava/lang/String;

    sget-object v0, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;->E_CAB_QAM64:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->QAM_Type:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    const/16 v0, 0x3e7

    iput v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->FREQUENCY_MAX:I

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "16 QAM"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "32 QAM"

    aput-object v2, v0, v1

    const-string v1, "64 QAM"

    aput-object v1, v0, v6

    const/4 v1, 0x3

    const-string v2, "128 QAM"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "256 QAM"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->modulationtype:[Ljava/lang/String;

    iput-object v4, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    iput-boolean v3, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->bManualScanByUser:Z

    const/16 v0, 0x5dc

    iput v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->waitExpireTime:I

    iput v5, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->inputChannelNumber:I

    iput v5, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->previousChannelNumber:I

    iput v5, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->inputdvbcfreq:I

    iput-boolean v3, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->bDvbcMode:Z

    iput-object v4, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->s3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    iput-object v4, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->pvr:Lcom/mstar/android/tvapi/common/PvrManager;

    iput-object v4, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->rfInfo:Lcom/mstar/android/tvapi/dtv/vo/RfInfo;

    return-void
.end method

.method private InitialProgressValueForSignalQuality()V
    .locals 4

    const/4 v0, 0x0

    :goto_0
    const/16 v2, 0x9

    if-le v0, v2, :cond_0

    return-void

    :cond_0
    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ViewHolder;->linear_cha_dtvmanualtuning_signalquality_val:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200a9    # com.konka.tvsettings.R.drawable.picture_serchprogressbar_empty

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private InitialProgressValueForSignalStrengh()V
    .locals 4

    const/4 v0, 0x0

    :goto_0
    const/16 v2, 0x9

    if-le v0, v2, :cond_0

    return-void

    :cond_0
    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ViewHolder;->linear_cha_dtvmanualtuning_signalstrength_val:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200a9    # com.konka.tvsettings.R.drawable.picture_serchprogressbar_empty

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/channel/DTVManualTuning;)Lcom/konka/tvsettings/channel/ViewHolder;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/channel/DTVManualTuning;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/channel/DTVManualTuning;->setProgressValueForSignalQuality(I)V

    return-void
.end method

.method static synthetic access$10(Lcom/konka/tvsettings/channel/DTVManualTuning;)Lcom/mstar/android/tvapi/dtv/vo/RfInfo;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->rfInfo:Lcom/mstar/android/tvapi/dtv/vo/RfInfo;

    return-object v0
.end method

.method static synthetic access$11(Lcom/konka/tvsettings/channel/DTVManualTuning;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->previousChannelNumber:I

    return v0
.end method

.method static synthetic access$12(Lcom/konka/tvsettings/channel/DTVManualTuning;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->channelno:I

    return-void
.end method

.method static synthetic access$13(Lcom/konka/tvsettings/channel/DTVManualTuning;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->initScanResult()V

    return-void
.end method

.method static synthetic access$14(Lcom/konka/tvsettings/channel/DTVManualTuning;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->previousChannelNumber:I

    return-void
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/channel/DTVManualTuning;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/channel/DTVManualTuning;->setProgressValueForSignalStrengh(I)V

    return-void
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/channel/DTVManualTuning;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->bManualScanByUser:Z

    return v0
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/channel/DTVManualTuning;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->bDvbcMode:Z

    return v0
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/channel/DTVManualTuning;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->bManualScanByUser:Z

    return-void
.end method

.method static synthetic access$6(Lcom/konka/tvsettings/channel/DTVManualTuning;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->updatedtvManualtuningComponents()V

    return-void
.end method

.method static synthetic access$7(Lcom/konka/tvsettings/channel/DTVManualTuning;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->inputChannelNumber:I

    return-void
.end method

.method static synthetic access$8(Lcom/konka/tvsettings/channel/DTVManualTuning;)Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;
    .locals 1

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->getTurningCountry()Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$9(Lcom/konka/tvsettings/channel/DTVManualTuning;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->channelno:I

    return v0
.end method

.method private getTurningCountry()Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;
    .locals 2

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->getSystemCountry()Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    move-result-object v1

    return-object v1
.end method

.method private getcurfreq()V
    .locals 6

    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_DTV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-interface {v2, v3}, Lcom/konka/kkinterface/tv/ChannelDesk;->getProgramCount(Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;)I

    move-result v1

    if-lez v1, :cond_1

    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrentMuxInfo()Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbMuxInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v2, v0, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbMuxInfo;->frequency:I

    int-to-double v2, v2

    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double/2addr v2, v4

    iput-wide v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->dvbcfreq:D

    :goto_0
    return-void

    :cond_0
    const-wide v2, 0x408bc00000000000L    # 888.0

    iput-wide v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->dvbcfreq:D

    const-string v2, "Tvapp"

    const-string v3, "getCurrentMuxInfo error"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const-wide v2, 0x4078a00000000000L    # 394.0

    iput-wide v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->dvbcfreq:D

    goto :goto_0
.end method

.method private initScanResult()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_dtvmanualtuning_tuningresult_dtv_val:Landroid/widget/TextView;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_dtvmanualtuning_tuningresult_radio_val:Landroid/widget/TextView;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_dtvmanualtuning_tuningresult_data_val:Landroid/widget/TextView;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->InitialProgressValueForSignalQuality()V

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->InitialProgressValueForSignalStrengh()V

    return-void
.end method

.method private inputFrequencyNumber(I)V
    .locals 3
    .param p1    # I

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->strfreq:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->strfreq:Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->strfreq:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    const/16 v1, 0x3e7

    if-le v0, v1, :cond_0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->strfreq:Ljava/lang/String;

    int-to-double v1, p1

    iput-wide v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->dvbcfreq:D

    :goto_0
    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_dtvmanualtuning_frequency_val:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->strfreq:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->strfreq:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x4

    if-lt v1, v2, :cond_1

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->strfreq:Ljava/lang/String;

    int-to-double v1, p1

    iput-wide v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->dvbcfreq:D

    goto :goto_0

    :cond_1
    int-to-double v1, v0

    iput-wide v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->dvbcfreq:D

    goto :goto_0
.end method

.method private inputSymbolNumber(I)V
    .locals 3
    .param p1    # I

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->strsymbol:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->strsymbol:Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->strsymbol:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    int-to-short v0, v1

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->strsymbol:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x5

    if-lt v1, v2, :cond_0

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->strsymbol:Ljava/lang/String;

    int-to-short v1, p1

    iput-short v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->dvbcsymbol:S

    :goto_0
    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_dtvmanualtuning_symbol_val:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->strsymbol:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    iput-short v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->dvbcsymbol:S

    goto :goto_0
.end method

.method private refreshTimer()V
    .locals 4

    iget-object v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->timerHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->timerRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->timerHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->timerRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private setProgressValueForSignalQuality(I)V
    .locals 6
    .param p1    # I

    const v5, 0x7f0200aa    # com.konka.tvsettings.R.drawable.picture_serchprogressbar_solid

    const v4, 0x7f0200a9    # com.konka.tvsettings.R.drawable.picture_serchprogressbar_empty

    const/16 v2, 0xa

    const/16 v3, 0x9

    if-gt p1, v2, :cond_3

    if-lez p1, :cond_3

    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v2, p1, -0x1

    if-le v0, v2, :cond_1

    move v0, p1

    :goto_1
    if-le v0, v3, :cond_2

    :cond_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ViewHolder;->linear_cha_dtvmanualtuning_signalquality_val:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ViewHolder;->linear_cha_dtvmanualtuning_signalquality_val:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    if-le p1, v2, :cond_4

    const/4 v0, 0x0

    :goto_2
    if-gt v0, v3, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ViewHolder;->linear_cha_dtvmanualtuning_signalquality_val:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_3
    if-gt v0, v3, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ViewHolder;->linear_cha_dtvmanualtuning_signalquality_val:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_3
.end method

.method private setProgressValueForSignalStrengh(I)V
    .locals 6
    .param p1    # I

    const v5, 0x7f0200aa    # com.konka.tvsettings.R.drawable.picture_serchprogressbar_solid

    const v4, 0x7f0200a9    # com.konka.tvsettings.R.drawable.picture_serchprogressbar_empty

    const/16 v2, 0xa

    const/16 v3, 0x9

    if-gt p1, v2, :cond_3

    if-lez p1, :cond_3

    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v2, p1, -0x1

    if-le v0, v2, :cond_1

    move v0, p1

    :goto_1
    if-le v0, v3, :cond_2

    :cond_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ViewHolder;->linear_cha_dtvmanualtuning_signalstrength_val:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ViewHolder;->linear_cha_dtvmanualtuning_signalstrength_val:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    if-le p1, v2, :cond_4

    const/4 v0, 0x0

    :goto_2
    if-gt v0, v3, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ViewHolder;->linear_cha_dtvmanualtuning_signalstrength_val:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_3
    if-gt v0, v3, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ViewHolder;->linear_cha_dtvmanualtuning_signalstrength_val:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_3
.end method

.method private startdtvmanutuning()V
    .locals 12

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_dtvmanualtuning_frequency_val:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_dtvmanualtuning_symbol_val:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->dvbcsymbol:S

    const-string v1, "DTVManualTuning"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Sfreq========="

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v6}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v1

    iput-wide v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->dvbcfreq:D

    const-string v1, "DTVManualTuning"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "dvbcfreq======"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->dvbcfreq:D

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvGetAntennaType()Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;->E_ROUTE_DVBC:Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;

    if-ne v1, v2, :cond_1

    iget-short v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->dvbcsymbol:S

    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->QAM_Type:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    move v4, v3

    move v5, v3

    invoke-interface/range {v0 .. v5}, Lcom/konka/kkinterface/tv/ChannelDesk;->dvbcsetScanParam(SLcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;IIS)Z

    iget-wide v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->dvbcfreq:D

    const-wide v3, 0x408f400000000000L    # 1000.0

    mul-double/2addr v1, v3

    double-to-int v1, v1

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvManualScanFreq(I)Z

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvStartManualScan()Z

    :cond_0
    :goto_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->bManualScanByUser:Z

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getS3DManagerInstance()Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->s3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->s3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/S3DDesk;->setDisplayFormat(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;)Z

    return-void

    :cond_1
    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvGetAntennaType()Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;->E_ROUTE_DVBT:Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;

    if-ne v1, v2, :cond_4

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->getSystemCountry()Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    move-result-object v9

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CHINA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne v1, v9, :cond_3

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_UK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->setSystemCountry(Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;)V

    :cond_2
    :goto_1
    iget v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->channelno:I

    int-to-short v1, v1

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvManualScanRF(S)Z

    iget v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->channelno:I

    iput v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->previousChannelNumber:I

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvStartManualScan()Z

    goto :goto_0

    :cond_3
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne v1, v9, :cond_2

    const-string v11, "Europe/London"

    new-instance v10, Landroid/text/format/Time;

    invoke-direct {v10}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v10}, Landroid/text/format/Time;->setToNow()V

    iget-object v1, v10, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-virtual {v11, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "alarm"

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/DTVManualTuning;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/AlarmManager;

    invoke-virtual {v8, v11}, Landroid/app/AlarmManager;->setTimeZone(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvGetAntennaType()Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;->E_ROUTE_DTMB:Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->channelno:I

    int-to-short v1, v1

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvManualScanRF(S)Z

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvStartManualScan()Z

    goto :goto_0
.end method

.method private updatedtvManualtuningComponents()V
    .locals 7

    const/16 v6, 0x8

    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_dtvmanualtuning_symbol_val:Landroid/widget/TextView;

    iget-short v3, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->dvbcsymbol:S

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-wide v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->dvbcfreq:D

    const-wide v4, 0x404a400000000000L    # 52.5

    cmpl-double v2, v2, v4

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_dtvmanualtuning_frequency_val:Landroid/widget/TextView;

    iget-wide v3, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->dvbcfreq:D

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvGetAntennaType()Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;

    move-result-object v2

    sget-object v3, Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;->E_ROUTE_DVBT:Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvGetAntennaType()Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;

    move-result-object v2

    sget-object v3, Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;->E_ROUTE_DTMB:Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;

    if-ne v2, v3, :cond_3

    :cond_0
    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v3, Lcom/mstar/android/tvapi/dtv/vo/RfInfo$EnumInfoType;->E_FIRST_TO_SHOW_RF:Lcom/mstar/android/tvapi/dtv/vo/RfInfo$EnumInfoType;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvGetRFInfo(Lcom/mstar/android/tvapi/dtv/vo/RfInfo$EnumInfoType;I)Lcom/mstar/android/tvapi/dtv/vo/RfInfo;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->rfInfo:Lcom/mstar/android/tvapi/dtv/vo/RfInfo;

    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->rfInfo:Lcom/mstar/android/tvapi/dtv/vo/RfInfo;

    if-eqz v2, :cond_1

    const-string v2, "DST"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "channelno:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->channelno:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " rfInfo.rfPhyNum:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->rfInfo:Lcom/mstar/android/tvapi/dtv/vo/RfInfo;

    iget-short v4, v4, Lcom/mstar/android/tvapi/dtv/vo/RfInfo;->rfPhyNum:S

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " rfInfo.rfName:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->rfInfo:Lcom/mstar/android/tvapi/dtv/vo/RfInfo;

    iget-object v4, v4, Lcom/mstar/android/tvapi/dtv/vo/RfInfo;->rfName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->rfInfo:Lcom/mstar/android/tvapi/dtv/vo/RfInfo;

    iget-short v2, v2, Lcom/mstar/android/tvapi/dtv/vo/RfInfo;->rfPhyNum:S

    iput v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->channelno:I

    iget v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->channelno:I

    iput v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->previousChannelNumber:I

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->rfInfo:Lcom/mstar/android/tvapi/dtv/vo/RfInfo;

    iget-object v1, v2, Lcom/mstar/android/tvapi/dtv/vo/RfInfo;->rfName:Ljava/lang/String;

    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_dtvmanualtuning_channelnum_val:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    const v2, 0x7f07008c    # com.konka.tvsettings.R.id.linearlayout_cha_dtvmanualtuning_frequency

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/channel/DTVManualTuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const v2, 0x7f07008f    # com.konka.tvsettings.R.id.linearlayout_cha_dtvmanualtuning_modulation

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/channel/DTVManualTuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const v2, 0x7f070092    # com.konka.tvsettings.R.id.linearlayout_cha_dtvmanualtuning_symbol

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/channel/DTVManualTuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_1
    return-void

    :cond_2
    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v2, v2, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_dtvmanualtuning_frequency_val:Landroid/widget/TextView;

    iget-wide v3, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->dvbcfreq:D

    double-to-int v3, v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_3
    const v2, 0x7f07008a    # com.konka.tvsettings.R.id.linearlayout_cha_dtvmanualtuning_channelnum

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/channel/DTVManualTuning;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method public close3D()V
    .locals 8

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->getCurrent3dFormat()Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v2

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getS3DManagerInstance()Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/S3DDesk;->getDisplay3DTo2DMode()Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    move-result-object v0

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-ne v4, v2, :cond_0

    sget-object v4, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_1

    if-eq v0, v4, :cond_1

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    const/4 v5, 0x1

    sget-object v6, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7, v3}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_0
    :try_start_2
    invoke-static {}, Lcom/konka/tvsettings/channel/DTVManualTuning;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$Enum3dType()[I

    move-result-object v4

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    :pswitch_0
    sget-object v4, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    if-eq v0, v4, :cond_2

    sget-object v4, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;->DB_ThreeD_Video_3DTO2D_FRAME_PACKING:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_3DTO2D;

    if-eq v0, v4, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v4

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3dTo2d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z

    :cond_2
    :goto_1
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :pswitch_1
    :try_start_3
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v4

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z

    goto :goto_1

    :pswitch_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v4

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_PACKING_720P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3dTo2d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z

    goto :goto_1

    :pswitch_3
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v4

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_FRAME_PACKING_1080P:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3dTo2d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x1

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030013    # com.konka.tvsettings.R.layout.dtvmanualtuning

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->setContentView(I)V

    new-instance v0, Lcom/konka/tvsettings/channel/ViewHolder;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/channel/ViewHolder;-><init>(Lcom/konka/tvsettings/channel/DTVManualTuning;)V

    iput-object v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    invoke-virtual {v0}, Lcom/konka/tvsettings/channel/ViewHolder;->findViewForDtvManualTuning()V

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->InitialProgressValueForSignalQuality()V

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->InitialProgressValueForSignalStrengh()V

    new-instance v0, Lcom/konka/tvsettings/channel/DTVManualTuning$MyHandler;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/channel/DTVManualTuning$MyHandler;-><init>(Lcom/konka/tvsettings/channel/DTVManualTuning;)V

    iput-object v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->myHandler:Lcom/konka/tvsettings/channel/DTVManualTuning$MyHandler;

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->getApplication()Landroid/app/Application;

    move-result-object v7

    check-cast v7, Lcom/konka/tvsettings/TVRootApp;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->myHandler:Lcom/konka/tvsettings/channel/DTVManualTuning$MyHandler;

    invoke-interface {v0, v1, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->setHandler(Landroid/os/Handler;I)Z

    iget-object v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->makeSourceDtv()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "searchtype"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->searchtype:I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->pvr:Lcom/mstar/android/tvapi/common/PvrManager;

    iput v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->inputdvbcfreq:I

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->getcurfreq()V

    iget-object v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvGetAntennaType()Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;

    move-result-object v6

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\n=====>>>>antenna "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v0, Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;->E_ROUTE_DVBC:Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;

    if-ne v6, v0, :cond_0

    iput-boolean v4, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->bDvbcMode:Z

    iget-object v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    iget-short v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->dvbcsymbol:S

    iget-object v2, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->QAM_Type:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    move v4, v3

    move v5, v3

    invoke-interface/range {v0 .. v5}, Lcom/konka/kkinterface/tv/ChannelDesk;->dvbcsetScanParam(SLcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;IIS)Z

    :cond_0
    invoke-direct {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->updatedtvManualtuningComponents()V

    iput-boolean v3, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->bManualScanByUser:Z

    iget-object v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvGetAntennaType()Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;

    move-result-object v0

    sget-object v1, Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;->E_ROUTE_DVBT:Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    iget v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->channelno:I

    int-to-short v1, v1

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvManualScanRF(S)Z

    :cond_1
    iget-object v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->linear_cha_dtvmanualtuning_channelnum:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/tvsettings/channel/DTVManualTuning$1;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/channel/DTVManualTuning$1;-><init>(Lcom/konka/tvsettings/channel/DTVManualTuning;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->timerHandler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/tvsettings/channel/DTVManualTuning$2;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/channel/DTVManualTuning$2;-><init>(Lcom/konka/tvsettings/channel/DTVManualTuning;)V

    iput-object v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->timerRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method protected onDestroy()V
    .locals 4

    const/4 v3, 0x0

    const-string v0, "TvApp"

    const-string v1, "DTVManualTuning destroy~~"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->bManualScanByUser:Z

    if-eqz v0, :cond_0

    const-string v0, "TvApp"

    const-string v1, "DTVManualTuning exit"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->DtvStopScan()Z

    iget-object v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;->E_FIRST_SERVICE_DTV:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;->E_DEFAULT:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->changeToFirstService(Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;)Z

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->timerHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->timerRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iput-object v3, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->timerHandler:Landroid/os/Handler;

    iput-object v3, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->timerRunnable:Ljava/lang/Runnable;

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 11
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    new-instance v5, Ljava/lang/String;

    invoke-direct {v5}, Ljava/lang/String;-><init>()V

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->getCurrentFocus()Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getId()I

    move-result v0

    invoke-static {}, Lcom/konka/kkinterface/tv/TranslateIrKeyDesk;->getInstance()Lcom/konka/kkinterface/tv/TranslateIrKeyDesk;

    move-result-object v7

    invoke-virtual {v7, p1}, Lcom/konka/kkinterface/tv/TranslateIrKeyDesk;->translateIRKey(I)I

    move-result p1

    sparse-switch p1, :sswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    const/4 v7, 0x7

    if-lt p1, v7, :cond_1

    const/16 v7, 0x10

    if-le p1, v7, :cond_2

    :cond_1
    const/4 v7, -0x1

    iput v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->inputChannelNumber:I

    const/4 v7, -0x1

    iput v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->inputdvbcfreq:I

    :cond_2
    invoke-super {p0, p1, p2}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v7

    :goto_1
    return v7

    :sswitch_0
    packed-switch v0, :pswitch_data_0

    :pswitch_1
    goto :goto_0

    :pswitch_2
    iget-object v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v7}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v6

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvGetAntennaType()Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;

    move-result-object v7

    sget-object v8, Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;->E_ROUTE_DVBT:Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;

    if-eq v7, v8, :cond_3

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvGetAntennaType()Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;

    move-result-object v7

    sget-object v8, Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;->E_ROUTE_DTMB:Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;

    if-ne v7, v8, :cond_4

    :cond_3
    sget-object v7, Lcom/mstar/android/tvapi/dtv/vo/RfInfo$EnumInfoType;->E_NEXT_RF:Lcom/mstar/android/tvapi/dtv/vo/RfInfo$EnumInfoType;

    iget v8, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->channelno:I

    invoke-interface {v6, v7, v8}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvGetRFInfo(Lcom/mstar/android/tvapi/dtv/vo/RfInfo$EnumInfoType;I)Lcom/mstar/android/tvapi/dtv/vo/RfInfo;

    move-result-object v7

    iput-object v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->rfInfo:Lcom/mstar/android/tvapi/dtv/vo/RfInfo;

    iget-object v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->rfInfo:Lcom/mstar/android/tvapi/dtv/vo/RfInfo;

    iget-short v7, v7, Lcom/mstar/android/tvapi/dtv/vo/RfInfo;->rfPhyNum:S

    iput v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->channelno:I

    iget-object v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->rfInfo:Lcom/mstar/android/tvapi/dtv/vo/RfInfo;

    iget-object v5, v7, Lcom/mstar/android/tvapi/dtv/vo/RfInfo;->rfName:Ljava/lang/String;

    :cond_4
    iget-object v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v7, v7, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_dtvmanualtuning_channelnum_val:Landroid/widget/TextView;

    invoke-virtual {v7, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->refreshTimer()V

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->initScanResult()V

    iget-object v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v7}, Lcom/konka/kkinterface/tv/ChannelDesk;->DtvStopScan()Z

    goto :goto_0

    :pswitch_3
    iget v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->modulationindex:I

    sget-object v8, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;->E_CAB_QAM256:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;->ordinal()I

    move-result v8

    if-ne v7, v8, :cond_5

    const/4 v7, 0x0

    iput v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->modulationindex:I

    :goto_2
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;->values()[Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    move-result-object v7

    iget v8, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->modulationindex:I

    aget-object v7, v7, v8

    iput-object v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->QAM_Type:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    iget-object v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v7, v7, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_dtvmanualtuning_modulation_val:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->modulationtype:[Ljava/lang/String;

    iget v9, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->modulationindex:I

    aget-object v8, v8, v9

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_5
    iget v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->modulationindex:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->modulationindex:I

    goto :goto_2

    :pswitch_4
    iget-wide v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->dvbcfreq:D

    const-wide/high16 v9, 0x404a000000000000L    # 52.0

    cmpl-double v7, v7, v9

    if-nez v7, :cond_6

    const-wide v7, 0x404a400000000000L    # 52.5

    iput-wide v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->dvbcfreq:D

    :goto_3
    invoke-direct {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->updatedtvManualtuningComponents()V

    goto/16 :goto_0

    :cond_6
    iget-wide v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->dvbcfreq:D

    const-wide v9, 0x404a400000000000L    # 52.5

    cmpl-double v7, v7, v9

    if-nez v7, :cond_7

    const-wide v7, 0x404a800000000000L    # 53.0

    iput-wide v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->dvbcfreq:D

    goto :goto_3

    :cond_7
    iget-wide v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->dvbcfreq:D

    const-wide/high16 v9, 0x3ff0000000000000L    # 1.0

    add-double/2addr v7, v9

    const-wide v9, 0x408f380000000000L    # 999.0

    rem-double/2addr v7, v9

    iput-wide v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->dvbcfreq:D

    goto :goto_3

    :sswitch_1
    packed-switch v0, :pswitch_data_1

    :pswitch_5
    goto/16 :goto_0

    :pswitch_6
    iget-object v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v7}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v6

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvGetAntennaType()Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;

    move-result-object v7

    sget-object v8, Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;->E_ROUTE_DVBT:Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;

    if-eq v7, v8, :cond_8

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvGetAntennaType()Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;

    move-result-object v7

    sget-object v8, Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;->E_ROUTE_DTMB:Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;

    if-ne v7, v8, :cond_9

    :cond_8
    sget-object v7, Lcom/mstar/android/tvapi/dtv/vo/RfInfo$EnumInfoType;->E_PREV_RF:Lcom/mstar/android/tvapi/dtv/vo/RfInfo$EnumInfoType;

    iget v8, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->channelno:I

    invoke-interface {v6, v7, v8}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvGetRFInfo(Lcom/mstar/android/tvapi/dtv/vo/RfInfo$EnumInfoType;I)Lcom/mstar/android/tvapi/dtv/vo/RfInfo;

    move-result-object v7

    iput-object v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->rfInfo:Lcom/mstar/android/tvapi/dtv/vo/RfInfo;

    iget-object v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->rfInfo:Lcom/mstar/android/tvapi/dtv/vo/RfInfo;

    iget-short v7, v7, Lcom/mstar/android/tvapi/dtv/vo/RfInfo;->rfPhyNum:S

    iput v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->channelno:I

    iget-object v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->rfInfo:Lcom/mstar/android/tvapi/dtv/vo/RfInfo;

    iget-object v5, v7, Lcom/mstar/android/tvapi/dtv/vo/RfInfo;->rfName:Ljava/lang/String;

    :cond_9
    iget-object v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v7, v7, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_dtvmanualtuning_channelnum_val:Landroid/widget/TextView;

    invoke-virtual {v7, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->refreshTimer()V

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->initScanResult()V

    iget-object v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v7}, Lcom/konka/kkinterface/tv/ChannelDesk;->DtvStopScan()Z

    goto/16 :goto_0

    :pswitch_7
    iget v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->modulationindex:I

    sget-object v8, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;->E_CAB_QAM16:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;->ordinal()I

    move-result v8

    if-ne v7, v8, :cond_a

    sget-object v7, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;->E_CAB_QAM256:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    invoke-virtual {v7}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;->ordinal()I

    move-result v7

    iput v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->modulationindex:I

    :goto_4
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;->values()[Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    move-result-object v7

    iget v8, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->modulationindex:I

    aget-object v7, v7, v8

    iput-object v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->QAM_Type:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    iget-object v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v7, v7, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_dtvmanualtuning_modulation_val:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->modulationtype:[Ljava/lang/String;

    iget v9, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->modulationindex:I

    aget-object v8, v8, v9

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_a
    iget v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->modulationindex:I

    add-int/lit8 v7, v7, -0x1

    iput v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->modulationindex:I

    goto :goto_4

    :pswitch_8
    iget-wide v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->dvbcfreq:D

    const-wide v9, 0x404a800000000000L    # 53.0

    cmpl-double v7, v7, v9

    if-nez v7, :cond_b

    const-wide v7, 0x404a400000000000L    # 52.5

    iput-wide v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->dvbcfreq:D

    :goto_5
    invoke-direct {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->updatedtvManualtuningComponents()V

    goto/16 :goto_0

    :cond_b
    iget-wide v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->dvbcfreq:D

    const-wide v9, 0x404a400000000000L    # 52.5

    cmpl-double v7, v7, v9

    if-nez v7, :cond_c

    const-wide/high16 v7, 0x404a000000000000L    # 52.0

    iput-wide v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->dvbcfreq:D

    goto :goto_5

    :cond_c
    iget-wide v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->dvbcfreq:D

    const-wide v9, 0x408f380000000000L    # 999.0

    add-double/2addr v7, v9

    const-wide/high16 v9, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v7, v9

    const-wide v9, 0x408f380000000000L    # 999.0

    rem-double/2addr v7, v9

    iput-wide v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->dvbcfreq:D

    goto :goto_5

    :sswitch_2
    const v7, 0x7f07008a    # com.konka.tvsettings.R.id.linearlayout_cha_dtvmanualtuning_channelnum

    if-ne v0, v7, :cond_13

    const/4 v7, -0x1

    iget v8, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->inputChannelNumber:I

    if-ne v7, v8, :cond_f

    const/4 v7, 0x7

    if-ne v7, p1, :cond_d

    const/4 v7, 0x1

    goto/16 :goto_1

    :cond_d
    add-int/lit8 v7, p1, -0x7

    iput v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->inputChannelNumber:I

    :goto_6
    const-string v7, "inputno"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "#######inputChannelNumber:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v9, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->inputChannelNumber:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "  previousChannelNumber:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->previousChannelNumber:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "  getTurningCountry():"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->getTurningCountry()Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->inputChannelNumber:I

    const/16 v8, 0x45

    if-le v7, v8, :cond_e

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->getTurningCountry()Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    move-result-object v7

    sget-object v8, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUSSIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne v7, v8, :cond_12

    iget v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->inputChannelNumber:I

    const/16 v8, 0x46

    if-ne v7, v8, :cond_12

    const-string v7, "inputno"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "#######inputChannelNumber:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v9, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->inputChannelNumber:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "  previousChannelNumber:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->previousChannelNumber:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_e
    :goto_7
    iget-object v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v7, v7, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_dtvmanualtuning_channelnum_val:Landroid/widget/TextView;

    iget v8, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->inputChannelNumber:I

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->inputChannelNumber:I

    iput v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->channelno:I

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->refreshTimer()V

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->initScanResult()V

    iget-object v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v7}, Lcom/konka/kkinterface/tv/ChannelDesk;->DtvStopScan()Z

    goto/16 :goto_0

    :cond_f
    iget v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->inputChannelNumber:I

    const/16 v8, 0xa

    if-lt v7, v8, :cond_11

    const/4 v7, 0x7

    if-ne v7, p1, :cond_10

    const/4 v7, 0x1

    goto/16 :goto_1

    :cond_10
    add-int/lit8 v7, p1, -0x7

    iput v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->inputChannelNumber:I

    goto/16 :goto_6

    :cond_11
    iget v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->inputChannelNumber:I

    mul-int/lit8 v7, v7, 0xa

    add-int/lit8 v8, p1, -0x7

    add-int/2addr v7, v8

    iput v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->inputChannelNumber:I

    goto/16 :goto_6

    :cond_12
    iget v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->previousChannelNumber:I

    iput v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->inputChannelNumber:I

    goto :goto_7

    :cond_13
    const v7, 0x7f07008c    # com.konka.tvsettings.R.id.linearlayout_cha_dtvmanualtuning_frequency

    if-ne v0, v7, :cond_19

    const/4 v7, -0x1

    iget v8, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->inputdvbcfreq:I

    if-ne v7, v8, :cond_16

    const/4 v7, 0x7

    if-ne v7, p1, :cond_14

    const/4 v7, 0x1

    goto/16 :goto_1

    :cond_14
    add-int/lit8 v7, p1, -0x7

    iput v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->inputdvbcfreq:I

    :goto_8
    iget v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->inputdvbcfreq:I

    const/16 v8, 0x3e6

    if-le v7, v8, :cond_15

    iget-wide v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->dvbcfreq:D

    double-to-int v7, v7

    rem-int/lit16 v7, v7, 0x3e7

    iput v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->inputdvbcfreq:I

    :cond_15
    iget v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->inputdvbcfreq:I

    int-to-double v7, v7

    iput-wide v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->dvbcfreq:D

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->updatedtvManualtuningComponents()V

    goto/16 :goto_0

    :cond_16
    iget v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->inputdvbcfreq:I

    const/16 v8, 0x64

    if-lt v7, v8, :cond_18

    const/4 v7, 0x7

    if-ne v7, p1, :cond_17

    const/4 v7, 0x1

    goto/16 :goto_1

    :cond_17
    add-int/lit8 v7, p1, -0x7

    iput v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->inputdvbcfreq:I

    goto :goto_8

    :cond_18
    iget-wide v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->dvbcfreq:D

    double-to-int v7, v7

    mul-int/lit8 v7, v7, 0xa

    add-int/lit8 v8, p1, -0x7

    add-int/2addr v7, v8

    iput v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->inputdvbcfreq:I

    goto :goto_8

    :cond_19
    const v7, 0x7f070092    # com.konka.tvsettings.R.id.linearlayout_cha_dtvmanualtuning_symbol

    if-ne v0, v7, :cond_0

    add-int/lit8 v7, p1, -0x7

    iput v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->inputsymbol:I

    iget v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->inputsymbol:I

    invoke-direct {p0, v7}, Lcom/konka/tvsettings/channel/DTVManualTuning;->inputSymbolNumber(I)V

    goto/16 :goto_0

    :sswitch_3
    const-string v7, "DTVManualTuning"

    const-string v8, "=======start to tuning \n"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch v0, :pswitch_data_2

    goto/16 :goto_0

    :pswitch_9
    const-string v7, "DTVManualTuning"

    const-string v8, "=======start to tuning \n"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->pvr:Lcom/mstar/android/tvapi/common/PvrManager;

    invoke-virtual {v7}, Lcom/mstar/android/tvapi/common/PvrManager;->isTimeShiftRecording()Z

    move-result v7

    if-eqz v7, :cond_1a

    const-string v7, "DTVManualTuning"

    const-string v8, "=======stop time shift\n"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v7, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->pvr:Lcom/mstar/android/tvapi/common/PvrManager;

    invoke-virtual {v7}, Lcom/mstar/android/tvapi/common/PvrManager;->stopTimeShiftRecord()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1a
    :goto_9
    new-instance v4, Landroid/content/Intent;

    const-string v7, "com.konka.tv.hotkey.service.UNFREEZE"

    invoke-direct {v4, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lcom/konka/tvsettings/channel/DTVManualTuning;->sendBroadcast(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f0a0070    # com.konka.tvsettings.R.string.str_cha_dtvmanualtuning_searching

    invoke-static {v7, v8}, Lcom/konka/tvsettings/common/LittleMenu;->popToast(Landroid/content/Context;I)V

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->startdtvmanutuning()V

    goto/16 :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_9

    :sswitch_4
    new-instance v3, Landroid/content/Intent;

    const-class v7, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v3, p0, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/channel/DTVManualTuning;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->finish()V

    const/4 v7, 0x0

    const v8, 0x7f040005    # com.konka.tvsettings.R.anim.anim_menu_exit

    invoke-virtual {p0, v7, v8}, Lcom/konka/tvsettings/channel/DTVManualTuning;->overridePendingTransition(II)V

    goto/16 :goto_0

    :sswitch_5
    const-class v7, Lcom/konka/tvsettings/channel/ProgramSettingMain;

    invoke-virtual {v2, p0, v7}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v7, "currentPage"

    const/4 v8, 0x2

    invoke-virtual {v2, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/channel/DTVManualTuning;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->finish()V

    goto/16 :goto_0

    :sswitch_6
    const/4 v7, 0x1

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_4
        0x7 -> :sswitch_2
        0x8 -> :sswitch_2
        0x9 -> :sswitch_2
        0xa -> :sswitch_2
        0xb -> :sswitch_2
        0xc -> :sswitch_2
        0xd -> :sswitch_2
        0xe -> :sswitch_2
        0xf -> :sswitch_2
        0x10 -> :sswitch_2
        0x15 -> :sswitch_1
        0x16 -> :sswitch_0
        0x42 -> :sswitch_3
        0x52 -> :sswitch_5
        0xb2 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x7f07008a
        :pswitch_2    # com.konka.tvsettings.R.id.linearlayout_cha_dtvmanualtuning_channelnum
        :pswitch_1    # com.konka.tvsettings.R.id.textview_cha_dtvmanualtuning_channelnum_val
        :pswitch_4    # com.konka.tvsettings.R.id.linearlayout_cha_dtvmanualtuning_frequency
        :pswitch_1    # com.konka.tvsettings.R.id.textview_cha_dtvmanualtuning_frequency
        :pswitch_1    # com.konka.tvsettings.R.id.textview_cha_dtvmanualtuning_frequency_val
        :pswitch_3    # com.konka.tvsettings.R.id.linearlayout_cha_dtvmanualtuning_modulation
        :pswitch_1    # com.konka.tvsettings.R.id.textview_cha_dtvmanualtuning_modulation
        :pswitch_1    # com.konka.tvsettings.R.id.textview_cha_dtvmanualtuning_modulation_val
        :pswitch_0    # com.konka.tvsettings.R.id.linearlayout_cha_dtvmanualtuning_symbol
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x7f07008a
        :pswitch_6    # com.konka.tvsettings.R.id.linearlayout_cha_dtvmanualtuning_channelnum
        :pswitch_5    # com.konka.tvsettings.R.id.textview_cha_dtvmanualtuning_channelnum_val
        :pswitch_8    # com.konka.tvsettings.R.id.linearlayout_cha_dtvmanualtuning_frequency
        :pswitch_5    # com.konka.tvsettings.R.id.textview_cha_dtvmanualtuning_frequency
        :pswitch_5    # com.konka.tvsettings.R.id.textview_cha_dtvmanualtuning_frequency_val
        :pswitch_7    # com.konka.tvsettings.R.id.linearlayout_cha_dtvmanualtuning_modulation
        :pswitch_5    # com.konka.tvsettings.R.id.textview_cha_dtvmanualtuning_modulation
        :pswitch_5    # com.konka.tvsettings.R.id.textview_cha_dtvmanualtuning_modulation_val
        :pswitch_0    # com.konka.tvsettings.R.id.linearlayout_cha_dtvmanualtuning_symbol
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x7f070095
        :pswitch_9    # com.konka.tvsettings.R.id.linearlayout_cha_dtvmanualtuning_starttuning
    .end packed-switch
.end method

.method protected onPause()V
    .locals 4

    const-string v1, "TvApp"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DTVManualTuning onPause:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->bManualScanByUser:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->bManualScanByUser:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->close3D()V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->DtvStopScan()Z

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;->E_FIRST_SERVICE_DTV:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;->E_DEFAULT:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/ChannelDesk;->changeToFirstService(Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;)Z

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableAlTimeShift()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "DTVmanualTuning"

    const-string v2, "DTVManualTuning timeshift \n"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/SettingDesk;->getAlTimeShift()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->pvr:Lcom/mstar/android/tvapi/common/PvrManager;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PvrManager;->isTimeShiftRecording()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->isSignalStable()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "DTVmanualTuning"

    const-string v2, "=============start time shift \n"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->pvr:Lcom/mstar/android/tvapi/common/PvrManager;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PvrManager;->startTimeShiftRecord()Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/CommonDesk;->releaseHandler(I)V

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onPause()V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 6

    const/4 v5, 0x1

    const-string v3, "DTVManualTuning"

    const-string v4, "\n OnResume \n"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->myHandler:Lcom/konka/tvsettings/channel/DTVManualTuning$MyHandler;

    invoke-interface {v3, v4, v5}, Lcom/konka/kkinterface/tv/CommonDesk;->setHandler(Landroid/os/Handler;I)Z

    iget v3, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->searchtype:I

    if-ne v3, v5, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->ts:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/ChannelDesk;->getSystemCountry()Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    move-result-object v0

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRALIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq v3, v0, :cond_1

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq v3, v0, :cond_1

    const/16 v3, 0x22

    iput v3, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->channelno:I

    :goto_0
    iget v3, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->channelno:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->viewholder_dtvmanualtuning:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_dtvmanualtuning_channelnum_val:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iput-boolean v5, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->bManualScanByUser:Z

    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.konka.tv.hotkey.service.UNFREEZE"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/DTVManualTuning;->sendBroadcast(Landroid/content/Intent;)V

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/DTVManualTuning;->startdtvmanutuning()V

    :cond_0
    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onResume()V

    return-void

    :cond_1
    const/16 v3, 0x25

    iput v3, p0, Lcom/konka/tvsettings/channel/DTVManualTuning;->channelno:I

    goto :goto_0
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onUserInteraction()V

    return-void
.end method
