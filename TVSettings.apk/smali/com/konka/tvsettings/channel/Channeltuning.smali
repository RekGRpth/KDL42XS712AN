.class public Lcom/konka/tvsettings/channel/Channeltuning;
.super Lcom/konka/tvsettings/teletext/MstarBaseActivity;
.source "Channeltuning.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$konka$kkinterface$tv$ChannelDesk$TV_TS_STATUS:[I

.field private static ATV_EVENTINTERVAL:I

.field private static ATV_MAX_FREQ:I

.field private static ATV_MIN_FREQ:I

.field private static dtvServiceCount:I


# instance fields
.field private bExitActivity:Z

.field private channelno:I

.field private isDVBCAntennaType:Z

.field private isDtvAutoUpdateScan:Z

.field private isFirstChannelOver:Z

.field private isFromInstallation:I

.field private itemAtvName:Landroid/widget/TextView;

.field private itemAtvNum:Landroid/widget/TextView;

.field private itemAtvPro:Landroid/widget/TextView;

.field private itemDataName:Landroid/widget/TextView;

.field private itemDataNum:Landroid/widget/TextView;

.field private itemDataPro:Landroid/widget/TextView;

.field private itemDtvName:Landroid/widget/TextView;

.field private itemDtvNum:Landroid/widget/TextView;

.field private itemDtvPro:Landroid/widget/TextView;

.field private itemRadioName:Landroid/widget/TextView;

.field private itemRadioNum:Landroid/widget/TextView;

.field private itemRadioPro:Landroid/widget/TextView;

.field private myHandler:Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;

.field private s3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

.field private scanStartTime:Landroid/text/format/Time;

.field serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

.field private tvCommonManager:Lcom/mstar/android/tv/TvCommonManager;

.field private viewholder_channeltune:Lcom/konka/tvsettings/channel/ViewHolder;


# direct methods
.method static synthetic $SWITCH_TABLE$com$konka$kkinterface$tv$ChannelDesk$TV_TS_STATUS()[I
    .locals 3

    sget-object v0, Lcom/konka/tvsettings/channel/Channeltuning;->$SWITCH_TABLE$com$konka$kkinterface$tv$ChannelDesk$TV_TS_STATUS:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->values()[Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_ATV_AUTO_TUNING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_8

    :goto_1
    :try_start_1
    sget-object v1, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_ATV_MANU_TUNING_LEFT:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_7

    :goto_2
    :try_start_2
    sget-object v1, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_ATV_MANU_TUNING_RIGHT:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_6

    :goto_3
    :try_start_3
    sget-object v1, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_ATV_SCAN_PAUSING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_5

    :goto_4
    :try_start_4
    sget-object v1, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_DTV_AUTO_TUNING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :goto_5
    :try_start_5
    sget-object v1, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_DTV_FULL_TUNING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_3

    :goto_6
    :try_start_6
    sget-object v1, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_DTV_MANU_TUNING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_2

    :goto_7
    :try_start_7
    sget-object v1, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_DTV_SCAN_PAUSING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_1

    :goto_8
    :try_start_8
    sget-object v1, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_NONE:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_0

    :goto_9
    sput-object v0, Lcom/konka/tvsettings/channel/Channeltuning;->$SWITCH_TABLE$com$konka$kkinterface$tv$ChannelDesk$TV_TS_STATUS:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_9

    :catch_1
    move-exception v1

    goto :goto_8

    :catch_2
    move-exception v1

    goto :goto_7

    :catch_3
    move-exception v1

    goto :goto_6

    :catch_4
    move-exception v1

    goto :goto_5

    :catch_5
    move-exception v1

    goto :goto_4

    :catch_6
    move-exception v1

    goto :goto_3

    :catch_7
    move-exception v1

    goto :goto_2

    :catch_8
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    const v0, 0xa8f2

    sput v0, Lcom/konka/tvsettings/channel/Channeltuning;->ATV_MIN_FREQ:I

    const v0, 0xd62c2

    sput v0, Lcom/konka/tvsettings/channel/Channeltuning;->ATV_MAX_FREQ:I

    const v0, 0x7a120

    sput v0, Lcom/konka/tvsettings/channel/Channeltuning;->ATV_EVENTINTERVAL:I

    const/4 v0, 0x0

    sput v0, Lcom/konka/tvsettings/channel/Channeltuning;->dtvServiceCount:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;-><init>()V

    iput-boolean v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->isDtvAutoUpdateScan:Z

    iput-boolean v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->bExitActivity:Z

    iput-object v2, p0, Lcom/konka/tvsettings/channel/Channeltuning;->tvCommonManager:Lcom/mstar/android/tv/TvCommonManager;

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/channel/Channeltuning;->scanStartTime:Landroid/text/format/Time;

    iput-object v2, p0, Lcom/konka/tvsettings/channel/Channeltuning;->s3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    iput v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->channelno:I

    iput-boolean v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->isFirstChannelOver:Z

    iput v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->isFromInstallation:I

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/channel/Channeltuning;)Lcom/konka/tvsettings/channel/ViewHolder;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/Channeltuning;->viewholder_channeltune:Lcom/konka/tvsettings/channel/ViewHolder;

    return-object v0
.end method

.method static synthetic access$1()I
    .locals 1

    sget v0, Lcom/konka/tvsettings/channel/Channeltuning;->ATV_MAX_FREQ:I

    return v0
.end method

.method static synthetic access$10()I
    .locals 1

    sget v0, Lcom/konka/tvsettings/channel/Channeltuning;->ATV_EVENTINTERVAL:I

    return v0
.end method

.method static synthetic access$11()I
    .locals 1

    sget v0, Lcom/konka/tvsettings/channel/Channeltuning;->ATV_MIN_FREQ:I

    return v0
.end method

.method static synthetic access$12(Lcom/konka/tvsettings/channel/Channeltuning;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/channel/Channeltuning;->isDtvAutoUpdateScan:Z

    return v0
.end method

.method static synthetic access$13(Lcom/konka/tvsettings/channel/Channeltuning;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/channel/Channeltuning;->bExitActivity:Z

    return v0
.end method

.method static synthetic access$14(Lcom/konka/tvsettings/channel/Channeltuning;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->bExitActivity:Z

    return-void
.end method

.method static synthetic access$2()I
    .locals 1

    sget v0, Lcom/konka/tvsettings/channel/Channeltuning;->dtvServiceCount:I

    return v0
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/channel/Channeltuning;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/Channeltuning;->channetuningActivityExit()V

    return-void
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/channel/Channeltuning;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/channel/Channeltuning;->isDVBCAntennaType:Z

    return v0
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/channel/Channeltuning;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/channel/Channeltuning;->isFirstChannelOver:Z

    return v0
.end method

.method static synthetic access$6(Lcom/konka/tvsettings/channel/Channeltuning;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->channelno:I

    return-void
.end method

.method static synthetic access$7(Lcom/konka/tvsettings/channel/Channeltuning;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->isFirstChannelOver:Z

    return-void
.end method

.method static synthetic access$8(Lcom/konka/tvsettings/channel/Channeltuning;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/channel/Channeltuning;->channelno:I

    return v0
.end method

.method static synthetic access$9(I)V
    .locals 0

    sput p0, Lcom/konka/tvsettings/channel/Channeltuning;->dtvServiceCount:I

    return-void
.end method

.method private channetuningActivityExit()V
    .locals 4

    const/4 v3, 0x0

    const-string v1, "TvService"

    const-string v2, "channetuningActivityExit"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/Channeltuning;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/Channeltuning;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/Channeltuning;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "installation"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->isFromInstallation:I

    :cond_0
    iget v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->isFromInstallation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/Channeltuning;->finish()V

    :goto_0
    return-void

    :cond_1
    iput v3, p0, Lcom/konka/tvsettings/channel/Channeltuning;->isFromInstallation:I

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v1, Lcom/konka/tvsettings/channel/ProgramSettingMain;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "currentPage"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/channel/Channeltuning;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/Channeltuning;->finish()V

    goto :goto_0
.end method

.method private channetuningActivityLeave()V
    .locals 3

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v0

    invoke-static {}, Lcom/konka/tvsettings/channel/Channeltuning;->$SWITCH_TABLE$com$konka$kkinterface$tv$ChannelDesk$TV_TS_STATUS()[I

    move-result-object v1

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->GetTsStatus()Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->atvSetAutoTuningPause()Z

    goto :goto_0

    :pswitch_2
    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvPauseScan()Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private pauseChannelTuning()V
    .locals 5

    iget-object v2, p0, Lcom/konka/tvsettings/channel/Channeltuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v1

    invoke-static {}, Lcom/konka/tvsettings/channel/Channeltuning;->$SWITCH_TABLE$com$konka$kkinterface$tv$ChannelDesk$TV_TS_STATUS()[I

    move-result-object v2

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->GetTsStatus()Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->ordinal()I

    move-result v3

    aget v2, v2, v3

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    invoke-interface {v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->atvSetAutoTuningEnd()Z

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;->E_FIRST_SERVICE_ATV:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;->E_DEFAULT:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/ChannelDesk;->changeToFirstService(Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;)Z

    goto :goto_0

    :sswitch_1
    invoke-interface {v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->DtvStopScan()Z

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->getUserScanType()Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    move-result-object v2

    sget-object v3, Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;->E_SCAN_ALL:Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    if-ne v2, v3, :cond_1

    sget v2, Lcom/konka/tvsettings/channel/Channeltuning;->ATV_EVENTINTERVAL:I

    sget v3, Lcom/konka/tvsettings/channel/Channeltuning;->ATV_MIN_FREQ:I

    sget v4, Lcom/konka/tvsettings/channel/Channeltuning;->ATV_MAX_FREQ:I

    invoke-interface {v1, v2, v3, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->atvSetAutoTuningStart(III)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v2, "TuningService"

    const-string v3, "atvSetAutoTuningStart Error!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;->E_FIRST_SERVICE_DTV:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;->E_DEFAULT:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/ChannelDesk;->changeToFirstService(Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;)Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0x9 -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 12
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f03000b    # com.konka.tvsettings.R.layout.channeltuning

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/Channeltuning;->setContentView(I)V

    new-instance v1, Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;-><init>(Lcom/konka/tvsettings/channel/Channeltuning;)V

    iput-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->myHandler:Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/Channeltuning;->getApplication()Landroid/app/Application;

    move-result-object v6

    check-cast v6, Lcom/konka/tvsettings/TVRootApp;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/tvsettings/channel/Channeltuning;->myHandler:Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/CommonDesk;->setHandler(Landroid/os/Handler;I)Z

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvGetAntennaType()Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;->E_ROUTE_DVBC:Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;

    if-ne v1, v2, :cond_3

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->isDVBCAntennaType:Z

    :goto_0
    new-instance v1, Lcom/konka/tvsettings/channel/ViewHolder;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/channel/ViewHolder;-><init>(Lcom/konka/tvsettings/channel/Channeltuning;)V

    iput-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->viewholder_channeltune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->viewholder_channeltune:Lcom/konka/tvsettings/channel/ViewHolder;

    invoke-virtual {v1}, Lcom/konka/tvsettings/channel/ViewHolder;->findViewForChannelTuning()V

    const v1, 0x7f070046    # com.konka.tvsettings.R.id.textview_cha_tv

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/Channeltuning;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->itemAtvName:Landroid/widget/TextView;

    const v1, 0x7f070047    # com.konka.tvsettings.R.id.textview_cha_tvprogram_val

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/Channeltuning;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->itemAtvNum:Landroid/widget/TextView;

    const v1, 0x7f070048    # com.konka.tvsettings.R.id.textview_cha_tvprogram_pro

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/Channeltuning;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->itemAtvPro:Landroid/widget/TextView;

    const v1, 0x7f07004a    # com.konka.tvsettings.R.id.textview_cha_dtv

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/Channeltuning;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->itemDtvName:Landroid/widget/TextView;

    const v1, 0x7f07004b    # com.konka.tvsettings.R.id.textview_cha_dtvprogram_val

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/Channeltuning;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->itemDtvNum:Landroid/widget/TextView;

    const v1, 0x7f07004c    # com.konka.tvsettings.R.id.textview_cha_dtvprogram_pro

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/Channeltuning;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->itemDtvPro:Landroid/widget/TextView;

    const v1, 0x7f07004e    # com.konka.tvsettings.R.id.textview_cha_radio

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/Channeltuning;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->itemRadioName:Landroid/widget/TextView;

    const v1, 0x7f07004f    # com.konka.tvsettings.R.id.textview_cha_radioprogram_val

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/Channeltuning;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->itemRadioNum:Landroid/widget/TextView;

    const v1, 0x7f070050    # com.konka.tvsettings.R.id.textview_cha_radioprogram_pro

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/Channeltuning;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->itemRadioPro:Landroid/widget/TextView;

    const v1, 0x7f070052    # com.konka.tvsettings.R.id.textview_cha_data

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/Channeltuning;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->itemDataName:Landroid/widget/TextView;

    const v1, 0x7f070053    # com.konka.tvsettings.R.id.textview_cha_dataprogram_val

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/Channeltuning;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->itemDataNum:Landroid/widget/TextView;

    const v1, 0x7f070054    # com.konka.tvsettings.R.id.textview_cha_dataprogram_pro

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/Channeltuning;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->itemDataPro:Landroid/widget/TextView;

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getInstance()Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->tvCommonManager:Lcom/mstar/android/tv/TvCommonManager;

    const/4 v1, 0x0

    sput v1, Lcom/konka/tvsettings/channel/Channeltuning;->dtvServiceCount:I

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->scanStartTime:Landroid/text/format/Time;

    invoke-virtual {v1}, Landroid/text/format/Time;->setToNow()V

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->getUserScanType()Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;->E_SCAN_DTV:Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->itemAtvName:Landroid/widget/TextView;

    const v2, -0xa5a5a6

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->itemAtvNum:Landroid/widget/TextView;

    const v2, -0xa5a5a6

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->itemAtvPro:Landroid/widget/TextView;

    const v2, -0xa5a5a6

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_0
    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->getUserScanType()Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;->E_SCAN_ATV:Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->itemDtvName:Landroid/widget/TextView;

    const v2, -0xa5a5a6

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->itemDtvNum:Landroid/widget/TextView;

    const v2, -0xa5a5a6

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->itemDtvPro:Landroid/widget/TextView;

    const v2, -0xa5a5a6

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->itemRadioName:Landroid/widget/TextView;

    const v2, -0xa5a5a6

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->itemRadioNum:Landroid/widget/TextView;

    const v2, -0xa5a5a6

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->itemRadioPro:Landroid/widget/TextView;

    const v2, -0xa5a5a6

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->itemDataName:Landroid/widget/TextView;

    const v2, -0xa5a5a6

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->itemDataNum:Landroid/widget/TextView;

    const v2, -0xa5a5a6

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->itemDataPro:Landroid/widget/TextView;

    const v2, -0xa5a5a6

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_1
    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/Channeltuning;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/Channeltuning;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/Channeltuning;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "DtvAutoUpdateScan"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->isDtvAutoUpdateScan:Z

    :cond_2
    iget-boolean v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->isDtvAutoUpdateScan:Z

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->viewholder_channeltune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_tuningprogress_type:Landroid/widget/TextView;

    const v2, 0x7f0a00c9    # com.konka.tvsettings.R.string.str_popup_input_menu_dtv

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    const-string v1, "tvApp"

    const-string v2, "switchMSrvDtvRouteCmd 1"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_DTV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->getProgramCount(Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;)I

    move-result v9

    new-instance v10, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;

    invoke-direct {v10}, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;-><init>()V

    sget-object v1, Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;->E_ROUTE_DVBC:Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;->ordinal()I

    move-result v1

    int-to-short v1, v1

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->switchMSrvDtvRouteCmd(S)Z

    invoke-interface {v0, v10}, Lcom/konka/kkinterface/tv/ChannelDesk;->dvbcgetScanParam(Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;)Z

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getS3DManagerInstance()Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->s3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->s3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/S3DDesk;->setDisplayFormat(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;)Z

    if-lez v9, :cond_5

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrentMuxInfo()Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbMuxInfo;

    move-result-object v8

    if-eqz v8, :cond_4

    iget v1, v8, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbMuxInfo;->frequency:I

    iput v1, v10, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;->u32NITFrequency:I

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;->values()[Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    move-result-object v1

    iget-short v2, v8, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbMuxInfo;->modulationMode:S

    aget-object v1, v1, v2

    iput-object v1, v10, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;->QAM_Type:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    iget v1, v8, Lcom/mstar/android/tvapi/dtv/dvb/vo/DvbMuxInfo;->symbRate:I

    int-to-short v1, v1

    iput-short v1, v10, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;->u16SymbolRate:S

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\ndmi.u32NITFrequencye: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, v10, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;->u32NITFrequency:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\ndmi.QAM_Type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v10, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;->QAM_Type:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\ndmi.u16SymbolRate: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-short v3, v10, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;->u16SymbolRate:S

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v1, Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;->E_SCAN_DTV:Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->setUserScanType(Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;)V

    iget-short v1, v10, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;->u16SymbolRate:S

    iget-object v2, v10, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;->QAM_Type:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    iget v3, v10, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;->u32NITFrequency:I

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Lcom/konka/kkinterface/tv/ChannelDesk;->dvbcsetScanParam(SLcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;IIS)Z

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->startQuickScan()Z

    :goto_1
    return-void

    :cond_3
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->isDVBCAntennaType:Z

    goto/16 :goto_0

    :cond_4
    const-string v1, "Tvapp"

    const-string v2, "getCurrentMuxInfo error"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_5
    const-string v1, "Tvapp"

    const-string v2, "m_nServiceNum = 0"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_6
    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->getUserScanType()Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;->E_SCAN_ALL:Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    if-eq v1, v2, :cond_7

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->getUserScanType()Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;->E_SCAN_DTV:Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    if-ne v1, v2, :cond_b

    :cond_7
    iget-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->viewholder_channeltune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_tuningprogress_type:Landroid/widget/TextView;

    const v2, 0x7f0a00c9    # com.konka.tvsettings.R.string.str_popup_input_menu_dtv

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-boolean v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->isDVBCAntennaType:Z

    if-eqz v1, :cond_9

    const-string v1, "tvApp"

    const-string v2, "switchMSrvDtvRouteCmd 1"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v10, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;

    invoke-direct {v10}, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;-><init>()V

    sget-object v1, Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;->E_ROUTE_DVBC:Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;->ordinal()I

    move-result v1

    int-to-short v1, v1

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->switchMSrvDtvRouteCmd(S)Z

    invoke-interface {v0, v10}, Lcom/konka/kkinterface/tv/ChannelDesk;->dvbcgetScanParam(Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;)Z

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getS3DManagerInstance()Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->s3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->s3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/S3DDesk;->setDisplayFormat(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;)Z

    const-string v1, "tvApp"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "get ArrayExtra NitScanPara==="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/Channeltuning;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "NitScanPara"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/Channeltuning;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/Channeltuning;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "NitScanPara"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v1

    if-eqz v1, :cond_8

    const-string v1, "tvApp"

    const-string v2, "dvbc nit scan!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/Channeltuning;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "NitScanPara"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v7

    const/4 v1, 0x0

    aget v1, v7, v1

    mul-int/lit16 v1, v1, 0x3e8

    iput v1, v10, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;->u32NITFrequency:I

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;->values()[Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    move-result-object v1

    const/4 v2, 0x1

    aget v2, v7, v2

    aget-object v1, v1, v2

    iput-object v1, v10, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;->QAM_Type:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    const/4 v1, 0x2

    aget v1, v7, v1

    int-to-short v1, v1

    iput-short v1, v10, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;->u16SymbolRate:S

    iget-short v1, v10, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;->u16SymbolRate:S

    iget-object v2, v10, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;->QAM_Type:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    iget v3, v10, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;->u32NITFrequency:I

    const v4, 0xdcf28

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Lcom/konka/kkinterface/tv/ChannelDesk;->dvbcsetScanParam(SLcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;IIS)Z

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvAutoScan()Z

    goto/16 :goto_1

    :cond_8
    const-string v1, "tvApp"

    const-string v2, "dvbc full scan!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "tvApp"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sp.u16SymbolRate ====="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-short v3, v10, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;->u16SymbolRate:S

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "tvApp"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sp.QAM_Type==========="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v10, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;->QAM_Type:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "tvApp"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sp.u32NITFrequency===="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, v10, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;->u32NITFrequency:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    sget-object v2, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;->E_CAB_INVALID:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Lcom/konka/kkinterface/tv/ChannelDesk;->dvbcsetScanParam(SLcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;IIS)Z

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvFullScan()Z

    goto/16 :goto_1

    :cond_9
    iget-boolean v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->isDVBCAntennaType:Z

    if-nez v1, :cond_a

    const-string v1, "tvApp"

    const-string v2, "switchMSrvDtvRouteCmd 2"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;->E_ROUTE_DVBT:Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;->ordinal()I

    move-result v1

    int-to-short v1, v1

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->switchMSrvDtvRouteCmd(S)Z

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvAutoScan()Z

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getS3DManagerInstance()Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->s3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->s3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/S3DDesk;->setDisplayFormat(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;)Z

    goto/16 :goto_1

    :cond_a
    const-string v1, "tvApp"

    const-string v2, "switchMSrvDtvRouteCmd 0"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;->E_ROUTE_DTMB:Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;->ordinal()I

    move-result v1

    int-to-short v1, v1

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->switchMSrvDtvRouteCmd(S)Z

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvAutoScan()Z

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getS3DManagerInstance()Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->s3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->s3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/S3DDesk;->setDisplayFormat(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;)Z

    goto/16 :goto_1

    :cond_b
    iget-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->viewholder_channeltune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_tuningprogress_type:Landroid/widget/TextView;

    const v2, 0x7f0a00ca    # com.konka.tvsettings.R.string.str_popup_input_menu_atv

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    const-string v11, "0%43.25"

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->viewholder_channeltune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_tuningprogress_val:Landroid/widget/TextView;

    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v1, Lcom/konka/tvsettings/channel/Channeltuning;->ATV_EVENTINTERVAL:I

    sget v2, Lcom/konka/tvsettings/channel/Channeltuning;->ATV_MIN_FREQ:I

    sget v3, Lcom/konka/tvsettings/channel/Channeltuning;->ATV_MAX_FREQ:I

    invoke-interface {v0, v1, v2, v3}, Lcom/konka/kkinterface/tv/ChannelDesk;->atvSetAutoTuningStart(III)Z

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getS3DManagerInstance()Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->s3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->s3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/S3DDesk;->setDisplayFormat(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;)Z

    goto/16 :goto_1
.end method

.method protected onDestroy()V
    .locals 0

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 13
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const v12, 0x7f0a0066    # com.konka.tvsettings.R.string.str_cha_exittuning_info

    const v11, 0x7f060019    # com.konka.tvsettings.R.style.Dialog

    const/16 v10, 0x8

    const/4 v4, 0x0

    const/4 v5, 0x1

    sparse-switch p1, :sswitch_data_0

    :goto_0
    invoke-super {p0, p1, p2}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v4

    :goto_1
    return v4

    :sswitch_0
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    iget-object v6, p0, Lcom/konka/tvsettings/channel/Channeltuning;->scanStartTime:Landroid/text/format/Time;

    invoke-virtual {v0, v6}, Landroid/text/format/Time;->after(Landroid/text/format/Time;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v0, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    iget-object v8, p0, Lcom/konka/tvsettings/channel/Channeltuning;->scanStartTime:Landroid/text/format/Time;

    invoke-virtual {v8, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v8

    sub-long/2addr v6, v8

    const-wide/16 v8, 0x7d0

    cmp-long v6, v6, v8

    if-gez v6, :cond_0

    const-string v6, "Wait for a moment please!"

    invoke-static {p0, v6, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_1

    :cond_0
    invoke-direct {p0}, Lcom/konka/tvsettings/channel/Channeltuning;->channetuningActivityLeave()V

    iget-object v5, p0, Lcom/konka/tvsettings/channel/Channeltuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getS3DManagerInstance()Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v5

    iput-object v5, p0, Lcom/konka/tvsettings/channel/Channeltuning;->s3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    iget-object v5, p0, Lcom/konka/tvsettings/channel/Channeltuning;->s3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    sget-object v6, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-interface {v5, v6}, Lcom/konka/kkinterface/tv/S3DDesk;->setDisplayFormat(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;)Z

    iget-object v5, p0, Lcom/konka/tvsettings/channel/Channeltuning;->viewholder_channeltune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v5, v5, Lcom/konka/tvsettings/channel/ViewHolder;->linear_cha_mainlinear:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    new-instance v1, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;

    iget-object v5, p0, Lcom/konka/tvsettings/channel/Channeltuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-direct {v1, p0, v11, v5, v4}, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;-><init>(Landroid/content/Context;ILcom/konka/kkinterface/tv/TvDeskProvider;Z)V

    new-instance v4, Lcom/konka/tvsettings/channel/Channeltuning$1;

    invoke-direct {v4, p0}, Lcom/konka/tvsettings/channel/Channeltuning$1;-><init>(Lcom/konka/tvsettings/channel/Channeltuning;)V

    invoke-virtual {v1, v4}, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    invoke-virtual {v1}, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->show()V

    invoke-virtual {v1, v12}, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->setDialogContentText(I)V

    goto :goto_0

    :sswitch_1
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    iget-object v6, p0, Lcom/konka/tvsettings/channel/Channeltuning;->scanStartTime:Landroid/text/format/Time;

    invoke-virtual {v0, v6}, Landroid/text/format/Time;->after(Landroid/text/format/Time;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v0, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    iget-object v8, p0, Lcom/konka/tvsettings/channel/Channeltuning;->scanStartTime:Landroid/text/format/Time;

    invoke-virtual {v8, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v8

    sub-long/2addr v6, v8

    const-wide/16 v8, 0x7d0

    cmp-long v6, v6, v8

    if-gez v6, :cond_1

    const-string v6, "Wait for a moment please!"

    invoke-static {p0, v6, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    :cond_1
    invoke-direct {p0}, Lcom/konka/tvsettings/channel/Channeltuning;->channetuningActivityLeave()V

    iget-object v4, p0, Lcom/konka/tvsettings/channel/Channeltuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getS3DManagerInstance()Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/tvsettings/channel/Channeltuning;->s3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    iget-object v4, p0, Lcom/konka/tvsettings/channel/Channeltuning;->s3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    sget-object v6, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-interface {v4, v6}, Lcom/konka/kkinterface/tv/S3DDesk;->setDisplayFormat(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;)Z

    iget-object v4, p0, Lcom/konka/tvsettings/channel/Channeltuning;->viewholder_channeltune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v4, v4, Lcom/konka/tvsettings/channel/ViewHolder;->linear_cha_mainlinear:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    new-instance v1, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;

    iget-object v4, p0, Lcom/konka/tvsettings/channel/Channeltuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-direct {v1, p0, v11, v4, v5}, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;-><init>(Landroid/content/Context;ILcom/konka/kkinterface/tv/TvDeskProvider;Z)V

    new-instance v4, Lcom/konka/tvsettings/channel/Channeltuning$2;

    invoke-direct {v4, p0}, Lcom/konka/tvsettings/channel/Channeltuning$2;-><init>(Lcom/konka/tvsettings/channel/Channeltuning;)V

    invoke-virtual {v1, v4}, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    invoke-virtual {v1}, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->show()V

    const/16 v4, 0x52

    if-ne p1, v4, :cond_3

    iget-object v4, p0, Lcom/konka/tvsettings/channel/Channeltuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/ChannelDesk;->GetTsStatus()Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    move-result-object v4

    sget-object v5, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_DTV_SCAN_PAUSING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    if-ne v4, v5, :cond_2

    const v4, 0x7f0a0068    # com.konka.tvsettings.R.string.str_cha_skip_dtvtuning_info

    invoke-virtual {v1, v4}, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->setDialogContentText(I)V

    goto/16 :goto_0

    :cond_2
    const v4, 0x7f0a0067    # com.konka.tvsettings.R.string.str_cha_skip_atvtuning_info

    invoke-virtual {v1, v4}, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->setDialogContentText(I)V

    goto/16 :goto_0

    :cond_3
    invoke-virtual {v1, v12}, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->setDialogContentText(I)V

    goto/16 :goto_0

    :sswitch_2
    move v4, v5

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x52 -> :sswitch_1
        0xb2 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 0

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/konka/tvsettings/channel/Channeltuning;->bExitActivity:Z

    iget-object v0, p0, Lcom/konka/tvsettings/channel/Channeltuning;->viewholder_channeltune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->linear_cha_mainlinear:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onResume()V

    return-void
.end method

.method protected onStop()V
    .locals 2

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onStop()V

    iget-object v0, p0, Lcom/konka/tvsettings/channel/Channeltuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/CommonDesk;->releaseHandler(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/channel/Channeltuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/konka/tvsettings/channel/Channeltuning;->channetuningActivityLeave()V

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/Channeltuning;->pauseChannelTuning()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/Channeltuning;->finish()V

    goto :goto_0
.end method
