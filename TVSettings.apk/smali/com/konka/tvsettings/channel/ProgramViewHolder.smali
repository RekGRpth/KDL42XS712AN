.class public Lcom/konka/tvsettings/channel/ProgramViewHolder;
.super Ljava/lang/Object;
.source "ProgramViewHolder.java"


# instance fields
.field protected button_atvautosearch_confirmbutton:Landroid/widget/Button;

.field protected button_dtvautosearch_confirmbutton:Landroid/widget/Button;

.field protected button_dtvmanualsearch_confirmbutton:Landroid/widget/Button;

.field private commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

.field private dtvsignalinfo:Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;

.field protected layout_search_hint:Landroid/widget/LinearLayout;

.field protected layout_search_result_hint:Landroid/widget/LinearLayout;

.field protected layout_searching_hint:Landroid/widget/LinearLayout;

.field protected linear_dtv_signalquality_val:Landroid/widget/LinearLayout;

.field protected linear_dtv_signalstrength_val:Landroid/widget/LinearLayout;

.field protected linearlayout_atvautosearch_seekbar:Landroid/widget/LinearLayout;

.field protected linearlayout_dtvautosearch_seekbar:Landroid/widget/LinearLayout;

.field protected linearlayout_item_antennatype:Landroid/widget/LinearLayout;

.field protected linearlayout_item_antennatype_parent:Landroid/widget/LinearLayout;

.field protected linearlayout_item_atvmanualsearch:Landroid/widget/LinearLayout;

.field protected linearlayout_item_autosearch:Landroid/widget/LinearLayout;

.field protected linearlayout_item_ciInformation:Landroid/widget/LinearLayout;

.field protected linearlayout_item_dtvmanualsearch:Landroid/widget/LinearLayout;

.field protected linearlayout_item_epg:Landroid/widget/LinearLayout;

.field protected linearlayout_item_epg_item_container:Landroid/widget/LinearLayout;

.field protected linearlayout_item_lcnarrange:Landroid/widget/LinearLayout;

.field protected linearlayout_item_lcnarrange_parent:Landroid/widget/LinearLayout;

.field protected linearlayout_item_programedit:Landroid/widget/LinearLayout;

.field protected linearlayout_item_signalinfo:Landroid/widget/LinearLayout;

.field private programsettingmain:Lcom/konka/tvsettings/channel/ProgramSettingMain;

.field protected progressbar_atvautosearch_curprogress:Landroid/widget/ProgressBar;

.field protected progressbar_dtvautosearch_curprogress:Landroid/widget/ProgressBar;

.field protected textview_atvautosearch_curchannel:Landroid/widget/TextView;

.field protected textview_atvautosearch_curfrequency:Landroid/widget/TextView;

.field protected textview_atvautosearch_curprogress:Landroid/widget/TextView;

.field protected textview_atvautosearch_curtvprogramcounts:Landroid/widget/TextView;

.field protected textview_ddtvsignalinfo_modulation:Landroid/widget/TextView;

.field protected textview_ddtvsignalinfo_quality_percent:Landroid/widget/TextView;

.field protected textview_ddtvsignalinfo_strength_percent:Landroid/widget/TextView;

.field protected textview_dtvautosearch_broadcast_programcounts:Landroid/widget/TextView;

.field protected textview_dtvautosearch_curchannel:Landroid/widget/TextView;

.field protected textview_dtvautosearch_curfrequency:Landroid/widget/TextView;

.field protected textview_dtvautosearch_curprogress:Landroid/widget/TextView;

.field protected textview_dtvautosearch_dtv_programcounts:Landroid/widget/TextView;

.field protected textview_dtvmanualsearch_broadcast_programcounts:Landroid/widget/TextView;

.field protected textview_dtvmanualsearch_curchannel:Landroid/widget/TextView;

.field protected textview_dtvmanualsearch_dtv_programcounts:Landroid/widget/TextView;

.field protected textview_dtvmanualsearch_signalquality:Landroid/widget/TextView;

.field protected textview_dtvmanualsearch_signalstrength:Landroid/widget/TextView;

.field protected textview_dtvsignalinfo_curchannel:Landroid/widget/TextView;

.field protected textview_dtvsignalinfo_network:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;)V
    .locals 1
    .param p1    # Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    iput-object p1, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->dtvsignalinfo:Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;

    return-void
.end method

.method public constructor <init>(Lcom/konka/tvsettings/channel/ProgramSettingMain;)V
    .locals 1
    .param p1    # Lcom/konka/tvsettings/channel/ProgramSettingMain;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    iput-object p1, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->programsettingmain:Lcom/konka/tvsettings/channel/ProgramSettingMain;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    return-void
.end method


# virtual methods
.method findViewforDTVSignalInfo()V
    .locals 8

    const/4 v7, 0x1

    iget-object v5, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->dtvsignalinfo:Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;

    const v6, 0x7f070080    # com.konka.tvsettings.R.id.signal_info_current_channel

    invoke-virtual {v5, v6}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->dtvsignalinfo:Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;

    const v6, 0x7f070081    # com.konka.tvsettings.R.id.signal_info_network

    invoke-virtual {v5, v6}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->dtvsignalinfo:Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;

    const v6, 0x7f070082    # com.konka.tvsettings.R.id.signal_info_modulation

    invoke-virtual {v5, v6}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->dtvsignalinfo:Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;

    const v6, 0x7f070083    # com.konka.tvsettings.R.id.signal_info_strength

    invoke-virtual {v5, v6}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->dtvsignalinfo:Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;

    const v6, 0x7f070086    # com.konka.tvsettings.R.id.signal_info_quality

    invoke-virtual {v5, v6}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->textview_dtvsignalinfo_curchannel:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->textview_dtvsignalinfo_network:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->textview_ddtvsignalinfo_modulation:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->dtvsignalinfo:Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;

    const v6, 0x7f070084    # com.konka.tvsettings.R.id.signal_info_strength_val

    invoke-virtual {v5, v6}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linear_dtv_signalstrength_val:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->dtvsignalinfo:Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;

    const v6, 0x7f070087    # com.konka.tvsettings.R.id.signal_info_quality_val

    invoke-virtual {v5, v6}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linear_dtv_signalquality_val:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->dtvsignalinfo:Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;

    const v6, 0x7f070085    # com.konka.tvsettings.R.id.signal_info_strength_percent

    invoke-virtual {v5, v6}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->textview_ddtvsignalinfo_strength_percent:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->dtvsignalinfo:Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;

    const v6, 0x7f070088    # com.konka.tvsettings.R.id.signal_info_quality_percent

    invoke-virtual {v5, v6}, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->textview_ddtvsignalinfo_quality_percent:Landroid/widget/TextView;

    return-void
.end method

.method findViewforProgramSettingMain()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->programsettingmain:Lcom/konka/tvsettings/channel/ProgramSettingMain;

    const v1, 0x7f070170    # com.konka.tvsettings.R.id.linearlayout_program_autotuning

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_autosearch:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->programsettingmain:Lcom/konka/tvsettings/channel/ProgramSettingMain;

    const v1, 0x7f07016e    # com.konka.tvsettings.R.id.linearlayout_program_antennatype

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_antennatype:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->programsettingmain:Lcom/konka/tvsettings/channel/ProgramSettingMain;

    const v1, 0x7f07016d    # com.konka.tvsettings.R.id.linearlayout_program_antennatype_parent

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_antennatype_parent:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->programsettingmain:Lcom/konka/tvsettings/channel/ProgramSettingMain;

    const v1, 0x7f070171    # com.konka.tvsettings.R.id.program_epg_item_container

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_epg_item_container:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->programsettingmain:Lcom/konka/tvsettings/channel/ProgramSettingMain;

    const v1, 0x7f070172    # com.konka.tvsettings.R.id.linearlayout_program_epg

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_epg:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->programsettingmain:Lcom/konka/tvsettings/channel/ProgramSettingMain;

    const v1, 0x7f070175    # com.konka.tvsettings.R.id.linearlayout_program_lcnarrange_parent

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_lcnarrange_parent:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->programsettingmain:Lcom/konka/tvsettings/channel/ProgramSettingMain;

    const v1, 0x7f070176    # com.konka.tvsettings.R.id.linearlayout_program_lcn_arrange

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_lcnarrange:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->programsettingmain:Lcom/konka/tvsettings/channel/ProgramSettingMain;

    const v1, 0x7f070173    # com.konka.tvsettings.R.id.linearlayout_program_atvmanualtuning

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_atvmanualsearch:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->programsettingmain:Lcom/konka/tvsettings/channel/ProgramSettingMain;

    const v1, 0x7f070174    # com.konka.tvsettings.R.id.linearlayout_program_dtvmanualtuning

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_dtvmanualsearch:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->programsettingmain:Lcom/konka/tvsettings/channel/ProgramSettingMain;

    const v1, 0x7f070177    # com.konka.tvsettings.R.id.linearlayout_program_edit

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_programedit:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->programsettingmain:Lcom/konka/tvsettings/channel/ProgramSettingMain;

    const v1, 0x7f070178    # com.konka.tvsettings.R.id.linearlayout_program_signal_info

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_signalinfo:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->programsettingmain:Lcom/konka/tvsettings/channel/ProgramSettingMain;

    const v1, 0x7f070179    # com.konka.tvsettings.R.id.linearlayout_program_ci_information

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_ciInformation:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableCI()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_ciInformation:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_ciInformation:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method
