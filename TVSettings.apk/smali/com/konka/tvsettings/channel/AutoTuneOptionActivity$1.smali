.class Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$1;
.super Landroid/os/Handler;
.source "AutoTuneOptionActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$1;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1    # Landroid/os/Message;

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget v1, p1, Landroid/os/Message;->what:I

    iget-object v2, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$1;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    # getter for: Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->MSG_ONCREAT:I
    invoke-static {v2}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$0(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)I

    move-result v2

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$1;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    # invokes: Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->updateUiCoutryRequestFocus()V
    invoke-static {v1}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$1(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$1;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    # getter for: Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;
    invoke-static {v1}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$2(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)Lcom/konka/tvsettings/channel/ViewHolder;

    move-result-object v1

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_autotuning_tuningtype_val:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$1;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    # getter for: Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->OptionScanTypes:[Ljava/lang/String;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$3(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)[Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$1;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    # getter for: Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->tuningtype:I
    invoke-static {v3}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$4(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$1;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    # getter for: Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;
    invoke-static {v1}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$2(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)Lcom/konka/tvsettings/channel/ViewHolder;

    move-result-object v1

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_hint_totalpage_val:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$1;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    # getter for: Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->page_total:I
    invoke-static {v2}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$5(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$1;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$1;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    const v3, 0x7f070026    # com.konka.tvsettings.R.id.image_left_arrow_hint

    invoke-virtual {v1, v3}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-static {v2, v1}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$6(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;Landroid/widget/ImageView;)V

    iget-object v2, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$1;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$1;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    const v3, 0x7f070031    # com.konka.tvsettings.R.id.image_right_arrow_hint

    invoke-virtual {v1, v3}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-static {v2, v1}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$7(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;Landroid/widget/ImageView;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$1;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    # invokes: Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->updateUiCoutrySelect()V
    invoke-static {v1}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$8(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)V

    :cond_0
    const/16 v1, 0x65

    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v1, v2, :cond_1

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$1;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    const-class v2, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$1;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    invoke-virtual {v1, v0}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$1;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->finish()V

    :cond_1
    return-void
.end method
