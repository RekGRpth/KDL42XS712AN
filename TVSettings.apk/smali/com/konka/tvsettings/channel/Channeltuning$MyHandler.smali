.class Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;
.super Landroid/os/Handler;
.source "Channeltuning.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/channel/Channeltuning;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MyHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/channel/Channeltuning;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/channel/Channeltuning;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/Channeltuning;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 25
    .param p1    # Landroid/os/Message;

    move-object/from16 v0, p1

    iget v0, v0, Landroid/os/Message;->what:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/Channeltuning;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/konka/tvsettings/channel/Channeltuning;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-object/from16 v20, v0

    invoke-interface/range {v20 .. v20}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v18

    sget-object v20, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual/range {v20 .. v20}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v20

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_4

    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    new-instance v15, Ljava/lang/String;

    invoke-direct {v15}, Ljava/lang/String;-><init>()V

    new-instance v16, Ljava/lang/String;

    invoke-direct/range {v16 .. v16}, Ljava/lang/String;-><init>()V

    const-string v20, "percent"

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v9

    const-string v20, "frequency"

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    const-string v20, "scanNum"

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v14

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v21, v14, 0x1

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, " "

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    div-int/lit16 v0, v7, 0x3e8

    move/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "."

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    rem-int/lit16 v0, v7, 0x3e8

    move/from16 v21, v0

    div-int/lit8 v21, v21, 0xa

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "Mhz"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/Channeltuning;

    move-object/from16 v20, v0

    # getter for: Lcom/konka/tvsettings/channel/Channeltuning;->viewholder_channeltune:Lcom/konka/tvsettings/channel/ViewHolder;
    invoke-static/range {v20 .. v20}, Lcom/konka/tvsettings/channel/Channeltuning;->access$0(Lcom/konka/tvsettings/channel/Channeltuning;)Lcom/konka/tvsettings/channel/ViewHolder;

    move-result-object v20

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_tvprogram_val:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/Channeltuning;

    move-object/from16 v20, v0

    # getter for: Lcom/konka/tvsettings/channel/Channeltuning;->viewholder_channeltune:Lcom/konka/tvsettings/channel/ViewHolder;
    invoke-static/range {v20 .. v20}, Lcom/konka/tvsettings/channel/Channeltuning;->access$0(Lcom/konka/tvsettings/channel/Channeltuning;)Lcom/konka/tvsettings/channel/ViewHolder;

    move-result-object v20

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->progressbar_cha_program_val:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const/16 v21, 0x25

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/Channeltuning;

    move-object/from16 v20, v0

    # getter for: Lcom/konka/tvsettings/channel/Channeltuning;->viewholder_channeltune:Lcom/konka/tvsettings/channel/ViewHolder;
    invoke-static/range {v20 .. v20}, Lcom/konka/tvsettings/channel/Channeltuning;->access$0(Lcom/konka/tvsettings/channel/Channeltuning;)Lcom/konka/tvsettings/channel/ViewHolder;

    move-result-object v20

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_tuningprogress_val:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/Channeltuning;

    move-object/from16 v20, v0

    # getter for: Lcom/konka/tvsettings/channel/Channeltuning;->viewholder_channeltune:Lcom/konka/tvsettings/channel/ViewHolder;
    invoke-static/range {v20 .. v20}, Lcom/konka/tvsettings/channel/Channeltuning;->access$0(Lcom/konka/tvsettings/channel/Channeltuning;)Lcom/konka/tvsettings/channel/ViewHolder;

    move-result-object v20

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->progressbar_cha_tuneprogress:Landroid/widget/ProgressBar;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Landroid/widget/ProgressBar;->setProgress(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/Channeltuning;

    move-object/from16 v20, v0

    # getter for: Lcom/konka/tvsettings/channel/Channeltuning;->viewholder_channeltune:Lcom/konka/tvsettings/channel/ViewHolder;
    invoke-static/range {v20 .. v20}, Lcom/konka/tvsettings/channel/Channeltuning;->access$0(Lcom/konka/tvsettings/channel/Channeltuning;)Lcom/konka/tvsettings/channel/ViewHolder;

    move-result-object v20

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_program_vhf:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const-string v21, "   "

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v20, 0x64

    move/from16 v0, v20

    if-ge v9, v0, :cond_0

    # getter for: Lcom/konka/tvsettings/channel/Channeltuning;->ATV_MAX_FREQ:I
    invoke-static {}, Lcom/konka/tvsettings/channel/Channeltuning;->access$1()I

    move-result v20

    move/from16 v0, v20

    if-le v7, v0, :cond_1

    :cond_0
    invoke-interface/range {v18 .. v18}, Lcom/konka/kkinterface/tv/ChannelDesk;->atvSetAutoTuningEnd()Z

    invoke-interface/range {v18 .. v18}, Lcom/konka/kkinterface/tv/ChannelDesk;->getUserScanType()Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    move-result-object v20

    sget-object v21, Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;->E_SCAN_ALL:Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-ne v0, v1, :cond_3

    const-string v20, "Candy_test"

    const-string v21, "@@@@@@@@@@@@@start makeSourceDtv"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    # getter for: Lcom/konka/tvsettings/channel/Channeltuning;->dtvServiceCount:I
    invoke-static {}, Lcom/konka/tvsettings/channel/Channeltuning;->access$2()I

    move-result v20

    if-lez v20, :cond_2

    invoke-interface/range {v18 .. v18}, Lcom/konka/kkinterface/tv/ChannelDesk;->makeSourceDtv()V

    sget-object v20, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;->E_FIRST_SERVICE_DTV:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;

    sget-object v21, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;->E_DEFAULT:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->changeToFirstService(Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;)Z

    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/Channeltuning;

    move-object/from16 v20, v0

    # invokes: Lcom/konka/tvsettings/channel/Channeltuning;->channetuningActivityExit()V
    invoke-static/range {v20 .. v20}, Lcom/konka/tvsettings/channel/Channeltuning;->access$3(Lcom/konka/tvsettings/channel/Channeltuning;)V

    :cond_1
    :goto_1
    invoke-super/range {p0 .. p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    return-void

    :cond_2
    const-string v20, "Candy_test"

    const-string v21, "@@@@@@@@@@@@@start makeSourceAtv"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface/range {v18 .. v18}, Lcom/konka/kkinterface/tv/ChannelDesk;->makeSourceAtv()V

    sget-object v20, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;->E_FIRST_SERVICE_ATV:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;

    sget-object v21, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;->E_DEFAULT:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->changeToFirstService(Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;)Z

    goto :goto_0

    :cond_3
    const-string v20, "Candy_test"

    const-string v21, "@@@@@@@@@@@@@start makeSourceAtv"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface/range {v18 .. v18}, Lcom/konka/kkinterface/tv/ChannelDesk;->makeSourceAtv()V

    sget-object v20, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;->E_FIRST_SERVICE_ATV:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;

    sget-object v21, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;->E_DEFAULT:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->changeToFirstService(Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;)Z

    goto :goto_0

    :cond_4
    sget-object v20, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual/range {v20 .. v20}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v20

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_1

    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v4

    new-instance v17, Ljava/lang/String;

    invoke-direct/range {v17 .. v17}, Ljava/lang/String;-><init>()V

    const-string v20, "dtvSrvCount"

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    const-string v20, "radioSrvCount"

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v10

    const-string v20, "dataSrvCount"

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    const-string v20, "percent"

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v9

    const-string v20, "curFre"

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    const-string v20, "scanstatus"

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v13

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/Channeltuning;

    move-object/from16 v20, v0

    # getter for: Lcom/konka/tvsettings/channel/Channeltuning;->viewholder_channeltune:Lcom/konka/tvsettings/channel/ViewHolder;
    invoke-static/range {v20 .. v20}, Lcom/konka/tvsettings/channel/Channeltuning;->access$0(Lcom/konka/tvsettings/channel/Channeltuning;)Lcom/konka/tvsettings/channel/ViewHolder;

    move-result-object v20

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_dtvprogram_val:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/Channeltuning;

    move-object/from16 v20, v0

    # getter for: Lcom/konka/tvsettings/channel/Channeltuning;->viewholder_channeltune:Lcom/konka/tvsettings/channel/ViewHolder;
    invoke-static/range {v20 .. v20}, Lcom/konka/tvsettings/channel/Channeltuning;->access$0(Lcom/konka/tvsettings/channel/Channeltuning;)Lcom/konka/tvsettings/channel/ViewHolder;

    move-result-object v20

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_radioprogram_val:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/Channeltuning;

    move-object/from16 v20, v0

    # getter for: Lcom/konka/tvsettings/channel/Channeltuning;->viewholder_channeltune:Lcom/konka/tvsettings/channel/ViewHolder;
    invoke-static/range {v20 .. v20}, Lcom/konka/tvsettings/channel/Channeltuning;->access$0(Lcom/konka/tvsettings/channel/Channeltuning;)Lcom/konka/tvsettings/channel/ViewHolder;

    move-result-object v20

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_dataprogram_val:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/Channeltuning;

    move-object/from16 v20, v0

    # getter for: Lcom/konka/tvsettings/channel/Channeltuning;->isDVBCAntennaType:Z
    invoke-static/range {v20 .. v20}, Lcom/konka/tvsettings/channel/Channeltuning;->access$4(Lcom/konka/tvsettings/channel/Channeltuning;)Z

    move-result v20

    if-eqz v20, :cond_6

    const v20, 0xcd14

    move/from16 v0, v20

    if-ne v8, v0, :cond_5

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const/16 v21, 0x25

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "  "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    int-to-double v0, v8

    move-wide/from16 v21, v0

    const-wide v23, 0x408f400000000000L    # 1000.0

    div-double v21, v21, v23

    invoke-static/range {v21 .. v22}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "MHz"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    :goto_2
    sget-object v20, Lcom/konka/kkinterface/tv/DtvInterface$DTV_SCAN_EVENT$EN_SCAN_RET_STATUS;->STATUS_SCAN_END:Lcom/konka/kkinterface/tv/DtvInterface$DTV_SCAN_EVENT$EN_SCAN_RET_STATUS;

    invoke-virtual/range {v20 .. v20}, Lcom/konka/kkinterface/tv/DtvInterface$DTV_SCAN_EVENT$EN_SCAN_RET_STATUS;->ordinal()I

    move-result v20

    move/from16 v0, v20

    if-eq v13, v0, :cond_a

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/Channeltuning;

    move-object/from16 v20, v0

    # getter for: Lcom/konka/tvsettings/channel/Channeltuning;->isFirstChannelOver:Z
    invoke-static/range {v20 .. v20}, Lcom/konka/tvsettings/channel/Channeltuning;->access$5(Lcom/konka/tvsettings/channel/Channeltuning;)Z

    move-result v20

    if-nez v20, :cond_7

    sget-object v20, Lcom/mstar/android/tvapi/dtv/vo/RfInfo$EnumInfoType;->E_FIRST_TO_SHOW_RF:Lcom/mstar/android/tvapi/dtv/vo/RfInfo$EnumInfoType;

    const/16 v21, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvGetRFInfo(Lcom/mstar/android/tvapi/dtv/vo/RfInfo$EnumInfoType;I)Lcom/mstar/android/tvapi/dtv/vo/RfInfo;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/Channeltuning;

    move-object/from16 v20, v0

    iget-short v0, v11, Lcom/mstar/android/tvapi/dtv/vo/RfInfo;->rfPhyNum:S

    move/from16 v21, v0

    invoke-static/range {v20 .. v21}, Lcom/konka/tvsettings/channel/Channeltuning;->access$6(Lcom/konka/tvsettings/channel/Channeltuning;I)V

    iget-object v0, v11, Lcom/mstar/android/tvapi/dtv/vo/RfInfo;->rfName:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/Channeltuning;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    invoke-static/range {v20 .. v21}, Lcom/konka/tvsettings/channel/Channeltuning;->access$7(Lcom/konka/tvsettings/channel/Channeltuning;Z)V

    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/Channeltuning;

    move-object/from16 v20, v0

    iget-short v0, v11, Lcom/mstar/android/tvapi/dtv/vo/RfInfo;->rfPhyNum:S

    move/from16 v21, v0

    invoke-static/range {v20 .. v21}, Lcom/konka/tvsettings/channel/Channeltuning;->access$6(Lcom/konka/tvsettings/channel/Channeltuning;I)V

    iget-object v0, v11, Lcom/mstar/android/tvapi/dtv/vo/RfInfo;->rfName:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/Channeltuning;

    move-object/from16 v20, v0

    # getter for: Lcom/konka/tvsettings/channel/Channeltuning;->isDVBCAntennaType:Z
    invoke-static/range {v20 .. v20}, Lcom/konka/tvsettings/channel/Channeltuning;->access$4(Lcom/konka/tvsettings/channel/Channeltuning;)Z

    move-result v20

    if-eqz v20, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/Channeltuning;

    move-object/from16 v20, v0

    # getter for: Lcom/konka/tvsettings/channel/Channeltuning;->viewholder_channeltune:Lcom/konka/tvsettings/channel/ViewHolder;
    invoke-static/range {v20 .. v20}, Lcom/konka/tvsettings/channel/Channeltuning;->access$0(Lcom/konka/tvsettings/channel/Channeltuning;)Lcom/konka/tvsettings/channel/ViewHolder;

    move-result-object v20

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->progressbar_cha_program_val:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const-string v21, " "

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/Channeltuning;

    move-object/from16 v20, v0

    # getter for: Lcom/konka/tvsettings/channel/Channeltuning;->viewholder_channeltune:Lcom/konka/tvsettings/channel/ViewHolder;
    invoke-static/range {v20 .. v20}, Lcom/konka/tvsettings/channel/Channeltuning;->access$0(Lcom/konka/tvsettings/channel/Channeltuning;)Lcom/konka/tvsettings/channel/ViewHolder;

    move-result-object v20

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->progressbar_cha_program_ch:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const-string v21, " "

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_4
    iget-boolean v0, v11, Lcom/mstar/android/tvapi/dtv/vo/RfInfo;->isVHF:Z

    move/from16 v20, v0

    if-eqz v20, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/Channeltuning;

    move-object/from16 v20, v0

    # getter for: Lcom/konka/tvsettings/channel/Channeltuning;->viewholder_channeltune:Lcom/konka/tvsettings/channel/ViewHolder;
    invoke-static/range {v20 .. v20}, Lcom/konka/tvsettings/channel/Channeltuning;->access$0(Lcom/konka/tvsettings/channel/Channeltuning;)Lcom/konka/tvsettings/channel/ViewHolder;

    move-result-object v20

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_program_vhf:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const-string v21, "VHF"

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/Channeltuning;

    move-object/from16 v20, v0

    # getter for: Lcom/konka/tvsettings/channel/Channeltuning;->viewholder_channeltune:Lcom/konka/tvsettings/channel/ViewHolder;
    invoke-static/range {v20 .. v20}, Lcom/konka/tvsettings/channel/Channeltuning;->access$0(Lcom/konka/tvsettings/channel/Channeltuning;)Lcom/konka/tvsettings/channel/ViewHolder;

    move-result-object v20

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_tuningprogress_val:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/Channeltuning;

    move-object/from16 v20, v0

    # getter for: Lcom/konka/tvsettings/channel/Channeltuning;->viewholder_channeltune:Lcom/konka/tvsettings/channel/ViewHolder;
    invoke-static/range {v20 .. v20}, Lcom/konka/tvsettings/channel/Channeltuning;->access$0(Lcom/konka/tvsettings/channel/Channeltuning;)Lcom/konka/tvsettings/channel/ViewHolder;

    move-result-object v20

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->progressbar_cha_tuneprogress:Landroid/widget/ProgressBar;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto/16 :goto_1

    :cond_5
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const/16 v21, 0x25

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "  "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    div-int/lit16 v0, v8, 0x3e8

    move/from16 v21, v0

    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "MHz"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    goto/16 :goto_2

    :cond_6
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const/16 v21, 0x25

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    goto/16 :goto_2

    :cond_7
    sget-object v20, Lcom/mstar/android/tvapi/dtv/vo/RfInfo$EnumInfoType;->E_NEXT_RF:Lcom/mstar/android/tvapi/dtv/vo/RfInfo$EnumInfoType;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/Channeltuning;

    move-object/from16 v21, v0

    # getter for: Lcom/konka/tvsettings/channel/Channeltuning;->channelno:I
    invoke-static/range {v21 .. v21}, Lcom/konka/tvsettings/channel/Channeltuning;->access$8(Lcom/konka/tvsettings/channel/Channeltuning;)I

    move-result v21

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvGetRFInfo(Lcom/mstar/android/tvapi/dtv/vo/RfInfo$EnumInfoType;I)Lcom/mstar/android/tvapi/dtv/vo/RfInfo;

    move-result-object v11

    goto/16 :goto_3

    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/Channeltuning;

    move-object/from16 v20, v0

    # getter for: Lcom/konka/tvsettings/channel/Channeltuning;->viewholder_channeltune:Lcom/konka/tvsettings/channel/ViewHolder;
    invoke-static/range {v20 .. v20}, Lcom/konka/tvsettings/channel/Channeltuning;->access$0(Lcom/konka/tvsettings/channel/Channeltuning;)Lcom/konka/tvsettings/channel/ViewHolder;

    move-result-object v20

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->progressbar_cha_program_val:Landroid/widget/TextView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/Channeltuning;

    move-object/from16 v20, v0

    # getter for: Lcom/konka/tvsettings/channel/Channeltuning;->viewholder_channeltune:Lcom/konka/tvsettings/channel/ViewHolder;
    invoke-static/range {v20 .. v20}, Lcom/konka/tvsettings/channel/Channeltuning;->access$0(Lcom/konka/tvsettings/channel/Channeltuning;)Lcom/konka/tvsettings/channel/ViewHolder;

    move-result-object v20

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_program_vhf:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const-string v21, "UHF"

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    :cond_a
    invoke-interface/range {v18 .. v18}, Lcom/konka/kkinterface/tv/ChannelDesk;->getUserScanType()Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    move-result-object v20

    sget-object v21, Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;->E_SCAN_ALL:Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-ne v0, v1, :cond_b

    add-int v20, v6, v10

    add-int v20, v20, v5

    invoke-static/range {v20 .. v20}, Lcom/konka/tvsettings/channel/Channeltuning;->access$9(I)V

    # getter for: Lcom/konka/tvsettings/channel/Channeltuning;->ATV_EVENTINTERVAL:I
    invoke-static {}, Lcom/konka/tvsettings/channel/Channeltuning;->access$10()I

    move-result v20

    # getter for: Lcom/konka/tvsettings/channel/Channeltuning;->ATV_MIN_FREQ:I
    invoke-static {}, Lcom/konka/tvsettings/channel/Channeltuning;->access$11()I

    move-result v21

    # getter for: Lcom/konka/tvsettings/channel/Channeltuning;->ATV_MAX_FREQ:I
    invoke-static {}, Lcom/konka/tvsettings/channel/Channeltuning;->access$1()I

    move-result v22

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-interface {v0, v1, v2, v3}, Lcom/konka/kkinterface/tv/ChannelDesk;->atvSetAutoTuningStart(III)Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/Channeltuning;

    move-object/from16 v20, v0

    # getter for: Lcom/konka/tvsettings/channel/Channeltuning;->viewholder_channeltune:Lcom/konka/tvsettings/channel/ViewHolder;
    invoke-static/range {v20 .. v20}, Lcom/konka/tvsettings/channel/Channeltuning;->access$0(Lcom/konka/tvsettings/channel/Channeltuning;)Lcom/konka/tvsettings/channel/ViewHolder;

    move-result-object v20

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_tuningprogress_type:Landroid/widget/TextView;

    move-object/from16 v20, v0

    const v21, 0x7f0a00ca    # com.konka.tvsettings.R.string.str_popup_input_menu_atv

    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1

    :cond_b
    invoke-interface/range {v18 .. v18}, Lcom/konka/kkinterface/tv/ChannelDesk;->getUserScanType()Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    move-result-object v20

    sget-object v21, Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;->E_SCAN_DTV:Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-ne v0, v1, :cond_1

    sget-object v20, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;->E_FIRST_SERVICE_DTV:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;

    sget-object v21, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;->E_DEFAULT:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    invoke-interface {v0, v1, v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->changeToFirstService(Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;)Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/Channeltuning;

    move-object/from16 v20, v0

    # getter for: Lcom/konka/tvsettings/channel/Channeltuning;->isDtvAutoUpdateScan:Z
    invoke-static/range {v20 .. v20}, Lcom/konka/tvsettings/channel/Channeltuning;->access$12(Lcom/konka/tvsettings/channel/Channeltuning;)Z

    move-result v20

    if-nez v20, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/Channeltuning;

    move-object/from16 v20, v0

    # getter for: Lcom/konka/tvsettings/channel/Channeltuning;->bExitActivity:Z
    invoke-static/range {v20 .. v20}, Lcom/konka/tvsettings/channel/Channeltuning;->access$13(Lcom/konka/tvsettings/channel/Channeltuning;)Z

    move-result v20

    if-eqz v20, :cond_d

    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/Channeltuning;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/konka/tvsettings/channel/Channeltuning;->finish()V

    goto/16 :goto_1

    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/Channeltuning;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    invoke-static/range {v20 .. v21}, Lcom/konka/tvsettings/channel/Channeltuning;->access$14(Lcom/konka/tvsettings/channel/Channeltuning;Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/channel/Channeltuning$MyHandler;->this$0:Lcom/konka/tvsettings/channel/Channeltuning;

    move-object/from16 v20, v0

    # invokes: Lcom/konka/tvsettings/channel/Channeltuning;->channetuningActivityExit()V
    invoke-static/range {v20 .. v20}, Lcom/konka/tvsettings/channel/Channeltuning;->access$3(Lcom/konka/tvsettings/channel/Channeltuning;)V

    goto/16 :goto_1
.end method
