.class public Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;
.super Landroid/app/Dialog;
.source "ExitTuningInfoDialog.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$konka$kkinterface$tv$ChannelDesk$TV_TS_STATUS:[I

.field private static ATV_EVENTINTERVAL:I

.field private static ATV_MAX_FREQ:I

.field private static ATV_MIN_FREQ:I


# instance fields
.field private isMenu:Z

.field private listener:Landroid/view/View$OnClickListener;

.field private m_DialogContentText:Landroid/widget/TextView;

.field serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

.field protected textview_cha_exittune_no:Landroid/widget/TextView;

.field protected textview_cha_exittune_yes:Landroid/widget/TextView;

.field private viewholder_channeltune:Lcom/konka/tvsettings/channel/ViewHolder;

.field private viewholder_exittune:Lcom/konka/tvsettings/channel/ViewHolder;


# direct methods
.method static synthetic $SWITCH_TABLE$com$konka$kkinterface$tv$ChannelDesk$TV_TS_STATUS()[I
    .locals 3

    sget-object v0, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->$SWITCH_TABLE$com$konka$kkinterface$tv$ChannelDesk$TV_TS_STATUS:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->values()[Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_ATV_AUTO_TUNING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_8

    :goto_1
    :try_start_1
    sget-object v1, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_ATV_MANU_TUNING_LEFT:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_7

    :goto_2
    :try_start_2
    sget-object v1, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_ATV_MANU_TUNING_RIGHT:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_6

    :goto_3
    :try_start_3
    sget-object v1, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_ATV_SCAN_PAUSING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_5

    :goto_4
    :try_start_4
    sget-object v1, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_DTV_AUTO_TUNING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :goto_5
    :try_start_5
    sget-object v1, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_DTV_FULL_TUNING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_3

    :goto_6
    :try_start_6
    sget-object v1, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_DTV_MANU_TUNING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_2

    :goto_7
    :try_start_7
    sget-object v1, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_DTV_SCAN_PAUSING:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_1

    :goto_8
    :try_start_8
    sget-object v1, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_NONE:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_0

    :goto_9
    sput-object v0, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->$SWITCH_TABLE$com$konka$kkinterface$tv$ChannelDesk$TV_TS_STATUS:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_9

    :catch_1
    move-exception v1

    goto :goto_8

    :catch_2
    move-exception v1

    goto :goto_7

    :catch_3
    move-exception v1

    goto :goto_6

    :catch_4
    move-exception v1

    goto :goto_5

    :catch_5
    move-exception v1

    goto :goto_4

    :catch_6
    move-exception v1

    goto :goto_3

    :catch_7
    move-exception v1

    goto :goto_2

    :catch_8
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    const v0, 0xa8f2

    sput v0, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->ATV_MIN_FREQ:I

    const v0, 0xd5eda

    sput v0, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->ATV_MAX_FREQ:I

    const v0, 0x7a120

    sput v0, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->ATV_EVENTINTERVAL:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILcom/konka/kkinterface/tv/TvDeskProvider;Z)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # Lcom/konka/kkinterface/tv/TvDeskProvider;
    .param p4    # Z

    invoke-direct {p0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->isMenu:Z

    new-instance v0, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog$1;-><init>(Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;)V

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->listener:Landroid/view/View$OnClickListener;

    iput-object p3, p0, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iput-boolean p4, p0, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->isMenu:Z

    return-void
.end method

.method private ExitTuningActivityExit(Z)V
    .locals 6
    .param p1    # Z

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    if-eqz p1, :cond_3

    iget-boolean v3, p0, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->isMenu:Z

    if-nez v3, :cond_0

    const-string v3, "liying"

    const-string v4, "atvSetAutoTuningEnd"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->DtvStopScan()Z

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->atvSetAutoTuningEnd()Z

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->makeSourceAtv()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->dismiss()V

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->$SWITCH_TABLE$com$konka$kkinterface$tv$ChannelDesk$TV_TS_STATUS()[I

    move-result-object v3

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->GetTsStatus()Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->ordinal()I

    move-result v4

    aget v3, v3, v4

    sparse-switch v3, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v3, "liying"

    const-string v4, "E_TS_ATV_SCAN_PAUSING"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->atvSetAutoTuningEnd()Z

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;->E_FIRST_SERVICE_ATV:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;->E_DEFAULT:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->changeToFirstService(Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;)Z

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->getContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/konka/tvsettings/channel/ProgramSettingMain;

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v3, "currentPage"

    const/4 v4, 0x2

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->dismiss()V

    goto :goto_0

    :sswitch_1
    invoke-interface {v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->DtvStopScan()Z

    const-string v3, "liying"

    const-string v4, "E_TS_DTV_SCAN_PAUSING"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->getUserScanType()Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    move-result-object v3

    sget-object v4, Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;->E_SCAN_ALL:Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    if-ne v3, v4, :cond_2

    const-string v3, "liying"

    const-string v4, "atvSetAutoTuningStart"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget v3, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->ATV_EVENTINTERVAL:I

    sget v4, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->ATV_MIN_FREQ:I

    sget v5, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->ATV_MAX_FREQ:I

    invoke-interface {v2, v3, v4, v5}, Lcom/konka/kkinterface/tv/ChannelDesk;->atvSetAutoTuningStart(III)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v3, "TuningService"

    const-string v4, "atvSetAutoTuningStart Error!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->dismiss()V

    goto :goto_0

    :cond_2
    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;->E_FIRST_SERVICE_DTV:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;->E_DEFAULT:Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->changeToFirstService(Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;)Z

    goto :goto_1

    :cond_3
    invoke-static {}, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->$SWITCH_TABLE$com$konka$kkinterface$tv$ChannelDesk$TV_TS_STATUS()[I

    move-result-object v3

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->GetTsStatus()Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->ordinal()I

    move-result v4

    aget v3, v3, v4

    sparse-switch v3, :sswitch_data_1

    goto/16 :goto_0

    :sswitch_2
    invoke-interface {v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->atvSetAutoTuningResume()Z

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->dismiss()V

    goto/16 :goto_0

    :sswitch_3
    invoke-interface {v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvResumeScan()Z

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->dismiss()V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_0
        0x9 -> :sswitch_1
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x5 -> :sswitch_2
        0x9 -> :sswitch_3
    .end sparse-switch
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->ExitTuningActivityExit(Z)V

    return-void
.end method

.method private registerListeners()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->textview_cha_exittune_yes:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->textview_cha_exittune_no:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f030016    # com.konka.tvsettings.R.layout.exittuninginfo_dialog

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->setContentView(I)V

    const v1, 0x7f0700ad    # com.konka.tvsettings.R.id.textview_cha_exittune_yes

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->textview_cha_exittune_yes:Landroid/widget/TextView;

    const v1, 0x7f0700ae    # com.konka.tvsettings.R.id.textview_cha_exittune_no

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->textview_cha_exittune_no:Landroid/widget/TextView;

    const v1, 0x7f0700ab    # com.konka.tvsettings.R.id.textview_cha_exittuning_info

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->m_DialogContentText:Landroid/widget/TextView;

    new-instance v1, Lcom/konka/tvsettings/channel/ViewHolder;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/channel/ViewHolder;-><init>(Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;)V

    iput-object v1, p0, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->viewholder_exittune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->textview_cha_exittune_yes:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->requestFocus()Z

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->registerListeners()V

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    const-string v1, "ExitTuninginfoDialog"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "+++++++++++++++keyCode="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",action = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    :sswitch_0
    return v0

    :sswitch_1
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->ExitTuningActivityExit(Z)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0xb2 -> :sswitch_0
    .end sparse-switch
.end method

.method public setDialogContentText(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ExitTuningInfoDialog;->m_DialogContentText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method
