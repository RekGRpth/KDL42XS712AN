.class public Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;
.super Lcom/konka/tvsettings/teletext/MstarBaseActivity;
.source "AutoTuneOptionActivity.java"


# static fields
.field public static PersionCustomer:Ljava/lang/String;

.field public static PersionCustomer1:Ljava/lang/String;


# instance fields
.field private CountrySelectTable:[I

.field private CountrySelectTable_snowa:[I

.field private CountrySelectTable_xvision:[I

.field private MAXINDEXS:I

.field private MSG_ONCREAT:I

.field private final ONEPINDEXS:I

.field private OptionCountrys:[Ljava/lang/String;

.field private OptionScanTypes:[Ljava/lang/String;

.field private antennatypeindex:I

.field private commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

.field public curCustomer:Ljava/lang/String;

.field private curItem_index:I

.field private hint_left_arrow:Landroid/widget/ImageView;

.field private hint_right_arrow:Landroid/widget/ImageView;

.field private listener:Landroid/view/View$OnClickListener;

.field private mHandler:Landroid/os/Handler;

.field private page_index:I

.field private page_total:I

.field private serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

.field private settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;

.field private settings:Landroid/content/SharedPreferences;

.field private tableCountrySelect_index:I

.field private tuningtype:I

.field private viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "snowa"

    sput-object v0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->PersionCustomer:Ljava/lang/String;

    const-string v0, "x.vision"

    sput-object v0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->PersionCustomer1:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/16 v4, 0x9

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;-><init>()V

    iput v3, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->tuningtype:I

    iput v3, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->page_index:I

    iput v3, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->curItem_index:I

    iput v4, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->ONEPINDEXS:I

    iput v3, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->page_total:I

    const/16 v0, 0x3e9

    iput v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->MSG_ONCREAT:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    iput v3, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->tableCountrySelect_index:I

    const/16 v0, 0x3d

    new-array v0, v0, [I

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRALIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v3

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v5

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BELGIUM:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v6

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BULGARIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v7

    const/4 v1, 0x4

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CROATIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CZECH:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_DENMARK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FINLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FRANCE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GERMANY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v4

    const/16 v1, 0xa

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GREECE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_HUNGARY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ITALY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LUXEMBOURG:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NETHERLANDS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NORWAY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_POLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PORTUGAL:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUMANIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUSSIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SERBIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SLOVENIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SPAIN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SWEDEN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SWITZERLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_UK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NEWZEALAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ARAB:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ESTONIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_HEBREW:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LATVIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SLOVAKIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TURKEY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRELAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FIJI:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_UZBEK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TAJIKISTAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ETHIOPIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AZERBAIJAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SOUTHAFRICA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ALGERIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_EGYPT:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SAUDI_ARABIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRAQ:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NAMIBIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_JORDAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_KUWAIT:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_INDONESIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ISRAEL:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_QATAR:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NIGERIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ZEMBABWE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LITHUANIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_MOROCCO:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TUNIS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_INDIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_YEMEN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LIBYA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PAKISTAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_OTHERS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    iput-object v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->CountrySelectTable:[I

    const/16 v0, 0x38

    new-array v0, v0, [I

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRALIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v3

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v5

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BELGIUM:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v6

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BULGARIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v7

    const/4 v1, 0x4

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CROATIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CZECH:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_DENMARK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FINLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FRANCE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GERMANY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v4

    const/16 v1, 0xa

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GREECE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_HUNGARY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ITALY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LUXEMBOURG:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NETHERLANDS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NORWAY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_POLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PORTUGAL:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUMANIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUSSIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SERBIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SLOVENIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SPAIN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SWEDEN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SWITZERLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_UK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NEWZEALAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ARAB:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ESTONIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LATVIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SLOVAKIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TURKEY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRELAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FIJI:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_UZBEK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TAJIKISTAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ETHIOPIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AZERBAIJAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SOUTHAFRICA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ALGERIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_EGYPT:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SAUDI_ARABIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRAQ:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NAMIBIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_JORDAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_KUWAIT:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_INDONESIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_QATAR:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NIGERIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ZEMBABWE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LITHUANIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_MOROCCO:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TUNIS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_INDIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_OTHERS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    iput-object v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->CountrySelectTable_snowa:[I

    const/16 v0, 0x3c

    new-array v0, v0, [I

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRALIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v3

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v5

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BELGIUM:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v6

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BULGARIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v7

    const/4 v1, 0x4

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CROATIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CZECH:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_DENMARK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FINLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FRANCE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GERMANY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v4

    const/16 v1, 0xa

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GREECE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_HUNGARY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ITALY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LUXEMBOURG:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NETHERLANDS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NORWAY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_POLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PORTUGAL:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUMANIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUSSIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SERBIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SLOVENIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SPAIN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SWEDEN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SWITZERLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_UK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NEWZEALAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ARAB:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ESTONIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_HEBREW:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LATVIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SLOVAKIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TURKEY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRELAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FIJI:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_UZBEK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TAJIKISTAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ETHIOPIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AZERBAIJAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SOUTHAFRICA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ALGERIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_EGYPT:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SAUDI_ARABIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRAQ:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NAMIBIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_JORDAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_KUWAIT:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_INDONESIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_QATAR:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NIGERIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ZEMBABWE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LITHUANIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_MOROCCO:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TUNIS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_INDIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_YEMEN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LIBYA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PAKISTAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_OTHERS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    iput-object v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->CountrySelectTable_xvision:[I

    new-instance v0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$1;-><init>(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$2;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$2;-><init>(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->listener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method private CoutrySelected(I)V
    .locals 8
    .param p1    # I

    iget-object v5, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->getUserScanType()Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    move-result-object v5

    invoke-virtual {v5}, Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;->ordinal()I

    move-result v5

    iput v5, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->tuningtype:I

    iget v5, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->page_index:I

    mul-int/lit8 v5, v5, 0x9

    add-int v2, v5, p1

    iget-object v5, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->curCustomer:Ljava/lang/String;

    sget-object v6, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->PersionCustomer:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->CountrySelectTable_snowa:[I

    aget v0, v5, v2

    :goto_0
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "\n===>>time.timezone "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v5, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_COUNTRY_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v5

    if-lt v0, v5, :cond_2

    :goto_1
    return-void

    :cond_0
    iget-object v5, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->curCustomer:Ljava/lang/String;

    sget-object v6, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->PersionCustomer1:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->CountrySelectTable_xvision:[I

    aget v0, v5, v2

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->CountrySelectTable:[I

    aget v0, v5, v2

    goto :goto_0

    :cond_2
    new-instance v3, Landroid/text/format/Time;

    invoke-direct {v3}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v3}, Landroid/text/format/Time;->setToNow()V

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    move-result-object v5

    aget-object v5, v5, v0

    invoke-interface {v4, v5}, Lcom/konka/kkinterface/tv/ChannelDesk;->setSystemCountry(Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;)V

    iget-object v5, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->curCustomer:Ljava/lang/String;

    sget-object v6, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->PersionCustomer1:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    sget-object v5, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NEWZEALAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-direct {p0, v5}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->setSystemTimeZone(Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;)V

    :goto_2
    iget-object v5, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/SettingDesk;->GetInstallationFlag()Z

    move-result v5

    if-eqz v5, :cond_5

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-class v5, Lcom/konka/tvsettings/popup/InstallationGuideActivity;

    invoke-virtual {v1, p0, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->startActivity(Landroid/content/Intent;)V

    :goto_3
    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->finish()V

    goto :goto_1

    :cond_3
    iget-object v5, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->curCustomer:Ljava/lang/String;

    sget-object v6, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->PersionCustomer:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    sget-object v5, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->getSystemCountry()Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    move-result-object v6

    if-ne v5, v6, :cond_4

    sget-object v5, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NEWZEALAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-direct {p0, v5}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->setSystemTimeZone(Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;)V

    goto :goto_2

    :cond_4
    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    move-result-object v5

    aget-object v5, v5, v0

    invoke-direct {p0, v5}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->setSystemTimeZone(Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;)V

    goto :goto_2

    :cond_5
    sget-object v5, Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;->E_SCAN_ATV:Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->getUserScanType()Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    move-result-object v6

    if-eq v5, v6, :cond_6

    iget v5, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->antennatypeindex:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_7

    sget-object v5, Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;->E_ROUTE_DVBC:Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;

    invoke-interface {v4, v5}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvSetAntennaType(Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;)V

    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "\n===>>when country is pakistan ,set Antenna Type is dvbc!!"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_6
    :goto_4
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-class v5, Lcom/konka/tvsettings/channel/Channeltuning;

    invoke-virtual {v1, p0, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_3

    :cond_7
    sget-object v5, Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;->E_ROUTE_DVBT:Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;

    invoke-interface {v4, v5}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvSetAntennaType(Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;)V

    goto :goto_4
.end method

.method private SetOnFocuseChangeListener()V
    .locals 2

    new-instance v0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$4;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$4;-><init>(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->linear_cha_autotuning_tuningtype:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->MSG_ONCREAT:I

    return v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->updateUiCoutryRequestFocus()V

    return-void
.end method

.method static synthetic access$10(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    return-object v0
.end method

.method static synthetic access$11(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)[I
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->CountrySelectTable_snowa:[I

    return-object v0
.end method

.method static synthetic access$12(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->tableCountrySelect_index:I

    return-void
.end method

.method static synthetic access$13(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->tableCountrySelect_index:I

    return v0
.end method

.method static synthetic access$14(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->setSystemTimeZone(Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;)V

    return-void
.end method

.method static synthetic access$15(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)[I
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->CountrySelectTable_xvision:[I

    return-object v0
.end method

.method static synthetic access$16(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)[I
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->CountrySelectTable:[I

    return-object v0
.end method

.method static synthetic access$17(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->page_index:I

    return-void
.end method

.method static synthetic access$18(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->curItem_index:I

    return-void
.end method

.method static synthetic access$19(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;Lcom/konka/tvsettings/channel/ViewHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    return-void
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)Lcom/konka/tvsettings/channel/ViewHolder;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    return-object v0
.end method

.method static synthetic access$20(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;Lcom/konka/kkinterface/tv/SettingDesk;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;

    return-void
.end method

.method static synthetic access$21(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->tuningtype:I

    return-void
.end method

.method static synthetic access$22(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;[Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->OptionCountrys:[Ljava/lang/String;

    return-void
.end method

.method static synthetic access$23(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->OptionCountrys:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$24(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->MAXINDEXS:I

    return-void
.end method

.method static synthetic access$25(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->MAXINDEXS:I

    return v0
.end method

.method static synthetic access$26(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->page_total:I

    return-void
.end method

.method static synthetic access$27(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->SetOnFocuseChangeListener()V

    return-void
.end method

.method static synthetic access$28(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$29(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->registerListeners()V

    return-void
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->OptionScanTypes:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->tuningtype:I

    return v0
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->page_total:I

    return v0
.end method

.method static synthetic access$6(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;Landroid/widget/ImageView;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->hint_left_arrow:Landroid/widget/ImageView;

    return-void
.end method

.method static synthetic access$7(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;Landroid/widget/ImageView;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->hint_right_arrow:Landroid/widget/ImageView;

    return-void
.end method

.method static synthetic access$8(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->updateUiCoutrySelect()V

    return-void
.end method

.method static synthetic access$9(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->CoutrySelected(I)V

    return-void
.end method

.method private registerListeners()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_1:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_2:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_3:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_4:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_5:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_6:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_7:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_8:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_9:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private setSystemTimeZone(Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;)V
    .locals 4
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v2, "Asia/Shanghai"

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRALIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_0

    const-string v2, "Australia/Sydney"

    :goto_0
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v1}, Landroid/text/format/Time;->setToNow()V

    iget-object v3, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_47

    :goto_1
    return-void

    :cond_0
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_1

    const-string v2, "Europe/Vienna"

    goto :goto_0

    :cond_1
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BELGIUM:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_2

    const-string v2, "Europe/Brussels"

    goto :goto_0

    :cond_2
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BULGARIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_3

    const-string v2, "Europe/Sofia"

    goto :goto_0

    :cond_3
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CROATIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_4

    const-string v2, "Europe/Zagreb"

    goto :goto_0

    :cond_4
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CZECH:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_5

    const-string v2, "Europe/Prague"

    goto :goto_0

    :cond_5
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_DENMARK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_6

    const-string v2, "Europe/Copenhagen"

    goto :goto_0

    :cond_6
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FINLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_7

    const-string v2, "Europe/Helsinki"

    goto :goto_0

    :cond_7
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FRANCE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_8

    const-string v2, "Europe/Paris"

    goto :goto_0

    :cond_8
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GERMANY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_9

    const-string v2, "Europe/Berlin"

    goto :goto_0

    :cond_9
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GREECE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_a

    const-string v2, "Europe/Athens"

    goto :goto_0

    :cond_a
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_HUNGARY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_b

    const-string v2, "Europe/Budapest"

    goto :goto_0

    :cond_b
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ITALY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_c

    const-string v2, "Europe/Rome"

    goto :goto_0

    :cond_c
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LUXEMBOURG:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_d

    const-string v2, "Europe/Luxembourg"

    goto :goto_0

    :cond_d
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NETHERLANDS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_e

    const-string v2, "Europe/Amsterdam"

    goto :goto_0

    :cond_e
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NORWAY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_f

    const-string v2, "Europe/Oslo"

    goto :goto_0

    :cond_f
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_POLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_10

    const-string v2, "Europe/Warsaw"

    goto :goto_0

    :cond_10
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PORTUGAL:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_11

    const-string v2, "Europe/Lisbon"

    goto/16 :goto_0

    :cond_11
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUMANIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_12

    const-string v2, "Europe/London"

    goto/16 :goto_0

    :cond_12
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUSSIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_13

    const-string v2, "Asia/Baghdad"

    goto/16 :goto_0

    :cond_13
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SERBIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_14

    const-string v2, "Europe/Belgrade"

    goto/16 :goto_0

    :cond_14
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SLOVENIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_15

    const-string v2, "Europe/Ljubljana"

    goto/16 :goto_0

    :cond_15
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SPAIN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_16

    const-string v2, "Europe/Madrid"

    goto/16 :goto_0

    :cond_16
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SWEDEN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_17

    const-string v2, "Europe/Stockholm"

    goto/16 :goto_0

    :cond_17
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SWITZERLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_18

    const-string v2, "Europe/Zurich"

    goto/16 :goto_0

    :cond_18
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_UK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_19

    const-string v2, "Europe/London"

    goto/16 :goto_0

    :cond_19
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NEWZEALAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_1a

    const-string v2, "Europe/London"

    goto/16 :goto_0

    :cond_1a
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ARAB:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_1b

    const-string v2, "Asia/Dubai"

    goto/16 :goto_0

    :cond_1b
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ESTONIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_1c

    const-string v2, "Europe/Tallinn"

    goto/16 :goto_0

    :cond_1c
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_HEBREW:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_1d

    const-string v2, "Europe/Tallinn"

    goto/16 :goto_0

    :cond_1d
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LATVIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_1e

    const-string v2, "Europe/Riga"

    goto/16 :goto_0

    :cond_1e
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SLOVAKIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_1f

    const-string v2, "Europe/Bratislava"

    goto/16 :goto_0

    :cond_1f
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TURKEY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_20

    const-string v2, "Europe/Istanbul"

    goto/16 :goto_0

    :cond_20
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRELAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_21

    const-string v2, "Europe/Dublin"

    goto/16 :goto_0

    :cond_21
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_JAPAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_22

    const-string v2, "Asia/Tokyo"

    goto/16 :goto_0

    :cond_22
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PHILIPPINES:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_23

    const-string v2, "Asia/Manila"

    goto/16 :goto_0

    :cond_23
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_THAILAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_24

    const-string v2, "Asia/Bangkok"

    goto/16 :goto_0

    :cond_24
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_MALDIVES:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_25

    const-string v2, "Indian/Maldives"

    goto/16 :goto_0

    :cond_25
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_URUGUAY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_26

    const-string v2, "America/Montevideo"

    goto/16 :goto_0

    :cond_26
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PERU:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_27

    const-string v2, "America/Lima"

    goto/16 :goto_0

    :cond_27
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ARGENTINA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_28

    const-string v2, "America/Argentina/Buenos_Aires"

    goto/16 :goto_0

    :cond_28
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CHILE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_29

    const-string v2, "America/Santiago"

    goto/16 :goto_0

    :cond_29
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_VENEZUELA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_2a

    const-string v2, "America/Caracas"

    goto/16 :goto_0

    :cond_2a
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ECUADOR:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_2b

    const-string v2, "America/Guayaquil"

    goto/16 :goto_0

    :cond_2b
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_COSTARICA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_2c

    const-string v2, "America/Guayaquil"

    goto/16 :goto_0

    :cond_2c
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PARAGUAY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_2d

    const-string v2, "America/Asuncion"

    goto/16 :goto_0

    :cond_2d
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BOLIVIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_2e

    const-string v2, "America/La_Paz"

    goto/16 :goto_0

    :cond_2e
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BELIZE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_2f

    const-string v2, "America/Belize"

    goto/16 :goto_0

    :cond_2f
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NICARAGUA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_30

    const-string v2, "America/Managua"

    goto/16 :goto_0

    :cond_30
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GUATEMALA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_31

    const-string v2, "America/Guatemala"

    goto/16 :goto_0

    :cond_31
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CHINA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_32

    const-string v2, "Asia/Shanghai"

    goto/16 :goto_0

    :cond_32
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TAIWAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_33

    const-string v2, "Asia/Taipei"

    goto/16 :goto_0

    :cond_33
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BRAZIL:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_34

    const-string v2, "America/Noronha"

    goto/16 :goto_0

    :cond_34
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CANADA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_35

    const-string v2, "America/St_Johns"

    goto/16 :goto_0

    :cond_35
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_MEXICO:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_36

    const-string v2, "America/Mexico_City"

    goto/16 :goto_0

    :cond_36
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_US:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_37

    const-string v2, "America/New_York"

    goto/16 :goto_0

    :cond_37
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SOUTHKOREA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_38

    const-string v2, "Asia/Tokyo"

    goto/16 :goto_0

    :cond_38
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FIJI:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_39

    const-string v2, "Pacific/Fiji"

    goto/16 :goto_0

    :cond_39
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_UZBEK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_3a

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TAJIKISTAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_3a

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PAKISTAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_3b

    :cond_3a
    const-string v2, "Asia/Karachi"

    goto/16 :goto_0

    :cond_3b
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ETHIOPIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_3c

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SAUDI_ARABIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_3c

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRAQ:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_3c

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_KUWAIT:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_3c

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_QATAR:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_3c

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_YEMEN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_3d

    :cond_3c
    const-string v2, "Asia/Baghdad"

    goto/16 :goto_0

    :cond_3d
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_3e

    const-string v2, "Asia/Tehran"

    goto/16 :goto_0

    :cond_3e
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AZERBAIJAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_3f

    const-string v2, "Asia/Dubai"

    goto/16 :goto_0

    :cond_3f
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SOUTHAFRICA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_40

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_EGYPT:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_40

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NAMIBIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_40

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_JORDAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_40

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ISRAEL:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_40

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ZEMBABWE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_40

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LITHUANIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_40

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LIBYA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_41

    :cond_40
    const-string v2, "Europe/Athens"

    goto/16 :goto_0

    :cond_41
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ALGERIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_42

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NIGERIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_42

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TUNIS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_43

    :cond_42
    const-string v2, "Europe/Amsterdam"

    goto/16 :goto_0

    :cond_43
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_MOROCCO:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_44

    const-string v2, "Europe/London"

    goto/16 :goto_0

    :cond_44
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_INDIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_45

    const-string v2, "Asia/Calcutta"

    goto/16 :goto_0

    :cond_45
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_INDONESIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_46

    const-string v2, "Asia/Bangkok"

    goto/16 :goto_0

    :cond_46
    const-string v2, "Europe/London"

    goto/16 :goto_0

    :cond_47
    const-string v3, "alarm"

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->setTimeZone(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private updateUiCoutryRequestFocus()V
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->curItem_index:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_1:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_2:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_3:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_4:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_5:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_6:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_6
    iget-object v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_7:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_7
    iget-object v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_8:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_8
    iget-object v0, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_9:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private updateUiCoutrySelect()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->page_index:I

    mul-int/lit8 v0, v1, 0x9

    iget v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->MAXINDEXS:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_1:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->OptionCountrys:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_1:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setFocusable(Z)V

    :goto_0
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->MAXINDEXS:I

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_2:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->OptionCountrys:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_2:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setFocusable(Z)V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->MAXINDEXS:I

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_3:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->OptionCountrys:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_3:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setFocusable(Z)V

    :goto_2
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->MAXINDEXS:I

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_4:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->OptionCountrys:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_4:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setFocusable(Z)V

    :goto_3
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->MAXINDEXS:I

    if-ge v0, v1, :cond_4

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_5:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->OptionCountrys:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_5:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setFocusable(Z)V

    :goto_4
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->MAXINDEXS:I

    if-ge v0, v1, :cond_5

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_6:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->OptionCountrys:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_6:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setFocusable(Z)V

    :goto_5
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->MAXINDEXS:I

    if-ge v0, v1, :cond_6

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_7:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->OptionCountrys:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_7:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setFocusable(Z)V

    :goto_6
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->MAXINDEXS:I

    if-ge v0, v1, :cond_7

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_8:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->OptionCountrys:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_8:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setFocusable(Z)V

    :goto_7
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->MAXINDEXS:I

    if-ge v0, v1, :cond_8

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_9:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->OptionCountrys:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_9:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setFocusable(Z)V

    :goto_8
    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_hint_currentpage_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->page_index:I

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->page_index:I

    if-nez v1, :cond_9

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->hint_left_arrow:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    :goto_9
    iget v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->page_index:I

    iget v2, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->page_total:I

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_a

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->hint_right_arrow:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    :goto_a
    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_1:Landroid/widget/Button;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_1:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setFocusable(Z)V

    goto/16 :goto_0

    :cond_1
    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_2:Landroid/widget/Button;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_2:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setFocusable(Z)V

    goto/16 :goto_1

    :cond_2
    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_3:Landroid/widget/Button;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_3:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setFocusable(Z)V

    goto/16 :goto_2

    :cond_3
    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_4:Landroid/widget/Button;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_4:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setFocusable(Z)V

    goto/16 :goto_3

    :cond_4
    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_5:Landroid/widget/Button;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_5:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setFocusable(Z)V

    goto/16 :goto_4

    :cond_5
    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_6:Landroid/widget/Button;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_6:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setFocusable(Z)V

    goto/16 :goto_5

    :cond_6
    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_7:Landroid/widget/Button;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_7:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setFocusable(Z)V

    goto/16 :goto_6

    :cond_7
    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_8:Landroid/widget/Button;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_8:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setFocusable(Z)V

    goto/16 :goto_7

    :cond_8
    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_9:Landroid/widget/Button;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_9:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setFocusable(Z)V

    goto/16 :goto_8

    :cond_9
    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->hint_left_arrow:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto/16 :goto_9

    :cond_a
    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->hint_right_arrow:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto/16 :goto_a
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f030005    # com.konka.tvsettings.R.layout.autotuning

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b003e    # com.konka.tvsettings.R.array.tv_options

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->OptionScanTypes:[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/konka/tvsettings/TVRootApp;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    iget-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->getCustomerInfo()Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;

    move-result-object v1

    iget-object v1, v1, Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;->strCustomerName:Ljava/lang/String;

    iput-object v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->curCustomer:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->getSettingMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->getDvbMode()I

    move-result v1

    iput v1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->antennatypeindex:I

    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;

    invoke-direct {v2, p0}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;-><init>(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 7
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v6, 0x0

    const/4 v5, 0x2

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch p1, :sswitch_data_0

    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v3

    invoke-static {}, Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;->values()[Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    move-result-object v4

    iget v5, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->tuningtype:I

    aget-object v4, v4, v5

    invoke-interface {v3, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->setUserScanType(Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;)V

    invoke-super {p0, p1, p2}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v4

    :goto_1
    return v4

    :sswitch_0
    sparse-switch v0, :sswitch_data_1

    goto :goto_0

    :sswitch_1
    iget v4, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->tuningtype:I

    if-ne v4, v5, :cond_1

    iput v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->tuningtype:I

    :goto_2
    iget-object v4, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v4, v4, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_autotuning_tuningtype_val:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->OptionScanTypes:[Ljava/lang/String;

    iget v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->tuningtype:I

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget v4, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->tuningtype:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->tuningtype:I

    goto :goto_2

    :sswitch_2
    iget v4, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->page_index:I

    add-int/lit8 v4, v4, 0x1

    mul-int/lit8 v4, v4, 0x9

    iget v5, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->MAXINDEXS:I

    if-ge v4, v5, :cond_0

    iget v4, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->page_index:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->page_index:I

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->updateUiCoutrySelect()V

    iget-object v4, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v4, v4, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_1:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :sswitch_3
    iget v4, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->page_index:I

    add-int/lit8 v4, v4, 0x1

    mul-int/lit8 v4, v4, 0x9

    iget v5, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->MAXINDEXS:I

    if-ge v4, v5, :cond_0

    iget v4, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->page_index:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->page_index:I

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->updateUiCoutrySelect()V

    iget-object v4, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v4, v4, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_2:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :sswitch_4
    iget v4, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->page_index:I

    add-int/lit8 v4, v4, 0x1

    mul-int/lit8 v4, v4, 0x9

    iget v5, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->MAXINDEXS:I

    if-ge v4, v5, :cond_0

    iget v4, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->page_index:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->page_index:I

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->updateUiCoutrySelect()V

    iget-object v4, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v4, v4, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_3:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->requestFocus()Z

    goto/16 :goto_0

    :sswitch_5
    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_0

    :pswitch_1
    iget v4, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->tuningtype:I

    if-nez v4, :cond_2

    iput v5, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->tuningtype:I

    :goto_3
    iget-object v4, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v4, v4, Lcom/konka/tvsettings/channel/ViewHolder;->text_cha_autotuning_tuningtype_val:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->OptionScanTypes:[Ljava/lang/String;

    iget v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->tuningtype:I

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_2
    iget v4, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->tuningtype:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->tuningtype:I

    goto :goto_3

    :pswitch_2
    iget v4, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->page_index:I

    if-lez v4, :cond_0

    iget v4, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->page_index:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->page_index:I

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->updateUiCoutrySelect()V

    iget-object v4, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v4, v4, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_8:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->requestFocus()Z

    goto/16 :goto_0

    :pswitch_3
    iget v4, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->page_index:I

    if-lez v4, :cond_0

    iget v4, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->page_index:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->page_index:I

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->updateUiCoutrySelect()V

    iget-object v4, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v4, v4, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_7:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->requestFocus()Z

    goto/16 :goto_0

    :pswitch_4
    iget v4, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->page_index:I

    if-lez v4, :cond_0

    iget v4, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->page_index:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->page_index:I

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->updateUiCoutrySelect()V

    iget-object v4, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v4, v4, Lcom/konka/tvsettings/channel/ViewHolder;->button_cha_country_9:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->requestFocus()Z

    goto/16 :goto_0

    :sswitch_6
    new-instance v2, Landroid/content/Intent;

    const-class v4, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v2, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->finish()V

    const v4, 0x7f040005    # com.konka.tvsettings.R.anim.anim_menu_exit

    invoke-virtual {p0, v6, v4}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->overridePendingTransition(II)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v4, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/SettingDesk;->GetInstallationFlag()Z

    move-result v4

    if-eqz v4, :cond_3

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-class v4, Lcom/konka/tvsettings/popup/InstallationGuideActivity;

    invoke-virtual {v1, p0, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->finish()V

    goto/16 :goto_0

    :cond_3
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-class v4, Lcom/konka/tvsettings/channel/ProgramSettingMain;

    invoke-virtual {v1, p0, v4}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v4, "currentPage"

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->finish()V

    goto/16 :goto_0

    :sswitch_8
    const/4 v4, 0x1

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_6
        0x15 -> :sswitch_5
        0x16 -> :sswitch_0
        0x52 -> :sswitch_7
        0xb2 -> :sswitch_8
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x7f070022 -> :sswitch_1    # com.konka.tvsettings.R.id.linearlayout_cha_autotuning_tuningtype
        0x7f07002e -> :sswitch_2    # com.konka.tvsettings.R.id.button_cha_autotuning_choosecountry_denmark
        0x7f07002f -> :sswitch_3    # com.konka.tvsettings.R.id.button_cha_autotuning_choosecountry_finland
        0x7f070030 -> :sswitch_4    # com.konka.tvsettings.R.id.button_cha_autotuning_choosecountry_france
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x7f070022
        :pswitch_1    # com.konka.tvsettings.R.id.linearlayout_cha_autotuning_tuningtype
        :pswitch_0    # com.konka.tvsettings.R.id.textview_cha_autotuning_tuningtype
        :pswitch_0    # com.konka.tvsettings.R.id.textview_cha_autotuning_tuningtype_val
        :pswitch_0    # com.konka.tvsettings.R.id.relativelayout_option_body
        :pswitch_0    # com.konka.tvsettings.R.id.image_left_arrow_hint
        :pswitch_0    # com.konka.tvsettings.R.id.relativelayout_country_choose
        :pswitch_3    # com.konka.tvsettings.R.id.button_cha_autotuning_choosecountry_australia
        :pswitch_2    # com.konka.tvsettings.R.id.button_cha_autotuning_choosecountry_austria
        :pswitch_4    # com.konka.tvsettings.R.id.button_cha_autotuning_choosecountry_beligum
    .end packed-switch
.end method

.method protected onPause()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->pauseMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resumeMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onResume()V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resetMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onUserInteraction()V

    return-void
.end method
