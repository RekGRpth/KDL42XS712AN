.class Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;
.super Ljava/lang/Object;
.source "AutoTuneOptionActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    iget-object v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    # getter for: Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v6}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$10(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v6

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getDataBaseManagerInstance()Lcom/konka/kkinterface/tv/DataBaseDesk;

    move-result-object v6

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/DataBaseDesk;->queryCurCountry()I

    move-result v0

    iget-object v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    iget-object v6, v6, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->curCustomer:Ljava/lang/String;

    sget-object v7, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->PersionCustomer:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    # getter for: Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->CountrySelectTable_snowa:[I
    invoke-static {v6}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$11(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)[I

    move-result-object v6

    array-length v3, v6

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v3, :cond_1

    :goto_1
    iget-object v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    # getter for: Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->CountrySelectTable_snowa:[I
    invoke-static {v6}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$11(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)[I

    move-result-object v6

    iget-object v7, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    # getter for: Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->tableCountrySelect_index:I
    invoke-static {v7}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$13(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)I

    move-result v7

    aget v6, v6, v7

    sget-object v7, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v7}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v7

    if-ne v6, v7, :cond_0

    iget-object v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    sget-object v7, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NEWZEALAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    # invokes: Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->setSystemTimeZone(Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;)V
    invoke-static {v6, v7}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$14(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;)V

    :cond_0
    :goto_2
    iget-object v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    iget-object v7, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    # getter for: Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->tableCountrySelect_index:I
    invoke-static {v7}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$13(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)I

    move-result v7

    div-int/lit8 v7, v7, 0x9

    invoke-static {v6, v7}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$17(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;I)V

    iget-object v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    iget-object v7, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    # getter for: Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->tableCountrySelect_index:I
    invoke-static {v7}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$13(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)I

    move-result v7

    rem-int/lit8 v7, v7, 0x9

    invoke-static {v6, v7}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$18(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;I)V

    iget-object v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    new-instance v7, Lcom/konka/tvsettings/channel/ViewHolder;

    iget-object v8, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    invoke-direct {v7, v8}, Lcom/konka/tvsettings/channel/ViewHolder;-><init>(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)V

    invoke-static {v6, v7}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$19(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;Lcom/konka/tvsettings/channel/ViewHolder;)V

    iget-object v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    # getter for: Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/tvsettings/channel/ViewHolder;
    invoke-static {v6}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$2(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)Lcom/konka/tvsettings/channel/ViewHolder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/konka/tvsettings/channel/ViewHolder;->findViewsForAutoTuning()V

    iget-object v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    iget-object v7, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    # getter for: Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v7}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$10(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v7

    invoke-interface {v7}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$20(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;Lcom/konka/kkinterface/tv/SettingDesk;)V

    iget-object v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    # getter for: Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v6}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$10(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v6

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v5

    iget-object v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/ChannelDesk;->getUserScanType()Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    move-result-object v7

    invoke-virtual {v7}, Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;->ordinal()I

    move-result v7

    invoke-static {v6, v7}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$21(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;I)V

    iget-object v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    iget-object v6, v6, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->curCustomer:Ljava/lang/String;

    sget-object v7, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->PersionCustomer:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    iget-object v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    iget-object v7, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    invoke-virtual {v7}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0006    # com.konka.tvsettings.R.array.str_arr_autotuning_country_snowa

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$22(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;[Ljava/lang/String;)V

    :goto_3
    iget-object v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    iget-object v7, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    # getter for: Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->OptionCountrys:[Ljava/lang/String;
    invoke-static {v7}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$23(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)[Ljava/lang/String;

    move-result-object v7

    array-length v7, v7

    invoke-static {v6, v7}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$24(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;I)V

    iget-object v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    # getter for: Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->MAXINDEXS:I
    invoke-static {v6}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$25(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)I

    move-result v6

    rem-int/lit8 v6, v6, 0x9

    if-nez v6, :cond_9

    iget-object v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    iget-object v7, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    # getter for: Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->MAXINDEXS:I
    invoke-static {v7}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$25(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)I

    move-result v7

    div-int/lit8 v7, v7, 0x9

    invoke-static {v6, v7}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$26(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;I)V

    :goto_4
    iget-object v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    # invokes: Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->SetOnFocuseChangeListener()V
    invoke-static {v6}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$27(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)V

    iget-object v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    # getter for: Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$28(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)Landroid/os/Handler;

    move-result-object v6

    invoke-static {v6}, Lcom/konka/tvsettings/common/LittleDownTimer;->setHandler(Landroid/os/Handler;)V

    iget-object v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    # invokes: Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->registerListeners()V
    invoke-static {v6}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$29(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)V

    iget-object v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    # getter for: Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$28(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)Landroid/os/Handler;

    move-result-object v6

    iget-object v7, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    # getter for: Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->MSG_ONCREAT:I
    invoke-static {v7}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$0(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void

    :cond_1
    iget-object v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    # getter for: Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->CountrySelectTable_snowa:[I
    invoke-static {v6}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$11(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)[I

    move-result-object v6

    aget v6, v6, v1

    if-ne v6, v0, :cond_2

    iget-object v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    invoke-static {v6, v1}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$12(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;I)V

    goto/16 :goto_1

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    :cond_3
    iget-object v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    iget-object v6, v6, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->curCustomer:Ljava/lang/String;

    sget-object v7, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->PersionCustomer1:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    # getter for: Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->CountrySelectTable_xvision:[I
    invoke-static {v6}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$15(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)[I

    move-result-object v6

    array-length v4, v6

    const/4 v1, 0x0

    :goto_5
    if-ge v1, v4, :cond_0

    iget-object v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    # getter for: Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->CountrySelectTable_xvision:[I
    invoke-static {v6}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$15(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)[I

    move-result-object v6

    aget v6, v6, v1

    if-ne v6, v0, :cond_4

    iget-object v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    invoke-static {v6, v1}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$12(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;I)V

    goto/16 :goto_2

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_5
    iget-object v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    # getter for: Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->CountrySelectTable:[I
    invoke-static {v6}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$16(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)[I

    move-result-object v6

    array-length v2, v6

    const/4 v1, 0x0

    :goto_6
    if-ge v1, v2, :cond_0

    iget-object v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    # getter for: Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->CountrySelectTable:[I
    invoke-static {v6}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$16(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)[I

    move-result-object v6

    aget v6, v6, v1

    if-ne v6, v0, :cond_6

    iget-object v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    invoke-static {v6, v1}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$12(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;I)V

    goto/16 :goto_2

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_7
    iget-object v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    iget-object v6, v6, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->curCustomer:Ljava/lang/String;

    sget-object v7, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->PersionCustomer1:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    iget-object v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    iget-object v7, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    invoke-virtual {v7}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0005    # com.konka.tvsettings.R.array.str_arr_autotuning_country_xvision

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$22(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;[Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_8
    iget-object v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    iget-object v7, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    invoke-virtual {v7}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0004    # com.konka.tvsettings.R.array.str_arr_autotuning_country

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$22(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;[Ljava/lang/String;)V

    goto/16 :goto_3

    :cond_9
    iget-object v6, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    iget-object v7, p0, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity$3;->this$0:Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    # getter for: Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->MAXINDEXS:I
    invoke-static {v7}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$25(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;)I

    move-result v7

    div-int/lit8 v7, v7, 0x9

    add-int/lit8 v7, v7, 0x1

    invoke-static {v6, v7}, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->access$26(Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;I)V

    goto/16 :goto_4
.end method
