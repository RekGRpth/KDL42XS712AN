.class Lcom/konka/tvsettings/channel/CimmiActivity$2;
.super Ljava/lang/Object;
.source "CimmiActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/channel/CimmiActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/channel/CimmiActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/channel/CimmiActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/channel/CimmiActivity$2;->this$0:Lcom/konka/tvsettings/channel/CimmiActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/konka/tvsettings/channel/CimmiActivity$2;->this$0:Lcom/konka/tvsettings/channel/CimmiActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/channel/CimmiActivity;->resetCimmiTimeCounter()V

    :try_start_0
    iget-object v1, p0, Lcom/konka/tvsettings/channel/CimmiActivity$2;->this$0:Lcom/konka/tvsettings/channel/CimmiActivity;

    # getter for: Lcom/konka/tvsettings/channel/CimmiActivity;->ci:Lcom/konka/kkinterface/tv/CiDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/channel/CimmiActivity;->access$0(Lcom/konka/tvsettings/channel/CimmiActivity;)Lcom/konka/kkinterface/tv/CiDesk;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/tvsettings/channel/CimmiActivity$2;->this$0:Lcom/konka/tvsettings/channel/CimmiActivity;

    # getter for: Lcom/konka/tvsettings/channel/CimmiActivity;->cimmiListView:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/konka/tvsettings/channel/CimmiActivity;->access$1(Lcom/konka/tvsettings/channel/CimmiActivity;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    int-to-short v2, v2

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/CiDesk;->answerMenu(S)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method
