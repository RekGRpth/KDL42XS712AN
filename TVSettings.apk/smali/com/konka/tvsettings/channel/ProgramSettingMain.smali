.class public Lcom/konka/tvsettings/channel/ProgramSettingMain;
.super Landroid/app/Activity;
.source "ProgramSettingMain.java"


# static fields
.field private static final EXIT_MENU:I = 0x10


# instance fields
.field private DvbcEnable:Z

.field private commonHandler:Lcom/konka/tvsettings/MainMenuActivity$MainMenuHandler;

.field public curCustomer:Ljava/lang/String;

.field private iCurFocusId:I

.field private itemAntennaType:Lcom/konka/tvsettings/picture/PictureSettingItem1;

.field private itemLcnArrange:Lcom/konka/tvsettings/picture/PictureSettingItem1;

.field private m_commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

.field private m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

.field private myHandler:Landroid/os/Handler;

.field private rootApp:Lcom/konka/tvsettings/TVRootApp;

.field private tvPipPopManager:Lcom/mstar/android/tv/TvPipPopManager;

.field private viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->iCurFocusId:I

    iput-object v1, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->commonHandler:Lcom/konka/tvsettings/MainMenuActivity$MainMenuHandler;

    iput-object v1, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->itemLcnArrange:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    iput-object v1, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->itemAntennaType:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->DvbcEnable:Z

    new-instance v0, Lcom/konka/tvsettings/channel/ProgramSettingMain$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/channel/ProgramSettingMain$1;-><init>(Lcom/konka/tvsettings/channel/ProgramSettingMain;)V

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->myHandler:Landroid/os/Handler;

    return-void
.end method

.method private OnEnterKeyDownAction()V
    .locals 14

    const v13, 0x7f0a0014    # com.konka.tvsettings.R.string.warning_dtvnotsupport

    const/4 v12, 0x3

    const/4 v11, 0x0

    const v10, 0x7f0a0015    # com.konka.tvsettings.R.string.warning_notrightsource

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->getCurrentFocus()Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->getId()I

    move-result v3

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v8

    iput-object v8, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->tvPipPopManager:Lcom/mstar/android/tv/TvPipPopManager;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "the current focus item id==="

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tv/TvPipPopManager;->getSubInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v8

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v8, v9, :cond_1

    iget-object v8, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->m_commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v8

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v8, v9, :cond_1

    const-string v8, "ProgramSettingMain"

    const-string v9, "ATV selected, close PIP..."

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tv/TvPipPopManager;->isPipEnabled()Z

    move-result v8

    if-eqz v8, :cond_1

    new-instance v5, Landroid/content/Intent;

    const-string v8, "com.konka.hotkey.disablePip"

    invoke-direct {v5, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v5}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->sendBroadcast(Landroid/content/Intent;)V

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tv/TvPipPopManager;->disablePip()Z

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v8

    invoke-virtual {v8, v11}, Lcom/mstar/android/tv/TvPipPopManager;->setPipOnFlag(Z)Z

    :cond_1
    iget-object v8, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->m_commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v8

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v8, v9, :cond_2

    iget-object v8, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->m_commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v8

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v8, v9, :cond_4

    :cond_2
    invoke-direct {p0}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->getSystemLock()Z

    move-result v8

    if-eqz v8, :cond_3

    const-class v8, Lcom/konka/tvsettings/popup/CheckParentalPwd;

    invoke-virtual {v4, p0, v8}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v8, "lockType"

    invoke-virtual {v4, v8, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v4}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->finish()V

    goto :goto_0

    :cond_3
    const-class v8, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;

    invoke-virtual {v4, p0, v8}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v4}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->finish()V

    goto :goto_0

    :cond_4
    invoke-static {p0, v10}, Lcom/konka/tvsettings/common/LittleMenu;->popToast(Landroid/content/Context;I)V

    goto :goto_0

    :pswitch_2
    iget-object v8, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->m_commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v8

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v8, v9, :cond_5

    const-string v8, "start the EPG apk!!!"

    invoke-static {v8}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v8, Ljava/lang/Thread;

    new-instance v9, Lcom/konka/tvsettings/channel/ProgramSettingMain$5;

    invoke-direct {v9, p0}, Lcom/konka/tvsettings/channel/ProgramSettingMain$5;-><init>(Lcom/konka/tvsettings/channel/ProgramSettingMain;)V

    invoke-direct {v8, v9}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v8}, Ljava/lang/Thread;->start()V

    iget-object v8, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->commonHandler:Lcom/konka/tvsettings/MainMenuActivity$MainMenuHandler;

    const/16 v9, 0x10

    invoke-virtual {v8, v9}, Lcom/konka/tvsettings/MainMenuActivity$MainMenuHandler;->sendEmptyMessage(I)Z

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->finish()V

    goto/16 :goto_0

    :cond_5
    invoke-static {p0, v13}, Lcom/konka/tvsettings/common/LittleMenu;->popToast(Landroid/content/Context;I)V

    goto/16 :goto_0

    :pswitch_3
    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tv/TvPipPopManager;->getSubInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v8

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v8, v9, :cond_6

    iget-object v8, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->m_commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v8

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v8, v9, :cond_6

    const-string v8, "ProgramSettingMain"

    const-string v9, "ATV selected, close PIP..."

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tv/TvPipPopManager;->isPipEnabled()Z

    move-result v8

    if-eqz v8, :cond_6

    new-instance v5, Landroid/content/Intent;

    const-string v8, "com.konka.hotkey.disablePip"

    invoke-direct {v5, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v5}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->sendBroadcast(Landroid/content/Intent;)V

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tv/TvPipPopManager;->disablePip()Z

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v8

    invoke-virtual {v8, v11}, Lcom/mstar/android/tv/TvPipPopManager;->setPipOnFlag(Z)Z

    :cond_6
    iget-object v8, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->m_commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v8

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v8, v9, :cond_7

    iget-object v8, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->m_commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v8

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v8, v9, :cond_9

    :cond_7
    invoke-direct {p0}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->getSystemLock()Z

    move-result v8

    if-eqz v8, :cond_8

    const-class v8, Lcom/konka/tvsettings/popup/CheckParentalPwd;

    invoke-virtual {v4, p0, v8}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v8, "lockType"

    const/4 v9, 0x2

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v4}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->finish()V

    goto/16 :goto_0

    :cond_8
    const-class v8, Lcom/konka/tvsettings/channel/Atvmanualtuning;

    invoke-virtual {v4, p0, v8}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v4}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->finish()V

    goto/16 :goto_0

    :cond_9
    invoke-static {p0, v10}, Lcom/konka/tvsettings/common/LittleMenu;->popToast(Landroid/content/Context;I)V

    goto/16 :goto_0

    :pswitch_4
    iget-object v8, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->m_commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v8

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v8, v9, :cond_a

    iget-object v8, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->m_commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v8

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v8, v9, :cond_c

    :cond_a
    invoke-direct {p0}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->getSystemLock()Z

    move-result v8

    if-eqz v8, :cond_b

    const-class v8, Lcom/konka/tvsettings/popup/CheckParentalPwd;

    invoke-virtual {v4, p0, v8}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v8, "lockType"

    const/4 v9, 0x1

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v4}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->finish()V

    goto/16 :goto_0

    :cond_b
    const-class v8, Lcom/konka/tvsettings/channel/DTVManualTuning;

    invoke-virtual {v4, p0, v8}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v4}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->finish()V

    goto/16 :goto_0

    :cond_c
    invoke-static {p0, v10}, Lcom/konka/tvsettings/common/LittleMenu;->popToast(Landroid/content/Context;I)V

    goto/16 :goto_0

    :pswitch_5
    iget-object v8, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->m_commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v8

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v8, v9, :cond_d

    iget-object v8, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->m_commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v8

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v8, v9, :cond_f

    :cond_d
    invoke-direct {p0}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->getSystemLock()Z

    move-result v8

    if-eqz v8, :cond_e

    const-class v8, Lcom/konka/tvsettings/popup/CheckParentalPwd;

    invoke-virtual {v4, p0, v8}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v8, "lockType"

    const/4 v9, 0x6

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v4}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->finish()V

    goto/16 :goto_0

    :cond_e
    const-class v8, Lcom/konka/tvsettings/channel/ProgramListViewActivity;

    invoke-virtual {v4, p0, v8}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v4}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->finish()V

    goto/16 :goto_0

    :cond_f
    invoke-static {p0, v10}, Lcom/konka/tvsettings/common/LittleMenu;->popToast(Landroid/content/Context;I)V

    goto/16 :goto_0

    :pswitch_6
    iget-object v8, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->m_commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v8

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v8, v9, :cond_10

    const-class v8, Lcom/konka/tvsettings/channel/DTVSignalInfoActivity;

    invoke-virtual {v4, p0, v8}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v4}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->finish()V

    goto/16 :goto_0

    :cond_10
    invoke-static {p0, v13}, Lcom/konka/tvsettings/common/LittleMenu;->popToast(Landroid/content/Context;I)V

    goto/16 :goto_0

    :pswitch_7
    iget-object v8, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v8

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    sget-object v8, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v1, v8, :cond_0

    iget-object v8, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCiManagerInstance()Lcom/konka/kkinterface/tv/CiDesk;

    move-result-object v0

    sget-object v6, Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;->E_NO:Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;

    :try_start_0
    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CiDesk;->getCardState()Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    :goto_1
    sget-object v8, Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;->E_NO:Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;

    if-ne v6, v8, :cond_11

    const v8, 0x7f0a01a7    # com.konka.tvsettings.R.string.str_cimmi_hint_ci_no_module

    invoke-static {p0, v8, v12}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :cond_11
    sget-object v8, Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;->E_INITIALIZING:Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;

    if-ne v6, v8, :cond_12

    const v8, 0x7f0a01a8    # com.konka.tvsettings.R.string.str_cimmi_hint_ci_try_again

    invoke-static {p0, v8, v12}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_12
    sget-object v8, Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;->E_READY:Lcom/mstar/android/tvapi/dtv/vo/EnumCardState;

    if-ne v6, v8, :cond_0

    :try_start_1
    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CiDesk;->enterMenu()V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    const-class v8, Lcom/konka/tvsettings/channel/CimmiActivity;

    invoke-virtual {v4, p0, v8}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v4}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->finish()V

    goto/16 :goto_0

    :catch_1
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x7f070170
        :pswitch_1    # com.konka.tvsettings.R.id.linearlayout_program_autotuning
        :pswitch_0    # com.konka.tvsettings.R.id.program_epg_item_container
        :pswitch_2    # com.konka.tvsettings.R.id.linearlayout_program_epg
        :pswitch_3    # com.konka.tvsettings.R.id.linearlayout_program_atvmanualtuning
        :pswitch_4    # com.konka.tvsettings.R.id.linearlayout_program_dtvmanualtuning
        :pswitch_0    # com.konka.tvsettings.R.id.linearlayout_program_lcnarrange_parent
        :pswitch_0    # com.konka.tvsettings.R.id.linearlayout_program_lcn_arrange
        :pswitch_5    # com.konka.tvsettings.R.id.linearlayout_program_edit
        :pswitch_6    # com.konka.tvsettings.R.id.linearlayout_program_signal_info
        :pswitch_7    # com.konka.tvsettings.R.id.linearlayout_program_ci_information
    .end packed-switch
.end method

.method private SetOnClickListener()V
    .locals 2

    new-instance v0, Lcom/konka/tvsettings/channel/ProgramSettingMain$4;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/channel/ProgramSettingMain$4;-><init>(Lcom/konka/tvsettings/channel/ProgramSettingMain;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_autosearch:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_epg:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_atvmanualsearch:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_dtvmanualsearch:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_programedit:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_signalinfo:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v1, v1, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_ciInformation:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/channel/ProgramSettingMain;)Lcom/konka/tvsettings/picture/PictureSettingItem1;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->itemAntennaType:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/channel/ProgramSettingMain;)Lcom/konka/kkinterface/tv/TvDeskProvider;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/channel/ProgramSettingMain;)Lcom/konka/tvsettings/picture/PictureSettingItem1;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->itemLcnArrange:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/channel/ProgramSettingMain;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->OnEnterKeyDownAction()V

    return-void
.end method

.method private addItemAntennaType()V
    .locals 8

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->getSettingMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;

    move-result-object v6

    invoke-virtual {v6}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->getDvbMode()I

    move-result v5

    new-instance v0, Lcom/konka/tvsettings/channel/ProgramSettingMain$2;

    const v3, 0x7f07016e    # com.konka.tvsettings.R.id.linearlayout_program_antennatype

    const v4, 0x7f0b003d    # com.konka.tvsettings.R.array.str_arr_antennatype

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/konka/tvsettings/channel/ProgramSettingMain$2;-><init>(Lcom/konka/tvsettings/channel/ProgramSettingMain;Landroid/app/Activity;IIILcom/konka/kkimplements/tv/mstar/SettingDeskImpl;)V

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->itemAntennaType:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->m_commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_antennatype:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_antennatype:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method private addItemLcnArrange()V
    .locals 8

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->getSettingMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;

    move-result-object v6

    invoke-virtual {v6}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->getArrangeLCN()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v5, 0x1

    :goto_0
    new-instance v0, Lcom/konka/tvsettings/channel/ProgramSettingMain$3;

    const v3, 0x7f070176    # com.konka.tvsettings.R.id.linearlayout_program_lcn_arrange

    const v4, 0x7f0b0031    # com.konka.tvsettings.R.array.str_arr_pro_lcnarrange_vals

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/konka/tvsettings/channel/ProgramSettingMain$3;-><init>(Lcom/konka/tvsettings/channel/ProgramSettingMain;Landroid/app/Activity;IIILcom/konka/kkimplements/tv/mstar/SettingDeskImpl;)V

    iput-object v0, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->itemLcnArrange:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->m_commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->m_commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_lcnarrange:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_lcnarrange:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    :cond_0
    return-void

    :cond_1
    move v5, v7

    goto :goto_0
.end method

.method private getSystemLock()Z
    .locals 1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getParentalcontrolManager()Lcom/mstar/android/tvapi/common/ParentalcontrolManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/ParentalcontrolManager;->isSystemLock()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public InitUIComponent()V
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->iCurFocusId:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_antennatype:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_autosearch:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_epg:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_atvmanualsearch:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_dtvmanualsearch:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0

    :pswitch_6
    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_lcnarrange:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0

    :pswitch_7
    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_programedit:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0

    :pswitch_8
    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_signalinfo:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0

    :pswitch_9
    iget-object v0, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v0, v0, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_ciInformation:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f07016e
        :pswitch_1    # com.konka.tvsettings.R.id.linearlayout_program_antennatype
        :pswitch_0    # com.konka.tvsettings.R.id.linearlayout_program_antennatype_option
        :pswitch_2    # com.konka.tvsettings.R.id.linearlayout_program_autotuning
        :pswitch_0    # com.konka.tvsettings.R.id.program_epg_item_container
        :pswitch_3    # com.konka.tvsettings.R.id.linearlayout_program_epg
        :pswitch_4    # com.konka.tvsettings.R.id.linearlayout_program_atvmanualtuning
        :pswitch_5    # com.konka.tvsettings.R.id.linearlayout_program_dtvmanualtuning
        :pswitch_0    # com.konka.tvsettings.R.id.linearlayout_program_lcnarrange_parent
        :pswitch_6    # com.konka.tvsettings.R.id.linearlayout_program_lcn_arrange
        :pswitch_7    # com.konka.tvsettings.R.id.linearlayout_program_edit
        :pswitch_8    # com.konka.tvsettings.R.id.linearlayout_program_signal_info
        :pswitch_9    # com.konka.tvsettings.R.id.linearlayout_program_ci_information
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const/16 v6, 0x8

    const/4 v5, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v3, 0x7f03003b    # com.konka.tvsettings.R.layout.program_menu

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->setContentView(I)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->getApplication()Landroid/app/Application;

    move-result-object v3

    check-cast v3, Lcom/konka/tvsettings/TVRootApp;

    iput-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->rootApp:Lcom/konka/tvsettings/TVRootApp;

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->rootApp:Lcom/konka/tvsettings/TVRootApp;

    invoke-virtual {v3}, Lcom/konka/tvsettings/TVRootApp;->getMainmenuHandler()Lcom/konka/tvsettings/MainMenuActivity$MainMenuHandler;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->commonHandler:Lcom/konka/tvsettings/MainMenuActivity$MainMenuHandler;

    new-instance v3, Lcom/konka/tvsettings/channel/ProgramViewHolder;

    invoke-direct {v3, p0}, Lcom/konka/tvsettings/channel/ProgramViewHolder;-><init>(Lcom/konka/tvsettings/channel/ProgramSettingMain;)V

    iput-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    invoke-virtual {v3}, Lcom/konka/tvsettings/channel/ProgramViewHolder;->findViewforProgramSettingMain()V

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->m_commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    :goto_0
    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->m_commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->getCustomerInfo()Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;

    move-result-object v3

    iget-object v3, v3, Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;->strCustomerName:Ljava/lang/String;

    iput-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->curCustomer:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v3, "focusID"

    const v4, 0x7f070170    # com.konka.tvsettings.R.id.linearlayout_program_autotuning

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->iCurFocusId:I

    :cond_0
    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v1, v3, :cond_1

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_ciInformation:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_ciInformation:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_ciInformation:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_ciInformation:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080003    # com.konka.tvsettings.R.color.text_forbidden_col

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_1
    invoke-direct {p0}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->SetOnClickListener()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->InitUIComponent()V

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->m_commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableDVBC()Z

    move-result v3

    iput-boolean v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->DvbcEnable:Z

    iget-boolean v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->DvbcEnable:Z

    if-eqz v3, :cond_5

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->addItemAntennaType()V

    :goto_1
    invoke-direct {p0}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->addItemLcnArrange()V

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->getInputSourceInfo()Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    move-result-object v3

    iget v3, v3, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountDTV:I

    if-gtz v3, :cond_2

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_epg_item_container:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_2
    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->curCustomer:Ljava/lang/String;

    sget-object v4, Lcom/konka/tvsettings/channel/AutoTuneOptionActivity;->PersionCustomer1:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_lcnarrange_parent:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_3
    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->myHandler:Landroid/os/Handler;

    invoke-static {v3}, Lcom/konka/tvsettings/common/LittleDownTimer;->setHandler(Landroid/os/Handler;)V

    return-void

    :cond_4
    const-string v3, "the m_serviceProvider is null"

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_antennatype_parent:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const v7, 0x7f070179    # com.konka.tvsettings.R.id.linearlayout_program_ci_information

    const v6, 0x7f040005    # com.konka.tvsettings.R.anim.anim_menu_exit

    const v5, 0x7f070178    # com.konka.tvsettings.R.id.linearlayout_program_signal_info

    const/4 v2, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->getCurrentFocus()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch p1, :sswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    :goto_1
    return v2

    :sswitch_0
    const-string v2, "KEYCODE_ENTER"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->OnEnterKeyDownAction()V

    goto :goto_0

    :sswitch_1
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->finish()V

    invoke-virtual {p0, v4, v6}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->overridePendingTransition(II)V

    goto :goto_0

    :sswitch_2
    const-string v2, "Exit"

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->finish()V

    invoke-virtual {p0, v4, v6}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->overridePendingTransition(II)V

    goto :goto_0

    :sswitch_3
    iget-boolean v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->DvbcEnable:Z

    if-eqz v3, :cond_2

    const v3, 0x7f07016e    # com.konka.tvsettings.R.id.linearlayout_program_antennatype

    if-ne v0, v3, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_ciInformation:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_ciInformation:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->requestFocus()Z

    :goto_2
    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_ciInformation:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->playSoundEffect(I)V

    goto :goto_1

    :cond_1
    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_signalinfo:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_2

    :cond_2
    const v3, 0x7f070170    # com.konka.tvsettings.R.id.linearlayout_program_autotuning

    if-ne v0, v3, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_ciInformation:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_ciInformation:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->isFocusable()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_ciInformation:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->requestFocus()Z

    :goto_3
    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_ciInformation:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->playSoundEffect(I)V

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_signalinfo:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_3

    :sswitch_4
    iget-boolean v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->DvbcEnable:Z

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_ciInformation:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v3

    if-nez v3, :cond_4

    if-ne v0, v7, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_antennatype:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->requestFocus()Z

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_antennatype:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->playSoundEffect(I)V

    goto/16 :goto_1

    :cond_4
    if-ne v0, v5, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_antennatype:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->requestFocus()Z

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_antennatype:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->playSoundEffect(I)V

    goto/16 :goto_1

    :cond_5
    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_ciInformation:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v3

    if-nez v3, :cond_7

    if-ne v0, v7, :cond_6

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_autosearch:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->requestFocus()Z

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_autosearch:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->playSoundEffect(I)V

    goto/16 :goto_1

    :cond_6
    if-ne v0, v5, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_ciInformation:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->isFocusable()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_autosearch:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->requestFocus()Z

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_autosearch:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->playSoundEffect(I)V

    goto/16 :goto_1

    :cond_7
    if-ne v0, v5, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_autosearch:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->requestFocus()Z

    iget-object v3, p0, Lcom/konka/tvsettings/channel/ProgramSettingMain;->viewHolder:Lcom/konka/tvsettings/channel/ProgramViewHolder;

    iget-object v3, v3, Lcom/konka/tvsettings/channel/ProgramViewHolder;->linearlayout_item_autosearch:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->playSoundEffect(I)V

    goto/16 :goto_1

    :sswitch_5
    const v2, 0x7f070176    # com.konka.tvsettings.R.id.linearlayout_program_lcn_arrange

    if-ne v0, v2, :cond_0

    invoke-direct {p0}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->addItemLcnArrange()V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x13 -> :sswitch_3
        0x14 -> :sswitch_4
        0x15 -> :sswitch_5
        0x16 -> :sswitch_5
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
        0x52 -> :sswitch_2
        0xb2 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->pauseMenu()V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resumeMenu()V

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    const v0, 0x7f040007    # com.konka.tvsettings.R.anim.anim_right_in

    const v1, 0x7f04000a    # com.konka.tvsettings.R.anim.anim_zoom_out

    invoke-virtual {p0, v0, v1}, Lcom/konka/tvsettings/channel/ProgramSettingMain;->overridePendingTransition(II)V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resetMenu()V

    invoke-super {p0}, Landroid/app/Activity;->onUserInteraction()V

    return-void
.end method
