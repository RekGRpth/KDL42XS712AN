.class Lcom/konka/tvsettings/popup/PvrActivity$17;
.super Ljava/lang/Object;
.source "PvrActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/popup/PvrActivity;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/PvrActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/popup/PvrActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/PvrActivity$17;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity$17;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->timeChooser:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/konka/tvsettings/popup/PvrActivity;->access$41(Lcom/konka/tvsettings/popup/PvrActivity;)Landroid/app/AlertDialog;

    move-result-object v2

    const v3, 0x7f0701b9    # com.konka.tvsettings.R.id.pvr_menu_dialog_hours

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/konka/tvsettings/popup/TimeChooser;

    invoke-virtual {v2}, Lcom/konka/tvsettings/popup/TimeChooser;->getValue()I

    move-result v2

    mul-int/lit16 v3, v2, 0xe10

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity$17;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->timeChooser:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/konka/tvsettings/popup/PvrActivity;->access$41(Lcom/konka/tvsettings/popup/PvrActivity;)Landroid/app/AlertDialog;

    move-result-object v2

    const v4, 0x7f0701bb    # com.konka.tvsettings.R.id.pvr_menu_dialog_minutes

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/konka/tvsettings/popup/TimeChooser;

    invoke-virtual {v2}, Lcom/konka/tvsettings/popup/TimeChooser;->getValue()I

    move-result v2

    mul-int/lit8 v2, v2, 0x3c

    add-int/2addr v3, v2

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity$17;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->timeChooser:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/konka/tvsettings/popup/PvrActivity;->access$41(Lcom/konka/tvsettings/popup/PvrActivity;)Landroid/app/AlertDialog;

    move-result-object v2

    const v4, 0x7f0701bc    # com.konka.tvsettings.R.id.pvr_menu_dialog_seconds

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/konka/tvsettings/popup/TimeChooser;

    invoke-virtual {v2}, Lcom/konka/tvsettings/popup/TimeChooser;->getValue()I

    move-result v2

    add-int v1, v3, v2

    const-string v2, "TAG"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "=============>>> CURRENT JUMP TIME = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity$17;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v2}, Lcom/konka/tvsettings/popup/PvrActivity;->access$3(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/konka/kkinterface/tv/PvrDesk;->jumpPlaybackTime(I)Z

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity$17;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    invoke-virtual {v2}, Lcom/konka/tvsettings/popup/PvrActivity;->onKeyPlay()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method
