.class Lcom/konka/tvsettings/popup/PvrActivity$OnStopRecordCancelClickListener;
.super Ljava/lang/Object;
.source "PvrActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/popup/PvrActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnStopRecordCancelClickListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/PvrActivity;


# direct methods
.method private constructor <init>(Lcom/konka/tvsettings/popup/PvrActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/PvrActivity$OnStopRecordCancelClickListener;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/tvsettings/popup/PvrActivity;Lcom/konka/tvsettings/popup/PvrActivity$OnStopRecordCancelClickListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/popup/PvrActivity$OnStopRecordCancelClickListener;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->DEBUG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/konka/tvsettings/popup/PvrActivity;->access$0()Ljava/lang/String;

    move-result-object v0

    const-string v1, "OnStopRecordCancelClickListener onClick"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    const/4 v0, 0x0

    invoke-static {v0}, Lcom/konka/tvsettings/popup/PvrActivity;->access$1(Landroid/app/Dialog;)V

    return-void
.end method
