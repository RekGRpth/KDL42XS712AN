.class Lcom/konka/tvsettings/popup/PvrActivity$15;
.super Ljava/lang/Object;
.source "PvrActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/popup/PvrActivity;->onBackPressed()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/PvrActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/popup/PvrActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/PvrActivity$15;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    :try_start_0
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$15;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$3(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->isPlaybacking()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$15;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # invokes: Lcom/konka/tvsettings/popup/PvrActivity;->resumeLang()V
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$34(Lcom/konka/tvsettings/popup/PvrActivity;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$15;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # invokes: Lcom/konka/tvsettings/popup/PvrActivity;->resumeSubtitle()V
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$35(Lcom/konka/tvsettings/popup/PvrActivity;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$15;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$3(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->stopPlayback()V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$15;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$3(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->stopPlaybackLoop()V

    :cond_0
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$15;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$3(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->isTimeShiftRecording()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$15;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->isAlwaysTimeShiftPlay:Z
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$40(Lcom/konka/tvsettings/popup/PvrActivity;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "PvrActivity=====>>>stopTimeShift555n"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$15;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$3(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->stopTimeShift()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$15;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # invokes: Lcom/konka/tvsettings/popup/PvrActivity;->saveAndExit()V
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$19(Lcom/konka/tvsettings/popup/PvrActivity;)V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method
