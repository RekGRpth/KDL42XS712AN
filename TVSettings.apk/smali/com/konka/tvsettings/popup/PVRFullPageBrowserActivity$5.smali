.class Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$5;
.super Ljava/lang/Object;
.source "PVRFullPageBrowserActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemSelectedListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$5;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$5;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    invoke-static {v3, p3}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$13(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;I)V

    :try_start_0
    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$5;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    invoke-virtual {v3, p3}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->getFileNameByIndex(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "PVRFullPageBrowserActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "===========>>>> current Selected fileName = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$5;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    invoke-virtual {v3, v1}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->constructThumbnailList(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$5;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    # getter for: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->current_recording:I
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$0(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)I

    move-result v3

    if-ne v3, p3, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$5;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    const/4 v4, 0x1

    invoke-static {v3, v4}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$1(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;Z)V

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$5;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    # getter for: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->current_pvr_position:I
    invoke-static {v4}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$12(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$5;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    # getter for: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr_file_number:I
    invoke-static {v4}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$14(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$5;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    # getter for: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvrFileNumberText:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$15(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$5;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    # invokes: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->playRecordedFile(I)V
    invoke-static {v3, p3}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$16(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;I)V

    :goto_1
    return-void

    :cond_0
    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$5;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$1(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;Z)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    return-void
.end method
