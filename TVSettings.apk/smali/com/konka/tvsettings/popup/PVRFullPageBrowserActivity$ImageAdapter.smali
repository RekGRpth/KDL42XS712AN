.class public Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$ImageAdapter;
.super Landroid/widget/BaseAdapter;
.source "PVRFullPageBrowserActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ImageAdapter"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field mGalleryItemBackground:I

.field private mImageArray:[Landroid/graphics/Bitmap;

.field final synthetic this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;


# direct methods
.method public constructor <init>(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;Landroid/content/Context;[Landroid/graphics/Bitmap;)V
    .locals 3
    .param p2    # Landroid/content/Context;
    .param p3    # [Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$ImageAdapter;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p2, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$ImageAdapter;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/konka/tvsettings/R$styleable;->Gallery:[I

    invoke-virtual {p1, v1}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$ImageAdapter;->mGalleryItemBackground:I

    iput-object p3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$ImageAdapter;->mImageArray:[Landroid/graphics/Bitmap;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$ImageAdapter;->mImageArray:[Landroid/graphics/Bitmap;

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$ImageAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$ImageAdapter;->mImageArray:[Landroid/graphics/Bitmap;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    new-instance v1, Landroid/widget/Gallery$LayoutParams;

    const/16 v2, 0xde

    const/16 v3, 0xe7

    invoke-direct {v1, v2, v3}, Landroid/widget/Gallery$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$ImageAdapter;->mGalleryItemBackground:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    return-object v0
.end method
