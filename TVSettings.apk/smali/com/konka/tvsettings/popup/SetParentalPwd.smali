.class public Lcom/konka/tvsettings/popup/SetParentalPwd;
.super Landroid/app/Activity;
.source "SetParentalPwd.java"


# static fields
.field private static PwdNo1:I

.field private static PwdNo2:I


# instance fields
.field private c1:I

.field private c2:I

.field private c3:I

.field private c4:I

.field private conf_pwd:I

.field private confirm_pwd1:Landroid/widget/EditText;

.field private confirm_pwd2:Landroid/widget/EditText;

.field private confirm_pwd3:Landroid/widget/EditText;

.field private confirm_pwd4:Landroid/widget/EditText;

.field private n1:I

.field private n2:I

.field private n3:I

.field private n4:I

.field private new_pwd:I

.field private new_pwd1:Landroid/widget/EditText;

.field private new_pwd2:Landroid/widget/EditText;

.field private new_pwd3:Landroid/widget/EditText;

.field private new_pwd4:Landroid/widget/EditText;

.field private o1:I

.field private o2:I

.field private o3:I

.field private o4:I

.field private old_pwd:I

.field private old_pwd1:Landroid/widget/EditText;

.field private old_pwd2:Landroid/widget/EditText;

.field private old_pwd3:Landroid/widget/EditText;

.field private old_pwd4:Landroid/widget/EditText;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo1:I

    sput v0, Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo2:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$0()I
    .locals 1

    sget v0, Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo1:I

    return v0
.end method

.method static synthetic access$1(I)V
    .locals 0

    sput p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo1:I

    return-void
.end method

.method static synthetic access$10(Lcom/konka/tvsettings/popup/SetParentalPwd;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->o3:I

    return-void
.end method

.method static synthetic access$11(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->new_pwd1:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$12(Lcom/konka/tvsettings/popup/SetParentalPwd;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->o4:I

    return-void
.end method

.method static synthetic access$13(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->new_pwd2:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$14(Lcom/konka/tvsettings/popup/SetParentalPwd;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->n1:I

    return-void
.end method

.method static synthetic access$15(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->new_pwd3:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$16(Lcom/konka/tvsettings/popup/SetParentalPwd;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->n2:I

    return-void
.end method

.method static synthetic access$17(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->new_pwd4:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$18(Lcom/konka/tvsettings/popup/SetParentalPwd;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->n3:I

    return-void
.end method

.method static synthetic access$19(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->confirm_pwd1:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$2()I
    .locals 1

    sget v0, Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo2:I

    return v0
.end method

.method static synthetic access$20(Lcom/konka/tvsettings/popup/SetParentalPwd;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->n4:I

    return-void
.end method

.method static synthetic access$21(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->confirm_pwd2:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$22(Lcom/konka/tvsettings/popup/SetParentalPwd;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->c1:I

    return-void
.end method

.method static synthetic access$23(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->confirm_pwd3:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$24(Lcom/konka/tvsettings/popup/SetParentalPwd;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->c2:I

    return-void
.end method

.method static synthetic access$25(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->confirm_pwd4:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$26(Lcom/konka/tvsettings/popup/SetParentalPwd;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->c3:I

    return-void
.end method

.method static synthetic access$27(Lcom/konka/tvsettings/popup/SetParentalPwd;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->setpassword()V

    return-void
.end method

.method static synthetic access$28(Lcom/konka/tvsettings/popup/SetParentalPwd;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->c4:I

    return-void
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->old_pwd2:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->old_pwd1:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/popup/SetParentalPwd;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->o1:I

    return-void
.end method

.method static synthetic access$6(I)V
    .locals 0

    sput p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo2:I

    return-void
.end method

.method static synthetic access$7(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->old_pwd3:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$8(Lcom/konka/tvsettings/popup/SetParentalPwd;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->o2:I

    return-void
.end method

.method static synthetic access$9(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->old_pwd4:Landroid/widget/EditText;

    return-object v0
.end method

.method private findView()V
    .locals 2

    const/4 v1, 0x0

    const v0, 0x7f0701d6    # com.konka.tvsettings.R.id.old_pwd1

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->old_pwd1:Landroid/widget/EditText;

    const v0, 0x7f0701d7    # com.konka.tvsettings.R.id.old_pwd2

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->old_pwd2:Landroid/widget/EditText;

    const v0, 0x7f0701d8    # com.konka.tvsettings.R.id.old_pwd3

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->old_pwd3:Landroid/widget/EditText;

    const v0, 0x7f0701d9    # com.konka.tvsettings.R.id.old_pwd4

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->old_pwd4:Landroid/widget/EditText;

    const v0, 0x7f0701da    # com.konka.tvsettings.R.id.new_pwd1

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->new_pwd1:Landroid/widget/EditText;

    const v0, 0x7f0701db    # com.konka.tvsettings.R.id.new_pwd2

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->new_pwd2:Landroid/widget/EditText;

    const v0, 0x7f0701dc    # com.konka.tvsettings.R.id.new_pwd3

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->new_pwd3:Landroid/widget/EditText;

    const v0, 0x7f0701dd    # com.konka.tvsettings.R.id.new_pwd4

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->new_pwd4:Landroid/widget/EditText;

    const v0, 0x7f0701de    # com.konka.tvsettings.R.id.confirm_pwd1

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->confirm_pwd1:Landroid/widget/EditText;

    const v0, 0x7f0701df    # com.konka.tvsettings.R.id.confirm_pwd2

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->confirm_pwd2:Landroid/widget/EditText;

    const v0, 0x7f0701e0    # com.konka.tvsettings.R.id.confirm_pwd3

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->confirm_pwd3:Landroid/widget/EditText;

    const v0, 0x7f0701e1    # com.konka.tvsettings.R.id.confirm_pwd4

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->confirm_pwd4:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->old_pwd1:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->old_pwd2:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->old_pwd3:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->old_pwd4:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->new_pwd1:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->new_pwd2:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->new_pwd3:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->new_pwd4:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->confirm_pwd1:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->confirm_pwd2:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->confirm_pwd3:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->confirm_pwd4:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setInputType(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->old_pwd1:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->setMyEditListener(Landroid/widget/EditText;)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->old_pwd2:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->setMyEditListener(Landroid/widget/EditText;)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->old_pwd3:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->setMyEditListener(Landroid/widget/EditText;)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->old_pwd4:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->setMyEditListener(Landroid/widget/EditText;)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->new_pwd1:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->setMyEditListener(Landroid/widget/EditText;)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->new_pwd2:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->setMyEditListener(Landroid/widget/EditText;)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->new_pwd3:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->setMyEditListener(Landroid/widget/EditText;)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->new_pwd4:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->setMyEditListener(Landroid/widget/EditText;)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->confirm_pwd1:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->setMyEditListener(Landroid/widget/EditText;)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->confirm_pwd2:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->setMyEditListener(Landroid/widget/EditText;)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->confirm_pwd3:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->setMyEditListener(Landroid/widget/EditText;)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->confirm_pwd4:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->setMyEditListener(Landroid/widget/EditText;)V

    return-void
.end method

.method private getpassword(IIII)I
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    mul-int/lit16 v0, p1, 0x3e8

    mul-int/lit8 v1, p2, 0x64

    add-int/2addr v0, v1

    mul-int/lit8 v1, p3, 0xa

    add-int/2addr v0, v1

    add-int/2addr v0, p4

    return v0
.end method

.method private setMyEditListener(Landroid/widget/EditText;)V
    .locals 1
    .param p1    # Landroid/widget/EditText;

    new-instance v0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/popup/SetParentalPwd$1;-><init>(Lcom/konka/tvsettings/popup/SetParentalPwd;)V

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    new-instance v0, Lcom/konka/tvsettings/popup/SetParentalPwd$2;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/popup/SetParentalPwd$2;-><init>(Lcom/konka/tvsettings/popup/SetParentalPwd;)V

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    return-void
.end method

.method private setpassword()V
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    iget v3, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->o1:I

    iget v4, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->o2:I

    iget v5, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->o3:I

    iget v6, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->o4:I

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/konka/tvsettings/popup/SetParentalPwd;->getpassword(IIII)I

    move-result v3

    iput v3, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->old_pwd:I

    iget v3, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->n1:I

    iget v4, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->n2:I

    iget v5, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->n3:I

    iget v6, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->n4:I

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/konka/tvsettings/popup/SetParentalPwd;->getpassword(IIII)I

    move-result v3

    iput v3, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->new_pwd:I

    iget v3, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->c1:I

    iget v4, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->c2:I

    iget v5, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->c3:I

    iget v6, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->c4:I

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/konka/tvsettings/popup/SetParentalPwd;->getpassword(IIII)I

    move-result v3

    iput v3, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->conf_pwd:I

    const-string v3, "SetParentalPwd"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "old:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->old_pwd:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " new:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->new_pwd:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " conf:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->conf_pwd:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v3, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->new_pwd:I

    iget v4, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->conf_pwd:I

    if-eq v3, v4, :cond_0

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->getApplication()Landroid/app/Application;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a017a    # com.konka.tvsettings.R.string.str_password_does_not_match

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-class v3, Lcom/konka/tvsettings/function/LockSettingActivity;

    invoke-virtual {v1, p0, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/popup/SetParentalPwd;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->finish()V

    :goto_0
    return-void

    :cond_0
    iget v3, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->old_pwd:I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getParentalcontrolManager()Lcom/mstar/android/tvapi/common/ParentalcontrolManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/ParentalcontrolManager;->GetParentalPassword()I

    move-result v4

    if-eq v3, v4, :cond_2

    iget v3, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->old_pwd:I

    const/16 v4, 0x7bc

    if-ne v3, v4, :cond_1

    const-string v3, "menu_check_pwd"

    invoke-virtual {p0, v3, v7}, Lcom/konka/tvsettings/popup/SetParentalPwd;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "pwd_ok"

    invoke-interface {v0, v3, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getParentalcontrolManager()Lcom/mstar/android/tvapi/common/ParentalcontrolManager;

    move-result-object v3

    iget v4, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->new_pwd:I

    invoke-virtual {v3, v4}, Lcom/mstar/android/tvapi/common/ParentalcontrolManager;->setParentalPassword(I)V

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-class v3, Lcom/konka/tvsettings/function/LockSettingActivity;

    invoke-virtual {v1, p0, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/popup/SetParentalPwd;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->finish()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->getApplication()Landroid/app/Application;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Application;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0179    # com.konka.tvsettings.R.string.str_wrong_password

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-class v3, Lcom/konka/tvsettings/function/LockSettingActivity;

    invoke-virtual {v1, p0, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/popup/SetParentalPwd;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->finish()V

    goto :goto_0

    :cond_2
    const-string v3, "menu_check_pwd"

    invoke-virtual {p0, v3, v7}, Lcom/konka/tvsettings/popup/SetParentalPwd;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "pwd_ok"

    invoke-interface {v0, v3, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getParentalcontrolManager()Lcom/mstar/android/tvapi/common/ParentalcontrolManager;

    move-result-object v3

    iget v4, p0, Lcom/konka/tvsettings/popup/SetParentalPwd;->new_pwd:I

    invoke-virtual {v3, v4}, Lcom/mstar/android/tvapi/common/ParentalcontrolManager;->setParentalPassword(I)V

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-class v3, Lcom/konka/tvsettings/function/LockSettingActivity;

    invoke-virtual {v1, p0, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/popup/SetParentalPwd;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->finish()V

    goto/16 :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030049    # com.konka.tvsettings.R.layout.set_parental_pwd

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->setContentView(I)V

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->findView()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v3, 0x0

    sparse-switch p1, :sswitch_data_0

    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    :goto_1
    return v2

    :sswitch_0
    sput v3, Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo1:I

    sput v3, Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo2:I

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/popup/SetParentalPwd;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->finish()V

    const v2, 0x7f040005    # com.konka.tvsettings.R.anim.anim_menu_exit

    invoke-virtual {p0, v3, v2}, Lcom/konka/tvsettings/popup/SetParentalPwd;->overridePendingTransition(II)V

    goto :goto_0

    :sswitch_1
    sput v3, Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo1:I

    sput v3, Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo2:I

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v2, Lcom/konka/tvsettings/function/LockSettingActivity;

    invoke-virtual {v0, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->finish()V

    goto :goto_0

    :sswitch_2
    const/4 v2, 0x1

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x13 -> :sswitch_2
        0x14 -> :sswitch_2
        0x15 -> :sswitch_2
        0x16 -> :sswitch_2
        0x42 -> :sswitch_2
        0x52 -> :sswitch_1
        0xb2 -> :sswitch_2
    .end sparse-switch
.end method
