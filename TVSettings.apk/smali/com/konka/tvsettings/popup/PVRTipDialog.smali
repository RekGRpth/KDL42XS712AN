.class public Lcom/konka/tvsettings/popup/PVRTipDialog;
.super Landroid/app/AlertDialog;
.source "PVRTipDialog.java"


# static fields
.field public static isPVRTipShowing:Z


# instance fields
.field private cancel:Landroid/widget/Button;

.field private context:Landroid/content/Context;

.field private standyRecording:Landroid/widget/Button;

.field private stopRecording:Landroid/widget/Button;

.field private textViewWarning:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/tvsettings/popup/PVRTipDialog;->isPVRTipShowing:Z

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lcom/konka/tvsettings/popup/PVRTipDialog;->context:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/popup/PVRTipDialog;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PVRTipDialog;->saveAndExit()V

    return-void
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/popup/PVRTipDialog;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PVRTipDialog;->context:Landroid/content/Context;

    return-object v0
.end method

.method private saveAndExit()V
    .locals 3

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v1

    if-eqz v1, :cond_1

    :try_start_0
    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PvrManager;->isPlaybacking()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PvrManager;->stopPlayback()V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PvrManager;->stopPlaybackLoop()V

    :cond_0
    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PvrManager;->isTimeShiftRecording()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PvrManager;->stopTimeShift()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public dismiss()V
    .locals 1

    invoke-super {p0}, Landroid/app/AlertDialog;->dismiss()V

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/tvsettings/popup/PVRTipDialog;->isPVRTipShowing:Z

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/AlertDialog;->onCreate(Landroid/os/Bundle;)V

    const v2, 0x7f030054    # com.konka.tvsettings.R.layout.tip

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/popup/PVRTipDialog;->setContentView(I)V

    const v2, 0x7f070216    # com.konka.tvsettings.R.id.stopRecording

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/popup/PVRTipDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/konka/tvsettings/popup/PVRTipDialog;->stopRecording:Landroid/widget/Button;

    const v2, 0x7f070217    # com.konka.tvsettings.R.id.StandyBy

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/popup/PVRTipDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/konka/tvsettings/popup/PVRTipDialog;->standyRecording:Landroid/widget/Button;

    const v2, 0x7f070218    # com.konka.tvsettings.R.id.cancel

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/popup/PVRTipDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/konka/tvsettings/popup/PVRTipDialog;->cancel:Landroid/widget/Button;

    const v2, 0x7f070215    # com.konka.tvsettings.R.id.standbyRecordingTip

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/popup/PVRTipDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/konka/tvsettings/popup/PVRTipDialog;->textViewWarning:Landroid/widget/TextView;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_0
    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PvrManager;->isTimeShiftRecording()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PVRTipDialog;->standyRecording:Landroid/widget/Button;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PVRTipDialog;->textViewWarning:Landroid/widget/TextView;

    const v3, 0x7f0a01ab    # com.konka.tvsettings.R.string.str_pvr_standby_timeshift_dialog_message

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PVRTipDialog;->stopRecording:Landroid/widget/Button;

    const v3, 0x7f0a0188    # com.konka.tvsettings.R.string.str_stop_record_dialog_confirm

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(I)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/konka/tvsettings/popup/PVRTipDialog;->stopRecording:Landroid/widget/Button;

    new-instance v3, Lcom/konka/tvsettings/popup/PVRTipDialog$1;

    invoke-direct {v3, p0}, Lcom/konka/tvsettings/popup/PVRTipDialog$1;-><init>(Lcom/konka/tvsettings/popup/PVRTipDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PVRTipDialog;->standyRecording:Landroid/widget/Button;

    new-instance v3, Lcom/konka/tvsettings/popup/PVRTipDialog$2;

    invoke-direct {v3, p0}, Lcom/konka/tvsettings/popup/PVRTipDialog$2;-><init>(Lcom/konka/tvsettings/popup/PVRTipDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PVRTipDialog;->cancel:Landroid/widget/Button;

    new-instance v3, Lcom/konka/tvsettings/popup/PVRTipDialog$3;

    invoke-direct {v3, p0}, Lcom/konka/tvsettings/popup/PVRTipDialog$3;-><init>(Lcom/konka/tvsettings/popup/PVRTipDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public show()V
    .locals 1

    invoke-super {p0}, Landroid/app/AlertDialog;->show()V

    const/4 v0, 0x1

    sput-boolean v0, Lcom/konka/tvsettings/popup/PVRTipDialog;->isPVRTipShowing:Z

    return-void
.end method
