.class public Lcom/konka/tvsettings/popup/PVRBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PVRBroadcastReceiver.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    sget-boolean v1, Lcom/konka/tvsettings/popup/PVRTipDialog;->isPVRTipShowing:Z

    if-nez v1, :cond_0

    new-instance v0, Lcom/konka/tvsettings/popup/PVRTipDialog;

    invoke-direct {v0, p1}, Lcom/konka/tvsettings/popup/PVRTipDialog;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/16 v2, 0x7d3

    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    :cond_0
    return-void
.end method
