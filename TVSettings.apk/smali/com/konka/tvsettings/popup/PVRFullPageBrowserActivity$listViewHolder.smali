.class Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$listViewHolder;
.super Ljava/lang/Object;
.source "PVRFullPageBrowserActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "listViewHolder"
.end annotation


# instance fields
.field private pvr_text_view_channel:Ljava/lang/String;

.field private pvr_text_view_lcn:Ljava/lang/String;

.field private pvr_text_view_program_service:Ljava/lang/String;

.field final synthetic this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;


# direct methods
.method private constructor <init>(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$listViewHolder;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$listViewHolder;->pvr_text_view_lcn:Ljava/lang/String;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$listViewHolder;->pvr_text_view_channel:Ljava/lang/String;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$listViewHolder;->pvr_text_view_program_service:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$listViewHolder;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$listViewHolder;-><init>(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)V

    return-void
.end method


# virtual methods
.method public getPvr_text_view_channel()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$listViewHolder;->pvr_text_view_channel:Ljava/lang/String;

    return-object v0
.end method

.method public getPvr_text_view_lcn()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$listViewHolder;->pvr_text_view_lcn:Ljava/lang/String;

    return-object v0
.end method

.method public getPvr_text_view_program_service()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$listViewHolder;->pvr_text_view_program_service:Ljava/lang/String;

    return-object v0
.end method

.method public setPvr_text_view_channel(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$listViewHolder;->pvr_text_view_channel:Ljava/lang/String;

    return-void
.end method

.method public setPvr_text_view_lcn(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$listViewHolder;->pvr_text_view_lcn:Ljava/lang/String;

    return-void
.end method

.method public setPvr_text_view_program_service(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$listViewHolder;->pvr_text_view_program_service:Ljava/lang/String;

    return-void
.end method
