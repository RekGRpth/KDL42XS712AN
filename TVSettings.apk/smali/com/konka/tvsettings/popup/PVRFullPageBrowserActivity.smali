.class public Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;
.super Lcom/konka/tvsettings/teletext/MstarBaseActivity;
.source "PVRFullPageBrowserActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$ImageAdapter;,
        Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PVRListViewAdapter;,
        Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;,
        Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$UsbReceiver;,
        Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$listViewHolder;
    }
.end annotation


# instance fields
.field private IsItemClicked:Z

.field private IsScalerInitialized:Z

.field private PVRPlaybackProgress:Landroid/widget/ProgressBar;

.field private PVRPlaybackView:Landroid/view/SurfaceView;

.field private final TAG:Ljava/lang/String;

.field private bitmap:Landroid/graphics/Bitmap;

.field private bitmapList:[Landroid/graphics/Bitmap;

.field private current_pvr_position:I

.field private current_recording:I

.field private gallery:Landroid/widget/LinearLayout;

.field private handler:Landroid/os/Handler;

.field private isRecordingItem:Z

.field private listview:Landroid/widget/ListView;

.field private nCurSortKey:I

.field private pvr:Lcom/konka/kkinterface/tv/PvrDesk;

.field private pvrAdapter:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PVRListViewAdapter;

.field private pvrFileNumberText:Landroid/widget/TextView;

.field private pvrList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$listViewHolder;",
            ">;"
        }
    .end annotation
.end field

.field private pvr_file_number:I

.field private settingDesk:Lcom/konka/kkinterface/tv/SettingDesk;

.field private subtitlePosLive:I

.field private thumbnail:Lcom/konka/tvsettings/popup/PVRThumbnail;

.field private totalRecordTime:Landroid/widget/TextView;

.field private usbReceiver:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$UsbReceiver;

.field private usbSelecter:Lcom/konka/tvsettings/function/USBDiskSelecter;


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;-><init>()V

    const-string v0, "PVRFullPageBrowserActivity"

    iput-object v0, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->TAG:Ljava/lang/String;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->PVRPlaybackView:Landroid/view/SurfaceView;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->totalRecordTime:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvrFileNumberText:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->PVRPlaybackProgress:Landroid/widget/ProgressBar;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvrList:Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->usbReceiver:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$UsbReceiver;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->handler:Landroid/os/Handler;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->thumbnail:Lcom/konka/tvsettings/popup/PVRThumbnail;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->usbSelecter:Lcom/konka/tvsettings/function/USBDiskSelecter;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->settingDesk:Lcom/konka/kkinterface/tv/SettingDesk;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->bitmap:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->bitmapList:[Landroid/graphics/Bitmap;

    iput-boolean v2, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->IsScalerInitialized:Z

    iput-boolean v2, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->IsItemClicked:Z

    iput v2, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->nCurSortKey:I

    iput v2, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->current_pvr_position:I

    iput v2, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr_file_number:I

    iput v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->current_recording:I

    iput-boolean v2, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->isRecordingItem:Z

    iput v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->subtitlePosLive:I

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->current_recording:I

    return v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->isRecordingItem:Z

    return-void
.end method

.method static synthetic access$10(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->scaleToFullScreen()V

    return-void
.end method

.method static synthetic access$11(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->IsItemClicked:Z

    return-void
.end method

.method static synthetic access$12(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->current_pvr_position:I

    return v0
.end method

.method static synthetic access$13(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->current_pvr_position:I

    return-void
.end method

.method static synthetic access$14(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr_file_number:I

    return v0
.end method

.method static synthetic access$15(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvrFileNumberText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$16(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->playRecordedFile(I)V

    return-void
.end method

.method static synthetic access$17(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->IsItemClicked:Z

    return v0
.end method

.method static synthetic access$18(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->IsScalerInitialized:Z

    return v0
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)Lcom/konka/kkinterface/tv/PvrDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->isRecordingItem:Z

    return v0
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->totalRecordTime:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;I)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->getTimeString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$7(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)Landroid/widget/ProgressBar;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->PVRPlaybackProgress:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$8(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->saveChooseDiskSettings(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$9(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->init()V

    return-void
.end method

.method private createPVRPlaybackView()V
    .locals 3

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->PVRPlaybackView:Landroid/view/SurfaceView;

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    const/4 v2, 0x3

    invoke-interface {v1, v2}, Landroid/view/SurfaceHolder;->setType(I)V

    new-instance v0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$7;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$7;-><init>(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->PVRPlaybackView:Landroid/view/SurfaceView;

    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    return-void
.end method

.method private getChooseDiskPath()Ljava/lang/String;
    .locals 3

    const-string v1, "save_setting_select"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "DISK_PATH"

    const-string v2, "unknown"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getChooseDiskSettings()Z
    .locals 3

    const/4 v2, 0x0

    const-string v1, "save_setting_select"

    invoke-virtual {p0, v1, v2}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "IS_ALREADY_CHOOSE_DISK"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method private getTimeString(I)Ljava/lang/String;
    .locals 7
    .param p1    # I

    const/16 v6, 0xa

    const-string v0, "00"

    const-string v1, "00"

    const-string v3, "00"

    rem-int/lit8 v4, p1, 0x3c

    if-ge v4, v6, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "0"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    rem-int/lit8 v5, p1, 0x3c

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_0
    div-int/lit8 v2, p1, 0x3c

    rem-int/lit8 v4, v2, 0x3c

    if-ge v4, v6, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "0"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    rem-int/lit8 v5, v2, 0x3c

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_1
    div-int/lit16 v2, p1, 0xe10

    if-ge v2, v6, :cond_2

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "0"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    rem-int/lit8 v5, p1, 0x3c

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    rem-int/lit8 v5, v2, 0x3c

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method private init()V
    .locals 7

    const/4 v5, -0x1

    :try_start_0
    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->isPVRAvailable()Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "PVR Browser ========>>>create 2"

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    :cond_0
    const v3, 0x7f070192    # com.konka.tvsettings.R.id.pvr_gallery

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->gallery:Landroid/widget/LinearLayout;

    new-instance v3, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$2;

    iget-object v4, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-direct {v3, p0, p0, v4}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$2;-><init>(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;Landroid/content/Context;Lcom/konka/kkinterface/tv/PvrDesk;)V

    iput-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->thumbnail:Lcom/konka/tvsettings/popup/PVRThumbnail;

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->gallery:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->thumbnail:Lcom/konka/tvsettings/popup/PVRThumbnail;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->thumbnail:Lcom/konka/tvsettings/popup/PVRThumbnail;

    invoke-virtual {v3, v2}, Lcom/konka/tvsettings/popup/PVRThumbnail;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->handler:Landroid/os/Handler;

    new-instance v4, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$3;

    invoke-direct {v4, p0}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$3;-><init>(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)V

    const-wide/16 v5, 0x1f4

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    const v3, 0x7f07018a    # com.konka.tvsettings.R.id.pvr_listview

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    iput-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->listview:Landroid/widget/ListView;

    new-instance v3, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PVRListViewAdapter;

    iget-object v4, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvrList:Ljava/util/ArrayList;

    invoke-direct {v3, p0, p0, v4}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PVRListViewAdapter;-><init>(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvrAdapter:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PVRListViewAdapter;

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->listview:Landroid/widget/ListView;

    iget-object v4, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvrAdapter:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PVRListViewAdapter;

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->listview:Landroid/widget/ListView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setDividerHeight(I)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->listview:Landroid/widget/ListView;

    new-instance v4, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$4;

    invoke-direct {v4, p0}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$4;-><init>(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->listview:Landroid/widget/ListView;

    new-instance v4, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$5;

    invoke-direct {v4, p0}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$5;-><init>(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    :try_start_1
    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->constructRecorderList()V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    new-instance v3, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;-><init>(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;)V

    invoke-virtual {v3}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;->start()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method private initTVScaler()Z
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-boolean v4, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->IsScalerInitialized:Z

    if-eqz v4, :cond_0

    :goto_0
    return v2

    :cond_0
    new-instance v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;

    invoke-direct {v1}, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;-><init>()V

    const/4 v4, 0x2

    new-array v0, v4, [I

    iget-object v4, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->PVRPlaybackView:Landroid/view/SurfaceView;

    invoke-virtual {v4, v0}, Landroid/view/SurfaceView;->getLocationOnScreen([I)V

    invoke-static {p0}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v4

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/konka/kkinterface/tv/CommonDesk;->is1280x720(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_2

    aget v4, v0, v3

    add-int/lit8 v4, v4, 0x2d

    iput v4, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->x:I

    aget v4, v0, v2

    add-int/lit8 v4, v4, 0xa

    iput v4, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->y:I

    :goto_1
    iget-object v4, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->PVRPlaybackView:Landroid/view/SurfaceView;

    invoke-virtual {v4}, Landroid/view/SurfaceView;->getHeight()I

    move-result v4

    iput v4, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->height:I

    iget-object v4, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->PVRPlaybackView:Landroid/view/SurfaceView;

    invoke-virtual {v4}, Landroid/view/SurfaceView;->getWidth()I

    move-result v4

    iput v4, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->width:I

    const-string v4, "PVRFullPageBrowserActivity"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "[[[[[[[[[[[[[[the [x,y][w,h]=====["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->x:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->y:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->width:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->height:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget v4, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->width:I

    if-eqz v4, :cond_1

    iget v4, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->height:I

    if-nez v4, :cond_3

    :cond_1
    move v2, v3

    goto/16 :goto_0

    :cond_2
    aget v4, v0, v3

    iput v4, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->x:I

    aget v4, v0, v2

    iput v4, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->y:I

    goto :goto_1

    :cond_3
    invoke-static {p0}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v3

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/konka/kkinterface/tv/CommonDesk;->is1280x720(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    const/16 v4, 0x556

    const/16 v5, 0x300

    invoke-interface {v3, v1, v4, v5}, Lcom/konka/kkinterface/tv/PvrDesk;->setPlaybackWindow(Lcom/mstar/android/tvapi/common/vo/VideoWindowType;II)V

    :goto_2
    iput-boolean v2, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->IsScalerInitialized:Z

    goto/16 :goto_0

    :cond_4
    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    const/16 v4, 0x780

    const/16 v5, 0x438

    invoke-interface {v3, v1, v4, v5}, Lcom/konka/kkinterface/tv/PvrDesk;->setPlaybackWindow(Lcom/mstar/android/tvapi/common/vo/VideoWindowType;II)V

    goto :goto_2
.end method

.method private isPVRAvailable()Z
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const/16 v9, 0x1f4

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v6, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/PvrDesk;->getPvrFileNumber()I

    move-result v6

    iput v6, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr_file_number:I

    const-string v6, "PVRFBActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "===========>>>> pvr.getPvrFileNumber() = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v8, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr_file_number:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v6, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr_file_number:I

    if-gtz v6, :cond_1

    const-string v5, "PVR Browser ========>>>create 6"

    invoke-static {v5}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const v5, 0x7f0a018f    # com.konka.tvsettings.R.string.str_pvr_no_record_file

    invoke-static {p0, v5, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->finish()V

    :cond_0
    :goto_0
    return v4

    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "1/"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr_file_number:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v6, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvrFileNumberText:Landroid/widget/TextView;

    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v6

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v6

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/ChannelDesk;->closeSubtitle()Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    sget-object v7, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    invoke-static {p0}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v8

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v8

    invoke-virtual {v6, v5, v7, v4, v8}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v6

    sget-object v7, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    invoke-virtual {v6, v7}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->enable3d(Lcom/mstar/android/tvapi/common/vo/Enum3dType;)Z

    :cond_2
    const-string v6, "PVR Browser ========>>>create 7"

    invoke-static {v6}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    iget-object v7, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v7}, Lcom/konka/kkinterface/tv/PvrDesk;->getMetadataSortKey()I

    move-result v7

    invoke-interface {v6, v4, v7}, Lcom/konka/kkinterface/tv/PvrDesk;->getPvrFileInfo(II)Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;

    move-result-object v6

    iget-object v0, v6, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;->filename:Ljava/lang/String;

    iget-object v6, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v6, v0}, Lcom/konka/kkinterface/tv/PvrDesk;->startPlayback(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    move-result-object v1

    const-string v6, "PVRFBActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "===========>>>> firstFileName = ["

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "] playbackStatus="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v6, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_SUCCESS:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    invoke-virtual {v1, v6}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Can\'t PlayBack Properly, the Reason is "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    sget-object v6, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    invoke-static {p0}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v7

    invoke-interface {v7}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v7

    invoke-virtual {v5, v4, v6, v4, v7}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z

    goto/16 :goto_0

    :cond_3
    const v4, 0x7f07018f    # com.konka.tvsettings.R.id.pvr_lcn_img

    invoke-virtual {p0, v4}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/SurfaceView;

    iput-object v4, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->PVRPlaybackView:Landroid/view/SurfaceView;

    const v4, 0x7f070191    # com.konka.tvsettings.R.id.pvr_progressBar

    invoke-virtual {p0, v4}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ProgressBar;

    iput-object v4, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->PVRPlaybackProgress:Landroid/widget/ProgressBar;

    iget-object v4, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v4, v0}, Lcom/konka/kkinterface/tv/PvrDesk;->getRecordedFileDurationTime(Ljava/lang/String;)I

    move-result v3

    iget-object v4, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->PVRPlaybackProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v4, v3}, Landroid/widget/ProgressBar;->setMax(I)V

    const-string v4, "PVRFullPageBrowserActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "===========>>>> current playback file totalTime = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->totalRecordTime:Landroid/widget/TextView;

    invoke-direct {p0, v3}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->getTimeString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v4, "PVR Browser ========>>>show totalRecordTime"

    invoke-static {v4}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->createPVRPlaybackView()V

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->initTVScaler()Z

    move v4, v5

    goto/16 :goto_0
.end method

.method private playRecordedFile(I)V
    .locals 7
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->getFileNameByIndex(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->initTVScaler()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v3, "PVRFullPageBrowserActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "===========>>>> current playback fileName = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/PvrDesk;->getCurPlaybackingFileName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/PvrDesk;->isPlaybacking()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/PvrDesk;->stopPlayback()V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/PvrDesk;->stopPlaybackLoop()V

    :cond_2
    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v3, v0}, Lcom/konka/kkinterface/tv/PvrDesk;->startPlayback(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    move-result-object v1

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v3, v0}, Lcom/konka/kkinterface/tv/PvrDesk;->getRecordedFileDurationTime(Ljava/lang/String;)I

    move-result v2

    const-string v3, "PVRFullPageBrowserActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "===========>>>> current playback file totalTime = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->PVRPlaybackProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v2}, Landroid/widget/ProgressBar;->setMax(I)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->totalRecordTime:Landroid/widget/TextView;

    invoke-direct {p0, v2}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->getTimeString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_SUCCESS:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    invoke-virtual {v1, v3}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Can\'t PlayBack Properly, the Reason is "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x1f4

    invoke-static {p0, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    :cond_3
    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->handler:Landroid/os/Handler;

    new-instance v4, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$8;

    invoke-direct {v4, p0}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$8;-><init>(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)V

    const-wide/16 v5, 0x1f4

    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0
.end method

.method private registerUSBDetector()V
    .locals 3

    const-string v1, "PVR Browser ========>>>create 9"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v1, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$UsbReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$UsbReceiver;-><init>(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$UsbReceiver;)V

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->usbReceiver:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$UsbReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->usbReceiver:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$UsbReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->usbReceiver:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$UsbReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method private resumeSubtitle()V
    .locals 7

    const/4 v6, -0x1

    const-string v4, "TvSetting"

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v4, "subtitlePos"

    invoke-interface {v2, v4, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->subtitlePosLive:I

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->getSubtitleInfo()Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "PVR Browser ========>>>subtitlePosLive="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->subtitlePosLive:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "->"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-short v5, v3, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->subtitleServiceNumber:S

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "->"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-short v5, v3, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->currentSubtitleIndex:S

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    if-eqz v3, :cond_0

    iget v4, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->subtitlePosLive:I

    if-eq v4, v6, :cond_0

    iget v4, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->subtitlePosLive:I

    iget-short v5, v3, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->subtitleServiceNumber:S

    if-gt v4, v5, :cond_0

    iget v4, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->subtitlePosLive:I

    iget-short v5, v3, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->currentSubtitleIndex:S

    add-int/lit8 v5, v5, 0x1

    if-eq v4, v5, :cond_0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->closeSubtitle()Z

    iget v4, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->subtitlePosLive:I

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v0, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->openSubtitle(I)Z

    const-string v4, "PVR Browser ========>>>reopen subtitle "

    invoke-static {v4}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v4, "subtitlePos"

    iget v5, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->subtitlePosLive:I

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iput v6, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->subtitlePosLive:I

    :cond_0
    return-void
.end method

.method private saveChooseDiskSettings(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Z
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    const-string v2, "save_setting_select"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "IS_ALREADY_CHOOSE_DISK"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string v2, "DISK_PATH"

    invoke-interface {v0, v2, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v2, "DISK_LABEL"

    invoke-interface {v0, v2, p3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v2, "DISK_LABEL_PC_STYLE"

    invoke-interface {v0, v2, p4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method private scaleToFullScreen()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const v1, 0xffff

    const/4 v2, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;-><init>()V

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->height:I

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->width:I

    iput v1, v0, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->x:I

    iput v1, v0, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->y:I

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v1, v0, v2, v2}, Lcom/konka/kkinterface/tv/PvrDesk;->setPlaybackWindow(Lcom/mstar/android/tvapi/common/vo/VideoWindowType;II)V

    return-void
.end method


# virtual methods
.method public constructRecorderList()V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    iget-object v8, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/PvrDesk;->getPvrFileNumber()I

    move-result v4

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v4, :cond_0

    iget-object v8, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvrAdapter:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PVRListViewAdapter;

    invoke-virtual {v8}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PVRListViewAdapter;->notifyDataSetChanged()V

    iget-object v8, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->listview:Landroid/widget/ListView;

    invoke-virtual {v8}, Landroid/widget/ListView;->invalidate()V

    return-void

    :cond_0
    new-instance v7, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$listViewHolder;

    const/4 v8, 0x0

    invoke-direct {v7, p0, v8}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$listViewHolder;-><init>(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$listViewHolder;)V

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v2, 0x0

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;-><init>()V

    iget-object v8, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    iget-object v9, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v9}, Lcom/konka/kkinterface/tv/PvrDesk;->getMetadataSortKey()I

    move-result v9

    invoke-interface {v8, v1, v9}, Lcom/konka/kkinterface/tv/PvrDesk;->getPvrFileInfo(II)Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;

    move-result-object v0

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "CH "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v9, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v9, v1}, Lcom/konka/kkinterface/tv/PvrDesk;->getFileLcn(I)I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v8, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    iget-object v9, v0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;->filename:Ljava/lang/String;

    invoke-interface {v8, v9}, Lcom/konka/kkinterface/tv/PvrDesk;->getFileServiceName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v8, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    iget-object v9, v0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;->filename:Ljava/lang/String;

    invoke-interface {v8, v9}, Lcom/konka/kkinterface/tv/PvrDesk;->getFileEventName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v8, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/PvrDesk;->getCurRecordingFileName()Ljava/lang/String;

    move-result-object v6

    iget-object v8, v0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;->filename:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    iput v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->current_recording:I

    :cond_1
    invoke-virtual {v7, v3}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$listViewHolder;->setPvr_text_view_lcn(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$listViewHolder;->setPvr_text_view_channel(Ljava/lang/String;)V

    invoke-virtual {v7, v2}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$listViewHolder;->setPvr_text_view_program_service(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvrList:Ljava/util/ArrayList;

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public constructThumbnailList(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v0, p1}, Lcom/konka/kkinterface/tv/PvrDesk;->assignThumbnailFileInfoHandler(Ljava/lang/String;)Z

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->thumbnail:Lcom/konka/tvsettings/popup/PVRThumbnail;

    invoke-virtual {v0}, Lcom/konka/tvsettings/popup/PVRThumbnail;->updateThumbnail()Z

    return-void
.end method

.method public decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 10
    .param p1    # Ljava/lang/String;

    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "filepath in decode file .. "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v7, 0x1

    iput-boolean v7, v3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    invoke-static {p1, v3}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    const/16 v1, 0x64

    const/16 v0, 0x32

    iget v6, v3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v2, v3, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    const/4 v5, 0x1

    :goto_0
    const/16 v7, 0x64

    if-ge v6, v7, :cond_0

    const/16 v7, 0x32

    if-ge v2, v7, :cond_0

    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput v5, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "decode file ........... "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-static {p1, v4}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v7

    iput-object v7, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->bitmap:Landroid/graphics/Bitmap;

    iget-object v7, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->bitmap:Landroid/graphics/Bitmap;

    return-object v7

    :cond_0
    div-int/lit8 v6, v6, 0x2

    div-int/lit8 v2, v2, 0x2

    mul-int/lit8 v5, v5, 0x2

    goto :goto_0
.end method

.method public getFileNameByIndex(I)Ljava/lang/String;
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvrList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    new-instance v0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;-><init>()V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/PvrDesk;->getMetadataSortKey()I

    move-result v2

    invoke-interface {v1, p1, v2}, Lcom/konka/kkinterface/tv/PvrDesk;->getPvrFileInfo(II)Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;

    move-result-object v0

    iget-object v1, v0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;->filename:Ljava/lang/String;

    goto :goto_0
.end method

.method public getSelectedFileName()Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvrList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    new-instance v0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;-><init>()V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->listview:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v2

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/PvrDesk;->getMetadataSortKey()I

    move-result v3

    invoke-interface {v1, v2, v3}, Lcom/konka/kkinterface/tv/PvrDesk;->getPvrFileInfo(II)Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;

    move-result-object v0

    iget-object v1, v0, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;->filename:Ljava/lang/String;

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onCreate(Landroid/os/Bundle;)V

    const v6, 0x7f03003d    # com.konka.tvsettings.R.layout.pvr_full_page_browser

    invoke-virtual {p0, v6}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/TVRootApp;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v6

    iput-object v6, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->settingDesk:Lcom/konka/kkinterface/tv/SettingDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPvrManagerInstance()Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v6

    iput-object v6, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    const-string v6, "PVR Browser ========>>>create "

    invoke-static {v6}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const v6, 0x7f070190    # com.konka.tvsettings.R.id.total_record_time

    invoke-virtual {p0, v6}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->totalRecordTime:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->totalRecordTime:Landroid/widget/TextView;

    const-string v7, ""

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v6, 0x7f07018b    # com.konka.tvsettings.R.id.pvr_file_number

    invoke-virtual {p0, v6}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvrFileNumberText:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvrFileNumberText:Landroid/widget/TextView;

    const-string v7, ""

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :try_start_0
    new-instance v6, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$1;

    invoke-direct {v6, p0, p0}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$1;-><init>(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;Landroid/content/Context;)V

    iput-object v6, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->usbSelecter:Lcom/konka/tvsettings/function/USBDiskSelecter;

    iget-object v6, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->usbSelecter:Lcom/konka/tvsettings/function/USBDiskSelecter;

    invoke-virtual {v6}, Lcom/konka/tvsettings/function/USBDiskSelecter;->getDriverCount()I

    move-result v5

    iget-object v6, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->usbSelecter:Lcom/konka/tvsettings/function/USBDiskSelecter;

    invoke-virtual {v6}, Lcom/konka/tvsettings/function/USBDiskSelecter;->getBestDiskPath()Ljava/lang/String;

    move-result-object v1

    const-string v6, "qhc"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "bestPath::"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    if-gtz v5, :cond_0

    const v6, 0x7f0a00eb    # com.konka.tvsettings.R.string.str_pvr_insert_usb

    const/16 v7, 0x1f4

    invoke-static {p0, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    const-string v6, "NO_DISK"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    const v6, 0x7f0a018f    # com.konka.tvsettings.R.string.str_pvr_no_record_file

    const/16 v7, 0x1f4

    invoke-static {p0, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->finish()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_1
    :try_start_1
    const-string v6, "CHOOSE_DISK"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->usbSelecter:Lcom/konka/tvsettings/function/USBDiskSelecter;

    invoke-virtual {v6}, Lcom/konka/tvsettings/function/USBDiskSelecter;->start()V

    goto :goto_0

    :cond_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "/_MSTPVR"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_3

    const-string v6, "PVRFBActivity"

    const-string v7, "=============>>>>> USB Disk Path is NULL !!!"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const-string v6, "PVRFBActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "=============>>>>> USB Disk Path = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/PvrDesk;->clearMetaData()V

    iget-object v6, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    const/4 v7, 0x2

    invoke-interface {v6, v1, v7}, Lcom/konka/kkinterface/tv/PvrDesk;->setPVRParas(Ljava/lang/String;S)Z

    iget-object v6, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v6, v4}, Lcom/konka/kkinterface/tv/PvrDesk;->createMetaData(Ljava/lang/String;)Z

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->init()V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    const-string v1, "PVR Browser onDestroy "

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.konka.start.alwaytimeshiftthread"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 9
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v5, 0x1

    const/4 v7, 0x0

    const/4 v0, 0x0

    sparse-switch p1, :sswitch_data_0

    :goto_0
    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :cond_0
    move v5, v0

    :goto_1
    return v5

    :sswitch_0
    :try_start_0
    iget v6, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->current_pvr_position:I

    invoke-virtual {p0, v6}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->getFileNameByIndex(I)Ljava/lang/String;

    move-result-object v2

    const-string v6, "PVR"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "get fileName;"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/PvrDesk;->stopPlayback()V

    iget-object v6, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvrList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    iget-object v6, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    const/4 v7, 0x0

    invoke-interface {v6, v7, v2}, Lcom/konka/kkinterface/tv/PvrDesk;->deletefile(ILjava/lang/String;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_2

    :try_start_1
    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->constructRecorderList()V

    iget-object v6, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/PvrDesk;->getPvrFileNumber()I

    move-result v6

    iput v6, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr_file_number:I

    iget v6, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr_file_number:I

    if-nez v6, :cond_1

    const v6, 0x7f0a018f    # com.konka.tvsettings.R.string.str_pvr_no_record_file

    const/16 v7, 0x1f4

    invoke-static {p0, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->finish()V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    :try_start_2
    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    :goto_2
    const/4 v0, 0x1

    :try_start_3
    iget v5, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->current_pvr_position:I

    invoke-virtual {p0, v5}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->getFileNameByIndex(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->constructThumbnailList(Ljava/lang/String;)V

    const-string v5, "PVR"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "jump filename:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v5, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->current_pvr_position:I

    invoke-direct {p0, v5}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->playRecordedFile(I)V
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_1
    :try_start_4
    new-instance v5, Ljava/lang/StringBuilder;

    iget v6, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->current_pvr_position:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr_file_number:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvrFileNumberText:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_4
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_2

    :catch_2
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2

    :sswitch_1
    :try_start_5
    iget-object v5, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/PvrDesk;->isRecording()Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/PvrDesk;->stopPlayback()V

    :cond_2
    iget v5, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->nCurSortKey:I

    const/4 v6, 0x5

    if-ge v5, v6, :cond_3

    iget v5, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->nCurSortKey:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->nCurSortKey:I

    :goto_3
    iget-object v5, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvrList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    iget-object v5, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    const/4 v6, 0x1

    invoke-interface {v5, v6}, Lcom/konka/kkinterface/tv/PvrDesk;->setMetadataSortAscending(Z)V

    iget-object v5, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    iget v6, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->nCurSortKey:I

    invoke-interface {v5, v6}, Lcom/konka/kkinterface/tv/PvrDesk;->setMetadataSortKey(I)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->constructRecorderList()V

    iget-object v5, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->listview:Landroid/widget/ListView;

    invoke-virtual {v5}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->getFileNameByIndex(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->constructThumbnailList(Ljava/lang/String;)V

    invoke-direct {p0, v3}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->playRecordedFile(I)V

    :goto_4
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_3
    const/4 v5, 0x0

    iput v5, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->nCurSortKey:I
    :try_end_5
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_3

    :catch_3
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_4

    :sswitch_2
    iget v5, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->current_pvr_position:I

    const/16 v6, 0xa

    if-lt v5, v6, :cond_4

    iget-object v5, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->listview:Landroid/widget/ListView;

    iget v6, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->current_pvr_position:I

    add-int/lit8 v6, v6, -0xa

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setSelection(I)V

    :goto_5
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_4
    iget-object v5, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->listview:Landroid/widget/ListView;

    invoke-virtual {v5, v7}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_5

    :sswitch_3
    iget v5, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->current_pvr_position:I

    iget v6, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr_file_number:I

    add-int/lit8 v6, v6, -0xb

    if-gt v5, v6, :cond_5

    iget-object v5, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->listview:Landroid/widget/ListView;

    iget v6, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->current_pvr_position:I

    add-int/lit8 v6, v6, 0xa

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setSelection(I)V

    :goto_6
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_5
    iget-object v5, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->listview:Landroid/widget/ListView;

    iget v6, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr_file_number:I

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_6

    nop

    :sswitch_data_0
    .sparse-switch
        0xb7 -> :sswitch_0
        0xba -> :sswitch_1
        0x132 -> :sswitch_3
        0x134 -> :sswitch_2
    .end sparse-switch
.end method

.method protected onResume()V
    .locals 0

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onResume()V

    invoke-static {p0}, Lcom/konka/tvsettings/SwitchMenuHelper;->setActivity(Landroid/app/Activity;)V

    return-void
.end method

.method protected onStart()V
    .locals 1

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onStart()V

    const-string v0, "PVR Browser ========>>>create 8 "

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->registerUSBDetector()V

    return-void
.end method

.method protected onStop()V
    .locals 2

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onStop()V

    const-string v0, "PVR Browser onStop "

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->usbSelecter:Lcom/konka/tvsettings/function/USBDiskSelecter;

    invoke-virtual {v0}, Lcom/konka/tvsettings/function/USBDiskSelecter;->dismiss()V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->usbReceiver:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$UsbReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->usbReceiver:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$UsbReceiver;

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$6;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$6;-><init>(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
