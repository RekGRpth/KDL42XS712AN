.class Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$UsbReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PVRFullPageBrowserActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UsbReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;


# direct methods
.method private constructor <init>(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$UsbReceiver;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$UsbReceiver;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$UsbReceiver;-><init>(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    const-string v7, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    const/4 v3, 0x0

    :try_start_0
    iget-object v7, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$UsbReceiver;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    # getter for: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v7}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$3(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v7

    invoke-interface {v7}, Lcom/konka/kkinterface/tv/PvrDesk;->getPvrMountPath()Ljava/lang/String;

    move-result-object v3

    const-string v7, "qhc"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "mountPath:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    move-object v5, v4

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    :try_start_1
    iget-object v7, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$UsbReceiver;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    # getter for: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v7}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$3(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v7

    invoke-interface {v7}, Lcom/konka/kkinterface/tv/PvrDesk;->clearMetaData()V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    iget-object v7, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$UsbReceiver;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    invoke-virtual {v7}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->finish()V

    :cond_0
    return-void

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method
