.class public abstract Lcom/konka/tvsettings/popup/PVRThumbnail;
.super Landroid/widget/Gallery;
.source "PVRThumbnail.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/popup/PVRThumbnail$ThumbnailAdapter;
    }
.end annotation


# instance fields
.field private adapter:Lcom/konka/tvsettings/popup/PVRThumbnail$ThumbnailAdapter;

.field private images:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field private isShown:Z

.field private mContext:Landroid/content/Context;

.field private pvr:Lcom/konka/kkinterface/tv/PvrDesk;

.field private totalNumber:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/konka/kkinterface/tv/PvrDesk;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/konka/kkinterface/tv/PvrDesk;

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/widget/Gallery;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PVRThumbnail;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PVRThumbnail;->mContext:Landroid/content/Context;

    iput v2, p0, Lcom/konka/tvsettings/popup/PVRThumbnail;->totalNumber:I

    iput-boolean v2, p0, Lcom/konka/tvsettings/popup/PVRThumbnail;->isShown:Z

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PVRThumbnail;->adapter:Lcom/konka/tvsettings/popup/PVRThumbnail$ThumbnailAdapter;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PVRThumbnail;->images:Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/konka/tvsettings/popup/PVRThumbnail;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/konka/tvsettings/popup/PVRThumbnail;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    :try_start_0
    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PVRThumbnail;->constructImages()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v1, Lcom/konka/tvsettings/popup/PVRThumbnail$ThumbnailAdapter;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/popup/PVRThumbnail$ThumbnailAdapter;-><init>(Lcom/konka/tvsettings/popup/PVRThumbnail;)V

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PVRThumbnail;->adapter:Lcom/konka/tvsettings/popup/PVRThumbnail$ThumbnailAdapter;

    new-instance v1, Lcom/konka/tvsettings/popup/PVRThumbnail$1;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/popup/PVRThumbnail$1;-><init>(Lcom/konka/tvsettings/popup/PVRThumbnail;)V

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/popup/PVRThumbnail;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PVRThumbnail;->adapter:Lcom/konka/tvsettings/popup/PVRThumbnail$ThumbnailAdapter;

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/popup/PVRThumbnail;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/popup/PVRThumbnail;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/popup/PVRThumbnail;->totalNumber:I

    return v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/popup/PVRThumbnail;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PVRThumbnail;->images:Ljava/util/ArrayList;

    return-object v0
.end method

.method private constructImages()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/konka/tvsettings/popup/PVRThumbnail;->images:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRThumbnail;->images:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    const/4 v2, 0x0

    iput v2, p0, Lcom/konka/tvsettings/popup/PVRThumbnail;->totalNumber:I

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v2, :cond_0

    return-void

    :cond_0
    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRThumbnail;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v3, v0}, Lcom/konka/kkinterface/tv/PvrDesk;->getThumbnailPath(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/tvsettings/popup/PVRThumbnail;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v4, v0}, Lcom/konka/kkinterface/tv/PvrDesk;->getThumbnailDisplay(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v3, v4}, Lcom/konka/tvsettings/popup/PVRThumbnail;->createThumbnailImage(Ljava/lang/String;Ljava/lang/String;)Landroid/widget/ImageView;

    move-result-object v1

    if-nez v1, :cond_1

    iget v3, p0, Lcom/konka/tvsettings/popup/PVRThumbnail;->totalNumber:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/tvsettings/popup/PVRThumbnail;->totalNumber:I

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRThumbnail;->images:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/konka/tvsettings/popup/PVRThumbnail;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v4, v0}, Lcom/konka/kkinterface/tv/PvrDesk;->getThumbnailPath(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/konka/tvsettings/popup/PVRThumbnail;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v5, v0}, Lcom/konka/kkinterface/tv/PvrDesk;->getThumbnailDisplay(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/konka/tvsettings/popup/PVRThumbnail;->createThumbnailImage(Ljava/lang/String;Ljava/lang/String;)Landroid/widget/ImageView;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private createReflectedImage(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 26
    .param p1    # Landroid/graphics/Bitmap;

    const/16 v24, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v22

    new-instance v6, Landroid/graphics/Matrix;

    invoke-direct {v6}, Landroid/graphics/Matrix;-><init>()V

    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v2, -0x40800000    # -1.0f

    invoke-virtual {v6, v1, v2}, Landroid/graphics/Matrix;->preScale(FF)Z

    const/4 v2, 0x0

    div-int/lit8 v3, v22, 0x2

    div-int/lit8 v5, v22, 0x2

    const/4 v7, 0x0

    move-object/from16 v1, p1

    invoke-static/range {v1 .. v7}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v25

    div-int/lit8 v1, v22, 0x2

    add-int v1, v1, v22

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v21

    new-instance v7, Landroid/graphics/Canvas;

    move-object/from16 v0, v21

    invoke-direct {v7, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v7, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    new-instance v12, Landroid/graphics/Paint;

    invoke-direct {v12}, Landroid/graphics/Paint;-><init>()V

    const/4 v8, 0x0

    move/from16 v0, v22

    int-to-float v9, v0

    int-to-float v10, v4

    add-int/lit8 v1, v22, 0x0

    int-to-float v11, v1

    invoke-virtual/range {v7 .. v12}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    const/4 v1, 0x0

    add-int/lit8 v2, v22, 0x0

    int-to-float v2, v2

    const/4 v3, 0x0

    move-object/from16 v0, v25

    invoke-virtual {v7, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    new-instance v23, Landroid/graphics/Paint;

    invoke-direct/range {v23 .. v23}, Landroid/graphics/Paint;-><init>()V

    new-instance v13, Landroid/graphics/LinearGradient;

    const/4 v14, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v15, v1

    const/16 v16, 0x0

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    add-int/lit8 v1, v1, 0x0

    int-to-float v0, v1

    move/from16 v17, v0

    const v18, 0x70ffffff

    const v19, 0xffffff

    sget-object v20, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v13 .. v20}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    move-object/from16 v0, v23

    invoke-virtual {v0, v13}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    move-object/from16 v0, v23

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    const/4 v15, 0x0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v16, v0

    int-to-float v0, v4

    move/from16 v17, v0

    invoke-virtual/range {v21 .. v21}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    add-int/lit8 v1, v1, 0x0

    int-to-float v0, v1

    move/from16 v18, v0

    move-object v14, v7

    move-object/from16 v19, v23

    invoke-virtual/range {v14 .. v19}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    return-object v21
.end method

.method private createThumbnailImage(Ljava/lang/String;Ljava/lang/String;)Landroid/widget/ImageView;
    .locals 19
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    const/4 v10, 0x0

    :goto_0
    return-object v10

    :cond_1
    new-instance v9, Landroid/widget/Button;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/popup/PVRThumbnail;->mContext:Landroid/content/Context;

    invoke-direct {v9, v2}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    invoke-direct/range {p0 .. p1}, Lcom/konka/tvsettings/popup/PVRThumbnail;->getBitmapfromPath(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-nez v1, :cond_2

    const/4 v10, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    const/high16 v2, 0x43960000    # 300.0f

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/konka/tvsettings/popup/PVRThumbnail;->dip2px(F)I

    move-result v12

    const/high16 v2, 0x43160000    # 150.0f

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/konka/tvsettings/popup/PVRThumbnail;->dip2px(F)I

    move-result v11

    int-to-float v2, v12

    int-to-float v3, v4

    div-float v15, v2, v3

    int-to-float v2, v11

    int-to-float v3, v5

    div-float v14, v2, v3

    new-instance v6, Landroid/graphics/Matrix;

    invoke-direct {v6}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {v6, v15, v14}, Landroid/graphics/Matrix;->postScale(FF)Z

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v7, 0x1

    invoke-static/range {v1 .. v7}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v13

    new-instance v8, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v8, v13}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v9, v8}, Landroid/widget/Button;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, p2

    invoke-virtual {v9, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    const/16 v2, 0x51

    invoke-virtual {v9, v2}, Landroid/widget/Button;->setGravity(I)V

    const/high16 v2, 0x40000000    # 2.0f

    const/4 v3, 0x0

    const/4 v7, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    invoke-static/range {v16 .. v18}, Landroid/graphics/Color;->rgb(III)I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v9, v2, v3, v7, v0}, Landroid/widget/Button;->setShadowLayer(FFFI)V

    const/4 v2, 0x1

    invoke-virtual {v9, v2}, Landroid/widget/Button;->setDrawingCacheEnabled(Z)V

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v12, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v11, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v9, v2, v3}, Landroid/widget/Button;->measure(II)V

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v9}, Landroid/widget/Button;->getMeasuredWidth()I

    move-result v7

    invoke-virtual {v9}, Landroid/widget/Button;->getMeasuredHeight()I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v9, v2, v3, v7, v0}, Landroid/widget/Button;->layout(IIII)V

    invoke-virtual {v9}, Landroid/widget/Button;->buildDrawingCache()V

    new-instance v10, Landroid/widget/ImageButton;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/konka/tvsettings/popup/PVRThumbnail;->mContext:Landroid/content/Context;

    invoke-direct {v10, v2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    invoke-virtual {v9}, Landroid/widget/Button;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v10, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_0
.end method

.method private createTxtImage(Ljava/lang/String;I)Landroid/graphics/Bitmap;
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    mul-int/2addr v3, p2

    add-int/lit8 v3, v3, 0x4

    add-int/lit8 v4, p2, 0x4

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    int-to-float v3, p2

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    const/high16 v3, 0x40000000    # 2.0f

    add-int/lit8 v4, p2, -0x2

    int-to-float v4, v4

    invoke-virtual {v0, p1, v3, v4, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-object v1
.end method

.method private dip2px(F)I
    .locals 3
    .param p1    # F

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PVRThumbnail;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v0, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float v1, p1, v0

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v1, v1

    return v1
.end method

.method private getBitmapfromPath(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 9
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    const-string v6, "PVRTHUM"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "==============>>>>> getBitmapfromPath = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    new-instance v4, Ljava/io/FileInputStream;

    invoke-direct {v4, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/FileInputStream;->available()I

    move-result v5

    const-string v6, "PVRTHUM"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "==============>>>>> data length = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-array v2, v5, [B

    invoke-virtual {v4, v2}, Ljava/io/FileInputStream;->read([B)I

    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    const/16 v6, 0xc0

    const/16 v7, 0x6c

    sget-object v8, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v7, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->copyPixelsFromBuffer(Ljava/nio/Buffer;)V

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    :goto_0
    return-object v0

    :catch_0
    move-exception v3

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public Show(Z)V
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/PVRThumbnail;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PVRThumbnail;->requestFocus()Z

    :goto_0
    iput-boolean p1, p0, Lcom/konka/tvsettings/popup/PVRThumbnail;->isShown:Z

    return-void

    :cond_0
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/PVRThumbnail;->setVisibility(I)V

    goto :goto_0
.end method

.method public addThumbnail(I)Z
    .locals 4
    .param p1    # I

    :try_start_0
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PVRThumbnail;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->getThumbnailNumber()I

    move-result v1

    iput v1, p0, Lcom/konka/tvsettings/popup/PVRThumbnail;->totalNumber:I

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PVRThumbnail;->images:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PVRThumbnail;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v2, p1}, Lcom/konka/kkinterface/tv/PvrDesk;->getThumbnailPath(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRThumbnail;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v3, p1}, Lcom/konka/kkinterface/tv/PvrDesk;->getThumbnailDisplay(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/konka/tvsettings/popup/PVRThumbnail;->createThumbnailImage(Ljava/lang/String;Ljava/lang/String;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PVRThumbnail;->adapter:Lcom/konka/tvsettings/popup/PVRThumbnail$ThumbnailAdapter;

    invoke-virtual {v1}, Lcom/konka/tvsettings/popup/PVRThumbnail$ThumbnailAdapter;->notifyDataSetChanged()V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public isShown()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/popup/PVRThumbnail;->isShown:Z

    return v0
.end method

.method abstract onItemClicked(I)V
.end method

.method public updateThumbnail()Z
    .locals 2

    :try_start_0
    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PVRThumbnail;->constructImages()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PVRThumbnail;->adapter:Lcom/konka/tvsettings/popup/PVRThumbnail$ThumbnailAdapter;

    invoke-virtual {v1}, Lcom/konka/tvsettings/popup/PVRThumbnail$ThumbnailAdapter;->notifyDataSetChanged()V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method
