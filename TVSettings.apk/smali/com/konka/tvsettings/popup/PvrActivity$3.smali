.class Lcom/konka/tvsettings/popup/PvrActivity$3;
.super Lcom/konka/tvsettings/function/USBDiskSelecter;
.source "PvrActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/popup/PvrActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/PvrActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/popup/PvrActivity;Landroid/content/Context;)V
    .locals 0
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/konka/tvsettings/popup/PvrActivity$3;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    invoke-direct {p0, p2}, Lcom/konka/tvsettings/function/USBDiskSelecter;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onItemCancel()V
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity$3;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    invoke-virtual {v0}, Lcom/konka/tvsettings/popup/PvrActivity;->finish()V

    return-void
.end method

.method public onItemChosen(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity$3;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    invoke-static {v3, p3}, Lcom/konka/tvsettings/popup/PvrActivity;->access$23(Lcom/konka/tvsettings/popup/PvrActivity;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity$3;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    invoke-static {v3, p2}, Lcom/konka/tvsettings/popup/PvrActivity;->access$24(Lcom/konka/tvsettings/popup/PvrActivity;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity$3;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->usbLabel:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PvrActivity;->access$25(Lcom/konka/tvsettings/popup/PvrActivity;)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v3, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v3, "PVR"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "=============>>>>> current Selected Disk = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/konka/tvsettings/popup/PvrActivity$3;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->recordDiskPath:Ljava/lang/String;
    invoke-static {v5}, Lcom/konka/tvsettings/popup/PvrActivity;->access$26(Lcom/konka/tvsettings/popup/PvrActivity;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity$3;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->isOneTouchPauseMode:Z
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PvrActivity;->access$27(Lcom/konka/tvsettings/popup/PvrActivity;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity$3;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->isAlwaysTimeShiftBegin:Z
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PvrActivity;->access$28(Lcom/konka/tvsettings/popup/PvrActivity;)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_0
    const-string v1, "FAT"

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity$3;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->recordDiskLable:Ljava/lang/String;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PvrActivity;->access$29(Lcom/konka/tvsettings/popup/PvrActivity;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x6

    const/4 v5, 0x0

    const/4 v6, 0x3

    invoke-virtual {v3, v4, v1, v5, v6}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity$3;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    const/4 v4, 0x1

    # invokes: Lcom/konka/tvsettings/popup/PvrActivity;->doPVRTimeShift(Z)V
    invoke-static {v3, v4}, Lcom/konka/tvsettings/popup/PvrActivity;->access$30(Lcom/konka/tvsettings/popup/PvrActivity;Z)V

    new-instance v3, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    iget-object v4, p0, Lcom/konka/tvsettings/popup/PvrActivity$3;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;)V

    invoke-virtual {v3}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->start()V

    :cond_1
    :goto_0
    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity$3;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    const/4 v4, 0x1

    # invokes: Lcom/konka/tvsettings/popup/PvrActivity;->saveChooseDiskSettings(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v3, v4, p3, p2, p4}, Lcom/konka/tvsettings/popup/PvrActivity;->access$33(Lcom/konka/tvsettings/popup/PvrActivity;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_2
    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity$3;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->activity:Landroid/app/Activity;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PvrActivity;->access$31(Lcom/konka/tvsettings/popup/PvrActivity;)Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f0a018a    # com.konka.tvsettings.R.string.str_pvr_unsurpt_flsystem

    const/16 v5, 0x1f4

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :cond_3
    :try_start_1
    const-string v1, "FAT"

    const-string v2, "NTFS"

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity$3;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->recordDiskLable:Ljava/lang/String;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PvrActivity;->access$29(Lcom/konka/tvsettings/popup/PvrActivity;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x6

    const/4 v5, 0x0

    const/4 v6, 0x3

    invoke-virtual {v3, v4, v1, v5, v6}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity$3;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    const/4 v4, 0x1

    # invokes: Lcom/konka/tvsettings/popup/PvrActivity;->doPVRRecord(Z)V
    invoke-static {v3, v4}, Lcom/konka/tvsettings/popup/PvrActivity;->access$32(Lcom/konka/tvsettings/popup/PvrActivity;Z)V

    const-string v3, "PVR ========>>>USBDiskSelecter PlayBackProgress "

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v3, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    iget-object v4, p0, Lcom/konka/tvsettings/popup/PvrActivity$3;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;)V

    invoke-virtual {v3}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->start()V

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity$3;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->recordDiskLable:Ljava/lang/String;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PvrActivity;->access$29(Lcom/konka/tvsettings/popup/PvrActivity;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x6

    const/4 v5, 0x0

    const/4 v6, 0x4

    invoke-virtual {v3, v4, v2, v5, v6}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/konka/tvsettings/popup/PvrActivity$3;->noDismiss:Z

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity$3;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->activity:Landroid/app/Activity;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PvrActivity;->access$31(Lcom/konka/tvsettings/popup/PvrActivity;)Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f0a018a    # com.konka.tvsettings.R.string.str_pvr_unsurpt_flsystem

    const/16 v5, 0x1f4

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
