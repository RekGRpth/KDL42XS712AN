.class Lcom/konka/tvsettings/popup/PvrActivity$22;
.super Ljava/lang/Object;
.source "PvrActivity.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/popup/PvrActivity;->createAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private count:I

.field private strName:Ljava/lang/String;

.field private text:Landroid/widget/TextView;

.field private text2:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/konka/tvsettings/popup/PvrActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/popup/PvrActivity;)V
    .locals 1

    iput-object p1, p0, Lcom/konka/tvsettings/popup/PvrActivity$22;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/tvsettings/popup/PvrActivity$22;->count:I

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->recordingText:Landroid/widget/TextView;
    invoke-static {p1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$47(Lcom/konka/tvsettings/popup/PvrActivity;)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity$22;->text:Landroid/widget/TextView;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->recordingText:Landroid/widget/TextView;
    invoke-static {p1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$47(Lcom/konka/tvsettings/popup/PvrActivity;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity$22;->strName:Ljava/lang/String;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;
    invoke-static {p1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$48(Lcom/konka/tvsettings/popup/PvrActivity;)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity$22;->text2:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1    # Landroid/animation/Animator;

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 0
    .param p1    # Landroid/animation/Animator;

    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 4
    .param p1    # Landroid/animation/Animator;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$22;->strName:Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/konka/tvsettings/popup/PvrActivity$22;->count:I

    add-int/lit8 v2, v2, 0x1

    if-lt v0, v2, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity$22;->text:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v2, p0, Lcom/konka/tvsettings/popup/PvrActivity$22;->count:I

    add-int/lit8 v2, v2, 0x1

    rem-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/konka/tvsettings/popup/PvrActivity$22;->count:I

    return-void

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 3
    .param p1    # Landroid/animation/Animator;

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity$22;->text:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity$22;->strName:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
