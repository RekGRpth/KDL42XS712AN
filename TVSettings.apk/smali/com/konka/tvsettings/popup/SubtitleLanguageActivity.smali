.class public Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;
.super Lcom/konka/tvsettings/teletext/MstarBaseActivity;
.source "SubtitleLanguageActivity.java"


# static fields
.field private static subtitlePos:I


# instance fields
.field private adapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private subtitleLanguageListView:Landroid/widget/ListView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;->subtitlePos:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;->adapter:Landroid/widget/ArrayAdapter;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;->subtitleLanguageListView:Landroid/widget/ListView;

    return-void
.end method

.method static synthetic access$0(I)V
    .locals 0

    sput p0, Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;->subtitlePos:I

    return-void
.end method

.method static synthetic access$1()I
    .locals 1

    sget v0, Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;->subtitlePos:I

    return v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    const/4 v8, 0x0

    invoke-super {p0, p1}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onCreate(Landroid/os/Bundle;)V

    const v6, 0x7f03004f    # com.konka.tvsettings.R.layout.subtitle_language

    invoke-virtual {p0, v6}, Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;->setContentView(I)V

    const v6, 0x7f0701f4    # com.konka.tvsettings.R.id.subtitle_language_list_view

    invoke-virtual {p0, v6}, Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ListView;

    iput-object v6, p0, Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;->subtitleLanguageListView:Landroid/widget/ListView;

    new-instance v4, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;

    invoke-direct {v4}, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;-><init>()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/TVRootApp;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->getSubtitleInfo()Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;

    move-result-object v4

    iget-short v6, v4, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->subtitleServiceNumber:S

    if-lez v6, :cond_0

    iget-short v6, v4, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->subtitleServiceNumber:S

    add-int/lit8 v6, v6, 0x1

    new-array v5, v6, [Ljava/lang/String;

    const-string v6, "CLOSE"

    aput-object v6, v5, v8

    const/4 v2, 0x0

    :goto_0
    iget-short v6, v4, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->subtitleServiceNumber:S

    if-lt v2, v6, :cond_1

    new-instance v6, Landroid/widget/ArrayAdapter;

    const v7, 0x7f030041    # com.konka.tvsettings.R.layout.pvr_menu_info_list_view_item

    invoke-direct {v6, p0, v7, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object v6, p0, Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;->adapter:Landroid/widget/ArrayAdapter;

    iget-object v6, p0, Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;->subtitleLanguageListView:Landroid/widget/ListView;

    iget-object v7, p0, Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;->adapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v6, v7}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v6, p0, Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;->subtitleLanguageListView:Landroid/widget/ListView;

    invoke-virtual {v6, v8}, Landroid/widget/ListView;->setDividerHeight(I)V

    iget-object v6, p0, Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;->subtitleLanguageListView:Landroid/widget/ListView;

    new-instance v7, Lcom/konka/tvsettings/popup/SubtitleLanguageActivity$1;

    invoke-direct {v7, p0}, Lcom/konka/tvsettings/popup/SubtitleLanguageActivity$1;-><init>(Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;)V

    invoke-virtual {v6, v7}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    :cond_0
    return-void

    :cond_1
    add-int/lit8 v6, v2, 0x1

    iget-object v7, v4, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->subtitleServices:[Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;

    aget-object v7, v7, v2

    invoke-virtual {v7}, Lcom/mstar/android/tvapi/dtv/vo/MenuSubtitleService;->getLanguage()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    move-result-object v7

    invoke-virtual {v7}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->name()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    add-int/lit8 v6, v2, 0x1

    int-to-short v2, v6

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    :sswitch_0
    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;->finish()V

    const/4 v0, 0x1

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x102 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onResume()V
    .locals 3

    const/4 v2, 0x0

    const-string v1, "TvSetting"

    invoke-virtual {p0, v1, v2}, Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "subtitlePos"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    sput v1, Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;->subtitlePos:I

    iget-object v1, p0, Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;->subtitleLanguageListView:Landroid/widget/ListView;

    sget v2, Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;->subtitlePos:I

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setSelection(I)V

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onResume()V

    return-void
.end method
