.class public Lcom/konka/tvsettings/popup/SourceInfoActivity;
.super Landroid/app/Activity;
.source "SourceInfoActivity.java"


# static fields
.field private static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$AtvSystemStandard$EnumAtvSystemStandard:[I = null

.field private static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumAtvAudioModeType:[I = null

.field private static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumAvdVideoStandardType:[I = null

.field private static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumVideoType:[I = null

.field private static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I = null

.field private static final INFODISPLAY_TIMEOUT:I = 0x4b5

.field private static final REFRESH_TEXT:I = 0x7b


# instance fields
.field private DTVcurrentTime:Landroid/widget/TextView;

.field private IsSourceInfofirstOnCreate:Z

.field private age:I

.field private bCheckisAustralia:Z

.field private broadcastReceiver:Landroid/content/BroadcastReceiver;

.field private channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

.field private channelNum:I

.field private commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

.field private currentTime:Landroid/widget/TextView;

.field private dtveitinfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;

.field private epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

.field private mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

.field private mViewRlATVMoreInfo:Landroid/widget/RelativeLayout;

.field private mViewRlDTVMoreInfo:Landroid/widget/RelativeLayout;

.field private mViewRlDtvInfo:Landroid/widget/RelativeLayout;

.field private mViewRlNoDtvInfo:Landroid/widget/RelativeLayout;

.field private mViewTvDTVCurrName:Landroid/widget/TextView;

.field private mViewTvDTVCurrTime:Landroid/widget/TextView;

.field private mViewTvDTVDate:Landroid/widget/TextView;

.field private mViewTvDTVName:Landroid/widget/TextView;

.field private mViewTvDTVNextName:Landroid/widget/TextView;

.field private mViewTvDTVNextTime:Landroid/widget/TextView;

.field private mViewTvDTVNum:Landroid/widget/TextView;

.field private mViewTvInfo:Landroid/widget/TextView;

.field private m_InfoDisplayTimer:Lcom/konka/tvsettings/common/CountDownTimer;

.field private m_bIsCloseTimer:Z

.field private meInputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field private miCurCountry:I

.field private miCurrChannelNum:I

.field private mstrText:Ljava/lang/String;

.field private myHandler:Landroid/os/Handler;

.field private rootApp:Lcom/konka/tvsettings/TVRootApp;

.field private rootmenuHandler:Lcom/konka/tvsettings/RootActivity$RootMenuHandler;

.field private serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

.field private settingDesk:Lcom/konka/kkinterface/tv/SettingDesk;

.field private source_info_DTVname:Landroid/widget/TextView;

.field private source_info_DTVnumber:Landroid/widget/TextView;

.field private source_info_Subtitle:Landroid/widget/TextView;

.field private source_info_age:Landroid/widget/TextView;

.field private source_info_audio_format:Landroid/widget/TextView;

.field private source_info_audioformat:Landroid/widget/TextView;

.field private source_info_description:Landroid/widget/TextView;

.field private source_info_digital_TV:Landroid/widget/TextView;

.field private source_info_imageformat:Landroid/widget/TextView;

.field private source_info_language:Landroid/widget/TextView;

.field private source_info_mheg5:Landroid/widget/TextView;

.field private source_info_program_name:Landroid/widget/TextView;

.field private source_info_program_period:Landroid/widget/TextView;

.field private source_info_program_type:Landroid/widget/TextView;

.field private source_info_soundformat:Landroid/widget/TextView;

.field private source_info_teletext:Landroid/widget/TextView;

.field private source_info_tvname:Landroid/widget/TextView;

.field private source_info_tvnumber:Landroid/widget/TextView;

.field private source_info_video_format:Landroid/widget/TextView;

.field private str:Ljava/lang/String;

.field private str_video_info:Ljava/lang/String;


# direct methods
.method static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$AtvSystemStandard$EnumAtvSystemStandard()[I
    .locals 3

    sget-object v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$AtvSystemStandard$EnumAtvSystemStandard:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->values()[Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->E_BG:Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_5

    :goto_1
    :try_start_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->E_DK:Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_4

    :goto_2
    :try_start_2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->E_I:Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_3

    :goto_3
    :try_start_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->E_L:Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_2

    :goto_4
    :try_start_4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->E_M:Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_1

    :goto_5
    :try_start_5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->E_NUM:Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_0

    :goto_6
    sput-object v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$AtvSystemStandard$EnumAtvSystemStandard:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_6

    :catch_1
    move-exception v1

    goto :goto_5

    :catch_2
    move-exception v1

    goto :goto_4

    :catch_3
    move-exception v1

    goto :goto_3

    :catch_4
    move-exception v1

    goto :goto_2

    :catch_5
    move-exception v1

    goto :goto_1
.end method

.method static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumAtvAudioModeType()[I
    .locals 3

    sget-object v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumAtvAudioModeType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_DUAL_A:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_13

    :goto_1
    :try_start_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_DUAL_AB:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_12

    :goto_2
    :try_start_2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_DUAL_B:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_11

    :goto_3
    :try_start_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_FORCED_MONO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_10

    :goto_4
    :try_start_4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_G_STEREO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_f

    :goto_5
    :try_start_5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_HIDEV_MONO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_e

    :goto_6
    :try_start_6
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_INVALID:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_d

    :goto_7
    :try_start_7
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_K_STEREO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_c

    :goto_8
    :try_start_8
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_LEFT_LEFT:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_b

    :goto_9
    :try_start_9
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_LEFT_RIGHT:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/16 v2, 0x13

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_a

    :goto_a
    :try_start_a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_MONO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_9

    :goto_b
    :try_start_b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_MONO_SAP:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_8

    :goto_c
    :try_start_c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_NICAM_DUAL_A:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_7

    :goto_d
    :try_start_d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_NICAM_DUAL_AB:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_6

    :goto_e
    :try_start_e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_NICAM_DUAL_B:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_5

    :goto_f
    :try_start_f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_NICAM_MONO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_4

    :goto_10
    :try_start_10
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_NICAM_STEREO:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_3

    :goto_11
    :try_start_11
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_NUM:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/16 v2, 0x14

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_2

    :goto_12
    :try_start_12
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_RIGHT_RIGHT:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/16 v2, 0x12

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_1

    :goto_13
    :try_start_13
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->E_ATV_AUDIOMODE_STEREO_SAP:Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_0

    :goto_14
    sput-object v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumAtvAudioModeType:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_14

    :catch_1
    move-exception v1

    goto :goto_13

    :catch_2
    move-exception v1

    goto :goto_12

    :catch_3
    move-exception v1

    goto :goto_11

    :catch_4
    move-exception v1

    goto :goto_10

    :catch_5
    move-exception v1

    goto :goto_f

    :catch_6
    move-exception v1

    goto :goto_e

    :catch_7
    move-exception v1

    goto :goto_d

    :catch_8
    move-exception v1

    goto :goto_c

    :catch_9
    move-exception v1

    goto :goto_b

    :catch_a
    move-exception v1

    goto :goto_a

    :catch_b
    move-exception v1

    goto/16 :goto_9

    :catch_c
    move-exception v1

    goto/16 :goto_8

    :catch_d
    move-exception v1

    goto/16 :goto_7

    :catch_e
    move-exception v1

    goto/16 :goto_6

    :catch_f
    move-exception v1

    goto/16 :goto_5

    :catch_10
    move-exception v1

    goto/16 :goto_4

    :catch_11
    move-exception v1

    goto/16 :goto_3

    :catch_12
    move-exception v1

    goto/16 :goto_2

    :catch_13
    move-exception v1

    goto/16 :goto_1
.end method

.method static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumAvdVideoStandardType()[I
    .locals 3

    sget-object v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumAvdVideoStandardType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->AUTO:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_9

    :goto_1
    :try_start_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->MAX:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_8

    :goto_2
    :try_start_2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->NOTSTANDARD:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_7

    :goto_3
    :try_start_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->NTSC_44:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_6

    :goto_4
    :try_start_4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->NTSC_M:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_5

    :goto_5
    :try_start_5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->PAL_60:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_4

    :goto_6
    :try_start_6
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->PAL_BGHI:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_3

    :goto_7
    :try_start_7
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->PAL_M:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_2

    :goto_8
    :try_start_8
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->PAL_N:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_1

    :goto_9
    :try_start_9
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->SECAM:Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_0

    :goto_a
    sput-object v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumAvdVideoStandardType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_a

    :catch_1
    move-exception v1

    goto :goto_9

    :catch_2
    move-exception v1

    goto :goto_8

    :catch_3
    move-exception v1

    goto :goto_7

    :catch_4
    move-exception v1

    goto :goto_6

    :catch_5
    move-exception v1

    goto :goto_5

    :catch_6
    move-exception v1

    goto :goto_4

    :catch_7
    move-exception v1

    goto :goto_3

    :catch_8
    move-exception v1

    goto :goto_2

    :catch_9
    move-exception v1

    goto :goto_1
.end method

.method static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumVideoType()[I
    .locals 3

    sget-object v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumVideoType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumVideoType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumVideoType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumVideoType;->E_VIDEOTYPE_AVS:Lcom/mstar/android/tvapi/common/vo/EnumVideoType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumVideoType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_1
    :try_start_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumVideoType;->E_VIDEOTYPE_H264:Lcom/mstar/android/tvapi/common/vo/EnumVideoType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumVideoType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_2
    :try_start_2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumVideoType;->E_VIDEOTYPE_MPEG:Lcom/mstar/android/tvapi/common/vo/EnumVideoType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumVideoType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    :try_start_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumVideoType;->E_VIDEOTYPE_NONE:Lcom/mstar/android/tvapi/common/vo/EnumVideoType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumVideoType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_4
    :try_start_4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumVideoType;->E_VIDEOTYPE_VC1:Lcom/mstar/android/tvapi/common/vo/EnumVideoType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumVideoType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_5
    sput-object v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumVideoType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method

.method static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource()[I
    .locals 3

    sget-object v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_28

    :goto_1
    :try_start_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_27

    :goto_2
    :try_start_2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_26

    :goto_3
    :try_start_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_25

    :goto_4
    :try_start_4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_24

    :goto_5
    :try_start_5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS5:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_23

    :goto_6
    :try_start_6
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS6:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_22

    :goto_7
    :try_start_7
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS7:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_21

    :goto_8
    :try_start_8
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS8:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_20

    :goto_9
    :try_start_9
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_1f

    :goto_a
    :try_start_a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1d

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_1e

    :goto_b
    :try_start_b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x26

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_1d

    :goto_c
    :try_start_c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1e

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_1c

    :goto_d
    :try_start_d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1f

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_1b

    :goto_e
    :try_start_e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x20

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_1a

    :goto_f
    :try_start_f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x21

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_19

    :goto_10
    :try_start_10
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x22

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_18

    :goto_11
    :try_start_11
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x18

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_17

    :goto_12
    :try_start_12
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x19

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_16

    :goto_13
    :try_start_13
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1a

    aput v2, v0, v1
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_15

    :goto_14
    :try_start_14
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1b

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_14} :catch_14

    :goto_15
    :try_start_15
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_HDMI_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x1c

    aput v2, v0, v1
    :try_end_15
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15 .. :try_end_15} :catch_13

    :goto_16
    :try_start_16
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_JPEG:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x25

    aput v2, v0, v1
    :try_end_16
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16 .. :try_end_16} :catch_12

    :goto_17
    :try_start_17
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_KTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x24

    aput v2, v0, v1
    :try_end_17
    .catch Ljava/lang/NoSuchFieldError; {:try_start_17 .. :try_end_17} :catch_11

    :goto_18
    :try_start_18
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x29

    aput v2, v0, v1
    :try_end_18
    .catch Ljava/lang/NoSuchFieldError; {:try_start_18 .. :try_end_18} :catch_10

    :goto_19
    :try_start_19
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NUM:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x28

    aput v2, v0, v1
    :try_end_19
    .catch Ljava/lang/NoSuchFieldError; {:try_start_19 .. :try_end_19} :catch_f

    :goto_1a
    :try_start_1a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SCART:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x15

    aput v2, v0, v1
    :try_end_1a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a .. :try_end_1a} :catch_e

    :goto_1b
    :try_start_1b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SCART2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x16

    aput v2, v0, v1
    :try_end_1b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1b .. :try_end_1b} :catch_d

    :goto_1c
    :try_start_1c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SCART_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x17

    aput v2, v0, v1
    :try_end_1c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1c .. :try_end_1c} :catch_c

    :goto_1d
    :try_start_1d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x23

    aput v2, v0, v1
    :try_end_1d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1d .. :try_end_1d} :catch_b

    :goto_1e
    :try_start_1e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x27

    aput v2, v0, v1
    :try_end_1e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1e .. :try_end_1e} :catch_a

    :goto_1f
    :try_start_1f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_1f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1f .. :try_end_1f} :catch_9

    :goto_20
    :try_start_20
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_20
    .catch Ljava/lang/NoSuchFieldError; {:try_start_20 .. :try_end_20} :catch_8

    :goto_21
    :try_start_21
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_21
    .catch Ljava/lang/NoSuchFieldError; {:try_start_21 .. :try_end_21} :catch_7

    :goto_22
    :try_start_22
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_22
    .catch Ljava/lang/NoSuchFieldError; {:try_start_22 .. :try_end_22} :catch_6

    :goto_23
    :try_start_23
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_SVIDEO_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_23
    .catch Ljava/lang/NoSuchFieldError; {:try_start_23 .. :try_end_23} :catch_5

    :goto_24
    :try_start_24
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_VGA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_24
    .catch Ljava/lang/NoSuchFieldError; {:try_start_24 .. :try_end_24} :catch_4

    :goto_25
    :try_start_25
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_25
    .catch Ljava/lang/NoSuchFieldError; {:try_start_25 .. :try_end_25} :catch_3

    :goto_26
    :try_start_26
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x12

    aput v2, v0, v1
    :try_end_26
    .catch Ljava/lang/NoSuchFieldError; {:try_start_26 .. :try_end_26} :catch_2

    :goto_27
    :try_start_27
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR3:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x13

    aput v2, v0, v1
    :try_end_27
    .catch Ljava/lang/NoSuchFieldError; {:try_start_27 .. :try_end_27} :catch_1

    :goto_28
    :try_start_28
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_YPBPR_MAX:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    const/16 v2, 0x14

    aput v2, v0, v1
    :try_end_28
    .catch Ljava/lang/NoSuchFieldError; {:try_start_28 .. :try_end_28} :catch_0

    :goto_29
    sput-object v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_29

    :catch_1
    move-exception v1

    goto :goto_28

    :catch_2
    move-exception v1

    goto :goto_27

    :catch_3
    move-exception v1

    goto :goto_26

    :catch_4
    move-exception v1

    goto :goto_25

    :catch_5
    move-exception v1

    goto :goto_24

    :catch_6
    move-exception v1

    goto :goto_23

    :catch_7
    move-exception v1

    goto :goto_22

    :catch_8
    move-exception v1

    goto :goto_21

    :catch_9
    move-exception v1

    goto :goto_20

    :catch_a
    move-exception v1

    goto :goto_1f

    :catch_b
    move-exception v1

    goto/16 :goto_1e

    :catch_c
    move-exception v1

    goto/16 :goto_1d

    :catch_d
    move-exception v1

    goto/16 :goto_1c

    :catch_e
    move-exception v1

    goto/16 :goto_1b

    :catch_f
    move-exception v1

    goto/16 :goto_1a

    :catch_10
    move-exception v1

    goto/16 :goto_19

    :catch_11
    move-exception v1

    goto/16 :goto_18

    :catch_12
    move-exception v1

    goto/16 :goto_17

    :catch_13
    move-exception v1

    goto/16 :goto_16

    :catch_14
    move-exception v1

    goto/16 :goto_15

    :catch_15
    move-exception v1

    goto/16 :goto_14

    :catch_16
    move-exception v1

    goto/16 :goto_13

    :catch_17
    move-exception v1

    goto/16 :goto_12

    :catch_18
    move-exception v1

    goto/16 :goto_11

    :catch_19
    move-exception v1

    goto/16 :goto_10

    :catch_1a
    move-exception v1

    goto/16 :goto_f

    :catch_1b
    move-exception v1

    goto/16 :goto_e

    :catch_1c
    move-exception v1

    goto/16 :goto_d

    :catch_1d
    move-exception v1

    goto/16 :goto_c

    :catch_1e
    move-exception v1

    goto/16 :goto_b

    :catch_1f
    move-exception v1

    goto/16 :goto_a

    :catch_20
    move-exception v1

    goto/16 :goto_9

    :catch_21
    move-exception v1

    goto/16 :goto_8

    :catch_22
    move-exception v1

    goto/16 :goto_7

    :catch_23
    move-exception v1

    goto/16 :goto_6

    :catch_24
    move-exception v1

    goto/16 :goto_5

    :catch_25
    move-exception v1

    goto/16 :goto_4

    :catch_26
    move-exception v1

    goto/16 :goto_3

    :catch_27
    move-exception v1

    goto/16 :goto_2

    :catch_28
    move-exception v1

    goto/16 :goto_1
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->DTVcurrentTime:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->currentTime:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str_video_info:Ljava/lang/String;

    iput v2, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->miCurrChannelNum:I

    iput-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->rootApp:Lcom/konka/tvsettings/TVRootApp;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->rootmenuHandler:Lcom/konka/tvsettings/RootActivity$RootMenuHandler;

    iput-boolean v2, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->m_bIsCloseTimer:Z

    iput-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->broadcastReceiver:Landroid/content/BroadcastReceiver;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->m_InfoDisplayTimer:Lcom/konka/tvsettings/common/CountDownTimer;

    iput-boolean v1, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->IsSourceInfofirstOnCreate:Z

    iput v1, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->age:I

    iput v1, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->miCurCountry:I

    iput-boolean v1, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->bCheckisAustralia:Z

    new-instance v0, Lcom/konka/tvsettings/popup/SourceInfoActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/popup/SourceInfoActivity$1;-><init>(Lcom/konka/tvsettings/popup/SourceInfoActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->myHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/popup/SourceInfoActivity;)Lcom/konka/kkinterface/tv/CommonDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/popup/SourceInfoActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->openAlTimeShift()V

    return-void
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/popup/SourceInfoActivity;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/popup/SourceInfoActivity;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->meInputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    return-void
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/popup/SourceInfoActivity;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->meInputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/popup/SourceInfoActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mViewTvInfo:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/tvsettings/popup/SourceInfoActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$7(Lcom/konka/tvsettings/popup/SourceInfoActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->updateChannelInfo()V

    return-void
.end method

.method private checkSystemAutoTime(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V
    .locals 6
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    const/4 v5, 0x1

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/SettingDesk;->GetSystemAutoTimeType()I

    move-result v2

    :try_start_0
    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "auto_time"

    invoke-static {v3, v4}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/provider/Settings$SettingNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne p1, v3, :cond_1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "auto_time"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    const-string v3, "TVSetting check"

    const-string v4, "set auto time to 0"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq p1, v3, :cond_0

    if-ne v2, v5, :cond_0

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "auto_time"

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    const-string v3, "TVSetting check"

    const-string v4, "set auto time to 1"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private cutLongString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    const/4 v0, 0x0

    const/16 v1, 0xf

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method private getATVSoundFormat()Ljava/lang/String;
    .locals 4

    const-string v1, ""

    iget-object v2, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v0

    invoke-static {}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumAtvAudioModeType()[I

    move-result-object v2

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/SoundDesk;->getAtvMtsMode()Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    const-string v1, "UNKNOWN"

    :goto_0
    return-object v1

    :pswitch_0
    const-string v1, "MONO"

    goto :goto_0

    :pswitch_1
    const-string v1, "DUAL A"

    goto :goto_0

    :pswitch_2
    const-string v1, "DUAL AB"

    goto :goto_0

    :pswitch_3
    const-string v1, "DUAL B"

    goto :goto_0

    :pswitch_4
    const-string v1, "MONO"

    goto :goto_0

    :pswitch_5
    const-string v1, "STEREO"

    goto :goto_0

    :pswitch_6
    const-string v1, "HIDEV MONO"

    goto :goto_0

    :pswitch_7
    const-string v1, "UNKNOWN"

    goto :goto_0

    :pswitch_8
    const-string v1, "STEREO"

    goto :goto_0

    :pswitch_9
    const-string v1, "LEFT LEFT"

    goto :goto_0

    :pswitch_a
    const-string v1, "LEFT RIGHT"

    goto :goto_0

    :pswitch_b
    const-string v1, "MONO SAP"

    goto :goto_0

    :pswitch_c
    const-string v1, "DUAL A"

    goto :goto_0

    :pswitch_d
    const-string v1, "DUAL AB"

    goto :goto_0

    :pswitch_e
    const-string v1, "DUAL B"

    goto :goto_0

    :pswitch_f
    const-string v1, "NICAM MONO"

    goto :goto_0

    :pswitch_10
    const-string v1, "NICAM STEREO"

    goto :goto_0

    :pswitch_11
    const-string v1, "RIGHT RIGHT"

    goto :goto_0

    :pswitch_12
    const-string v1, "STEREO SAP"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_8
        :pswitch_b
        :pswitch_12
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_f
        :pswitch_10
        :pswitch_c
        :pswitch_e
        :pswitch_d
        :pswitch_6
        :pswitch_9
        :pswitch_11
        :pswitch_a
        :pswitch_7
    .end packed-switch
.end method

.method private getCurProgrameName()Ljava/lang/String;
    .locals 6

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v4

    iget-short v2, v4, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrentChannelNumber()I

    move-result v1

    sget-object v3, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->E_SERVICETYPE_ATV:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v4, v5, :cond_0

    sget-object v4, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->E_SERVICETYPE_RADIO:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    invoke-virtual {v4}, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->ordinal()I

    move-result v4

    if-ne v2, v4, :cond_1

    sget-object v3, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->E_SERVICETYPE_RADIO:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v1, v3, v5}, Lcom/konka/kkinterface/tv/ChannelDesk;->getProgramName(ILcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;S)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    sget-object v3, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->E_SERVICETYPE_DTV:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    goto :goto_0
.end method

.method private openAlTimeShift()V
    .locals 4

    iget-object v2, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->meInputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->meInputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->isSignalStabled()Z

    move-result v2

    if-eqz v2, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->settingDesk:Lcom/konka/kkinterface/tv/SettingDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/SettingDesk;->getAlTimeShift()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/PvrManager;->isTimeShiftRecording()Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/konka/tvsettings/popup/PvrActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "PVR_AlTimeShift_Call"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private showTVNum(I)V
    .locals 3
    .param p1    # I

    const-string v0, ""

    const/16 v1, 0xa

    if-ge p1, v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "00"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mViewTvDTVNum:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    const/16 v1, 0x64

    if-ge p1, v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "0"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private showTime(Landroid/text/format/Time;Landroid/text/format/Time;Landroid/widget/TextView;)V
    .locals 7
    .param p1    # Landroid/text/format/Time;
    .param p2    # Landroid/text/format/Time;
    .param p3    # Landroid/widget/TextView;

    const/16 v6, 0x9

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x0

    iget v4, p1, Landroid/text/format/Time;->hour:I

    if-gt v4, v6, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "0"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p1, Landroid/text/format/Time;->hour:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_0
    iget v4, p1, Landroid/text/format/Time;->minute:I

    if-gt v4, v6, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "0"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p1, Landroid/text/format/Time;->minute:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_1
    iget v4, p2, Landroid/text/format/Time;->hour:I

    if-gt v4, v6, :cond_2

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "0"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p2, Landroid/text/format/Time;->hour:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    iget v4, p2, Landroid/text/format/Time;->minute:I

    if-gt v4, v6, :cond_3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "0"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p2, Landroid/text/format/Time;->minute:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, p1, Landroid/text/format/Time;->hour:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, p1, Landroid/text/format/Time;->minute:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, p2, Landroid/text/format/Time;->hour:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, p2, Landroid/text/format/Time;->minute:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_3
.end method

.method private showYear(Landroid/text/format/Time;)V
    .locals 7
    .param p1    # Landroid/text/format/Time;

    const/16 v6, 0xa

    const-string v3, ""

    const-string v0, ""

    const-string v1, ""

    const-string v2, ""

    iget v4, p1, Landroid/text/format/Time;->month:I

    add-int/lit8 v4, v4, 0x1

    if-ge v4, v6, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "0"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p1, Landroid/text/format/Time;->month:I

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_0
    iget v4, p1, Landroid/text/format/Time;->monthDay:I

    if-ge v4, v6, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "0"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p1, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    iget v4, p1, Landroid/text/format/Time;->hour:I

    if-ge v4, v6, :cond_2

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "0"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p1, Landroid/text/format/Time;->hour:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_2
    iget v4, p1, Landroid/text/format/Time;->minute:I

    if-ge v4, v6, :cond_3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "0"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p1, Landroid/text/format/Time;->minute:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_3
    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mViewTvDTVDate:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget v6, p1, Landroid/text/format/Time;->year:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "   "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, p1, Landroid/text/format/Time;->month:I

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, p1, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, p1, Landroid/text/format/Time;->hour:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    :cond_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v5, p1, Landroid/text/format/Time;->minute:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3
.end method

.method private updateChannelInfo()V
    .locals 22

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->meInputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->meInputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-object/from16 v18, v0

    sget-object v19, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_1

    const v18, 0x7f07013f    # com.konka.tvsettings.R.id.source_info_DTVtime

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->DTVcurrentTime:Landroid/widget/TextView;

    const v18, 0x7f07013c    # com.konka.tvsettings.R.id.source_info_DTVnumber

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_DTVnumber:Landroid/widget/TextView;

    const v18, 0x7f07013d    # com.konka.tvsettings.R.id.source_info_DTVname

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_DTVname:Landroid/widget/TextView;

    const v18, 0x7f07014a    # com.konka.tvsettings.R.id.source_info_teletext

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_teletext:Landroid/widget/TextView;

    const v18, 0x7f070141    # com.konka.tvsettings.R.id.source_info_program_name

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_program_name:Landroid/widget/TextView;

    const v18, 0x7f070146    # com.konka.tvsettings.R.id.source_info_Subtitle

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_Subtitle:Landroid/widget/TextView;

    const v18, 0x7f070143    # com.konka.tvsettings.R.id.source_info_mheg5

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_mheg5:Landroid/widget/TextView;

    const v18, 0x7f070144    # com.konka.tvsettings.R.id.source_info_video_format

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_video_format:Landroid/widget/TextView;

    const v18, 0x7f070145    # com.konka.tvsettings.R.id.source_info_audio_format

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_audio_format:Landroid/widget/TextView;

    const v18, 0x7f07013e    # com.konka.tvsettings.R.id.source_info_program_type

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_program_type:Landroid/widget/TextView;

    const v18, 0x7f070140    # com.konka.tvsettings.R.id.source_info_program_period

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_program_period:Landroid/widget/TextView;

    const v18, 0x7f07014c    # com.konka.tvsettings.R.id.source_info_description

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_description:Landroid/widget/TextView;

    const v18, 0x7f070142    # com.konka.tvsettings.R.id.source_info_digital_TV

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_digital_TV:Landroid/widget/TextView;

    const v18, 0x7f070147    # com.konka.tvsettings.R.id.source_info_language

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_language:Landroid/widget/TextView;

    const v18, 0x7f070148    # com.konka.tvsettings.R.id.source_info_age

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_age:Landroid/widget/TextView;

    :goto_0
    invoke-interface {v8}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrentChannelNumber()I

    move-result v18

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->miCurrChannelNum:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->meInputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-object/from16 v18, v0

    sget-object v19, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_18

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v13

    iget v0, v13, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    move/from16 v18, v0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->miCurrChannelNum:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_DTVnumber:Landroid/widget/TextView;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    const v20, 0x7f0a0030    # com.konka.tvsettings.R.string.str_program_channelno

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v20, " : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->miCurrChannelNum:I

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_DTVname:Landroid/widget/TextView;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    const v20, 0x7f0a0031    # com.konka.tvsettings.R.string.str_textview_record_chaneel_name

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v20, " : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-direct/range {p0 .. p0}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->getCurProgrameName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v5, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    invoke-direct {v5}, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;-><init>()V

    new-instance v9, Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;

    invoke-direct {v9}, Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;-><init>()V

    sget-object v18, Lcom/mstar/android/tvapi/common/vo/EnumProgramInfoType;->E_INFO_CURRENT:Lcom/mstar/android/tvapi/common/vo/EnumProgramInfoType;

    move-object/from16 v0, v18

    invoke-interface {v8, v9, v0}, Lcom/konka/kkinterface/tv/ChannelDesk;->getProgramInfo(Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;Lcom/mstar/android/tvapi/common/vo/EnumProgramInfoType;)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-object/from16 v18, v0

    invoke-interface/range {v18 .. v18}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getEpgManagerInstance()Lcom/konka/kkinterface/tv/EpgDesk;

    move-result-object v11

    new-instance v12, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;

    invoke-direct {v12}, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;-><init>()V

    new-instance v7, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;

    invoke-direct {v7}, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;-><init>()V

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/ChannelDesk;->getAudioInfo()Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;

    move-result-object v7

    :try_start_0
    iget-short v0, v5, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    move/from16 v18, v0

    iget v0, v5, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    move/from16 v19, v0

    const/16 v20, 0x1

    sget-object v21, Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgDescriptionType;->E_DETAIL_DESCRIPTION:Lcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgDescriptionType;

    move/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    move-object/from16 v3, v21

    invoke-interface {v11, v0, v1, v2, v3}, Lcom/konka/kkinterface/tv/EpgDesk;->getPresentFollowingEventInfo(SIZLcom/mstar/android/tvapi/dtv/vo/DtvType$EnumEpgDescriptionType;)Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v12

    if-nez v12, :cond_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    const v18, 0x7f070137    # com.konka.tvsettings.R.id.source_info_tvtime

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->currentTime:Landroid/widget/TextView;

    const v18, 0x7f070135    # com.konka.tvsettings.R.id.source_info_tvnumber

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_tvnumber:Landroid/widget/TextView;

    const v18, 0x7f070136    # com.konka.tvsettings.R.id.source_info_tvname

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_tvname:Landroid/widget/TextView;

    const v18, 0x7f070138    # com.konka.tvsettings.R.id.source_info_imageformat

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_imageformat:Landroid/widget/TextView;

    const v18, 0x7f07013a    # com.konka.tvsettings.R.id.source_info_soundformat

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_soundformat:Landroid/widget/TextView;

    const v18, 0x7f070139    # com.konka.tvsettings.R.id.source_info_audioformat

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/TextView;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_audioformat:Landroid/widget/TextView;

    goto/16 :goto_0

    :cond_2
    :try_start_1
    new-instance v18, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;

    invoke-direct/range {v18 .. v18}, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->dtveitinfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;

    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-interface {v11, v0}, Lcom/konka/kkinterface/tv/EpgDesk;->getEitInfo(Z)Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->dtveitinfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :goto_2
    iget v0, v5, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    move/from16 v18, v0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->miCurrChannelNum:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_program_name:Landroid/widget/TextView;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->dtveitinfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->eitCurrentEventPf:Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->eventName:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->bCheckisAustralia:Z

    move/from16 v18, v0

    if-eqz v18, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->dtveitinfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->eitCurrentEventPf:Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-short v6, v0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->parentalControl:S

    const-string v4, " "

    if-ltz v6, :cond_4

    const/16 v18, 0x4

    move/from16 v0, v18

    if-gt v6, v0, :cond_4

    const-string v4, "-"

    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_age:Landroid/widget/TextView;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    const v20, 0x7f0a0173    # com.konka.tvsettings.R.string.str_lock_parentalguidance

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v20, ":"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_4
    invoke-interface {v8}, Lcom/konka/kkinterface/tv/ChannelDesk;->getSubtitleInfo()Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_Subtitle:Landroid/widget/TextView;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    const v20, 0x7f0a0142    # com.konka.tvsettings.R.string.str_dtv_source_info_Subtitle

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v20, " : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    iget-short v0, v14, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->subtitleServiceNumber:S

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v12, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->componentInfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-boolean v0, v0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->teletextService:Z

    move/from16 v18, v0

    if-eqz v18, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_teletext:Landroid/widget/TextView;

    move-object/from16 v18, v0

    const v19, 0x7f0a0144    # com.konka.tvsettings.R.string.str_dtv_source_info_teletext

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_5
    iget-object v0, v12, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->componentInfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-boolean v0, v0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->mheg5Service:Z

    move/from16 v18, v0

    if-eqz v18, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_mheg5:Landroid/widget/TextView;

    move-object/from16 v18, v0

    const v19, 0x7f0a0141    # com.konka.tvsettings.R.string.str_dtv_source_info_mheg5

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_6
    invoke-static {}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumVideoType()[I

    move-result-object v18

    iget-object v0, v12, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->componentInfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->getVideoType()Lcom/mstar/android/tvapi/common/vo/EnumVideoType;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/mstar/android/tvapi/common/vo/EnumVideoType;->ordinal()I

    move-result v19

    aget v18, v18, v19

    packed-switch v18, :pswitch_data_0

    const-string v18, ""

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_video_format:Landroid/widget/TextView;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    const v20, 0x7f0a013f    # com.konka.tvsettings.R.string.str_dtv_source_info_program_format

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v20, " : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v7, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;->audioInfos:[Lcom/mstar/android/tvapi/dtv/vo/AudioInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    array-length v0, v0

    move/from16 v18, v0

    if-eqz v18, :cond_0

    iget-object v0, v7, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;->audioInfos:[Lcom/mstar/android/tvapi/dtv/vo/AudioInfo;

    move-object/from16 v18, v0

    iget-short v0, v7, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;->currentAudioIndex:S

    move/from16 v19, v0

    aget-object v18, v18, v19

    move-object/from16 v0, v18

    iget-short v0, v0, Lcom/mstar/android/tvapi/dtv/vo/AudioInfo;->audioType:S

    move/from16 v18, v0

    packed-switch v18, :pswitch_data_1

    :pswitch_0
    const-string v18, ""

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    :goto_8
    iget-object v0, v12, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->componentInfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-boolean v0, v0, Lcom/mstar/android/tvapi/dtv/vo/DtvEventComponentInfo;->isAd:Z

    move/from16 v18, v0

    if-eqz v18, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v18, "  AD"

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_audio_format:Landroid/widget/TextView;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    const v20, 0x7f0a0140    # com.konka.tvsettings.R.string.str_dtv_source_info_audio_format

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v20, " : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-short v0, v5, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    move/from16 v18, v0

    sget-object v19, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->E_SERVICETYPE_DTV:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    invoke-virtual/range {v19 .. v19}, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->ordinal()I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_10

    const-string v18, "DTV"

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_program_type:Landroid/widget/TextView;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    const v20, 0x7f0a013e    # com.konka.tvsettings.R.string.str_dtv_source_info_program_type

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v20, " : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_program_period:Landroid/widget/TextView;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    const v20, 0x7f0a013c    # com.konka.tvsettings.R.string.str_dtv_source_info_program_period

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v20, " : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    iget-object v0, v12, Lcom/mstar/android/tvapi/common/vo/PresentFollowingEventInfo;->eventInfo:Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->durationTime:I

    move/from16 v20, v0

    div-int/lit8 v20, v20, 0x3c

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "Min"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->dtveitinfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->eitCurrentEventPf:Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->shortEventText:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v18

    if-lez v18, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->dtveitinfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->eitCurrentEventPf:Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->shortEventText:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    :goto_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-short v0, v0, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    move/from16 v18, v0

    if-lez v18, :cond_17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->enScanType:Lcom/konka/kkinterface/tv/CommonDesk$EN_SCAN_TYPE;

    move-object/from16 v18, v0

    sget-object v19, Lcom/konka/kkinterface/tv/CommonDesk$EN_SCAN_TYPE;->E_PROGRESSIVE:Lcom/konka/kkinterface/tv/CommonDesk$EN_SCAN_TYPE;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_16

    new-instance v18, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-short v0, v0, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    move/from16 v19, v0

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v19, "P"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str_video_info:Ljava/lang/String;

    :goto_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_description:Landroid/widget/TextView;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_digital_TV:Landroid/widget/TextView;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    const v20, 0x7f0a0147    # com.konka.tvsettings.R.string.str_dtv_source_info_resolution

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v20, " : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str_video_info:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_language:Landroid/widget/TextView;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    const v20, 0x7f0a0143    # com.konka.tvsettings.R.string.str_dtv_source_info_language

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v20, " : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    iget-short v0, v7, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;->audioLangNum:S

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_description:Landroid/widget/TextView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/TextView;->requestFocus()Z

    new-instance v18, Ljava/text/SimpleDateFormat;

    const-string v19, "HH:mm yyyy/MM/dd E"

    invoke-direct/range {v18 .. v19}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v19, Ljava/util/Date;

    invoke-direct/range {v19 .. v19}, Ljava/util/Date;-><init>()V

    invoke-virtual/range {v18 .. v19}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->DTVcurrentTime:Landroid/widget/TextView;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    const v20, 0x7f0a014a    # com.konka.tvsettings.R.string.str_time_time

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v20, " : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :catch_0
    move-exception v10

    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    :cond_4
    const/16 v18, 0x5

    move/from16 v0, v18

    if-gt v0, v6, :cond_5

    const/16 v18, 0x6

    move/from16 v0, v18

    if-gt v6, v0, :cond_5

    const-string v4, "P"

    goto/16 :goto_3

    :cond_5
    const/16 v18, 0x7

    move/from16 v0, v18

    if-gt v0, v6, :cond_6

    const/16 v18, 0x8

    move/from16 v0, v18

    if-gt v6, v0, :cond_6

    const-string v4, "C"

    goto/16 :goto_3

    :cond_6
    const/16 v18, 0x9

    move/from16 v0, v18

    if-gt v0, v6, :cond_7

    const/16 v18, 0xa

    move/from16 v0, v18

    if-gt v6, v0, :cond_7

    const-string v4, "G"

    goto/16 :goto_3

    :cond_7
    const/16 v18, 0xb

    move/from16 v0, v18

    if-gt v0, v6, :cond_8

    const/16 v18, 0xc

    move/from16 v0, v18

    if-gt v6, v0, :cond_8

    const-string v4, "PG"

    goto/16 :goto_3

    :cond_8
    const/16 v18, 0xd

    move/from16 v0, v18

    if-gt v0, v6, :cond_9

    const/16 v18, 0xe

    move/from16 v0, v18

    if-gt v6, v0, :cond_9

    const-string v4, "M"

    goto/16 :goto_3

    :cond_9
    const/16 v18, 0xf

    move/from16 v0, v18

    if-gt v0, v6, :cond_a

    const/16 v18, 0x10

    move/from16 v0, v18

    if-gt v6, v0, :cond_a

    const-string v4, "MA"

    goto/16 :goto_3

    :cond_a
    const/16 v18, 0x11

    move/from16 v0, v18

    if-ne v6, v0, :cond_b

    const-string v4, "AV"

    goto/16 :goto_3

    :cond_b
    const/16 v18, 0x12

    move/from16 v0, v18

    if-ne v6, v0, :cond_c

    const-string v4, "R"

    goto/16 :goto_3

    :cond_c
    const-string v4, "NO BLOCK"

    goto/16 :goto_3

    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_age:Landroid/widget/TextView;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    const v20, 0x7f0a017b    # com.konka.tvsettings.R.string.str_dtv_source_info_age

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v20, ":"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->dtveitinfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->eitCurrentEventPf:Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-short v0, v0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->parentalControl:S

    move/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    :cond_e
    const-string v18, ""

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_teletext:Landroid/widget/TextView;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    :cond_f
    const-string v18, ""

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_mheg5:Landroid/widget/TextView;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    :pswitch_1
    const-string v18, "MPEG"

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    goto/16 :goto_7

    :pswitch_2
    const-string v18, "H.264"

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    goto/16 :goto_7

    :pswitch_3
    const-string v18, "AVS"

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    goto/16 :goto_7

    :pswitch_4
    const-string v18, "VC1"

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    goto/16 :goto_7

    :pswitch_5
    const-string v18, "MPEG"

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    goto/16 :goto_8

    :pswitch_6
    const-string v18, "AC3"

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    goto/16 :goto_8

    :pswitch_7
    const-string v18, "AAC"

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    goto/16 :goto_8

    :pswitch_8
    const-string v18, "AC3P"

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    goto/16 :goto_8

    :cond_10
    iget-short v0, v5, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    move/from16 v18, v0

    sget-object v19, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->E_SERVICETYPE_RADIO:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    invoke-virtual/range {v19 .. v19}, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->ordinal()I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_11

    const-string v18, "RADIO"

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    goto/16 :goto_9

    :cond_11
    iget-short v0, v5, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    move/from16 v18, v0

    sget-object v19, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->E_SERVICETYPE_DATA:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    invoke-virtual/range {v19 .. v19}, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->ordinal()I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_12

    const-string v18, "DATA"

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    goto/16 :goto_9

    :cond_12
    const-string v18, ""

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    goto/16 :goto_9

    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->dtveitinfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->eitCurrentEventPf:Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->extendedEventText:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v18

    if-lez v18, :cond_14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->dtveitinfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->eitCurrentEventPf:Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->extendedEventText:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    goto/16 :goto_a

    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->dtveitinfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->eitCurrentEventPf:Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->extendedEventItem:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v18

    if-lez v18, :cond_15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->dtveitinfo:Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/mstar/android/tvapi/dtv/vo/DtvEitInfo;->eitCurrentEventPf:Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/mstar/android/tvapi/dtv/vo/EitCurrentEventPf;->extendedEventItem:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    goto/16 :goto_a

    :cond_15
    const-string v18, ""

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    goto/16 :goto_a

    :cond_16
    new-instance v18, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-short v0, v0, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    move/from16 v19, v0

    invoke-static/range {v19 .. v19}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v19, "I"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str_video_info:Ljava/lang/String;

    goto/16 :goto_b

    :cond_17
    const-string v18, ""

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str_video_info:Ljava/lang/String;

    goto/16 :goto_b

    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->meInputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-object/from16 v18, v0

    sget-object v19, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-ne v0, v1, :cond_1a

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v13

    if-nez v13, :cond_19

    const-string v18, "TvApp"

    const-string v19, "getProgramInfo Error"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_19
    iget v0, v13, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    move/from16 v18, v0

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->miCurrChannelNum:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_tvnumber:Landroid/widget/TextView;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    const v20, 0x7f0a0030    # com.konka.tvsettings.R.string.str_program_channelno

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v20, " : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->miCurrChannelNum:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, 0x1

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_tvname:Landroid/widget/TextView;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    const v20, 0x7f0a0031    # com.konka.tvsettings.R.string.str_textview_record_chaneel_name

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v20, " : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-direct/range {p0 .. p0}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->getCurProgrameName()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget-object v18, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramInfo;->E_GET_VIDEO_STANDARD_OF_PROGRAM:Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramInfo;

    iget v0, v13, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    move/from16 v19, v0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    move-object/from16 v3, v21

    invoke-interface {v8, v0, v1, v2, v3}, Lcom/konka/kkinterface/tv/ChannelDesk;->atvGetProgramInfo(Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramInfo;IILjava/lang/StringBuffer;)I

    move-result v16

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    move-result-object v18

    aget-object v17, v18, v16

    invoke-static {}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumAvdVideoStandardType()[I

    move-result-object v18

    invoke-virtual/range {v17 .. v17}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I

    move-result v19

    aget v18, v18, v19

    packed-switch v18, :pswitch_data_2

    const-string v18, ""

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    :goto_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_imageformat:Landroid/widget/TextView;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    const v20, 0x7f0a002d    # com.konka.tvsettings.R.string.str_program_searching_colorformat

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v20, " : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct/range {p0 .. p0}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->getATVSoundFormat()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_soundformat:Landroid/widget/TextView;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    const v20, 0x7f0a009d    # com.konka.tvsettings.R.string.str_sound_audio_choice

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$AtvSystemStandard$EnumAtvSystemStandard()[I

    move-result-object v18

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/ChannelDesk;->atvGetSoundSystem()Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->ordinal()I

    move-result v19

    aget v18, v18, v19

    packed-switch v18, :pswitch_data_3

    const-string v18, "BG"

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    :goto_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->source_info_audioformat:Landroid/widget/TextView;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    const v20, 0x7f0a002e    # com.konka.tvsettings.R.string.str_program_searching_soundformat

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v20, " : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v18, Ljava/text/SimpleDateFormat;

    const-string v19, "HH:mm yyyy/MM/dd E"

    invoke-direct/range {v18 .. v19}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v19, Ljava/util/Date;

    invoke-direct/range {v19 .. v19}, Ljava/util/Date;-><init>()V

    invoke-virtual/range {v18 .. v19}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->currentTime:Landroid/widget/TextView;

    move-object/from16 v18, v0

    new-instance v19, Ljava/lang/StringBuilder;

    const v20, 0x7f0a014a    # com.konka.tvsettings.R.string.str_time_time

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->getString(I)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-direct/range {v19 .. v20}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v20, " : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :pswitch_9
    const-string v18, "PAL"

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    goto/16 :goto_c

    :pswitch_a
    const-string v18, "NTSC"

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    goto/16 :goto_c

    :pswitch_b
    const-string v18, "SECAM"

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    goto/16 :goto_c

    :pswitch_c
    const-string v18, "BG"

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    goto/16 :goto_d

    :pswitch_d
    const-string v18, "DK"

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    goto/16 :goto_d

    :pswitch_e
    const-string v18, " I"

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    goto/16 :goto_d

    :pswitch_f
    const-string v18, " L"

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    goto/16 :goto_d

    :pswitch_10
    const-string v18, " M"

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/SourceInfoActivity;->str:Ljava/lang/String;

    goto/16 :goto_d

    :cond_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mViewRlATVMoreInfo:Landroid/widget/RelativeLayout;

    move-object/from16 v18, v0

    const/16 v19, 0x4

    invoke-virtual/range {v18 .. v19}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mViewRlDTVMoreInfo:Landroid/widget/RelativeLayout;

    move-object/from16 v18, v0

    const/16 v19, 0x4

    invoke-virtual/range {v18 .. v19}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_9
        :pswitch_9
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method


# virtual methods
.method public getVideoInfo(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V
    .locals 11
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    const/16 v10, 0x442

    const/16 v9, 0x438

    const/16 v8, 0x42e

    const/16 v7, 0x2c6

    const/16 v6, 0x48

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/CommonDesk;->getVideoInfo()Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->isSignalStabled()Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "==================is not signal stabled=============="

    invoke-static {v4}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :cond_1
    const-string v4, "==================is signal stabled=============="

    invoke-static {v4}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v4, v4, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    if-eqz v4, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "==================FrameRate=============="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16FrameRate:S

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v4, v4, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16FrameRate:S

    add-int/lit8 v4, v4, 0x5

    div-int/lit8 v1, v4, 0xa

    const/16 v4, 0x2e

    if-le v1, v4, :cond_3

    const/16 v4, 0x34

    if-ge v1, v4, :cond_3

    const/16 v1, 0x32

    :cond_2
    :goto_1
    invoke-static {}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource()[I

    move-result-object v4

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    :pswitch_1
    goto :goto_0

    :pswitch_2
    if-nez v1, :cond_7

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "  "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16HResolution:S

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "X"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto :goto_0

    :cond_3
    const/16 v4, 0x38

    if-le v1, v4, :cond_4

    const/16 v4, 0x3e

    if-ge v1, v4, :cond_4

    const/16 v1, 0x3c

    goto :goto_1

    :cond_4
    const/16 v4, 0x43

    if-le v1, v4, :cond_5

    if-ge v1, v6, :cond_5

    const/16 v1, 0x46

    goto :goto_1

    :cond_5
    if-le v1, v6, :cond_6

    const/16 v4, 0x4d

    if-ge v1, v4, :cond_6

    const/16 v1, 0x4b

    goto :goto_1

    :cond_6
    const/16 v4, 0x50

    if-le v1, v4, :cond_2

    const/16 v4, 0x55

    if-ge v1, v4, :cond_2

    const/16 v1, 0x55

    goto :goto_1

    :cond_7
    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "  "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16HResolution:S

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "X"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "@"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "HZ"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_3
    const-string v2, ""

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->getVideoStandard()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    move-result-object v3

    invoke-static {}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumAvdVideoStandardType()[I

    move-result-object v4

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_1

    const-string v2, "AUTO"

    :goto_2
    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "  "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_4
    const-string v2, "PAL"

    goto :goto_2

    :pswitch_5
    const-string v2, "NTSC"

    goto :goto_2

    :pswitch_6
    const-string v2, "SECAM"

    goto :goto_2

    :pswitch_7
    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/CommonDesk;->isHdmiSignalMode()Z

    move-result v4

    if-eqz v4, :cond_f

    const/4 v0, 0x1

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v4, v4, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16HResolution:S

    if-lt v4, v7, :cond_8

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v4, v4, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16HResolution:S

    const/16 v5, 0x604

    if-gt v4, v5, :cond_8

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v4, v4, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    const/16 v5, 0x1d6

    if-lt v4, v5, :cond_8

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v4, v4, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    const/16 v5, 0x1ea

    if-gt v4, v5, :cond_8

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    const/16 v5, 0x1e0

    iput-short v5, v4, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    :goto_3
    if-eqz v0, :cond_e

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "  "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-object v4, v4, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->enScanType:Lcom/konka/kkinterface/tv/CommonDesk$EN_SCAN_TYPE;

    sget-object v5, Lcom/konka/kkinterface/tv/CommonDesk$EN_SCAN_TYPE;->E_PROGRESSIVE:Lcom/konka/kkinterface/tv/CommonDesk$EN_SCAN_TYPE;

    if-ne v4, v5, :cond_d

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "P"

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    :goto_4
    if-eqz v1, :cond_0

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "@"

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "HZ"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto/16 :goto_0

    :cond_8
    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v4, v4, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    const/16 v5, 0x236

    if-le v4, v5, :cond_9

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v4, v4, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    const/16 v5, 0x24a

    if-ge v4, v5, :cond_9

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    const/16 v5, 0x240

    iput-short v5, v4, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    goto :goto_3

    :cond_9
    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v4, v4, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    if-le v4, v7, :cond_a

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v4, v4, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    const/16 v5, 0x2da

    if-ge v4, v5, :cond_a

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v4, v4, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16HResolution:S

    const/16 v5, 0x4f6

    if-le v4, v5, :cond_a

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v4, v4, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16HResolution:S

    const/16 v5, 0x50a

    if-ge v4, v5, :cond_a

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    const/16 v5, 0x2d0

    iput-short v5, v4, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    goto/16 :goto_3

    :cond_a
    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v4, v4, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    if-le v4, v8, :cond_b

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v4, v4, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    if-ge v4, v10, :cond_b

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v4, v4, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16HResolution:S

    const/16 v5, 0x776

    if-le v4, v5, :cond_b

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v4, v4, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16HResolution:S

    const/16 v5, 0x78a

    if-ge v4, v5, :cond_b

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iput-short v9, v4, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    goto/16 :goto_3

    :cond_b
    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v4, v4, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    if-le v4, v8, :cond_c

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v4, v4, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    if-ge v4, v10, :cond_c

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v4, v4, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16HResolution:S

    const/16 v5, 0x3b6

    if-le v4, v5, :cond_c

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v4, v4, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16HResolution:S

    const/16 v5, 0x3ca

    if-ge v4, v5, :cond_c

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iput-short v9, v4, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    goto/16 :goto_3

    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_3

    :cond_d
    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "I"

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto/16 :goto_4

    :cond_e
    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "  "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16HResolution:S

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "X"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto/16 :goto_4

    :cond_f
    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v4, v4, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16HResolution:S

    const/16 v5, 0x4f6

    if-lt v4, v5, :cond_10

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v4, v4, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16HResolution:S

    const/16 v5, 0x50a

    if-gt v4, v5, :cond_10

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v4, v4, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    const/16 v5, 0x2f8

    if-lt v4, v5, :cond_10

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v4, v4, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    const/16 v5, 0x302

    if-gt v4, v5, :cond_10

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    const/16 v5, 0x300

    iput-short v5, v4, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    :cond_10
    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "  "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16HResolution:S

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "X"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto/16 :goto_4

    :pswitch_8
    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "  "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-object v4, v4, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->enScanType:Lcom/konka/kkinterface/tv/CommonDesk$EN_SCAN_TYPE;

    sget-object v5, Lcom/konka/kkinterface/tv/CommonDesk$EN_SCAN_TYPE;->E_PROGRESSIVE:Lcom/konka/kkinterface/tv/CommonDesk$EN_SCAN_TYPE;

    if-ne v4, v5, :cond_11

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "P"

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    :goto_5
    if-eqz v1, :cond_0

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "@"

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "HZ"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto/16 :goto_0

    :cond_11
    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "I"

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_8
        :pswitch_8
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public loadDtvInfo()V
    .locals 12

    const-wide/16 v10, 0x3e8

    const v6, 0x7f07012d    # com.konka.tvsettings.R.id.popup_dtv_source_info_tv_num

    invoke-virtual {p0, v6}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mViewTvDTVNum:Landroid/widget/TextView;

    const v6, 0x7f07012e    # com.konka.tvsettings.R.id.popup_dtv_source_info_tv_name

    invoke-virtual {p0, v6}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mViewTvDTVName:Landroid/widget/TextView;

    const v6, 0x7f07012f    # com.konka.tvsettings.R.id.popup_dtv_source_info_tv_date

    invoke-virtual {p0, v6}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mViewTvDTVDate:Landroid/widget/TextView;

    const v6, 0x7f070132    # com.konka.tvsettings.R.id.popup_dtv_source_info_tv_current_time

    invoke-virtual {p0, v6}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mViewTvDTVCurrTime:Landroid/widget/TextView;

    const v6, 0x7f070133    # com.konka.tvsettings.R.id.popup_dtv_source_info_tv_current_name

    invoke-virtual {p0, v6}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mViewTvDTVCurrName:Landroid/widget/TextView;

    const v6, 0x7f070130    # com.konka.tvsettings.R.id.popup_dtv_source_info_tv_next_time

    invoke-virtual {p0, v6}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mViewTvDTVNextTime:Landroid/widget/TextView;

    const v6, 0x7f070131    # com.konka.tvsettings.R.id.popup_dtv_source_info_tv_next_name

    invoke-virtual {p0, v6}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mViewTvDTVNextName:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v3

    iget v6, v3, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    invoke-direct {p0, v6}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->showTVNum(I)V

    iget-object v6, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mViewTvDTVName:Landroid/widget/TextView;

    iget-object v7, v3, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceName:Ljava/lang/String;

    invoke-direct {p0, v7}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->cutLongString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v6, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/EpgDesk;->getCurClkTime()Landroid/text/format/Time;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->showYear(Landroid/text/format/Time;)V

    invoke-virtual {v5}, Landroid/text/format/Time;->setToNow()V

    const/4 v2, 0x0

    :try_start_0
    iget-object v6, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->epgDesk:Lcom/konka/kkinterface/tv/EpgDesk;

    iget-short v7, v3, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    iget v8, v3, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    const/4 v9, 0x2

    invoke-interface {v6, v7, v8, v5, v9}, Lcom/konka/kkinterface/tv/EpgDesk;->getEventInfo(SILandroid/text/format/Time;I)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_1

    new-instance v4, Landroid/text/format/Time;

    invoke-direct {v4}, Landroid/text/format/Time;-><init>()V

    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v6, 0x0

    :try_start_1
    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_0

    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    iget v6, v6, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->startTime:I

    int-to-long v6, v6

    mul-long/2addr v6, v10

    invoke-virtual {v4, v6, v7}, Landroid/text/format/Time;->set(J)V

    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    iget v6, v6, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->endTime:I

    int-to-long v6, v6

    mul-long/2addr v6, v10

    invoke-virtual {v1, v6, v7}, Landroid/text/format/Time;->set(J)V

    iget-object v6, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mViewTvDTVCurrTime:Landroid/widget/TextView;

    invoke-direct {p0, v4, v1, v6}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->showTime(Landroid/text/format/Time;Landroid/text/format/Time;Landroid/widget/TextView;)V

    iget-object v7, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mViewTvDTVCurrName:Landroid/widget/TextView;

    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    iget-object v6, v6, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->cutLongString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_1

    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    iget v6, v6, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->startTime:I

    int-to-long v6, v6

    mul-long/2addr v6, v10

    invoke-virtual {v4, v6, v7}, Landroid/text/format/Time;->set(J)V

    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    iget v6, v6, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->endTime:I

    int-to-long v6, v6

    mul-long/2addr v6, v10

    invoke-virtual {v1, v6, v7}, Landroid/text/format/Time;->set(J)V

    iget-object v6, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mViewTvDTVNextTime:Landroid/widget/TextView;

    invoke-direct {p0, v4, v1, v6}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->showTime(Landroid/text/format/Time;Landroid/text/format/Time;Landroid/widget/TextView;)V

    iget-object v7, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mViewTvDTVNextName:Landroid/widget/TextView;

    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    iget-object v6, v6, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->cutLongString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1    # Landroid/os/Bundle;

    const/4 v2, 0x1

    const/4 v1, 0x0

    const-string v0, "onCreate"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030031    # com.konka.tvsettings.R.layout.popup_source_info_menu

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/TVRootApp;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->rootApp:Lcom/konka/tvsettings/TVRootApp;

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->rootApp:Lcom/konka/tvsettings/TVRootApp;

    invoke-virtual {v0}, Lcom/konka/tvsettings/TVRootApp;->getRootmenuHandler()Lcom/konka/tvsettings/RootActivity$RootMenuHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->rootmenuHandler:Lcom/konka/tvsettings/RootActivity$RootMenuHandler;

    iput-boolean v2, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->IsSourceInfofirstOnCreate:Z

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->settingDesk:Lcom/konka/kkinterface/tv/SettingDesk;

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getDataBaseManagerInstance()Lcom/konka/kkinterface/tv/DataBaseDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk;->queryCurCountry()I

    move-result v0

    iput v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->miCurCountry:I

    iget v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->miCurCountry:I

    if-nez v0, :cond_0

    iput-boolean v2, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->bCheckisAustralia:Z

    :cond_0
    const v0, 0x7f07012b    # com.konka.tvsettings.R.id.popup_source_info_menu_tv_num

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mViewTvInfo:Landroid/widget/TextView;

    const v0, 0x7f07012c    # com.konka.tvsettings.R.id.popup_source_info_menu_dtv_info

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mViewRlDtvInfo:Landroid/widget/RelativeLayout;

    const v0, 0x7f07012a    # com.konka.tvsettings.R.id.popup_source_info_menu_no_dtv_info

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mViewRlNoDtvInfo:Landroid/widget/RelativeLayout;

    const v0, 0x7f07013b    # com.konka.tvsettings.R.id.dtv_source_info_more_info

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mViewRlDTVMoreInfo:Landroid/widget/RelativeLayout;

    const v0, 0x7f070134    # com.konka.tvsettings.R.id.source_info_more_info

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mViewRlATVMoreInfo:Landroid/widget/RelativeLayout;

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvPipPopManager;->isPipEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v7, 0x6e

    new-instance v8, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x2

    invoke-direct {v8, v0, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v0, 0xd

    invoke-virtual {v8, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v0, 0xb

    invoke-virtual {v8, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v0, 0x23

    invoke-virtual {v8, v1, v1, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mViewRlNoDtvInfo:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v8}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    new-instance v0, Lcom/konka/tvsettings/common/CountDownTimer;

    const/4 v1, 0x5

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->myHandler:Landroid/os/Handler;

    const/16 v4, 0x4b5

    const/16 v5, 0x7b

    invoke-direct/range {v0 .. v5}, Lcom/konka/tvsettings/common/CountDownTimer;-><init>(IILandroid/os/Handler;II)V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->m_InfoDisplayTimer:Lcom/konka/tvsettings/common/CountDownTimer;

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->m_InfoDisplayTimer:Lcom/konka/tvsettings/common/CountDownTimer;

    invoke-virtual {v0}, Lcom/konka/tvsettings/common/CountDownTimer;->start()V

    new-instance v6, Landroid/content/IntentFilter;

    invoke-direct {v6}, Landroid/content/IntentFilter;-><init>()V

    const-string v0, "com.konka.tv.action.SIGNAL_LOCK"

    invoke-virtual {v6, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v0, Lcom/konka/tvsettings/popup/SourceInfoActivity$2;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/popup/SourceInfoActivity$2;-><init>(Lcom/konka/tvsettings/popup/SourceInfoActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->broadcastReceiver:Landroid/content/BroadcastReceiver;

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->broadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0, v6}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    const-string v0, "onDestroy"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->m_bIsCloseTimer:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->m_InfoDisplayTimer:Lcom/konka/tvsettings/common/CountDownTimer;

    invoke-virtual {v0}, Lcom/konka/tvsettings/common/CountDownTimer;->stop()V

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->broadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v2, 0x1

    const/4 v3, 0x4

    if-ne p1, v3, :cond_1

    iget-object v2, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableAlTimeShift()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->openAlTimeShift()V

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    :goto_0
    return v2

    :cond_1
    const/16 v3, 0xa5

    if-ne p1, v3, :cond_2

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v3, v4, :cond_2

    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/konka/tvsettings/popup/EventInfoActivity;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    const-string v3, "==================finish1=============="

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->finish()V

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->m_bIsCloseTimer:Z

    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    const/16 v3, 0x1000

    iput v3, v1, Landroid/os/Message;->what:I

    iput p1, v1, Landroid/os/Message;->arg1:I

    iput-object p2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->rootmenuHandler:Lcom/konka/tvsettings/RootActivity$RootMenuHandler;

    invoke-virtual {v3, v1}, Lcom/konka/tvsettings/RootActivity$RootMenuHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->IsSourceInfofirstOnCreate:Z

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 3

    const-string v1, "onResume"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->meInputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->meInputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->checkSystemAutoTime(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    if-nez v1, :cond_0

    const-string v1, "==================finish2=============="

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->finish()V

    :cond_0
    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->updateInfo()V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->meInputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->getVideoInfo(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mViewTvInfo:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->updateChannelInfo()V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->m_InfoDisplayTimer:Lcom/konka/tvsettings/common/CountDownTimer;

    invoke-virtual {v1}, Lcom/konka/tvsettings/common/CountDownTimer;->reStart()V

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    sget-boolean v1, Lcom/konka/tvsettings/popup/CheckParentalPwd;->IsCheckPwdInSourceInfo:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->IsSourceInfofirstOnCreate:Z

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->finish()V

    const-string v1, "sourceinfoactivity"

    const-string v2, "======start checkparentalpwd"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/konka/tvsettings/popup/CheckParentalPwd;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "lockType"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/16 v1, 0x64

    invoke-virtual {p0, v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_1
    return-void
.end method

.method public updateInfo()V
    .locals 6

    const/4 v5, 0x4

    iget-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrentChannelNumber()I

    move-result v3

    iput v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->miCurrChannelNum:I

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "before message: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " and the source is ==="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->meInputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource()[I

    move-result-object v3

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->meInputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    :goto_0
    :pswitch_0
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "after message: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void

    :pswitch_1
    iget-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mViewRlATVMoreInfo:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, v2, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto :goto_0

    :pswitch_2
    const-string v3, "DTV2"

    iput-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto :goto_0

    :pswitch_3
    iget-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mViewRlDTVMoreInfo:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, v1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto :goto_0

    :pswitch_4
    iget-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->getInputSourceInfo()Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    move-result-object v3

    iget v0, v3, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountAV:I

    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    const-string v3, "AV"

    iput-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string v3, "AV1"

    iput-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto :goto_0

    :pswitch_5
    const-string v3, "AV2"

    iput-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto :goto_0

    :pswitch_6
    const-string v3, "AV3"

    iput-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto :goto_0

    :pswitch_7
    const-string v3, "AV4"

    iput-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto :goto_0

    :pswitch_8
    const-string v3, "AV5"

    iput-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto :goto_0

    :pswitch_9
    const-string v3, "AV6"

    iput-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_a
    const-string v3, "AV7"

    iput-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_b
    const-string v3, "AV8"

    iput-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_c
    iget-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/ChannelDesk;->isSignalStabled()Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "HDMI1"

    iput-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto/16 :goto_0

    :cond_1
    iget-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->isHdmiSignalMode()Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "HDMI1"

    iput-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto/16 :goto_0

    :cond_2
    const-string v3, "DVI1"

    iput-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_d
    iget-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/ChannelDesk;->isSignalStabled()Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "HDMI2"

    iput-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto/16 :goto_0

    :cond_3
    iget-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->isHdmiSignalMode()Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "HDMI2"

    iput-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto/16 :goto_0

    :cond_4
    const-string v3, "DVI2"

    iput-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_e
    iget-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/ChannelDesk;->isSignalStabled()Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "HDMI3"

    iput-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto/16 :goto_0

    :cond_5
    iget-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->isHdmiSignalMode()Z

    move-result v3

    if-eqz v3, :cond_6

    const-string v3, "HDMI3"

    iput-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto/16 :goto_0

    :cond_6
    const-string v3, "DVI3"

    iput-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_f
    iget-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/ChannelDesk;->isSignalStabled()Z

    move-result v3

    if-nez v3, :cond_7

    const-string v3, "HDMI4"

    iput-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto/16 :goto_0

    :cond_7
    iget-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->isHdmiSignalMode()Z

    move-result v3

    if-eqz v3, :cond_8

    const-string v3, "HDMI4"

    iput-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto/16 :goto_0

    :cond_8
    const-string v3, "DVI4"

    iput-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_10
    const-string v3, "YPBPR"

    iput-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_11
    const-string v3, "YPBPR2"

    iput-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_12
    const-string v3, "YPBPR3"

    iput-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_13
    const-string v3, "VGA"

    iput-object v3, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_13
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
