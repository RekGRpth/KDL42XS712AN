.class Lcom/konka/tvsettings/popup/SetParentalPwd$1;
.super Ljava/lang/Object;
.source "SetParentalPwd.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/popup/SetParentalPwd;->setMyEditListener(Landroid/widget/EditText;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/popup/SetParentalPwd;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 6
    .param p1    # Landroid/text/Editable;

    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo1:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$0()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$1(I)V

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo1:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$0()I

    move-result v0

    if-ne v0, v1, :cond_2

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo2:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$2()I

    move-result v0

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->old_pwd2:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$3(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->old_pwd1:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$4(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$5(Lcom/konka/tvsettings/popup/SetParentalPwd;I)V

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo1:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$0()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$1(I)V

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo2:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$2()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$6(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->old_pwd1:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$4(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;

    move-result-object v0

    const-string v1, "*"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo1:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$0()I

    move-result v0

    if-ne v0, v3, :cond_4

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo2:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$2()I

    move-result v0

    if-ne v0, v3, :cond_3

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->old_pwd3:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$7(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->old_pwd2:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$3(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$8(Lcom/konka/tvsettings/popup/SetParentalPwd;I)V

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo1:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$0()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$1(I)V

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo2:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$2()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$6(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->old_pwd2:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$3(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;

    move-result-object v0

    const-string v1, "*"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_4
    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo1:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$0()I

    move-result v0

    if-ne v0, v4, :cond_6

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo2:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$2()I

    move-result v0

    if-ne v0, v4, :cond_5

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->old_pwd4:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$9(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->old_pwd3:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$7(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$10(Lcom/konka/tvsettings/popup/SetParentalPwd;I)V

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo1:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$0()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$1(I)V

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo2:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$2()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$6(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->old_pwd3:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$7(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;

    move-result-object v0

    const-string v1, "*"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_6
    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo1:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$0()I

    move-result v0

    if-ne v0, v5, :cond_8

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo2:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$2()I

    move-result v0

    if-ne v0, v5, :cond_7

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->new_pwd1:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$11(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->old_pwd4:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$9(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$12(Lcom/konka/tvsettings/popup/SetParentalPwd;I)V

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo1:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$0()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$1(I)V

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo2:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$2()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$6(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->old_pwd4:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$9(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;

    move-result-object v0

    const-string v1, "*"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_8
    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo1:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$0()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_a

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo2:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$2()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_9

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->new_pwd2:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$13(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto/16 :goto_0

    :cond_9
    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->new_pwd1:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$11(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$14(Lcom/konka/tvsettings/popup/SetParentalPwd;I)V

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo1:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$0()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$1(I)V

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo2:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$2()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$6(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->new_pwd1:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$11(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;

    move-result-object v0

    const-string v1, "*"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_a
    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo1:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$0()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_c

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo2:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$2()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_b

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->new_pwd3:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$15(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto/16 :goto_0

    :cond_b
    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->new_pwd2:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$13(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$16(Lcom/konka/tvsettings/popup/SetParentalPwd;I)V

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo1:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$0()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$1(I)V

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo2:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$2()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$6(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->new_pwd2:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$13(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;

    move-result-object v0

    const-string v1, "*"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_c
    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo1:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$0()I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_e

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo2:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$2()I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_d

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->new_pwd4:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$17(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto/16 :goto_0

    :cond_d
    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->new_pwd3:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$15(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$18(Lcom/konka/tvsettings/popup/SetParentalPwd;I)V

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo1:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$0()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$1(I)V

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo2:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$2()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$6(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->new_pwd3:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$15(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;

    move-result-object v0

    const-string v1, "*"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_e
    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo1:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$0()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_10

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo2:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$2()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_f

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->confirm_pwd1:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$19(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto/16 :goto_0

    :cond_f
    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->new_pwd4:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$17(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$20(Lcom/konka/tvsettings/popup/SetParentalPwd;I)V

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo1:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$0()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$1(I)V

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo2:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$2()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$6(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->new_pwd4:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$17(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;

    move-result-object v0

    const-string v1, "*"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_10
    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo1:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$0()I

    move-result v0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_12

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo2:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$2()I

    move-result v0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_11

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->confirm_pwd2:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$21(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto/16 :goto_0

    :cond_11
    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->confirm_pwd1:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$19(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$22(Lcom/konka/tvsettings/popup/SetParentalPwd;I)V

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo1:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$0()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$1(I)V

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo2:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$2()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$6(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->confirm_pwd1:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$19(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;

    move-result-object v0

    const-string v1, "*"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_12
    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo1:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$0()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_14

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo2:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$2()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_13

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->confirm_pwd3:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$23(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto/16 :goto_0

    :cond_13
    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->confirm_pwd2:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$21(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$24(Lcom/konka/tvsettings/popup/SetParentalPwd;I)V

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo1:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$0()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$1(I)V

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo2:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$2()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$6(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->confirm_pwd2:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$21(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;

    move-result-object v0

    const-string v1, "*"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_14
    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo1:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$0()I

    move-result v0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_16

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo2:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$2()I

    move-result v0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_15

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->confirm_pwd4:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$25(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto/16 :goto_0

    :cond_15
    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->confirm_pwd3:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$23(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$26(Lcom/konka/tvsettings/popup/SetParentalPwd;I)V

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo1:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$0()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$1(I)V

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo2:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$2()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$6(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->confirm_pwd3:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$23(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;

    move-result-object v0

    const-string v1, "*"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_16
    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo1:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$0()I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo2:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$2()I

    move-result v0

    const/16 v1, 0xc

    if-ne v0, v1, :cond_17

    invoke-static {v2}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$1(I)V

    invoke-static {v2}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$6(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # invokes: Lcom/konka/tvsettings/popup/SetParentalPwd;->setpassword()V
    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$27(Lcom/konka/tvsettings/popup/SetParentalPwd;)V

    goto/16 :goto_0

    :cond_17
    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->confirm_pwd4:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$25(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$28(Lcom/konka/tvsettings/popup/SetParentalPwd;I)V

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo1:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$0()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$1(I)V

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->PwdNo2:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$2()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$6(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SetParentalPwd$1;->this$0:Lcom/konka/tvsettings/popup/SetParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/SetParentalPwd;->confirm_pwd4:Landroid/widget/EditText;
    invoke-static {v0}, Lcom/konka/tvsettings/popup/SetParentalPwd;->access$25(Lcom/konka/tvsettings/popup/SetParentalPwd;)Landroid/widget/EditText;

    move-result-object v0

    const-string v1, "*"

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method
