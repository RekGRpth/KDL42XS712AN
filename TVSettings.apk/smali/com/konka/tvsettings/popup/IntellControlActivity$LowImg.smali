.class Lcom/konka/tvsettings/popup/IntellControlActivity$LowImg;
.super Landroid/widget/ImageView;
.source "IntellControlActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/popup/IntellControlActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LowImg"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/IntellControlActivity;


# direct methods
.method public constructor <init>(Lcom/konka/tvsettings/popup/IntellControlActivity;)V
    .locals 3

    iput-object p1, p0, Lcom/konka/tvsettings/popup/IntellControlActivity$LowImg;->this$0:Lcom/konka/tvsettings/popup/IntellControlActivity;

    invoke-virtual {p1}, Lcom/konka/tvsettings/popup/IntellControlActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    # getter for: Lcom/konka/tvsettings/popup/IntellControlActivity;->WIDTH:I
    invoke-static {p1}, Lcom/konka/tvsettings/popup/IntellControlActivity;->access$2(Lcom/konka/tvsettings/popup/IntellControlActivity;)I

    move-result v1

    # getter for: Lcom/konka/tvsettings/popup/IntellControlActivity;->HEIGHT:I
    invoke-static {p1}, Lcom/konka/tvsettings/popup/IntellControlActivity;->access$3(Lcom/konka/tvsettings/popup/IntellControlActivity;)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    # getter for: Lcom/konka/tvsettings/popup/IntellControlActivity;->LEFT_MARGIN:I
    invoke-static {p1}, Lcom/konka/tvsettings/popup/IntellControlActivity;->access$4(Lcom/konka/tvsettings/popup/IntellControlActivity;)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/IntellControlActivity$LowImg;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v1, 0x7f0200eb    # com.konka.tvsettings.R.drawable.popup_intell_control_menu_low

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/popup/IntellControlActivity$LowImg;->setBackgroundResource(I)V

    return-void
.end method
