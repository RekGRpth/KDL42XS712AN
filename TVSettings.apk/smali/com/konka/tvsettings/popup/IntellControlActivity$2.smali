.class Lcom/konka/tvsettings/popup/IntellControlActivity$2;
.super Ljava/util/TimerTask;
.source "IntellControlActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/popup/IntellControlActivity;->ChannelTimerTask()Ljava/util/TimerTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/IntellControlActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/popup/IntellControlActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/IntellControlActivity$2;->this$0:Lcom/konka/tvsettings/popup/IntellControlActivity;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/popup/IntellControlActivity$2;->this$0:Lcom/konka/tvsettings/popup/IntellControlActivity;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/PictureManager;->getBacklight()I

    move-result v2

    invoke-static {v1, v2}, Lcom/konka/tvsettings/popup/IntellControlActivity;->access$5(Lcom/konka/tvsettings/popup/IntellControlActivity;I)V

    :cond_0
    iget-object v1, p0, Lcom/konka/tvsettings/popup/IntellControlActivity$2;->this$0:Lcom/konka/tvsettings/popup/IntellControlActivity;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/IntellControlActivity$2;->this$0:Lcom/konka/tvsettings/popup/IntellControlActivity;

    # getter for: Lcom/konka/tvsettings/popup/IntellControlActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;
    invoke-static {v2}, Lcom/konka/tvsettings/popup/IntellControlActivity;->access$6(Lcom/konka/tvsettings/popup/IntellControlActivity;)Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/PictureDesk;->getDlcAverageLuma()S

    move-result v2

    invoke-static {v1, v2}, Lcom/konka/tvsettings/popup/IntellControlActivity;->access$7(Lcom/konka/tvsettings/popup/IntellControlActivity;I)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/IntellControlActivity$2;->this$0:Lcom/konka/tvsettings/popup/IntellControlActivity;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/IntellControlActivity$2;->this$0:Lcom/konka/tvsettings/popup/IntellControlActivity;

    # getter for: Lcom/konka/tvsettings/popup/IntellControlActivity;->miEcoProg:I
    invoke-static {v2}, Lcom/konka/tvsettings/popup/IntellControlActivity;->access$0(Lcom/konka/tvsettings/popup/IntellControlActivity;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/konka/tvsettings/popup/IntellControlActivity;->access$8(Lcom/konka/tvsettings/popup/IntellControlActivity;I)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/IntellControlActivity$2;->this$0:Lcom/konka/tvsettings/popup/IntellControlActivity;

    # getter for: Lcom/konka/tvsettings/popup/IntellControlActivity;->myHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/IntellControlActivity;->access$9(Lcom/konka/tvsettings/popup/IntellControlActivity;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x7b

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method
