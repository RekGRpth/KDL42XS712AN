.class public Lcom/konka/tvsettings/popup/PvrActivity;
.super Lcom/konka/tvsettings/teletext/MstarBaseActivity;
.source "PvrActivity.java"

# interfaces
.implements Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/popup/PvrActivity$BrowserCalledPlayBackProgress;,
        Lcom/konka/tvsettings/popup/PvrActivity$DirectoryFilter;,
        Lcom/konka/tvsettings/popup/PvrActivity$OnStopRecordCancelClickListener;,
        Lcom/konka/tvsettings/popup/PvrActivity$OnStopRecordConfirmClickListener;,
        Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;,
        Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;,
        Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;,
        Lcom/konka/tvsettings/popup/PvrActivity$RootMenuEventReceiver;,
        Lcom/konka/tvsettings/popup/PvrActivity$usbInfoUpdate;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumPvrThumbnailStatus:[I = null

.field private static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$PvrPlaybackSpeed$EnumPvrPlaybackSpeed:[I = null

.field private static final CHOOSE_DISK:Ljava/lang/String; = "CHOOSE_DISK"

.field private static final DEBUG_TAG:Ljava/lang/String;

.field private static final INVALID_TIME:I = -0x1

.field private static final NO_DISK:Ljava/lang/String; = "NO_DISK"

.field public static isPVRActivityActive:Z

.field private static stopRecordDialog:Landroid/app/Dialog;


# instance fields
.field private A_progress:I

.field private AutoRecord:Z

.field private RPProgress:Lcom/konka/tvsettings/popup/TextProgressBar;

.field private activity:Landroid/app/Activity;

.field private audioLangPosLive:S

.field private curPvrMode:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

.field private currentPlaybackIndex:I

.field private currentlooptime:I

.field private eventNameText:Landroid/widget/TextView;

.field private ff:Landroid/widget/ImageView;

.field private handler:Landroid/os/Handler;

.field private isAlwaysTimeShiftBegin:Z

.field private isAlwaysTimeShiftPlay:Z

.field private isBrowserCalled:Z

.field private isMenuHide:Z

.field private isNotifyRecordStop:Z

.field private isOneTouchPauseMode:Z

.field private isOneTouchPlayMode:Z

.field private isWatchRcodFilInRcoding:Z

.field private looptime:I

.field private lp4LoopAB:Landroid/widget/RelativeLayout$LayoutParams;

.field private m_RecordingImage:Landroid/widget/ImageView;

.field private menuHideAnimation:Landroid/animation/AnimatorSet;

.field private menuShowAnimation:Landroid/animation/AnimatorSet;

.field private pause:Landroid/widget/ImageView;

.field private play:Landroid/widget/ImageView;

.field private playbackText:Landroid/widget/TextView;

.field private progress_loopab:Landroid/widget/ProgressBar;

.field private pvr:Lcom/konka/kkinterface/tv/PvrDesk;

.field private pvrABLoopEndTime:I

.field private pvrABLoopStartTime:I

.field private pvrImageFlag:Lcom/konka/tvsettings/popup/PvrImageFlag;

.field private pvrView:Landroid/widget/RelativeLayout;

.field private recordDiskLable:Ljava/lang/String;

.field private recordDiskPath:Ljava/lang/String;

.field private recordIconAnimation:Landroid/animation/AnimatorSet;

.field private recorder:Landroid/widget/ImageView;

.field private recordingText:Landroid/widget/TextView;

.field private recordingView:Landroid/widget/RelativeLayout;

.field private rev:Landroid/widget/ImageView;

.field private rootView:Landroid/widget/RelativeLayout;

.field private rootmenuEventReceiver:Lcom/konka/tvsettings/popup/PvrActivity$RootMenuEventReceiver;

.field private final savingProgress:I

.field private serviceNameText:Landroid/widget/TextView;

.field private setPvrABLoop:Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

.field private settingDesk:Lcom/konka/kkinterface/tv/SettingDesk;

.field private slow:Landroid/widget/ImageView;

.field private stop:Landroid/widget/ImageView;

.field private subtitlePosLive:I

.field private tPreviousEvent:Landroid/view/KeyEvent;

.field private textViewFf:Landroid/widget/TextView;

.field private textViewPlay:Landroid/widget/TextView;

.field private textViewRev:Landroid/widget/TextView;

.field private textViewSlow:Landroid/widget/TextView;

.field private thumbnail:Lcom/konka/tvsettings/popup/PVRThumbnail;

.field private time:Landroid/widget/ImageView;

.field private final timeChoose:I

.field private timeChooser:Landroid/app/AlertDialog;

.field private totalRecordTime:Landroid/widget/TextView;

.field private usbDriverLabel:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private usbDriverLabelPcStyle:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private usbDriverPath:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private usbFreeSpace:Landroid/widget/ProgressBar;

.field private usbLabel:Landroid/widget/TextView;

.field private usbPercentage:Landroid/widget/TextView;

.field private usbSelecter:Lcom/konka/tvsettings/function/USBDiskSelecter;


# direct methods
.method static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumPvrThumbnailStatus()[I
    .locals 3

    sget-object v0, Lcom/konka/tvsettings/popup/PvrActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumPvrThumbnailStatus:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumPvrThumbnailStatus;->values()[Lcom/mstar/android/tvapi/common/vo/EnumPvrThumbnailStatus;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPvrThumbnailStatus;->E_FAIL:Lcom/mstar/android/tvapi/common/vo/EnumPvrThumbnailStatus;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumPvrThumbnailStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPvrThumbnailStatus;->E_OK:Lcom/mstar/android/tvapi/common/vo/EnumPvrThumbnailStatus;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumPvrThumbnailStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPvrThumbnailStatus;->E_REPLACE:Lcom/mstar/android/tvapi/common/vo/EnumPvrThumbnailStatus;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumPvrThumbnailStatus;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/konka/tvsettings/popup/PvrActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumPvrThumbnailStatus:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method static synthetic $SWITCH_TABLE$com$mstar$android$tvapi$common$vo$PvrPlaybackSpeed$EnumPvrPlaybackSpeed()[I
    .locals 3

    sget-object v0, Lcom/konka/tvsettings/popup/PvrActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$PvrPlaybackSpeed$EnumPvrPlaybackSpeed:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->values()[Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_0X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_13

    :goto_1
    :try_start_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_16XFB:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_12

    :goto_2
    :try_start_2
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_16XFF:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->ordinal()I

    move-result v1

    const/16 v2, 0x12

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_11

    :goto_3
    :try_start_3
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_1X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->ordinal()I

    move-result v1

    const/16 v2, 0xe

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_10

    :goto_4
    :try_start_4
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_1XFB:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_f

    :goto_5
    :try_start_5
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_2XFB:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_e

    :goto_6
    :try_start_6
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_2XFF:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->ordinal()I

    move-result v1

    const/16 v2, 0xf

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_d

    :goto_7
    :try_start_7
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_32XFB:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_c

    :goto_8
    :try_start_8
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_32XFF:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->ordinal()I

    move-result v1

    const/16 v2, 0x13

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_b

    :goto_9
    :try_start_9
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_4XFB:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_a

    :goto_a
    :try_start_a
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_4XFF:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->ordinal()I

    move-result v1

    const/16 v2, 0x10

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_9

    :goto_b
    :try_start_b
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_8XFB:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_8

    :goto_c
    :try_start_c
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_8XFF:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->ordinal()I

    move-result v1

    const/16 v2, 0x11

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_7

    :goto_d
    :try_start_d
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_FF_1_16X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_6

    :goto_e
    :try_start_e
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_FF_1_2X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_5

    :goto_f
    :try_start_f
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_FF_1_32X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_4

    :goto_10
    :try_start_10
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_FF_1_4X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_3

    :goto_11
    :try_start_11
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_FF_1_8X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_2

    :goto_12
    :try_start_12
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_INVALID:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->ordinal()I

    move-result v1

    const/16 v2, 0x14

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_1

    :goto_13
    :try_start_13
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_STEP_IN:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_0

    :goto_14
    sput-object v0, Lcom/konka/tvsettings/popup/PvrActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$PvrPlaybackSpeed$EnumPvrPlaybackSpeed:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_14

    :catch_1
    move-exception v1

    goto :goto_13

    :catch_2
    move-exception v1

    goto :goto_12

    :catch_3
    move-exception v1

    goto :goto_11

    :catch_4
    move-exception v1

    goto :goto_10

    :catch_5
    move-exception v1

    goto :goto_f

    :catch_6
    move-exception v1

    goto :goto_e

    :catch_7
    move-exception v1

    goto :goto_d

    :catch_8
    move-exception v1

    goto :goto_c

    :catch_9
    move-exception v1

    goto :goto_b

    :catch_a
    move-exception v1

    goto :goto_a

    :catch_b
    move-exception v1

    goto/16 :goto_9

    :catch_c
    move-exception v1

    goto/16 :goto_8

    :catch_d
    move-exception v1

    goto/16 :goto_7

    :catch_e
    move-exception v1

    goto/16 :goto_6

    :catch_f
    move-exception v1

    goto/16 :goto_5

    :catch_10
    move-exception v1

    goto/16 :goto_4

    :catch_11
    move-exception v1

    goto/16 :goto_3

    :catch_12
    move-exception v1

    goto/16 :goto_2

    :catch_13
    move-exception v1

    goto/16 :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/tvsettings/popup/PvrActivity;->isPVRActivityActive:Z

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/tvsettings/popup/PvrActivity;->stopRecordDialog:Landroid/app/Dialog;

    const-class v0, Lcom/konka/tvsettings/popup/PvrActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/konka/tvsettings/popup/PvrActivity;->DEBUG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;-><init>()V

    sget-object v0, Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;->E_PVR_AB_LOOP_STATUS_NONE:Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->setPvrABLoop:Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

    iput v3, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvrABLoopStartTime:I

    iput v3, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvrABLoopEndTime:I

    sget-object v0, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;->E_PVR_MODE_NONE:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->curPvrMode:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    iput v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->savingProgress:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->timeChoose:I

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->handler:Landroid/os/Handler;

    iput-boolean v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isMenuHide:Z

    iput-boolean v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isBrowserCalled:Z

    iput-boolean v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isOneTouchPlayMode:Z

    iput-boolean v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isOneTouchPauseMode:Z

    iput-boolean v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isNotifyRecordStop:Z

    iput-boolean v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isWatchRcodFilInRcoding:Z

    iput-boolean v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isAlwaysTimeShiftBegin:Z

    iput-boolean v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isAlwaysTimeShiftPlay:Z

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->settingDesk:Lcom/konka/kkinterface/tv/SettingDesk;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvrImageFlag:Lcom/konka/tvsettings/popup/PvrImageFlag;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->rootView:Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordingView:Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvrView:Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recorder:Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->play:Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->stop:Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pause:Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->rev:Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->ff:Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->slow:Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->time:Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->serviceNameText:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->eventNameText:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->totalRecordTime:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->usbLabel:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->usbPercentage:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->activity:Landroid/app/Activity;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordDiskPath:Ljava/lang/String;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordDiskLable:Ljava/lang/String;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->usbSelecter:Lcom/konka/tvsettings/function/USBDiskSelecter;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->timeChooser:Landroid/app/AlertDialog;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->thumbnail:Lcom/konka/tvsettings/popup/PVRThumbnail;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->RPProgress:Lcom/konka/tvsettings/popup/TextProgressBar;

    iput-boolean v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->AutoRecord:Z

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->usbFreeSpace:Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->m_RecordingImage:Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->textViewPlay:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->progress_loopab:Landroid/widget/ProgressBar;

    iput v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->A_progress:I

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->textViewRev:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->textViewFf:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->textViewSlow:Landroid/widget/TextView;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->usbDriverLabel:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->usbDriverPath:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->usbDriverLabelPcStyle:Ljava/util/ArrayList;

    iput v3, p0, Lcom/konka/tvsettings/popup/PvrActivity;->subtitlePosLive:I

    iput-short v3, p0, Lcom/konka/tvsettings/popup/PvrActivity;->audioLangPosLive:S

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordingText:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    iput v3, p0, Lcom/konka/tvsettings/popup/PvrActivity;->currentPlaybackIndex:I

    return-void
.end method

.method private AddThumbnail(I)Z
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->thumbnail:Lcom/konka/tvsettings/popup/PVRThumbnail;

    invoke-virtual {v0, p1}, Lcom/konka/tvsettings/popup/PVRThumbnail;->addThumbnail(I)Z

    move-result v0

    return v0
.end method

.method private CaptureThumbnail()Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/PvrDesk;->captureThumbnail()Lcom/mstar/android/tvapi/common/vo/CaptureThumbnailResult;

    move-result-object v0

    const-string v2, "PVR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "=============>>>> result.getPvrThumbnailStatus() = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/CaptureThumbnailResult;->getPvrThumbnailStatus()Lcom/mstar/android/tvapi/common/vo/EnumPvrThumbnailStatus;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/konka/tvsettings/popup/PvrActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$EnumPvrThumbnailStatus()[I

    move-result-object v2

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/CaptureThumbnailResult;->getPvrThumbnailStatus()Lcom/mstar/android/tvapi/common/vo/EnumPvrThumbnailStatus;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/EnumPvrThumbnailStatus;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    const-string v2, "PVR"

    const-string v3, "capture fail !!!\n"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :pswitch_1
    iget v2, v0, Lcom/mstar/android/tvapi/common/vo/CaptureThumbnailResult;->thumbnailIndex:I

    invoke-direct {p0, v2}, Lcom/konka/tvsettings/popup/PvrActivity;->AddThumbnail(I)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "PVR"

    const-string v3, "=============>>>> AddThumbnail fail !!! "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    :pswitch_2
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->thumbnail:Lcom/konka/tvsettings/popup/PVRThumbnail;

    invoke-virtual {v1}, Lcom/konka/tvsettings/popup/PVRThumbnail;->isShown()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->thumbnail:Lcom/konka/tvsettings/popup/PVRThumbnail;

    iget v2, v0, Lcom/mstar/android/tvapi/common/vo/CaptureThumbnailResult;->thumbnailIndex:I

    invoke-virtual {v1, v2}, Lcom/konka/tvsettings/popup/PVRThumbnail;->setSelection(I)V

    :cond_1
    const/4 v1, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private CheckNeedToStopRecord(Landroid/view/KeyEvent;)Z
    .locals 5
    .param p1    # Landroid/view/KeyEvent;

    const/4 v1, 0x0

    const/4 v0, 0x0

    :try_start_0
    iget-object v4, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/PvrDesk;->isRecording()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    const/16 v4, 0xa6

    if-eq v3, v4, :cond_1

    const/16 v4, 0xa7

    if-eq v3, v4, :cond_1

    const/16 v4, 0xfe

    if-eq v3, v4, :cond_1

    const/16 v4, 0x43

    if-eq v3, v4, :cond_1

    const/16 v4, 0xb2

    if-eq v3, v4, :cond_1

    :cond_0
    :goto_1
    return v1

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_0

    iget-boolean v4, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isMenuHide:Z

    if-eqz v4, :cond_2

    iget-boolean v4, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isAlwaysTimeShiftPlay:Z

    if-nez v4, :cond_2

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->menuShow()V

    goto :goto_1

    :cond_2
    iput-object p1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->tPreviousEvent:Landroid/view/KeyEvent;

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->showStopRecordDialog()Z

    move-result v1

    goto :goto_1
.end method

.method private JumpToThumbnail(I)Z
    .locals 3
    .param p1    # I

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v2, p1}, Lcom/konka/kkinterface/tv/PvrDesk;->jumpToThumbnail(I)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private OnClick_ABLoop()V
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const/4 v1, 0x1

    const v6, 0x7f0a00f7    # com.konka.tvsettings.R.string.str_player_play

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->setPvrABLoop:Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

    sget-object v3, Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;->E_PVR_AB_LOOP_STATUS_NONE:Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/PvrDesk;->getCurPlaybackTimeInSecond()I

    move-result v2

    iput v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvrABLoopStartTime:I

    sget-object v2, Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;->E_PVR_AB_LOOP_STATUS_A:Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

    iput-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->setPvrABLoop:Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->textViewPlay:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p0, v6}, Lcom/konka/tvsettings/popup/PvrActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " A"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->RPProgress:Lcom/konka/tvsettings/popup/TextProgressBar;

    invoke-virtual {v2}, Lcom/konka/tvsettings/popup/TextProgressBar;->getProgress()I

    move-result v2

    iput v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->A_progress:I

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->RPProgress:Lcom/konka/tvsettings/popup/TextProgressBar;

    invoke-virtual {v2}, Lcom/konka/tvsettings/popup/TextProgressBar;->getWidth()I

    move-result v2

    iget v3, p0, Lcom/konka/tvsettings/popup/PvrActivity;->A_progress:I

    mul-int/2addr v2, v3

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity;->RPProgress:Lcom/konka/tvsettings/popup/TextProgressBar;

    invoke-virtual {v3}, Lcom/konka/tvsettings/popup/TextProgressBar;->getMax()I

    move-result v3

    if-nez v3, :cond_0

    :goto_0
    div-int v0, v2, v1

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->lp4LoopAB:Landroid/widget/RelativeLayout$LayoutParams;

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iput v5, p0, Lcom/konka/tvsettings/popup/PvrActivity;->looptime:I

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->progress_loopab:Landroid/widget/ProgressBar;

    iget v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->looptime:I

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setMax(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->progress_loopab:Landroid/widget/ProgressBar;

    iget v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->looptime:I

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->progress_loopab:Landroid/widget/ProgressBar;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->lp4LoopAB:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->progress_loopab:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :goto_1
    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->RPProgress:Lcom/konka/tvsettings/popup/TextProgressBar;

    invoke-virtual {v1}, Lcom/konka/tvsettings/popup/TextProgressBar;->getMax()I

    move-result v1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->setPvrABLoop:Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

    sget-object v3, Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;->E_PVR_AB_LOOP_STATUS_A:Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/PvrDesk;->getCurPlaybackTimeInSecond()I

    move-result v2

    iput v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvrABLoopEndTime:I

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    iget v3, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvrABLoopStartTime:I

    iget v4, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvrABLoopEndTime:I

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/PvrDesk;->startPlaybackLoop(II)V

    sget-object v2, Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;->E_PVR_AB_LOOP_STATUS_AB:Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

    iput-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->setPvrABLoop:Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->textViewPlay:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p0, v6}, Lcom/konka/tvsettings/popup/PvrActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " A-B"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->lp4LoopAB:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity;->RPProgress:Lcom/konka/tvsettings/popup/TextProgressBar;

    invoke-virtual {v3}, Lcom/konka/tvsettings/popup/TextProgressBar;->getProgress()I

    move-result v3

    iget v4, p0, Lcom/konka/tvsettings/popup/PvrActivity;->A_progress:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/konka/tvsettings/popup/PvrActivity;->RPProgress:Lcom/konka/tvsettings/popup/TextProgressBar;

    invoke-virtual {v4}, Lcom/konka/tvsettings/popup/TextProgressBar;->getWidth()I

    move-result v4

    mul-int/2addr v3, v4

    iget-object v4, p0, Lcom/konka/tvsettings/popup/PvrActivity;->RPProgress:Lcom/konka/tvsettings/popup/TextProgressBar;

    invoke-virtual {v4}, Lcom/konka/tvsettings/popup/TextProgressBar;->getMax()I

    move-result v4

    if-nez v4, :cond_2

    :goto_2
    div-int v1, v3, v1

    iput v1, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->progress_loopab:Landroid/widget/ProgressBar;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->lp4LoopAB:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->looptime:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->looptime:I

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->progress_loopab:Landroid/widget/ProgressBar;

    iget v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->looptime:I

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setMax(I)V

    iput v5, p0, Lcom/konka/tvsettings/popup/PvrActivity;->currentlooptime:I

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->RPProgress:Lcom/konka/tvsettings/popup/TextProgressBar;

    invoke-virtual {v1}, Lcom/konka/tvsettings/popup/TextProgressBar;->getMax()I

    move-result v1

    goto :goto_2

    :cond_3
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->stopPlaybackLoop()V

    sget-object v1, Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;->E_PVR_AB_LOOP_STATUS_NONE:Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->setPvrABLoop:Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->textViewPlay:Landroid/widget/TextView;

    invoke-virtual {p0, v6}, Lcom/konka/tvsettings/popup/PvrActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->lp4LoopAB:Landroid/widget/RelativeLayout$LayoutParams;

    iput v5, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->progress_loopab:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v5}, Landroid/widget/ProgressBar;->setMax(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->progress_loopab:Landroid/widget/ProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto/16 :goto_1
.end method

.method private OnStopRecordConfirm()V
    .locals 6

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/TVRootApp;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v1

    const/4 v5, 0x0

    sput-object v5, Lcom/konka/tvsettings/popup/PvrActivity;->stopRecordDialog:Landroid/app/Dialog;

    :try_start_0
    iget-object v5, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/PvrDesk;->isTimeShiftRecording()Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/PvrDesk;->isPlaybacking()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->resumeLang()V

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->resumeSubtitle()V

    iget-object v5, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/PvrDesk;->stopPlayback()V

    iget-object v5, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/PvrDesk;->stopPlaybackLoop()V

    :cond_0
    const-string v5, "PvrActivity=====>>>stopTimeShift33\n"

    invoke-static {v5}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-boolean v5, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isAlwaysTimeShiftPlay:Z

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/PvrDesk;->stopTimeShift()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->saveAndExit()V

    iget-object v5, p0, Lcom/konka/tvsettings/popup/PvrActivity;->tPreviousEvent:Landroid/view/KeyEvent;

    invoke-virtual {v5}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :goto_1
    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->finish()V

    return-void

    :cond_2
    :try_start_1
    iget-object v5, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/PvrDesk;->isPlaybacking()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->resumeLang()V

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->resumeSubtitle()V

    iget-object v5, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/PvrDesk;->stopPlayback()V

    iget-object v5, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/PvrDesk;->stopPlaybackLoop()V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :sswitch_0
    invoke-interface {v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->programUp()Z

    goto :goto_1

    :sswitch_1
    invoke-interface {v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->programDown()Z

    goto :goto_1

    :sswitch_2
    invoke-interface {v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->programReturn()Z

    goto :goto_1

    :sswitch_3
    new-instance v3, Landroid/content/Intent;

    const-class v5, Lcom/konka/tvsettings/popup/InputActivity;

    invoke-direct {v3, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/popup/PvrActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x43 -> :sswitch_2
        0xa6 -> :sswitch_0
        0xa7 -> :sswitch_1
        0xb2 -> :sswitch_3
        0xfe -> :sswitch_2
    .end sparse-switch
.end method

.method private RemoveThumbnail()Z
    .locals 4

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity;->thumbnail:Lcom/konka/tvsettings/popup/PVRThumbnail;

    invoke-virtual {v3}, Lcom/konka/tvsettings/popup/PVRThumbnail;->isShown()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity;->thumbnail:Lcom/konka/tvsettings/popup/PVRThumbnail;

    invoke-virtual {v3}, Lcom/konka/tvsettings/popup/PVRThumbnail;->getSelectedItemPosition()I

    move-result v0

    const/4 v1, 0x0

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->thumbnail:Lcom/konka/tvsettings/popup/PVRThumbnail;

    invoke-virtual {v2, v0}, Lcom/konka/tvsettings/popup/PVRThumbnail;->removeViewAt(I)V

    const/4 v2, 0x1

    goto :goto_0
.end method

.method static synthetic access$0()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/konka/tvsettings/popup/PvrActivity;->DEBUG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1(Landroid/app/Dialog;)V
    .locals 0

    sput-object p0, Lcom/konka/tvsettings/popup/PvrActivity;->stopRecordDialog:Landroid/app/Dialog;

    return-void
.end method

.method static synthetic access$10(Lcom/konka/tvsettings/popup/PvrActivity;I)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/popup/PvrActivity;->getTimeString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$11(Lcom/konka/tvsettings/popup/PvrActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->looptime:I

    return v0
.end method

.method static synthetic access$12(Lcom/konka/tvsettings/popup/PvrActivity;)Landroid/widget/ProgressBar;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->progress_loopab:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$13(Lcom/konka/tvsettings/popup/PvrActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->totalRecordTime:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$14(Lcom/konka/tvsettings/popup/PvrActivity;)Landroid/widget/RelativeLayout$LayoutParams;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->lp4LoopAB:Landroid/widget/RelativeLayout$LayoutParams;

    return-object v0
.end method

.method static synthetic access$15(Lcom/konka/tvsettings/popup/PvrActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->A_progress:I

    return v0
.end method

.method static synthetic access$16(Lcom/konka/tvsettings/popup/PvrActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->looptime:I

    return-void
.end method

.method static synthetic access$17(Lcom/konka/tvsettings/popup/PvrActivity;)Z
    .locals 1

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->isFastBackPlaying()Z

    move-result v0

    return v0
.end method

.method static synthetic access$18(Lcom/konka/tvsettings/popup/PvrActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->updateUSBInfo()V

    return-void
.end method

.method static synthetic access$19(Lcom/konka/tvsettings/popup/PvrActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->saveAndExit()V

    return-void
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/popup/PvrActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->OnStopRecordConfirm()V

    return-void
.end method

.method static synthetic access$20(Lcom/konka/tvsettings/popup/PvrActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->createAnimation()V

    return-void
.end method

.method static synthetic access$21(Lcom/konka/tvsettings/popup/PvrActivity;)Landroid/widget/RelativeLayout;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordingView:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$22(Lcom/konka/tvsettings/popup/PvrActivity;)Landroid/animation/AnimatorSet;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordIconAnimation:Landroid/animation/AnimatorSet;

    return-object v0
.end method

.method static synthetic access$23(Lcom/konka/tvsettings/popup/PvrActivity;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordDiskPath:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$24(Lcom/konka/tvsettings/popup/PvrActivity;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordDiskLable:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$25(Lcom/konka/tvsettings/popup/PvrActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->usbLabel:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$26(Lcom/konka/tvsettings/popup/PvrActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordDiskPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$27(Lcom/konka/tvsettings/popup/PvrActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isOneTouchPauseMode:Z

    return v0
.end method

.method static synthetic access$28(Lcom/konka/tvsettings/popup/PvrActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isAlwaysTimeShiftBegin:Z

    return v0
.end method

.method static synthetic access$29(Lcom/konka/tvsettings/popup/PvrActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordDiskLable:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/kkinterface/tv/PvrDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    return-object v0
.end method

.method static synthetic access$30(Lcom/konka/tvsettings/popup/PvrActivity;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/popup/PvrActivity;->doPVRTimeShift(Z)V

    return-void
.end method

.method static synthetic access$31(Lcom/konka/tvsettings/popup/PvrActivity;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->activity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$32(Lcom/konka/tvsettings/popup/PvrActivity;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/popup/PvrActivity;->doPVRRecord(Z)V

    return-void
.end method

.method static synthetic access$33(Lcom/konka/tvsettings/popup/PvrActivity;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/konka/tvsettings/popup/PvrActivity;->saveChooseDiskSettings(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$34(Lcom/konka/tvsettings/popup/PvrActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->resumeLang()V

    return-void
.end method

.method static synthetic access$35(Lcom/konka/tvsettings/popup/PvrActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->resumeSubtitle()V

    return-void
.end method

.method static synthetic access$36(Lcom/konka/tvsettings/popup/PvrActivity;)Z
    .locals 1

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->isBootedByRecord()Z

    move-result v0

    return v0
.end method

.method static synthetic access$37(Lcom/konka/tvsettings/popup/PvrActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->standbySystem()V

    return-void
.end method

.method static synthetic access$38(Lcom/konka/tvsettings/popup/PvrActivity;I)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/popup/PvrActivity;->JumpToThumbnail(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$39(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/tvsettings/popup/PvrImageFlag;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvrImageFlag:Lcom/konka/tvsettings/popup/PvrImageFlag;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/popup/PvrActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isWatchRcodFilInRcoding:Z

    return v0
.end method

.method static synthetic access$40(Lcom/konka/tvsettings/popup/PvrActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isAlwaysTimeShiftPlay:Z

    return v0
.end method

.method static synthetic access$41(Lcom/konka/tvsettings/popup/PvrActivity;)Landroid/app/AlertDialog;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->timeChooser:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$43(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/kkinterface/tv/SettingDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->settingDesk:Lcom/konka/kkinterface/tv/SettingDesk;

    return-object v0
.end method

.method static synthetic access$44(Lcom/konka/tvsettings/popup/PvrActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isMenuHide:Z

    return v0
.end method

.method static synthetic access$45(Lcom/konka/tvsettings/popup/PvrActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isMenuHide:Z

    return-void
.end method

.method static synthetic access$46(Lcom/konka/tvsettings/popup/PvrActivity;)Landroid/widget/RelativeLayout;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->rootView:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$47(Lcom/konka/tvsettings/popup/PvrActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordingText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$48(Lcom/konka/tvsettings/popup/PvrActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/popup/PvrActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/tvsettings/popup/TextProgressBar;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->RPProgress:Lcom/konka/tvsettings/popup/TextProgressBar;

    return-object v0
.end method

.method static synthetic access$7(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->setPvrABLoop:Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

    return-object v0
.end method

.method static synthetic access$8(Lcom/konka/tvsettings/popup/PvrActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->currentlooptime:I

    return v0
.end method

.method static synthetic access$9(Lcom/konka/tvsettings/popup/PvrActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->currentlooptime:I

    return-void
.end method

.method private createAnimation()V
    .locals 15

    const/4 v14, 0x0

    const/4 v13, 0x0

    const/4 v12, 0x1

    const-wide/16 v10, 0x12c

    const/4 v9, 0x2

    iget-object v5, p0, Lcom/konka/tvsettings/popup/PvrActivity;->menuShowAnimation:Landroid/animation/AnimatorSet;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/konka/tvsettings/popup/PvrActivity;->menuHideAnimation:Landroid/animation/AnimatorSet;

    if-eqz v5, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v5, p0, Lcom/konka/tvsettings/popup/PvrActivity;->rootView:Landroid/widget/RelativeLayout;

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v5

    iget-object v6, p0, Lcom/konka/tvsettings/popup/PvrActivity;->rootView:Landroid/widget/RelativeLayout;

    invoke-virtual {v6}, Landroid/widget/RelativeLayout;->getPaddingBottom()I

    move-result v6

    add-int v2, v5, v6

    iget-object v5, p0, Lcom/konka/tvsettings/popup/PvrActivity;->rootView:Landroid/widget/RelativeLayout;

    const-string v6, "alpha"

    new-array v7, v9, [F

    fill-array-data v7, :array_0

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    new-instance v5, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v5}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {v0, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v5, p0, Lcom/konka/tvsettings/popup/PvrActivity;->rootView:Landroid/widget/RelativeLayout;

    const-string v6, "alpha"

    new-array v7, v9, [F

    fill-array-data v7, :array_1

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    new-instance v5, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v5}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {v1, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v5, p0, Lcom/konka/tvsettings/popup/PvrActivity;->rootView:Landroid/widget/RelativeLayout;

    const-string v6, "translationY"

    new-array v7, v9, [F

    int-to-float v8, v2

    aput v8, v7, v14

    aput v13, v7, v12

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    new-instance v5, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v4, v5}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {v4, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v5, p0, Lcom/konka/tvsettings/popup/PvrActivity;->rootView:Landroid/widget/RelativeLayout;

    const-string v6, "translationY"

    new-array v7, v9, [F

    aput v13, v7, v14

    int-to-float v8, v2

    aput v8, v7, v12

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    new-instance v5, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v3, v5}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {v3, v10, v11}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    new-instance v5, Landroid/animation/AnimatorSet;

    invoke-direct {v5}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v5, p0, Lcom/konka/tvsettings/popup/PvrActivity;->menuShowAnimation:Landroid/animation/AnimatorSet;

    new-instance v5, Landroid/animation/AnimatorSet;

    invoke-direct {v5}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v5, p0, Lcom/konka/tvsettings/popup/PvrActivity;->menuHideAnimation:Landroid/animation/AnimatorSet;

    new-instance v5, Landroid/animation/AnimatorSet;

    invoke-direct {v5}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v5, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordIconAnimation:Landroid/animation/AnimatorSet;

    iget-object v5, p0, Lcom/konka/tvsettings/popup/PvrActivity;->menuShowAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v5, v4}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    iget-object v5, p0, Lcom/konka/tvsettings/popup/PvrActivity;->menuHideAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v5, v3}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    iget-object v5, p0, Lcom/konka/tvsettings/popup/PvrActivity;->menuShowAnimation:Landroid/animation/AnimatorSet;

    new-instance v6, Lcom/konka/tvsettings/popup/PvrActivity$20;

    invoke-direct {v6, p0}, Lcom/konka/tvsettings/popup/PvrActivity$20;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;)V

    invoke-virtual {v5, v6}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v5, p0, Lcom/konka/tvsettings/popup/PvrActivity;->menuHideAnimation:Landroid/animation/AnimatorSet;

    new-instance v6, Lcom/konka/tvsettings/popup/PvrActivity$21;

    invoke-direct {v6, p0}, Lcom/konka/tvsettings/popup/PvrActivity$21;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;)V

    invoke-virtual {v5, v6}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v5, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordingView:Landroid/widget/RelativeLayout;

    const v6, 0x7f0701b4    # com.konka.tvsettings.R.id.pvrrecordimage

    invoke-virtual {v5, v6}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    const-string v6, "alpha"

    new-array v7, v9, [F

    fill-array-data v7, :array_2

    invoke-static {v5, v6, v7}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    new-instance v5, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v5}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    const-wide/16 v5, 0x7d0

    invoke-virtual {v1, v5, v6}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    const/4 v5, -0x1

    invoke-virtual {v1, v5}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    invoke-virtual {v1, v12}, Landroid/animation/ObjectAnimator;->setRepeatMode(I)V

    new-instance v5, Lcom/konka/tvsettings/popup/PvrActivity$22;

    invoke-direct {v5, p0}, Lcom/konka/tvsettings/popup/PvrActivity$22;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;)V

    invoke-virtual {v1, v5}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v5, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordIconAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v5, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    goto/16 :goto_0

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    :array_2
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method private dip2px(I)I
    .locals 3
    .param p1    # I

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v0, v1, Landroid/util/DisplayMetrics;->density:F

    int-to-float v1, p1

    mul-float/2addr v1, v0

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v1, v1

    return v1
.end method

.method private doPVRRecord(Z)V
    .locals 11
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const/16 v10, 0x1f4

    const/4 v7, 0x0

    if-eqz p1, :cond_6

    iget-object v6, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordingView:Landroid/widget/RelativeLayout;

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v6, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvrImageFlag:Lcom/konka/tvsettings/popup/PvrImageFlag;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lcom/konka/tvsettings/popup/PvrImageFlag;->setRecorderFlag(Z)V

    iget-object v6, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordIconAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v6}, Landroid/animation/AnimatorSet;->start()V

    iget-object v6, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/PvrDesk;->isRecordPaused()Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/PvrDesk;->resumeRecord()V

    :goto_0
    return-void

    :cond_0
    const-string v1, "FAT"

    const-string v4, "NTFS"

    :try_start_0
    iget-object v6, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordDiskPath:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_4

    const-string v6, "PVRFBActivity"

    const-string v7, "=============>>>>> USB Disk Path is NULL !!!"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    :cond_1
    :goto_1
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "PvrActivity=====>>>Start 333..."

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "..."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/PvrDesk;->startRecord()Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "PvrActivity=====>>>Start 444..."

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "..."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ">>>["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    sget-object v6, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_SUCCESS:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    if-eq v5, v6, :cond_3

    sget-object v6, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_SUCCESS_CI_PLUS_COPY_PROTECTION:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    if-eq v5, v6, :cond_3

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->ordinal()I

    move-result v2

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b003a    # com.konka.tvsettings.R.array.str_pvr_error_state

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v3

    iget-object v6, p0, Lcom/konka/tvsettings/popup/PvrActivity;->activity:Landroid/app/Activity;

    aget-object v7, v3, v2

    invoke-static {v6, v7, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    iget-object v6, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/PvrDesk;->stopRecord()V

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->isBootedByRecord()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->standbySystem()V

    :cond_2
    const-string v6, "PvrActivity=====>>>finish 1 \n"

    invoke-static {v6}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->finish()V

    :cond_3
    sget-object v6, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;->E_PVR_MODE_RECORD:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    iput-object v6, p0, Lcom/konka/tvsettings/popup/PvrActivity;->curPvrMode:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    goto/16 :goto_0

    :cond_4
    :try_start_1
    const-string v6, "PVRFBActivity"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "=============>>>>> USB Disk Path = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordDiskPath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordDiskLable:Ljava/lang/String;

    const/4 v7, 0x6

    const/4 v8, 0x0

    const/4 v9, 0x3

    invoke-virtual {v6, v7, v1, v8, v9}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v6

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    iget-object v7, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordDiskPath:Ljava/lang/String;

    const/4 v8, 0x2

    invoke-interface {v6, v7, v8}, Lcom/konka/kkinterface/tv/PvrDesk;->setPVRParas(Ljava/lang/String;S)Z

    goto/16 :goto_1

    :cond_5
    iget-object v6, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordDiskLable:Ljava/lang/String;

    const/4 v7, 0x6

    const/4 v8, 0x0

    const/4 v9, 0x4

    invoke-virtual {v6, v7, v4, v8, v9}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/konka/tvsettings/popup/PvrActivity;->activity:Landroid/app/Activity;

    const v7, 0x7f0a018a    # com.konka.tvsettings.R.string.str_pvr_unsurpt_flsystem

    const/16 v8, 0x1f4

    invoke-static {v6, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    :cond_6
    iget-object v6, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordingView:Landroid/widget/RelativeLayout;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v6, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordIconAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v6}, Landroid/animation/AnimatorSet;->end()V

    iget-object v6, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/PvrDesk;->pauseRecord()V

    goto/16 :goto_0
.end method

.method private doPVRTimeShift(Z)V
    .locals 6
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/PvrDesk;->stepInPlayback()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->setBarStatusOfStartTimeShift()V

    if-eqz p1, :cond_3

    iget-boolean v3, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isAlwaysTimeShiftBegin:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordingView:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordIconAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->start()V

    :cond_0
    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvrImageFlag:Lcom/konka/tvsettings/popup/PvrImageFlag;

    invoke-virtual {v3, v4}, Lcom/konka/tvsettings/popup/PvrImageFlag;->setRecorderFlag(Z)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/PvrDesk;->isRecordPaused()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/PvrDesk;->resumeRecord()V

    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    iget-object v4, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordDiskPath:Ljava/lang/String;

    const/4 v5, 0x2

    invoke-interface {v3, v4, v5}, Lcom/konka/kkinterface/tv/PvrDesk;->setPVRParas(Ljava/lang/String;S)Z

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/PvrDesk;->startTimeShiftRecord()Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "PvrActivity=====>>>Start 444...startTimeShiftRecord...>>>["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_SUCCESS:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    if-eq v2, v3, :cond_2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_SUCCESS_CI_PLUS_COPY_PROTECTION:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    if-eq v2, v3, :cond_2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->ordinal()I

    move-result v0

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b003a    # com.konka.tvsettings.R.array.str_pvr_error_state

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v1

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity;->activity:Landroid/app/Activity;

    aget-object v4, v1, v0

    const/16 v5, 0x1f4

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/PvrDesk;->stopTimeShift()V

    const-string v3, "PvrActivity=====>>>finish 22 \n"

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->finish()V

    :cond_2
    sget-object v3, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;->E_PVR_MODE_TIME_SHIFT:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    iput-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity;->curPvrMode:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordingView:Landroid/widget/RelativeLayout;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordIconAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->end()V

    const-string v3, "PvrActivity=====>>>stopTimeShiftRecord\n"

    invoke-static {v3}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/PvrDesk;->stopTimeShiftRecord()V

    goto :goto_0
.end method

.method private getAvaliableDiskForStandBy()Ljava/lang/String;
    .locals 2

    const-string v1, "/mnt/usb/"

    invoke-direct {p0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->getFirstUseableDiskAtParentDir(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getChooseDiskLable()Ljava/lang/String;
    .locals 3

    const-string v1, "save_setting_select"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/konka/tvsettings/popup/PvrActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "DISK_LABEL"

    const-string v2, "unknown"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getChooseDiskLablePcStyle()Ljava/lang/String;
    .locals 3

    const-string v1, "save_setting_select"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/konka/tvsettings/popup/PvrActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "DISK_LABEL_PC_STYLE"

    const-string v2, "unknown"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getChooseDiskPath()Ljava/lang/String;
    .locals 3

    const-string v1, "save_setting_select"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/konka/tvsettings/popup/PvrActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "DISK_PATH"

    const-string v2, "unknown"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getChooseDiskSettings()Z
    .locals 3

    const/4 v2, 0x0

    const-string v1, "save_setting_select"

    invoke-virtual {p0, v1, v2}, Lcom/konka/tvsettings/popup/PvrActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "IS_ALREADY_CHOOSE_DISK"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method private getFileSystem(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/lang/String;

    if-nez p1, :cond_1

    const-string v5, ""

    :cond_0
    :goto_0
    return-object v5

    :cond_1
    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/FileReader;

    invoke-direct {v5, p1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_a
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_9
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v4

    :goto_1
    if-nez v4, :cond_3

    if-eqz v1, :cond_a

    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_8

    move-object v0, v1

    :cond_2
    :goto_2
    const-string v5, ""

    goto :goto_0

    :cond_3
    :try_start_3
    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x1

    aget-object v5, v3, v5

    invoke-virtual {v5, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    const/4 v5, 0x2

    aget-object v5, v3, v5

    const-string v6, "ntfs3g"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_a
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_9
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v5

    if-eqz v5, :cond_5

    if-eqz v1, :cond_4

    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    :cond_4
    :goto_3
    const-string v5, "NTFS"

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    :cond_5
    const/4 v5, 0x2

    :try_start_5
    aget-object v5, v3, v5

    const-string v6, "vfat"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_a
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_9
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result v5

    if-eqz v5, :cond_7

    if-eqz v1, :cond_6

    :try_start_6
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    :cond_6
    :goto_4
    const-string v5, "FAT"

    goto :goto_0

    :catch_1
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    :cond_7
    const/4 v5, 0x2

    :try_start_7
    aget-object v5, v3, v5
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_a
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_9
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    if-eqz v1, :cond_0

    :try_start_8
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_2

    goto :goto_0

    :catch_2
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    :cond_8
    :try_start_9
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_9
    .catch Ljava/io/FileNotFoundException; {:try_start_9 .. :try_end_9} :catch_a
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    move-result-object v4

    goto :goto_1

    :catch_3
    move-exception v2

    :goto_5
    :try_start_a
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    if-eqz v0, :cond_2

    :try_start_b
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_4

    goto :goto_2

    :catch_4
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    :catch_5
    move-exception v2

    :goto_6
    :try_start_c
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    if-eqz v0, :cond_2

    :try_start_d
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_6

    goto :goto_2

    :catch_6
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    :catchall_0
    move-exception v5

    :goto_7
    if-eqz v0, :cond_9

    :try_start_e
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_7

    :cond_9
    :goto_8
    throw v5

    :catch_7
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_8

    :catch_8
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    :cond_a
    move-object v0, v1

    goto/16 :goto_2

    :catchall_1
    move-exception v5

    move-object v0, v1

    goto :goto_7

    :catch_9
    move-exception v2

    move-object v0, v1

    goto :goto_6

    :catch_a
    move-exception v2

    move-object v0, v1

    goto :goto_5
.end method

.method private getFirstUseableDiskAtParentDir(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1    # Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_0

    new-instance v1, Lcom/konka/tvsettings/popup/PvrActivity$DirectoryFilter;

    invoke-direct {v1, v5}, Lcom/konka/tvsettings/popup/PvrActivity$DirectoryFilter;-><init>(Lcom/konka/tvsettings/popup/PvrActivity$DirectoryFilter;)V

    invoke-virtual {v0, v1}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v3

    array-length v6, v3

    add-int/lit8 v2, v6, -0x1

    :goto_0
    if-gez v2, :cond_1

    :cond_0
    :goto_1
    return-object v5

    :cond_1
    aget-object v4, v3, v2

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "/_MSTPVR/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/konka/tvsettings/popup/PvrActivity;->isFileExisted(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    new-instance v6, Ljava/lang/String;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-static {v6}, Lcom/konka/tvsettings/function/UsbReceiver;->isDiskExisted(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    :cond_2
    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :cond_3
    add-int/lit8 v2, v2, -0x1

    goto :goto_0
.end method

.method private getTimeString(I)Ljava/lang/String;
    .locals 7
    .param p1    # I

    const/16 v6, 0xa

    const-string v0, "00"

    const-string v1, "00"

    const-string v3, "00"

    rem-int/lit8 v4, p1, 0x3c

    if-ge v4, v6, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "0"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    rem-int/lit8 v5, p1, 0x3c

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_0
    div-int/lit8 v2, p1, 0x3c

    rem-int/lit8 v4, v2, 0x3c

    if-ge v4, v6, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "0"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    rem-int/lit8 v5, v2, 0x3c

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_1
    div-int/lit16 v2, p1, 0xe10

    if-ge v2, v6, :cond_2

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "0"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4

    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    rem-int/lit8 v5, p1, 0x3c

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    rem-int/lit8 v5, v2, 0x3c

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method private getUsbIndexByPath(Ljava/lang/String;)I
    .locals 19
    .param p1    # Ljava/lang/String;

    invoke-static/range {p0 .. p0}, Lcom/mstar/android/storage/MStorageManager;->getInstance(Landroid/content/Context;)Lcom/mstar/android/storage/MStorageManager;

    move-result-object v11

    invoke-virtual {v11}, Lcom/mstar/android/storage/MStorageManager;->getVolumePaths()[Ljava/lang/String;

    move-result-object v16

    const/16 v17, 0x10

    move/from16 v0, v17

    new-array v12, v0, [Ljava/lang/String;

    const/16 v17, 0x0

    const-string v18, "C"

    aput-object v18, v12, v17

    const/16 v17, 0x1

    const-string v18, "D"

    aput-object v18, v12, v17

    const/16 v17, 0x2

    const-string v18, "E"

    aput-object v18, v12, v17

    const/16 v17, 0x3

    const-string v18, "F"

    aput-object v18, v12, v17

    const/16 v17, 0x4

    const-string v18, "G"

    aput-object v18, v12, v17

    const/16 v17, 0x5

    const-string v18, "H"

    aput-object v18, v12, v17

    const/16 v17, 0x6

    const-string v18, "I"

    aput-object v18, v12, v17

    const/16 v17, 0x7

    const-string v18, "J"

    aput-object v18, v12, v17

    const/16 v17, 0x8

    const-string v18, "K"

    aput-object v18, v12, v17

    const/16 v17, 0x9

    const-string v18, "L"

    aput-object v18, v12, v17

    const/16 v17, 0xa

    const-string v18, "M"

    aput-object v18, v12, v17

    const/16 v17, 0xb

    const-string v18, "N"

    aput-object v18, v12, v17

    const/16 v17, 0xc

    const-string v18, "O"

    aput-object v18, v12, v17

    const/16 v17, 0xd

    const-string v18, "P"

    aput-object v18, v12, v17

    const/16 v17, 0xe

    const-string v18, "Q"

    aput-object v18, v12, v17

    const/16 v17, 0xf

    const-string v18, "R"

    aput-object v18, v12, v17

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->usbDriverLabel:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->clear()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->usbDriverPath:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->clear()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->usbDriverLabelPcStyle:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->clear()V

    if-nez v16, :cond_1

    const-string v17, "PVR"

    const-string v18, "=============>>>>> volumes == null..."

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, -0x1

    :cond_0
    :goto_0
    return v4

    :cond_1
    new-instance v3, Ljava/io/File;

    const-string v17, "proc/mounts"

    move-object/from16 v0, v17

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v17

    if-eqz v17, :cond_2

    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v17

    if-eqz v17, :cond_3

    :cond_2
    const-string v17, "PVR"

    const-string v18, "=============>>>>> file == null..."

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x0

    :cond_3
    const/4 v4, 0x1

    :goto_1
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    if-lt v4, v0, :cond_6

    const-string v17, "/"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_4

    const/16 v17, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    :cond_4
    const-string v17, "/"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_5

    const/16 v17, 0x0

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v18

    add-int/lit8 v18, v18, -0x1

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    :cond_5
    const/4 v4, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->usbDriverPath:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v17

    move/from16 v0, v17

    if-lt v4, v0, :cond_d

    const/4 v4, -0x1

    goto :goto_0

    :cond_6
    aget-object v17, v16, v4

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Lcom/mstar/android/storage/MStorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_7

    const-string v17, "mounted"

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_8

    :cond_7
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_8
    aget-object v8, v16, v4

    const-string v17, "/"

    move-object/from16 v0, v17

    invoke-virtual {v8, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    array-length v0, v9

    move/from16 v17, v0

    add-int/lit8 v17, v17, -0x1

    aget-object v6, v9, v17

    invoke-virtual {v11, v8}, Lcom/mstar/android/storage/MStorageManager;->getVolumeLabel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    if-eqz v15, :cond_9

    const-string v17, " "

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    const-string v15, ""

    const/4 v5, 0x0

    :goto_4
    array-length v0, v13

    move/from16 v17, v0

    move/from16 v0, v17

    if-lt v5, v0, :cond_a

    :cond_9
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v18, ": "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v8}, Lcom/konka/tvsettings/popup/PvrActivity;->getFileSystem(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    array-length v0, v12

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v4, v0, :cond_c

    if-lez v4, :cond_c

    add-int/lit8 v17, v4, -0x1

    aget-object v7, v12, v17

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v18, ": "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v8}, Lcom/konka/tvsettings/popup/PvrActivity;->getFileSystem(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->usbDriverLabelPcStyle:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v14, v7}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->usbDriverLabel:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v14, v6}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->usbDriverPath:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v14, v8}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_3

    :cond_a
    array-length v0, v13

    move/from16 v17, v0

    add-int/lit8 v17, v17, -0x1

    move/from16 v0, v17

    if-eq v5, v0, :cond_b

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-static {v15}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v18, v13, v5

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    :goto_6
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_4

    :cond_b
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-static {v15}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v18, v13, v5

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    goto :goto_6

    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->usbDriverLabelPcStyle:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v14, v6}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_5

    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->usbDriverPath:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_0

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_2
.end method

.method private getUsbPathByPcLabel(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1    # Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {p0}, Lcom/mstar/android/storage/MStorageManager;->getInstance(Landroid/content/Context;)Lcom/mstar/android/storage/MStorageManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/storage/MStorageManager;->getVolumePaths()[Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x10

    new-array v2, v4, [Ljava/lang/String;

    const-string v4, "C"

    aput-object v4, v2, v7

    const/4 v4, 0x1

    const-string v5, "D"

    aput-object v5, v2, v4

    const/4 v4, 0x2

    const-string v5, "E"

    aput-object v5, v2, v4

    const/4 v4, 0x3

    const-string v5, "F"

    aput-object v5, v2, v4

    const/4 v4, 0x4

    const-string v5, "G"

    aput-object v5, v2, v4

    const/4 v4, 0x5

    const-string v5, "H"

    aput-object v5, v2, v4

    const/4 v4, 0x6

    const-string v5, "I"

    aput-object v5, v2, v4

    const/4 v4, 0x7

    const-string v5, "J"

    aput-object v5, v2, v4

    const/16 v4, 0x8

    const-string v5, "K"

    aput-object v5, v2, v4

    const/16 v4, 0x9

    const-string v5, "L"

    aput-object v5, v2, v4

    const/16 v4, 0xa

    const-string v5, "M"

    aput-object v5, v2, v4

    const/16 v4, 0xb

    const-string v5, "N"

    aput-object v5, v2, v4

    const/16 v4, 0xc

    const-string v5, "O"

    aput-object v5, v2, v4

    const/16 v4, 0xd

    const-string v5, "P"

    aput-object v5, v2, v4

    const/16 v4, 0xe

    const-string v5, "Q"

    aput-object v5, v2, v4

    const/16 v4, 0xf

    const-string v5, "R"

    aput-object v5, v2, v4

    const-string v4, "unknown"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, ""

    :goto_0
    return-object v4

    :cond_0
    const/4 v0, 0x0

    :goto_1
    array-length v4, v2

    if-lt v0, v4, :cond_2

    :cond_1
    const-string v4, ""

    goto :goto_0

    :cond_2
    array-length v4, v3

    add-int/lit8 v4, v4, -0x1

    if-ge v0, v4, :cond_1

    aget-object v4, v2, v0

    const-string v5, ":"

    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    aget-object v5, v5, v7

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string v4, "PVR"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "XXX==========>>>>>getUsbPathByPcLabel-["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v6, v2, v0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {p1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v6, v0, 0x1

    aget-object v6, v3, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v4, v0, 0x1

    aget-object v4, v3, v4

    goto :goto_0

    :cond_3
    const-string v4, "PVR"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "XXX==========>>>>> getUsbPathByPcLabel-["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v6, v2, v0

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {p1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    aget-object v6, v6, v7

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    add-int/lit8 v6, v0, 0x1

    aget-object v6, v3, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1
.end method

.method private isBootedByRecord()Z
    .locals 2

    const/4 v0, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PvrManager;->getIsBootByRecord()Z

    move-result v0

    :cond_0
    return v0
.end method

.method private isFastBackPlaying()Z
    .locals 5

    const/4 v2, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/PvrDesk;->getPlaybackSpeed()Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_32XFB:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_0

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_1XFB:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->ordinal()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    if-gt v3, v4, :cond_0

    const/4 v2, 0x1

    :cond_0
    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private isFileExisted(Ljava/lang/String;)Z
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    const-string v2, ""

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private menuHide()V
    .locals 3

    const-string v1, "PVR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "=============>>>> menuHide = "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isMenuHide:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isMenuHide:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->menuShowAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->menuHideAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private menuShow()V
    .locals 3

    const-string v0, "PVR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "=============>>>> menuShow = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isMenuHide:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isMenuHide:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->menuHideAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->menuShowAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    :cond_0
    return-void
.end method

.method private resumeLang()V
    .locals 7

    const/4 v6, -0x1

    new-instance v1, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;

    invoke-direct {v1}, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;-><init>()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/TVRootApp;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->getAudioInfo()Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-short v4, p0, Lcom/konka/tvsettings/popup/PvrActivity;->audioLangPosLive:S

    if-eq v4, v6, :cond_0

    iget-short v4, v1, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;->audioLangNum:S

    iget-short v5, p0, Lcom/konka/tvsettings/popup/PvrActivity;->audioLangPosLive:S

    if-le v4, v5, :cond_0

    iget-short v4, v1, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;->currentAudioIndex:S

    iget-short v5, p0, Lcom/konka/tvsettings/popup/PvrActivity;->audioLangPosLive:S

    if-eq v4, v5, :cond_0

    iget-short v4, p0, Lcom/konka/tvsettings/popup/PvrActivity;->audioLangPosLive:S

    invoke-interface {v2, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->switchAudioTrack(I)V

    iput-short v6, p0, Lcom/konka/tvsettings/popup/PvrActivity;->audioLangPosLive:S

    :cond_0
    return-void
.end method

.method private resumeSubtitle()V
    .locals 9

    const/4 v8, -0x1

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/TVRootApp;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->getSubtitleInfo()Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;

    move-result-object v5

    if-eqz v5, :cond_0

    iget v6, p0, Lcom/konka/tvsettings/popup/PvrActivity;->subtitlePosLive:I

    if-eq v6, v8, :cond_0

    iget v6, p0, Lcom/konka/tvsettings/popup/PvrActivity;->subtitlePosLive:I

    iget-short v7, v5, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->subtitleServiceNumber:S

    if-gt v6, v7, :cond_0

    iget v6, p0, Lcom/konka/tvsettings/popup/PvrActivity;->subtitlePosLive:I

    iget-short v7, v5, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->currentSubtitleIndex:S

    add-int/lit8 v7, v7, 0x1

    if-eq v6, v7, :cond_0

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->closeSubtitle()Z

    iget v6, p0, Lcom/konka/tvsettings/popup/PvrActivity;->subtitlePosLive:I

    add-int/lit8 v6, v6, -0x1

    invoke-interface {v1, v6}, Lcom/konka/kkinterface/tv/ChannelDesk;->openSubtitle(I)Z

    const-string v6, "TvSetting"

    const/4 v7, 0x0

    invoke-virtual {p0, v6, v7}, Lcom/konka/tvsettings/popup/PvrActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v6, "subtitlePos"

    iget v7, p0, Lcom/konka/tvsettings/popup/PvrActivity;->subtitlePosLive:I

    invoke-interface {v2, v6, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iput v8, p0, Lcom/konka/tvsettings/popup/PvrActivity;->subtitlePosLive:I

    :cond_0
    return-void
.end method

.method private saveAndExit()V
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/PvrActivity;->showDialog(I)V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/tvsettings/popup/PvrActivity$19;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/popup/PvrActivity$19;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private saveChooseDiskSettings(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Z
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    const-string v2, "save_setting_select"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/konka/tvsettings/popup/PvrActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "IS_ALREADY_CHOOSE_DISK"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    const-string v2, "DISK_PATH"

    invoke-interface {v0, v2, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v2, "DISK_LABEL"

    invoke-interface {v0, v2, p3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v2, "DISK_LABEL_PC_STYLE"

    invoke-interface {v0, v2, p4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method private showStopRecordDialog()Z
    .locals 5

    const/4 v4, 0x0

    const/4 v0, 0x1

    sget-object v2, Lcom/konka/tvsettings/popup/PvrActivity;->stopRecordDialog:Landroid/app/Dialog;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/konka/tvsettings/popup/PvrActivity;->stopRecordDialog:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/konka/tvsettings/popup/PvrActivity;->DEBUG_TAG:Ljava/lang/String;

    const-string v3, "StopRecordDialog allready exist"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0a0186    # com.konka.tvsettings.R.string.str_stop_record_dialog_title

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0187    # com.konka.tvsettings.R.string.str_stop_record_dialog_message

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0188    # com.konka.tvsettings.R.string.str_stop_record_dialog_confirm

    new-instance v3, Lcom/konka/tvsettings/popup/PvrActivity$OnStopRecordConfirmClickListener;

    invoke-direct {v3, p0, v4}, Lcom/konka/tvsettings/popup/PvrActivity$OnStopRecordConfirmClickListener;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;Lcom/konka/tvsettings/popup/PvrActivity$OnStopRecordConfirmClickListener;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0a0189    # com.konka.tvsettings.R.string.str_stop_record_dialog_cancel

    new-instance v3, Lcom/konka/tvsettings/popup/PvrActivity$OnStopRecordCancelClickListener;

    invoke-direct {v3, p0, v4}, Lcom/konka/tvsettings/popup/PvrActivity$OnStopRecordCancelClickListener;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;Lcom/konka/tvsettings/popup/PvrActivity$OnStopRecordCancelClickListener;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    if-nez v1, :cond_1

    sget-object v2, Lcom/konka/tvsettings/popup/PvrActivity;->DEBUG_TAG:Ljava/lang/String;

    const-string v3, "showStopRecordDialog -- AlertDialog.Builder init fail"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    sput-object v2, Lcom/konka/tvsettings/popup/PvrActivity;->stopRecordDialog:Landroid/app/Dialog;

    sget-object v2, Lcom/konka/tvsettings/popup/PvrActivity;->stopRecordDialog:Landroid/app/Dialog;

    if-nez v2, :cond_2

    sget-object v2, Lcom/konka/tvsettings/popup/PvrActivity;->DEBUG_TAG:Ljava/lang/String;

    const-string v3, "showStopRecordDialog -- AlertDialog.Builder create dialog fail"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-boolean v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isAlwaysTimeShiftBegin:Z

    if-nez v2, :cond_3

    iget-boolean v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isAlwaysTimeShiftPlay:Z

    if-eqz v2, :cond_4

    :cond_3
    sget-object v2, Lcom/konka/tvsettings/popup/PvrActivity;->DEBUG_TAG:Ljava/lang/String;

    const-string v3, "StopRecordDialog dismiss"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v2, Lcom/konka/tvsettings/popup/PvrActivity;->stopRecordDialog:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->dismiss()V

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->OnStopRecordConfirm()V

    goto :goto_0

    :cond_4
    sget-object v2, Lcom/konka/tvsettings/popup/PvrActivity;->DEBUG_TAG:Ljava/lang/String;

    const-string v3, "StopRecordDialog Show"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v2, Lcom/konka/tvsettings/popup/PvrActivity;->stopRecordDialog:Landroid/app/Dialog;

    invoke-virtual {v2}, Landroid/app/Dialog;->show()V

    goto :goto_0
.end method

.method private standbySystem()V
    .locals 2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/PvrManager;->setIsBootByRecord(Z)V

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getInstance()Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v0

    const-string v1, "vpr"

    invoke-virtual {v0, v1}, Lcom/mstar/android/tv/TvCommonManager;->standbySystem(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private updateUSBInfo()V
    .locals 6

    :try_start_0
    new-instance v2, Landroid/os/StatFs;

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordDiskPath:Ljava/lang/String;

    invoke-direct {v2, v3}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2}, Landroid/os/StatFs;->getFreeBlocks()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2}, Landroid/os/StatFs;->getBlockCount()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v4, v5

    sub-float/2addr v3, v4

    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v3, v4

    float-to-int v1, v3

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity;->usbPercentage:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity;->usbFreeSpace:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v1}, Landroid/widget/ProgressBar;->setProgress(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public getBestDiskPath()Ljava/lang/String;
    .locals 6

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->getChooseDiskSettings()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->getChooseDiskPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->getChooseDiskLablePcStyle()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/konka/tvsettings/popup/PvrActivity;->getUsbPathByPcLabel(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "/_MSTPVR"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/konka/tvsettings/popup/PvrActivity;->isFileExisted(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Lcom/konka/tvsettings/function/UsbReceiver;->isDiskExisted(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_0
    move-object v0, v2

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "/_MSTPVR"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/konka/tvsettings/popup/PvrActivity;->isFileExisted(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Lcom/konka/tvsettings/function/UsbReceiver;->isDiskExisted(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_3
    move-object v0, v3

    goto :goto_0

    :cond_4
    const-string v1, "/mnt/usb/"

    invoke-direct {p0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->getFirstUseableDiskAtParentDir(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "NO_DISK"

    goto :goto_0

    :cond_5
    iget-boolean v4, p0, Lcom/konka/tvsettings/popup/PvrActivity;->AutoRecord:Z

    if-eqz v4, :cond_6

    const-string v1, "/mnt/usb/"

    invoke-direct {p0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->getFirstUseableDiskAtParentDir(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "NO_DISK"

    goto :goto_0

    :cond_6
    const-string v0, "CHOOSE_DISK"

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 7

    const/4 v0, 0x0

    :try_start_0
    iget-object v4, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/PvrDesk;->isTimeShiftRecording()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    iget-object v4, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvrImageFlag:Lcom/konka/tvsettings/popup/PvrImageFlag;

    invoke-virtual {v4}, Lcom/konka/tvsettings/popup/PvrImageFlag;->isRecorderFlag()Z

    move-result v4

    if-nez v4, :cond_0

    if-eqz v0, :cond_5

    :cond_0
    iget-boolean v4, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isAlwaysTimeShiftBegin:Z

    if-eqz v4, :cond_1

    if-eqz v0, :cond_1

    const-string v4, "PVRActivity"

    const-string v5, "Do not exit at AlwaysTimeShift mode \n"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->finish()V

    :goto_1
    return-void

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0a0180    # com.konka.tvsettings.R.string.str_root_alert_dialog_title

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f0a00f0    # com.konka.tvsettings.R.string.str_pvr_exit_confirm

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f0a0182    # com.konka.tvsettings.R.string.str_root_alert_dialog_confirm

    new-instance v6, Lcom/konka/tvsettings/popup/PvrActivity$15;

    invoke-direct {v6, p0}, Lcom/konka/tvsettings/popup/PvrActivity$15;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f0a0183    # com.konka.tvsettings.R.string.str_root_alert_dialog_cancel

    new-instance v6, Lcom/konka/tvsettings/popup/PvrActivity$16;

    invoke-direct {v6, p0}, Lcom/konka/tvsettings/popup/PvrActivity$16;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    iget-boolean v4, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isAlwaysTimeShiftPlay:Z

    if-eqz v4, :cond_4

    :try_start_1
    iget-object v4, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/PvrDesk;->isPlaybacking()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->resumeLang()V

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->resumeSubtitle()V

    iget-object v4, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/PvrDesk;->stopPlayback()V

    iget-object v4, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/PvrDesk;->stopPlaybackLoop()V

    :cond_2
    iget-object v4, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/PvrDesk;->isTimeShiftRecording()Z

    move-result v4

    if-eqz v4, :cond_3

    iget-boolean v4, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isAlwaysTimeShiftPlay:Z

    if-nez v4, :cond_3

    const-string v4, "PvrActivity=====>>>stopTimeShift555n"

    invoke-static {v4}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/PvrDesk;->stopTimeShift()V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_3
    :goto_2
    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->saveAndExit()V

    goto :goto_1

    :catch_1
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2

    :cond_4
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_1

    :cond_5
    :try_start_2
    iget-boolean v4, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isOneTouchPlayMode:Z

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/PvrDesk;->isPlaybacking()Z

    move-result v4

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/PvrDesk;->stopPlayback()V

    iget-object v4, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/PvrDesk;->stopPlaybackLoop()V
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_6
    :goto_3
    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->isBootedByRecord()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->standbySystem()V

    :cond_7
    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->finish()V

    goto/16 :goto_1

    :cond_8
    :try_start_3
    iget-object v4, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/PvrDesk;->isPlaybacking()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->resumeLang()V

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->resumeSubtitle()V

    iget-object v4, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/PvrDesk;->stopPlayback()V

    iget-object v4, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/PvrDesk;->stopPlaybackLoop()V

    iget-object v4, p0, Lcom/konka/tvsettings/popup/PvrActivity;->settingDesk:Lcom/konka/kkinterface/tv/SettingDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/SettingDesk;->getAlTimeShift()Z

    move-result v4

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/PvrDesk;->startTimeShiftRecord()Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_3

    :catch_2
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_3
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 35
    .param p1    # Landroid/os/Bundle;

    const-string v31, "PvrActivity=====onCreate 1"

    invoke-static/range {v31 .. v31}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "PvrActivity=====>>>Start 555..."

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v32

    invoke-virtual/range {v31 .. v33}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, "..."

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v31 .. v31}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-super/range {p0 .. p1}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onCreate(Landroid/os/Bundle;)V

    const v31, 0x7f03003f    # com.konka.tvsettings.R.layout.pvr_menu

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->setContentView(I)V

    const/16 v31, 0x1

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->alwaysTimeout:Z

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->activity:Landroid/app/Activity;

    const/16 v31, 0x1

    sput-boolean v31, Lcom/konka/tvsettings/popup/PvrActivity;->isPVRActivityActive:Z

    invoke-virtual/range {p0 .. p0}, Lcom/konka/tvsettings/popup/PvrActivity;->getApplication()Landroid/app/Application;

    move-result-object v4

    check-cast v4, Lcom/konka/tvsettings/TVRootApp;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v28

    invoke-interface/range {v28 .. v28}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPvrManagerInstance()Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface/range {v28 .. v28}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->settingDesk:Lcom/konka/kkinterface/tv/SettingDesk;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/PvrDesk;->setOnPvrEventListener(Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;)V

    new-instance v31, Lcom/konka/tvsettings/popup/PvrImageFlag;

    invoke-direct/range {v31 .. v31}, Lcom/konka/tvsettings/popup/PvrImageFlag;-><init>()V

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->pvrImageFlag:Lcom/konka/tvsettings/popup/PvrImageFlag;

    const v31, 0x7f070197    # com.konka.tvsettings.R.id.pvrrootmenu

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->rootView:Landroid/widget/RelativeLayout;

    const-string v31, "PvrActivity=====>>>onCreate 2"

    invoke-static/range {v31 .. v31}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const v31, 0x7f0701b3    # com.konka.tvsettings.R.id.pvrisrecording

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->recordingView:Landroid/widget/RelativeLayout;

    const-string v31, "PvrActivity=====>>>onCreate 3"

    invoke-static/range {v31 .. v31}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const v31, 0x7f0701a2    # com.konka.tvsettings.R.id.player_recorder

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/ImageView;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->recorder:Landroid/widget/ImageView;

    const v31, 0x7f0701a3    # com.konka.tvsettings.R.id.player_play

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/ImageView;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->play:Landroid/widget/ImageView;

    const v31, 0x7f0701a5    # com.konka.tvsettings.R.id.player_stop

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/ImageView;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->stop:Landroid/widget/ImageView;

    const v31, 0x7f0701a6    # com.konka.tvsettings.R.id.player_pause

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/ImageView;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->pause:Landroid/widget/ImageView;

    const v31, 0x7f0701a7    # com.konka.tvsettings.R.id.player_rev

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/ImageView;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->rev:Landroid/widget/ImageView;

    const v31, 0x7f0701a9    # com.konka.tvsettings.R.id.player_ff

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/ImageView;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->ff:Landroid/widget/ImageView;

    const v31, 0x7f0701ab    # com.konka.tvsettings.R.id.player_slow

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/ImageView;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->slow:Landroid/widget/ImageView;

    const v31, 0x7f0701ad    # com.konka.tvsettings.R.id.player_time

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/ImageView;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->time:Landroid/widget/ImageView;

    const v31, 0x7f070199    # com.konka.tvsettings.R.id.textView1

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/TextView;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->serviceNameText:Landroid/widget/TextView;

    const v31, 0x7f07019a    # com.konka.tvsettings.R.id.textView2

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/TextView;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->eventNameText:Landroid/widget/TextView;

    const v31, 0x7f07019f    # com.konka.tvsettings.R.id.record_time

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/TextView;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->totalRecordTime:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->totalRecordTime:Landroid/widget/TextView;

    move-object/from16 v31, v0

    const-string v32, "00:00:00"

    invoke-virtual/range {v31 .. v32}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v31, 0x7f0701b2    # com.konka.tvsettings.R.id.usbLabelName

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/TextView;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->usbLabel:Landroid/widget/TextView;

    const v31, 0x7f0701b1    # com.konka.tvsettings.R.id.usbFreeSpacePercent

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/TextView;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->usbPercentage:Landroid/widget/TextView;

    const v31, 0x7f07019d    # com.konka.tvsettings.R.id.play_record_progress

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Lcom/konka/tvsettings/popup/TextProgressBar;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->RPProgress:Lcom/konka/tvsettings/popup/TextProgressBar;

    const v31, 0x7f0701b0    # com.konka.tvsettings.R.id.usbFreeSpace

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/ProgressBar;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->usbFreeSpace:Landroid/widget/ProgressBar;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->usbFreeSpace:Landroid/widget/ProgressBar;

    move-object/from16 v31, v0

    const/16 v32, 0x64

    invoke-virtual/range {v31 .. v32}, Landroid/widget/ProgressBar;->setMax(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->pvrImageFlag:Lcom/konka/tvsettings/popup/PvrImageFlag;

    move-object/from16 v31, v0

    const/16 v32, 0x1

    invoke-virtual/range {v31 .. v32}, Lcom/konka/tvsettings/popup/PvrImageFlag;->setPauseFlag(Z)V

    const v31, 0x7f0701a4    # com.konka.tvsettings.R.id.text_view_player_play

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/TextView;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->textViewPlay:Landroid/widget/TextView;

    const v31, 0x7f07019e    # com.konka.tvsettings.R.id.progressbar_loopab

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/ProgressBar;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->progress_loopab:Landroid/widget/ProgressBar;

    new-instance v31, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v32, 0x0

    const/16 v33, 0x5

    move-object/from16 v0, p0

    move/from16 v1, v33

    invoke-direct {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->dip2px(I)I

    move-result v33

    invoke-direct/range {v31 .. v33}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->lp4LoopAB:Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->lp4LoopAB:Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v31, v0

    const/16 v32, 0x5

    move/from16 v0, v32

    move-object/from16 v1, v31

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    const v31, 0x7f0701a8    # com.konka.tvsettings.R.id.text_view_player_rev

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/TextView;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->textViewRev:Landroid/widget/TextView;

    const v31, 0x7f0701aa    # com.konka.tvsettings.R.id.text_view_player_ff

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/TextView;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->textViewFf:Landroid/widget/TextView;

    const v31, 0x7f0701ac    # com.konka.tvsettings.R.id.text_view_player_slow

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/TextView;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->textViewSlow:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->recordingView:Landroid/widget/RelativeLayout;

    move-object/from16 v31, v0

    const v32, 0x7f0701b5    # com.konka.tvsettings.R.id.pvrrecordtext

    invoke-virtual/range {v31 .. v32}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/TextView;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->recordingText:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->recordingView:Landroid/widget/RelativeLayout;

    move-object/from16 v31, v0

    const v32, 0x7f0701b6    # com.konka.tvsettings.R.id.pvrPlaybacktext

    invoke-virtual/range {v31 .. v32}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/TextView;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    move-object/from16 v31, v0

    const-string v32, ""

    invoke-virtual/range {v31 .. v32}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v31, 0x7f0701b4    # com.konka.tvsettings.R.id.pvrrecordimage

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/ImageView;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->m_RecordingImage:Landroid/widget/ImageView;

    new-instance v31, Lcom/konka/tvsettings/popup/PvrActivity$RootMenuEventReceiver;

    const/16 v32, 0x0

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    move-object/from16 v2, v32

    invoke-direct {v0, v1, v2}, Lcom/konka/tvsettings/popup/PvrActivity$RootMenuEventReceiver;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;Lcom/konka/tvsettings/popup/PvrActivity$RootMenuEventReceiver;)V

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->rootmenuEventReceiver:Lcom/konka/tvsettings/popup/PvrActivity$RootMenuEventReceiver;

    new-instance v17, Landroid/content/IntentFilter;

    invoke-direct/range {v17 .. v17}, Landroid/content/IntentFilter;-><init>()V

    const-string v31, "com.konka.tv.action.PVR_STOP_RECORD"

    move-object/from16 v0, v17

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->rootmenuEventReceiver:Lcom/konka/tvsettings/popup/PvrActivity$RootMenuEventReceiver;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v31

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lcom/konka/tvsettings/popup/PvrActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual/range {p0 .. p0}, Lcom/konka/tvsettings/popup/PvrActivity;->getIntent()Landroid/content/Intent;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v31

    if-eqz v31, :cond_d

    invoke-virtual/range {p0 .. p0}, Lcom/konka/tvsettings/popup/PvrActivity;->getIntent()Landroid/content/Intent;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v31

    const-string v32, "FullPageBrowserCall"

    const/16 v33, 0x0

    invoke-virtual/range {v31 .. v33}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v31

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->isBrowserCalled:Z

    invoke-virtual/range {p0 .. p0}, Lcom/konka/tvsettings/popup/PvrActivity;->getIntent()Landroid/content/Intent;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v31

    const-string v32, "Current_Playback_Index"

    const/16 v33, -0x1

    invoke-virtual/range {v31 .. v33}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v31

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->currentPlaybackIndex:I

    invoke-virtual/range {p0 .. p0}, Lcom/konka/tvsettings/popup/PvrActivity;->getIntent()Landroid/content/Intent;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v31

    const-string v32, "PVR_ONE_TOUCH_MODE"

    invoke-virtual/range {v31 .. v32}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v31

    const/16 v32, 0x2

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_8

    const/16 v31, 0x1

    :goto_0
    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->isOneTouchPlayMode:Z

    invoke-virtual/range {p0 .. p0}, Lcom/konka/tvsettings/popup/PvrActivity;->getIntent()Landroid/content/Intent;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v31

    const-string v32, "PVR_ONE_TOUCH_MODE"

    invoke-virtual/range {v31 .. v32}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v31

    const/16 v32, 0x3

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_9

    const/16 v31, 0x1

    :goto_1
    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->isOneTouchPauseMode:Z

    invoke-virtual/range {p0 .. p0}, Lcom/konka/tvsettings/popup/PvrActivity;->getIntent()Landroid/content/Intent;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v31

    const-string v32, "PVR_AlTimeShift_Call"

    invoke-virtual/range {v31 .. v32}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v31

    const/16 v32, 0x1

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_a

    const/16 v31, 0x1

    :goto_2
    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->isAlwaysTimeShiftBegin:Z

    invoke-virtual/range {p0 .. p0}, Lcom/konka/tvsettings/popup/PvrActivity;->getIntent()Landroid/content/Intent;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v31

    const-string v32, "PVR_TIME_SHIFT_PLAY"

    invoke-virtual/range {v31 .. v32}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v31

    const/16 v32, 0x1

    move/from16 v0, v31

    move/from16 v1, v32

    if-ne v0, v1, :cond_b

    const/16 v31, 0x1

    :goto_3
    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->isAlwaysTimeShiftPlay:Z

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->isBrowserCalled:Z

    move/from16 v31, v0

    if-nez v31, :cond_0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->isOneTouchPlayMode:Z

    move/from16 v31, v0

    if-eqz v31, :cond_1

    :cond_0
    const v31, 0x7f0701ae    # com.konka.tvsettings.R.id.usbInfoLayout

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->findViewById(I)Landroid/view/View;

    move-result-object v31

    const/16 v32, 0x8

    invoke-virtual/range {v31 .. v32}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->pvrImageFlag:Lcom/konka/tvsettings/popup/PvrImageFlag;

    move-object/from16 v31, v0

    const/16 v32, 0x0

    invoke-virtual/range {v31 .. v32}, Lcom/konka/tvsettings/popup/PvrImageFlag;->setPauseFlag(Z)V

    sget-object v31, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;->E_PVR_MODE_PLAYBACK:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->curPvrMode:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->recordingText:Landroid/widget/TextView;

    move-object/from16 v31, v0

    const v32, 0x7f0a0190    # com.konka.tvsettings.R.string.str_pvr_is_playbacking

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->getString(I)Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->handler:Landroid/os/Handler;

    move-object/from16 v31, v0

    new-instance v32, Lcom/konka/tvsettings/popup/PvrActivity$1;

    move-object/from16 v0, v32

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity$1;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;)V

    const-wide/16 v33, 0x5dc

    invoke-virtual/range {v31 .. v34}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->isAlwaysTimeShiftPlay:Z

    move/from16 v31, v0

    if-eqz v31, :cond_2

    sget-object v31, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;->E_PVR_MODE_TIME_SHIFT:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->curPvrMode:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->m_RecordingImage:Landroid/widget/ImageView;

    move-object/from16 v31, v0

    const v32, 0x7f020067    # com.konka.tvsettings.R.drawable.idle_image_press_status_pvr1

    invoke-virtual/range {v31 .. v32}, Landroid/widget/ImageView;->setImageResource(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->recordingText:Landroid/widget/TextView;

    move-object/from16 v31, v0

    const v32, 0x7f0a018e    # com.konka.tvsettings.R.string.str_pvr_is_time_shift

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->getString(I)Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->recordingText:Landroid/widget/TextView;

    move-object/from16 v31, v0

    const/16 v32, 0x8

    invoke-virtual/range {v31 .. v32}, Landroid/widget/TextView;->setVisibility(I)V

    const/16 v31, 0x1

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->isMenuHide:Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->rootView:Landroid/widget/RelativeLayout;

    move-object/from16 v31, v0

    const/16 v32, 0x8

    invoke-virtual/range {v31 .. v32}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->handler:Landroid/os/Handler;

    move-object/from16 v31, v0

    new-instance v32, Lcom/konka/tvsettings/popup/PvrActivity$2;

    move-object/from16 v0, v32

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity$2;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;)V

    const-wide/16 v33, 0x5dc

    invoke-virtual/range {v31 .. v34}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->isOneTouchPauseMode:Z

    move/from16 v31, v0

    if-nez v31, :cond_3

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->isAlwaysTimeShiftBegin:Z

    move/from16 v31, v0

    if-eqz v31, :cond_c

    :cond_3
    const-string v31, "PvrActivity=====is AlwaysTimeShiftBegin or Pausemode"

    invoke-static/range {v31 .. v31}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    sget-object v31, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;->E_PVR_MODE_TIME_SHIFT:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->curPvrMode:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->recordingText:Landroid/widget/TextView;

    move-object/from16 v31, v0

    const v32, 0x7f0a018e    # com.konka.tvsettings.R.string.str_pvr_is_time_shift

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->getString(I)Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->isAlwaysTimeShiftBegin:Z

    move/from16 v31, v0

    if-eqz v31, :cond_4

    const/16 v31, 0x1

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->isMenuHide:Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->rootView:Landroid/widget/RelativeLayout;

    move-object/from16 v31, v0

    const/16 v32, 0x8

    invoke-virtual/range {v31 .. v32}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    :cond_4
    :goto_4
    new-instance v31, Lcom/konka/tvsettings/popup/PvrActivity$3;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->activity:Landroid/app/Activity;

    move-object/from16 v32, v0

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    move-object/from16 v2, v32

    invoke-direct {v0, v1, v2}, Lcom/konka/tvsettings/popup/PvrActivity$3;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;Landroid/content/Context;)V

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->usbSelecter:Lcom/konka/tvsettings/function/USBDiskSelecter;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->usbSelecter:Lcom/konka/tvsettings/function/USBDiskSelecter;

    move-object/from16 v31, v0

    new-instance v32, Lcom/konka/tvsettings/popup/PvrActivity$4;

    move-object/from16 v0, v32

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity$4;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;)V

    invoke-virtual/range {v31 .. v32}, Lcom/konka/tvsettings/function/USBDiskSelecter;->setUSBListener(Lcom/konka/tvsettings/function/USBDiskSelecter$usbListener;)V

    new-instance v31, Lcom/konka/tvsettings/popup/PvrActivity$5;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    move-object/from16 v32, v0

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    move-object/from16 v2, p0

    move-object/from16 v3, v32

    invoke-direct {v0, v1, v2, v3}, Lcom/konka/tvsettings/popup/PvrActivity$5;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;Landroid/content/Context;Lcom/konka/kkinterface/tv/PvrDesk;)V

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->thumbnail:Lcom/konka/tvsettings/popup/PVRThumbnail;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->usbSelecter:Lcom/konka/tvsettings/function/USBDiskSelecter;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/konka/tvsettings/function/USBDiskSelecter;->getDriverCount()I

    move-result v31

    if-nez v31, :cond_6

    const-string v31, "PVR ========>>>usbDriverCount is 0,finish PVR menu"

    invoke-static/range {v31 .. v31}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->activity:Landroid/app/Activity;

    move-object/from16 v31, v0

    const v32, 0x7f0a00eb    # com.konka.tvsettings.R.string.str_pvr_insert_usb

    const/16 v33, 0x1f4

    invoke-static/range {v31 .. v33}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Landroid/widget/Toast;->show()V

    invoke-direct/range {p0 .. p0}, Lcom/konka/tvsettings/popup/PvrActivity;->isBootedByRecord()Z

    move-result v31

    if-eqz v31, :cond_5

    invoke-direct/range {p0 .. p0}, Lcom/konka/tvsettings/popup/PvrActivity;->standbySystem()V

    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/konka/tvsettings/popup/PvrActivity;->finish()V

    :cond_6
    const v31, 0x7f0701b7    # com.konka.tvsettings.R.id.thumbnailRoot

    move-object/from16 v0, p0

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->findViewById(I)Landroid/view/View;

    move-result-object v31

    check-cast v31, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->thumbnail:Lcom/konka/tvsettings/popup/PVRThumbnail;

    move-object/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    new-instance v18, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v31, -0x1

    const/16 v32, -0x1

    move-object/from16 v0, v18

    move/from16 v1, v31

    move/from16 v2, v32

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v31, 0xa

    move-object/from16 v0, v18

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->thumbnail:Lcom/konka/tvsettings/popup/PVRThumbnail;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/PVRThumbnail;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->thumbnail:Lcom/konka/tvsettings/popup/PVRThumbnail;

    move-object/from16 v31, v0

    const/16 v32, 0x0

    invoke-virtual/range {v31 .. v32}, Lcom/konka/tvsettings/popup/PVRThumbnail;->Show(Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->recorder:Landroid/widget/ImageView;

    move-object/from16 v31, v0

    new-instance v32, Lcom/konka/tvsettings/popup/PvrActivity$6;

    move-object/from16 v0, v32

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity$6;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;)V

    invoke-virtual/range {v31 .. v32}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->play:Landroid/widget/ImageView;

    move-object/from16 v31, v0

    new-instance v32, Lcom/konka/tvsettings/popup/PvrActivity$7;

    move-object/from16 v0, v32

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity$7;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;)V

    invoke-virtual/range {v31 .. v32}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->stop:Landroid/widget/ImageView;

    move-object/from16 v31, v0

    new-instance v32, Lcom/konka/tvsettings/popup/PvrActivity$8;

    move-object/from16 v0, v32

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity$8;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;)V

    invoke-virtual/range {v31 .. v32}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->pause:Landroid/widget/ImageView;

    move-object/from16 v31, v0

    new-instance v32, Lcom/konka/tvsettings/popup/PvrActivity$9;

    move-object/from16 v0, v32

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity$9;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;)V

    invoke-virtual/range {v31 .. v32}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->rev:Landroid/widget/ImageView;

    move-object/from16 v31, v0

    new-instance v32, Lcom/konka/tvsettings/popup/PvrActivity$10;

    move-object/from16 v0, v32

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity$10;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;)V

    invoke-virtual/range {v31 .. v32}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->ff:Landroid/widget/ImageView;

    move-object/from16 v31, v0

    new-instance v32, Lcom/konka/tvsettings/popup/PvrActivity$11;

    move-object/from16 v0, v32

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity$11;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;)V

    invoke-virtual/range {v31 .. v32}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->slow:Landroid/widget/ImageView;

    move-object/from16 v31, v0

    new-instance v32, Lcom/konka/tvsettings/popup/PvrActivity$12;

    move-object/from16 v0, v32

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity$12;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;)V

    invoke-virtual/range {v31 .. v32}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->time:Landroid/widget/ImageView;

    move-object/from16 v31, v0

    new-instance v32, Lcom/konka/tvsettings/popup/PvrActivity$13;

    move-object/from16 v0, v32

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity$13;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;)V

    invoke-virtual/range {v31 .. v32}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-interface/range {v28 .. v28}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v5

    invoke-interface/range {v28 .. v28}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getEpgManagerInstance()Lcom/konka/kkinterface/tv/EpgDesk;

    move-result-object v13

    const/4 v6, 0x0

    new-instance v14, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    invoke-direct {v14}, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;-><init>()V

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v6

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "CH"

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v6, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    move/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    iget-object v0, v6, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceName:Ljava/lang/String;

    move-object/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->serviceNameText:Landroid/widget/TextView;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v7, Landroid/text/format/Time;

    invoke-direct {v7}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v7}, Landroid/text/format/Time;->setToNow()V

    const/16 v31, 0x1

    move/from16 v0, v31

    invoke-virtual {v7, v0}, Landroid/text/format/Time;->toMillis(Z)J

    :try_start_0
    iget-short v0, v6, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    move/from16 v31, v0

    iget v0, v6, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    move/from16 v32, v0

    move/from16 v0, v31

    move/from16 v1, v32

    invoke-interface {v13, v0, v1, v7}, Lcom/konka/kkinterface/tv/EpgDesk;->getEventInfoByTime(SILandroid/text/format/Time;)Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v14

    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->eventNameText:Landroid/widget/TextView;

    move-object/from16 v31, v0

    iget-object v0, v14, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->name:Ljava/lang/String;

    move-object/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/16 v31, -0x1

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->pvrABLoopStartTime:I

    const/16 v31, -0x1

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->pvrABLoopEndTime:I

    sget-object v31, Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;->E_PVR_AB_LOOP_STATUS_NONE:Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->setPvrABLoop:Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->isBrowserCalled:Z

    move/from16 v31, v0

    if-eqz v31, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->play:Landroid/widget/ImageView;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Landroid/widget/ImageView;->performClick()Z

    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->handler:Landroid/os/Handler;

    move-object/from16 v31, v0

    new-instance v32, Lcom/konka/tvsettings/popup/PvrActivity$14;

    move-object/from16 v0, v32

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity$14;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;)V

    const-wide/16 v33, 0x5dc

    invoke-virtual/range {v31 .. v34}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    move-object/from16 v32, v0

    invoke-interface/range {v32 .. v32}, Lcom/konka/kkinterface/tv/PvrDesk;->getCurPlaybackingFileName()Ljava/lang/String;

    move-result-object v32

    invoke-interface/range {v31 .. v32}, Lcom/konka/kkinterface/tv/PvrDesk;->getRecordedFileDurationTime(Ljava/lang/String;)I

    move-result v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->RPProgress:Lcom/konka/tvsettings/popup/TextProgressBar;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/TextProgressBar;->setMax(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->totalRecordTime:Landroid/widget/TextView;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    move/from16 v1, v29

    invoke-direct {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->getTimeString(I)Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->currentPlaybackIndex:I

    move/from16 v31, v0

    const/16 v32, -0x1

    move/from16 v0, v31

    move/from16 v1, v32

    if-eq v0, v1, :cond_7

    new-instance v15, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;

    invoke-direct {v15}, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->currentPlaybackIndex:I

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lcom/konka/kkinterface/tv/PvrDesk;->getMetadataSortKey()I

    move-result v33

    invoke-interface/range {v31 .. v33}, Lcom/konka/kkinterface/tv/PvrDesk;->getPvrFileInfo(II)Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;

    move-result-object v15

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "CH"

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->currentPlaybackIndex:I

    move/from16 v33, v0

    invoke-interface/range {v32 .. v33}, Lcom/konka/kkinterface/tv/PvrDesk;->getFileLcn(I)I

    move-result v32

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    const-string v32, " "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    move-object/from16 v32, v0

    iget-object v0, v15, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;->filename:Ljava/lang/String;

    move-object/from16 v33, v0

    invoke-interface/range {v32 .. v33}, Lcom/konka/kkinterface/tv/PvrDesk;->getFileServiceName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    move-object/from16 v31, v0

    iget-object v0, v15, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;->filename:Ljava/lang/String;

    move-object/from16 v32, v0

    invoke-interface/range {v31 .. v32}, Lcom/konka/kkinterface/tv/PvrDesk;->getFileEventName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->serviceNameText:Landroid/widget/TextView;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->eventNameText:Landroid/widget/TextView;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->play:Landroid/widget/ImageView;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Landroid/widget/ImageView;->requestFocus()Z

    new-instance v31, Lcom/konka/tvsettings/popup/PvrActivity$BrowserCalledPlayBackProgress;

    const/16 v32, 0x0

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    move-object/from16 v2, v32

    invoke-direct {v0, v1, v2}, Lcom/konka/tvsettings/popup/PvrActivity$BrowserCalledPlayBackProgress;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;Lcom/konka/tvsettings/popup/PvrActivity$BrowserCalledPlayBackProgress;)V

    invoke-virtual/range {v31 .. v31}, Lcom/konka/tvsettings/popup/PvrActivity$BrowserCalledPlayBackProgress;->start()V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_6
    return-void

    :cond_8
    const/16 v31, 0x0

    goto/16 :goto_0

    :cond_9
    const/16 v31, 0x0

    goto/16 :goto_1

    :cond_a
    const/16 v31, 0x0

    goto/16 :goto_2

    :cond_b
    const/16 v31, 0x0

    goto/16 :goto_3

    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/konka/tvsettings/popup/PvrActivity;->setBarStatusOfStartRecord()V

    invoke-virtual/range {p0 .. p0}, Lcom/konka/tvsettings/popup/PvrActivity;->getIntent()Landroid/content/Intent;

    move-result-object v31

    const-string v32, "auto_record"

    const/16 v33, 0x0

    invoke-virtual/range {v31 .. v33}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v31

    move/from16 v0, v31

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/konka/tvsettings/popup/PvrActivity;->AutoRecord:Z

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "\n Auto record start! "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->AutoRecord:Z

    move/from16 v32, v0

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v31 .. v31}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/konka/tvsettings/popup/PvrActivity;->setBarStatusOfStartRecord()V

    goto/16 :goto_4

    :catch_0
    move-exception v12

    invoke-virtual {v12}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_5

    :catch_1
    move-exception v11

    invoke-virtual {v11}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_6

    :cond_e
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->isOneTouchPlayMode:Z

    move/from16 v31, v0

    if-eqz v31, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->play:Landroid/widget/ImageView;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Landroid/widget/ImageView;->performClick()Z

    :try_start_2
    const-string v31, "PVRActivity"

    new-instance v32, Ljava/lang/StringBuilder;

    const-string v33, "===========>>>> pvr recordDiskPath is "

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lcom/konka/kkinterface/tv/PvrDesk;->getUsbDeviceIndex()S

    move-result v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->recordDiskPath:Ljava/lang/String;

    move-object/from16 v33, v0

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    move-object/from16 v31, v0

    invoke-interface/range {v31 .. v31}, Lcom/konka/kkinterface/tv/PvrDesk;->getPvrFileNumber()I

    move-result v23

    if-nez v23, :cond_f

    const-string v31, "PVRActivity"

    const-string v32, "===========>>>> pvrFileNumber is 0 !!!!!"

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {p0 .. p0}, Lcom/konka/tvsettings/popup/PvrActivity;->finish()V
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_6

    :catch_2
    move-exception v11

    invoke-virtual {v11}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_6

    :cond_f
    :try_start_3
    new-instance v15, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;

    invoke-direct {v15}, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    move-object/from16 v31, v0

    const/16 v32, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lcom/konka/kkinterface/tv/PvrDesk;->getMetadataSortKey()I

    move-result v33

    invoke-interface/range {v31 .. v33}, Lcom/konka/kkinterface/tv/PvrDesk;->getPvrFileInfo(II)Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;

    move-result-object v15

    iget-object v0, v15, Lcom/mstar/android/tvapi/common/vo/PvrFileInfo;->filename:Ljava/lang/String;

    move-object/from16 v22, v0

    if-nez v22, :cond_10

    const-string v31, "PVRActivity"

    const-string v32, "===========>>>> pvrFileName is NULL !!!!!"

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual/range {p0 .. p0}, Lcom/konka/tvsettings/popup/PvrActivity;->finish()V

    goto/16 :goto_6

    :cond_10
    const/16 v21, 0x0

    const/16 v24, 0x0

    const/16 v20, 0x0

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "CH "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    move-object/from16 v32, v0

    const/16 v33, 0x0

    invoke-interface/range {v32 .. v33}, Lcom/konka/kkinterface/tv/PvrDesk;->getFileLcn(I)I

    move-result v32

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/PvrDesk;->getFileServiceName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/PvrDesk;->getFileEventName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    new-instance v31, Ljava/lang/StringBuilder;

    invoke-static/range {v21 .. v21}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v32

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v32, " "

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    move-object/from16 v0, v31

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->serviceNameText:Landroid/widget/TextView;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->eventNameText:Landroid/widget/TextView;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v31, "PVRActivity"

    new-instance v32, Ljava/lang/StringBuilder;

    const-string v33, "===========>>>> pvrFileNumber = "

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lcom/konka/kkinterface/tv/PvrDesk;->getPvrFileNumber()I

    move-result v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v31, "PVRActivity"

    new-instance v32, Ljava/lang/StringBuilder;

    const-string v33, "===========>>>> current playback fileName = "

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v32

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v31, "PVRActivity"

    new-instance v32, Ljava/lang/StringBuilder;

    const-string v33, "===========>>>> pvrFileLcn = "

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v32

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v31, "PVRActivity"

    new-instance v32, Ljava/lang/StringBuilder;

    const-string v33, "===========>>>> pvrFileServiceName = "

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v32

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v31, "PVRActivity"

    new-instance v32, Ljava/lang/StringBuilder;

    const-string v33, "===========>>>> pvrFileEventName = "

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v32

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    move-object/from16 v31, v0

    invoke-interface/range {v31 .. v31}, Lcom/konka/kkinterface/tv/PvrDesk;->isPlaybacking()Z

    move-result v31

    if-eqz v31, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    move-object/from16 v31, v0

    invoke-interface/range {v31 .. v31}, Lcom/konka/kkinterface/tv/PvrDesk;->stopPlayback()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    move-object/from16 v31, v0

    invoke-interface/range {v31 .. v31}, Lcom/konka/kkinterface/tv/PvrDesk;->stopPlaybackLoop()V

    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/PvrDesk;->startPlayback(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    move-result-object v19

    const-string v31, "PVRActivity"

    new-instance v32, Ljava/lang/StringBuilder;

    const-string v33, "==========>>> playbackStatus = "

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v32

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v31, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_SUCCESS:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    move-object/from16 v0, v19

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-nez v31, :cond_12

    new-instance v31, Ljava/lang/StringBuilder;

    const-string v32, "Can\'t PlayBack Properly, the Reason is "

    invoke-direct/range {v31 .. v32}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v19 .. v19}, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    const/16 v32, 0x1f4

    move-object/from16 v0, p0

    move-object/from16 v1, v31

    move/from16 v2, v32

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v31

    invoke-virtual/range {v31 .. v31}, Landroid/widget/Toast;->show()V

    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/PvrDesk;->getRecordedFileDurationTime(Ljava/lang/String;)I

    move-result v30

    const-string v31, "PVRActivity"

    new-instance v32, Ljava/lang/StringBuilder;

    const-string v33, "==========>>> totalTime = "

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v32

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->RPProgress:Lcom/konka/tvsettings/popup/TextProgressBar;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/TextProgressBar;->setMax(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->totalRecordTime:Landroid/widget/TextView;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-direct {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->getTimeString(I)Ljava/lang/String;

    move-result-object v32

    invoke-virtual/range {v31 .. v32}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->stop:Landroid/widget/ImageView;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Landroid/widget/ImageView;->requestFocus()Z

    new-instance v31, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    const/16 v32, 0x0

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    move-object/from16 v2, v32

    invoke-direct {v0, v1, v2}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;)V

    invoke-virtual/range {v31 .. v31}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->start()V
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_6

    :cond_13
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->isAlwaysTimeShiftPlay:Z

    move/from16 v31, v0

    if-eqz v31, :cond_17

    const-string v31, "PVRActivity"

    const-string v32, "==========>>> play perform click  "

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->play:Landroid/widget/ImageView;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Landroid/widget/ImageView;->performClick()Z

    invoke-virtual/range {p0 .. p0}, Lcom/konka/tvsettings/popup/PvrActivity;->getBestDiskPath()Ljava/lang/String;

    move-result-object v10

    const/4 v8, 0x0

    const/4 v9, 0x0

    new-instance v31, Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-direct {v0, v10}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v31

    invoke-direct {v0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->getUsbIndexByPath(Ljava/lang/String;)I

    move-result v16

    if-ltz v16, :cond_14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->usbDriverLabel:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Ljava/util/ArrayList;->size()I

    move-result v31

    move/from16 v0, v16

    move/from16 v1, v31

    if-ge v0, v1, :cond_14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->usbDriverLabel:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->usbDriverLabelPcStyle:Ljava/util/ArrayList;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    :cond_14
    const-string v31, "qhc"

    new-instance v32, Ljava/lang/StringBuilder;

    const-string v33, "getBestDiskPath:"

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v32

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/konka/tvsettings/popup/PvrActivity;->recordDiskPath:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v8, v0, Lcom/konka/tvsettings/popup/PvrActivity;->recordDiskLable:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->usbLabel:Landroid/widget/TextView;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v31, "PVR"

    const-string v32, "=============>>>>> disk has been set in PVR Option menu,get it..."

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v31, "PVR"

    new-instance v32, Ljava/lang/StringBuilder;

    const-string v33, "========>>>>> recordDiskPath["

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->recordDiskPath:Ljava/lang/String;

    move-object/from16 v33, v0

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "]recordDiskLable["

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->recordDiskLable:Ljava/lang/String;

    move-object/from16 v33, v0

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "]"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    move-object/from16 v31, v0

    invoke-interface/range {v31 .. v31}, Lcom/konka/kkinterface/tv/PvrDesk;->isRecording()Z

    move-result v31

    if-eqz v31, :cond_16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    move-object/from16 v31, v0

    invoke-interface/range {v31 .. v31}, Lcom/konka/kkinterface/tv/PvrDesk;->getCurRecordTimeInSecond()I

    move-result v29

    :goto_7
    const-string v31, "maisie"

    new-instance v32, Ljava/lang/StringBuilder;

    const-string v33, "##################getCurRecordTimeInSecond ["

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    move-object/from16 v33, v0

    invoke-interface/range {v33 .. v33}, Lcom/konka/kkinterface/tv/PvrDesk;->getCurRecordTimeInSecond()I

    move-result v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "] \n"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v31, "maisie"

    new-instance v32, Ljava/lang/StringBuilder;

    const-string v33, "##################getRecordedFileDurationTime ["

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    move-object/from16 v34, v0

    invoke-interface/range {v34 .. v34}, Lcom/konka/kkinterface/tv/PvrDesk;->getCurPlaybackingFileName()Ljava/lang/String;

    move-result-object v34

    invoke-interface/range {v33 .. v34}, Lcom/konka/kkinterface/tv/PvrDesk;->getRecordedFileDurationTime(Ljava/lang/String;)I

    move-result v33

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "] \n"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v31, "maisie"

    new-instance v32, Ljava/lang/StringBuilder;

    const-string v33, "##################Total Time ["

    invoke-direct/range {v32 .. v33}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v32

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v32

    const-string v33, "] \n"

    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v32

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v31, 0xb4

    move/from16 v0, v29

    move/from16 v1, v31

    if-le v0, v1, :cond_15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    move-object/from16 v31, v0

    move/from16 v0, v29

    add-int/lit16 v0, v0, -0xb4

    move/from16 v32, v0

    invoke-interface/range {v31 .. v32}, Lcom/konka/kkinterface/tv/PvrDesk;->jumpPlaybackTime(I)Z
    :try_end_4
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_4 .. :try_end_4} :catch_3

    :cond_15
    :goto_8
    new-instance v31, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    const/16 v32, 0x0

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    move-object/from16 v2, v32

    invoke-direct {v0, v1, v2}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;)V

    invoke-virtual/range {v31 .. v31}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->start()V

    goto/16 :goto_6

    :cond_16
    :try_start_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    move-object/from16 v32, v0

    invoke-interface/range {v32 .. v32}, Lcom/konka/kkinterface/tv/PvrDesk;->getCurPlaybackingFileName()Ljava/lang/String;

    move-result-object v32

    invoke-interface/range {v31 .. v32}, Lcom/konka/kkinterface/tv/PvrDesk;->getRecordedFileDurationTime(Ljava/lang/String;)I
    :try_end_5
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_5 .. :try_end_5} :catch_3

    move-result v29

    goto/16 :goto_7

    :catch_3
    move-exception v11

    invoke-virtual {v11}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_8

    :cond_17
    const-string v31, "PVRActivity"

    const-string v32, "==========>>> record perform click  "

    invoke-static/range {v31 .. v32}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    move-object/from16 v31, v0

    invoke-interface/range {v31 .. v31}, Lcom/konka/kkinterface/tv/PvrDesk;->isTimeShiftRecording()Z

    move-result v31

    if-eqz v31, :cond_18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->settingDesk:Lcom/konka/kkinterface/tv/SettingDesk;

    move-object/from16 v31, v0

    invoke-interface/range {v31 .. v31}, Lcom/konka/kkinterface/tv/SettingDesk;->getAlTimeShift()Z

    move-result v31

    if-eqz v31, :cond_18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    move-object/from16 v31, v0

    invoke-interface/range {v31 .. v31}, Lcom/konka/kkinterface/tv/PvrDesk;->stopTimeShiftRecord()V
    :try_end_6
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_6 .. :try_end_6} :catch_4

    :cond_18
    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/tvsettings/popup/PvrActivity;->recorder:Landroid/widget/ImageView;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Landroid/widget/ImageView;->performClick()Z

    goto/16 :goto_6

    :catch_4
    move-exception v11

    invoke-virtual {v11}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_9
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 6
    .param p1    # I

    const/4 v5, 0x0

    packed-switch p1, :pswitch_data_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :pswitch_0
    new-instance v2, Landroid/app/ProgressDialog;

    invoke-direct {v2, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a00ef    # com.konka.tvsettings.R.string.str_pvr_program_saving

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v5}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    invoke-virtual {v2, v5}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v4, 0x7f030040    # com.konka.tvsettings.R.layout.pvr_menu_dialog

    const v3, 0x7f0701b8    # com.konka.tvsettings.R.id.pvr_dialog

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/popup/PvrActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v0, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    new-instance v3, Landroid/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/konka/tvsettings/popup/PvrActivity;->activity:Landroid/app/Activity;

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0a00fd    # com.konka.tvsettings.R.string.str_player_time

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0a0182    # com.konka.tvsettings.R.string.str_root_alert_dialog_confirm

    new-instance v5, Lcom/konka/tvsettings/popup/PvrActivity$17;

    invoke-direct {v5, p0}, Lcom/konka/tvsettings/popup/PvrActivity$17;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0a0183    # com.konka.tvsettings.R.string.str_root_alert_dialog_cancel

    new-instance v5, Lcom/konka/tvsettings/popup/PvrActivity$18;

    invoke-direct {v5, p0}, Lcom/konka/tvsettings/popup/PvrActivity$18;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity;->timeChooser:Landroid/app/AlertDialog;

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->setBarStatusOfPlayToOthers()V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->timeChooser:Landroid/app/AlertDialog;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 2

    const-string v1, "PvrActivity========>>>onDestroy"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->rootmenuEventReceiver:Lcom/konka/tvsettings/popup/PvrActivity$RootMenuEventReceiver;

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onDestroy()V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->usbSelecter:Lcom/konka/tvsettings/function/USBDiskSelecter;

    invoke-virtual {v1}, Lcom/konka/tvsettings/function/USBDiskSelecter;->dismiss()V

    :try_start_0
    iget-boolean v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isBrowserCalled:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->isPlaybacking()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "PvrActivity========>>>stopPlayback"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->resumeLang()V

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->resumeSubtitle()V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->stopPlayback()V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->stopPlaybackLoop()V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->settingDesk:Lcom/konka/kkinterface/tv/SettingDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/SettingDesk;->getAlTimeShift()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->startTimeShiftRecord()Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public onKeyBackward()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->isPlaybacking()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->isRecording()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->curPvrMode:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    sget-object v2, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;->E_PVR_MODE_ALWAYS_TIME_SHIFT:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->isAlwaysTimeShiftPlaybackPaused()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->startAlwaysTimeShiftPlayback()S

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PvrDesk;->pauseAlwaysTimeShiftPlayback(Z)Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->startAlwaysTimeShiftPlayback()S

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->setBarStatusOfRecordToPause()V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->play:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->requestFocus()Z

    :cond_2
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->getCurPlaybackTimeInSecond()I

    move-result v0

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->setBarStatusOfPlayToOthers()V

    :cond_3
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->isPlaybacking()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->isPlaybackPaused()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->getPlaybackSpeed()Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_STEP_IN:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->getCurPlaybackTimeInSecond()I

    move-result v0

    const/16 v1, 0x1e

    if-le v0, v1, :cond_4

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    add-int/lit8 v2, v0, -0x1e

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PvrDesk;->jumpPlaybackTime(I)Z

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PvrDesk;->jumpPlaybackTime(I)Z

    goto :goto_0
.end method

.method public onKeyCapture()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/PvrDesk;->isTimeShiftRecording()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/PvrDesk;->isPlaybacking()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->CaptureThumbnail()Z

    :cond_0
    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->setBarStatusOfPlayToOthers()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 10
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v7, 0x1

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "PvrActivity========>>>onKeyDown:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-boolean v8, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isAlwaysTimeShiftPlay:Z

    if-eqz v8, :cond_1

    const/16 v8, 0x12e

    if-eq p1, v8, :cond_1

    const/16 v8, 0x131

    if-eq p1, v8, :cond_1

    const/16 v8, 0x130

    if-eq p1, v8, :cond_1

    const/16 v8, 0xba

    if-eq p1, v8, :cond_1

    invoke-super {p0, p1, p2}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v7

    :cond_0
    :goto_0
    return v7

    :cond_1
    sparse-switch p1, :sswitch_data_0

    :goto_1
    invoke-direct {p0, p2}, Lcom/konka/tvsettings/popup/PvrActivity;->CheckNeedToStopRecord(Landroid/view/KeyEvent;)Z

    move-result v8

    if-nez v8, :cond_0

    const/4 v7, 0x4

    if-eq p1, v7, :cond_2

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->menuShow()V

    :cond_2
    invoke-super {p0, p1, p2}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v7

    goto :goto_0

    :sswitch_0
    :try_start_0
    new-instance v1, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;

    invoke-direct {v1}, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;-><init>()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/TVRootApp;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v5

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->getAudioInfo()Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-short v8, v1, Lcom/mstar/android/tvapi/dtv/vo/DtvAudioInfo;->currentAudioIndex:S

    iput-short v8, p0, Lcom/konka/tvsettings/popup/PvrActivity;->audioLangPosLive:S

    :cond_3
    const/4 v8, 0x0

    invoke-virtual {p0, v8}, Lcom/konka/tvsettings/popup/PvrActivity;->isGoingToBeClosed(Z)V

    new-instance v4, Landroid/content/Intent;

    const-class v8, Lcom/konka/tvsettings/nicam/AudioLanguageActivity;

    invoke-direct {v4, p0, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v4}, Lcom/konka/tvsettings/popup/PvrActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    :sswitch_1
    const/4 v8, 0x0

    :try_start_1
    invoke-virtual {p0, v8}, Lcom/konka/tvsettings/popup/PvrActivity;->isGoingToBeClosed(Z)V

    const-string v8, "TvSetting"

    const/4 v9, 0x0

    invoke-virtual {p0, v8, v9}, Lcom/konka/tvsettings/popup/PvrActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    const-string v8, "subtitlePos"

    const/4 v9, -0x1

    invoke-interface {v6, v8, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    iput v8, p0, Lcom/konka/tvsettings/popup/PvrActivity;->subtitlePosLive:I

    new-instance v4, Landroid/content/Intent;

    const-class v8, Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;

    invoke-direct {v4, p0, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v4}, Lcom/konka/tvsettings/popup/PvrActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v3

    invoke-virtual {v3}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    :sswitch_2
    :try_start_2
    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->onKeyStop()V
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :sswitch_3
    :try_start_3
    iget-object v8, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/PvrDesk;->isTimeShiftRecording()Z

    move-result v8

    if-eqz v8, :cond_4

    iget-object v8, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/PvrDesk;->isPlaybacking()Z

    move-result v8

    if-eqz v8, :cond_5

    :cond_4
    iget-object v8, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/PvrDesk;->isPlaybacking()Z

    move-result v8

    if-eqz v8, :cond_6

    iget-object v8, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/PvrDesk;->getPlaybackSpeed()Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    move-result-object v8

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_1X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    if-eq v8, v9, :cond_6

    :cond_5
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "PvrActivity========>>>onKeyDown:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "Play"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->onKeyPlay()V
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    :catch_3
    move-exception v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0

    :cond_6
    :try_start_4
    iget-object v8, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/PvrDesk;->isPlaybacking()Z

    move-result v8

    if-eqz v8, :cond_0

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "PvrActivity========>>>onKeyDown:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "Pause"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-boolean v8, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isAlwaysTimeShiftPlay:Z

    if-eqz v8, :cond_7

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->onBackPressed()V

    goto/16 :goto_0

    :cond_7
    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->onKeyPause()V
    :try_end_4
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_4 .. :try_end_4} :catch_3

    goto/16 :goto_0

    :sswitch_4
    :try_start_5
    iget-object v8, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/PvrDesk;->isPlaybacking()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->onKeyRev()V
    :try_end_5
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_0

    :catch_4
    move-exception v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0

    :sswitch_5
    :try_start_6
    iget-object v8, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/PvrDesk;->isPlaybacking()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->onKeyFF()V
    :try_end_6
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_6 .. :try_end_6} :catch_5

    goto/16 :goto_0

    :catch_5
    move-exception v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0

    :sswitch_6
    :try_start_7
    iget-object v8, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/PvrDesk;->isPlaybacking()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->onKeyBackward()V
    :try_end_7
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_7 .. :try_end_7} :catch_6

    goto/16 :goto_0

    :catch_6
    move-exception v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0

    :sswitch_7
    :try_start_8
    iget-object v8, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/PvrDesk;->isPlaybacking()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->onKeyForward()V
    :try_end_8
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_8 .. :try_end_8} :catch_7

    goto/16 :goto_0

    :catch_7
    move-exception v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0

    :sswitch_8
    invoke-super {p0, p1, p2}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v7

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x18 -> :sswitch_8
        0x19 -> :sswitch_8
        0xa4 -> :sswitch_8
        0xba -> :sswitch_3
        0xff -> :sswitch_8
        0x102 -> :sswitch_1
        0x104 -> :sswitch_0
        0x106 -> :sswitch_7
        0x12e -> :sswitch_3
        0x12f -> :sswitch_2
        0x130 -> :sswitch_5
        0x131 -> :sswitch_4
        0x133 -> :sswitch_6
    .end sparse-switch
.end method

.method public onKeyFF()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const v3, 0x7f0a00fb    # com.konka.tvsettings.R.string.str_player_ff

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->isPlaybacking()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->getPlaybackSpeed()Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    move-result-object v0

    invoke-static {}, Lcom/konka/tvsettings/popup/PvrActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$PvrPlaybackSpeed$EnumPvrPlaybackSpeed()[I

    move-result-object v1

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_1X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PvrDesk;->setPlaybackSpeed(Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->setBarStatusOfPlayToOthers()V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->ff:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->requestFocus()Z

    :cond_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_2XFF:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PvrDesk;->setPlaybackSpeed(Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/popup/PvrActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " 2X"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_4XFF:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PvrDesk;->setPlaybackSpeed(Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/popup/PvrActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " 4X"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_8XFF:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PvrDesk;->setPlaybackSpeed(Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/popup/PvrActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " 8X"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_16XFF:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PvrDesk;->setPlaybackSpeed(Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/popup/PvrActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " 16X"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_32XFF:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PvrDesk;->setPlaybackSpeed(Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/popup/PvrActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " 32X"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_5
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_1X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PvrDesk;->setPlaybackSpeed(Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0xe
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onKeyForward()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/PvrDesk;->isPlaybacking()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/PvrDesk;->isPlaybackPaused()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/PvrDesk;->getPlaybackSpeed()Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_STEP_IN:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/PvrDesk;->doPlaybackJumpForward()V

    :cond_0
    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->setBarStatusOfPlayToOthers()V

    return-void
.end method

.method public onKeyGoToTime()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/PvrDesk;->isPlaybacking()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/PvrActivity;->showDialog(I)V

    :cond_0
    return-void
.end method

.method public onKeyPause()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const v3, 0x7f0a00f9    # com.konka.tvsettings.R.string.str_player_pause

    const-string v0, "PVR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "=========>>>> curPvrMode is :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->curPvrMode:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->curPvrMode:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    sget-object v1, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;->E_PVR_MODE_NONE:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/PvrDesk;->isTimeShiftRecording()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/PvrDesk;->isPlaybacking()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/PvrDesk;->pausePlayback()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->setBarStatusOfPlayToOthers()V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/popup/PvrActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_1
    sget-object v0, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;->E_PVR_MODE_SHORT:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pause:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->requestFocus()Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->curPvrMode:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    sget-object v1, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;->E_PVR_MODE_PLAYBACK:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    if-ne v0, v1, :cond_3

    const-string v0, "PVR"

    const-string v1, "=========>>>> stepInPlayback !!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/PvrDesk;->pausePlayback()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->setBarStatusOfPlayToOthers()V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/popup/PvrActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->curPvrMode:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    sget-object v1, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;->E_PVR_MODE_RECORD:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/PvrDesk;->isPlaybacking()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/PvrDesk;->stepInPlayback()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->setBarStatusOfRecordToPause()V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/popup/PvrActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/PvrDesk;->pauseRecord()V

    goto :goto_1
.end method

.method public onKeyPlay()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const-string v1, "PVR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "=========>>>> CurPlaybackmode "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity;->curPvrMode:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "     \n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->curPvrMode:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    sget-object v2, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;->E_PVR_MODE_NONE:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    if-ne v1, v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvrImageFlag:Lcom/konka/tvsettings/popup/PvrImageFlag;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/konka/tvsettings/popup/PvrImageFlag;->setPlayFlag(Z)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->isTimeShiftRecording()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->play:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->requestFocus()Z

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->getPlaybackSpeed()Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_INVALID:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->isAlwaysTimeShiftPlaybackPaused()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "PVR"

    const-string v2, "=========>>>> isAlwaysTimeShiftPlaybackPaused \n"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->startAlwaysTimeShiftPlayback()S

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "PVR"

    const-string v2, "=========>>>> startAlwaysTimeShiftPlayback is not E_SUCCESS!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const-string v1, "PVR"

    const-string v2, "=========>>>> isAlwaysTimeShiftPlayback \n"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->startTimeShiftPlayback()Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_SUCCESS:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    if-eq v1, v2, :cond_3

    const-string v1, "PVR"

    const-string v2, "=========>>>> startTimeShiftPlayback is not E_SUCCESS!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->play:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->requestFocus()Z

    invoke-static {}, Lcom/konka/tvsettings/popup/PvrActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$PvrPlaybackSpeed$EnumPvrPlaybackSpeed()[I

    move-result-object v1

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/PvrDesk;->getPlaybackSpeed()Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :cond_3
    :goto_1
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->curPvrMode:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    sget-object v2, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;->E_PVR_MODE_SHORT:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    if-ne v1, v2, :cond_4

    const-string v1, "PVR"

    const-string v2, "=========>>>> curPvrMode is SHORT!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->curPvrMode:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    sget-object v2, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;->E_PVR_MODE_PLAYBACK:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    if-eq v1, v2, :cond_5

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->curPvrMode:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    sget-object v2, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;->E_PVR_MODE_TIME_SHIFT:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    if-ne v1, v2, :cond_a

    :cond_5
    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->setBarStatusOfPlayToOthers()V

    goto/16 :goto_0

    :pswitch_0
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->resumePlayback()V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :pswitch_1
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_1X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PvrDesk;->setPlaybackSpeed(Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :pswitch_2
    const-string v1, "PvrActivity========>>>onClick ABLoop"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :pswitch_3
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_1X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PvrDesk;->setPlaybackSpeed(Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_6
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->curPvrMode:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    sget-object v2, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;->E_PVR_MODE_PLAYBACK:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    if-ne v1, v2, :cond_7

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->play:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->requestFocus()Z

    const-string v1, "PVRActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "===========>>>> PlaybackSpeed is "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/PvrDesk;->getPlaybackSpeed()Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/konka/tvsettings/popup/PvrActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$PvrPlaybackSpeed$EnumPvrPlaybackSpeed()[I

    move-result-object v1

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/PvrDesk;->getPlaybackSpeed()Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    goto/16 :goto_1

    :pswitch_4
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_1X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PvrDesk;->setPlaybackSpeed(Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :pswitch_5
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->resumePlayback()V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :pswitch_6
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_1X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PvrDesk;->setPlaybackSpeed(Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :pswitch_7
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_7
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->curPvrMode:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    sget-object v2, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;->E_PVR_MODE_RECORD:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->play:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->requestFocus()Z

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->isPlaybacking()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-static {}, Lcom/konka/tvsettings/popup/PvrActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$PvrPlaybackSpeed$EnumPvrPlaybackSpeed()[I

    move-result-object v1

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/PvrDesk;->getPlaybackSpeed()Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_2

    goto/16 :goto_1

    :pswitch_8
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_1X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PvrDesk;->setPlaybackSpeed(Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :pswitch_9
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->resumePlayback()V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :pswitch_a
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_1X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PvrDesk;->setPlaybackSpeed(Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :pswitch_b
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_8
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->getCurRecordingFileName()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;->E_SUCCESS:Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v2, v0}, Lcom/konka/kkinterface/tv/PvrDesk;->startPlayback(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/EnumPvrStatus;

    move-result-object v2

    if-eq v1, v2, :cond_9

    const-string v1, "PVR"

    const-string v2, "=========>>>> startPlayback is not E_SUCCESS!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_9
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v1, v0}, Lcom/konka/kkinterface/tv/PvrDesk;->assignThumbnailFileInfoHandler(Ljava/lang/String;)Z

    sget-object v1, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;->E_PVR_MODE_PLAYBACK:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->curPvrMode:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    goto/16 :goto_1

    :cond_a
    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->setBarStatusOfRecordToPlay()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_7
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_b
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
    .end packed-switch
.end method

.method public onKeyRecord()V
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const/16 v12, 0x1f4

    const/4 v11, 0x1

    const-string v8, "PVRActivity"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "===========>>>> pvr recordDiskPath["

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v10}, Lcom/konka/kkinterface/tv/PvrDesk;->getPvrMountPath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]DeviceIndex["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v10}, Lcom/konka/kkinterface/tv/PvrDesk;->getUsbDeviceIndex()S

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvrImageFlag:Lcom/konka/tvsettings/popup/PvrImageFlag;

    invoke-virtual {v8}, Lcom/konka/tvsettings/popup/PvrImageFlag;->isRecorderFlag()Z

    move-result v8

    if-nez v8, :cond_c

    iget-object v8, p0, Lcom/konka/tvsettings/popup/PvrActivity;->usbSelecter:Lcom/konka/tvsettings/function/USBDiskSelecter;

    invoke-virtual {v8}, Lcom/konka/tvsettings/function/USBDiskSelecter;->getDriverCount()I

    move-result v7

    if-gtz v7, :cond_1

    iget-object v8, p0, Lcom/konka/tvsettings/popup/PvrActivity;->activity:Landroid/app/Activity;

    const v9, 0x7f0a00eb    # com.konka.tvsettings.R.string.str_pvr_insert_usb

    invoke-static {v8, v9, v12}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Toast;->show()V

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->isBootedByRecord()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->standbySystem()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->getBestDiskPath()Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x0

    const/4 v1, 0x0

    const-string v8, "qhc"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "getBestDiskPath:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, "NO_DISK"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/konka/tvsettings/popup/PvrActivity;->activity:Landroid/app/Activity;

    const v9, 0x7f0a00eb    # com.konka.tvsettings.R.string.str_pvr_insert_usb

    invoke-static {v8, v9, v12}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Toast;->show()V

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->isBootedByRecord()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->standbySystem()V

    goto :goto_0

    :cond_2
    const-string v8, "CHOOSE_DISK"

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->isBootedByRecord()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->getAvaliableDiskForStandBy()Ljava/lang/String;

    move-result-object v2

    const-string v8, "qhc1"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "getAvaliableDiskForStandBy="

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v2, :cond_4

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->standbySystem()V

    goto :goto_0

    :cond_3
    const-string v8, "PvrActivity"

    const-string v9, "---------Usb Selecter start"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/konka/tvsettings/popup/PvrActivity;->usbSelecter:Lcom/konka/tvsettings/function/USBDiskSelecter;

    invoke-virtual {v8}, Lcom/konka/tvsettings/function/USBDiskSelecter;->start()V

    goto :goto_0

    :cond_4
    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v8}, Lcom/konka/tvsettings/popup/PvrActivity;->getUsbIndexByPath(Ljava/lang/String;)I

    move-result v5

    if-ltz v5, :cond_5

    iget-object v8, p0, Lcom/konka/tvsettings/popup/PvrActivity;->usbDriverLabel:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v5, v8, :cond_5

    iget-object v8, p0, Lcom/konka/tvsettings/popup/PvrActivity;->usbDriverLabel:Ljava/util/ArrayList;

    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v8, p0, Lcom/konka/tvsettings/popup/PvrActivity;->usbDriverLabelPcStyle:Ljava/util/ArrayList;

    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    :cond_5
    iput-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordDiskPath:Ljava/lang/String;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordDiskLable:Ljava/lang/String;

    iget-object v8, p0, Lcom/konka/tvsettings/popup/PvrActivity;->usbLabel:Landroid/widget/TextView;

    invoke-virtual {v8, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v8, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordDiskPath:Ljava/lang/String;

    iget-object v9, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordDiskLable:Ljava/lang/String;

    invoke-direct {p0, v11, v8, v9, v1}, Lcom/konka/tvsettings/popup/PvrActivity;->saveChooseDiskSettings(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "PVR"

    const-string v9, "=============>>>>> disk has been set in PVR Option menu,get it..."

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v8, "PVR"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "========>>>>> recordDiskPath["

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordDiskPath:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]recordDiskLable["

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordDiskLable:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "]"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-boolean v8, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isOneTouchPauseMode:Z

    if-nez v8, :cond_6

    iget-boolean v8, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isAlwaysTimeShiftBegin:Z

    if-eqz v8, :cond_9

    :cond_6
    const-string v4, "FAT"

    iget-object v8, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordDiskLable:Ljava/lang/String;

    const/4 v9, 0x6

    const/4 v10, 0x0

    const/4 v11, 0x3

    invoke-virtual {v8, v9, v4, v10, v11}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v8

    if-eqz v8, :cond_8

    iget-boolean v8, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isAlwaysTimeShiftBegin:Z

    if-eqz v8, :cond_7

    const/4 v8, 0x1

    invoke-direct {p0, v8}, Lcom/konka/tvsettings/popup/PvrActivity;->doPVRTimeShift(Z)V

    new-instance v8, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    const/4 v9, 0x0

    invoke-direct {v8, p0, v9}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;)V

    invoke-virtual {v8}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->start()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->finish()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0

    :cond_7
    const/4 v8, 0x1

    :try_start_1
    invoke-direct {p0, v8}, Lcom/konka/tvsettings/popup/PvrActivity;->doPVRTimeShift(Z)V

    new-instance v8, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    const/4 v9, 0x0

    invoke-direct {v8, p0, v9}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;)V

    invoke-virtual {v8}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->start()V

    goto/16 :goto_0

    :cond_8
    iget-object v8, p0, Lcom/konka/tvsettings/popup/PvrActivity;->activity:Landroid/app/Activity;

    const v9, 0x7f0a018a    # com.konka.tvsettings.R.string.str_pvr_unsurpt_flsystem

    const/16 v10, 0x1f4

    invoke-static {v8, v9, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->finish()V

    goto/16 :goto_0

    :cond_9
    const-string v4, "FAT"

    const-string v6, "NTFS"

    iget-object v8, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordDiskLable:Ljava/lang/String;

    const/4 v9, 0x6

    const/4 v10, 0x0

    const/4 v11, 0x3

    invoke-virtual {v8, v9, v4, v10, v11}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v8

    if-eqz v8, :cond_a

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "PvrActivity=====>>>Start 111..."

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "..."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const/4 v8, 0x1

    invoke-direct {p0, v8}, Lcom/konka/tvsettings/popup/PvrActivity;->doPVRRecord(Z)V

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "PvrActivity=====>>>Start 222..."

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "..."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v8, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    const/4 v9, 0x0

    invoke-direct {v8, p0, v9}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;)V

    invoke-virtual {v8}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->start()V

    goto/16 :goto_0

    :cond_a
    iget-object v8, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recordDiskLable:Ljava/lang/String;

    const/4 v9, 0x6

    const/4 v10, 0x0

    const/4 v11, 0x4

    invoke-virtual {v8, v9, v6, v10, v11}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v8

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/konka/tvsettings/popup/PvrActivity;->activity:Landroid/app/Activity;

    const v9, 0x7f0a018a    # com.konka.tvsettings.R.string.str_pvr_unsurpt_flsystem

    const/16 v10, 0x1f4

    invoke-static {v8, v9, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Toast;->show()V

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->isBootedByRecord()Z

    move-result v8

    if-eqz v8, :cond_b

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->standbySystem()V

    :cond_b
    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->finish()V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    :cond_c
    iget-object v8, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/PvrDesk;->isRecordPaused()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-direct {p0, v11}, Lcom/konka/tvsettings/popup/PvrActivity;->doPVRRecord(Z)V

    goto/16 :goto_0
.end method

.method public onKeyRev()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const v3, 0x7f0a00fa    # com.konka.tvsettings.R.string.str_player_rev

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->isPlaybacking()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->getPlaybackSpeed()Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    move-result-object v0

    invoke-static {}, Lcom/konka/tvsettings/popup/PvrActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$PvrPlaybackSpeed$EnumPvrPlaybackSpeed()[I

    move-result-object v1

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_1XFB:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PvrDesk;->setPlaybackSpeed(Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/popup/PvrActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->setBarStatusOfPlayToOthers()V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->rev:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->requestFocus()Z

    :cond_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_2XFB:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PvrDesk;->setPlaybackSpeed(Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/popup/PvrActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " 2X"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_4XFB:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PvrDesk;->setPlaybackSpeed(Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/popup/PvrActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " 4X"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_8XFB:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PvrDesk;->setPlaybackSpeed(Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/popup/PvrActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " 8X"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_16XFB:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PvrDesk;->setPlaybackSpeed(Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/popup/PvrActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " 16X"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_32XFB:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PvrDesk;->setPlaybackSpeed(Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/popup/PvrActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " 32X"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_5
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_1XFB:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PvrDesk;->setPlaybackSpeed(Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/popup/PvrActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onKeySlowMotion()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const v3, 0x7f0a00fc    # com.konka.tvsettings.R.string.str_player_slow

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->isPlaybacking()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->getPlaybackSpeed()Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    move-result-object v0

    invoke-static {}, Lcom/konka/tvsettings/popup/PvrActivity;->$SWITCH_TABLE$com$mstar$android$tvapi$common$vo$PvrPlaybackSpeed$EnumPvrPlaybackSpeed()[I

    move-result-object v1

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_FF_1_2X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PvrDesk;->setPlaybackSpeed(Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/popup/PvrActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " 2X"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->setBarStatusOfPlayToOthers()V

    :cond_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_FF_1_2X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PvrDesk;->setPlaybackSpeed(Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/popup/PvrActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " 2X"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_1X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PvrDesk;->setPlaybackSpeed(Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_FF_1_32X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PvrDesk;->setPlaybackSpeed(Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/popup/PvrActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " 32X"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_FF_1_16X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PvrDesk;->setPlaybackSpeed(Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/popup/PvrActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " 16X"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_FF_1_8X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PvrDesk;->setPlaybackSpeed(Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/popup/PvrActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " 8X"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_5
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;->E_PVR_PLAYBACK_SPEED_FF_1_4X:Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PvrDesk;->setPlaybackSpeed(Lcom/mstar/android/tvapi/common/vo/PvrPlaybackSpeed$EnumPvrPlaybackSpeed;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->playbackText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/popup/PvrActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " 4X"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onKeyStop()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/tvapi/common/exception/TvCommonException;
        }
    .end annotation

    const-string v0, "PVR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "=========>>>> curPvrMode is :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->curPvrMode:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->curPvrMode:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    sget-object v1, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;->E_PVR_MODE_NONE:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->stop:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->requestFocus()Z

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Intent;

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onNewIntent(Landroid/content/Intent;)V

    invoke-virtual {p0, p1}, Lcom/konka/tvsettings/popup/PvrActivity;->setIntent(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "PVR_ONE_TOUCH_MODE"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isNotifyRecordStop:Z

    iget-boolean v1, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isNotifyRecordStop:Z

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->saveAndExit()V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PvrManager;->getIsBootByRecord()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/PvrManager;->setIsBootByRecord(Z)V

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getInstance()Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v1

    const-string v2, "pvr"

    invoke-virtual {v1, v2}, Lcom/mstar/android/tv/TvCommonManager;->standbySystem(Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method public onPvrNotifyFormatFinished(Lcom/mstar/android/tvapi/common/PvrManager;III)Z
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/PvrManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const-string v0, "PVR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "==========>>> onPvrNotifyFormatFinished = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public onPvrNotifyPlaybackBegin(Lcom/mstar/android/tvapi/common/PvrManager;III)Z
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/PvrManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const-string v0, "PVR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "==========>>> onPvrNotifyPlaybackBegin = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public onPvrNotifyPlaybackStop(Lcom/mstar/android/tvapi/common/PvrManager;III)Z
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/PvrManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const-string v0, "PVR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "==========>>> onPvrNotifyPlaybackStop = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public onPvrNotifyUsbInserted(Lcom/mstar/android/tvapi/common/PvrManager;III)Z
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/PvrManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const-string v0, "PVR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "==========>>> onPvrNotifyUsbInserted = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public onPvrNotifyUsbRemoved(Lcom/mstar/android/tvapi/common/PvrManager;III)Z
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/PvrManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const-string v0, "PVR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "==========>>> onPvrNotifyUsbRemoved = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public onResume()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onResume()V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;

    invoke-interface {v0, p0}, Lcom/konka/kkinterface/tv/PvrDesk;->setOnPvrEventListener(Lcom/mstar/android/tvapi/common/PvrManager$OnPvrEventListener;)V

    iput-boolean v2, p0, Lcom/konka/tvsettings/popup/PvrActivity;->isWatchRcodFilInRcoding:Z

    const-string v0, "PVRActivity"

    const-string v1, "==========>>> onResume "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/popup/PvrActivity;->isGoingToBeClosed(Z)V

    return-void
.end method

.method protected onStop()V
    .locals 1

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onStop()V

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/tvsettings/popup/PvrActivity;->isPVRActivityActive:Z

    return-void
.end method

.method public onTimeOut()V
    .locals 0

    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onTimeOut()V

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/PvrActivity;->menuHide()V

    return-void
.end method

.method public setBarStatusOfPlayToOthers()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recorder:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->play:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->stop:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pause:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->rev:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->ff:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->slow:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->time:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recorder:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->play:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->stop:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pause:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->rev:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->ff:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->slow:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->time:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    return-void
.end method

.method public setBarStatusOfPlayToPause()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recorder:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->play:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->stop:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pause:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->rev:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->ff:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->slow:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->time:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recorder:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->play:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->stop:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pause:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->rev:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->ff:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->slow:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->time:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    return-void
.end method

.method public setBarStatusOfRecordToPause()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recorder:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->play:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->stop:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pause:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->rev:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->ff:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->slow:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->time:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recorder:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->play:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->stop:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pause:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->rev:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->ff:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->slow:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->time:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    return-void
.end method

.method public setBarStatusOfRecordToPlay()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recorder:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->play:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->stop:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pause:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->rev:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->ff:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->slow:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->time:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recorder:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->play:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->stop:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pause:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->rev:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->ff:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->slow:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->time:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    return-void
.end method

.method public setBarStatusOfStartRecord()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recorder:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->play:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->stop:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pause:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->rev:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->ff:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->slow:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->time:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recorder:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->play:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->stop:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pause:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->rev:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->ff:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->slow:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->time:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    return-void
.end method

.method public setBarStatusOfStartTimeShift()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recorder:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->play:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->stop:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pause:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->rev:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->ff:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->slow:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->time:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->recorder:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->play:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->stop:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->pause:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->rev:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->ff:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->slow:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity;->time:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    return-void
.end method
