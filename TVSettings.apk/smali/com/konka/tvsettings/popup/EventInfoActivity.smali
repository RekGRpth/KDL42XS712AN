.class public Lcom/konka/tvsettings/popup/EventInfoActivity;
.super Landroid/app/Activity;
.source "EventInfoActivity.java"


# instance fields
.field private m_CancelBtn:Landroid/widget/Button;

.field private m_ContentText:Landroid/widget/TextView;

.field private m_EventName:Landroid/widget/TextView;

.field private strDescrib:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/EventInfoActivity;->m_ContentText:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/EventInfoActivity;->m_CancelBtn:Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/EventInfoActivity;->m_EventName:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/EventInfoActivity;->strDescrib:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030032    # com.konka.tvsettings.R.layout.popwin_eventinfo

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/EventInfoActivity;->setContentView(I)V

    return-void
.end method

.method protected onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/EventInfoActivity;->finish()V

    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    :cond_1
    const/16 v0, 0x42

    if-eq p1, v0, :cond_2

    const/16 v0, 0x17

    if-ne p1, v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/konka/tvsettings/popup/EventInfoActivity;->m_CancelBtn:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "aaaaaaaaaaa"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/EventInfoActivity;->finish()V

    goto :goto_0

    :cond_3
    const/16 v0, 0x13

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/popup/EventInfoActivity;->m_CancelBtn:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "EpgEventInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "========>>>keyCode=["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/tvsettings/popup/EventInfoActivity;->m_ContentText:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    goto :goto_0
.end method

.method protected onPause()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/EventInfoActivity;->finish()V

    return-void
.end method

.method protected onResume()V
    .locals 9

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    const v4, 0x7f070150    # com.konka.tvsettings.R.id.epg_popwin_eventinfo_content

    invoke-virtual {p0, v4}, Lcom/konka/tvsettings/popup/EventInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/konka/tvsettings/popup/EventInfoActivity;->m_ContentText:Landroid/widget/TextView;

    const v4, 0x7f070151    # com.konka.tvsettings.R.id.epg_popwin_cancel_btn

    invoke-virtual {p0, v4}, Lcom/konka/tvsettings/popup/EventInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lcom/konka/tvsettings/popup/EventInfoActivity;->m_CancelBtn:Landroid/widget/Button;

    const v4, 0x7f07014e    # com.konka.tvsettings.R.id.epg_popwin_evetinfo_eventtitle

    invoke-virtual {p0, v4}, Lcom/konka/tvsettings/popup/EventInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/konka/tvsettings/popup/EventInfoActivity;->m_EventName:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/konka/tvsettings/popup/EventInfoActivity;->strDescrib:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/konka/tvsettings/popup/EventInfoActivity;->strDescrib:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    :cond_0
    iget-object v4, p0, Lcom/konka/tvsettings/popup/EventInfoActivity;->m_CancelBtn:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->requestFocus()Z

    iget-object v4, p0, Lcom/konka/tvsettings/popup/EventInfoActivity;->m_CancelBtn:Landroid/widget/Button;

    new-instance v5, Lcom/konka/tvsettings/popup/EventInfoActivity$1;

    invoke-direct {v5, p0}, Lcom/konka/tvsettings/popup/EventInfoActivity$1;-><init>(Lcom/konka/tvsettings/popup/EventInfoActivity;)V

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v2

    new-instance v3, Landroid/text/format/Time;

    invoke-direct {v3}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v3}, Landroid/text/format/Time;->setToNow()V

    :try_start_0
    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getEpgManagerInstance()Lcom/konka/kkinterface/tv/EpgDesk;

    move-result-object v4

    iget-short v5, v2, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    iget v6, v2, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    invoke-interface {v4, v5, v6, v3}, Lcom/konka/kkinterface/tv/EpgDesk;->getEventInfoByTime(SILandroid/text/format/Time;)Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;

    move-result-object v1

    iget-object v4, p0, Lcom/konka/tvsettings/popup/EventInfoActivity;->m_EventName:Landroid/widget/TextView;

    iget-object v5, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->name:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getEpgManagerInstance()Lcom/konka/kkinterface/tv/EpgDesk;

    move-result-object v4

    iget v5, v1, Lcom/mstar/android/tvapi/dtv/vo/EpgEventInfo;->startTime:I

    invoke-interface {v4, v5}, Lcom/konka/kkinterface/tv/EpgDesk;->getEventDetailDescriptor(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/tvsettings/popup/EventInfoActivity;->strDescrib:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v4, p0, Lcom/konka/tvsettings/popup/EventInfoActivity;->strDescrib:Ljava/lang/String;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/konka/tvsettings/popup/EventInfoActivity;->strDescrib:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v5, 0x2

    if-ge v4, v5, :cond_2

    :cond_1
    iget-object v4, p0, Lcom/konka/tvsettings/popup/EventInfoActivity;->m_ContentText:Landroid/widget/TextView;

    const v5, 0x7f0a0107    # com.konka.tvsettings.R.string.epg_popwin_evnetinfo_null_str

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/konka/tvsettings/popup/EventInfoActivity;->strDescrib:Ljava/lang/String;

    iget-object v5, p0, Lcom/konka/tvsettings/popup/EventInfoActivity;->strDescrib:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x2

    iget-object v6, p0, Lcom/konka/tvsettings/popup/EventInfoActivity;->strDescrib:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    const-string v5, "\u3002"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/konka/tvsettings/popup/EventInfoActivity;->m_ContentText:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "\t\t "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/konka/tvsettings/popup/EventInfoActivity;->strDescrib:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    iget-object v4, p0, Lcom/konka/tvsettings/popup/EventInfoActivity;->m_ContentText:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "\t\t "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/konka/tvsettings/popup/EventInfoActivity;->strDescrib:Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/konka/tvsettings/popup/EventInfoActivity;->strDescrib:Ljava/lang/String;

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "......"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method protected onStart()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    return-void
.end method

.method protected onStop()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method
