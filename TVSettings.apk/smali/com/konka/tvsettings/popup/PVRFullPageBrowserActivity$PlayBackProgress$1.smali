.class Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress$1;
.super Ljava/lang/Object;
.source "PVRFullPageBrowserActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    :try_start_0
    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    move-result-object v3

    # getter for: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$3(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/PvrDesk;->getCurPlaybackTimeInSecond()I

    move-result v0

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    move-result-object v3

    # getter for: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->isRecordingItem:Z
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$4(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    move-result-object v3

    # getter for: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$3(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/PvrDesk;->isRecording()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    move-result-object v3

    # getter for: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$3(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/PvrDesk;->getCurRecordTimeInSecond()I

    move-result v2

    :goto_0
    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    move-result-object v3

    # getter for: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->totalRecordTime:Landroid/widget/TextView;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$5(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)Landroid/widget/TextView;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;
    invoke-static {v4}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    move-result-object v4

    # invokes: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->getTimeString(I)Ljava/lang/String;
    invoke-static {v4, v2}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$6(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    move-result-object v3

    # getter for: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->PVRPlaybackProgress:Landroid/widget/ProgressBar;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$7(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)Landroid/widget/ProgressBar;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/widget/ProgressBar;->setMax(I)V

    :cond_0
    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    move-result-object v3

    # getter for: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->PVRPlaybackProgress:Landroid/widget/ProgressBar;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$7(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)Landroid/widget/ProgressBar;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    :goto_1
    return-void

    :cond_1
    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    move-result-object v3

    # getter for: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$3(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/PvrDesk;->getCurPlaybackTimeInSecond()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method
