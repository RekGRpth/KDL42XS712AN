.class Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$4;
.super Ljava/lang/Object;
.source "PVRFullPageBrowserActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$4;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    const/4 v7, 0x1

    :try_start_0
    const-string v2, "PVR Browser ========>>>create 5 "

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    const/4 v3, 0x1

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$4;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    invoke-static {v6}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v6

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z

    :cond_0
    iget-object v2, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$4;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    # getter for: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v2}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$3(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/konka/kkinterface/tv/PvrDesk;->jumpPlaybackTime(I)Z

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$4;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    # invokes: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->scaleToFullScreen()V
    invoke-static {v2}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$10(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v2, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$4;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    invoke-static {v2, v7}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$11(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;Z)V

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$4;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    const-class v3, Lcom/konka/tvsettings/popup/PvrActivity;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v2, "FullPageBrowserCall"

    invoke-virtual {v1, v2, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v2, "Current_Playback_Index"

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$4;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    # getter for: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->current_pvr_position:I
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$12(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$4;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    invoke-virtual {v2, v1}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$4;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    invoke-virtual {v2}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->finish()V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method
