.class Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$6;
.super Ljava/lang/Object;
.source "PVRFullPageBrowserActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->onStop()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$6;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$6;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    # getter for: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->IsItemClicked:Z
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$17(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$6;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    # getter for: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$3(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->isPlaybacking()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$6;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    # getter for: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$3(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->stopPlayback()V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$6;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    # getter for: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$3(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->stopPlaybackLoop()V

    :cond_0
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$6;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    # getter for: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->IsScalerInitialized:Z
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$18(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$6;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    # invokes: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->scaleToFullScreen()V
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$10(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)V

    :cond_1
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$6;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    invoke-static {v1}, Lcom/konka/tvsettings/SwitchMenuHelper;->stopSwitchInput(Landroid/app/Activity;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method
