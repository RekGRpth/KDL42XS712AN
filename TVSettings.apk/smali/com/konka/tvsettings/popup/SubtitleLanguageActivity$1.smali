.class Lcom/konka/tvsettings/popup/SubtitleLanguageActivity$1;
.super Ljava/lang/Object;
.source "SubtitleLanguageActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemSelectedListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/SubtitleLanguageActivity$1;->this$0:Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v5, p0, Lcom/konka/tvsettings/popup/SubtitleLanguageActivity$1;->this$0:Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;

    invoke-virtual {v5}, Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/TVRootApp;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v1

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v5

    invoke-static {v5}, Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;->access$0(I)V

    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "\n=====>Set subtitle language index:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    # getter for: Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;->subtitlePos:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;->access$1()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/ChannelDesk;->closeSubtitle()Z

    # getter for: Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;->subtitlePos:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;->access$1()I

    move-result v5

    if-lez v5, :cond_0

    # getter for: Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;->subtitlePos:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;->access$1()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-interface {v1, v5}, Lcom/konka/kkinterface/tv/ChannelDesk;->openSubtitle(I)Z

    :cond_0
    iget-object v5, p0, Lcom/konka/tvsettings/popup/SubtitleLanguageActivity$1;->this$0:Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;

    const-string v6, "TvSetting"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v5, "subtitlePos"

    # getter for: Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;->subtitlePos:I
    invoke-static {}, Lcom/konka/tvsettings/popup/SubtitleLanguageActivity;->access$1()I

    move-result v6

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    return-void
.end method
