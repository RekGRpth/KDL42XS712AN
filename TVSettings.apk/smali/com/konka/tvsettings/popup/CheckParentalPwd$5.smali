.class Lcom/konka/tvsettings/popup/CheckParentalPwd$5;
.super Ljava/lang/Object;
.source "CheckParentalPwd.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/popup/CheckParentalPwd;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/CheckParentalPwd;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/popup/CheckParentalPwd;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd$5;->this$0:Lcom/konka/tvsettings/popup/CheckParentalPwd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$4(I)V

    invoke-static {v2}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$10(I)V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd$5;->this$0:Lcom/konka/tvsettings/popup/CheckParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/CheckParentalPwd;->lockType:I
    invoke-static {v2}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$17(Lcom/konka/tvsettings/popup/CheckParentalPwd;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :goto_0
    :pswitch_0
    iget-object v2, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd$5;->this$0:Lcom/konka/tvsettings/popup/CheckParentalPwd;

    invoke-virtual {v2}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->finish()V

    return-void

    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd$5;->this$0:Lcom/konka/tvsettings/popup/CheckParentalPwd;

    invoke-virtual {v2}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->getApplication()Landroid/app/Application;

    move-result-object v2

    const-class v3, Lcom/konka/tvsettings/channel/ProgramSettingMain;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd$5;->this$0:Lcom/konka/tvsettings/popup/CheckParentalPwd;

    invoke-virtual {v2, v0}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_2
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd$5;->this$0:Lcom/konka/tvsettings/popup/CheckParentalPwd;

    invoke-virtual {v2}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->getApplication()Landroid/app/Application;

    move-result-object v2

    const-class v3, Lcom/konka/tvsettings/function/FunctionSettingActivity;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd$5;->this$0:Lcom/konka/tvsettings/popup/CheckParentalPwd;

    invoke-virtual {v2, v1}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
