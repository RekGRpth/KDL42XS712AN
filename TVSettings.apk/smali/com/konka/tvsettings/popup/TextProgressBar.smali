.class public Lcom/konka/tvsettings/popup/TextProgressBar;
.super Landroid/widget/ProgressBar;
.source "TextProgressBar.java"


# instance fields
.field private drawingText:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/tvsettings/popup/TextProgressBar;->drawingText:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/tvsettings/popup/TextProgressBar;->drawingText:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/tvsettings/popup/TextProgressBar;->drawingText:Ljava/lang/String;

    return-void
.end method

.method private dip2px(F)I
    .locals 3
    .param p1    # F

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/TextProgressBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v0, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float v1, p1, v0

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v1, v1

    return v1
.end method


# virtual methods
.method protected declared-synchronized onDraw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1    # Landroid/graphics/Canvas;

    const/4 v2, 0x1

    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Landroid/widget/ProgressBar;->onDraw(Landroid/graphics/Canvas;)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    const/4 v3, -0x1

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    const/high16 v3, 0x41200000    # 10.0f

    invoke-direct {p0, v3}, Lcom/konka/tvsettings/popup/TextProgressBar;->dip2px(F)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    sget-object v3, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    sget-object v3, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/TextProgressBar;->getProgress()I

    move-result v3

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/TextProgressBar;->getWidth()I

    move-result v4

    mul-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/TextProgressBar;->getMax()I

    move-result v4

    if-nez v4, :cond_0

    :goto_0
    div-int v2, v3, v2

    int-to-float v1, v2

    iget-object v3, p0, Lcom/konka/tvsettings/popup/TextProgressBar;->drawingText:Ljava/lang/String;

    const/high16 v2, 0x42200000    # 40.0f

    invoke-direct {p0, v2}, Lcom/konka/tvsettings/popup/TextProgressBar;->dip2px(F)I

    move-result v2

    int-to-float v2, v2

    cmpl-float v2, v1, v2

    if-lez v2, :cond_1

    const/high16 v2, 0x42200000    # 40.0f

    invoke-direct {p0, v2}, Lcom/konka/tvsettings/popup/TextProgressBar;->dip2px(F)I

    move-result v2

    int-to-float v2, v2

    sub-float v2, v1, v2

    :goto_1
    const/high16 v4, 0x41200000    # 10.0f

    invoke-direct {p0, v4}, Lcom/konka/tvsettings/popup/TextProgressBar;->dip2px(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p1, v3, v2, v4, v0}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/TextProgressBar;->getMax()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public setTextProgress(Ljava/lang/String;I)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # I

    iput-object p1, p0, Lcom/konka/tvsettings/popup/TextProgressBar;->drawingText:Ljava/lang/String;

    invoke-virtual {p0, p2}, Lcom/konka/tvsettings/popup/TextProgressBar;->setProgress(I)V

    return-void
.end method
