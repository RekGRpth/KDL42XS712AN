.class Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$1;
.super Lcom/konka/tvsettings/function/USBDiskSelecter;
.source "PVRFullPageBrowserActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;Landroid/content/Context;)V
    .locals 0
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$1;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    invoke-direct {p0, p2}, Lcom/konka/tvsettings/function/USBDiskSelecter;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onItemCancel()V
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$1;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    invoke-virtual {v0}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->finish()V

    return-void
.end method

.method public onItemChosen(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;

    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "/_MSTPVR"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "PVRFBActivity"

    const-string v3, "=============>>>>> USB Disk Path is NULL !!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const-string v2, "PVRFBActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "=============>>>>> USB Disk Path = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$1;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    # getter for: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v2}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$3(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/PvrDesk;->clearMetaData()V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$1;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    # getter for: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v2}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$3(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {v2, p3, v3}, Lcom/konka/kkinterface/tv/PvrDesk;->setPVRParas(Ljava/lang/String;S)Z

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$1;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    # getter for: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v2}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$3(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/konka/kkinterface/tv/PvrDesk;->createMetaData(Ljava/lang/String;)Z

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$1;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    const/4 v3, 0x1

    # invokes: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->saveChooseDiskSettings(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v2, v3, p3, p2, p4}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$8(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity$1;->this$0:Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;

    # invokes: Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->init()V
    invoke-static {v2}, Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;->access$9(Lcom/konka/tvsettings/popup/PVRFullPageBrowserActivity;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method
