.class Lcom/konka/tvsettings/popup/PvrActivity$20;
.super Landroid/animation/AnimatorListenerAdapter;
.source "PvrActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/popup/PvrActivity;->createAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/PvrActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/popup/PvrActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/PvrActivity$20;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 4
    .param p1    # Landroid/animation/Animator;

    const/4 v3, 0x0

    const-string v0, "PVR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "=============>>>> onAnimationStart = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity$20;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->isMenuHide:Z
    invoke-static {v2}, Lcom/konka/tvsettings/popup/PvrActivity;->access$44(Lcom/konka/tvsettings/popup/PvrActivity;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity$20;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    invoke-static {v0, v3}, Lcom/konka/tvsettings/popup/PvrActivity;->access$45(Lcom/konka/tvsettings/popup/PvrActivity;Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity$20;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->rootView:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/konka/tvsettings/popup/PvrActivity;->access$46(Lcom/konka/tvsettings/popup/PvrActivity;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity$20;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->rootView:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/konka/tvsettings/popup/PvrActivity;->access$46(Lcom/konka/tvsettings/popup/PvrActivity;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->requestFocus()Z

    return-void
.end method
