.class Lcom/konka/tvsettings/popup/SourceInfoActivity$2;
.super Landroid/content/BroadcastReceiver;
.source "SourceInfoActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/popup/SourceInfoActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/SourceInfoActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/popup/SourceInfoActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity$2;->this$0:Lcom/konka/tvsettings/popup/SourceInfoActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity$2;->this$0:Lcom/konka/tvsettings/popup/SourceInfoActivity;

    invoke-virtual {v0}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity$2;->this$0:Lcom/konka/tvsettings/popup/SourceInfoActivity;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity$2;->this$0:Lcom/konka/tvsettings/popup/SourceInfoActivity;

    # getter for: Lcom/konka/tvsettings/popup/SourceInfoActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->access$0(Lcom/konka/tvsettings/popup/SourceInfoActivity;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->access$3(Lcom/konka/tvsettings/popup/SourceInfoActivity;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity$2;->this$0:Lcom/konka/tvsettings/popup/SourceInfoActivity;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->access$2(Lcom/konka/tvsettings/popup/SourceInfoActivity;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity$2;->this$0:Lcom/konka/tvsettings/popup/SourceInfoActivity;

    invoke-virtual {v0}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->updateInfo()V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity$2;->this$0:Lcom/konka/tvsettings/popup/SourceInfoActivity;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity$2;->this$0:Lcom/konka/tvsettings/popup/SourceInfoActivity;

    # getter for: Lcom/konka/tvsettings/popup/SourceInfoActivity;->meInputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->access$4(Lcom/konka/tvsettings/popup/SourceInfoActivity;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->getVideoInfo(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity$2;->this$0:Lcom/konka/tvsettings/popup/SourceInfoActivity;

    # getter for: Lcom/konka/tvsettings/popup/SourceInfoActivity;->mViewTvInfo:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->access$5(Lcom/konka/tvsettings/popup/SourceInfoActivity;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity$2;->this$0:Lcom/konka/tvsettings/popup/SourceInfoActivity;

    # getter for: Lcom/konka/tvsettings/popup/SourceInfoActivity;->mstrText:Ljava/lang/String;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->access$6(Lcom/konka/tvsettings/popup/SourceInfoActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/SourceInfoActivity$2;->this$0:Lcom/konka/tvsettings/popup/SourceInfoActivity;

    # invokes: Lcom/konka/tvsettings/popup/SourceInfoActivity;->updateChannelInfo()V
    invoke-static {v0}, Lcom/konka/tvsettings/popup/SourceInfoActivity;->access$7(Lcom/konka/tvsettings/popup/SourceInfoActivity;)V

    :cond_0
    return-void
.end method
