.class public Lcom/konka/tvsettings/popup/InstallationGuideActivity;
.super Landroid/app/Activity;
.source "InstallationGuideActivity.java"


# static fields
.field protected static final Arabic:I = 0x1

.field protected static final Chinese:I = 0x9

.field protected static final ENGLISH:I = 0x0

.field protected static final Franch:I = 0x5

.field protected static final Hebrew:I = 0x8

.field protected static final Indonesia:I = 0x6

.field protected static final Kurdish:I = 0x3

.field protected static final Persian:I = 0x2

.field public static PersionCustomer:Ljava/lang/String; = null

.field public static PersionCustomer1:Ljava/lang/String; = null

.field protected static final Russia:I = 0x4

.field protected static final Thai:I = 0xa

.field protected static final Turkish:I = 0x7


# instance fields
.field private AntennaTypeLayout:Landroid/widget/LinearLayout;

.field private AutoSearchLayout:Landroid/widget/LinearLayout;

.field private CountrySelectLayout:Landroid/widget/LinearLayout;

.field private CountrySelectTable:[I

.field private DvbcEnable:Z

.field private LanguageSelectLayout:Landroid/widget/LinearLayout;

.field private commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

.field public curCustomer:Ljava/lang/String;

.field private itemAntennaType:Lcom/konka/tvsettings/picture/PictureSettingItem1;

.field private language_values:[Ljava/lang/CharSequence;

.field private m_iCurrLangIndex:I

.field private miCurCountry:I

.field private serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

.field private settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;

.field private tableCountrySelect_index:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "snowa"

    sput-object v0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->PersionCustomer:Ljava/lang/String;

    const-string v0, "x.vision"

    sput-object v0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->PersionCustomer1:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-boolean v3, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->DvbcEnable:Z

    iput-object v0, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->LanguageSelectLayout:Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->CountrySelectLayout:Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->AntennaTypeLayout:Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->AutoSearchLayout:Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->language_values:[Ljava/lang/CharSequence;

    iput v3, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->m_iCurrLangIndex:I

    iput v3, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->miCurCountry:I

    iput-object v0, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->itemAntennaType:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    const/16 v0, 0x3d

    new-array v0, v0, [I

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRALIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v3

    const/4 v1, 0x1

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BELGIUM:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BULGARIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CROATIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CZECH:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_DENMARK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FINLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FRANCE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GERMANY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GREECE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_HUNGARY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ITALY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LUXEMBOURG:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NETHERLANDS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NORWAY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_POLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PORTUGAL:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUMANIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUSSIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SERBIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SLOVENIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SPAIN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SWEDEN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SWITZERLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_UK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NEWZEALAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ARAB:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ESTONIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_HEBREW:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LATVIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SLOVAKIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TURKEY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRELAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FIJI:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_UZBEK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TAJIKISTAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ETHIOPIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AZERBAIJAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SOUTHAFRICA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ALGERIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_EGYPT:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SAUDI_ARABIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRAQ:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NAMIBIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_JORDAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_KUWAIT:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_INDONESIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ISRAEL:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_QATAR:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NIGERIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ZEMBABWE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LITHUANIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_MOROCCO:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TUNIS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_INDIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_YEMEN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LIBYA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PAKISTAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_OTHERS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    iput-object v0, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->CountrySelectTable:[I

    iput v3, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->tableCountrySelect_index:I

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/popup/InstallationGuideActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->itemAntennaType:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/popup/InstallationGuideActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/popup/InstallationGuideActivity;)Lcom/konka/kkinterface/tv/SettingDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/popup/InstallationGuideActivity;Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->setSystemTimeZone(Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;)V

    return-void
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/popup/InstallationGuideActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->miCurCountry:I

    return v0
.end method

.method private addItemAntennaType()V
    .locals 7

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->getSettingMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;

    move-result-object v6

    invoke-virtual {v6}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->getDvbMode()I

    move-result v5

    new-instance v0, Lcom/konka/tvsettings/popup/InstallationGuideActivity$3;

    const v3, 0x7f0700ce    # com.konka.tvsettings.R.id.linearlayout_install_guide_antennatype

    const v4, 0x7f0b003d    # com.konka.tvsettings.R.array.str_arr_antennatype

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/konka/tvsettings/popup/InstallationGuideActivity$3;-><init>(Lcom/konka/tvsettings/popup/InstallationGuideActivity;Landroid/app/Activity;IIILcom/konka/kkimplements/tv/mstar/SettingDeskImpl;)V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->itemAntennaType:Lcom/konka/tvsettings/picture/PictureSettingItem1;

    return-void
.end method

.method private addItemSelectCountry()V
    .locals 2

    const v0, 0x7f0700cb    # com.konka.tvsettings.R.id.linearlayout_install_guide_country

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->CountrySelectLayout:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->updateCountryData()V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->CountrySelectLayout:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/tvsettings/popup/InstallationGuideActivity$2;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/popup/InstallationGuideActivity$2;-><init>(Lcom/konka/tvsettings/popup/InstallationGuideActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private addItemSelectLanguage()V
    .locals 2

    const v0, 0x7f0700c8    # com.konka.tvsettings.R.id.linearlayout_install_guide_language

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->LanguageSelectLayout:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->updateLanguageData()V

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->updateCurrentLang()V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->LanguageSelectLayout:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/tvsettings/popup/InstallationGuideActivity$1;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/popup/InstallationGuideActivity$1;-><init>(Lcom/konka/tvsettings/popup/InstallationGuideActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private addItemStartAutoSearch()V
    .locals 2

    const v0, 0x7f0700d1    # com.konka.tvsettings.R.id.linearlayout_install_guide_tuning

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->AutoSearchLayout:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->AutoSearchLayout:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/tvsettings/popup/InstallationGuideActivity$4;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/popup/InstallationGuideActivity$4;-><init>(Lcom/konka/tvsettings/popup/InstallationGuideActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private addView()V
    .locals 2

    const v0, 0x7f0700ce    # com.konka.tvsettings.R.id.linearlayout_install_guide_antennatype

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->AntennaTypeLayout:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->addItemSelectLanguage()V

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->addItemSelectCountry()V

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->addItemStartAutoSearch()V

    iget-boolean v0, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->DvbcEnable:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->addItemAntennaType()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->AntennaTypeLayout:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private setSystemTimeZone(Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;)V
    .locals 4
    .param p1    # Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    const-string v2, "Asia/Shanghai"

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRALIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_0

    const-string v2, "Australia/Sydney"

    :goto_0
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v1}, Landroid/text/format/Time;->setToNow()V

    iget-object v3, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_47

    :goto_1
    return-void

    :cond_0
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_1

    const-string v2, "Europe/Vienna"

    goto :goto_0

    :cond_1
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BELGIUM:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_2

    const-string v2, "Europe/Brussels"

    goto :goto_0

    :cond_2
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BULGARIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_3

    const-string v2, "Europe/Sofia"

    goto :goto_0

    :cond_3
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CROATIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_4

    const-string v2, "Europe/Zagreb"

    goto :goto_0

    :cond_4
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CZECH:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_5

    const-string v2, "Europe/Prague"

    goto :goto_0

    :cond_5
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_DENMARK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_6

    const-string v2, "Europe/Copenhagen"

    goto :goto_0

    :cond_6
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FINLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_7

    const-string v2, "Europe/Helsinki"

    goto :goto_0

    :cond_7
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FRANCE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_8

    const-string v2, "Europe/Paris"

    goto :goto_0

    :cond_8
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GERMANY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_9

    const-string v2, "Europe/Berlin"

    goto :goto_0

    :cond_9
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GREECE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_a

    const-string v2, "Europe/Athens"

    goto :goto_0

    :cond_a
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_HUNGARY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_b

    const-string v2, "Europe/Budapest"

    goto :goto_0

    :cond_b
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ITALY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_c

    const-string v2, "Europe/Rome"

    goto :goto_0

    :cond_c
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LUXEMBOURG:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_d

    const-string v2, "Europe/Luxembourg"

    goto :goto_0

    :cond_d
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NETHERLANDS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_e

    const-string v2, "Europe/Amsterdam"

    goto :goto_0

    :cond_e
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NORWAY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_f

    const-string v2, "Europe/Oslo"

    goto :goto_0

    :cond_f
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_POLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_10

    const-string v2, "Europe/Warsaw"

    goto :goto_0

    :cond_10
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PORTUGAL:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_11

    const-string v2, "Europe/Lisbon"

    goto/16 :goto_0

    :cond_11
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUMANIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_12

    const-string v2, "Europe/London"

    goto/16 :goto_0

    :cond_12
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUSSIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_13

    const-string v2, "Asia/Baghdad"

    goto/16 :goto_0

    :cond_13
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SERBIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_14

    const-string v2, "Europe/Belgrade"

    goto/16 :goto_0

    :cond_14
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SLOVENIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_15

    const-string v2, "Europe/Ljubljana"

    goto/16 :goto_0

    :cond_15
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SPAIN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_16

    const-string v2, "Europe/Madrid"

    goto/16 :goto_0

    :cond_16
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SWEDEN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_17

    const-string v2, "Europe/Stockholm"

    goto/16 :goto_0

    :cond_17
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SWITZERLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_18

    const-string v2, "Europe/Zurich"

    goto/16 :goto_0

    :cond_18
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_UK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_19

    const-string v2, "Europe/London"

    goto/16 :goto_0

    :cond_19
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NEWZEALAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_1a

    const-string v2, "Europe/London"

    goto/16 :goto_0

    :cond_1a
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ARAB:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_1b

    const-string v2, "Asia/Dubai"

    goto/16 :goto_0

    :cond_1b
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ESTONIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_1c

    const-string v2, "Europe/Tallinn"

    goto/16 :goto_0

    :cond_1c
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_HEBREW:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_1d

    const-string v2, "Europe/Tallinn"

    goto/16 :goto_0

    :cond_1d
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LATVIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_1e

    const-string v2, "Europe/Riga"

    goto/16 :goto_0

    :cond_1e
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SLOVAKIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_1f

    const-string v2, "Europe/Bratislava"

    goto/16 :goto_0

    :cond_1f
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TURKEY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_20

    const-string v2, "Europe/Istanbul"

    goto/16 :goto_0

    :cond_20
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRELAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_21

    const-string v2, "Europe/Dublin"

    goto/16 :goto_0

    :cond_21
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_JAPAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_22

    const-string v2, "Asia/Tokyo"

    goto/16 :goto_0

    :cond_22
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PHILIPPINES:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_23

    const-string v2, "Asia/Manila"

    goto/16 :goto_0

    :cond_23
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_THAILAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_24

    const-string v2, "Asia/Bangkok"

    goto/16 :goto_0

    :cond_24
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_MALDIVES:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_25

    const-string v2, "Indian/Maldives"

    goto/16 :goto_0

    :cond_25
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_URUGUAY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_26

    const-string v2, "America/Montevideo"

    goto/16 :goto_0

    :cond_26
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PERU:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_27

    const-string v2, "America/Lima"

    goto/16 :goto_0

    :cond_27
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ARGENTINA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_28

    const-string v2, "America/Argentina/Buenos_Aires"

    goto/16 :goto_0

    :cond_28
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CHILE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_29

    const-string v2, "America/Santiago"

    goto/16 :goto_0

    :cond_29
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_VENEZUELA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_2a

    const-string v2, "America/Caracas"

    goto/16 :goto_0

    :cond_2a
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ECUADOR:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_2b

    const-string v2, "America/Guayaquil"

    goto/16 :goto_0

    :cond_2b
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_COSTARICA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_2c

    const-string v2, "America/Guayaquil"

    goto/16 :goto_0

    :cond_2c
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PARAGUAY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_2d

    const-string v2, "America/Asuncion"

    goto/16 :goto_0

    :cond_2d
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BOLIVIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_2e

    const-string v2, "America/La_Paz"

    goto/16 :goto_0

    :cond_2e
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BELIZE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_2f

    const-string v2, "America/Belize"

    goto/16 :goto_0

    :cond_2f
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NICARAGUA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_30

    const-string v2, "America/Managua"

    goto/16 :goto_0

    :cond_30
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GUATEMALA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_31

    const-string v2, "America/Guatemala"

    goto/16 :goto_0

    :cond_31
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CHINA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_32

    const-string v2, "Asia/Shanghai"

    goto/16 :goto_0

    :cond_32
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TAIWAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_33

    const-string v2, "Asia/Taipei"

    goto/16 :goto_0

    :cond_33
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BRAZIL:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_34

    const-string v2, "America/Noronha"

    goto/16 :goto_0

    :cond_34
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CANADA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_35

    const-string v2, "America/St_Johns"

    goto/16 :goto_0

    :cond_35
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_MEXICO:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_36

    const-string v2, "America/Mexico_City"

    goto/16 :goto_0

    :cond_36
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_US:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_37

    const-string v2, "America/New_York"

    goto/16 :goto_0

    :cond_37
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SOUTHKOREA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_38

    const-string v2, "Asia/Tokyo"

    goto/16 :goto_0

    :cond_38
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FIJI:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_39

    const-string v2, "Pacific/Fiji"

    goto/16 :goto_0

    :cond_39
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_UZBEK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_3a

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TAJIKISTAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_3a

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PAKISTAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_3b

    :cond_3a
    const-string v2, "Asia/Karachi"

    goto/16 :goto_0

    :cond_3b
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ETHIOPIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_3c

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SAUDI_ARABIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_3c

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRAQ:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_3c

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_KUWAIT:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_3c

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_QATAR:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_3c

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_YEMEN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_3d

    :cond_3c
    const-string v2, "Asia/Baghdad"

    goto/16 :goto_0

    :cond_3d
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_3e

    const-string v2, "Asia/Tehran"

    goto/16 :goto_0

    :cond_3e
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AZERBAIJAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_3f

    const-string v2, "Asia/Dubai"

    goto/16 :goto_0

    :cond_3f
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SOUTHAFRICA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_40

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_EGYPT:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_40

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NAMIBIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_40

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_JORDAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_40

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ISRAEL:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_40

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ZEMBABWE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_40

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LITHUANIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_40

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LIBYA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_41

    :cond_40
    const-string v2, "Europe/Athens"

    goto/16 :goto_0

    :cond_41
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ALGERIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_42

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NIGERIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-eq p1, v3, :cond_42

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TUNIS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_43

    :cond_42
    const-string v2, "Europe/Amsterdam"

    goto/16 :goto_0

    :cond_43
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_MOROCCO:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_44

    const-string v2, "Europe/London"

    goto/16 :goto_0

    :cond_44
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_INDIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_45

    const-string v2, "Asia/Calcutta"

    goto/16 :goto_0

    :cond_45
    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_INDONESIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    if-ne p1, v3, :cond_46

    const-string v2, "Asia/Bangkok"

    goto/16 :goto_0

    :cond_46
    const-string v2, "Europe/London"

    goto/16 :goto_0

    :cond_47
    const-string v3, "alarm"

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->setTimeZone(Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private updateCountryData()V
    .locals 6

    iget-object v4, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->CountrySelectLayout:Landroid/widget/LinearLayout;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getDataBaseManagerInstance()Lcom/konka/kkinterface/tv/DataBaseDesk;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/DataBaseDesk;->queryCurCountry()I

    move-result v1

    iget-object v4, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->CountrySelectTable:[I

    array-length v3, v4

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v3, :cond_0

    :goto_1
    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0004    # com.konka.tvsettings.R.array.str_arr_autotuning_country

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->tableCountrySelect_index:I

    aget-object v4, v4, v5

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    iget-object v4, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->CountrySelectTable:[I

    aget v4, v4, v2

    if-ne v4, v1, :cond_1

    iput v2, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->tableCountrySelect_index:I

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private updateCurrentLang()V
    .locals 3

    iget-object v1, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->LanguageSelectLayout:Landroid/widget/LinearLayout;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->LanguageSelectLayout:Landroid/widget/LinearLayout;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->language_values:[Ljava/lang/CharSequence;

    iget v2, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->m_iCurrLangIndex:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateLanguageData()V
    .locals 4

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v2, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "#the country is +++"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const-string v2, "US"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    iput v2, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->m_iCurrLangIndex:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v2, "RU"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x4

    iput v2, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->m_iCurrLangIndex:I

    goto :goto_0

    :cond_2
    const-string v2, "FR"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v2, 0x5

    iput v2, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->m_iCurrLangIndex:I

    goto :goto_0

    :cond_3
    const-string v2, "EG"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    iput v2, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->m_iCurrLangIndex:I

    goto :goto_0

    :cond_4
    const-string v2, "IL"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/16 v2, 0x8

    iput v2, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->m_iCurrLangIndex:I

    goto :goto_0

    :cond_5
    const-string v2, "ID"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x6

    iput v2, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->m_iCurrLangIndex:I

    goto :goto_0

    :cond_6
    const-string v2, "IR"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    const/4 v2, 0x2

    iput v2, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->m_iCurrLangIndex:I

    goto :goto_0

    :cond_7
    const-string v2, "KD"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    const/4 v2, 0x3

    iput v2, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->m_iCurrLangIndex:I

    goto :goto_0

    :cond_8
    const-string v2, "TR"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    const/4 v2, 0x7

    iput v2, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->m_iCurrLangIndex:I

    goto :goto_0

    :cond_9
    const-string v2, "CN"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v2, 0x9

    iput v2, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->m_iCurrLangIndex:I

    goto :goto_0

    :cond_a
    const-string v2, "TH"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0xa

    iput v2, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->m_iCurrLangIndex:I

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f03001c    # com.konka.tvsettings.R.layout.installation_guide_menu

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/TVRootApp;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v0, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    iget-object v0, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->getCustomerInfo()Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;->strCustomerName:Ljava/lang/String;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->curCustomer:Ljava/lang/String;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0029    # com.konka.tvsettings.R.array.str_arr_osdlanguage_vals

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->language_values:[Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getDataBaseManagerInstance()Lcom/konka/kkinterface/tv/DataBaseDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk;->queryCurCountry()I

    move-result v0

    iput v0, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->miCurCountry:I

    iget-object v0, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableDVBC()Z

    move-result v0

    iput-boolean v0, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->DvbcEnable:Z

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->addView()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    sparse-switch p1, :sswitch_data_0

    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_1
    return v0

    :sswitch_0
    const/4 v0, 0x1

    goto :goto_1

    :sswitch_1
    iget-object v0, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/SettingDesk;->SetInstallationFlag(Z)Z

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0xb2 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    const v0, 0x7f040009    # com.konka.tvsettings.R.anim.anim_zoom_in

    const v1, 0x7f040008    # com.konka.tvsettings.R.anim.anim_right_out

    invoke-virtual {p0, v0, v1}, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->overridePendingTransition(II)V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onUserInteraction()V

    return-void
.end method
