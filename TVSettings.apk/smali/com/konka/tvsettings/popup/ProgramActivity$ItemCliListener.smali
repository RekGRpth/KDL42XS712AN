.class Lcom/konka/tvsettings/popup/ProgramActivity$ItemCliListener;
.super Ljava/lang/Object;
.source "ProgramActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/popup/ProgramActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ItemCliListener"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/ProgramActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/popup/ProgramActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/ProgramActivity$ItemCliListener;->this$0:Lcom/konka/tvsettings/popup/ProgramActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "click: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ProgramActivity$ItemCliListener;->this$0:Lcom/konka/tvsettings/popup/ProgramActivity;

    invoke-static {v2, p3}, Lcom/konka/tvsettings/popup/ProgramActivity;->access$0(Lcom/konka/tvsettings/popup/ProgramActivity;I)V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ProgramActivity$ItemCliListener;->this$0:Lcom/konka/tvsettings/popup/ProgramActivity;

    # getter for: Lcom/konka/tvsettings/popup/ProgramActivity;->adapter:Lcom/konka/tvsettings/popup/ProgramActivity$ProgramListAdapter;
    invoke-static {v2}, Lcom/konka/tvsettings/popup/ProgramActivity;->access$1(Lcom/konka/tvsettings/popup/ProgramActivity;)Lcom/konka/tvsettings/popup/ProgramActivity$ProgramListAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/tvsettings/popup/ProgramActivity$ProgramListAdapter;->notifyDataSetChanged()V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ProgramActivity$ItemCliListener;->this$0:Lcom/konka/tvsettings/popup/ProgramActivity;

    # getter for: Lcom/konka/tvsettings/popup/ProgramActivity;->mViewLvProgList:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/konka/tvsettings/popup/ProgramActivity;->access$2(Lcom/konka/tvsettings/popup/ProgramActivity;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->clearFocus()V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ProgramActivity$ItemCliListener;->this$0:Lcom/konka/tvsettings/popup/ProgramActivity;

    # getter for: Lcom/konka/tvsettings/popup/ProgramActivity;->mViewLvProgList:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/konka/tvsettings/popup/ProgramActivity;->access$2(Lcom/konka/tvsettings/popup/ProgramActivity;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->requestFocus()Z

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ProgramActivity$ItemCliListener;->this$0:Lcom/konka/tvsettings/popup/ProgramActivity;

    # getter for: Lcom/konka/tvsettings/popup/ProgramActivity;->mViewLvProgList:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/konka/tvsettings/popup/ProgramActivity;->access$2(Lcom/konka/tvsettings/popup/ProgramActivity;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2, p3}, Landroid/widget/ListView;->setSelection(I)V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ProgramActivity$ItemCliListener;->this$0:Lcom/konka/tvsettings/popup/ProgramActivity;

    iget-object v2, v2, Lcom/konka/tvsettings/popup/ProgramActivity;->mlistProgList:Ljava/util/ArrayList;

    invoke-virtual {v2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ProgramActivity$ItemCliListener;->this$0:Lcom/konka/tvsettings/popup/ProgramActivity;

    # getter for: Lcom/konka/tvsettings/popup/ProgramActivity;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;
    invoke-static {v2}, Lcom/konka/tvsettings/popup/ProgramActivity;->access$3(Lcom/konka/tvsettings/popup/ProgramActivity;)Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v2

    iget v3, v1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    invoke-static {}, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->values()[Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    move-result-object v4

    iget-short v5, v1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    aget-object v4, v4, v5

    invoke-interface {v2, v3, v4}, Lcom/konka/kkinterface/tv/ChannelDesk;->programSel(ILcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;)Z

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ProgramActivity$ItemCliListener;->this$0:Lcom/konka/tvsettings/popup/ProgramActivity;

    invoke-virtual {v2}, Lcom/konka/tvsettings/popup/ProgramActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/konka/tvsettings/popup/SourceInfoActivity;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ProgramActivity$ItemCliListener;->this$0:Lcom/konka/tvsettings/popup/ProgramActivity;

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ProgramActivity$ItemCliListener;->this$0:Lcom/konka/tvsettings/popup/ProgramActivity;

    # getter for: Lcom/konka/tvsettings/popup/ProgramActivity;->myHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/ProgramActivity;->access$4(Lcom/konka/tvsettings/popup/ProgramActivity;)Landroid/os/Handler;

    move-result-object v3

    const/16 v4, 0x1f4

    invoke-static {v2, v3, v0, v4}, Lcom/konka/tvsettings/SwitchMenuHelper;->delay2ShowMenu(Landroid/app/Activity;Landroid/os/Handler;Landroid/content/Intent;I)V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ProgramActivity$ItemCliListener;->this$0:Lcom/konka/tvsettings/popup/ProgramActivity;

    invoke-virtual {v2}, Lcom/konka/tvsettings/popup/ProgramActivity;->finish()V

    return-void
.end method
