.class Lcom/konka/tvsettings/popup/ProgramActivity$ItemSelListener;
.super Ljava/lang/Object;
.source "ProgramActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/popup/ProgramActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ItemSelListener"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemSelectedListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/ProgramActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/popup/ProgramActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/ProgramActivity$ItemSelListener;->this$0:Lcom/konka/tvsettings/popup/ProgramActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity$ItemSelListener;->this$0:Lcom/konka/tvsettings/popup/ProgramActivity;

    invoke-static {v0, p3}, Lcom/konka/tvsettings/popup/ProgramActivity;->access$0(Lcom/konka/tvsettings/popup/ProgramActivity;I)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity$ItemSelListener;->this$0:Lcom/konka/tvsettings/popup/ProgramActivity;

    # getter for: Lcom/konka/tvsettings/popup/ProgramActivity;->adapter:Lcom/konka/tvsettings/popup/ProgramActivity$ProgramListAdapter;
    invoke-static {v0}, Lcom/konka/tvsettings/popup/ProgramActivity;->access$1(Lcom/konka/tvsettings/popup/ProgramActivity;)Lcom/konka/tvsettings/popup/ProgramActivity$ProgramListAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/tvsettings/popup/ProgramActivity$ProgramListAdapter;->notifyDataSetChanged()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    move-result-object v2

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ProgramActivity$ItemSelListener;->this$0:Lcom/konka/tvsettings/popup/ProgramActivity;

    iget-object v0, v0, Lcom/konka/tvsettings/popup/ProgramActivity;->mlistProgList:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ProgramActivity$ItemSelListener;->this$0:Lcom/konka/tvsettings/popup/ProgramActivity;

    # getter for: Lcom/konka/tvsettings/popup/ProgramActivity;->miCurrPos:I
    invoke-static {v3}, Lcom/konka/tvsettings/popup/ProgramActivity;->access$5(Lcom/konka/tvsettings/popup/ProgramActivity;)I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    iget-short v0, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    aget-object v0, v2, v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    return-void
.end method
