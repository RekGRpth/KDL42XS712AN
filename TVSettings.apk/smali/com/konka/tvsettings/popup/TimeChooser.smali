.class public Lcom/konka/tvsettings/popup/TimeChooser;
.super Landroid/widget/EditText;
.source "TimeChooser.java"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field private final TAG:Ljava/lang/String;

.field private currentType:I

.field private final hour:I

.field private isEditBegin:Z

.field private maxHourTime:I

.field private final minute:I

.field private final second:I

.field private timeChooseHour:I

.field private timeChooseMinute:I

.field private timeChooseSecond:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    const-string v0, "TimeChooser"

    iput-object v0, p0, Lcom/konka/tvsettings/popup/TimeChooser;->TAG:Ljava/lang/String;

    iput v1, p0, Lcom/konka/tvsettings/popup/TimeChooser;->hour:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/konka/tvsettings/popup/TimeChooser;->minute:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/konka/tvsettings/popup/TimeChooser;->second:I

    iput-boolean v1, p0, Lcom/konka/tvsettings/popup/TimeChooser;->isEditBegin:Z

    const/16 v0, 0x32

    iput v0, p0, Lcom/konka/tvsettings/popup/TimeChooser;->maxHourTime:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/konka/tvsettings/popup/TimeChooser;->currentType:I

    iput v1, p0, Lcom/konka/tvsettings/popup/TimeChooser;->timeChooseHour:I

    iput v1, p0, Lcom/konka/tvsettings/popup/TimeChooser;->timeChooseMinute:I

    iput v1, p0, Lcom/konka/tvsettings/popup/TimeChooser;->timeChooseSecond:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/16 v4, 0x32

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const-string v1, "TimeChooser"

    iput-object v1, p0, Lcom/konka/tvsettings/popup/TimeChooser;->TAG:Ljava/lang/String;

    iput v2, p0, Lcom/konka/tvsettings/popup/TimeChooser;->hour:I

    iput v3, p0, Lcom/konka/tvsettings/popup/TimeChooser;->minute:I

    const/4 v1, 0x2

    iput v1, p0, Lcom/konka/tvsettings/popup/TimeChooser;->second:I

    iput-boolean v2, p0, Lcom/konka/tvsettings/popup/TimeChooser;->isEditBegin:Z

    iput v4, p0, Lcom/konka/tvsettings/popup/TimeChooser;->maxHourTime:I

    const/4 v1, -0x1

    iput v1, p0, Lcom/konka/tvsettings/popup/TimeChooser;->currentType:I

    iput v2, p0, Lcom/konka/tvsettings/popup/TimeChooser;->timeChooseHour:I

    iput v2, p0, Lcom/konka/tvsettings/popup/TimeChooser;->timeChooseMinute:I

    iput v2, p0, Lcom/konka/tvsettings/popup/TimeChooser;->timeChooseSecond:I

    sget-object v1, Lcom/konka/tvsettings/R$styleable;->TimeChooser:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/konka/tvsettings/popup/TimeChooser;->currentType:I

    iget v1, p0, Lcom/konka/tvsettings/popup/TimeChooser;->currentType:I

    if-nez v1, :cond_0

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/konka/tvsettings/popup/TimeChooser;->maxHourTime:I

    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p0, p0}, Lcom/konka/tvsettings/popup/TimeChooser;->addTextChangedListener(Landroid/text/TextWatcher;)V

    new-instance v1, Lcom/konka/tvsettings/popup/TimeChooser$1;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/popup/TimeChooser$1;-><init>(Lcom/konka/tvsettings/popup/TimeChooser;)V

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/popup/TimeChooser;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const-string v0, "TimeChooser"

    iput-object v0, p0, Lcom/konka/tvsettings/popup/TimeChooser;->TAG:Ljava/lang/String;

    iput v1, p0, Lcom/konka/tvsettings/popup/TimeChooser;->hour:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/konka/tvsettings/popup/TimeChooser;->minute:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/konka/tvsettings/popup/TimeChooser;->second:I

    iput-boolean v1, p0, Lcom/konka/tvsettings/popup/TimeChooser;->isEditBegin:Z

    const/16 v0, 0x32

    iput v0, p0, Lcom/konka/tvsettings/popup/TimeChooser;->maxHourTime:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/konka/tvsettings/popup/TimeChooser;->currentType:I

    iput v1, p0, Lcom/konka/tvsettings/popup/TimeChooser;->timeChooseHour:I

    iput v1, p0, Lcom/konka/tvsettings/popup/TimeChooser;->timeChooseMinute:I

    iput v1, p0, Lcom/konka/tvsettings/popup/TimeChooser;->timeChooseSecond:I

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/popup/TimeChooser;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/tvsettings/popup/TimeChooser;->isEditBegin:Z

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 6
    .param p1    # Landroid/text/Editable;

    const/16 v4, 0x3c

    const/4 v5, 0x0

    :try_start_0
    invoke-interface {p1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iget v3, p0, Lcom/konka/tvsettings/popup/TimeChooser;->currentType:I

    packed-switch v3, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget v3, p0, Lcom/konka/tvsettings/popup/TimeChooser;->maxHourTime:I

    if-lt v2, v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    iget v4, p0, Lcom/konka/tvsettings/popup/TimeChooser;->maxHourTime:I

    add-int/lit8 v4, v4, -0x1

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/TimeChooser;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/popup/TimeChooser;->setSelection(I)V

    iget v3, p0, Lcom/konka/tvsettings/popup/TimeChooser;->maxHourTime:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/tvsettings/popup/TimeChooser;->timeChooseHour:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    iget v3, p0, Lcom/konka/tvsettings/popup/TimeChooser;->currentType:I

    packed-switch v3, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v5, p0, Lcom/konka/tvsettings/popup/TimeChooser;->timeChooseHour:I

    goto :goto_0

    :cond_0
    :try_start_1
    iput v2, p0, Lcom/konka/tvsettings/popup/TimeChooser;->timeChooseHour:I

    goto :goto_0

    :pswitch_2
    if-lt v2, v4, :cond_1

    const-string v3, "59"

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/popup/TimeChooser;->setText(Ljava/lang/CharSequence;)V

    const/4 v3, 0x2

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/popup/TimeChooser;->setSelection(I)V

    const/16 v3, 0x3b

    iput v3, p0, Lcom/konka/tvsettings/popup/TimeChooser;->timeChooseMinute:I

    goto :goto_0

    :cond_1
    iput v2, p0, Lcom/konka/tvsettings/popup/TimeChooser;->timeChooseMinute:I

    goto :goto_0

    :pswitch_3
    if-lt v2, v4, :cond_2

    const-string v3, "59"

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/popup/TimeChooser;->setText(Ljava/lang/CharSequence;)V

    const/4 v3, 0x2

    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/popup/TimeChooser;->setSelection(I)V

    const/16 v3, 0x3b

    iput v3, p0, Lcom/konka/tvsettings/popup/TimeChooser;->timeChooseSecond:I

    goto :goto_0

    :cond_2
    iput v2, p0, Lcom/konka/tvsettings/popup/TimeChooser;->timeChooseSecond:I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :pswitch_4
    iput v5, p0, Lcom/konka/tvsettings/popup/TimeChooser;->timeChooseMinute:I

    goto :goto_0

    :pswitch_5
    iput v5, p0, Lcom/konka/tvsettings/popup/TimeChooser;->timeChooseSecond:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # Landroid/view/KeyEvent;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/TimeChooser;->editDone()V

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/EditText;->dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public editDone()V
    .locals 4

    iget-boolean v2, p0, Lcom/konka/tvsettings/popup/TimeChooser;->isEditBegin:Z

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/konka/tvsettings/popup/TimeChooser;->isEditBegin:Z

    :try_start_0
    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/TimeChooser;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    const/16 v2, 0xa

    if-ge v0, v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "0"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/popup/TimeChooser;->setText(Ljava/lang/CharSequence;)V

    const/4 v2, 0x2

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/popup/TimeChooser;->setSelection(I)V

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_0

    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/popup/TimeChooser;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/popup/TimeChooser;->setSelection(I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0
.end method

.method public getValue()I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/popup/TimeChooser;->currentType:I

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    iget v0, p0, Lcom/konka/tvsettings/popup/TimeChooser;->timeChooseHour:I

    goto :goto_0

    :pswitch_1
    iget v0, p0, Lcom/konka/tvsettings/popup/TimeChooser;->timeChooseMinute:I

    goto :goto_0

    :pswitch_2
    iget v0, p0, Lcom/konka/tvsettings/popup/TimeChooser;->timeChooseSecond:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method
