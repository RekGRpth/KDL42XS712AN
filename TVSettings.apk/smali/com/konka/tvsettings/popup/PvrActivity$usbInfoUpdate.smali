.class Lcom/konka/tvsettings/popup/PvrActivity$usbInfoUpdate;
.super Ljava/lang/Thread;
.source "PvrActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/popup/PvrActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "usbInfoUpdate"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/PvrActivity;


# direct methods
.method private constructor <init>(Lcom/konka/tvsettings/popup/PvrActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/PvrActivity$usbInfoUpdate;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/tvsettings/popup/PvrActivity;Lcom/konka/tvsettings/popup/PvrActivity$usbInfoUpdate;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/popup/PvrActivity$usbInfoUpdate;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;)V

    return-void
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/popup/PvrActivity$usbInfoUpdate;)Lcom/konka/tvsettings/popup/PvrActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity$usbInfoUpdate;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 3

    invoke-super {p0}, Ljava/lang/Thread;->run()V

    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$usbInfoUpdate;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$3(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->isPlaybacking()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$usbInfoUpdate;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$3(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->isRecording()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$usbInfoUpdate;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$usbInfoUpdate;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$5(Lcom/konka/tvsettings/popup/PvrActivity;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/konka/tvsettings/popup/PvrActivity$usbInfoUpdate$1;

    invoke-direct {v2, p0}, Lcom/konka/tvsettings/popup/PvrActivity$usbInfoUpdate$1;-><init>(Lcom/konka/tvsettings/popup/PvrActivity$usbInfoUpdate;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const-wide/16 v1, 0x1388

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1
.end method
