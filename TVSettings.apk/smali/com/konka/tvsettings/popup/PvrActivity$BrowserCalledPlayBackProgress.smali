.class Lcom/konka/tvsettings/popup/PvrActivity$BrowserCalledPlayBackProgress;
.super Ljava/lang/Thread;
.source "PvrActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/popup/PvrActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BrowserCalledPlayBackProgress"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/PvrActivity;


# direct methods
.method private constructor <init>(Lcom/konka/tvsettings/popup/PvrActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/PvrActivity$BrowserCalledPlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/tvsettings/popup/PvrActivity;Lcom/konka/tvsettings/popup/PvrActivity$BrowserCalledPlayBackProgress;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/popup/PvrActivity$BrowserCalledPlayBackProgress;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;)V

    return-void
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/popup/PvrActivity$BrowserCalledPlayBackProgress;)Lcom/konka/tvsettings/popup/PvrActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/PvrActivity$BrowserCalledPlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 6

    invoke-super {p0}, Ljava/lang/Thread;->run()V

    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity$BrowserCalledPlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PvrActivity;->access$3(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/PvrDesk;->isPlaybacking()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity$BrowserCalledPlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PvrActivity;->access$3(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/PvrDesk;->isRecording()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity$BrowserCalledPlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    invoke-virtual {v3}, Lcom/konka/tvsettings/popup/PvrActivity;->isFinishing()Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity$BrowserCalledPlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PvrActivity;->access$3(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/PvrDesk;->getCurPlaybackTimeInSecond()I

    move-result v0

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity$BrowserCalledPlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PvrActivity;->access$3(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/PvrDesk;->isRecording()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity$BrowserCalledPlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PvrActivity;->access$3(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/PvrDesk;->isTimeShiftRecording()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity$BrowserCalledPlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PvrActivity;->access$3(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/tvsettings/popup/PvrActivity$BrowserCalledPlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v4}, Lcom/konka/tvsettings/popup/PvrActivity;->access$3(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/PvrDesk;->getCurPlaybackingFileName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/konka/kkinterface/tv/PvrDesk;->getRecordedFileDurationTime(Ljava/lang/String;)I

    move-result v2

    :goto_2
    const-string v3, "PVR"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "==========>>> current time = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "PVR"

    const-string v4, "ddddddddddddddddddddddddddddddddd \n"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity$BrowserCalledPlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->handler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PvrActivity;->access$5(Lcom/konka/tvsettings/popup/PvrActivity;)Landroid/os/Handler;

    move-result-object v3

    new-instance v4, Lcom/konka/tvsettings/popup/PvrActivity$BrowserCalledPlayBackProgress$1;

    invoke-direct {v4, p0, v0, v2}, Lcom/konka/tvsettings/popup/PvrActivity$BrowserCalledPlayBackProgress$1;-><init>(Lcom/konka/tvsettings/popup/PvrActivity$BrowserCalledPlayBackProgress;II)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const-wide/16 v3, 0x3e8

    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :cond_3
    :try_start_1
    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity$BrowserCalledPlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PvrActivity;->access$3(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/PvrDesk;->getCurRecordTimeInSecond()I

    move-result v2

    goto :goto_2

    :cond_4
    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity$BrowserCalledPlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PvrActivity;->access$3(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/tvsettings/popup/PvrActivity$BrowserCalledPlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v4}, Lcom/konka/tvsettings/popup/PvrActivity;->access$3(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/PvrDesk;->getCurPlaybackingFileName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/konka/kkinterface/tv/PvrDesk;->getRecordedFileDurationTime(Ljava/lang/String;)I
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v2

    goto :goto_2

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_1
.end method
