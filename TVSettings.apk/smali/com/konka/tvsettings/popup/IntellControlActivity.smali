.class public Lcom/konka/tvsettings/popup/IntellControlActivity;
.super Landroid/app/Activity;
.source "IntellControlActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/popup/IntellControlActivity$DeepImg;,
        Lcom/konka/tvsettings/popup/IntellControlActivity$LowImg;,
        Lcom/konka/tvsettings/popup/IntellControlActivity$signalChangeReceiver;
    }
.end annotation


# static fields
.field private static final DELAY_TIME:I = 0x3e8

.field private static final REFRESH_UI:I = 0x7b


# instance fields
.field private HEIGHT:I

.field private LEFT_MARGIN:I

.field private MAX_BACK_LIGHT:I

.field private WIDTH:I

.field private mViewLlBri:Landroid/widget/LinearLayout;

.field private mViewLlCon:Landroid/widget/LinearLayout;

.field private mViewLlEco:Landroid/widget/LinearLayout;

.field private mViewTvBriProg:Landroid/widget/TextView;

.field private mViewTvConProg:Landroid/widget/TextView;

.field private mViewTvEcoProg:Landroid/widget/TextView;

.field private miBriProg:I

.field private miConProg:I

.field private miEcoProg:I

.field private myHandler:Landroid/os/Handler;

.field private pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

.field private receiver:Lcom/konka/tvsettings/popup/IntellControlActivity$signalChangeReceiver;

.field private rootApp:Lcom/konka/tvsettings/TVRootApp;

.field private rootmenuHandler:Lcom/konka/tvsettings/RootActivity$RootMenuHandler;

.field private serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

.field private timer:Ljava/util/Timer;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->rootApp:Lcom/konka/tvsettings/TVRootApp;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->rootmenuHandler:Lcom/konka/tvsettings/RootActivity$RootMenuHandler;

    new-instance v0, Lcom/konka/tvsettings/popup/IntellControlActivity$signalChangeReceiver;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/popup/IntellControlActivity$signalChangeReceiver;-><init>(Lcom/konka/tvsettings/popup/IntellControlActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->receiver:Lcom/konka/tvsettings/popup/IntellControlActivity$signalChangeReceiver;

    iput v1, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->WIDTH:I

    iput v1, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->HEIGHT:I

    iput v1, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->LEFT_MARGIN:I

    iput v1, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->MAX_BACK_LIGHT:I

    iput v1, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->miEcoProg:I

    iput v1, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->miBriProg:I

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->timer:Ljava/util/Timer;

    new-instance v0, Lcom/konka/tvsettings/popup/IntellControlActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/popup/IntellControlActivity$1;-><init>(Lcom/konka/tvsettings/popup/IntellControlActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->myHandler:Landroid/os/Handler;

    return-void
.end method

.method private ChannelTimerTask()Ljava/util/TimerTask;
    .locals 1

    new-instance v0, Lcom/konka/tvsettings/popup/IntellControlActivity$2;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/popup/IntellControlActivity$2;-><init>(Lcom/konka/tvsettings/popup/IntellControlActivity;)V

    return-object v0
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/popup/IntellControlActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->miEcoProg:I

    return v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/popup/IntellControlActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->miBriProg:I

    return v0
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/popup/IntellControlActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->WIDTH:I

    return v0
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/popup/IntellControlActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->HEIGHT:I

    return v0
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/popup/IntellControlActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->LEFT_MARGIN:I

    return v0
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/popup/IntellControlActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->miEcoProg:I

    return-void
.end method

.method static synthetic access$6(Lcom/konka/tvsettings/popup/IntellControlActivity;)Lcom/konka/kkinterface/tv/PictureDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    return-object v0
.end method

.method static synthetic access$7(Lcom/konka/tvsettings/popup/IntellControlActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->miBriProg:I

    return-void
.end method

.method static synthetic access$8(Lcom/konka/tvsettings/popup/IntellControlActivity;I)V
    .locals 0

    iput p1, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->miConProg:I

    return-void
.end method

.method static synthetic access$9(Lcom/konka/tvsettings/popup/IntellControlActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->myHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public addChild(Landroid/widget/LinearLayout;II)V
    .locals 2
    .param p1    # Landroid/widget/LinearLayout;
    .param p2    # I
    .param p3    # I

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->removeAllViews()V

    const/4 v0, 0x0

    :goto_0
    if-lt v0, p2, :cond_0

    const/4 v0, 0x0

    :goto_1
    if-lt v0, p3, :cond_1

    return-void

    :cond_0
    new-instance v1, Lcom/konka/tvsettings/popup/IntellControlActivity$DeepImg;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/popup/IntellControlActivity$DeepImg;-><init>(Lcom/konka/tvsettings/popup/IntellControlActivity;)V

    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-instance v1, Lcom/konka/tvsettings/popup/IntellControlActivity$LowImg;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/popup/IntellControlActivity$LowImg;-><init>(Lcom/konka/tvsettings/popup/IntellControlActivity;)V

    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f03002e    # com.konka.tvsettings.R.layout.popup_intell_control_menu

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/IntellControlActivity;->setContentView(I)V

    const v0, 0x7f070121    # com.konka.tvsettings.R.id.popup_intell_control_menu_ll_economization

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/IntellControlActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->mViewLlEco:Landroid/widget/LinearLayout;

    const v0, 0x7f070120    # com.konka.tvsettings.R.id.popup_intell_control_menu_tv_economization

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/IntellControlActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->mViewTvEcoProg:Landroid/widget/TextView;

    const v0, 0x7f070123    # com.konka.tvsettings.R.id.popup_intell_control_menu_ll_bright

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/IntellControlActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->mViewLlBri:Landroid/widget/LinearLayout;

    const v0, 0x7f070122    # com.konka.tvsettings.R.id.popup_intell_control_menu_tv_bright

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/IntellControlActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->mViewTvBriProg:Landroid/widget/TextView;

    const v0, 0x7f070125    # com.konka.tvsettings.R.id.popup_intell_control_menu_ll_contrast

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/IntellControlActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->mViewLlCon:Landroid/widget/LinearLayout;

    const v0, 0x7f070124    # com.konka.tvsettings.R.id.popup_intell_control_menu_tv_contrast

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/IntellControlActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->mViewTvConProg:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/IntellControlActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/TVRootApp;

    iput-object v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->rootApp:Lcom/konka/tvsettings/TVRootApp;

    iget-object v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->rootApp:Lcom/konka/tvsettings/TVRootApp;

    invoke-virtual {v0}, Lcom/konka/tvsettings/TVRootApp;->getRootmenuHandler()Lcom/konka/tvsettings/RootActivity$RootMenuHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->rootmenuHandler:Lcom/konka/tvsettings/RootActivity$RootMenuHandler;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/IntellControlActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09001d    # com.konka.tvsettings.R.dimen.TVSettingIntellControl_Width

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->WIDTH:I

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/IntellControlActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09001e    # com.konka.tvsettings.R.dimen.TVSettingIntellControl_Height

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->HEIGHT:I

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/IntellControlActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f09001f    # com.konka.tvsettings.R.dimen.TVSettingIntellControl_LeftMargin

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->LEFT_MARGIN:I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PictureManager;->getBacklightMaxValue()I

    move-result v0

    iput v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->MAX_BACK_LIGHT:I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/PictureManager;->getBacklight()I

    move-result v0

    iput v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->miEcoProg:I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/PictureDesk;->getDlcAverageLuma()S

    move-result v0

    iput v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->miBriProg:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->miConProg:I

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "==================MAX_BACK_LIGHT: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->MAX_BACK_LIGHT:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "==================Economization: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->miEcoProg:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "==================Bright: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->miBriProg:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "contrast: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->miConProg:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->miEcoProg:I

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/IntellControlActivity;->showeEonomization(I)V

    iget v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->miBriProg:I

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/IntellControlActivity;->showBrightness(I)V

    iget v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->miEcoProg:I

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/IntellControlActivity;->showContrast(I)V

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->timer:Ljava/util/Timer;

    iget-object v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->timer:Ljava/util/Timer;

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/IntellControlActivity;->ChannelTimerTask()Ljava/util/TimerTask;

    move-result-object v1

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x3e8

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    new-instance v7, Landroid/content/IntentFilter;

    const-string v0, "com.konka.tv.action.SIGNAL_UNLOCK"

    invoke-direct {v7, v0}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->receiver:Lcom/konka/tvsettings/popup/IntellControlActivity$signalChangeReceiver;

    invoke-virtual {p0, v0, v7}, Lcom/konka/tvsettings/popup/IntellControlActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void

    :catch_0
    move-exception v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->receiver:Lcom/konka/tvsettings/popup/IntellControlActivity$signalChangeReceiver;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/IntellControlActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v1, 0x4

    if-ne p1, v1, :cond_0

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/IntellControlActivity;->finish()V

    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    const/16 v1, 0x1000

    iput v1, v0, Landroid/os/Message;->what:I

    iput p1, v0, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->rootmenuHandler:Lcom/konka/tvsettings/RootActivity$RootMenuHandler;

    invoke-virtual {v1, v0}, Lcom/konka/tvsettings/RootActivity$RootMenuHandler;->sendMessage(Landroid/os/Message;)Z

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public showBrightness(I)V
    .locals 6
    .param p1    # I

    const/4 v0, 0x0

    const/4 v1, 0x0

    mul-int/lit8 v3, p1, 0x64

    div-int/lit16 v3, v3, 0xff

    int-to-float v2, v3

    const/high16 v3, 0x42c80000    # 100.0f

    cmpl-float v3, v2, v3

    if-lez v3, :cond_0

    const/high16 v2, 0x42c80000    # 100.0f

    :cond_0
    const/4 v3, 0x0

    cmpg-float v3, v2, v3

    if-gtz v3, :cond_1

    const/4 v0, 0x0

    const/16 v1, 0x19

    :goto_0
    iget-object v3, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->mViewLlBri:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v3, v0, v1}, Lcom/konka/tvsettings/popup/IntellControlActivity;->addChild(Landroid/widget/LinearLayout;II)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->mViewTvBriProg:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_1
    const/high16 v3, 0x40800000    # 4.0f

    div-float v3, v2, v3

    float-to-int v0, v3

    rsub-int/lit8 v1, v0, 0x19

    goto :goto_0
.end method

.method public showContrast(I)V
    .locals 6
    .param p1    # I

    const/4 v0, 0x0

    const/4 v1, 0x0

    mul-int/lit8 v3, p1, 0x64

    iget v4, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->MAX_BACK_LIGHT:I

    div-int/2addr v3, v4

    int-to-float v2, v3

    const/high16 v3, 0x42c80000    # 100.0f

    cmpl-float v3, v2, v3

    if-lez v3, :cond_0

    const/high16 v2, 0x42c80000    # 100.0f

    :cond_0
    const/4 v3, 0x0

    cmpg-float v3, v2, v3

    if-gtz v3, :cond_1

    const/4 v0, 0x0

    const/16 v1, 0x19

    :goto_0
    iget-object v3, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->mViewLlCon:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v3, v0, v1}, Lcom/konka/tvsettings/popup/IntellControlActivity;->addChild(Landroid/widget/LinearLayout;II)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->mViewTvConProg:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_1
    const/high16 v3, 0x40800000    # 4.0f

    div-float v3, v2, v3

    float-to-int v0, v3

    rsub-int/lit8 v1, v0, 0x19

    goto :goto_0
.end method

.method public showeEonomization(I)V
    .locals 6
    .param p1    # I

    const/4 v0, 0x0

    const/4 v1, 0x0

    mul-int/lit8 v3, p1, 0x64

    iget v4, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->MAX_BACK_LIGHT:I

    div-int/2addr v3, v4

    int-to-float v2, v3

    const/high16 v3, 0x42c80000    # 100.0f

    cmpl-float v3, v2, v3

    if-lez v3, :cond_0

    const/high16 v2, 0x42c80000    # 100.0f

    :cond_0
    const/4 v3, 0x0

    cmpg-float v3, v2, v3

    if-gtz v3, :cond_1

    const/4 v0, 0x0

    const/16 v1, 0x19

    :goto_0
    iget-object v3, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->mViewLlEco:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v3, v0, v1}, Lcom/konka/tvsettings/popup/IntellControlActivity;->addChild(Landroid/widget/LinearLayout;II)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/IntellControlActivity;->mViewTvEcoProg:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "%"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_1
    const/high16 v3, 0x40800000    # 4.0f

    div-float v3, v2, v3

    float-to-int v0, v3

    rsub-int/lit8 v1, v0, 0x19

    goto :goto_0
.end method
