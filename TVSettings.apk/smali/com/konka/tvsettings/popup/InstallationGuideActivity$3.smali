.class Lcom/konka/tvsettings/popup/InstallationGuideActivity$3;
.super Lcom/konka/tvsettings/picture/PictureSettingItem1;
.source "InstallationGuideActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/popup/InstallationGuideActivity;->addItemAntennaType()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/InstallationGuideActivity;

.field private final synthetic val$deskImpl:Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/popup/InstallationGuideActivity;Landroid/app/Activity;IIILcom/konka/kkimplements/tv/mstar/SettingDeskImpl;)V
    .locals 0
    .param p2    # Landroid/app/Activity;
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iput-object p1, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity$3;->this$0:Lcom/konka/tvsettings/popup/InstallationGuideActivity;

    iput-object p6, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity$3;->val$deskImpl:Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/konka/tvsettings/picture/PictureSettingItem1;-><init>(Landroid/app/Activity;III)V

    return-void
.end method


# virtual methods
.method public doUpdate()V
    .locals 3

    iget-object v1, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity$3;->this$0:Lcom/konka/tvsettings/popup/InstallationGuideActivity;

    # getter for: Lcom/konka/tvsettings/popup/InstallationGuideActivity;->itemAntennaType:Lcom/konka/tvsettings/picture/PictureSettingItem1;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->access$0(Lcom/konka/tvsettings/popup/InstallationGuideActivity;)Lcom/konka/tvsettings/picture/PictureSettingItem1;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/tvsettings/picture/PictureSettingItem1;->getCurrentState()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity$3;->this$0:Lcom/konka/tvsettings/popup/InstallationGuideActivity;

    # getter for: Lcom/konka/tvsettings/popup/InstallationGuideActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->access$1(Lcom/konka/tvsettings/popup/InstallationGuideActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;->E_ROUTE_DVBT:Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvSetAntennaType(Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;)V

    :goto_0
    iget-object v1, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity$3;->val$deskImpl:Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;

    invoke-virtual {v1, v0}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->setDvbMode(I)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/tvsettings/popup/InstallationGuideActivity$3;->this$0:Lcom/konka/tvsettings/popup/InstallationGuideActivity;

    # getter for: Lcom/konka/tvsettings/popup/InstallationGuideActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/InstallationGuideActivity;->access$1(Lcom/konka/tvsettings/popup/InstallationGuideActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;->E_ROUTE_DVBC:Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->dtvSetAntennaType(Lcom/konka/kkinterface/tv/DtvInterface$EN_ANTENNA_TYPE;)V

    goto :goto_0
.end method
