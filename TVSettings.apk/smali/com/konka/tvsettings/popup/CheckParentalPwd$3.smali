.class Lcom/konka/tvsettings/popup/CheckParentalPwd$3;
.super Ljava/lang/Object;
.source "CheckParentalPwd.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/popup/CheckParentalPwd;->setMyEditListener(Landroid/widget/EditText;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/CheckParentalPwd;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/popup/CheckParentalPwd;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd$3;->this$0:Lcom/konka/tvsettings/popup/CheckParentalPwd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 7
    .param p1    # Landroid/text/Editable;

    const/4 v6, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    # getter for: Lcom/konka/tvsettings/popup/CheckParentalPwd;->PwdNo1:I
    invoke-static {}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$3()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$4(I)V

    # getter for: Lcom/konka/tvsettings/popup/CheckParentalPwd;->PwdNo1:I
    invoke-static {}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$3()I

    move-result v1

    if-ne v1, v2, :cond_2

    # getter for: Lcom/konka/tvsettings/popup/CheckParentalPwd;->PwdNo2:I
    invoke-static {}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$5()I

    move-result v1

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd$3;->this$0:Lcom/konka/tvsettings/popup/CheckParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/CheckParentalPwd;->check2:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$6(Lcom/konka/tvsettings/popup/CheckParentalPwd;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    iget-object v1, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd$3;->this$0:Lcom/konka/tvsettings/popup/CheckParentalPwd;

    invoke-static {v1, v2}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$7(Lcom/konka/tvsettings/popup/CheckParentalPwd;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd$3;->this$0:Lcom/konka/tvsettings/popup/CheckParentalPwd;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd$3;->this$0:Lcom/konka/tvsettings/popup/CheckParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/CheckParentalPwd;->check1:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$8(Lcom/konka/tvsettings/popup/CheckParentalPwd;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$9(Lcom/konka/tvsettings/popup/CheckParentalPwd;I)V

    # getter for: Lcom/konka/tvsettings/popup/CheckParentalPwd;->PwdNo1:I
    invoke-static {}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$3()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$4(I)V

    # getter for: Lcom/konka/tvsettings/popup/CheckParentalPwd;->PwdNo2:I
    invoke-static {}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$5()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$10(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd$3;->this$0:Lcom/konka/tvsettings/popup/CheckParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/CheckParentalPwd;->check1:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$8(Lcom/konka/tvsettings/popup/CheckParentalPwd;)Landroid/widget/EditText;

    move-result-object v1

    const-string v2, "*"

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    # getter for: Lcom/konka/tvsettings/popup/CheckParentalPwd;->PwdNo1:I
    invoke-static {}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$3()I

    move-result v1

    if-ne v1, v3, :cond_4

    # getter for: Lcom/konka/tvsettings/popup/CheckParentalPwd;->PwdNo2:I
    invoke-static {}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$5()I

    move-result v1

    if-ne v1, v3, :cond_3

    iget-object v1, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd$3;->this$0:Lcom/konka/tvsettings/popup/CheckParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/CheckParentalPwd;->check3:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$11(Lcom/konka/tvsettings/popup/CheckParentalPwd;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    iget-object v1, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd$3;->this$0:Lcom/konka/tvsettings/popup/CheckParentalPwd;

    invoke-static {v1, v3}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$7(Lcom/konka/tvsettings/popup/CheckParentalPwd;I)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd$3;->this$0:Lcom/konka/tvsettings/popup/CheckParentalPwd;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd$3;->this$0:Lcom/konka/tvsettings/popup/CheckParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/CheckParentalPwd;->check2:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$6(Lcom/konka/tvsettings/popup/CheckParentalPwd;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$12(Lcom/konka/tvsettings/popup/CheckParentalPwd;I)V

    # getter for: Lcom/konka/tvsettings/popup/CheckParentalPwd;->PwdNo1:I
    invoke-static {}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$3()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$4(I)V

    # getter for: Lcom/konka/tvsettings/popup/CheckParentalPwd;->PwdNo2:I
    invoke-static {}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$5()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$10(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd$3;->this$0:Lcom/konka/tvsettings/popup/CheckParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/CheckParentalPwd;->check2:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$6(Lcom/konka/tvsettings/popup/CheckParentalPwd;)Landroid/widget/EditText;

    move-result-object v1

    const-string v2, "*"

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_4
    # getter for: Lcom/konka/tvsettings/popup/CheckParentalPwd;->PwdNo1:I
    invoke-static {}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$3()I

    move-result v1

    if-ne v1, v4, :cond_6

    # getter for: Lcom/konka/tvsettings/popup/CheckParentalPwd;->PwdNo2:I
    invoke-static {}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$5()I

    move-result v1

    if-ne v1, v4, :cond_5

    iget-object v1, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd$3;->this$0:Lcom/konka/tvsettings/popup/CheckParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/CheckParentalPwd;->check4:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$13(Lcom/konka/tvsettings/popup/CheckParentalPwd;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    iget-object v1, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd$3;->this$0:Lcom/konka/tvsettings/popup/CheckParentalPwd;

    invoke-static {v1, v4}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$7(Lcom/konka/tvsettings/popup/CheckParentalPwd;I)V

    goto/16 :goto_0

    :cond_5
    iget-object v1, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd$3;->this$0:Lcom/konka/tvsettings/popup/CheckParentalPwd;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd$3;->this$0:Lcom/konka/tvsettings/popup/CheckParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/CheckParentalPwd;->check3:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$11(Lcom/konka/tvsettings/popup/CheckParentalPwd;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$14(Lcom/konka/tvsettings/popup/CheckParentalPwd;I)V

    # getter for: Lcom/konka/tvsettings/popup/CheckParentalPwd;->PwdNo1:I
    invoke-static {}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$3()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$4(I)V

    # getter for: Lcom/konka/tvsettings/popup/CheckParentalPwd;->PwdNo2:I
    invoke-static {}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$5()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$10(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd$3;->this$0:Lcom/konka/tvsettings/popup/CheckParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/CheckParentalPwd;->check3:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$11(Lcom/konka/tvsettings/popup/CheckParentalPwd;)Landroid/widget/EditText;

    move-result-object v1

    const-string v2, "*"

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_6
    # getter for: Lcom/konka/tvsettings/popup/CheckParentalPwd;->PwdNo1:I
    invoke-static {}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$3()I

    move-result v1

    if-ne v1, v6, :cond_0

    # getter for: Lcom/konka/tvsettings/popup/CheckParentalPwd;->PwdNo2:I
    invoke-static {}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$5()I

    move-result v1

    if-ne v1, v6, :cond_7

    invoke-static {v5}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$4(I)V

    invoke-static {v5}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$10(I)V

    :try_start_0
    iget-object v1, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd$3;->this$0:Lcom/konka/tvsettings/popup/CheckParentalPwd;

    # invokes: Lcom/konka/tvsettings/popup/CheckParentalPwd;->checkpsw()V
    invoke-static {v1}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$15(Lcom/konka/tvsettings/popup/CheckParentalPwd;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0

    :cond_7
    iget-object v1, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd$3;->this$0:Lcom/konka/tvsettings/popup/CheckParentalPwd;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd$3;->this$0:Lcom/konka/tvsettings/popup/CheckParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/CheckParentalPwd;->check4:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$13(Lcom/konka/tvsettings/popup/CheckParentalPwd;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$16(Lcom/konka/tvsettings/popup/CheckParentalPwd;I)V

    # getter for: Lcom/konka/tvsettings/popup/CheckParentalPwd;->PwdNo1:I
    invoke-static {}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$3()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$4(I)V

    # getter for: Lcom/konka/tvsettings/popup/CheckParentalPwd;->PwdNo2:I
    invoke-static {}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$5()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$10(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/CheckParentalPwd$3;->this$0:Lcom/konka/tvsettings/popup/CheckParentalPwd;

    # getter for: Lcom/konka/tvsettings/popup/CheckParentalPwd;->check4:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/CheckParentalPwd;->access$13(Lcom/konka/tvsettings/popup/CheckParentalPwd;)Landroid/widget/EditText;

    move-result-object v1

    const-string v2, "*"

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method
