.class Lcom/konka/tvsettings/popup/ChooseCountryActivity$1;
.super Ljava/lang/Object;
.source "ChooseCountryActivity.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/popup/ChooseCountryActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/ChooseCountryActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/popup/ChooseCountryActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/ChooseCountryActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChooseCountryActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v3, 0x0

    const/4 v2, 0x1

    const/16 v4, 0x42

    if-ne p2, v4, :cond_2

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    if-ne v4, v2, :cond_2

    check-cast p1, Landroid/widget/ListView;

    invoke-virtual {p1}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v1

    iget-object v4, p0, Lcom/konka/tvsettings/popup/ChooseCountryActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChooseCountryActivity;

    # invokes: Lcom/konka/tvsettings/popup/ChooseCountryActivity;->updateCountry(I)V
    invoke-static {v4, v1}, Lcom/konka/tvsettings/popup/ChooseCountryActivity;->access$0(Lcom/konka/tvsettings/popup/ChooseCountryActivity;I)V

    iget-object v4, p0, Lcom/konka/tvsettings/popup/ChooseCountryActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChooseCountryActivity;

    # getter for: Lcom/konka/tvsettings/popup/ChooseCountryActivity;->sp:Landroid/content/SharedPreferences;
    invoke-static {v4}, Lcom/konka/tvsettings/popup/ChooseCountryActivity;->access$1(Lcom/konka/tvsettings/popup/ChooseCountryActivity;)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "CHOOSED_COUNTRY"

    invoke-interface {v4, v5, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/konka/tvsettings/popup/ChooseCountryActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChooseCountryActivity;

    # getter for: Lcom/konka/tvsettings/popup/ChooseCountryActivity;->sp:Landroid/content/SharedPreferences;
    invoke-static {v4}, Lcom/konka/tvsettings/popup/ChooseCountryActivity;->access$1(Lcom/konka/tvsettings/popup/ChooseCountryActivity;)Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v4, "tuningCountry"

    iget-object v5, p0, Lcom/konka/tvsettings/popup/ChooseCountryActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChooseCountryActivity;

    # getter for: Lcom/konka/tvsettings/popup/ChooseCountryActivity;->country:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;
    invoke-static {v5}, Lcom/konka/tvsettings/popup/ChooseCountryActivity;->access$2(Lcom/konka/tvsettings/popup/ChooseCountryActivity;)Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    move-result-object v5

    invoke-virtual {v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v5

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string v4, "CHOOSED_COUNTRY"

    invoke-interface {v0, v4, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_0
    iget-object v4, p0, Lcom/konka/tvsettings/popup/ChooseCountryActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChooseCountryActivity;

    # getter for: Lcom/konka/tvsettings/popup/ChooseCountryActivity;->sp:Landroid/content/SharedPreferences;
    invoke-static {v4}, Lcom/konka/tvsettings/popup/ChooseCountryActivity;->access$1(Lcom/konka/tvsettings/popup/ChooseCountryActivity;)Landroid/content/SharedPreferences;

    move-result-object v4

    const-string v5, "FIRST_RUN_GUIDE"

    invoke-interface {v4, v5, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ChooseCountryActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChooseCountryActivity;

    # getter for: Lcom/konka/tvsettings/popup/ChooseCountryActivity;->sp:Landroid/content/SharedPreferences;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/ChooseCountryActivity;->access$1(Lcom/konka/tvsettings/popup/ChooseCountryActivity;)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "FIRST_RUN_GUIDE"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_1
    iget-object v3, p0, Lcom/konka/tvsettings/popup/ChooseCountryActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChooseCountryActivity;

    invoke-virtual {v3}, Lcom/konka/tvsettings/popup/ChooseCountryActivity;->finish()V

    :goto_0
    return v2

    :cond_2
    move v2, v3

    goto :goto_0
.end method
