.class public Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity$OsdLanguageListAdapter;
.super Landroid/widget/BaseAdapter;
.source "ChooseOsdLanguageActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "OsdLanguageListAdapter"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;


# direct methods
.method public constructor <init>(Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;Landroid/content/Context;)V
    .locals 0
    .param p2    # Landroid/content/Context;

    iput-object p1, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity$OsdLanguageListAdapter;->this$0:Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p2, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity$OsdLanguageListAdapter;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity$OsdLanguageListAdapter;->this$0:Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;

    # getter for: Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->languages:[Lcom/konka/kkimplements/tv/LanguageItem;
    invoke-static {v0}, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->access$0(Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;)[Lcom/konka/kkimplements/tv/LanguageItem;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity$OsdLanguageListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030022    # com.konka.tvsettings.R.layout.osd_language_list_item

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    const v2, 0x7f0700ea    # com.konka.tvsettings.R.id.osd_language_text_option

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity$OsdLanguageListAdapter;->this$0:Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;

    # getter for: Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->languages:[Lcom/konka/kkimplements/tv/LanguageItem;
    invoke-static {v2}, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->access$0(Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;)[Lcom/konka/kkimplements/tv/LanguageItem;

    move-result-object v2

    aget-object v2, v2, p1

    iget-object v2, v2, Lcom/konka/kkimplements/tv/LanguageItem;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v2, 0x7f0700eb    # com.konka.tvsettings.R.id.osd_language_select_icon

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v2, 0x7f02009c    # com.konka.tvsettings.R.drawable.osd_lang_icon_uns

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity$OsdLanguageListAdapter;->this$0:Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;

    # getter for: Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->m_iCurrLangIndex:I
    invoke-static {v2}, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->access$1(Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;)I

    move-result v2

    if-ne p1, v2, :cond_0

    const v2, 0x7f02009b    # com.konka.tvsettings.R.drawable.osd_lang_icon_s

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    return-object p2
.end method
