.class Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;
.super Ljava/lang/Object;
.source "PvrActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

.field private final synthetic val$currentTime:I

.field private final synthetic val$total:I


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;II)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    iput p2, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->val$total:I

    iput p3, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->val$currentTime:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PvrActivity;

    move-result-object v1

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->RPProgress:Lcom/konka/tvsettings/popup/TextProgressBar;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$6(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/tvsettings/popup/TextProgressBar;

    move-result-object v1

    iget v2, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->val$total:I

    invoke-virtual {v1, v2}, Lcom/konka/tvsettings/popup/TextProgressBar;->setMax(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PvrActivity;

    move-result-object v1

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->setPvrABLoop:Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$7(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

    move-result-object v1

    sget-object v2, Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;->E_PVR_AB_LOOP_STATUS_AB:Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PvrActivity;

    move-result-object v1

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->currentlooptime:I
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$8(Lcom/konka/tvsettings/popup/PvrActivity;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v1, v2}, Lcom/konka/tvsettings/popup/PvrActivity;->access$9(Lcom/konka/tvsettings/popup/PvrActivity;I)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PvrActivity;

    move-result-object v1

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->RPProgress:Lcom/konka/tvsettings/popup/TextProgressBar;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$6(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/tvsettings/popup/TextProgressBar;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;
    invoke-static {v2}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PvrActivity;

    move-result-object v2

    iget v3, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->val$currentTime:I

    # invokes: Lcom/konka/tvsettings/popup/PvrActivity;->getTimeString(I)Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/konka/tvsettings/popup/PvrActivity;->access$10(Lcom/konka/tvsettings/popup/PvrActivity;I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/konka/tvsettings/popup/TextProgressBar;->setTextProgress(Ljava/lang/String;I)V

    const-string v1, "PVR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "looptime:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PvrActivity;

    move-result-object v3

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->looptime:I
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PvrActivity;->access$11(Lcom/konka/tvsettings/popup/PvrActivity;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PvrActivity;

    move-result-object v1

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->progress_loopab:Landroid/widget/ProgressBar;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$12(Lcom/konka/tvsettings/popup/PvrActivity;)Landroid/widget/ProgressBar;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;
    invoke-static {v2}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PvrActivity;

    move-result-object v2

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->currentlooptime:I
    invoke-static {v2}, Lcom/konka/tvsettings/popup/PvrActivity;->access$8(Lcom/konka/tvsettings/popup/PvrActivity;)I

    move-result v2

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PvrActivity;

    move-result-object v3

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->looptime:I
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PvrActivity;->access$11(Lcom/konka/tvsettings/popup/PvrActivity;)I

    move-result v3

    rem-int/2addr v2, v3

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    const-string v1, "PVR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "max:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PvrActivity;

    move-result-object v3

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->progress_loopab:Landroid/widget/ProgressBar;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PvrActivity;->access$12(Lcom/konka/tvsettings/popup/PvrActivity;)Landroid/widget/ProgressBar;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ProgressBar;->getMax()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "PVR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "current"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PvrActivity;

    move-result-object v3

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->progress_loopab:Landroid/widget/ProgressBar;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PvrActivity;->access$12(Lcom/konka/tvsettings/popup/PvrActivity;)Landroid/widget/ProgressBar;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/ProgressBar;->getProgress()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PvrActivity;

    move-result-object v1

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->totalRecordTime:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$13(Lcom/konka/tvsettings/popup/PvrActivity;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;
    invoke-static {v2}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PvrActivity;

    move-result-object v2

    iget v3, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->val$total:I

    # invokes: Lcom/konka/tvsettings/popup/PvrActivity;->getTimeString(I)Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/konka/tvsettings/popup/PvrActivity;->access$10(Lcom/konka/tvsettings/popup/PvrActivity;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PvrActivity;

    move-result-object v1

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->setPvrABLoop:Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$7(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

    move-result-object v1

    sget-object v2, Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;->E_PVR_AB_LOOP_STATUS_A:Lcom/konka/tvsettings/popup/PvrActivity$PVR_AB_LOOP_STATUS;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PvrActivity;

    move-result-object v1

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->lp4LoopAB:Landroid/widget/RelativeLayout$LayoutParams;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$14(Lcom/konka/tvsettings/popup/PvrActivity;)Landroid/widget/RelativeLayout$LayoutParams;

    move-result-object v2

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PvrActivity;

    move-result-object v1

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->RPProgress:Lcom/konka/tvsettings/popup/TextProgressBar;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$6(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/tvsettings/popup/TextProgressBar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/tvsettings/popup/TextProgressBar;->getProgress()I

    move-result v1

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PvrActivity;

    move-result-object v3

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->A_progress:I
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PvrActivity;->access$15(Lcom/konka/tvsettings/popup/PvrActivity;)I

    move-result v3

    sub-int/2addr v1, v3

    iget-object v3, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PvrActivity;

    move-result-object v3

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->RPProgress:Lcom/konka/tvsettings/popup/TextProgressBar;
    invoke-static {v3}, Lcom/konka/tvsettings/popup/PvrActivity;->access$6(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/tvsettings/popup/TextProgressBar;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/tvsettings/popup/TextProgressBar;->getWidth()I

    move-result v3

    mul-int/2addr v3, v1

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PvrActivity;

    move-result-object v1

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->RPProgress:Lcom/konka/tvsettings/popup/TextProgressBar;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$6(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/tvsettings/popup/TextProgressBar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/tvsettings/popup/TextProgressBar;->getMax()I

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x1

    :goto_1
    div-int v1, v3, v1

    iput v1, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PvrActivity;

    move-result-object v1

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->looptime:I
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$11(Lcom/konka/tvsettings/popup/PvrActivity;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v1, v2}, Lcom/konka/tvsettings/popup/PvrActivity;->access$16(Lcom/konka/tvsettings/popup/PvrActivity;I)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PvrActivity;

    move-result-object v1

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->progress_loopab:Landroid/widget/ProgressBar;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$12(Lcom/konka/tvsettings/popup/PvrActivity;)Landroid/widget/ProgressBar;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;
    invoke-static {v2}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PvrActivity;

    move-result-object v2

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->lp4LoopAB:Landroid/widget/RelativeLayout$LayoutParams;
    invoke-static {v2}, Lcom/konka/tvsettings/popup/PvrActivity;->access$14(Lcom/konka/tvsettings/popup/PvrActivity;)Landroid/widget/RelativeLayout$LayoutParams;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PvrActivity;

    move-result-object v1

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$3(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->isPlaybacking()Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->val$currentTime:I

    iget v2, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->val$total:I

    if-ge v1, v2, :cond_1

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PvrActivity;

    move-result-object v1

    # invokes: Lcom/konka/tvsettings/popup/PvrActivity;->isFastBackPlaying()Z
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$17(Lcom/konka/tvsettings/popup/PvrActivity;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->val$currentTime:I

    if-gtz v1, :cond_2

    :cond_1
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PvrActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->onKeyPlay()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_2
    return-void

    :cond_3
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PvrActivity;

    move-result-object v1

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->RPProgress:Lcom/konka/tvsettings/popup/TextProgressBar;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$6(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/tvsettings/popup/TextProgressBar;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;
    invoke-static {v2}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PvrActivity;

    move-result-object v2

    iget v3, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->val$currentTime:I

    # invokes: Lcom/konka/tvsettings/popup/PvrActivity;->getTimeString(I)Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/konka/tvsettings/popup/PvrActivity;->access$10(Lcom/konka/tvsettings/popup/PvrActivity;I)Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->val$currentTime:I

    invoke-virtual {v1, v2, v3}, Lcom/konka/tvsettings/popup/TextProgressBar;->setTextProgress(Ljava/lang/String;I)V

    goto/16 :goto_0

    :cond_4
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress$1;->this$1:Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;->access$1(Lcom/konka/tvsettings/popup/PvrActivity$PlayBackProgress;)Lcom/konka/tvsettings/popup/PvrActivity;

    move-result-object v1

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->RPProgress:Lcom/konka/tvsettings/popup/TextProgressBar;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$6(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/tvsettings/popup/TextProgressBar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/tvsettings/popup/TextProgressBar;->getMax()I

    move-result v1

    goto/16 :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2
.end method
