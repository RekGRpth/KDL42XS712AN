.class Lcom/konka/tvsettings/popup/InputActivity$InputItemView;
.super Lcom/konka/tvsettings/popup/HotkeyImgView;
.source "InputActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/popup/InputActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "InputItemView"
.end annotation


# instance fields
.field public mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field final synthetic this$0:Lcom/konka/tvsettings/popup/InputActivity;


# direct methods
.method public constructor <init>(Lcom/konka/tvsettings/popup/InputActivity;Landroid/content/Context;Ljava/lang/String;IIILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;I)V
    .locals 7
    .param p2    # Landroid/content/Context;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .param p8    # I

    iput-object p1, p0, Lcom/konka/tvsettings/popup/InputActivity$InputItemView;->this$0:Lcom/konka/tvsettings/popup/InputActivity;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    move v6, p8

    invoke-direct/range {v0 .. v6}, Lcom/konka/tvsettings/popup/HotkeyImgView;-><init>(Landroid/content/Context;Ljava/lang/String;IIII)V

    iput-object p7, p0, Lcom/konka/tvsettings/popup/InputActivity$InputItemView;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    return-void
.end method


# virtual methods
.method public doUpdate()V
    .locals 4

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/popup/InputActivity$InputItemView;->setRunning(Z)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/InputActivity$InputItemView;->this$0:Lcom/konka/tvsettings/popup/InputActivity;

    # getter for: Lcom/konka/tvsettings/popup/InputActivity;->mViewInputMenu:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/InputActivity;->access$0(Lcom/konka/tvsettings/popup/InputActivity;)Landroid/widget/RelativeLayout;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    const-string v1, "==================================================input menu invisible"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/InputActivity$InputItemView;->this$0:Lcom/konka/tvsettings/popup/InputActivity;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/InputActivity$InputItemView;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-static {v1, v2}, Lcom/konka/tvsettings/popup/InputActivity;->access$1(Lcom/konka/tvsettings/popup/InputActivity;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/InputActivity$InputItemView;->this$0:Lcom/konka/tvsettings/popup/InputActivity;

    # invokes: Lcom/konka/tvsettings/popup/InputActivity;->stopTimeShift()V
    invoke-static {v1}, Lcom/konka/tvsettings/popup/InputActivity;->access$2(Lcom/konka/tvsettings/popup/InputActivity;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/InputActivity$InputItemView;->this$0:Lcom/konka/tvsettings/popup/InputActivity;

    # getter for: Lcom/konka/tvsettings/popup/InputActivity;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/InputActivity;->access$3(Lcom/konka/tvsettings/popup/InputActivity;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/popup/InputActivity$InputItemView;->this$0:Lcom/konka/tvsettings/popup/InputActivity;

    # invokes: Lcom/konka/tvsettings/popup/InputActivity;->switchToUsbSource()V
    invoke-static {v1}, Lcom/konka/tvsettings/popup/InputActivity;->access$4(Lcom/konka/tvsettings/popup/InputActivity;)V

    :goto_0
    iget-object v1, p0, Lcom/konka/tvsettings/popup/InputActivity$InputItemView;->this$0:Lcom/konka/tvsettings/popup/InputActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/popup/InputActivity;->finish()V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "======click: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/InputActivity$InputItemView;->mInputMode:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/tvsettings/popup/InputActivity$InputItemView;->this$0:Lcom/konka/tvsettings/popup/InputActivity;

    # getter for: Lcom/konka/tvsettings/popup/InputActivity;->rootActivity:Lcom/konka/tvsettings/RootActivity;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/InputActivity;->access$5(Lcom/konka/tvsettings/popup/InputActivity;)Lcom/konka/tvsettings/RootActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/tvsettings/popup/InputActivity$InputItemView;->this$0:Lcom/konka/tvsettings/popup/InputActivity;

    # getter for: Lcom/konka/tvsettings/popup/InputActivity;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    invoke-static {v2}, Lcom/konka/tvsettings/popup/InputActivity;->access$3(Lcom/konka/tvsettings/popup/InputActivity;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/tvsettings/popup/InputActivity$InputItemView;->this$0:Lcom/konka/tvsettings/popup/InputActivity;

    invoke-virtual {v3}, Lcom/konka/tvsettings/popup/InputActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/konka/tvsettings/RootActivity;->setTVInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Landroid/content/Context;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/InputActivity$InputItemView;->this$0:Lcom/konka/tvsettings/popup/InputActivity;

    invoke-static {v1}, Lcom/konka/tvsettings/SwitchMenuHelper;->unFfeeze(Landroid/app/Activity;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/InputActivity$InputItemView;->this$0:Lcom/konka/tvsettings/popup/InputActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/popup/InputActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/konka/tvsettings/popup/SourceInfoActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/InputActivity$InputItemView;->this$0:Lcom/konka/tvsettings/popup/InputActivity;

    invoke-virtual {v1, v0}, Lcom/konka/tvsettings/popup/InputActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
