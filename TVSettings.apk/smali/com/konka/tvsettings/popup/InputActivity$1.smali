.class Lcom/konka/tvsettings/popup/InputActivity$1;
.super Landroid/os/Handler;
.source "InputActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/popup/InputActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/InputActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/popup/InputActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/InputActivity$1;->this$0:Lcom/konka/tvsettings/popup/InputActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1    # Landroid/os/Message;

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    const/16 v1, 0x65

    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v1, v2, :cond_1

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/InputActivity$1;->this$0:Lcom/konka/tvsettings/popup/InputActivity;

    const-class v2, Lcom/konka/tvsettings/RootActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/InputActivity$1;->this$0:Lcom/konka/tvsettings/popup/InputActivity;

    invoke-virtual {v1, v0}, Lcom/konka/tvsettings/popup/InputActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/InputActivity$1;->this$0:Lcom/konka/tvsettings/popup/InputActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/popup/InputActivity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v1, 0x2bf

    iget v2, p1, Landroid/os/Message;->what:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/popup/InputActivity$1;->this$0:Lcom/konka/tvsettings/popup/InputActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/popup/InputActivity;->isFocusOnCurrentSource()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/popup/InputActivity$1;->this$0:Lcom/konka/tvsettings/popup/InputActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/popup/InputActivity;->changeInputSourceTimeout()V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/InputActivity$1;->this$0:Lcom/konka/tvsettings/popup/InputActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/popup/InputActivity;->finish()V

    goto :goto_0
.end method
