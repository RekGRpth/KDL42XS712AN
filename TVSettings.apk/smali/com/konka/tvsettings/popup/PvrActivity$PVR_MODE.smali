.class public final enum Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;
.super Ljava/lang/Enum;
.source "PvrActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/popup/PvrActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PVR_MODE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

.field public static final enum E_PVR_MODE_ALWAYS_TIME_SHIFT:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

.field public static final enum E_PVR_MODE_FILE_BROWSER:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

.field public static final enum E_PVR_MODE_NONE:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

.field public static final enum E_PVR_MODE_PLAYBACK:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

.field public static final enum E_PVR_MODE_RECORD:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

.field public static final enum E_PVR_MODE_SHORT:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

.field public static final enum E_PVR_MODE_TIME_SHIFT:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    const-string v1, "E_PVR_MODE_NONE"

    invoke-direct {v0, v1, v3}, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;->E_PVR_MODE_NONE:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    new-instance v0, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    const-string v1, "E_PVR_MODE_RECORD"

    invoke-direct {v0, v1, v4}, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;->E_PVR_MODE_RECORD:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    new-instance v0, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    const-string v1, "E_PVR_MODE_PLAYBACK"

    invoke-direct {v0, v1, v5}, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;->E_PVR_MODE_PLAYBACK:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    new-instance v0, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    const-string v1, "E_PVR_MODE_TIME_SHIFT"

    invoke-direct {v0, v1, v6}, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;->E_PVR_MODE_TIME_SHIFT:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    new-instance v0, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    const-string v1, "E_PVR_MODE_ALWAYS_TIME_SHIFT"

    invoke-direct {v0, v1, v7}, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;->E_PVR_MODE_ALWAYS_TIME_SHIFT:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    new-instance v0, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    const-string v1, "E_PVR_MODE_FILE_BROWSER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;->E_PVR_MODE_FILE_BROWSER:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    new-instance v0, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    const-string v1, "E_PVR_MODE_SHORT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;->E_PVR_MODE_SHORT:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    sget-object v1, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;->E_PVR_MODE_NONE:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;->E_PVR_MODE_RECORD:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;->E_PVR_MODE_PLAYBACK:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;->E_PVR_MODE_TIME_SHIFT:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    aput-object v1, v0, v6

    sget-object v1, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;->E_PVR_MODE_ALWAYS_TIME_SHIFT:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;->E_PVR_MODE_FILE_BROWSER:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;->E_PVR_MODE_SHORT:Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;->ENUM$VALUES:[Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;
    .locals 1

    const-class v0, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    return-object v0
.end method

.method public static values()[Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;->ENUM$VALUES:[Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/tvsettings/popup/PvrActivity$PVR_MODE;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
