.class Lcom/konka/tvsettings/popup/PvrActivity$RootMenuEventReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PvrActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/popup/PvrActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RootMenuEventReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/PvrActivity;


# direct methods
.method private constructor <init>(Lcom/konka/tvsettings/popup/PvrActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/PvrActivity$RootMenuEventReceiver;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/tvsettings/popup/PvrActivity;Lcom/konka/tvsettings/popup/PvrActivity$RootMenuEventReceiver;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/popup/PvrActivity$RootMenuEventReceiver;-><init>(Lcom/konka/tvsettings/popup/PvrActivity;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "get action=="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const-string v2, "com.konka.tv.action.PVR_STOP_RECORD"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :try_start_0
    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity$RootMenuEventReceiver;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v2}, Lcom/konka/tvsettings/popup/PvrActivity;->access$3(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/PvrDesk;->isRecording()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity$RootMenuEventReceiver;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v2}, Lcom/konka/tvsettings/popup/PvrActivity;->access$3(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/PvrDesk;->isTimeShiftRecording()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/konka/tvsettings/popup/PvrActivity$RootMenuEventReceiver;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # invokes: Lcom/konka/tvsettings/popup/PvrActivity;->saveAndExit()V
    invoke-static {v2}, Lcom/konka/tvsettings/popup/PvrActivity;->access$19(Lcom/konka/tvsettings/popup/PvrActivity;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method
