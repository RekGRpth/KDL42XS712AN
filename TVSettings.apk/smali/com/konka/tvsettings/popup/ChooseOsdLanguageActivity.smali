.class public Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;
.super Lcom/konka/tvsettings/BaseKonkaActivity;
.source "ChooseOsdLanguageActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity$OsdLanguageListAdapter;
    }
.end annotation


# instance fields
.field private adapter:Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity$OsdLanguageListAdapter;

.field private languages:[Lcom/konka/kkimplements/tv/LanguageItem;

.field private m_iCurrLangIndex:I

.field private m_locale:Ljava/util/Locale;

.field private myHandler:Landroid/os/Handler;

.field private osdLangListView:Landroid/widget/ListView;

.field private s3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

.field private serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

.field private settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;-><init>()V

    iput-object v1, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->adapter:Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity$OsdLanguageListAdapter;

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->m_iCurrLangIndex:I

    iput-object v1, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->s3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    new-instance v0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity$1;-><init>(Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->myHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;)[Lcom/konka/kkimplements/tv/LanguageItem;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->languages:[Lcom/konka/kkimplements/tv/LanguageItem;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;)I
    .locals 1

    iget v0, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->m_iCurrLangIndex:I

    return v0
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;)Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->osdLangListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->updateOsdLanguage(I)V

    return-void
.end method

.method private getLocale(Ljava/lang/String;)Ljava/util/Locale;
    .locals 4
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/util/Locale;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    const/4 v3, 0x5

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->m_locale:Ljava/util/Locale;

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->m_locale:Ljava/util/Locale;

    return-object v0
.end method

.method private initLanguageData()V
    .locals 3

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v2, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->languages:[Lcom/konka/kkimplements/tv/LanguageItem;

    invoke-static {v2, v1}, Lcom/konka/kkimplements/tv/LanguageUtil;->getCurrentLanguageIndex([Lcom/konka/kkimplements/tv/LanguageItem;Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->m_iCurrLangIndex:I

    return-void
.end method

.method private updateOsdLanguage(I)V
    .locals 9
    .param p1    # I

    const/4 v8, 0x1

    new-instance v5, Lcom/konka/tvsettings/common/PreferancesTools;

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/konka/tvsettings/common/PreferancesTools;-><init>(Landroid/content/Context;)V

    iget-object v6, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->languages:[Lcom/konka/kkimplements/tv/LanguageItem;

    aget-object v6, v6, p1

    iget-object v6, v6, Lcom/konka/kkimplements/tv/LanguageItem;->mLanguage:Ljava/lang/String;

    invoke-direct {p0, v6}, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->getLocale(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v6

    iput-object v6, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->m_locale:Ljava/util/Locale;

    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v3

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    new-instance v6, Ljava/lang/StringBuilder;

    iget-object v7, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v7}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "_"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v7}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x0

    :try_start_0
    invoke-virtual {v5, v6}, Lcom/konka/tvsettings/common/PreferancesTools;->setShowInfoMenuStatus(Z)V

    invoke-interface {v3}, Landroid/app/IActivityManager;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v6, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->m_locale:Ljava/util/Locale;

    iput-object v6, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    const/4 v6, 0x1

    iput-boolean v6, v1, Landroid/content/res/Configuration;->userSetLocale:Z

    invoke-interface {v3, v1}, Landroid/app/IActivityManager;->updateConfiguration(Landroid/content/res/Configuration;)V

    iget-object v6, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->languages:[Lcom/konka/kkimplements/tv/LanguageItem;

    aget-object v6, v6, p1

    iget-object v6, v6, Lcom/konka/kkimplements/tv/LanguageItem;->mLanguage:Ljava/lang/String;

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getS3DManagerInstance()Lcom/konka/kkinterface/tv/S3DDesk;

    move-result-object v6

    iput-object v6, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->s3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    iget-object v6, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->s3dDesk:Lcom/konka/kkinterface/tv/S3DDesk;

    sget-object v7, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-interface {v6, v7}, Lcom/konka/kkinterface/tv/S3DDesk;->setDisplayFormat(Lcom/konka/kkinterface/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;)Z

    :cond_0
    const-string v6, "com.android.providers.settings"

    invoke-static {v6}, Landroid/app/backup/BackupManager;->dataChanged(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    invoke-virtual {v5, v8}, Lcom/konka/tvsettings/common/PreferancesTools;->setShowInfoMenuStatus(Z)V

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const-string v2, "onCreate"

    const-string v3, "-----------------------OSDLanguage Avtivity OnCreate---------------------"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Lcom/konka/tvsettings/BaseKonkaActivity;->onCreate(Landroid/os/Bundle;)V

    const v2, 0x7f030021    # com.konka.tvsettings.R.layout.osd_language_list

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/TVRootApp;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/kkimplements/tv/LanguageUtil;->getCustomeLanguageList(Landroid/content/Context;)[Lcom/konka/kkimplements/tv/LanguageItem;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->languages:[Lcom/konka/kkimplements/tv/LanguageItem;

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->languages:[Lcom/konka/kkimplements/tv/LanguageItem;

    array-length v2, v2

    if-lt v1, v2, :cond_0

    const v2, 0x7f0700e8    # com.konka.tvsettings.R.id.osd_language_list_view

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->osdLangListView:Landroid/widget/ListView;

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->initLanguageData()V

    new-instance v2, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity$OsdLanguageListAdapter;

    invoke-direct {v2, p0, p0}, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity$OsdLanguageListAdapter;-><init>(Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->adapter:Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity$OsdLanguageListAdapter;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->osdLangListView:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->adapter:Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity$OsdLanguageListAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->osdLangListView:Landroid/widget/ListView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setDividerHeight(I)V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->osdLangListView:Landroid/widget/ListView;

    iget v3, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->m_iCurrLangIndex:I

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setSelection(I)V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->osdLangListView:Landroid/widget/ListView;

    new-instance v3, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity$2;

    invoke-direct {v3, p0}, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity$2;-><init>(Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->myHandler:Landroid/os/Handler;

    invoke-static {v2}, Lcom/konka/tvsettings/common/LittleDownTimer;->setHandler(Landroid/os/Handler;)V

    return-void

    :cond_0
    const-string v2, "languages"

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->languages:[Lcom/konka/kkimplements/tv/LanguageItem;

    aget-object v4, v4, v1

    iget-object v4, v4, Lcom/konka/kkimplements/tv/LanguageItem;->mLanguage:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->languages:[Lcom/konka/kkimplements/tv/LanguageItem;

    aget-object v4, v4, v1

    iget-object v4, v4, Lcom/konka/kkimplements/tv/LanguageItem;->mCountry:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->languages:[Lcom/konka/kkimplements/tv/LanguageItem;

    aget-object v4, v4, v1

    iget-object v4, v4, Lcom/konka/kkimplements/tv/LanguageItem;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v1, 0x1

    const/16 v2, 0x13

    if-ne p1, v2, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->osdLangListView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->osdLangListView:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->languages:[Lcom/konka/kkimplements/tv/LanguageItem;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setSelection(I)V

    :goto_0
    return v1

    :cond_0
    const/16 v2, 0x14

    if-ne p1, v2, :cond_1

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->osdLangListView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v2

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->languages:[Lcom/konka/kkimplements/tv/LanguageItem;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->osdLangListView:Landroid/widget/ListView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_0

    :cond_1
    const/16 v2, 0x52

    if-ne p1, v2, :cond_3

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->settingdesk:Lcom/konka/kkinterface/tv/SettingDesk;

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/SettingDesk;->GetInstallationFlag()Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v2, Lcom/konka/tvsettings/popup/InstallationGuideActivity;

    invoke-virtual {v0, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->finish()V

    goto :goto_0

    :cond_2
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v2, Lcom/konka/tvsettings/function/OsdSettingActivity;

    invoke-virtual {v0, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v2, "currentPage"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/ChooseOsdLanguageActivity;->finish()V

    goto :goto_0

    :cond_3
    invoke-super {p0, p1, p2}, Lcom/konka/tvsettings/BaseKonkaActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0
.end method

.method protected onPause()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->pauseMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resumeMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onResume()V

    return-void
.end method

.method public onUserInteraction()V
    .locals 0

    invoke-static {}, Lcom/konka/tvsettings/common/LittleDownTimer;->resetMenu()V

    invoke-super {p0}, Lcom/konka/tvsettings/BaseKonkaActivity;->onUserInteraction()V

    return-void
.end method
