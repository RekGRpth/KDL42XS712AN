.class Lcom/konka/tvsettings/popup/ChannelListActivity$1;
.super Ljava/lang/Object;
.source "ChannelListActivity.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/popup/ChannelListActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/ChannelListActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/popup/ChannelListActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/ChannelListActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChannelListActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 12
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    iget-object v8, p0, Lcom/konka/tvsettings/popup/ChannelListActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChannelListActivity;

    # getter for: Lcom/konka/tvsettings/popup/ChannelListActivity;->proListView:Landroid/widget/ListView;
    invoke-static {v8}, Lcom/konka/tvsettings/popup/ChannelListActivity;->access$0(Lcom/konka/tvsettings/popup/ChannelListActivity;)Landroid/widget/ListView;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/ListView;->getSelectedItemId()J

    move-result-wide v8

    long-to-int v6, v8

    const/16 v8, 0x42

    if-ne p2, v8, :cond_5

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_5

    const-string v8, "TvApp"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "selItemIndex"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/konka/tvsettings/popup/ChannelListActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChannelListActivity;

    # getter for: Lcom/konka/tvsettings/popup/ChannelListActivity;->progInfoList:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/konka/tvsettings/popup/ChannelListActivity;->access$1(Lcom/konka/tvsettings/popup/ChannelListActivity;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lt v6, v8, :cond_0

    const/4 v8, 0x0

    :goto_0
    return v8

    :cond_0
    iget-object v8, p0, Lcom/konka/tvsettings/popup/ChannelListActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChannelListActivity;

    # getter for: Lcom/konka/tvsettings/popup/ChannelListActivity;->progInfoList:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/konka/tvsettings/popup/ChannelListActivity;->access$1(Lcom/konka/tvsettings/popup/ChannelListActivity;)Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    const-string v8, "TvApp"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "number"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v10, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/konka/tvsettings/popup/ChannelListActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChannelListActivity;

    iget-object v8, v8, Lcom/konka/tvsettings/popup/ChannelListActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v8

    iget v8, v8, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    iget v9, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    if-ne v8, v9, :cond_1

    iget-object v8, p0, Lcom/konka/tvsettings/popup/ChannelListActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChannelListActivity;

    iget-object v8, v8, Lcom/konka/tvsettings/popup/ChannelListActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v8

    iget-short v8, v8, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    iget-short v9, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    if-ne v8, v9, :cond_1

    const-string v8, "TuningService"

    const-string v9, "CH List :Select the same channel!!!"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    const/4 v8, 0x1

    goto :goto_0

    :cond_1
    iget-short v8, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    sget-object v9, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->E_SERVICETYPE_INVALID:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    invoke-virtual {v9}, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->ordinal()I

    move-result v9

    if-ge v8, v9, :cond_4

    new-instance v5, Landroid/content/Intent;

    const-string v8, "com.konka.tv.hotkey.service.UNFREEZE"

    invoke-direct {v5, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/konka/tvsettings/popup/ChannelListActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChannelListActivity;

    invoke-virtual {v8, v5}, Lcom/konka/tvsettings/popup/ChannelListActivity;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v8, p0, Lcom/konka/tvsettings/popup/ChannelListActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChannelListActivity;

    iget-object v8, v8, Lcom/konka/tvsettings/popup/ChannelListActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    iget-object v8, p0, Lcom/konka/tvsettings/popup/ChannelListActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChannelListActivity;

    iget-object v8, v8, Lcom/konka/tvsettings/popup/ChannelListActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v8}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v7

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->isEnableAlTimeShift()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v8

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v8, v9, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    if-eqz v8, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v8

    if-eqz v8, :cond_2

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/PvrManager;->isTimeShiftRecording()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v7}, Lcom/konka/kkinterface/tv/SettingDesk;->getAlTimeShift()Z

    move-result v8

    if-eqz v8, :cond_2

    const-string v8, "========>>>stop PVR TimeShift"

    invoke-static {v8}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v4, Landroid/content/Intent;

    const-string v8, "com.konka.stop.alwaytimeshiftthread"

    invoke-direct {v4, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/konka/tvsettings/popup/ChannelListActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChannelListActivity;

    invoke-virtual {v8}, Lcom/konka/tvsettings/popup/ChannelListActivity;->getApplication()Landroid/app/Application;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/TvManager;->getPvrManager()Lcom/mstar/android/tvapi/common/PvrManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/PvrManager;->stopTimeShift()V

    iget-object v8, p0, Lcom/konka/tvsettings/popup/ChannelListActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChannelListActivity;

    const/4 v9, 0x1

    invoke-static {v8, v9}, Lcom/konka/tvsettings/popup/ChannelListActivity;->access$2(Lcom/konka/tvsettings/popup/ChannelListActivity;Z)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_2
    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tv/TvPipPopManager;->getSubInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v8

    sget-object v9, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_CVBS:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v8, v9, :cond_3

    iget-short v8, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    sget-object v9, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->E_SERVICETYPE_ATV:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    invoke-virtual {v9}, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->ordinal()I

    move-result v9

    if-ne v8, v9, :cond_3

    const-string v8, "ChannelListActivity"

    const-string v9, "ATV selected, close PIP..."

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tv/TvPipPopManager;->isPipEnabled()Z

    move-result v8

    if-eqz v8, :cond_3

    new-instance v3, Landroid/content/Intent;

    const-string v8, "com.konka.hotkey.disablePip"

    invoke-direct {v3, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/konka/tvsettings/popup/ChannelListActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChannelListActivity;

    invoke-virtual {v8, v3}, Lcom/konka/tvsettings/popup/ChannelListActivity;->sendBroadcast(Landroid/content/Intent;)V

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tv/TvPipPopManager;->disablePip()Z

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/mstar/android/tv/TvPipPopManager;->setPipOnFlag(Z)Z

    :cond_3
    iget-object v8, p0, Lcom/konka/tvsettings/popup/ChannelListActivity$1;->this$0:Lcom/konka/tvsettings/popup/ChannelListActivity;

    iget-object v8, v8, Lcom/konka/tvsettings/popup/ChannelListActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    iget v9, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    invoke-static {}, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->values()[Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    move-result-object v10

    iget-short v11, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    aget-object v10, v10, v11

    invoke-interface {v8, v9, v10}, Lcom/konka/kkinterface/tv/ChannelDesk;->programSel(ILcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;)Z

    goto/16 :goto_1

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2

    :cond_4
    const/4 v8, 0x0

    goto/16 :goto_0

    :cond_5
    const/4 v8, 0x0

    goto/16 :goto_0
.end method
