.class public Lcom/konka/tvsettings/popup/PvrImageFlag;
.super Ljava/lang/Object;
.source "PvrImageFlag.java"


# instance fields
.field private backwardFlag:Z

.field private captureFlag:Z

.field private ffFlag:Z

.field private fowardFlag:Z

.field private pauseFlag:Z

.field private playFlag:Z

.field private recorderFlag:Z

.field private revFlag:Z

.field private slowFlag:Z

.field private stopFlag:Z

.field private timeFlag:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/konka/tvsettings/popup/PvrImageFlag;->playFlag:Z

    iput-boolean v0, p0, Lcom/konka/tvsettings/popup/PvrImageFlag;->stopFlag:Z

    iput-boolean v0, p0, Lcom/konka/tvsettings/popup/PvrImageFlag;->pauseFlag:Z

    iput-boolean v0, p0, Lcom/konka/tvsettings/popup/PvrImageFlag;->recorderFlag:Z

    iput-boolean v0, p0, Lcom/konka/tvsettings/popup/PvrImageFlag;->ffFlag:Z

    iput-boolean v0, p0, Lcom/konka/tvsettings/popup/PvrImageFlag;->revFlag:Z

    iput-boolean v0, p0, Lcom/konka/tvsettings/popup/PvrImageFlag;->slowFlag:Z

    iput-boolean v0, p0, Lcom/konka/tvsettings/popup/PvrImageFlag;->timeFlag:Z

    iput-boolean v0, p0, Lcom/konka/tvsettings/popup/PvrImageFlag;->fowardFlag:Z

    iput-boolean v0, p0, Lcom/konka/tvsettings/popup/PvrImageFlag;->backwardFlag:Z

    iput-boolean v0, p0, Lcom/konka/tvsettings/popup/PvrImageFlag;->captureFlag:Z

    return-void
.end method


# virtual methods
.method public isBackwardFlag()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/popup/PvrImageFlag;->backwardFlag:Z

    return v0
.end method

.method public isCaptureFlag()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/popup/PvrImageFlag;->captureFlag:Z

    return v0
.end method

.method public isFfFlag()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/popup/PvrImageFlag;->ffFlag:Z

    return v0
.end method

.method public isFowardFlag()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/popup/PvrImageFlag;->fowardFlag:Z

    return v0
.end method

.method public isPauseFlag()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/popup/PvrImageFlag;->pauseFlag:Z

    return v0
.end method

.method public isPlayFlag()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/popup/PvrImageFlag;->playFlag:Z

    return v0
.end method

.method public isRecorderFlag()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/popup/PvrImageFlag;->recorderFlag:Z

    return v0
.end method

.method public isRevFlag()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/popup/PvrImageFlag;->revFlag:Z

    return v0
.end method

.method public isSlowFlag()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/popup/PvrImageFlag;->slowFlag:Z

    return v0
.end method

.method public isStopFlag()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/popup/PvrImageFlag;->stopFlag:Z

    return v0
.end method

.method public isTimeFlag()Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/popup/PvrImageFlag;->timeFlag:Z

    return v0
.end method

.method public setBackwardFlag(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/tvsettings/popup/PvrImageFlag;->backwardFlag:Z

    return-void
.end method

.method public setCaptureFlag(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/tvsettings/popup/PvrImageFlag;->captureFlag:Z

    return-void
.end method

.method public setFfFlag(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/tvsettings/popup/PvrImageFlag;->ffFlag:Z

    return-void
.end method

.method public setFowardFlag(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/tvsettings/popup/PvrImageFlag;->fowardFlag:Z

    return-void
.end method

.method public setPauseFlag(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/tvsettings/popup/PvrImageFlag;->pauseFlag:Z

    return-void
.end method

.method public setPlayFlag(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/tvsettings/popup/PvrImageFlag;->playFlag:Z

    return-void
.end method

.method public setRecorderFlag(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/tvsettings/popup/PvrImageFlag;->recorderFlag:Z

    return-void
.end method

.method public setRevFlag(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/tvsettings/popup/PvrImageFlag;->revFlag:Z

    return-void
.end method

.method public setSlowFlag(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/tvsettings/popup/PvrImageFlag;->slowFlag:Z

    return-void
.end method

.method public setStopFlag(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/tvsettings/popup/PvrImageFlag;->stopFlag:Z

    return-void
.end method

.method public setTimeFlag(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/tvsettings/popup/PvrImageFlag;->timeFlag:Z

    return-void
.end method
