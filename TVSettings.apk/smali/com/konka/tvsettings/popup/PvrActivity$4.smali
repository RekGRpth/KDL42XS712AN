.class Lcom/konka/tvsettings/popup/PvrActivity$4;
.super Ljava/lang/Object;
.source "PvrActivity.java"

# interfaces
.implements Lcom/konka/tvsettings/function/USBDiskSelecter$usbListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/popup/PvrActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/popup/PvrActivity;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/popup/PvrActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/popup/PvrActivity$4;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onUSBEject(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const-string v1, "PVR ========>>>onUSBEject"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$4;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->recordDiskPath:Ljava/lang/String;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$26(Lcom/konka/tvsettings/popup/PvrActivity;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$4;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->recordDiskPath:Ljava/lang/String;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$26(Lcom/konka/tvsettings/popup/PvrActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$4;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$3(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->isTimeShiftRecording()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$4;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$3(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->stopTimeShift()V

    :cond_2
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$4;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$3(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->isPlaybacking()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$4;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # invokes: Lcom/konka/tvsettings/popup/PvrActivity;->resumeLang()V
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$34(Lcom/konka/tvsettings/popup/PvrActivity;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$4;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # invokes: Lcom/konka/tvsettings/popup/PvrActivity;->resumeSubtitle()V
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$35(Lcom/konka/tvsettings/popup/PvrActivity;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$4;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$3(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->stopPlayback()V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$4;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$3(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->stopPlaybackLoop()V

    :cond_3
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$4;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$3(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->isRecording()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$4;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # getter for: Lcom/konka/tvsettings/popup/PvrActivity;->pvr:Lcom/konka/kkinterface/tv/PvrDesk;
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$3(Lcom/konka/tvsettings/popup/PvrActivity;)Lcom/konka/kkinterface/tv/PvrDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/PvrDesk;->stopRecord()V

    :cond_4
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$4;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # invokes: Lcom/konka/tvsettings/popup/PvrActivity;->isBootedByRecord()Z
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$36(Lcom/konka/tvsettings/popup/PvrActivity;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$4;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    # invokes: Lcom/konka/tvsettings/popup/PvrActivity;->standbySystem()V
    invoke-static {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->access$37(Lcom/konka/tvsettings/popup/PvrActivity;)V

    :cond_5
    iget-object v1, p0, Lcom/konka/tvsettings/popup/PvrActivity$4;->this$0:Lcom/konka/tvsettings/popup/PvrActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/popup/PvrActivity;->finish()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public onUSBMounted(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method

.method public onUSBUnmounted(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    return-void
.end method
