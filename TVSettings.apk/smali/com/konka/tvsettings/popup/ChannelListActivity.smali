.class public Lcom/konka/tvsettings/popup/ChannelListActivity;
.super Lcom/konka/tvsettings/teletext/MstarBaseActivity;
.source "ChannelListActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/popup/ChannelListActivity$ChannelListAdapter;,
        Lcom/konka/tvsettings/popup/ChannelListActivity$ProgramFavoriteObject;
    }
.end annotation


# instance fields
.field private adapter:Lcom/konka/tvsettings/popup/ChannelListActivity$ChannelListAdapter;

.field private adapter1:Lcom/konka/tvsettings/channel/ProgramEditAdapter;

.field private bNeedRestartAlwaysTimeshift:Z

.field cd:Lcom/konka/kkinterface/tv/ChannelDesk;

.field private m_nServiceNum:I

.field private pfos:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/tvsettings/popup/ChannelListActivity$ProgramFavoriteObject;",
            ">;"
        }
    .end annotation
.end field

.field private plvios:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/konka/tvsettings/channel/ProgramListViewItemObject;",
            ">;"
        }
    .end annotation
.end field

.field private proListView:Landroid/widget/ListView;

.field private progInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/mstar/android/tvapi/common/vo/ProgramInfo;",
            ">;"
        }
    .end annotation
.end field

.field private proglistId:I

.field serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;-><init>()V

    iput v1, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->proglistId:I

    iput v1, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->m_nServiceNum:I

    iput-object v2, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->plvios:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->pfos:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->progInfoList:Ljava/util/ArrayList;

    iput-object v2, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->adapter:Lcom/konka/tvsettings/popup/ChannelListActivity$ChannelListAdapter;

    iput-object v2, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->adapter1:Lcom/konka/tvsettings/channel/ProgramEditAdapter;

    iput-boolean v1, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->bNeedRestartAlwaysTimeshift:Z

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/popup/ChannelListActivity;)Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->proListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/popup/ChannelListActivity;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->progInfoList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/popup/ChannelListActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->bNeedRestartAlwaysTimeshift:Z

    return-void
.end method

.method private addOneListViewItem(Lcom/mstar/android/tvapi/common/vo/ProgramInfo;)V
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    new-instance v1, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;

    invoke-direct {v1}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;-><init>()V

    iget-object v2, p1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setTvName(Ljava/lang/String;)V

    iget-short v2, p1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    sget-object v3, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->E_SERVICETYPE_ATV:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->ordinal()I

    move-result v3

    if-ne v2, v3, :cond_2

    iget v2, p1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setTvNumber(Ljava/lang/String;)V

    :goto_0
    const/4 v0, 0x0

    iget-short v2, p1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->favorite:S

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v1, v0}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setFavoriteImg(Z)V

    iget-boolean v0, p1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->isSkip:Z

    invoke-virtual {v1, v0}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setSkipImg(Z)V

    iget-boolean v0, p1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->isScramble:Z

    invoke-virtual {v1, v0}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setSslImg(Z)V

    iget-boolean v0, p1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->isLock:Z

    invoke-virtual {v1, v0}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setLockImg(Z)V

    iget-short v2, p1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    invoke-virtual {v1, v2}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setServiceType(S)V

    iget-short v2, p1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    sget-object v3, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->E_SERVICETYPE_ATV:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->ordinal()I

    move-result v3

    if-ne v2, v3, :cond_3

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setSourceImg(Z)V

    :goto_1
    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->plvios:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void

    :cond_2
    iget v2, p1, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setTvNumber(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/konka/tvsettings/channel/ProgramListViewItemObject;->setSourceImg(Z)V

    goto :goto_1
.end method

.method private getListInfo()V
    .locals 9

    const/4 v8, 0x0

    const/4 v4, 0x0

    const/4 v1, 0x0

    iget-object v5, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v5

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v5, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    sget-object v6, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->E_COUNT_ATV_DTV:Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    invoke-interface {v5, v6}, Lcom/konka/kkinterface/tv/ChannelDesk;->getProgramCount(Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;)I

    move-result v5

    iput v5, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->m_nServiceNum:I

    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "\n--->indexBase: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "\n--->m_nServiceNum: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->m_nServiceNum:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    move v2, v1

    :goto_0
    iget v5, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->m_nServiceNum:I

    if-lt v2, v5, :cond_0

    return-void

    :cond_0
    iget-object v5, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v5, v2}, Lcom/konka/kkinterface/tv/ChannelDesk;->getProgramInfoByIndex(I)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v4

    if-eqz v4, :cond_1

    iget v5, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->proglistId:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_4

    iget-boolean v5, v4, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->isDelete:Z

    if-nez v5, :cond_1

    iget-boolean v5, v4, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->isVisible:Z

    if-eqz v5, :cond_1

    iget-short v5, v4, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->favorite:S

    if-eqz v5, :cond_1

    iget-boolean v5, v4, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->isSkip:Z

    if-eqz v5, :cond_2

    :cond_1
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    new-instance v3, Lcom/konka/tvsettings/popup/ChannelListActivity$ProgramFavoriteObject;

    invoke-direct {v3, p0, v8}, Lcom/konka/tvsettings/popup/ChannelListActivity$ProgramFavoriteObject;-><init>(Lcom/konka/tvsettings/popup/ChannelListActivity;Lcom/konka/tvsettings/popup/ChannelListActivity$ProgramFavoriteObject;)V

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v0, v5, :cond_3

    iget v5, v4, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    add-int/lit8 v5, v5, 0x1

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/konka/tvsettings/popup/ChannelListActivity$ProgramFavoriteObject;->setChannelId(Ljava/lang/String;)V

    :goto_2
    iget-object v5, v4, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceName:Ljava/lang/String;

    invoke-virtual {v3, v5}, Lcom/konka/tvsettings/popup/ChannelListActivity$ProgramFavoriteObject;->setChannelName(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->pfos:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v5, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->progInfoList:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, v4}, Lcom/konka/tvsettings/popup/ChannelListActivity;->addOneListViewItem(Lcom/mstar/android/tvapi/common/vo/ProgramInfo;)V

    goto :goto_1

    :cond_3
    iget v5, v4, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/konka/tvsettings/popup/ChannelListActivity$ProgramFavoriteObject;->setChannelId(Ljava/lang/String;)V

    goto :goto_2

    :cond_4
    iget v5, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->proglistId:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_1

    iget-boolean v5, v4, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->isDelete:Z

    if-nez v5, :cond_1

    iget-boolean v5, v4, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->isVisible:Z

    if-eqz v5, :cond_1

    iget-boolean v5, v4, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->isSkip:Z

    if-nez v5, :cond_1

    new-instance v3, Lcom/konka/tvsettings/popup/ChannelListActivity$ProgramFavoriteObject;

    invoke-direct {v3, p0, v8}, Lcom/konka/tvsettings/popup/ChannelListActivity$ProgramFavoriteObject;-><init>(Lcom/konka/tvsettings/popup/ChannelListActivity;Lcom/konka/tvsettings/popup/ChannelListActivity$ProgramFavoriteObject;)V

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v0, v5, :cond_5

    iget v5, v4, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    add-int/lit8 v5, v5, 0x1

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/konka/tvsettings/popup/ChannelListActivity$ProgramFavoriteObject;->setChannelId(Ljava/lang/String;)V

    :goto_3
    iget-object v5, v4, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceName:Ljava/lang/String;

    invoke-virtual {v3, v5}, Lcom/konka/tvsettings/popup/ChannelListActivity$ProgramFavoriteObject;->setChannelName(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->pfos:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v5, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->progInfoList:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, v4}, Lcom/konka/tvsettings/popup/ChannelListActivity;->addOneListViewItem(Lcom/mstar/android/tvapi/common/vo/ProgramInfo;)V

    goto :goto_1

    :cond_5
    iget v5, v4, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/konka/tvsettings/popup/ChannelListActivity$ProgramFavoriteObject;->setChannelId(Ljava/lang/String;)V

    goto :goto_3
.end method

.method private getfocusIndex()I
    .locals 5

    const/4 v1, 0x0

    iget v3, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->proglistId:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v0

    const/4 v2, 0x0

    :goto_0
    iget-object v3, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->progInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v2, v3, :cond_1

    :cond_0
    return v1

    :cond_1
    iget v4, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->progInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    iget v3, v3, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    if-ne v4, v3, :cond_2

    iget-short v4, v0, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->progInfoList:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    iget-short v3, v3, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    if-ne v4, v3, :cond_2

    move v1, v2

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private redrawComponents()V
    .locals 3

    const v1, 0x7f070156    # com.konka.tvsettings.R.id.program_favorite_title

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/popup/ChannelListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->proglistId:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    const v1, 0x7f0a0028    # com.konka.tvsettings.R.string.str_channelList_favorite

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v1, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->proglistId:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const v1, 0x7f0a0029    # com.konka.tvsettings.R.string.str_channelList_program

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f030036    # com.konka.tvsettings.R.layout.program_favorite_list

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/popup/ChannelListActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/ChannelListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "ListId"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->proglistId:I

    const v1, 0x7f070157    # com.konka.tvsettings.R.id.program_favorite_list_view

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/popup/ChannelListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->proListView:Landroid/widget/ListView;

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/ChannelListActivity;->redrawComponents()V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/ChannelListActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/konka/tvsettings/TVRootApp;

    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->cd:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/ChannelListActivity;->getListInfo()V

    new-instance v1, Lcom/konka/tvsettings/popup/ChannelListActivity$ChannelListAdapter;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->pfos:Ljava/util/ArrayList;

    invoke-direct {v1, p0, p0, v2}, Lcom/konka/tvsettings/popup/ChannelListActivity$ChannelListAdapter;-><init>(Lcom/konka/tvsettings/popup/ChannelListActivity;Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v1, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->adapter:Lcom/konka/tvsettings/popup/ChannelListActivity$ChannelListAdapter;

    new-instance v1, Lcom/konka/tvsettings/channel/ProgramEditAdapter;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->plvios:Ljava/util/ArrayList;

    invoke-direct {v1, p0, v2}, Lcom/konka/tvsettings/channel/ProgramEditAdapter;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iput-object v1, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->adapter1:Lcom/konka/tvsettings/channel/ProgramEditAdapter;

    iget-object v1, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->proListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->adapter:Lcom/konka/tvsettings/popup/ChannelListActivity$ChannelListAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->proListView:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->adapter1:Lcom/konka/tvsettings/channel/ProgramEditAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->proListView:Landroid/widget/ListView;

    invoke-virtual {v1, v3}, Landroid/widget/ListView;->setDividerHeight(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->proListView:Landroid/widget/ListView;

    invoke-direct {p0}, Lcom/konka/tvsettings/popup/ChannelListActivity;->getfocusIndex()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setSelection(I)V

    iget-object v1, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->proListView:Landroid/widget/ListView;

    new-instance v2, Lcom/konka/tvsettings/popup/ChannelListActivity$1;

    invoke-direct {v2, p0}, Lcom/konka/tvsettings/popup/ChannelListActivity$1;-><init>(Lcom/konka/tvsettings/popup/ChannelListActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    iget-boolean v1, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->bNeedRestartAlwaysTimeshift:Z

    if-eqz v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.konka.start.alwaytimeshiftthread"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/ChannelListActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Application;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :cond_0
    invoke-super {p0}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v4, 0x0

    const/4 v1, 0x1

    const/16 v2, 0x13

    if-ne p1, v2, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->proListView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->proListView:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->pfos:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setSelection(I)V

    :goto_0
    return v1

    :cond_0
    const/16 v2, 0x14

    if-ne p1, v2, :cond_1

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->proListView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v2

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->pfos:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->proListView:Landroid/widget/ListView;

    invoke-virtual {v2, v4}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_0

    :cond_1
    const/16 v2, 0x52

    if-ne p1, v2, :cond_2

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-class v2, Lcom/konka/tvsettings/MainMenuActivity;

    invoke-virtual {v0, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v2, "currentPage"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/popup/ChannelListActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/ChannelListActivity;->finish()V

    goto :goto_0

    :cond_2
    const/16 v2, 0xb2

    if-ne p1, v2, :cond_3

    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/ChannelListActivity;->finish()V

    goto :goto_0

    :cond_3
    const/16 v2, 0x101

    if-ne p1, v2, :cond_4

    iget v2, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->proglistId:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_6

    :cond_4
    const/16 v2, 0x103

    if-eq p1, v2, :cond_5

    const/16 v2, 0x22

    if-ne p1, v2, :cond_7

    :cond_5
    iget v2, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->proglistId:I

    if-ne v2, v1, :cond_7

    :cond_6
    invoke-virtual {p0}, Lcom/konka/tvsettings/popup/ChannelListActivity;->finish()V

    goto :goto_0

    :cond_7
    const/16 v2, 0x134

    if-ne p1, v2, :cond_9

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->proListView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v2

    const/16 v3, 0xa

    if-lt v2, v3, :cond_8

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->proListView:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->proListView:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v3

    add-int/lit8 v3, v3, -0xa

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_0

    :cond_8
    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->proListView:Landroid/widget/ListView;

    invoke-virtual {v2, v4}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_0

    :cond_9
    const/16 v2, 0x132

    if-ne p1, v2, :cond_b

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->proListView:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v2

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->pfos:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0xb

    if-gt v2, v3, :cond_a

    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->proListView:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->proListView:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v3

    add-int/lit8 v3, v3, 0xa

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setSelection(I)V

    goto/16 :goto_0

    :cond_a
    iget-object v2, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->proListView:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/konka/tvsettings/popup/ChannelListActivity;->pfos:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setSelection(I)V

    goto/16 :goto_0

    :cond_b
    invoke-super {p0, p1, p2}, Lcom/konka/tvsettings/teletext/MstarBaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto/16 :goto_0
.end method
