.class Lcom/konka/tvsettings/nicam/AudioLanguageActivity$AudioAdapter;
.super Landroid/widget/BaseAdapter;
.source "AudioLanguageActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/nicam/AudioLanguageActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AudioAdapter"
.end annotation


# instance fields
.field private audioLang:[Ljava/lang/String;

.field private audioModeText:[I

.field private audioTypeText:[I

.field final synthetic this$0:Lcom/konka/tvsettings/nicam/AudioLanguageActivity;


# direct methods
.method public constructor <init>(Lcom/konka/tvsettings/nicam/AudioLanguageActivity;[Ljava/lang/String;[I[I)V
    .locals 1
    .param p2    # [Ljava/lang/String;
    .param p3    # [I
    .param p4    # [I

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity$AudioAdapter;->this$0:Lcom/konka/tvsettings/nicam/AudioLanguageActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity$AudioAdapter;->audioLang:[Ljava/lang/String;

    iput-object v0, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity$AudioAdapter;->audioTypeText:[I

    iput-object v0, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity$AudioAdapter;->audioModeText:[I

    iput-object p2, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity$AudioAdapter;->audioLang:[Ljava/lang/String;

    iput-object p3, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity$AudioAdapter;->audioTypeText:[I

    iput-object p4, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity$AudioAdapter;->audioModeText:[I

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity$AudioAdapter;->this$0:Lcom/konka/tvsettings/nicam/AudioLanguageActivity;

    # getter for: Lcom/konka/tvsettings/nicam/AudioLanguageActivity;->audioLangCount:S
    invoke-static {v0}, Lcom/konka/tvsettings/nicam/AudioLanguageActivity;->access$0(Lcom/konka/tvsettings/nicam/AudioLanguageActivity;)S

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity$AudioAdapter;->audioLang:[Ljava/lang/String;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v9, 0x0

    const v8, 0x7f020088    # com.konka.tvsettings.R.drawable.mts_audio_right

    const v7, 0x7f020086    # com.konka.tvsettings.R.drawable.mts_audio_left

    if-nez p2, :cond_0

    iget-object v5, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity$AudioAdapter;->this$0:Lcom/konka/tvsettings/nicam/AudioLanguageActivity;

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Lcom/konka/tvsettings/nicam/AudioLanguageActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    const v5, 0x7f03001f    # com.konka.tvsettings.R.layout.mts_info_list_view_item

    const/4 v6, 0x0

    invoke-virtual {v3, v5, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_0
    const v5, 0x7f0700e2    # com.konka.tvsettings.R.id.mts_audio_info_type

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity$AudioAdapter;->audioTypeText:[I

    aget v5, v5, p1

    packed-switch v5, :pswitch_data_0

    :goto_0
    :pswitch_0
    const v5, 0x7f0700e4    # com.konka.tvsettings.R.id.mts_audio_info_left

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v5, 0x7f0700e5    # com.konka.tvsettings.R.id.mts_audio_info_right

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity$AudioAdapter;->audioModeText:[I

    aget v5, v5, p1

    packed-switch v5, :pswitch_data_1

    :goto_1
    const v5, 0x7f0700e1    # com.konka.tvsettings.R.id.mts_audio_info_text

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iget-object v5, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity$AudioAdapter;->audioLang:[Ljava/lang/String;

    aget-object v5, v5, p1

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object p2

    :pswitch_1
    iget-object v5, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity$AudioAdapter;->this$0:Lcom/konka/tvsettings/nicam/AudioLanguageActivity;

    invoke-virtual {v5}, Lcom/konka/tvsettings/nicam/AudioLanguageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f020087    # com.konka.tvsettings.R.drawable.mts_audio_mpeg

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :pswitch_2
    iget-object v5, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity$AudioAdapter;->this$0:Lcom/konka/tvsettings/nicam/AudioLanguageActivity;

    invoke-virtual {v5}, Lcom/konka/tvsettings/nicam/AudioLanguageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f020084    # com.konka.tvsettings.R.drawable.mts_audio_dolby

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :pswitch_3
    iget-object v5, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity$AudioAdapter;->this$0:Lcom/konka/tvsettings/nicam/AudioLanguageActivity;

    invoke-virtual {v5}, Lcom/konka/tvsettings/nicam/AudioLanguageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f020083    # com.konka.tvsettings.R.drawable.mts_audio_aac

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :pswitch_4
    iget-object v5, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity$AudioAdapter;->this$0:Lcom/konka/tvsettings/nicam/AudioLanguageActivity;

    invoke-virtual {v5}, Lcom/konka/tvsettings/nicam/AudioLanguageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f020085    # com.konka.tvsettings.R.drawable.mts_audio_dolbyp

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :pswitch_5
    iget-object v5, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity$AudioAdapter;->this$0:Lcom/konka/tvsettings/nicam/AudioLanguageActivity;

    invoke-virtual {v5}, Lcom/konka/tvsettings/nicam/AudioLanguageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v5, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity$AudioAdapter;->this$0:Lcom/konka/tvsettings/nicam/AudioLanguageActivity;

    invoke-virtual {v5}, Lcom/konka/tvsettings/nicam/AudioLanguageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    :pswitch_6
    iget-object v5, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity$AudioAdapter;->this$0:Lcom/konka/tvsettings/nicam/AudioLanguageActivity;

    invoke-virtual {v5}, Lcom/konka/tvsettings/nicam/AudioLanguageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    :pswitch_7
    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v5, p0, Lcom/konka/tvsettings/nicam/AudioLanguageActivity$AudioAdapter;->this$0:Lcom/konka/tvsettings/nicam/AudioLanguageActivity;

    invoke-virtual {v5}, Lcom/konka/tvsettings/nicam/AudioLanguageActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
