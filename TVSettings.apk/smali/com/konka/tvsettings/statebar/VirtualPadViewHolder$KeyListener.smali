.class Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$KeyListener;
.super Ljava/lang/Object;
.source "VirtualPadViewHolder.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "KeyListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;


# direct methods
.method private constructor <init>(Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$KeyListener;->this$0:Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$KeyListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$KeyListener;-><init>(Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;)V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v1, 0x1

    const-string v0, "StateColumn"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "get KeyEvent, keyCode = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sparse-switch p2, :sswitch_data_0

    :goto_0
    const/4 v0, 0x0

    :goto_1
    return v0

    :sswitch_0
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/konka/tvsettings/statebar/VirtualPadView;->getInstance(Landroid/app/Activity;)Lcom/konka/tvsettings/statebar/VirtualPadView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/tvsettings/statebar/VirtualPadView;->removeView()V

    goto :goto_0

    :sswitch_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$KeyListener;->this$0:Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;

    # getter for: Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mAct:Ljava/lang/ref/WeakReference;
    invoke-static {v0}, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->access$4(Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.konka.tvsettings.action.PROGRAM_DOWN"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    :cond_0
    move v0, v1

    goto :goto_1

    :sswitch_2
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$KeyListener;->this$0:Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;

    # getter for: Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mAct:Ljava/lang/ref/WeakReference;
    invoke-static {v0}, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->access$4(Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.konka.tvsettings.action.PROGRAM_UP"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    :cond_1
    move v0, v1

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x6f -> :sswitch_0
        0xa6 -> :sswitch_2
        0xa7 -> :sswitch_1
    .end sparse-switch
.end method
