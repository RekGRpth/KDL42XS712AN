.class Lcom/konka/tvsettings/statebar/InternetState$ClickHandler;
.super Ljava/lang/Object;
.source "InternetState.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/statebar/InternetState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ClickHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/statebar/InternetState;


# direct methods
.method private constructor <init>(Lcom/konka/tvsettings/statebar/InternetState;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/statebar/InternetState$ClickHandler;->this$0:Lcom/konka/tvsettings/statebar/InternetState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/tvsettings/statebar/InternetState;Lcom/konka/tvsettings/statebar/InternetState$ClickHandler;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/statebar/InternetState$ClickHandler;-><init>(Lcom/konka/tvsettings/statebar/InternetState;)V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return v2

    :pswitch_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/InternetState$ClickHandler;->this$0:Lcom/konka/tvsettings/statebar/InternetState;

    const/4 v1, 0x1

    # invokes: Lcom/konka/tvsettings/statebar/InternetState;->changeImg(Z)V
    invoke-static {v0, v1}, Lcom/konka/tvsettings/statebar/InternetState;->access$2(Lcom/konka/tvsettings/statebar/InternetState;Z)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/statebar/InternetState$ClickHandler;->this$0:Lcom/konka/tvsettings/statebar/InternetState;

    # invokes: Lcom/konka/tvsettings/statebar/InternetState;->changeImg(Z)V
    invoke-static {v0, v2}, Lcom/konka/tvsettings/statebar/InternetState;->access$2(Lcom/konka/tvsettings/statebar/InternetState;Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/InternetState$ClickHandler;->this$0:Lcom/konka/tvsettings/statebar/InternetState;

    # invokes: Lcom/konka/tvsettings/statebar/InternetState;->onClickNetworkStatusIndicator()V
    invoke-static {v0}, Lcom/konka/tvsettings/statebar/InternetState;->access$3(Lcom/konka/tvsettings/statebar/InternetState;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0701e9
        :pswitch_0    # com.konka.tvsettings.R.id.imgbtn_wifi
    .end packed-switch
.end method
