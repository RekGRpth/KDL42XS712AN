.class Lcom/konka/tvsettings/statebar/SeekBarButton$2;
.super Ljava/lang/Object;
.source "SeekBarButton.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/statebar/SeekBarButton;-><init>(Landroid/app/Activity;IIZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/statebar/SeekBarButton;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/statebar/SeekBarButton;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/statebar/SeekBarButton$2;->this$0:Lcom/konka/tvsettings/statebar/SeekBarButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-ne v2, v0, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/SeekBarButton$2;->this$0:Lcom/konka/tvsettings/statebar/SeekBarButton;

    invoke-virtual {v2}, Lcom/konka/tvsettings/statebar/SeekBarButton;->setFocused()V

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/SeekBarButton$2;->this$0:Lcom/konka/tvsettings/statebar/SeekBarButton;

    iget-object v2, v2, Lcom/konka/tvsettings/statebar/SeekBarButton;->seekbar:Landroid/widget/SeekBar;

    invoke-virtual {v2, v1}, Landroid/widget/SeekBar;->setVisibility(I)V

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/SeekBarButton$2;->this$0:Lcom/konka/tvsettings/statebar/SeekBarButton;

    invoke-virtual {v2}, Lcom/konka/tvsettings/statebar/SeekBarButton;->setFocused()V

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/SeekBarButton$2;->this$0:Lcom/konka/tvsettings/statebar/SeekBarButton;

    iget-object v2, v2, Lcom/konka/tvsettings/statebar/SeekBarButton;->seekbar:Landroid/widget/SeekBar;

    invoke-virtual {v2, v1}, Landroid/widget/SeekBar;->setVisibility(I)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
