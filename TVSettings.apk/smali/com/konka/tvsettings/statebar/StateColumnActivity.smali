.class public Lcom/konka/tvsettings/statebar/StateColumnActivity;
.super Landroid/app/Activity;
.source "StateColumnActivity.java"

# interfaces
.implements Lcom/konka/tvsettings/statebar/UsbState$UsbNumsUpdate;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/statebar/StateColumnActivity$ImgBtn_OnClickListeners;,
        Lcom/konka/tvsettings/statebar/StateColumnActivity$StateColumn_Listener;
    }
.end annotation


# static fields
.field private static final SOURCE_TYPE_NUM:I = 0x6

.field private static final TAG:Ljava/lang/String; = "StateColumn"

.field private static final TIME_EXIT_SECOND:I = 0x1388


# instance fields
.field private ImgBtn_History:Landroid/widget/ImageButton;

.field private ImgBtn_Homepage:Landroid/widget/ImageButton;

.field private ImgBtn_KeyPad:Landroid/widget/ImageButton;

.field private ImgBtn_Search:Landroid/widget/ImageButton;

.field private ImgBtn_Setting:Landroid/widget/ImageButton;

.field private ImgBtn_Usb:Landroid/widget/ImageButton;

.field private ImgBtn_Wifi:Landroid/widget/ImageButton;

.field private IsImgBtnHovered:Z

.field private StateColumn_Container:Landroid/widget/RelativeLayout;

.field private Tv_channel:Landroid/widget/TextView;

.field private Tv_source:Landroid/widget/TextView;

.field private Tv_usbnums:Landroid/widget/TextView;

.field private bIsExit:Z

.field private commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

.field private draglayer:Lcom/konka/tvsettings/statebar/DragLayer;

.field private eInputSource:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field private mPadView:Lcom/konka/tvsettings/statebar/VirtualPadView;

.field private mSourceStr:[Ljava/lang/String;

.field private mTimer:Ljava/util/Timer;

.field private mTimerTask:Ljava/util/TimerTask;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->mPadView:Lcom/konka/tvsettings/statebar/VirtualPadView;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->StateColumn_Container:Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->Tv_channel:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->Tv_source:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->Tv_usbnums:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->ImgBtn_Homepage:Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->ImgBtn_KeyPad:Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->ImgBtn_Setting:Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->ImgBtn_History:Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->ImgBtn_Search:Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->ImgBtn_Usb:Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->ImgBtn_Wifi:Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->draglayer:Lcom/konka/tvsettings/statebar/DragLayer;

    iput-boolean v1, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->bIsExit:Z

    iput-boolean v1, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->IsImgBtnHovered:Z

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->mSourceStr:[Ljava/lang/String;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->eInputSource:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    return-void
.end method

.method private TimerBegin()V
    .locals 4

    const-string v0, "StateColumn"

    const-string v1, "EXIT the layout"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->mTimer:Ljava/util/Timer;

    new-instance v0, Lcom/konka/tvsettings/statebar/StateColumnActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/statebar/StateColumnActivity$1;-><init>(Lcom/konka/tvsettings/statebar/StateColumnActivity;)V

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->mTimerTask:Ljava/util/TimerTask;

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->mTimer:Ljava/util/Timer;

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->mTimerTask:Ljava/util/TimerTask;

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->bIsExit:Z

    return-void
.end method

.method private TimerStop()V
    .locals 3

    const-string v1, "StateColumn"

    const-string v2, "Enter the layout"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v1, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->bIsExit:Z

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->mTimer:Ljava/util/Timer;

    invoke-virtual {v1}, Ljava/util/Timer;->cancel()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->mTimer:Ljava/util/Timer;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->mTimerTask:Ljava/util/TimerTask;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/statebar/StateColumnActivity;)Lcom/konka/tvsettings/statebar/VirtualPadView;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->mPadView:Lcom/konka/tvsettings/statebar/VirtualPadView;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/statebar/StateColumnActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->TimerStop()V

    return-void
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/statebar/StateColumnActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->IsImgBtnHovered:Z

    return v0
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/statebar/StateColumnActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->TimerBegin()V

    return-void
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/statebar/StateColumnActivity;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->IsImgBtnHovered:Z

    return-void
.end method

.method private enumToString(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Ljava/lang/String;
    .locals 2
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->eInputSource:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    const/4 v1, 0x0

    :goto_1
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->eInputSource:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aget-object v1, v1, v0

    if-ne v1, p1, :cond_1

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->mSourceStr:[Ljava/lang/String;

    aget-object v1, v1, v0

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private findview()V
    .locals 1

    invoke-static {p0}, Lcom/konka/tvsettings/statebar/VirtualPadView;->getInstance(Landroid/app/Activity;)Lcom/konka/tvsettings/statebar/VirtualPadView;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->mPadView:Lcom/konka/tvsettings/statebar/VirtualPadView;

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    const v0, 0x7f0701e6    # com.konka.tvsettings.R.id.layout_statecolumn

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->StateColumn_Container:Landroid/widget/RelativeLayout;

    const v0, 0x7f0701e7    # com.konka.tvsettings.R.id.tv_channel

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->Tv_channel:Landroid/widget/TextView;

    const v0, 0x7f0701e8    # com.konka.tvsettings.R.id.tv_inputsource

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->Tv_source:Landroid/widget/TextView;

    const v0, 0x7f0701eb    # com.konka.tvsettings.R.id.tv_usb_nums

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->Tv_usbnums:Landroid/widget/TextView;

    const v0, 0x7f0701f2    # com.konka.tvsettings.R.id.imgbtn_homepage

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->ImgBtn_Homepage:Landroid/widget/ImageButton;

    const v0, 0x7f0701f0    # com.konka.tvsettings.R.id.imgbtn_keypad

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->ImgBtn_KeyPad:Landroid/widget/ImageButton;

    const v0, 0x7f0701ef    # com.konka.tvsettings.R.id.imgbtn_setting

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->ImgBtn_Setting:Landroid/widget/ImageButton;

    const v0, 0x7f0701ee    # com.konka.tvsettings.R.id.imgbtn_history

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->ImgBtn_History:Landroid/widget/ImageButton;

    const v0, 0x7f0701ed    # com.konka.tvsettings.R.id.imgbtn_search

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->ImgBtn_Search:Landroid/widget/ImageButton;

    const v0, 0x7f0701ea    # com.konka.tvsettings.R.id.imgbtn_usb

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->ImgBtn_Usb:Landroid/widget/ImageButton;

    const v0, 0x7f0701e9    # com.konka.tvsettings.R.id.imgbtn_wifi

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->ImgBtn_Wifi:Landroid/widget/ImageButton;

    return-void
.end method

.method private getSourceInfo()V
    .locals 13

    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v6, 0x6

    new-array v0, v6, [I

    iget-object v6, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/CommonDesk;->getInputSourceInfo()Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    move-result-object v6

    iget v6, v6, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountDTV:I

    aput v6, v0, v8

    iget-object v6, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/CommonDesk;->getInputSourceInfo()Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    move-result-object v6

    iget v6, v6, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountATV:I

    aput v6, v0, v9

    iget-object v6, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/CommonDesk;->getInputSourceInfo()Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    move-result-object v6

    iget v6, v6, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountAV:I

    aput v6, v0, v10

    iget-object v6, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/CommonDesk;->getInputSourceInfo()Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    move-result-object v6

    iget v6, v6, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountYPbPr:I

    aput v6, v0, v11

    iget-object v6, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/CommonDesk;->getInputSourceInfo()Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    move-result-object v6

    iget v6, v6, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountVGA:I

    aput v6, v0, v12

    const/4 v6, 0x5

    iget-object v7, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v7}, Lcom/konka/kkinterface/tv/CommonDesk;->getInputSourceInfo()Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    move-result-object v7

    iget v7, v7, Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;->sourceCountHDMI:I

    aput v7, v0, v6

    aget v6, v0, v8

    aget v7, v0, v9

    add-int/2addr v6, v7

    aget v7, v0, v10

    add-int/2addr v6, v7

    aget v7, v0, v11

    add-int/2addr v6, v7

    aget v7, v0, v12

    add-int/2addr v6, v7

    const/4 v7, 0x5

    aget v7, v0, v7

    add-int v1, v6, v7

    invoke-virtual {p0}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b0025    # com.konka.tvsettings.R.array.inputsource_array

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x6

    new-array v2, v6, [Ljava/lang/Object;

    sget-object v6, Lcom/konka/tvsettings/ConfigurationData;->DTVInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aput-object v6, v2, v8

    sget-object v6, Lcom/konka/tvsettings/ConfigurationData;->ATVInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aput-object v6, v2, v9

    sget-object v6, Lcom/konka/tvsettings/ConfigurationData;->AVInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aput-object v6, v2, v10

    sget-object v6, Lcom/konka/tvsettings/ConfigurationData;->YPbPrInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aput-object v6, v2, v11

    sget-object v6, Lcom/konka/tvsettings/ConfigurationData;->VGAInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aput-object v6, v2, v12

    const/4 v6, 0x5

    sget-object v7, Lcom/konka/tvsettings/ConfigurationData;->HDMIInputSourceModes:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aput-object v7, v2, v6

    new-array v6, v1, [Ljava/lang/String;

    iput-object v6, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->mSourceStr:[Ljava/lang/String;

    new-array v6, v1, [Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object v6, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->eInputSource:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    const/4 v4, 0x0

    const/4 v3, 0x0

    :goto_0
    const/4 v6, 0x6

    if-lt v3, v6, :cond_0

    const/4 v3, 0x0

    :goto_1
    if-lt v3, v1, :cond_1

    return-void

    :cond_0
    aget v7, v0, v3

    aget-object v6, v2, v3

    check-cast v6, [Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v4, v7, v6}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->initSourceEnum(II[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V

    aget v6, v0, v3

    aget-object v7, v5, v3

    invoke-direct {p0, v4, v6, v7}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->initSourceStr(IILjava/lang/String;)I

    move-result v4

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    const-string v6, "StateColumn"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "SourceStr = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->mSourceStr:[Ljava/lang/String;

    aget-object v8, v8, v3

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "\nSourceEnum = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->eInputSource:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    aget-object v8, v8, v3

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method private initSourceEnum(II[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # [Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-nez p2, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->eInputSource:[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    add-int v2, p1, v0

    aget-object v3, p3, v0

    aput-object v3, v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private initSourceStr(IILjava/lang/String;)I
    .locals 9
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/lang/String;

    const/4 v8, 0x0

    const/4 v7, 0x1

    if-nez p2, :cond_0

    move v1, p1

    :goto_0
    return v1

    :cond_0
    const/4 v0, 0x0

    :goto_1
    if-lt v0, p2, :cond_1

    add-int/2addr p1, p2

    move v1, p1

    goto :goto_0

    :cond_1
    if-nez v0, :cond_3

    if-ne v7, p2, :cond_2

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->mSourceStr:[Ljava/lang/String;

    add-int v3, p1, v0

    new-array v4, v7, [Ljava/lang/Object;

    const-string v5, ""

    aput-object v5, v4, v8

    invoke-static {p3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->mSourceStr:[Ljava/lang/String;

    add-int v3, p1, v0

    new-array v4, v7, [Ljava/lang/Object;

    const-string v5, "1"

    aput-object v5, v4, v8

    invoke-static {p3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    goto :goto_2

    :cond_3
    iget-object v2, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->mSourceStr:[Ljava/lang/String;

    add-int v3, p1, v0

    new-array v4, v7, [Ljava/lang/Object;

    new-instance v5, Ljava/lang/StringBuilder;

    add-int/lit8 v6, v0, 0x1

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-static {p3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    goto :goto_2
.end method

.method private initial()V
    .locals 4

    new-instance v1, Lcom/konka/tvsettings/statebar/DragLayer;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/statebar/DragLayer;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->draglayer:Lcom/konka/tvsettings/statebar/DragLayer;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->bIsExit:Z

    invoke-direct {p0}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->getSourceInfo()V

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->Tv_source:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->enumToString(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->Tv_channel:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrentChannelNumber()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v1, v0, :cond_1

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV2:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v1, v0, :cond_2

    :cond_1
    iget-object v1, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->Tv_channel:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/ChannelDesk;->getCurrentChannelNumber()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->Tv_channel:Landroid/widget/TextView;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private setlisteners()V
    .locals 4

    const/4 v2, 0x0

    const/4 v3, 0x1

    new-instance v0, Lcom/konka/tvsettings/statebar/StateColumnActivity$ImgBtn_OnClickListeners;

    invoke-direct {v0, p0, v2}, Lcom/konka/tvsettings/statebar/StateColumnActivity$ImgBtn_OnClickListeners;-><init>(Lcom/konka/tvsettings/statebar/StateColumnActivity;Lcom/konka/tvsettings/statebar/StateColumnActivity$ImgBtn_OnClickListeners;)V

    new-instance v1, Lcom/konka/tvsettings/statebar/StateColumnActivity$StateColumn_Listener;

    invoke-direct {v1, p0, v2}, Lcom/konka/tvsettings/statebar/StateColumnActivity$StateColumn_Listener;-><init>(Lcom/konka/tvsettings/statebar/StateColumnActivity;Lcom/konka/tvsettings/statebar/StateColumnActivity$StateColumn_Listener;)V

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->ImgBtn_Homepage:Landroid/widget/ImageButton;

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->ImgBtn_KeyPad:Landroid/widget/ImageButton;

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->ImgBtn_Setting:Landroid/widget/ImageButton;

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->ImgBtn_History:Landroid/widget/ImageButton;

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->ImgBtn_Search:Landroid/widget/ImageButton;

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->ImgBtn_Usb:Landroid/widget/ImageButton;

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->ImgBtn_Wifi:Landroid/widget/ImageButton;

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->StateColumn_Container:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->ImgBtn_Homepage:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->ImgBtn_KeyPad:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->ImgBtn_Setting:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->ImgBtn_History:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->ImgBtn_Search:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->ImgBtn_Usb:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->ImgBtn_Wifi:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->StateColumn_Container:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->StateColumn_Container:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setFocusableInTouchMode(Z)V

    return-void
.end method

.method private settags()V
    .locals 4

    const v3, 0x7f070003    # com.konka.tvsettings.R.id.hint_text

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->ImgBtn_Homepage:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a012a    # com.konka.tvsettings.R.string.str_hint_text_homepage

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/ImageButton;->setTag(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->ImgBtn_KeyPad:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a012b    # com.konka.tvsettings.R.string.str_hint_text_keypad

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/ImageButton;->setTag(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->ImgBtn_Setting:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a012c    # com.konka.tvsettings.R.string.str_hint_text_setting

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/ImageButton;->setTag(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->ImgBtn_History:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a012d    # com.konka.tvsettings.R.string.str_hint_text_history

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/ImageButton;->setTag(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->ImgBtn_Search:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a012e    # com.konka.tvsettings.R.string.str_hint_text_search

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/ImageButton;->setTag(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->ImgBtn_Usb:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a012f    # com.konka.tvsettings.R.string.str_hint_text_usb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/ImageButton;->setTag(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->ImgBtn_Wifi:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0130    # com.konka.tvsettings.R.string.str_hint_text_network

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/ImageButton;->setTag(ILjava/lang/Object;)V

    return-void
.end method

.method private stringToEnum(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .locals 2
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->mSourceStr:[Ljava/lang/String;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    const/4 v1, 0x0

    :goto_1
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->mSourceStr:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    aget-object v1, v1, v0

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method SetUsbText(I)V
    .locals 3
    .param p1    # I

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->Tv_usbnums:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->Tv_usbnums:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public doUpdate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    sget-object v1, Lcom/konka/tvsettings/statebar/UsbState;->USB_NUMS:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "usb nums"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->SetUsbText(I)V

    return-void
.end method

.method public hidehint(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    :try_start_0
    iget-object v1, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->draglayer:Lcom/konka/tvsettings/statebar/DragLayer;

    invoke-virtual {v1, p1}, Lcom/konka/tvsettings/statebar/DragLayer;->hideHint(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f03004e    # com.konka.tvsettings.R.layout.statecolumn

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->setContentView(I)V

    invoke-direct {p0}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->findview()V

    invoke-direct {p0}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->settags()V

    invoke-direct {p0}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->setlisteners()V

    invoke-direct {p0}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->initial()V

    return-void
.end method

.method protected onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method protected onPause()V
    .locals 1

    invoke-static {p0}, Lcom/konka/tvsettings/statebar/UsbState;->getInstance(Landroid/app/Activity;)Lcom/konka/tvsettings/statebar/UsbState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/tvsettings/statebar/UsbState;->destroy()V

    invoke-static {p0}, Lcom/konka/tvsettings/statebar/InternetState;->getInstance(Landroid/app/Activity;)Lcom/konka/tvsettings/statebar/InternetState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/tvsettings/statebar/InternetState;->destroy()V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 0

    invoke-static {p0}, Lcom/konka/tvsettings/statebar/UsbState;->getInstance(Landroid/app/Activity;)Lcom/konka/tvsettings/statebar/UsbState;

    invoke-static {p0}, Lcom/konka/tvsettings/statebar/InternetState;->getInstance(Landroid/app/Activity;)Lcom/konka/tvsettings/statebar/InternetState;

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method

.method protected onStart()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    return-void
.end method

.method protected onStop()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method public showhint(Landroid/view/View;)V
    .locals 8
    .param p1    # Landroid/view/View;

    const v0, 0x7f070003    # com.konka.tvsettings.R.id.hint_text

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    :try_start_0
    iget-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity;->draglayer:Lcom/konka/tvsettings/statebar/DragLayer;

    const-wide/16 v3, 0x2bc

    const-wide/16 v5, 0x3e8

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Lcom/konka/tvsettings/statebar/DragLayer;->showDelayedHint(Landroid/view/View;Ljava/lang/String;JJ)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v7

    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
