.class Lcom/konka/tvsettings/statebar/DragLayer$ShowHintRunnable;
.super Ljava/lang/Object;
.source "DragLayer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/statebar/DragLayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ShowHintRunnable"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/statebar/DragLayer;


# direct methods
.method private constructor <init>(Lcom/konka/tvsettings/statebar/DragLayer;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/statebar/DragLayer$ShowHintRunnable;->this$0:Lcom/konka/tvsettings/statebar/DragLayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/tvsettings/statebar/DragLayer;Lcom/konka/tvsettings/statebar/DragLayer$ShowHintRunnable;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/statebar/DragLayer$ShowHintRunnable;-><init>(Lcom/konka/tvsettings/statebar/DragLayer;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    const/4 v6, 0x0

    iget-object v4, p0, Lcom/konka/tvsettings/statebar/DragLayer$ShowHintRunnable;->this$0:Lcom/konka/tvsettings/statebar/DragLayer;

    # getter for: Lcom/konka/tvsettings/statebar/DragLayer;->mHintContentView:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/konka/tvsettings/statebar/DragLayer;->access$0(Lcom/konka/tvsettings/statebar/DragLayer;)Landroid/widget/TextView;

    move-result-object v4

    iget-object v5, p0, Lcom/konka/tvsettings/statebar/DragLayer$ShowHintRunnable;->this$0:Lcom/konka/tvsettings/statebar/DragLayer;

    # getter for: Lcom/konka/tvsettings/statebar/DragLayer;->mHintText:Ljava/lang/String;
    invoke-static {v5}, Lcom/konka/tvsettings/statebar/DragLayer;->access$1(Lcom/konka/tvsettings/statebar/DragLayer;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {v6, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    iget-object v4, p0, Lcom/konka/tvsettings/statebar/DragLayer$ShowHintRunnable;->this$0:Lcom/konka/tvsettings/statebar/DragLayer;

    # getter for: Lcom/konka/tvsettings/statebar/DragLayer;->mHintContentView:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/konka/tvsettings/statebar/DragLayer;->access$0(Lcom/konka/tvsettings/statebar/DragLayer;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4, v2, v2}, Landroid/widget/TextView;->measure(II)V

    iget-object v4, p0, Lcom/konka/tvsettings/statebar/DragLayer$ShowHintRunnable;->this$0:Lcom/konka/tvsettings/statebar/DragLayer;

    # getter for: Lcom/konka/tvsettings/statebar/DragLayer;->mHintContentView:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/konka/tvsettings/statebar/DragLayer;->access$0(Lcom/konka/tvsettings/statebar/DragLayer;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    iget-object v4, p0, Lcom/konka/tvsettings/statebar/DragLayer$ShowHintRunnable;->this$0:Lcom/konka/tvsettings/statebar/DragLayer;

    # getter for: Lcom/konka/tvsettings/statebar/DragLayer;->mHintContentView:Landroid/widget/TextView;
    invoke-static {v4}, Lcom/konka/tvsettings/statebar/DragLayer;->access$0(Lcom/konka/tvsettings/statebar/DragLayer;)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    iget-object v4, p0, Lcom/konka/tvsettings/statebar/DragLayer$ShowHintRunnable;->this$0:Lcom/konka/tvsettings/statebar/DragLayer;

    # getter for: Lcom/konka/tvsettings/statebar/DragLayer;->mHintPopup:Landroid/widget/PopupWindow;
    invoke-static {v4}, Lcom/konka/tvsettings/statebar/DragLayer;->access$2(Lcom/konka/tvsettings/statebar/DragLayer;)Landroid/widget/PopupWindow;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/konka/tvsettings/statebar/DragLayer$ShowHintRunnable;->this$0:Lcom/konka/tvsettings/statebar/DragLayer;

    # getter for: Lcom/konka/tvsettings/statebar/DragLayer;->mHintPopup:Landroid/widget/PopupWindow;
    invoke-static {v4}, Lcom/konka/tvsettings/statebar/DragLayer;->access$2(Lcom/konka/tvsettings/statebar/DragLayer;)Landroid/widget/PopupWindow;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/widget/PopupWindow;->setWidth(I)V

    iget-object v4, p0, Lcom/konka/tvsettings/statebar/DragLayer$ShowHintRunnable;->this$0:Lcom/konka/tvsettings/statebar/DragLayer;

    # getter for: Lcom/konka/tvsettings/statebar/DragLayer;->mHintPopup:Landroid/widget/PopupWindow;
    invoke-static {v4}, Lcom/konka/tvsettings/statebar/DragLayer;->access$2(Lcom/konka/tvsettings/statebar/DragLayer;)Landroid/widget/PopupWindow;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/widget/PopupWindow;->setHeight(I)V

    :try_start_0
    iget-object v4, p0, Lcom/konka/tvsettings/statebar/DragLayer$ShowHintRunnable;->this$0:Lcom/konka/tvsettings/statebar/DragLayer;

    # getter for: Lcom/konka/tvsettings/statebar/DragLayer;->mHintPopup:Landroid/widget/PopupWindow;
    invoke-static {v4}, Lcom/konka/tvsettings/statebar/DragLayer;->access$2(Lcom/konka/tvsettings/statebar/DragLayer;)Landroid/widget/PopupWindow;

    move-result-object v4

    iget-object v5, p0, Lcom/konka/tvsettings/statebar/DragLayer$ShowHintRunnable;->this$0:Lcom/konka/tvsettings/statebar/DragLayer;

    # getter for: Lcom/konka/tvsettings/statebar/DragLayer;->mHintAnchor:Landroid/view/View;
    invoke-static {v5}, Lcom/konka/tvsettings/statebar/DragLayer;->access$3(Lcom/konka/tvsettings/statebar/DragLayer;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v4, p0, Lcom/konka/tvsettings/statebar/DragLayer$ShowHintRunnable;->this$0:Lcom/konka/tvsettings/statebar/DragLayer;

    # getter for: Lcom/konka/tvsettings/statebar/DragLayer;->mHintHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/konka/tvsettings/statebar/DragLayer;->access$4(Lcom/konka/tvsettings/statebar/DragLayer;)Landroid/os/Handler;

    move-result-object v4

    iget-object v5, p0, Lcom/konka/tvsettings/statebar/DragLayer$ShowHintRunnable;->this$0:Lcom/konka/tvsettings/statebar/DragLayer;

    # getter for: Lcom/konka/tvsettings/statebar/DragLayer;->mHideHintRunnalbe:Lcom/konka/tvsettings/statebar/DragLayer$HideHintRunnable;
    invoke-static {v5}, Lcom/konka/tvsettings/statebar/DragLayer;->access$5(Lcom/konka/tvsettings/statebar/DragLayer;)Lcom/konka/tvsettings/statebar/DragLayer$HideHintRunnable;

    move-result-object v5

    iget-object v6, p0, Lcom/konka/tvsettings/statebar/DragLayer$ShowHintRunnable;->this$0:Lcom/konka/tvsettings/statebar/DragLayer;

    # getter for: Lcom/konka/tvsettings/statebar/DragLayer;->mHintDuration:J
    invoke-static {v6}, Lcom/konka/tvsettings/statebar/DragLayer;->access$6(Lcom/konka/tvsettings/statebar/DragLayer;)J

    move-result-wide v6

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_0
    iget-object v4, p0, Lcom/konka/tvsettings/statebar/DragLayer$ShowHintRunnable;->this$0:Lcom/konka/tvsettings/statebar/DragLayer;

    # getter for: Lcom/konka/tvsettings/statebar/DragLayer;->mHintPopup:Landroid/widget/PopupWindow;
    invoke-static {v4}, Lcom/konka/tvsettings/statebar/DragLayer;->access$2(Lcom/konka/tvsettings/statebar/DragLayer;)Landroid/widget/PopupWindow;

    move-result-object v4

    iget-object v5, p0, Lcom/konka/tvsettings/statebar/DragLayer$ShowHintRunnable;->this$0:Lcom/konka/tvsettings/statebar/DragLayer;

    # getter for: Lcom/konka/tvsettings/statebar/DragLayer;->mHintAnchor:Landroid/view/View;
    invoke-static {v5}, Lcom/konka/tvsettings/statebar/DragLayer;->access$3(Lcom/konka/tvsettings/statebar/DragLayer;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v4, v5, v3, v1}, Landroid/widget/PopupWindow;->update(Landroid/view/View;II)V

    goto :goto_0
.end method
