.class Lcom/konka/tvsettings/statebar/UsbState$UsbBroadCastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "UsbState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/statebar/UsbState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UsbBroadCastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/statebar/UsbState;


# direct methods
.method private constructor <init>(Lcom/konka/tvsettings/statebar/UsbState;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/statebar/UsbState$UsbBroadCastReceiver;->this$0:Lcom/konka/tvsettings/statebar/UsbState;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/tvsettings/statebar/UsbState;Lcom/konka/tvsettings/statebar/UsbState$UsbBroadCastReceiver;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/statebar/UsbState$UsbBroadCastReceiver;-><init>(Lcom/konka/tvsettings/statebar/UsbState;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v2, 0x1

    const-string v0, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState$UsbBroadCastReceiver;->this$0:Lcom/konka/tvsettings/statebar/UsbState;

    invoke-static {v0, v2}, Lcom/konka/tvsettings/statebar/UsbState;->access$0(Lcom/konka/tvsettings/statebar/UsbState;Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState$UsbBroadCastReceiver;->this$0:Lcom/konka/tvsettings/statebar/UsbState;

    # getter for: Lcom/konka/tvsettings/statebar/UsbState;->mUsbBtn:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/konka/tvsettings/statebar/UsbState;->access$1(Lcom/konka/tvsettings/statebar/UsbState;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/UsbState$UsbBroadCastReceiver;->this$0:Lcom/konka/tvsettings/statebar/UsbState;

    # getter for: Lcom/konka/tvsettings/statebar/UsbState;->mUsbInUnSImg:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lcom/konka/tvsettings/statebar/UsbState;->access$2(Lcom/konka/tvsettings/statebar/UsbState;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState$UsbBroadCastReceiver;->this$0:Lcom/konka/tvsettings/statebar/UsbState;

    # invokes: Lcom/konka/tvsettings/statebar/UsbState;->UsbDeviceCount()I
    invoke-static {v0}, Lcom/konka/tvsettings/statebar/UsbState;->access$3(Lcom/konka/tvsettings/statebar/UsbState;)I

    return-void

    :cond_1
    const-string v0, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState$UsbBroadCastReceiver;->this$0:Lcom/konka/tvsettings/statebar/UsbState;

    # invokes: Lcom/konka/tvsettings/statebar/UsbState;->UsbDeviceCount()I
    invoke-static {v0}, Lcom/konka/tvsettings/statebar/UsbState;->access$3(Lcom/konka/tvsettings/statebar/UsbState;)I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState$UsbBroadCastReceiver;->this$0:Lcom/konka/tvsettings/statebar/UsbState;

    invoke-static {v0, v2}, Lcom/konka/tvsettings/statebar/UsbState;->access$0(Lcom/konka/tvsettings/statebar/UsbState;Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState$UsbBroadCastReceiver;->this$0:Lcom/konka/tvsettings/statebar/UsbState;

    # getter for: Lcom/konka/tvsettings/statebar/UsbState;->mUsbBtn:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/konka/tvsettings/statebar/UsbState;->access$1(Lcom/konka/tvsettings/statebar/UsbState;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/UsbState$UsbBroadCastReceiver;->this$0:Lcom/konka/tvsettings/statebar/UsbState;

    # getter for: Lcom/konka/tvsettings/statebar/UsbState;->mUsbInUnSImg:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lcom/konka/tvsettings/statebar/UsbState;->access$2(Lcom/konka/tvsettings/statebar/UsbState;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState$UsbBroadCastReceiver;->this$0:Lcom/konka/tvsettings/statebar/UsbState;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/konka/tvsettings/statebar/UsbState;->access$0(Lcom/konka/tvsettings/statebar/UsbState;Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/UsbState$UsbBroadCastReceiver;->this$0:Lcom/konka/tvsettings/statebar/UsbState;

    # getter for: Lcom/konka/tvsettings/statebar/UsbState;->mUsbBtn:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/konka/tvsettings/statebar/UsbState;->access$1(Lcom/konka/tvsettings/statebar/UsbState;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/UsbState$UsbBroadCastReceiver;->this$0:Lcom/konka/tvsettings/statebar/UsbState;

    # getter for: Lcom/konka/tvsettings/statebar/UsbState;->mUsbOutUnSImg:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lcom/konka/tvsettings/statebar/UsbState;->access$4(Lcom/konka/tvsettings/statebar/UsbState;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
