.class public Lcom/konka/tvsettings/statebar/DragLayer;
.super Landroid/widget/FrameLayout;
.source "DragLayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/statebar/DragLayer$HideHintRunnable;,
        Lcom/konka/tvsettings/statebar/DragLayer$HintContent;,
        Lcom/konka/tvsettings/statebar/DragLayer$LayoutParams;,
        Lcom/konka/tvsettings/statebar/DragLayer$ShowHintRunnable;
    }
.end annotation


# static fields
.field public static final DEFAULT_HINT_DELAY_MILLIS:J = 0x2bcL

.field public static final DEFAULT_HINT_DURATION_MILLIS:J = 0x3e8L

.field private static final RESHOWING_GAP_MILLIS:J = 0x1f4L

.field public static instance:Lcom/konka/tvsettings/statebar/DragLayer;


# instance fields
.field private mCubicEaseOutInterpolator:Landroid/animation/TimeInterpolator;

.field private mDropAnim:Landroid/animation/ValueAnimator;

.field private mDropView:Landroid/view/View;

.field private mDropViewAlpha:F

.field private mDropViewPos:[I

.field private mDropViewScale:F

.field private mFadeOutAnim:Landroid/animation/ValueAnimator;

.field private mHideHintRunnalbe:Lcom/konka/tvsettings/statebar/DragLayer$HideHintRunnable;

.field private mHintAnchor:Landroid/view/View;

.field private mHintContentView:Landroid/widget/TextView;

.field private mHintDuration:J

.field private mHintHandler:Landroid/os/Handler;

.field private mHintPopup:Landroid/widget/PopupWindow;

.field private mHintText:Ljava/lang/String;

.field private mHitRect:Landroid/graphics/Rect;

.field private mHoverPointClosesFolder:Z

.field private mLastShowHintMillis:J

.field private mQsbIndex:I

.field private mShowHintRunnable:Lcom/konka/tvsettings/statebar/DragLayer$ShowHintRunnable;

.field private mTmpXY:[I

.field private mWorkspaceIndex:I

.field private mXDown:I

.field private mYDown:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/tvsettings/statebar/DragLayer;->instance:Lcom/konka/tvsettings/statebar/DragLayer;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1    # Landroid/content/Context;

    const/4 v6, 0x2

    const/4 v5, -0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    new-array v1, v6, [I

    iput-object v1, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mTmpXY:[I

    iput-object v3, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mDropAnim:Landroid/animation/ValueAnimator;

    iput-object v3, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mFadeOutAnim:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v2, 0x3fc00000    # 1.5f

    invoke-direct {v1, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v1, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mCubicEaseOutInterpolator:Landroid/animation/TimeInterpolator;

    iput-object v3, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mDropView:Landroid/view/View;

    new-array v1, v6, [I

    iput-object v1, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mDropViewPos:[I

    iput-boolean v4, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mHoverPointClosesFolder:Z

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mHitRect:Landroid/graphics/Rect;

    iput v5, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mWorkspaceIndex:I

    iput v5, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mQsbIndex:I

    new-instance v1, Lcom/konka/tvsettings/statebar/DragLayer$ShowHintRunnable;

    invoke-direct {v1, p0, v3}, Lcom/konka/tvsettings/statebar/DragLayer$ShowHintRunnable;-><init>(Lcom/konka/tvsettings/statebar/DragLayer;Lcom/konka/tvsettings/statebar/DragLayer$ShowHintRunnable;)V

    iput-object v1, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mShowHintRunnable:Lcom/konka/tvsettings/statebar/DragLayer$ShowHintRunnable;

    new-instance v1, Lcom/konka/tvsettings/statebar/DragLayer$HideHintRunnable;

    invoke-direct {v1, p0, v3}, Lcom/konka/tvsettings/statebar/DragLayer$HideHintRunnable;-><init>(Lcom/konka/tvsettings/statebar/DragLayer;Lcom/konka/tvsettings/statebar/DragLayer$HideHintRunnable;)V

    iput-object v1, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mHideHintRunnalbe:Lcom/konka/tvsettings/statebar/DragLayer$HideHintRunnable;

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mLastShowHintMillis:J

    invoke-virtual {p0, v4}, Lcom/konka/tvsettings/statebar/DragLayer;->setMotionEventSplittingEnabled(Z)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/konka/tvsettings/statebar/DragLayer;->setChildrenDrawingOrderEnabled(Z)V

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mHintHandler:Landroid/os/Handler;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030019    # com.konka.tvsettings.R.layout.hint_popup

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mHintContentView:Landroid/widget/TextView;

    new-instance v1, Landroid/widget/PopupWindow;

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mHintContentView:Landroid/widget/TextView;

    invoke-direct {v1, v2}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;)V

    iput-object v1, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mHintPopup:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mHintPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v4}, Landroid/widget/PopupWindow;->setTouchable(Z)V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/statebar/DragLayer;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mHintContentView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/statebar/DragLayer;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mHintText:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/statebar/DragLayer;)Landroid/widget/PopupWindow;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mHintPopup:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/statebar/DragLayer;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mHintAnchor:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/statebar/DragLayer;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mHintHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/tvsettings/statebar/DragLayer;)Lcom/konka/tvsettings/statebar/DragLayer$HideHintRunnable;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mHideHintRunnalbe:Lcom/konka/tvsettings/statebar/DragLayer$HideHintRunnable;

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/tvsettings/statebar/DragLayer;)J
    .locals 2

    iget-wide v0, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mHintDuration:J

    return-wide v0
.end method


# virtual methods
.method public hideHint(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mHintHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mHideHintRunnalbe:Lcom/konka/tvsettings/statebar/DragLayer$HideHintRunnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mHintAnchor:Landroid/view/View;

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mHintHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mShowHintRunnable:Lcom/konka/tvsettings/statebar/DragLayer$ShowHintRunnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :try_start_0
    iget-object v1, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mHintPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mHintPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v1}, Landroid/widget/PopupWindow;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x0

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 10
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    invoke-virtual {p0}, Lcom/konka/tvsettings/statebar/DragLayer;->getChildCount()I

    move-result v1

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, v3}, Lcom/konka/tvsettings/statebar/DragLayer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout$LayoutParams;

    instance-of v5, v2, Lcom/konka/tvsettings/statebar/DragLayer$LayoutParams;

    if-eqz v5, :cond_1

    move-object v4, v2

    check-cast v4, Lcom/konka/tvsettings/statebar/DragLayer$LayoutParams;

    iget-boolean v5, v4, Lcom/konka/tvsettings/statebar/DragLayer$LayoutParams;->customPosition:Z

    if-eqz v5, :cond_1

    iget v5, v4, Lcom/konka/tvsettings/statebar/DragLayer$LayoutParams;->x:I

    iget v6, v4, Lcom/konka/tvsettings/statebar/DragLayer$LayoutParams;->y:I

    iget v7, v4, Lcom/konka/tvsettings/statebar/DragLayer$LayoutParams;->x:I

    iget v8, v4, Lcom/konka/tvsettings/statebar/DragLayer$LayoutParams;->width:I

    add-int/2addr v7, v8

    iget v8, v4, Lcom/konka/tvsettings/statebar/DragLayer$LayoutParams;->y:I

    iget v9, v4, Lcom/konka/tvsettings/statebar/DragLayer$LayoutParams;->height:I

    add-int/2addr v8, v9

    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/view/View;->layout(IIII)V

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public showDelayedHint(Landroid/view/View;Ljava/lang/String;JJ)V
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .param p5    # J

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mHintHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mHideHintRunnalbe:Lcom/konka/tvsettings/statebar/DragLayer$HideHintRunnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mHintHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mShowHintRunnable:Lcom/konka/tvsettings/statebar/DragLayer$ShowHintRunnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mHintAnchor:Landroid/view/View;

    if-ne v2, p1, :cond_0

    iget-wide v2, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mLastShowHintMillis:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x1f4

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    const-string v2, "DragLayer"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "current("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") and last("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mLastShowHintMillis:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") too close, not showing"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iput-wide v0, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mLastShowHintMillis:J

    iput-object p1, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mHintAnchor:Landroid/view/View;

    iput-object p2, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mHintText:Ljava/lang/String;

    iput-wide p5, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mHintDuration:J

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mHintHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/konka/tvsettings/statebar/DragLayer;->mShowHintRunnable:Lcom/konka/tvsettings/statebar/DragLayer$ShowHintRunnable;

    invoke-virtual {v2, v3, p3, p4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
