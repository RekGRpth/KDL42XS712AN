.class public Lcom/konka/tvsettings/statebar/VirtualPadView;
.super Ljava/lang/Object;
.source "VirtualPadView.java"


# static fields
.field private static instance:Lcom/konka/tvsettings/statebar/VirtualPadView;


# instance fields
.field private mAct:Landroid/app/Activity;

.field private mParams:Landroid/view/WindowManager$LayoutParams;

.field private mView:Landroid/view/View;

.field private mWm:Landroid/view/WindowManager;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 3
    .param p1    # Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/konka/tvsettings/statebar/VirtualPadView;->mAct:Landroid/app/Activity;

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadView;->mAct:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadView;->mWm:Landroid/view/WindowManager;

    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadView;->mParams:Landroid/view/WindowManager$LayoutParams;

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadView;->mParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x7d3

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadView;->mParams:Landroid/view/WindowManager$LayoutParams;

    const/4 v1, 0x1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadView;->mParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x33

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadView;->mParams:Landroid/view/WindowManager$LayoutParams;

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/VirtualPadView;->mAct:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090021    # com.konka.tvsettings.R.dimen.VirtualPadWidth

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadView;->mParams:Landroid/view/WindowManager$LayoutParams;

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/VirtualPadView;->mAct:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090022    # com.konka.tvsettings.R.dimen.VirtualPadHeight

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadView;->mParams:Landroid/view/WindowManager$LayoutParams;

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/VirtualPadView;->mAct:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090023    # com.konka.tvsettings.R.dimen.VirtualPadPaddingRight

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadView;->mParams:Landroid/view/WindowManager$LayoutParams;

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/VirtualPadView;->mAct:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090024    # com.konka.tvsettings.R.dimen.VirtualPadPaddingTop

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadView;->mParams:Landroid/view/WindowManager$LayoutParams;

    const v1, 0x1030003    # android.R.style.Animation_Translucent

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadView;->mAct:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030060    # com.konka.tvsettings.R.layout.virtual_pad_main

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadView;->mView:Landroid/view/View;

    return-void
.end method

.method public static getInstance(Landroid/app/Activity;)Lcom/konka/tvsettings/statebar/VirtualPadView;
    .locals 1
    .param p0    # Landroid/app/Activity;

    sget-object v0, Lcom/konka/tvsettings/statebar/VirtualPadView;->instance:Lcom/konka/tvsettings/statebar/VirtualPadView;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/tvsettings/statebar/VirtualPadView;

    invoke-direct {v0, p0}, Lcom/konka/tvsettings/statebar/VirtualPadView;-><init>(Landroid/app/Activity;)V

    sput-object v0, Lcom/konka/tvsettings/statebar/VirtualPadView;->instance:Lcom/konka/tvsettings/statebar/VirtualPadView;

    :cond_0
    sget-object v0, Lcom/konka/tvsettings/statebar/VirtualPadView;->instance:Lcom/konka/tvsettings/statebar/VirtualPadView;

    return-object v0
.end method


# virtual methods
.method public addView()V
    .locals 3

    new-instance v0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/VirtualPadView;->mAct:Landroid/app/Activity;

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/VirtualPadView;->mView:Landroid/view/View;

    invoke-direct {v0, v1, v2}, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;-><init>(Landroid/app/Activity;Landroid/view/View;)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadView;->mWm:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/VirtualPadView;->mView:Landroid/view/View;

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/VirtualPadView;->mParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public removeView()V
    .locals 3

    :try_start_0
    iget-object v1, p0, Lcom/konka/tvsettings/statebar/VirtualPadView;->mWm:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/VirtualPadView;->mView:Landroid/view/View;

    invoke-interface {v1, v2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
