.class Lcom/konka/tvsettings/statebar/SeekBarButton$1;
.super Ljava/lang/Object;
.source "SeekBarButton.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/statebar/SeekBarButton;-><init>(Landroid/app/Activity;IIZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/statebar/SeekBarButton;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/statebar/SeekBarButton;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/statebar/SeekBarButton$1;->this$0:Lcom/konka/tvsettings/statebar/SeekBarButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-static {}, Lcom/konka/kkinterface/tv/TranslateIrKeyDesk;->getInstance()Lcom/konka/kkinterface/tv/TranslateIrKeyDesk;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/konka/kkinterface/tv/TranslateIrKeyDesk;->translateIRKey(I)I

    move-result p2

    const/16 v2, 0x42

    if-ne p2, v2, :cond_2

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/SeekBarButton$1;->this$0:Lcom/konka/tvsettings/statebar/SeekBarButton;

    iget-object v2, v2, Lcom/konka/tvsettings/statebar/SeekBarButton;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->isSelected()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/SeekBarButton$1;->this$0:Lcom/konka/tvsettings/statebar/SeekBarButton;

    iget-object v1, v1, Lcom/konka/tvsettings/statebar/SeekBarButton;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setSelected(Z)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/konka/tvsettings/statebar/SeekBarButton$1;->this$0:Lcom/konka/tvsettings/statebar/SeekBarButton;

    iget-object v2, v2, Lcom/konka/tvsettings/statebar/SeekBarButton;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setSelected(Z)V

    goto :goto_0

    :cond_2
    const/16 v2, 0x14

    if-eq p2, v2, :cond_3

    const/16 v2, 0x13

    if-ne p2, v2, :cond_4

    :cond_3
    iget-object v2, p0, Lcom/konka/tvsettings/statebar/SeekBarButton$1;->this$0:Lcom/konka/tvsettings/statebar/SeekBarButton;

    iget-object v2, v2, Lcom/konka/tvsettings/statebar/SeekBarButton;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setSelected(Z)V

    :cond_4
    packed-switch p2, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/SeekBarButton$1;->this$0:Lcom/konka/tvsettings/statebar/SeekBarButton;

    iget-object v2, v2, Lcom/konka/tvsettings/statebar/SeekBarButton;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->isSelected()Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/SeekBarButton$1;->this$0:Lcom/konka/tvsettings/statebar/SeekBarButton;

    # getter for: Lcom/konka/tvsettings/statebar/SeekBarButton;->isSelectedDifferent:Z
    invoke-static {v2}, Lcom/konka/tvsettings/statebar/SeekBarButton;->access$0(Lcom/konka/tvsettings/statebar/SeekBarButton;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_5
    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton$1;->this$0:Lcom/konka/tvsettings/statebar/SeekBarButton;

    invoke-virtual {v0}, Lcom/konka/tvsettings/statebar/SeekBarButton;->decreaseProgress()V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton$1;->this$0:Lcom/konka/tvsettings/statebar/SeekBarButton;

    invoke-virtual {v0}, Lcom/konka/tvsettings/statebar/SeekBarButton;->doUpdate()V

    move v0, v1

    goto :goto_0

    :pswitch_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/SeekBarButton$1;->this$0:Lcom/konka/tvsettings/statebar/SeekBarButton;

    iget-object v2, v2, Lcom/konka/tvsettings/statebar/SeekBarButton;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->isSelected()Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/SeekBarButton$1;->this$0:Lcom/konka/tvsettings/statebar/SeekBarButton;

    # getter for: Lcom/konka/tvsettings/statebar/SeekBarButton;->isSelectedDifferent:Z
    invoke-static {v2}, Lcom/konka/tvsettings/statebar/SeekBarButton;->access$0(Lcom/konka/tvsettings/statebar/SeekBarButton;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_6
    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton$1;->this$0:Lcom/konka/tvsettings/statebar/SeekBarButton;

    invoke-virtual {v0}, Lcom/konka/tvsettings/statebar/SeekBarButton;->increaseProgress()V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton$1;->this$0:Lcom/konka/tvsettings/statebar/SeekBarButton;

    invoke-virtual {v0}, Lcom/konka/tvsettings/statebar/SeekBarButton;->doUpdate()V

    move v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x15
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
