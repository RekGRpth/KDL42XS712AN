.class public Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;
.super Ljava/lang/Object;
.source "VirtualPadViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$ClickListener;,
        Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$KeyListener;
    }
.end annotation


# instance fields
.field private mAct:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/app/Activity;",
            ">;"
        }
    .end annotation
.end field

.field private mAudioManager:Landroid/media/AudioManager;

.field private mChAdd:Landroid/widget/TextView;

.field private mChHint:Landroid/widget/TextView;

.field private mChNumEdit:Landroid/widget/EditText;

.field private mChSub:Landroid/widget/TextView;

.field private mClearBtn:Landroid/widget/Button;

.field private mDeletBtn:Landroid/widget/Button;

.field private mGoBtn:Landroid/widget/Button;

.field private mNums:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/Button;",
            ">;"
        }
    .end annotation
.end field

.field private mParentView:Landroid/view/View;

.field private mVolAdd:Landroid/widget/TextView;

.field private mVolHint:Landroid/widget/TextView;

.field private mVolSub:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/app/Activity;
    .param p2    # Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mAct:Ljava/lang/ref/WeakReference;

    iput-object p2, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mParentView:Landroid/view/View;

    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mAudioManager:Landroid/media/AudioManager;

    invoke-direct {p0}, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->findView()V

    invoke-direct {p0}, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->setListeners()V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mChNumEdit:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mChNumEdit:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mChNumEdit:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;I)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->checkNumOfButton(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->deletOneNum()V

    return-void
.end method

.method static synthetic access$3(Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;)Landroid/media/AudioManager;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;)Ljava/lang/ref/WeakReference;
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mAct:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method private checkNumOfButton(I)Ljava/lang/String;
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mNums:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    const-string v1, ""

    :goto_0
    return-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->getId()I

    move-result v2

    if-ne p1, v2, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mNums:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private deletOneNum()V
    .locals 4

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mChNumEdit:Landroid/widget/EditText;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mChNumEdit:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->length()I

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mChNumEdit:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-ne v2, v3, :cond_0

    const-string v0, ""

    :cond_0
    iget-object v2, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mChNumEdit:Landroid/widget/EditText;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mChNumEdit:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mChNumEdit:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->length()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setSelection(I)V

    :cond_1
    return-void
.end method

.method private findView()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mParentView:Landroid/view/View;

    const v1, 0x7f070237    # com.konka.tvsettings.R.id.virtual_pad_volume_add_text

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mVolAdd:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mParentView:Landroid/view/View;

    const v1, 0x7f070239    # com.konka.tvsettings.R.id.virtual_pad_volume_hint_text

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mVolHint:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mParentView:Landroid/view/View;

    const v1, 0x7f070238    # com.konka.tvsettings.R.id.virtual_pad_volume_sub_text

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mVolSub:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mParentView:Landroid/view/View;

    const v1, 0x7f070225    # com.konka.tvsettings.R.id.virtual_pad_channel_add_text

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mChAdd:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mParentView:Landroid/view/View;

    const v1, 0x7f070227    # com.konka.tvsettings.R.id.virtual_pad_channel_hint_text

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mChHint:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mParentView:Landroid/view/View;

    const v1, 0x7f070226    # com.konka.tvsettings.R.id.virtual_pad_channel_sub_text

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mChSub:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mParentView:Landroid/view/View;

    const v1, 0x7f070228    # com.konka.tvsettings.R.id.virtual_pad_edittext

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mChNumEdit:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mChNumEdit:Landroid/widget/EditText;

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setInputType(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mParentView:Landroid/view/View;

    const v1, 0x7f070229    # com.konka.tvsettings.R.id.virtual_pad_go_btn

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mGoBtn:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mParentView:Landroid/view/View;

    const v1, 0x7f070233    # com.konka.tvsettings.R.id.virtual_num_c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mClearBtn:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mParentView:Landroid/view/View;

    const v1, 0x7f070235    # com.konka.tvsettings.R.id.virtual_num_back

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mDeletBtn:Landroid/widget/Button;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mNums:Ljava/util/List;

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mNums:Ljava/util/List;

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mParentView:Landroid/view/View;

    const v2, 0x7f070234    # com.konka.tvsettings.R.id.virtual_num_0

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-interface {v1, v3, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mNums:Ljava/util/List;

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mParentView:Landroid/view/View;

    const v3, 0x7f07022a    # com.konka.tvsettings.R.id.virtual_num_1

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mNums:Ljava/util/List;

    const/4 v2, 0x2

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mParentView:Landroid/view/View;

    const v3, 0x7f07022b    # com.konka.tvsettings.R.id.virtual_num_2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mNums:Ljava/util/List;

    const/4 v2, 0x3

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mParentView:Landroid/view/View;

    const v3, 0x7f07022c    # com.konka.tvsettings.R.id.virtual_num_3

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mNums:Ljava/util/List;

    const/4 v2, 0x4

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mParentView:Landroid/view/View;

    const v3, 0x7f07022d    # com.konka.tvsettings.R.id.virtual_num_4

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mNums:Ljava/util/List;

    const/4 v2, 0x5

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mParentView:Landroid/view/View;

    const v3, 0x7f07022e    # com.konka.tvsettings.R.id.virtual_num_5

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mNums:Ljava/util/List;

    const/4 v2, 0x6

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mParentView:Landroid/view/View;

    const v3, 0x7f07022f    # com.konka.tvsettings.R.id.virtual_num_6

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mNums:Ljava/util/List;

    const/4 v2, 0x7

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mParentView:Landroid/view/View;

    const v3, 0x7f070230    # com.konka.tvsettings.R.id.virtual_num_7

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mNums:Ljava/util/List;

    const/16 v2, 0x8

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mParentView:Landroid/view/View;

    const v3, 0x7f070231    # com.konka.tvsettings.R.id.virtual_num_8

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mNums:Ljava/util/List;

    const/16 v2, 0x9

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mParentView:Landroid/view/View;

    const v3, 0x7f070232    # com.konka.tvsettings.R.id.virtual_num_9

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-interface {v1, v2, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mVolHint:Landroid/widget/TextView;

    const-string v1, "V\no\nl"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mChHint:Landroid/widget/TextView;

    const-string v1, "C\nH"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private setListeners()V
    .locals 5

    const/4 v3, 0x0

    new-instance v0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$ClickListener;

    invoke-direct {v0, p0, v3}, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$ClickListener;-><init>(Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$ClickListener;)V

    new-instance v1, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$KeyListener;

    invoke-direct {v1, p0, v3}, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$KeyListener;-><init>(Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;Lcom/konka/tvsettings/statebar/VirtualPadViewHolder$KeyListener;)V

    iget-object v3, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mVolAdd:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mVolAdd:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v3, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mVolSub:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mVolSub:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v3, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mChAdd:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mChAdd:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v3, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mChSub:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mChSub:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v3, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mGoBtn:Landroid/widget/Button;

    invoke-virtual {v3, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mGoBtn:Landroid/widget/Button;

    invoke-virtual {v3, v1}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v3, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mClearBtn:Landroid/widget/Button;

    invoke-virtual {v3, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mClearBtn:Landroid/widget/Button;

    invoke-virtual {v3, v1}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v3, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mDeletBtn:Landroid/widget/Button;

    invoke-virtual {v3, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mDeletBtn:Landroid/widget/Button;

    invoke-virtual {v3, v1}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v3, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mChNumEdit:Landroid/widget/EditText;

    invoke-virtual {v3, v1}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v3, p0, Lcom/konka/tvsettings/statebar/VirtualPadViewHolder;->mNums:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    return-void

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v2, v1}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    goto :goto_0
.end method
