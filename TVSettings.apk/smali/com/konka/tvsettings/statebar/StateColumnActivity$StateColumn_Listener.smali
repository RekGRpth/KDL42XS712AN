.class Lcom/konka/tvsettings/statebar/StateColumnActivity$StateColumn_Listener;
.super Ljava/lang/Object;
.source "StateColumnActivity.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/statebar/StateColumnActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "StateColumn_Listener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/statebar/StateColumnActivity;


# direct methods
.method private constructor <init>(Lcom/konka/tvsettings/statebar/StateColumnActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity$StateColumn_Listener;->this$0:Lcom/konka/tvsettings/statebar/StateColumnActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/tvsettings/statebar/StateColumnActivity;Lcom/konka/tvsettings/statebar/StateColumnActivity$StateColumn_Listener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/statebar/StateColumnActivity$StateColumn_Listener;-><init>(Lcom/konka/tvsettings/statebar/StateColumnActivity;)V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    const/4 v2, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return v2

    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity$StateColumn_Listener;->this$0:Lcom/konka/tvsettings/statebar/StateColumnActivity;

    # invokes: Lcom/konka/tvsettings/statebar/StateColumnActivity;->TimerStop()V
    invoke-static {v0}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->access$1(Lcom/konka/tvsettings/statebar/StateColumnActivity;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity$StateColumn_Listener;->this$0:Lcom/konka/tvsettings/statebar/StateColumnActivity;

    # getter for: Lcom/konka/tvsettings/statebar/StateColumnActivity;->IsImgBtnHovered:Z
    invoke-static {v0}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->access$2(Lcom/konka/tvsettings/statebar/StateColumnActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity$StateColumn_Listener;->this$0:Lcom/konka/tvsettings/statebar/StateColumnActivity;

    # invokes: Lcom/konka/tvsettings/statebar/StateColumnActivity;->TimerBegin()V
    invoke-static {v0}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->access$3(Lcom/konka/tvsettings/statebar/StateColumnActivity;)V

    goto :goto_0

    :pswitch_4
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    :pswitch_5
    const-string v0, "StateColumn"

    const-string v1, "imgbtns_hovered enter"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity$StateColumn_Listener;->this$0:Lcom/konka/tvsettings/statebar/StateColumnActivity;

    invoke-virtual {v0, p1}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->showhint(Landroid/view/View;)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity$StateColumn_Listener;->this$0:Lcom/konka/tvsettings/statebar/StateColumnActivity;

    invoke-static {v0, v2}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->access$4(Lcom/konka/tvsettings/statebar/StateColumnActivity;Z)V

    goto :goto_0

    :pswitch_6
    const-string v0, "StateColumn"

    const-string v1, "imgbtns_hovered exit"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity$StateColumn_Listener;->this$0:Lcom/konka/tvsettings/statebar/StateColumnActivity;

    invoke-virtual {v0, p1}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->hidehint(Landroid/view/View;)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity$StateColumn_Listener;->this$0:Lcom/konka/tvsettings/statebar/StateColumnActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->access$4(Lcom/konka/tvsettings/statebar/StateColumnActivity;Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0701e6
        :pswitch_1    # com.konka.tvsettings.R.id.layout_statecolumn
        :pswitch_0    # com.konka.tvsettings.R.id.tv_channel
        :pswitch_0    # com.konka.tvsettings.R.id.tv_inputsource
        :pswitch_4    # com.konka.tvsettings.R.id.imgbtn_wifi
        :pswitch_4    # com.konka.tvsettings.R.id.imgbtn_usb
        :pswitch_0    # com.konka.tvsettings.R.id.tv_usb_nums
        :pswitch_0    # com.konka.tvsettings.R.id.imgview_line1
        :pswitch_4    # com.konka.tvsettings.R.id.imgbtn_search
        :pswitch_4    # com.konka.tvsettings.R.id.imgbtn_history
        :pswitch_4    # com.konka.tvsettings.R.id.imgbtn_setting
        :pswitch_4    # com.konka.tvsettings.R.id.imgbtn_keypad
        :pswitch_0    # com.konka.tvsettings.R.id.imgview_line2
        :pswitch_4    # com.konka.tvsettings.R.id.imgbtn_homepage
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x9
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x9
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
