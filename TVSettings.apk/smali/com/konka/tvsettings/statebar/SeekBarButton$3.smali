.class Lcom/konka/tvsettings/statebar/SeekBarButton$3;
.super Ljava/lang/Object;
.source "SeekBarButton.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/tvsettings/statebar/SeekBarButton;-><init>(Landroid/app/Activity;IIZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/statebar/SeekBarButton;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/statebar/SeekBarButton;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/statebar/SeekBarButton$3;->this$0:Lcom/konka/tvsettings/statebar/SeekBarButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 0
    .param p1    # Landroid/widget/SeekBar;
    .param p2    # I
    .param p3    # Z

    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1    # Landroid/widget/SeekBar;

    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1    # Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton$3;->this$0:Lcom/konka/tvsettings/statebar/SeekBarButton;

    iget-object v0, v0, Lcom/konka/tvsettings/statebar/SeekBarButton;->textViewProgress:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton$3;->this$0:Lcom/konka/tvsettings/statebar/SeekBarButton;

    invoke-virtual {v0}, Lcom/konka/tvsettings/statebar/SeekBarButton;->doUpdate()V

    return-void
.end method
