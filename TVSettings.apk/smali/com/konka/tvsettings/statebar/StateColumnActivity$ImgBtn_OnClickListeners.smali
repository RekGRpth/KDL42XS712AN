.class Lcom/konka/tvsettings/statebar/StateColumnActivity$ImgBtn_OnClickListeners;
.super Ljava/lang/Object;
.source "StateColumnActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/statebar/StateColumnActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ImgBtn_OnClickListeners"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/tvsettings/statebar/StateColumnActivity;


# direct methods
.method private constructor <init>(Lcom/konka/tvsettings/statebar/StateColumnActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity$ImgBtn_OnClickListeners;->this$0:Lcom/konka/tvsettings/statebar/StateColumnActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/tvsettings/statebar/StateColumnActivity;Lcom/konka/tvsettings/statebar/StateColumnActivity$ImgBtn_OnClickListeners;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/tvsettings/statebar/StateColumnActivity$ImgBtn_OnClickListeners;-><init>(Lcom/konka/tvsettings/statebar/StateColumnActivity;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1    # Landroid/view/View;

    const-string v1, "imgbtn_onclicklisteners"

    const-string v2, "onclicked"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    const-string v1, "StateColumn"

    const-string v2, "start send broadcast of com.konka.action.LAUNCH_HOME"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v1, "android.intent.category.HOME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x10200000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v1, "goHomePage"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity$ImgBtn_OnClickListeners;->this$0:Lcom/konka/tvsettings/statebar/StateColumnActivity;

    invoke-virtual {v1, v0}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_2
    const-string v1, "StateColumn"

    const-string v2, "keypad answer"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity$ImgBtn_OnClickListeners;->this$0:Lcom/konka/tvsettings/statebar/StateColumnActivity;

    # getter for: Lcom/konka/tvsettings/statebar/StateColumnActivity;->mPadView:Lcom/konka/tvsettings/statebar/VirtualPadView;
    invoke-static {v1}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->access$0(Lcom/konka/tvsettings/statebar/StateColumnActivity;)Lcom/konka/tvsettings/statebar/VirtualPadView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/tvsettings/statebar/VirtualPadView;->addView()V

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity$ImgBtn_OnClickListeners;->this$0:Lcom/konka/tvsettings/statebar/StateColumnActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->finish()V

    goto :goto_0

    :pswitch_3
    const-string v1, "StateColumn"

    const-string v2, "setting answer"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity$ImgBtn_OnClickListeners;->this$0:Lcom/konka/tvsettings/statebar/StateColumnActivity;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity$ImgBtn_OnClickListeners;->this$0:Lcom/konka/tvsettings/statebar/StateColumnActivity;

    const-class v4, Lcom/konka/tvsettings/MainMenuActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v2}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity$ImgBtn_OnClickListeners;->this$0:Lcom/konka/tvsettings/statebar/StateColumnActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->finish()V

    goto :goto_0

    :pswitch_4
    const-string v1, "StateColumn"

    const-string v2, "histroy answer"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.konka.systemui.action.SHOW_RECENT_DIALOG"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity$ImgBtn_OnClickListeners;->this$0:Lcom/konka/tvsettings/statebar/StateColumnActivity;

    invoke-virtual {v1, v0}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_5
    const-string v1, "StateColumn"

    const-string v2, "search answer"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.android.quicksearchbox"

    const-string v3, "com.android.quicksearchbox.SearchActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity$ImgBtn_OnClickListeners;->this$0:Lcom/konka/tvsettings/statebar/StateColumnActivity;

    invoke-virtual {v1, v0}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/StateColumnActivity$ImgBtn_OnClickListeners;->this$0:Lcom/konka/tvsettings/statebar/StateColumnActivity;

    invoke-virtual {v1}, Lcom/konka/tvsettings/statebar/StateColumnActivity;->finish()V

    goto/16 :goto_0

    :pswitch_6
    const-string v1, "StateColumn"

    const-string v2, "usb answer"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_7
    const-string v1, "StateColumn"

    const-string v2, "wifi answer"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f0701e9
        :pswitch_7    # com.konka.tvsettings.R.id.imgbtn_wifi
        :pswitch_6    # com.konka.tvsettings.R.id.imgbtn_usb
        :pswitch_0    # com.konka.tvsettings.R.id.tv_usb_nums
        :pswitch_0    # com.konka.tvsettings.R.id.imgview_line1
        :pswitch_5    # com.konka.tvsettings.R.id.imgbtn_search
        :pswitch_4    # com.konka.tvsettings.R.id.imgbtn_history
        :pswitch_3    # com.konka.tvsettings.R.id.imgbtn_setting
        :pswitch_2    # com.konka.tvsettings.R.id.imgbtn_keypad
        :pswitch_0    # com.konka.tvsettings.R.id.imgview_line2
        :pswitch_1    # com.konka.tvsettings.R.id.imgbtn_homepage
    .end packed-switch
.end method
