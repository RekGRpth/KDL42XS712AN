.class public Lcom/konka/tvsettings/statebar/SeekBarButton;
.super Ljava/lang/Object;
.source "SeekBarButton.java"

# interfaces
.implements Lcom/konka/tvsettings/view/IUpdateSysData;


# static fields
.field private static final PROGRESS_BAR_IDX:I = 0x1

.field private static final TEXT_VIEW_NAME_IDX:I = 0x0

.field private static final TEXT_VIEW_PROGRESS_IDX:I = 0x2


# instance fields
.field container:Landroid/widget/LinearLayout;

.field private isSelectedDifferent:Z

.field seekbar:Landroid/widget/SeekBar;

.field private step:I

.field textViewName:Landroid/widget/TextView;

.field textViewProgress:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/app/Activity;IIZ)V
    .locals 4
    .param p1    # Landroid/app/Activity;
    .param p2    # I
    .param p3    # I
    .param p4    # Z

    const v3, 0x7f0700aa    # com.konka.tvsettings.R.id.linearlayout_sound_equalizer10khz

    const v2, 0x7f0700a6    # com.konka.tvsettings.R.id.linearlayout_sound_equalizer120hz

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->isSelectedDifferent:Z

    iput-boolean p4, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->isSelectedDifferent:Z

    iput p3, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->step:I

    invoke-virtual {p1, p2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->container:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->container:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->textViewName:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->container:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->seekbar:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->container:Landroid/widget/LinearLayout;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->textViewProgress:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->textViewProgress:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->seekbar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->container:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/tvsettings/statebar/SeekBarButton$1;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/statebar/SeekBarButton$1;-><init>(Lcom/konka/tvsettings/statebar/SeekBarButton;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    if-ne p2, v2, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setNextFocusUpId(I)V

    :cond_0
    if-ne p2, v3, :cond_1

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setNextFocusDownId(I)V

    :cond_1
    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->container:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/tvsettings/statebar/SeekBarButton$2;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/statebar/SeekBarButton$2;-><init>(Lcom/konka/tvsettings/statebar/SeekBarButton;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->seekbar:Landroid/widget/SeekBar;

    new-instance v1, Lcom/konka/tvsettings/statebar/SeekBarButton$3;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/statebar/SeekBarButton$3;-><init>(Lcom/konka/tvsettings/statebar/SeekBarButton;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    :cond_2
    return-void
.end method

.method public constructor <init>(Landroid/app/Dialog;II)V
    .locals 2
    .param p1    # Landroid/app/Dialog;
    .param p2    # I
    .param p3    # I

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->isSelectedDifferent:Z

    iput p3, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->step:I

    invoke-virtual {p1, p2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->container:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->textViewName:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->container:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->seekbar:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->container:Landroid/widget/LinearLayout;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->textViewProgress:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->textViewProgress:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->seekbar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->container:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/konka/tvsettings/statebar/SeekBarButton$4;

    invoke-direct {v1, p0}, Lcom/konka/tvsettings/statebar/SeekBarButton$4;-><init>(Lcom/konka/tvsettings/statebar/SeekBarButton;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/tvsettings/statebar/SeekBarButton;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->isSelectedDifferent:Z

    return v0
.end method


# virtual methods
.method protected decreaseProgress()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->seekbar:Landroid/widget/SeekBar;

    iget v1, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->step:I

    neg-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->incrementProgressBy(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->textViewProgress:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->seekbar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public doUpdate()V
    .locals 0

    return-void
.end method

.method public getProgress()S
    .locals 1

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->seekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method protected increaseProgress()V
    .locals 2

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->seekbar:Landroid/widget/SeekBar;

    iget v1, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->step:I

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->incrementProgressBy(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->textViewProgress:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->seekbar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setEnable(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    return-void
.end method

.method public setFocusable(Z)V
    .locals 3
    .param p1    # Z

    const/4 v2, -0x1

    const v1, -0x777778

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->textViewName:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->textViewProgress:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_0
    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->textViewName:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->textViewProgress:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public setFocused()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnFocusChangeListener;

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->container:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method public setProgress(S)V
    .locals 2
    .param p1    # S

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->seekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->textViewProgress:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->seekbar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setSeletedDifferent(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->isSelectedDifferent:Z

    return-void
.end method

.method public setVisibility(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->container:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/statebar/SeekBarButton;->container:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method
