.class Lcom/konka/tvsettings/TVRootApp$MonitorThread;
.super Ljava/lang/Thread;
.source "TVRootApp.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/TVRootApp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MonitorThread"
.end annotation


# static fields
.field private static final AMPLITUDE_TOLERANCE:I = 0x2

.field private static final BACKLIGHT_STEP:I = 0x1

.field private static final ENERGY_SAMPLING_NUM:I = 0xa

.field private static final ENERGY_SAMPLING_TIME:I = 0x4

.field private static final SAMPLING_DYNAMIC_NUM:I = 0x2

.field private static final SAMPLING_DYNAMIC_TOLERANCE:I = 0x1

.field private static final SLEEP_TIME_MS:I = 0x64


# instance fields
.field am:Landroid/app/ActivityManager;

.field private backlight:S

.field private energySamplingCounter:S

.field private flagSampling:[Z

.field private inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field private lumaSampling:[S

.field private lumaSamplingPointer:S

.field final synthetic this$0:Lcom/konka/tvsettings/TVRootApp;


# direct methods
.method constructor <init>(Lcom/konka/tvsettings/TVRootApp;)V
    .locals 3

    const/16 v2, 0xa

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->this$0:Lcom/konka/tvsettings/TVRootApp;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-short v1, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->backlight:S

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object v0, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    new-array v0, v2, [S

    iput-object v0, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->lumaSampling:[S

    new-array v0, v2, [Z

    iput-object v0, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->flagSampling:[Z

    iput-short v1, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->lumaSamplingPointer:S

    iput-short v1, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->energySamplingCounter:S

    const-string v0, "activity"

    invoke-virtual {p1, v0}, Lcom/konka/tvsettings/TVRootApp;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    iput-object v0, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->am:Landroid/app/ActivityManager;

    return-void
.end method

.method private EnergyControl()V
    .locals 15

    iget-short v12, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->energySamplingCounter:S

    add-int/lit8 v12, v12, 0x1

    int-to-short v12, v12

    iput-short v12, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->energySamplingCounter:S

    iget-short v12, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->energySamplingCounter:S

    const/4 v13, 0x4

    if-ge v12, v13, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v12, 0x0

    iput-short v12, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->energySamplingCounter:S

    iget-object v12, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->am:Landroid/app/ActivityManager;

    const/4 v13, 0x2

    invoke-virtual {v12, v13}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v12

    const/4 v13, 0x0

    invoke-interface {v12, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v4, v12, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v12

    const-string v13, "com.konka.avenger"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_2

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v12

    const-string v13, "com.konka.metrolauncher"

    invoke-virtual {v12, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v12

    if-eqz v12, :cond_4

    :cond_2
    const/4 v2, 0x0

    # getter for: Lcom/konka/tvsettings/TVRootApp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->access$0()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v12

    invoke-interface {v12}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v12

    invoke-interface {v12}, Lcom/konka/kkinterface/tv/SettingDesk;->getSmartEnergySaving()Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    move-result-object v12

    sget-object v13, Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;->MODE_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    if-ne v12, v13, :cond_3

    # getter for: Lcom/konka/tvsettings/TVRootApp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->access$0()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v12

    invoke-interface {v12}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v12

    invoke-interface {v12}, Lcom/konka/kkinterface/tv/PictureDesk;->GetBacklight()S

    move-result v2

    :goto_1
    iget-short v12, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->backlight:S

    if-eq v12, v2, :cond_0

    iput-short v2, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->backlight:S

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v12

    if-eqz v12, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v12

    if-eqz v12, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v12

    iget-short v13, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->backlight:S

    invoke-virtual {v12, v13}, Lcom/mstar/android/tvapi/common/PictureManager;->setBacklight(I)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v7

    invoke-virtual {v7}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_3
    const/16 v2, 0x64

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v12

    if-eqz v12, :cond_5

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v12

    if-eqz v12, :cond_5

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/mstar/android/tvapi/common/PictureManager;->getDlcAverageLuma()S
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_2

    move-result v1

    :cond_5
    :goto_2
    if-eqz v1, :cond_0

    const/4 v3, 0x0

    iget-object v12, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->lumaSampling:[S

    iget-short v13, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->lumaSamplingPointer:S

    aget-short v12, v12, v13

    if-eqz v12, :cond_a

    iget-object v12, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->lumaSampling:[S

    iget-short v13, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->lumaSamplingPointer:S

    aget-short v12, v12, v13

    sub-int/2addr v12, v1

    invoke-static {v12}, Ljava/lang/Math;->abs(I)I

    move-result v12

    const/4 v13, 0x1

    if-le v12, v13, :cond_a

    iget-object v12, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->flagSampling:[Z

    iget-short v13, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->lumaSamplingPointer:S

    const/4 v14, 0x1

    aput-boolean v14, v12, v13

    const/4 v3, 0x1

    :goto_3
    iget-short v12, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->lumaSamplingPointer:S

    add-int/lit8 v12, v12, 0x1

    int-to-short v12, v12

    iput-short v12, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->lumaSamplingPointer:S

    iget-short v12, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->lumaSamplingPointer:S

    const/16 v13, 0xa

    if-lt v12, v13, :cond_6

    const/4 v12, 0x0

    iput-short v12, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->lumaSamplingPointer:S

    :cond_6
    iget-object v12, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->lumaSampling:[S

    iget-short v13, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->lumaSamplingPointer:S

    aput-short v1, v12, v13

    const/4 v6, 0x0

    const/4 v9, 0x0

    :goto_4
    const/16 v12, 0xa

    if-lt v9, v12, :cond_b

    iget-object v12, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->lumaSampling:[S

    const/4 v13, 0x0

    aget-short v10, v12, v13

    iget-object v12, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->lumaSampling:[S

    const/4 v13, 0x0

    aget-short v11, v12, v13

    const/4 v0, 0x0

    const/4 v9, 0x1

    :goto_5
    const/16 v12, 0xa

    if-lt v9, v12, :cond_d

    if-eqz v10, :cond_7

    if-nez v11, :cond_10

    :cond_7
    const/4 v0, 0x0

    :goto_6
    const/4 v12, 0x2

    if-lt v6, v12, :cond_11

    const/4 v12, 0x2

    if-le v0, v12, :cond_11

    if-eqz v3, :cond_0

    # getter for: Lcom/konka/tvsettings/TVRootApp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->access$0()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v12

    invoke-interface {v12}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v12

    invoke-interface {v12}, Lcom/konka/kkinterface/tv/PictureDesk;->getEnergyPercent()S

    move-result v8

    const/16 v12, 0x64

    if-le v8, v12, :cond_8

    const/4 v8, 0x0

    :cond_8
    rsub-int/lit8 v12, v8, 0x64

    int-to-short v8, v12

    iget-short v12, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->backlight:S

    if-le v12, v8, :cond_0

    iget-short v12, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->backlight:S

    add-int/lit8 v12, v12, -0x1

    int-to-short v12, v12

    iput-short v12, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->backlight:S

    iget-short v12, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->backlight:S

    if-ge v12, v8, :cond_9

    iput-short v8, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->backlight:S

    :cond_9
    :try_start_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v12

    if-eqz v12, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v12

    if-eqz v12, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v12

    iget-short v13, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->backlight:S

    invoke-virtual {v12, v13}, Lcom/mstar/android/tvapi/common/PictureManager;->setBacklight(I)V
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v7

    invoke-virtual {v7}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0

    :catch_2
    move-exception v7

    invoke-virtual {v7}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_2

    :cond_a
    iget-object v12, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->flagSampling:[Z

    iget-short v13, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->lumaSamplingPointer:S

    const/4 v14, 0x0

    aput-boolean v14, v12, v13

    const/4 v3, 0x0

    goto/16 :goto_3

    :cond_b
    iget-object v12, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->flagSampling:[Z

    aget-boolean v12, v12, v9

    if-eqz v12, :cond_c

    add-int/lit8 v12, v6, 0x1

    int-to-short v6, v12

    :cond_c
    add-int/lit8 v12, v9, 0x1

    int-to-short v9, v12

    goto/16 :goto_4

    :cond_d
    iget-object v12, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->lumaSampling:[S

    aget-short v12, v12, v9

    if-eqz v12, :cond_f

    iget-object v12, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->lumaSampling:[S

    aget-short v12, v12, v9

    if-le v12, v10, :cond_e

    iget-object v12, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->lumaSampling:[S

    aget-short v10, v12, v9

    :cond_e
    iget-object v12, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->lumaSampling:[S

    aget-short v12, v12, v9

    if-ge v12, v11, :cond_f

    iget-object v12, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->lumaSampling:[S

    aget-short v11, v12, v9

    :cond_f
    add-int/lit8 v12, v9, 0x1

    int-to-short v9, v12

    goto/16 :goto_5

    :cond_10
    sub-int v12, v10, v11

    int-to-short v0, v12

    goto/16 :goto_6

    :cond_11
    if-nez v3, :cond_0

    const/16 v5, 0x64

    iget-short v12, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->backlight:S

    if-ge v12, v5, :cond_0

    iget-short v12, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->backlight:S

    add-int/lit8 v12, v12, 0x1

    int-to-short v12, v12

    iput-short v12, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->backlight:S

    iget-short v12, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->backlight:S

    if-le v12, v5, :cond_12

    iput-short v5, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->backlight:S

    :cond_12
    :try_start_3
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v12

    if-eqz v12, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v12

    if-eqz v12, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v12

    iget-short v13, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->backlight:S

    invoke-virtual {v12, v13}, Lcom/mstar/android/tvapi/common/PictureManager;->setBacklight(I)V
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    :catch_3
    move-exception v7

    invoke-virtual {v7}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0
.end method

.method private SmartEnergySavingControl()V
    .locals 6

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    # getter for: Lcom/konka/tvsettings/TVRootApp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->access$0()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    if-eq v2, v3, :cond_1

    # getter for: Lcom/konka/tvsettings/TVRootApp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->access$0()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    :try_start_0
    # getter for: Lcom/konka/tvsettings/TVRootApp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->access$0()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getDataBaseManagerInstance()Lcom/konka/kkinterface/tv/DataBaseDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk;->SyncUserSettingDB()V

    # getter for: Lcom/konka/tvsettings/TVRootApp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->access$0()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v2

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-interface {v2, v3}, Lcom/konka/kkinterface/tv/SettingDesk;->setSmartEnergySaving(Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;)Z

    # getter for: Lcom/konka/tvsettings/TVRootApp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->access$0()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/SettingDesk;->getSmartEnergySaving()Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    move-result-object v2

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;->MODE_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    if-ne v2, v3, :cond_3

    # getter for: Lcom/konka/tvsettings/TVRootApp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->access$0()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/PictureDesk;->GetBacklight()S

    move-result v2

    iput-short v2, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->backlight:S

    :goto_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    iget-short v3, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->backlight:S

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setBacklight(I)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_1
    iput-short v5, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->lumaSamplingPointer:S

    const/4 v1, 0x0

    :goto_2
    const/16 v2, 0xa

    if-lt v1, v2, :cond_4

    :cond_1
    iget-object v2, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->inputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_KTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v2, v3, :cond_5

    :cond_2
    :goto_3
    return-void

    :cond_3
    const/16 v2, 0x64

    :try_start_1
    iput-short v2, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->backlight:S
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->lumaSampling:[S

    aput-short v5, v2, v1

    add-int/lit8 v2, v1, 0x1

    int-to-short v1, v2

    goto :goto_2

    :cond_5
    # getter for: Lcom/konka/tvsettings/TVRootApp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->access$0()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->isSignalStable()Z

    move-result v2

    if-eqz v2, :cond_2

    # getter for: Lcom/konka/tvsettings/TVRootApp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {}, Lcom/konka/tvsettings/TVRootApp;->access$0()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/SettingDesk;->getSmartEnergySaving()Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    move-result-object v2

    sget-object v3, Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;->MODE_OFF:Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    if-eq v2, v3, :cond_2

    invoke-direct {p0}, Lcom/konka/tvsettings/TVRootApp$MonitorThread;->EnergyControl()V

    goto :goto_3
.end method


# virtual methods
.method public run()V
    .locals 2

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/PictureManager;->enableDlc()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method
