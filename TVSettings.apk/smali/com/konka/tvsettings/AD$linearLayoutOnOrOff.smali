.class Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;
.super Landroid/widget/LinearLayout;
.source "AD.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/AD;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "linearLayoutOnOrOff"
.end annotation


# instance fields
.field private textViewOnOrOff:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/konka/tvsettings/AD;


# direct methods
.method public constructor <init>(Lcom/konka/tvsettings/AD;Landroid/content/Context;Ljava/lang/String;)V
    .locals 5
    .param p2    # Landroid/content/Context;
    .param p3    # Ljava/lang/String;

    const/4 v2, 0x1

    const/4 v4, -0x1

    const/4 v3, -0x2

    iput-object p1, p0, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->this$0:Lcom/konka/tvsettings/AD;

    invoke-direct {p0, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v4, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->setFocusable(Z)V

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->setFocusableInTouchMode(Z)V

    invoke-virtual {p0, p0}, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, p0}, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    const v2, 0x7f02006f    # com.konka.tvsettings.R.drawable.input_menu_item_state

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->setBackgroundResource(I)V

    new-instance v2, Landroid/widget/TextView;

    invoke-direct {v2, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->textViewOnOrOff:Landroid/widget/TextView;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v4, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0xd

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v2, p0, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->textViewOnOrOff:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->textViewOnOrOff:Landroid/widget/TextView;

    invoke-virtual {v2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->textViewOnOrOff:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080002    # com.konka.tvsettings.R.color.text_normal_col

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v2, p0, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->textViewOnOrOff:Landroid/widget/TextView;

    const/16 v3, 0x11

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v2, p0, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->textViewOnOrOff:Landroid/widget/TextView;

    invoke-virtual {p0, v2}, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->this$0:Lcom/konka/tvsettings/AD;

    # getter for: Lcom/konka/tvsettings/AD;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v3}, Lcom/konka/tvsettings/AD;->access$0(Lcom/konka/tvsettings/AD;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/SoundDesk;->getADAbsoluteVolume()I

    move-result v0

    iget-object v3, p0, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->this$0:Lcom/konka/tvsettings/AD;

    # getter for: Lcom/konka/tvsettings/AD;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v3}, Lcom/konka/tvsettings/AD;->access$0(Lcom/konka/tvsettings/AD;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/SoundDesk;->getVolume()S

    move-result v2

    iget-object v3, p0, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->this$0:Lcom/konka/tvsettings/AD;

    # getter for: Lcom/konka/tvsettings/AD;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v3}, Lcom/konka/tvsettings/AD;->access$0(Lcom/konka/tvsettings/AD;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v3

    const/16 v4, 0x32

    invoke-interface {v3, v4}, Lcom/konka/kkinterface/tv/SoundDesk;->setADAbsoluteVolume(I)V

    iget-object v3, p0, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->textViewOnOrOff:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v3, "On"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->this$0:Lcom/konka/tvsettings/AD;

    # getter for: Lcom/konka/tvsettings/AD;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v3}, Lcom/konka/tvsettings/AD;->access$0(Lcom/konka/tvsettings/AD;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v3

    invoke-interface {v3, v6}, Lcom/konka/kkinterface/tv/SoundDesk;->setADEnable(Z)V

    iget-object v3, p0, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->this$0:Lcom/konka/tvsettings/AD;

    # getter for: Lcom/konka/tvsettings/AD;->On:Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;
    invoke-static {v3}, Lcom/konka/tvsettings/AD;->access$1(Lcom/konka/tvsettings/AD;)Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->setEnabled(Z)V

    iget-object v3, p0, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->this$0:Lcom/konka/tvsettings/AD;

    # getter for: Lcom/konka/tvsettings/AD;->On:Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;
    invoke-static {v3}, Lcom/konka/tvsettings/AD;->access$1(Lcom/konka/tvsettings/AD;)Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->setFocusable(Z)V

    iget-object v3, p0, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->this$0:Lcom/konka/tvsettings/AD;

    # getter for: Lcom/konka/tvsettings/AD;->Off:Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;
    invoke-static {v3}, Lcom/konka/tvsettings/AD;->access$2(Lcom/konka/tvsettings/AD;)Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->setEnabled(Z)V

    iget-object v3, p0, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->this$0:Lcom/konka/tvsettings/AD;

    # getter for: Lcom/konka/tvsettings/AD;->Off:Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;
    invoke-static {v3}, Lcom/konka/tvsettings/AD;->access$2(Lcom/konka/tvsettings/AD;)Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->setFocusable(Z)V

    iget-object v3, p0, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->this$0:Lcom/konka/tvsettings/AD;

    # getter for: Lcom/konka/tvsettings/AD;->Off:Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;
    invoke-static {v3}, Lcom/konka/tvsettings/AD;->access$2(Lcom/konka/tvsettings/AD;)Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->requestFocus()Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v3, "Off"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->this$0:Lcom/konka/tvsettings/AD;

    # getter for: Lcom/konka/tvsettings/AD;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v3}, Lcom/konka/tvsettings/AD;->access$0(Lcom/konka/tvsettings/AD;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;

    move-result-object v3

    invoke-interface {v3, v5}, Lcom/konka/kkinterface/tv/SoundDesk;->setADEnable(Z)V

    iget-object v3, p0, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->this$0:Lcom/konka/tvsettings/AD;

    # getter for: Lcom/konka/tvsettings/AD;->Off:Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;
    invoke-static {v3}, Lcom/konka/tvsettings/AD;->access$2(Lcom/konka/tvsettings/AD;)Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->setEnabled(Z)V

    iget-object v3, p0, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->this$0:Lcom/konka/tvsettings/AD;

    # getter for: Lcom/konka/tvsettings/AD;->Off:Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;
    invoke-static {v3}, Lcom/konka/tvsettings/AD;->access$2(Lcom/konka/tvsettings/AD;)Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->setFocusable(Z)V

    iget-object v3, p0, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->this$0:Lcom/konka/tvsettings/AD;

    # getter for: Lcom/konka/tvsettings/AD;->On:Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;
    invoke-static {v3}, Lcom/konka/tvsettings/AD;->access$1(Lcom/konka/tvsettings/AD;)Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->setEnabled(Z)V

    iget-object v3, p0, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->this$0:Lcom/konka/tvsettings/AD;

    # getter for: Lcom/konka/tvsettings/AD;->On:Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;
    invoke-static {v3}, Lcom/konka/tvsettings/AD;->access$1(Lcom/konka/tvsettings/AD;)Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->setFocusable(Z)V

    iget-object v3, p0, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->this$0:Lcom/konka/tvsettings/AD;

    # getter for: Lcom/konka/tvsettings/AD;->On:Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;
    invoke-static {v3}, Lcom/konka/tvsettings/AD;->access$1(Lcom/konka/tvsettings/AD;)Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;

    move-result-object v3

    invoke-virtual {v3}, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->requestFocus()Z

    goto :goto_0
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Z

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->textViewOnOrOff:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080001    # com.konka.tvsettings.R.color.text_select_col

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->textViewOnOrOff:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080002    # com.konka.tvsettings.R.color.text_normal_col

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public requestFocus(ILandroid/graphics/Rect;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/graphics/Rect;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/konka/tvsettings/AD$linearLayoutOnOrOff;->playSoundEffect(I)V

    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method
