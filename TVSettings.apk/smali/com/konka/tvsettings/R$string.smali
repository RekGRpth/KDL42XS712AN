.class public final Lcom/konka/tvsettings/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/tvsettings/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final action_settings:I = 0x7f0a01ad

.field public static final antennatype:I = 0x7f0a01b0

.field public static final app_name:I = 0x7f0a0000

.field public static final atv:I = 0x7f0a01c9

.field public static final av:I = 0x7f0a01ca

.field public static final black_externsions:I = 0x7f0a00b4

.field public static final blue_externsions:I = 0x7f0a00b5

.field public static final capture_failed:I = 0x7f0a019c

.field public static final channel_conflict:I = 0x7f0a01d2

.field public static final common_adjust:I = 0x7f0a0006

.field public static final common_exit:I = 0x7f0a0001

.field public static final common_false_off:I = 0x7f0a0005

.field public static final common_no:I = 0x7f0a0003

.field public static final common_return:I = 0x7f0a0007

.field public static final common_true_on:I = 0x7f0a0004

.field public static final common_yes:I = 0x7f0a0002

.field public static final cursor_color:I = 0x7f0a01d1

.field public static final dtmb:I = 0x7f0a01cb

.field public static final dvb:I = 0x7f0a01cc

.field public static final epg_Popwin_Eventinfo_Title_Str:I = 0x7f0a0105

.field public static final epg_Popwin_Yes_Str:I = 0x7f0a0106

.field public static final epg_popwin_evnetinfo_null_str:I = 0x7f0a0107

.field public static final hdmi1:I = 0x7f0a01cd

.field public static final hdmi2:I = 0x7f0a01ce

.field public static final hello_world:I = 0x7f0a01ae

.field public static final insert:I = 0x7f0a01c3

.field public static final mainmenu_jieping:I = 0x7f0a01be

.field public static final mainmenu_kandian:I = 0x7f0a01bd

.field public static final mainmenu_shezhi:I = 0x7f0a01c0

.field public static final mainmenu_source:I = 0x7f0a01bf

.field public static final mainmenu_weibo:I = 0x7f0a01bc

.field public static final negative_3d:I = 0x7f0a0199

.field public static final negative_4k2k:I = 0x7f0a019a

.field public static final negative_gold:I = 0x7f0a01d4

.field public static final negative_nosignal:I = 0x7f0a019b

.field public static final negative_shot:I = 0x7f0a01c1

.field public static final no_usb_device:I = 0x7f0a0120

.field public static final root_exit_alert_dialog_msg:I = 0x7f0a0012

.field public static final savefailed:I = 0x7f0a019f

.field public static final savetodisk:I = 0x7f0a01a0

.field public static final savetousb:I = 0x7f0a019e

.field public static final set_3d:I = 0x7f0a000c

.field public static final set_cicard:I = 0x7f0a000d

.field public static final set_function:I = 0x7f0a0010

.field public static final set_image:I = 0x7f0a0009

.field public static final set_program:I = 0x7f0a000a

.field public static final set_setting:I = 0x7f0a000f

.field public static final set_sound:I = 0x7f0a0008

.field public static final set_source:I = 0x7f0a000b

.field public static final set_time:I = 0x7f0a0011

.field public static final set_weibo:I = 0x7f0a000e

.field public static final share_to_network:I = 0x7f0a01a1

.field public static final shareto:I = 0x7f0a019d

.field public static final shareweibo:I = 0x7f0a01c2

.field public static final shortcut_set_3d:I = 0x7f0a0136

.field public static final shortcut_set_channel:I = 0x7f0a0132

.field public static final shortcut_set_energy:I = 0x7f0a0137

.field public static final shortcut_set_input:I = 0x7f0a0138

.field public static final shortcut_set_network:I = 0x7f0a0139

.field public static final shortcut_set_picture:I = 0x7f0a0133

.field public static final shortcut_set_sound:I = 0x7f0a0134

.field public static final shortcut_set_source:I = 0x7f0a0135

.field public static final shortcut_set_time:I = 0x7f0a013a

.field public static final shortcut_set_update:I = 0x7f0a013b

.field public static final source_des:I = 0x7f0a01c8

.field public static final srs_dccontrol:I = 0x7f0a010d

.field public static final srs_debug_close:I = 0x7f0a0110

.field public static final srs_debug_open:I = 0x7f0a010f

.field public static final srs_definitioncontrol:I = 0x7f0a010e

.field public static final srs_inputgain:I = 0x7f0a0108

.field public static final srs_speakeranalysis:I = 0x7f0a010b

.field public static final srs_speakeraudio:I = 0x7f0a010a

.field public static final srs_surrlevelcontrol:I = 0x7f0a0109

.field public static final srs_trubasscontrol:I = 0x7f0a010c

.field public static final srt_pvr_program_service_textview:I = 0x7f0a00e2

.field public static final str_3dot:I = 0x7f0a002b

.field public static final str_ad:I = 0x7f0a01af

.field public static final str_advanced_setting_language:I = 0x7f0a0157

.field public static final str_audio_language_title:I = 0x7f0a0184

.field public static final str_cec_control_menu:I = 0x7f0a0161

.field public static final str_cec_control_power:I = 0x7f0a015e

.field public static final str_cec_control_root_menu:I = 0x7f0a015f

.field public static final str_cec_control_setup_menu:I = 0x7f0a0160

.field public static final str_cha_atvmanualtuning:I = 0x7f0a0045

.field public static final str_cha_atvmanualtuning_ch:I = 0x7f0a007d

.field public static final str_cha_atvmanualtuning_channel_1:I = 0x7f0a007e

.field public static final str_cha_atvmanualtuning_channelno:I = 0x7f0a007c

.field public static final str_cha_atvmanualtuning_colorsystem:I = 0x7f0a007f

.field public static final str_cha_atvmanualtuning_colorsystem_Auto:I = 0x7f0a0080

.field public static final str_cha_atvmanualtuning_frequency:I = 0x7f0a0085

.field public static final str_cha_atvmanualtuning_frequency_427:I = 0x7f0a0086

.field public static final str_cha_atvmanualtuning_frequency_mhz:I = 0x7f0a0087

.field public static final str_cha_atvmanualtuning_soundsystem:I = 0x7f0a0081

.field public static final str_cha_atvmanualtuning_soundsystem_Auto:I = 0x7f0a0082

.field public static final str_cha_atvmanualtuning_starttuning:I = 0x7f0a0083

.field public static final str_cha_autotuning:I = 0x7f0a0043

.field public static final str_cha_autotuning_country_australia:I = 0x7f0a004d

.field public static final str_cha_autotuning_country_austria:I = 0x7f0a004e

.field public static final str_cha_autotuning_country_beligum:I = 0x7f0a004f

.field public static final str_cha_autotuning_country_bulgaral:I = 0x7f0a0050

.field public static final str_cha_autotuning_country_croatia:I = 0x7f0a0051

.field public static final str_cha_autotuning_country_czech:I = 0x7f0a0052

.field public static final str_cha_autotuning_country_denmark:I = 0x7f0a0053

.field public static final str_cha_autotuning_country_finland:I = 0x7f0a0054

.field public static final str_cha_autotuning_country_france:I = 0x7f0a0055

.field public static final str_cha_autotuning_tuningtype:I = 0x7f0a004b

.field public static final str_cha_autotuning_tuningtype_atv:I = 0x7f0a004c

.field public static final str_cha_channel:I = 0x7f0a018c

.field public static final str_cha_channeltuning:I = 0x7f0a0056

.field public static final str_cha_ciinformation:I = 0x7f0a004a

.field public static final str_cha_data:I = 0x7f0a005b

.field public static final str_cha_default_audio_lan:I = 0x7f0a01b7

.field public static final str_cha_default_audio_lan_2nd:I = 0x7f0a01b8

.field public static final str_cha_default_subtitle_lan:I = 0x7f0a01b9

.field public static final str_cha_default_subtitle_lan_2nd:I = 0x7f0a01ba

.field public static final str_cha_dtv:I = 0x7f0a0059

.field public static final str_cha_dtvautotuning_frequency:I = 0x7f0a0061

.field public static final str_cha_dtvautotuning_modulation:I = 0x7f0a0062

.field public static final str_cha_dtvautotuning_signalquality:I = 0x7f0a0065

.field public static final str_cha_dtvautotuning_signalstrength:I = 0x7f0a0064

.field public static final str_cha_dtvautotuning_symbol:I = 0x7f0a0063

.field public static final str_cha_dtvcmanualtuning_return_warning:I = 0x7f0a01b1

.field public static final str_cha_dtvmanualtuning:I = 0x7f0a0044

.field public static final str_cha_dtvmanualtuning_ch:I = 0x7f0a006c

.field public static final str_cha_dtvmanualtuning_ch_1:I = 0x7f0a006d

.field public static final str_cha_dtvmanualtuning_channelno:I = 0x7f0a006b

.field public static final str_cha_dtvmanualtuning_frequency:I = 0x7f0a0072

.field public static final str_cha_dtvmanualtuning_modulation:I = 0x7f0a0073

.field public static final str_cha_dtvmanualtuning_modulation_64qam:I = 0x7f0a0071

.field public static final str_cha_dtvmanualtuning_return_warning:I = 0x7f0a006f

.field public static final str_cha_dtvmanualtuning_searching:I = 0x7f0a0070

.field public static final str_cha_dtvmanualtuning_signalquality:I = 0x7f0a0077

.field public static final str_cha_dtvmanualtuning_signalstrength:I = 0x7f0a0076

.field public static final str_cha_dtvmanualtuning_starttuning:I = 0x7f0a0075

.field public static final str_cha_dtvmanualtuning_symbol:I = 0x7f0a0074

.field public static final str_cha_dtvmanualtuning_tuningresult_0:I = 0x7f0a0079

.field public static final str_cha_dtvmanualtuning_tuningresult_data:I = 0x7f0a007b

.field public static final str_cha_dtvmanualtuning_tuningresult_dtv:I = 0x7f0a0078

.field public static final str_cha_dtvmanualtuning_tuningresult_radio:I = 0x7f0a007a

.field public static final str_cha_dtvmanualtuning_warning:I = 0x7f0a006e

.field public static final str_cha_exittuning_info:I = 0x7f0a0066

.field public static final str_cha_exittuning_no:I = 0x7f0a006a

.field public static final str_cha_exittuning_yes:I = 0x7f0a0069

.field public static final str_cha_oadscan:I = 0x7f0a01b6

.field public static final str_cha_program:I = 0x7f0a005c

.field public static final str_cha_programedit:I = 0x7f0a0046

.field public static final str_cha_radio:I = 0x7f0a005a

.field public static final str_cha_reset_hint_info:I = 0x7f0a016d

.field public static final str_cha_skip_atvtuning_info:I = 0x7f0a0067

.field public static final str_cha_skip_dtvtuning_info:I = 0x7f0a0068

.field public static final str_cha_software_update_oad:I = 0x7f0a01b5

.field public static final str_cha_tuningprogress_ch:I = 0x7f0a005f

.field public static final str_cha_tuningprogress_dtv:I = 0x7f0a0060

.field public static final str_cha_tuningprogress_percent_0:I = 0x7f0a005d

.field public static final str_cha_tuningprogress_vhf:I = 0x7f0a005e

.field public static final str_cha_tv:I = 0x7f0a0057

.field public static final str_cha_tv_0:I = 0x7f0a0058

.field public static final str_channelList_favorite:I = 0x7f0a0028

.field public static final str_channelList_program:I = 0x7f0a0029

.field public static final str_ci_info_loading:I = 0x7f0a0102

.field public static final str_ci_info_mainmenu:I = 0x7f0a0104

.field public static final str_ci_info_nocard:I = 0x7f0a0103

.field public static final str_cimmi_hint_ci_auto_test:I = 0x7f0a01a5

.field public static final str_cimmi_hint_ci_inserted:I = 0x7f0a01a3

.field public static final str_cimmi_hint_ci_no_module:I = 0x7f0a01a7

.field public static final str_cimmi_hint_ci_passwd_hint:I = 0x7f0a01a6

.field public static final str_cimmi_hint_ci_removed:I = 0x7f0a01a4

.field public static final str_cimmi_hint_ci_try_again:I = 0x7f0a01a8

.field public static final str_cimmi_title:I = 0x7f0a01a2

.field public static final str_color_wheel_blue:I = 0x7f0a0126

.field public static final str_color_wheel_blue_hight:I = 0x7f0a0129

.field public static final str_color_wheel_green:I = 0x7f0a0125

.field public static final str_color_wheel_green_hight:I = 0x7f0a0128

.field public static final str_color_wheel_red:I = 0x7f0a0124

.field public static final str_color_wheel_red_hight:I = 0x7f0a0127

.field public static final str_del_epg_schedule_msg:I = 0x7f0a0191

.field public static final str_dtv_source_info_Subtitle:I = 0x7f0a0142

.field public static final str_dtv_source_info_age:I = 0x7f0a017b

.field public static final str_dtv_source_info_audio_format:I = 0x7f0a0140

.field public static final str_dtv_source_info_description:I = 0x7f0a0146

.field public static final str_dtv_source_info_language:I = 0x7f0a0143

.field public static final str_dtv_source_info_mheg5:I = 0x7f0a0141

.field public static final str_dtv_source_info_no_teletext:I = 0x7f0a0145

.field public static final str_dtv_source_info_program_format:I = 0x7f0a013f

.field public static final str_dtv_source_info_program_name:I = 0x7f0a013d

.field public static final str_dtv_source_info_program_period:I = 0x7f0a013c

.field public static final str_dtv_source_info_program_type:I = 0x7f0a013e

.field public static final str_dtv_source_info_resolution:I = 0x7f0a0147

.field public static final str_dtv_source_info_teletext:I = 0x7f0a0144

.field public static final str_editText1:I = 0x7f0a00f2

.field public static final str_function_DemoMode:I = 0x7f0a01b3

.field public static final str_function_StickerDemo:I = 0x7f0a01b2

.field public static final str_function_cec:I = 0x7f0a01b4

.field public static final str_function_cec_autopoweroff:I = 0x7f0a015c

.field public static final str_function_cec_autostandby:I = 0x7f0a015b

.field public static final str_function_cec_control:I = 0x7f0a015d

.field public static final str_function_cec_devicelist:I = 0x7f0a015a

.field public static final str_function_cec_function:I = 0x7f0a0159

.field public static final str_function_cec_no_device:I = 0x7f0a01bb

.field public static final str_function_cec_setting:I = 0x7f0a0158

.field public static final str_function_cec_title:I = 0x7f0a0154

.field public static final str_function_hearingimpaired:I = 0x7f0a016f

.field public static final str_function_lock:I = 0x7f0a0163

.field public static final str_function_mhlautoswitch:I = 0x7f0a0167

.field public static final str_function_pri_audio_lang:I = 0x7f0a017c

.field public static final str_function_pri_subtitle_lang:I = 0x7f0a017e

.field public static final str_function_rgb_range:I = 0x7f0a0165

.field public static final str_function_screensaver:I = 0x7f0a0164

.field public static final str_function_sec_audio_lang:I = 0x7f0a017d

.field public static final str_function_sec_subtitle_lang:I = 0x7f0a017f

.field public static final str_function_setting_ARC:I = 0x7f0a016c

.field public static final str_function_setting_Always_Time_Shift:I = 0x7f0a016b

.field public static final str_function_setting_audio_only:I = 0x7f0a016a

.field public static final str_function_setting_game_mode:I = 0x7f0a0169

.field public static final str_function_setting_language:I = 0x7f0a0156

.field public static final str_function_setting_osdsetting:I = 0x7f0a0155

.field public static final str_function_setting_reset:I = 0x7f0a0168

.field public static final str_function_setting_time:I = 0x7f0a0162

.field public static final str_function_ssu:I = 0x7f0a0166

.field public static final str_function_ttxlanguage:I = 0x7f0a016e

.field public static final str_hint_text_history:I = 0x7f0a012d

.field public static final str_hint_text_homepage:I = 0x7f0a012a

.field public static final str_hint_text_keypad:I = 0x7f0a012b

.field public static final str_hint_text_network:I = 0x7f0a0130

.field public static final str_hint_text_network_wifi:I = 0x7f0a0131

.field public static final str_hint_text_search:I = 0x7f0a012e

.field public static final str_hint_text_setting:I = 0x7f0a012c

.field public static final str_hint_text_usb:I = 0x7f0a012f

.field public static final str_hk_picture_menu_dynamic:I = 0x7f0a01d5

.field public static final str_hk_picture_menu_normal:I = 0x7f0a01d6

.field public static final str_hk_picture_menu_soft:I = 0x7f0a01d7

.field public static final str_hk_picture_menu_user:I = 0x7f0a01d9

.field public static final str_hk_picture_menu_vivid:I = 0x7f0a01d8

.field public static final str_hk_sound_menu_movie:I = 0x7f0a01dc

.field public static final str_hk_sound_menu_music:I = 0x7f0a01db

.field public static final str_hk_sound_menu_news:I = 0x7f0a01dd

.field public static final str_hk_sound_menu_normal:I = 0x7f0a01da

.field public static final str_hk_sound_menu_sport:I = 0x7f0a01de

.field public static final str_hk_zoom_menu_16x9:I = 0x7f0a01df

.field public static final str_hk_zoom_menu_4x3:I = 0x7f0a01e0

.field public static final str_hk_zoom_menu_caption:I = 0x7f0a01e4

.field public static final str_hk_zoom_menu_dotbydot:I = 0x7f0a01e5

.field public static final str_hk_zoom_menu_intell:I = 0x7f0a01e1

.field public static final str_hk_zoom_menu_movie:I = 0x7f0a01e3

.field public static final str_hk_zoom_menu_panorama:I = 0x7f0a01e2

.field public static final str_installation_guide:I = 0x7f0a0195

.field public static final str_installation_guide_country:I = 0x7f0a0196

.field public static final str_item_bootswitch:I = 0x7f0a014f

.field public static final str_item_haltswitch:I = 0x7f0a014d

.field public static final str_language_back:I = 0x7f0a0023

.field public static final str_lock_blockprogram:I = 0x7f0a0172

.field public static final str_lock_childlock:I = 0x7f0a0197

.field public static final str_lock_locksystem:I = 0x7f0a0170

.field public static final str_lock_parentalguidance:I = 0x7f0a0173

.field public static final str_lock_setpassword:I = 0x7f0a0171

.field public static final str_mainmenu_menu:I = 0x7f0a002f

.field public static final str_null_text:I = 0x7f0a0178

.field public static final str_password_does_not_match:I = 0x7f0a017a

.field public static final str_pic_setting_adv:I = 0x7f0a00b2

.field public static final str_pic_setting_auto_adjust:I = 0x7f0a00aa

.field public static final str_pic_setting_backlight:I = 0x7f0a00af

.field public static final str_pic_setting_brightness:I = 0x7f0a00a5

.field public static final str_pic_setting_clearness:I = 0x7f0a00a8

.field public static final str_pic_setting_color_temp:I = 0x7f0a00a2

.field public static final str_pic_setting_color_wheel:I = 0x7f0a00b1

.field public static final str_pic_setting_contrast:I = 0x7f0a00a6

.field public static final str_pic_setting_detail_enhancer:I = 0x7f0a00b6

.field public static final str_pic_setting_dynamic_backlight:I = 0x7f0a01c7

.field public static final str_pic_setting_dynamic_color:I = 0x7f0a00b7

.field public static final str_pic_setting_dynamic_denoise:I = 0x7f0a00a4

.field public static final str_pic_setting_horizontal_position:I = 0x7f0a00ab

.field public static final str_pic_setting_hue:I = 0x7f0a00a9

.field public static final str_pic_setting_mfc:I = 0x7f0a01c6

.field public static final str_pic_setting_osd_mod:I = 0x7f0a00a1

.field public static final str_pic_setting_pc_adjust:I = 0x7f0a00a3

.field public static final str_pic_setting_phase:I = 0x7f0a00ae

.field public static final str_pic_setting_pic_mod:I = 0x7f0a00a0

.field public static final str_pic_setting_rate:I = 0x7f0a00ad

.field public static final str_pic_setting_saturation:I = 0x7f0a00a7

.field public static final str_pic_setting_skin_adjust:I = 0x7f0a00b3

.field public static final str_pic_setting_vertical_position:I = 0x7f0a00ac

.field public static final str_player_backward:I = 0x7f0a00ff

.field public static final str_player_capture:I = 0x7f0a0100

.field public static final str_player_ff:I = 0x7f0a00fb

.field public static final str_player_forward:I = 0x7f0a00fe

.field public static final str_player_pause:I = 0x7f0a00f9

.field public static final str_player_play:I = 0x7f0a00f7

.field public static final str_player_recorder:I = 0x7f0a00f6

.field public static final str_player_rev:I = 0x7f0a00fa

.field public static final str_player_slow:I = 0x7f0a00fc

.field public static final str_player_stop:I = 0x7f0a00f8

.field public static final str_player_time:I = 0x7f0a00fd

.field public static final str_popup_confirm_password:I = 0x7f0a0177

.field public static final str_popup_countdown_menu_content:I = 0x7f0a00d2

.field public static final str_popup_enter_password:I = 0x7f0a0174

.field public static final str_popup_input_menu_atv:I = 0x7f0a00ca

.field public static final str_popup_input_menu_av:I = 0x7f0a00cb

.field public static final str_popup_input_menu_dtv:I = 0x7f0a00c9

.field public static final str_popup_input_menu_hdmi:I = 0x7f0a00cc

.field public static final str_popup_input_menu_usb:I = 0x7f0a00cf

.field public static final str_popup_input_menu_vga:I = 0x7f0a00ce

.field public static final str_popup_input_menu_ypbpr:I = 0x7f0a00cd

.field public static final str_popup_intell_control_menu_brightness:I = 0x7f0a00d4

.field public static final str_popup_intell_control_menu_contrast:I = 0x7f0a00d5

.field public static final str_popup_intell_control_menu_economization:I = 0x7f0a00d3

.field public static final str_popup_new_password:I = 0x7f0a0176

.field public static final str_popup_no_signal_prompt_menu:I = 0x7f0a00d6

.field public static final str_popup_old_password:I = 0x7f0a0175

.field public static final str_popup_program_menu_list:I = 0x7f0a00d0

.field public static final str_popup_program_menu_warm:I = 0x7f0a00d1

.field public static final str_program_atvmanualsearch:I = 0x7f0a0018

.field public static final str_program_autosearch:I = 0x7f0a0017

.field public static final str_program_channelno:I = 0x7f0a0030

.field public static final str_program_curchannel:I = 0x7f0a002c

.field public static final str_program_dtvcity:I = 0x7f0a0040

.field public static final str_program_dtvmanualsearch:I = 0x7f0a0019

.field public static final str_program_edit:I = 0x7f0a001c

.field public static final str_program_edit_del:I = 0x7f0a0020

.field public static final str_program_edit_dialog_cancel:I = 0x7f0a0027

.field public static final str_program_edit_dialog_input:I = 0x7f0a0025

.field public static final str_program_edit_dialog_ok:I = 0x7f0a0026

.field public static final str_program_edit_edit:I = 0x7f0a0021

.field public static final str_program_edit_fav:I = 0x7f0a001f

.field public static final str_program_edit_lock:I = 0x7f0a0022

.field public static final str_program_edit_move:I = 0x7f0a001e

.field public static final str_program_edit_skip:I = 0x7f0a001d

.field public static final str_program_epg:I = 0x7f0a001b

.field public static final str_program_exit_autosearching:I = 0x7f0a003a

.field public static final str_program_exit_manualsearching:I = 0x7f0a0039

.field public static final str_program_fine_tuning:I = 0x7f0a0084

.field public static final str_program_lcnarrange:I = 0x7f0a001a

.field public static final str_program_lock:I = 0x7f0a0024

.field public static final str_program_modulationmode:I = 0x7f0a018b

.field public static final str_program_netgrid_frequency:I = 0x7f0a0041

.field public static final str_program_search_hint:I = 0x7f0a0035

.field public static final str_program_search_setting:I = 0x7f0a003e

.field public static final str_program_searching_broadcast:I = 0x7f0a003b

.field public static final str_program_searching_colorformat:I = 0x7f0a002d

.field public static final str_program_searching_curFrequency:I = 0x7f0a0032

.field public static final str_program_searching_curProgress:I = 0x7f0a0033

.field public static final str_program_searching_hint:I = 0x7f0a0034

.field public static final str_program_searching_result:I = 0x7f0a0036

.field public static final str_program_searching_result_counts:I = 0x7f0a0037

.field public static final str_program_searching_search:I = 0x7f0a0038

.field public static final str_program_searching_signalquality:I = 0x7f0a003d

.field public static final str_program_searching_signalstrength:I = 0x7f0a003c

.field public static final str_program_searching_soundformat:I = 0x7f0a002e

.field public static final str_program_searching_tv:I = 0x7f0a01c4

.field public static final str_program_searchtype:I = 0x7f0a003f

.field public static final str_program_signal_info:I = 0x7f0a018d

.field public static final str_program_symbol_frequency:I = 0x7f0a0042

.field public static final str_program_warning_notsupport_funciton:I = 0x7f0a0047

.field public static final str_pvr_channel_textview:I = 0x7f0a00e1

.field public static final str_pvr_exit_confirm:I = 0x7f0a00f0

.field public static final str_pvr_file_system_always_context:I = 0x7f0a00df

.field public static final str_pvr_file_system_always_timeshift:I = 0x7f0a00de

.field public static final str_pvr_file_system_format_context:I = 0x7f0a00db

.field public static final str_pvr_file_system_format_start:I = 0x7f0a00da

.field public static final str_pvr_file_system_menu:I = 0x7f0a0101

.field public static final str_pvr_file_system_select_disk:I = 0x7f0a00d8

.field public static final str_pvr_file_system_speed_check:I = 0x7f0a00dc

.field public static final str_pvr_file_system_speed_context:I = 0x7f0a00dd

.field public static final str_pvr_file_system_time_shift_size:I = 0x7f0a00d9

.field public static final str_pvr_file_system_title:I = 0x7f0a00d7

.field public static final str_pvr_format_usb:I = 0x7f0a00ec

.field public static final str_pvr_insert_usb:I = 0x7f0a00eb

.field public static final str_pvr_is_playbacking:I = 0x7f0a0190

.field public static final str_pvr_is_recording:I = 0x7f0a00f1

.field public static final str_pvr_is_time_shift:I = 0x7f0a018e

.field public static final str_pvr_lcn_data:I = 0x7f0a00e9

.field public static final str_pvr_lcn_record_data:I = 0x7f0a00e8

.field public static final str_pvr_lcn_textview:I = 0x7f0a00e0

.field public static final str_pvr_lcn_title:I = 0x7f0a00e7

.field public static final str_pvr_listview_item:I = 0x7f0a00ea

.field public static final str_pvr_no_record_file:I = 0x7f0a018f

.field public static final str_pvr_program_saving:I = 0x7f0a00ef

.field public static final str_pvr_program_service_tips_blue:I = 0x7f0a00e5

.field public static final str_pvr_program_service_tips_green:I = 0x7f0a00e4

.field public static final str_pvr_program_service_tips_red:I = 0x7f0a00e3

.field public static final str_pvr_program_service_tips_yellow:I = 0x7f0a00e6

.field public static final str_pvr_standby_recording_dialog_message:I = 0x7f0a01aa

.field public static final str_pvr_standby_recording_dialog_standby:I = 0x7f0a01a9

.field public static final str_pvr_standby_timeshift_dialog_message:I = 0x7f0a01ab

.field public static final str_pvr_unsurpt_flsystem:I = 0x7f0a018a

.field public static final str_pvr_usb_format_progressing:I = 0x7f0a00ee

.field public static final str_pvr_usb_speed_test_progressing:I = 0x7f0a00ed

.field public static final str_root_alert_dialog_cancel:I = 0x7f0a0183

.field public static final str_root_alert_dialog_confirm:I = 0x7f0a0182

.field public static final str_root_alert_dialog_message:I = 0x7f0a0181

.field public static final str_root_alert_dialog_title:I = 0x7f0a0180

.field public static final str_s3d_auto_wait:I = 0x7f0a00c7

.field public static final str_screensaver_nosignal:I = 0x7f0a0121

.field public static final str_screensaver_vgaupsupport:I = 0x7f0a0122

.field public static final str_setting_pvrfs:I = 0x7f0a002a

.field public static final str_showradio_audioonly:I = 0x7f0a0123

.field public static final str_sound_ADVolume:I = 0x7f0a009f

.field public static final str_sound_audio_choice:I = 0x7f0a009d

.field public static final str_sound_audio_language:I = 0x7f0a009b

.field public static final str_sound_audiolangdefault:I = 0x7f0a009c

.field public static final str_sound_balance:I = 0x7f0a0091

.field public static final str_sound_equalizer:I = 0x7f0a0095

.field public static final str_sound_equalizer10khz:I = 0x7f0a009a

.field public static final str_sound_equalizer120hz:I = 0x7f0a0096

.field public static final str_sound_equalizer1_5khz:I = 0x7f0a0098

.field public static final str_sound_equalizer500hz:I = 0x7f0a0097

.field public static final str_sound_equalizer5khz:I = 0x7f0a0099

.field public static final str_sound_hdmi:I = 0x7f0a009e

.field public static final str_sound_mod_tip_fbd:I = 0x7f0a0089

.field public static final str_sound_soundmode:I = 0x7f0a0088

.field public static final str_sound_spdifoutput:I = 0x7f0a0094

.field public static final str_sound_srs:I = 0x7f0a008a

.field public static final str_sound_srs_definition:I = 0x7f0a008f

.field public static final str_sound_srs_dynamicclarity:I = 0x7f0a008e

.field public static final str_sound_srs_title:I = 0x7f0a008b

.field public static final str_sound_srs_truebass:I = 0x7f0a008d

.field public static final str_sound_srs_tshd:I = 0x7f0a008c

.field public static final str_sound_srsdots:I = 0x7f0a0090

.field public static final str_sound_surround:I = 0x7f0a0092

.field public static final str_sound_voice_ctrl:I = 0x7f0a0093

.field public static final str_stop_record_dialog_cancel:I = 0x7f0a0189

.field public static final str_stop_record_dialog_confirm:I = 0x7f0a0188

.field public static final str_stop_record_dialog_message:I = 0x7f0a0187

.field public static final str_stop_record_dialog_title:I = 0x7f0a0186

.field public static final str_subtitle_language:I = 0x7f0a01c5

.field public static final str_subtitle_language_title:I = 0x7f0a0185

.field public static final str_sys_language:I = 0x7f0a00c8

.field public static final str_textview_record_chaneel_name:I = 0x7f0a0031

.field public static final str_time_clock:I = 0x7f0a0148

.field public static final str_time_date:I = 0x7f0a0149

.field public static final str_time_datesetting:I = 0x7f0a014b

.field public static final str_time_hour:I = 0x7f0a00f3

.field public static final str_time_minute:I = 0x7f0a00f4

.field public static final str_time_offtime:I = 0x7f0a014e

.field public static final str_time_ontime:I = 0x7f0a0150

.field public static final str_time_second:I = 0x7f0a00f5

.field public static final str_time_setting_mode:I = 0x7f0a0192

.field public static final str_time_sleeptimer:I = 0x7f0a0152

.field public static final str_time_source:I = 0x7f0a0151

.field public static final str_time_time:I = 0x7f0a014a

.field public static final str_time_timesetting:I = 0x7f0a014c

.field public static final str_time_timezone:I = 0x7f0a0153

.field public static final str_v3d_2dto3d:I = 0x7f0a00bc

.field public static final str_v3d_3d_off:I = 0x7f0a00ba

.field public static final str_v3d_3dto2d:I = 0x7f0a00bb

.field public static final str_v3d_auto:I = 0x7f0a00be

.field public static final str_v3d_clarity:I = 0x7f0a00bd

.field public static final str_v3d_clarity_intelligence:I = 0x7f0a00c6

.field public static final str_v3d_effect:I = 0x7f0a00c0

.field public static final str_v3d_engine:I = 0x7f0a00bf

.field public static final str_v3d_improve_pic_quality:I = 0x7f0a00c4

.field public static final str_v3d_motion_intelligence:I = 0x7f0a00c5

.field public static final str_v3d_pos_left_right:I = 0x7f0a00b8

.field public static final str_v3d_pos_up_down:I = 0x7f0a00b9

.field public static final str_v3d_sequence:I = 0x7f0a00c3

.field public static final str_v3d_usreffect_depth:I = 0x7f0a00c1

.field public static final str_v3d_usreffect_viewpoint:I = 0x7f0a00c2

.field public static final str_warning_underconstruction:I = 0x7f0a0013

.field public static final str_wrong_password:I = 0x7f0a0179

.field public static final str_zoom_warm:I = 0x7f0a00b0

.field public static final title_activity_ad:I = 0x7f0a01ac

.field public static final vga:I = 0x7f0a01cf

.field public static final virtual_pad_channel:I = 0x7f0a0112

.field public static final virtual_pad_go:I = 0x7f0a0113

.field public static final virtual_pad_num_0:I = 0x7f0a0114

.field public static final virtual_pad_num_1:I = 0x7f0a0115

.field public static final virtual_pad_num_2:I = 0x7f0a0116

.field public static final virtual_pad_num_3:I = 0x7f0a0117

.field public static final virtual_pad_num_4:I = 0x7f0a0118

.field public static final virtual_pad_num_5:I = 0x7f0a0119

.field public static final virtual_pad_num_6:I = 0x7f0a011a

.field public static final virtual_pad_num_7:I = 0x7f0a011b

.field public static final virtual_pad_num_8:I = 0x7f0a011c

.field public static final virtual_pad_num_9:I = 0x7f0a011d

.field public static final virtual_pad_num_C:I = 0x7f0a011e

.field public static final virtual_pad_num_back:I = 0x7f0a011f

.field public static final virtual_pad_volume:I = 0x7f0a0111

.field public static final warning_4k2knotsupport:I = 0x7f0a0198

.field public static final warning_cicard_timeout:I = 0x7f0a01e6

.field public static final warning_disabled:I = 0x7f0a01d3

.field public static final warning_dtv_notfound:I = 0x7f0a01e8

.field public static final warning_dtvnotsupport:I = 0x7f0a0014

.field public static final warning_dtvproblem:I = 0x7f0a0016

.field public static final warning_encryptedprogram:I = 0x7f0a0049

.field public static final warning_nocicard:I = 0x7f0a0048

.field public static final warning_notrightsource:I = 0x7f0a0015

.field public static final warning_notsupport_colorwheel:I = 0x7f0a01e7

.field public static final warning_stop_alwaystimeshift:I = 0x7f0a0194

.field public static final warning_tvnotsupport:I = 0x7f0a0193

.field public static final ypbpr:I = 0x7f0a01d0


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
