.class public final enum Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;
.super Ljava/lang/Enum;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkinterface/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EN_SOUND_AD_OUTPUT"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum AD_BOTH:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;

.field public static final enum AD_HEADPHONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;

.field public static final enum AD_SPEAKER:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;

.field private static final synthetic ENUM$VALUES:[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;

    const-string v1, "AD_SPEAKER"

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;->AD_SPEAKER:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;

    const-string v1, "AD_HEADPHONE"

    invoke-direct {v0, v1, v3}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;->AD_HEADPHONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;

    const-string v1, "AD_BOTH"

    invoke-direct {v0, v1, v4}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;->AD_BOTH:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;->AD_SPEAKER:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;

    aput-object v1, v0, v2

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;->AD_HEADPHONE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;->AD_BOTH:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;

    aput-object v1, v0, v4

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;->ENUM$VALUES:[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;
    .locals 1

    const-class v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;

    return-object v0
.end method

.method public static values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;->ENUM$VALUES:[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_AD_OUTPUT;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
