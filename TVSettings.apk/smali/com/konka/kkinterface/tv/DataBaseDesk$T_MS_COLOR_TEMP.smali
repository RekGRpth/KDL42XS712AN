.class public Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;
.super Ljava/lang/Object;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkinterface/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "T_MS_COLOR_TEMP"
.end annotation


# instance fields
.field public astColorTemp:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

.field public u16CheckSum:I


# direct methods
.method public constructor <init>()V
    .locals 9

    const/16 v1, 0x80

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    invoke-virtual {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->ordinal()I

    move-result v0

    new-array v0, v0, [Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    iput-object v0, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;->astColorTemp:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    const/4 v7, 0x0

    :goto_0
    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;

    invoke-virtual {v0}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_MS_COLOR_TEMP;->ordinal()I

    move-result v0

    if-lt v7, v0, :cond_0

    const v0, 0xffff

    iput v0, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;->u16CheckSum:I

    return-void

    :cond_0
    iget-object v8, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP;->astColorTemp:[Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    invoke-direct/range {v0 .. v6}, Lcom/konka/kkinterface/tv/DataBaseDesk$T_MS_COLOR_TEMP_DATA;-><init>(SSSSSS)V

    aput-object v0, v8, v7

    add-int/lit8 v7, v7, 0x1

    goto :goto_0
.end method
