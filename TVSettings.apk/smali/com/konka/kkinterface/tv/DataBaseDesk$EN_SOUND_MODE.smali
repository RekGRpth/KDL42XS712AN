.class public final enum Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;
.super Ljava/lang/Enum;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkinterface/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EN_SOUND_MODE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

.field public static final enum SOUND_MODE_MOVIE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

.field public static final enum SOUND_MODE_MUSIC:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

.field public static final enum SOUND_MODE_NEWS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

.field public static final enum SOUND_MODE_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

.field public static final enum SOUND_MODE_ONSITE1:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

.field public static final enum SOUND_MODE_ONSITE2:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

.field public static final enum SOUND_MODE_STANDARD:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

.field public static final enum SOUND_MODE_USER:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    const-string v1, "SOUND_MODE_STANDARD"

    invoke-direct {v0, v1, v3}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->SOUND_MODE_STANDARD:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    const-string v1, "SOUND_MODE_MUSIC"

    invoke-direct {v0, v1, v4}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->SOUND_MODE_MUSIC:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    const-string v1, "SOUND_MODE_MOVIE"

    invoke-direct {v0, v1, v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->SOUND_MODE_MOVIE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    const-string v1, "SOUND_MODE_NEWS"

    invoke-direct {v0, v1, v6}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->SOUND_MODE_NEWS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    const-string v1, "SOUND_MODE_USER"

    invoke-direct {v0, v1, v7}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->SOUND_MODE_USER:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    const-string v1, "SOUND_MODE_ONSITE1"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->SOUND_MODE_ONSITE1:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    const-string v1, "SOUND_MODE_ONSITE2"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->SOUND_MODE_ONSITE2:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    const-string v1, "SOUND_MODE_NUM"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->SOUND_MODE_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->SOUND_MODE_STANDARD:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->SOUND_MODE_MUSIC:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->SOUND_MODE_MOVIE:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->SOUND_MODE_NEWS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    aput-object v1, v0, v6

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->SOUND_MODE_USER:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->SOUND_MODE_ONSITE1:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->SOUND_MODE_ONSITE2:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->SOUND_MODE_NUM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->ENUM$VALUES:[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;
    .locals 1

    const-class v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    return-object v0
.end method

.method public static values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;->ENUM$VALUES:[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/kkinterface/tv/DataBaseDesk$EN_SOUND_MODE;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
