.class public Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;
.super Landroid/os/Binder;
.source "TvDeskImpl.java"

# interfaces
.implements Lcom/konka/kkinterface/tv/TvDeskProvider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TvServiceBinder"
.end annotation


# instance fields
.field caManager:Lcom/konka/kkinterface/tv/CaDesk;

.field channelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

.field ciManager:Lcom/konka/kkinterface/tv/CiDesk;

.field comManager:Lcom/konka/kkinterface/tv/CommonDesk;

.field dataBaseManager:Lcom/konka/kkinterface/tv/DataBaseDesk;

.field demoManager:Lcom/konka/kkinterface/tv/DemoDesk;

.field epgManager:Lcom/konka/kkinterface/tv/EpgDesk;

.field factoryManager:Lcom/konka/kkinterface/tv/FactoryDesk;

.field pictureManager:Lcom/konka/kkinterface/tv/PictureDesk;

.field pvrManager:Lcom/konka/kkinterface/tv/PvrDesk;

.field s3dManager:Lcom/konka/kkinterface/tv/S3DDesk;

.field settingManager:Lcom/konka/kkinterface/tv/SettingDesk;

.field soundManager:Lcom/konka/kkinterface/tv/SoundDesk;

.field final synthetic this$0:Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;


# direct methods
.method public constructor <init>(Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->this$0:Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->comManager:Lcom/konka/kkinterface/tv/CommonDesk;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->pictureManager:Lcom/konka/kkinterface/tv/PictureDesk;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->dataBaseManager:Lcom/konka/kkinterface/tv/DataBaseDesk;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->settingManager:Lcom/konka/kkinterface/tv/SettingDesk;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->channelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->soundManager:Lcom/konka/kkinterface/tv/SoundDesk;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->s3dManager:Lcom/konka/kkinterface/tv/S3DDesk;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->demoManager:Lcom/konka/kkinterface/tv/DemoDesk;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->factoryManager:Lcom/konka/kkinterface/tv/FactoryDesk;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->epgManager:Lcom/konka/kkinterface/tv/EpgDesk;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->pvrManager:Lcom/konka/kkinterface/tv/PvrDesk;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->ciManager:Lcom/konka/kkinterface/tv/CiDesk;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->caManager:Lcom/konka/kkinterface/tv/CaDesk;

    # getter for: Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;->context:Landroid/content/Context;
    invoke-static {p1}, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;->access$0(Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->dataBaseManager:Lcom/konka/kkinterface/tv/DataBaseDesk;

    return-void
.end method


# virtual methods
.method public getCaManagerInstance()Lcom/konka/kkinterface/tv/CaDesk;
    .locals 1

    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/CaDeskImpl;->getCaMgrInstance()Lcom/konka/kkimplements/tv/mstar/CaDeskImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->caManager:Lcom/konka/kkinterface/tv/CaDesk;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->caManager:Lcom/konka/kkinterface/tv/CaDesk;

    return-object v0
.end method

.method public getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->this$0:Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;

    # getter for: Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;->access$0(Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->getChannelMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->channelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->channelManager:Lcom/konka/kkinterface/tv/ChannelDesk;

    return-object v0
.end method

.method public getCiManagerInstance()Lcom/konka/kkinterface/tv/CiDesk;
    .locals 1

    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/CiDeskImpl;->getCiMgrInstance()Lcom/konka/kkimplements/tv/mstar/CiDeskImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->ciManager:Lcom/konka/kkinterface/tv/CiDesk;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->ciManager:Lcom/konka/kkinterface/tv/CiDesk;

    return-object v0
.end method

.method public getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->this$0:Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;

    # getter for: Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;->access$0(Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->comManager:Lcom/konka/kkinterface/tv/CommonDesk;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->comManager:Lcom/konka/kkinterface/tv/CommonDesk;

    return-object v0
.end method

.method public getDataBaseManagerInstance()Lcom/konka/kkinterface/tv/DataBaseDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->dataBaseManager:Lcom/konka/kkinterface/tv/DataBaseDesk;

    return-object v0
.end method

.method public getDemoManagerInstance()Lcom/konka/kkinterface/tv/DemoDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->this$0:Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;

    # getter for: Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;->access$0(Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;->getDemoMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DemoDeskImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->demoManager:Lcom/konka/kkinterface/tv/DemoDesk;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->demoManager:Lcom/konka/kkinterface/tv/DemoDesk;

    return-object v0
.end method

.method public getEpgManagerInstance()Lcom/konka/kkinterface/tv/EpgDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->this$0:Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;

    # getter for: Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;->access$0(Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;->getEpgMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/EpgDeskImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->epgManager:Lcom/konka/kkinterface/tv/EpgDesk;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->epgManager:Lcom/konka/kkinterface/tv/EpgDesk;

    return-object v0
.end method

.method public getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->this$0:Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;

    # getter for: Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;->access$0(Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;->getPictureMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/PictureDeskImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->pictureManager:Lcom/konka/kkinterface/tv/PictureDesk;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->pictureManager:Lcom/konka/kkinterface/tv/PictureDesk;

    return-object v0
.end method

.method public getPvrManagerInstance()Lcom/konka/kkinterface/tv/PvrDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->this$0:Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;

    # getter for: Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;->access$0(Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/PvrDeskImpl;->getPvrMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/PvrDeskImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->pvrManager:Lcom/konka/kkinterface/tv/PvrDesk;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->pvrManager:Lcom/konka/kkinterface/tv/PvrDesk;

    return-object v0
.end method

.method public getS3DManagerInstance()Lcom/konka/kkinterface/tv/S3DDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->this$0:Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;

    # getter for: Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;->access$0(Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;->getS3DMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/S3DDeskImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->s3dManager:Lcom/konka/kkinterface/tv/S3DDesk;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->s3dManager:Lcom/konka/kkinterface/tv/S3DDesk;

    return-object v0
.end method

.method public getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->this$0:Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;

    # getter for: Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;->access$0(Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;->getSettingMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/SettingDeskImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->settingManager:Lcom/konka/kkinterface/tv/SettingDesk;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->settingManager:Lcom/konka/kkinterface/tv/SettingDesk;

    return-object v0
.end method

.method public getSoundManagerInstance()Lcom/konka/kkinterface/tv/SoundDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->this$0:Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;

    # getter for: Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;->access$0(Lcom/konka/kkimplements/tv/mstar/TvDeskImpl;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;->getSoundMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/SoundDeskImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->soundManager:Lcom/konka/kkinterface/tv/SoundDesk;

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/TvDeskImpl$TvServiceBinder;->soundManager:Lcom/konka/kkinterface/tv/SoundDesk;

    return-object v0
.end method

.method public initTvSrvProvider()V
    .locals 0

    return-void
.end method
