.class public Lcom/konka/kkimplements/tv/LanguageUtil;
.super Ljava/lang/Object;
.source "LanguageUtil.java"


# static fields
.field public static final BURMA_CUSTOMER:I = 0x3

.field public static final GENERAL_CUSTOMER:I = 0x0

.field public static GreekCustomer:Ljava/lang/String; = null

.field public static GreekCustomer2:Ljava/lang/String; = null

.field public static final MLS_CUSTOMER:I = 0x4

.field public static PersionCustomer:Ljava/lang/String; = null

.field public static final SNOWA_CUSTOMER:I = 0x1

.field public static final THAI_CUSTOMER:I = 0x2

.field public static ThaiCustomer:Ljava/lang/String;

.field private static final languageMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "[",
            "Lcom/konka/kkimplements/tv/LanguageItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 15

    const/4 v14, 0x4

    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    const-string v5, "snowa"

    sput-object v5, Lcom/konka/kkimplements/tv/LanguageUtil;->PersionCustomer:Ljava/lang/String;

    const-string v5, "Thai"

    sput-object v5, Lcom/konka/kkimplements/tv/LanguageUtil;->ThaiCustomer:Ljava/lang/String;

    const-string v5, "MLS_HD"

    sput-object v5, Lcom/konka/kkimplements/tv/LanguageUtil;->GreekCustomer:Ljava/lang/String;

    const-string v5, "MLS_SD"

    sput-object v5, Lcom/konka/kkimplements/tv/LanguageUtil;->GreekCustomer2:Ljava/lang/String;

    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    sput-object v5, Lcom/konka/kkimplements/tv/LanguageUtil;->languageMap:Ljava/util/HashMap;

    const/16 v5, 0x9

    new-array v0, v5, [Lcom/konka/kkimplements/tv/LanguageItem;

    new-instance v5, Lcom/konka/kkimplements/tv/LanguageItem;

    const-string v6, "en_US"

    const-string v7, "US"

    const-string v8, "English"

    invoke-direct {v5, v6, v7, v8}, Lcom/konka/kkimplements/tv/LanguageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v0, v10

    new-instance v5, Lcom/konka/kkimplements/tv/LanguageItem;

    const-string v6, "ar_EG"

    const-string v7, "EG"

    const-string v8, "\u0639\u0631\u0628\u0649"

    invoke-direct {v5, v6, v7, v8}, Lcom/konka/kkimplements/tv/LanguageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v0, v11

    new-instance v5, Lcom/konka/kkimplements/tv/LanguageItem;

    const-string v6, "fa_IR"

    const-string v7, "IR"

    const-string v8, "\u0641\u0627\u0631\u0633\u06cc"

    invoke-direct {v5, v6, v7, v8}, Lcom/konka/kkimplements/tv/LanguageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v0, v12

    new-instance v5, Lcom/konka/kkimplements/tv/LanguageItem;

    const-string v6, "kd_KD"

    const-string v7, "KD"

    const-string v8, "\u06a9\u0648\u0631\u062f\u06cc"

    invoke-direct {v5, v6, v7, v8}, Lcom/konka/kkimplements/tv/LanguageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v0, v13

    new-instance v5, Lcom/konka/kkimplements/tv/LanguageItem;

    const-string v6, "ru_RU"

    const-string v7, "RU"

    const-string v8, "\u0420\u0443\u0441\u0441\u043a\u0438\u0439"

    invoke-direct {v5, v6, v7, v8}, Lcom/konka/kkimplements/tv/LanguageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v0, v14

    const/4 v5, 0x5

    new-instance v6, Lcom/konka/kkimplements/tv/LanguageItem;

    const-string v7, "fr_FR"

    const-string v8, "FR"

    const-string v9, "Fran\u03c2ais"

    invoke-direct {v6, v7, v8, v9}, Lcom/konka/kkimplements/tv/LanguageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v6, v0, v5

    const/4 v5, 0x6

    new-instance v6, Lcom/konka/kkimplements/tv/LanguageItem;

    const-string v7, "in_ID"

    const-string v8, "ID"

    const-string v9, "Indonesia"

    invoke-direct {v6, v7, v8, v9}, Lcom/konka/kkimplements/tv/LanguageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v6, v0, v5

    const/4 v5, 0x7

    new-instance v6, Lcom/konka/kkimplements/tv/LanguageItem;

    const-string v7, "tr_TR"

    const-string v8, "TR"

    const-string v9, "T\u00fcrk\u00e7e"

    invoke-direct {v6, v7, v8, v9}, Lcom/konka/kkimplements/tv/LanguageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v6, v0, v5

    const/16 v5, 0x8

    new-instance v6, Lcom/konka/kkimplements/tv/LanguageItem;

    const-string v7, "iw_IL"

    const-string v8, "IL"

    const-string v9, "\u05e2\u05d1\u05e8\u05d9\u05ea"

    invoke-direct {v6, v7, v8, v9}, Lcom/konka/kkimplements/tv/LanguageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v6, v0, v5

    sget-object v5, Lcom/konka/kkimplements/tv/LanguageUtil;->languageMap:Ljava/util/HashMap;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-array v1, v13, [Lcom/konka/kkimplements/tv/LanguageItem;

    new-instance v5, Lcom/konka/kkimplements/tv/LanguageItem;

    const-string v6, "en_US"

    const-string v7, "US"

    const-string v8, "English"

    invoke-direct {v5, v6, v7, v8}, Lcom/konka/kkimplements/tv/LanguageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v1, v10

    new-instance v5, Lcom/konka/kkimplements/tv/LanguageItem;

    const-string v6, "ar_EG"

    const-string v7, "EG"

    const-string v8, "\u0639\u0631\u0628\u06cc"

    invoke-direct {v5, v6, v7, v8}, Lcom/konka/kkimplements/tv/LanguageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v1, v11

    new-instance v5, Lcom/konka/kkimplements/tv/LanguageItem;

    const-string v6, "fa_IR"

    const-string v7, "IR"

    const-string v8, "\u0641\u0627\u0631\u0633\u06cc"

    invoke-direct {v5, v6, v7, v8}, Lcom/konka/kkimplements/tv/LanguageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v1, v12

    sget-object v5, Lcom/konka/kkimplements/tv/LanguageUtil;->languageMap:Ljava/util/HashMap;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v5, 0xa

    new-array v2, v5, [Lcom/konka/kkimplements/tv/LanguageItem;

    new-instance v5, Lcom/konka/kkimplements/tv/LanguageItem;

    const-string v6, "en_US"

    const-string v7, "US"

    const-string v8, "English"

    invoke-direct {v5, v6, v7, v8}, Lcom/konka/kkimplements/tv/LanguageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v2, v10

    new-instance v5, Lcom/konka/kkimplements/tv/LanguageItem;

    const-string v6, "ar_EG"

    const-string v7, "EG"

    const-string v8, "\u0639\u0631\u0628\u0649"

    invoke-direct {v5, v6, v7, v8}, Lcom/konka/kkimplements/tv/LanguageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v2, v11

    new-instance v5, Lcom/konka/kkimplements/tv/LanguageItem;

    const-string v6, "fa_IR"

    const-string v7, "IR"

    const-string v8, "\u0641\u0627\u0631\u0633\u06cc"

    invoke-direct {v5, v6, v7, v8}, Lcom/konka/kkimplements/tv/LanguageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v2, v12

    new-instance v5, Lcom/konka/kkimplements/tv/LanguageItem;

    const-string v6, "kd_KD"

    const-string v7, "KD"

    const-string v8, "\u06a9\u0648\u0631\u062f\u06cc"

    invoke-direct {v5, v6, v7, v8}, Lcom/konka/kkimplements/tv/LanguageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v2, v13

    new-instance v5, Lcom/konka/kkimplements/tv/LanguageItem;

    const-string v6, "ru_RU"

    const-string v7, "RU"

    const-string v8, "\u0420\u0443\u0441\u0441\u043a\u0438\u0439"

    invoke-direct {v5, v6, v7, v8}, Lcom/konka/kkimplements/tv/LanguageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v2, v14

    const/4 v5, 0x5

    new-instance v6, Lcom/konka/kkimplements/tv/LanguageItem;

    const-string v7, "fr_FR"

    const-string v8, "FR"

    const-string v9, "Fran\u03c2ais"

    invoke-direct {v6, v7, v8, v9}, Lcom/konka/kkimplements/tv/LanguageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v6, v2, v5

    const/4 v5, 0x6

    new-instance v6, Lcom/konka/kkimplements/tv/LanguageItem;

    const-string v7, "in_ID"

    const-string v8, "ID"

    const-string v9, "Indonesia"

    invoke-direct {v6, v7, v8, v9}, Lcom/konka/kkimplements/tv/LanguageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v6, v2, v5

    const/4 v5, 0x7

    new-instance v6, Lcom/konka/kkimplements/tv/LanguageItem;

    const-string v7, "tr_TR"

    const-string v8, "TR"

    const-string v9, "T\u00fcrk\u00e7e"

    invoke-direct {v6, v7, v8, v9}, Lcom/konka/kkimplements/tv/LanguageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v6, v2, v5

    const/16 v5, 0x8

    new-instance v6, Lcom/konka/kkimplements/tv/LanguageItem;

    const-string v7, "iw_IL"

    const-string v8, "IL"

    const-string v9, "\u05e2\u05d1\u05e8\u05d9\u05ea"

    invoke-direct {v6, v7, v8, v9}, Lcom/konka/kkimplements/tv/LanguageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v6, v2, v5

    const/16 v5, 0x9

    new-instance v6, Lcom/konka/kkimplements/tv/LanguageItem;

    const-string v7, "th_TH"

    const-string v8, "TH"

    const-string v9, "\u0e20\u0e32\u0e29\u0e32\u0e44\u0e17\u0e22"

    invoke-direct {v6, v7, v8, v9}, Lcom/konka/kkimplements/tv/LanguageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v6, v2, v5

    sget-object v5, Lcom/konka/kkimplements/tv/LanguageUtil;->languageMap:Ljava/util/HashMap;

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-array v3, v12, [Lcom/konka/kkimplements/tv/LanguageItem;

    new-instance v5, Lcom/konka/kkimplements/tv/LanguageItem;

    const-string v6, "en_US"

    const-string v7, "US"

    const-string v8, "English"

    invoke-direct {v5, v6, v7, v8}, Lcom/konka/kkimplements/tv/LanguageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v10

    new-instance v5, Lcom/konka/kkimplements/tv/LanguageItem;

    const-string v6, "zh_CN"

    const-string v7, "CN"

    const-string v8, "\u4e2d\u6587"

    invoke-direct {v5, v6, v7, v8}, Lcom/konka/kkimplements/tv/LanguageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v3, v11

    sget-object v5, Lcom/konka/kkimplements/tv/LanguageUtil;->languageMap:Ljava/util/HashMap;

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v5, 0xa

    new-array v4, v5, [Lcom/konka/kkimplements/tv/LanguageItem;

    new-instance v5, Lcom/konka/kkimplements/tv/LanguageItem;

    const-string v6, "en_US"

    const-string v7, "US"

    const-string v8, "English"

    invoke-direct {v5, v6, v7, v8}, Lcom/konka/kkimplements/tv/LanguageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v4, v10

    new-instance v5, Lcom/konka/kkimplements/tv/LanguageItem;

    const-string v6, "ar_EG"

    const-string v7, "EG"

    const-string v8, "\u0639\u0631\u0628\u0649"

    invoke-direct {v5, v6, v7, v8}, Lcom/konka/kkimplements/tv/LanguageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v4, v11

    new-instance v5, Lcom/konka/kkimplements/tv/LanguageItem;

    const-string v6, "fa_IR"

    const-string v7, "IR"

    const-string v8, "\u0641\u0627\u0631\u0633\u06cc"

    invoke-direct {v5, v6, v7, v8}, Lcom/konka/kkimplements/tv/LanguageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v4, v12

    new-instance v5, Lcom/konka/kkimplements/tv/LanguageItem;

    const-string v6, "kd_KD"

    const-string v7, "KD"

    const-string v8, "\u06a9\u0648\u0631\u062f\u06cc"

    invoke-direct {v5, v6, v7, v8}, Lcom/konka/kkimplements/tv/LanguageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v4, v13

    new-instance v5, Lcom/konka/kkimplements/tv/LanguageItem;

    const-string v6, "ru_RU"

    const-string v7, "RU"

    const-string v8, "\u0420\u0443\u0441\u0441\u043a\u0438\u0439"

    invoke-direct {v5, v6, v7, v8}, Lcom/konka/kkimplements/tv/LanguageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v4, v14

    const/4 v5, 0x5

    new-instance v6, Lcom/konka/kkimplements/tv/LanguageItem;

    const-string v7, "fr_FR"

    const-string v8, "FR"

    const-string v9, "Fran\u03c2ais"

    invoke-direct {v6, v7, v8, v9}, Lcom/konka/kkimplements/tv/LanguageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v6, v4, v5

    const/4 v5, 0x6

    new-instance v6, Lcom/konka/kkimplements/tv/LanguageItem;

    const-string v7, "in_ID"

    const-string v8, "ID"

    const-string v9, "Indonesia"

    invoke-direct {v6, v7, v8, v9}, Lcom/konka/kkimplements/tv/LanguageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v6, v4, v5

    const/4 v5, 0x7

    new-instance v6, Lcom/konka/kkimplements/tv/LanguageItem;

    const-string v7, "tr_TR"

    const-string v8, "TR"

    const-string v9, "T\u00fcrk\u00e7e"

    invoke-direct {v6, v7, v8, v9}, Lcom/konka/kkimplements/tv/LanguageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v6, v4, v5

    const/16 v5, 0x8

    new-instance v6, Lcom/konka/kkimplements/tv/LanguageItem;

    const-string v7, "iw_IL"

    const-string v8, "IL"

    const-string v9, "\u05e2\u05d1\u05e8\u05d9\u05ea"

    invoke-direct {v6, v7, v8, v9}, Lcom/konka/kkimplements/tv/LanguageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v6, v4, v5

    const/16 v5, 0x9

    new-instance v6, Lcom/konka/kkimplements/tv/LanguageItem;

    const-string v7, "el_GR"

    const-string v8, "GR"

    const-string v9, "\u0395\u03bb\u03bb\u03b7\u03bd\u03b9\u03ba\u03ae"

    invoke-direct {v6, v7, v8, v9}, Lcom/konka/kkimplements/tv/LanguageItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v6, v4, v5

    sget-object v5, Lcom/konka/kkimplements/tv/LanguageUtil;->languageMap:Ljava/util/HashMap;

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCurrentLanguageIndex([Lcom/konka/kkimplements/tv/LanguageItem;Ljava/lang/String;)I
    .locals 2
    .param p0    # [Lcom/konka/kkimplements/tv/LanguageItem;
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    if-eqz p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    array-length v1, p0

    if-lt v0, v1, :cond_2

    :cond_0
    const/4 v0, 0x0

    :cond_1
    return v0

    :cond_2
    aget-object v1, p0, v0

    iget-object v1, v1, Lcom/konka/kkimplements/tv/LanguageItem;->mCountry:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static getCustomeLanguageList(Landroid/content/Context;)[Lcom/konka/kkimplements/tv/LanguageItem;
    .locals 2
    .param p0    # Landroid/content/Context;

    const-string v0, "LanguageUtil"

    const-string v1, "getCustomeLanguageList"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/konka/kkimplements/tv/LanguageUtil;->languageMap:Ljava/util/HashMap;

    invoke-static {p0}, Lcom/konka/kkimplements/tv/LanguageUtil;->mapCurCustomerIndex(Landroid/content/Context;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/konka/kkimplements/tv/LanguageItem;

    return-object v0
.end method

.method private static mapCurCustomerIndex(Landroid/content/Context;)I
    .locals 6
    .param p0    # Landroid/content/Context;

    const/4 v3, 0x1

    const-string v4, "LanguageUtil"

    const-string v5, "mapCurCustomerIndex"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v1}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v4, "/customercfg/model/config.ini"

    invoke-virtual {v1, v4}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    const-string v4, "APPLICATION_OPTIONS:CHINESE_ENABLE"

    const-string v5, "0"

    invoke-virtual {v1, v4, v5}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/CommonDesk;->getCustomerInfo()Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;

    move-result-object v4

    iget-object v0, v4, Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;->strCustomerName:Ljava/lang/String;

    sget-object v4, Lcom/konka/kkimplements/tv/LanguageUtil;->PersionCustomer:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_0
    return v3

    :cond_0
    sget-object v4, Lcom/konka/kkimplements/tv/LanguageUtil;->ThaiCustomer:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v3, 0x2

    goto :goto_0

    :cond_1
    sget-object v4, Lcom/konka/kkimplements/tv/LanguageUtil;->GreekCustomer:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    sget-object v4, Lcom/konka/kkimplements/tv/LanguageUtil;->GreekCustomer2:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_2
    const/4 v3, 0x4

    goto :goto_0

    :cond_3
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    if-ne v4, v3, :cond_4

    const/4 v3, 0x3

    goto :goto_0

    :cond_4
    const/4 v3, 0x0

    goto :goto_0
.end method
