.class public interface abstract Lcom/konka/kkinterface/tv/DtvInterface;
.super Ljava/lang/Object;
.source "DtvInterface.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;
    }
.end annotation


# static fields
.field public static final dtvantentype:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "DTMB"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "DVB-C"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "DVB-T"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "MAX"

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/kkinterface/tv/DtvInterface;->dtvantentype:[Ljava/lang/String;

    return-void
.end method
