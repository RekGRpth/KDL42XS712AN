.class public Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_CI_SETTING;
.super Ljava/lang/Object;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkinterface/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ST_FACTORY_CI_SETTING"
.end annotation


# instance fields
.field public u8CIFunctionDebugLevel:[S


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_DEBUG_COUNT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->ordinal()I

    move-result v1

    new-array v1, v1, [S

    iput-object v1, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_CI_SETTING;->u8CIFunctionDebugLevel:[S

    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_DEBUG_COUNT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->ordinal()I

    move-result v1

    if-lt v0, v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$ST_FACTORY_CI_SETTING;->u8CIFunctionDebugLevel:[S

    const/16 v2, 0x80

    aput-short v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
