.class public interface abstract Lcom/konka/kkinterface/tv/ChannelDesk;
.super Ljava/lang/Object;
.source "ChannelDesk.java"

# interfaces
.implements Lcom/konka/kkinterface/tv/DtvInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;,
        Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;
    }
.end annotation


# static fields
.field public static final msrvPlayer:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/kkinterface/tv/ChannelDesk;->msrvPlayer:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public abstract getUserScanType()Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;
.end method

.method public abstract getVideoStandard()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;
.end method

.method public abstract isSignalStabled()Z
.end method

.method public abstract setUserScanType(Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;)V
.end method
