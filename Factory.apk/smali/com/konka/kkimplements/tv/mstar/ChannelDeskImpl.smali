.class public Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;
.super Lcom/konka/kkimplements/tv/mstar/BaseDeskImpl;
.source "ChannelDeskImpl.java"

# interfaces
.implements Lcom/konka/kkinterface/tv/ChannelDesk;


# static fields
.field private static channelMgrImpl:Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;


# instance fields
.field private context:Landroid/content/Context;

.field private curChannelNumber:I

.field private curDtvRoute:S

.field dvbcsp:Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;

.field private prevChannelNumber:I

.field sm:Lcom/mstar/android/tvapi/dtv/common/SubtitleManager;

.field private tuning_scan_type:Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

.field private tv_tuning_status:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1    # Landroid/content/Context;

    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/BaseDeskImpl;-><init>()V

    iput v4, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->curChannelNumber:I

    iput v4, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->prevChannelNumber:I

    sget-object v2, Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;->E_TS_NONE:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    iput-object v2, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->tv_tuning_status:Lcom/konka/kkinterface/tv/ChannelDesk$TV_TS_STATUS;

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->sm:Lcom/mstar/android/tvapi/dtv/common/SubtitleManager;

    iput-short v4, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->curDtvRoute:S

    sget-object v2, Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;->E_SCAN_ALL:Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    iput-object v2, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->tuning_scan_type:Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->context:Landroid/content/Context;

    iput v4, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->curChannelNumber:I

    new-instance v2, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;

    invoke-direct {v2}, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;-><init>()V

    iput-object v2, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->dvbcsp:Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->dvbcsp:Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;

    const/16 v3, 0x1adb

    iput-short v3, v2, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;->u16SymbolRate:S

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->dvbcsp:Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;

    sget-object v3, Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;->E_CAB_QAM64:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    iput-object v3, v2, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;->QAM_Type:Lcom/mstar/android/tvapi/dtv/dvb/dvbc/vo/EnumCabConstelType;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->dvbcsp:Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;

    iput v4, v2, Lcom/konka/kkinterface/tv/DtvInterface$DvbcScanParam;->u32NITFrequency:I

    invoke-static {p1}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {p1}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/CommonDesk;->getCustomerInfo()Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;

    move-result-object v2

    iget-object v0, v2, Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;->strCustomerName:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v2, "teac"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;->E_SCAN_DTV:Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    iput-object v2, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->tuning_scan_type:Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    :cond_0
    :goto_0
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getSubtitleManager()Lcom/mstar/android/tvapi/dtv/common/SubtitleManager;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->sm:Lcom/mstar/android/tvapi/dtv/common/SubtitleManager;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :cond_1
    sget-object v2, Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;->E_SCAN_ALL:Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    iput-object v2, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->tuning_scan_type:Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public static getChannelMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;
    .locals 1
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->channelMgrImpl:Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;

    invoke-direct {v0, p0}, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->channelMgrImpl:Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;

    :cond_0
    sget-object v0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->channelMgrImpl:Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;

    return-object v0
.end method


# virtual methods
.method public getUserScanType()Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->tuning_scan_type:Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    return-object v0
.end method

.method public getVideoStandard()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;
    .locals 2

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v1

    invoke-interface {v1}, Lcom/mstar/android/tvapi/common/TvPlayer;->getVideoStandard()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isSignalStabled()Z
    .locals 3

    const/4 v1, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v2

    if-eqz v2, :cond_0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v2

    invoke-interface {v2}, Lcom/mstar/android/tvapi/common/TvPlayer;->isSignalStable()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setUserScanType(Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;)V
    .locals 0
    .param p1    # Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->tuning_scan_type:Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    return-void
.end method
