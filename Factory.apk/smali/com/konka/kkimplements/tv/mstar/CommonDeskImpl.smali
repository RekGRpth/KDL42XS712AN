.class public Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;
.super Lcom/konka/kkimplements/tv/mstar/BaseDeskImpl;
.source "CommonDeskImpl.java"

# interfaces
.implements Lcom/konka/kkinterface/tv/CommonDesk;


# static fields
.field public static bThreadIsrunning:Z

.field private static commonService:Lcom/konka/kkinterface/tv/CommonDesk;


# instance fields
.field private applicationsInfo:Lcom/konka/kkinterface/tv/CommonDesk$ApplicationsInfo;

.field private audioInfo:Lcom/konka/kkinterface/tv/CommonDesk$AudioInfo;

.field private batteryInfo:Lcom/konka/kkinterface/tv/CommonDesk$BatteryInfo;

.field private bforcechangesource:Z

.field private ciCardInfo:Lcom/konka/kkinterface/tv/CommonDesk$CiCardInfo;

.field private context:Landroid/content/Context;

.field private cpuInfo:Lcom/konka/kkinterface/tv/CommonDesk$CPUInfo;

.field private curSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field private customerInfo:Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;

.field deskAtvPlayerLister:Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;

.field deskCaLister:Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;

.field deskCiLister:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;

.field deskDtvPlayerLister:Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;

.field deskTimerLister:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;

.field deskTvLister:Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;

.field deskTvPlayerLister:Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;

.field private exStorageInfo:Lcom/konka/kkinterface/tv/CommonDesk$ExternalStorageInfo;

.field private inputSourceInfo:Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

.field private localdimmingInfo:Lcom/konka/kkinterface/tv/CommonDesk$LocalDimmingInfo;

.field private panel4k2k:Lcom/konka/kkinterface/tv/CommonDesk$Panel4K2K;

.field private panelsupportInfo:Lcom/konka/kkinterface/tv/CommonDesk$PanelSupportInfo;

.field private pipInfo:Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;

.field psl:[I

.field private systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

.field private systemBuildInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBuildInfo;

.field private typeOf3DImpl:Lcom/konka/kkinterface/tv/CommonDesk$TypeOf3DImpl;

.field private typeThreeDImp:Lcom/konka/kkinterface/tv/CommonDesk$ThreeDImplType;

.field private ursaSelectType:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

.field private videoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

.field private wifiDeviceInfo:Lcom/konka/kkinterface/tv/CommonDesk$WifiDeviceInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->commonService:Lcom/konka/kkinterface/tv/CommonDesk;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->bThreadIsrunning:Z

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/BaseDeskImpl;-><init>()V

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->psl:[I

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->curSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->videoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->inputSourceInfo:Lcom/konka/kkinterface/tv/CommonDesk$InputSourceInfo;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->audioInfo:Lcom/konka/kkinterface/tv/CommonDesk$AudioInfo;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->batteryInfo:Lcom/konka/kkinterface/tv/CommonDesk$BatteryInfo;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->ciCardInfo:Lcom/konka/kkinterface/tv/CommonDesk$CiCardInfo;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->pipInfo:Lcom/konka/kkinterface/tv/CommonDesk$PipInfo;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBuildInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBuildInfo;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->applicationsInfo:Lcom/konka/kkinterface/tv/CommonDesk$ApplicationsInfo;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->customerInfo:Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->localdimmingInfo:Lcom/konka/kkinterface/tv/CommonDesk$LocalDimmingInfo;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->panel4k2k:Lcom/konka/kkinterface/tv/CommonDesk$Panel4K2K;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->typeOf3DImpl:Lcom/konka/kkinterface/tv/CommonDesk$TypeOf3DImpl;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->typeThreeDImp:Lcom/konka/kkinterface/tv/CommonDesk$ThreeDImplType;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->ursaSelectType:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->wifiDeviceInfo:Lcom/konka/kkinterface/tv/CommonDesk$WifiDeviceInfo;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->panelsupportInfo:Lcom/konka/kkinterface/tv/CommonDesk$PanelSupportInfo;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->cpuInfo:Lcom/konka/kkinterface/tv/CommonDesk$CPUInfo;

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->exStorageInfo:Lcom/konka/kkinterface/tv/CommonDesk$ExternalStorageInfo;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->bforcechangesource:Z

    iput-object p1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->context:Landroid/content/Context;

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    const/16 v1, 0x780

    const/16 v2, 0x438

    const/16 v3, 0x3c

    const/16 v4, 0xc

    sget-object v5, Lcom/konka/kkinterface/tv/CommonDesk$EN_SCAN_TYPE;->E_PROGRESSIVE:Lcom/konka/kkinterface/tv/CommonDesk$EN_SCAN_TYPE;

    invoke-direct/range {v0 .. v5}, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;-><init>(SSSSLcom/konka/kkinterface/tv/CommonDesk$EN_SCAN_TYPE;)V

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->videoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    invoke-direct {p0}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->InitSourceList()V

    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;->getInstance()Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskAtvPlayerLister:Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;

    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->getInstance()Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskDtvPlayerLister:Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;

    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;->getInstance()Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskTvPlayerLister:Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;

    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;->getInstance()Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskTvLister:Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;

    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;->getInstance()Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskCiLister:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;

    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;->getInstance()Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskCaLister:Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;

    invoke-static {}, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->getInstance()Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskTimerLister:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskTvPlayerLister:Lcom/konka/kkimplements/tv/mstar/DeskTvPlayerEventListener;

    invoke-interface {v0, v1}, Lcom/mstar/android/tvapi/common/TvPlayer;->setOnTvPlayerEventListener(Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskTvLister:Lcom/konka/kkimplements/tv/mstar/DeskTvEventListener;

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/TvManager;->setOnTvEventListener(Lcom/mstar/android/tvapi/common/listener/OnTvEventListener;)V

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskDtvPlayerLister:Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;

    invoke-interface {v0, v1}, Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;->setOnDtvPlayerEventListener(Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;)V

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvPlayerManager()Lcom/mstar/android/tvapi/atv/AtvPlayer;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvPlayerManager()Lcom/mstar/android/tvapi/atv/AtvPlayer;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskAtvPlayerLister:Lcom/konka/kkimplements/tv/mstar/DeskAtvPlayerEventListener;

    invoke-interface {v0, v1}, Lcom/mstar/android/tvapi/atv/AtvPlayer;->setOnAtvPlayerEventListener(Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;)V

    :cond_2
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getCiManager()Lcom/mstar/android/tvapi/dtv/common/CiManager;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskCiLister:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/dtv/common/CiManager;->setOnCiEventListener(Lcom/mstar/android/tvapi/dtv/common/CiManager$OnCiEventListener;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->getInstance()Lcom/mstar/android/tvapi/dtv/common/CaManager;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->getInstance()Lcom/mstar/android/tvapi/dtv/common/CaManager;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskCaLister:Lcom/konka/kkimplements/tv/mstar/DeskCaEventListener;

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/dtv/common/CaManager;->setOnCaEventListener(Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;)V

    :cond_4
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getTimerManager()Lcom/mstar/android/tvapi/common/TimerManager;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->deskTimerLister:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/TimerManager;->setOnTimerEventListener(Lcom/mstar/android/tvapi/common/TimerManager$OnTimerEventListener;)V

    :cond_5
    return-void

    :catch_0
    move-exception v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private InitSourceList()V
    .locals 2

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getSourceList()[I

    move-result-object v1

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->psl:[I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;
    .locals 1
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->commonService:Lcom/konka/kkinterface/tv/CommonDesk;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;

    invoke-direct {v0, p0}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->commonService:Lcom/konka/kkinterface/tv/CommonDesk;

    :cond_0
    sget-object v0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->commonService:Lcom/konka/kkinterface/tv/CommonDesk;

    return-object v0
.end method


# virtual methods
.method public GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .locals 2

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->curSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->curSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getCustomerInfo()Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;
    .locals 4

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->customerInfo:Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;

    invoke-direct {v0}, Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;-><init>()V

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->customerInfo:Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;

    new-instance v0, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v0}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v1, "customercfg/model/config.ini"

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->customerInfo:Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;

    const-string v2, "MISC_CUSTOMER_CFG:CUSTOMER"

    const-string v3, "default"

    invoke-virtual {v0, v2, v3}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;->strCustomerName:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->customerInfo:Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;

    return-object v0
.end method

.method public getSystemBoardInfo()Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;
    .locals 6

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    invoke-direct {v0}, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;-><init>()V

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    new-instance v0, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v0}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v1, "/customercfg/model/config.ini"

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    const-string v2, "MISC_BOARD_CFG:PLATFORM"

    const-string v3, "6a801"

    invoke-virtual {v0, v2, v3}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strPlatform:Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    const-string v2, "MISC_BOARD_CFG:BOARD"

    const-string v3, "LEDxx"

    invoke-virtual {v0, v2, v3}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strBoard:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/konka/kkimplements/common/IniEditor;->unloadFile()V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getSystemPanelName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strPanelName:Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    const-string v3, "MBoot_VER"

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/TvManager;->getEnvironment(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strVersionMboot:Ljava/lang/String;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/factory/FactoryManager;->ursaGetVersionInfo()Lcom/mstar/android/tvapi/factory/vo/UrsaVersionInfo;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "V"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v1, Lcom/mstar/android/tvapi/factory/vo/UrsaVersionInfo;->u16Version:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, v1, Lcom/mstar/android/tvapi/factory/vo/UrsaVersionInfo;->u32Changelist:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strVersion6m30:Ljava/lang/String;

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "fetch from sn:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    iget-object v3, v3, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strVersion6m30:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v1, "/customercfg/model/version.ini"

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strPanelName:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ":MODEL"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    const-string v3, "LED"

    invoke-virtual {v0, v1, v3}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strSerial:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strPanelName:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ":VERSION"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    const-string v3, "V1.0.00"

    invoke-virtual {v0, v1, v3}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strVersion:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strPanelName:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ":BUILD"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    const-string v3, "1234"

    invoke-virtual {v0, v1, v3}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strBuild:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strPanelName:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ":CODE"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    const-string v3, "99000000"

    invoke-virtual {v0, v1, v3}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strCode:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    iget-object v2, v2, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strPanelName:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ":DATE"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    const-string v3, "00:00:00 2012-02-25"

    invoke-virtual {v0, v1, v3}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strDate:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/konka/kkimplements/common/IniEditor;->unloadFile()V

    :cond_0
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strPlatform:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strSerial:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strPanelName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strVersion:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strBuild:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strCode:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strDate:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    iget-object v1, v1, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strVersion6m30:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    return-object v0

    :cond_1
    :try_start_1
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "Can\'t fetch 6M30version!!!"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    const-string v2, "No"

    iput-object v2, v1, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strVersion6m30:Ljava/lang/String;
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v1

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    const-string v2, "panel"

    iput-object v2, v1, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strPanelName:Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    const-string v2, "V1.0.00"

    iput-object v2, v1, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strVersionMboot:Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    const-string v2, "No"

    iput-object v2, v1, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strVersion6m30:Ljava/lang/String;

    goto/16 :goto_0

    :cond_2
    :try_start_2
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    const-string v2, "panel"

    iput-object v2, v1, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strPanelName:Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    const-string v2, "V1.0.00"

    iput-object v2, v1, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strVersionMboot:Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->systemBoardInfo:Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    const-string v2, "No"

    iput-object v2, v1, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strVersion6m30:Ljava/lang/String;
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0
.end method

.method public getVideoInfo()Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;
    .locals 5

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v2

    invoke-interface {v2}, Lcom/mstar/android/tvapi/common/TvPlayer;->getVideoInfo()Lcom/mstar/android/tvapi/common/vo/VideoInfo;

    move-result-object v1

    :cond_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->videoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget v3, v1, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->frameRate:I

    int-to-short v3, v3

    iput-short v3, v2, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16FrameRate:S

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->videoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget v3, v1, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->hResolution:I

    int-to-short v3, v3

    iput-short v3, v2, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16HResolution:S

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->videoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget v3, v1, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->vResolution:I

    int-to-short v3, v3

    iput-short v3, v2, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->videoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget v3, v1, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->modeIndex:I

    int-to-short v3, v3

    iput-short v3, v2, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16ModeIndex:S

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->videoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    invoke-static {}, Lcom/konka/kkinterface/tv/CommonDesk$EN_SCAN_TYPE;->values()[Lcom/konka/kkinterface/tv/CommonDesk$EN_SCAN_TYPE;

    move-result-object v3

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/VideoInfo;->getScanType()Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/VideoInfo$EnumScanType;->ordinal()I

    move-result v4

    aget-object v3, v3, v4

    iput-object v3, v2, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->enScanType:Lcom/konka/kkinterface/tv/CommonDesk$EN_SCAN_TYPE;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->videoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    return-object v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public isHdmiSignalMode()Z
    .locals 1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v0

    invoke-interface {v0}, Lcom/mstar/android/tvapi/common/TvPlayer;->isHdmiMode()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSignalStable()Z
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v2

    invoke-interface {v2}, Lcom/mstar/android/tvapi/common/TvPlayer;->isSignalStable()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method
