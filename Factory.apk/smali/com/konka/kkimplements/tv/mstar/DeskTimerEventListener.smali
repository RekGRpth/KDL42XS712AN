.class public Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;
.super Ljava/lang/Object;
.source "DeskTimerEventListener.java"

# interfaces
.implements Lcom/mstar/android/tvapi/common/TimerManager$OnTimerEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;
    }
.end annotation


# static fields
.field private static timerEventListener:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;


# instance fields
.field private m_handler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->timerEventListener:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->m_handler:Landroid/os/Handler;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->m_handler:Landroid/os/Handler;

    return-void
.end method

.method public static getInstance()Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;
    .locals 1

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->timerEventListener:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;

    invoke-direct {v0}, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;-><init>()V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->timerEventListener:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;

    :cond_0
    sget-object v0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->timerEventListener:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;

    return-object v0
.end method


# virtual methods
.method public onDestroyCountDown(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v3

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->EV_DESTROY_COUNTDOWN:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onEpgTimeUp(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v3

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "LeftTime"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->EV_EPG_TIME_UP:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onEpgTimerCountDown(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v3

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "LeftTime"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->EV_EPGTIMER_COUNTDOWN:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onEpgTimerRecordStart(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v3

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "LeftTime"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->EV_EPGTIMER_RECORD_START:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onLastMinuteWarn(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v3

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "LeftTime"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->EV_LASTMINUTE_WARN:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onOadTimeScan(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v3

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "LeftTime"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->EV_OAD_TIMESCAN:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onOneSecondBeat(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v3

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "LeftTime"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->EV_ONESECOND_BEAT:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onPowerDownTime(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onPvrNotifyRecordStop(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v3

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "LeftTime"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->EV_PVR_NOTIFY_RECORD_STOP:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onSignalLock(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v3

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->EV_SIGNAL_LOCK:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onSystemClkChg(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onUpdateLastMinute(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v3

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "LeftTime"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->EV_UPDATE_LASTMINUTE:Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener$EVENT;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskTimerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
