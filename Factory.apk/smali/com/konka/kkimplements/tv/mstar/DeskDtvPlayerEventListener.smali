.class public Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;
.super Ljava/lang/Object;
.source "DeskDtvPlayerEventListener.java"

# interfaces
.implements Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;


# static fields
.field private static dtvEventListener:Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;


# instance fields
.field private m_handler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->dtvEventListener:Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    return-void
.end method

.method static getInstance()Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;
    .locals 1

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->dtvEventListener:Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;

    invoke-direct {v0}, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;-><init>()V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->dtvEventListener:Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;

    :cond_0
    sget-object v0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->dtvEventListener:Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;

    return-object v0
.end method


# virtual methods
.method public onAudioModeChange(IZ)Z
    .locals 4
    .param p1    # I
    .param p2    # Z

    const-string v2, "TvApp"

    const-string v3, "onAudioModeChange in DeskDtvPlayerEventListener"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    iput p1, v1, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "AudioModeChange"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public onChangeTtxStatus(IZ)Z
    .locals 4
    .param p1    # I
    .param p2    # Z

    const-string v2, "TvApp"

    const-string v3, "onChangeTtxStatus in DeskDtvPlayerEventListener"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    iput p1, v1, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "ChangeTtxStatus"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public onCiLoadCredentialFail(I)Z
    .locals 3
    .param p1    # I

    const-string v1, "TvApp"

    const-string v2, "onCiLoadCredentialFail in DeskDtvPlayerEventListener"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput p1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public onDtvAutoTuningScanInfo(ILcom/mstar/android/tvapi/dtv/vo/DtvEventScan;)Z
    .locals 6
    .param p1    # I
    .param p2    # Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;

    const-string v3, "TvApp"

    const-string v4, "onDtvAutoTuningScanInfo"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v3}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v3

    iput v3, v2, Landroid/os/Message;->what:I

    move-object v1, p2

    const-string v3, "dtvSrvCount"

    iget-short v4, v1, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->dtvSrvCount:S

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v3, "radioSrvCount"

    iget-short v4, v1, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->radioSrvCount:S

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v3, "dataSrvCount"

    iget-short v4, v1, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->dataSrvCount:S

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v3, "percent"

    iget-short v4, v1, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->scanPercentageNum:S

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v3, "quality"

    iget-short v4, v1, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->signalQuality:S

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v3, "strength"

    iget-short v4, v1, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->signalStrength:S

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v3, "scanstatus"

    iget v4, v1, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->scanStatus:I

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v3, "curFre"

    iget v4, v1, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->currFrequency:I

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v3, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const-string v3, "TvApp"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "scanstatus"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v1, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->scanStatus:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "TvApp"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "dtvSrvCount"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-short v5, v1, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->dtvSrvCount:S

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "TvApp"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "quality"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-short v5, v1, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->signalQuality:S

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "TvApp"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "strength"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-short v5, v1, Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;->signalStrength:S

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x1

    goto/16 :goto_0
.end method

.method public onDtvAutoUpdateScan(I)Z
    .locals 3
    .param p1    # I

    const-string v1, "TvApp"

    const-string v2, "onDtvAutoUpdateScan in DeskDtvPlayerEventListener"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput p1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public onDtvChannelNameReady(I)Z
    .locals 4
    .param p1    # I

    const-string v2, "TvApp"

    const-string v3, "onDtvChannelNameReady"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    const/16 v2, 0x41

    iput v2, v1, Landroid/os/Message;->what:I

    const-string v2, "CmdIndex"

    sget-object v3, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_DTV_CHANNELNAME_READY:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->ordinal()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public onDtvPriComponentMissing(I)Z
    .locals 3
    .param p1    # I

    const-string v1, "TvApp"

    const-string v2, "onDtvPriComponentMissing in DeskDtvPlayerEventListener"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput p1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public onDtvProgramInfoReady(I)Z
    .locals 4
    .param p1    # I

    const-string v2, "TvApp"

    const-string v3, "onDtvProgramInfoReady"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    const/16 v2, 0x41

    iput v2, v1, Landroid/os/Message;->what:I

    const-string v2, "CmdIndex"

    sget-object v3, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_DTV_PROGRAM_INFO_READY:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->ordinal()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public onEpgTimerSimulcast(II)Z
    .locals 4
    .param p1    # I
    .param p2    # I

    const-string v2, "TvApp"

    const-string v3, "onEpgTimerSimulcast in DeskDtvPlayerEventListener"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    iput p1, v1, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "EpgTimerSimulcast"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public onGingaStatusMode(IZ)Z
    .locals 4
    .param p1    # I
    .param p2    # Z

    const-string v2, "TvApp"

    const-string v3, "onGingaStatusMode in DeskDtvPlayerEventListener"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    iput p1, v1, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "GingaStatusMode"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public onHbbtvStatusMode(IZ)Z
    .locals 4
    .param p1    # I
    .param p2    # Z

    const-string v2, "TvApp"

    const-string v3, "onHbbtvStatusMode in DeskDtvPlayerEventListener"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    iput p1, v1, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "HbbtvStatusMode"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public onMheg5EventHandler(II)Z
    .locals 4
    .param p1    # I
    .param p2    # I

    const-string v2, "TvApp"

    const-string v3, "onMheg5EventHandler in DeskDtvPlayerEventListener"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    iput p1, v1, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "Mheg5EventHandler"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public onMheg5ReturnKey(II)Z
    .locals 4
    .param p1    # I
    .param p2    # I

    const-string v2, "TvApp"

    const-string v3, "onMheg5ReturnKey in DeskDtvPlayerEventListener"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    iput p1, v1, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "Mheg5ReturnKey"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public onMheg5StatusMode(II)Z
    .locals 4
    .param p1    # I
    .param p2    # I

    const-string v2, "TvApp"

    const-string v3, "onMheg5StatusMode in DeskDtvPlayerEventListener"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_MHEG5_STATUS_MODE:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    const-string v2, "Mheg5StatusMode"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v2, "onMheg5StatusMode"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public onOadDownload(II)Z
    .locals 4
    .param p1    # I
    .param p2    # I

    const-string v2, "TvApp"

    const-string v3, "onOadDownload in DeskDtvPlayerEventListener"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    iput p1, v1, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "OadDownload"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public onOadHandler(III)Z
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const-string v2, "TvApp"

    const-string v3, "onOadHandler in DeskDtvPlayerEventListener"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    iput p1, v1, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "OadHandler"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public onOadTimeout(II)Z
    .locals 4
    .param p1    # I
    .param p2    # I

    const-string v2, "TvApp"

    const-string v3, "onOadTimeout in DeskDtvPlayerEventListener"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    iput p1, v1, Landroid/os/Message;->what:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "OadTimeout"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public onPopupScanDialogFrequencyChange(I)Z
    .locals 3
    .param p1    # I

    const-string v1, "TvApp"

    const-string v2, "onPopupScanDialogFrequencyChange in DeskDtvPlayerEventListener"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput p1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public onPopupScanDialogLossSignal(I)Z
    .locals 3
    .param p1    # I

    const-string v1, "TvApp"

    const-string v2, "onPopupScanDialogLossSignal in DeskDtvPlayerEventListener"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput p1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public onPopupScanDialogNewMultiplex(I)Z
    .locals 3
    .param p1    # I

    const-string v1, "TvApp"

    const-string v2, "onPopupScanDialogNewMultiplex in DeskDtvPlayerEventListener"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput p1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public onRctPresence(I)Z
    .locals 3
    .param p1    # I

    const-string v1, "TvApp"

    const-string v2, "onRctPresence in DeskDtvPlayerEventListener"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput p1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public onSignalLock(I)Z
    .locals 4

    const-string v0, "TvApp"

    const-string v1, "onSignalLock in DeskDtvPlayerEventListener"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    sget-object v1, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_SIGNAL_LOCK:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->ordinal()I

    move-result v1

    iput v1, v0, Landroid/os/Message;->what:I

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "MsgSource"

    const-string v3, "DeskDtvPlayerEventListener"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onSignalUnLock(I)Z
    .locals 4

    const-string v0, "TvApp"

    const-string v1, "onSignalUnLock in DeskDtvPlayerEventListener"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    sget-object v1, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->EV_SIGNAL_UNLOCK:Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/CommonDesk$EnumDeskEvent;->ordinal()I

    move-result v1

    iput v1, v0, Landroid/os/Message;->what:I

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "MsgSource"

    const-string v3, "DeskDtvPlayerEventListener"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onTsChange(I)Z
    .locals 3
    .param p1    # I

    const-string v1, "TvApp"

    const-string v2, "onTsChange in DeskDtvPlayerEventListener"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    iput p1, v0, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/konka/kkimplements/tv/mstar/DeskDtvPlayerEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v1, 0x1

    goto :goto_0
.end method
