.class public Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;
.super Ljava/lang/Object;
.source "DeskCiEventListener.java"

# interfaces
.implements Lcom/mstar/android/tvapi/dtv/common/CiManager$OnCiEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;
    }
.end annotation


# static fields
.field private static ciEventListener:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;


# instance fields
.field private m_handler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;->ciEventListener:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;->m_handler:Landroid/os/Handler;

    iput-object v0, p0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;->m_handler:Landroid/os/Handler;

    return-void
.end method

.method public static getInstance()Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;
    .locals 1

    sget-object v0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;->ciEventListener:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;

    invoke-direct {v0}, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;-><init>()V

    sput-object v0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;->ciEventListener:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;

    :cond_0
    sget-object v0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;->ciEventListener:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;

    return-object v0
.end method


# virtual methods
.method public onUiAutotestMessageShown(Lcom/mstar/android/tvapi/dtv/common/CiManager;I)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CiManager;
    .param p2    # I

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v3

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->EV_CIMMI_UI_AUTOTEST_MESSAGE_SHOWN:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onUiCardInserted(Lcom/mstar/android/tvapi/dtv/common/CiManager;I)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CiManager;
    .param p2    # I

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v3

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->EV_CIMMI_UI_CARD_INSERTED:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onUiCardRemoved(Lcom/mstar/android/tvapi/dtv/common/CiManager;I)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CiManager;
    .param p2    # I

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v3

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->EV_CIMMI_UI_CARD_REMOVED:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onUiCloseMmi(Lcom/mstar/android/tvapi/dtv/common/CiManager;I)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CiManager;
    .param p2    # I

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v3

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->EV_CIMMI_UI_CLOSEMMI:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public onUiDataReady(Lcom/mstar/android/tvapi/dtv/common/CiManager;I)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CiManager;
    .param p2    # I

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;->m_handler:Landroid/os/Handler;

    if-nez v2, :cond_0

    :goto_0
    return v3

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    sget-object v2, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->EV_CIMMI_UI_DATA_READY:Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;

    invoke-virtual {v2}, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener$EVENT;->ordinal()I

    move-result v2

    iput v2, v1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    iget-object v2, p0, Lcom/konka/kkimplements/tv/mstar/DeskCiEventListener;->m_handler:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
