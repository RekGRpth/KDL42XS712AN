.class public final enum Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;
.super Ljava/lang/Enum;
.source "IFactoryDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/factory/desk/IFactoryDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MS_NLA_SET_INDEX"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

.field public static final enum EN_NLA_BACKLIGHT:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

.field public static final enum EN_NLA_BRIGHTNESS:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

.field public static final enum EN_NLA_CONTRAST:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

.field public static final enum EN_NLA_HUE:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

.field public static final enum EN_NLA_NUMS:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

.field public static final enum EN_NLA_SATURATION:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

.field public static final enum EN_NLA_SHARPNESS:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

.field public static final enum EN_NLA_VOLUME:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    const-string v1, "EN_NLA_VOLUME"

    invoke-direct {v0, v1, v3}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->EN_NLA_VOLUME:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    const-string v1, "EN_NLA_BRIGHTNESS"

    invoke-direct {v0, v1, v4}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->EN_NLA_BRIGHTNESS:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    const-string v1, "EN_NLA_CONTRAST"

    invoke-direct {v0, v1, v5}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->EN_NLA_CONTRAST:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    const-string v1, "EN_NLA_SATURATION"

    invoke-direct {v0, v1, v6}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->EN_NLA_SATURATION:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    const-string v1, "EN_NLA_SHARPNESS"

    invoke-direct {v0, v1, v7}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->EN_NLA_SHARPNESS:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    const-string v1, "EN_NLA_HUE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->EN_NLA_HUE:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    const-string v1, "EN_NLA_BACKLIGHT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->EN_NLA_BACKLIGHT:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    const-string v1, "EN_NLA_NUMS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->EN_NLA_NUMS:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->EN_NLA_VOLUME:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->EN_NLA_BRIGHTNESS:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->EN_NLA_CONTRAST:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    aput-object v1, v0, v5

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->EN_NLA_SATURATION:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    aput-object v1, v0, v6

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->EN_NLA_SHARPNESS:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->EN_NLA_HUE:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->EN_NLA_BACKLIGHT:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->EN_NLA_NUMS:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->$VALUES:[Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;
    .locals 1

    const-class v0, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    return-object v0
.end method

.method public static values()[Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;
    .locals 1

    sget-object v0, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->$VALUES:[Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v0}, [Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    return-object v0
.end method
