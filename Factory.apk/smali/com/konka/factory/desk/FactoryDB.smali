.class public Lcom/konka/factory/desk/FactoryDB;
.super Ljava/lang/Object;
.source "FactoryDB.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/factory/desk/FactoryDB$1;,
        Lcom/konka/factory/desk/FactoryDB$USER_SETTING_FIELD;
    }
.end annotation


# static fields
.field public static PersionCustomer:Ljava/lang/String;

.field private static instance:Lcom/konka/factory/desk/FactoryDB;

.field public static mDexpCustomer:Ljava/lang/String;


# instance fields
.field private context:Landroid/content/Context;

.field private cr:Landroid/content/ContentResolver;

.field public curCustomer:Ljava/lang/String;

.field private factoryCusSchema:Ljava/lang/String;

.field private final factorySchema:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private userSettingCusSchema:Ljava/lang/String;

.field private userSettingSchema:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "snowa"

    sput-object v0, Lcom/konka/factory/desk/FactoryDB;->PersionCustomer:Ljava/lang/String;

    const-string v0, "DEXP"

    sput-object v0, Lcom/konka/factory/desk/FactoryDB;->mDexpCustomer:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/factory/desk/FactoryDB;->mContext:Landroid/content/Context;

    const-string v0, "content://mstar.tv.factory"

    iput-object v0, p0, Lcom/konka/factory/desk/FactoryDB;->factorySchema:Ljava/lang/String;

    const-string v0, "content://mstar.tv.usersetting"

    iput-object v0, p0, Lcom/konka/factory/desk/FactoryDB;->userSettingSchema:Ljava/lang/String;

    const-string v0, "content://konka.tv.usersetting"

    iput-object v0, p0, Lcom/konka/factory/desk/FactoryDB;->userSettingCusSchema:Ljava/lang/String;

    const-string v0, "content://konka.tv.factory"

    iput-object v0, p0, Lcom/konka/factory/desk/FactoryDB;->factoryCusSchema:Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/factory/desk/FactoryDB;->context:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    return-void
.end method

.method private getContentResolver()Landroid/content/ContentResolver;
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDB;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    :cond_0
    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/konka/factory/desk/FactoryDB;
    .locals 1
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/konka/factory/desk/FactoryDB;->instance:Lcom/konka/factory/desk/FactoryDB;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/factory/desk/FactoryDB;

    invoke-direct {v0, p0}, Lcom/konka/factory/desk/FactoryDB;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/konka/factory/desk/FactoryDB;->instance:Lcom/konka/factory/desk/FactoryDB;

    :cond_0
    sget-object v0, Lcom/konka/factory/desk/FactoryDB;->instance:Lcom/konka/factory/desk/FactoryDB;

    return-object v0
.end method


# virtual methods
.method public closeDB()V
    .locals 0

    return-void
.end method

.method public openDB()V
    .locals 0

    return-void
.end method

.method public queryADCAdjust(I)Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;
    .locals 8
    .param p1    # I

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "content://mstar.tv.factory/adcadjust/sourceid/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    new-instance v7, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    invoke-direct {v7}, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;-><init>()V

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "u16RedGain"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->redGain:I

    const-string v0, "u16GreenGain"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->greenGain:I

    const-string v0, "u16BlueGain"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->blueGain:I

    const-string v0, "u16RedOffset"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->redOffset:I

    const-string v0, "u16GreenOffset"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->greenOffset:I

    const-string v0, "u16BlueOffset"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->blueOffset:I

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v7
.end method

.method public queryArcMode(I)I
    .locals 8
    .param p1    # I

    const/4 v2, 0x0

    const/4 v7, -0x1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDB;->userSettingSchema:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "/videosetting/inputsrc/"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "enARCType"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return v7
.end method

.method public queryColorTmpIdx(II)I
    .locals 6

    const/4 v2, 0x0

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDB;->userSettingCusSchema:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/picmode_setting/inputsrc/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/picmode/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    const/4 v0, -0x1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v0, "eColorTemp"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return v0
.end method

.method public queryCurInputSrc()I
    .locals 7

    const/4 v6, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_KTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v0, v1, :cond_1

    :cond_0
    const/16 v0, 0x22

    :goto_0
    return v0

    :cond_1
    invoke-direct {p0}, Lcom/konka/factory/desk/FactoryDB;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/systemsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "enInputSourceType"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    :goto_1
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    :goto_2
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v1, v0

    move v0, v6

    goto :goto_2

    :cond_2
    move v0, v6

    goto :goto_1
.end method

.method public queryCustomerCfgMiscSetting()Lcom/konka/factory/desk/IFactoryDesk$CustomerCfgMiscSetting;
    .locals 7

    const/4 v2, 0x0

    new-instance v6, Lcom/konka/factory/desk/IFactoryDesk$CustomerCfgMiscSetting;

    invoke-direct {v6}, Lcom/konka/factory/desk/IFactoryDesk$CustomerCfgMiscSetting;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDB;->factoryCusSchema:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/miscsetting/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "EnergyEnable"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v6, Lcom/konka/factory/desk/IFactoryDesk$CustomerCfgMiscSetting;->energyEnable:Z

    const-string v1, "EnergyPercent"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v6, Lcom/konka/factory/desk/IFactoryDesk$CustomerCfgMiscSetting;->energyPercent:S

    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return-object v6
.end method

.method public queryFactoryColorTempData(I)Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMP_DATA;
    .locals 9

    const/4 v2, 0x0

    const/4 v8, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://mstar.tv.factory/factorycolortemp/colortemperatureid/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    const-string v5, "ColorTemperatureID"

    move-object v3, v2

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMP_DATA;

    move v1, v8

    move v2, v8

    move v3, v8

    move v4, v8

    move v5, v8

    move v6, v8

    invoke-direct/range {v0 .. v6}, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMP_DATA;-><init>(SSSSSS)V

    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "u8RedGain"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMP_DATA;->redgain:S

    const-string v1, "u8GreenGain"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMP_DATA;->greengain:S

    const-string v1, "u8BlueGain"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMP_DATA;->bluegain:S

    const-string v1, "u8RedOffset"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMP_DATA;->redoffset:S

    const-string v1, "u8GreenOffset"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMP_DATA;->greenoffset:S

    const-string v1, "u8BlueOffset"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v0, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMP_DATA;->blueoffset:S

    :cond_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    return-object v0
.end method

.method public queryFactoryColorTempExData()Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;
    .locals 9

    const/4 v7, 0x0

    new-instance v8, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;

    invoke-direct {v8}, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;-><init>()V

    move v6, v7

    :goto_0
    sget-object v0, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_NUM:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    invoke-virtual {v0}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v0

    if-ge v6, v0, :cond_2

    const-string v3, " InputSourceID = ? "

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDB;->factoryCusSchema:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/factorycolortempex"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const-string v5, "ColorTemperatureID"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    move v0, v7

    :goto_1
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_NUM:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

    invoke-virtual {v2}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;->ordinal()I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v8, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v2, v2, v0

    aget-object v2, v2, v6

    const-string v3, "u16RedGain"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;->redgain:I

    iget-object v2, v8, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v2, v2, v0

    aget-object v2, v2, v6

    const-string v3, "u16GreenGain"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;->greengain:I

    iget-object v2, v8, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v2, v2, v0

    aget-object v2, v2, v6

    const-string v3, "u16BlueGain"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;->bluegain:I

    iget-object v2, v8, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v2, v2, v0

    aget-object v2, v2, v6

    const-string v3, "u16RedOffset"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;->redoffset:I

    iget-object v2, v8, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v2, v2, v0

    aget-object v2, v2, v6

    const-string v3, "u16GreenOffset"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;->greenoffset:I

    iget-object v2, v8, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v2, v2, v0

    aget-object v2, v2, v6

    const-string v3, "u16BlueOffset"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;->blueoffset:I

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto/16 :goto_0

    :cond_2
    return-object v8
.end method

.method public queryFactoryExtern()Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;
    .locals 10

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v2, 0x0

    const-string v0, "content://mstar.tv.factory/factoryextern"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    new-instance v7, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

    invoke-direct {v7}, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;-><init>()V

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "SoftWareVersion"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->softVersion:Ljava/lang/String;

    const-string v0, "BoardType"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->boardType:Ljava/lang/String;

    const-string v0, "PanelType"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->panelType:Ljava/lang/String;

    const-string v0, "CompileTime"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->dayAndTime:Ljava/lang/String;

    const-string v0, "TestPatternMode"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->testPatternMode:I

    const-string v0, "stPowerMode"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->stPowerMode:I

    const-string v0, "DtvAvAbnormalDelay"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_1

    move v0, v8

    :goto_0
    iput-boolean v0, v7, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->dtvAvAbnormalDelay:Z

    const-string v0, "FactoryPreSetFeature"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->factoryPreset:I

    const-string v0, "PanelSwing"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->panelSwingVal:S

    const-string v0, "AudioPrescale"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->audioPreScale:S

    const-string v0, "vdDspVersion"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->vdDspVersion:S

    const-string v0, "eHidevMode"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput v0, v7, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->eHidevMode:I

    const-string v0, "audioNrThr"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->audioNrThr:S

    const-string v0, "audioSifThreshold"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->audioSifThreshold:S

    const-string v0, "audioDspVersion"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->audioDspVersion:S

    const-string v0, "m_bAgingMode"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_2

    move v0, v8

    :goto_1
    iput-boolean v0, v7, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->m_bAgingMode:Z

    const-string v0, "bBurnIn"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_3

    :goto_2
    iput-boolean v8, v7, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->bBurnIn:Z

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v7

    :cond_1
    move v0, v9

    goto/16 :goto_0

    :cond_2
    move v0, v9

    goto :goto_1

    :cond_3
    move v8, v9

    goto :goto_2
.end method

.method public queryNoStandSet()Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;
    .locals 8

    const/4 v2, 0x0

    new-instance v7, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-direct {v7}, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;-><init>()V

    const-string v0, "content://mstar.tv.factory/nonstandardadjust"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "u8AFEC_D4"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D4:S

    const-string v0, "u8AFEC_D5_Bit2"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D5_Bit2:S

    const-string v0, "u8AFEC_D8_Bit3210"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D8_Bit3210:S

    const-string v0, "u8AFEC_D9_Bit0"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D9_Bit0:S

    const-string v0, "u8AFEC_D7_LOW_BOUND"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D7_LOW_BOUND:S

    const-string v0, "u8AFEC_D7_HIGH_BOUND"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D7_HIGH_BOUND:S

    const-string v0, "u8AFEC_A0"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_A0:S

    const-string v0, "u8AFEC_A1"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_A1:S

    const-string v0, "u8AFEC_66_Bit76"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_66_Bit76:S

    const-string v0, "u8AFEC_6E_Bit7654"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_6E_Bit7654:S

    const-string v0, "u8AFEC_6E_Bit3210"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_6E_Bit3210:S

    const-string v0, "u8AFEC_44"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_44:S

    const-string v0, "u8AFEC_CB"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_CB:S

    const-string v0, "u8AFEC_43"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_43:S

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v7
.end method

.method public queryNoStandVifSet()Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;
    .locals 10

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v2, 0x0

    const-string v0, "content://mstar.tv.factory/nonstandardadjust"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    new-instance v7, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    invoke-direct {v7}, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;-><init>()V

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "VifTop"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifTop:S

    const-string v0, "VifVgaMaximum"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifVgaMaximum:I

    const-string v0, "VifCrKp"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifCrKp:S

    const-string v0, "VifCrKi"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifCrKi:S

    const-string v0, "VifCrKp1"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifCrKp1:S

    const-string v0, "VifCrKi1"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifCrKi1:S

    const-string v0, "VifCrKp2"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifCrKp2:S

    const-string v0, "VifCrKi2"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifCrKi2:S

    const-string v0, "VifAsiaSignalOption"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_1

    move v0, v8

    :goto_0
    iput-boolean v0, v7, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifAsiaSignalOption:Z

    const-string v0, "VifCrKpKiAdjust"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    if-nez v0, :cond_2

    move v0, v8

    :goto_1
    iput-boolean v0, v7, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifCrKpKiAdjust:Z

    const-string v0, "VifOverModulation"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_3

    :goto_2
    iput-boolean v8, v7, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifOverModulation:Z

    const-string v0, "VifClampgainGainOvNegative"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifClampgainGainOvNegative:I

    const-string v0, "ChinaDescramblerBox"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->ChinaDescramblerBox:S

    const-string v0, "VifDelayReduce"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifDelayReduce:S

    const-string v0, "VifCrThr"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput v0, v7, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifCrThr:I

    const-string v0, "VifVersion"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifVersion:S

    const-string v0, "VifACIAGCREF"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    int-to-short v0, v0

    iput-short v0, v7, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifACIAGCREF:S

    const-string v0, "GainDistributionThr"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v7, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->GainDistributionThr:I

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v7

    :cond_1
    move v0, v9

    goto :goto_0

    :cond_2
    move v0, v9

    goto :goto_1

    :cond_3
    move v8, v9

    goto :goto_2
.end method

.method public queryNonLinearAdjusts()Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;
    .locals 11

    const/4 v2, 0x0

    new-instance v9, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

    invoke-direct {v9}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;-><init>()V

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lcom/konka/factory/desk/FactoryDB;->factoryCusSchema:Ljava/lang/String;

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, "/nonlinearadjust"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "InputSrcType = "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/konka/factory/desk/FactoryDB;->queryCurInputSrc()I

    move-result v10

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v5, "CurveTypeIndex"

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v7, 0x0

    iget-object v0, v9, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;

    array-length v8, v0

    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    add-int/lit8 v0, v8, -0x1

    if-le v7, v0, :cond_1

    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    const-string v3, " InputSrcType = ? AND CurveTypeIndex = ? "

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    sget-object v5, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_VGA:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    invoke-virtual {v5}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v0, 0x1

    sget-object v5, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->EN_NLA_BACKLIGHT:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v5}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/konka/factory/desk/FactoryDB;->factoryCusSchema:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "/nonlinearadjust"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, v9, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->EN_NLA_BACKLIGHT:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v2}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v2

    aget-object v0, v0, v2

    const-string v2, "u8OSD_V0"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput v2, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;->u8OSD_V0:I

    iget-object v0, v9, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->EN_NLA_BACKLIGHT:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v2}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v2

    aget-object v0, v0, v2

    const-string v2, "u8OSD_V25"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput v2, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;->u8OSD_V25:I

    iget-object v0, v9, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->EN_NLA_BACKLIGHT:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v2}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v2

    aget-object v0, v0, v2

    const-string v2, "u8OSD_V50"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput v2, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;->u8OSD_V50:I

    iget-object v0, v9, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->EN_NLA_BACKLIGHT:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v2}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v2

    aget-object v0, v0, v2

    const-string v2, "u8OSD_V75"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput v2, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;->u8OSD_V75:I

    iget-object v0, v9, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->EN_NLA_BACKLIGHT:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v2}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v2

    aget-object v0, v0, v2

    const-string v2, "u8OSD_V100"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-short v2, v2

    iput v2, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;->u8OSD_V100:I

    goto :goto_1

    :cond_1
    iget-object v0, v9, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;

    aget-object v0, v0, v7

    const-string v5, "u8OSD_V0"

    invoke-interface {v6, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v6, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    iput v5, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;->u8OSD_V0:I

    iget-object v0, v9, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;

    aget-object v0, v0, v7

    const-string v5, "u8OSD_V25"

    invoke-interface {v6, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v6, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    iput v5, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;->u8OSD_V25:I

    iget-object v0, v9, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;

    aget-object v0, v0, v7

    const-string v5, "u8OSD_V50"

    invoke-interface {v6, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v6, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    iput v5, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;->u8OSD_V50:I

    iget-object v0, v9, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;

    aget-object v0, v0, v7

    const-string v5, "u8OSD_V75"

    invoke-interface {v6, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v6, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    iput v5, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;->u8OSD_V75:I

    iget-object v0, v9, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;

    aget-object v0, v0, v7

    const-string v5, "u8OSD_V100"

    invoke-interface {v6, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v6, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    iput v5, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;->u8OSD_V100:I

    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    return-object v9
.end method

.method public queryOverscanAdjusts(I)[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;
    .locals 26
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const/4 v1, 0x0

    check-cast v1, [[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    :goto_0
    return-object v1

    :pswitch_0
    const-string v1, "content://mstar.tv.factory/dtvoverscansetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "ResolutionTypeNum"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$MAX_DTV_Resolution_Info;->E_DTV_MAX:Lcom/konka/factory/desk/IFactoryDesk$MAX_DTV_Resolution_Info;

    invoke-virtual {v1}, Lcom/konka/factory/desk/IFactoryDesk$MAX_DTV_Resolution_Info;->ordinal()I

    move-result v17

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v18

    filled-new-array/range {v17 .. v18}, [I

    move-result-object v1

    const-class v3, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-static {v3, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, [[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    const/4 v12, 0x0

    :goto_1
    move/from16 v0, v17

    if-ge v12, v0, :cond_2

    const/4 v14, 0x0

    :goto_2
    move/from16 v0, v18

    if-ge v14, v0, :cond_1

    new-instance v13, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-direct {v13}, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;-><init>()V

    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "u16H_CapStart"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v13, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16H_CapStart:I

    const-string v1, "u16V_CapStart"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v13, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16V_CapStart:I

    const-string v1, "u8HCrop_Left"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    const-string v1, "u8HCrop_Right"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    const-string v1, "u8VCrop_Up"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    const-string v1, "u8VCrop_Down"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    :cond_0
    aget-object v1, v25, v12

    aput-object v13, v1, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    :cond_1
    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :goto_3
    move-object/from16 v1, v25

    goto/16 :goto_0

    :pswitch_1
    const-string v1, "content://mstar.tv.factory/hdmioverscansetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "ResolutionTypeNum"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->E_HDMI_MAX:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    invoke-virtual {v1}, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->ordinal()I

    move-result v19

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v20

    filled-new-array/range {v19 .. v20}, [I

    move-result-object v1

    const-class v3, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-static {v3, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, [[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    const/4 v12, 0x0

    :goto_4
    move/from16 v0, v19

    if-ge v12, v0, :cond_5

    const/4 v14, 0x0

    :goto_5
    move/from16 v0, v20

    if-ge v14, v0, :cond_4

    new-instance v13, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-direct {v13}, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;-><init>()V

    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "u16H_CapStart"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v13, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16H_CapStart:I

    const-string v1, "u16V_CapStart"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v13, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16V_CapStart:I

    const-string v1, "u8HCrop_Left"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    const-string v1, "u8HCrop_Right"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    const-string v1, "u8VCrop_Up"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    const-string v1, "u8VCrop_Down"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    :cond_3
    aget-object v1, v25, v12

    aput-object v13, v1, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_5

    :cond_4
    add-int/lit8 v12, v12, 0x1

    goto :goto_4

    :cond_5
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto/16 :goto_3

    :pswitch_2
    const-string v1, "content://mstar.tv.factory/ypbproverscansetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "ResolutionTypeNum"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$MAX_YPbPr_Resolution_Info;->E_YPbPr_MAX:Lcom/konka/factory/desk/IFactoryDesk$MAX_YPbPr_Resolution_Info;

    invoke-virtual {v1}, Lcom/konka/factory/desk/IFactoryDesk$MAX_YPbPr_Resolution_Info;->ordinal()I

    move-result v23

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v24

    filled-new-array/range {v23 .. v24}, [I

    move-result-object v1

    const-class v3, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-static {v3, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, [[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    const/4 v12, 0x0

    :goto_6
    move/from16 v0, v23

    if-ge v12, v0, :cond_8

    const/4 v14, 0x0

    :goto_7
    move/from16 v0, v24

    if-ge v14, v0, :cond_7

    new-instance v13, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-direct {v13}, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;-><init>()V

    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, "u16H_CapStart"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v13, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16H_CapStart:I

    const-string v1, "u16V_CapStart"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v13, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16V_CapStart:I

    const-string v1, "u8HCrop_Left"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    const-string v1, "u8HCrop_Right"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    const-string v1, "u8VCrop_Up"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    const-string v1, "u8VCrop_Down"

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    :cond_6
    aget-object v1, v25, v12

    aput-object v13, v1, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_7

    :cond_7
    add-int/lit8 v12, v12, 0x1

    goto :goto_6

    :cond_8
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto/16 :goto_3

    :pswitch_3
    const-string v1, "content://mstar.tv.factory/overscanadjust"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "FactoryOverScanType"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NUMS:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    invoke-virtual {v1}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v21

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v22

    filled-new-array/range {v21 .. v22}, [I

    move-result-object v1

    const-class v3, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-static {v3, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, [[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    const/4 v12, 0x0

    :goto_8
    move/from16 v0, v21

    if-ge v12, v0, :cond_b

    const/4 v14, 0x0

    :goto_9
    move/from16 v0, v22

    if-ge v14, v0, :cond_a

    new-instance v13, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-direct {v13}, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;-><init>()V

    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_9

    const-string v1, "u16H_CapStart"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v13, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16H_CapStart:I

    const-string v1, "u16V_CapStart"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v13, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16V_CapStart:I

    const-string v1, "u8HCrop_Left"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    const-string v1, "u8HCrop_Right"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    const-string v1, "u8VCrop_Up"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    const-string v1, "u8VCrop_Down"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    :cond_9
    aget-object v1, v25, v12

    aput-object v13, v1, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_9

    :cond_a
    add-int/lit8 v12, v12, 0x1

    goto :goto_8

    :cond_b
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto/16 :goto_3

    :pswitch_4
    const-string v1, "content://mstar.tv.factory/atvoverscansetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "ResolutionTypeNum"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NUMS:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    invoke-virtual {v1}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v15

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v1}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->ordinal()I

    move-result v16

    filled-new-array/range {v15 .. v16}, [I

    move-result-object v1

    const-class v3, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-static {v3, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, [[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    const/4 v12, 0x0

    :goto_a
    if-ge v12, v15, :cond_e

    const/4 v14, 0x0

    :goto_b
    move/from16 v0, v16

    if-ge v14, v0, :cond_d

    new-instance v13, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-direct {v13}, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;-><init>()V

    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_c

    const-string v1, "u16H_CapStart"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v13, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16H_CapStart:I

    const-string v1, "u16V_CapStart"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v13, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16V_CapStart:I

    const-string v1, "u8HCrop_Left"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    const-string v1, "u8HCrop_Right"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    const-string v1, "u8VCrop_Up"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    const-string v1, "u8VCrop_Down"

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v11, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-short v1, v1

    iput-short v1, v13, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    :cond_c
    aget-object v1, v25, v12

    aput-object v13, v1, v14

    add-int/lit8 v14, v14, 0x1

    goto :goto_b

    :cond_d
    add-int/lit8 v12, v12, 0x1

    goto :goto_a

    :cond_e
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public queryPEQAdjusts()Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;
    .locals 6

    const/4 v2, 0x0

    const-string v0, "content://mstar.tv.factory/peqadjust"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    new-instance v2, Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;

    invoke-direct {v2}, Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;-><init>()V

    const/4 v0, 0x0

    iget-object v3, v2, Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;

    array-length v3, v3

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_0

    add-int/lit8 v4, v3, -0x1

    if-le v0, v4, :cond_1

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v2

    :cond_1
    iget-object v4, v2, Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;

    aget-object v4, v4, v0

    const-string v5, "Band"

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    iput v5, v4, Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;->Band:I

    iget-object v4, v2, Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;

    aget-object v4, v4, v0

    const-string v5, "Gain"

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    iput v5, v4, Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;->Gain:I

    iget-object v4, v2, Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;

    aget-object v4, v4, v0

    const-string v5, "Foh"

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    iput v5, v4, Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;->Foh:I

    iget-object v4, v2, Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;

    aget-object v4, v4, v0

    const-string v5, "Fol"

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    iput v5, v4, Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;->Fol:I

    iget-object v4, v2, Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;

    aget-object v4, v4, v0

    const-string v5, "QValue"

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    iput v5, v4, Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;->QValue:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public queryPicModeSetting(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;II)I
    .locals 6

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDB;->userSettingCusSchema:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/picmode_setting/inputsrc/0/picmode/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    const-string v5, "PictureModeType"

    move-object v3, v2

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    const/4 v0, 0x0

    sget-object v2, Lcom/konka/factory/desk/FactoryDB$1;->$SwitchMap$com$konka$factory$desk$IFactoryDesk$EN_MS_VIDEOITEM:[I

    invoke-virtual {p1}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return v0

    :pswitch_0
    const-string v0, "u8Brightness"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    goto :goto_0

    :pswitch_1
    const-string v0, "u8Contrast"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    goto :goto_0

    :pswitch_2
    const-string v0, "u8Hue"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    goto :goto_0

    :pswitch_3
    const-string v0, "u8Saturation"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    goto :goto_0

    :pswitch_4
    const-string v0, "u8Sharpness"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    goto :goto_0

    :pswitch_5
    const-string v0, "u8Backlight"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public querySSCAdjust()Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    const-string v0, "content://mstar.tv.factory/sscadjust"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    new-instance v2, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;

    invoke-direct {v2}, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;-><init>()V

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Miu_SscEnable"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_1

    move v0, v6

    :goto_0
    iput-boolean v0, v2, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Miu_SscEnable:Z

    const-string v0, "Lvds_SscEnable"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_2

    :goto_1
    iput-boolean v6, v2, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Lvds_SscEnable:Z

    const-string v0, "Lvds_SscSpan"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v2, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Lvds_SscSpan:I

    const-string v0, "Lvds_SscStep"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v2, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Lvds_SscStep:I

    const-string v0, "Miu_SscSpan"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v2, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Miu0_SscSpan:I

    const-string v0, "Miu_SscStep"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v2, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Miu0_SscStep:I

    const-string v0, "Miu1_SscSpan"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v2, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Miu1_SscSpan:I

    const-string v0, "Miu1_SscStep"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v2, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Miu1_SscStep:I

    const-string v0, "Miu2_SscSpan"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v2, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Miu2_SscSpan:I

    const-string v0, "Miu2_SscStep"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v2, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Miu2_SscStep:I

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v2

    :cond_1
    move v0, v7

    goto :goto_0

    :cond_2
    move v6, v7

    goto :goto_1
.end method

.method public querySelfAdaptiveLevel(I)I
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method public querySoundEqSetting(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;I)I
    .locals 6

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDB;->userSettingCusSchema:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/soundmodesetting/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "EqBand"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "terry eq"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " value is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return v1
.end method

.method public queryUserSysSetting(Lcom/konka/factory/desk/FactoryDB$USER_SETTING_FIELD;)I
    .locals 8

    const/4 v7, -0x1

    const/4 v2, 0x0

    const-string v0, ""

    sget-object v0, Lcom/konka/factory/desk/FactoryDB$1;->$SwitchMap$com$konka$factory$desk$FactoryDB$USER_SETTING_FIELD:[I

    invoke-virtual {p1}, Lcom/konka/factory/desk/FactoryDB$USER_SETTING_FIELD;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return v7

    :pswitch_0
    const-string v0, "bEnableWDT"

    move-object v6, v0

    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDB;->userSettingSchema:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/systemsetting"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    :goto_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move v7, v0

    goto :goto_0

    :pswitch_1
    const-string v0, "bUartBus"

    move-object v6, v0

    goto :goto_1

    :pswitch_2
    const-string v0, "bTeletext"

    move-object v6, v0

    goto :goto_1

    :cond_0
    move v0, v7

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public queryePicMode(I)I
    .locals 6

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDB;->userSettingSchema:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/videosetting/inputsrc/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    const/4 v0, -0x1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v0, "ePicture"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return v0
.end method

.method public restoreToDefault()V
    .locals 5

    :try_start_0
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "defaultconfig yes"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    const-string v0, "setEnvironment"

    const-string v1, "UARTOnOff off"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    const-string v1, "UARTOnOff"

    const-string v2, "off"

    invoke-virtual {v0, v1, v2}, Lcom/mstar/android/tvapi/common/TvManager;->setEnvironment(Ljava/lang/String;Ljava/lang/String;)Z

    const-string v0, "setEnvironment"

    const-string v1, "game mode disable"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    const-string v1, "game_mode"

    const-string v2, "disable"

    invoke-virtual {v0, v1, v2}, Lcom/mstar/android/tvapi/common/TvManager;->setEnvironment(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_0
    :try_start_2
    new-instance v0, Ljava/io/FileInputStream;

    const-string v1, "/tvdatabase/DatabaseBackup/factory.db"

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/io/FileOutputStream;

    const-string v2, "/tvdatabase/Database/factory.db"

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    const/16 v2, 0x2000

    new-array v2, v2, [B

    :goto_1
    invoke-virtual {v0, v2}, Ljava/io/FileInputStream;->read([B)I

    move-result v3

    if-lez v3, :cond_0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4, v3}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "defaultconfig fail!"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    :goto_2
    return-void

    :catch_1
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V

    new-instance v0, Ljava/io/FileInputStream;

    const-string v1, "/customercfg/panel/factorybackup.db"

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/io/FileOutputStream;

    const-string v3, "/customercfg/panel/factory.db"

    invoke-direct {v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    :goto_3
    invoke-virtual {v0, v2}, Ljava/io/FileInputStream;->read([B)I

    move-result v3

    if-lez v3, :cond_1

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4, v3}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_3

    :cond_1
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V

    new-instance v0, Ljava/io/FileInputStream;

    const-string v1, "/tvdatabase/DatabaseBackup/user_setting.db"

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/io/FileOutputStream;

    const-string v3, "/tvdatabase/Database/user_setting.db"

    invoke-direct {v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    :goto_4
    invoke-virtual {v0, v2}, Ljava/io/FileInputStream;->read([B)I

    move-result v3

    if-lez v3, :cond_2

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4, v3}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_4

    :cond_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V

    new-instance v0, Ljava/io/FileInputStream;

    const-string v1, "/customercfg/user_settingbackup.db"

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/io/FileOutputStream;

    const-string v3, "/customercfg/user_setting.db"

    invoke-direct {v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    :goto_5
    invoke-virtual {v0, v2}, Ljava/io/FileInputStream;->read([B)I

    move-result v3

    if-lez v3, :cond_3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4, v3}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_5

    :cond_3
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDB;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    check-cast v0, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getCustomerInfo()Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;->strCustomerName:Ljava/lang/String;

    iput-object v0, p0, Lcom/konka/factory/desk/FactoryDB;->curCustomer:Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDB;->curCustomer:Ljava/lang/String;

    sget-object v1, Lcom/konka/factory/desk/FactoryDB;->PersionCustomer:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "restoreToDefault"

    const-string v1, "restoreToDefault ,customer is Snowa"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/io/FileInputStream;

    const-string v1, "/tvdatabase/DatabaseBackup/atv_cmdb_snowa_factory.bin"

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/io/FileOutputStream;

    const-string v3, "/tvdatabase/Database/atv_cmdb.bin"

    invoke-direct {v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    :goto_6
    invoke-virtual {v0, v2}, Ljava/io/FileInputStream;->read([B)I

    move-result v3

    if-lez v3, :cond_4

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4, v3}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_6

    :cond_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V

    :goto_7
    new-instance v0, Ljava/io/FileInputStream;

    const-string v1, "/tvdatabase/DatabaseBackup/atv_cmdb_cable_factory.bin"

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/io/FileOutputStream;

    const-string v3, "/tvdatabase/Database/atv_cmdb_cable.bin"

    invoke-direct {v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    :goto_8
    invoke-virtual {v0, v2}, Ljava/io/FileInputStream;->read([B)I

    move-result v3

    if-lez v3, :cond_a

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4, v3}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_8

    :cond_5
    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDB;->curCustomer:Ljava/lang/String;

    sget-object v1, Lcom/konka/factory/desk/FactoryDB;->mDexpCustomer:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "restoreToDefault"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "restoreToDefault ,customer is:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/konka/factory/desk/FactoryDB;->mDexpCustomer:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/io/FileInputStream;

    const-string v1, "/tvdatabase/DatabaseBackup/atv_cmdb_dexp_factory.bin"

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/io/FileOutputStream;

    const-string v3, "/tvdatabase/Database/atv_cmdb.bin"

    invoke-direct {v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    :goto_9
    invoke-virtual {v0, v2}, Ljava/io/FileInputStream;->read([B)I

    move-result v3

    if-lez v3, :cond_6

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4, v3}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_9

    :cond_6
    new-instance v0, Ljava/io/FileInputStream;

    const-string v1, "/tvdatabase/DatabaseBackup/dtv_cmdb_2_dexp_factory.bin"

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/io/FileOutputStream;

    const-string v3, "/tvdatabase/Database/dtv_cmdb_2.bin"

    invoke-direct {v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    :goto_a
    invoke-virtual {v0, v2}, Ljava/io/FileInputStream;->read([B)I

    move-result v3

    if-lez v3, :cond_7

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4, v3}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_a

    :cond_7
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V

    goto :goto_7

    :cond_8
    new-instance v0, Ljava/io/FileInputStream;

    const-string v1, "/tvdatabase/DatabaseBackup/atv_cmdb_factory.bin"

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/io/FileOutputStream;

    const-string v3, "/tvdatabase/Database/atv_cmdb.bin"

    invoke-direct {v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    :goto_b
    invoke-virtual {v0, v2}, Ljava/io/FileInputStream;->read([B)I

    move-result v3

    if-lez v3, :cond_9

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4, v3}, Ljava/io/FileOutputStream;->write([BII)V

    goto :goto_b

    :cond_9
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V

    goto/16 :goto_7

    :cond_a
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V

    invoke-static {}, Lcom/konka/factory/TvRootApp;->getCommonSkin()Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v0

    const-string v1, "reboot"

    invoke-virtual {v0, v1}, Lcom/mstar/android/tv/TvCommonManager;->rebootSystem(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_2
.end method

.method public updateADCAdjust(Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;I)V
    .locals 8
    .param p1    # Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;
    .param p2    # I

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "u16RedGain"

    iget v6, p1, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->redGain:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "u16GreenGain"

    iget v6, p1, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->greenGain:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "u16BlueGain"

    iget v6, p1, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->blueGain:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "u16RedOffset"

    iget v6, p1, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->redOffset:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "u16GreenOffset"

    iget v6, p1, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->greenOffset:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "u16BlueOffset"

    iget v6, p1, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->blueOffset:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-wide/16 v1, -0x1

    :try_start_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "content://mstar.tv.factory/adcadjust/sourceid/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v5, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v5, v3, v4, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    int-to-long v1, v5

    :goto_0
    const-wide/16 v5, -0x1

    cmp-long v5, v1, v5

    if-nez v5, :cond_0

    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v5, "DataBaseDeskImpl"

    const-string v6, "update failed"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v5

    const/16 v6, 0x25

    invoke-virtual {v5, v6}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public updateATVOverscanAdjust(II[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V
    .locals 8
    .param p1    # I
    .param p2    # I
    .param p3    # [[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    const-wide/16 v1, -0x1

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "u16H_CapStart"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget v5, v5, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16H_CapStart:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u16V_CapStart"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget v5, v5, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16V_CapStart:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "u8HCrop_Left"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget-short v5, v5, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8HCrop_Right"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget-short v5, v5, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8VCrop_Up"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget-short v5, v5, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v4, "u8VCrop_Down"

    aget-object v5, p3, p1

    aget-object v5, v5, p2

    iget-short v5, v5, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    invoke-static {v5}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    :try_start_0
    invoke-direct {p0}, Lcom/konka/factory/desk/FactoryDB;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "content://mstar.tv.factory/atvoverscansetting/resolutiontypenum/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/_id/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v3, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v4

    int-to-long v1, v4

    :goto_0
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "update tbl_ATVOverscanSetting ignored"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x33

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v4

    goto :goto_0
.end method

.method public updateColorTempIdx(IILcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;)V
    .locals 7

    const-wide/16 v2, -0x1

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "eColorTemp"

    invoke-virtual {p3}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;->ordinal()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const/4 v1, 0x0

    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/konka/factory/desk/FactoryDB;->userSettingCusSchema:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/picmode_setting/inputsrc/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/picmode/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v4, v1, v0, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    int-to-long v0, v0

    :goto_0
    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v0, "DataBaseDeskImpl"

    const-string v1, "update failed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v0, v2

    goto :goto_0

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v0

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public updateCustomerCfgMiscSetting(Lcom/konka/factory/desk/IFactoryDesk$CustomerCfgMiscSetting;)V
    .locals 7

    const-wide/16 v2, -0x1

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "EnergyEnable"

    iget-boolean v4, p1, Lcom/konka/factory/desk/IFactoryDesk$CustomerCfgMiscSetting;->energyEnable:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v1, "EnergyPercent"

    iget-short v4, p1, Lcom/konka/factory/desk/IFactoryDesk$CustomerCfgMiscSetting;->energyPercent:S

    invoke-static {v4}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDB;->factoryCusSchema:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "/miscsetting/"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v4, v1, v0, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    int-to-long v0, v0

    :goto_0
    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "DataBaseDeskImpl"

    const-string v1, "update failed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v0, v2

    goto :goto_0
.end method

.method public updateDTVOverscanAdjust(II[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V
    .locals 9
    .param p1    # I
    .param p2    # I
    .param p3    # [[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    const-string v7, "u16H_CapStart"

    aget-object v8, p3, p1

    aget-object v8, v8, p2

    iget v8, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16H_CapStart:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v7, "u16V_CapStart"

    aget-object v8, p3, p1

    aget-object v8, v8, p2

    iget v8, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16V_CapStart:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v7, "u8HCrop_Left"

    aget-object v8, p3, p1

    aget-object v8, v8, p2

    iget-short v8, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v7, "u8HCrop_Right"

    aget-object v8, p3, p1

    aget-object v8, v8, p2

    iget-short v8, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v7, "u8VCrop_Up"

    aget-object v8, p3, p1

    aget-object v8, v8, p2

    iget-short v8, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v7, "u8VCrop_Down"

    aget-object v8, p3, p1

    aget-object v8, v8, p2

    iget-short v8, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-wide/16 v1, -0x1

    :try_start_0
    const-string v7, "content://mstar.tv.factory/dtvoverscansetting"

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const-string v6, " ResolutionTypeNum = ? and  _id = ? "

    const/4 v7, 0x2

    new-array v3, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v3, v7

    const/4 v7, 0x1

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v3, v7

    iget-object v7, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    invoke-virtual {v7, v4, v5, v6, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    int-to-long v1, v7

    :goto_0
    const-wide/16 v7, -0x1

    cmp-long v7, v1, v7

    if-nez v7, :cond_0

    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v7, "DataBaseDeskImpl"

    const-string v8, "update failed"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v7

    const/16 v8, 0x31

    invoke-virtual {v7, v8}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public updateFactoryColorTempExData(Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;II)V
    .locals 8

    const-wide/16 v2, -0x1

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "u16RedGain"

    iget v4, p1, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;->redgain:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "u16GreenGain"

    iget v4, p1, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;->greengain:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "u16BlueGain"

    iget v4, p1, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;->bluegain:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "u16RedOffset"

    iget v4, p1, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;->redoffset:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "u16GreenOffset"

    iget v4, p1, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;->greenoffset:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "u16BlueOffset"

    iget v4, p1, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;->blueoffset:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    const-string v1, " InputSourceID = ? and ColorTemperatureID = ? "

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    iget-object v5, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/konka/factory/desk/FactoryDB;->factoryCusSchema:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/factorycolortempex"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6, v0, v1, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    int-to-long v0, v0

    :goto_0
    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v0, "DataBaseDeskImpl"

    const-string v1, "update failed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v0, v2

    goto :goto_0

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public updateFactoryExtern(Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;)V
    .locals 9
    .param p1    # Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

    const/4 v6, 0x1

    const/4 v7, 0x0

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "SoftWareVersion"

    sget-object v8, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->softVersion:Ljava/lang/String;

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "BoardType"

    sget-object v8, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->boardType:Ljava/lang/String;

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "PanelType"

    sget-object v8, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->panelType:Ljava/lang/String;

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "CompileTime"

    sget-object v8, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->dayAndTime:Ljava/lang/String;

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "TestPatternMode"

    iget v8, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->testPatternMode:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "stPowerMode"

    iget v8, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->stPowerMode:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v8, "DtvAvAbnormalDelay"

    iget-boolean v5, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->dtvAvAbnormalDelay:Z

    if-eqz v5, :cond_0

    move v5, v6

    :goto_0
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v8, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "FactoryPreSetFeature"

    iget v8, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->factoryPreset:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "PanelSwing"

    iget-short v8, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->panelSwingVal:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "AudioPrescale"

    iget-short v8, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->audioPreScale:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "vdDspVersion"

    iget-short v8, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->vdDspVersion:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "eHidevMode"

    iget v8, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->eHidevMode:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "audioNrThr"

    iget-short v8, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->audioNrThr:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "audioSifThreshold"

    iget-short v8, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->audioSifThreshold:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "audioDspVersion"

    iget-short v8, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->audioDspVersion:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v8, "bBurnIn"

    iget-boolean v5, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->bBurnIn:Z

    if-eqz v5, :cond_1

    move v5, v6

    :goto_1
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v8, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "m_bAgingMode"

    iget-boolean v8, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->m_bAgingMode:Z

    if-eqz v8, :cond_2

    :goto_2
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-wide/16 v1, -0x1

    :try_start_0
    const-string v5, "content://mstar.tv.factory/factoryextern"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v5, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v5, v3, v4, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    int-to-long v1, v5

    :goto_3
    const-wide/16 v5, -0x1

    cmp-long v5, v1, v5

    if-nez v5, :cond_3

    :goto_4
    return-void

    :cond_0
    move v5, v7

    goto/16 :goto_0

    :cond_1
    move v5, v7

    goto :goto_1

    :cond_2
    move v6, v7

    goto :goto_2

    :catch_0
    move-exception v0

    const-string v5, "DataBaseDeskImpl"

    const-string v6, "update failed"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_3
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v5

    const/16 v6, 0x28

    invoke-virtual {v5, v6}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_4

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_4
.end method

.method public updateHDMIOverscanAdjust(II[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V
    .locals 9
    .param p1    # I
    .param p2    # I
    .param p3    # [[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    const-string v7, "u16H_CapStart"

    aget-object v8, p3, p1

    aget-object v8, v8, p2

    iget v8, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16H_CapStart:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v7, "u16V_CapStart"

    aget-object v8, p3, p1

    aget-object v8, v8, p2

    iget v8, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16V_CapStart:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v7, "u8HCrop_Left"

    aget-object v8, p3, p1

    aget-object v8, v8, p2

    iget-short v8, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v7, "u8HCrop_Right"

    aget-object v8, p3, p1

    aget-object v8, v8, p2

    iget-short v8, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v7, "u8VCrop_Up"

    aget-object v8, p3, p1

    aget-object v8, v8, p2

    iget-short v8, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v7, "u8VCrop_Down"

    aget-object v8, p3, p1

    aget-object v8, v8, p2

    iget-short v8, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-wide/16 v1, -0x1

    :try_start_0
    const-string v7, "content://mstar.tv.factory/hdmioverscansetting"

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const-string v6, " ResolutionTypeNum = ? and _id = ? "

    const/4 v7, 0x2

    new-array v3, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v3, v7

    const/4 v7, 0x1

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v3, v7

    iget-object v7, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    invoke-virtual {v7, v4, v5, v6, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    int-to-long v1, v7

    :goto_0
    const-wide/16 v7, -0x1

    cmp-long v7, v1, v7

    if-nez v7, :cond_0

    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/database/SQLException;->printStackTrace()V

    const-string v7, "DataBaseDeskImpl"

    const-string v8, "update failed"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v7

    const/16 v8, 0x2f

    invoke-virtual {v7, v8}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public updateNonLinearAdjust(Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;I)V
    .locals 9
    .param p1    # Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;
    .param p2    # I

    const-wide/16 v1, -0x1

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "u8OSD_V0"

    iget v6, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;->u8OSD_V0:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "u8OSD_V25"

    iget v6, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;->u8OSD_V25:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "u8OSD_V50"

    iget v6, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;->u8OSD_V50:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "u8OSD_V75"

    iget v6, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;->u8OSD_V75:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "u8OSD_V100"

    iget v6, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;->u8OSD_V100:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {p0}, Lcom/konka/factory/desk/FactoryDB;->queryCurInputSrc()I

    move-result v3

    sget-object v5, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->EN_NLA_BACKLIGHT:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v5}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v5

    if-ne v5, p2, :cond_0

    sget-object v5, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_VGA:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    invoke-virtual {v5}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v3

    :cond_0
    :try_start_0
    iget-object v5, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/konka/factory/desk/FactoryDB;->factoryCusSchema:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/nonlinearadjust/inputsrctype/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/curvetypeindex/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v4, v7, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v5

    int-to-long v1, v5

    :goto_0
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "update tbl_NonLinearAdjust come here"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const-wide/16 v5, -0x1

    cmp-long v5, v1, v5

    if-nez v5, :cond_1

    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "update tbl_NonLinearAdjust ignored"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_1
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "update tbl_NonLinearAdjust come here111"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :try_start_1
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "update tbl_NonLinearAdjust come222 here"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v5

    const/16 v6, 0x2b

    invoke-virtual {v5, v6}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v5

    goto :goto_0
.end method

.method public updateNonStandardAdjust(Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;)V
    .locals 9
    .param p1    # Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    const/4 v6, 0x1

    const/4 v7, 0x0

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "VifTop"

    iget-short v8, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifTop:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "VifVgaMaximum"

    iget v8, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifVgaMaximum:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "VifCrKp"

    iget-short v8, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifCrKp:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "VifCrKi"

    iget-short v8, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifCrKi:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "VifCrKp1"

    iget-short v8, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifCrKp1:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "VifCrKi1"

    iget-short v8, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifCrKi1:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "VifCrKp2"

    iget-short v8, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifCrKp2:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "VifCrKi2"

    iget-short v8, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifCrKi2:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v8, "VifAsiaSignalOption"

    iget-boolean v5, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifAsiaSignalOption:Z

    if-eqz v5, :cond_0

    move v5, v6

    :goto_0
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v8, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v8, "VifCrKpKiAdjust"

    iget-boolean v5, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifCrKpKiAdjust:Z

    if-eqz v5, :cond_1

    move v5, v6

    :goto_1
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v8, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "VifOverModulation"

    iget-boolean v8, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifOverModulation:Z

    if-eqz v8, :cond_2

    :goto_2
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "VifClampgainGainOvNegative"

    iget v6, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifClampgainGainOvNegative:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "ChinaDescramblerBox"

    iget-short v6, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->ChinaDescramblerBox:S

    invoke-static {v6}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "VifDelayReduce"

    iget-short v6, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifDelayReduce:S

    invoke-static {v6}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "VifCrThr"

    iget v6, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifCrThr:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "VifVersion"

    iget-short v6, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifVersion:S

    invoke-static {v6}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "VifACIAGCREF"

    iget-short v6, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifACIAGCREF:S

    invoke-static {v6}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "GainDistributionThr"

    iget v6, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->GainDistributionThr:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-wide/16 v1, -0x1

    :try_start_0
    const-string v5, "content://mstar.tv.factory/nonstandardadjust"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v5, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v5, v3, v4, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    int-to-long v1, v5

    :goto_3
    const-wide/16 v5, -0x1

    cmp-long v5, v1, v5

    if-nez v5, :cond_3

    :goto_4
    return-void

    :cond_0
    move v5, v7

    goto/16 :goto_0

    :cond_1
    move v5, v7

    goto :goto_1

    :cond_2
    move v6, v7

    goto :goto_2

    :catch_0
    move-exception v0

    const-string v5, "DataBaseDeskImpl"

    const-string v6, "update failed"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_3
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v5

    const/16 v6, 0x29

    invoke-virtual {v5, v6}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_4

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_4
.end method

.method public updateNonStandardAdjust(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    .locals 8
    .param p1    # Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "u8AFEC_D4"

    iget-short v6, p1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D4:S

    invoke-static {v6}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "u8AFEC_D5_Bit2"

    iget-short v6, p1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D5_Bit2:S

    invoke-static {v6}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "u8AFEC_D8_Bit3210"

    iget-short v6, p1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_6E_Bit3210:S

    invoke-static {v6}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "u8AFEC_D9_Bit0"

    iget-short v6, p1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D9_Bit0:S

    invoke-static {v6}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "u8AFEC_D7_LOW_BOUND"

    iget-short v6, p1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D7_LOW_BOUND:S

    invoke-static {v6}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "u8AFEC_D7_HIGH_BOUND"

    iget-short v6, p1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D7_HIGH_BOUND:S

    invoke-static {v6}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "u8AFEC_A0"

    iget-short v6, p1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_A0:S

    invoke-static {v6}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "u8AFEC_A1"

    iget-short v6, p1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_A1:S

    invoke-static {v6}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "u8AFEC_66_Bit76"

    iget-short v6, p1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_66_Bit76:S

    invoke-static {v6}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "u8AFEC_6E_Bit7654"

    iget-short v6, p1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_6E_Bit7654:S

    invoke-static {v6}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "u8AFEC_6E_Bit3210"

    iget-short v6, p1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_6E_Bit3210:S

    invoke-static {v6}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "u8AFEC_43"

    iget-short v6, p1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_43:S

    invoke-static {v6}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "u8AFEC_44"

    iget-short v6, p1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_44:S

    invoke-static {v6}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "u8AFEC_CB"

    iget-short v6, p1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_CB:S

    invoke-static {v6}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-wide/16 v1, -0x1

    :try_start_0
    const-string v5, "content://mstar.tv.factory/nonstandardadjust"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v5, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v5, v3, v4, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-wide/16 v5, -0x1

    cmp-long v5, v1, v5

    if-nez v5, :cond_0

    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v5, "DataBaseDeskImpl"

    const-string v6, "update failed"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v5

    const/16 v6, 0x29

    invoke-virtual {v5, v6}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public updateOverscanAdjust(II[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V
    .locals 8
    .param p1    # I
    .param p2    # I
    .param p3    # [[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "u16H_CapStart"

    aget-object v6, p3, p1

    aget-object v6, v6, p2

    iget v6, v6, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16H_CapStart:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "u16V_CapStart"

    aget-object v6, p3, p1

    aget-object v6, v6, p2

    iget v6, v6, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16V_CapStart:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "u8HCrop_Left"

    aget-object v6, p3, p1

    aget-object v6, v6, p2

    iget-short v6, v6, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-static {v6}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "u8HCrop_Right"

    aget-object v6, p3, p1

    aget-object v6, v6, p2

    iget-short v6, v6, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    invoke-static {v6}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "u8VCrop_Up"

    aget-object v6, p3, p1

    aget-object v6, v6, p2

    iget-short v6, v6, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    invoke-static {v6}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v5, "u8VCrop_Down"

    aget-object v6, p3, p1

    aget-object v6, v6, p2

    iget-short v6, v6, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    invoke-static {v6}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-wide/16 v1, -0x1

    :try_start_0
    sget-object v5, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v6

    invoke-virtual {p0}, Lcom/konka/factory/desk/FactoryDB;->queryCurInputSrc()I

    move-result v7

    aget-object v6, v6, v7

    if-ne v5, v6, :cond_0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "content://mstar.tv.factory/atvoverscansetting/resolutiontypenum/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/_id/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    :goto_0
    iget-object v5, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v5, v3, v4, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    int-to-long v1, v5

    :goto_1
    const-wide/16 v5, -0x1

    cmp-long v5, v1, v5

    if-nez v5, :cond_1

    :goto_2
    return-void

    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "content://mstar.tv.factory/overscanadjust/factoryoverscantype/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/_id/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/database/SQLException;->printStackTrace()V

    const-string v5, "DataBaseDeskImpl"

    const-string v6, "update failed"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v5

    const/16 v6, 0x2c

    invoke-virtual {v5, v6}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2
.end method

.method public updatePEQAdjust(Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;I)V
    .locals 7

    const-wide/16 v2, -0x1

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "Band"

    iget v4, p1, Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;->Band:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "Gain"

    iget v4, p1, Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;->Gain:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "Foh"

    iget v4, p1, Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;->Foh:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "Fol"

    iget v4, p1, Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;->Fol:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "QValue"

    iget v4, p1, Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;->QValue:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "content://mstar.tv.factory/peqadjust/"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v4, v1, v0, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    int-to-long v0, v0

    :goto_0
    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v0, "DataBaseDeskImpl"

    const-string v1, "update failed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v0, v2

    goto :goto_0

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v0

    const/16 v1, 0x2d

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public updatePicModeSetting(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;III)V
    .locals 7

    const-wide/16 v2, -0x1

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    sget-object v1, Lcom/konka/factory/desk/FactoryDB$1;->$SwitchMap$com$konka$factory$desk$IFactoryDesk$EN_MS_VIDEOITEM:[I

    invoke-virtual {p1}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v4

    aget v1, v1, v4

    packed-switch v1, :pswitch_data_0

    :goto_0
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDB;->userSettingCusSchema:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "/picmode_setting/inputsrc/0/picmode/"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v4, v1, v0, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    int-to-long v0, v0

    :goto_1
    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    :goto_2
    return-void

    :pswitch_0
    const-string v1, "u8Brightness"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    :pswitch_1
    const-string v1, "u8Contrast"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    :pswitch_2
    const-string v1, "u8Hue"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    :pswitch_3
    const-string v1, "u8Saturation"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    :pswitch_4
    const-string v1, "u8Sharpness"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    :pswitch_5
    const-string v1, "u8Backlight"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "DataBaseDeskImpl"

    const-string v1, "update failed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v0, v2

    goto :goto_1

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v0

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public updatePictureMode(II)V
    .locals 7

    const-wide/16 v2, -0x1

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "ePicture"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDB;->userSettingSchema:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "/videosetting/inputsrc/"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v4, v1, v0, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    int-to-long v0, v0

    :goto_0
    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v0, "DataBaseDeskImpl"

    const-string v1, "update failed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v0, v2

    goto :goto_0

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v0

    const/16 v1, 0x22

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public updateSSCAdjust(Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;)V
    .locals 7

    const-wide/16 v3, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    const-string v6, "Miu_SscEnable"

    iget-boolean v0, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Miu_SscEnable:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "Lvds_SscEnable"

    iget-boolean v6, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Lvds_SscEnable:Z

    if-eqz v6, :cond_1

    :goto_1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "Lvds_SscSpan"

    iget v1, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Lvds_SscSpan:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "Lvds_SscStep"

    iget v1, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Lvds_SscStep:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "Miu_SscSpan"

    iget v1, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Miu0_SscSpan:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "Miu_SscStep"

    iget v1, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Miu0_SscStep:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "Miu1_SscSpan"

    iget v1, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Miu1_SscSpan:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "Miu1_SscStep"

    iget v1, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Miu1_SscStep:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "Miu2_SscSpan"

    iget v1, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Miu2_SscSpan:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "Miu2_SscStep"

    iget v1, p1, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Miu2_SscStep:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    const-string v0, "content://mstar.tv.factory/sscadjust"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    const/4 v2, 0x0

    const/4 v6, 0x0

    invoke-virtual {v1, v0, v5, v2, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    int-to-long v0, v0

    :goto_2
    cmp-long v0, v0, v3

    if-nez v0, :cond_2

    :goto_3
    return-void

    :cond_0
    move v0, v2

    goto/16 :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v0, "DataBaseDeskImpl"

    const-string v1, "update failed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v0, v3

    goto :goto_2

    :cond_2
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v0

    const/16 v1, 0x2a

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_3
.end method

.method public updateSelfAdaptiveLevel(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    return-void
.end method

.method public updateSoundEqSetting(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;II)I
    .locals 7

    const-wide/16 v2, -0x1

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "EqBand"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDB;->userSettingCusSchema:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "/soundmodesetting/"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;->ordinal()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v4, v1, v0, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    int-to-long v0, v0

    :goto_0
    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    :goto_1
    return p3

    :catch_0
    move-exception v0

    const-string v0, "DataBaseDeskImpl"

    const-string v1, "update failed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v0, v2

    goto :goto_0

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v0

    const/16 v1, 0x16

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public updateUserSysSetting(Lcom/konka/factory/desk/FactoryDB$USER_SETTING_FIELD;I)V
    .locals 7

    const-wide/16 v2, -0x1

    const-string v0, ""

    sget-object v0, Lcom/konka/factory/desk/FactoryDB$1;->$SwitchMap$com$konka$factory$desk$FactoryDB$USER_SETTING_FIELD:[I

    invoke-virtual {p1}, Lcom/konka/factory/desk/FactoryDB$USER_SETTING_FIELD;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const-string v0, "bEnableWDT"

    :goto_1
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDB;->userSettingSchema:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "/systemsetting"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v4, v0, v1, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    int-to-long v0, v0

    :goto_2
    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v0

    const/16 v1, 0x19

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_1
    const-string v0, "bUartBus"

    goto :goto_1

    :pswitch_2
    const-string v0, "bTeletext"

    goto :goto_1

    :catch_1
    move-exception v0

    const-string v0, "DataBaseDeskImpl"

    const-string v1, "update failed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-wide v0, v2

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public updateYPbPrOverscanAdjust(II[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V
    .locals 9
    .param p1    # I
    .param p2    # I
    .param p3    # [[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    const-string v7, "u16H_CapStart"

    aget-object v8, p3, p1

    aget-object v8, v8, p2

    iget v8, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16H_CapStart:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v7, "u16V_CapStart"

    aget-object v8, p3, p1

    aget-object v8, v8, p2

    iget v8, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u16V_CapStart:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v7, "u8HCrop_Left"

    aget-object v8, p3, p1

    aget-object v8, v8, p2

    iget-short v8, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v7, "u8HCrop_Right"

    aget-object v8, p3, p1

    aget-object v8, v8, p2

    iget-short v8, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v7, "u8VCrop_Up"

    aget-object v8, p3, p1

    aget-object v8, v8, p2

    iget-short v8, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-string v7, "u8VCrop_Down"

    aget-object v8, p3, p1

    aget-object v8, v8, p2

    iget-short v8, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    invoke-static {v8}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Short;)V

    const-wide/16 v1, -0x1

    :try_start_0
    const-string v7, "content://mstar.tv.factory/ypbproverscansetting"

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const-string v6, " ResolutionTypeNum = ? and _id = ? "

    const/4 v7, 0x2

    new-array v3, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v3, v7

    const/4 v7, 0x1

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v3, v7

    iget-object v7, p0, Lcom/konka/factory/desk/FactoryDB;->cr:Landroid/content/ContentResolver;

    invoke-virtual {v7, v4, v5, v6, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    int-to-long v1, v7

    :goto_0
    const-wide/16 v7, -0x1

    cmp-long v7, v1, v7

    if-nez v7, :cond_0

    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v7, "DataBaseDeskImpl"

    const-string v8, "update failed"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v7

    const/16 v8, 0x30

    invoke-virtual {v7, v8}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method
