.class public interface abstract Lcom/konka/factory/desk/IFactoryDesk;
.super Ljava/lang/Object;
.source "IFactoryDesk.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;,
        Lcom/konka/factory/desk/IFactoryDesk$EN_MS_PICTURE;,
        Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;,
        Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;,
        Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;,
        Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;,
        Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;,
        Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;,
        Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;,
        Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;,
        Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMP_DATA;,
        Lcom/konka/factory/desk/IFactoryDesk$CustomerCfgMiscSetting;,
        Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;,
        Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;,
        Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;,
        Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;,
        Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;,
        Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;,
        Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;,
        Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;,
        Lcom/konka/factory/desk/IFactoryDesk$MAX_YPbPr_Resolution_Info;,
        Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;,
        Lcom/konka/factory/desk/IFactoryDesk$MAX_DTV_Resolution_Info;,
        Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;,
        Lcom/konka/factory/desk/IFactoryDesk$MAPI_AVD_VideoStandardType;
    }
.end annotation


# virtual methods
.method public abstract ExecAutoADC()Z
.end method

.method public abstract enableUartDebug()Z
.end method

.method public abstract execVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;I)Z
.end method

.method public abstract get3DSelfAdaptiveLevel()I
.end method

.method public abstract getADCBlueGain()I
.end method

.method public abstract getADCBlueOffset()I
.end method

.method public abstract getADCGreenGain()I
.end method

.method public abstract getADCGreenOffset()I
.end method

.method public abstract getADCPhase()I
.end method

.method public abstract getADCRedGain()I
.end method

.method public abstract getADCRedOffset()I
.end method

.method public abstract getAEFC_43()S
.end method

.method public abstract getAEFC_44()S
.end method

.method public abstract getAEFC_66Bit76()S
.end method

.method public abstract getAEFC_6EBit3210()S
.end method

.method public abstract getAEFC_6EBit7654()S
.end method

.method public abstract getAEFC_A0()S
.end method

.method public abstract getAEFC_A1()S
.end method

.method public abstract getAEFC_CB()S
.end method

.method public abstract getAEFC_CFBit2_ATV()S
.end method

.method public abstract getAEFC_CFBit2_AV()S
.end method

.method public abstract getAEFC_D4()S
.end method

.method public abstract getAEFC_D5Bit2()S
.end method

.method public abstract getAEFC_D7HighBoun()S
.end method

.method public abstract getAEFC_D7LowBoun()S
.end method

.method public abstract getAEFC_D8Bit3210()S
.end method

.method public abstract getAEFC_D9Bit0()S
.end method

.method public abstract getAdcIdx()Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;
.end method

.method public abstract getAudioDelayTime()I
.end method

.method public abstract getAudioHiDevMode()I
.end method

.method public abstract getAudioNrThr()S
.end method

.method public abstract getAudioPrescale()S
.end method

.method public abstract getBoardType()Ljava/lang/String;
.end method

.method public abstract getBurnInMode()Z
.end method

.method public abstract getChinaDescramblerBox()S
.end method

.method public abstract getColorTmpIdx()I
.end method

.method public abstract getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
.end method

.method public abstract getCurveType()Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;
.end method

.method public abstract getDefaultAutoTuningCountry()I
.end method

.method public abstract getDefaultLanguage()Ljava/lang/String;
.end method

.method public abstract getDelayReduce()S
.end method

.method public abstract getDtvAvAbnormalDelay()Z
.end method

.method public abstract getEnergyEnable()Z
.end method

.method public abstract getEnergyPercent()S
.end method

.method public abstract getFactoryPreSetFeature()I
.end method

.method public abstract getGainDistributionThr()I
.end method

.method public abstract getLVDSenalbe()Z
.end method

.method public abstract getLVDSmodulation()I
.end method

.method public abstract getLVDSpercentage()I
.end method

.method public abstract getMIUenalbe()Z
.end method

.method public abstract getMIUmodulation()I
.end method

.method public abstract getMIUpercentage()I
.end method

.method public abstract getOsdV0Nonlinear()I
.end method

.method public abstract getOsdV100Nonlinear()I
.end method

.method public abstract getOsdV25Nonlinear()I
.end method

.method public abstract getOsdV50Nonlinear()I
.end method

.method public abstract getOsdV75Nonlinear()I
.end method

.method public abstract getOverScanHposition()S
.end method

.method public abstract getOverScanHsize()S
.end method

.method public abstract getOverScanSourceType()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
.end method

.method public abstract getOverScanVposition()S
.end method

.method public abstract getOverScanVsize()S
.end method

.method public abstract getPQVersion(I)Ljava/lang/String;
.end method

.method public abstract getPanelSwing()S
.end method

.method public abstract getPanelType()Ljava/lang/String;
.end method

.method public abstract getPeqFoCoarse(I)I
.end method

.method public abstract getPeqFoFine(I)I
.end method

.method public abstract getPeqGain(I)I
.end method

.method public abstract getPeqQ(I)I
.end method

.method public abstract getPictureModeIdx()I
.end method

.method public abstract getPowerOffLogoDspTime()I
.end method

.method public abstract getPowerOffLogoMode()I
.end method

.method public abstract getPowerOnMode()I
.end method

.method public abstract getSoundEqItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;I)I
.end method

.method public abstract getTeletextMode()I
.end method

.method public abstract getTestPattern()I
.end method

.method public abstract getUartOnOff()Z
.end method

.method public abstract getVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;)I
.end method

.method public abstract getVifAgcRef()S
.end method

.method public abstract getVifAsiaSignalOption()Z
.end method

.method public abstract getVifClampGainOvNegative()I
.end method

.method public abstract getVifCrKi()S
.end method

.method public abstract getVifCrKp()S
.end method

.method public abstract getVifCrKpKiAdjust()Z
.end method

.method public abstract getVifCrThr()I
.end method

.method public abstract getVifOverModulation()Z
.end method

.method public abstract getVifTop()S
.end method

.method public abstract getVifVersion()S
.end method

.method public abstract getVifVgaMaximum()I
.end method

.method public abstract getWatchDogMode()S
.end method

.method public abstract getWbBlueGain()I
.end method

.method public abstract getWbBlueOffset()I
.end method

.method public abstract getWbGreenGain()I
.end method

.method public abstract getWbGreenOffset()I
.end method

.method public abstract getWbRedGain()I
.end method

.method public abstract getWbRedOffset()I
.end method

.method public abstract loadEssentialDataFromDB()V
.end method

.method public abstract releaseHandler(I)V
.end method

.method public abstract restoreToDefault()Z
.end method

.method public abstract set3DSelfAdaptiveLevel(I)Z
.end method

.method public abstract setADCBlueGain(I)Z
.end method

.method public abstract setADCBlueOffset(I)Z
.end method

.method public abstract setADCGreenGain(I)Z
.end method

.method public abstract setADCGreenOffset(I)Z
.end method

.method public abstract setADCPhase(I)Z
.end method

.method public abstract setADCRedGain(I)Z
.end method

.method public abstract setADCRedOffset(I)Z
.end method

.method public abstract setAEFC_43(S)Z
.end method

.method public abstract setAEFC_44(S)Z
.end method

.method public abstract setAEFC_66Bit76(S)Z
.end method

.method public abstract setAEFC_6EBit3210(S)Z
.end method

.method public abstract setAEFC_6EBit7654(S)Z
.end method

.method public abstract setAEFC_A0(S)Z
.end method

.method public abstract setAEFC_A1(S)Z
.end method

.method public abstract setAEFC_CB(S)Z
.end method

.method public abstract setAEFC_CFBit2_ATV(S)Z
.end method

.method public abstract setAEFC_CFBit2_AV(S)Z
.end method

.method public abstract setAEFC_D4(S)Z
.end method

.method public abstract setAEFC_D5Bit2(S)Z
.end method

.method public abstract setAEFC_D7HighBoun(S)Z
.end method

.method public abstract setAEFC_D7LowBoun(S)Z
.end method

.method public abstract setAEFC_D8Bit3210(S)Z
.end method

.method public abstract setAEFC_D9Bit0(S)Z
.end method

.method public abstract setAdcIdx(Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;)Z
.end method

.method public abstract setAudioDelayTime(I)Z
.end method

.method public abstract setAudioHiDevMode(I)Z
.end method

.method public abstract setAudioNrThr(S)Z
.end method

.method public abstract setAudioPrescale(S)Z
.end method

.method public abstract setBurnInMode(Z)Z
.end method

.method public abstract setChinaDescramblerBox(S)Z
.end method

.method public abstract setColorTmpIdx(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;)V
.end method

.method public abstract setCurveType(Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;)Z
.end method

.method public abstract setCustomerSettingEnable()Z
.end method

.method public abstract setDefaultAutoTuningCountry(I)Z
.end method

.method public abstract setDefaultLanguage(Ljava/lang/String;)Z
.end method

.method public abstract setDelayReduce(S)Z
.end method

.method public abstract setDtvAvAbnormalDelay(Z)Z
.end method

.method public abstract setEQ(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;)Z
.end method

.method public abstract setEnergyEnable(Z)Z
.end method

.method public abstract setEnergyPercent(S)Z
.end method

.method public abstract setFactoryPreSetFeature(I)Z
.end method

.method public abstract setGainDistributionThr(I)Z
.end method

.method public abstract setHandler(Landroid/os/Handler;I)Z
.end method

.method public abstract setLVDSenable(Z)Z
.end method

.method public abstract setLVDSmodulation(I)Z
.end method

.method public abstract setLVDSpercentage(I)Z
.end method

.method public abstract setMIUenable(Z)Z
.end method

.method public abstract setMIUmodulation(I)Z
.end method

.method public abstract setMIUpercentage(I)Z
.end method

.method public abstract setOsdV0Nonlinear(S)Z
.end method

.method public abstract setOsdV100Nonlinear(S)Z
.end method

.method public abstract setOsdV25Nonlinear(S)Z
.end method

.method public abstract setOsdV50Nonlinear(S)Z
.end method

.method public abstract setOsdV75Nonlinear(S)Z
.end method

.method public abstract setOverScanHposition(S)Z
.end method

.method public abstract setOverScanHsize(S)Z
.end method

.method public abstract setOverScanVposition(S)Z
.end method

.method public abstract setOverScanVsize(S)Z
.end method

.method public abstract setPEQ()Z
.end method

.method public abstract setPEQ85HzGain(I)Z
.end method

.method public abstract setPanelSwing(S)Z
.end method

.method public abstract setPeqFoCoarse(II)Z
.end method

.method public abstract setPeqFoFine(II)Z
.end method

.method public abstract setPeqGain(II)Z
.end method

.method public abstract setPeqQ(II)Z
.end method

.method public abstract setPictureModeIdx(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_PICTURE;)V
.end method

.method public abstract setPowerOffLogoDspTime(I)Z
.end method

.method public abstract setPowerOffLogoMode(I)Z
.end method

.method public abstract setPowerOnMode(I)Z
.end method

.method public abstract setSoundEqItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;II)I
.end method

.method public abstract setTeletextMode(I)Z
.end method

.method public abstract setTestPattern(I)Z
.end method

.method public abstract setUartOnOff(Z)Z
.end method

.method public abstract setVifAgcRef(S)Z
.end method

.method public abstract setVifAsiaSignalOption(Z)Z
.end method

.method public abstract setVifClampGainOvNegative(I)Z
.end method

.method public abstract setVifCrKi(S)Z
.end method

.method public abstract setVifCrKp(S)Z
.end method

.method public abstract setVifCrKpKiAdjust(Z)Z
.end method

.method public abstract setVifCrThr(I)Z
.end method

.method public abstract setVifOverModulation(Z)Z
.end method

.method public abstract setVifTop(S)Z
.end method

.method public abstract setVifVersion(S)Z
.end method

.method public abstract setVifVgaMaximum(I)Z
.end method

.method public abstract setWatchDogMode(S)Z
.end method

.method public abstract setWbBlueGain(S)Z
.end method

.method public abstract setWbBlueOffset(S)Z
.end method

.method public abstract setWbGreenGain(S)Z
.end method

.method public abstract setWbGreenOffset(S)Z
.end method

.method public abstract setWbRedGain(S)Z
.end method

.method public abstract setWbRedOffset(S)Z
.end method

.method public abstract updatePqIniFiles()V
.end method
