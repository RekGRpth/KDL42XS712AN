.class public Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;
.super Ljava/lang/Object;
.source "IFactoryDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/factory/desk/IFactoryDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MS_FACTORY_EXTERN_SETTING"
.end annotation


# static fields
.field public static boardType:Ljava/lang/String;

.field public static dayAndTime:Ljava/lang/String;

.field public static panelType:Ljava/lang/String;

.field public static softVersion:Ljava/lang/String;


# instance fields
.field public audioDspVersion:S

.field public audioNrThr:S

.field public audioPreScale:S

.field public audioSifThreshold:S

.field public bBurnIn:Z

.field public dtvAvAbnormalDelay:Z

.field public eHidevMode:I

.field public factoryPreset:I

.field public m_bAgingMode:Z

.field public panelSwingVal:S

.field public stPowerMode:I

.field public testPatternMode:I

.field public vdDspVersion:S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "0.0.1"

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->softVersion:Ljava/lang/String;

    const-string v0, "A3"

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->boardType:Ljava/lang/String;

    const-string v0, "Full-HD"

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->panelType:Ljava/lang/String;

    const-string v0, "2011.9.22 12:00"

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->dayAndTime:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x5

    iput v0, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->testPatternMode:I

    iput v1, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->factoryPreset:I

    iput-boolean v1, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->dtvAvAbnormalDelay:Z

    iput-short v1, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->panelSwingVal:S

    iput-short v1, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->audioPreScale:S

    const/4 v0, 0x1

    iput v0, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->stPowerMode:I

    iput v1, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->eHidevMode:I

    iput-boolean v1, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->m_bAgingMode:Z

    iput-short v1, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->vdDspVersion:S

    iput-short v1, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->audioNrThr:S

    iput-short v1, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->audioSifThreshold:S

    iput-short v1, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->audioDspVersion:S

    iput-boolean v1, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->bBurnIn:Z

    return-void
.end method
