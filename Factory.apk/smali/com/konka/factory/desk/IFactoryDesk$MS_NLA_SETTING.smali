.class public Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;
.super Ljava/lang/Object;
.source "IFactoryDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/factory/desk/IFactoryDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MS_NLA_SETTING"
.end annotation


# instance fields
.field public msNlaSetIndex:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

.field public stNLASetting:[Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->EN_NLA_NUMS:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v1}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v1

    new-array v1, v1, [Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;

    iput-object v1, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;

    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->EN_NLA_NUMS:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v1}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;

    new-instance v2, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;

    invoke-direct {v2}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;-><init>()V

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->EN_NLA_VOLUME:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    iput-object v1, p0, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    return-void
.end method
