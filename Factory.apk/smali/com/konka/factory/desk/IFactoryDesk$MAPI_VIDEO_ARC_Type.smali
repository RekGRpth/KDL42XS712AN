.class public final enum Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;
.super Ljava/lang/Enum;
.source "IFactoryDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/factory/desk/IFactoryDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MAPI_VIDEO_ARC_Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

.field public static final enum E_AR_14x9:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

.field public static final enum E_AR_16x9:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

.field public static final enum E_AR_16x9_Combind:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

.field public static final enum E_AR_16x9_PanScan:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

.field public static final enum E_AR_16x9_PillarBox:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

.field public static final enum E_AR_4x3:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

.field public static final enum E_AR_4x3_Combind:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

.field public static final enum E_AR_4x3_LetterBox:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

.field public static final enum E_AR_4x3_PanScan:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

.field public static final enum E_AR_AUTO:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

.field public static final enum E_AR_DEFAULT:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

.field public static final enum E_AR_DotByDot:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

.field public static final enum E_AR_JustScan:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

.field public static final enum E_AR_MAX:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

.field public static final enum E_AR_Movie:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

.field public static final enum E_AR_Panorama:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

.field public static final enum E_AR_Personal:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

.field public static final enum E_AR_Subtitle:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

.field public static final enum E_AR_Zoom1:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

.field public static final enum E_AR_Zoom2:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

.field public static final enum E_AR_Zoom_2x:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

.field public static final enum E_AR_Zoom_3x:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

.field public static final enum E_AR_Zoom_4x:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    const-string v1, "E_AR_DEFAULT"

    invoke-direct {v0, v1, v3}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_DEFAULT:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    const-string v1, "E_AR_16x9"

    invoke-direct {v0, v1, v4}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_16x9:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    const-string v1, "E_AR_4x3"

    invoke-direct {v0, v1, v5}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_4x3:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    const-string v1, "E_AR_AUTO"

    invoke-direct {v0, v1, v6}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_AUTO:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    const-string v1, "E_AR_Panorama"

    invoke-direct {v0, v1, v7}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_Panorama:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    const-string v1, "E_AR_JustScan"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_JustScan:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    const-string v1, "E_AR_Zoom1"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_Zoom1:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    const-string v1, "E_AR_Zoom2"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_Zoom2:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    const-string v1, "E_AR_14x9"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_14x9:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    const-string v1, "E_AR_DotByDot"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_DotByDot:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    const-string v1, "E_AR_Subtitle"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_Subtitle:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    const-string v1, "E_AR_Movie"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_Movie:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    const-string v1, "E_AR_Personal"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_Personal:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    const-string v1, "E_AR_4x3_PanScan"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_4x3_PanScan:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    const-string v1, "E_AR_4x3_LetterBox"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_4x3_LetterBox:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    const-string v1, "E_AR_16x9_PillarBox"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_16x9_PillarBox:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    const-string v1, "E_AR_16x9_PanScan"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_16x9_PanScan:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    const-string v1, "E_AR_4x3_Combind"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_4x3_Combind:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    const-string v1, "E_AR_16x9_Combind"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_16x9_Combind:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    const-string v1, "E_AR_Zoom_2x"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_Zoom_2x:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    const-string v1, "E_AR_Zoom_3x"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_Zoom_3x:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    const-string v1, "E_AR_Zoom_4x"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_Zoom_4x:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    const-string v1, "E_AR_MAX"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    const/16 v0, 0x17

    new-array v0, v0, [Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_DEFAULT:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_16x9:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_4x3:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    aput-object v1, v0, v5

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_AUTO:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    aput-object v1, v0, v6

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_Panorama:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_JustScan:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_Zoom1:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_Zoom2:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_14x9:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_DotByDot:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_Subtitle:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_Movie:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_Personal:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_4x3_PanScan:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_4x3_LetterBox:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_16x9_PillarBox:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_16x9_PanScan:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_4x3_Combind:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_16x9_Combind:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_Zoom_2x:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_Zoom_3x:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_Zoom_4x:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->E_AR_MAX:Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->$VALUES:[Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;
    .locals 1

    const-class v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    return-object v0
.end method

.method public static values()[Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;
    .locals 1

    sget-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->$VALUES:[Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    invoke-virtual {v0}, [Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/konka/factory/desk/IFactoryDesk$MAPI_VIDEO_ARC_Type;

    return-object v0
.end method
