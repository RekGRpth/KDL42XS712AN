.class public Lcom/konka/factory/desk/FactoryDeskImpl;
.super Ljava/lang/Object;
.source "FactoryDeskImpl.java"

# interfaces
.implements Lcom/konka/factory/desk/IFactoryDesk;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/factory/desk/FactoryDeskImpl$3;
    }
.end annotation


# static fields
.field private static instance:Lcom/konka/factory/desk/FactoryDeskImpl;

.field private static final log:Landroid/util/Log;


# instance fields
.field private adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

.field private am:Lcom/mstar/android/tvapi/common/AudioManager;

.field private context:Landroid/content/Context;

.field private customerCfgMiscSetting:Lcom/konka/factory/desk/IFactoryDesk$CustomerCfgMiscSetting;

.field private databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

.field private eAdcIdx:Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;

.field private f_phase:I

.field private factoryATVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

.field private factoryColorTmpEx:Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;

.field private factoryCusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

.field private factoryDB:Lcom/konka/factory/desk/FactoryDB;

.field private factoryDTVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

.field private factoryExternSetting:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

.field private factoryHDMIOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

.field private factoryPEQSet:Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;

.field private factoryVDOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

.field private factoryYPbPrOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

.field private facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

.field private fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

.field private mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

.field private matvplayer:Lcom/mstar/android/tvapi/atv/AtvPlayer;

.field private final max_handler:I

.field private mfactoryNLASet:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

.field private mtvplayer:Lcom/mstar/android/tvapi/common/TvPlayer;

.field private overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field pHandler:[Landroid/os/Handler;

.field private pm:Lcom/mstar/android/tvapi/common/PictureManager;

.field private s3m:Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

.field private sscSet:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;

.field private tp:Lcom/mstar/android/tvapi/common/TvPlayer;

.field private vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

.field private wb_gainoffset:Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMP_DATA;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/factory/desk/FactoryDeskImpl;->log:Landroid/util/Log;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->am:Lcom/mstar/android/tvapi/common/AudioManager;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvPlayerManager()Lcom/mstar/android/tvapi/atv/AtvPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->matvplayer:Lcom/mstar/android/tvapi/atv/AtvPlayer;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->s3m:Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->tp:Lcom/mstar/android/tvapi/common/TvPlayer;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mtvplayer:Lcom/mstar/android/tvapi/common/TvPlayer;

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->max_handler:I

    new-array v0, v1, [Landroid/os/Handler;

    iput-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pHandler:[Landroid/os/Handler;

    iput-object p1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->context:Landroid/content/Context;

    invoke-static {p1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    const/16 v0, 0x3b

    iput v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->f_phase:I

    invoke-static {p1}, Lcom/konka/factory/desk/FactoryDB;->getInstance(Landroid/content/Context;)Lcom/konka/factory/desk/FactoryDB;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    return-void
.end method

.method private GetResolutionMapping(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)I
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    const/4 v0, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    invoke-virtual {v2, p1}, Lcom/mstar/android/tvapi/factory/FactoryManager;->getResolutionMappingIndex(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)S
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private InitVif()V
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->matvplayer:Lcom/mstar/android/tvapi/atv/AtvPlayer;

    invoke-interface {v1}, Lcom/mstar/android/tvapi/atv/AtvPlayer;->initAtvVif()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private UpdateSscPara()V
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/factory/FactoryManager;->updateSscParameter()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/konka/factory/desk/FactoryDeskImpl;)Lcom/mstar/android/tvapi/factory/FactoryManager;
    .locals 1
    .param p0    # Lcom/konka/factory/desk/FactoryDeskImpl;

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    return-object v0
.end method

.method private chmodFile(Ljava/io/File;)V
    .locals 4

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "chmod 666 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "zyl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "command = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "zyl"

    const-string v2, "chmod fail!!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private copyFile(Ljava/io/File;Ljava/io/File;)Z
    .locals 2

    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-direct {p0, v1, p2}, Lcom/konka/factory/desk/FactoryDeskImpl;->copyToFile(Ljava/io/InputStream;Ljava/io/File;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :goto_0
    invoke-direct {p0, p2}, Lcom/konka/factory/desk/FactoryDeskImpl;->chmodFile(Ljava/io/File;)V

    return v0

    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :catch_0
    move-exception v0

    const-string v1, "copyFile(File srcFile, File destFile)"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private copyToFile(Ljava/io/InputStream;Ljava/io/File;)Z
    .locals 6

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Ljava/io/File;->delete()Z

    :cond_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v1, 0x1000

    :try_start_1
    new-array v1, v1, [B

    :goto_0
    invoke-virtual {p1, v1}, Ljava/io/InputStream;->read([B)I

    move-result v3

    if-ltz v3, :cond_1

    const-string v4, " out.write(buffer, 0, bytesRead);"

    const-string v5, " out.write(buffer, 0, bytesRead);"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v4, v3}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/FileDescriptor;->sync()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    :goto_1
    :try_start_4
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    throw v1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    :catch_0
    move-exception v1

    const-string v2, "copyToFile(InputStream inputStream, File destFile)"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    return v0

    :cond_1
    :try_start_5
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->flush()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    :try_start_6
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/FileDescriptor;->sync()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    :goto_3
    :try_start_7
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0

    const/4 v0, 0x1

    goto :goto_2

    :catch_1
    move-exception v3

    goto :goto_1

    :catch_2
    move-exception v1

    goto :goto_3
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/konka/factory/desk/FactoryDeskImpl;
    .locals 1
    .param p0    # Landroid/content/Context;

    sget-object v0, Lcom/konka/factory/desk/FactoryDeskImpl;->instance:Lcom/konka/factory/desk/FactoryDeskImpl;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/factory/desk/FactoryDeskImpl;

    invoke-direct {v0, p0}, Lcom/konka/factory/desk/FactoryDeskImpl;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/konka/factory/desk/FactoryDeskImpl;->instance:Lcom/konka/factory/desk/FactoryDeskImpl;

    :cond_0
    sget-object v0, Lcom/konka/factory/desk/FactoryDeskImpl;->instance:Lcom/konka/factory/desk/FactoryDeskImpl;

    return-object v0
.end method

.method private setADCGainOffset(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;I)V
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;
    .param p2    # I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;->values()[Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    move-result-object v2

    aget-object v1, v2, p2

    const-string v2, "FACTORY"

    const-string v3, "setADCGainOffset"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    invoke-virtual {v2, p1, v1, v3}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setAdcGainOffset(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;)V

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    invoke-virtual {v2, v3, p2}, Lcom/konka/factory/desk/FactoryDB;->updateADCAdjust(Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;I)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private setWBGainOffset(III)V
    .locals 10
    .param p1    # I
    .param p2    # I
    .param p3    # I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;->values()[Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

    move-result-object v0

    add-int/lit8 v2, p1, 0x1

    aget-object v1, v0, v2

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    aget-object v8, v0, p2

    const-string v0, "FactoryDeskImpl"

    const-string v2, "--------sssssssss------1-------------"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryColorTmpEx:Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;

    iget-object v2, v2, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v2, v2, p1

    aget-object v2, v2, p3

    iget v2, v2, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;->redgain:I

    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryColorTmpEx:Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;

    iget-object v3, v3, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v3, v3, p1

    aget-object v3, v3, p3

    iget v3, v3, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;->greengain:I

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryColorTmpEx:Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;

    iget-object v4, v4, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v4, v4, p1

    aget-object v4, v4, p3

    iget v4, v4, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;->bluegain:I

    iget-object v5, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryColorTmpEx:Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;

    iget-object v5, v5, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v5, v5, p1

    aget-object v5, v5, p3

    iget v5, v5, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;->redoffset:I

    iget-object v6, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryColorTmpEx:Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;

    iget-object v6, v6, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v6, v6, p1

    aget-object v6, v6, p3

    iget v6, v6, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;->greenoffset:I

    iget-object v7, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryColorTmpEx:Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;

    iget-object v7, v7, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v7, v7, p1

    aget-object v7, v7, p3

    iget v7, v7, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;->blueoffset:I

    invoke-virtual/range {v0 .. v8}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setWbGainOffsetEx(Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;IIIIIILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v0, "FactoryDeskImpl"

    const-string v2, "--------sssssssss------2-------------"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryColorTmpEx:Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;

    iget-object v2, v2, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v2, v2, p1

    aget-object v2, v2, p3

    invoke-virtual {v0, v2, p3, p1}, Lcom/konka/factory/desk/FactoryDB;->updateFactoryColorTempExData(Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;II)V

    return-void

    :catch_0
    move-exception v9

    invoke-virtual {v9}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public ExecAutoADC()Z
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/factory/desk/FactoryDeskImpl$1;

    invoke-direct {v1, p0}, Lcom/konka/factory/desk/FactoryDeskImpl$1;-><init>(Lcom/konka/factory/desk/FactoryDeskImpl;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    const/4 v0, 0x1

    return v0
.end method

.method public InputSourceTransfer(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v0, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_NONE:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    sget-object v1, Lcom/konka/factory/desk/FactoryDeskImpl$3;->$SwitchMap$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_VGA:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_ATV:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_CVBS:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_SVIDEO:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    goto :goto_0

    :pswitch_4
    sget-object v0, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_YPBPR:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    goto :goto_0

    :pswitch_5
    sget-object v0, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_SCART:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    goto :goto_0

    :pswitch_6
    sget-object v0, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_HDMI:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    goto :goto_0

    :pswitch_7
    sget-object v0, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_DTV:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    goto :goto_0

    :pswitch_8
    sget-object v0, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_OTHERS:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    goto :goto_0

    :pswitch_9
    sget-object v0, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_NONE:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_9
        :pswitch_9
    .end packed-switch
.end method

.method public enableUartDebug()Z
    .locals 2

    :try_start_0
    const-string v0, "FactoryDeskImpl"

    const-string v1, "---------mstv_tool------1----"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/factory/FactoryManager;->enableUartDebug()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public execVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;I)Z
    .locals 5
    .param p1    # Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;
    .param p2    # I

    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v3}, Lcom/konka/factory/desk/FactoryDB;->queryCurInputSrc()I

    move-result v1

    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v3, v1}, Lcom/konka/factory/desk/FactoryDB;->queryePicMode(I)I

    move-result v2

    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v3, p1, v1, v2, p2}, Lcom/konka/factory/desk/FactoryDB;->updatePicModeSetting(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;III)V

    invoke-virtual {p1}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v3

    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_BRIGHTNESS:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_1

    :try_start_0
    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    int-to-short v4, p2

    invoke-virtual {v3, v4}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeBrightness(S)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v3, 0x1

    return v3

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v3

    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_CONTRAST:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_2

    :try_start_1
    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    int-to-short v4, p2

    invoke-virtual {v3, v4}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeContrast(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v3

    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_SATURATION:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_3

    :try_start_2
    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    int-to-short v4, p2

    invoke-virtual {v3, v4}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeColor(S)V
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v3

    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_SHARPNESS:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_4

    :try_start_3
    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    int-to-short v4, p2

    invoke-virtual {v3, v4}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeSharpness(S)V
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_0

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v3

    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_HUE:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_5

    :try_start_4
    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    int-to-short v4, p2

    invoke-virtual {v3, v4}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeTint(S)V
    :try_end_4
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_0

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_5
    invoke-virtual {p1}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v3

    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_BACKLIGHT:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_0

    :try_start_5
    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    int-to-short v4, p2

    invoke-virtual {v3, v4}, Lcom/mstar/android/tvapi/common/PictureManager;->setBacklight(I)V
    :try_end_5
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_5 .. :try_end_5} :catch_5

    goto :goto_0

    :catch_5
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public get3DSelfAdaptiveLevel()I
    .locals 2

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v1}, Lcom/konka/factory/desk/FactoryDB;->queryCurInputSrc()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/factory/desk/FactoryDB;->querySelfAdaptiveLevel(I)I

    move-result v0

    return v0
.end method

.method public getADCBlueGain()I
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iget v0, v0, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->blueGain:I

    return v0
.end method

.method public getADCBlueOffset()I
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iget v0, v0, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->blueOffset:I

    return v0
.end method

.method public getADCGreenGain()I
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iget v0, v0, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->greenGain:I

    return v0
.end method

.method public getADCGreenOffset()I
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iget v0, v0, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->greenOffset:I

    return v0
.end method

.method public getADCPhase()I
    .locals 1

    iget v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->f_phase:I

    return v0
.end method

.method public getADCRedGain()I
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iget v0, v0, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->redGain:I

    return v0
.end method

.method public getADCRedOffset()I
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iget v0, v0, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->redOffset:I

    return v0
.end method

.method public getAEFC_43()S
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v0, v0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_43:S

    return v0
.end method

.method public getAEFC_44()S
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v0, v0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_44:S

    return v0
.end method

.method public getAEFC_66Bit76()S
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v0, v0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_66_Bit76:S

    return v0
.end method

.method public getAEFC_6EBit3210()S
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v0, v0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_6E_Bit3210:S

    return v0
.end method

.method public getAEFC_6EBit7654()S
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v0, v0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_6E_Bit7654:S

    return v0
.end method

.method public getAEFC_A0()S
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v0, v0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_A0:S

    return v0
.end method

.method public getAEFC_A1()S
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v0, v0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_A1:S

    return v0
.end method

.method public getAEFC_CB()S
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v0, v0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_CB:S

    return v0
.end method

.method public getAEFC_CFBit2_ATV()S
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v0, v0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_CF_Bit2_ATV:S

    return v0
.end method

.method public getAEFC_CFBit2_AV()S
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v0, v0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_CF_Bit2_AV:S

    return v0
.end method

.method public getAEFC_D4()S
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v0, v0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D4:S

    return v0
.end method

.method public getAEFC_D5Bit2()S
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v0, v0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D5_Bit2:S

    return v0
.end method

.method public getAEFC_D7HighBoun()S
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v0, v0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D7_HIGH_BOUND:S

    return v0
.end method

.method public getAEFC_D7LowBoun()S
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v0, v0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D7_LOW_BOUND:S

    return v0
.end method

.method public getAEFC_D8Bit3210()S
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v0, v0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D8_Bit3210:S

    return v0
.end method

.method public getAEFC_D9Bit0()S
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v0, v0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D9_Bit0:S

    return v0
.end method

.method public getAdcIdx()Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->eAdcIdx:Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;

    return-object v0
.end method

.method public getAudioDelayTime()I
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryCusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    iget v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->AudioDelayTime:I

    return v0
.end method

.method public getAudioHiDevMode()I
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryExternSetting:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

    iget v0, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->eHidevMode:I

    return v0
.end method

.method public getAudioNrThr()S
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryExternSetting:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

    iget-short v0, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->audioNrThr:S

    return v0
.end method

.method public getAudioPrescale()S
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryExternSetting:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

    iget-short v0, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->audioPreScale:S

    return v0
.end method

.method public getBoardType()Ljava/lang/String;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryExternSetting:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getSystemBoardName()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->boardType:Ljava/lang/String;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryExternSetting:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->boardType:Ljava/lang/String;

    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getBurnInMode()Z
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryExternSetting:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

    iget-boolean v0, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->m_bAgingMode:Z

    return v0
.end method

.method public getChinaDescramblerBox()S
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    iget-short v0, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->ChinaDescramblerBox:S

    return v0
.end method

.method public getColorTmpIdx()I
    .locals 4

    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v3}, Lcom/konka/factory/desk/FactoryDB;->queryCurInputSrc()I

    move-result v0

    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v3, v0}, Lcom/konka/factory/desk/FactoryDB;->queryePicMode(I)I

    move-result v2

    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v3, v0, v2}, Lcom/konka/factory/desk/FactoryDB;->queryColorTmpIdx(II)I

    move-result v1

    return v1
.end method

.method public getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .locals 2

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v1}, Lcom/konka/factory/desk/FactoryDB;->queryCurInputSrc()I

    move-result v0

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    aget-object v1, v1, v0

    return-object v1
.end method

.method public getCurveType()Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mfactoryNLASet:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

    iget-object v0, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    return-object v0
.end method

.method public getDefaultAutoTuningCountry()I
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryCusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    iget v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->DefTuningCountry:I

    return v0
.end method

.method public getDefaultLanguage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryCusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    iget-object v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->DefOSDLanguage:Ljava/lang/String;

    return-object v0
.end method

.method public getDelayReduce()S
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    iget-short v0, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifDelayReduce:S

    return v0
.end method

.method public getDtvAvAbnormalDelay()Z
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryExternSetting:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

    iget-boolean v0, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->dtvAvAbnormalDelay:Z

    return v0
.end method

.method public getEnergyEnable()Z
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->customerCfgMiscSetting:Lcom/konka/factory/desk/IFactoryDesk$CustomerCfgMiscSetting;

    iget-boolean v0, v0, Lcom/konka/factory/desk/IFactoryDesk$CustomerCfgMiscSetting;->energyEnable:Z

    return v0
.end method

.method public getEnergyPercent()S
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->customerCfgMiscSetting:Lcom/konka/factory/desk/IFactoryDesk$CustomerCfgMiscSetting;

    iget-short v0, v0, Lcom/konka/factory/desk/IFactoryDesk$CustomerCfgMiscSetting;->energyPercent:S

    return v0
.end method

.method public getFactoryPreSetFeature()I
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryExternSetting:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

    iget v0, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->factoryPreset:I

    return v0
.end method

.method public getGainDistributionThr()I
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    iget v0, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->GainDistributionThr:I

    return v0
.end method

.method public getHandler(I)Landroid/os/Handler;
    .locals 1
    .param p1    # I

    const/4 v0, 0x4

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pHandler:[Landroid/os/Handler;

    aget-object v0, v0, p1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLVDSenalbe()Z
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->sscSet:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;

    iget-boolean v0, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Lvds_SscEnable:Z

    return v0
.end method

.method public getLVDSmodulation()I
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->sscSet:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;

    iget v0, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Lvds_SscSpan:I

    return v0
.end method

.method public getLVDSpercentage()I
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->sscSet:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;

    iget v0, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Lvds_SscStep:I

    return v0
.end method

.method public getMIUenalbe()Z
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->sscSet:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;

    iget-boolean v0, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Miu_SscEnable:Z

    return v0
.end method

.method public getMIUmodulation()I
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->sscSet:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;

    iget v0, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Miu0_SscSpan:I

    return v0
.end method

.method public getMIUpercentage()I
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->sscSet:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;

    iget v0, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Miu0_SscStep:I

    return v0
.end method

.method public getOsdV0Nonlinear()I
    .locals 2

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mfactoryNLASet:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

    iget-object v1, v1, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v1}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v0

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mfactoryNLASet:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

    iget-object v1, v1, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;->u8OSD_V0:I

    return v1
.end method

.method public getOsdV100Nonlinear()I
    .locals 2

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mfactoryNLASet:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

    iget-object v1, v1, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v1}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v0

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mfactoryNLASet:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

    iget-object v1, v1, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;->u8OSD_V100:I

    return v1
.end method

.method public getOsdV25Nonlinear()I
    .locals 2

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mfactoryNLASet:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

    iget-object v1, v1, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v1}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v0

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mfactoryNLASet:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

    iget-object v1, v1, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;->u8OSD_V25:I

    return v1
.end method

.method public getOsdV50Nonlinear()I
    .locals 2

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mfactoryNLASet:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

    iget-object v1, v1, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v1}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v0

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mfactoryNLASet:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

    iget-object v1, v1, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;->u8OSD_V50:I

    return v1
.end method

.method public getOsdV75Nonlinear()I
    .locals 2

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mfactoryNLASet:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

    iget-object v1, v1, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v1}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v0

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mfactoryNLASet:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

    iget-object v1, v1, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;->u8OSD_V75:I

    return v1
.end method

.method public getOverScanHposition()S
    .locals 8

    iget-object v6, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v6}, Lcom/konka/factory/desk/FactoryDB;->queryCurInputSrc()I

    move-result v3

    iget-object v6, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v6, v3}, Lcom/konka/factory/desk/FactoryDB;->queryArcMode(I)I

    move-result v0

    const/4 v5, 0x0

    :try_start_0
    iget-object v6, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mtvplayer:Lcom/mstar/android/tvapi/common/TvPlayer;

    invoke-interface {v6}, Lcom/mstar/android/tvapi/common/TvPlayer;->getVideoStandard()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    :goto_0
    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_AVD_VideoStandardType;->values()[Lcom/konka/factory/desk/IFactoryDesk$MAPI_AVD_VideoStandardType;

    move-result-object v6

    aget-object v4, v6, v5

    sget-object v6, Lcom/konka/factory/desk/FactoryDeskImpl$3;->$SwitchMap$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    iget-object v7, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v7}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    :pswitch_0
    const/4 v6, -0x1

    :goto_1
    return v6

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_1
    sget-object v6, Lcom/konka/factory/desk/FactoryDeskImpl$3;->$SwitchMap$com$konka$factory$desk$IFactoryDesk$MAPI_AVD_VideoStandardType:[I

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_AVD_VideoStandardType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_1

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    :goto_2
    iget-object v6, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryVDOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v2}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    goto :goto_1

    :pswitch_2
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_3
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_4
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_SECAM:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_5
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_6
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL_M:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_7
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL_NC:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_8
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_9
    sget-object v6, Lcom/konka/factory/desk/FactoryDeskImpl$3;->$SwitchMap$com$konka$factory$desk$IFactoryDesk$MAPI_AVD_VideoStandardType:[I

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_AVD_VideoStandardType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_2

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    :goto_3
    iget-object v6, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryATVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v2}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    goto :goto_1

    :pswitch_a
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_b
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_c
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_SECAM:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_d
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_e
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL_M:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_f
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL_NC:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_10
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_11
    iget-object v6, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryYPbPrOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v7, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v7}, Lcom/konka/factory/desk/FactoryDeskImpl;->GetResolutionMapping(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    goto :goto_1

    :pswitch_12
    iget-object v6, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryHDMIOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v7, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v7}, Lcom/konka/factory/desk/FactoryDeskImpl;->GetResolutionMapping(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    goto/16 :goto_1

    :pswitch_13
    iget-object v6, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDTVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v7, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v7}, Lcom/konka/factory/desk/FactoryDeskImpl;->GetResolutionMapping(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_9
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_0
        :pswitch_13
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method public getOverScanHsize()S
    .locals 8

    iget-object v6, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v6}, Lcom/konka/factory/desk/FactoryDB;->queryCurInputSrc()I

    move-result v3

    iget-object v6, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v6, v3}, Lcom/konka/factory/desk/FactoryDB;->queryArcMode(I)I

    move-result v0

    const/4 v5, 0x0

    :try_start_0
    iget-object v6, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mtvplayer:Lcom/mstar/android/tvapi/common/TvPlayer;

    invoke-interface {v6}, Lcom/mstar/android/tvapi/common/TvPlayer;->getVideoStandard()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    :goto_0
    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_AVD_VideoStandardType;->values()[Lcom/konka/factory/desk/IFactoryDesk$MAPI_AVD_VideoStandardType;

    move-result-object v6

    aget-object v4, v6, v5

    sget-object v6, Lcom/konka/factory/desk/FactoryDeskImpl$3;->$SwitchMap$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    iget-object v7, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v7}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    :pswitch_0
    const/4 v6, -0x1

    :goto_1
    return v6

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_1
    sget-object v6, Lcom/konka/factory/desk/FactoryDeskImpl$3;->$SwitchMap$com$konka$factory$desk$IFactoryDesk$MAPI_AVD_VideoStandardType:[I

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_AVD_VideoStandardType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_1

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    :goto_2
    iget-object v6, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryVDOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v2}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    goto :goto_1

    :pswitch_2
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_3
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_4
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_SECAM:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_5
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_6
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL_M:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_7
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL_NC:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_8
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_9
    sget-object v6, Lcom/konka/factory/desk/FactoryDeskImpl$3;->$SwitchMap$com$konka$factory$desk$IFactoryDesk$MAPI_AVD_VideoStandardType:[I

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_AVD_VideoStandardType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_2

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    :goto_3
    iget-object v6, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryATVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v2}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    goto :goto_1

    :pswitch_a
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_b
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_c
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_SECAM:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_d
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_e
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL_M:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_f
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL_NC:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_10
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_11
    iget-object v6, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryYPbPrOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v7, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v7}, Lcom/konka/factory/desk/FactoryDeskImpl;->GetResolutionMapping(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    goto :goto_1

    :pswitch_12
    iget-object v6, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryHDMIOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v7, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v7}, Lcom/konka/factory/desk/FactoryDeskImpl;->GetResolutionMapping(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    goto/16 :goto_1

    :pswitch_13
    iget-object v6, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDTVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v7, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v7}, Lcom/konka/factory/desk/FactoryDeskImpl;->GetResolutionMapping(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_9
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_0
        :pswitch_13
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method public getOverScanSourceType()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .locals 2

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v1}, Lcom/konka/factory/desk/FactoryDB;->queryCurInputSrc()I

    move-result v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    if-le v0, v1, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    :goto_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    return-object v1

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    aget-object v1, v1, v0

    iput-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    goto :goto_0
.end method

.method public getOverScanVposition()S
    .locals 8

    iget-object v6, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v6}, Lcom/konka/factory/desk/FactoryDB;->queryCurInputSrc()I

    move-result v3

    iget-object v6, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v6, v3}, Lcom/konka/factory/desk/FactoryDB;->queryArcMode(I)I

    move-result v0

    const/4 v5, 0x0

    :try_start_0
    iget-object v6, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mtvplayer:Lcom/mstar/android/tvapi/common/TvPlayer;

    invoke-interface {v6}, Lcom/mstar/android/tvapi/common/TvPlayer;->getVideoStandard()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    :goto_0
    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_AVD_VideoStandardType;->values()[Lcom/konka/factory/desk/IFactoryDesk$MAPI_AVD_VideoStandardType;

    move-result-object v6

    aget-object v4, v6, v5

    sget-object v6, Lcom/konka/factory/desk/FactoryDeskImpl$3;->$SwitchMap$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    iget-object v7, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v7}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    :pswitch_0
    const/4 v6, -0x1

    :goto_1
    return v6

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_1
    sget-object v6, Lcom/konka/factory/desk/FactoryDeskImpl$3;->$SwitchMap$com$konka$factory$desk$IFactoryDesk$MAPI_AVD_VideoStandardType:[I

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_AVD_VideoStandardType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_1

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    :goto_2
    iget-object v6, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryVDOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v2}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    goto :goto_1

    :pswitch_2
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_3
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_4
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_SECAM:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_5
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_6
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL_M:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_7
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL_NC:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_8
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_9
    sget-object v6, Lcom/konka/factory/desk/FactoryDeskImpl$3;->$SwitchMap$com$konka$factory$desk$IFactoryDesk$MAPI_AVD_VideoStandardType:[I

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_AVD_VideoStandardType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_2

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    :goto_3
    iget-object v6, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryATVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v2}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    goto :goto_1

    :pswitch_a
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_b
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_c
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_SECAM:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_d
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_e
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL_M:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_f
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL_NC:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_10
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_11
    iget-object v6, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryYPbPrOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v7, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v7}, Lcom/konka/factory/desk/FactoryDeskImpl;->GetResolutionMapping(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    goto :goto_1

    :pswitch_12
    iget-object v6, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryHDMIOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v7, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v7}, Lcom/konka/factory/desk/FactoryDeskImpl;->GetResolutionMapping(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    goto/16 :goto_1

    :pswitch_13
    iget-object v6, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDTVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v7, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v7}, Lcom/konka/factory/desk/FactoryDeskImpl;->GetResolutionMapping(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_9
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_0
        :pswitch_13
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method public getOverScanVsize()S
    .locals 8

    iget-object v6, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v6}, Lcom/konka/factory/desk/FactoryDB;->queryCurInputSrc()I

    move-result v3

    iget-object v6, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v6, v3}, Lcom/konka/factory/desk/FactoryDB;->queryArcMode(I)I

    move-result v0

    const/4 v5, 0x0

    :try_start_0
    iget-object v6, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mtvplayer:Lcom/mstar/android/tvapi/common/TvPlayer;

    invoke-interface {v6}, Lcom/mstar/android/tvapi/common/TvPlayer;->getVideoStandard()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    :goto_0
    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_AVD_VideoStandardType;->values()[Lcom/konka/factory/desk/IFactoryDesk$MAPI_AVD_VideoStandardType;

    move-result-object v6

    aget-object v4, v6, v5

    sget-object v6, Lcom/konka/factory/desk/FactoryDeskImpl$3;->$SwitchMap$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    iget-object v7, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v7}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    :pswitch_0
    const/4 v6, -0x1

    :goto_1
    return v6

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_1
    sget-object v6, Lcom/konka/factory/desk/FactoryDeskImpl$3;->$SwitchMap$com$konka$factory$desk$IFactoryDesk$MAPI_AVD_VideoStandardType:[I

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_AVD_VideoStandardType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_1

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    :goto_2
    iget-object v6, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryVDOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v2}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    goto :goto_1

    :pswitch_2
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_3
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_4
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_SECAM:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_5
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_6
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL_M:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_7
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL_NC:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_8
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_9
    sget-object v6, Lcom/konka/factory/desk/FactoryDeskImpl$3;->$SwitchMap$com$konka$factory$desk$IFactoryDesk$MAPI_AVD_VideoStandardType:[I

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_AVD_VideoStandardType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_2

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    :goto_3
    iget-object v6, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryATVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v2}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    goto :goto_1

    :pswitch_a
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_b
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_c
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_SECAM:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_d
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_e
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL_M:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_f
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL_NC:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_10
    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_11
    iget-object v6, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryYPbPrOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v7, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v7}, Lcom/konka/factory/desk/FactoryDeskImpl;->GetResolutionMapping(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    goto :goto_1

    :pswitch_12
    iget-object v6, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryHDMIOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v7, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v7}, Lcom/konka/factory/desk/FactoryDeskImpl;->GetResolutionMapping(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    goto/16 :goto_1

    :pswitch_13
    iget-object v6, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDTVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v7, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v7}, Lcom/konka/factory/desk/FactoryDeskImpl;->GetResolutionMapping(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_9
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_0
        :pswitch_13
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method public getPQVersion(I)Ljava/lang/String;
    .locals 4
    .param p1    # I

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    if-eqz p1, :cond_0

    const/4 v2, 0x1

    if-ne p1, v2, :cond_1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->values()[Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    move-result-object v3

    aget-object v3, v3, p1

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/factory/FactoryManager;->getPQVersion(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :cond_1
    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getPanelSwing()S
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryExternSetting:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

    iget-short v0, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->panelSwingVal:S

    return v0
.end method

.method public getPanelType()Ljava/lang/String;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryExternSetting:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getSystemPanelName()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->panelType:Ljava/lang/String;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryExternSetting:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->panelType:Ljava/lang/String;

    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getPeqFoCoarse(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryPEQSet:Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v0, v0, Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;

    aget-object v0, v0, p1

    iget v0, v0, Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;->Foh:I

    return v0
.end method

.method public getPeqFoFine(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryPEQSet:Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v0, v0, Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;

    aget-object v0, v0, p1

    iget v0, v0, Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;->Fol:I

    return v0
.end method

.method public getPeqGain(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryPEQSet:Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v0, v0, Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;

    aget-object v0, v0, p1

    iget v0, v0, Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;->Gain:I

    return v0
.end method

.method public getPeqQ(I)I
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryPEQSet:Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v0, v0, Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;

    aget-object v0, v0, p1

    iget v0, v0, Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;->QValue:I

    return v0
.end method

.method public getPictureModeIdx()I
    .locals 3

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v2}, Lcom/konka/factory/desk/FactoryDB;->queryCurInputSrc()I

    move-result v0

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v2, v0}, Lcom/konka/factory/desk/FactoryDB;->queryePicMode(I)I

    move-result v1

    return v1
.end method

.method public getPowerOffLogoDspTime()I
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryCusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    iget v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->PowerOffLogoDspTime:I

    return v0
.end method

.method public getPowerOffLogoMode()I
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryCusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    iget v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->PowerOffLogoEnabled:I

    return v0
.end method

.method public getPowerOnMode()I
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryExternSetting:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

    iget v0, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->stPowerMode:I

    return v0
.end method

.method public getSoundEqItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;I)I
    .locals 1
    .param p1    # Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;
    .param p2    # I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v0, p1, p2}, Lcom/konka/factory/desk/FactoryDB;->querySoundEqSetting(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;I)I

    move-result v0

    return v0
.end method

.method public getTeletextMode()I
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryCusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    iget v0, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->IsTeletextEnabled:I

    return v0
.end method

.method public getTestPattern()I
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryExternSetting:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

    iget v0, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->testPatternMode:I

    return v0
.end method

.method public getUartOnOff()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    sget-object v2, Lcom/konka/factory/desk/FactoryDB$USER_SETTING_FIELD;->bUartBus:Lcom/konka/factory/desk/FactoryDB$USER_SETTING_FIELD;

    invoke-virtual {v1, v2}, Lcom/konka/factory/desk/FactoryDB;->queryUserSysSetting(Lcom/konka/factory/desk/FactoryDB$USER_SETTING_FIELD;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;)I
    .locals 3
    .param p1    # Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v2}, Lcom/konka/factory/desk/FactoryDB;->queryCurInputSrc()I

    move-result v0

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v2, v0}, Lcom/konka/factory/desk/FactoryDB;->queryePicMode(I)I

    move-result v1

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v2, p1, v0, v1}, Lcom/konka/factory/desk/FactoryDB;->queryPicModeSetting(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;II)I

    move-result v2

    return v2
.end method

.method public getVifAgcRef()S
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    iget-short v0, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifVersion:S

    return v0
.end method

.method public getVifAsiaSignalOption()Z
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    iget-boolean v0, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifAsiaSignalOption:Z

    return v0
.end method

.method public getVifClampGainOvNegative()I
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    iget v0, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifClampgainGainOvNegative:I

    return v0
.end method

.method public getVifCrKi()S
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    iget-short v0, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifCrKi:S

    return v0
.end method

.method public getVifCrKp()S
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    iget-short v0, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifCrKp:S

    return v0
.end method

.method public getVifCrKpKiAdjust()Z
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    iget-boolean v0, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifCrKpKiAdjust:Z

    return v0
.end method

.method public getVifCrThr()I
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    iget v0, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifCrThr:I

    return v0
.end method

.method public getVifOverModulation()Z
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    iget-boolean v0, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifOverModulation:Z

    return v0
.end method

.method public getVifTop()S
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    iget-short v0, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifTop:S

    return v0
.end method

.method public getVifVersion()S
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    iget-short v0, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifVersion:S

    return v0
.end method

.method public getVifVgaMaximum()I
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    iget v0, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifVgaMaximum:I

    return v0
.end method

.method public getWatchDogMode()S
    .locals 2

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    sget-object v1, Lcom/konka/factory/desk/FactoryDB$USER_SETTING_FIELD;->bEnableWDT:Lcom/konka/factory/desk/FactoryDB$USER_SETTING_FIELD;

    invoke-virtual {v0, v1}, Lcom/konka/factory/desk/FactoryDB;->queryUserSysSetting(Lcom/konka/factory/desk/FactoryDB$USER_SETTING_FIELD;)I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method public getWbBlueGain()I
    .locals 5

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4}, Lcom/konka/factory/desk/FactoryDB;->queryCurInputSrc()I

    move-result v0

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4, v0}, Lcom/konka/factory/desk/FactoryDB;->queryePicMode(I)I

    move-result v2

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4, v0, v2}, Lcom/konka/factory/desk/FactoryDB;->queryColorTmpIdx(II)I

    move-result v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    aget-object v4, v4, v0

    invoke-virtual {p0, v4}, Lcom/konka/factory/desk/FactoryDeskImpl;->InputSourceTransfer(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v3

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryColorTmpEx:Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;

    iget-object v4, v4, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v4, v4, v1

    aget-object v4, v4, v3

    iget v4, v4, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;->bluegain:I

    return v4
.end method

.method public getWbBlueOffset()I
    .locals 5

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4}, Lcom/konka/factory/desk/FactoryDB;->queryCurInputSrc()I

    move-result v0

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4, v0}, Lcom/konka/factory/desk/FactoryDB;->queryePicMode(I)I

    move-result v2

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4, v0, v2}, Lcom/konka/factory/desk/FactoryDB;->queryColorTmpIdx(II)I

    move-result v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    aget-object v4, v4, v0

    invoke-virtual {p0, v4}, Lcom/konka/factory/desk/FactoryDeskImpl;->InputSourceTransfer(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v3

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryColorTmpEx:Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;

    iget-object v4, v4, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v4, v4, v1

    aget-object v4, v4, v3

    iget v4, v4, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;->blueoffset:I

    return v4
.end method

.method public getWbGreenGain()I
    .locals 5

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4}, Lcom/konka/factory/desk/FactoryDB;->queryCurInputSrc()I

    move-result v0

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4, v0}, Lcom/konka/factory/desk/FactoryDB;->queryePicMode(I)I

    move-result v2

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4, v0, v2}, Lcom/konka/factory/desk/FactoryDB;->queryColorTmpIdx(II)I

    move-result v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    aget-object v4, v4, v0

    invoke-virtual {p0, v4}, Lcom/konka/factory/desk/FactoryDeskImpl;->InputSourceTransfer(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v3

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryColorTmpEx:Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;

    iget-object v4, v4, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v4, v4, v1

    aget-object v4, v4, v3

    iget v4, v4, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;->greengain:I

    return v4
.end method

.method public getWbGreenOffset()I
    .locals 5

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4}, Lcom/konka/factory/desk/FactoryDB;->queryCurInputSrc()I

    move-result v0

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4, v0}, Lcom/konka/factory/desk/FactoryDB;->queryePicMode(I)I

    move-result v2

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4, v0, v2}, Lcom/konka/factory/desk/FactoryDB;->queryColorTmpIdx(II)I

    move-result v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    aget-object v4, v4, v0

    invoke-virtual {p0, v4}, Lcom/konka/factory/desk/FactoryDeskImpl;->InputSourceTransfer(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v3

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryColorTmpEx:Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;

    iget-object v4, v4, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v4, v4, v1

    aget-object v4, v4, v3

    iget v4, v4, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;->greenoffset:I

    return v4
.end method

.method public getWbRedGain()I
    .locals 5

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4}, Lcom/konka/factory/desk/FactoryDB;->queryCurInputSrc()I

    move-result v0

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4, v0}, Lcom/konka/factory/desk/FactoryDB;->queryePicMode(I)I

    move-result v2

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4, v0, v2}, Lcom/konka/factory/desk/FactoryDB;->queryColorTmpIdx(II)I

    move-result v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    aget-object v4, v4, v0

    invoke-virtual {p0, v4}, Lcom/konka/factory/desk/FactoryDeskImpl;->InputSourceTransfer(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v3

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryColorTmpEx:Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;

    iget-object v4, v4, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v4, v4, v1

    aget-object v4, v4, v3

    iget v4, v4, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;->redgain:I

    return v4
.end method

.method public getWbRedOffset()I
    .locals 5

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4}, Lcom/konka/factory/desk/FactoryDB;->queryCurInputSrc()I

    move-result v0

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4, v0}, Lcom/konka/factory/desk/FactoryDB;->queryePicMode(I)I

    move-result v2

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4, v0, v2}, Lcom/konka/factory/desk/FactoryDB;->queryColorTmpIdx(II)I

    move-result v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    aget-object v4, v4, v0

    invoke-virtual {p0, v4}, Lcom/konka/factory/desk/FactoryDeskImpl;->InputSourceTransfer(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v3

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryColorTmpEx:Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;

    iget-object v4, v4, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v4, v4, v1

    aget-object v4, v4, v3

    iget v4, v4, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;->redoffset:I

    return v4
.end method

.method public loadCurAdcDataFromDB(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v0, p1}, Lcom/konka/factory/desk/FactoryDB;->queryADCAdjust(I)Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    return-void
.end method

.method public loadEssentialDataFromDB()V
    .locals 6

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4}, Lcom/konka/factory/desk/FactoryDB;->queryCurInputSrc()I

    move-result v0

    invoke-virtual {p0}, Lcom/konka/factory/desk/FactoryDeskImpl;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_VGA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v1, v4, :cond_0

    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;->ADC_SET_VGA:Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;

    iput-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->eAdcIdx:Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;

    :goto_0
    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4, v0}, Lcom/konka/factory/desk/FactoryDB;->queryePicMode(I)I

    move-result v3

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4, v0, v3}, Lcom/konka/factory/desk/FactoryDB;->queryColorTmpIdx(II)I

    move-result v2

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4, v2}, Lcom/konka/factory/desk/FactoryDB;->queryFactoryColorTempData(I)Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMP_DATA;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->wb_gainoffset:Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMP_DATA;

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4}, Lcom/konka/factory/desk/FactoryDB;->queryNoStandVifSet()Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4}, Lcom/konka/factory/desk/FactoryDB;->queryNoStandSet()Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4}, Lcom/konka/factory/desk/FactoryDB;->queryFactoryExtern()Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryExternSetting:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v4}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryCusDefSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryCusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4}, Lcom/konka/factory/desk/FactoryDB;->querySSCAdjust()Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->sscSet:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4}, Lcom/konka/factory/desk/FactoryDB;->queryPEQAdjusts()Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryPEQSet:Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4}, Lcom/konka/factory/desk/FactoryDB;->queryFactoryColorTempExData()Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryColorTmpEx:Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4}, Lcom/konka/factory/desk/FactoryDB;->queryNonLinearAdjusts()Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mfactoryNLASet:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/konka/factory/desk/FactoryDB;->queryOverscanAdjusts(I)[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDTVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/konka/factory/desk/FactoryDB;->queryOverscanAdjusts(I)[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryHDMIOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/konka/factory/desk/FactoryDB;->queryOverscanAdjusts(I)[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryYPbPrOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Lcom/konka/factory/desk/FactoryDB;->queryOverscanAdjusts(I)[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryVDOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/konka/factory/desk/FactoryDB;->queryOverscanAdjusts(I)[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryATVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v5, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->eAdcIdx:Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;

    invoke-virtual {v5}, Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;->ordinal()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/konka/factory/desk/FactoryDB;->queryADCAdjust(I)Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4}, Lcom/konka/factory/desk/FactoryDB;->queryCustomerCfgMiscSetting()Lcom/konka/factory/desk/IFactoryDesk$CustomerCfgMiscSetting;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->customerCfgMiscSetting:Lcom/konka/factory/desk/IFactoryDesk$CustomerCfgMiscSetting;

    return-void

    :cond_0
    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->context:Landroid/content/Context;

    invoke-static {v4}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/CommonDesk;->getVideoInfo()Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v4, v4, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    const/16 v5, 0x2d0

    if-lt v4, v5, :cond_1

    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;->ADC_SET_YPBPR_HD:Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;

    iput-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->eAdcIdx:Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;

    goto/16 :goto_0

    :cond_1
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;->ADC_SET_YPBPR_SD:Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;

    iput-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->eAdcIdx:Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;

    goto/16 :goto_0
.end method

.method public releaseHandler(I)V
    .locals 2
    .param p1    # I

    const/4 v0, 0x4

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pHandler:[Landroid/os/Handler;

    const/4 v1, 0x0

    aput-object v1, v0, p1

    :cond_0
    return-void
.end method

.method public restoreToDefault()Z
    .locals 5

    const/4 v4, 0x0

    new-instance v0, Ljava/io/File;

    const-string v1, "/tvdatabase/DatabaseBackup/"

    const-string v2, "user_setting.db"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/io/File;

    const-string v2, "/tvdatabase/Database/"

    const-string v3, "user_setting.db"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0, v1}, Lcom/konka/factory/desk/FactoryDeskImpl;->copyFile(Ljava/io/File;Ljava/io/File;)Z

    new-instance v0, Ljava/io/File;

    const-string v1, "/tvdatabase/DatabaseBackup/"

    const-string v2, "factory.db"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/io/File;

    const-string v2, "/tvdatabase/Database/"

    const-string v3, "factory.db"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0, v1}, Lcom/konka/factory/desk/FactoryDeskImpl;->copyFile(Ljava/io/File;Ljava/io/File;)Z

    return v4
.end method

.method public set3DSelfAdaptiveLevel(I)Z
    .locals 5
    .param p1    # I

    const/4 v4, 0x1

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v3}, Lcom/konka/factory/desk/FactoryDB;->queryCurInputSrc()I

    move-result v3

    invoke-virtual {v2, p1, v3}, Lcom/konka/factory/desk/FactoryDB;->updateSelfAdaptiveLevel(II)V

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;-><init>()V

    if-nez p1, :cond_1

    const/4 v2, 0x4

    :try_start_0
    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->gYPixelThreshold:I

    const/4 v2, 0x4

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->rCrPixelThreshold:I

    const/4 v2, 0x4

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->bCbPixelThreshold:I

    const/4 v2, 0x7

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->horSampleCount:I

    const/4 v2, 0x7

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->verSampleCount:I

    const/16 v2, 0x5a

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->hitPixelPercentage:I

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->s3m:Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->setDetect3dFormatParameters(Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return v4

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_1
    if-ne p1, v4, :cond_2

    const/16 v2, 0x8

    :try_start_1
    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->gYPixelThreshold:I

    const/16 v2, 0x8

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->rCrPixelThreshold:I

    const/16 v2, 0x8

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->bCbPixelThreshold:I

    const/4 v2, 0x6

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->horSampleCount:I

    const/4 v2, 0x6

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->verSampleCount:I

    const/16 v2, 0x46

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->hitPixelPercentage:I

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->s3m:Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->setDetect3dFormatParameters(Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;)Z
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_2
    const/4 v2, 0x2

    if-ne p1, v2, :cond_0

    const/16 v2, 0xc

    :try_start_2
    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->gYPixelThreshold:I

    const/16 v2, 0xc

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->rCrPixelThreshold:I

    const/16 v2, 0xc

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->bCbPixelThreshold:I

    const/4 v2, 0x5

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->horSampleCount:I

    const/4 v2, 0x5

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->verSampleCount:I

    const/16 v2, 0x32

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->hitPixelPercentage:I

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->s3m:Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->setDetect3dFormatParameters(Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;)Z
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setADCBlueGain(I)Z
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->eAdcIdx:Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;

    invoke-virtual {v1}, Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;->ordinal()I

    move-result v0

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iput p1, v1, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->blueGain:I

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-direct {p0, v1, v0}, Lcom/konka/factory/desk/FactoryDeskImpl;->setADCGainOffset(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;I)V

    const/4 v1, 0x1

    return v1
.end method

.method public setADCBlueOffset(I)Z
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->eAdcIdx:Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;

    invoke-virtual {v1}, Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;->ordinal()I

    move-result v0

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iput p1, v1, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->blueOffset:I

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-direct {p0, v1, v0}, Lcom/konka/factory/desk/FactoryDeskImpl;->setADCGainOffset(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;I)V

    const/4 v1, 0x1

    return v1
.end method

.method public setADCGreenGain(I)Z
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->eAdcIdx:Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;

    invoke-virtual {v1}, Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;->ordinal()I

    move-result v0

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iput p1, v1, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->greenGain:I

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-direct {p0, v1, v0}, Lcom/konka/factory/desk/FactoryDeskImpl;->setADCGainOffset(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;I)V

    const/4 v1, 0x1

    return v1
.end method

.method public setADCGreenOffset(I)Z
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->eAdcIdx:Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;

    invoke-virtual {v1}, Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;->ordinal()I

    move-result v0

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iput p1, v1, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->greenOffset:I

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-direct {p0, v1, v0}, Lcom/konka/factory/desk/FactoryDeskImpl;->setADCGainOffset(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;I)V

    const/4 v1, 0x1

    return v1
.end method

.method public setADCPhase(I)Z
    .locals 3
    .param p1    # I

    iput p1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->f_phase:I

    :try_start_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->tp:Lcom/mstar/android/tvapi/common/TvPlayer;

    iget v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->f_phase:I

    invoke-interface {v1, v2}, Lcom/mstar/android/tvapi/common/TvPlayer;->setPhase(I)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setADCRedGain(I)Z
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->eAdcIdx:Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;

    invoke-virtual {v1}, Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;->ordinal()I

    move-result v0

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iput p1, v1, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->redGain:I

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-direct {p0, v1, v0}, Lcom/konka/factory/desk/FactoryDeskImpl;->setADCGainOffset(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;I)V

    const/4 v1, 0x1

    return v1
.end method

.method public setADCRedOffset(I)Z
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->eAdcIdx:Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;

    invoke-virtual {v1}, Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;->ordinal()I

    move-result v0

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iput p1, v1, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->redOffset:I

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-direct {p0, v1, v0}, Lcom/konka/factory/desk/FactoryDeskImpl;->setADCGainOffset(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;I)V

    const/4 v1, 0x1

    return v1
.end method

.method public setAEFC_43(S)Z
    .locals 3
    .param p1    # S

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iput-short p1, v1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_43:S

    :try_start_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdInitParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/konka/factory/desk/FactoryDB;->updateNonStandardAdjust(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAEFC_44(S)Z
    .locals 3
    .param p1    # S

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iput-short p1, v1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_44:S

    :try_start_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdInitParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/konka/factory/desk/FactoryDB;->updateNonStandardAdjust(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAEFC_66Bit76(S)Z
    .locals 3
    .param p1    # S

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iput-short p1, v1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_66_Bit76:S

    :try_start_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdInitParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/konka/factory/desk/FactoryDB;->updateNonStandardAdjust(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAEFC_6EBit3210(S)Z
    .locals 3
    .param p1    # S

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iput-short p1, v1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_6E_Bit3210:S

    :try_start_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdInitParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/konka/factory/desk/FactoryDB;->updateNonStandardAdjust(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAEFC_6EBit7654(S)Z
    .locals 3
    .param p1    # S

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iput-short p1, v1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_6E_Bit7654:S

    :try_start_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdInitParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/konka/factory/desk/FactoryDB;->updateNonStandardAdjust(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAEFC_A0(S)Z
    .locals 3
    .param p1    # S

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iput-short p1, v1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_A0:S

    :try_start_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdInitParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/konka/factory/desk/FactoryDB;->updateNonStandardAdjust(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAEFC_A1(S)Z
    .locals 3
    .param p1    # S

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iput-short p1, v1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_A1:S

    :try_start_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdInitParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/konka/factory/desk/FactoryDB;->updateNonStandardAdjust(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAEFC_CB(S)Z
    .locals 3
    .param p1    # S

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iput-short p1, v1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_CB:S

    :try_start_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdInitParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/konka/factory/desk/FactoryDB;->updateNonStandardAdjust(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAEFC_CFBit2_ATV(S)Z
    .locals 3
    .param p1    # S

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iput-short p1, v1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_CF_Bit2_ATV:S

    :try_start_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdInitParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/konka/factory/desk/FactoryDB;->updateNonStandardAdjust(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAEFC_CFBit2_AV(S)Z
    .locals 3
    .param p1    # S

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iput-short p1, v1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_CF_Bit2_AV:S

    :try_start_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdInitParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/konka/factory/desk/FactoryDB;->updateNonStandardAdjust(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAEFC_D4(S)Z
    .locals 3
    .param p1    # S

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iput-short p1, v1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D4:S

    :try_start_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdInitParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/konka/factory/desk/FactoryDB;->updateNonStandardAdjust(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAEFC_D5Bit2(S)Z
    .locals 3
    .param p1    # S

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iput-short p1, v1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D5_Bit2:S

    :try_start_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdInitParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/konka/factory/desk/FactoryDB;->updateNonStandardAdjust(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAEFC_D7HighBoun(S)Z
    .locals 3
    .param p1    # S

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iput-short p1, v1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D7_HIGH_BOUND:S

    :try_start_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdInitParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/konka/factory/desk/FactoryDB;->updateNonStandardAdjust(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAEFC_D7LowBoun(S)Z
    .locals 3
    .param p1    # S

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iput-short p1, v1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D7_LOW_BOUND:S

    :try_start_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdInitParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/konka/factory/desk/FactoryDB;->updateNonStandardAdjust(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAEFC_D8Bit3210(S)Z
    .locals 3
    .param p1    # S

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iput-short p1, v1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D8_Bit3210:S

    :try_start_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdInitParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/konka/factory/desk/FactoryDB;->updateNonStandardAdjust(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAEFC_D9Bit0(S)Z
    .locals 3
    .param p1    # S

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iput-short p1, v1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D9_Bit0:S

    :try_start_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdInitParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/konka/factory/desk/FactoryDB;->updateNonStandardAdjust(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAdcIdx(Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;)Z
    .locals 1
    .param p1    # Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;

    iput-object p1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->eAdcIdx:Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;

    const/4 v0, 0x1

    return v0
.end method

.method public setAudioDelayTime(I)Z
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryCusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    iput p1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->AudioDelayTime:I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryCusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->setFactoryCusDefSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;)Z

    const/4 v0, 0x1

    return v0
.end method

.method public setAudioHiDevMode(I)Z
    .locals 4
    .param p1    # I

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryExternSetting:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

    iput p1, v1, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->eHidevMode:I

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryExternSetting:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

    invoke-virtual {v1, v2}, Lcom/konka/factory/desk/FactoryDB;->updateFactoryExtern(Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;)V

    :try_start_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->am:Lcom/mstar/android/tvapi/common/AudioManager;

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAtvInfoType;->E_ATV_HIDEV_INFO:Lcom/mstar/android/tvapi/common/vo/EnumAtvInfoType;

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumSoundHidevMode;->values()[Lcom/mstar/android/tvapi/common/vo/EnumSoundHidevMode;

    move-result-object v3

    aget-object v3, v3, p1

    invoke-virtual {v1, v2, v3}, Lcom/mstar/android/tvapi/common/AudioManager;->setAtvInfo(Lcom/mstar/android/tvapi/common/vo/EnumAtvInfoType;Lcom/mstar/android/tvapi/common/vo/EnumSoundHidevMode;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAudioNrThr(S)Z
    .locals 3
    .param p1    # S

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryExternSetting:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

    iput-short p1, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->audioNrThr:S

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryExternSetting:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

    invoke-virtual {v0, v1}, Lcom/konka/factory/desk/FactoryDB;->updateFactoryExtern(Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;)V

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->am:Lcom/mstar/android/tvapi/common/AudioManager;

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_NR_THRESHOLD_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Lcom/mstar/android/tvapi/common/AudioManager;->setSoundParameter(Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;II)S

    const/4 v0, 0x1

    return v0
.end method

.method public setAudioPrescale(S)Z
    .locals 5
    .param p1    # S

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryExternSetting:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

    iput-short p1, v3, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->audioPreScale:S

    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryExternSetting:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

    invoke-virtual {v3, v4}, Lcom/konka/factory/desk/FactoryDB;->updateFactoryExtern(Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;)V

    :try_start_0
    new-instance v1, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;

    invoke-direct {v1}, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;-><init>()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iput p1, v1, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->preScale:I

    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->am:Lcom/mstar/android/tvapi/common/AudioManager;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_PRESCALE:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    invoke-virtual {v3, v4, v1}, Lcom/mstar/android/tvapi/common/AudioManager;->setBasicSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v1

    :goto_0
    const/4 v3, 0x1

    return v3

    :catch_0
    move-exception v2

    :goto_1
    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v2

    move-object v0, v1

    goto :goto_1
.end method

.method public setBurnInMode(Z)Z
    .locals 6

    const/4 v5, 0x1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryExternSetting:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

    iput-boolean p1, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->m_bAgingMode:Z

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryExternSetting:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

    invoke-virtual {v0, v1}, Lcom/konka/factory/desk/FactoryDB;->updateFactoryExtern(Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;)V

    if-ne v5, p1, :cond_2

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    const-string v1, "factory_burnin_mode"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Lcom/mstar/android/tvapi/common/TvManager;->setEnvironment(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->context:Landroid/content/Context;

    invoke-static {v0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->isSignalStable()Z

    move-result v0

    if-nez v0, :cond_4

    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-eq v0, v1, :cond_0

    if-ne p1, v5, :cond_3

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_WHITE:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->context:Landroid/content/Context;

    invoke-static {v3}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v3

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_1
    return v5

    :cond_2
    :try_start_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    const-string v1, "factory_burnin_mode"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Lcom/mstar/android/tvapi/common/TvManager;->setEnvironment(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_3
    :try_start_3
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    const/4 v1, 0x1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->context:Landroid/content/Context;

    invoke-static {v4}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :cond_4
    if-nez p1, :cond_1

    :try_start_4
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    const/4 v1, 0x0

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;->E_BLACK:Lcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->context:Landroid/content/Context;

    invoke-static {v4}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v4

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/mstar/android/tvapi/common/TvManager;->setVideoMute(ZLcom/mstar/android/tvapi/common/vo/EnumScreenMuteType;ILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_4
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public setChinaDescramblerBox(S)Z
    .locals 2
    .param p1    # S

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    iput-short p1, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->ChinaDescramblerBox:S

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    invoke-virtual {v0, v1}, Lcom/konka/factory/desk/FactoryDB;->updateNonStandardAdjust(Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;)V

    invoke-direct {p0}, Lcom/konka/factory/desk/FactoryDeskImpl;->InitVif()V

    const/4 v0, 0x1

    return v0
.end method

.method public setColorTmpIdx(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;)V
    .locals 5
    .param p1    # Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4}, Lcom/konka/factory/desk/FactoryDB;->queryCurInputSrc()I

    move-result v0

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4, v0}, Lcom/konka/factory/desk/FactoryDB;->queryePicMode(I)I

    move-result v2

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4, v0, v2, p1}, Lcom/konka/factory/desk/FactoryDB;->updateColorTempIdx(IILcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;)V

    invoke-virtual {p1}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;->ordinal()I

    move-result v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    aget-object v4, v4, v0

    invoke-virtual {p0, v4}, Lcom/konka/factory/desk/FactoryDeskImpl;->InputSourceTransfer(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v3

    invoke-direct {p0, v1, v0, v3}, Lcom/konka/factory/desk/FactoryDeskImpl;->setWBGainOffset(III)V

    return-void
.end method

.method public setCurveType(Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;)Z
    .locals 1
    .param p1    # Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mfactoryNLASet:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

    iput-object p1, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    const/4 v0, 0x1

    return v0
.end method

.method public setCustomerSettingEnable()Z
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryCusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->IsRestoreNeeded:I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryCusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    iput v1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->HotelEnabled:I

    return v1
.end method

.method public setDefaultAutoTuningCountry(I)Z
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryCusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    iput p1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->DefTuningCountry:I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateCUS_DEF_SETTING()V

    const/4 v0, 0x1

    return v0
.end method

.method public setDefaultLanguage(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryCusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    iput-object p1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->DefOSDLanguage:Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->updateCUS_DEF_SETTING()V

    const/4 v0, 0x1

    return v0
.end method

.method public setDelayReduce(S)Z
    .locals 2
    .param p1    # S

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    iput-short p1, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifDelayReduce:S

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    invoke-virtual {v0, v1}, Lcom/konka/factory/desk/FactoryDB;->updateNonStandardAdjust(Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;)V

    invoke-direct {p0}, Lcom/konka/factory/desk/FactoryDeskImpl;->InitVif()V

    const/4 v0, 0x1

    return v0
.end method

.method public setDtvAvAbnormalDelay(Z)Z
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryExternSetting:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

    iput-boolean p1, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->dtvAvAbnormalDelay:Z

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryExternSetting:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

    invoke-virtual {v0, v1}, Lcom/konka/factory/desk/FactoryDB;->updateFactoryExtern(Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;)V

    const/4 v0, 0x1

    return v0
.end method

.method public setEQ(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;)Z
    .locals 7
    .param p1    # Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;

    const/4 v6, 0x1

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;-><init>()V

    invoke-virtual {p0, p1, v6}, Lcom/konka/factory/desk/FactoryDeskImpl;->getSoundEqItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;I)I

    :try_start_0
    iget-object v2, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0, p1, v3}, Lcom/konka/factory/desk/FactoryDeskImpl;->getSoundEqItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;I)I

    move-result v3

    iput v3, v2, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v2, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    const/4 v3, 0x2

    invoke-virtual {p0, p1, v3}, Lcom/konka/factory/desk/FactoryDeskImpl;->getSoundEqItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;I)I

    move-result v3

    iput v3, v2, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v2, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    const/4 v3, 0x3

    invoke-virtual {p0, p1, v3}, Lcom/konka/factory/desk/FactoryDeskImpl;->getSoundEqItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;I)I

    move-result v3

    iput v3, v2, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v2, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    const/4 v3, 0x4

    invoke-virtual {p0, p1, v3}, Lcom/konka/factory/desk/FactoryDeskImpl;->getSoundEqItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;I)I

    move-result v3

    iput v3, v2, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    iget-object v2, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    const/4 v3, 0x5

    invoke-virtual {p0, p1, v3}, Lcom/konka/factory/desk/FactoryDeskImpl;->getSoundEqItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;I)I

    move-result v3

    iput v3, v2, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "terry set eq1: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    iget v4, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "eq2:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x1

    aget-object v4, v4, v5

    iget v4, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "eq3:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x2

    aget-object v4, v4, v5

    iget v4, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "eq4:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x3

    aget-object v4, v4, v5

    iget v4, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "eq5:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterEqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;

    const/4 v5, 0x4

    aget-object v4, v4, v5

    iget v4, v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterEq;->eqLevel:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v2, 0x5

    iput-short v2, v0, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->eqBandNumber:S

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->am:Lcom/mstar/android/tvapi/common/AudioManager;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_EQ:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/mstar/android/tvapi/common/AudioManager;->enableBasicSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;Z)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->am:Lcom/mstar/android/tvapi/common/AudioManager;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_EQ:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    invoke-virtual {v2, v3, v0}, Lcom/mstar/android/tvapi/common/AudioManager;->setBasicSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v6

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setEnergyEnable(Z)Z
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->customerCfgMiscSetting:Lcom/konka/factory/desk/IFactoryDesk$CustomerCfgMiscSetting;

    iput-boolean p1, v0, Lcom/konka/factory/desk/IFactoryDesk$CustomerCfgMiscSetting;->energyEnable:Z

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->customerCfgMiscSetting:Lcom/konka/factory/desk/IFactoryDesk$CustomerCfgMiscSetting;

    invoke-virtual {v0, v1}, Lcom/konka/factory/desk/FactoryDB;->updateCustomerCfgMiscSetting(Lcom/konka/factory/desk/IFactoryDesk$CustomerCfgMiscSetting;)V

    const/4 v0, 0x1

    return v0
.end method

.method public setEnergyPercent(S)Z
    .locals 2
    .param p1    # S

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->customerCfgMiscSetting:Lcom/konka/factory/desk/IFactoryDesk$CustomerCfgMiscSetting;

    iput-short p1, v0, Lcom/konka/factory/desk/IFactoryDesk$CustomerCfgMiscSetting;->energyPercent:S

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->customerCfgMiscSetting:Lcom/konka/factory/desk/IFactoryDesk$CustomerCfgMiscSetting;

    invoke-virtual {v0, v1}, Lcom/konka/factory/desk/FactoryDB;->updateCustomerCfgMiscSetting(Lcom/konka/factory/desk/IFactoryDesk$CustomerCfgMiscSetting;)V

    const/4 v0, 0x1

    return v0
.end method

.method public setFactoryPreSetFeature(I)Z
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryExternSetting:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

    iput p1, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->factoryPreset:I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryExternSetting:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

    invoke-virtual {v0, v1}, Lcom/konka/factory/desk/FactoryDB;->updateFactoryExtern(Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;)V

    const/4 v0, 0x1

    return v0
.end method

.method public setGainDistributionThr(I)Z
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    iput p1, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->GainDistributionThr:I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    invoke-virtual {v0, v1}, Lcom/konka/factory/desk/FactoryDB;->updateNonStandardAdjust(Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;)V

    invoke-direct {p0}, Lcom/konka/factory/desk/FactoryDeskImpl;->InitVif()V

    const/4 v0, 0x1

    return v0
.end method

.method public setHandler(Landroid/os/Handler;I)Z
    .locals 2

    const/4 v0, 0x4

    if-ge p2, v0, :cond_1

    if-lez p2, :cond_0

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pHandler:[Landroid/os/Handler;

    aget-object v0, v0, p2

    if-eqz v0, :cond_0

    const-string v0, "TvApp"

    const-string v1, "Warning ,!!!Some Activity lose release activity"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "TvApp"

    const-string v1, "Warning ,!!!Some Activity lose release activity"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pHandler:[Landroid/os/Handler;

    aput-object p1, v0, p2

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setLVDSenable(Z)Z
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->sscSet:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;

    iput-boolean p1, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Lvds_SscEnable:Z

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->sscSet:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;

    invoke-virtual {v0, v1}, Lcom/konka/factory/desk/FactoryDB;->updateSSCAdjust(Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;)V

    invoke-direct {p0}, Lcom/konka/factory/desk/FactoryDeskImpl;->UpdateSscPara()V

    const/4 v0, 0x1

    return v0
.end method

.method public setLVDSmodulation(I)Z
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->sscSet:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;

    iput p1, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Lvds_SscSpan:I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->sscSet:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;

    invoke-virtual {v0, v1}, Lcom/konka/factory/desk/FactoryDB;->updateSSCAdjust(Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;)V

    invoke-direct {p0}, Lcom/konka/factory/desk/FactoryDeskImpl;->UpdateSscPara()V

    const/4 v0, 0x1

    return v0
.end method

.method public setLVDSpercentage(I)Z
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->sscSet:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;

    iput p1, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Lvds_SscStep:I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->sscSet:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;

    invoke-virtual {v0, v1}, Lcom/konka/factory/desk/FactoryDB;->updateSSCAdjust(Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;)V

    invoke-direct {p0}, Lcom/konka/factory/desk/FactoryDeskImpl;->UpdateSscPara()V

    const/4 v0, 0x1

    return v0
.end method

.method public setMIUenable(Z)Z
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->sscSet:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;

    iput-boolean p1, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Miu_SscEnable:Z

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->sscSet:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;

    invoke-virtual {v0, v1}, Lcom/konka/factory/desk/FactoryDB;->updateSSCAdjust(Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;)V

    invoke-direct {p0}, Lcom/konka/factory/desk/FactoryDeskImpl;->UpdateSscPara()V

    const/4 v0, 0x1

    return v0
.end method

.method public setMIUmodulation(I)Z
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->sscSet:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;

    iput p1, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Miu0_SscSpan:I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->sscSet:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;

    iput p1, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Miu1_SscSpan:I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->sscSet:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;

    iput p1, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Miu2_SscSpan:I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->sscSet:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;

    invoke-virtual {v0, v1}, Lcom/konka/factory/desk/FactoryDB;->updateSSCAdjust(Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;)V

    invoke-direct {p0}, Lcom/konka/factory/desk/FactoryDeskImpl;->UpdateSscPara()V

    const/4 v0, 0x1

    return v0
.end method

.method public setMIUpercentage(I)Z
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->sscSet:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;

    iput p1, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Miu0_SscStep:I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->sscSet:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;

    iput p1, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Miu1_SscStep:I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->sscSet:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;

    iput p1, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;->Miu2_SscStep:I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->sscSet:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;

    invoke-virtual {v0, v1}, Lcom/konka/factory/desk/FactoryDB;->updateSSCAdjust(Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_SSC_SET;)V

    invoke-direct {p0}, Lcom/konka/factory/desk/FactoryDeskImpl;->UpdateSscPara()V

    const/4 v0, 0x1

    return v0
.end method

.method public setOsdV0Nonlinear(S)Z
    .locals 5
    .param p1    # S

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mfactoryNLASet:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

    iget-object v2, v2, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v2}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v1

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mfactoryNLASet:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

    iget-object v2, v2, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;

    aget-object v2, v2, v1

    iput p1, v2, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;->u8OSD_V0:I

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mfactoryNLASet:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

    iget-object v3, v3, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;

    aget-object v3, v3, v1

    invoke-virtual {v2, v3, v1}, Lcom/konka/factory/desk/FactoryDB;->updateNonLinearAdjust(Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;I)V

    sget-object v2, Lcom/konka/factory/desk/FactoryDeskImpl$3;->$SwitchMap$com$konka$factory$desk$IFactoryDesk$MS_NLA_SET_INDEX:[I

    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mfactoryNLASet:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

    iget-object v3, v3, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v3}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :goto_0
    const/4 v2, 0x1

    return v2

    :pswitch_0
    :try_start_0
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->am:Lcom/mstar/android/tvapi/common/AudioManager;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;->E_VOL_SOURCE_SPEAKER_OUT:Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;

    int-to-byte v4, p1

    invoke-virtual {v2, v3, v4}, Lcom/mstar/android/tvapi/common/AudioManager;->setAudioVolume(Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;B)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_1
    :try_start_1
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeBrightness(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_2
    :try_start_2
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeContrast(S)V
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_3
    :try_start_3
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeColor(S)V
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_0

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_4
    :try_start_4
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeSharpness(S)V
    :try_end_4
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_0

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_5
    :try_start_5
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeTint(S)V
    :try_end_5
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_5 .. :try_end_5} :catch_5

    goto :goto_0

    :catch_5
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_6
    :try_start_6
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setBacklight(I)V
    :try_end_6
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_6 .. :try_end_6} :catch_6

    goto :goto_0

    :catch_6
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public setOsdV100Nonlinear(S)Z
    .locals 5
    .param p1    # S

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mfactoryNLASet:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

    iget-object v2, v2, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v2}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v1

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mfactoryNLASet:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

    iget-object v2, v2, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;

    aget-object v2, v2, v1

    iput p1, v2, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;->u8OSD_V100:I

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mfactoryNLASet:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

    iget-object v3, v3, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;

    aget-object v3, v3, v1

    invoke-virtual {v2, v3, v1}, Lcom/konka/factory/desk/FactoryDB;->updateNonLinearAdjust(Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;I)V

    sget-object v2, Lcom/konka/factory/desk/FactoryDeskImpl$3;->$SwitchMap$com$konka$factory$desk$IFactoryDesk$MS_NLA_SET_INDEX:[I

    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mfactoryNLASet:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

    iget-object v3, v3, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v3}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :goto_0
    const/4 v2, 0x1

    return v2

    :pswitch_0
    :try_start_0
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->am:Lcom/mstar/android/tvapi/common/AudioManager;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;->E_VOL_SOURCE_SPEAKER_OUT:Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;

    int-to-byte v4, p1

    invoke-virtual {v2, v3, v4}, Lcom/mstar/android/tvapi/common/AudioManager;->setAudioVolume(Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;B)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_1
    :try_start_1
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeBrightness(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_2
    :try_start_2
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeContrast(S)V
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_3
    :try_start_3
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeColor(S)V
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_0

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_4
    :try_start_4
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeSharpness(S)V
    :try_end_4
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_0

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_5
    :try_start_5
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeTint(S)V
    :try_end_5
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_5 .. :try_end_5} :catch_5

    goto :goto_0

    :catch_5
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_6
    :try_start_6
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setBacklight(I)V
    :try_end_6
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_6 .. :try_end_6} :catch_6

    goto :goto_0

    :catch_6
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public setOsdV25Nonlinear(S)Z
    .locals 5
    .param p1    # S

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mfactoryNLASet:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

    iget-object v2, v2, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v2}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v1

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mfactoryNLASet:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

    iget-object v2, v2, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;

    aget-object v2, v2, v1

    iput p1, v2, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;->u8OSD_V25:I

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mfactoryNLASet:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

    iget-object v3, v3, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;

    aget-object v3, v3, v1

    invoke-virtual {v2, v3, v1}, Lcom/konka/factory/desk/FactoryDB;->updateNonLinearAdjust(Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;I)V

    sget-object v2, Lcom/konka/factory/desk/FactoryDeskImpl$3;->$SwitchMap$com$konka$factory$desk$IFactoryDesk$MS_NLA_SET_INDEX:[I

    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mfactoryNLASet:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

    iget-object v3, v3, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v3}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :goto_0
    const/4 v2, 0x1

    return v2

    :pswitch_0
    :try_start_0
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->am:Lcom/mstar/android/tvapi/common/AudioManager;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;->E_VOL_SOURCE_SPEAKER_OUT:Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;

    int-to-byte v4, p1

    invoke-virtual {v2, v3, v4}, Lcom/mstar/android/tvapi/common/AudioManager;->setAudioVolume(Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;B)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_1
    :try_start_1
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    const/16 v3, 0x19

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeBrightness(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_2
    :try_start_2
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    const/16 v3, 0x19

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeContrast(S)V
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_3
    :try_start_3
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    const/16 v3, 0x19

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeColor(S)V
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_0

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_4
    :try_start_4
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    const/16 v3, 0x19

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeSharpness(S)V
    :try_end_4
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_0

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_5
    :try_start_5
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    const/16 v3, 0x19

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeTint(S)V
    :try_end_5
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_5 .. :try_end_5} :catch_5

    goto :goto_0

    :catch_5
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_6
    :try_start_6
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    const/16 v3, 0x19

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setBacklight(I)V
    :try_end_6
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_6 .. :try_end_6} :catch_6

    goto :goto_0

    :catch_6
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public setOsdV50Nonlinear(S)Z
    .locals 5
    .param p1    # S

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mfactoryNLASet:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

    iget-object v2, v2, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v2}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v1

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mfactoryNLASet:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

    iget-object v2, v2, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;

    aget-object v2, v2, v1

    iput p1, v2, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;->u8OSD_V50:I

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mfactoryNLASet:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

    iget-object v3, v3, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;

    aget-object v3, v3, v1

    invoke-virtual {v2, v3, v1}, Lcom/konka/factory/desk/FactoryDB;->updateNonLinearAdjust(Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;I)V

    sget-object v2, Lcom/konka/factory/desk/FactoryDeskImpl$3;->$SwitchMap$com$konka$factory$desk$IFactoryDesk$MS_NLA_SET_INDEX:[I

    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mfactoryNLASet:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

    iget-object v3, v3, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v3}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :goto_0
    const/4 v2, 0x1

    return v2

    :pswitch_0
    :try_start_0
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->am:Lcom/mstar/android/tvapi/common/AudioManager;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;->E_VOL_SOURCE_SPEAKER_OUT:Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;

    int-to-byte v4, p1

    invoke-virtual {v2, v3, v4}, Lcom/mstar/android/tvapi/common/AudioManager;->setAudioVolume(Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;B)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_1
    :try_start_1
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    const/16 v3, 0x32

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeBrightness(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_2
    :try_start_2
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    const/16 v3, 0x32

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeContrast(S)V
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_3
    :try_start_3
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    const/16 v3, 0x32

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeColor(S)V
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_0

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_4
    :try_start_4
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    const/16 v3, 0x32

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeSharpness(S)V
    :try_end_4
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_0

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_5
    :try_start_5
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    const/16 v3, 0x32

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeTint(S)V
    :try_end_5
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_5 .. :try_end_5} :catch_5

    goto :goto_0

    :catch_5
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_6
    :try_start_6
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    const/16 v3, 0x32

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setBacklight(I)V
    :try_end_6
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_6 .. :try_end_6} :catch_6

    goto :goto_0

    :catch_6
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public setOsdV75Nonlinear(S)Z
    .locals 5
    .param p1    # S

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mfactoryNLASet:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

    iget-object v2, v2, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v2}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v1

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mfactoryNLASet:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

    iget-object v2, v2, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;

    aget-object v2, v2, v1

    iput p1, v2, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;->u8OSD_V75:I

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mfactoryNLASet:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

    iget-object v3, v3, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;

    aget-object v3, v3, v1

    invoke-virtual {v2, v3, v1}, Lcom/konka/factory/desk/FactoryDB;->updateNonLinearAdjust(Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_POINT;I)V

    sget-object v2, Lcom/konka/factory/desk/FactoryDeskImpl$3;->$SwitchMap$com$konka$factory$desk$IFactoryDesk$MS_NLA_SET_INDEX:[I

    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mfactoryNLASet:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;

    iget-object v3, v3, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v3}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :goto_0
    const/4 v2, 0x1

    return v2

    :pswitch_0
    :try_start_0
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->am:Lcom/mstar/android/tvapi/common/AudioManager;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;->E_VOL_SOURCE_SPEAKER_OUT:Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;

    int-to-byte v4, p1

    invoke-virtual {v2, v3, v4}, Lcom/mstar/android/tvapi/common/AudioManager;->setAudioVolume(Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;B)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_1
    :try_start_1
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    const/16 v3, 0x4b

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeBrightness(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_2
    :try_start_2
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    const/16 v3, 0x4b

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeContrast(S)V
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_3
    :try_start_3
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    const/16 v3, 0x4b

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeColor(S)V
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_0

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_4
    :try_start_4
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    const/16 v3, 0x4b

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeSharpness(S)V
    :try_end_4
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_0

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_5
    :try_start_5
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    const/16 v3, 0x4b

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeTint(S)V
    :try_end_5
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_5 .. :try_end_5} :catch_5

    goto :goto_0

    :catch_5
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_6
    :try_start_6
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    const/16 v3, 0x4b

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setBacklight(I)V
    :try_end_6
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_6 .. :try_end_6} :catch_6

    goto :goto_0

    :catch_6
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public setOverScanHposition(S)Z
    .locals 14
    .param p1    # S

    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v8}, Lcom/konka/factory/desk/FactoryDB;->queryCurInputSrc()I

    move-result v5

    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v8, v5}, Lcom/konka/factory/desk/FactoryDB;->queryArcMode(I)I

    move-result v0

    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v8}, Lcom/konka/factory/desk/FactoryDeskImpl;->GetResolutionMapping(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)I

    move-result v1

    const/4 v7, 0x0

    :try_start_0
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mtvplayer:Lcom/mstar/android/tvapi/common/TvPlayer;

    invoke-interface {v8}, Lcom/mstar/android/tvapi/common/TvPlayer;->getVideoStandard()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    :goto_0
    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_AVD_VideoStandardType;->values()[Lcom/konka/factory/desk/IFactoryDesk$MAPI_AVD_VideoStandardType;

    move-result-object v8

    aget-object v6, v8, v7

    sget-object v8, Lcom/konka/factory/desk/FactoryDeskImpl$3;->$SwitchMap$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v9}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    :pswitch_0
    const/4 v8, 0x0

    :goto_1
    return v8

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_1
    sget-object v8, Lcom/konka/factory/desk/FactoryDeskImpl$3;->$SwitchMap$com$konka$factory$desk$IFactoryDesk$MAPI_AVD_VideoStandardType:[I

    invoke-virtual {v6}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_AVD_VideoStandardType;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_1

    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    :goto_2
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryVDOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v9

    aget-object v8, v8, v9

    aget-object v8, v8, v0

    iput-short p1, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v9

    iget-object v10, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryVDOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v9, v0, v10}, Lcom/konka/factory/desk/FactoryDB;->updateOverscanAdjust(II[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    :try_start_1
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryVDOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v10

    aget-object v9, v9, v10

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryVDOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v11

    aget-object v10, v10, v11

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryVDOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v12

    aget-object v11, v11, v12

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryVDOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v13

    aget-object v12, v12, v13

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_3
    const/4 v8, 0x1

    goto :goto_1

    :pswitch_2
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_3
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_4
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_SECAM:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_5
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_6
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL_M:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_7
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL_NC:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_8
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :catch_1
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_3

    :pswitch_9
    sget-object v8, Lcom/konka/factory/desk/FactoryDeskImpl$3;->$SwitchMap$com$konka$factory$desk$IFactoryDesk$MAPI_AVD_VideoStandardType:[I

    invoke-virtual {v6}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_AVD_VideoStandardType;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_2

    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    :goto_4
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryATVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v9

    aget-object v8, v8, v9

    aget-object v8, v8, v0

    iput-short p1, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v9

    iget-object v10, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryATVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v9, v0, v10}, Lcom/konka/factory/desk/FactoryDB;->updateATVOverscanAdjust(II[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    :try_start_2
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryATVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v10

    aget-object v9, v9, v10

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryATVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v11

    aget-object v10, v10, v11

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryATVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v12

    aget-object v11, v11, v12

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryATVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v13

    aget-object v12, v12, v13

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    :goto_5
    const/4 v8, 0x1

    goto/16 :goto_1

    :pswitch_a
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_b
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_c
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_SECAM:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_d
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_e
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL_M:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_f
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL_NC:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_10
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :catch_2
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_5

    :pswitch_11
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryYPbPrOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v8, v8, v1

    aget-object v8, v8, v0

    iput-short p1, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryYPbPrOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v1, v0, v9}, Lcom/konka/factory/desk/FactoryDB;->updateYPbPrOverscanAdjust(II[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    :try_start_3
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryYPbPrOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v9, v9, v1

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryYPbPrOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v1

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryYPbPrOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v11, v11, v1

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryYPbPrOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v12, v12, v1

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_3

    :goto_6
    const/4 v8, 0x1

    goto/16 :goto_1

    :catch_3
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_6

    :pswitch_12
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryHDMIOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v8, v8, v1

    aget-object v8, v8, v0

    iput-short p1, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryHDMIOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v1, v0, v9}, Lcom/konka/factory/desk/FactoryDB;->updateHDMIOverscanAdjust(II[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    :try_start_4
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryHDMIOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v9, v9, v1

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryHDMIOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v1

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryHDMIOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v11, v11, v1

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryHDMIOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v12, v12, v1

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_4
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_4 .. :try_end_4} :catch_4

    :goto_7
    const/4 v8, 0x1

    goto/16 :goto_1

    :catch_4
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_7

    :pswitch_13
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDTVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v8, v8, v1

    aget-object v8, v8, v0

    iput-short p1, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDTVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v1, v0, v9}, Lcom/konka/factory/desk/FactoryDB;->updateDTVOverscanAdjust(II[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    :try_start_5
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDTVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v9, v9, v1

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDTVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v1

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDTVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v11, v11, v1

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDTVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v12, v12, v1

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_5
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_5 .. :try_end_5} :catch_5

    :goto_8
    const/4 v8, 0x1

    goto/16 :goto_1

    :catch_5
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_8

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_9
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_0
        :pswitch_13
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method public setOverScanHsize(S)Z
    .locals 14
    .param p1    # S

    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v8}, Lcom/konka/factory/desk/FactoryDB;->queryCurInputSrc()I

    move-result v5

    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v8, v5}, Lcom/konka/factory/desk/FactoryDB;->queryArcMode(I)I

    move-result v0

    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v8}, Lcom/konka/factory/desk/FactoryDeskImpl;->GetResolutionMapping(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)I

    move-result v1

    const/4 v7, 0x0

    :try_start_0
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mtvplayer:Lcom/mstar/android/tvapi/common/TvPlayer;

    invoke-interface {v8}, Lcom/mstar/android/tvapi/common/TvPlayer;->getVideoStandard()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    :goto_0
    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_AVD_VideoStandardType;->values()[Lcom/konka/factory/desk/IFactoryDesk$MAPI_AVD_VideoStandardType;

    move-result-object v8

    aget-object v6, v8, v7

    sget-object v8, Lcom/konka/factory/desk/FactoryDeskImpl$3;->$SwitchMap$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v9}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    :pswitch_0
    const/4 v8, 0x0

    :goto_1
    return v8

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_1
    sget-object v8, Lcom/konka/factory/desk/FactoryDeskImpl$3;->$SwitchMap$com$konka$factory$desk$IFactoryDesk$MAPI_AVD_VideoStandardType:[I

    invoke-virtual {v6}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_AVD_VideoStandardType;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_1

    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    :goto_2
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryVDOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v9

    aget-object v8, v8, v9

    aget-object v8, v8, v0

    iput-short p1, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v9

    iget-object v10, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryVDOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v9, v0, v10}, Lcom/konka/factory/desk/FactoryDB;->updateOverscanAdjust(II[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    :try_start_1
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryVDOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v10

    aget-object v9, v9, v10

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryVDOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v11

    aget-object v10, v10, v11

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryVDOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v12

    aget-object v11, v11, v12

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryVDOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v13

    aget-object v12, v12, v13

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_3
    const/4 v8, 0x1

    goto :goto_1

    :pswitch_2
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_3
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_4
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_SECAM:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_5
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_6
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL_M:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_7
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL_NC:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_8
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :catch_1
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_3

    :pswitch_9
    sget-object v8, Lcom/konka/factory/desk/FactoryDeskImpl$3;->$SwitchMap$com$konka$factory$desk$IFactoryDesk$MAPI_AVD_VideoStandardType:[I

    invoke-virtual {v6}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_AVD_VideoStandardType;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_2

    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    :goto_4
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryATVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v9

    aget-object v8, v8, v9

    aget-object v8, v8, v0

    iput-short p1, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v9

    iget-object v10, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryATVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v9, v0, v10}, Lcom/konka/factory/desk/FactoryDB;->updateATVOverscanAdjust(II[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    :try_start_2
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryATVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v10

    aget-object v9, v9, v10

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryATVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v11

    aget-object v10, v10, v11

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryATVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v12

    aget-object v11, v11, v12

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryATVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v13

    aget-object v12, v12, v13

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    :goto_5
    const/4 v8, 0x1

    goto/16 :goto_1

    :pswitch_a
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_b
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_c
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_SECAM:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_d
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_e
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL_M:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_f
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL_NC:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_10
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :catch_2
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_5

    :pswitch_11
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryYPbPrOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v8, v8, v1

    aget-object v8, v8, v0

    iput-short p1, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryYPbPrOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v1, v0, v9}, Lcom/konka/factory/desk/FactoryDB;->updateYPbPrOverscanAdjust(II[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    :try_start_3
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryYPbPrOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v9, v9, v1

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryYPbPrOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v1

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryYPbPrOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v11, v11, v1

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryYPbPrOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v12, v12, v1

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_3

    :goto_6
    const/4 v8, 0x1

    goto/16 :goto_1

    :catch_3
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_6

    :pswitch_12
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryHDMIOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v8, v8, v1

    aget-object v8, v8, v0

    iput-short p1, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryHDMIOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v1, v0, v9}, Lcom/konka/factory/desk/FactoryDB;->updateHDMIOverscanAdjust(II[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    :try_start_4
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryHDMIOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v9, v9, v1

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryHDMIOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v1

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryHDMIOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v11, v11, v1

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryHDMIOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v12, v12, v1

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_4
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_4 .. :try_end_4} :catch_4

    :goto_7
    const/4 v8, 0x1

    goto/16 :goto_1

    :catch_4
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_7

    :pswitch_13
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDTVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v8, v8, v1

    aget-object v8, v8, v0

    iput-short p1, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDTVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v1, v0, v9}, Lcom/konka/factory/desk/FactoryDB;->updateDTVOverscanAdjust(II[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    :try_start_5
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDTVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v9, v9, v1

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDTVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v1

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDTVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v11, v11, v1

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDTVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v12, v12, v1

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_5
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_5 .. :try_end_5} :catch_5

    :goto_8
    const/4 v8, 0x1

    goto/16 :goto_1

    :catch_5
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_8

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_9
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_0
        :pswitch_13
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method public setOverScanVposition(S)Z
    .locals 14
    .param p1    # S

    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v8}, Lcom/konka/factory/desk/FactoryDB;->queryCurInputSrc()I

    move-result v5

    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v8, v5}, Lcom/konka/factory/desk/FactoryDB;->queryArcMode(I)I

    move-result v0

    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v8}, Lcom/konka/factory/desk/FactoryDeskImpl;->GetResolutionMapping(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)I

    move-result v1

    const/4 v7, 0x0

    :try_start_0
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mtvplayer:Lcom/mstar/android/tvapi/common/TvPlayer;

    invoke-interface {v8}, Lcom/mstar/android/tvapi/common/TvPlayer;->getVideoStandard()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    :goto_0
    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_AVD_VideoStandardType;->values()[Lcom/konka/factory/desk/IFactoryDesk$MAPI_AVD_VideoStandardType;

    move-result-object v8

    aget-object v6, v8, v7

    sget-object v8, Lcom/konka/factory/desk/FactoryDeskImpl$3;->$SwitchMap$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v9}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    :pswitch_0
    const/4 v8, 0x0

    :goto_1
    return v8

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_1
    sget-object v8, Lcom/konka/factory/desk/FactoryDeskImpl$3;->$SwitchMap$com$konka$factory$desk$IFactoryDesk$MAPI_AVD_VideoStandardType:[I

    invoke-virtual {v6}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_AVD_VideoStandardType;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_1

    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    :goto_2
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryVDOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v9

    aget-object v8, v8, v9

    aget-object v8, v8, v0

    iput-short p1, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v9

    iget-object v10, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryVDOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v9, v0, v10}, Lcom/konka/factory/desk/FactoryDB;->updateOverscanAdjust(II[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    :try_start_1
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryVDOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v10

    aget-object v9, v9, v10

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryVDOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v11

    aget-object v10, v10, v11

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryVDOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v12

    aget-object v11, v11, v12

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryVDOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v13

    aget-object v12, v12, v13

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_3
    const/4 v8, 0x1

    goto :goto_1

    :pswitch_2
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_3
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_4
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_SECAM:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_5
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_6
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL_M:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_7
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL_NC:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_8
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :catch_1
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_3

    :pswitch_9
    sget-object v8, Lcom/konka/factory/desk/FactoryDeskImpl$3;->$SwitchMap$com$konka$factory$desk$IFactoryDesk$MAPI_AVD_VideoStandardType:[I

    invoke-virtual {v6}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_AVD_VideoStandardType;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_2

    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    :goto_4
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryATVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v9

    aget-object v8, v8, v9

    aget-object v8, v8, v0

    iput-short p1, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v9

    iget-object v10, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryATVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v9, v0, v10}, Lcom/konka/factory/desk/FactoryDB;->updateATVOverscanAdjust(II[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    :try_start_2
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryATVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v10

    aget-object v9, v9, v10

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryATVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v11

    aget-object v10, v10, v11

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryATVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v12

    aget-object v11, v11, v12

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryATVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v13

    aget-object v12, v12, v13

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    :goto_5
    const/4 v8, 0x1

    goto/16 :goto_1

    :pswitch_a
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_b
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_c
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_SECAM:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_d
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_e
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL_M:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_f
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL_NC:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_10
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :catch_2
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_5

    :pswitch_11
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryYPbPrOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v8, v8, v1

    aget-object v8, v8, v0

    iput-short p1, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryYPbPrOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v1, v0, v9}, Lcom/konka/factory/desk/FactoryDB;->updateYPbPrOverscanAdjust(II[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    :try_start_3
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryYPbPrOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v9, v9, v1

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryYPbPrOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v1

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryYPbPrOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v11, v11, v1

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryYPbPrOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v12, v12, v1

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_3

    :goto_6
    const/4 v8, 0x1

    goto/16 :goto_1

    :catch_3
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_6

    :pswitch_12
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryHDMIOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v8, v8, v1

    aget-object v8, v8, v0

    iput-short p1, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryHDMIOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v1, v0, v9}, Lcom/konka/factory/desk/FactoryDB;->updateHDMIOverscanAdjust(II[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    :try_start_4
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryHDMIOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v9, v9, v1

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryHDMIOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v1

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryHDMIOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v11, v11, v1

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryHDMIOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v12, v12, v1

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_4
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_4 .. :try_end_4} :catch_4

    :goto_7
    const/4 v8, 0x1

    goto/16 :goto_1

    :catch_4
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_7

    :pswitch_13
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDTVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v8, v8, v1

    aget-object v8, v8, v0

    iput-short p1, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDTVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v1, v0, v9}, Lcom/konka/factory/desk/FactoryDB;->updateDTVOverscanAdjust(II[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    :try_start_5
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDTVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v9, v9, v1

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDTVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v1

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDTVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v11, v11, v1

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDTVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v12, v12, v1

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_5
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_5 .. :try_end_5} :catch_5

    :goto_8
    const/4 v8, 0x1

    goto/16 :goto_1

    :catch_5
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_8

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_9
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_0
        :pswitch_13
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method public setOverScanVsize(S)Z
    .locals 14
    .param p1    # S

    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v8}, Lcom/konka/factory/desk/FactoryDB;->queryCurInputSrc()I

    move-result v5

    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v8, v5}, Lcom/konka/factory/desk/FactoryDB;->queryArcMode(I)I

    move-result v0

    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v8}, Lcom/konka/factory/desk/FactoryDeskImpl;->GetResolutionMapping(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)I

    move-result v1

    const/4 v7, 0x0

    :try_start_0
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->mtvplayer:Lcom/mstar/android/tvapi/common/TvPlayer;

    invoke-interface {v8}, Lcom/mstar/android/tvapi/common/TvPlayer;->getVideoStandard()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    :goto_0
    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_AVD_VideoStandardType;->values()[Lcom/konka/factory/desk/IFactoryDesk$MAPI_AVD_VideoStandardType;

    move-result-object v8

    aget-object v6, v8, v7

    sget-object v8, Lcom/konka/factory/desk/FactoryDeskImpl$3;->$SwitchMap$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v9}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    :pswitch_0
    const/4 v8, 0x0

    :goto_1
    return v8

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_1
    sget-object v8, Lcom/konka/factory/desk/FactoryDeskImpl$3;->$SwitchMap$com$konka$factory$desk$IFactoryDesk$MAPI_AVD_VideoStandardType:[I

    invoke-virtual {v6}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_AVD_VideoStandardType;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_1

    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    :goto_2
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryVDOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v9

    aget-object v8, v8, v9

    aget-object v8, v8, v0

    iput-short p1, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v9

    iget-object v10, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryVDOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v9, v0, v10}, Lcom/konka/factory/desk/FactoryDB;->updateOverscanAdjust(II[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    :try_start_1
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryVDOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v10

    aget-object v9, v9, v10

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryVDOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v11

    aget-object v10, v10, v11

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryVDOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v12

    aget-object v11, v11, v12

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryVDOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v13

    aget-object v12, v12, v13

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_3
    const/4 v8, 0x1

    goto :goto_1

    :pswitch_2
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_3
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_4
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_SECAM:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_5
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_6
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL_M:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_7
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL_NC:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_8
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :catch_1
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_3

    :pswitch_9
    sget-object v8, Lcom/konka/factory/desk/FactoryDeskImpl$3;->$SwitchMap$com$konka$factory$desk$IFactoryDesk$MAPI_AVD_VideoStandardType:[I

    invoke-virtual {v6}, Lcom/konka/factory/desk/IFactoryDesk$MAPI_AVD_VideoStandardType;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_2

    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    :goto_4
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryATVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v9

    aget-object v8, v8, v9

    aget-object v8, v8, v0

    iput-short p1, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v9

    iget-object v10, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryATVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v9, v0, v10}, Lcom/konka/factory/desk/FactoryDB;->updateATVOverscanAdjust(II[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    :try_start_2
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryATVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v10

    aget-object v9, v9, v10

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryATVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v11

    aget-object v10, v10, v11

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryATVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v12

    aget-object v11, v11, v12

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryATVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v13

    aget-object v12, v12, v13

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    :goto_5
    const/4 v8, 0x1

    goto/16 :goto_1

    :pswitch_a
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_b
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_c
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_SECAM:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_d
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_e
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL_M:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_f
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_PAL_NC:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_10
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/konka/factory/desk/IFactoryDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :catch_2
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_5

    :pswitch_11
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryYPbPrOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v8, v8, v1

    aget-object v8, v8, v0

    iput-short p1, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryYPbPrOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v1, v0, v9}, Lcom/konka/factory/desk/FactoryDB;->updateYPbPrOverscanAdjust(II[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    :try_start_3
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryYPbPrOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v9, v9, v1

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryYPbPrOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v1

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryYPbPrOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v11, v11, v1

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryYPbPrOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v12, v12, v1

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_3

    :goto_6
    const/4 v8, 0x1

    goto/16 :goto_1

    :catch_3
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_6

    :pswitch_12
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryHDMIOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v8, v8, v1

    aget-object v8, v8, v0

    iput-short p1, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryHDMIOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v1, v0, v9}, Lcom/konka/factory/desk/FactoryDB;->updateHDMIOverscanAdjust(II[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    :try_start_4
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryHDMIOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v9, v9, v1

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryHDMIOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v1

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryHDMIOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v11, v11, v1

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryHDMIOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v12, v12, v1

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_4
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_4 .. :try_end_4} :catch_4

    :goto_7
    const/4 v8, 0x1

    goto/16 :goto_1

    :catch_4
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_7

    :pswitch_13
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDTVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v8, v8, v1

    aget-object v8, v8, v0

    iput-short p1, v8, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDTVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v1, v0, v9}, Lcom/konka/factory/desk/FactoryDB;->updateDTVOverscanAdjust(II[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    :try_start_5
    iget-object v8, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->pm:Lcom/mstar/android/tvapi/common/PictureManager;

    iget-object v9, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDTVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v9, v9, v1

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDTVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v1

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDTVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v11, v11, v1

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDTVOverscanSet:[[Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v12, v12, v1

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/konka/factory/desk/IFactoryDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_5
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_5 .. :try_end_5} :catch_5

    :goto_8
    const/4 v8, 0x1

    goto/16 :goto_1

    :catch_5
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_8

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_9
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_12
        :pswitch_0
        :pswitch_13
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method public setPEQ()Z
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x5

    new-instance v2, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;

    invoke-direct {v2}, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;-><init>()V

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v6, :cond_0

    iget-object v3, v2, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterPeqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;

    new-instance v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;

    invoke-direct {v4}, Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;-><init>()V

    aput-object v4, v3, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v6, :cond_1

    iget-object v3, v2, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterPeqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;

    aget-object v3, v3, v1

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryPEQSet:Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v4, v4, Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;

    aget-object v4, v4, v1

    iget v4, v4, Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;->Foh:I

    mul-int/lit8 v4, v4, 0x64

    iget-object v5, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryPEQSet:Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v5, v5, Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;

    aget-object v5, v5, v1

    iget v5, v5, Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;->Fol:I

    add-int/2addr v4, v5

    iput v4, v3, Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;->peqGc:I

    iget-object v3, v2, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterPeqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;

    aget-object v3, v3, v1

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryPEQSet:Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v4, v4, Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;

    aget-object v4, v4, v1

    iget v4, v4, Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;->Gain:I

    iput v4, v3, Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;->peqGain:I

    iget-object v3, v2, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterPeqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;

    aget-object v3, v3, v1

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryPEQSet:Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v4, v4, Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;

    aget-object v4, v4, v1

    iget v4, v4, Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;->QValue:I

    iput v4, v3, Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;->peqQvalue:I

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    iput-short v6, v2, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->peqBandNumber:S

    :try_start_0
    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->am:Lcom/mstar/android/tvapi/common/AudioManager;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_PEQ:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/mstar/android/tvapi/common/AudioManager;->enableBasicSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;Z)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->am:Lcom/mstar/android/tvapi/common/AudioManager;

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_PEQ:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    invoke-virtual {v3, v4, v2}, Lcom/mstar/android/tvapi/common/AudioManager;->setBasicSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    return v7

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2
.end method

.method public setPEQ85HzGain(I)Z
    .locals 7
    .param p1    # I

    const/4 v6, 0x1

    const/4 v5, 0x0

    new-instance v1, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;

    invoke-direct {v1}, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;-><init>()V

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterPeqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;

    new-instance v3, Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;

    invoke-direct {v3}, Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;-><init>()V

    aput-object v3, v2, v5

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterPeqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;

    aget-object v2, v2, v5

    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryPEQSet:Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v3, v3, Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;

    aget-object v3, v3, v5

    iget v3, v3, Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;->Foh:I

    mul-int/lit8 v3, v3, 0x64

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryPEQSet:Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v4, v4, Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;

    aget-object v4, v4, v5

    iget v4, v4, Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;->Fol:I

    add-int/2addr v3, v4

    iput v3, v2, Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;->peqGc:I

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterPeqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;

    aget-object v2, v2, v5

    iput p1, v2, Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;->peqGain:I

    iget-object v2, v1, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterPeqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;

    aget-object v2, v2, v5

    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryPEQSet:Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v3, v3, Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;

    aget-object v3, v3, v5

    iget v3, v3, Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;->QValue:I

    iput v3, v2, Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;->peqQvalue:I

    iput-short v6, v1, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->peqBandNumber:S

    :try_start_0
    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->am:Lcom/mstar/android/tvapi/common/AudioManager;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_PEQ:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/mstar/android/tvapi/common/AudioManager;->enableBasicSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;Z)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->am:Lcom/mstar/android/tvapi/common/AudioManager;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_PEQ:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    invoke-virtual {v2, v3, v1}, Lcom/mstar/android/tvapi/common/AudioManager;->setBasicSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v6

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setPanelSwing(S)Z
    .locals 2
    .param p1    # S

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryExternSetting:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

    iput-short p1, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->panelSwingVal:S

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryExternSetting:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

    invoke-virtual {v0, v1}, Lcom/konka/factory/desk/FactoryDB;->updateFactoryExtern(Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;)V

    const/4 v0, 0x1

    return v0
.end method

.method public setPeqFoCoarse(II)Z
    .locals 2
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryPEQSet:Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v0, v0, Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;

    aget-object v0, v0, p1

    iput p2, v0, Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;->Foh:I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryPEQSet:Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v1, v1, Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1, p1}, Lcom/konka/factory/desk/FactoryDB;->updatePEQAdjust(Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;I)V

    const/4 v0, 0x1

    return v0
.end method

.method public setPeqFoFine(II)Z
    .locals 2
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryPEQSet:Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v0, v0, Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;

    aget-object v0, v0, p1

    iput p2, v0, Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;->Fol:I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryPEQSet:Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v1, v1, Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1, p1}, Lcom/konka/factory/desk/FactoryDB;->updatePEQAdjust(Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;I)V

    const/4 v0, 0x1

    return v0
.end method

.method public setPeqGain(II)Z
    .locals 2
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryPEQSet:Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v0, v0, Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;

    aget-object v0, v0, p1

    iput p2, v0, Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;->Gain:I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryPEQSet:Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v1, v1, Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1, p1}, Lcom/konka/factory/desk/FactoryDB;->updatePEQAdjust(Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;I)V

    const/4 v0, 0x1

    return v0
.end method

.method public setPeqQ(II)Z
    .locals 2
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryPEQSet:Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v0, v0, Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;

    aget-object v0, v0, p1

    iput p2, v0, Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;->QValue:I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryPEQSet:Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v1, v1, Lcom/konka/factory/desk/IFactoryDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1, p1}, Lcom/konka/factory/desk/FactoryDB;->updatePEQAdjust(Lcom/konka/factory/desk/IFactoryDesk$AUDIO_PEQ_PARAM;I)V

    const/4 v0, 0x1

    return v0
.end method

.method public setPictureModeIdx(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_PICTURE;)V
    .locals 3
    .param p1    # Lcom/konka/factory/desk/IFactoryDesk$EN_MS_PICTURE;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v1}, Lcom/konka/factory/desk/FactoryDB;->queryCurInputSrc()I

    move-result v0

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {p1}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_PICTURE;->ordinal()I

    move-result v2

    invoke-virtual {v1, v2, v0}, Lcom/konka/factory/desk/FactoryDB;->updatePictureMode(II)V

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_BRIGHTNESS:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_BRIGHTNESS:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    invoke-virtual {p0, v2}, Lcom/konka/factory/desk/FactoryDeskImpl;->getVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;)I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/konka/factory/desk/FactoryDeskImpl;->execVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;I)Z

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_CONTRAST:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_CONTRAST:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    invoke-virtual {p0, v2}, Lcom/konka/factory/desk/FactoryDeskImpl;->getVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;)I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/konka/factory/desk/FactoryDeskImpl;->execVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;I)Z

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_SATURATION:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_SATURATION:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    invoke-virtual {p0, v2}, Lcom/konka/factory/desk/FactoryDeskImpl;->getVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;)I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/konka/factory/desk/FactoryDeskImpl;->execVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;I)Z

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_SHARPNESS:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_SHARPNESS:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    invoke-virtual {p0, v2}, Lcom/konka/factory/desk/FactoryDeskImpl;->getVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;)I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/konka/factory/desk/FactoryDeskImpl;->execVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;I)Z

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_HUE:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_HUE:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    invoke-virtual {p0, v2}, Lcom/konka/factory/desk/FactoryDeskImpl;->getVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;)I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/konka/factory/desk/FactoryDeskImpl;->execVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;I)Z

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_BACKLIGHT:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_BACKLIGHT:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    invoke-virtual {p0, v2}, Lcom/konka/factory/desk/FactoryDeskImpl;->getVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;)I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/konka/factory/desk/FactoryDeskImpl;->execVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;I)Z

    return-void
.end method

.method public setPowerOffLogoDspTime(I)Z
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryCusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    iput p1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->PowerOffLogoDspTime:I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryCusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->setFactoryCusDefSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;)Z

    const/4 v0, 0x1

    return v0
.end method

.method public setPowerOffLogoMode(I)Z
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryCusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    iput p1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->PowerOffLogoEnabled:I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryCusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->setFactoryCusDefSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;)Z

    const/4 v0, 0x1

    return v0
.end method

.method public setPowerOnMode(I)Z
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryExternSetting:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

    iput p1, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->stPowerMode:I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryExternSetting:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

    invoke-virtual {v0, v1}, Lcom/konka/factory/desk/FactoryDB;->updateFactoryExtern(Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;)V

    const/4 v0, 0x1

    return v0
.end method

.method public setSoundEqItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;II)I
    .locals 1
    .param p1    # Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v0, p1, p2, p3}, Lcom/konka/factory/desk/FactoryDB;->updateSoundEqSetting(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_SOUND;II)I

    move-result v0

    return v0
.end method

.method public setTeletextMode(I)Z
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryCusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    iput p1, v0, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->IsTeletextEnabled:I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->databaseMgr:Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryCusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->setFactoryCusDefSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;)Z

    const/4 v0, 0x1

    return v0
.end method

.method public setTestPattern(I)Z
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryExternSetting:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

    iput p1, v1, Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;->testPatternMode:I

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v2, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryExternSetting:Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;

    invoke-virtual {v1, v2}, Lcom/konka/factory/desk/FactoryDB;->updateFactoryExtern(Lcom/konka/factory/desk/IFactoryDesk$MS_FACTORY_EXTERN_SETTING;)V

    :try_start_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    invoke-static {}, Lcom/mstar/android/tvapi/factory/vo/EnumScreenMute;->values()[Lcom/mstar/android/tvapi/factory/vo/EnumScreenMute;

    move-result-object v2

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setVideoTestPattern(Lcom/mstar/android/tvapi/factory/vo/EnumScreenMute;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setUartOnOff(Z)Z
    .locals 5
    .param p1    # Z

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    sget-object v4, Lcom/konka/factory/desk/FactoryDB$USER_SETTING_FIELD;->bUartBus:Lcom/konka/factory/desk/FactoryDB$USER_SETTING_FIELD;

    if-eqz p1, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {v3, v4, v1}, Lcom/konka/factory/desk/FactoryDB;->updateUserSysSetting(Lcom/konka/factory/desk/FactoryDB$USER_SETTING_FIELD;I)V

    if-eqz p1, :cond_1

    :try_start_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/factory/FactoryManager;->enableUart()Z

    :goto_1
    return v2

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/factory/FactoryManager;->disableUart()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public setVifAgcRef(S)Z
    .locals 2
    .param p1    # S

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    iput-short p1, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifACIAGCREF:S

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    invoke-virtual {v0, v1}, Lcom/konka/factory/desk/FactoryDB;->updateNonStandardAdjust(Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;)V

    invoke-direct {p0}, Lcom/konka/factory/desk/FactoryDeskImpl;->InitVif()V

    const/4 v0, 0x1

    return v0
.end method

.method public setVifAsiaSignalOption(Z)Z
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    iput-boolean p1, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifAsiaSignalOption:Z

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    invoke-virtual {v0, v1}, Lcom/konka/factory/desk/FactoryDB;->updateNonStandardAdjust(Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;)V

    invoke-direct {p0}, Lcom/konka/factory/desk/FactoryDeskImpl;->InitVif()V

    const/4 v0, 0x1

    return v0
.end method

.method public setVifClampGainOvNegative(I)Z
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    iput p1, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifClampgainGainOvNegative:I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    invoke-virtual {v0, v1}, Lcom/konka/factory/desk/FactoryDB;->updateNonStandardAdjust(Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;)V

    invoke-direct {p0}, Lcom/konka/factory/desk/FactoryDeskImpl;->InitVif()V

    const/4 v0, 0x1

    return v0
.end method

.method public setVifCrKi(S)Z
    .locals 2
    .param p1    # S

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    iput-short p1, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifCrKi:S

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    invoke-virtual {v0, v1}, Lcom/konka/factory/desk/FactoryDB;->updateNonStandardAdjust(Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;)V

    invoke-direct {p0}, Lcom/konka/factory/desk/FactoryDeskImpl;->InitVif()V

    const/4 v0, 0x1

    return v0
.end method

.method public setVifCrKp(S)Z
    .locals 2
    .param p1    # S

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    iput-short p1, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifCrKp:S

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    invoke-virtual {v0, v1}, Lcom/konka/factory/desk/FactoryDB;->updateNonStandardAdjust(Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;)V

    invoke-direct {p0}, Lcom/konka/factory/desk/FactoryDeskImpl;->InitVif()V

    const/4 v0, 0x1

    return v0
.end method

.method public setVifCrKpKiAdjust(Z)Z
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    iput-boolean p1, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifCrKpKiAdjust:Z

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    invoke-virtual {v0, v1}, Lcom/konka/factory/desk/FactoryDB;->updateNonStandardAdjust(Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;)V

    invoke-direct {p0}, Lcom/konka/factory/desk/FactoryDeskImpl;->InitVif()V

    const/4 v0, 0x1

    return v0
.end method

.method public setVifCrThr(I)Z
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    iput p1, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifCrThr:I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    invoke-virtual {v0, v1}, Lcom/konka/factory/desk/FactoryDB;->updateNonStandardAdjust(Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;)V

    invoke-direct {p0}, Lcom/konka/factory/desk/FactoryDeskImpl;->InitVif()V

    const/4 v0, 0x1

    return v0
.end method

.method public setVifOverModulation(Z)Z
    .locals 2
    .param p1    # Z

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    iput-boolean p1, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifOverModulation:Z

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    invoke-virtual {v0, v1}, Lcom/konka/factory/desk/FactoryDB;->updateNonStandardAdjust(Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;)V

    invoke-direct {p0}, Lcom/konka/factory/desk/FactoryDeskImpl;->InitVif()V

    const/4 v0, 0x1

    return v0
.end method

.method public setVifTop(S)Z
    .locals 2
    .param p1    # S

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    iput-short p1, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifTop:S

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    invoke-virtual {v0, v1}, Lcom/konka/factory/desk/FactoryDB;->updateNonStandardAdjust(Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;)V

    invoke-direct {p0}, Lcom/konka/factory/desk/FactoryDeskImpl;->InitVif()V

    const/4 v0, 0x1

    return v0
.end method

.method public setVifVersion(S)Z
    .locals 2
    .param p1    # S

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    iput-short p1, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifVersion:S

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    invoke-virtual {v0, v1}, Lcom/konka/factory/desk/FactoryDB;->updateNonStandardAdjust(Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;)V

    const/4 v0, 0x1

    return v0
.end method

.method public setVifVgaMaximum(I)Z
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    iput p1, v0, Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;->VifVgaMaximum:I

    iget-object v0, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->vifSet:Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;

    invoke-virtual {v0, v1}, Lcom/konka/factory/desk/FactoryDB;->updateNonStandardAdjust(Lcom/konka/factory/desk/IFactoryDesk$MS_Factory_NS_VIF_SET;)V

    invoke-direct {p0}, Lcom/konka/factory/desk/FactoryDeskImpl;->InitVif()V

    const/4 v0, 0x1

    return v0
.end method

.method public setWatchDogMode(S)Z
    .locals 4
    .param p1    # S

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    sget-object v2, Lcom/konka/factory/desk/FactoryDB$USER_SETTING_FIELD;->bEnableWDT:Lcom/konka/factory/desk/FactoryDB$USER_SETTING_FIELD;

    invoke-virtual {v1, v2, p1}, Lcom/konka/factory/desk/FactoryDB;->updateUserSysSetting(Lcom/konka/factory/desk/FactoryDB$USER_SETTING_FIELD;I)V

    if-ne p1, v3, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/factory/FactoryManager;->enableWdt()Z

    :goto_0
    return v3

    :cond_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/factory/FactoryManager;->disableWdt()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setWbBlueGain(S)Z
    .locals 5
    .param p1    # S

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4}, Lcom/konka/factory/desk/FactoryDB;->queryCurInputSrc()I

    move-result v0

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4, v0}, Lcom/konka/factory/desk/FactoryDB;->queryePicMode(I)I

    move-result v2

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4, v0, v2}, Lcom/konka/factory/desk/FactoryDB;->queryColorTmpIdx(II)I

    move-result v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    aget-object v4, v4, v0

    invoke-virtual {p0, v4}, Lcom/konka/factory/desk/FactoryDeskImpl;->InputSourceTransfer(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v3

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryColorTmpEx:Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;

    iget-object v4, v4, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v4, v4, v1

    aget-object v4, v4, v3

    iput p1, v4, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;->bluegain:I

    invoke-direct {p0, v1, v0, v3}, Lcom/konka/factory/desk/FactoryDeskImpl;->setWBGainOffset(III)V

    const/4 v4, 0x1

    return v4
.end method

.method public setWbBlueOffset(S)Z
    .locals 5
    .param p1    # S

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4}, Lcom/konka/factory/desk/FactoryDB;->queryCurInputSrc()I

    move-result v0

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4, v0}, Lcom/konka/factory/desk/FactoryDB;->queryePicMode(I)I

    move-result v2

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4, v0, v2}, Lcom/konka/factory/desk/FactoryDB;->queryColorTmpIdx(II)I

    move-result v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    aget-object v4, v4, v0

    invoke-virtual {p0, v4}, Lcom/konka/factory/desk/FactoryDeskImpl;->InputSourceTransfer(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v3

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryColorTmpEx:Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;

    iget-object v4, v4, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v4, v4, v1

    aget-object v4, v4, v3

    iput p1, v4, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;->blueoffset:I

    invoke-direct {p0, v1, v0, v3}, Lcom/konka/factory/desk/FactoryDeskImpl;->setWBGainOffset(III)V

    const/4 v4, 0x1

    return v4
.end method

.method public setWbGreenGain(S)Z
    .locals 5
    .param p1    # S

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4}, Lcom/konka/factory/desk/FactoryDB;->queryCurInputSrc()I

    move-result v0

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4, v0}, Lcom/konka/factory/desk/FactoryDB;->queryePicMode(I)I

    move-result v2

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4, v0, v2}, Lcom/konka/factory/desk/FactoryDB;->queryColorTmpIdx(II)I

    move-result v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    aget-object v4, v4, v0

    invoke-virtual {p0, v4}, Lcom/konka/factory/desk/FactoryDeskImpl;->InputSourceTransfer(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v3

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryColorTmpEx:Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;

    iget-object v4, v4, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v4, v4, v1

    aget-object v4, v4, v3

    iput p1, v4, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;->greengain:I

    invoke-direct {p0, v1, v0, v3}, Lcom/konka/factory/desk/FactoryDeskImpl;->setWBGainOffset(III)V

    const/4 v4, 0x1

    return v4
.end method

.method public setWbGreenOffset(S)Z
    .locals 5
    .param p1    # S

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4}, Lcom/konka/factory/desk/FactoryDB;->queryCurInputSrc()I

    move-result v0

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4, v0}, Lcom/konka/factory/desk/FactoryDB;->queryePicMode(I)I

    move-result v2

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4, v0, v2}, Lcom/konka/factory/desk/FactoryDB;->queryColorTmpIdx(II)I

    move-result v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    aget-object v4, v4, v0

    invoke-virtual {p0, v4}, Lcom/konka/factory/desk/FactoryDeskImpl;->InputSourceTransfer(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v3

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryColorTmpEx:Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;

    iget-object v4, v4, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v4, v4, v1

    aget-object v4, v4, v3

    iput p1, v4, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;->greenoffset:I

    invoke-direct {p0, v1, v0, v3}, Lcom/konka/factory/desk/FactoryDeskImpl;->setWBGainOffset(III)V

    const/4 v4, 0x1

    return v4
.end method

.method public setWbRedGain(S)Z
    .locals 5
    .param p1    # S

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4}, Lcom/konka/factory/desk/FactoryDB;->queryCurInputSrc()I

    move-result v0

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4, v0}, Lcom/konka/factory/desk/FactoryDB;->queryePicMode(I)I

    move-result v2

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4, v0, v2}, Lcom/konka/factory/desk/FactoryDB;->queryColorTmpIdx(II)I

    move-result v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    aget-object v4, v4, v0

    invoke-virtual {p0, v4}, Lcom/konka/factory/desk/FactoryDeskImpl;->InputSourceTransfer(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v3

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryColorTmpEx:Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;

    iget-object v4, v4, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v4, v4, v1

    aget-object v4, v4, v3

    iput p1, v4, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;->redgain:I

    invoke-direct {p0, v1, v0, v3}, Lcom/konka/factory/desk/FactoryDeskImpl;->setWBGainOffset(III)V

    const/4 v4, 0x1

    return v4
.end method

.method public setWbRedOffset(S)Z
    .locals 5
    .param p1    # S

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4}, Lcom/konka/factory/desk/FactoryDB;->queryCurInputSrc()I

    move-result v0

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4, v0}, Lcom/konka/factory/desk/FactoryDB;->queryePicMode(I)I

    move-result v2

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryDB:Lcom/konka/factory/desk/FactoryDB;

    invoke-virtual {v4, v0, v2}, Lcom/konka/factory/desk/FactoryDB;->queryColorTmpIdx(II)I

    move-result v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    aget-object v4, v4, v0

    invoke-virtual {p0, v4}, Lcom/konka/factory/desk/FactoryDeskImpl;->InputSourceTransfer(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v3

    iget-object v4, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->factoryColorTmpEx:Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;

    iget-object v4, v4, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v4, v4, v1

    aget-object v4, v4, v3

    iput p1, v4, Lcom/konka/factory/desk/IFactoryDesk$T_MS_COLOR_TEMPEX_DATA;->redoffset:I

    invoke-direct {p0, v1, v0, v3}, Lcom/konka/factory/desk/FactoryDeskImpl;->setWBGainOffset(III)V

    const/4 v4, 0x1

    return v4
.end method

.method public updatePqIniFiles()V
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/konka/factory/desk/FactoryDeskImpl;->fm:Lcom/mstar/android/tvapi/factory/FactoryManager;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/factory/FactoryManager;->updatePqIniFiles()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method
