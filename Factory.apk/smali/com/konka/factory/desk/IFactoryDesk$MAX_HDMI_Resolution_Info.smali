.class public final enum Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;
.super Ljava/lang/Enum;
.source "IFactoryDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/factory/desk/IFactoryDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MAX_HDMI_Resolution_Info"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

.field public static final enum E_HDMI1080i_50:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

.field public static final enum E_HDMI1080i_60:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

.field public static final enum E_HDMI1080p_24:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

.field public static final enum E_HDMI1080p_30:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

.field public static final enum E_HDMI1080p_50:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

.field public static final enum E_HDMI1080p_60:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

.field public static final enum E_HDMI1440x480i_60:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

.field public static final enum E_HDMI1440x480p_60:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

.field public static final enum E_HDMI1440x576i_50:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

.field public static final enum E_HDMI1440x576p_50:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

.field public static final enum E_HDMI480i_60:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

.field public static final enum E_HDMI480p_60:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

.field public static final enum E_HDMI576i_50:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

.field public static final enum E_HDMI576p_50:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

.field public static final enum E_HDMI720p_50:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

.field public static final enum E_HDMI720p_60:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

.field public static final enum E_HDMI_MAX:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    const-string v1, "E_HDMI480i_60"

    invoke-direct {v0, v1, v3}, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->E_HDMI480i_60:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    const-string v1, "E_HDMI480p_60"

    invoke-direct {v0, v1, v4}, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->E_HDMI480p_60:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    const-string v1, "E_HDMI576i_50"

    invoke-direct {v0, v1, v5}, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->E_HDMI576i_50:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    const-string v1, "E_HDMI576p_50"

    invoke-direct {v0, v1, v6}, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->E_HDMI576p_50:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    const-string v1, "E_HDMI720p_60"

    invoke-direct {v0, v1, v7}, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->E_HDMI720p_60:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    const-string v1, "E_HDMI720p_50"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->E_HDMI720p_50:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    const-string v1, "E_HDMI1080i_60"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->E_HDMI1080i_60:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    const-string v1, "E_HDMI1080i_50"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->E_HDMI1080i_50:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    const-string v1, "E_HDMI1080p_60"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->E_HDMI1080p_60:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    const-string v1, "E_HDMI1080p_50"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->E_HDMI1080p_50:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    const-string v1, "E_HDMI1080p_30"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->E_HDMI1080p_30:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    const-string v1, "E_HDMI1080p_24"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->E_HDMI1080p_24:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    const-string v1, "E_HDMI1440x480i_60"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->E_HDMI1440x480i_60:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    const-string v1, "E_HDMI1440x480p_60"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->E_HDMI1440x480p_60:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    const-string v1, "E_HDMI1440x576i_50"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->E_HDMI1440x576i_50:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    const-string v1, "E_HDMI1440x576p_50"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->E_HDMI1440x576p_50:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    new-instance v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    const-string v1, "E_HDMI_MAX"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->E_HDMI_MAX:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    const/16 v0, 0x11

    new-array v0, v0, [Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->E_HDMI480i_60:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->E_HDMI480p_60:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->E_HDMI576i_50:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    aput-object v1, v0, v5

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->E_HDMI576p_50:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    aput-object v1, v0, v6

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->E_HDMI720p_60:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->E_HDMI720p_50:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->E_HDMI1080i_60:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->E_HDMI1080i_50:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->E_HDMI1080p_60:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->E_HDMI1080p_50:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->E_HDMI1080p_30:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->E_HDMI1080p_24:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->E_HDMI1440x480i_60:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->E_HDMI1440x480p_60:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->E_HDMI1440x576i_50:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->E_HDMI1440x576p_50:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->E_HDMI_MAX:Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->$VALUES:[Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;
    .locals 1

    const-class v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    return-object v0
.end method

.method public static values()[Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;
    .locals 1

    sget-object v0, Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->$VALUES:[Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    invoke-virtual {v0}, [Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/konka/factory/desk/IFactoryDesk$MAX_HDMI_Resolution_Info;

    return-object v0
.end method
