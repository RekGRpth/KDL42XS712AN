.class public Lcom/konka/factory/Storage;
.super Ljava/lang/Object;
.source "Storage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/factory/Storage$UsbReceiver;
    }
.end annotation


# instance fields
.field private UsbVolumeIndex:I

.field private stm:Lcom/mstar/android/storage/MStorageManager;

.field private usbReceiver:Lcom/konka/factory/Storage$UsbReceiver;

.field private volumes:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v1, 0x0

    iput v1, p0, Lcom/konka/factory/Storage;->UsbVolumeIndex:I

    new-instance v1, Lcom/konka/factory/Storage$UsbReceiver;

    invoke-direct {v1, p0}, Lcom/konka/factory/Storage$UsbReceiver;-><init>(Lcom/konka/factory/Storage;)V

    iput-object v1, p0, Lcom/konka/factory/Storage;->usbReceiver:Lcom/konka/factory/Storage$UsbReceiver;

    invoke-static {p1}, Lcom/mstar/android/storage/MStorageManager;->getInstance(Landroid/content/Context;)Lcom/mstar/android/storage/MStorageManager;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/factory/Storage;->stm:Lcom/mstar/android/storage/MStorageManager;

    iget-object v1, p0, Lcom/konka/factory/Storage;->stm:Lcom/mstar/android/storage/MStorageManager;

    invoke-virtual {v1}, Lcom/mstar/android/storage/MStorageManager;->getVolumePaths()[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/factory/Storage;->volumes:[Ljava/lang/String;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/factory/Storage;->usbReceiver:Lcom/konka/factory/Storage$UsbReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "file"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/factory/Storage;->usbReceiver:Lcom/konka/factory/Storage$UsbReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method


# virtual methods
.method public getUsbPath()Ljava/lang/String;
    .locals 8

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/konka/factory/Storage;->stm:Lcom/mstar/android/storage/MStorageManager;

    invoke-virtual {v5}, Lcom/mstar/android/storage/MStorageManager;->getVolumePaths()[Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/konka/factory/Storage;->volumes:[Ljava/lang/String;

    iget-object v5, p0, Lcom/konka/factory/Storage;->volumes:[Ljava/lang/String;

    if-eqz v5, :cond_3

    const-string v2, "Usbs now exist:\n"

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    iget-object v5, p0, Lcom/konka/factory/Storage;->volumes:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_2

    iget-object v5, p0, Lcom/konka/factory/Storage;->stm:Lcom/mstar/android/storage/MStorageManager;

    iget-object v6, p0, Lcom/konka/factory/Storage;->volumes:[Ljava/lang/String;

    aget-object v6, v6, v1

    invoke-virtual {v5, v6}, Lcom/mstar/android/storage/MStorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "volumes[i].getPath()="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/konka/factory/Storage;->volumes:[Ljava/lang/String;

    aget-object v7, v7, v1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\n"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    if-eqz v3, :cond_0

    const-string v5, "mounted"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lcom/konka/factory/Storage;->volumes:[Ljava/lang/String;

    aget-object v5, v5, v1

    const-string v6, "/mnt/usb/mmcblka1"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/konka/factory/Storage;->volumes:[Ljava/lang/String;

    aget-object v6, v6, v1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x1

    iput v1, p0, Lcom/konka/factory/Storage;->UsbVolumeIndex:I

    :cond_2
    if-eqz v0, :cond_4

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v4, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/factory/Storage;->volumes:[Ljava/lang/String;

    iget v5, p0, Lcom/konka/factory/Storage;->UsbVolumeIndex:I

    aget-object v4, v4, v5

    :cond_3
    :goto_1
    return-object v4

    :cond_4
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v6, "333333No usb exist!"

    invoke-virtual {v5, v6}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_1
.end method
