.class public Lcom/konka/factory/CustomizedUpdateActivity;
.super Landroid/app/Activity;
.source "CustomizedUpdateActivity.java"


# instance fields
.field KeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field private PowerMOdeEnable:[Ljava/lang/String;

.field private defaultCountryIndex:I

.field private defaultCustomerID:Ljava/lang/String;

.field private defaultLanguageIndex:Ljava/lang/String;

.field private defaultPowerModeIndex:I

.field private defaultPowerOffLogoDspTime:I

.field private defaultTeletextIndex:I

.field private defaultTunrOffLogoIndex:I

.field private flagAdsVideo:Z

.field private flagAdsVideoConfig:Z

.field private flagLogo:Z

.field private flagSticker:Z

.field handler:Landroid/os/Handler;

.field private mpDialog:Landroid/app/ProgressDialog;

.field private needUpdateAdsVideoIndex:I

.field private needUpdateLogoIndex:I

.field private needUpdateStickerIndex:I

.field private serviceVersion:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "secondary"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "memory"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "direct"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/konka/factory/CustomizedUpdateActivity;->PowerMOdeEnable:[Ljava/lang/String;

    const-string v0, "en_US"

    iput-object v0, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultLanguageIndex:Ljava/lang/String;

    iput v3, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultCountryIndex:I

    iput v3, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultTeletextIndex:I

    iput v3, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultPowerModeIndex:I

    iput v3, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultTunrOffLogoIndex:I

    iput v3, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultPowerOffLogoDspTime:I

    iput v3, p0, Lcom/konka/factory/CustomizedUpdateActivity;->needUpdateLogoIndex:I

    iput v3, p0, Lcom/konka/factory/CustomizedUpdateActivity;->needUpdateStickerIndex:I

    iput v3, p0, Lcom/konka/factory/CustomizedUpdateActivity;->needUpdateAdsVideoIndex:I

    const-string v0, "ENI"

    iput-object v0, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultCustomerID:Ljava/lang/String;

    const-string v0, "Default"

    iput-object v0, p0, Lcom/konka/factory/CustomizedUpdateActivity;->serviceVersion:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/factory/CustomizedUpdateActivity;->mpDialog:Landroid/app/ProgressDialog;

    iput-boolean v3, p0, Lcom/konka/factory/CustomizedUpdateActivity;->flagLogo:Z

    iput-boolean v3, p0, Lcom/konka/factory/CustomizedUpdateActivity;->flagSticker:Z

    iput-boolean v3, p0, Lcom/konka/factory/CustomizedUpdateActivity;->flagAdsVideo:Z

    iput-boolean v3, p0, Lcom/konka/factory/CustomizedUpdateActivity;->flagAdsVideoConfig:Z

    new-instance v0, Lcom/konka/factory/CustomizedUpdateActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/factory/CustomizedUpdateActivity$1;-><init>(Lcom/konka/factory/CustomizedUpdateActivity;)V

    iput-object v0, p0, Lcom/konka/factory/CustomizedUpdateActivity;->handler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/factory/CustomizedUpdateActivity$4;

    invoke-direct {v0, p0}, Lcom/konka/factory/CustomizedUpdateActivity$4;-><init>(Lcom/konka/factory/CustomizedUpdateActivity;)V

    iput-object v0, p0, Lcom/konka/factory/CustomizedUpdateActivity;->KeyListener:Landroid/content/DialogInterface$OnKeyListener;

    return-void
.end method

.method static synthetic access$000(Lcom/konka/factory/CustomizedUpdateActivity;)Z
    .locals 1
    .param p0    # Lcom/konka/factory/CustomizedUpdateActivity;

    iget-boolean v0, p0, Lcom/konka/factory/CustomizedUpdateActivity;->flagLogo:Z

    return v0
.end method

.method static synthetic access$100(Lcom/konka/factory/CustomizedUpdateActivity;)Z
    .locals 1
    .param p0    # Lcom/konka/factory/CustomizedUpdateActivity;

    iget-boolean v0, p0, Lcom/konka/factory/CustomizedUpdateActivity;->flagSticker:Z

    return v0
.end method

.method static synthetic access$200(Lcom/konka/factory/CustomizedUpdateActivity;)Z
    .locals 1
    .param p0    # Lcom/konka/factory/CustomizedUpdateActivity;

    iget-boolean v0, p0, Lcom/konka/factory/CustomizedUpdateActivity;->flagAdsVideo:Z

    return v0
.end method

.method static synthetic access$300(Lcom/konka/factory/CustomizedUpdateActivity;)Z
    .locals 1
    .param p0    # Lcom/konka/factory/CustomizedUpdateActivity;

    iget-boolean v0, p0, Lcom/konka/factory/CustomizedUpdateActivity;->flagAdsVideoConfig:Z

    return v0
.end method

.method static synthetic access$400(Lcom/konka/factory/CustomizedUpdateActivity;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/konka/factory/CustomizedUpdateActivity;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/konka/factory/CustomizedUpdateActivity;->showMessage(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/konka/factory/CustomizedUpdateActivity;)V
    .locals 0
    .param p0    # Lcom/konka/factory/CustomizedUpdateActivity;

    invoke-direct {p0}, Lcom/konka/factory/CustomizedUpdateActivity;->checkUpgradeAction()V

    return-void
.end method

.method static synthetic access$600(Lcom/konka/factory/CustomizedUpdateActivity;Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p0    # Lcom/konka/factory/CustomizedUpdateActivity;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/konka/factory/CustomizedUpdateActivity;->downloadFile(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/konka/factory/CustomizedUpdateActivity;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p0    # Lcom/konka/factory/CustomizedUpdateActivity;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/konka/factory/CustomizedUpdateActivity;->moveFileToDestination(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/konka/factory/CustomizedUpdateActivity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/konka/factory/CustomizedUpdateActivity;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Lcom/konka/factory/CustomizedUpdateActivity;->delFile(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$900(Lcom/konka/factory/CustomizedUpdateActivity;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/konka/factory/CustomizedUpdateActivity;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/konka/factory/CustomizedUpdateActivity;->setFlag(Ljava/lang/String;)V

    return-void
.end method

.method private checkUpgradeAction()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    invoke-direct {p0}, Lcom/konka/factory/CustomizedUpdateActivity;->getCusDefSettings()Z

    move-result v1

    if-ne v3, v1, :cond_0

    iget-object v1, p0, Lcom/konka/factory/CustomizedUpdateActivity;->serviceVersion:Ljava/lang/String;

    const-string v2, "V1.0.01"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/konka/factory/CustomizedUpdateActivity;->mpDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    const-string v1, "Failed! Service version not supported."

    const/16 v2, 0x5dc

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v1, p0, Lcom/konka/factory/CustomizedUpdateActivity;->needUpdateLogoIndex:I

    if-ne v3, v1, :cond_2

    const-string v1, "bootanimation.zip"

    const-string v2, "customerlogo/"

    invoke-direct {p0, v1, v2, v4}, Lcom/konka/factory/CustomizedUpdateActivity;->updateFile(Ljava/lang/String;Ljava/lang/String;Z)V

    :goto_1
    iget v1, p0, Lcom/konka/factory/CustomizedUpdateActivity;->needUpdateStickerIndex:I

    if-ne v3, v1, :cond_3

    const-string v1, "sticker.png"

    const-string v2, "StickerDemo/"

    invoke-direct {p0, v1, v2, v4}, Lcom/konka/factory/CustomizedUpdateActivity;->updateFile(Ljava/lang/String;Ljava/lang/String;Z)V

    :goto_2
    iget v1, p0, Lcom/konka/factory/CustomizedUpdateActivity;->needUpdateAdsVideoIndex:I

    if-ne v3, v1, :cond_4

    const-string v1, "bootvideo.mp4"

    const-string v2, "AdBoot/AdBootMedia/"

    invoke-direct {p0, v1, v2, v3}, Lcom/konka/factory/CustomizedUpdateActivity;->updateFile(Ljava/lang/String;Ljava/lang/String;Z)V

    const-string v1, "videoConfigs.txt"

    const-string v2, "AdBoot/AdBootMedia/"

    invoke-direct {p0, v1, v2, v3}, Lcom/konka/factory/CustomizedUpdateActivity;->updateFile(Ljava/lang/String;Ljava/lang/String;Z)V

    :goto_3
    iget-boolean v1, p0, Lcom/konka/factory/CustomizedUpdateActivity;->flagLogo:Z

    if-ne v1, v3, :cond_6

    iget-boolean v1, p0, Lcom/konka/factory/CustomizedUpdateActivity;->flagSticker:Z

    if-ne v1, v3, :cond_6

    iget-boolean v1, p0, Lcom/konka/factory/CustomizedUpdateActivity;->flagAdsVideo:Z

    if-ne v1, v3, :cond_6

    iget-boolean v1, p0, Lcom/konka/factory/CustomizedUpdateActivity;->flagAdsVideoConfig:Z

    if-ne v1, v3, :cond_6

    invoke-direct {p0}, Lcom/konka/factory/CustomizedUpdateActivity;->updateConfigData()Z

    move-result v1

    if-ne v1, v3, :cond_5

    const-string v1, "config.txt"

    const-string v2, "/"

    invoke-direct {p0, v1, v2}, Lcom/konka/factory/CustomizedUpdateActivity;->delFile(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "Write customer default settings success."

    invoke-direct {p0, v1}, Lcom/konka/factory/CustomizedUpdateActivity;->showMessage(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iput-boolean v3, p0, Lcom/konka/factory/CustomizedUpdateActivity;->flagLogo:Z

    goto :goto_1

    :cond_3
    iput-boolean v3, p0, Lcom/konka/factory/CustomizedUpdateActivity;->flagSticker:Z

    goto :goto_2

    :cond_4
    iput-boolean v3, p0, Lcom/konka/factory/CustomizedUpdateActivity;->flagAdsVideo:Z

    iput-boolean v3, p0, Lcom/konka/factory/CustomizedUpdateActivity;->flagAdsVideoConfig:Z

    goto :goto_3

    :cond_5
    const-string v1, "Failed to upgrade customer default settings!"

    invoke-direct {p0, v1}, Lcom/konka/factory/CustomizedUpdateActivity;->showMessage(Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    invoke-direct {p0}, Lcom/konka/factory/CustomizedUpdateActivity;->updateConfigData()Z

    move-result v1

    if-ne v1, v3, :cond_7

    const-string v1, "config.txt"

    const-string v2, "/"

    invoke-direct {p0, v1, v2}, Lcom/konka/factory/CustomizedUpdateActivity;->delFile(Ljava/lang/String;Ljava/lang/String;)V

    iput v4, v0, Landroid/os/Message;->arg1:I

    :goto_4
    iget-object v1, p0, Lcom/konka/factory/CustomizedUpdateActivity;->handler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    :cond_7
    iput v3, v0, Landroid/os/Message;->arg1:I

    goto :goto_4
.end method

.method private chmodFile(Ljava/io/File;)V
    .locals 6
    .param p1    # Ljava/io/File;

    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "chmod 666 "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "CustomizedUpdateActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "command = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    const-string v3, "CustomizedUpdateActivity"

    const-string v4, "chmod fail!!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private copyFile(Ljava/io/File;Ljava/io/File;)Z
    .locals 9
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v6, 0x0

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "CustomizedUpdateActivity"

    const-string v8, "~src file not exits~"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v6

    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->isFile()Z

    move-result v7

    if-nez v7, :cond_1

    const-string v7, "CustomizedUpdateActivity"

    const-string v8, "~src file is not a file~"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->canRead()Z

    move-result v7

    if-nez v7, :cond_2

    const-string v7, "CustomizedUpdateActivity"

    const-string v8, "~src file can  not read~"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_4

    const-string v7, "CustomizedUpdateActivity"

    const-string v8, "~dest file not exits~"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_3

    invoke-virtual {p2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v7

    invoke-virtual {v7}, Ljava/io/File;->mkdirs()Z

    :cond_3
    invoke-virtual {p2}, Ljava/io/File;->createNewFile()Z

    :cond_4
    invoke-virtual {p2}, Ljava/io/File;->canRead()Z

    move-result v7

    if-nez v7, :cond_5

    const-string v7, "CustomizedUpdateActivity"

    const-string v8, "~dest file can  not read~"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_5
    const-string v7, "CustomizedUpdateActivity"

    const-string v8, "~src file OK~"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-direct {v1, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    new-instance v4, Ljava/io/BufferedOutputStream;

    invoke-direct {v4, v5}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    const v7, 0x1f4000

    new-array v0, v7, [B

    :goto_1
    invoke-virtual {v1, v0}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v3

    const/4 v7, -0x1

    if-eq v3, v7, :cond_6

    invoke-virtual {v4, v0, v6, v3}, Ljava/io/BufferedOutputStream;->write([BII)V

    goto :goto_1

    :cond_6
    invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->flush()V

    invoke-virtual {v1}, Ljava/io/BufferedInputStream;->close()V

    invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->close()V

    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    const-string v6, "CustomizedUpdateActivity"

    const-string v7, "~chmod dest file OK~"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v6, 0x1

    goto/16 :goto_0
.end method

.method private delFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v1, Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "/data/misc/konka/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v0

    const-string v2, "CustomizedUpdateActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "delfile"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "==========>>>["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private downloadCusDefSettingFile()V
    .locals 3

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/konka/factory/CustomizedUpdateActivity$2;

    invoke-direct {v2, p0, v0}, Lcom/konka/factory/CustomizedUpdateActivity$2;-><init>(Lcom/konka/factory/CustomizedUpdateActivity;Landroid/os/Handler;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method private downloadFile(Ljava/lang/String;Ljava/lang/String;)I
    .locals 13
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v8, 0x0

    new-instance v3, Ljava/io/File;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "/data/misc/konka/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v3, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_0

    invoke-virtual {v3}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v10

    invoke-virtual {v10}, Ljava/io/File;->mkdirs()Z

    :cond_0
    const/4 v5, 0x0

    :try_start_0
    new-instance v0, Ljava/net/URL;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "http://192.168.0.100:8080/FactoryFastConfig/ResponseFileRequest?filename="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v0, v10}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    check-cast v2, Ljava/net/HttpURLConnection;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->connect()V

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v6

    new-instance v9, Ljava/io/RandomAccessFile;

    const-string v10, "rwd"

    invoke-direct {v9, v3, v10}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    const/16 v10, 0x2000

    :try_start_1
    new-array v1, v10, [B

    const/4 v7, 0x0

    :goto_0
    const/4 v10, 0x0

    const/16 v11, 0x2000

    invoke-virtual {v6, v1, v10, v11}, Ljava/io/InputStream;->read([BII)I

    move-result v7

    const/4 v10, -0x1

    if-eq v7, v10, :cond_1

    const/4 v10, 0x0

    invoke-virtual {v9, v1, v10, v7}, Ljava/io/RandomAccessFile;->write([BII)V
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    :catch_0
    move-exception v4

    move-object v8, v9

    :goto_1
    invoke-virtual {v4}, Ljava/net/MalformedURLException;->printStackTrace()V

    const/4 v5, 0x1

    :goto_2
    return v5

    :cond_1
    :try_start_2
    const-string v10, "CustomizedUpdateActivity"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "download "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " File==========>>>OK\n"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/net/MalformedURLException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    move-object v8, v9

    goto :goto_2

    :catch_1
    move-exception v4

    :goto_3
    invoke-virtual {v4}, Ljava/io/IOException;->printStackTrace()V

    const/4 v5, 0x1

    goto :goto_2

    :catch_2
    move-exception v4

    move-object v8, v9

    goto :goto_3

    :catch_3
    move-exception v4

    goto :goto_1
.end method

.method private getCusDefSettings()Z
    .locals 4

    new-instance v1, Lcom/konka/factory/IniEditor;

    invoke-direct {v1}, Lcom/konka/factory/IniEditor;-><init>()V

    const-string v2, "/data/misc/konka/config.txt"

    invoke-virtual {v1, v2}, Lcom/konka/factory/IniEditor;->loadFile(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v2, "Failed to upgrade customer default settings!"

    invoke-direct {p0, v2}, Lcom/konka/factory/CustomizedUpdateActivity;->showMessage(Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    const-string v2, "CURRENT_SERVICE_VERSION:ServiceVersion"

    const-string v3, "default"

    invoke-virtual {v1, v2, v3}, Lcom/konka/factory/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/factory/CustomizedUpdateActivity;->serviceVersion:Ljava/lang/String;

    const-string v2, "CUS_DEF_SETTING:OSDLanguage"

    const-string v3, "konka"

    invoke-virtual {v1, v2, v3}, Lcom/konka/factory/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultLanguageIndex:Ljava/lang/String;

    const-string v2, "CUS_DEF_SETTING:TuningCountry"

    const-string v3, "-1"

    invoke-virtual {v1, v2, v3}, Lcom/konka/factory/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultCountryIndex:I

    const-string v2, "CUS_DEF_SETTING:TeletextEnabled"

    const-string v3, "0"

    invoke-virtual {v1, v2, v3}, Lcom/konka/factory/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultTeletextIndex:I

    const-string v2, "CUS_DEF_SETTING:PowerOnMode"

    const-string v3, "-1"

    invoke-virtual {v1, v2, v3}, Lcom/konka/factory/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultPowerModeIndex:I

    const-string v2, "CUS_DEF_SETTING:PowerOffLogoEnabled"

    const-string v3, "-1"

    invoke-virtual {v1, v2, v3}, Lcom/konka/factory/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultTunrOffLogoIndex:I

    const-string v2, "CUS_DEF_SETTING:PowerOffLogoDspTime"

    const-string v3, "0"

    invoke-virtual {v1, v2, v3}, Lcom/konka/factory/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultPowerOffLogoDspTime:I

    const-string v2, "CUS_DEF_SETTING:UpdateLogoEnabled"

    const-string v3, "0"

    invoke-virtual {v1, v2, v3}, Lcom/konka/factory/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lcom/konka/factory/CustomizedUpdateActivity;->needUpdateLogoIndex:I

    const-string v2, "CUS_DEF_SETTING:UpdateStickerEnabled"

    const-string v3, "0"

    invoke-virtual {v1, v2, v3}, Lcom/konka/factory/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lcom/konka/factory/CustomizedUpdateActivity;->needUpdateStickerIndex:I

    const-string v2, "CUS_DEF_SETTING:UpdateAdsVideoEnabled"

    const-string v3, "0"

    invoke-virtual {v1, v2, v3}, Lcom/konka/factory/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lcom/konka/factory/CustomizedUpdateActivity;->needUpdateAdsVideoIndex:I

    const-string v2, "CUS_DEF_SETTING:CustomerID"

    const-string v3, "konka"

    invoke-virtual {v1, v2, v3}, Lcom/konka/factory/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultCustomerID:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method private moveFileToDestination(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "/customercfg/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v5, "CustomizedUpdateActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ".... "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " update....destPath is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ".........."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "/data/misc/konka/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_0
    invoke-direct {p0, v3, v0}, Lcom/konka/factory/CustomizedUpdateActivity;->copyFile(Ljava/io/File;Ljava/io/File;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v4, 0x1

    :cond_0
    invoke-direct {p0, v0}, Lcom/konka/factory/CustomizedUpdateActivity;->chmodFile(Ljava/io/File;)V

    const-string v5, "CustomizedUpdateActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "update "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " success!"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v4

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    const/4 v4, 0x0

    goto :goto_0
.end method

.method private setFlag(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x1

    const-string v0, "bootanimation.zip"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-boolean v1, p0, Lcom/konka/factory/CustomizedUpdateActivity;->flagLogo:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "sticker.png"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iput-boolean v1, p0, Lcom/konka/factory/CustomizedUpdateActivity;->flagSticker:Z

    goto :goto_0

    :cond_2
    const-string v0, "bootvideo.mp4"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iput-boolean v1, p0, Lcom/konka/factory/CustomizedUpdateActivity;->flagAdsVideo:Z

    goto :goto_0

    :cond_3
    const-string v0, "videoConfigs.txt"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/konka/factory/CustomizedUpdateActivity;->flagAdsVideoConfig:Z

    goto :goto_0
.end method

.method private showMessage(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/factory/CustomizedUpdateActivity;->mpDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/konka/factory/CustomizedUpdateActivity;->finish()V

    return-void
.end method

.method private updateConfigData()Z
    .locals 2

    const/4 v0, 0x1

    invoke-direct {p0}, Lcom/konka/factory/CustomizedUpdateActivity;->updateCustomOption()Z

    move-result v1

    if-ne v1, v0, :cond_0

    invoke-direct {p0}, Lcom/konka/factory/CustomizedUpdateActivity;->updateCustomerID()Z

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateCustomOption()Z
    .locals 6

    const/4 v5, -0x1

    const/4 v2, 0x0

    :try_start_0
    invoke-static {p0}, Lcom/konka/factory/desk/FactoryDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/factory/desk/FactoryDeskImpl;

    move-result-object v1

    iget-object v3, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultLanguageIndex:Ljava/lang/String;

    const-string v4, "konka"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    invoke-interface {v1}, Lcom/konka/factory/desk/IFactoryDesk;->setCustomerSettingEnable()Z

    iget-object v3, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultLanguageIndex:Ljava/lang/String;

    invoke-interface {v1, v3}, Lcom/konka/factory/desk/IFactoryDesk;->setDefaultLanguage(Ljava/lang/String;)Z

    :cond_0
    iget v3, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultCountryIndex:I

    if-eq v3, v5, :cond_1

    invoke-interface {v1}, Lcom/konka/factory/desk/IFactoryDesk;->setCustomerSettingEnable()Z

    iget v3, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultCountryIndex:I

    const/16 v4, 0x22

    if-ge v3, v4, :cond_6

    iget v3, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultCountryIndex:I

    iput v3, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultCountryIndex:I

    :goto_0
    iget v3, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultCountryIndex:I

    invoke-interface {v1, v3}, Lcom/konka/factory/desk/IFactoryDesk;->setDefaultAutoTuningCountry(I)Z

    :cond_1
    iget v3, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultTunrOffLogoIndex:I

    if-eq v3, v5, :cond_2

    iget v3, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultTunrOffLogoIndex:I

    invoke-interface {v1, v3}, Lcom/konka/factory/desk/IFactoryDesk;->setPowerOffLogoMode(I)Z

    :cond_2
    iget v3, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultPowerOffLogoDspTime:I

    if-eq v3, v5, :cond_3

    iget v3, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultPowerOffLogoDspTime:I

    mul-int/lit16 v3, v3, 0x3e8

    invoke-interface {v1, v3}, Lcom/konka/factory/desk/IFactoryDesk;->setPowerOffLogoDspTime(I)Z

    :cond_3
    iget v3, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultTeletextIndex:I

    if-eq v3, v5, :cond_4

    iget v3, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultTeletextIndex:I

    invoke-interface {v1, v3}, Lcom/konka/factory/desk/IFactoryDesk;->setTeletextMode(I)Z

    :cond_4
    iget v3, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultPowerModeIndex:I

    if-eq v3, v5, :cond_5

    iget v3, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultPowerModeIndex:I

    invoke-interface {v1, v3}, Lcom/konka/factory/desk/IFactoryDesk;->setPowerOnMode(I)Z

    invoke-virtual {p0}, Lcom/konka/factory/CustomizedUpdateActivity;->SetPowerOnMode()V

    :cond_5
    const/4 v2, 0x1

    const-string v3, "CustomizedUpdateActivity"

    const-string v4, "updateCustomOption success!"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return v2

    :cond_6
    iget v3, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultCountryIndex:I

    add-int/lit8 v3, v3, 0x17

    iput v3, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultCountryIndex:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_1
.end method

.method private updateCustomerID()Z
    .locals 12

    const/4 v11, 0x2

    const/4 v10, 0x0

    const/16 v9, 0x8

    const/4 v8, 0x1

    const/4 v4, 0x1

    iget-object v6, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultCustomerID:Ljava/lang/String;

    const-string v7, "konka"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eq v6, v8, :cond_2

    const/4 v5, 0x0

    new-array v0, v9, [B

    iget-object v6, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultCustomerID:Ljava/lang/String;

    invoke-virtual {v6, v10}, Ljava/lang/String;->charAt(I)C

    move-result v6

    int-to-byte v6, v6

    aput-byte v6, v0, v10

    iget-object v6, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultCustomerID:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/lang/String;->charAt(I)C

    move-result v6

    int-to-byte v6, v6

    aput-byte v6, v0, v8

    iget-object v6, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultCustomerID:Ljava/lang/String;

    invoke-virtual {v6, v11}, Ljava/lang/String;->charAt(I)C

    move-result v6

    int-to-byte v6, v6

    aput-byte v6, v0, v11

    const/4 v3, 0x3

    :goto_0
    if-ge v3, v9, :cond_0

    const-string v6, "KONKA"

    add-int/lit8 v7, v3, -0x3

    invoke-virtual {v6, v7}, Ljava/lang/String;->charAt(I)C

    move-result v6

    int-to-byte v6, v6

    aput-byte v6, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    const/16 v6, 0x2000

    :try_start_0
    new-array v1, v6, [S

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    const/16 v7, 0x1d

    const/16 v8, 0x2000

    invoke-virtual {v6, v7, v8}, Lcom/mstar/android/tvapi/common/TvManager;->readFromSpiFlashByBank(II)[S

    move-result-object v1

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v9, :cond_1

    add-int/lit16 v6, v3, 0xd0

    aget-byte v7, v0, v3

    int-to-short v7, v7

    aput-short v7, v1, v6

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    const/16 v7, 0x1d

    invoke-virtual {v6, v7, v1}, Lcom/mstar/android/tvapi/common/TvManager;->writeToSpiFlashByBank(I[S)Z

    const/4 v4, 0x1

    const-string v6, "CustomizedUpdateActivity"

    const-string v7, "updateCustomerID success!"

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    return v4

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v4, 0x0

    goto :goto_2

    :cond_2
    const-string v6, "CustomizedUpdateActivity"

    const-string v7, "No need updateCustomerID!"

    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private updateFile(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/factory/CustomizedUpdateActivity$3;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/konka/factory/CustomizedUpdateActivity$3;-><init>(Lcom/konka/factory/CustomizedUpdateActivity;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method


# virtual methods
.method public SetPowerOnMode()V
    .locals 5

    :try_start_0
    const-string v1, "CustomizedUpdateActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "factory_poweron_mode "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/factory/CustomizedUpdateActivity;->PowerMOdeEnable:[Ljava/lang/String;

    iget v4, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultPowerModeIndex:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    const-string v2, "factory_poweron_mode"

    iget-object v3, p0, Lcom/konka/factory/CustomizedUpdateActivity;->PowerMOdeEnable:[Ljava/lang/String;

    iget v4, p0, Lcom/konka/factory/CustomizedUpdateActivity;->defaultPowerModeIndex:I

    aget-object v3, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/mstar/android/tvapi/common/TvManager;->setEnvironment(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/konka/factory/CustomizedUpdateActivity;->mpDialog:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/konka/factory/CustomizedUpdateActivity;->mpDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    iget-object v0, p0, Lcom/konka/factory/CustomizedUpdateActivity;->mpDialog:Landroid/app/ProgressDialog;

    const-string v1, "Start updating customer default settings!"

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/CustomizedUpdateActivity;->mpDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    iget-object v0, p0, Lcom/konka/factory/CustomizedUpdateActivity;->mpDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/konka/factory/CustomizedUpdateActivity;->mpDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    iget-object v0, p0, Lcom/konka/factory/CustomizedUpdateActivity;->mpDialog:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/konka/factory/CustomizedUpdateActivity;->KeyListener:Landroid/content/DialogInterface$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    invoke-direct {p0}, Lcom/konka/factory/CustomizedUpdateActivity;->downloadCusDefSettingFile()V

    return-void
.end method
