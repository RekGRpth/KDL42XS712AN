.class public Lcom/konka/factory/MainmenuActivity;
.super Landroid/app/Activity;
.source "MainmenuActivity.java"


# static fields
.field private static usbman:Landroid/hardware/usb/UsbManager;


# instance fields
.field private adcViewHolder:Lcom/konka/factory/ADCAdjustViewHolder;

.field private currentPage:I

.field private customerspecialsettingsViewHolder:Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;

.field private defaultconfigViewHolder:Lcom/konka/factory/DEFAULTCONFIGAdjustViewHolder;

.field private energyViewHolder:Lcom/konka/factory/EnergyAjustViewHolder;

.field private factoryDesk:Lcom/konka/factory/desk/IFactoryDesk;

.field private factoryViewHolder:Lcom/konka/factory/FactoryMenuViewHolder;

.field private isFirst:Z

.field private listener:Landroid/view/View$OnClickListener;

.field private netfactoryViewHolder:Lcom/konka/factory/NETFACTORYAdjustViewHolder;

.field private nonLinearViewHolder:Lcom/konka/factory/NonLinearAdjustViewHolder;

.field private nonStandardViewHolder:Lcom/konka/factory/NonStandardAdjustViewHolder;

.field private otherOptionViewHolder:Lcom/konka/factory/OtherOptionAdjustViewHolder;

.field private overScanViewHolder:Lcom/konka/factory/OverScanAdjustViewHolder;

.field private peqViewHolder:Lcom/konka/factory/PEQAdjustViewHolder;

.field private picModeViewHolder:Lcom/konka/factory/PictureModeAdjustViewHolder;

.field private serialprintViewHolder:Lcom/konka/factory/SerialPrintAdjustViewHolder;

.field private soundModeViewHolder:Lcom/konka/factory/SoundModeAdjustViewHolder;

.field private sscViewHolder:Lcom/konka/factory/SSCAdjustViewHolder;

.field private updateViewHolder:Lcom/konka/factory/UPDATEAdjustViewHolder;

.field private viewFlipper:Landroid/widget/ViewFlipper;

.field private wbViewHolder:Lcom/konka/factory/WBAdjustViewHolder;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/factory/MainmenuActivity;->currentPage:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/factory/MainmenuActivity;->isFirst:Z

    new-instance v0, Lcom/konka/factory/MainmenuActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/factory/MainmenuActivity$1;-><init>(Lcom/konka/factory/MainmenuActivity;)V

    iput-object v0, p0, Lcom/konka/factory/MainmenuActivity;->listener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public static CheckUsbIsExist()Z
    .locals 2

    const/4 v0, 0x0

    sget-object v1, Lcom/konka/factory/MainmenuActivity;->usbman:Landroid/hardware/usb/UsbManager;

    invoke-virtual {v1}, Landroid/hardware/usb/UsbManager;->getDeviceList()Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method static synthetic access$002(Lcom/konka/factory/MainmenuActivity;I)I
    .locals 0
    .param p0    # Lcom/konka/factory/MainmenuActivity;
    .param p1    # I

    iput p1, p0, Lcom/konka/factory/MainmenuActivity;->currentPage:I

    return p1
.end method

.method static synthetic access$100(Lcom/konka/factory/MainmenuActivity;)Landroid/widget/ViewFlipper;
    .locals 1
    .param p0    # Lcom/konka/factory/MainmenuActivity;

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity;->viewFlipper:Landroid/widget/ViewFlipper;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/OverScanAdjustViewHolder;
    .locals 1
    .param p0    # Lcom/konka/factory/MainmenuActivity;

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity;->overScanViewHolder:Lcom/konka/factory/OverScanAdjustViewHolder;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/OverScanAdjustViewHolder;)Lcom/konka/factory/OverScanAdjustViewHolder;
    .locals 0
    .param p0    # Lcom/konka/factory/MainmenuActivity;
    .param p1    # Lcom/konka/factory/OverScanAdjustViewHolder;

    iput-object p1, p0, Lcom/konka/factory/MainmenuActivity;->overScanViewHolder:Lcom/konka/factory/OverScanAdjustViewHolder;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/SSCAdjustViewHolder;
    .locals 1
    .param p0    # Lcom/konka/factory/MainmenuActivity;

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity;->sscViewHolder:Lcom/konka/factory/SSCAdjustViewHolder;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/SSCAdjustViewHolder;)Lcom/konka/factory/SSCAdjustViewHolder;
    .locals 0
    .param p0    # Lcom/konka/factory/MainmenuActivity;
    .param p1    # Lcom/konka/factory/SSCAdjustViewHolder;

    iput-object p1, p0, Lcom/konka/factory/MainmenuActivity;->sscViewHolder:Lcom/konka/factory/SSCAdjustViewHolder;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/PEQAdjustViewHolder;
    .locals 1
    .param p0    # Lcom/konka/factory/MainmenuActivity;

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity;->peqViewHolder:Lcom/konka/factory/PEQAdjustViewHolder;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/PEQAdjustViewHolder;)Lcom/konka/factory/PEQAdjustViewHolder;
    .locals 0
    .param p0    # Lcom/konka/factory/MainmenuActivity;
    .param p1    # Lcom/konka/factory/PEQAdjustViewHolder;

    iput-object p1, p0, Lcom/konka/factory/MainmenuActivity;->peqViewHolder:Lcom/konka/factory/PEQAdjustViewHolder;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/UPDATEAdjustViewHolder;
    .locals 1
    .param p0    # Lcom/konka/factory/MainmenuActivity;

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity;->updateViewHolder:Lcom/konka/factory/UPDATEAdjustViewHolder;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/UPDATEAdjustViewHolder;)Lcom/konka/factory/UPDATEAdjustViewHolder;
    .locals 0
    .param p0    # Lcom/konka/factory/MainmenuActivity;
    .param p1    # Lcom/konka/factory/UPDATEAdjustViewHolder;

    iput-object p1, p0, Lcom/konka/factory/MainmenuActivity;->updateViewHolder:Lcom/konka/factory/UPDATEAdjustViewHolder;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/NETFACTORYAdjustViewHolder;
    .locals 1
    .param p0    # Lcom/konka/factory/MainmenuActivity;

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity;->netfactoryViewHolder:Lcom/konka/factory/NETFACTORYAdjustViewHolder;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/NETFACTORYAdjustViewHolder;)Lcom/konka/factory/NETFACTORYAdjustViewHolder;
    .locals 0
    .param p0    # Lcom/konka/factory/MainmenuActivity;
    .param p1    # Lcom/konka/factory/NETFACTORYAdjustViewHolder;

    iput-object p1, p0, Lcom/konka/factory/MainmenuActivity;->netfactoryViewHolder:Lcom/konka/factory/NETFACTORYAdjustViewHolder;

    return-object p1
.end method

.method static synthetic access$1502(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/DEFAULTCONFIGAdjustViewHolder;)Lcom/konka/factory/DEFAULTCONFIGAdjustViewHolder;
    .locals 0
    .param p0    # Lcom/konka/factory/MainmenuActivity;
    .param p1    # Lcom/konka/factory/DEFAULTCONFIGAdjustViewHolder;

    iput-object p1, p0, Lcom/konka/factory/MainmenuActivity;->defaultconfigViewHolder:Lcom/konka/factory/DEFAULTCONFIGAdjustViewHolder;

    return-object p1
.end method

.method static synthetic access$1602(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/SerialPrintAdjustViewHolder;)Lcom/konka/factory/SerialPrintAdjustViewHolder;
    .locals 0
    .param p0    # Lcom/konka/factory/MainmenuActivity;
    .param p1    # Lcom/konka/factory/SerialPrintAdjustViewHolder;

    iput-object p1, p0, Lcom/konka/factory/MainmenuActivity;->serialprintViewHolder:Lcom/konka/factory/SerialPrintAdjustViewHolder;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/OtherOptionAdjustViewHolder;
    .locals 1
    .param p0    # Lcom/konka/factory/MainmenuActivity;

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity;->otherOptionViewHolder:Lcom/konka/factory/OtherOptionAdjustViewHolder;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/OtherOptionAdjustViewHolder;)Lcom/konka/factory/OtherOptionAdjustViewHolder;
    .locals 0
    .param p0    # Lcom/konka/factory/MainmenuActivity;
    .param p1    # Lcom/konka/factory/OtherOptionAdjustViewHolder;

    iput-object p1, p0, Lcom/konka/factory/MainmenuActivity;->otherOptionViewHolder:Lcom/konka/factory/OtherOptionAdjustViewHolder;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;
    .locals 1
    .param p0    # Lcom/konka/factory/MainmenuActivity;

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity;->customerspecialsettingsViewHolder:Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;

    return-object v0
.end method

.method static synthetic access$1802(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;)Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;
    .locals 0
    .param p0    # Lcom/konka/factory/MainmenuActivity;
    .param p1    # Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;

    iput-object p1, p0, Lcom/konka/factory/MainmenuActivity;->customerspecialsettingsViewHolder:Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;

    return-object p1
.end method

.method static synthetic access$200(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/ADCAdjustViewHolder;
    .locals 1
    .param p0    # Lcom/konka/factory/MainmenuActivity;

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity;->adcViewHolder:Lcom/konka/factory/ADCAdjustViewHolder;

    return-object v0
.end method

.method static synthetic access$202(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/ADCAdjustViewHolder;)Lcom/konka/factory/ADCAdjustViewHolder;
    .locals 0
    .param p0    # Lcom/konka/factory/MainmenuActivity;
    .param p1    # Lcom/konka/factory/ADCAdjustViewHolder;

    iput-object p1, p0, Lcom/konka/factory/MainmenuActivity;->adcViewHolder:Lcom/konka/factory/ADCAdjustViewHolder;

    return-object p1
.end method

.method static synthetic access$300(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/desk/IFactoryDesk;
    .locals 1
    .param p0    # Lcom/konka/factory/MainmenuActivity;

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity;->factoryDesk:Lcom/konka/factory/desk/IFactoryDesk;

    return-object v0
.end method

.method static synthetic access$400(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/WBAdjustViewHolder;
    .locals 1
    .param p0    # Lcom/konka/factory/MainmenuActivity;

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity;->wbViewHolder:Lcom/konka/factory/WBAdjustViewHolder;

    return-object v0
.end method

.method static synthetic access$402(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/WBAdjustViewHolder;)Lcom/konka/factory/WBAdjustViewHolder;
    .locals 0
    .param p0    # Lcom/konka/factory/MainmenuActivity;
    .param p1    # Lcom/konka/factory/WBAdjustViewHolder;

    iput-object p1, p0, Lcom/konka/factory/MainmenuActivity;->wbViewHolder:Lcom/konka/factory/WBAdjustViewHolder;

    return-object p1
.end method

.method static synthetic access$500(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/PictureModeAdjustViewHolder;
    .locals 1
    .param p0    # Lcom/konka/factory/MainmenuActivity;

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity;->picModeViewHolder:Lcom/konka/factory/PictureModeAdjustViewHolder;

    return-object v0
.end method

.method static synthetic access$502(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/PictureModeAdjustViewHolder;)Lcom/konka/factory/PictureModeAdjustViewHolder;
    .locals 0
    .param p0    # Lcom/konka/factory/MainmenuActivity;
    .param p1    # Lcom/konka/factory/PictureModeAdjustViewHolder;

    iput-object p1, p0, Lcom/konka/factory/MainmenuActivity;->picModeViewHolder:Lcom/konka/factory/PictureModeAdjustViewHolder;

    return-object p1
.end method

.method static synthetic access$600(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/SoundModeAdjustViewHolder;
    .locals 1
    .param p0    # Lcom/konka/factory/MainmenuActivity;

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity;->soundModeViewHolder:Lcom/konka/factory/SoundModeAdjustViewHolder;

    return-object v0
.end method

.method static synthetic access$602(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/SoundModeAdjustViewHolder;)Lcom/konka/factory/SoundModeAdjustViewHolder;
    .locals 0
    .param p0    # Lcom/konka/factory/MainmenuActivity;
    .param p1    # Lcom/konka/factory/SoundModeAdjustViewHolder;

    iput-object p1, p0, Lcom/konka/factory/MainmenuActivity;->soundModeViewHolder:Lcom/konka/factory/SoundModeAdjustViewHolder;

    return-object p1
.end method

.method static synthetic access$700(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/EnergyAjustViewHolder;
    .locals 1
    .param p0    # Lcom/konka/factory/MainmenuActivity;

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity;->energyViewHolder:Lcom/konka/factory/EnergyAjustViewHolder;

    return-object v0
.end method

.method static synthetic access$702(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/EnergyAjustViewHolder;)Lcom/konka/factory/EnergyAjustViewHolder;
    .locals 0
    .param p0    # Lcom/konka/factory/MainmenuActivity;
    .param p1    # Lcom/konka/factory/EnergyAjustViewHolder;

    iput-object p1, p0, Lcom/konka/factory/MainmenuActivity;->energyViewHolder:Lcom/konka/factory/EnergyAjustViewHolder;

    return-object p1
.end method

.method static synthetic access$800(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/NonStandardAdjustViewHolder;
    .locals 1
    .param p0    # Lcom/konka/factory/MainmenuActivity;

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity;->nonStandardViewHolder:Lcom/konka/factory/NonStandardAdjustViewHolder;

    return-object v0
.end method

.method static synthetic access$802(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/NonStandardAdjustViewHolder;)Lcom/konka/factory/NonStandardAdjustViewHolder;
    .locals 0
    .param p0    # Lcom/konka/factory/MainmenuActivity;
    .param p1    # Lcom/konka/factory/NonStandardAdjustViewHolder;

    iput-object p1, p0, Lcom/konka/factory/MainmenuActivity;->nonStandardViewHolder:Lcom/konka/factory/NonStandardAdjustViewHolder;

    return-object p1
.end method

.method static synthetic access$900(Lcom/konka/factory/MainmenuActivity;)Lcom/konka/factory/NonLinearAdjustViewHolder;
    .locals 1
    .param p0    # Lcom/konka/factory/MainmenuActivity;

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity;->nonLinearViewHolder:Lcom/konka/factory/NonLinearAdjustViewHolder;

    return-object v0
.end method

.method static synthetic access$902(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/NonLinearAdjustViewHolder;)Lcom/konka/factory/NonLinearAdjustViewHolder;
    .locals 0
    .param p0    # Lcom/konka/factory/MainmenuActivity;
    .param p1    # Lcom/konka/factory/NonLinearAdjustViewHolder;

    iput-object p1, p0, Lcom/konka/factory/MainmenuActivity;->nonLinearViewHolder:Lcom/konka/factory/NonLinearAdjustViewHolder;

    return-object p1
.end method

.method private intentPage()V
    .locals 3

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/konka/factory/MainmenuActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "android.settings.NETFACTORYAdjustViewHolder"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iput v2, p0, Lcom/konka/factory/MainmenuActivity;->currentPage:I

    iget-object v1, p0, Lcom/konka/factory/MainmenuActivity;->viewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v1, v2}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    new-instance v1, Lcom/konka/factory/NETFACTORYAdjustViewHolder;

    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity;->factoryDesk:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-direct {v1, p0, v2}, Lcom/konka/factory/NETFACTORYAdjustViewHolder;-><init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V

    iput-object v1, p0, Lcom/konka/factory/MainmenuActivity;->netfactoryViewHolder:Lcom/konka/factory/NETFACTORYAdjustViewHolder;

    iget-object v1, p0, Lcom/konka/factory/MainmenuActivity;->netfactoryViewHolder:Lcom/konka/factory/NETFACTORYAdjustViewHolder;

    invoke-virtual {v1}, Lcom/konka/factory/NETFACTORYAdjustViewHolder;->findView()V

    iget-object v1, p0, Lcom/konka/factory/MainmenuActivity;->netfactoryViewHolder:Lcom/konka/factory/NETFACTORYAdjustViewHolder;

    invoke-virtual {v1}, Lcom/konka/factory/NETFACTORYAdjustViewHolder;->onCreate()V

    :cond_0
    return-void
.end method

.method private registerListeners()V
    .locals 2

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity;->factoryViewHolder:Lcom/konka/factory/FactoryMenuViewHolder;

    iget-object v0, v0, Lcom/konka/factory/FactoryMenuViewHolder;->linear_factory_adc:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/factory/MainmenuActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity;->factoryViewHolder:Lcom/konka/factory/FactoryMenuViewHolder;

    iget-object v0, v0, Lcom/konka/factory/FactoryMenuViewHolder;->linear_factory_whitebalance:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/factory/MainmenuActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity;->factoryViewHolder:Lcom/konka/factory/FactoryMenuViewHolder;

    iget-object v0, v0, Lcom/konka/factory/FactoryMenuViewHolder;->linear_factory_picturemode:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/factory/MainmenuActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity;->factoryViewHolder:Lcom/konka/factory/FactoryMenuViewHolder;

    iget-object v0, v0, Lcom/konka/factory/FactoryMenuViewHolder;->linear_factory_soundmode:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/factory/MainmenuActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity;->factoryViewHolder:Lcom/konka/factory/FactoryMenuViewHolder;

    iget-object v0, v0, Lcom/konka/factory/FactoryMenuViewHolder;->linear_factory_nonstandard:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/factory/MainmenuActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity;->factoryViewHolder:Lcom/konka/factory/FactoryMenuViewHolder;

    iget-object v0, v0, Lcom/konka/factory/FactoryMenuViewHolder;->linear_factory_energy:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/factory/MainmenuActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity;->factoryViewHolder:Lcom/konka/factory/FactoryMenuViewHolder;

    iget-object v0, v0, Lcom/konka/factory/FactoryMenuViewHolder;->linear_factory_non_linear:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/factory/MainmenuActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity;->factoryViewHolder:Lcom/konka/factory/FactoryMenuViewHolder;

    iget-object v0, v0, Lcom/konka/factory/FactoryMenuViewHolder;->linear_factory_overscan:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/factory/MainmenuActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity;->factoryViewHolder:Lcom/konka/factory/FactoryMenuViewHolder;

    iget-object v0, v0, Lcom/konka/factory/FactoryMenuViewHolder;->linear_factory_ssc:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/factory/MainmenuActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity;->factoryViewHolder:Lcom/konka/factory/FactoryMenuViewHolder;

    iget-object v0, v0, Lcom/konka/factory/FactoryMenuViewHolder;->linear_factory_peq:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/factory/MainmenuActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity;->factoryViewHolder:Lcom/konka/factory/FactoryMenuViewHolder;

    iget-object v0, v0, Lcom/konka/factory/FactoryMenuViewHolder;->linear_factory_update:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/factory/MainmenuActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity;->factoryViewHolder:Lcom/konka/factory/FactoryMenuViewHolder;

    iget-object v0, v0, Lcom/konka/factory/FactoryMenuViewHolder;->linear_factory_netfactory:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/factory/MainmenuActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity;->factoryViewHolder:Lcom/konka/factory/FactoryMenuViewHolder;

    iget-object v0, v0, Lcom/konka/factory/FactoryMenuViewHolder;->linear_factory_defaultconfig:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/factory/MainmenuActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity;->factoryViewHolder:Lcom/konka/factory/FactoryMenuViewHolder;

    iget-object v0, v0, Lcom/konka/factory/FactoryMenuViewHolder;->linear_factory_serialprint:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/factory/MainmenuActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity;->factoryViewHolder:Lcom/konka/factory/FactoryMenuViewHolder;

    iget-object v0, v0, Lcom/konka/factory/FactoryMenuViewHolder;->linear_factory_otheroption:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/factory/MainmenuActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity;->factoryViewHolder:Lcom/konka/factory/FactoryMenuViewHolder;

    iget-object v0, v0, Lcom/konka/factory/FactoryMenuViewHolder;->linear_factory_customerspecialsettings:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/factory/MainmenuActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x0

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f030009    # com.konka.factory.R.layout.mainmenu

    invoke-virtual {p0, v1}, Lcom/konka/factory/MainmenuActivity;->setContentView(I)V

    const v1, 0x7f0a00a6    # com.konka.factory.R.id.factory_view_flipper

    invoke-virtual {p0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ViewFlipper;

    iput-object v1, p0, Lcom/konka/factory/MainmenuActivity;->viewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {p0}, Lcom/konka/factory/MainmenuActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "usb"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/usb/UsbManager;

    sput-object v1, Lcom/konka/factory/MainmenuActivity;->usbman:Landroid/hardware/usb/UsbManager;

    invoke-static {p0}, Lcom/konka/factory/desk/FactoryDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/factory/desk/FactoryDeskImpl;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/factory/MainmenuActivity;->factoryDesk:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {p0}, Lcom/konka/factory/desk/FactoryDB;->getInstance(Landroid/content/Context;)Lcom/konka/factory/desk/FactoryDB;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/factory/desk/FactoryDB;->openDB()V

    iget-object v1, p0, Lcom/konka/factory/MainmenuActivity;->factoryDesk:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v1}, Lcom/konka/factory/desk/IFactoryDesk;->loadEssentialDataFromDB()V

    new-instance v1, Lcom/konka/factory/FactoryMenuViewHolder;

    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity;->factoryDesk:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-direct {v1, p0, v2}, Lcom/konka/factory/FactoryMenuViewHolder;-><init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V

    iput-object v1, p0, Lcom/konka/factory/MainmenuActivity;->factoryViewHolder:Lcom/konka/factory/FactoryMenuViewHolder;

    iget-object v1, p0, Lcom/konka/factory/MainmenuActivity;->factoryViewHolder:Lcom/konka/factory/FactoryMenuViewHolder;

    invoke-virtual {v1}, Lcom/konka/factory/FactoryMenuViewHolder;->findView()V

    iget-object v1, p0, Lcom/konka/factory/MainmenuActivity;->factoryViewHolder:Lcom/konka/factory/FactoryMenuViewHolder;

    invoke-virtual {v1}, Lcom/konka/factory/FactoryMenuViewHolder;->oncreate()V

    invoke-direct {p0}, Lcom/konka/factory/MainmenuActivity;->registerListeners()V

    iput-boolean v3, p0, Lcom/konka/factory/MainmenuActivity;->isFirst:Z

    invoke-direct {p0}, Lcom/konka/factory/MainmenuActivity;->intentPage()V

    invoke-virtual {p0}, Lcom/konka/factory/MainmenuActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "nonstandard"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/konka/factory/MainmenuActivity;->viewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v1, v0}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    :cond_0
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v2, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/konka/factory/MainmenuActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v1

    iget v3, p0, Lcom/konka/factory/MainmenuActivity;->currentPage:I

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_0
    if-nez v0, :cond_1

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :cond_1
    move v2, v0

    :goto_1
    return v2

    :pswitch_0
    const v3, 0x7f0a009a    # com.konka.factory.R.id.linearlayout_factory_mountconfig

    if-eq v1, v3, :cond_2

    const v3, 0x7f0a009d    # com.konka.factory.R.id.linearlayout_factory_pqtableupdate

    if-eq v1, v3, :cond_2

    const v3, 0x7f0a0097    # com.konka.factory.R.id.linearlayout_factory_burninmode

    if-eq v1, v3, :cond_2

    const v3, 0x7f0a00a0    # com.konka.factory.R.id.linearlayout_factory_lockkeypad

    if-ne v1, v3, :cond_3

    :cond_2
    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity;->factoryViewHolder:Lcom/konka/factory/FactoryMenuViewHolder;

    invoke-virtual {v2, p1, p2}, Lcom/konka/factory/FactoryMenuViewHolder;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    :cond_3
    const v3, 0x7f0a007a    # com.konka.factory.R.id.linearlayout_factory_netfactory

    if-eq v1, v3, :cond_4

    const v3, 0x7f0a00a3    # com.konka.factory.R.id.linearlayout_factory_otheroption

    if-ne v1, v3, :cond_0

    :cond_4
    packed-switch p1, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    packed-switch v1, :pswitch_data_2

    goto :goto_0

    :pswitch_2
    const/16 v3, 0x13

    invoke-virtual {p0, v3}, Lcom/konka/factory/MainmenuActivity;->returnRoot(I)V

    goto :goto_1

    :pswitch_3
    packed-switch v1, :pswitch_data_3

    goto :goto_0

    :pswitch_4
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/konka/factory/MainmenuActivity;->returnRoot(I)V

    goto :goto_1

    :pswitch_5
    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity;->adcViewHolder:Lcom/konka/factory/ADCAdjustViewHolder;

    invoke-virtual {v2, p1, p2}, Lcom/konka/factory/ADCAdjustViewHolder;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    :pswitch_6
    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity;->wbViewHolder:Lcom/konka/factory/WBAdjustViewHolder;

    invoke-virtual {v2, p1, p2}, Lcom/konka/factory/WBAdjustViewHolder;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    :pswitch_7
    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity;->picModeViewHolder:Lcom/konka/factory/PictureModeAdjustViewHolder;

    invoke-virtual {v2, p1, p2}, Lcom/konka/factory/PictureModeAdjustViewHolder;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    :pswitch_8
    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity;->soundModeViewHolder:Lcom/konka/factory/SoundModeAdjustViewHolder;

    invoke-virtual {v2, p1, p2}, Lcom/konka/factory/SoundModeAdjustViewHolder;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    :pswitch_9
    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity;->energyViewHolder:Lcom/konka/factory/EnergyAjustViewHolder;

    invoke-virtual {v2, p1, p2}, Lcom/konka/factory/EnergyAjustViewHolder;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    :pswitch_a
    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity;->nonStandardViewHolder:Lcom/konka/factory/NonStandardAdjustViewHolder;

    invoke-virtual {v2, p1, p2}, Lcom/konka/factory/NonStandardAdjustViewHolder;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    :pswitch_b
    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity;->nonLinearViewHolder:Lcom/konka/factory/NonLinearAdjustViewHolder;

    invoke-virtual {v2, p1, p2}, Lcom/konka/factory/NonLinearAdjustViewHolder;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    :pswitch_c
    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity;->overScanViewHolder:Lcom/konka/factory/OverScanAdjustViewHolder;

    invoke-virtual {v2, p1, p2}, Lcom/konka/factory/OverScanAdjustViewHolder;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    :pswitch_d
    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity;->sscViewHolder:Lcom/konka/factory/SSCAdjustViewHolder;

    invoke-virtual {v2, p1, p2}, Lcom/konka/factory/SSCAdjustViewHolder;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_0

    :pswitch_e
    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity;->peqViewHolder:Lcom/konka/factory/PEQAdjustViewHolder;

    invoke-virtual {v2, p1, p2}, Lcom/konka/factory/PEQAdjustViewHolder;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_0

    :pswitch_f
    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity;->updateViewHolder:Lcom/konka/factory/UPDATEAdjustViewHolder;

    invoke-virtual {v2, p1, p2}, Lcom/konka/factory/UPDATEAdjustViewHolder;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_0

    :pswitch_10
    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity;->defaultconfigViewHolder:Lcom/konka/factory/DEFAULTCONFIGAdjustViewHolder;

    invoke-virtual {v2, p1, p2}, Lcom/konka/factory/DEFAULTCONFIGAdjustViewHolder;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_0

    :pswitch_11
    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity;->serialprintViewHolder:Lcom/konka/factory/SerialPrintAdjustViewHolder;

    invoke-virtual {v2, p1, p2}, Lcom/konka/factory/SerialPrintAdjustViewHolder;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_0

    :pswitch_12
    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity;->netfactoryViewHolder:Lcom/konka/factory/NETFACTORYAdjustViewHolder;

    invoke-virtual {v2, p1, p2}, Lcom/konka/factory/NETFACTORYAdjustViewHolder;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_0

    :pswitch_13
    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity;->otherOptionViewHolder:Lcom/konka/factory/OtherOptionAdjustViewHolder;

    invoke-virtual {v2, p1, p2}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_0

    :pswitch_14
    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity;->customerspecialsettingsViewHolder:Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;

    invoke-virtual {v2, p1, p2}, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_f
        :pswitch_14
        :pswitch_12
        :pswitch_10
        :pswitch_11
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_13
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x13
        :pswitch_1
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x7f0a007a
        :pswitch_2    # com.konka.factory.R.id.linearlayout_factory_netfactory
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x7f0a00a3
        :pswitch_4    # com.konka.factory.R.id.linearlayout_factory_otheroption
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    iget-object v1, p0, Lcom/konka/factory/MainmenuActivity;->factoryDesk:Lcom/konka/factory/desk/IFactoryDesk;

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/konka/factory/desk/FactoryDB;->getInstance(Landroid/content/Context;)Lcom/konka/factory/desk/FactoryDB;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/factory/desk/FactoryDB;->closeDB()V

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "factoryDirty"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/konka/factory/MainmenuActivity;->sendBroadcast(Landroid/content/Intent;)V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 2

    const/4 v1, 0x2

    iget v0, p0, Lcom/konka/factory/MainmenuActivity;->currentPage:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity;->viewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    new-instance v0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;

    iget-object v1, p0, Lcom/konka/factory/MainmenuActivity;->factoryDesk:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-direct {v0, p0, v1}, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;-><init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V

    iput-object v0, p0, Lcom/konka/factory/MainmenuActivity;->customerspecialsettingsViewHolder:Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity;->customerspecialsettingsViewHolder:Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;

    invoke-virtual {v0}, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->findView()V

    iget-object v0, p0, Lcom/konka/factory/MainmenuActivity;->customerspecialsettingsViewHolder:Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;

    invoke-virtual {v0}, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->OnCreate()V

    :cond_0
    iget-boolean v0, p0, Lcom/konka/factory/MainmenuActivity;->isFirst:Z

    if-nez v0, :cond_1

    invoke-static {p0}, Lcom/konka/factory/desk/FactoryDB;->getInstance(Landroid/content/Context;)Lcom/konka/factory/desk/FactoryDB;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/factory/desk/FactoryDB;->openDB()V

    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    return-void
.end method

.method protected onStop()V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.konka.tv.action.SYNCDB"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/konka/factory/MainmenuActivity;->sendBroadcast(Landroid/content/Intent;)V

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method public returnRoot(I)V
    .locals 5
    .param p1    # I

    const/4 v4, 0x1

    const/4 v3, 0x0

    iput v3, p0, Lcom/konka/factory/MainmenuActivity;->currentPage:I

    iget-object v2, p0, Lcom/konka/factory/MainmenuActivity;->viewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v2, v3}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    const v2, 0x7f0a0079    # com.konka.factory.R.id.linearlayout_factory_menu

    invoke-virtual {p0, v2}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->requestFocus()Z

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    return-void
.end method
