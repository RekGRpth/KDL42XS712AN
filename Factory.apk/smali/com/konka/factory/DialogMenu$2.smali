.class final Lcom/konka/factory/DialogMenu$2;
.super Landroid/os/Handler;
.source "DialogMenu.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/factory/DialogMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1    # Landroid/os/Message;

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget v0, p1, Landroid/os/Message;->what:I

    sget v1, Lcom/konka/factory/USBUpgradeThread;->UPGRATE_START:I

    if-ne v0, v1, :cond_0

    # invokes: Lcom/konka/factory/DialogMenu;->getProgressDialog()Landroid/app/ProgressDialog;
    invoke-static {}, Lcom/konka/factory/DialogMenu;->access$300()Landroid/app/ProgressDialog;

    move-result-object v0

    # setter for: Lcom/konka/factory/DialogMenu;->progressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/konka/factory/DialogMenu;->access$202(Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    # getter for: Lcom/konka/factory/DialogMenu;->progressDialog:Landroid/app/ProgressDialog;
    invoke-static {}, Lcom/konka/factory/DialogMenu;->access$200()Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    :goto_0
    return-void

    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    sget v1, Lcom/konka/factory/USBUpgradeThread;->UPGRATE_END_SUCCESS:I

    if-ne v0, v1, :cond_1

    # getter for: Lcom/konka/factory/DialogMenu;->progressDialog:Landroid/app/ProgressDialog;
    invoke-static {}, Lcom/konka/factory/DialogMenu;->access$200()Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    sget-object v0, Lcom/konka/factory/DialogMenu;->textview_dialog_hint:Landroid/widget/TextView;

    # getter for: Lcom/konka/factory/DialogMenu;->upgradeStatus:[Ljava/lang/String;
    invoke-static {}, Lcom/konka/factory/DialogMenu;->access$400()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    sget v1, Lcom/konka/factory/USBUpgradeThread;->UPGRATE_END_FILE_NOT_FOUND:I

    if-ne v0, v1, :cond_2

    # getter for: Lcom/konka/factory/DialogMenu;->progressDialog:Landroid/app/ProgressDialog;
    invoke-static {}, Lcom/konka/factory/DialogMenu;->access$200()Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    sget-object v0, Lcom/konka/factory/DialogMenu;->textview_dialog_hint:Landroid/widget/TextView;

    # getter for: Lcom/konka/factory/DialogMenu;->upgradeStatus:[Ljava/lang/String;
    invoke-static {}, Lcom/konka/factory/DialogMenu;->access$400()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    # getter for: Lcom/konka/factory/DialogMenu;->progressDialog:Landroid/app/ProgressDialog;
    invoke-static {}, Lcom/konka/factory/DialogMenu;->access$200()Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    sget-object v0, Lcom/konka/factory/DialogMenu;->textview_dialog_hint:Landroid/widget/TextView;

    # getter for: Lcom/konka/factory/DialogMenu;->upgradeStatus:[Ljava/lang/String;
    invoke-static {}, Lcom/konka/factory/DialogMenu;->access$400()[Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
