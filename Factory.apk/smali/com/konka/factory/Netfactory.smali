.class public Lcom/konka/factory/Netfactory;
.super Ljava/lang/Object;
.source "Netfactory.java"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "netfactory"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native CheckVersion()I
.end method

.method public static native CloseNetwork()I
.end method

.method public static native GetData()I
.end method

.method public static native GetHdcp()[B
.end method

.method public static native GetMac()[B
.end method

.method public static native GetSN()Ljava/lang/String;
.end method

.method public static native InitNetwork()I
.end method

.method public static native SetTVInfo(Ljava/lang/String;[BJ)I
.end method
