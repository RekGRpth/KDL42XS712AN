.class public final enum Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;
.super Ljava/lang/Enum;
.source "UPDATEAdjustViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/factory/UPDATEAdjustViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UpgradeMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;

.field public static final enum UPGRADE_MODE_NET:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;

.field public static final enum UPGRADE_MODE_USB:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;

    const-string v1, "UPGRADE_MODE_NET"

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;->UPGRADE_MODE_NET:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;

    new-instance v0, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;

    const-string v1, "UPGRADE_MODE_USB"

    invoke-direct {v0, v1, v3}, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;->UPGRADE_MODE_USB:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;

    sget-object v1, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;->UPGRADE_MODE_NET:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;->UPGRADE_MODE_USB:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;

    aput-object v1, v0, v3

    sput-object v0, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;->$VALUES:[Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;
    .locals 1

    const-class v0, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;

    return-object v0
.end method

.method public static values()[Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;
    .locals 1

    sget-object v0, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;->$VALUES:[Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;

    invoke-virtual {v0}, [Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;

    return-object v0
.end method
