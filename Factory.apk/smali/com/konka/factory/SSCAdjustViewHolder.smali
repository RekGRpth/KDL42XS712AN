.class public Lcom/konka/factory/SSCAdjustViewHolder;
.super Ljava/lang/Object;
.source "SSCAdjustViewHolder.java"


# instance fields
.field private dis:D

.field private factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

.field private lvdsModDouble:D

.field private lvdsPerDouble:D

.field private lvdsenable:[Ljava/lang/String;

.field private lvdsenableindex:I

.field private lvdsmodulation:I

.field private lvdspercengtage:I

.field private miuenable:[Ljava/lang/String;

.field private miuenableindex:I

.field private miumodulation:I

.field private miupercentage:I

.field private sscActivity:Lcom/konka/factory/MainmenuActivity;

.field protected text_factory_ssc_lvdsenable_val:Landroid/widget/TextView;

.field protected text_factory_ssc_lvdsmodulation_val:Landroid/widget/TextView;

.field protected text_factory_ssc_lvdspercentage_val:Landroid/widget/TextView;

.field protected text_factory_ssc_miuenable_val:Landroid/widget/TextView;

.field protected text_factory_ssc_miumodulation_val:Landroid/widget/TextView;

.field protected text_factory_ssc_miupercentage_val:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V
    .locals 5
    .param p1    # Lcom/konka/factory/MainmenuActivity;
    .param p2    # Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "Off"

    aput-object v1, v0, v2

    const-string v1, "On"

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miuenable:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "Off"

    aput-object v1, v0, v2

    const-string v1, "On"

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsenable:[Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/factory/SSCAdjustViewHolder;->sscActivity:Lcom/konka/factory/MainmenuActivity;

    iput-object p2, p0, Lcom/konka/factory/SSCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    return-void
.end method


# virtual methods
.method findView()V
    .locals 2

    iget-object v0, p0, Lcom/konka/factory/SSCAdjustViewHolder;->sscActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0216    # com.konka.factory.R.id.textview_factory_ssc_miuenable_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/SSCAdjustViewHolder;->text_factory_ssc_miuenable_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/SSCAdjustViewHolder;->sscActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0219    # com.konka.factory.R.id.textview_factory_ssc_miumodulation_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/SSCAdjustViewHolder;->text_factory_ssc_miumodulation_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/SSCAdjustViewHolder;->sscActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a021c    # com.konka.factory.R.id.textview_factory_ssc_miupercentage_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/SSCAdjustViewHolder;->text_factory_ssc_miupercentage_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/SSCAdjustViewHolder;->sscActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a021f    # com.konka.factory.R.id.textview_factory_ssc_lvdsenable_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/SSCAdjustViewHolder;->text_factory_ssc_lvdsenable_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/SSCAdjustViewHolder;->sscActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0222    # com.konka.factory.R.id.textview_factory_ssc_lvdsmodulation_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/SSCAdjustViewHolder;->text_factory_ssc_lvdsmodulation_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/SSCAdjustViewHolder;->sscActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0225    # com.konka.factory.R.id.textview_factory_ssc_lvdspercentage_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/SSCAdjustViewHolder;->text_factory_ssc_lvdspercentage_val:Landroid/widget/TextView;

    return-void
.end method

.method public onCreate()Z
    .locals 6

    const/4 v2, 0x0

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/konka/factory/SSCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getMIUenalbe()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput v0, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miuenableindex:I

    iget-object v0, p0, Lcom/konka/factory/SSCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getLVDSenalbe()Z

    move-result v0

    if-eqz v0, :cond_0

    move v2, v1

    :cond_0
    iput v2, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsenableindex:I

    iget-object v0, p0, Lcom/konka/factory/SSCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getMIUpercentage()I

    move-result v0

    iput v0, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miupercentage:I

    iget v0, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miupercentage:I

    int-to-double v2, v0

    div-double/2addr v2, v4

    iput-wide v2, p0, Lcom/konka/factory/SSCAdjustViewHolder;->dis:D

    iget-object v0, p0, Lcom/konka/factory/SSCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getLVDSpercentage()I

    move-result v0

    iput v0, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdspercengtage:I

    iget-object v0, p0, Lcom/konka/factory/SSCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getMIUmodulation()I

    move-result v0

    iput v0, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miumodulation:I

    iget-object v0, p0, Lcom/konka/factory/SSCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getLVDSmodulation()I

    move-result v0

    iput v0, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsmodulation:I

    iget v0, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsmodulation:I

    int-to-double v2, v0

    div-double/2addr v2, v4

    iput-wide v2, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsModDouble:D

    iget v0, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdspercengtage:I

    int-to-double v2, v0

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    div-double/2addr v2, v4

    iput-wide v2, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsPerDouble:D

    iget-object v0, p0, Lcom/konka/factory/SSCAdjustViewHolder;->text_factory_ssc_miuenable_val:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miuenable:[Ljava/lang/String;

    iget v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miuenableindex:I

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/SSCAdjustViewHolder;->text_factory_ssc_lvdsenable_val:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miuenable:[Ljava/lang/String;

    iget v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsenableindex:I

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/SSCAdjustViewHolder;->text_factory_ssc_lvdspercentage_val:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsPerDouble:D

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/SSCAdjustViewHolder;->text_factory_ssc_miupercentage_val:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->dis:D

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/SSCAdjustViewHolder;->text_factory_ssc_miumodulation_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miumodulation:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/SSCAdjustViewHolder;->text_factory_ssc_lvdsmodulation_val:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsModDouble:D

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return v1

    :cond_1
    move v0, v2

    goto/16 :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 11
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v10, 0xa

    const-wide/high16 v8, 0x4059000000000000L    # 100.0

    const-wide/high16 v6, 0x4024000000000000L    # 10.0

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v0, 0x1

    iget-object v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->sscActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v3}, Lcom/konka/factory/MainmenuActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v1

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2}, Ljava/lang/String;-><init>()V

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    sparse-switch v1, :sswitch_data_1

    goto :goto_0

    :sswitch_1
    iget v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miuenableindex:I

    if-ne v3, v4, :cond_0

    iput v5, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miuenableindex:I

    iget-object v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v3, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setMIUenable(Z)Z

    :goto_1
    iget-object v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->text_factory_ssc_miuenable_val:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miuenable:[Ljava/lang/String;

    iget v5, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miuenableindex:I

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    iget v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miuenableindex:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miuenableindex:I

    iget-object v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setMIUenable(Z)Z

    goto :goto_1

    :sswitch_2
    iget v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miumodulation:I

    const/16 v4, 0x28

    if-lt v3, v4, :cond_1

    const/16 v3, 0x14

    iput v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miumodulation:I

    :goto_2
    iget-object v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->text_factory_ssc_miumodulation_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miumodulation:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miumodulation:I

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setMIUmodulation(I)Z

    goto :goto_0

    :cond_1
    iget v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miumodulation:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miumodulation:I

    goto :goto_2

    :sswitch_3
    iget v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miupercentage:I

    if-lt v3, v10, :cond_2

    iput v5, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miupercentage:I

    :goto_3
    iget v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miupercentage:I

    int-to-double v3, v3

    div-double/2addr v3, v6

    iput-wide v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->dis:D

    iget-object v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->text_factory_ssc_miupercentage_val:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, p0, Lcom/konka/factory/SSCAdjustViewHolder;->dis:D

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miupercentage:I

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setMIUpercentage(I)Z

    goto :goto_0

    :cond_2
    iget v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miupercentage:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miupercentage:I

    goto :goto_3

    :sswitch_4
    iget v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsenableindex:I

    if-ne v3, v4, :cond_3

    iput v5, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsenableindex:I

    iget-object v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v3, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setLVDSenable(Z)Z

    :goto_4
    iget-object v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->text_factory_ssc_lvdsenable_val:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsenable:[Ljava/lang/String;

    iget v5, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsenableindex:I

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_3
    iget v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsenableindex:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsenableindex:I

    iget-object v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setLVDSenable(Z)Z

    goto :goto_4

    :sswitch_5
    iget v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsmodulation:I

    const/16 v4, 0x190

    if-lt v3, v4, :cond_4

    const/16 v3, 0xc8

    iput v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsmodulation:I

    :goto_5
    iget v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsmodulation:I

    int-to-double v3, v3

    div-double/2addr v3, v6

    iput-wide v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsModDouble:D

    iget-object v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->text_factory_ssc_lvdsmodulation_val:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsModDouble:D

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsmodulation:I

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setLVDSmodulation(I)Z

    goto/16 :goto_0

    :cond_4
    iget v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsmodulation:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsmodulation:I

    goto :goto_5

    :sswitch_6
    iget v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdspercengtage:I

    const/16 v4, 0x12c

    if-lt v3, v4, :cond_5

    iput v5, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdspercengtage:I

    :goto_6
    iget v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdspercengtage:I

    int-to-double v3, v3

    div-double/2addr v3, v8

    iput-wide v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsPerDouble:D

    iget-object v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->text_factory_ssc_lvdspercentage_val:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsPerDouble:D

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdspercengtage:I

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setLVDSpercentage(I)Z

    goto/16 :goto_0

    :cond_5
    iget v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdspercengtage:I

    add-int/lit8 v3, v3, 0xa

    iput v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdspercengtage:I

    goto :goto_6

    :sswitch_7
    sparse-switch v1, :sswitch_data_2

    goto/16 :goto_0

    :sswitch_8
    iget v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miuenableindex:I

    if-nez v3, :cond_6

    iput v4, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miuenableindex:I

    iget-object v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setMIUenable(Z)Z

    :goto_7
    iget-object v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->text_factory_ssc_miuenable_val:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miuenable:[Ljava/lang/String;

    iget v5, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miuenableindex:I

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_6
    iget v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miuenableindex:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miuenableindex:I

    iget-object v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v3, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setMIUenable(Z)Z

    goto :goto_7

    :sswitch_9
    iget v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miumodulation:I

    const/16 v4, 0x14

    if-gt v3, v4, :cond_7

    const/16 v3, 0x28

    iput v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miumodulation:I

    :goto_8
    iget-object v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->text_factory_ssc_miumodulation_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miumodulation:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miumodulation:I

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setMIUmodulation(I)Z

    goto/16 :goto_0

    :cond_7
    iget v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miumodulation:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miumodulation:I

    goto :goto_8

    :sswitch_a
    iget v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miupercentage:I

    if-gtz v3, :cond_8

    iput v10, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miupercentage:I

    :goto_9
    iget v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miupercentage:I

    int-to-double v3, v3

    div-double/2addr v3, v6

    iput-wide v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->dis:D

    iget-object v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->text_factory_ssc_miupercentage_val:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, p0, Lcom/konka/factory/SSCAdjustViewHolder;->dis:D

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miupercentage:I

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setMIUpercentage(I)Z

    goto/16 :goto_0

    :cond_8
    iget v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miupercentage:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->miupercentage:I

    goto :goto_9

    :sswitch_b
    iget v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsenableindex:I

    if-nez v3, :cond_9

    iput v4, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsenableindex:I

    iget-object v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setLVDSenable(Z)Z

    :goto_a
    iget-object v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->text_factory_ssc_lvdsenable_val:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsenable:[Ljava/lang/String;

    iget v5, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsenableindex:I

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_9
    iget v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsenableindex:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsenableindex:I

    iget-object v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v3, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setLVDSenable(Z)Z

    goto :goto_a

    :sswitch_c
    iget v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsmodulation:I

    const/16 v4, 0xc8

    if-gt v3, v4, :cond_a

    const/16 v3, 0x190

    iput v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsmodulation:I

    :goto_b
    iget v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsmodulation:I

    int-to-double v3, v3

    div-double/2addr v3, v6

    iput-wide v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsModDouble:D

    iget-object v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->text_factory_ssc_lvdsmodulation_val:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsModDouble:D

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsmodulation:I

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setLVDSmodulation(I)Z

    goto/16 :goto_0

    :cond_a
    iget v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsmodulation:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsmodulation:I

    goto :goto_b

    :sswitch_d
    iget v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdspercengtage:I

    if-gtz v3, :cond_b

    const/16 v3, 0x12c

    iput v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdspercengtage:I

    :goto_c
    iget v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdspercengtage:I

    int-to-double v3, v3

    div-double/2addr v3, v8

    iput-wide v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsPerDouble:D

    iget-object v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->text_factory_ssc_lvdspercentage_val:Landroid/widget/TextView;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdsPerDouble:D

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdspercengtage:I

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setLVDSpercentage(I)Z

    goto/16 :goto_0

    :cond_b
    iget v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdspercengtage:I

    add-int/lit8 v3, v3, -0xa

    iput v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->lvdspercengtage:I

    goto :goto_c

    :sswitch_e
    iget-object v3, p0, Lcom/konka/factory/SSCAdjustViewHolder;->sscActivity:Lcom/konka/factory/MainmenuActivity;

    const/16 v4, 0xd

    invoke-virtual {v3, v4}, Lcom/konka/factory/MainmenuActivity;->returnRoot(I)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_e
        0x15 -> :sswitch_7
        0x16 -> :sswitch_0
        0x52 -> :sswitch_e
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x7f0a0214 -> :sswitch_1    # com.konka.factory.R.id.linearlayout_factory_ssc_miuenable
        0x7f0a0217 -> :sswitch_2    # com.konka.factory.R.id.linearlayout_factory_ssc_miumodulation
        0x7f0a021a -> :sswitch_3    # com.konka.factory.R.id.linearlayout_factory_ssc_miupercentage
        0x7f0a021d -> :sswitch_4    # com.konka.factory.R.id.linearlayout_factory_ssc_lvdsenable
        0x7f0a0220 -> :sswitch_5    # com.konka.factory.R.id.linearlayout_factory_ssc_lvdsmodulation
        0x7f0a0223 -> :sswitch_6    # com.konka.factory.R.id.linearlayout_factory_ssc_lvdspercentage
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x7f0a0214 -> :sswitch_8    # com.konka.factory.R.id.linearlayout_factory_ssc_miuenable
        0x7f0a0217 -> :sswitch_9    # com.konka.factory.R.id.linearlayout_factory_ssc_miumodulation
        0x7f0a021a -> :sswitch_a    # com.konka.factory.R.id.linearlayout_factory_ssc_miupercentage
        0x7f0a021d -> :sswitch_b    # com.konka.factory.R.id.linearlayout_factory_ssc_lvdsenable
        0x7f0a0220 -> :sswitch_c    # com.konka.factory.R.id.linearlayout_factory_ssc_lvdsmodulation
        0x7f0a0223 -> :sswitch_d    # com.konka.factory.R.id.linearlayout_factory_ssc_lvdspercentage
    .end sparse-switch
.end method
