.class public Lcom/konka/factory/ADCAdjustViewHolder;
.super Ljava/lang/Object;
.source "ADCAdjustViewHolder.java"


# instance fields
.field private adcActivity:Lcom/konka/factory/MainmenuActivity;

.field private bgainvalADC:I

.field private boffsetvalADC:I

.field private factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

.field private ggainvalADC:I

.field private goffsetvalADC:I

.field protected handler:Landroid/os/Handler;

.field private phasevalADC:I

.field private progressDialog:Landroid/app/ProgressDialog;

.field private rgainvalADC:I

.field private roffsetvalADC:I

.field private sourcearrayADC:[Ljava/lang/String;

.field private sourceindexADC:I

.field protected text_factory_adc_bgain_val:Landroid/widget/TextView;

.field protected text_factory_adc_boffset_val:Landroid/widget/TextView;

.field protected text_factory_adc_ggain_val:Landroid/widget/TextView;

.field protected text_factory_adc_goffset_val:Landroid/widget/TextView;

.field protected text_factory_adc_phase_val:Landroid/widget/TextView;

.field protected text_factory_adc_rgain_val:Landroid/widget/TextView;

.field protected text_factory_adc_roffset_val:Landroid/widget/TextView;

.field protected text_factory_adc_source_val:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V
    .locals 5
    .param p1    # Lcom/konka/factory/MainmenuActivity;
    .param p2    # Lcom/konka/factory/desk/IFactoryDesk;

    const/16 v1, 0x7ff

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/16 v0, 0xfff

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v2, p0, Lcom/konka/factory/ADCAdjustViewHolder;->sourceindexADC:I

    iput v0, p0, Lcom/konka/factory/ADCAdjustViewHolder;->rgainvalADC:I

    iput v0, p0, Lcom/konka/factory/ADCAdjustViewHolder;->ggainvalADC:I

    iput v0, p0, Lcom/konka/factory/ADCAdjustViewHolder;->bgainvalADC:I

    iput v1, p0, Lcom/konka/factory/ADCAdjustViewHolder;->roffsetvalADC:I

    const/16 v0, 0xff

    iput v0, p0, Lcom/konka/factory/ADCAdjustViewHolder;->goffsetvalADC:I

    iput v1, p0, Lcom/konka/factory/ADCAdjustViewHolder;->boffsetvalADC:I

    const/16 v0, 0x32

    iput v0, p0, Lcom/konka/factory/ADCAdjustViewHolder;->phasevalADC:I

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "VGA"

    aput-object v1, v0, v2

    const-string v1, "YPbPr(SD)"

    aput-object v1, v0, v3

    const-string v1, "YPbPr(HD)"

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder;->sourcearrayADC:[Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder;->progressDialog:Landroid/app/ProgressDialog;

    new-instance v0, Lcom/konka/factory/ADCAdjustViewHolder$1;

    invoke-direct {v0, p0}, Lcom/konka/factory/ADCAdjustViewHolder$1;-><init>(Lcom/konka/factory/ADCAdjustViewHolder;)V

    iput-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder;->handler:Landroid/os/Handler;

    iput-object p1, p0, Lcom/konka/factory/ADCAdjustViewHolder;->adcActivity:Lcom/konka/factory/MainmenuActivity;

    iput-object p2, p0, Lcom/konka/factory/ADCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {p2}, Lcom/konka/factory/desk/IFactoryDesk;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_VGA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v0, v1, :cond_0

    iput v4, p0, Lcom/konka/factory/ADCAdjustViewHolder;->sourceindexADC:I

    :goto_0
    iget-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder;->handler:Landroid/os/Handler;

    invoke-interface {p2, v0, v3}, Lcom/konka/factory/desk/IFactoryDesk;->setHandler(Landroid/os/Handler;I)Z

    return-void

    :cond_0
    iput v2, p0, Lcom/konka/factory/ADCAdjustViewHolder;->sourceindexADC:I

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/konka/factory/ADCAdjustViewHolder;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0    # Lcom/konka/factory/ADCAdjustViewHolder;

    iget-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder;->progressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$002(Lcom/konka/factory/ADCAdjustViewHolder;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0    # Lcom/konka/factory/ADCAdjustViewHolder;
    .param p1    # Landroid/app/ProgressDialog;

    iput-object p1, p0, Lcom/konka/factory/ADCAdjustViewHolder;->progressDialog:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$100(Lcom/konka/factory/ADCAdjustViewHolder;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0    # Lcom/konka/factory/ADCAdjustViewHolder;

    invoke-direct {p0}, Lcom/konka/factory/ADCAdjustViewHolder;->getProgressDialog()Landroid/app/ProgressDialog;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/konka/factory/ADCAdjustViewHolder;)Lcom/konka/factory/MainmenuActivity;
    .locals 1
    .param p0    # Lcom/konka/factory/ADCAdjustViewHolder;

    iget-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder;->adcActivity:Lcom/konka/factory/MainmenuActivity;

    return-object v0
.end method

.method private getProgressDialog()Landroid/app/ProgressDialog;
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder;->progressDialog:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/konka/factory/ADCAdjustViewHolder;->adcActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder;->progressDialog:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder;->progressDialog:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/konka/factory/ADCAdjustViewHolder;->adcActivity:Lcom/konka/factory/MainmenuActivity;

    const v2, 0x7f08000b    # com.konka.factory.R.string.str_factory_adc_autoadjust_val

    invoke-virtual {v1, v2}, Lcom/konka/factory/MainmenuActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    iget-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    :cond_0
    iget-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder;->progressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method


# virtual methods
.method findView()V
    .locals 2

    iget-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder;->adcActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0002    # com.konka.factory.R.id.textview_factory_adc_source_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder;->text_factory_adc_source_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder;->adcActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0005    # com.konka.factory.R.id.textview_factory_adc_rgain_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder;->text_factory_adc_rgain_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder;->adcActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0008    # com.konka.factory.R.id.textview_factory_adc_ggain_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder;->text_factory_adc_ggain_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder;->adcActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a000b    # com.konka.factory.R.id.textview_factory_adc_bgain_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder;->text_factory_adc_bgain_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder;->adcActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a000e    # com.konka.factory.R.id.textview_factory_adc_roffset_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder;->text_factory_adc_roffset_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder;->adcActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0011    # com.konka.factory.R.id.textview_factory_adc_goffset_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder;->text_factory_adc_goffset_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder;->adcActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0014    # com.konka.factory.R.id.textview_factory_adc_boffset_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder;->text_factory_adc_boffset_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder;->adcActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0017    # com.konka.factory.R.id.textview_factory_adc_phase_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder;->text_factory_adc_phase_val:Landroid/widget/TextView;

    return-void
.end method

.method public onCreate()Z
    .locals 4

    iget-object v1, p0, Lcom/konka/factory/ADCAdjustViewHolder;->adcActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-static {v1}, Lcom/konka/factory/desk/FactoryDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/factory/desk/FactoryDeskImpl;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/factory/ADCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v1}, Lcom/konka/factory/desk/IFactoryDesk;->getAdcIdx()Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/factory/desk/FactoryDeskImpl;->loadCurAdcDataFromDB(I)V

    iget-object v1, p0, Lcom/konka/factory/ADCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v1}, Lcom/konka/factory/desk/IFactoryDesk;->getADCRedGain()I

    move-result v1

    iput v1, p0, Lcom/konka/factory/ADCAdjustViewHolder;->rgainvalADC:I

    iget-object v1, p0, Lcom/konka/factory/ADCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v1}, Lcom/konka/factory/desk/IFactoryDesk;->getADCGreenGain()I

    move-result v1

    iput v1, p0, Lcom/konka/factory/ADCAdjustViewHolder;->ggainvalADC:I

    iget-object v1, p0, Lcom/konka/factory/ADCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v1}, Lcom/konka/factory/desk/IFactoryDesk;->getADCBlueGain()I

    move-result v1

    iput v1, p0, Lcom/konka/factory/ADCAdjustViewHolder;->bgainvalADC:I

    iget-object v1, p0, Lcom/konka/factory/ADCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v1}, Lcom/konka/factory/desk/IFactoryDesk;->getADCRedOffset()I

    move-result v1

    iput v1, p0, Lcom/konka/factory/ADCAdjustViewHolder;->roffsetvalADC:I

    iget-object v1, p0, Lcom/konka/factory/ADCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v1}, Lcom/konka/factory/desk/IFactoryDesk;->getADCGreenOffset()I

    move-result v1

    iput v1, p0, Lcom/konka/factory/ADCAdjustViewHolder;->goffsetvalADC:I

    iget-object v1, p0, Lcom/konka/factory/ADCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v1}, Lcom/konka/factory/desk/IFactoryDesk;->getADCBlueOffset()I

    move-result v1

    iput v1, p0, Lcom/konka/factory/ADCAdjustViewHolder;->boffsetvalADC:I

    iget-object v1, p0, Lcom/konka/factory/ADCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v1}, Lcom/konka/factory/desk/IFactoryDesk;->getADCPhase()I

    move-result v1

    iput v1, p0, Lcom/konka/factory/ADCAdjustViewHolder;->phasevalADC:I

    iget-object v1, p0, Lcom/konka/factory/ADCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v1}, Lcom/konka/factory/desk/IFactoryDesk;->getAdcIdx()Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;->ordinal()I

    move-result v1

    iput v1, p0, Lcom/konka/factory/ADCAdjustViewHolder;->sourceindexADC:I

    iget-object v1, p0, Lcom/konka/factory/ADCAdjustViewHolder;->text_factory_adc_rgain_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/ADCAdjustViewHolder;->rgainvalADC:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/ADCAdjustViewHolder;->text_factory_adc_ggain_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/ADCAdjustViewHolder;->ggainvalADC:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/ADCAdjustViewHolder;->text_factory_adc_bgain_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/ADCAdjustViewHolder;->bgainvalADC:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/ADCAdjustViewHolder;->text_factory_adc_roffset_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/ADCAdjustViewHolder;->roffsetvalADC:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/ADCAdjustViewHolder;->text_factory_adc_goffset_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/ADCAdjustViewHolder;->goffsetvalADC:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/ADCAdjustViewHolder;->text_factory_adc_boffset_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/ADCAdjustViewHolder;->boffsetvalADC:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/ADCAdjustViewHolder;->text_factory_adc_phase_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/ADCAdjustViewHolder;->phasevalADC:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/ADCAdjustViewHolder;->text_factory_adc_source_val:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/factory/ADCAdjustViewHolder;->sourcearrayADC:[Ljava/lang/String;

    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->sourceindexADC:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v1, 0x1

    return v1
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 9
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v8, 0xff

    const/4 v7, 0x2

    const/16 v6, 0xfff

    const/16 v4, 0x1fff

    const/4 v5, 0x0

    const/4 v0, 0x1

    iget-object v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->adcActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v3}, Lcom/konka/factory/MainmenuActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v1

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2}, Ljava/lang/String;-><init>()V

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    :sswitch_0
    sparse-switch v1, :sswitch_data_1

    goto :goto_0

    :sswitch_1
    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->sourceindexADC:I

    if-lt v3, v7, :cond_1

    iput v5, p0, Lcom/konka/factory/ADCAdjustViewHolder;->sourceindexADC:I

    :goto_1
    iget-object v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;->values()[Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;

    move-result-object v4

    iget v5, p0, Lcom/konka/factory/ADCAdjustViewHolder;->sourceindexADC:I

    aget-object v4, v4, v5

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAdcIdx(Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;)Z

    invoke-virtual {p0}, Lcom/konka/factory/ADCAdjustViewHolder;->onCreate()Z

    goto :goto_0

    :cond_1
    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->sourceindexADC:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->sourceindexADC:I

    goto :goto_1

    :sswitch_2
    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->rgainvalADC:I

    if-eq v3, v4, :cond_2

    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->rgainvalADC:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->rgainvalADC:I

    :goto_2
    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->rgainvalADC:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->text_factory_adc_rgain_val:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/ADCAdjustViewHolder;->rgainvalADC:I

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setADCRedGain(I)Z

    goto :goto_0

    :cond_2
    iput v5, p0, Lcom/konka/factory/ADCAdjustViewHolder;->rgainvalADC:I

    goto :goto_2

    :sswitch_3
    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->ggainvalADC:I

    if-eq v3, v4, :cond_3

    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->ggainvalADC:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->ggainvalADC:I

    :goto_3
    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->ggainvalADC:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->text_factory_adc_ggain_val:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/ADCAdjustViewHolder;->ggainvalADC:I

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setADCGreenGain(I)Z

    goto :goto_0

    :cond_3
    iput v5, p0, Lcom/konka/factory/ADCAdjustViewHolder;->ggainvalADC:I

    goto :goto_3

    :sswitch_4
    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->bgainvalADC:I

    if-eq v3, v4, :cond_4

    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->bgainvalADC:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->bgainvalADC:I

    :goto_4
    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->bgainvalADC:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->text_factory_adc_bgain_val:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/ADCAdjustViewHolder;->bgainvalADC:I

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setADCBlueGain(I)Z

    goto :goto_0

    :cond_4
    iput v5, p0, Lcom/konka/factory/ADCAdjustViewHolder;->bgainvalADC:I

    goto :goto_4

    :sswitch_5
    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->roffsetvalADC:I

    if-eq v3, v6, :cond_5

    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->roffsetvalADC:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->roffsetvalADC:I

    :goto_5
    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->roffsetvalADC:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->text_factory_adc_roffset_val:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/ADCAdjustViewHolder;->roffsetvalADC:I

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setADCRedOffset(I)Z

    goto/16 :goto_0

    :cond_5
    iput v5, p0, Lcom/konka/factory/ADCAdjustViewHolder;->roffsetvalADC:I

    goto :goto_5

    :sswitch_6
    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->goffsetvalADC:I

    const/16 v4, 0x1ff

    if-eq v3, v4, :cond_6

    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->goffsetvalADC:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->goffsetvalADC:I

    :goto_6
    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->goffsetvalADC:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->text_factory_adc_goffset_val:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/ADCAdjustViewHolder;->goffsetvalADC:I

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setADCGreenOffset(I)Z

    goto/16 :goto_0

    :cond_6
    iput v5, p0, Lcom/konka/factory/ADCAdjustViewHolder;->goffsetvalADC:I

    goto :goto_6

    :sswitch_7
    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->boffsetvalADC:I

    if-eq v3, v6, :cond_7

    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->boffsetvalADC:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->boffsetvalADC:I

    :goto_7
    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->boffsetvalADC:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->text_factory_adc_boffset_val:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/ADCAdjustViewHolder;->boffsetvalADC:I

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setADCBlueOffset(I)Z

    goto/16 :goto_0

    :cond_7
    iput v5, p0, Lcom/konka/factory/ADCAdjustViewHolder;->boffsetvalADC:I

    goto :goto_7

    :sswitch_8
    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->phasevalADC:I

    if-eq v3, v8, :cond_8

    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->phasevalADC:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->phasevalADC:I

    :goto_8
    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->phasevalADC:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->text_factory_adc_phase_val:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/ADCAdjustViewHolder;->phasevalADC:I

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setADCPhase(I)Z

    goto/16 :goto_0

    :cond_8
    iput v5, p0, Lcom/konka/factory/ADCAdjustViewHolder;->phasevalADC:I

    goto :goto_8

    :sswitch_9
    sparse-switch v1, :sswitch_data_2

    goto/16 :goto_0

    :sswitch_a
    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->sourceindexADC:I

    if-nez v3, :cond_9

    iput v7, p0, Lcom/konka/factory/ADCAdjustViewHolder;->sourceindexADC:I

    :goto_9
    iget-object v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;->values()[Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;

    move-result-object v4

    iget v5, p0, Lcom/konka/factory/ADCAdjustViewHolder;->sourceindexADC:I

    aget-object v4, v4, v5

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAdcIdx(Lcom/konka/factory/desk/IFactoryDesk$E_ADC_SET_INDEX;)Z

    invoke-virtual {p0}, Lcom/konka/factory/ADCAdjustViewHolder;->onCreate()Z

    goto/16 :goto_0

    :cond_9
    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->sourceindexADC:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->sourceindexADC:I

    goto :goto_9

    :sswitch_b
    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->rgainvalADC:I

    if-ltz v3, :cond_a

    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->rgainvalADC:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->rgainvalADC:I

    :goto_a
    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->rgainvalADC:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->text_factory_adc_rgain_val:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/ADCAdjustViewHolder;->rgainvalADC:I

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setADCRedGain(I)Z

    goto/16 :goto_0

    :cond_a
    iput v4, p0, Lcom/konka/factory/ADCAdjustViewHolder;->rgainvalADC:I

    goto :goto_a

    :sswitch_c
    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->ggainvalADC:I

    if-eqz v3, :cond_b

    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->ggainvalADC:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->ggainvalADC:I

    :goto_b
    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->ggainvalADC:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->text_factory_adc_ggain_val:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/ADCAdjustViewHolder;->ggainvalADC:I

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setADCGreenGain(I)Z

    goto/16 :goto_0

    :cond_b
    iput v4, p0, Lcom/konka/factory/ADCAdjustViewHolder;->ggainvalADC:I

    goto :goto_b

    :sswitch_d
    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->bgainvalADC:I

    if-eqz v3, :cond_c

    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->bgainvalADC:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->bgainvalADC:I

    :goto_c
    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->bgainvalADC:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->text_factory_adc_bgain_val:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/ADCAdjustViewHolder;->bgainvalADC:I

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setADCBlueGain(I)Z

    goto/16 :goto_0

    :cond_c
    iput v4, p0, Lcom/konka/factory/ADCAdjustViewHolder;->bgainvalADC:I

    goto :goto_c

    :sswitch_e
    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->roffsetvalADC:I

    if-eqz v3, :cond_d

    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->roffsetvalADC:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->roffsetvalADC:I

    :goto_d
    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->roffsetvalADC:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->text_factory_adc_roffset_val:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/ADCAdjustViewHolder;->roffsetvalADC:I

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setADCRedOffset(I)Z

    goto/16 :goto_0

    :cond_d
    iput v6, p0, Lcom/konka/factory/ADCAdjustViewHolder;->roffsetvalADC:I

    goto :goto_d

    :sswitch_f
    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->goffsetvalADC:I

    if-eqz v3, :cond_e

    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->goffsetvalADC:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->goffsetvalADC:I

    :goto_e
    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->goffsetvalADC:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->text_factory_adc_goffset_val:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/ADCAdjustViewHolder;->goffsetvalADC:I

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setADCGreenOffset(I)Z

    goto/16 :goto_0

    :cond_e
    const/16 v3, 0x1ff

    iput v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->goffsetvalADC:I

    goto :goto_e

    :sswitch_10
    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->boffsetvalADC:I

    if-eqz v3, :cond_f

    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->boffsetvalADC:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->boffsetvalADC:I

    :goto_f
    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->boffsetvalADC:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->text_factory_adc_boffset_val:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/ADCAdjustViewHolder;->boffsetvalADC:I

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setADCBlueOffset(I)Z

    goto/16 :goto_0

    :cond_f
    iput v6, p0, Lcom/konka/factory/ADCAdjustViewHolder;->boffsetvalADC:I

    goto :goto_f

    :sswitch_11
    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->phasevalADC:I

    if-eqz v3, :cond_10

    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->phasevalADC:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->phasevalADC:I

    :goto_10
    iget v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->phasevalADC:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->text_factory_adc_phase_val:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/ADCAdjustViewHolder;->phasevalADC:I

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setADCPhase(I)Z

    goto/16 :goto_0

    :cond_10
    iput v8, p0, Lcom/konka/factory/ADCAdjustViewHolder;->phasevalADC:I

    goto :goto_10

    :sswitch_12
    iget-object v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->adcActivity:Lcom/konka/factory/MainmenuActivity;

    const/4 v4, 0x5

    invoke-virtual {v3, v4}, Lcom/konka/factory/MainmenuActivity;->returnRoot(I)V

    iget-object v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v4, 0x1

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->releaseHandler(I)V

    goto/16 :goto_0

    :sswitch_13
    const v3, 0x7f0a0018    # com.konka.factory.R.id.linearlayout_factory_adc_autoadjust

    if-ne v1, v3, :cond_0

    iget-object v3, p0, Lcom/konka/factory/ADCAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v3}, Lcom/konka/factory/desk/IFactoryDesk;->ExecAutoADC()Z

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_12
        0x15 -> :sswitch_9
        0x16 -> :sswitch_0
        0x42 -> :sswitch_13
        0x52 -> :sswitch_12
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x7f0a0000 -> :sswitch_1    # com.konka.factory.R.id.linearlayout_factory_adc_source
        0x7f0a0003 -> :sswitch_2    # com.konka.factory.R.id.linearlayout_factory_adc_rgain
        0x7f0a0006 -> :sswitch_3    # com.konka.factory.R.id.linearlayout_factory_adc_ggain
        0x7f0a0009 -> :sswitch_4    # com.konka.factory.R.id.linearlayout_factory_adc_bgain
        0x7f0a000c -> :sswitch_5    # com.konka.factory.R.id.linearlayout_factory_adc_roffset
        0x7f0a000f -> :sswitch_6    # com.konka.factory.R.id.linearlayout_factory_adc_goffset
        0x7f0a0012 -> :sswitch_7    # com.konka.factory.R.id.linearlayout_factory_adc_boffset
        0x7f0a0015 -> :sswitch_8    # com.konka.factory.R.id.linearlayout_factory_adc_phase
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x7f0a0000 -> :sswitch_a    # com.konka.factory.R.id.linearlayout_factory_adc_source
        0x7f0a0003 -> :sswitch_b    # com.konka.factory.R.id.linearlayout_factory_adc_rgain
        0x7f0a0006 -> :sswitch_c    # com.konka.factory.R.id.linearlayout_factory_adc_ggain
        0x7f0a0009 -> :sswitch_d    # com.konka.factory.R.id.linearlayout_factory_adc_bgain
        0x7f0a000c -> :sswitch_e    # com.konka.factory.R.id.linearlayout_factory_adc_roffset
        0x7f0a000f -> :sswitch_f    # com.konka.factory.R.id.linearlayout_factory_adc_goffset
        0x7f0a0012 -> :sswitch_10    # com.konka.factory.R.id.linearlayout_factory_adc_boffset
        0x7f0a0015 -> :sswitch_11    # com.konka.factory.R.id.linearlayout_factory_adc_phase
    .end sparse-switch
.end method
