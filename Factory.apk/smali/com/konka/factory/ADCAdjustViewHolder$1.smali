.class Lcom/konka/factory/ADCAdjustViewHolder$1;
.super Landroid/os/Handler;
.source "ADCAdjustViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/factory/ADCAdjustViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/factory/ADCAdjustViewHolder;


# direct methods
.method constructor <init>(Lcom/konka/factory/ADCAdjustViewHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/factory/ADCAdjustViewHolder$1;->this$0:Lcom/konka/factory/ADCAdjustViewHolder;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1    # Landroid/os/Message;

    const/4 v3, 0x0

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x4e8f

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder$1;->this$0:Lcom/konka/factory/ADCAdjustViewHolder;

    iget-object v1, p0, Lcom/konka/factory/ADCAdjustViewHolder$1;->this$0:Lcom/konka/factory/ADCAdjustViewHolder;

    # invokes: Lcom/konka/factory/ADCAdjustViewHolder;->getProgressDialog()Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/konka/factory/ADCAdjustViewHolder;->access$100(Lcom/konka/factory/ADCAdjustViewHolder;)Landroid/app/ProgressDialog;

    move-result-object v1

    # setter for: Lcom/konka/factory/ADCAdjustViewHolder;->progressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0, v1}, Lcom/konka/factory/ADCAdjustViewHolder;->access$002(Lcom/konka/factory/ADCAdjustViewHolder;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder$1;->this$0:Lcom/konka/factory/ADCAdjustViewHolder;

    # getter for: Lcom/konka/factory/ADCAdjustViewHolder;->progressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/konka/factory/ADCAdjustViewHolder;->access$000(Lcom/konka/factory/ADCAdjustViewHolder;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x4e90

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder$1;->this$0:Lcom/konka/factory/ADCAdjustViewHolder;

    # getter for: Lcom/konka/factory/ADCAdjustViewHolder;->progressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/konka/factory/ADCAdjustViewHolder;->access$000(Lcom/konka/factory/ADCAdjustViewHolder;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    iget-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder$1;->this$0:Lcom/konka/factory/ADCAdjustViewHolder;

    invoke-virtual {v0}, Lcom/konka/factory/ADCAdjustViewHolder;->onCreate()Z

    iget-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder$1;->this$0:Lcom/konka/factory/ADCAdjustViewHolder;

    # getter for: Lcom/konka/factory/ADCAdjustViewHolder;->adcActivity:Lcom/konka/factory/MainmenuActivity;
    invoke-static {v0}, Lcom/konka/factory/ADCAdjustViewHolder;->access$200(Lcom/konka/factory/ADCAdjustViewHolder;)Lcom/konka/factory/MainmenuActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/factory/ADCAdjustViewHolder$1;->this$0:Lcom/konka/factory/ADCAdjustViewHolder;

    # getter for: Lcom/konka/factory/ADCAdjustViewHolder;->adcActivity:Lcom/konka/factory/MainmenuActivity;
    invoke-static {v1}, Lcom/konka/factory/ADCAdjustViewHolder;->access$200(Lcom/konka/factory/ADCAdjustViewHolder;)Lcom/konka/factory/MainmenuActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/factory/MainmenuActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08000c    # com.konka.factory.R.string.str_factory_adc_autoadjust_ok

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x4e91

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder$1;->this$0:Lcom/konka/factory/ADCAdjustViewHolder;

    # getter for: Lcom/konka/factory/ADCAdjustViewHolder;->progressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/konka/factory/ADCAdjustViewHolder;->access$000(Lcom/konka/factory/ADCAdjustViewHolder;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    iget-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder$1;->this$0:Lcom/konka/factory/ADCAdjustViewHolder;

    invoke-virtual {v0}, Lcom/konka/factory/ADCAdjustViewHolder;->onCreate()Z

    iget-object v0, p0, Lcom/konka/factory/ADCAdjustViewHolder$1;->this$0:Lcom/konka/factory/ADCAdjustViewHolder;

    # getter for: Lcom/konka/factory/ADCAdjustViewHolder;->adcActivity:Lcom/konka/factory/MainmenuActivity;
    invoke-static {v0}, Lcom/konka/factory/ADCAdjustViewHolder;->access$200(Lcom/konka/factory/ADCAdjustViewHolder;)Lcom/konka/factory/MainmenuActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/factory/ADCAdjustViewHolder$1;->this$0:Lcom/konka/factory/ADCAdjustViewHolder;

    # getter for: Lcom/konka/factory/ADCAdjustViewHolder;->adcActivity:Lcom/konka/factory/MainmenuActivity;
    invoke-static {v1}, Lcom/konka/factory/ADCAdjustViewHolder;->access$200(Lcom/konka/factory/ADCAdjustViewHolder;)Lcom/konka/factory/MainmenuActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/factory/MainmenuActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08000d    # com.konka.factory.R.string.str_factory_adc_autoadjust_failed

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
