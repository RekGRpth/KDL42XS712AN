.class public Lcom/konka/factory/CustomerIDActivity;
.super Landroid/app/Activity;
.source "CustomerIDActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;
    }
.end annotation


# instance fields
.field private CustomerIDTable:[I

.field private MAXINDEXS:I

.field private MSG_ONCREAT:I

.field private final ONEPINDEXS:I

.field private OptionCustomers:[Ljava/lang/String;

.field private OptionCustomersToSave:[Ljava/lang/String;

.field private curItem_index:I

.field private hint_left_arrow:Landroid/widget/ImageView;

.field private hint_right_arrow:Landroid/widget/ImageView;

.field private listener:Landroid/view/View$OnClickListener;

.field private mHandler:Landroid/os/Handler;

.field private page_index:I

.field private page_total:I

.field private tableCustomerIDSelect_index:I

.field private updateAdjustViewHolder:Lcom/konka/factory/UPDATEAdjustViewHolder;

.field private viewholder_customerid:Lcom/konka/factory/ViewHolder;


# direct methods
.method public constructor <init>()V
    .locals 4

    const/16 v3, 0x9

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput v2, p0, Lcom/konka/factory/CustomerIDActivity;->tableCustomerIDSelect_index:I

    iput v3, p0, Lcom/konka/factory/CustomerIDActivity;->ONEPINDEXS:I

    iput v2, p0, Lcom/konka/factory/CustomerIDActivity;->page_index:I

    iput v2, p0, Lcom/konka/factory/CustomerIDActivity;->curItem_index:I

    iput v2, p0, Lcom/konka/factory/CustomerIDActivity;->page_total:I

    const/16 v0, 0x3e9

    iput v0, p0, Lcom/konka/factory/CustomerIDActivity;->MSG_ONCREAT:I

    const/16 v0, 0xb

    new-array v0, v0, [I

    sget-object v1, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_KOGAN:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    invoke-virtual {v1}, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->ordinal()I

    move-result v1

    aput v1, v0, v2

    const/4 v1, 0x1

    sget-object v2, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_TEAC:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    invoke-virtual {v2}, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_JVC:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    invoke-virtual {v2}, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_SNOWA:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    invoke-virtual {v2}, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_XVISION:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    invoke-virtual {v2}, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_DNS:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    invoke-virtual {v2}, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_ENIE:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    invoke-virtual {v2}, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_NASCO:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    invoke-virtual {v2}, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_NIDAA:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    invoke-virtual {v2}, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->ordinal()I

    move-result v2

    aput v2, v0, v1

    sget-object v1, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_OTHERS:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    invoke-virtual {v1}, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->ordinal()I

    move-result v1

    aput v1, v0, v3

    const/16 v1, 0xa

    sget-object v2, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_KONKA:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    invoke-virtual {v2}, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->ordinal()I

    move-result v2

    aput v2, v0, v1

    iput-object v0, p0, Lcom/konka/factory/CustomerIDActivity;->CustomerIDTable:[I

    new-instance v0, Lcom/konka/factory/CustomerIDActivity$1;

    invoke-direct {v0, p0}, Lcom/konka/factory/CustomerIDActivity$1;-><init>(Lcom/konka/factory/CustomerIDActivity;)V

    iput-object v0, p0, Lcom/konka/factory/CustomerIDActivity;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/konka/factory/CustomerIDActivity$3;

    invoke-direct {v0, p0}, Lcom/konka/factory/CustomerIDActivity$3;-><init>(Lcom/konka/factory/CustomerIDActivity;)V

    iput-object v0, p0, Lcom/konka/factory/CustomerIDActivity;->listener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method private CoutrySelected(I)V
    .locals 10
    .param p1    # I

    iget v7, p0, Lcom/konka/factory/CustomerIDActivity;->page_index:I

    mul-int/lit8 v7, v7, 0x9

    add-int v6, v7, p1

    iget-object v7, p0, Lcom/konka/factory/CustomerIDActivity;->CustomerIDTable:[I

    aget v2, v7, v6

    const-string v7, "CoutrySelected"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "***33***customerid_index==  "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v7, p0, Lcom/konka/factory/CustomerIDActivity;->updateAdjustViewHolder:Lcom/konka/factory/UPDATEAdjustViewHolder;

    const/16 v7, 0x2000

    new-array v3, v7, [B

    iget-object v7, p0, Lcom/konka/factory/CustomerIDActivity;->updateAdjustViewHolder:Lcom/konka/factory/UPDATEAdjustViewHolder;

    const/16 v7, 0x8

    new-array v0, v7, [B

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/konka/factory/CustomerIDActivity;->OptionCustomersToSave:[Ljava/lang/String;

    aget-object v8, v8, v2

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Ljava/lang/String;->charAt(I)C

    move-result v8

    int-to-byte v8, v8

    aput-byte v8, v0, v7

    const/4 v7, 0x1

    iget-object v8, p0, Lcom/konka/factory/CustomerIDActivity;->OptionCustomersToSave:[Ljava/lang/String;

    aget-object v8, v8, v2

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Ljava/lang/String;->charAt(I)C

    move-result v8

    int-to-byte v8, v8

    aput-byte v8, v0, v7

    const/4 v7, 0x2

    iget-object v8, p0, Lcom/konka/factory/CustomerIDActivity;->OptionCustomersToSave:[Ljava/lang/String;

    aget-object v8, v8, v2

    const/4 v9, 0x2

    invoke-virtual {v8, v9}, Ljava/lang/String;->charAt(I)C

    move-result v8

    int-to-byte v8, v8

    aput-byte v8, v0, v7

    const/4 v5, 0x3

    :goto_0
    const/16 v7, 0x8

    if-ge v5, v7, :cond_0

    const-string v7, "KONKA"

    add-int/lit8 v8, v5, -0x3

    invoke-virtual {v7, v8}, Ljava/lang/String;->charAt(I)C

    move-result v7

    int-to-byte v7, v7

    aput-byte v7, v0, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_0
    :try_start_0
    iget-object v7, p0, Lcom/konka/factory/CustomerIDActivity;->updateAdjustViewHolder:Lcom/konka/factory/UPDATEAdjustViewHolder;

    const/16 v7, 0x2000

    new-array v1, v7, [S

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v7

    iget-object v8, p0, Lcom/konka/factory/CustomerIDActivity;->updateAdjustViewHolder:Lcom/konka/factory/UPDATEAdjustViewHolder;

    const/16 v8, 0x1d

    iget-object v9, p0, Lcom/konka/factory/CustomerIDActivity;->updateAdjustViewHolder:Lcom/konka/factory/UPDATEAdjustViewHolder;

    const/16 v9, 0x2000

    invoke-virtual {v7, v8, v9}, Lcom/mstar/android/tvapi/common/TvManager;->readFromSpiFlashByBank(II)[S

    move-result-object v1

    const/4 v5, 0x0

    :goto_1
    iget-object v7, p0, Lcom/konka/factory/CustomerIDActivity;->updateAdjustViewHolder:Lcom/konka/factory/UPDATEAdjustViewHolder;

    const/16 v7, 0x8

    if-ge v5, v7, :cond_1

    iget-object v7, p0, Lcom/konka/factory/CustomerIDActivity;->updateAdjustViewHolder:Lcom/konka/factory/UPDATEAdjustViewHolder;

    add-int/lit16 v7, v5, 0xd0

    aget-byte v8, v0, v5

    int-to-short v8, v8

    aput-short v8, v1, v7

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v7

    iget-object v8, p0, Lcom/konka/factory/CustomerIDActivity;->updateAdjustViewHolder:Lcom/konka/factory/UPDATEAdjustViewHolder;

    const/16 v8, 0x1d

    invoke-virtual {v7, v8, v1}, Lcom/mstar/android/tvapi/common/TvManager;->writeToSpiFlashByBank(I[S)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    sget-object v7, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_CUSTOMER_NUM:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    invoke-virtual {v7}, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->ordinal()I

    move-result v7

    if-lt v2, v7, :cond_2

    :goto_3
    return-void

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2

    :cond_2
    invoke-virtual {p0}, Lcom/konka/factory/CustomerIDActivity;->finish()V

    goto :goto_3
.end method

.method static synthetic access$000(Lcom/konka/factory/CustomerIDActivity;)I
    .locals 1
    .param p0    # Lcom/konka/factory/CustomerIDActivity;

    iget v0, p0, Lcom/konka/factory/CustomerIDActivity;->MSG_ONCREAT:I

    return v0
.end method

.method static synthetic access$100(Lcom/konka/factory/CustomerIDActivity;)V
    .locals 0
    .param p0    # Lcom/konka/factory/CustomerIDActivity;

    invoke-direct {p0}, Lcom/konka/factory/CustomerIDActivity;->updateUiCoutryRequestFocus()V

    return-void
.end method

.method static synthetic access$1000(Lcom/konka/factory/CustomerIDActivity;)[I
    .locals 1
    .param p0    # Lcom/konka/factory/CustomerIDActivity;

    iget-object v0, p0, Lcom/konka/factory/CustomerIDActivity;->CustomerIDTable:[I

    return-object v0
.end method

.method static synthetic access$1100(Lcom/konka/factory/CustomerIDActivity;)I
    .locals 1
    .param p0    # Lcom/konka/factory/CustomerIDActivity;

    iget v0, p0, Lcom/konka/factory/CustomerIDActivity;->tableCustomerIDSelect_index:I

    return v0
.end method

.method static synthetic access$1102(Lcom/konka/factory/CustomerIDActivity;I)I
    .locals 0
    .param p0    # Lcom/konka/factory/CustomerIDActivity;
    .param p1    # I

    iput p1, p0, Lcom/konka/factory/CustomerIDActivity;->tableCustomerIDSelect_index:I

    return p1
.end method

.method static synthetic access$1202(Lcom/konka/factory/CustomerIDActivity;I)I
    .locals 0
    .param p0    # Lcom/konka/factory/CustomerIDActivity;
    .param p1    # I

    iput p1, p0, Lcom/konka/factory/CustomerIDActivity;->page_index:I

    return p1
.end method

.method static synthetic access$1302(Lcom/konka/factory/CustomerIDActivity;I)I
    .locals 0
    .param p0    # Lcom/konka/factory/CustomerIDActivity;
    .param p1    # I

    iput p1, p0, Lcom/konka/factory/CustomerIDActivity;->curItem_index:I

    return p1
.end method

.method static synthetic access$1400(Lcom/konka/factory/CustomerIDActivity;)I
    .locals 1
    .param p0    # Lcom/konka/factory/CustomerIDActivity;

    iget v0, p0, Lcom/konka/factory/CustomerIDActivity;->MAXINDEXS:I

    return v0
.end method

.method static synthetic access$1402(Lcom/konka/factory/CustomerIDActivity;I)I
    .locals 0
    .param p0    # Lcom/konka/factory/CustomerIDActivity;
    .param p1    # I

    iput p1, p0, Lcom/konka/factory/CustomerIDActivity;->MAXINDEXS:I

    return p1
.end method

.method static synthetic access$1500(Lcom/konka/factory/CustomerIDActivity;)V
    .locals 0
    .param p0    # Lcom/konka/factory/CustomerIDActivity;

    invoke-direct {p0}, Lcom/konka/factory/CustomerIDActivity;->registerListeners()V

    return-void
.end method

.method static synthetic access$1600(Lcom/konka/factory/CustomerIDActivity;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/konka/factory/CustomerIDActivity;

    iget-object v0, p0, Lcom/konka/factory/CustomerIDActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/konka/factory/CustomerIDActivity;I)V
    .locals 0
    .param p0    # Lcom/konka/factory/CustomerIDActivity;
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/konka/factory/CustomerIDActivity;->CoutrySelected(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/konka/factory/CustomerIDActivity;)I
    .locals 1
    .param p0    # Lcom/konka/factory/CustomerIDActivity;

    iget v0, p0, Lcom/konka/factory/CustomerIDActivity;->page_total:I

    return v0
.end method

.method static synthetic access$202(Lcom/konka/factory/CustomerIDActivity;I)I
    .locals 0
    .param p0    # Lcom/konka/factory/CustomerIDActivity;
    .param p1    # I

    iput p1, p0, Lcom/konka/factory/CustomerIDActivity;->page_total:I

    return p1
.end method

.method static synthetic access$300(Lcom/konka/factory/CustomerIDActivity;)Lcom/konka/factory/ViewHolder;
    .locals 1
    .param p0    # Lcom/konka/factory/CustomerIDActivity;

    iget-object v0, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    return-object v0
.end method

.method static synthetic access$302(Lcom/konka/factory/CustomerIDActivity;Lcom/konka/factory/ViewHolder;)Lcom/konka/factory/ViewHolder;
    .locals 0
    .param p0    # Lcom/konka/factory/CustomerIDActivity;
    .param p1    # Lcom/konka/factory/ViewHolder;

    iput-object p1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    return-object p1
.end method

.method static synthetic access$402(Lcom/konka/factory/CustomerIDActivity;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0    # Lcom/konka/factory/CustomerIDActivity;
    .param p1    # Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/konka/factory/CustomerIDActivity;->hint_left_arrow:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$502(Lcom/konka/factory/CustomerIDActivity;Landroid/widget/ImageView;)Landroid/widget/ImageView;
    .locals 0
    .param p0    # Lcom/konka/factory/CustomerIDActivity;
    .param p1    # Landroid/widget/ImageView;

    iput-object p1, p0, Lcom/konka/factory/CustomerIDActivity;->hint_right_arrow:Landroid/widget/ImageView;

    return-object p1
.end method

.method static synthetic access$600(Lcom/konka/factory/CustomerIDActivity;)V
    .locals 0
    .param p0    # Lcom/konka/factory/CustomerIDActivity;

    invoke-direct {p0}, Lcom/konka/factory/CustomerIDActivity;->updateUiCoutrySelect()V

    return-void
.end method

.method static synthetic access$700(Lcom/konka/factory/CustomerIDActivity;)[Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/konka/factory/CustomerIDActivity;

    iget-object v0, p0, Lcom/konka/factory/CustomerIDActivity;->OptionCustomers:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$702(Lcom/konka/factory/CustomerIDActivity;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/konka/factory/CustomerIDActivity;
    .param p1    # [Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/factory/CustomerIDActivity;->OptionCustomers:[Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$800(Lcom/konka/factory/CustomerIDActivity;)[Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/konka/factory/CustomerIDActivity;

    iget-object v0, p0, Lcom/konka/factory/CustomerIDActivity;->OptionCustomersToSave:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$802(Lcom/konka/factory/CustomerIDActivity;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/konka/factory/CustomerIDActivity;
    .param p1    # [Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/factory/CustomerIDActivity;->OptionCustomersToSave:[Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$900(Lcom/konka/factory/CustomerIDActivity;)Lcom/konka/factory/UPDATEAdjustViewHolder;
    .locals 1
    .param p0    # Lcom/konka/factory/CustomerIDActivity;

    iget-object v0, p0, Lcom/konka/factory/CustomerIDActivity;->updateAdjustViewHolder:Lcom/konka/factory/UPDATEAdjustViewHolder;

    return-object v0
.end method

.method private registerListeners()V
    .locals 2

    iget-object v0, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_customer_1:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_customer_2:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_customer_3:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_customer_4:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_customer_5:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_customer_6:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_customer_7:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_customer_8:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_customer_9:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private updateUiCoutryRequestFocus()V
    .locals 1

    iget v0, p0, Lcom/konka/factory/CustomerIDActivity;->curItem_index:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_customer_1:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_customer_2:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_customer_3:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_customer_4:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_customer_5:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_customer_6:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_6
    iget-object v0, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_customer_7:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_7
    iget-object v0, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_customer_8:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_8
    iget-object v0, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->button_cha_customer_9:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private updateUiCoutrySelect()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget v1, p0, Lcom/konka/factory/CustomerIDActivity;->page_index:I

    mul-int/lit8 v0, v1, 0x9

    iget v1, p0, Lcom/konka/factory/CustomerIDActivity;->MAXINDEXS:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_1:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/factory/CustomerIDActivity;->OptionCustomers:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_1:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setFocusable(Z)V

    :goto_0
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/konka/factory/CustomerIDActivity;->MAXINDEXS:I

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_2:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/factory/CustomerIDActivity;->OptionCustomers:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_2:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setFocusable(Z)V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/konka/factory/CustomerIDActivity;->MAXINDEXS:I

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_3:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/factory/CustomerIDActivity;->OptionCustomers:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_3:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setFocusable(Z)V

    :goto_2
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/konka/factory/CustomerIDActivity;->MAXINDEXS:I

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_4:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/factory/CustomerIDActivity;->OptionCustomers:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_4:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setFocusable(Z)V

    :goto_3
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/konka/factory/CustomerIDActivity;->MAXINDEXS:I

    if-ge v0, v1, :cond_4

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_5:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/factory/CustomerIDActivity;->OptionCustomers:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_5:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setFocusable(Z)V

    :goto_4
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/konka/factory/CustomerIDActivity;->MAXINDEXS:I

    if-ge v0, v1, :cond_5

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_6:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/factory/CustomerIDActivity;->OptionCustomers:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_6:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setFocusable(Z)V

    :goto_5
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/konka/factory/CustomerIDActivity;->MAXINDEXS:I

    if-ge v0, v1, :cond_6

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_7:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/factory/CustomerIDActivity;->OptionCustomers:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_7:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setFocusable(Z)V

    :goto_6
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/konka/factory/CustomerIDActivity;->MAXINDEXS:I

    if-ge v0, v1, :cond_7

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_8:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/factory/CustomerIDActivity;->OptionCustomers:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_8:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setFocusable(Z)V

    :goto_7
    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/konka/factory/CustomerIDActivity;->MAXINDEXS:I

    if-ge v0, v1, :cond_8

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_9:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/factory/CustomerIDActivity;->OptionCustomers:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_9:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setFocusable(Z)V

    :goto_8
    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->text_cha_hint_currentpage_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/CustomerIDActivity;->page_index:I

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v1, p0, Lcom/konka/factory/CustomerIDActivity;->page_index:I

    if-nez v1, :cond_9

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->hint_left_arrow:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    :goto_9
    iget v1, p0, Lcom/konka/factory/CustomerIDActivity;->page_index:I

    iget v2, p0, Lcom/konka/factory/CustomerIDActivity;->page_total:I

    add-int/lit8 v2, v2, -0x1

    if-ne v1, v2, :cond_a

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->hint_right_arrow:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    :goto_a
    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_1:Landroid/widget/Button;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_1:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setFocusable(Z)V

    goto/16 :goto_0

    :cond_1
    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_2:Landroid/widget/Button;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_2:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setFocusable(Z)V

    goto/16 :goto_1

    :cond_2
    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_3:Landroid/widget/Button;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_3:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setFocusable(Z)V

    goto/16 :goto_2

    :cond_3
    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_4:Landroid/widget/Button;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_4:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setFocusable(Z)V

    goto/16 :goto_3

    :cond_4
    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_5:Landroid/widget/Button;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_5:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setFocusable(Z)V

    goto/16 :goto_4

    :cond_5
    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_6:Landroid/widget/Button;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_6:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setFocusable(Z)V

    goto/16 :goto_5

    :cond_6
    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_7:Landroid/widget/Button;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_7:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setFocusable(Z)V

    goto/16 :goto_6

    :cond_7
    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_8:Landroid/widget/Button;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_8:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setFocusable(Z)V

    goto/16 :goto_7

    :cond_8
    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_9:Landroid/widget/Button;

    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_9:Landroid/widget/Button;

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setFocusable(Z)V

    goto/16 :goto_8

    :cond_9
    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->hint_left_arrow:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto/16 :goto_9

    :cond_a
    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->hint_right_arrow:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    goto/16 :goto_a
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030002    # com.konka.factory.R.layout.customerid

    invoke-virtual {p0, v0}, Lcom/konka/factory/CustomerIDActivity;->setContentView(I)V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/factory/CustomerIDActivity$2;

    invoke-direct {v1, p0}, Lcom/konka/factory/CustomerIDActivity$2;-><init>(Lcom/konka/factory/CustomerIDActivity;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    invoke-virtual {p0}, Lcom/konka/factory/CustomerIDActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch p1, :sswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    :goto_1
    return v1

    :sswitch_0
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget v1, p0, Lcom/konka/factory/CustomerIDActivity;->page_index:I

    add-int/lit8 v1, v1, 0x1

    mul-int/lit8 v1, v1, 0x9

    iget v2, p0, Lcom/konka/factory/CustomerIDActivity;->MAXINDEXS:I

    if-ge v1, v2, :cond_0

    iget v1, p0, Lcom/konka/factory/CustomerIDActivity;->page_index:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/konka/factory/CustomerIDActivity;->page_index:I

    invoke-direct {p0}, Lcom/konka/factory/CustomerIDActivity;->updateUiCoutrySelect()V

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_1:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_1
    iget v1, p0, Lcom/konka/factory/CustomerIDActivity;->page_index:I

    add-int/lit8 v1, v1, 0x1

    mul-int/lit8 v1, v1, 0x9

    iget v2, p0, Lcom/konka/factory/CustomerIDActivity;->MAXINDEXS:I

    if-ge v1, v2, :cond_0

    iget v1, p0, Lcom/konka/factory/CustomerIDActivity;->page_index:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/konka/factory/CustomerIDActivity;->page_index:I

    invoke-direct {p0}, Lcom/konka/factory/CustomerIDActivity;->updateUiCoutrySelect()V

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_2:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_2
    iget v1, p0, Lcom/konka/factory/CustomerIDActivity;->page_index:I

    add-int/lit8 v1, v1, 0x1

    mul-int/lit8 v1, v1, 0x9

    iget v2, p0, Lcom/konka/factory/CustomerIDActivity;->MAXINDEXS:I

    if-ge v1, v2, :cond_0

    iget v1, p0, Lcom/konka/factory/CustomerIDActivity;->page_index:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/konka/factory/CustomerIDActivity;->page_index:I

    invoke-direct {p0}, Lcom/konka/factory/CustomerIDActivity;->updateUiCoutrySelect()V

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_3:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :sswitch_1
    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_3
    iget v1, p0, Lcom/konka/factory/CustomerIDActivity;->page_index:I

    if-lez v1, :cond_0

    iget v1, p0, Lcom/konka/factory/CustomerIDActivity;->page_index:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/konka/factory/CustomerIDActivity;->page_index:I

    invoke-direct {p0}, Lcom/konka/factory/CustomerIDActivity;->updateUiCoutrySelect()V

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_7:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0

    :pswitch_4
    iget v1, p0, Lcom/konka/factory/CustomerIDActivity;->page_index:I

    if-lez v1, :cond_0

    iget v1, p0, Lcom/konka/factory/CustomerIDActivity;->page_index:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/konka/factory/CustomerIDActivity;->page_index:I

    invoke-direct {p0}, Lcom/konka/factory/CustomerIDActivity;->updateUiCoutrySelect()V

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_8:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->requestFocus()Z

    goto/16 :goto_0

    :pswitch_5
    iget v1, p0, Lcom/konka/factory/CustomerIDActivity;->page_index:I

    if-lez v1, :cond_0

    iget v1, p0, Lcom/konka/factory/CustomerIDActivity;->page_index:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/konka/factory/CustomerIDActivity;->page_index:I

    invoke-direct {p0}, Lcom/konka/factory/CustomerIDActivity;->updateUiCoutrySelect()V

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;

    iget-object v1, v1, Lcom/konka/factory/ViewHolder;->button_cha_customer_9:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->requestFocus()Z

    goto/16 :goto_0

    :sswitch_2
    invoke-virtual {p0}, Lcom/konka/factory/CustomerIDActivity;->finish()V

    goto/16 :goto_0

    :sswitch_3
    const/4 v1, 0x1

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_2
        0x15 -> :sswitch_1
        0x16 -> :sswitch_0
        0x52 -> :sswitch_2
        0xb2 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x7f0a0030
        :pswitch_0    # com.konka.factory.R.id.button_cha_autotuning_choosecustomer_seven
        :pswitch_1    # com.konka.factory.R.id.button_cha_autotuning_choosecustomer_eight
        :pswitch_2    # com.konka.factory.R.id.button_cha_autotuning_choosecustomer_nine
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x7f0a002a
        :pswitch_3    # com.konka.factory.R.id.button_cha_autotuning_choosecustomer_one
        :pswitch_4    # com.konka.factory.R.id.button_cha_autotuning_choosecustomer_two
        :pswitch_5    # com.konka.factory.R.id.button_cha_autotuning_choosecustomer_three
    .end packed-switch
.end method
