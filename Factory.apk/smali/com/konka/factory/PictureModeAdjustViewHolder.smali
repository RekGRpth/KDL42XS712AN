.class public Lcom/konka/factory/PictureModeAdjustViewHolder;
.super Ljava/lang/Object;
.source "PictureModeAdjustViewHolder.java"


# instance fields
.field private backlightval:I

.field private brightnessval:I

.field private contrastval:I

.field private factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

.field private hueval:I

.field private picModeActivity:Lcom/konka/factory/MainmenuActivity;

.field private picmodearray:[Ljava/lang/String;

.field private picmodeindex:I

.field private prevTime:J

.field protected progress_factory_picmode_backlight:Landroid/widget/ProgressBar;

.field protected progress_factory_picmode_brightness:Landroid/widget/ProgressBar;

.field protected progress_factory_picmode_contrast:Landroid/widget/ProgressBar;

.field protected progress_factory_picmode_hue:Landroid/widget/ProgressBar;

.field protected progress_factory_picmode_saturation:Landroid/widget/ProgressBar;

.field protected progress_factory_picmode_sharpness:Landroid/widget/ProgressBar;

.field private saturationval:I

.field private sharpnessval:I

.field protected text_factory_picmode_backlight_val:Landroid/widget/TextView;

.field protected text_factory_picmode_brightness_val:Landroid/widget/TextView;

.field protected text_factory_picmode_contrast_val:Landroid/widget/TextView;

.field protected text_factory_picmode_hue_val:Landroid/widget/TextView;

.field protected text_factory_picmode_picmode_val:Landroid/widget/TextView;

.field protected text_factory_picmode_saturation_val:Landroid/widget/TextView;

.field protected text_factory_picmode_sharpness_val:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V
    .locals 3
    .param p1    # Lcom/konka/factory/MainmenuActivity;
    .param p2    # Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v2, 0x0

    const/16 v0, 0x32

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v2, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->picmodeindex:I

    iput v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->brightnessval:I

    iput v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->contrastval:I

    iput v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->saturationval:I

    iput v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->hueval:I

    iput v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->sharpnessval:I

    iput v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->backlightval:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->prevTime:J

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Dynamic"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, "Normal"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "Soft"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "User"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "Vivid"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "Natural"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Sports"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->picmodearray:[Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->picModeActivity:Lcom/konka/factory/MainmenuActivity;

    iput-object p2, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    return-void
.end method

.method private freshDataToUIWhenPicModChange()V
    .locals 2

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_BRIGHTNESS:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    invoke-interface {v0, v1}, Lcom/konka/factory/desk/IFactoryDesk;->getVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->brightnessval:I

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_CONTRAST:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    invoke-interface {v0, v1}, Lcom/konka/factory/desk/IFactoryDesk;->getVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->contrastval:I

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_SATURATION:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    invoke-interface {v0, v1}, Lcom/konka/factory/desk/IFactoryDesk;->getVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->saturationval:I

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_HUE:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    invoke-interface {v0, v1}, Lcom/konka/factory/desk/IFactoryDesk;->getVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->hueval:I

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_SHARPNESS:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    invoke-interface {v0, v1}, Lcom/konka/factory/desk/IFactoryDesk;->getVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->sharpnessval:I

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_BACKLIGHT:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    invoke-interface {v0, v1}, Lcom/konka/factory/desk/IFactoryDesk;->getVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->backlightval:I

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->progress_factory_picmode_brightness:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->brightnessval:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->progress_factory_picmode_contrast:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->contrastval:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->progress_factory_picmode_hue:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->hueval:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->progress_factory_picmode_saturation:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->saturationval:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->progress_factory_picmode_sharpness:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->sharpnessval:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->progress_factory_picmode_backlight:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->backlightval:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->text_factory_picmode_brightness_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->brightnessval:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->text_factory_picmode_contrast_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->contrastval:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->text_factory_picmode_saturation_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->saturationval:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->text_factory_picmode_hue_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->hueval:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->text_factory_picmode_sharpness_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->sharpnessval:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->text_factory_picmode_backlight_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->backlightval:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method findView()V
    .locals 2

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->picModeActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a01d8    # com.konka.factory.R.id.textview_factory_picmode_picmode_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->text_factory_picmode_picmode_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->picModeActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a01db    # com.konka.factory.R.id.textview_factory_picmode_brightness_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->text_factory_picmode_brightness_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->picModeActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a01e0    # com.konka.factory.R.id.textview_factory_picmode_contrast_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->text_factory_picmode_contrast_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->picModeActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a01e5    # com.konka.factory.R.id.textview_factory_picmode_saturation_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->text_factory_picmode_saturation_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->picModeActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a01ea    # com.konka.factory.R.id.textview_factory_picmode_hue_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->text_factory_picmode_hue_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->picModeActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a01ef    # com.konka.factory.R.id.textview_factory_picmode_sharpness_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->text_factory_picmode_sharpness_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->picModeActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a01f4    # com.konka.factory.R.id.textview_factory_picmode_backlight_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->text_factory_picmode_backlight_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->picModeActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a01dc    # com.konka.factory.R.id.progressbar_facroty_picmode_brightness

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->progress_factory_picmode_brightness:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->picModeActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a01e1    # com.konka.factory.R.id.progressbar_facroty_picmode_contrast

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->progress_factory_picmode_contrast:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->picModeActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a01e6    # com.konka.factory.R.id.progressbar_facroty_picmode_saturation

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->progress_factory_picmode_saturation:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->picModeActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a01eb    # com.konka.factory.R.id.progressbar_facroty_picmode_hue

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->progress_factory_picmode_hue:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->picModeActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a01f0    # com.konka.factory.R.id.progressbar_facroty_picmode_sharpness

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->progress_factory_picmode_sharpness:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->picModeActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a01f5    # com.konka.factory.R.id.progressbar_facroty_picmode_backlight

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->progress_factory_picmode_backlight:Landroid/widget/ProgressBar;

    return-void
.end method

.method public onCreate()Z
    .locals 3

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getPictureModeIdx()I

    move-result v0

    iput v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->picmodeindex:I

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_BRIGHTNESS:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    invoke-interface {v0, v1}, Lcom/konka/factory/desk/IFactoryDesk;->getVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->brightnessval:I

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_CONTRAST:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    invoke-interface {v0, v1}, Lcom/konka/factory/desk/IFactoryDesk;->getVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->contrastval:I

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_SATURATION:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    invoke-interface {v0, v1}, Lcom/konka/factory/desk/IFactoryDesk;->getVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->saturationval:I

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_HUE:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    invoke-interface {v0, v1}, Lcom/konka/factory/desk/IFactoryDesk;->getVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->hueval:I

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_SHARPNESS:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    invoke-interface {v0, v1}, Lcom/konka/factory/desk/IFactoryDesk;->getVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->sharpnessval:I

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    sget-object v1, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_BACKLIGHT:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    invoke-interface {v0, v1}, Lcom/konka/factory/desk/IFactoryDesk;->getVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->backlightval:I

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->text_factory_picmode_brightness_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->brightnessval:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->text_factory_picmode_contrast_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->contrastval:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->text_factory_picmode_saturation_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->saturationval:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->text_factory_picmode_hue_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->hueval:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->text_factory_picmode_sharpness_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->sharpnessval:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->text_factory_picmode_backlight_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->backlightval:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->progress_factory_picmode_brightness:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->brightnessval:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->progress_factory_picmode_contrast:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->contrastval:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->progress_factory_picmode_saturation:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->saturationval:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->progress_factory_picmode_hue:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->hueval:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->progress_factory_picmode_sharpness:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->sharpnessval:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->progress_factory_picmode_backlight:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->backlightval:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->text_factory_picmode_picmode_val:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->picmodearray:[Ljava/lang/String;

    iget v2, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->picmodeindex:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->prevTime:J

    const/4 v0, 0x1

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 9
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const-wide/16 v7, 0x12c

    const/4 v6, 0x0

    const/16 v5, 0x64

    const/4 v0, 0x1

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->picModeActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v4}, Lcom/konka/factory/MainmenuActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v1

    new-instance v3, Ljava/lang/String;

    invoke-direct {v3}, Ljava/lang/String;-><init>()V

    const/4 v2, 0x0

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    :sswitch_0
    sparse-switch v1, :sswitch_data_1

    goto :goto_0

    :sswitch_1
    invoke-virtual {p0}, Lcom/konka/factory/PictureModeAdjustViewHolder;->timeDiffMs()J

    move-result-wide v4

    cmp-long v4, v4, v7

    if-ltz v4, :cond_0

    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_PICTURE;->PICTURE_NUMS:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_PICTURE;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_PICTURE;->ordinal()I

    move-result v2

    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->picmodeindex:I

    add-int/lit8 v5, v2, -0x1

    if-ge v4, v5, :cond_1

    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->picmodeindex:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->picmodeindex:I

    :goto_1
    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->text_factory_picmode_picmode_val:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->picmodearray:[Ljava/lang/String;

    iget v6, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->picmodeindex:I

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_PICTURE;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_PICTURE;

    move-result-object v5

    iget v6, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->picmodeindex:I

    aget-object v5, v5, v6

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setPictureModeIdx(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_PICTURE;)V

    invoke-direct {p0}, Lcom/konka/factory/PictureModeAdjustViewHolder;->freshDataToUIWhenPicModChange()V

    goto :goto_0

    :cond_1
    iput v6, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->picmodeindex:I

    goto :goto_1

    :sswitch_2
    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->brightnessval:I

    if-eq v4, v5, :cond_2

    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->brightnessval:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->brightnessval:I

    :goto_2
    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->brightnessval:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->text_factory_picmode_brightness_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->progress_factory_picmode_brightness:Landroid/widget/ProgressBar;

    iget v5, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->brightnessval:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    sget-object v5, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_BRIGHTNESS:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    iget v6, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->brightnessval:I

    int-to-short v6, v6

    invoke-interface {v4, v5, v6}, Lcom/konka/factory/desk/IFactoryDesk;->execVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;I)Z

    goto :goto_0

    :cond_2
    iput v6, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->brightnessval:I

    goto :goto_2

    :sswitch_3
    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->contrastval:I

    if-eq v4, v5, :cond_3

    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->contrastval:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->contrastval:I

    :goto_3
    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->contrastval:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->text_factory_picmode_contrast_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->progress_factory_picmode_contrast:Landroid/widget/ProgressBar;

    iget v5, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->contrastval:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    sget-object v5, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_CONTRAST:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    iget v6, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->contrastval:I

    int-to-short v6, v6

    invoke-interface {v4, v5, v6}, Lcom/konka/factory/desk/IFactoryDesk;->execVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;I)Z

    goto/16 :goto_0

    :cond_3
    iput v6, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->contrastval:I

    goto :goto_3

    :sswitch_4
    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->saturationval:I

    if-eq v4, v5, :cond_4

    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->saturationval:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->saturationval:I

    :goto_4
    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->saturationval:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->text_factory_picmode_saturation_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->progress_factory_picmode_saturation:Landroid/widget/ProgressBar;

    iget v5, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->saturationval:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    sget-object v5, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_SATURATION:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    iget v6, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->saturationval:I

    int-to-short v6, v6

    invoke-interface {v4, v5, v6}, Lcom/konka/factory/desk/IFactoryDesk;->execVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;I)Z

    goto/16 :goto_0

    :cond_4
    iput v6, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->saturationval:I

    goto :goto_4

    :sswitch_5
    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->hueval:I

    if-eq v4, v5, :cond_5

    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->hueval:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->hueval:I

    :goto_5
    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->hueval:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->text_factory_picmode_hue_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->progress_factory_picmode_hue:Landroid/widget/ProgressBar;

    iget v5, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->hueval:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    sget-object v5, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_HUE:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    iget v6, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->hueval:I

    int-to-short v6, v6

    invoke-interface {v4, v5, v6}, Lcom/konka/factory/desk/IFactoryDesk;->execVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;I)Z

    goto/16 :goto_0

    :cond_5
    iput v6, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->hueval:I

    goto :goto_5

    :sswitch_6
    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->sharpnessval:I

    if-eq v4, v5, :cond_6

    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->sharpnessval:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->sharpnessval:I

    :goto_6
    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->sharpnessval:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->text_factory_picmode_sharpness_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->progress_factory_picmode_sharpness:Landroid/widget/ProgressBar;

    iget v5, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->sharpnessval:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    sget-object v5, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_SHARPNESS:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    iget v6, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->sharpnessval:I

    int-to-short v6, v6

    invoke-interface {v4, v5, v6}, Lcom/konka/factory/desk/IFactoryDesk;->execVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;I)Z

    goto/16 :goto_0

    :cond_6
    iput v6, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->sharpnessval:I

    goto :goto_6

    :sswitch_7
    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->backlightval:I

    if-eq v4, v5, :cond_7

    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->backlightval:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->backlightval:I

    :goto_7
    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->backlightval:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->text_factory_picmode_backlight_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->progress_factory_picmode_backlight:Landroid/widget/ProgressBar;

    iget v5, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->backlightval:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    sget-object v5, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_BACKLIGHT:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    iget v6, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->backlightval:I

    int-to-short v6, v6

    invoke-interface {v4, v5, v6}, Lcom/konka/factory/desk/IFactoryDesk;->execVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;I)Z

    goto/16 :goto_0

    :cond_7
    iput v6, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->backlightval:I

    goto :goto_7

    :sswitch_8
    sparse-switch v1, :sswitch_data_2

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p0}, Lcom/konka/factory/PictureModeAdjustViewHolder;->timeDiffMs()J

    move-result-wide v4

    cmp-long v4, v4, v7

    if-ltz v4, :cond_0

    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_PICTURE;->PICTURE_NUMS:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_PICTURE;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_PICTURE;->ordinal()I

    move-result v2

    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->picmodeindex:I

    if-lez v4, :cond_8

    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->picmodeindex:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->picmodeindex:I

    :goto_8
    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->text_factory_picmode_picmode_val:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->picmodearray:[Ljava/lang/String;

    iget v6, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->picmodeindex:I

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_PICTURE;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_PICTURE;

    move-result-object v5

    iget v6, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->picmodeindex:I

    aget-object v5, v5, v6

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setPictureModeIdx(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_PICTURE;)V

    invoke-direct {p0}, Lcom/konka/factory/PictureModeAdjustViewHolder;->freshDataToUIWhenPicModChange()V

    goto/16 :goto_0

    :cond_8
    add-int/lit8 v4, v2, -0x1

    iput v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->picmodeindex:I

    goto :goto_8

    :sswitch_a
    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->brightnessval:I

    if-eqz v4, :cond_9

    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->brightnessval:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->brightnessval:I

    :goto_9
    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->brightnessval:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->text_factory_picmode_brightness_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->progress_factory_picmode_brightness:Landroid/widget/ProgressBar;

    iget v5, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->brightnessval:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    sget-object v5, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_BRIGHTNESS:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    iget v6, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->brightnessval:I

    int-to-short v6, v6

    invoke-interface {v4, v5, v6}, Lcom/konka/factory/desk/IFactoryDesk;->execVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;I)Z

    goto/16 :goto_0

    :cond_9
    iput v5, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->brightnessval:I

    goto :goto_9

    :sswitch_b
    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->contrastval:I

    if-eqz v4, :cond_a

    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->contrastval:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->contrastval:I

    :goto_a
    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->contrastval:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->text_factory_picmode_contrast_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->progress_factory_picmode_contrast:Landroid/widget/ProgressBar;

    iget v5, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->contrastval:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    sget-object v5, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_CONTRAST:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    iget v6, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->contrastval:I

    int-to-short v6, v6

    invoke-interface {v4, v5, v6}, Lcom/konka/factory/desk/IFactoryDesk;->execVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;I)Z

    goto/16 :goto_0

    :cond_a
    iput v5, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->contrastval:I

    goto :goto_a

    :sswitch_c
    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->saturationval:I

    if-eqz v4, :cond_b

    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->saturationval:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->saturationval:I

    :goto_b
    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->saturationval:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->text_factory_picmode_saturation_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->progress_factory_picmode_saturation:Landroid/widget/ProgressBar;

    iget v5, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->saturationval:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    sget-object v5, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_SATURATION:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    iget v6, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->saturationval:I

    int-to-short v6, v6

    invoke-interface {v4, v5, v6}, Lcom/konka/factory/desk/IFactoryDesk;->execVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;I)Z

    goto/16 :goto_0

    :cond_b
    iput v5, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->saturationval:I

    goto :goto_b

    :sswitch_d
    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->hueval:I

    if-eqz v4, :cond_c

    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->hueval:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->hueval:I

    :goto_c
    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->hueval:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->text_factory_picmode_hue_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->progress_factory_picmode_hue:Landroid/widget/ProgressBar;

    iget v5, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->hueval:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    sget-object v5, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_HUE:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    iget v6, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->hueval:I

    int-to-short v6, v6

    invoke-interface {v4, v5, v6}, Lcom/konka/factory/desk/IFactoryDesk;->execVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;I)Z

    goto/16 :goto_0

    :cond_c
    iput v5, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->hueval:I

    goto :goto_c

    :sswitch_e
    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->sharpnessval:I

    if-eqz v4, :cond_d

    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->sharpnessval:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->sharpnessval:I

    :goto_d
    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->sharpnessval:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->text_factory_picmode_sharpness_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->progress_factory_picmode_sharpness:Landroid/widget/ProgressBar;

    iget v5, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->sharpnessval:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    sget-object v5, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_SHARPNESS:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    iget v6, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->sharpnessval:I

    int-to-short v6, v6

    invoke-interface {v4, v5, v6}, Lcom/konka/factory/desk/IFactoryDesk;->execVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;I)Z

    goto/16 :goto_0

    :cond_d
    iput v5, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->sharpnessval:I

    goto :goto_d

    :sswitch_f
    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->backlightval:I

    if-eqz v4, :cond_e

    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->backlightval:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->backlightval:I

    :goto_e
    iget v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->backlightval:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->text_factory_picmode_backlight_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->progress_factory_picmode_backlight:Landroid/widget/ProgressBar;

    iget v5, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->backlightval:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    sget-object v5, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_BACKLIGHT:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;

    iget v6, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->backlightval:I

    int-to-short v6, v6

    invoke-interface {v4, v5, v6}, Lcom/konka/factory/desk/IFactoryDesk;->execVideoItem(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_VIDEOITEM;I)Z

    goto/16 :goto_0

    :cond_e
    iput v5, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->backlightval:I

    goto :goto_e

    :sswitch_10
    iget-object v4, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->picModeActivity:Lcom/konka/factory/MainmenuActivity;

    const/4 v5, 0x7

    invoke-virtual {v4, v5}, Lcom/konka/factory/MainmenuActivity;->returnRoot(I)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_10
        0x15 -> :sswitch_8
        0x16 -> :sswitch_0
        0x52 -> :sswitch_10
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x7f0a01d6 -> :sswitch_1    # com.konka.factory.R.id.linearlayout_factory_picmode_picmode
        0x7f0a01d9 -> :sswitch_2    # com.konka.factory.R.id.linearlayout_factory_picmode_brightness
        0x7f0a01de -> :sswitch_3    # com.konka.factory.R.id.linearlayout_factory_picmode_contrast
        0x7f0a01e3 -> :sswitch_4    # com.konka.factory.R.id.linearlayout_factory_picmode_saturation
        0x7f0a01e8 -> :sswitch_5    # com.konka.factory.R.id.linearlayout_factory_picmode_hue
        0x7f0a01ed -> :sswitch_6    # com.konka.factory.R.id.linearlayout_factory_picmode_sharpness
        0x7f0a01f2 -> :sswitch_7    # com.konka.factory.R.id.linearlayout_factory_picmode_backlight
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x7f0a01d6 -> :sswitch_9    # com.konka.factory.R.id.linearlayout_factory_picmode_picmode
        0x7f0a01d9 -> :sswitch_a    # com.konka.factory.R.id.linearlayout_factory_picmode_brightness
        0x7f0a01de -> :sswitch_b    # com.konka.factory.R.id.linearlayout_factory_picmode_contrast
        0x7f0a01e3 -> :sswitch_c    # com.konka.factory.R.id.linearlayout_factory_picmode_saturation
        0x7f0a01e8 -> :sswitch_d    # com.konka.factory.R.id.linearlayout_factory_picmode_hue
        0x7f0a01ed -> :sswitch_e    # com.konka.factory.R.id.linearlayout_factory_picmode_sharpness
        0x7f0a01f2 -> :sswitch_f    # com.konka.factory.R.id.linearlayout_factory_picmode_backlight
    .end sparse-switch
.end method

.method public timeDiffMs()J
    .locals 7

    const-wide/16 v0, 0x0

    const-wide/16 v2, -0x1

    new-instance v4, Landroid/text/format/Time;

    invoke-direct {v4}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v4}, Landroid/text/format/Time;->setToNow()V

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    iget-wide v5, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->prevTime:J

    cmp-long v5, v0, v5

    if-ltz v5, :cond_0

    iget-wide v5, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->prevTime:J

    sub-long v2, v0, v5

    :cond_0
    iput-wide v0, p0, Lcom/konka/factory/PictureModeAdjustViewHolder;->prevTime:J

    return-wide v2
.end method
