.class public Lcom/konka/factory/OverScanAdjustViewHolder;
.super Ljava/lang/Object;
.source "OverScanAdjustViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/factory/OverScanAdjustViewHolder$1;
    }
.end annotation


# instance fields
.field private channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

.field private commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

.field private factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

.field private hpositionval:I

.field private hsizeval:I

.field private mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

.field private meInputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field private mstrText:Ljava/lang/String;

.field private overScanActivity:Lcom/konka/factory/MainmenuActivity;

.field protected progress_factory_overscan_hposition:Landroid/widget/ProgressBar;

.field protected progress_factory_overscan_hsize:Landroid/widget/ProgressBar;

.field protected progress_factory_overscan_vpositon:Landroid/widget/ProgressBar;

.field protected progress_factory_overscan_vsize:Landroid/widget/ProgressBar;

.field private serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

.field private sourcearray:[Ljava/lang/String;

.field private sourceindex:I

.field protected text_factory_overscan_hposition_val:Landroid/widget/TextView;

.field protected text_factory_overscan_hsize_val:Landroid/widget/TextView;

.field protected text_factory_overscan_source_res:Landroid/widget/TextView;

.field protected text_factory_overscan_source_val:Landroid/widget/TextView;

.field protected text_factory_overscan_vpositon_val:Landroid/widget/TextView;

.field protected text_factory_overscan_vsize_val:Landroid/widget/TextView;

.field private videoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

.field private vpositionval:I

.field private vsizeval:I


# direct methods
.method public constructor <init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V
    .locals 3
    .param p1    # Lcom/konka/factory/MainmenuActivity;
    .param p2    # Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v2, 0x0

    const/16 v1, 0x32

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v2, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->sourceindex:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->videoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iput v1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->hsizeval:I

    iput v1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->hpositionval:I

    iput v1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->vsizeval:I

    iput v1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->vpositionval:I

    const/16 v0, 0x21

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "VGA"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, "ATV"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "AV1"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "AV2"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "AV3"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "AV4"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "AV5"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "AV6"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "AV7"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "AV8"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "AVMAX"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "SV1"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "SV2"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "SV3"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "SV4"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "SVMAX"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "YPbPr1"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "YPbPr2"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "YPbPr3"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "YPbPrMAX"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "SCART1"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "SCART2"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "SCARTMAX"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "HDMI1"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "HDMI2"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "HDMI3"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "HDMI4"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "HDMIMAX"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "DTV"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "DVI1"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "DVI2"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "DVI3"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "DVI4"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->sourcearray:[Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->overScanActivity:Lcom/konka/factory/MainmenuActivity;

    iput-object p2, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    return-void
.end method


# virtual methods
.method DisableSourceLinear()V
    .locals 5

    const/4 v4, 0x0

    const v3, -0x777778

    iget-object v1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->overScanActivity:Lcom/konka/factory/MainmenuActivity;

    const v2, 0x7f0a0182    # com.konka.factory.R.id.linearlayout_factory_overscan_source

    invoke-virtual {v1, v2}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    return-void
.end method

.method findView()V
    .locals 2

    iget-object v0, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->overScanActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0184    # com.konka.factory.R.id.textview_factory_overscan_source_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->text_factory_overscan_source_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->overScanActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0185    # com.konka.factory.R.id.textview_factory_overscan_source_res

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->text_factory_overscan_source_res:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->overScanActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0188    # com.konka.factory.R.id.textview_factory_overscan_hsize_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->text_factory_overscan_hsize_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->overScanActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a018d    # com.konka.factory.R.id.textview_factory_overscan_hposition_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->text_factory_overscan_hposition_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->overScanActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0192    # com.konka.factory.R.id.textview_factory_overscan_vsize_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->text_factory_overscan_vsize_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->overScanActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0197    # com.konka.factory.R.id.textview_factory_overscan_vposition_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->text_factory_overscan_vpositon_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->overScanActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0189    # com.konka.factory.R.id.progressbar_facroty_overscan_hsize

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->progress_factory_overscan_hsize:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->overScanActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a018e    # com.konka.factory.R.id.progressbar_facroty_overscan_hposition

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->progress_factory_overscan_hposition:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->overScanActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0193    # com.konka.factory.R.id.progressbar_facroty_overscan_vsize

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->progress_factory_overscan_vsize:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->overScanActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0198    # com.konka.factory.R.id.progressbar_facroty_overscan_vposition

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->progress_factory_overscan_vpositon:Landroid/widget/ProgressBar;

    return-void
.end method

.method public getVideoInfo(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V
    .locals 11
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    const/16 v10, 0x442

    const/16 v9, 0x438

    const/16 v8, 0x42e

    const/16 v7, 0x2c6

    const/16 v6, 0x48

    iget-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/ChannelDesk;->isSignalStabled()Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :cond_1
    iget-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/CommonDesk;->getVideoInfo()Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    move-result-object v5

    iput-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16FrameRate:S

    add-int/lit8 v5, v5, 0x5

    div-int/lit8 v2, v5, 0xa

    const/16 v5, 0x2e

    if-le v2, v5, :cond_3

    const/16 v5, 0x34

    if-ge v2, v5, :cond_3

    const/16 v2, 0x32

    :cond_2
    :goto_1
    sget-object v5, Lcom/konka/factory/OverScanAdjustViewHolder$1;->$SwitchMap$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    goto :goto_0

    :pswitch_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mstrText:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v6, v6, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16HResolution:S

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "X"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v6, v6, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "@"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "HZ"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mstrText:Ljava/lang/String;

    goto :goto_0

    :cond_3
    const/16 v5, 0x38

    if-le v2, v5, :cond_4

    const/16 v5, 0x3e

    if-ge v2, v5, :cond_4

    const/16 v2, 0x3c

    goto :goto_1

    :cond_4
    const/16 v5, 0x43

    if-le v2, v5, :cond_5

    if-ge v2, v6, :cond_5

    const/16 v2, 0x46

    goto :goto_1

    :cond_5
    if-le v2, v6, :cond_6

    const/16 v5, 0x4d

    if-ge v2, v5, :cond_6

    const/16 v2, 0x4b

    goto :goto_1

    :cond_6
    const/16 v5, 0x50

    if-le v2, v5, :cond_2

    const/16 v5, 0x55

    if-ge v2, v5, :cond_2

    const/16 v2, 0x55

    goto :goto_1

    :pswitch_2
    const/4 v3, 0x0

    iget-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v1

    iget-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/ChannelDesk;->getVideoStandard()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    move-result-object v4

    sget-object v5, Lcom/konka/factory/OverScanAdjustViewHolder$1;->$SwitchMap$com$mstar$android$tvapi$common$vo$EnumAvdVideoStandardType:[I

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_1

    const-string v5, ""

    iput-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mstrText:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_3
    const-string v5, "PAL"

    iput-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mstrText:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_4
    const-string v5, "NTSC"

    iput-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mstrText:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_5
    const-string v5, "SECAM"

    iput-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mstrText:Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_6
    const/4 v5, 0x1

    iget-object v6, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/CommonDesk;->isHdmiSignalMode()Z

    move-result v6

    if-ne v5, v6, :cond_e

    const/4 v0, 0x1

    iget-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16HResolution:S

    if-lt v5, v7, :cond_7

    iget-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16HResolution:S

    const/16 v6, 0x604

    if-gt v5, v6, :cond_7

    iget-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    const/16 v6, 0x1d6

    if-lt v5, v6, :cond_7

    iget-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    const/16 v6, 0x1ea

    if-gt v5, v6, :cond_7

    iget-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    const/16 v6, 0x1e0

    iput-short v6, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    :goto_2
    if-eqz v0, :cond_d

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mstrText:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v6, v6, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mstrText:Ljava/lang/String;

    iget-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-object v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->enScanType:Lcom/konka/kkinterface/tv/CommonDesk$EN_SCAN_TYPE;

    sget-object v6, Lcom/konka/kkinterface/tv/CommonDesk$EN_SCAN_TYPE;->E_PROGRESSIVE:Lcom/konka/kkinterface/tv/CommonDesk$EN_SCAN_TYPE;

    if-ne v5, v6, :cond_c

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mstrText:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "P"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mstrText:Ljava/lang/String;

    :goto_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mstrText:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "@"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "HZ"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mstrText:Ljava/lang/String;

    goto/16 :goto_0

    :cond_7
    iget-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    const/16 v6, 0x236

    if-le v5, v6, :cond_8

    iget-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    const/16 v6, 0x24a

    if-ge v5, v6, :cond_8

    iget-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    const/16 v6, 0x240

    iput-short v6, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    goto :goto_2

    :cond_8
    iget-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    if-le v5, v7, :cond_9

    iget-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    const/16 v6, 0x2da

    if-ge v5, v6, :cond_9

    iget-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16HResolution:S

    const/16 v6, 0x4f6

    if-le v5, v6, :cond_9

    iget-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16HResolution:S

    const/16 v6, 0x50a

    if-ge v5, v6, :cond_9

    iget-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    const/16 v6, 0x2d0

    iput-short v6, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    goto/16 :goto_2

    :cond_9
    iget-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    if-le v5, v8, :cond_a

    iget-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    if-ge v5, v10, :cond_a

    iget-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16HResolution:S

    const/16 v6, 0x776

    if-le v5, v6, :cond_a

    iget-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16HResolution:S

    const/16 v6, 0x78a

    if-ge v5, v6, :cond_a

    iget-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iput-short v9, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    goto/16 :goto_2

    :cond_a
    iget-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    if-le v5, v8, :cond_b

    iget-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    if-ge v5, v10, :cond_b

    iget-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16HResolution:S

    const/16 v6, 0x3b6

    if-le v5, v6, :cond_b

    iget-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16HResolution:S

    const/16 v6, 0x3ca

    if-ge v5, v6, :cond_b

    iget-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iput-short v9, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    goto/16 :goto_2

    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_c
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mstrText:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "I"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mstrText:Ljava/lang/String;

    goto/16 :goto_3

    :cond_d
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mstrText:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v6, v6, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16HResolution:S

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "X"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v6, v6, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mstrText:Ljava/lang/String;

    goto/16 :goto_3

    :cond_e
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mstrText:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v6, v6, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16HResolution:S

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "X"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v6, v6, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mstrText:Ljava/lang/String;

    goto/16 :goto_3

    :pswitch_7
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mstrText:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-short v6, v6, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mstrText:Ljava/lang/String;

    iget-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mVideoInfo:Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;

    iget-object v5, v5, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->enScanType:Lcom/konka/kkinterface/tv/CommonDesk$EN_SCAN_TYPE;

    sget-object v6, Lcom/konka/kkinterface/tv/CommonDesk$EN_SCAN_TYPE;->E_PROGRESSIVE:Lcom/konka/kkinterface/tv/CommonDesk$EN_SCAN_TYPE;

    if-ne v5, v6, :cond_f

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mstrText:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "P"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mstrText:Ljava/lang/String;

    :goto_4
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mstrText:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "@"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "HZ"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mstrText:Ljava/lang/String;

    goto/16 :goto_0

    :cond_f
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mstrText:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "I"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mstrText:Ljava/lang/String;

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_7
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onCreate()Z
    .locals 4

    iget-object v1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v1}, Lcom/konka/factory/desk/IFactoryDesk;->getOverScanSourceType()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v1

    iput v1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->sourceindex:I

    iget-object v1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->overScanActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v1}, Lcom/konka/factory/MainmenuActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/konka/factory/TvRootApp;

    invoke-virtual {v0}, Lcom/konka/factory/TvRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    iget-object v1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->channelDesk:Lcom/konka/kkinterface/tv/ChannelDesk;

    iget-object v1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->commonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->GetCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->meInputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    const-string v1, ""

    iput-object v1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mstrText:Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->meInputSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {p0, v1}, Lcom/konka/factory/OverScanAdjustViewHolder;->getVideoInfo(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V

    iget-object v1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->text_factory_overscan_source_res:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->mstrText:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v1}, Lcom/konka/factory/desk/IFactoryDesk;->getOverScanHsize()S

    move-result v1

    iput v1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->hsizeval:I

    iget-object v1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v1}, Lcom/konka/factory/desk/IFactoryDesk;->getOverScanHposition()S

    move-result v1

    iput v1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->hpositionval:I

    iget-object v1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v1}, Lcom/konka/factory/desk/IFactoryDesk;->getOverScanVsize()S

    move-result v1

    iput v1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->vsizeval:I

    iget-object v1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v1}, Lcom/konka/factory/desk/IFactoryDesk;->getOverScanVposition()S

    move-result v1

    iput v1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->vpositionval:I

    iget-object v1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->text_factory_overscan_source_val:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->sourcearray:[Ljava/lang/String;

    iget v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->sourceindex:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->text_factory_overscan_hsize_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->hsizeval:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->text_factory_overscan_hposition_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->hpositionval:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->text_factory_overscan_vsize_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->vsizeval:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->text_factory_overscan_vpositon_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->vpositionval:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->progress_factory_overscan_hsize:Landroid/widget/ProgressBar;

    iget v2, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->hsizeval:I

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->progress_factory_overscan_hposition:Landroid/widget/ProgressBar;

    iget v2, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->hpositionval:I

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->progress_factory_overscan_vsize:Landroid/widget/ProgressBar;

    iget v2, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->vsizeval:I

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v1, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->progress_factory_overscan_vpositon:Landroid/widget/ProgressBar;

    iget v2, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->vpositionval:I

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    invoke-virtual {p0}, Lcom/konka/factory/OverScanAdjustViewHolder;->DisableSourceLinear()V

    const/4 v1, 0x1

    return v1
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v4, 0x64

    const/4 v0, 0x1

    iget-object v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->overScanActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v3}, Lcom/konka/factory/MainmenuActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getId()I

    move-result v1

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2}, Ljava/lang/String;-><init>()V

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    sparse-switch v1, :sswitch_data_1

    goto :goto_0

    :sswitch_1
    iget v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->hsizeval:I

    if-eq v3, v4, :cond_0

    iget v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->hsizeval:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->hsizeval:I

    :cond_0
    iget v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->hsizeval:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->text_factory_overscan_hsize_val:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->progress_factory_overscan_hsize:Landroid/widget/ProgressBar;

    iget v4, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->hsizeval:I

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->hsizeval:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setOverScanHsize(S)Z

    goto :goto_0

    :sswitch_2
    iget v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->hpositionval:I

    if-eq v3, v4, :cond_1

    iget v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->hpositionval:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->hpositionval:I

    :cond_1
    iget v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->hpositionval:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->text_factory_overscan_hposition_val:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->progress_factory_overscan_hposition:Landroid/widget/ProgressBar;

    iget v4, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->hpositionval:I

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->hpositionval:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setOverScanHposition(S)Z

    goto :goto_0

    :sswitch_3
    iget v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->vsizeval:I

    if-eq v3, v4, :cond_2

    iget v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->vsizeval:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->vsizeval:I

    :cond_2
    iget v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->vsizeval:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->text_factory_overscan_vsize_val:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->progress_factory_overscan_vsize:Landroid/widget/ProgressBar;

    iget v4, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->vsizeval:I

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->vsizeval:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setOverScanVsize(S)Z

    goto :goto_0

    :sswitch_4
    iget v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->vpositionval:I

    if-eq v3, v4, :cond_3

    iget v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->vpositionval:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->vpositionval:I

    :cond_3
    iget v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->vpositionval:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->text_factory_overscan_vpositon_val:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->progress_factory_overscan_vpositon:Landroid/widget/ProgressBar;

    iget v4, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->vpositionval:I

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->vpositionval:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setOverScanVposition(S)Z

    goto/16 :goto_0

    :sswitch_5
    sparse-switch v1, :sswitch_data_2

    goto/16 :goto_0

    :sswitch_6
    iget v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->hsizeval:I

    if-eqz v3, :cond_4

    iget v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->hsizeval:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->hsizeval:I

    :cond_4
    iget v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->hsizeval:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->text_factory_overscan_hsize_val:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->progress_factory_overscan_hsize:Landroid/widget/ProgressBar;

    iget v4, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->hsizeval:I

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->hsizeval:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setOverScanHsize(S)Z

    goto/16 :goto_0

    :sswitch_7
    iget v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->hpositionval:I

    if-eqz v3, :cond_5

    iget v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->hpositionval:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->hpositionval:I

    :cond_5
    iget v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->hpositionval:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->text_factory_overscan_hposition_val:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->progress_factory_overscan_hposition:Landroid/widget/ProgressBar;

    iget v4, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->hpositionval:I

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->hpositionval:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setOverScanHposition(S)Z

    goto/16 :goto_0

    :sswitch_8
    iget v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->vsizeval:I

    if-eqz v3, :cond_6

    iget v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->vsizeval:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->vsizeval:I

    :cond_6
    iget v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->vsizeval:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->text_factory_overscan_vsize_val:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->progress_factory_overscan_vsize:Landroid/widget/ProgressBar;

    iget v4, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->vsizeval:I

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->vsizeval:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setOverScanVsize(S)Z

    goto/16 :goto_0

    :sswitch_9
    iget v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->vpositionval:I

    if-eqz v3, :cond_7

    iget v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->vpositionval:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->vpositionval:I

    :cond_7
    iget v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->vpositionval:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->text_factory_overscan_vpositon_val:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->progress_factory_overscan_vpositon:Landroid/widget/ProgressBar;

    iget v4, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->vpositionval:I

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->vpositionval:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setOverScanVposition(S)Z

    goto/16 :goto_0

    :sswitch_a
    iget-object v3, p0, Lcom/konka/factory/OverScanAdjustViewHolder;->overScanActivity:Lcom/konka/factory/MainmenuActivity;

    const/16 v4, 0xc

    invoke-virtual {v3, v4}, Lcom/konka/factory/MainmenuActivity;->returnRoot(I)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_a
        0x15 -> :sswitch_5
        0x16 -> :sswitch_0
        0x52 -> :sswitch_a
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x7f0a0186 -> :sswitch_1    # com.konka.factory.R.id.linearlayout_factory_overscan_hsize
        0x7f0a018b -> :sswitch_2    # com.konka.factory.R.id.linearlayout_factory_overscan_hposition
        0x7f0a0190 -> :sswitch_3    # com.konka.factory.R.id.linearlayout_factory_overscan_vsize
        0x7f0a0195 -> :sswitch_4    # com.konka.factory.R.id.linearlayout_factory_overscan_vposition
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x7f0a0186 -> :sswitch_6    # com.konka.factory.R.id.linearlayout_factory_overscan_hsize
        0x7f0a018b -> :sswitch_7    # com.konka.factory.R.id.linearlayout_factory_overscan_hposition
        0x7f0a0190 -> :sswitch_8    # com.konka.factory.R.id.linearlayout_factory_overscan_vsize
        0x7f0a0195 -> :sswitch_9    # com.konka.factory.R.id.linearlayout_factory_overscan_vposition
    .end sparse-switch
.end method
