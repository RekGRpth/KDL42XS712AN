.class public Lcom/konka/factory/NonStandardAdjustViewHolder;
.super Ljava/lang/Object;
.source "NonStandardAdjustViewHolder.java"


# instance fields
.field private AudioHiDevMode:[Ljava/lang/String;

.field private aefcd7highval:I

.field private afec43val:I

.field private afec44val:I

.field private afec66ival:I

.field private afec6eival:I

.field private afec6elowival:I

.field private afeca0ival:I

.field private afeca1ival:I

.field private afeccbval:I

.field private afeccfatvval:I

.field private afeccfavval:I

.field private afecd4val:I

.field private afecd5ival:I

.field private afecd7ival:I

.field private afecd8ival:I

.field private afecd9ival:I

.field private audiohidevval:I

.field private audionrval:I

.field private autoAGCMode:[Ljava/lang/String;

.field private chinaadeval:I

.field private delayval:I

.field private factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

.field private nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

.field protected text_factory_nonstandard_aefc43_val:Landroid/widget/TextView;

.field protected text_factory_nonstandard_aefc44_val:Landroid/widget/TextView;

.field protected text_factory_nonstandard_aefc66_val:Landroid/widget/TextView;

.field protected text_factory_nonstandard_aefc6e_val:Landroid/widget/TextView;

.field protected text_factory_nonstandard_aefc6elow_val:Landroid/widget/TextView;

.field protected text_factory_nonstandard_aefca0_val:Landroid/widget/TextView;

.field protected text_factory_nonstandard_aefca1_val:Landroid/widget/TextView;

.field protected text_factory_nonstandard_aefccb_val:Landroid/widget/TextView;

.field protected text_factory_nonstandard_aefcd5_val:Landroid/widget/TextView;

.field protected text_factory_nonstandard_aefcd7_val:Landroid/widget/TextView;

.field protected text_factory_nonstandard_aefcd7high_val:Landroid/widget/TextView;

.field protected text_factory_nonstandard_aefcd8_val:Landroid/widget/TextView;

.field protected text_factory_nonstandard_aefcd9_val:Landroid/widget/TextView;

.field protected text_factory_nonstandard_afeccf_atv_val:Landroid/widget/TextView;

.field protected text_factory_nonstandard_afeccf_av_val:Landroid/widget/TextView;

.field protected text_factory_nonstandard_afecd4_val:Landroid/widget/TextView;

.field protected text_factory_nonstandard_audiohidev_val:Landroid/widget/TextView;

.field protected text_factory_nonstandard_audionr_val:Landroid/widget/TextView;

.field protected text_factory_nonstandard_chinadescrambler_val:Landroid/widget/TextView;

.field protected text_factory_nonstandard_delay_val:Landroid/widget/TextView;

.field protected text_factory_nonstandard_factory_nonstandard_vifvgamaximum_val:Landroid/widget/TextView;

.field protected text_factory_nonstandard_gaindistributionthr_val:Landroid/widget/TextView;

.field protected text_factory_nonstandard_vifagcref_val:Landroid/widget/TextView;

.field protected text_factory_nonstandard_vifasiasignaloption_val:Landroid/widget/TextView;

.field protected text_factory_nonstandard_vifclampgain_val:Landroid/widget/TextView;

.field protected text_factory_nonstandard_vifcpki_val:Landroid/widget/TextView;

.field protected text_factory_nonstandard_vifcpkp_val:Landroid/widget/TextView;

.field protected text_factory_nonstandard_vifcpkpkiadjust_val:Landroid/widget/TextView;

.field protected text_factory_nonstandard_vifcrthr_val:Landroid/widget/TextView;

.field protected text_factory_nonstandard_vifovermodulation_val:Landroid/widget/TextView;

.field protected text_factory_nonstandard_viftop_val:Landroid/widget/TextView;

.field protected text_factory_nonstandard_vifversion_val:Landroid/widget/TextView;

.field private vifagcval:I

.field private vifasiaval:I

.field private vifclampval:I

.field private vifcpkival:I

.field private vifcpkpkival:I

.field private vifcpkpval:I

.field private vifcrthr:I

.field private vifgainindval:I

.field private vifoverval:I

.field private viftopval:I

.field private vifverval:I

.field private vifvgamaxval:I


# direct methods
.method public constructor <init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V
    .locals 6
    .param p1    # Lcom/konka/factory/MainmenuActivity;
    .param p2    # Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/16 v0, 0x80

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->viftopval:I

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifvgamaxval:I

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkpval:I

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkival:I

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifasiaval:I

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkpkival:I

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifoverval:I

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifclampval:I

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->chinaadeval:I

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->delayval:I

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcrthr:I

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifverval:I

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifagcval:I

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifgainindval:I

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd4val:I

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd5ival:I

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd8ival:I

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd9ival:I

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd7ival:I

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeca0ival:I

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeca1ival:I

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec66ival:I

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec6eival:I

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec6elowival:I

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec43val:I

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec44val:I

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccbval:I

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccfatvval:I

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccfavval:I

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->audiohidevval:I

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->audionrval:I

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->aefcd7highval:I

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "OFF"

    aput-object v1, v0, v3

    const-string v1, "LV1"

    aput-object v1, v0, v4

    const-string v1, "LV2"

    aput-object v1, v0, v5

    const/4 v1, 0x3

    const-string v2, "LV3"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->AudioHiDevMode:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "OFF"

    aput-object v1, v0, v3

    const-string v1, "ON"

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->autoAGCMode:[Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

    iput-object p2, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    return-void
.end method


# virtual methods
.method findView()V
    .locals 2

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a00ec    # com.konka.factory.R.id.textview_factory_nonstandard_viftop_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_viftop_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a00ef    # com.konka.factory.R.id.textview_factory_nonstandard_factory_nonstandard_vifvgamaximum_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_factory_nonstandard_vifvgamaximum_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a00f2    # com.konka.factory.R.id.textview_factory_nonstandard_vifcpkp_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifcpkp_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a00f5    # com.konka.factory.R.id.textview_factory_nonstandard_vifcpki_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifcpki_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a00e0    # com.konka.factory.R.id.textview_factory_nonstandard_vifasiasignaloption_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifasiasignaloption_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a00f8    # com.konka.factory.R.id.textview_factory_nonstandard_vifcpkpkiadjust_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifcpkpkiadjust_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a00fb    # com.konka.factory.R.id.textview_factory_nonstandard_vifovermodulation_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifovermodulation_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a00fe    # com.konka.factory.R.id.textview_factory_nonstandard_vifclampgain_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifclampgain_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a00e3    # com.konka.factory.R.id.textview_factory_nonstandard_chinadescrambler_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_chinadescrambler_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0101    # com.konka.factory.R.id.textview_factory_nonstandard_delay_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_delay_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0104    # com.konka.factory.R.id.textview_factory_nonstandard_vifcrthr_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifcrthr_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0107    # com.konka.factory.R.id.textview_factory_nonstandard_vifversion_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifversion_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a010a    # com.konka.factory.R.id.textview_factory_nonstandard_vifagcref_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifagcref_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a010d    # com.konka.factory.R.id.textview_factory_nonstandard_gaindistributionthr_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_gaindistributionthr_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0110    # com.konka.factory.R.id.textview_factory_nonstandard_afecd4_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_afecd4_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0113    # com.konka.factory.R.id.textview_factory_nonstandard_aefcd5_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefcd5_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0116    # com.konka.factory.R.id.textview_factory_nonstandard_aefcd8_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefcd8_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0119    # com.konka.factory.R.id.textview_factory_nonstandard_aefcd9_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefcd9_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a011f    # com.konka.factory.R.id.textview_factory_nonstandard_aefcd7_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefcd7_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0122    # com.konka.factory.R.id.textview_factory_nonstandard_aefca0_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefca0_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0125    # com.konka.factory.R.id.textview_factory_nonstandard_aefca1_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefca1_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0128    # com.konka.factory.R.id.textview_factory_nonstandard_aefc66_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefc66_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a012b    # com.konka.factory.R.id.textview_factory_nonstandard_aefc6e_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefc6e_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a012e    # com.konka.factory.R.id.textview_factory_nonstandard_aefc6elow_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefc6elow_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0131    # com.konka.factory.R.id.textview_factory_nonstandard_aefc43_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefc43_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0134    # com.konka.factory.R.id.textview_factory_nonstandard_aefc44_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefc44_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0137    # com.konka.factory.R.id.textview_factory_nonstandard_aefccb_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefccb_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a013a    # com.konka.factory.R.id.textview_factory_nonstandard_afeccf_atv_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_afeccf_atv_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a013d    # com.konka.factory.R.id.textview_factory_nonstandard_afeccf_av_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_afeccf_av_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a00e6    # com.konka.factory.R.id.textview_factory_nonstandard_audiohidev_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_audiohidev_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a00e9    # com.konka.factory.R.id.textview_factory_nonstandard_audionr_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_audionr_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a011c    # com.konka.factory.R.id.textview_factory_nonstandard_aefcd7high_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefcd7high_val:Landroid/widget/TextView;

    return-void
.end method

.method public onCreate()Z
    .locals 4

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getVifTop()S

    move-result v0

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->viftopval:I

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getVifVgaMaximum()I

    move-result v0

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifvgamaxval:I

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getVifCrKp()S

    move-result v0

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkpval:I

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getVifCrKi()S

    move-result v0

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkival:I

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getVifAsiaSignalOption()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifasiaval:I

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getVifCrKpKiAdjust()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkpkival:I

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getVifOverModulation()Z

    move-result v0

    if-eqz v0, :cond_0

    move v2, v1

    :cond_0
    iput v2, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifoverval:I

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getVifClampGainOvNegative()I

    move-result v0

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifclampval:I

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getChinaDescramblerBox()S

    move-result v0

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->chinaadeval:I

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getDelayReduce()S

    move-result v0

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->delayval:I

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getVifCrThr()I

    move-result v0

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcrthr:I

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getVifVersion()S

    move-result v0

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifverval:I

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getVifAgcRef()S

    move-result v0

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifagcval:I

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getGainDistributionThr()I

    move-result v0

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifgainindval:I

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getAEFC_D4()S

    move-result v0

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd4val:I

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getAEFC_D5Bit2()S

    move-result v0

    shr-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd5ival:I

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getAEFC_D8Bit3210()S

    move-result v0

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd8ival:I

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getAEFC_D9Bit0()S

    move-result v0

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd9ival:I

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getAEFC_D7HighBoun()S

    move-result v0

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->aefcd7highval:I

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getAEFC_D7LowBoun()S

    move-result v0

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd7ival:I

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getAEFC_A0()S

    move-result v0

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeca0ival:I

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getAEFC_A1()S

    move-result v0

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeca1ival:I

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getAEFC_66Bit76()S

    move-result v0

    shr-int/lit8 v0, v0, 0x6

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec66ival:I

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getAEFC_6EBit7654()S

    move-result v0

    shr-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec6eival:I

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getAEFC_6EBit3210()S

    move-result v0

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec6elowival:I

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getAEFC_43()S

    move-result v0

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec43val:I

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getAEFC_44()S

    move-result v0

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec44val:I

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getAEFC_CB()S

    move-result v0

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccbval:I

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getAEFC_CFBit2_ATV()S

    move-result v0

    shr-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccfatvval:I

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getAEFC_CFBit2_AV()S

    move-result v0

    shr-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccfavval:I

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getAudioHiDevMode()I

    move-result v0

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->audiohidevval:I

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getAudioNrThr()S

    move-result v0

    iput v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->audionrval:I

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_viftop_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->viftopval:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_factory_nonstandard_vifvgamaximum_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifvgamaxval:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifcpkp_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkpval:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifcpki_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkival:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifasiasignaloption_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifasiaval:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifcpkpkiadjust_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkpkival:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifovermodulation_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifoverval:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifclampgain_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifclampval:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_chinadescrambler_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->chinaadeval:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_delay_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->delayval:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifcrthr_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcrthr:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifversion_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifverval:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifagcref_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifagcval:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_gaindistributionthr_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifgainindval:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_afecd4_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd4val:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefcd5_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd5ival:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefcd8_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd8ival:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefcd9_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd9ival:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefcd7_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd7ival:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefca0_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeca0ival:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefca1_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeca1ival:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefc66_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec66ival:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefc6e_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec6eival:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefc6elow_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec6elowival:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefc43_val:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->autoAGCMode:[Ljava/lang/String;

    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec43val:I

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefc44_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec44val:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefccb_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccbval:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_afeccf_atv_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccfatvval:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_afeccf_av_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccfavval:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_audiohidev_val:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->AudioHiDevMode:[Ljava/lang/String;

    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->audiohidevval:I

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_audionr_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->audionrval:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefcd7high_val:Landroid/widget/TextView;

    iget v2, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->aefcd7highval:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return v1

    :cond_1
    move v0, v2

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto/16 :goto_1
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v7, 0x3

    const/16 v6, 0xf

    const/4 v1, 0x1

    const/16 v5, 0xff

    const/4 v3, 0x0

    const/4 v0, 0x1

    iget-object v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v4}, Lcom/konka/factory/MainmenuActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v2

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    sparse-switch v2, :sswitch_data_1

    goto :goto_0

    :sswitch_1
    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifasiaval:I

    if-nez v4, :cond_4

    iput v1, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifasiaval:I

    :goto_1
    iget-object v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifasiasignaloption_val:Landroid/widget/TextView;

    iget v5, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifasiaval:I

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifasiaval:I

    if-lez v4, :cond_5

    :goto_2
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v3, v1}, Lcom/konka/factory/desk/IFactoryDesk;->setVifAsiaSignalOption(Z)Z

    goto :goto_0

    :sswitch_2
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->viftopval:I

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->viftopval:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->viftopval:I

    :cond_0
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_viftop_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->viftopval:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->viftopval:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setVifTop(S)Z

    goto :goto_0

    :sswitch_3
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifvgamaxval:I

    if-eqz v3, :cond_1

    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifvgamaxval:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifvgamaxval:I

    :cond_1
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_factory_nonstandard_vifvgamaximum_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifvgamaxval:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifvgamaxval:I

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setVifVgaMaximum(I)Z

    goto :goto_0

    :sswitch_4
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkpval:I

    if-eqz v3, :cond_2

    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkpval:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkpval:I

    :cond_2
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifcpkp_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkpval:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkpval:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setVifCrKp(S)Z

    goto :goto_0

    :sswitch_5
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkival:I

    if-eqz v3, :cond_3

    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkival:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkival:I

    :cond_3
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifcpki_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkival:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkival:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setVifCrKi(S)Z

    goto/16 :goto_0

    :cond_4
    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifasiaval:I

    goto/16 :goto_1

    :cond_5
    move v1, v3

    goto/16 :goto_2

    :sswitch_6
    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkpkival:I

    if-nez v4, :cond_6

    iput v1, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkpkival:I

    :goto_3
    iget-object v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifcpkpkiadjust_val:Landroid/widget/TextView;

    iget v5, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkpkival:I

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkpkival:I

    if-lez v4, :cond_7

    :goto_4
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v3, v1}, Lcom/konka/factory/desk/IFactoryDesk;->setVifCrKpKiAdjust(Z)Z

    goto/16 :goto_0

    :cond_6
    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkpkival:I

    goto :goto_3

    :cond_7
    move v1, v3

    goto :goto_4

    :sswitch_7
    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifoverval:I

    if-nez v4, :cond_8

    iput v1, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifoverval:I

    :goto_5
    iget-object v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifovermodulation_val:Landroid/widget/TextView;

    iget v5, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifoverval:I

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifoverval:I

    if-lez v4, :cond_9

    :goto_6
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v3, v1}, Lcom/konka/factory/desk/IFactoryDesk;->setVifOverModulation(Z)Z

    goto/16 :goto_0

    :cond_8
    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifoverval:I

    goto :goto_5

    :cond_9
    move v1, v3

    goto :goto_6

    :sswitch_8
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifclampval:I

    if-eqz v3, :cond_a

    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifclampval:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifclampval:I

    :cond_a
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifclampgain_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifclampval:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifclampval:I

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setVifClampGainOvNegative(I)Z

    goto/16 :goto_0

    :sswitch_9
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->chinaadeval:I

    if-eqz v3, :cond_b

    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->chinaadeval:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->chinaadeval:I

    :cond_b
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_chinadescrambler_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->chinaadeval:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->chinaadeval:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setChinaDescramblerBox(S)Z

    goto/16 :goto_0

    :sswitch_a
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->delayval:I

    if-eqz v3, :cond_c

    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->delayval:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->delayval:I

    :cond_c
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_delay_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->delayval:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->delayval:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setDelayReduce(S)Z

    goto/16 :goto_0

    :sswitch_b
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcrthr:I

    if-eqz v3, :cond_d

    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcrthr:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcrthr:I

    :cond_d
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifcrthr_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcrthr:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcrthr:I

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setVifCrThr(I)Z

    goto/16 :goto_0

    :sswitch_c
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifverval:I

    if-eqz v3, :cond_e

    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifverval:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifverval:I

    :cond_e
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifversion_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifverval:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifverval:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setVifVersion(S)Z

    goto/16 :goto_0

    :sswitch_d
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifagcval:I

    if-eqz v3, :cond_f

    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifagcval:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifagcval:I

    :cond_f
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifagcref_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifagcval:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifagcval:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setVifAgcRef(S)Z

    goto/16 :goto_0

    :sswitch_e
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifgainindval:I

    if-eqz v3, :cond_10

    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifgainindval:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifgainindval:I

    :cond_10
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_gaindistributionthr_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifgainindval:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifgainindval:I

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setGainDistributionThr(I)Z

    goto/16 :goto_0

    :sswitch_f
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd4val:I

    if-nez v3, :cond_11

    iput v5, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd4val:I

    :goto_7
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_afecd4_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd4val:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd4val:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAEFC_D4(S)Z

    goto/16 :goto_0

    :cond_11
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd4val:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd4val:I

    goto :goto_7

    :sswitch_10
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd5ival:I

    if-nez v3, :cond_12

    iput v1, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd5ival:I

    :goto_8
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefcd5_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd5ival:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd5ival:I

    shl-int/lit8 v4, v4, 0x2

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAEFC_D5Bit2(S)Z

    goto/16 :goto_0

    :cond_12
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd5ival:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd5ival:I

    goto :goto_8

    :sswitch_11
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd8ival:I

    if-nez v3, :cond_13

    iput v6, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd8ival:I

    :goto_9
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefcd8_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd8ival:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd8ival:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAEFC_D8Bit3210(S)Z

    goto/16 :goto_0

    :cond_13
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd8ival:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd8ival:I

    goto :goto_9

    :sswitch_12
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd9ival:I

    if-nez v3, :cond_14

    iput v1, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd9ival:I

    :goto_a
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefcd9_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd9ival:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd9ival:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAEFC_D9Bit0(S)Z

    goto/16 :goto_0

    :cond_14
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd9ival:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd9ival:I

    goto :goto_a

    :sswitch_13
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd7ival:I

    if-nez v3, :cond_15

    iput v5, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd7ival:I

    :goto_b
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefcd7_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd7ival:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd7ival:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAEFC_D7LowBoun(S)Z

    goto/16 :goto_0

    :cond_15
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd7ival:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd7ival:I

    goto :goto_b

    :sswitch_14
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeca0ival:I

    if-nez v3, :cond_16

    iput v5, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeca0ival:I

    :goto_c
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefca0_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeca0ival:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeca0ival:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAEFC_A0(S)Z

    goto/16 :goto_0

    :cond_16
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeca0ival:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeca0ival:I

    goto :goto_c

    :sswitch_15
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeca1ival:I

    if-nez v3, :cond_17

    iput v5, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeca1ival:I

    :goto_d
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefca1_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeca1ival:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeca1ival:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAEFC_A1(S)Z

    goto/16 :goto_0

    :cond_17
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeca1ival:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeca1ival:I

    goto :goto_d

    :sswitch_16
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec66ival:I

    if-nez v3, :cond_18

    iput v7, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec66ival:I

    :goto_e
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefc66_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec66ival:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec66ival:I

    shl-int/lit8 v4, v4, 0x6

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAEFC_66Bit76(S)Z

    goto/16 :goto_0

    :cond_18
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec66ival:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec66ival:I

    goto :goto_e

    :sswitch_17
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec6eival:I

    if-nez v3, :cond_19

    iput v6, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec6eival:I

    :goto_f
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefc6e_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec6eival:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec6eival:I

    shl-int/lit8 v4, v4, 0x4

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAEFC_6EBit7654(S)Z

    goto/16 :goto_0

    :cond_19
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec6eival:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec6eival:I

    goto :goto_f

    :sswitch_18
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec6elowival:I

    if-nez v3, :cond_1a

    iput v6, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec6elowival:I

    :goto_10
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefc6elow_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec6elowival:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec6elowival:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAEFC_6EBit3210(S)Z

    goto/16 :goto_0

    :cond_1a
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec6elowival:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec6elowival:I

    goto :goto_10

    :sswitch_19
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec43val:I

    if-nez v3, :cond_1b

    iput v1, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec43val:I

    :goto_11
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefc43_val:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->autoAGCMode:[Ljava/lang/String;

    iget v5, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec43val:I

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec43val:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAEFC_43(S)Z

    goto/16 :goto_0

    :cond_1b
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec43val:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec43val:I

    goto :goto_11

    :sswitch_1a
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec44val:I

    if-nez v3, :cond_1c

    iput v5, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec44val:I

    :goto_12
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefc44_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec44val:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec44val:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAEFC_44(S)Z

    goto/16 :goto_0

    :cond_1c
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec44val:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec44val:I

    goto :goto_12

    :sswitch_1b
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccbval:I

    if-nez v3, :cond_1d

    iput v5, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccbval:I

    :goto_13
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefccb_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccbval:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccbval:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAEFC_CB(S)Z

    goto/16 :goto_0

    :cond_1d
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccbval:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccbval:I

    goto :goto_13

    :sswitch_1c
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccfatvval:I

    if-nez v3, :cond_1e

    iput v1, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccfatvval:I

    :goto_14
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_afeccf_atv_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccfatvval:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccfatvval:I

    shl-int/lit8 v4, v4, 0x2

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAEFC_CFBit2_ATV(S)Z

    goto/16 :goto_0

    :cond_1e
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccfatvval:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccfatvval:I

    goto :goto_14

    :sswitch_1d
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccfavval:I

    if-nez v3, :cond_1f

    iput v1, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccfavval:I

    :goto_15
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_afeccf_av_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccfavval:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccfavval:I

    shl-int/lit8 v4, v4, 0x2

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAEFC_CFBit2_AV(S)Z

    goto/16 :goto_0

    :cond_1f
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccfavval:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccfavval:I

    goto :goto_15

    :sswitch_1e
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->audiohidevval:I

    if-eqz v3, :cond_20

    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->audiohidevval:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->audiohidevval:I

    :goto_16
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_audiohidev_val:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->AudioHiDevMode:[Ljava/lang/String;

    iget v5, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->audiohidevval:I

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->audiohidevval:I

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAudioHiDevMode(I)Z

    goto/16 :goto_0

    :cond_20
    iput v7, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->audiohidevval:I

    goto :goto_16

    :sswitch_1f
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->audionrval:I

    if-eqz v3, :cond_21

    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->audionrval:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->audionrval:I

    :cond_21
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_audionr_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->audionrval:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->audionrval:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAudioNrThr(S)Z

    goto/16 :goto_0

    :sswitch_20
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->aefcd7highval:I

    if-nez v3, :cond_22

    iput v5, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->aefcd7highval:I

    :goto_17
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefcd7high_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->aefcd7highval:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->aefcd7highval:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAEFC_D7HighBoun(S)Z

    goto/16 :goto_0

    :cond_22
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->aefcd7highval:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->aefcd7highval:I

    goto :goto_17

    :sswitch_21
    sparse-switch v2, :sswitch_data_2

    goto/16 :goto_0

    :sswitch_22
    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifasiaval:I

    if-nez v4, :cond_27

    iput v1, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifasiaval:I

    :goto_18
    iget-object v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifasiasignaloption_val:Landroid/widget/TextView;

    iget v5, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifasiaval:I

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifasiaval:I

    if-lez v4, :cond_28

    :goto_19
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v3, v1}, Lcom/konka/factory/desk/IFactoryDesk;->setVifAsiaSignalOption(Z)Z

    goto/16 :goto_0

    :sswitch_23
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->viftopval:I

    if-eq v3, v5, :cond_23

    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->viftopval:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->viftopval:I

    :cond_23
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_viftop_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->viftopval:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->viftopval:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setVifTop(S)Z

    goto/16 :goto_0

    :sswitch_24
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifvgamaxval:I

    if-eq v3, v5, :cond_24

    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifvgamaxval:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifvgamaxval:I

    :cond_24
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_factory_nonstandard_vifvgamaximum_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifvgamaxval:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifvgamaxval:I

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setVifVgaMaximum(I)Z

    goto/16 :goto_0

    :sswitch_25
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkpval:I

    if-eq v3, v5, :cond_25

    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkpval:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkpval:I

    :cond_25
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifcpkp_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkpval:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkpval:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setVifCrKp(S)Z

    goto/16 :goto_0

    :sswitch_26
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkival:I

    if-eq v3, v5, :cond_26

    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkival:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkival:I

    :cond_26
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifcpki_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkival:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkival:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setVifCrKi(S)Z

    goto/16 :goto_0

    :cond_27
    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifasiaval:I

    goto/16 :goto_18

    :cond_28
    move v1, v3

    goto/16 :goto_19

    :sswitch_27
    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkpkival:I

    if-nez v4, :cond_29

    iput v1, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkpkival:I

    :goto_1a
    iget-object v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifcpkpkiadjust_val:Landroid/widget/TextView;

    iget v5, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkpkival:I

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkpkival:I

    if-lez v4, :cond_2a

    :goto_1b
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v3, v1}, Lcom/konka/factory/desk/IFactoryDesk;->setVifCrKpKiAdjust(Z)Z

    goto/16 :goto_0

    :cond_29
    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkpkival:I

    goto :goto_1a

    :cond_2a
    move v1, v3

    goto :goto_1b

    :sswitch_28
    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifoverval:I

    if-nez v4, :cond_2b

    iput v1, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifoverval:I

    :goto_1c
    iget-object v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifovermodulation_val:Landroid/widget/TextView;

    iget v5, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifoverval:I

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcpkpkival:I

    if-lez v4, :cond_2c

    :goto_1d
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v3, v1}, Lcom/konka/factory/desk/IFactoryDesk;->setVifOverModulation(Z)Z

    goto/16 :goto_0

    :cond_2b
    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifoverval:I

    goto :goto_1c

    :cond_2c
    move v1, v3

    goto :goto_1d

    :sswitch_29
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifclampval:I

    if-eq v3, v5, :cond_2d

    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifclampval:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifclampval:I

    :cond_2d
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifclampgain_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifclampval:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifclampval:I

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setVifClampGainOvNegative(I)Z

    goto/16 :goto_0

    :sswitch_2a
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->chinaadeval:I

    if-eq v3, v5, :cond_2e

    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->chinaadeval:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->chinaadeval:I

    :cond_2e
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_chinadescrambler_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->chinaadeval:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->chinaadeval:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setChinaDescramblerBox(S)Z

    goto/16 :goto_0

    :sswitch_2b
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->delayval:I

    if-eq v3, v5, :cond_2f

    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->delayval:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->delayval:I

    :cond_2f
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_delay_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->delayval:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->delayval:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setDelayReduce(S)Z

    goto/16 :goto_0

    :sswitch_2c
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcrthr:I

    if-eq v3, v5, :cond_30

    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcrthr:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcrthr:I

    :cond_30
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifcrthr_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcrthr:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifcrthr:I

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setVifCrThr(I)Z

    goto/16 :goto_0

    :sswitch_2d
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifverval:I

    if-eq v3, v5, :cond_31

    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifverval:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifverval:I

    :cond_31
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifversion_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifverval:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifverval:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setVifVersion(S)Z

    goto/16 :goto_0

    :sswitch_2e
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifagcval:I

    if-eq v3, v5, :cond_32

    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifagcval:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifagcval:I

    :cond_32
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_vifagcref_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifagcval:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifagcval:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setVifAgcRef(S)Z

    goto/16 :goto_0

    :sswitch_2f
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifgainindval:I

    if-eq v3, v5, :cond_33

    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifgainindval:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifgainindval:I

    :cond_33
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_gaindistributionthr_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifgainindval:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->vifgainindval:I

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setGainDistributionThr(I)Z

    goto/16 :goto_0

    :sswitch_30
    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd4val:I

    if-ne v4, v5, :cond_34

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd4val:I

    :goto_1e
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_afecd4_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd4val:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd4val:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAEFC_D4(S)Z

    goto/16 :goto_0

    :cond_34
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd4val:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd4val:I

    goto :goto_1e

    :sswitch_31
    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd5ival:I

    if-ne v4, v1, :cond_35

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd5ival:I

    :goto_1f
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefcd5_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd5ival:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd5ival:I

    shl-int/lit8 v4, v4, 0x2

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAEFC_D5Bit2(S)Z

    goto/16 :goto_0

    :cond_35
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd5ival:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd5ival:I

    goto :goto_1f

    :sswitch_32
    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd8ival:I

    if-ne v4, v6, :cond_36

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd8ival:I

    :goto_20
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefcd8_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd8ival:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd8ival:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAEFC_D8Bit3210(S)Z

    goto/16 :goto_0

    :cond_36
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd8ival:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd8ival:I

    goto :goto_20

    :sswitch_33
    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd9ival:I

    if-ne v4, v1, :cond_37

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd9ival:I

    :goto_21
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefcd9_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd9ival:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd9ival:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAEFC_D9Bit0(S)Z

    goto/16 :goto_0

    :cond_37
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd9ival:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd9ival:I

    goto :goto_21

    :sswitch_34
    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd7ival:I

    if-ne v4, v5, :cond_38

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd7ival:I

    :goto_22
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefcd7_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd7ival:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd7ival:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAEFC_D7LowBoun(S)Z

    goto/16 :goto_0

    :cond_38
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd7ival:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afecd7ival:I

    goto :goto_22

    :sswitch_35
    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeca0ival:I

    if-ne v4, v5, :cond_39

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeca0ival:I

    :goto_23
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefca0_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeca0ival:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeca0ival:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAEFC_A0(S)Z

    goto/16 :goto_0

    :cond_39
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeca0ival:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeca0ival:I

    goto :goto_23

    :sswitch_36
    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeca1ival:I

    if-ne v4, v5, :cond_3a

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeca1ival:I

    :goto_24
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefca1_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeca1ival:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeca1ival:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAEFC_A1(S)Z

    goto/16 :goto_0

    :cond_3a
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeca1ival:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeca1ival:I

    goto :goto_24

    :sswitch_37
    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec66ival:I

    if-ne v4, v7, :cond_3b

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec66ival:I

    :goto_25
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefc66_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec66ival:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec66ival:I

    shl-int/lit8 v4, v4, 0x6

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAEFC_66Bit76(S)Z

    goto/16 :goto_0

    :cond_3b
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec66ival:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec66ival:I

    goto :goto_25

    :sswitch_38
    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec6eival:I

    if-ne v4, v6, :cond_3c

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec6eival:I

    :goto_26
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefc6e_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec6eival:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec6eival:I

    shl-int/lit8 v4, v4, 0x4

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAEFC_6EBit7654(S)Z

    goto/16 :goto_0

    :cond_3c
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec6eival:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec6eival:I

    goto :goto_26

    :sswitch_39
    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec6elowival:I

    if-ne v4, v6, :cond_3d

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec6elowival:I

    :goto_27
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefc6elow_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec6elowival:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec6elowival:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAEFC_6EBit3210(S)Z

    goto/16 :goto_0

    :cond_3d
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec6elowival:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec6elowival:I

    goto :goto_27

    :sswitch_3a
    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec43val:I

    if-ne v4, v1, :cond_3e

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec43val:I

    :goto_28
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefc43_val:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->autoAGCMode:[Ljava/lang/String;

    iget v5, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec43val:I

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec43val:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAEFC_43(S)Z

    goto/16 :goto_0

    :cond_3e
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec43val:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec43val:I

    goto :goto_28

    :sswitch_3b
    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec44val:I

    if-ne v4, v5, :cond_3f

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec44val:I

    :goto_29
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefc44_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec44val:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec44val:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAEFC_44(S)Z

    goto/16 :goto_0

    :cond_3f
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec44val:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afec44val:I

    goto :goto_29

    :sswitch_3c
    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccbval:I

    if-ne v4, v5, :cond_40

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccbval:I

    :goto_2a
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefccb_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccbval:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccbval:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAEFC_CB(S)Z

    goto/16 :goto_0

    :cond_40
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccbval:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccbval:I

    goto :goto_2a

    :sswitch_3d
    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccfatvval:I

    if-ne v4, v1, :cond_41

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccfatvval:I

    :goto_2b
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_afeccf_atv_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccfatvval:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccfatvval:I

    shl-int/lit8 v4, v4, 0x2

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAEFC_CFBit2_ATV(S)Z

    goto/16 :goto_0

    :cond_41
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccfatvval:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccfatvval:I

    goto :goto_2b

    :sswitch_3e
    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccfavval:I

    if-ne v4, v1, :cond_42

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccfavval:I

    :goto_2c
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_afeccf_av_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccfavval:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccfavval:I

    shl-int/lit8 v4, v4, 0x2

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAEFC_CFBit2_AV(S)Z

    goto/16 :goto_0

    :cond_42
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccfavval:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->afeccfavval:I

    goto :goto_2c

    :sswitch_3f
    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->audiohidevval:I

    if-eq v4, v7, :cond_43

    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->audiohidevval:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->audiohidevval:I

    :goto_2d
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_audiohidev_val:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->AudioHiDevMode:[Ljava/lang/String;

    iget v5, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->audiohidevval:I

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->audiohidevval:I

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAudioHiDevMode(I)Z

    goto/16 :goto_0

    :cond_43
    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->audiohidevval:I

    goto :goto_2d

    :sswitch_40
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->audionrval:I

    if-eq v3, v5, :cond_44

    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->audionrval:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->audionrval:I

    :cond_44
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_audionr_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->audionrval:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->audionrval:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAudioNrThr(S)Z

    goto/16 :goto_0

    :sswitch_41
    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->aefcd7highval:I

    if-ne v4, v5, :cond_45

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->aefcd7highval:I

    :goto_2e
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->text_factory_nonstandard_aefcd7high_val:Landroid/widget/TextView;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->aefcd7highval:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v4, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->aefcd7highval:I

    int-to-short v4, v4

    invoke-interface {v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setAEFC_D7HighBoun(S)Z

    goto/16 :goto_0

    :cond_45
    iget v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->aefcd7highval:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->aefcd7highval:I

    goto :goto_2e

    :sswitch_42
    iget-object v3, p0, Lcom/konka/factory/NonStandardAdjustViewHolder;->nonStandardActivity:Lcom/konka/factory/MainmenuActivity;

    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Lcom/konka/factory/MainmenuActivity;->returnRoot(I)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_42
        0x15 -> :sswitch_0
        0x16 -> :sswitch_21
        0x52 -> :sswitch_42
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x7f0a00de -> :sswitch_1    # com.konka.factory.R.id.linearlayout_factory_nonstandard_vifasiasignaloption
        0x7f0a00e1 -> :sswitch_9    # com.konka.factory.R.id.linearlayout_factory_nonstandard_chinadescrambler
        0x7f0a00e4 -> :sswitch_1e    # com.konka.factory.R.id.linearlayout_factory_nonstandard_audiohidev
        0x7f0a00e7 -> :sswitch_1f    # com.konka.factory.R.id.linearlayout_factory_nonstandard_audionr
        0x7f0a00ea -> :sswitch_2    # com.konka.factory.R.id.linearlayout_factory_nonstandard_viftop
        0x7f0a00ed -> :sswitch_3    # com.konka.factory.R.id.linearlayout_factory_nonstandard_vifvgamaximum
        0x7f0a00f0 -> :sswitch_4    # com.konka.factory.R.id.linearlayout_factory_nonstandard_vifcpkp
        0x7f0a00f3 -> :sswitch_5    # com.konka.factory.R.id.linearlayout_factory_nonstandard_vifcpki
        0x7f0a00f6 -> :sswitch_6    # com.konka.factory.R.id.linearlayout_factory_nonstandard_vifcpkpkiadjust
        0x7f0a00f9 -> :sswitch_7    # com.konka.factory.R.id.linearlayout_factory_nonstandard_vifovermodulation
        0x7f0a00fc -> :sswitch_8    # com.konka.factory.R.id.linearlayout_factory_nonstandard_vifclampgain
        0x7f0a00ff -> :sswitch_a    # com.konka.factory.R.id.linearlayout_factory_nonstandard_delay
        0x7f0a0102 -> :sswitch_b    # com.konka.factory.R.id.linearlayout_factory_nonstandard_vifcrthr
        0x7f0a0105 -> :sswitch_c    # com.konka.factory.R.id.linearlayout_factory_nonstandard_vifversion
        0x7f0a0108 -> :sswitch_d    # com.konka.factory.R.id.linearlayout_factory_nonstandard_vifagcref
        0x7f0a010b -> :sswitch_e    # com.konka.factory.R.id.linearlayout_factory_nonstandard_gaindistributionthr
        0x7f0a010e -> :sswitch_f    # com.konka.factory.R.id.linearlayout_factory_nonstandard_afecd4
        0x7f0a0111 -> :sswitch_10    # com.konka.factory.R.id.linearlayout_factory_nonstandard_aefcd5
        0x7f0a0114 -> :sswitch_11    # com.konka.factory.R.id.linearlayout_factory_nonstandard_aefcd8
        0x7f0a0117 -> :sswitch_12    # com.konka.factory.R.id.linearlayout_factory_nonstandard_aefcd9
        0x7f0a011a -> :sswitch_20    # com.konka.factory.R.id.linearlayout_factory_nonstandard_aefcd7high
        0x7f0a011d -> :sswitch_13    # com.konka.factory.R.id.linearlayout_factory_nonstandard_aefcd7
        0x7f0a0120 -> :sswitch_14    # com.konka.factory.R.id.linearlayout_factory_nonstandard_aefca0
        0x7f0a0123 -> :sswitch_15    # com.konka.factory.R.id.linearlayout_factory_nonstandard_aefca1
        0x7f0a0126 -> :sswitch_16    # com.konka.factory.R.id.linearlayout_factory_nonstandard_aefc66
        0x7f0a0129 -> :sswitch_17    # com.konka.factory.R.id.linearlayout_factory_nonstandard_aefc6e
        0x7f0a012c -> :sswitch_18    # com.konka.factory.R.id.linearlayout_factory_nonstandard_aefc6elow
        0x7f0a012f -> :sswitch_19    # com.konka.factory.R.id.linearlayout_factory_nonstandard_aefc43
        0x7f0a0132 -> :sswitch_1a    # com.konka.factory.R.id.linearlayout_factory_nonstandard_aefc44
        0x7f0a0135 -> :sswitch_1b    # com.konka.factory.R.id.linearlayout_factory_nonstandard_aefccb
        0x7f0a0138 -> :sswitch_1c    # com.konka.factory.R.id.linearlayout_factory_nonstandard_afeccf_atv
        0x7f0a013b -> :sswitch_1d    # com.konka.factory.R.id.linearlayout_factory_nonstandard_afeccf_av
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x7f0a00de -> :sswitch_22    # com.konka.factory.R.id.linearlayout_factory_nonstandard_vifasiasignaloption
        0x7f0a00e1 -> :sswitch_2a    # com.konka.factory.R.id.linearlayout_factory_nonstandard_chinadescrambler
        0x7f0a00e4 -> :sswitch_3f    # com.konka.factory.R.id.linearlayout_factory_nonstandard_audiohidev
        0x7f0a00e7 -> :sswitch_40    # com.konka.factory.R.id.linearlayout_factory_nonstandard_audionr
        0x7f0a00ea -> :sswitch_23    # com.konka.factory.R.id.linearlayout_factory_nonstandard_viftop
        0x7f0a00ed -> :sswitch_24    # com.konka.factory.R.id.linearlayout_factory_nonstandard_vifvgamaximum
        0x7f0a00f0 -> :sswitch_25    # com.konka.factory.R.id.linearlayout_factory_nonstandard_vifcpkp
        0x7f0a00f3 -> :sswitch_26    # com.konka.factory.R.id.linearlayout_factory_nonstandard_vifcpki
        0x7f0a00f6 -> :sswitch_27    # com.konka.factory.R.id.linearlayout_factory_nonstandard_vifcpkpkiadjust
        0x7f0a00f9 -> :sswitch_28    # com.konka.factory.R.id.linearlayout_factory_nonstandard_vifovermodulation
        0x7f0a00fc -> :sswitch_29    # com.konka.factory.R.id.linearlayout_factory_nonstandard_vifclampgain
        0x7f0a00ff -> :sswitch_2b    # com.konka.factory.R.id.linearlayout_factory_nonstandard_delay
        0x7f0a0102 -> :sswitch_2c    # com.konka.factory.R.id.linearlayout_factory_nonstandard_vifcrthr
        0x7f0a0105 -> :sswitch_2d    # com.konka.factory.R.id.linearlayout_factory_nonstandard_vifversion
        0x7f0a0108 -> :sswitch_2e    # com.konka.factory.R.id.linearlayout_factory_nonstandard_vifagcref
        0x7f0a010b -> :sswitch_2f    # com.konka.factory.R.id.linearlayout_factory_nonstandard_gaindistributionthr
        0x7f0a010e -> :sswitch_30    # com.konka.factory.R.id.linearlayout_factory_nonstandard_afecd4
        0x7f0a0111 -> :sswitch_31    # com.konka.factory.R.id.linearlayout_factory_nonstandard_aefcd5
        0x7f0a0114 -> :sswitch_32    # com.konka.factory.R.id.linearlayout_factory_nonstandard_aefcd8
        0x7f0a0117 -> :sswitch_33    # com.konka.factory.R.id.linearlayout_factory_nonstandard_aefcd9
        0x7f0a011a -> :sswitch_41    # com.konka.factory.R.id.linearlayout_factory_nonstandard_aefcd7high
        0x7f0a011d -> :sswitch_34    # com.konka.factory.R.id.linearlayout_factory_nonstandard_aefcd7
        0x7f0a0120 -> :sswitch_35    # com.konka.factory.R.id.linearlayout_factory_nonstandard_aefca0
        0x7f0a0123 -> :sswitch_36    # com.konka.factory.R.id.linearlayout_factory_nonstandard_aefca1
        0x7f0a0126 -> :sswitch_37    # com.konka.factory.R.id.linearlayout_factory_nonstandard_aefc66
        0x7f0a0129 -> :sswitch_38    # com.konka.factory.R.id.linearlayout_factory_nonstandard_aefc6e
        0x7f0a012c -> :sswitch_39    # com.konka.factory.R.id.linearlayout_factory_nonstandard_aefc6elow
        0x7f0a012f -> :sswitch_3a    # com.konka.factory.R.id.linearlayout_factory_nonstandard_aefc43
        0x7f0a0132 -> :sswitch_3b    # com.konka.factory.R.id.linearlayout_factory_nonstandard_aefc44
        0x7f0a0135 -> :sswitch_3c    # com.konka.factory.R.id.linearlayout_factory_nonstandard_aefccb
        0x7f0a0138 -> :sswitch_3d    # com.konka.factory.R.id.linearlayout_factory_nonstandard_afeccf_atv
        0x7f0a013b -> :sswitch_3e    # com.konka.factory.R.id.linearlayout_factory_nonstandard_afeccf_av
    .end sparse-switch
.end method
