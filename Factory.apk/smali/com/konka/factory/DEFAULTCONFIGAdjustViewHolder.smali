.class public Lcom/konka/factory/DEFAULTCONFIGAdjustViewHolder;
.super Ljava/lang/Object;
.source "DEFAULTCONFIGAdjustViewHolder.java"


# instance fields
.field private defaultconfigActivity:Lcom/konka/factory/MainmenuActivity;

.field private factoryManager:Lcom/konka/factory/desk/IFactoryDesk;


# direct methods
.method public constructor <init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V
    .locals 0
    .param p1    # Lcom/konka/factory/MainmenuActivity;
    .param p2    # Lcom/konka/factory/desk/IFactoryDesk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/konka/factory/DEFAULTCONFIGAdjustViewHolder;->defaultconfigActivity:Lcom/konka/factory/MainmenuActivity;

    iput-object p2, p0, Lcom/konka/factory/DEFAULTCONFIGAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    return-void
.end method


# virtual methods
.method public defaultconfig_no()V
    .locals 2

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "defaultconfig no"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/factory/DEFAULTCONFIGAdjustViewHolder;->defaultconfigActivity:Lcom/konka/factory/MainmenuActivity;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->returnRoot(I)V

    return-void
.end method

.method public defaultconfig_yes()V
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/DEFAULTCONFIGAdjustViewHolder;->defaultconfigActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-static {v0}, Lcom/konka/factory/desk/FactoryDB;->getInstance(Landroid/content/Context;)Lcom/konka/factory/desk/FactoryDB;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/factory/desk/FactoryDB;->restoreToDefault()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/konka/factory/DEFAULTCONFIGAdjustViewHolder;->defaultconfigActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v2}, Lcom/konka/factory/MainmenuActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v1

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/konka/factory/DEFAULTCONFIGAdjustViewHolder;->defaultconfig_yes()V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/konka/factory/DEFAULTCONFIGAdjustViewHolder;->defaultconfig_no()V

    goto :goto_0

    :sswitch_1
    iget-object v2, p0, Lcom/konka/factory/DEFAULTCONFIGAdjustViewHolder;->defaultconfigActivity:Lcom/konka/factory/MainmenuActivity;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/konka/factory/MainmenuActivity;->returnRoot(I)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x42 -> :sswitch_0
        0x52 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x7f0a0061
        :pswitch_1    # com.konka.factory.R.id.linearlayout_factory_defaultconfig_yes
        :pswitch_0    # com.konka.factory.R.id.textview_factory_defaultconfig_yes
        :pswitch_2    # com.konka.factory.R.id.linearlayout_factory_defaultconfig_no
    .end packed-switch
.end method
