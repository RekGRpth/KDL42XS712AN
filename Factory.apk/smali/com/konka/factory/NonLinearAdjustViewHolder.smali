.class public Lcom/konka/factory/NonLinearAdjustViewHolder;
.super Ljava/lang/Object;
.source "NonLinearAdjustViewHolder.java"


# instance fields
.field private BackLightMaxVol:I

.field private BrightnessMaxVol:I

.field private ContrastMaxVol:I

.field private HueMaxVol:I

.field private SaturationMaxVol:I

.field private SharpnessMaxVol:I

.field private VolumeMaxVol:I

.field private curvemaxarray:[I

.field private curvetypearray:[Ljava/lang/String;

.field private curvetypeindex:I

.field private factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

.field private nonLinearActivity:Lcom/konka/factory/MainmenuActivity;

.field private osd0val:I

.field private osd100val:I

.field private osd25val:I

.field private osd50val:I

.field private osd75val:I

.field protected progress_factory_nonlinear_osd0:Landroid/widget/ProgressBar;

.field protected progress_factory_nonlinear_osd100:Landroid/widget/ProgressBar;

.field protected progress_factory_nonlinear_osd25:Landroid/widget/ProgressBar;

.field protected progress_factory_nonlinear_osd50:Landroid/widget/ProgressBar;

.field protected progress_factory_nonlinear_osd75:Landroid/widget/ProgressBar;

.field protected text_factory_nonlinear_curvetype_val:Landroid/widget/TextView;

.field protected text_factory_nonlinear_osd0_val:Landroid/widget/TextView;

.field protected text_factory_nonlinear_osd100_val:Landroid/widget/TextView;

.field protected text_factory_nonlinear_osd25_val:Landroid/widget/TextView;

.field protected text_factory_nonlinear_osd50_val:Landroid/widget/TextView;

.field protected text_factory_nonlinear_osd75_val:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V
    .locals 6
    .param p1    # Lcom/konka/factory/MainmenuActivity;
    .param p2    # Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/16 v1, 0xff

    const/16 v0, 0x32

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v3, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvetypeindex:I

    iput v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd0val:I

    iput v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd25val:I

    iput v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd50val:I

    iput v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd75val:I

    iput v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd100val:I

    const/16 v0, 0x64

    iput v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->VolumeMaxVol:I

    iput v1, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->BrightnessMaxVol:I

    iput v1, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->ContrastMaxVol:I

    iput v1, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->SaturationMaxVol:I

    const/16 v0, 0x3f

    iput v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->SharpnessMaxVol:I

    const/16 v0, 0x64

    iput v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->HueMaxVol:I

    iput v1, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->BackLightMaxVol:I

    const/4 v0, 0x7

    new-array v0, v0, [I

    iget v1, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->VolumeMaxVol:I

    aput v1, v0, v3

    iget v1, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->BrightnessMaxVol:I

    aput v1, v0, v4

    iget v1, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->ContrastMaxVol:I

    aput v1, v0, v5

    const/4 v1, 0x3

    iget v2, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->SaturationMaxVol:I

    aput v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->SharpnessMaxVol:I

    aput v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->HueMaxVol:I

    aput v2, v0, v1

    const/4 v1, 0x6

    iget v2, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->BackLightMaxVol:I

    aput v2, v0, v1

    iput-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvemaxarray:[I

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Volume"

    aput-object v1, v0, v3

    const-string v1, "Brightness"

    aput-object v1, v0, v4

    const-string v1, "Contrast"

    aput-object v1, v0, v5

    const/4 v1, 0x3

    const-string v2, "Saturation"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "Sharpness"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "Hue"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "BackLight"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvetypearray:[Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->nonLinearActivity:Lcom/konka/factory/MainmenuActivity;

    iput-object p2, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    return-void
.end method


# virtual methods
.method findView()V
    .locals 2

    iget-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->nonLinearActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a00c4    # com.konka.factory.R.id.textview_factory_nonlinear_curvetype_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->text_factory_nonlinear_curvetype_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->nonLinearActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a00c7    # com.konka.factory.R.id.textview_factory_nonlinear_osd0_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->text_factory_nonlinear_osd0_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->nonLinearActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a00cc    # com.konka.factory.R.id.textview_factory_nonlinear_osd25_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->text_factory_nonlinear_osd25_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->nonLinearActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a00d1    # com.konka.factory.R.id.textview_factory_nonlinear_osd50_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->text_factory_nonlinear_osd50_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->nonLinearActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a00d6    # com.konka.factory.R.id.textview_factory_nonlinear_osd75_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->text_factory_nonlinear_osd75_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->nonLinearActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a00db    # com.konka.factory.R.id.textview_factory_nonlinear_osd100_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->text_factory_nonlinear_osd100_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->nonLinearActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a00c8    # com.konka.factory.R.id.progressbar_facroty_nonlinear_osd0

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->progress_factory_nonlinear_osd0:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->nonLinearActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a00cd    # com.konka.factory.R.id.progressbar_facroty_nonlinear_osd25

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->progress_factory_nonlinear_osd25:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->nonLinearActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a00d2    # com.konka.factory.R.id.progressbar_facroty_nonlinear_osd50

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->progress_factory_nonlinear_osd50:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->nonLinearActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a00d7    # com.konka.factory.R.id.progressbar_facroty_nonlinear_osd75

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->progress_factory_nonlinear_osd75:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->nonLinearActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a00dc    # com.konka.factory.R.id.progressbar_facroty_nonlinear_osd100

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->progress_factory_nonlinear_osd100:Landroid/widget/ProgressBar;

    return-void
.end method

.method public onCreate()Z
    .locals 3

    iget-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getCurveType()Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v0

    iput v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvetypeindex:I

    iget-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getOsdV0Nonlinear()I

    move-result v0

    iput v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd0val:I

    iget-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getOsdV25Nonlinear()I

    move-result v0

    iput v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd25val:I

    iget-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getOsdV50Nonlinear()I

    move-result v0

    iput v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd50val:I

    iget-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getOsdV75Nonlinear()I

    move-result v0

    iput v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd75val:I

    iget-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getOsdV100Nonlinear()I

    move-result v0

    iput v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd100val:I

    iget-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->text_factory_nonlinear_osd0_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd0val:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->text_factory_nonlinear_osd25_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd25val:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->text_factory_nonlinear_osd50_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd50val:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->text_factory_nonlinear_osd75_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd75val:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->text_factory_nonlinear_osd100_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd100val:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->progress_factory_nonlinear_osd0:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd0val:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->progress_factory_nonlinear_osd25:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd25val:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->progress_factory_nonlinear_osd50:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd50val:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->progress_factory_nonlinear_osd75:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd75val:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->progress_factory_nonlinear_osd100:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd100val:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->text_factory_nonlinear_curvetype_val:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvetypearray:[Ljava/lang/String;

    iget v2, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvetypeindex:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x1

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v7, 0x0

    const/4 v0, 0x1

    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->nonLinearActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v4}, Lcom/konka/factory/MainmenuActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v1

    new-instance v3, Ljava/lang/String;

    invoke-direct {v3}, Ljava/lang/String;-><init>()V

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    sparse-switch v1, :sswitch_data_1

    goto :goto_0

    :sswitch_1
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->EN_NLA_NUMS:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v2

    iget v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvetypeindex:I

    add-int/lit8 v5, v2, -0x1

    if-ge v4, v5, :cond_0

    iget v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvetypeindex:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvetypeindex:I

    :goto_1
    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->text_factory_nonlinear_curvetype_val:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvetypearray:[Ljava/lang/String;

    iget v6, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvetypeindex:I

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->values()[Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    move-result-object v5

    iget v6, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvetypeindex:I

    aget-object v5, v5, v6

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setCurveType(Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;)Z

    invoke-virtual {p0}, Lcom/konka/factory/NonLinearAdjustViewHolder;->onCreate()Z

    goto :goto_0

    :cond_0
    iput v7, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvetypeindex:I

    goto :goto_1

    :sswitch_2
    iget v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd0val:I

    iget-object v5, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvemaxarray:[I

    iget v6, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvetypeindex:I

    aget v5, v5, v6

    if-eq v4, v5, :cond_1

    iget v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd0val:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd0val:I

    :goto_2
    iget v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd0val:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->text_factory_nonlinear_osd0_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->progress_factory_nonlinear_osd0:Landroid/widget/ProgressBar;

    iget v5, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd0val:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v5, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd0val:I

    int-to-short v5, v5

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setOsdV0Nonlinear(S)Z

    goto :goto_0

    :cond_1
    iput v7, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd0val:I

    goto :goto_2

    :sswitch_3
    iget v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd25val:I

    iget-object v5, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvemaxarray:[I

    iget v6, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvetypeindex:I

    aget v5, v5, v6

    if-eq v4, v5, :cond_2

    iget v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd25val:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd25val:I

    :goto_3
    iget v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd25val:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->text_factory_nonlinear_osd25_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->progress_factory_nonlinear_osd25:Landroid/widget/ProgressBar;

    iget v5, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd25val:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v5, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd25val:I

    int-to-short v5, v5

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setOsdV25Nonlinear(S)Z

    goto/16 :goto_0

    :cond_2
    iput v7, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd25val:I

    goto :goto_3

    :sswitch_4
    iget v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd50val:I

    iget-object v5, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvemaxarray:[I

    iget v6, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvetypeindex:I

    aget v5, v5, v6

    if-eq v4, v5, :cond_3

    iget v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd50val:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd50val:I

    :goto_4
    iget v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd50val:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->text_factory_nonlinear_osd50_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->progress_factory_nonlinear_osd50:Landroid/widget/ProgressBar;

    iget v5, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd50val:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v5, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd50val:I

    int-to-short v5, v5

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setOsdV50Nonlinear(S)Z

    goto/16 :goto_0

    :cond_3
    iput v7, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd50val:I

    goto :goto_4

    :sswitch_5
    iget v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd75val:I

    iget-object v5, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvemaxarray:[I

    iget v6, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvetypeindex:I

    aget v5, v5, v6

    if-eq v4, v5, :cond_4

    iget v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd75val:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd75val:I

    :goto_5
    iget v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd75val:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->text_factory_nonlinear_osd75_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->progress_factory_nonlinear_osd75:Landroid/widget/ProgressBar;

    iget v5, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd75val:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v5, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd75val:I

    int-to-short v5, v5

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setOsdV75Nonlinear(S)Z

    goto/16 :goto_0

    :cond_4
    iput v7, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd75val:I

    goto :goto_5

    :sswitch_6
    iget v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd100val:I

    iget-object v5, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvemaxarray:[I

    iget v6, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvetypeindex:I

    aget v5, v5, v6

    if-eq v4, v5, :cond_5

    iget v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd100val:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd100val:I

    :goto_6
    iget v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd100val:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->text_factory_nonlinear_osd100_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->progress_factory_nonlinear_osd100:Landroid/widget/ProgressBar;

    iget v5, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd100val:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v5, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd100val:I

    int-to-short v5, v5

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setOsdV100Nonlinear(S)Z

    goto/16 :goto_0

    :cond_5
    iput v7, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd100val:I

    goto :goto_6

    :sswitch_7
    sparse-switch v1, :sswitch_data_2

    goto/16 :goto_0

    :sswitch_8
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->EN_NLA_NUMS:Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v2

    iget v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvetypeindex:I

    if-lez v4, :cond_6

    iget v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvetypeindex:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvetypeindex:I

    :goto_7
    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->text_factory_nonlinear_curvetype_val:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvetypearray:[Ljava/lang/String;

    iget v6, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvetypeindex:I

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;->values()[Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;

    move-result-object v5

    iget v6, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvetypeindex:I

    aget-object v5, v5, v6

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setCurveType(Lcom/konka/factory/desk/IFactoryDesk$MS_NLA_SET_INDEX;)Z

    invoke-virtual {p0}, Lcom/konka/factory/NonLinearAdjustViewHolder;->onCreate()Z

    goto/16 :goto_0

    :cond_6
    add-int/lit8 v4, v2, -0x1

    iput v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvetypeindex:I

    goto :goto_7

    :sswitch_9
    iget v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd0val:I

    if-eqz v4, :cond_7

    iget v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd0val:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd0val:I

    :goto_8
    iget v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd0val:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->text_factory_nonlinear_osd0_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->progress_factory_nonlinear_osd0:Landroid/widget/ProgressBar;

    iget v5, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd0val:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v5, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd0val:I

    int-to-short v5, v5

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setOsdV0Nonlinear(S)Z

    goto/16 :goto_0

    :cond_7
    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvemaxarray:[I

    iget v5, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvetypeindex:I

    aget v4, v4, v5

    iput v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd0val:I

    goto :goto_8

    :sswitch_a
    iget v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd25val:I

    if-eqz v4, :cond_8

    iget v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd25val:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd25val:I

    :goto_9
    iget v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd25val:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->text_factory_nonlinear_osd25_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->progress_factory_nonlinear_osd25:Landroid/widget/ProgressBar;

    iget v5, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd25val:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v5, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd25val:I

    int-to-short v5, v5

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setOsdV25Nonlinear(S)Z

    goto/16 :goto_0

    :cond_8
    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvemaxarray:[I

    iget v5, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvetypeindex:I

    aget v4, v4, v5

    iput v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd25val:I

    goto :goto_9

    :sswitch_b
    iget v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd50val:I

    if-eqz v4, :cond_9

    iget v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd50val:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd50val:I

    :goto_a
    iget v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd50val:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->text_factory_nonlinear_osd50_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->progress_factory_nonlinear_osd50:Landroid/widget/ProgressBar;

    iget v5, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd50val:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v5, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd50val:I

    int-to-short v5, v5

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setOsdV50Nonlinear(S)Z

    goto/16 :goto_0

    :cond_9
    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvemaxarray:[I

    iget v5, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvetypeindex:I

    aget v4, v4, v5

    iput v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd50val:I

    goto :goto_a

    :sswitch_c
    iget v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd75val:I

    if-eqz v4, :cond_a

    iget v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd75val:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd75val:I

    :goto_b
    iget v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd75val:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->text_factory_nonlinear_osd75_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->progress_factory_nonlinear_osd75:Landroid/widget/ProgressBar;

    iget v5, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd75val:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v5, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd75val:I

    int-to-short v5, v5

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setOsdV75Nonlinear(S)Z

    goto/16 :goto_0

    :cond_a
    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvemaxarray:[I

    iget v5, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvetypeindex:I

    aget v4, v4, v5

    iput v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd75val:I

    goto :goto_b

    :sswitch_d
    iget v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd100val:I

    if-eqz v4, :cond_b

    iget v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd100val:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd100val:I

    :goto_c
    iget v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd100val:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->text_factory_nonlinear_osd100_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->progress_factory_nonlinear_osd100:Landroid/widget/ProgressBar;

    iget v5, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd100val:I

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v5, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd100val:I

    int-to-short v5, v5

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setOsdV100Nonlinear(S)Z

    goto/16 :goto_0

    :cond_b
    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvemaxarray:[I

    iget v5, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->curvetypeindex:I

    aget v4, v4, v5

    iput v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->osd100val:I

    goto :goto_c

    :sswitch_e
    iget-object v4, p0, Lcom/konka/factory/NonLinearAdjustViewHolder;->nonLinearActivity:Lcom/konka/factory/MainmenuActivity;

    const/16 v5, 0xb

    invoke-virtual {v4, v5}, Lcom/konka/factory/MainmenuActivity;->returnRoot(I)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_e
        0x15 -> :sswitch_7
        0x16 -> :sswitch_0
        0x52 -> :sswitch_e
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x7f0a00c2 -> :sswitch_1    # com.konka.factory.R.id.linearlayout_factory_nonlinear_curvetype
        0x7f0a00c5 -> :sswitch_2    # com.konka.factory.R.id.linearlayout_factory_nonlinear_osd0
        0x7f0a00ca -> :sswitch_3    # com.konka.factory.R.id.linearlayout_factory_nonlinear_osd25
        0x7f0a00cf -> :sswitch_4    # com.konka.factory.R.id.linearlayout_factory_nonlinear_osd50
        0x7f0a00d4 -> :sswitch_5    # com.konka.factory.R.id.linearlayout_factory_nonlinear_osd75
        0x7f0a00d9 -> :sswitch_6    # com.konka.factory.R.id.linearlayout_factory_nonlinear_osd100
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x7f0a00c2 -> :sswitch_8    # com.konka.factory.R.id.linearlayout_factory_nonlinear_curvetype
        0x7f0a00c5 -> :sswitch_9    # com.konka.factory.R.id.linearlayout_factory_nonlinear_osd0
        0x7f0a00ca -> :sswitch_a    # com.konka.factory.R.id.linearlayout_factory_nonlinear_osd25
        0x7f0a00cf -> :sswitch_b    # com.konka.factory.R.id.linearlayout_factory_nonlinear_osd50
        0x7f0a00d4 -> :sswitch_c    # com.konka.factory.R.id.linearlayout_factory_nonlinear_osd75
        0x7f0a00d9 -> :sswitch_d    # com.konka.factory.R.id.linearlayout_factory_nonlinear_osd100
    .end sparse-switch
.end method
