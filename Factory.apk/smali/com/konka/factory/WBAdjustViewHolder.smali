.class public Lcom/konka/factory/WBAdjustViewHolder;
.super Ljava/lang/Object;
.source "WBAdjustViewHolder.java"


# instance fields
.field private bgainvalWB:I

.field private boffsetvalWB:I

.field private clortempindex:I

.field private colortemparray:[Ljava/lang/String;

.field private factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

.field private final gainStepWB:I

.field private ggainvalWB:I

.field private goffsetvalWB:I

.field private final offsetStepWB:I

.field private rgainvalWB:I

.field private roffsetvalWB:I

.field private sourcearrayWB:[Ljava/lang/String;

.field private sourceindexWB:I

.field protected text_factory_wbadjust_bgain_val:Landroid/widget/TextView;

.field protected text_factory_wbadjust_boffset_val:Landroid/widget/TextView;

.field protected text_factory_wbadjust_colortemp_val:Landroid/widget/TextView;

.field protected text_factory_wbadjust_ggain_val:Landroid/widget/TextView;

.field protected text_factory_wbadjust_goffset_val:Landroid/widget/TextView;

.field protected text_factory_wbadjust_rgain_val:Landroid/widget/TextView;

.field protected text_factory_wbadjust_roffset_val:Landroid/widget/TextView;

.field protected text_factory_wbadjust_source_val:Landroid/widget/TextView;

.field private wbActivity:Lcom/konka/factory/MainmenuActivity;


# direct methods
.method public constructor <init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V
    .locals 7
    .param p1    # Lcom/konka/factory/MainmenuActivity;
    .param p2    # Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x4

    const/4 v3, 0x0

    const/16 v1, 0x400

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v3, p0, Lcom/konka/factory/WBAdjustViewHolder;->sourceindexWB:I

    iput v3, p0, Lcom/konka/factory/WBAdjustViewHolder;->clortempindex:I

    iput v1, p0, Lcom/konka/factory/WBAdjustViewHolder;->rgainvalWB:I

    iput v1, p0, Lcom/konka/factory/WBAdjustViewHolder;->ggainvalWB:I

    iput v1, p0, Lcom/konka/factory/WBAdjustViewHolder;->bgainvalWB:I

    const/16 v0, 0x8

    iput v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->gainStepWB:I

    iput v1, p0, Lcom/konka/factory/WBAdjustViewHolder;->roffsetvalWB:I

    iput v1, p0, Lcom/konka/factory/WBAdjustViewHolder;->goffsetvalWB:I

    iput v1, p0, Lcom/konka/factory/WBAdjustViewHolder;->boffsetvalWB:I

    iput v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->offsetStepWB:I

    const/16 v0, 0x23

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "VGA"

    aput-object v1, v0, v3

    const-string v1, "ATV"

    aput-object v1, v0, v5

    const-string v1, "AV1"

    aput-object v1, v0, v6

    const/4 v1, 0x3

    const-string v2, "AV2"

    aput-object v2, v0, v1

    const-string v1, "AV3"

    aput-object v1, v0, v4

    const/4 v1, 0x5

    const-string v2, "AV4"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "AV5"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "AV6"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "AV7"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "AV8"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "AVMAX"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "SV1"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "SV2"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "SV3"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "SV4"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "SVMAX"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "YPbPr1"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "YPbPr2"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "YPbPr3"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "YPbPrMAX"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "SCART1"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "SCART2"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "SCARTMAX"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "HDMI1"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "HDMI2"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "HDMI3"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "HDMI4"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "HDMIMAX"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "DTV"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "DVI1"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "DVI2"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "DVI3"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "DVI4"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "DVIMAX"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "STORAGE"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->sourcearrayWB:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "Cool"

    aput-object v1, v0, v3

    const-string v1, "Standard"

    aput-object v1, v0, v5

    const-string v1, "Warm"

    aput-object v1, v0, v6

    const/4 v1, 0x3

    const-string v2, "User"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->colortemparray:[Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/factory/WBAdjustViewHolder;->wbActivity:Lcom/konka/factory/MainmenuActivity;

    iput-object p2, p0, Lcom/konka/factory/WBAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    return-void
.end method


# virtual methods
.method DisableSourceLinear()V
    .locals 5

    const v4, -0x777778

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/konka/factory/WBAdjustViewHolder;->wbActivity:Lcom/konka/factory/MainmenuActivity;

    const v2, 0x7f0a023c    # com.konka.factory.R.id.linearlayout_factory_wbadjust_source

    invoke-virtual {v1, v2}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    return-void
.end method

.method findView()V
    .locals 2

    iget-object v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->wbActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a023e    # com.konka.factory.R.id.textview_factory_wbadjust_source_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->text_factory_wbadjust_source_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->wbActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0244    # com.konka.factory.R.id.textview_factory_wbadjust_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->text_factory_wbadjust_rgain_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->wbActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0247    # com.konka.factory.R.id.textview_factory_wbadjust_ggain_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->text_factory_wbadjust_ggain_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->wbActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a024a    # com.konka.factory.R.id.textview_factory_wbadjust_bgain_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->text_factory_wbadjust_bgain_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->wbActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a024d    # com.konka.factory.R.id.textview_factory_wbadjust_roffset_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->text_factory_wbadjust_roffset_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->wbActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0250    # com.konka.factory.R.id.textview_factory_wbadjust_goffset_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->text_factory_wbadjust_goffset_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->wbActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0253    # com.konka.factory.R.id.textview_factory_wbadjust_boffset_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->text_factory_wbadjust_boffset_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->wbActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0241    # com.konka.factory.R.id.textview_factory_wbadjust_colortemp_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->text_factory_wbadjust_colortemp_val:Landroid/widget/TextView;

    return-void
.end method

.method public onCreate()Z
    .locals 3

    iget-object v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getWbRedGain()I

    move-result v0

    iput v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->rgainvalWB:I

    iget-object v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getWbGreenGain()I

    move-result v0

    iput v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->ggainvalWB:I

    iget-object v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getWbBlueGain()I

    move-result v0

    iput v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->bgainvalWB:I

    iget-object v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getWbRedOffset()I

    move-result v0

    iput v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->roffsetvalWB:I

    iget-object v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getWbGreenOffset()I

    move-result v0

    iput v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->goffsetvalWB:I

    iget-object v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getWbBlueOffset()I

    move-result v0

    iput v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->boffsetvalWB:I

    iget-object v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v0

    iput v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->sourceindexWB:I

    iget-object v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getColorTmpIdx()I

    move-result v0

    iput v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->clortempindex:I

    :cond_0
    iget-object v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->text_factory_wbadjust_rgain_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/WBAdjustViewHolder;->rgainvalWB:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->text_factory_wbadjust_ggain_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/WBAdjustViewHolder;->ggainvalWB:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->text_factory_wbadjust_bgain_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/WBAdjustViewHolder;->bgainvalWB:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->text_factory_wbadjust_roffset_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/WBAdjustViewHolder;->roffsetvalWB:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->text_factory_wbadjust_goffset_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/WBAdjustViewHolder;->goffsetvalWB:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->text_factory_wbadjust_boffset_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/WBAdjustViewHolder;->boffsetvalWB:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->text_factory_wbadjust_source_val:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/factory/WBAdjustViewHolder;->sourcearrayWB:[Ljava/lang/String;

    iget v2, p0, Lcom/konka/factory/WBAdjustViewHolder;->sourceindexWB:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/WBAdjustViewHolder;->text_factory_wbadjust_colortemp_val:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/factory/WBAdjustViewHolder;->colortemparray:[Ljava/lang/String;

    iget v2, p0, Lcom/konka/factory/WBAdjustViewHolder;->clortempindex:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/konka/factory/WBAdjustViewHolder;->DisableSourceLinear()V

    const/4 v0, 0x1

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 7
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v6, 0x0

    const/16 v5, 0x7ff

    const/4 v0, 0x1

    iget-object v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->wbActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v4}, Lcom/konka/factory/MainmenuActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v1

    new-instance v3, Ljava/lang/String;

    invoke-direct {v3}, Ljava/lang/String;-><init>()V

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    sparse-switch v1, :sswitch_data_1

    goto :goto_0

    :sswitch_1
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_NUM:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;->ordinal()I

    move-result v2

    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->clortempindex:I

    add-int/lit8 v5, v2, -0x2

    if-ge v4, v5, :cond_1

    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->clortempindex:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->clortempindex:I

    :goto_1
    iget-object v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->text_factory_wbadjust_colortemp_val:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/konka/factory/WBAdjustViewHolder;->colortemparray:[Ljava/lang/String;

    iget v6, p0, Lcom/konka/factory/WBAdjustViewHolder;->clortempindex:I

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

    move-result-object v5

    iget v6, p0, Lcom/konka/factory/WBAdjustViewHolder;->clortempindex:I

    aget-object v5, v5, v6

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setColorTmpIdx(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;)V

    :cond_0
    invoke-virtual {p0}, Lcom/konka/factory/WBAdjustViewHolder;->onCreate()Z

    goto :goto_0

    :cond_1
    iput v6, p0, Lcom/konka/factory/WBAdjustViewHolder;->clortempindex:I

    goto :goto_1

    :sswitch_2
    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->rgainvalWB:I

    if-ge v4, v5, :cond_3

    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->rgainvalWB:I

    add-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->rgainvalWB:I

    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->rgainvalWB:I

    if-le v4, v5, :cond_2

    iput v5, p0, Lcom/konka/factory/WBAdjustViewHolder;->rgainvalWB:I

    :cond_2
    :goto_2
    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->rgainvalWB:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->text_factory_wbadjust_rgain_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v5, p0, Lcom/konka/factory/WBAdjustViewHolder;->rgainvalWB:I

    int-to-short v5, v5

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setWbRedGain(S)Z

    goto :goto_0

    :cond_3
    iput v6, p0, Lcom/konka/factory/WBAdjustViewHolder;->rgainvalWB:I

    goto :goto_2

    :sswitch_3
    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->ggainvalWB:I

    if-ge v4, v5, :cond_5

    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->ggainvalWB:I

    add-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->ggainvalWB:I

    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->ggainvalWB:I

    if-le v4, v5, :cond_4

    iput v5, p0, Lcom/konka/factory/WBAdjustViewHolder;->ggainvalWB:I

    :cond_4
    :goto_3
    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->ggainvalWB:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->text_factory_wbadjust_ggain_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v5, p0, Lcom/konka/factory/WBAdjustViewHolder;->ggainvalWB:I

    int-to-short v5, v5

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setWbGreenGain(S)Z

    goto/16 :goto_0

    :cond_5
    iput v6, p0, Lcom/konka/factory/WBAdjustViewHolder;->ggainvalWB:I

    goto :goto_3

    :sswitch_4
    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->bgainvalWB:I

    if-ge v4, v5, :cond_7

    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->bgainvalWB:I

    add-int/lit8 v4, v4, 0x8

    iput v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->bgainvalWB:I

    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->bgainvalWB:I

    if-le v4, v5, :cond_6

    iput v5, p0, Lcom/konka/factory/WBAdjustViewHolder;->bgainvalWB:I

    :cond_6
    :goto_4
    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->bgainvalWB:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->text_factory_wbadjust_bgain_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v5, p0, Lcom/konka/factory/WBAdjustViewHolder;->bgainvalWB:I

    int-to-short v5, v5

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setWbBlueGain(S)Z

    goto/16 :goto_0

    :cond_7
    iput v6, p0, Lcom/konka/factory/WBAdjustViewHolder;->bgainvalWB:I

    goto :goto_4

    :sswitch_5
    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->roffsetvalWB:I

    if-ge v4, v5, :cond_9

    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->roffsetvalWB:I

    add-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->roffsetvalWB:I

    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->roffsetvalWB:I

    if-le v4, v5, :cond_8

    iput v5, p0, Lcom/konka/factory/WBAdjustViewHolder;->roffsetvalWB:I

    :cond_8
    :goto_5
    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->roffsetvalWB:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->text_factory_wbadjust_roffset_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v5, p0, Lcom/konka/factory/WBAdjustViewHolder;->roffsetvalWB:I

    int-to-short v5, v5

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setWbRedOffset(S)Z

    goto/16 :goto_0

    :cond_9
    iput v6, p0, Lcom/konka/factory/WBAdjustViewHolder;->roffsetvalWB:I

    goto :goto_5

    :sswitch_6
    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->goffsetvalWB:I

    if-ge v4, v5, :cond_b

    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->goffsetvalWB:I

    add-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->goffsetvalWB:I

    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->goffsetvalWB:I

    if-le v4, v5, :cond_a

    iput v5, p0, Lcom/konka/factory/WBAdjustViewHolder;->goffsetvalWB:I

    :cond_a
    :goto_6
    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->goffsetvalWB:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->text_factory_wbadjust_goffset_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v5, p0, Lcom/konka/factory/WBAdjustViewHolder;->goffsetvalWB:I

    int-to-short v5, v5

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setWbGreenOffset(S)Z

    goto/16 :goto_0

    :cond_b
    iput v6, p0, Lcom/konka/factory/WBAdjustViewHolder;->goffsetvalWB:I

    goto :goto_6

    :sswitch_7
    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->boffsetvalWB:I

    if-ge v4, v5, :cond_d

    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->boffsetvalWB:I

    add-int/lit8 v4, v4, 0x4

    iput v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->boffsetvalWB:I

    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->boffsetvalWB:I

    if-le v4, v5, :cond_c

    iput v5, p0, Lcom/konka/factory/WBAdjustViewHolder;->boffsetvalWB:I

    :cond_c
    :goto_7
    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->boffsetvalWB:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->text_factory_wbadjust_boffset_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v5, p0, Lcom/konka/factory/WBAdjustViewHolder;->boffsetvalWB:I

    int-to-short v5, v5

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setWbBlueOffset(S)Z

    goto/16 :goto_0

    :cond_d
    iput v6, p0, Lcom/konka/factory/WBAdjustViewHolder;->boffsetvalWB:I

    goto :goto_7

    :sswitch_8
    sparse-switch v1, :sswitch_data_2

    goto/16 :goto_0

    :sswitch_9
    sget-object v4, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;->MS_COLOR_TEMP_NUM:Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

    invoke-virtual {v4}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;->ordinal()I

    move-result v2

    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->clortempindex:I

    if-lez v4, :cond_f

    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->clortempindex:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->clortempindex:I

    :goto_8
    iget-object v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->text_factory_wbadjust_colortemp_val:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/konka/factory/WBAdjustViewHolder;->colortemparray:[Ljava/lang/String;

    iget v6, p0, Lcom/konka/factory/WBAdjustViewHolder;->clortempindex:I

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    if-eqz v4, :cond_e

    iget-object v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-static {}, Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;->values()[Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;

    move-result-object v5

    iget v6, p0, Lcom/konka/factory/WBAdjustViewHolder;->clortempindex:I

    aget-object v5, v5, v6

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setColorTmpIdx(Lcom/konka/factory/desk/IFactoryDesk$EN_MS_COLOR_TEMP;)V

    :cond_e
    invoke-virtual {p0}, Lcom/konka/factory/WBAdjustViewHolder;->onCreate()Z

    goto/16 :goto_0

    :cond_f
    add-int/lit8 v4, v2, -0x2

    iput v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->clortempindex:I

    goto :goto_8

    :sswitch_a
    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->rgainvalWB:I

    if-lez v4, :cond_11

    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->rgainvalWB:I

    add-int/lit8 v4, v4, -0x8

    iput v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->rgainvalWB:I

    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->rgainvalWB:I

    if-gez v4, :cond_10

    iput v6, p0, Lcom/konka/factory/WBAdjustViewHolder;->rgainvalWB:I

    :cond_10
    :goto_9
    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->rgainvalWB:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->text_factory_wbadjust_rgain_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v5, p0, Lcom/konka/factory/WBAdjustViewHolder;->rgainvalWB:I

    int-to-short v5, v5

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setWbRedGain(S)Z

    goto/16 :goto_0

    :cond_11
    iput v5, p0, Lcom/konka/factory/WBAdjustViewHolder;->rgainvalWB:I

    goto :goto_9

    :sswitch_b
    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->ggainvalWB:I

    if-lez v4, :cond_13

    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->ggainvalWB:I

    add-int/lit8 v4, v4, -0x8

    iput v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->ggainvalWB:I

    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->ggainvalWB:I

    if-gez v4, :cond_12

    iput v6, p0, Lcom/konka/factory/WBAdjustViewHolder;->ggainvalWB:I

    :cond_12
    :goto_a
    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->ggainvalWB:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->text_factory_wbadjust_ggain_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v5, p0, Lcom/konka/factory/WBAdjustViewHolder;->ggainvalWB:I

    int-to-short v5, v5

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setWbGreenGain(S)Z

    goto/16 :goto_0

    :cond_13
    iput v5, p0, Lcom/konka/factory/WBAdjustViewHolder;->ggainvalWB:I

    goto :goto_a

    :sswitch_c
    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->bgainvalWB:I

    if-lez v4, :cond_15

    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->bgainvalWB:I

    add-int/lit8 v4, v4, -0x8

    iput v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->bgainvalWB:I

    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->bgainvalWB:I

    if-gez v4, :cond_14

    iput v6, p0, Lcom/konka/factory/WBAdjustViewHolder;->bgainvalWB:I

    :cond_14
    :goto_b
    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->bgainvalWB:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->text_factory_wbadjust_bgain_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v5, p0, Lcom/konka/factory/WBAdjustViewHolder;->bgainvalWB:I

    int-to-short v5, v5

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setWbBlueGain(S)Z

    goto/16 :goto_0

    :cond_15
    iput v5, p0, Lcom/konka/factory/WBAdjustViewHolder;->bgainvalWB:I

    goto :goto_b

    :sswitch_d
    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->roffsetvalWB:I

    if-lez v4, :cond_17

    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->roffsetvalWB:I

    add-int/lit8 v4, v4, -0x4

    iput v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->roffsetvalWB:I

    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->roffsetvalWB:I

    if-gez v4, :cond_16

    iput v6, p0, Lcom/konka/factory/WBAdjustViewHolder;->roffsetvalWB:I

    :cond_16
    :goto_c
    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->roffsetvalWB:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->text_factory_wbadjust_roffset_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v5, p0, Lcom/konka/factory/WBAdjustViewHolder;->roffsetvalWB:I

    int-to-short v5, v5

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setWbRedOffset(S)Z

    goto/16 :goto_0

    :cond_17
    iput v5, p0, Lcom/konka/factory/WBAdjustViewHolder;->roffsetvalWB:I

    goto :goto_c

    :sswitch_e
    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->goffsetvalWB:I

    if-lez v4, :cond_19

    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->goffsetvalWB:I

    add-int/lit8 v4, v4, -0x4

    iput v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->goffsetvalWB:I

    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->goffsetvalWB:I

    if-gez v4, :cond_18

    iput v6, p0, Lcom/konka/factory/WBAdjustViewHolder;->goffsetvalWB:I

    :cond_18
    :goto_d
    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->goffsetvalWB:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->text_factory_wbadjust_goffset_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v5, p0, Lcom/konka/factory/WBAdjustViewHolder;->goffsetvalWB:I

    int-to-short v5, v5

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setWbGreenOffset(S)Z

    goto/16 :goto_0

    :cond_19
    iput v5, p0, Lcom/konka/factory/WBAdjustViewHolder;->goffsetvalWB:I

    goto :goto_d

    :sswitch_f
    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->boffsetvalWB:I

    if-lez v4, :cond_1b

    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->boffsetvalWB:I

    add-int/lit8 v4, v4, -0x4

    iput v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->boffsetvalWB:I

    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->boffsetvalWB:I

    if-gez v4, :cond_1a

    iput v6, p0, Lcom/konka/factory/WBAdjustViewHolder;->boffsetvalWB:I

    :cond_1a
    :goto_e
    iget v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->boffsetvalWB:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->text_factory_wbadjust_boffset_val:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v5, p0, Lcom/konka/factory/WBAdjustViewHolder;->boffsetvalWB:I

    int-to-short v5, v5

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setWbBlueOffset(S)Z

    goto/16 :goto_0

    :cond_1b
    iput v5, p0, Lcom/konka/factory/WBAdjustViewHolder;->boffsetvalWB:I

    goto :goto_e

    :sswitch_10
    iget-object v4, p0, Lcom/konka/factory/WBAdjustViewHolder;->wbActivity:Lcom/konka/factory/MainmenuActivity;

    const/4 v5, 0x6

    invoke-virtual {v4, v5}, Lcom/konka/factory/MainmenuActivity;->returnRoot(I)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_10
        0x15 -> :sswitch_8
        0x16 -> :sswitch_0
        0x52 -> :sswitch_10
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x7f0a023f -> :sswitch_1    # com.konka.factory.R.id.linearlayout_factory_wbadjust_colortemp
        0x7f0a0242 -> :sswitch_2    # com.konka.factory.R.id.linearlayout_factory_wbadjust_rgain
        0x7f0a0245 -> :sswitch_3    # com.konka.factory.R.id.linearlayout_factory_wbadjust_ggain
        0x7f0a0248 -> :sswitch_4    # com.konka.factory.R.id.linearlayout_factory_wbadjust_bgain
        0x7f0a024b -> :sswitch_5    # com.konka.factory.R.id.linearlayout_factory_wbadjust_roffset
        0x7f0a024e -> :sswitch_6    # com.konka.factory.R.id.linearlayout_factory_wbadjust_goffset
        0x7f0a0251 -> :sswitch_7    # com.konka.factory.R.id.linearlayout_factory_wbadjust_boffset
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x7f0a023f -> :sswitch_9    # com.konka.factory.R.id.linearlayout_factory_wbadjust_colortemp
        0x7f0a0242 -> :sswitch_a    # com.konka.factory.R.id.linearlayout_factory_wbadjust_rgain
        0x7f0a0245 -> :sswitch_b    # com.konka.factory.R.id.linearlayout_factory_wbadjust_ggain
        0x7f0a0248 -> :sswitch_c    # com.konka.factory.R.id.linearlayout_factory_wbadjust_bgain
        0x7f0a024b -> :sswitch_d    # com.konka.factory.R.id.linearlayout_factory_wbadjust_roffset
        0x7f0a024e -> :sswitch_e    # com.konka.factory.R.id.linearlayout_factory_wbadjust_goffset
        0x7f0a0251 -> :sswitch_f    # com.konka.factory.R.id.linearlayout_factory_wbadjust_boffset
    .end sparse-switch
.end method
