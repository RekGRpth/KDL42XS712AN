.class public Lcom/konka/factory/PEQAdjustViewHolder;
.super Ljava/lang/Object;
.source "PEQAdjustViewHolder.java"


# instance fields
.field private factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

.field private fo1coarseval:I

.field private fo1fineval:I

.field private fo2coarseval:I

.field private fo2fineval:I

.field private fo3coarseval:I

.field private fo3fineval:I

.field private fo4coarseval:I

.field private fo4fineval:I

.field private fo5coarseval:I

.field private fo5fineval:I

.field private gain1val:I

.field private gain2val:I

.field private gain3val:I

.field private gain4val:I

.field private gain5val:I

.field private peqActivity:Lcom/konka/factory/MainmenuActivity;

.field private q1val:I

.field private q2val:I

.field private q3val:I

.field private q4val:I

.field private q5val:I

.field protected text_factory_peq_fo1coarse_val:Landroid/widget/TextView;

.field protected text_factory_peq_fo1fine_val:Landroid/widget/TextView;

.field protected text_factory_peq_fo2coarse_val:Landroid/widget/TextView;

.field protected text_factory_peq_fo2fine_val:Landroid/widget/TextView;

.field protected text_factory_peq_fo3coarse_val:Landroid/widget/TextView;

.field protected text_factory_peq_fo3fine_val:Landroid/widget/TextView;

.field protected text_factory_peq_fo4coarse_val:Landroid/widget/TextView;

.field protected text_factory_peq_fo4fine_val:Landroid/widget/TextView;

.field protected text_factory_peq_fo5coarse_val:Landroid/widget/TextView;

.field protected text_factory_peq_fo5fine_val:Landroid/widget/TextView;

.field protected text_factory_peq_gain1_val:Landroid/widget/TextView;

.field protected text_factory_peq_gain2_val:Landroid/widget/TextView;

.field protected text_factory_peq_gain3_val:Landroid/widget/TextView;

.field protected text_factory_peq_gain4_val:Landroid/widget/TextView;

.field protected text_factory_peq_gain5_val:Landroid/widget/TextView;

.field protected text_factory_peq_q1_val:Landroid/widget/TextView;

.field protected text_factory_peq_q2_val:Landroid/widget/TextView;

.field protected text_factory_peq_q3_val:Landroid/widget/TextView;

.field protected text_factory_peq_q4_val:Landroid/widget/TextView;

.field protected text_factory_peq_q5_val:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V
    .locals 4
    .param p1    # Lcom/konka/factory/MainmenuActivity;
    .param p2    # Lcom/konka/factory/desk/IFactoryDesk;

    const/16 v3, 0xa0

    const/16 v2, 0x78

    const/16 v1, 0xa

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo1coarseval:I

    iput v1, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo2coarseval:I

    iput v1, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo3coarseval:I

    iput v1, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo4coarseval:I

    iput v1, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo5coarseval:I

    iput v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo1fineval:I

    iput v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo2fineval:I

    iput v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo3fineval:I

    iput v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo4fineval:I

    iput v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo5fineval:I

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain1val:I

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain2val:I

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain3val:I

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain4val:I

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain5val:I

    iput v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q1val:I

    iput v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q2val:I

    iput v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q3val:I

    iput v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q4val:I

    iput v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q5val:I

    iput-object p1, p0, Lcom/konka/factory/PEQAdjustViewHolder;->peqActivity:Lcom/konka/factory/MainmenuActivity;

    iput-object p2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    return-void
.end method


# virtual methods
.method findView()V
    .locals 2

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->peqActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a019c    # com.konka.factory.R.id.textview_factory_peq_fo1coarse_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo1coarse_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->peqActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a019f    # com.konka.factory.R.id.textview_factory_peq_fo1fine_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo1fine_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->peqActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a01a2    # com.konka.factory.R.id.textview_factory_peq_fo2coarse_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo2coarse_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->peqActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a01a5    # com.konka.factory.R.id.textview_factory_peq_fo2fine_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo2fine_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->peqActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a01a8    # com.konka.factory.R.id.textview_factory_peq_fo3coarse_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo3coarse_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->peqActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a01ab    # com.konka.factory.R.id.textview_factory_peq_fo3fine_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo3fine_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->peqActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a01ae    # com.konka.factory.R.id.textview_factory_peq_fo4coarse_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo4coarse_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->peqActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a01b1    # com.konka.factory.R.id.textview_factory_peq_fo4fine_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo4fine_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->peqActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a01b4    # com.konka.factory.R.id.textview_factory_peq_fo5coarse_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo5coarse_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->peqActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a01b7    # com.konka.factory.R.id.textview_factory_peq_fo5fine_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo5fine_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->peqActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a01ba    # com.konka.factory.R.id.textview_factory_peq_gain1_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_gain1_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->peqActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a01bd    # com.konka.factory.R.id.textview_factory_peq_gain2_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_gain2_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->peqActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a01c0    # com.konka.factory.R.id.textview_factory_peq_gain3_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_gain3_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->peqActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a01c3    # com.konka.factory.R.id.textview_factory_peq_gain4_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_gain4_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->peqActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a01c6    # com.konka.factory.R.id.textview_factory_peq_gain5_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_gain5_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->peqActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a01c9    # com.konka.factory.R.id.textview_factory_peq_q1_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_q1_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->peqActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a01cc    # com.konka.factory.R.id.textview_factory_peq_q2_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_q2_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->peqActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a01cf    # com.konka.factory.R.id.textview_factory_peq_q3_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_q3_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->peqActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a01d2    # com.konka.factory.R.id.textview_factory_peq_q4_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_q4_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->peqActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a01d5    # com.konka.factory.R.id.textview_factory_peq_q5_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_q5_val:Landroid/widget/TextView;

    return-void
.end method

.method public onCreate()V
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0, v1}, Lcom/konka/factory/desk/IFactoryDesk;->getPeqFoCoarse(I)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo1coarseval:I

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0, v2}, Lcom/konka/factory/desk/IFactoryDesk;->getPeqFoCoarse(I)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo2coarseval:I

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0, v3}, Lcom/konka/factory/desk/IFactoryDesk;->getPeqFoCoarse(I)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo3coarseval:I

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0, v4}, Lcom/konka/factory/desk/IFactoryDesk;->getPeqFoCoarse(I)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo4coarseval:I

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0, v5}, Lcom/konka/factory/desk/IFactoryDesk;->getPeqFoCoarse(I)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo5coarseval:I

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0, v1}, Lcom/konka/factory/desk/IFactoryDesk;->getPeqFoFine(I)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo1fineval:I

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0, v2}, Lcom/konka/factory/desk/IFactoryDesk;->getPeqFoFine(I)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo2fineval:I

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0, v3}, Lcom/konka/factory/desk/IFactoryDesk;->getPeqFoFine(I)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo3fineval:I

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0, v4}, Lcom/konka/factory/desk/IFactoryDesk;->getPeqFoFine(I)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo4fineval:I

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0, v5}, Lcom/konka/factory/desk/IFactoryDesk;->getPeqFoFine(I)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo5fineval:I

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0, v1}, Lcom/konka/factory/desk/IFactoryDesk;->getPeqGain(I)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain1val:I

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0, v2}, Lcom/konka/factory/desk/IFactoryDesk;->getPeqGain(I)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain2val:I

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0, v3}, Lcom/konka/factory/desk/IFactoryDesk;->getPeqGain(I)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain3val:I

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0, v4}, Lcom/konka/factory/desk/IFactoryDesk;->getPeqGain(I)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain4val:I

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0, v5}, Lcom/konka/factory/desk/IFactoryDesk;->getPeqGain(I)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain5val:I

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0, v1}, Lcom/konka/factory/desk/IFactoryDesk;->getPeqQ(I)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q1val:I

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0, v2}, Lcom/konka/factory/desk/IFactoryDesk;->getPeqQ(I)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q2val:I

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0, v3}, Lcom/konka/factory/desk/IFactoryDesk;->getPeqQ(I)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q3val:I

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0, v4}, Lcom/konka/factory/desk/IFactoryDesk;->getPeqQ(I)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q4val:I

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0, v5}, Lcom/konka/factory/desk/IFactoryDesk;->getPeqQ(I)I

    move-result v0

    iput v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q5val:I

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo1coarse_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo1coarseval:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo2coarse_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo2coarseval:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo3coarse_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo3coarseval:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo4coarse_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo4coarseval:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo5coarse_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo5coarseval:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo1fine_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo1fineval:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo2fine_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo2fineval:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo3fine_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo3fineval:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo4fine_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo4fineval:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo5fine_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo5fineval:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_gain1_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain1val:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_gain2_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain2val:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_gain3_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain3val:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_gain4_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain4val:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_gain5_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain5val:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_q1_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q1val:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_q2_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q2val:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_q3_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q3val:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_q4_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q4val:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_q5_val:Landroid/widget/TextView;

    iget v1, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q5val:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v7, 0xf0

    const/16 v6, 0x63

    const/4 v5, 0x5

    const/16 v3, 0xa0

    const/4 v4, 0x0

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->peqActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v2}, Lcom/konka/factory/MainmenuActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v1

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    const/16 v2, 0x15

    if-eq p1, v2, :cond_0

    const/16 v2, 0x16

    if-ne p1, v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v2}, Lcom/konka/factory/desk/IFactoryDesk;->setPEQ()Z

    :cond_1
    return v0

    :sswitch_0
    sparse-switch v1, :sswitch_data_1

    goto :goto_0

    :sswitch_1
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo1coarseval:I

    if-ge v2, v3, :cond_2

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo1coarseval:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo1coarseval:I

    :goto_1
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo1coarse_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo1coarseval:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo1coarseval:I

    invoke-interface {v2, v4, v3}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqFoCoarse(II)Z

    goto :goto_0

    :cond_2
    iput v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo1coarseval:I

    goto :goto_1

    :sswitch_2
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo2coarseval:I

    if-ge v2, v3, :cond_3

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo2coarseval:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo2coarseval:I

    :goto_2
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo2coarse_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo2coarseval:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v3, 0x1

    iget v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo2coarseval:I

    invoke-interface {v2, v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqFoCoarse(II)Z

    goto :goto_0

    :cond_3
    iput v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo2coarseval:I

    goto :goto_2

    :sswitch_3
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo3coarseval:I

    if-ge v2, v3, :cond_4

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo3coarseval:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo3coarseval:I

    :goto_3
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo3coarse_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo3coarseval:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v3, 0x2

    iget v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo3coarseval:I

    invoke-interface {v2, v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqFoCoarse(II)Z

    goto :goto_0

    :cond_4
    iput v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo3coarseval:I

    goto :goto_3

    :sswitch_4
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo4coarseval:I

    if-ge v2, v3, :cond_5

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo4coarseval:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo4coarseval:I

    :goto_4
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo4coarse_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo4coarseval:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v3, 0x3

    iget v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo4coarseval:I

    invoke-interface {v2, v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqFoCoarse(II)Z

    goto/16 :goto_0

    :cond_5
    iput v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo4coarseval:I

    goto :goto_4

    :sswitch_5
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo5coarseval:I

    if-ge v2, v3, :cond_6

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo5coarseval:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo5coarseval:I

    :goto_5
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo5coarse_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo5coarseval:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v3, 0x4

    iget v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo5coarseval:I

    invoke-interface {v2, v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqFoCoarse(II)Z

    goto/16 :goto_0

    :cond_6
    iput v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo5coarseval:I

    goto :goto_5

    :sswitch_6
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo1fineval:I

    if-ge v2, v6, :cond_7

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo1fineval:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo1fineval:I

    :goto_6
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo1fine_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo1fineval:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo1fineval:I

    invoke-interface {v2, v4, v3}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqFoFine(II)Z

    goto/16 :goto_0

    :cond_7
    iput v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo1fineval:I

    goto :goto_6

    :sswitch_7
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo2fineval:I

    if-ge v2, v6, :cond_8

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo2fineval:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo2fineval:I

    :goto_7
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo2fine_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo2fineval:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v3, 0x1

    iget v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo2fineval:I

    invoke-interface {v2, v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqFoFine(II)Z

    goto/16 :goto_0

    :cond_8
    iput v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo2fineval:I

    goto :goto_7

    :sswitch_8
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo3fineval:I

    if-ge v2, v6, :cond_9

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo3fineval:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo3fineval:I

    :goto_8
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo3fine_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo3fineval:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v3, 0x2

    iget v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo3fineval:I

    invoke-interface {v2, v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqFoFine(II)Z

    goto/16 :goto_0

    :cond_9
    iput v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo3fineval:I

    goto :goto_8

    :sswitch_9
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo4fineval:I

    if-ge v2, v6, :cond_a

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo4fineval:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo4fineval:I

    :goto_9
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo4fine_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo4fineval:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v3, 0x3

    iget v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo4fineval:I

    invoke-interface {v2, v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqFoFine(II)Z

    goto/16 :goto_0

    :cond_a
    iput v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo4fineval:I

    goto :goto_9

    :sswitch_a
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo5fineval:I

    if-ge v2, v6, :cond_b

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo5fineval:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo5fineval:I

    :goto_a
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo5fine_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo5fineval:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v3, 0x4

    iget v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo5fineval:I

    invoke-interface {v2, v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqFoFine(II)Z

    goto/16 :goto_0

    :cond_b
    iput v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo5fineval:I

    goto :goto_a

    :sswitch_b
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain1val:I

    if-ge v2, v7, :cond_c

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain1val:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain1val:I

    :goto_b
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_gain1_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain1val:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain1val:I

    invoke-interface {v2, v4, v3}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqGain(II)Z

    goto/16 :goto_0

    :cond_c
    iput v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain1val:I

    goto :goto_b

    :sswitch_c
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain2val:I

    if-ge v2, v7, :cond_d

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain2val:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain2val:I

    :goto_c
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_gain2_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain2val:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v3, 0x1

    iget v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain2val:I

    invoke-interface {v2, v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqGain(II)Z

    goto/16 :goto_0

    :cond_d
    iput v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain2val:I

    goto :goto_c

    :sswitch_d
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain3val:I

    if-ge v2, v7, :cond_e

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain3val:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain3val:I

    :goto_d
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_gain3_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain3val:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v3, 0x2

    iget v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain3val:I

    invoke-interface {v2, v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqGain(II)Z

    goto/16 :goto_0

    :cond_e
    iput v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain3val:I

    goto :goto_d

    :sswitch_e
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain4val:I

    if-ge v2, v7, :cond_f

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain4val:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain4val:I

    :goto_e
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_gain4_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain4val:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v3, 0x3

    iget v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain4val:I

    invoke-interface {v2, v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqGain(II)Z

    goto/16 :goto_0

    :cond_f
    iput v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain4val:I

    goto :goto_e

    :sswitch_f
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain5val:I

    if-ge v2, v7, :cond_10

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain5val:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain5val:I

    :goto_f
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_gain5_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain5val:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v3, 0x4

    iget v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain5val:I

    invoke-interface {v2, v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqGain(II)Z

    goto/16 :goto_0

    :cond_10
    iput v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain5val:I

    goto :goto_f

    :sswitch_10
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q1val:I

    if-ge v2, v3, :cond_11

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q1val:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q1val:I

    :goto_10
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_q1_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q1val:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q1val:I

    invoke-interface {v2, v4, v3}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqQ(II)Z

    goto/16 :goto_0

    :cond_11
    iput v5, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q1val:I

    goto :goto_10

    :sswitch_11
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q2val:I

    if-ge v2, v3, :cond_12

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q2val:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q2val:I

    :goto_11
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_q2_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q2val:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v3, 0x1

    iget v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q2val:I

    invoke-interface {v2, v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqQ(II)Z

    goto/16 :goto_0

    :cond_12
    iput v5, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q2val:I

    goto :goto_11

    :sswitch_12
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q3val:I

    if-ge v2, v3, :cond_13

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q3val:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q3val:I

    :goto_12
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_q3_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q3val:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v3, 0x2

    iget v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q3val:I

    invoke-interface {v2, v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqQ(II)Z

    goto/16 :goto_0

    :cond_13
    iput v5, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q3val:I

    goto :goto_12

    :sswitch_13
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q4val:I

    if-ge v2, v3, :cond_14

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q4val:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q4val:I

    :goto_13
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_q4_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q4val:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v3, 0x3

    iget v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q4val:I

    invoke-interface {v2, v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqQ(II)Z

    goto/16 :goto_0

    :cond_14
    iput v5, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q4val:I

    goto :goto_13

    :sswitch_14
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q5val:I

    if-ge v2, v3, :cond_15

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q5val:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q5val:I

    :goto_14
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_q5_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q5val:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v3, 0x4

    iget v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q5val:I

    invoke-interface {v2, v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqQ(II)Z

    goto/16 :goto_0

    :cond_15
    iput v5, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q5val:I

    goto :goto_14

    :sswitch_15
    sparse-switch v1, :sswitch_data_2

    goto/16 :goto_0

    :sswitch_16
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo1coarseval:I

    if-eqz v2, :cond_16

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo1coarseval:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo1coarseval:I

    :goto_15
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo1coarse_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo1coarseval:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo1coarseval:I

    invoke-interface {v2, v4, v3}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqFoCoarse(II)Z

    goto/16 :goto_0

    :cond_16
    iput v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo1coarseval:I

    goto :goto_15

    :sswitch_17
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo2coarseval:I

    if-eqz v2, :cond_17

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo2coarseval:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo2coarseval:I

    :goto_16
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo2coarse_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo2coarseval:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v3, 0x1

    iget v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo2coarseval:I

    invoke-interface {v2, v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqFoCoarse(II)Z

    goto/16 :goto_0

    :cond_17
    iput v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo2coarseval:I

    goto :goto_16

    :sswitch_18
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo3coarseval:I

    if-eqz v2, :cond_18

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo3coarseval:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo3coarseval:I

    :goto_17
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo3coarse_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo3coarseval:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v3, 0x2

    iget v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo3coarseval:I

    invoke-interface {v2, v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqFoCoarse(II)Z

    goto/16 :goto_0

    :cond_18
    iput v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo3coarseval:I

    goto :goto_17

    :sswitch_19
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo4coarseval:I

    if-eqz v2, :cond_19

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo4coarseval:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo4coarseval:I

    :goto_18
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo4coarse_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo4coarseval:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v3, 0x3

    iget v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo4coarseval:I

    invoke-interface {v2, v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqFoCoarse(II)Z

    goto/16 :goto_0

    :cond_19
    iput v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo4coarseval:I

    goto :goto_18

    :sswitch_1a
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo5coarseval:I

    if-eqz v2, :cond_1a

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo5coarseval:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo5coarseval:I

    :goto_19
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo5coarse_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo5coarseval:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v3, 0x4

    iget v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo5coarseval:I

    invoke-interface {v2, v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqFoCoarse(II)Z

    goto/16 :goto_0

    :cond_1a
    iput v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo5coarseval:I

    goto :goto_19

    :sswitch_1b
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo1fineval:I

    if-eqz v2, :cond_1b

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo1fineval:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo1fineval:I

    :goto_1a
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo1fine_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo1fineval:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo1fineval:I

    invoke-interface {v2, v4, v3}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqFoFine(II)Z

    goto/16 :goto_0

    :cond_1b
    iput v6, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo1fineval:I

    goto :goto_1a

    :sswitch_1c
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo2fineval:I

    if-eqz v2, :cond_1c

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo2fineval:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo2fineval:I

    :goto_1b
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo2fine_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo2fineval:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v3, 0x1

    iget v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo2fineval:I

    invoke-interface {v2, v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqFoFine(II)Z

    goto/16 :goto_0

    :cond_1c
    iput v6, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo2fineval:I

    goto :goto_1b

    :sswitch_1d
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo3fineval:I

    if-eqz v2, :cond_1d

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo3fineval:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo3fineval:I

    :goto_1c
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo3fine_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo3fineval:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v3, 0x2

    iget v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo3fineval:I

    invoke-interface {v2, v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqFoFine(II)Z

    goto/16 :goto_0

    :cond_1d
    iput v6, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo3fineval:I

    goto :goto_1c

    :sswitch_1e
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo4fineval:I

    if-eqz v2, :cond_1e

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo4fineval:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo4fineval:I

    :goto_1d
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo4fine_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo4fineval:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v3, 0x3

    iget v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo4fineval:I

    invoke-interface {v2, v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqFoFine(II)Z

    goto/16 :goto_0

    :cond_1e
    iput v6, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo4fineval:I

    goto :goto_1d

    :sswitch_1f
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo5fineval:I

    if-eqz v2, :cond_1f

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo5fineval:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo5fineval:I

    :goto_1e
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_fo5fine_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo5fineval:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v3, 0x4

    iget v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo5fineval:I

    invoke-interface {v2, v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqFoFine(II)Z

    goto/16 :goto_0

    :cond_1f
    iput v6, p0, Lcom/konka/factory/PEQAdjustViewHolder;->fo5fineval:I

    goto :goto_1e

    :sswitch_20
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain1val:I

    if-eqz v2, :cond_20

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain1val:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain1val:I

    :goto_1f
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_gain1_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain1val:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain1val:I

    invoke-interface {v2, v4, v3}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqGain(II)Z

    goto/16 :goto_0

    :cond_20
    iput v7, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain1val:I

    goto :goto_1f

    :sswitch_21
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain2val:I

    if-eqz v2, :cond_21

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain2val:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain2val:I

    :goto_20
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_gain2_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain2val:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v3, 0x1

    iget v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain2val:I

    invoke-interface {v2, v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqGain(II)Z

    goto/16 :goto_0

    :cond_21
    iput v7, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain2val:I

    goto :goto_20

    :sswitch_22
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain3val:I

    if-eqz v2, :cond_22

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain3val:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain3val:I

    :goto_21
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_gain3_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain3val:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v3, 0x2

    iget v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain3val:I

    invoke-interface {v2, v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqGain(II)Z

    goto/16 :goto_0

    :cond_22
    iput v7, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain3val:I

    goto :goto_21

    :sswitch_23
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain4val:I

    if-eqz v2, :cond_23

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain4val:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain4val:I

    :goto_22
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_gain4_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain4val:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v3, 0x3

    iget v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain4val:I

    invoke-interface {v2, v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqGain(II)Z

    goto/16 :goto_0

    :cond_23
    iput v7, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain4val:I

    goto :goto_22

    :sswitch_24
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain5val:I

    if-eqz v2, :cond_24

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain5val:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain5val:I

    :goto_23
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_gain5_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain5val:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v3, 0x4

    iget v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain5val:I

    invoke-interface {v2, v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqGain(II)Z

    goto/16 :goto_0

    :cond_24
    iput v7, p0, Lcom/konka/factory/PEQAdjustViewHolder;->gain5val:I

    goto :goto_23

    :sswitch_25
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q1val:I

    if-eq v2, v5, :cond_25

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q1val:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q1val:I

    :goto_24
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_q1_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q1val:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q1val:I

    invoke-interface {v2, v4, v3}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqQ(II)Z

    goto/16 :goto_0

    :cond_25
    iput v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q1val:I

    goto :goto_24

    :sswitch_26
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q2val:I

    if-eq v2, v5, :cond_26

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q2val:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q2val:I

    :goto_25
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_q2_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q2val:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v3, 0x1

    iget v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q2val:I

    invoke-interface {v2, v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqQ(II)Z

    goto/16 :goto_0

    :cond_26
    iput v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q2val:I

    goto :goto_25

    :sswitch_27
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q3val:I

    if-eq v2, v5, :cond_27

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q3val:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q3val:I

    :goto_26
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_q3_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q3val:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v3, 0x2

    iget v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q3val:I

    invoke-interface {v2, v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqQ(II)Z

    goto/16 :goto_0

    :cond_27
    iput v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q3val:I

    goto :goto_26

    :sswitch_28
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q4val:I

    if-eq v2, v5, :cond_28

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q4val:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q4val:I

    :goto_27
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_q4_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q4val:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v3, 0x3

    iget v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q4val:I

    invoke-interface {v2, v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqQ(II)Z

    goto/16 :goto_0

    :cond_28
    iput v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q4val:I

    goto :goto_27

    :sswitch_29
    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q5val:I

    if-eq v2, v5, :cond_29

    iget v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q5val:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q5val:I

    :goto_28
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->text_factory_peq_q5_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q5val:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v3, 0x4

    iget v4, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q5val:I

    invoke-interface {v2, v3, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setPeqQ(II)Z

    goto/16 :goto_0

    :cond_29
    iput v3, p0, Lcom/konka/factory/PEQAdjustViewHolder;->q5val:I

    goto :goto_28

    :sswitch_2a
    iget-object v2, p0, Lcom/konka/factory/PEQAdjustViewHolder;->peqActivity:Lcom/konka/factory/MainmenuActivity;

    const/16 v3, 0xe

    invoke-virtual {v2, v3}, Lcom/konka/factory/MainmenuActivity;->returnRoot(I)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_2a
        0x15 -> :sswitch_15
        0x16 -> :sswitch_0
        0x52 -> :sswitch_2a
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x7f0a019a -> :sswitch_1    # com.konka.factory.R.id.linearlayout_factory_peq_fo1coarse
        0x7f0a019d -> :sswitch_6    # com.konka.factory.R.id.linearlayout_factory_peq_fo1fine
        0x7f0a01a0 -> :sswitch_2    # com.konka.factory.R.id.linearlayout_factory_peq_fo2coarse
        0x7f0a01a3 -> :sswitch_7    # com.konka.factory.R.id.linearlayout_factory_peq_fo2fine
        0x7f0a01a6 -> :sswitch_3    # com.konka.factory.R.id.linearlayout_factory_peq_fo3coarse
        0x7f0a01a9 -> :sswitch_8    # com.konka.factory.R.id.linearlayout_factory_peq_fo3fine
        0x7f0a01ac -> :sswitch_4    # com.konka.factory.R.id.linearlayout_factory_peq_fo4coarse
        0x7f0a01af -> :sswitch_9    # com.konka.factory.R.id.linearlayout_factory_peq_fo4fine
        0x7f0a01b2 -> :sswitch_5    # com.konka.factory.R.id.linearlayout_factory_peq_fo5coarse
        0x7f0a01b5 -> :sswitch_a    # com.konka.factory.R.id.linearlayout_factory_peq_fo5fine
        0x7f0a01b8 -> :sswitch_b    # com.konka.factory.R.id.linearlayout_factory_peq_gain1
        0x7f0a01bb -> :sswitch_c    # com.konka.factory.R.id.linearlayout_factory_peq_gain2
        0x7f0a01be -> :sswitch_d    # com.konka.factory.R.id.linearlayout_factory_peq_gain3
        0x7f0a01c1 -> :sswitch_e    # com.konka.factory.R.id.linearlayout_factory_peq_gain4
        0x7f0a01c4 -> :sswitch_f    # com.konka.factory.R.id.linearlayout_factory_peq_gain5
        0x7f0a01c7 -> :sswitch_10    # com.konka.factory.R.id.linearlayout_factory_peq_q1
        0x7f0a01ca -> :sswitch_11    # com.konka.factory.R.id.linearlayout_factory_peq_q2
        0x7f0a01cd -> :sswitch_12    # com.konka.factory.R.id.linearlayout_factory_peq_q3
        0x7f0a01d0 -> :sswitch_13    # com.konka.factory.R.id.linearlayout_factory_peq_q4
        0x7f0a01d3 -> :sswitch_14    # com.konka.factory.R.id.linearlayout_factory_peq_q5
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x7f0a019a -> :sswitch_16    # com.konka.factory.R.id.linearlayout_factory_peq_fo1coarse
        0x7f0a019d -> :sswitch_1b    # com.konka.factory.R.id.linearlayout_factory_peq_fo1fine
        0x7f0a01a0 -> :sswitch_17    # com.konka.factory.R.id.linearlayout_factory_peq_fo2coarse
        0x7f0a01a3 -> :sswitch_1c    # com.konka.factory.R.id.linearlayout_factory_peq_fo2fine
        0x7f0a01a6 -> :sswitch_18    # com.konka.factory.R.id.linearlayout_factory_peq_fo3coarse
        0x7f0a01a9 -> :sswitch_1d    # com.konka.factory.R.id.linearlayout_factory_peq_fo3fine
        0x7f0a01ac -> :sswitch_19    # com.konka.factory.R.id.linearlayout_factory_peq_fo4coarse
        0x7f0a01af -> :sswitch_1e    # com.konka.factory.R.id.linearlayout_factory_peq_fo4fine
        0x7f0a01b2 -> :sswitch_1a    # com.konka.factory.R.id.linearlayout_factory_peq_fo5coarse
        0x7f0a01b5 -> :sswitch_1f    # com.konka.factory.R.id.linearlayout_factory_peq_fo5fine
        0x7f0a01b8 -> :sswitch_20    # com.konka.factory.R.id.linearlayout_factory_peq_gain1
        0x7f0a01bb -> :sswitch_21    # com.konka.factory.R.id.linearlayout_factory_peq_gain2
        0x7f0a01be -> :sswitch_22    # com.konka.factory.R.id.linearlayout_factory_peq_gain3
        0x7f0a01c1 -> :sswitch_23    # com.konka.factory.R.id.linearlayout_factory_peq_gain4
        0x7f0a01c4 -> :sswitch_24    # com.konka.factory.R.id.linearlayout_factory_peq_gain5
        0x7f0a01c7 -> :sswitch_25    # com.konka.factory.R.id.linearlayout_factory_peq_q1
        0x7f0a01ca -> :sswitch_26    # com.konka.factory.R.id.linearlayout_factory_peq_q2
        0x7f0a01cd -> :sswitch_27    # com.konka.factory.R.id.linearlayout_factory_peq_q3
        0x7f0a01d0 -> :sswitch_28    # com.konka.factory.R.id.linearlayout_factory_peq_q4
        0x7f0a01d3 -> :sswitch_29    # com.konka.factory.R.id.linearlayout_factory_peq_q5
    .end sparse-switch
.end method
