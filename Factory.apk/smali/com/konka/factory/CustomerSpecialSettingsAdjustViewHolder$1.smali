.class Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder$1;
.super Ljava/lang/Object;
.source "CustomerSpecialSettingsAdjustViewHolder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->updateAdVideoFile()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;

.field final synthetic val$handler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;Landroid/os/Handler;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder$1;->this$0:Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;

    iput-object p2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder$1;->val$handler:Landroid/os/Handler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    const/4 v3, 0x0

    new-instance v5, Ljava/io/File;

    const-string v7, "/mnt/usb/sda1/AdBoot/bootvideo.mp4"

    invoke-direct {v5, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/io/File;

    const-string v7, "/data/misc/konka/AdBoot/AdBootMedia/bootvideo.mp4"

    invoke-direct {v0, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v6, Ljava/io/File;

    const-string v7, "/mnt/usb/sda1/AdBoot/videoConfigs.txt"

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/io/File;

    const-string v7, "/data/misc/konka/AdBoot/AdBootMedia/videoConfigs.txt"

    invoke-direct {v1, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_0
    iget-object v7, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder$1;->this$0:Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;

    # invokes: Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->copyFile(Ljava/io/File;Ljava/io/File;)Z
    invoke-static {v7, v5, v0}, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->access$000(Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;Ljava/io/File;Ljava/io/File;)Z

    move-result v7

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder$1;->this$0:Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;

    # invokes: Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->copyFile(Ljava/io/File;Ljava/io/File;)Z
    invoke-static {v7, v6, v1}, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->access$000(Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;Ljava/io/File;Ljava/io/File;)Z

    move-result v7

    if-eqz v7, :cond_0

    const/4 v3, 0x1

    :cond_0
    iget-object v7, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder$1;->this$0:Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;

    # invokes: Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->chmodFile(Ljava/io/File;)V
    invoke-static {v7, v0}, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->access$100(Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;Ljava/io/File;)V

    iget-object v7, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder$1;->this$0:Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;

    # invokes: Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->chmodFile(Ljava/io/File;)V
    invoke-static {v7, v1}, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->access$100(Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    move v4, v3

    iget-object v7, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder$1;->val$handler:Landroid/os/Handler;

    new-instance v8, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder$1$1;

    invoke-direct {v8, p0, v4}, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder$1$1;-><init>(Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder$1;Z)V

    invoke-virtual {v7, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method
