.class Lcom/konka/factory/OtherOptionAdjustViewHolder$7;
.super Ljava/lang/Object;
.source "OtherOptionAdjustViewHolder.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/factory/OtherOptionAdjustViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/factory/OtherOptionAdjustViewHolder;


# direct methods
.method constructor <init>(Lcom/konka/factory/OtherOptionAdjustViewHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder$7;->this$0:Lcom/konka/factory/OtherOptionAdjustViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder$7;->this$0:Lcom/konka/factory/OtherOptionAdjustViewHolder;

    # getter for: Lcom/konka/factory/OtherOptionAdjustViewHolder;->otheroptionActivity:Lcom/konka/factory/MainmenuActivity;
    invoke-static {v1}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->access$700(Lcom/konka/factory/OtherOptionAdjustViewHolder;)Lcom/konka/factory/MainmenuActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/factory/MainmenuActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    iget-object v1, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder$7;->this$0:Lcom/konka/factory/OtherOptionAdjustViewHolder;

    # invokes: Lcom/konka/factory/OtherOptionAdjustViewHolder;->openLayoutDialog()V
    invoke-static {v1}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->access$800(Lcom/konka/factory/OtherOptionAdjustViewHolder;)V

    goto :goto_0

    :sswitch_1
    iget-object v1, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder$7;->this$0:Lcom/konka/factory/OtherOptionAdjustViewHolder;

    # invokes: Lcom/konka/factory/OtherOptionAdjustViewHolder;->openLayoutDialogMain()V
    invoke-static {v1}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->access$900(Lcom/konka/factory/OtherOptionAdjustViewHolder;)V

    goto :goto_0

    :sswitch_2
    iget-object v1, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder$7;->this$0:Lcom/konka/factory/OtherOptionAdjustViewHolder;

    # invokes: Lcom/konka/factory/OtherOptionAdjustViewHolder;->openLayoutDialog6M30()V
    invoke-static {v1}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->access$1000(Lcom/konka/factory/OtherOptionAdjustViewHolder;)V

    goto :goto_0

    :sswitch_3
    iget-object v1, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder$7;->this$0:Lcom/konka/factory/OtherOptionAdjustViewHolder;

    # getter for: Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;
    invoke-static {v1}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->access$1100(Lcom/konka/factory/OtherOptionAdjustViewHolder;)Lcom/konka/factory/desk/IFactoryDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/factory/desk/IFactoryDesk;->enableUartDebug()Z

    goto :goto_0

    :sswitch_4
    iget-object v1, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder$7;->this$0:Lcom/konka/factory/OtherOptionAdjustViewHolder;

    # getter for: Lcom/konka/factory/OtherOptionAdjustViewHolder;->pq_update_state:Z
    invoke-static {v1}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->access$1200(Lcom/konka/factory/OtherOptionAdjustViewHolder;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder$7;->this$0:Lcom/konka/factory/OtherOptionAdjustViewHolder;

    iget-object v1, v1, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_pq_updagte_state:Landroid/widget/TextView;

    const-string v2, "Please Click!"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder$7;->this$0:Lcom/konka/factory/OtherOptionAdjustViewHolder;

    const/4 v2, 0x0

    # setter for: Lcom/konka/factory/OtherOptionAdjustViewHolder;->pq_update_state:Z
    invoke-static {v1, v2}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->access$1202(Lcom/konka/factory/OtherOptionAdjustViewHolder;Z)Z

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder$7;->this$0:Lcom/konka/factory/OtherOptionAdjustViewHolder;

    invoke-virtual {v1}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->updatePqFile()V

    iget-object v1, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder$7;->this$0:Lcom/konka/factory/OtherOptionAdjustViewHolder;

    const/4 v2, 0x1

    # setter for: Lcom/konka/factory/OtherOptionAdjustViewHolder;->pq_update_state:Z
    invoke-static {v1, v2}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->access$1202(Lcom/konka/factory/OtherOptionAdjustViewHolder;Z)Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x7f0a015f -> :sswitch_0    # com.konka.factory.R.id.linearlayout_factory_otheroption_upgrademboot
        0x7f0a0162 -> :sswitch_1    # com.konka.factory.R.id.linearlayout_factory_otheroption_upgrademain
        0x7f0a0165 -> :sswitch_2    # com.konka.factory.R.id.linearlayout_factory_otheroption_upgrade6m30
        0x7f0a017d -> :sswitch_3    # com.konka.factory.R.id.linearlayout_factory_otheroption_mstv_tool
        0x7f0a017f -> :sswitch_4    # com.konka.factory.R.id.linearlayout_factory_otheroption_pq_update
    .end sparse-switch
.end method
