.class Lcom/konka/factory/FactoryAutoTuneOptionActivity$1;
.super Landroid/os/Handler;
.source "FactoryAutoTuneOptionActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/factory/FactoryAutoTuneOptionActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;


# direct methods
.method constructor <init>(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$1;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1    # Landroid/os/Message;

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget v0, p1, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$1;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    # getter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->MSG_ONCREAT:I
    invoke-static {v1}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$000(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$1;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    # invokes: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->updateUiCoutryRequestFocus()V
    invoke-static {v0}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$100(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)V

    iget-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$1;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    # getter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;
    invoke-static {v0}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$300(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)Lcom/konka/factory/ViewHolder;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->text_cha_hint_totalpage_val:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$1;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    # getter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->page_total:I
    invoke-static {v1}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$200(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$1;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    iget-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$1;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    const v2, 0x7f0a001b    # com.konka.factory.R.id.image_left_arrow_hint

    invoke-virtual {v0, v2}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    # setter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->hint_left_arrow:Landroid/widget/ImageView;
    invoke-static {v1, v0}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$402(Lcom/konka/factory/FactoryAutoTuneOptionActivity;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$1;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    iget-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$1;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    const v2, 0x7f0a0026    # com.konka.factory.R.id.image_right_arrow_hint

    invoke-virtual {v0, v2}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    # setter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->hint_right_arrow:Landroid/widget/ImageView;
    invoke-static {v1, v0}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$502(Lcom/konka/factory/FactoryAutoTuneOptionActivity;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$1;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    # invokes: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->updateUiCoutrySelect()V
    invoke-static {v0}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$600(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)V

    :cond_0
    return-void
.end method
