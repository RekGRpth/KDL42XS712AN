.class Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;
.super Ljava/lang/Object;
.source "FactoryAutoTuneOptionActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/factory/FactoryAutoTuneOptionActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;


# direct methods
.method constructor <init>(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    iget-object v6, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    iget-object v7, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    invoke-virtual {v7}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->getApplication()Landroid/app/Application;

    move-result-object v7

    invoke-static {v7}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v7

    invoke-virtual {v7}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->queryCusDefSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    move-result-object v7

    # setter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->factoryCusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;
    invoke-static {v6, v7}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$702(Lcom/konka/factory/FactoryAutoTuneOptionActivity;Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;)Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    iget-object v6, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    # getter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->factoryCusDefSetting:Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;
    invoke-static {v6}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$700(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    move-result-object v6

    iget v0, v6, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->DefTuningCountry:I

    const-string v6, "FactoryAutoTuneOption"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "***11***curCountryIdex==  "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    iget-object v6, v6, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->curCustomer:Ljava/lang/String;

    sget-object v7, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->PersionCustomer:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    # getter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->CountrySelectTable_snowa:[I
    invoke-static {v6}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$800(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)[I

    move-result-object v6

    array-length v3, v6

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v6, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    # getter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->CountrySelectTable_snowa:[I
    invoke-static {v6}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$800(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)[I

    move-result-object v6

    aget v6, v6, v1

    if-ne v6, v0, :cond_1

    iget-object v6, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    # setter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->tableCountrySelect_index:I
    invoke-static {v6, v1}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$902(Lcom/konka/factory/FactoryAutoTuneOptionActivity;I)I

    :cond_0
    :goto_1
    const-string v6, "FactoryAutoTuneOption"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "***22***tableCountrySelect_index==  "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    # getter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->tableCountrySelect_index:I
    invoke-static {v8}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$900(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    iget-object v7, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    # getter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->tableCountrySelect_index:I
    invoke-static {v7}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$900(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)I

    move-result v7

    div-int/lit8 v7, v7, 0x9

    # setter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->page_index:I
    invoke-static {v6, v7}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$1202(Lcom/konka/factory/FactoryAutoTuneOptionActivity;I)I

    iget-object v6, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    iget-object v7, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    # getter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->tableCountrySelect_index:I
    invoke-static {v7}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$900(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)I

    move-result v7

    rem-int/lit8 v7, v7, 0x9

    # setter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->curItem_index:I
    invoke-static {v6, v7}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$1302(Lcom/konka/factory/FactoryAutoTuneOptionActivity;I)I

    iget-object v6, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    new-instance v7, Lcom/konka/factory/ViewHolder;

    iget-object v8, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    invoke-direct {v7, v8}, Lcom/konka/factory/ViewHolder;-><init>(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)V

    # setter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;
    invoke-static {v6, v7}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$302(Lcom/konka/factory/FactoryAutoTuneOptionActivity;Lcom/konka/factory/ViewHolder;)Lcom/konka/factory/ViewHolder;

    iget-object v6, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    # getter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->viewholder_autotune:Lcom/konka/factory/ViewHolder;
    invoke-static {v6}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$300(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)Lcom/konka/factory/ViewHolder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/konka/factory/ViewHolder;->findViewsForAutoTuning()V

    iget-object v6, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    iget-object v7, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    invoke-virtual {v7}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v7

    # setter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v6, v7}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$1402(Lcom/konka/factory/FactoryAutoTuneOptionActivity;Lcom/konka/kkinterface/tv/TvDeskProvider;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v6, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    # getter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;
    invoke-static {v6}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$1400(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v6

    invoke-interface {v6}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getChannelManagerInstance()Lcom/konka/kkinterface/tv/ChannelDesk;

    move-result-object v5

    iget-object v6, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    invoke-interface {v5}, Lcom/konka/kkinterface/tv/ChannelDesk;->getUserScanType()Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;

    move-result-object v7

    invoke-virtual {v7}, Lcom/konka/kkinterface/tv/ChannelDesk$EN_TUNING_SCAN_TYPE;->ordinal()I

    move-result v7

    # setter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->tuningtype:I
    invoke-static {v6, v7}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$1502(Lcom/konka/factory/FactoryAutoTuneOptionActivity;I)I

    iget-object v6, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    iget-object v6, v6, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->curCustomer:Ljava/lang/String;

    sget-object v7, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->PersionCustomer:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    iget-object v7, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    invoke-virtual {v7}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090002    # com.konka.factory.R.array.str_arr_autotuning_country_snowa

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v7

    # setter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->OptionCountrys:[Ljava/lang/String;
    invoke-static {v6, v7}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$1602(Lcom/konka/factory/FactoryAutoTuneOptionActivity;[Ljava/lang/String;)[Ljava/lang/String;

    :goto_2
    iget-object v6, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    iget-object v7, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    # getter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->OptionCountrys:[Ljava/lang/String;
    invoke-static {v7}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$1600(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)[Ljava/lang/String;

    move-result-object v7

    array-length v7, v7

    # setter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->MAXINDEXS:I
    invoke-static {v6, v7}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$1702(Lcom/konka/factory/FactoryAutoTuneOptionActivity;I)I

    iget-object v6, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    # getter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->MAXINDEXS:I
    invoke-static {v6}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$1700(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)I

    move-result v6

    rem-int/lit8 v6, v6, 0x9

    if-nez v6, :cond_8

    iget-object v6, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    iget-object v7, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    # getter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->MAXINDEXS:I
    invoke-static {v7}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$1700(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)I

    move-result v7

    div-int/lit8 v7, v7, 0x9

    # setter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->page_total:I
    invoke-static {v6, v7}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$202(Lcom/konka/factory/FactoryAutoTuneOptionActivity;I)I

    :goto_3
    iget-object v6, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    # invokes: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->registerListeners()V
    invoke-static {v6}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$1800(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)V

    iget-object v6, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    # getter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->mHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$1900(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)Landroid/os/Handler;

    move-result-object v6

    iget-object v7, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    # getter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->MSG_ONCREAT:I
    invoke-static {v7}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$000(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    :cond_2
    iget-object v6, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    iget-object v6, v6, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->curCustomer:Ljava/lang/String;

    sget-object v7, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->PersionCustomer1:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    # getter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->CountrySelectTable_xvision:[I
    invoke-static {v6}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$1000(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)[I

    move-result-object v6

    array-length v4, v6

    const/4 v1, 0x0

    :goto_4
    if-ge v1, v4, :cond_0

    iget-object v6, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    # getter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->CountrySelectTable_xvision:[I
    invoke-static {v6}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$1000(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)[I

    move-result-object v6

    aget v6, v6, v1

    if-ne v6, v0, :cond_3

    iget-object v6, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    # setter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->tableCountrySelect_index:I
    invoke-static {v6, v1}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$902(Lcom/konka/factory/FactoryAutoTuneOptionActivity;I)I

    goto/16 :goto_1

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_4
    iget-object v6, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    # getter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->CountrySelectTable:[I
    invoke-static {v6}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$1100(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)[I

    move-result-object v6

    array-length v2, v6

    const/4 v1, 0x0

    :goto_5
    if-ge v1, v2, :cond_0

    iget-object v6, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    # getter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->CountrySelectTable:[I
    invoke-static {v6}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$1100(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)[I

    move-result-object v6

    aget v6, v6, v1

    if-ne v6, v0, :cond_5

    iget-object v6, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    # setter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->tableCountrySelect_index:I
    invoke-static {v6, v1}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$902(Lcom/konka/factory/FactoryAutoTuneOptionActivity;I)I

    goto/16 :goto_1

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_6
    iget-object v6, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    iget-object v6, v6, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->curCustomer:Ljava/lang/String;

    sget-object v7, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->PersionCustomer1:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    iget-object v6, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    iget-object v7, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    invoke-virtual {v7}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090003    # com.konka.factory.R.array.str_arr_autotuning_country_xvision

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v7

    # setter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->OptionCountrys:[Ljava/lang/String;
    invoke-static {v6, v7}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$1602(Lcom/konka/factory/FactoryAutoTuneOptionActivity;[Ljava/lang/String;)[Ljava/lang/String;

    goto/16 :goto_2

    :cond_7
    iget-object v6, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    iget-object v7, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    invoke-virtual {v7}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090001    # com.konka.factory.R.array.str_arr_autotuning_country

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v7

    # setter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->OptionCountrys:[Ljava/lang/String;
    invoke-static {v6, v7}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$1602(Lcom/konka/factory/FactoryAutoTuneOptionActivity;[Ljava/lang/String;)[Ljava/lang/String;

    goto/16 :goto_2

    :cond_8
    iget-object v6, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    iget-object v7, p0, Lcom/konka/factory/FactoryAutoTuneOptionActivity$2;->this$0:Lcom/konka/factory/FactoryAutoTuneOptionActivity;

    # getter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->MAXINDEXS:I
    invoke-static {v7}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$1700(Lcom/konka/factory/FactoryAutoTuneOptionActivity;)I

    move-result v7

    div-int/lit8 v7, v7, 0x9

    add-int/lit8 v7, v7, 0x1

    # setter for: Lcom/konka/factory/FactoryAutoTuneOptionActivity;->page_total:I
    invoke-static {v6, v7}, Lcom/konka/factory/FactoryAutoTuneOptionActivity;->access$202(Lcom/konka/factory/FactoryAutoTuneOptionActivity;I)I

    goto/16 :goto_3
.end method
