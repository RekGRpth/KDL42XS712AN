.class Lcom/konka/factory/CustomerIDActivity$1;
.super Landroid/os/Handler;
.source "CustomerIDActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/factory/CustomerIDActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/factory/CustomerIDActivity;


# direct methods
.method constructor <init>(Lcom/konka/factory/CustomerIDActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/factory/CustomerIDActivity$1;->this$0:Lcom/konka/factory/CustomerIDActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1    # Landroid/os/Message;

    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    iget v0, p1, Landroid/os/Message;->what:I

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity$1;->this$0:Lcom/konka/factory/CustomerIDActivity;

    # getter for: Lcom/konka/factory/CustomerIDActivity;->MSG_ONCREAT:I
    invoke-static {v1}, Lcom/konka/factory/CustomerIDActivity;->access$000(Lcom/konka/factory/CustomerIDActivity;)I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/factory/CustomerIDActivity$1;->this$0:Lcom/konka/factory/CustomerIDActivity;

    # invokes: Lcom/konka/factory/CustomerIDActivity;->updateUiCoutryRequestFocus()V
    invoke-static {v0}, Lcom/konka/factory/CustomerIDActivity;->access$100(Lcom/konka/factory/CustomerIDActivity;)V

    iget-object v0, p0, Lcom/konka/factory/CustomerIDActivity$1;->this$0:Lcom/konka/factory/CustomerIDActivity;

    # getter for: Lcom/konka/factory/CustomerIDActivity;->viewholder_customerid:Lcom/konka/factory/ViewHolder;
    invoke-static {v0}, Lcom/konka/factory/CustomerIDActivity;->access$300(Lcom/konka/factory/CustomerIDActivity;)Lcom/konka/factory/ViewHolder;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/factory/ViewHolder;->text_cha_hint_totalpage_val:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity$1;->this$0:Lcom/konka/factory/CustomerIDActivity;

    # getter for: Lcom/konka/factory/CustomerIDActivity;->page_total:I
    invoke-static {v1}, Lcom/konka/factory/CustomerIDActivity;->access$200(Lcom/konka/factory/CustomerIDActivity;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity$1;->this$0:Lcom/konka/factory/CustomerIDActivity;

    iget-object v0, p0, Lcom/konka/factory/CustomerIDActivity$1;->this$0:Lcom/konka/factory/CustomerIDActivity;

    const v2, 0x7f0a001b    # com.konka.factory.R.id.image_left_arrow_hint

    invoke-virtual {v0, v2}, Lcom/konka/factory/CustomerIDActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    # setter for: Lcom/konka/factory/CustomerIDActivity;->hint_left_arrow:Landroid/widget/ImageView;
    invoke-static {v1, v0}, Lcom/konka/factory/CustomerIDActivity;->access$402(Lcom/konka/factory/CustomerIDActivity;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/konka/factory/CustomerIDActivity$1;->this$0:Lcom/konka/factory/CustomerIDActivity;

    iget-object v0, p0, Lcom/konka/factory/CustomerIDActivity$1;->this$0:Lcom/konka/factory/CustomerIDActivity;

    const v2, 0x7f0a0026    # com.konka.factory.R.id.image_right_arrow_hint

    invoke-virtual {v0, v2}, Lcom/konka/factory/CustomerIDActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    # setter for: Lcom/konka/factory/CustomerIDActivity;->hint_right_arrow:Landroid/widget/ImageView;
    invoke-static {v1, v0}, Lcom/konka/factory/CustomerIDActivity;->access$502(Lcom/konka/factory/CustomerIDActivity;Landroid/widget/ImageView;)Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/konka/factory/CustomerIDActivity$1;->this$0:Lcom/konka/factory/CustomerIDActivity;

    # invokes: Lcom/konka/factory/CustomerIDActivity;->updateUiCoutrySelect()V
    invoke-static {v0}, Lcom/konka/factory/CustomerIDActivity;->access$600(Lcom/konka/factory/CustomerIDActivity;)V

    :cond_0
    return-void
.end method
