.class public Lcom/konka/factory/UPDATEAdjustViewHolder;
.super Ljava/lang/Object;
.source "UPDATEAdjustViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/factory/UPDATEAdjustViewHolder$1;,
        Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;,
        Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;
    }
.end annotation


# static fields
.field private static index:I

.field private static macstr:Ljava/lang/String;

.field private static offsetStr:Ljava/lang/String;


# instance fields
.field private factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

.field private gridview_hdcpkey:Landroid/widget/GridView;

.field private textview_usbreadhdcpkey:Landroid/widget/TextView;

.field private textview_usbwritehdcpkey:Landroid/widget/TextView;

.field private textview_usbwritemacaddr:Landroid/widget/TextView;

.field private updateActivity:Lcom/konka/factory/MainmenuActivity;


# direct methods
.method public constructor <init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V
    .locals 0
    .param p1    # Lcom/konka/factory/MainmenuActivity;
    .param p2    # Lcom/konka/factory/desk/IFactoryDesk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/konka/factory/UPDATEAdjustViewHolder;->updateActivity:Lcom/konka/factory/MainmenuActivity;

    iput-object p2, p0, Lcom/konka/factory/UPDATEAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    return-void
.end method

.method public static DetectFileFromUSB(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p0    # Ljava/lang/String;

    new-instance v3, Ljava/io/File;

    const-string v6, "/mnt/usb/"

    invoke-direct {v3, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/String;

    const-string v6, "/mnt/usb/"

    invoke-direct {v5, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->isDirectory()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v3}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    const/4 v2, 0x0

    :goto_0
    array-length v6, v1

    if-ge v2, v6, :cond_1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget-object v7, v1, v2

    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v4, Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_0

    move-object v6, v5

    :goto_1
    return-object v6

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public static Write_CIPlusKey(Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;Ljava/util/HashMap;[B)Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;
    .locals 21
    .param p0    # Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;
    .param p2    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Double;",
            ">;[B)",
            "Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;"
        }
    .end annotation

    sget-object v18, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v19, "Write_CIPLUSKey!"

    invoke-virtual/range {v18 .. v19}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/16 v18, 0x1572

    move/from16 v0, v18

    new-array v4, v0, [B

    new-instance v10, Ljava/io/File;

    const-string v18, "/customercfg/ci_plus_production.bin"

    move-object/from16 v0, v18

    invoke-direct {v10, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v15, 0x0

    sget-object v18, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;->UPGRADE_MODE_USB:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    if-ne v0, v1, :cond_3

    const-string v18, "ciplus_key/ciplus_index.txt"

    invoke-static/range {v18 .. v18}, Lcom/konka/factory/UPDATEAdjustViewHolder;->DetectFileFromUSB(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    if-nez v13, :cond_0

    sget-object v18, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_NO_FILE:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    :goto_0
    return-object v18

    :cond_0
    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v18, Ljava/io/FileReader;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "ciplus_key/ciplus_index.txt"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-direct {v3, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    const-string v2, ""

    const/16 v16, 0x0

    const/4 v9, 0x0

    :goto_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v16

    if-eqz v16, :cond_1

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    move v7, v8

    move v14, v7

    :goto_2
    add-int/lit8 v18, v7, 0x14

    move/from16 v0, v18

    if-ge v14, v0, :cond_2

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "ciplus_key/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ".bin"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/konka/factory/UPDATEAdjustViewHolder;->DetectFileFromUSB(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v18, "lidongdong"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "=pathUSB.isDirectory()  =cikeyPath==  "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v8, v14, 0x1

    if-eqz v9, :cond_4

    :cond_2
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, ""

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    const-string v18, "lidongdong"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "=pathUSB.isDirectory()  =s2==  "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v6, Ljava/io/BufferedWriter;

    new-instance v18, Ljava/io/FileWriter;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "ciplus_key/ciplus_index.txt"

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-direct/range {v18 .. v19}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v18

    invoke-direct {v6, v0}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/io/BufferedWriter;->write(Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/BufferedWriter;->close()V

    new-instance v11, Ljava/io/FileInputStream;

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "ciplus_key/"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    add-int/lit8 v19, v8, -0x1

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, ".bin"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v11, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v4}, Ljava/io/FileInputStream;->read([B)I

    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_3
    const/16 v18, 0x2000

    :try_start_1
    move/from16 v0, v18

    new-array v5, v0, [S

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v18

    const/16 v19, 0x1d

    const/16 v20, 0x2000

    invoke-virtual/range {v18 .. v20}, Lcom/mstar/android/tvapi/common/TvManager;->readFromSpiFlashByBank(II)[S

    move-result-object v5

    const/4 v14, 0x0

    :goto_3
    const/16 v18, 0x1572

    move/from16 v0, v18

    if-ge v14, v0, :cond_5

    add-int/lit16 v0, v14, 0x400

    move/from16 v18, v0

    aget-byte v19, v4, v14

    move/from16 v0, v19

    int-to-short v0, v0

    move/from16 v19, v0

    aput-short v19, v5, v18
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_2

    add-int/lit8 v14, v14, 0x1

    goto :goto_3

    :cond_4
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_2

    :catch_0
    move-exception v12

    invoke-virtual {v12}, Ljava/io/FileNotFoundException;->printStackTrace()V

    sget-object v18, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_NO_FILE:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    goto/16 :goto_0

    :catch_1
    move-exception v12

    invoke-virtual {v12}, Ljava/io/IOException;->printStackTrace()V

    sget-object v18, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_READ_FILE_FAIL:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    goto/16 :goto_0

    :cond_5
    :try_start_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v18

    const/16 v19, 0x1d

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v0, v1, v5}, Lcom/mstar/android/tvapi/common/TvManager;->writeToSpiFlashByBank(I[S)Z

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v18

    if-eqz v18, :cond_6

    invoke-virtual {v10}, Ljava/io/File;->isFile()Z

    move-result v18

    if-eqz v18, :cond_6

    invoke-virtual {v10}, Ljava/io/File;->delete()Z
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_6
    sget-object v18, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_OK:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    goto/16 :goto_0

    :catch_2
    move-exception v12

    invoke-virtual {v12}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    sget-object v18, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_RW_SPI_FAIL:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    goto/16 :goto_0
.end method

.method public static Write_HDCPKey(Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;Ljava/util/HashMap;[B)Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;
    .locals 29
    .param p0    # Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;
    .param p2    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Double;",
            ">;[B)",
            "Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;"
        }
    .end annotation

    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v25, "Write_HDCPKey!"

    invoke-virtual/range {v24 .. v25}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/16 v24, 0x130

    move/from16 v0, v24

    new-array v3, v0, [B

    const/16 v17, 0x0

    sget-object v24, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;->UPGRADE_MODE_USB:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_12

    const-string v24, "hdcpkey.bin"

    invoke-static/range {v24 .. v24}, Lcom/konka/factory/UPDATEAdjustViewHolder;->DetectFileFromUSB(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-nez v9, :cond_0

    sget-object v24, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_NO_FILE:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    :goto_0
    return-object v24

    :cond_0
    :try_start_0
    new-instance v11, Ljava/io/FileInputStream;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v24

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "hdcpkey_index.bin"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-direct {v11, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    const/16 v24, 0x4

    move/from16 v0, v24

    new-array v4, v0, [B

    invoke-virtual {v11, v4}, Ljava/io/FileInputStream;->read([B)I

    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V

    const/4 v14, 0x0

    const/4 v13, 0x3

    :goto_1
    if-ltz v13, :cond_2

    mul-int/lit16 v14, v14, 0x100

    aget-byte v23, v4, v13

    if-gez v23, :cond_1

    move/from16 v0, v23

    add-int/lit16 v0, v0, 0x100

    move/from16 v23, v0

    :cond_1
    add-int v14, v14, v23

    add-int/lit8 v13, v13, -0x1

    goto :goto_1

    :cond_2
    const-string v24, "index"

    int-to-double v0, v14

    move-wide/from16 v25, v0

    invoke-static/range {v25 .. v26}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v25, "hdcp key index: %d\n"

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    aput-object v28, v26, v27

    invoke-virtual/range {v24 .. v26}, Ljava/io/PrintStream;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;

    new-instance v10, Ljava/io/FileInputStream;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v24

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "hdcpkey.bin"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-direct {v10, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Ljava/io/FileInputStream;->available()I

    move-result v18

    const/16 v24, 0x20

    move/from16 v0, v18

    move/from16 v1, v24

    if-ge v0, v1, :cond_3

    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V

    sget-object v24, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_READ_FILE_FAIL:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    goto/16 :goto_0

    :cond_3
    const/16 v24, 0x0

    const/16 v25, 0x20

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v10, v3, v0, v1}, Ljava/io/FileInputStream;->read([BII)I

    const/16 v21, 0x0

    const/16 v13, 0x12

    :goto_2
    const/16 v24, 0x14

    move/from16 v0, v24

    if-ge v13, v0, :cond_5

    move/from16 v0, v21

    mul-int/lit16 v0, v0, 0x100

    move/from16 v24, v0

    move/from16 v0, v24

    int-to-short v0, v0

    move/from16 v21, v0

    aget-byte v23, v3, v13

    if-gez v23, :cond_4

    move/from16 v0, v23

    add-int/lit16 v0, v0, 0x100

    move/from16 v23, v0

    :cond_4
    add-int v24, v21, v23

    move/from16 v0, v24

    int-to-short v0, v0

    move/from16 v21, v0

    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    :cond_5
    const-string v24, "size"

    move/from16 v0, v21

    int-to-double v0, v0

    move-wide/from16 v25, v0

    invoke-static/range {v25 .. v26}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move/from16 v17, v21

    const/16 v19, 0x0

    const/16 v13, 0x14

    :goto_3
    const/16 v24, 0x18

    move/from16 v0, v24

    if-ge v13, v0, :cond_7

    move/from16 v0, v19

    mul-int/lit16 v0, v0, 0x100

    move/from16 v19, v0

    aget-byte v23, v3, v13

    if-gez v23, :cond_6

    move/from16 v0, v23

    add-int/lit16 v0, v0, 0x100

    move/from16 v23, v0

    :cond_6
    add-int v19, v19, v23

    add-int/lit8 v13, v13, 0x1

    goto :goto_3

    :cond_7
    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v25, "hdcp key max: %d\n"

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    aput-object v28, v26, v27

    invoke-virtual/range {v24 .. v26}, Ljava/io/PrintStream;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;

    move/from16 v0, v19

    if-le v14, v0, :cond_8

    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V

    sget-object v24, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_DATA_OVER:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    goto/16 :goto_0

    :cond_8
    mul-int v24, v21, v14

    add-int/lit8 v20, v24, 0x20

    const-string v24, "offset"

    move/from16 v0, v20

    int-to-double v0, v0

    move-wide/from16 v25, v0

    invoke-static/range {v25 .. v26}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v25, "hdcp key offset: 0x%08x\n"

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v28

    aput-object v28, v26, v27

    invoke-virtual/range {v24 .. v26}, Ljava/io/PrintStream;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;

    add-int v24, v20, v21

    move/from16 v0, v18

    move/from16 v1, v24

    if-lt v0, v1, :cond_9

    const/16 v24, 0x130

    move/from16 v0, v21

    move/from16 v1, v24

    if-le v0, v1, :cond_a

    :cond_9
    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V

    sget-object v24, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_READ_FILE_FAIL:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    goto/16 :goto_0

    :cond_a
    add-int/lit8 v24, v20, -0x20

    move/from16 v0, v24

    int-to-long v0, v0

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    invoke-virtual {v10, v0, v1}, Ljava/io/FileInputStream;->skip(J)J

    const/16 v24, 0x0

    move/from16 v0, v24

    move/from16 v1, v21

    invoke-virtual {v10, v3, v0, v1}, Ljava/io/FileInputStream;->read([BII)I

    const/4 v13, 0x0

    :goto_4
    move/from16 v0, v21

    if-ge v13, v0, :cond_c

    const/4 v15, 0x0

    :goto_5
    const/16 v24, 0x10

    move/from16 v0, v24

    if-ge v15, v0, :cond_b

    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v25, "%02X "

    const/16 v26, 0x1

    move/from16 v0, v26

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v26, v0

    const/16 v27, 0x0

    add-int v28, v13, v15

    aget-byte v28, v3, v28

    invoke-static/range {v28 .. v28}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v28

    aput-object v28, v26, v27

    invoke-virtual/range {v24 .. v26}, Ljava/io/PrintStream;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;

    add-int/lit8 v15, v15, 0x1

    goto :goto_5

    :cond_b
    sget-object v24, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual/range {v24 .. v24}, Ljava/io/PrintStream;->println()V

    add-int/lit8 v13, v13, 0x10

    goto :goto_4

    :cond_c
    add-int/lit8 v14, v14, 0x1

    const/4 v13, 0x0

    :goto_6
    const/16 v24, 0x4

    move/from16 v0, v24

    if-ge v13, v0, :cond_d

    rem-int/lit16 v0, v14, 0x100

    move/from16 v24, v0

    move/from16 v0, v24

    int-to-byte v0, v0

    move/from16 v24, v0

    aput-byte v24, v4, v13

    div-int/lit16 v14, v14, 0x100

    add-int/lit8 v13, v13, 0x1

    goto :goto_6

    :cond_d
    new-instance v12, Ljava/io/FileOutputStream;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v24

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "hdcpkey_index.bin"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-direct {v12, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v4}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual {v12}, Ljava/io/FileOutputStream;->close()V

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v24

    const-string v25, "sync"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    const-wide/16 v24, 0xbb8

    :try_start_1
    invoke-static/range {v24 .. v25}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    :goto_7
    const/4 v7, 0x0

    const/16 v22, 0x1

    :goto_8
    :try_start_2
    new-instance v11, Ljava/io/FileInputStream;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v24

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "hdcpkey_index.bin"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-direct {v11, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/io/FileInputStream;->available()I

    move-result v16

    const/16 v24, 0x4

    move/from16 v0, v16

    move/from16 v1, v24

    if-ge v0, v1, :cond_f

    const/16 v22, 0x0

    :cond_e
    :goto_9
    invoke-virtual {v11}, Ljava/io/FileInputStream;->close()V

    if-nez v22, :cond_13

    add-int/lit8 v7, v7, 0x1

    const/16 v24, 0x5

    move/from16 v0, v24

    if-le v7, v0, :cond_11

    sget-object v24, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_READ_FILE_FAIL:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    goto/16 :goto_0

    :catch_0
    move-exception v8

    invoke-virtual {v8}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_7

    :catch_1
    move-exception v8

    invoke-virtual {v8}, Ljava/io/FileNotFoundException;->printStackTrace()V

    sget-object v24, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_NO_FILE:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    goto/16 :goto_0

    :cond_f
    :try_start_3
    move/from16 v0, v16

    new-array v5, v0, [B

    invoke-virtual {v11, v5}, Ljava/io/FileInputStream;->read([B)I

    const/4 v13, 0x0

    :goto_a
    const/16 v24, 0x4

    move/from16 v0, v24

    if-ge v13, v0, :cond_e

    aget-byte v24, v5, v13

    aget-byte v25, v4, v13
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_10

    const/16 v22, 0x0

    goto :goto_9

    :cond_10
    add-int/lit8 v13, v13, 0x1

    goto :goto_a

    :cond_11
    const-wide/16 v24, 0x7d0

    :try_start_4
    invoke-static/range {v24 .. v25}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_8

    :catch_2
    move-exception v8

    :try_start_5
    invoke-virtual {v8}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_8

    :catch_3
    move-exception v8

    invoke-virtual {v8}, Ljava/io/IOException;->printStackTrace()V

    sget-object v24, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_READ_FILE_FAIL:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    goto/16 :goto_0

    :cond_12
    sget-object v24, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;->UPGRADE_MODE_NET:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    if-ne v0, v1, :cond_13

    move-object/from16 v0, p2

    array-length v0, v0

    move/from16 v17, v0

    const/4 v13, 0x0

    :goto_b
    move/from16 v0, v17

    if-ge v13, v0, :cond_13

    aget-byte v24, p2, v13

    aput-byte v24, v3, v13

    const/16 v24, 0x130

    move/from16 v0, v24

    if-le v13, v0, :cond_14

    :cond_13
    const/16 v24, 0x2000

    :try_start_6
    move/from16 v0, v24

    new-array v6, v0, [S

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v24

    const/16 v25, 0x1d

    const/16 v26, 0x2000

    invoke-virtual/range {v24 .. v26}, Lcom/mstar/android/tvapi/common/TvManager;->readFromSpiFlashByBank(II)[S

    move-result-object v6

    const/4 v13, 0x0

    :goto_c
    const/16 v24, 0x130

    move/from16 v0, v24

    if-ge v13, v0, :cond_15

    add-int/lit16 v0, v13, 0x100

    move/from16 v24, v0

    aget-byte v25, v3, v13

    move/from16 v0, v25

    int-to-short v0, v0

    move/from16 v25, v0

    aput-short v25, v6, v24

    add-int/lit8 v13, v13, 0x1

    goto :goto_c

    :cond_14
    add-int/lit8 v13, v13, 0x1

    goto :goto_b

    :cond_15
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v24

    const/16 v25, 0x1d

    move-object/from16 v0, v24

    move/from16 v1, v25

    invoke-virtual {v0, v1, v6}, Lcom/mstar/android/tvapi/common/TvManager;->writeToSpiFlashByBank(I[S)Z
    :try_end_6
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_6 .. :try_end_6} :catch_4

    sget-object v24, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_OK:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    goto/16 :goto_0

    :catch_4
    move-exception v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    sget-object v24, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_RW_SPI_FAIL:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    goto/16 :goto_0
.end method

.method public static Write_MACaddr(Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;[B)Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;
    .locals 27
    .param p0    # Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;
    .param p1    # [B

    sget-object v22, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v23, "Write_MACaddr!"

    invoke-virtual/range {v22 .. v23}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/16 v22, 0xc

    move/from16 v0, v22

    new-array v3, v0, [B

    sget-object v22, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;->UPGRADE_MODE_USB:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    if-ne v0, v1, :cond_c

    const-string v22, "mac.bin"

    invoke-static/range {v22 .. v22}, Lcom/konka/factory/UPDATEAdjustViewHolder;->DetectFileFromUSB(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    if-nez v11, :cond_0

    sget-object v22, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_NO_FILE:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    :goto_0
    return-object v22

    :cond_0
    :try_start_0
    new-instance v12, Ljava/io/FileInputStream;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "mac.bin"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-direct {v12, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/io/FileInputStream;->available()I

    move-result v15

    const/16 v22, 0xc

    move/from16 v0, v22

    if-ge v15, v0, :cond_1

    invoke-virtual {v12}, Ljava/io/FileInputStream;->close()V

    sget-object v22, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_READ_FILE_FAIL:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    goto :goto_0

    :cond_1
    invoke-virtual {v12, v3}, Ljava/io/FileInputStream;->read([B)I

    sget-object v22, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v23, "Mac Addr %02X:%02X:%02X:%02X:%02X:%02X\n"

    const/16 v24, 0x6

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    const/16 v26, 0x0

    aget-byte v26, v3, v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x1

    const/16 v26, 0x1

    aget-byte v26, v3, v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x2

    const/16 v26, 0x2

    aget-byte v26, v3, v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x3

    const/16 v26, 0x3

    aget-byte v26, v3, v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x4

    const/16 v26, 0x4

    aget-byte v26, v3, v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v26

    aput-object v26, v24, v25

    const/16 v25, 0x5

    const/16 v26, 0x5

    aget-byte v26, v3, v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v26

    aput-object v26, v24, v25

    invoke-virtual/range {v22 .. v24}, Ljava/io/PrintStream;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;

    const-wide/16 v8, 0x0

    const-wide/16 v17, 0x0

    const/16 v22, 0xc

    move/from16 v0, v22

    new-array v6, v0, [B

    const/4 v14, 0x0

    :goto_1
    const/16 v22, 0x6

    move/from16 v0, v22

    if-ge v14, v0, :cond_3

    const-wide/16 v22, 0x100

    mul-long v8, v8, v22

    aget-byte v21, v3, v14

    aget-byte v22, v3, v14

    aput-byte v22, v6, v14

    if-gez v21, :cond_2

    move/from16 v0, v21

    add-int/lit16 v0, v0, 0x100

    move/from16 v21, v0

    :cond_2
    move/from16 v0, v21

    int-to-long v0, v0

    move-wide/from16 v22, v0

    add-long v8, v8, v22

    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    :cond_3
    const/4 v14, 0x6

    :goto_2
    const/16 v22, 0xc

    move/from16 v0, v22

    if-ge v14, v0, :cond_5

    const-wide/16 v22, 0x100

    mul-long v17, v17, v22

    aget-byte v21, v3, v14

    aget-byte v22, v3, v14

    aput-byte v22, v6, v14

    if-gez v21, :cond_4

    move/from16 v0, v21

    add-int/lit16 v0, v0, 0x100

    move/from16 v21, v0

    :cond_4
    move/from16 v0, v21

    int-to-long v0, v0

    move-wide/from16 v22, v0

    add-long v17, v17, v22

    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    :cond_5
    cmp-long v22, v8, v17

    if-lez v22, :cond_6

    invoke-virtual {v12}, Ljava/io/FileInputStream;->close()V

    sget-object v22, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_DATA_OVER:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    goto/16 :goto_0

    :cond_6
    invoke-virtual {v12}, Ljava/io/FileInputStream;->close()V

    const-wide/16 v22, 0x1

    add-long v8, v8, v22

    const/4 v14, 0x5

    :goto_3
    if-ltz v14, :cond_7

    const-wide/16 v22, 0x100

    rem-long v22, v8, v22

    move-wide/from16 v0, v22

    long-to-int v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-byte v0, v0

    move/from16 v22, v0

    aput-byte v22, v6, v14

    const-wide/16 v22, 0x100

    div-long v8, v8, v22

    add-int/lit8 v14, v14, -0x1

    goto :goto_3

    :cond_7
    new-instance v13, Ljava/io/FileOutputStream;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "mac.bin"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-direct {v13, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v6}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v22

    const-string v23, "sync"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    const-wide/16 v22, 0xbb8

    :try_start_1
    invoke-static/range {v22 .. v23}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    :goto_4
    const/4 v7, 0x0

    const/16 v19, 0x1

    :goto_5
    :try_start_2
    new-instance v12, Ljava/io/FileInputStream;

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v22

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    const-string v23, "mac.bin"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-direct {v12, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12}, Ljava/io/FileInputStream;->available()I

    move-result v15

    const/16 v22, 0xc

    move/from16 v0, v22

    if-ge v15, v0, :cond_9

    const/16 v19, 0x0

    :cond_8
    :goto_6
    invoke-virtual {v12}, Ljava/io/FileInputStream;->close()V

    if-nez v19, :cond_d

    add-int/lit8 v7, v7, 0x1

    const/16 v22, 0x5

    move/from16 v0, v22

    if-le v7, v0, :cond_b

    sget-object v22, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_READ_FILE_FAIL:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    goto/16 :goto_0

    :catch_0
    move-exception v10

    invoke-virtual {v10}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_4

    :catch_1
    move-exception v10

    invoke-virtual {v10}, Ljava/io/FileNotFoundException;->printStackTrace()V

    sget-object v22, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_NO_FILE:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    goto/16 :goto_0

    :cond_9
    :try_start_3
    new-array v4, v15, [B

    invoke-virtual {v12, v4}, Ljava/io/FileInputStream;->read([B)I

    const/4 v14, 0x0

    :goto_7
    const/16 v22, 0xc

    move/from16 v0, v22

    if-ge v14, v0, :cond_8

    aget-byte v22, v4, v14

    aget-byte v23, v6, v14
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    move/from16 v0, v22

    move/from16 v1, v23

    if-eq v0, v1, :cond_a

    const/16 v19, 0x0

    goto :goto_6

    :cond_a
    add-int/lit8 v14, v14, 0x1

    goto :goto_7

    :cond_b
    const-wide/16 v22, 0x7d0

    :try_start_4
    invoke-static/range {v22 .. v23}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    goto :goto_5

    :catch_2
    move-exception v10

    :try_start_5
    invoke-virtual {v10}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_5

    :catch_3
    move-exception v10

    invoke-virtual {v10}, Ljava/io/IOException;->printStackTrace()V

    sget-object v22, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_READ_FILE_FAIL:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    goto/16 :goto_0

    :cond_c
    sget-object v22, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;->UPGRADE_MODE_NET:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    if-ne v0, v1, :cond_d

    move-object/from16 v0, p1

    array-length v0, v0

    move/from16 v16, v0

    const/4 v14, 0x0

    :goto_8
    move/from16 v0, v16

    if-ge v14, v0, :cond_d

    aget-byte v22, p1, v14

    aput-byte v22, v3, v14

    const/16 v22, 0x6

    move/from16 v0, v22

    if-le v14, v0, :cond_e

    :cond_d
    const/16 v22, 0x2000

    :try_start_6
    move/from16 v0, v22

    new-array v5, v0, [S

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v22

    const/16 v23, 0x1d

    const/16 v24, 0x2000

    invoke-virtual/range {v22 .. v24}, Lcom/mstar/android/tvapi/common/TvManager;->readFromSpiFlashByBank(II)[S

    move-result-object v5

    const/4 v14, 0x0

    :goto_9
    const/16 v22, 0x6

    move/from16 v0, v22

    if-ge v14, v0, :cond_f

    add-int/lit8 v22, v14, 0x0

    aget-byte v23, v3, v14

    move/from16 v0, v23

    int-to-short v0, v0

    move/from16 v23, v0

    aput-short v23, v5, v22

    add-int/lit8 v14, v14, 0x1

    goto :goto_9

    :cond_e
    add-int/lit8 v14, v14, 0x1

    goto :goto_8

    :cond_f
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v22

    const/16 v23, 0x1d

    move-object/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v0, v1, v5}, Lcom/mstar/android/tvapi/common/TvManager;->writeToSpiFlashByBank(I[S)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v22

    const/16 v23, 0x1d

    const/16 v24, 0x6

    invoke-virtual/range {v22 .. v24}, Lcom/mstar/android/tvapi/common/TvManager;->readFromSpiFlashByBank(II)[S

    move-result-object v5

    const/4 v14, 0x0

    :goto_a
    const/16 v22, 0x6

    move/from16 v0, v22

    if-ge v14, v0, :cond_10

    add-int/lit8 v22, v14, 0x0

    aget-short v22, v5, v22

    move/from16 v0, v22

    int-to-byte v0, v0

    move/from16 v22, v0

    aput-byte v22, p1, v14
    :try_end_6
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_6 .. :try_end_6} :catch_5

    add-int/lit8 v14, v14, 0x1

    goto :goto_a

    :cond_10
    :try_start_7
    const-string v22, "%02X:%02X:%02X:%02X:%02X:%02X"

    const/16 v23, 0x6

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/16 v25, 0x0

    aget-byte v25, v3, v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v25

    aput-object v25, v23, v24

    const/16 v24, 0x1

    const/16 v25, 0x1

    aget-byte v25, v3, v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v25

    aput-object v25, v23, v24

    const/16 v24, 0x2

    const/16 v25, 0x2

    aget-byte v25, v3, v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v25

    aput-object v25, v23, v24

    const/16 v24, 0x3

    const/16 v25, 0x3

    aget-byte v25, v3, v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v25

    aput-object v25, v23, v24

    const/16 v24, 0x4

    const/16 v25, 0x4

    aget-byte v25, v3, v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v25

    aput-object v25, v23, v24

    const/16 v24, 0x5

    const/16 v25, 0x5

    aget-byte v25, v3, v25

    invoke-static/range {v25 .. v25}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v25

    aput-object v25, v23, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    sput-object v20, Lcom/konka/factory/UPDATEAdjustViewHolder;->macstr:Ljava/lang/String;

    const-string v22, "setEnvironment"

    new-instance v23, Ljava/lang/StringBuilder;

    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    const-string v24, "ethaddr "

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v22

    const-string v23, "ethaddr"

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/mstar/android/tvapi/common/TvManager;->setEnvironment(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_7
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_7 .. :try_end_7} :catch_4

    :goto_b
    sget-object v22, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_OK:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    goto/16 :goto_0

    :catch_4
    move-exception v10

    :try_start_8
    invoke-virtual {v10}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V
    :try_end_8
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_8 .. :try_end_8} :catch_5

    goto :goto_b

    :catch_5
    move-exception v10

    invoke-virtual {v10}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    sget-object v22, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_RW_SPI_FAIL:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    goto/16 :goto_0
.end method

.method public static Write_SN(Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;[B)Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;
    .locals 14
    .param p0    # Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;
    .param p1    # [B

    const/16 v13, 0x32

    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v11, "Write_SN!"

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    new-array v0, v13, [B

    const/4 v6, 0x0

    sget-object v10, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;->UPGRADE_MODE_USB:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;

    if-ne p0, v10, :cond_3

    const-string v10, "SerialNum.enc"

    invoke-static {v10}, Lcom/konka/factory/UPDATEAdjustViewHolder;->DetectFileFromUSB(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_0

    sget-object v10, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_NO_FILE:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    :goto_0
    return-object v10

    :cond_0
    new-instance v8, Lcom/konka/factory/SnDecoder;

    invoke-direct {v8}, Lcom/konka/factory/SnDecoder;-><init>()V

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "SerialNum.enc"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Lcom/konka/factory/SnDecoder;->DecodeSN(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    array-length v6, v2

    const/4 v5, 0x0

    :goto_1
    if-ge v5, v6, :cond_1

    aget-byte v10, v2, v5

    aput-byte v10, v0, v5

    if-le v5, v13, :cond_2

    :cond_1
    const/16 v10, 0x2000

    :try_start_0
    new-array v1, v10, [S

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v10

    const/16 v11, 0x1d

    const/16 v12, 0x2000

    invoke-virtual {v10, v11, v12}, Lcom/mstar/android/tvapi/common/TvManager;->readFromSpiFlashByBank(II)[S

    move-result-object v1

    const/4 v5, 0x0

    :goto_2
    if-ge v5, v13, :cond_4

    add-int/lit16 v10, v5, 0x80

    aget-byte v11, v0, v5

    int-to-short v11, v11

    aput-short v11, v1, v10
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_3
    sget-object v10, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;->UPGRADE_MODE_NET:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;

    if-ne p0, v10, :cond_1

    array-length v6, p1

    const/4 v5, 0x0

    :goto_3
    if-ge v5, v6, :cond_1

    aget-byte v10, p1, v5

    aput-byte v10, v0, v5

    if-gt v5, v13, :cond_1

    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :cond_4
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v10

    const/16 v11, 0x1d

    invoke-virtual {v10, v11, v1}, Lcom/mstar/android/tvapi/common/TvManager;->writeToSpiFlashByBank(I[S)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v10

    const/16 v11, 0x1d

    const/16 v12, 0xb2

    invoke-virtual {v10, v11, v12}, Lcom/mstar/android/tvapi/common/TvManager;->readFromSpiFlashByBank(II)[S

    move-result-object v1

    const/4 v5, 0x0

    :goto_4
    if-ge v5, v6, :cond_5

    add-int/lit16 v10, v5, 0x80

    aget-short v10, v1, v10

    int-to-byte v10, v10

    aput-byte v10, p1, v5
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    sget-object v10, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_RW_SPI_FAIL:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    goto/16 :goto_0

    :cond_5
    invoke-static {p1}, Lorg/apache/http/util/EncodingUtils;->getAsciiString([B)Ljava/lang/String;

    move-result-object v9

    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v10, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v10, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_OK:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    goto/16 :goto_0
.end method

.method private static toChars([B)[C
    .locals 7
    .param p0    # [B

    const/16 v6, 0xa

    array-length v0, p0

    mul-int/lit8 v4, v0, 0x2

    new-array v3, v4, [C

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_2

    aget-byte v4, p0, v2

    shr-int/lit8 v4, v4, 0x4

    and-int/lit8 v1, v4, 0xf

    mul-int/lit8 v5, v2, 0x2

    if-lt v1, v6, :cond_0

    add-int/lit8 v4, v1, 0x61

    add-int/lit8 v4, v4, -0xa

    :goto_1
    int-to-char v4, v4

    aput-char v4, v3, v5

    aget-byte v4, p0, v2

    and-int/lit8 v1, v4, 0xf

    mul-int/lit8 v4, v2, 0x2

    add-int/lit8 v5, v4, 0x1

    if-lt v1, v6, :cond_1

    add-int/lit8 v4, v1, 0x61

    add-int/lit8 v4, v4, -0xa

    :goto_2
    int-to-char v4, v4

    aput-char v4, v3, v5

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    add-int/lit8 v4, v1, 0x30

    goto :goto_1

    :cond_1
    add-int/lit8 v4, v1, 0x30

    goto :goto_2

    :cond_2
    return-object v3
.end method


# virtual methods
.method public Chip_Update()V
    .locals 2

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "chip"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method

.method public Display(Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;I)V
    .locals 5
    .param p1    # Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;
    .param p2    # I

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/konka/factory/UPDATEAdjustViewHolder;->updateActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v2, "Tips"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const-string v2, "Yes"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/4 v0, 0x0

    sget-object v2, Lcom/konka/factory/UPDATEAdjustViewHolder$1;->$SwitchMap$com$konka$factory$UPDATEAdjustViewHolder$UpgradeInfo:[I

    invoke-virtual {p1}, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void

    :pswitch_0
    packed-switch p2, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/konka/factory/UPDATEAdjustViewHolder;->updateActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v2}, Lcom/konka/factory/MainmenuActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0800ca    # com.konka.factory.R.string.str_factory_upgradeok

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/konka/factory/UPDATEAdjustViewHolder;->updateActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v3}, Lcom/konka/factory/MainmenuActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0800c7    # com.konka.factory.R.string.str_factory_macaddr

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/konka/factory/UPDATEAdjustViewHolder;->macstr:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/konka/factory/UPDATEAdjustViewHolder;->updateActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v3}, Lcom/konka/factory/MainmenuActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0800c8    # com.konka.factory.R.string.str_factory_index

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/konka/factory/UPDATEAdjustViewHolder;->index:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/factory/UPDATEAdjustViewHolder;->updateActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v3}, Lcom/konka/factory/MainmenuActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0800c9    # com.konka.factory.R.string.str_factory_offset

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/konka/factory/UPDATEAdjustViewHolder;->offsetStr:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    iget-object v2, p0, Lcom/konka/factory/UPDATEAdjustViewHolder;->updateActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v2}, Lcom/konka/factory/MainmenuActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0800cb    # com.konka.factory.R.string.str_factory_upgradenofile

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_5
    iget-object v2, p0, Lcom/konka/factory/UPDATEAdjustViewHolder;->updateActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v2}, Lcom/konka/factory/MainmenuActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0800cc    # com.konka.factory.R.string.str_factory_upgradereadfilefail

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_6
    iget-object v2, p0, Lcom/konka/factory/UPDATEAdjustViewHolder;->updateActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v2}, Lcom/konka/factory/MainmenuActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0800cd    # com.konka.factory.R.string.str_factory_upgradedataover

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public Dongle_Update()V
    .locals 5

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "Dongle Update"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    new-instance v0, Landroid/content/ComponentName;

    const-string v3, "com.konka.dongleupgrade"

    const-string v4, "com.konka.dongleupgrade.DongleUpgradeActivity"

    invoke-direct {v0, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    :try_start_0
    iget-object v3, p0, Lcom/konka/factory/UPDATEAdjustViewHolder;->updateActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v3, v2}, Lcom/konka/factory/MainmenuActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public SoftWare_Update()V
    .locals 4

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "SoftWare Update Begin!"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :try_start_0
    const-string v1, "setEnvironment"

    const-string v2, "upgrade_mode usb"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    const-string v2, "upgrade_mode"

    const-string v3, "usb"

    invoke-virtual {v1, v2, v3}, Lcom/mstar/android/tvapi/common/TvManager;->setEnvironment(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v1, "SoftWare_Update"

    const-string v2, "rebootsystem begin"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/konka/factory/TvRootApp;->getCommonSkin()Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v1

    const-string v2, "reboot"

    invoke-virtual {v1, v2}, Lcom/mstar/android/tv/TvCommonManager;->rebootSystem(Ljava/lang/String;)V

    const-string v1, "SoftWare_Update"

    const-string v2, "rebootsystem end"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public USB_Check_CIPlusKey()V
    .locals 9

    const/16 v8, 0xff

    const/16 v3, 0x2000

    :try_start_0
    new-array v0, v3, [S

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    const/16 v4, 0x1d

    const/16 v5, 0x1972

    invoke-virtual {v3, v4, v5}, Lcom/mstar/android/tvapi/common/TvManager;->readFromSpiFlashByBank(II)[S

    move-result-object v0

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "lidongdong ci %02X:%02X:%02X:%02X:%02X:%02X\n"

    const/4 v5, 0x6

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/16 v7, 0x400

    aget-short v7, v0, v7

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const/16 v7, 0x401

    aget-short v7, v0, v7

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const/16 v7, 0x402

    aget-short v7, v0, v7

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x3

    const/16 v7, 0x403

    aget-short v7, v0, v7

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x4

    const/16 v7, 0x1970

    aget-short v7, v0, v7

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x5

    const/16 v7, 0x1971

    aget-short v7, v0, v7

    invoke-static {v7}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/io/PrintStream;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;

    const/16 v3, 0x400

    aget-short v3, v0, v3

    if-ne v3, v8, :cond_0

    const/16 v3, 0x401

    aget-short v3, v0, v3

    if-ne v3, v8, :cond_0

    const/16 v3, 0x402

    aget-short v3, v0, v3

    if-ne v3, v8, :cond_0

    const/16 v3, 0x1970

    aget-short v3, v0, v3

    if-ne v3, v8, :cond_0

    const/16 v3, 0x1971

    aget-short v3, v0, v3

    if-ne v3, v8, :cond_0

    iget-object v3, p0, Lcom/konka/factory/UPDATEAdjustViewHolder;->updateActivity:Lcom/konka/factory/MainmenuActivity;

    const-string v4, "NG"

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    :goto_0
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    :goto_1
    return-void

    :cond_0
    iget-object v3, p0, Lcom/konka/factory/UPDATEAdjustViewHolder;->updateActivity:Lcom/konka/factory/MainmenuActivity;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v5, 0x400

    aget-short v5, v0, v5

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x401

    aget-short v5, v0, v5

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x402

    aget-short v5, v0, v5

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/16 v5, 0x403

    aget-short v5, v0, v5

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public USB_Mboot_Update()V
    .locals 4

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "Mboot Update Begin!"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :try_start_0
    const-string v1, "setEnvironment"

    const-string v2, "upgrade_mode mboot"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    const-string v2, "upgrade_mode"

    const-string v3, "mboot"

    invoke-virtual {v1, v2, v3}, Lcom/mstar/android/tvapi/common/TvManager;->setEnvironment(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v1, "Mboot_Update"

    const-string v2, "rebootsystem begin"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/konka/factory/TvRootApp;->getCommonSkin()Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v1

    const-string v2, "reboot"

    invoke-virtual {v1, v2}, Lcom/mstar/android/tv/TvCommonManager;->rebootSystem(Ljava/lang/String;)V

    const-string v1, "Mboot_Update"

    const-string v2, "rebootsystem end"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public USB_Read_HDCPKey()V
    .locals 8

    const/16 v7, 0x130

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v5, "read HDCPKey"

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/16 v4, 0x2000

    new-array v0, v4, [S

    new-array v1, v7, [B

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    const/16 v5, 0x1d

    const/16 v6, 0x230

    invoke-virtual {v4, v5, v6}, Lcom/mstar/android/tvapi/common/TvManager;->readFromSpiFlashByBank(II)[S
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    const/4 v3, 0x0

    :goto_1
    if-ge v3, v7, :cond_0

    add-int/lit16 v4, v3, 0x100

    aget-short v4, v0, v4

    int-to-byte v4, v4

    aput-byte v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_0
    iget-object v4, p0, Lcom/konka/factory/UPDATEAdjustViewHolder;->gridview_hdcpkey:Landroid/widget/GridView;

    invoke-virtual {v4}, Landroid/widget/GridView;->isShown()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/konka/factory/UPDATEAdjustViewHolder;->gridview_hdcpkey:Landroid/widget/GridView;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Landroid/widget/GridView;->setVisibility(I)V

    :goto_2
    iget-object v4, p0, Lcom/konka/factory/UPDATEAdjustViewHolder;->gridview_hdcpkey:Landroid/widget/GridView;

    new-instance v5, Lcom/konka/factory/GridViewAdapter;

    iget-object v6, p0, Lcom/konka/factory/UPDATEAdjustViewHolder;->updateActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-static {v1}, Lcom/konka/factory/UPDATEAdjustViewHolder;->toChars([B)[C

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/konka/factory/GridViewAdapter;-><init>(Landroid/content/Context;[C)V

    invoke-virtual {v4, v5}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void

    :cond_1
    iget-object v4, p0, Lcom/konka/factory/UPDATEAdjustViewHolder;->gridview_hdcpkey:Landroid/widget/GridView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/GridView;->setVisibility(I)V

    goto :goto_2
.end method

.method public USB_Write_CIPlusKey()V
    .locals 4

    const/4 v2, 0x0

    const/16 v3, 0x1572

    new-array v1, v3, [B

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sget-object v3, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;->UPGRADE_MODE_USB:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;

    invoke-static {v3, v0, v1}, Lcom/konka/factory/UPDATEAdjustViewHolder;->Write_CIPlusKey(Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;Ljava/util/HashMap;[B)Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/konka/factory/UPDATEAdjustViewHolder;->Display(Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;I)V

    return-void
.end method

.method public USB_Write_HDCPKey()V
    .locals 14

    const/4 v13, 0x1

    const/4 v12, 0x0

    const/4 v3, 0x0

    const/16 v8, 0x130

    new-array v0, v8, [B

    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    sget-object v8, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;->UPGRADE_MODE_USB:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;

    invoke-static {v8, v5, v0}, Lcom/konka/factory/UPDATEAdjustViewHolder;->Write_HDCPKey(Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;Ljava/util/HashMap;[B)Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    move-result-object v3

    sget-object v8, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_OK:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    if-ne v3, v8, :cond_2

    const-string v8, "index"

    invoke-virtual {v5, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Double;

    invoke-virtual {v8}, Ljava/lang/Double;->intValue()I

    move-result v2

    const-string v8, "size"

    invoke-virtual {v5, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Double;

    invoke-virtual {v8}, Ljava/lang/Double;->intValue()I

    move-result v7

    const-string v8, "offset"

    invoke-virtual {v5, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Double;

    invoke-virtual {v8}, Ljava/lang/Double;->intValue()I

    move-result v6

    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v9, "index: %d\n"

    new-array v10, v13, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v12

    invoke-virtual {v8, v9, v10}, Ljava/io/PrintStream;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;

    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v9, "offset: 0x%08x\n"

    new-array v10, v13, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v12

    invoke-virtual {v8, v9, v10}, Ljava/io/PrintStream;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;

    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v9, "hdcp key:"

    invoke-virtual {v8, v9}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v7, :cond_1

    const/4 v4, 0x0

    :goto_1
    const/16 v8, 0x10

    if-ge v4, v8, :cond_0

    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v9, "%02X "

    new-array v10, v13, [Ljava/lang/Object;

    add-int v11, v1, v4

    aget-byte v11, v0, v11

    invoke-static {v11}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v11

    aput-object v11, v10, v12

    invoke-virtual {v8, v9, v10}, Ljava/io/PrintStream;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_0
    sget-object v8, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v8}, Ljava/io/PrintStream;->println()V

    add-int/lit8 v1, v1, 0x10

    goto :goto_0

    :cond_1
    sput v2, Lcom/konka/factory/UPDATEAdjustViewHolder;->index:I

    const-string v8, "0x%08x"

    new-array v9, v13, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v12

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    sput-object v8, Lcom/konka/factory/UPDATEAdjustViewHolder;->offsetStr:Ljava/lang/String;

    :cond_2
    const/4 v8, 0x2

    invoke-virtual {p0, v3, v8}, Lcom/konka/factory/UPDATEAdjustViewHolder;->Display(Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;I)V

    return-void
.end method

.method public USB_Write_MACaddr()V
    .locals 11

    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x6

    new-array v0, v2, [B

    sget-object v2, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;->UPGRADE_MODE_USB:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;

    invoke-static {v2, v0}, Lcom/konka/factory/UPDATEAdjustViewHolder;->Write_MACaddr(Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;[B)Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    move-result-object v1

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "Mac Addr %02X:%02X:%02X:%02X:%02X:%02X\n"

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    aget-byte v5, v0, v6

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    aput-object v5, v4, v6

    aget-byte v5, v0, v7

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    aput-object v5, v4, v7

    aget-byte v5, v0, v8

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    aput-object v5, v4, v8

    aget-byte v5, v0, v9

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    aput-object v5, v4, v9

    aget-byte v5, v0, v10

    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v5

    aput-object v5, v4, v10

    const/4 v5, 0x5

    const/4 v6, 0x5

    aget-byte v6, v0, v6

    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/io/PrintStream;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;

    invoke-virtual {p0, v1, v7}, Lcom/konka/factory/UPDATEAdjustViewHolder;->Display(Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;I)V

    return-void
.end method

.method public USB_Write_SN()V
    .locals 4

    const/4 v1, 0x0

    const/16 v3, 0x32

    new-array v0, v3, [B

    sget-object v3, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;->UPGRADE_MODE_USB:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;

    invoke-static {v3, v0}, Lcom/konka/factory/UPDATEAdjustViewHolder;->Write_SN(Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeMode;[B)Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    move-result-object v1

    invoke-static {v0}, Lorg/apache/http/util/EncodingUtils;->getAsciiString([B)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v3, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v3}, Lcom/konka/factory/UPDATEAdjustViewHolder;->Display(Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;I)V

    return-void
.end method

.method findview()V
    .locals 2

    iget-object v0, p0, Lcom/konka/factory/UPDATEAdjustViewHolder;->updateActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0231    # com.konka.factory.R.id.textview_factory_update_usbwritemacaddr

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/UPDATEAdjustViewHolder;->textview_usbwritemacaddr:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/UPDATEAdjustViewHolder;->updateActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0237    # com.konka.factory.R.id.textview_factory_update_usbwritehdcpkey

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/UPDATEAdjustViewHolder;->textview_usbwritehdcpkey:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/UPDATEAdjustViewHolder;->updateActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0239    # com.konka.factory.R.id.textview_factory_update_usbreadhdcpkey

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/UPDATEAdjustViewHolder;->textview_usbreadhdcpkey:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/UPDATEAdjustViewHolder;->updateActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a023b    # com.konka.factory.R.id.gridview_factory_update_hdcpkey

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/konka/factory/UPDATEAdjustViewHolder;->gridview_hdcpkey:Landroid/widget/GridView;

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/konka/factory/UPDATEAdjustViewHolder;->updateActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v2}, Lcom/konka/factory/MainmenuActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v1

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/konka/factory/UPDATEAdjustViewHolder;->SoftWare_Update()V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/konka/factory/UPDATEAdjustViewHolder;->USB_Mboot_Update()V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0}, Lcom/konka/factory/UPDATEAdjustViewHolder;->Chip_Update()V

    goto :goto_0

    :pswitch_4
    invoke-virtual {p0}, Lcom/konka/factory/UPDATEAdjustViewHolder;->Dongle_Update()V

    goto :goto_0

    :pswitch_5
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "sn"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/factory/UPDATEAdjustViewHolder;->USB_Write_SN()V

    goto :goto_0

    :pswitch_6
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "mac addr"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/factory/UPDATEAdjustViewHolder;->USB_Write_MACaddr()V

    goto :goto_0

    :pswitch_7
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "ci+ key update"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/factory/UPDATEAdjustViewHolder;->USB_Write_CIPlusKey()V

    goto :goto_0

    :pswitch_8
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "ci+ key check"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/factory/UPDATEAdjustViewHolder;->USB_Check_CIPlusKey()V

    goto :goto_0

    :pswitch_9
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "write hdcpkey"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/factory/UPDATEAdjustViewHolder;->USB_Write_HDCPKey()V

    goto :goto_0

    :pswitch_a
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "read hdcpkey"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/factory/UPDATEAdjustViewHolder;->USB_Read_HDCPKey()V

    goto :goto_0

    :sswitch_1
    iget-object v2, p0, Lcom/konka/factory/UPDATEAdjustViewHolder;->updateActivity:Lcom/konka/factory/MainmenuActivity;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/konka/factory/MainmenuActivity;->returnRoot(I)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x42 -> :sswitch_0
        0x52 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x7f0a0226
        :pswitch_1    # com.konka.factory.R.id.linearlayout_factory_update_software
        :pswitch_0    # com.konka.factory.R.id.textview_factory_update_software
        :pswitch_2    # com.konka.factory.R.id.linearlayout_factory_update_mboot
        :pswitch_0    # com.konka.factory.R.id.textview_factory_update_mboot
        :pswitch_3    # com.konka.factory.R.id.linearlayout_factory_update_chip
        :pswitch_0    # com.konka.factory.R.id.textview_factory_update_chip
        :pswitch_4    # com.konka.factory.R.id.linearlayout_factory_update_dongle
        :pswitch_0    # com.konka.factory.R.id.textview_factory_update_dongle
        :pswitch_5    # com.konka.factory.R.id.linearlayout_factory_update_usbwritesn
        :pswitch_0    # com.konka.factory.R.id.textview_factory_update_usbwritesn
        :pswitch_6    # com.konka.factory.R.id.linearlayout_factory_update_usbwritemacaddr
        :pswitch_0    # com.konka.factory.R.id.textview_factory_update_usbwritemacaddr
        :pswitch_7    # com.konka.factory.R.id.linearlayout_factory_update_usbwritecipluskey
        :pswitch_0    # com.konka.factory.R.id.textview_factory_update_usbwritecipluskey
        :pswitch_8    # com.konka.factory.R.id.linearlayout_factory_check_usbwritecipluskey
        :pswitch_0    # com.konka.factory.R.id.textview_factory_check_usbwritecipluskey
        :pswitch_9    # com.konka.factory.R.id.linearlayout_factory_update_usbwritehdcpkey
        :pswitch_0    # com.konka.factory.R.id.textview_factory_update_usbwritehdcpkey
        :pswitch_a    # com.konka.factory.R.id.linearlayout_factory_update_usbreadhdcpkey
    .end packed-switch
.end method
