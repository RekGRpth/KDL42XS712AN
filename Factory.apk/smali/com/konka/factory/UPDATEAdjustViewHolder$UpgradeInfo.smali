.class public final enum Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;
.super Ljava/lang/Enum;
.source "UPDATEAdjustViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/factory/UPDATEAdjustViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UpgradeInfo"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

.field public static final enum UPGRADE_DATA_OVER:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

.field public static final enum UPGRADE_NO_FILE:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

.field public static final enum UPGRADE_OK:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

.field public static final enum UPGRADE_READ_FILE_FAIL:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

.field public static final enum UPGRADE_RW_SPI_FAIL:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    const-string v1, "UPGRADE_OK"

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_OK:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    new-instance v0, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    const-string v1, "UPGRADE_DATA_OVER"

    invoke-direct {v0, v1, v3}, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_DATA_OVER:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    new-instance v0, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    const-string v1, "UPGRADE_NO_FILE"

    invoke-direct {v0, v1, v4}, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_NO_FILE:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    new-instance v0, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    const-string v1, "UPGRADE_READ_FILE_FAIL"

    invoke-direct {v0, v1, v5}, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_READ_FILE_FAIL:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    new-instance v0, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    const-string v1, "UPGRADE_RW_SPI_FAIL"

    invoke-direct {v0, v1, v6}, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_RW_SPI_FAIL:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    sget-object v1, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_OK:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    aput-object v1, v0, v2

    sget-object v1, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_DATA_OVER:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_NO_FILE:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_READ_FILE_FAIL:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    aput-object v1, v0, v5

    sget-object v1, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->UPGRADE_RW_SPI_FAIL:Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    aput-object v1, v0, v6

    sput-object v0, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->$VALUES:[Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;
    .locals 1

    const-class v0, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    return-object v0
.end method

.method public static values()[Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;
    .locals 1

    sget-object v0, Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->$VALUES:[Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    invoke-virtual {v0}, [Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/konka/factory/UPDATEAdjustViewHolder$UpgradeInfo;

    return-object v0
.end method
