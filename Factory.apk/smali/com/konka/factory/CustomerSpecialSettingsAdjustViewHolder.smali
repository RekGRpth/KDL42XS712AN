.class public Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;
.super Ljava/lang/Object;
.source "CustomerSpecialSettingsAdjustViewHolder.java"


# static fields
.field public static PersionCustomer:Ljava/lang/String;

.field public static PersionCustomer1:Ljava/lang/String;

.field public static usbState:Lcom/konka/factory/Storage;


# instance fields
.field private AdVideo_update_state:Z

.field private AudioDelayTime:I

.field private CountrySelectTable:[I

.field private CountrySelectTable_snowa:[I

.field private CountrySelectTable_xvision:[I

.field private DefOSDLanguageTmp:Ljava/lang/String;

.field private OptionCountrys:[Ljava/lang/String;

.field private OptionCountrysIndex:I

.field private OptionCustomerIndex:I

.field private OptionCustomers:[Ljava/lang/String;

.field private OptionCustomersToSave:[Ljava/lang/String;

.field private OsdSettingLanguage:Landroid/widget/LinearLayout;

.field private PowerMOdeEnable:[Ljava/lang/String;

.field private PowerMOdeEnableindex:I

.field private PowerOffLogoDspTime:I

.field private Sticker_update_state:Z

.field private TunrOffLogoEnable:[Ljava/lang/String;

.field private TunrOffLogoEnableindex:I

.field private context:Landroid/content/Context;

.field public curCustomer:Ljava/lang/String;

.field private customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

.field private defaultlanguage:[Ljava/lang/String;

.field private defaultlanguageindex:I

.field private factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

.field private intent:Landroid/content/Intent;

.field private language_values:[Ljava/lang/CharSequence;

.field protected linear_factory_customerspecialsettings_OSD_Language:Landroid/widget/LinearLayout;

.field protected linear_factory_customerspecialsettings_logo_update:Landroid/widget/LinearLayout;

.field protected linearlayout_factory_customerspecialsettings_AdVideo_update:Landroid/widget/LinearLayout;

.field protected linearlayout_factory_customerspecialsettings_Audio_delay:Landroid/widget/LinearLayout;

.field protected linearlayout_factory_customerspecialsettings_Sticker_update:Landroid/widget/LinearLayout;

.field protected linearlayout_factory_customerspecialsettings_hotel_mode:Landroid/widget/LinearLayout;

.field protected linearlayout_factory_customerspecialsettings_logo_time:Landroid/widget/LinearLayout;

.field protected linearlayout_factory_customerspecialsettings_power_mode:Landroid/widget/LinearLayout;

.field protected linearlayout_factory_customerspecialsettings_tunning_country:Landroid/widget/LinearLayout;

.field protected linearlayout_factory_customerspecialsettings_turnoff_logo:Landroid/widget/LinearLayout;

.field private logo_update_state:Z

.field private m_iCurrLangIndex:I

.field private m_strLocales:[Ljava/lang/String;

.field private mpDialog:Landroid/app/ProgressDialog;

.field private teletextenable:[Ljava/lang/String;

.field private teletextenableindex:I

.field protected text_factory_customerspecialsettings_OSD_language_option:Landroid/widget/TextView;

.field protected text_factory_customerspecialsettings_factory_update_state:Landroid/widget/TextView;

.field protected text_factory_customerspecialsettings_logo_updagte_state:Landroid/widget/TextView;

.field protected text_factory_customerspecialsettings_teletextenable_val:Landroid/widget/TextView;

.field protected text_factory_customerspecialsettings_tunning_country_option:Landroid/widget/TextView;

.field protected text_factory_customerspecialsettings_tunning_customer_id:Landroid/widget/TextView;

.field protected textview_factory_customerspecialsettings_AdVideo_update_state:Landroid/widget/TextView;

.field protected textview_factory_customerspecialsettings_Audio_delay_option:Landroid/widget/TextView;

.field protected textview_factory_customerspecialsettings_Sticker_update_state:Landroid/widget/TextView;

.field protected textview_factory_customerspecialsettings_logo_time_option:Landroid/widget/TextView;

.field protected textview_factory_customerspecialsettings_power_mode_option:Landroid/widget/TextView;

.field protected textview_factory_customerspecialsettings_turnoff_logo_option:Landroid/widget/TextView;

.field private textview_usbreadWB:Landroid/widget/TextView;

.field private textview_usbreadWBstate:Landroid/widget/TextView;

.field private textview_usbwriteWB:Landroid/widget/TextView;

.field private textview_usbwriteWBstate:Landroid/widget/TextView;

.field private updateAdjustViewHolder:Lcom/konka/factory/UPDATEAdjustViewHolder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "snowa"

    sput-object v0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PersionCustomer:Ljava/lang/String;

    const-string v0, "x.vision"

    sput-object v0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PersionCustomer1:Ljava/lang/String;

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->usbState:Lcom/konka/factory/Storage;

    return-void
.end method

.method public constructor <init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V
    .locals 8
    .param p1    # Lcom/konka/factory/MainmenuActivity;
    .param p2    # Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->OsdSettingLanguage:Landroid/widget/LinearLayout;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->language_values:[Ljava/lang/CharSequence;

    const-string v0, "en_US"

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->DefOSDLanguageTmp:Ljava/lang/String;

    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "en_US"

    aput-object v1, v0, v3

    const-string v1, "ru_RU"

    aput-object v1, v0, v4

    const-string v1, "fr_FR"

    aput-object v1, v0, v5

    const-string v1, "ar_EG"

    aput-object v1, v0, v6

    const-string v1, "iw_IL"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "in_ID"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "fa_IR"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "kd_KD"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "tr_TR"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "th_TH"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "zh_CN"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->m_strLocales:[Ljava/lang/String;

    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "English"

    aput-object v1, v0, v3

    const-string v1, "Russia"

    aput-object v1, v0, v4

    const-string v1, "French"

    aput-object v1, v0, v5

    const-string v1, "Arabic"

    aput-object v1, v0, v6

    const-string v1, "Hebrew"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "Indonesia"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Persian"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "Kurdish"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "Turkish"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "Thai"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "Chinese"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguage:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "Off"

    aput-object v1, v0, v3

    const-string v1, "On"

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->teletextenable:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "secondary"

    aput-object v1, v0, v3

    const-string v1, "memory"

    aput-object v1, v0, v4

    const-string v1, "direct"

    aput-object v1, v0, v5

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerMOdeEnable:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "Off"

    aput-object v1, v0, v3

    const-string v1, "On"

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->TunrOffLogoEnable:[Ljava/lang/String;

    iput v3, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->m_iCurrLangIndex:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->OptionCustomerIndex:I

    iput v3, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->teletextenableindex:I

    iput v3, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerMOdeEnableindex:I

    iput v3, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->TunrOffLogoEnableindex:I

    iput v3, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerOffLogoDspTime:I

    iput v3, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->AudioDelayTime:I

    iput-boolean v3, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->logo_update_state:Z

    iput-boolean v3, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->AdVideo_update_state:Z

    iput-boolean v3, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->Sticker_update_state:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->mpDialog:Landroid/app/ProgressDialog;

    const/16 v0, 0x3d

    new-array v0, v0, [I

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRALIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v3

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v4

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BELGIUM:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v5

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BULGARIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v6

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CROATIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CZECH:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_DENMARK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FINLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FRANCE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GERMANY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GREECE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_HUNGARY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ITALY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LUXEMBOURG:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NETHERLANDS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NORWAY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_POLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PORTUGAL:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUMANIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUSSIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SERBIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SLOVENIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SPAIN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SWEDEN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SWITZERLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_UK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NEWZEALAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ARAB:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ESTONIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_HEBREW:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LATVIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SLOVAKIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TURKEY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRELAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FIJI:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_UZBEK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TAJIKISTAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ETHIOPIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AZERBAIJAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SOUTHAFRICA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ALGERIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_EGYPT:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SAUDI_ARABIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRAQ:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NAMIBIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_JORDAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_KUWAIT:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_INDONESIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ISRAEL:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_QATAR:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NIGERIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ZEMBABWE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LITHUANIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_MOROCCO:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TUNIS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_INDIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_YEMEN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LIBYA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PAKISTAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_OTHERS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->CountrySelectTable:[I

    const/16 v0, 0x38

    new-array v0, v0, [I

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRALIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v3

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v4

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BELGIUM:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v5

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BULGARIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v6

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CROATIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CZECH:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_DENMARK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FINLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FRANCE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GERMANY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GREECE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_HUNGARY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ITALY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LUXEMBOURG:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NETHERLANDS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NORWAY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_POLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PORTUGAL:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUMANIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUSSIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SERBIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SLOVENIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SPAIN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SWEDEN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SWITZERLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_UK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NEWZEALAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ARAB:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ESTONIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LATVIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SLOVAKIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TURKEY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRELAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FIJI:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_UZBEK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TAJIKISTAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ETHIOPIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AZERBAIJAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SOUTHAFRICA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ALGERIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_EGYPT:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SAUDI_ARABIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRAQ:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NAMIBIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_JORDAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_KUWAIT:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_INDONESIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_QATAR:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NIGERIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ZEMBABWE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LITHUANIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_MOROCCO:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TUNIS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_INDIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_OTHERS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->CountrySelectTable_snowa:[I

    const/16 v0, 0x3c

    new-array v0, v0, [I

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRALIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v3

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AUSTRIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v4

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BELGIUM:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v5

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_BULGARIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v6

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CROATIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v1}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v1

    aput v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_CZECH:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_DENMARK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FINLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FRANCE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GERMANY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_GREECE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_HUNGARY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ITALY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LUXEMBOURG:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NETHERLANDS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NORWAY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_POLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PORTUGAL:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUMANIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_RUSSIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SERBIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SLOVENIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SPAIN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SWEDEN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SWITZERLAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_UK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NEWZEALAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ARAB:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ESTONIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_HEBREW:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LATVIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SLOVAKIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TURKEY:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRELAND:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_FIJI:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_UZBEK:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TAJIKISTAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ETHIOPIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_AZERBAIJAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SOUTHAFRICA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ALGERIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_EGYPT:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_SAUDI_ARABIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_IRAQ:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NAMIBIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_JORDAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_KUWAIT:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_INDONESIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_QATAR:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_NIGERIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_ZEMBABWE:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LITHUANIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_MOROCCO:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_TUNIS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_INDIA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_YEMEN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_LIBYA:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_PAKISTAN:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->E_OTHERS:Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;

    invoke-virtual {v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$MEMBER_COUNTRY;->ordinal()I

    move-result v2

    aput v2, v0, v1

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->CountrySelectTable_xvision:[I

    iput-object p1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    iput-object p2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    new-instance v0, Lcom/konka/factory/Storage;

    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-direct {v0, v1}, Lcom/konka/factory/Storage;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->usbState:Lcom/konka/factory/Storage;

    return-void
.end method

.method private IO(I)Z
    .locals 9

    const/4 v4, 0x3

    const/16 v7, 0x2f

    const/4 v1, 0x1

    const/4 v8, 0x2

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    new-array v3, v4, [Ljava/io/File;

    new-array v4, v4, [Ljava/io/File;

    new-instance v2, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->usbState:Lcom/konka/factory/Storage;

    invoke-virtual {v6}, Lcom/konka/factory/Storage;->getUsbPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "factory.bin"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    aput-object v2, v3, v0

    new-instance v2, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->usbState:Lcom/konka/factory/Storage;

    invoke-virtual {v6}, Lcom/konka/factory/Storage;->getUsbPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "factory.db"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    aput-object v2, v3, v1

    new-instance v2, Ljava/io/File;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->usbState:Lcom/konka/factory/Storage;

    invoke-virtual {v6}, Lcom/konka/factory/Storage;->getUsbPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "atv_cmdb_cable.bin"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    aput-object v2, v3, v8

    new-instance v2, Ljava/io/File;

    const-string v5, "tvdatabase/Database/factory.bin"

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    aput-object v2, v4, v0

    new-instance v2, Ljava/io/File;

    const-string v5, "customercfg/panel/factory.db"

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    aput-object v2, v4, v1

    new-instance v2, Ljava/io/File;

    const-string v5, "tvdatabase/Database/atv_cmdb_cable.bin"

    invoke-direct {v2, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    aput-object v2, v4, v8

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "XXX==========>>>["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v4, v0

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, v4, v0

    invoke-virtual {v6}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    if-ne p1, v1, :cond_1

    sget-object v2, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->usbState:Lcom/konka/factory/Storage;

    invoke-virtual {v2}, Lcom/konka/factory/Storage;->getUsbPath()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    :goto_1
    if-ge v0, v8, :cond_4

    aget-object v2, v4, v0

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    aget-object v5, v3, v0

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Lcom/konka/factory/Command;->cp(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/konka/factory/Command;->sync()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    if-nez p1, :cond_4

    move v2, v0

    :goto_2
    if-ge v2, v8, :cond_3

    aget-object v5, v3, v2

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_2

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "wen jian bu cun zai !!!!!"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    move v2, v0

    :goto_3
    if-ge v2, v8, :cond_4

    :try_start_0
    new-instance v5, Ljava/io/FileOutputStream;

    aget-object v6, v4, v2

    invoke-direct {v5, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    new-instance v6, Ljava/io/FileInputStream;

    aget-object v7, v3, v2

    invoke-direct {v6, v7}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v6}, Ljava/io/FileInputStream;->available()I

    move-result v7

    new-array v7, v7, [B

    invoke-virtual {v6, v7}, Ljava/io/FileInputStream;->read([B)I

    invoke-virtual {v5, v7}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;Ljava/io/File;Ljava/io/File;)Z
    .locals 1
    .param p0    # Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->copyFile(Ljava/io/File;Ljava/io/File;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;Ljava/io/File;)V
    .locals 0
    .param p0    # Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;
    .param p1    # Ljava/io/File;

    invoke-direct {p0, p1}, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->chmodFile(Ljava/io/File;)V

    return-void
.end method

.method static synthetic access$200(Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0    # Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->mpDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method private addItemOsdLanguage()V
    .locals 2

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0036    # com.konka.factory.R.id.linearlayout_factory_customerspecialsettings_OSD_language

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->OsdSettingLanguage:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->updateCusDefLanguage()V

    return-void
.end method

.method private chmodFile(Ljava/io/File;)V
    .locals 4

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "chmod 666 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "zyl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "command = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "zyl"

    const-string v2, "chmod fail!!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private copyFile(Ljava/io/File;Ljava/io/File;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "CustomerSpecialsetting"

    const-string v2, "~src file not exits~"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->isFile()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "CustomerSpecialsetting"

    const-string v2, "~src file is not a file~"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->canRead()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "CustomerSpecialsetting"

    const-string v2, "~src file can  not read~"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "CustomerSpecialsetting"

    const-string v2, "~dest file not exits~"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {p2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    :cond_3
    invoke-virtual {p2}, Ljava/io/File;->createNewFile()Z

    :cond_4
    invoke-virtual {p2}, Ljava/io/File;->canRead()Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "CustomerSpecialsetting"

    const-string v2, "~dest file can  not read~"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_5
    const-string v1, "CustomerSpecialsetting"

    const-string v2, "~src file OK~"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    new-instance v2, Ljava/io/BufferedInputStream;

    invoke-direct {v2, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    new-instance v4, Ljava/io/BufferedOutputStream;

    invoke-direct {v4, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    const v5, 0x1f4000

    new-array v5, v5, [B

    :goto_1
    invoke-virtual {v2, v5}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_6

    invoke-virtual {v4, v5, v0, v6}, Ljava/io/BufferedOutputStream;->write([BII)V

    goto :goto_1

    :cond_6
    invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->flush()V

    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V

    invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->close()V

    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    const-string v0, "CustomerSpecialsetting"

    const-string v1, "~chmod dest file OK~"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method private getCustomerToDisplay([C)Ljava/lang/String;
    .locals 7

    const/4 v6, 0x3

    const/4 v1, 0x0

    const/4 v5, -0x1

    invoke-static {p1}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v2

    const-string v0, "DST ======="

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iput v5, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->OptionCustomerIndex:I

    const/16 v0, 0x8

    invoke-virtual {v2, v6, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const-string v3, "KONKA"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->OptionCustomersToSave:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    invoke-virtual {v2, v1, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->OptionCustomersToSave:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->OptionCustomerIndex:I

    :cond_0
    iget v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->OptionCustomerIndex:I

    if-ne v0, v5, :cond_2

    const-string v0, "no"

    :goto_1
    return-object v0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->OptionCustomers:[Ljava/lang/String;

    iget v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->OptionCustomerIndex:I

    aget-object v0, v0, v1

    goto :goto_1
.end method

.method private updateCusDefLanguage()V
    .locals 3

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getDefaultLanguage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->DefOSDLanguageTmp:Ljava/lang/String;

    const-string v0, "DST OnCreate=========="

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->DefOSDLanguageTmp:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->DefOSDLanguageTmp:Ljava/lang/String;

    const-string v1, "en_US"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguageindex:I

    :cond_0
    :goto_0
    const-string v0, "DST OnCreate==========="

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguageindex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->DefOSDLanguageTmp:Ljava/lang/String;

    const-string v1, "ru_RU"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    iput v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguageindex:I

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->DefOSDLanguageTmp:Ljava/lang/String;

    const-string v1, "fr_FR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x2

    iput v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguageindex:I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->DefOSDLanguageTmp:Ljava/lang/String;

    const-string v1, "ar_EG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x3

    iput v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguageindex:I

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->DefOSDLanguageTmp:Ljava/lang/String;

    const-string v1, "iw_IL"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x4

    iput v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguageindex:I

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->DefOSDLanguageTmp:Ljava/lang/String;

    const-string v1, "in_ID"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x5

    iput v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguageindex:I

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->DefOSDLanguageTmp:Ljava/lang/String;

    const-string v1, "fa_IR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    const/4 v0, 0x6

    iput v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguageindex:I

    goto :goto_0

    :cond_7
    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->DefOSDLanguageTmp:Ljava/lang/String;

    const-string v1, "kd_KD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x7

    iput v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguageindex:I

    goto :goto_0

    :cond_8
    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->DefOSDLanguageTmp:Ljava/lang/String;

    const-string v1, "tr_TR"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    const/16 v0, 0x8

    iput v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguageindex:I

    goto/16 :goto_0

    :cond_9
    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->DefOSDLanguageTmp:Ljava/lang/String;

    const-string v1, "th_TH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    const/16 v0, 0x9

    iput v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguageindex:I

    goto/16 :goto_0

    :cond_a
    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->DefOSDLanguageTmp:Ljava/lang/String;

    const-string v1, "zh_CN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xa

    iput v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguageindex:I

    goto/16 :goto_0
.end method

.method private updateCusDefLanguageIndex()V
    .locals 2

    iget v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguageindex:I

    if-nez v0, :cond_1

    const-string v0, "en_US"

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->DefOSDLanguageTmp:Ljava/lang/String;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguageindex:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    const-string v0, "ru_RU"

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->DefOSDLanguageTmp:Ljava/lang/String;

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguageindex:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    const-string v0, "fr_FR"

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->DefOSDLanguageTmp:Ljava/lang/String;

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguageindex:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    const-string v0, "ar_EG"

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->DefOSDLanguageTmp:Ljava/lang/String;

    goto :goto_0

    :cond_4
    iget v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguageindex:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_5

    const-string v0, "iw_IL"

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->DefOSDLanguageTmp:Ljava/lang/String;

    goto :goto_0

    :cond_5
    iget v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguageindex:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_6

    const-string v0, "in_ID"

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->DefOSDLanguageTmp:Ljava/lang/String;

    goto :goto_0

    :cond_6
    iget v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguageindex:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_7

    const-string v0, "fa_IR"

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->DefOSDLanguageTmp:Ljava/lang/String;

    goto :goto_0

    :cond_7
    iget v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguageindex:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_8

    const-string v0, "kd_KD"

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->DefOSDLanguageTmp:Ljava/lang/String;

    goto :goto_0

    :cond_8
    iget v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguageindex:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_9

    const-string v0, "tr_TR"

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->DefOSDLanguageTmp:Ljava/lang/String;

    goto :goto_0

    :cond_9
    iget v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguageindex:I

    const/16 v1, 0x9

    if-ne v0, v1, :cond_a

    const-string v0, "th_TH"

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->DefOSDLanguageTmp:Ljava/lang/String;

    goto :goto_0

    :cond_a
    iget v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguageindex:I

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    const-string v0, "zh_CN"

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->DefOSDLanguageTmp:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public OnCreate()V
    .locals 13

    const/16 v12, 0x8

    iget-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->context:Landroid/content/Context;

    invoke-static {v9}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    check-cast v1, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;

    invoke-virtual {v1}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getCustomerInfo()Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;

    move-result-object v9

    iget-object v9, v9, Lcom/konka/kkinterface/tv/CommonDesk$CustomerInfo;->strCustomerName:Ljava/lang/String;

    iput-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->curCustomer:Ljava/lang/String;

    iget-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->curCustomer:Ljava/lang/String;

    sget-object v10, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PersionCustomer:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v9}, Lcom/konka/factory/MainmenuActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090002    # com.konka.factory.R.array.str_arr_autotuning_country_snowa

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->OptionCountrys:[Ljava/lang/String;

    :goto_0
    iget-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v9}, Lcom/konka/factory/desk/IFactoryDesk;->getDefaultAutoTuningCountry()I

    move-result v9

    iput v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->OptionCountrysIndex:I

    iget-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->curCustomer:Ljava/lang/String;

    sget-object v10, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PersionCustomer:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    iget-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->CountrySelectTable_snowa:[I

    array-length v6, v9

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v6, :cond_0

    iget-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->CountrySelectTable_snowa:[I

    aget v9, v9, v4

    iget v10, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->OptionCountrysIndex:I

    if-ne v9, v10, :cond_3

    iput v4, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->OptionCountrysIndex:I

    :cond_0
    :goto_2
    const-string v9, "UpdateCustomerDefaultSetting"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "**00**OptionCountrysIndex==  "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->OptionCountrysIndex:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->addItemOsdLanguage()V

    iget-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v9}, Lcom/konka/factory/MainmenuActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090004    # com.konka.factory.R.array.str_customer_id

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->OptionCustomers:[Ljava/lang/String;

    iget-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v9}, Lcom/konka/factory/MainmenuActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090005    # com.konka.factory.R.array.str_customer_id_to_save

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->OptionCustomersToSave:[Ljava/lang/String;

    iget-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->updateAdjustViewHolder:Lcom/konka/factory/UPDATEAdjustViewHolder;

    new-array v0, v12, [S

    iget-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->updateAdjustViewHolder:Lcom/konka/factory/UPDATEAdjustViewHolder;

    new-array v2, v12, [C

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v9

    iget-object v10, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->updateAdjustViewHolder:Lcom/konka/factory/UPDATEAdjustViewHolder;

    const/16 v10, 0x1d

    iget-object v11, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->updateAdjustViewHolder:Lcom/konka/factory/UPDATEAdjustViewHolder;

    iget-object v11, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->updateAdjustViewHolder:Lcom/konka/factory/UPDATEAdjustViewHolder;

    const/16 v11, 0xd8

    invoke-virtual {v9, v10, v11}, Lcom/mstar/android/tvapi/common/TvManager;->readFromSpiFlashByBank(II)[S
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_3
    const/4 v4, 0x0

    :goto_4
    iget-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->updateAdjustViewHolder:Lcom/konka/factory/UPDATEAdjustViewHolder;

    if-ge v4, v12, :cond_8

    iget-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->updateAdjustViewHolder:Lcom/konka/factory/UPDATEAdjustViewHolder;

    add-int/lit16 v9, v4, 0xd0

    aget-short v9, v0, v9

    int-to-char v9, v9

    aput-char v9, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :cond_1
    iget-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->curCustomer:Ljava/lang/String;

    sget-object v10, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PersionCustomer1:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    iget-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v9}, Lcom/konka/factory/MainmenuActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090003    # com.konka.factory.R.array.str_arr_autotuning_country_xvision

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->OptionCountrys:[Ljava/lang/String;

    goto/16 :goto_0

    :cond_2
    iget-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v9}, Lcom/konka/factory/MainmenuActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f090001    # com.konka.factory.R.array.str_arr_autotuning_country

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v9

    iput-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->OptionCountrys:[Ljava/lang/String;

    goto/16 :goto_0

    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    :cond_4
    iget-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->curCustomer:Ljava/lang/String;

    sget-object v10, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PersionCustomer1:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    iget-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->CountrySelectTable_xvision:[I

    array-length v7, v9

    const/4 v4, 0x0

    :goto_5
    if-ge v4, v7, :cond_0

    iget-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->CountrySelectTable_xvision:[I

    aget v9, v9, v4

    iget v10, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->OptionCountrysIndex:I

    if-ne v9, v10, :cond_5

    iput v4, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->OptionCountrysIndex:I

    goto/16 :goto_2

    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    :cond_6
    iget-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->CountrySelectTable:[I

    array-length v5, v9

    const/4 v4, 0x0

    :goto_6
    if-ge v4, v5, :cond_0

    iget-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->CountrySelectTable:[I

    aget v9, v9, v4

    iget v10, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->OptionCountrysIndex:I

    if-ne v9, v10, :cond_7

    iput v4, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->OptionCountrysIndex:I

    goto/16 :goto_2

    :cond_7
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_3

    :cond_8
    invoke-direct {p0, v2}, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->getCustomerToDisplay([C)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->text_factory_customerspecialsettings_OSD_language_option:Landroid/widget/TextView;

    iget-object v10, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguage:[Ljava/lang/String;

    iget v11, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguageindex:I

    aget-object v10, v10, v11

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->text_factory_customerspecialsettings_tunning_country_option:Landroid/widget/TextView;

    iget-object v10, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->OptionCountrys:[Ljava/lang/String;

    iget v11, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->OptionCountrysIndex:I

    aget-object v10, v10, v11

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v9}, Lcom/konka/factory/desk/IFactoryDesk;->getTeletextMode()I

    move-result v9

    iput v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->teletextenableindex:I

    iget-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->text_factory_customerspecialsettings_teletextenable_val:Landroid/widget/TextView;

    iget-object v10, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->teletextenable:[Ljava/lang/String;

    iget v11, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->teletextenableindex:I

    aget-object v10, v10, v11

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v9}, Lcom/konka/factory/desk/IFactoryDesk;->getPowerOnMode()I

    move-result v9

    iput v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerMOdeEnableindex:I

    iget-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->textview_factory_customerspecialsettings_power_mode_option:Landroid/widget/TextView;

    iget-object v10, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerMOdeEnable:[Ljava/lang/String;

    iget v11, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerMOdeEnableindex:I

    aget-object v10, v10, v11

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v9}, Lcom/konka/factory/desk/IFactoryDesk;->getPowerOffLogoMode()I

    move-result v9

    iput v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->TunrOffLogoEnableindex:I

    iget-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->textview_factory_customerspecialsettings_turnoff_logo_option:Landroid/widget/TextView;

    iget-object v10, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->TunrOffLogoEnable:[Ljava/lang/String;

    iget v11, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->TunrOffLogoEnableindex:I

    aget-object v10, v10, v11

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v9}, Lcom/konka/factory/desk/IFactoryDesk;->getPowerOffLogoDspTime()I

    move-result v9

    div-int/lit16 v9, v9, 0x3e8

    iput v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerOffLogoDspTime:I

    iget-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->textview_factory_customerspecialsettings_logo_time_option:Landroid/widget/TextView;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget v11, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerOffLogoDspTime:I

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "S"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->text_factory_customerspecialsettings_tunning_customer_id:Landroid/widget/TextView;

    invoke-virtual {v9, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v9}, Lcom/konka/factory/desk/IFactoryDesk;->getAudioDelayTime()I

    move-result v9

    iput v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->AudioDelayTime:I

    iget-object v9, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->textview_factory_customerspecialsettings_Audio_delay_option:Landroid/widget/TextView;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget v11, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->AudioDelayTime:I

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "MS"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public SetPowerOnMode()V
    .locals 4

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "SetPowerOnMode!"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :try_start_0
    const-string v0, "setEnvironment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "factory_poweron_mode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerMOdeEnable:[Ljava/lang/String;

    iget v3, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerMOdeEnableindex:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    const-string v1, "factory_poweron_mode"

    iget-object v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerMOdeEnable:[Ljava/lang/String;

    iget v3, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerMOdeEnableindex:I

    aget-object v2, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/mstar/android/tvapi/common/TvManager;->setEnvironment(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method findView()V
    .locals 2

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0036    # com.konka.factory.R.id.linearlayout_factory_customerspecialsettings_OSD_language

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->linear_factory_customerspecialsettings_OSD_Language:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0038    # com.konka.factory.R.id.textview_factory_customerspecialsettings_OSD_language_option

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->text_factory_customerspecialsettings_OSD_language_option:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0039    # com.konka.factory.R.id.linearlayout_factory_customerspecialsettings_tunning_country

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->linearlayout_factory_customerspecialsettings_tunning_country:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a003b    # com.konka.factory.R.id.textview_factory_customerspecialsettings_tunning_country_option

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->text_factory_customerspecialsettings_tunning_country_option:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a003c    # com.konka.factory.R.id.linearlayout_factory_customerspecialsettings_power_mode

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->linearlayout_factory_customerspecialsettings_power_mode:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0041    # com.konka.factory.R.id.linearlayout_factory_customerspecialsettings_turnoff_logo

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->linearlayout_factory_customerspecialsettings_turnoff_logo:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0044    # com.konka.factory.R.id.linearlayout_factory_customerspecialsettings_logo_time

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->linearlayout_factory_customerspecialsettings_logo_time:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0057    # com.konka.factory.R.id.textview_factory_customerspecialsettings_customer_id_option

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->text_factory_customerspecialsettings_tunning_customer_id:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0058    # com.konka.factory.R.id.linearlayout_factory_customerspecialsettings_Audio_delay

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->linearlayout_factory_customerspecialsettings_Audio_delay:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a005b    # com.konka.factory.R.id.linearlayout_factory_customerspecialsettings_AdVideo_update

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->linearlayout_factory_customerspecialsettings_AdVideo_update:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a005e    # com.konka.factory.R.id.linearlayout_factory_customerspecialsettings_Sticker_update

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->linearlayout_factory_customerspecialsettings_Sticker_update:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a003e    # com.konka.factory.R.id.textview_factory_customerspecialsettings_power_mode_option

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->textview_factory_customerspecialsettings_power_mode_option:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0043    # com.konka.factory.R.id.textview_factory_customerspecialsettings_turnoff_logo_option

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->textview_factory_customerspecialsettings_turnoff_logo_option:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0046    # com.konka.factory.R.id.textview_factory_customerspecialsettings_logo_time_option

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->textview_factory_customerspecialsettings_logo_time_option:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0047    # com.konka.factory.R.id.linearlayout_factory_customerspecialsettings_hotel_mode

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->linearlayout_factory_customerspecialsettings_hotel_mode:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a005a    # com.konka.factory.R.id.textview_factory_customerspecialsettings_Audio_delay_option

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->textview_factory_customerspecialsettings_Audio_delay_option:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a005d    # com.konka.factory.R.id.textview_factory_customerspecialsettings_AdVideo_update_state

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->textview_factory_customerspecialsettings_AdVideo_update_state:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0060    # com.konka.factory.R.id.textview_factory_customerspecialsettings_Sticker_update_state

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->textview_factory_customerspecialsettings_Sticker_update_state:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a004b    # com.konka.factory.R.id.textview_factory_customerspecialsettings_teletextenable_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->text_factory_customerspecialsettings_teletextenable_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a004e    # com.konka.factory.R.id.textview_factory_customerspecialsettings_update_state

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->text_factory_customerspecialsettings_logo_updagte_state:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a004c    # com.konka.factory.R.id.linearlayout_factory_customerspecialsettings_logo_update

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->linear_factory_customerspecialsettings_logo_update:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0050    # com.konka.factory.R.id.textview_factory_update_usbreadWB

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->textview_usbreadWB:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0053    # com.konka.factory.R.id.textview_factory_update_usbwriteWB

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->textview_usbwriteWB:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0051    # com.konka.factory.R.id.textview_factory_update_usbreadWBstate

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->textview_usbreadWBstate:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0054    # com.konka.factory.R.id.textview_factory_update_usbwriteWBstate

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->textview_usbwriteWBstate:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0035    # com.konka.factory.R.id.textview_factory_customerspecialsettings_factory_update_state

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->text_factory_customerspecialsettings_factory_update_state:Landroid/widget/TextView;

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5

    const/16 v4, 0xa

    const/4 v3, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v2}, Lcom/konka/factory/MainmenuActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    sparse-switch p1, :sswitch_data_0

    move v0, v1

    :cond_0
    :goto_0
    return v0

    :sswitch_0
    sparse-switch v2, :sswitch_data_1

    goto :goto_0

    :sswitch_1
    iget v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguageindex:I

    if-ge v2, v4, :cond_1

    iget v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguageindex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguageindex:I

    :goto_1
    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->text_factory_customerspecialsettings_OSD_language_option:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguage:[Ljava/lang/String;

    iget v3, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguageindex:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->updateCusDefLanguageIndex()V

    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v1}, Lcom/konka/factory/desk/IFactoryDesk;->setCustomerSettingEnable()Z

    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget-object v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->DefOSDLanguageTmp:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/konka/factory/desk/IFactoryDesk;->setDefaultLanguage(Ljava/lang/String;)Z

    goto :goto_0

    :cond_1
    iput v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguageindex:I

    goto :goto_1

    :sswitch_2
    iget v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->teletextenableindex:I

    if-nez v2, :cond_2

    iput v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->teletextenableindex:I

    :goto_2
    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->teletextenableindex:I

    invoke-interface {v1, v2}, Lcom/konka/factory/desk/IFactoryDesk;->setTeletextMode(I)Z

    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->text_factory_customerspecialsettings_teletextenable_val:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->teletextenable:[Ljava/lang/String;

    iget v3, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->teletextenableindex:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    iput v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->teletextenableindex:I

    goto :goto_2

    :sswitch_3
    iget v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerMOdeEnableindex:I

    if-eq v2, v3, :cond_3

    iget v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerMOdeEnableindex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerMOdeEnableindex:I

    :goto_3
    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->textview_factory_customerspecialsettings_power_mode_option:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerMOdeEnable:[Ljava/lang/String;

    iget v3, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerMOdeEnableindex:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerMOdeEnableindex:I

    invoke-interface {v1, v2}, Lcom/konka/factory/desk/IFactoryDesk;->setPowerOnMode(I)Z

    invoke-virtual {p0}, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->SetPowerOnMode()V

    goto :goto_0

    :cond_3
    iput v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerMOdeEnableindex:I

    goto :goto_3

    :sswitch_4
    iget v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->TunrOffLogoEnableindex:I

    if-ne v2, v0, :cond_4

    iput v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->TunrOffLogoEnableindex:I

    :goto_4
    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->TunrOffLogoEnableindex:I

    invoke-interface {v1, v2}, Lcom/konka/factory/desk/IFactoryDesk;->setPowerOffLogoMode(I)Z

    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->textview_factory_customerspecialsettings_turnoff_logo_option:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->TunrOffLogoEnable:[Ljava/lang/String;

    iget v3, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->TunrOffLogoEnableindex:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_4
    iput v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->TunrOffLogoEnableindex:I

    goto :goto_4

    :sswitch_5
    iget v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerOffLogoDspTime:I

    const/16 v2, 0x64

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerOffLogoDspTime:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerOffLogoDspTime:I

    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerOffLogoDspTime:I

    mul-int/lit16 v2, v2, 0x3e8

    invoke-interface {v1, v2}, Lcom/konka/factory/desk/IFactoryDesk;->setPowerOffLogoDspTime(I)Z

    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->textview_factory_customerspecialsettings_logo_time_option:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerOffLogoDspTime:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "S"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_6
    iget v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->AudioDelayTime:I

    const/16 v2, 0xfa

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->AudioDelayTime:I

    add-int/lit8 v1, v1, 0x32

    iput v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->AudioDelayTime:I

    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->AudioDelayTime:I

    invoke-interface {v1, v2}, Lcom/konka/factory/desk/IFactoryDesk;->setAudioDelayTime(I)Z

    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->textview_factory_customerspecialsettings_Audio_delay_option:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->AudioDelayTime:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "MS"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_7
    sparse-switch v2, :sswitch_data_2

    goto/16 :goto_0

    :sswitch_8
    iget v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguageindex:I

    if-eqz v1, :cond_5

    iget v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguageindex:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguageindex:I

    :goto_5
    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->text_factory_customerspecialsettings_OSD_language_option:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguage:[Ljava/lang/String;

    iget v3, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguageindex:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->updateCusDefLanguageIndex()V

    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v1}, Lcom/konka/factory/desk/IFactoryDesk;->setCustomerSettingEnable()Z

    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget-object v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->DefOSDLanguageTmp:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/konka/factory/desk/IFactoryDesk;->setDefaultLanguage(Ljava/lang/String;)Z

    goto/16 :goto_0

    :cond_5
    iput v4, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->defaultlanguageindex:I

    goto :goto_5

    :sswitch_9
    iget v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->teletextenableindex:I

    if-ne v2, v0, :cond_6

    iput v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->teletextenableindex:I

    :goto_6
    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->teletextenableindex:I

    invoke-interface {v1, v2}, Lcom/konka/factory/desk/IFactoryDesk;->setTeletextMode(I)Z

    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->text_factory_customerspecialsettings_teletextenable_val:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->teletextenable:[Ljava/lang/String;

    iget v3, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->teletextenableindex:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_6
    iput v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->teletextenableindex:I

    goto :goto_6

    :sswitch_a
    iget v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerMOdeEnableindex:I

    if-eqz v1, :cond_7

    iget v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerMOdeEnableindex:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerMOdeEnableindex:I

    :goto_7
    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->textview_factory_customerspecialsettings_power_mode_option:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerMOdeEnable:[Ljava/lang/String;

    iget v3, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerMOdeEnableindex:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerMOdeEnableindex:I

    invoke-interface {v1, v2}, Lcom/konka/factory/desk/IFactoryDesk;->setPowerOnMode(I)Z

    invoke-virtual {p0}, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->SetPowerOnMode()V

    goto/16 :goto_0

    :cond_7
    iput v3, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerMOdeEnableindex:I

    goto :goto_7

    :sswitch_b
    iget v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->TunrOffLogoEnableindex:I

    if-nez v2, :cond_8

    iput v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->TunrOffLogoEnableindex:I

    :goto_8
    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->TunrOffLogoEnableindex:I

    invoke-interface {v1, v2}, Lcom/konka/factory/desk/IFactoryDesk;->setPowerOffLogoMode(I)Z

    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->textview_factory_customerspecialsettings_turnoff_logo_option:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->TunrOffLogoEnable:[Ljava/lang/String;

    iget v3, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->TunrOffLogoEnableindex:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_8
    iput v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->TunrOffLogoEnableindex:I

    goto :goto_8

    :sswitch_c
    iget v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerOffLogoDspTime:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerOffLogoDspTime:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerOffLogoDspTime:I

    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerOffLogoDspTime:I

    mul-int/lit16 v2, v2, 0x3e8

    invoke-interface {v1, v2}, Lcom/konka/factory/desk/IFactoryDesk;->setPowerOffLogoDspTime(I)Z

    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->textview_factory_customerspecialsettings_logo_time_option:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->PowerOffLogoDspTime:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "S"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_d
    iget v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->AudioDelayTime:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->AudioDelayTime:I

    add-int/lit8 v1, v1, -0x32

    iput v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->AudioDelayTime:I

    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->AudioDelayTime:I

    invoke-interface {v1, v2}, Lcom/konka/factory/desk/IFactoryDesk;->setAudioDelayTime(I)Z

    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->textview_factory_customerspecialsettings_Audio_delay_option:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->AudioDelayTime:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "MS"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_e
    sparse-switch v2, :sswitch_data_3

    goto/16 :goto_0

    :sswitch_f
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.konka.factory.intent.action.CustomizedUpdateActivity"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->intent:Landroid/content/Intent;

    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    iget-object v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->intent:Landroid/content/Intent;

    invoke-virtual {v1, v2}, Lcom/konka/factory/MainmenuActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :sswitch_10
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.konka.factory.intent.action.FactoryAutoTuneOptionActivity"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->intent:Landroid/content/Intent;

    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    iget-object v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->intent:Landroid/content/Intent;

    invoke-virtual {v1, v2}, Lcom/konka/factory/MainmenuActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :sswitch_11
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.konka.factory.intent.action.CustomerIDActivity"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->intent:Landroid/content/Intent;

    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    iget-object v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->intent:Landroid/content/Intent;

    invoke-virtual {v1, v2}, Lcom/konka/factory/MainmenuActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :sswitch_12
    iget-boolean v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->logo_update_state:Z

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->text_factory_customerspecialsettings_logo_updagte_state:Landroid/widget/TextView;

    const-string v3, "Please Click!"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iput-boolean v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->logo_update_state:Z

    goto/16 :goto_0

    :cond_9
    invoke-virtual {p0}, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->updateLogoFile()V

    iput-boolean v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->logo_update_state:Z

    goto/16 :goto_0

    :sswitch_13
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "write usbWB"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->IO(I)Z

    move-result v1

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->textview_usbreadWBstate:Landroid/widget/TextView;

    const-string v2, "OK"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_a
    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->textview_usbreadWBstate:Landroid/widget/TextView;

    const-string v2, "failed"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_14
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "read usbWB"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->IO(I)Z

    move-result v1

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->textview_usbwriteWBstate:Landroid/widget/TextView;

    const-string v2, "OK"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_b
    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->textview_usbwriteWBstate:Landroid/widget/TextView;

    const-string v2, "failed"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_15
    iget-boolean v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->AdVideo_update_state:Z

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->textview_factory_customerspecialsettings_AdVideo_update_state:Landroid/widget/TextView;

    const-string v3, "Please Click!"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iput-boolean v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->AdVideo_update_state:Z

    goto/16 :goto_0

    :cond_c
    invoke-virtual {p0}, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->updateAdVideoFile()V

    iput-boolean v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->AdVideo_update_state:Z

    goto/16 :goto_0

    :sswitch_16
    iget-boolean v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->Sticker_update_state:Z

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->textview_factory_customerspecialsettings_Sticker_update_state:Landroid/widget/TextView;

    const-string v3, "Please Click!"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iput-boolean v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->Sticker_update_state:Z

    goto/16 :goto_0

    :cond_d
    invoke-virtual {p0}, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->updateStickerFile()V

    iput-boolean v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->Sticker_update_state:Z

    goto/16 :goto_0

    :sswitch_17
    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->text_factory_customerspecialsettings_logo_updagte_state:Landroid/widget/TextView;

    const-string v2, "Please Click!"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Lcom/konka/factory/MainmenuActivity;->returnRoot(I)V

    iget-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v1, v3}, Lcom/konka/factory/MainmenuActivity;->returnRoot(I)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_17
        0x15 -> :sswitch_7
        0x16 -> :sswitch_0
        0x42 -> :sswitch_e
        0x52 -> :sswitch_17
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x7f0a0036 -> :sswitch_1    # com.konka.factory.R.id.linearlayout_factory_customerspecialsettings_OSD_language
        0x7f0a003c -> :sswitch_3    # com.konka.factory.R.id.linearlayout_factory_customerspecialsettings_power_mode
        0x7f0a0041 -> :sswitch_4    # com.konka.factory.R.id.linearlayout_factory_customerspecialsettings_turnoff_logo
        0x7f0a0044 -> :sswitch_5    # com.konka.factory.R.id.linearlayout_factory_customerspecialsettings_logo_time
        0x7f0a0049 -> :sswitch_2    # com.konka.factory.R.id.linearlayout_factory_customerspecialsettings_teletext
        0x7f0a0058 -> :sswitch_6    # com.konka.factory.R.id.linearlayout_factory_customerspecialsettings_Audio_delay
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x7f0a0036 -> :sswitch_8    # com.konka.factory.R.id.linearlayout_factory_customerspecialsettings_OSD_language
        0x7f0a003c -> :sswitch_a    # com.konka.factory.R.id.linearlayout_factory_customerspecialsettings_power_mode
        0x7f0a0041 -> :sswitch_b    # com.konka.factory.R.id.linearlayout_factory_customerspecialsettings_turnoff_logo
        0x7f0a0044 -> :sswitch_c    # com.konka.factory.R.id.linearlayout_factory_customerspecialsettings_logo_time
        0x7f0a0049 -> :sswitch_9    # com.konka.factory.R.id.linearlayout_factory_customerspecialsettings_teletext
        0x7f0a0058 -> :sswitch_d    # com.konka.factory.R.id.linearlayout_factory_customerspecialsettings_Audio_delay
    .end sparse-switch

    :sswitch_data_3
    .sparse-switch
        0x7f0a0033 -> :sswitch_f    # com.konka.factory.R.id.linearlayout_factory_customerspecialsettings_factory_update
        0x7f0a0039 -> :sswitch_10    # com.konka.factory.R.id.linearlayout_factory_customerspecialsettings_tunning_country
        0x7f0a004c -> :sswitch_12    # com.konka.factory.R.id.linearlayout_factory_customerspecialsettings_logo_update
        0x7f0a004f -> :sswitch_13    # com.konka.factory.R.id.linearlayout_factory_update_usbreadWB
        0x7f0a0052 -> :sswitch_14    # com.konka.factory.R.id.linearlayout_factory_update_usbwriteWB
        0x7f0a0055 -> :sswitch_11    # com.konka.factory.R.id.linearlayout_factory_customerspecialsettings_customer_id
        0x7f0a005b -> :sswitch_15    # com.konka.factory.R.id.linearlayout_factory_customerspecialsettings_AdVideo_update
        0x7f0a005e -> :sswitch_16    # com.konka.factory.R.id.linearlayout_factory_customerspecialsettings_Sticker_update
    .end sparse-switch
.end method

.method public updateAdVideoFile()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->customerspecialsettingsActivity:Lcom/konka/factory/MainmenuActivity;

    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, v0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->mpDialog:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->mpDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->mpDialog:Landroid/app/ProgressDialog;

    const-string v1, "Start updating Advert Video file!"

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->mpDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->mpDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->mpDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Ljava/lang/Thread;

    new-instance v2, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder$1;

    invoke-direct {v2, p0, v0}, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder$1;-><init>(Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;Landroid/os/Handler;)V

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public updateLogoFile()V
    .locals 6

    const/4 v1, 0x0

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    const-string v0, "/customercfg/customerlogo/bootanimation.zip"

    const-string v2, "CustomerSpecialsetting"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "....logo update....destPath is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".........."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Ljava/io/File;

    const-string v3, "/mnt/usb/sda1/bootanimation.zip"

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_0
    invoke-direct {p0, v2, v3}, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->copyFile(Ljava/io/File;Ljava/io/File;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    :try_start_1
    invoke-direct {p0, v3}, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->chmodFile(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    const-string v1, "CustomerSpecialsetting"

    const-string v2, ".......update logo OK!!!!!!!!!!.........."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->text_factory_customerspecialsettings_logo_updagte_state:Landroid/widget/TextView;

    const-string v1, "OK!!!"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    return-void

    :catch_0
    move-exception v0

    move-object v5, v0

    move v0, v1

    move-object v1, v5

    :goto_3
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->text_factory_customerspecialsettings_logo_updagte_state:Landroid/widget/TextView;

    const-string v1, "No File On sdcard!"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_3

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public updateStickerFile()V
    .locals 5

    const/4 v1, 0x0

    new-instance v0, Ljava/io/File;

    const-string v2, "/mnt/usb/sda1/StickerDemo/sticker.png"

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/io/File;

    const-string v3, "/customercfg/StickerDemo/sticker.png"

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_0
    invoke-direct {p0, v0, v2}, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->copyFile(Ljava/io/File;Ljava/io/File;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    :try_start_1
    invoke-direct {p0, v2}, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->chmodFile(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    const-string v1, "CustomerSpecialsetting"

    const-string v2, ".......update Sticker OK!!!!!!!!!!.........."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->textview_factory_customerspecialsettings_Sticker_update_state:Landroid/widget/TextView;

    const-string v1, "OK!!!"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    return-void

    :catch_0
    move-exception v0

    move-object v4, v0

    move v0, v1

    move-object v1, v4

    :goto_3
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/konka/factory/CustomerSpecialSettingsAdjustViewHolder;->textview_factory_customerspecialsettings_Sticker_update_state:Landroid/widget/TextView;

    const-string v1, "No File On sdcard!"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_3

    :cond_1
    move v0, v1

    goto :goto_0
.end method
