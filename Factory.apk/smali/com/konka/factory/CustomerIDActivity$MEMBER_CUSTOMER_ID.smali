.class public final enum Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;
.super Ljava/lang/Enum;
.source "CustomerIDActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/factory/CustomerIDActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MEMBER_CUSTOMER_ID"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

.field public static final enum E_CUSTOMER_NUM:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

.field public static final enum E_DNS:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

.field public static final enum E_ENIE:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

.field public static final enum E_JVC:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

.field public static final enum E_KOGAN:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

.field public static final enum E_KONKA:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

.field public static final enum E_NASCO:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

.field public static final enum E_NIDAA:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

.field public static final enum E_OTHERS:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

.field public static final enum E_SNOWA:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

.field public static final enum E_TEAC:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

.field public static final enum E_XVISION:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    const-string v1, "E_KOGAN"

    invoke-direct {v0, v1, v3}, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_KOGAN:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    new-instance v0, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    const-string v1, "E_TEAC"

    invoke-direct {v0, v1, v4}, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_TEAC:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    new-instance v0, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    const-string v1, "E_JVC"

    invoke-direct {v0, v1, v5}, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_JVC:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    new-instance v0, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    const-string v1, "E_SNOWA"

    invoke-direct {v0, v1, v6}, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_SNOWA:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    new-instance v0, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    const-string v1, "E_XVISION"

    invoke-direct {v0, v1, v7}, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_XVISION:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    new-instance v0, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    const-string v1, "E_DNS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_DNS:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    new-instance v0, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    const-string v1, "E_ENIE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_ENIE:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    new-instance v0, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    const-string v1, "E_NASCO"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_NASCO:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    new-instance v0, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    const-string v1, "E_NIDAA"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_NIDAA:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    new-instance v0, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    const-string v1, "E_OTHERS"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_OTHERS:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    new-instance v0, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    const-string v1, "E_KONKA"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_KONKA:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    new-instance v0, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    const-string v1, "E_CUSTOMER_NUM"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_CUSTOMER_NUM:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    const/16 v0, 0xc

    new-array v0, v0, [Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    sget-object v1, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_KOGAN:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_TEAC:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_JVC:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    aput-object v1, v0, v5

    sget-object v1, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_SNOWA:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    aput-object v1, v0, v6

    sget-object v1, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_XVISION:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_DNS:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_ENIE:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_NASCO:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_NIDAA:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_OTHERS:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_KONKA:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->E_CUSTOMER_NUM:Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->$VALUES:[Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;
    .locals 1

    const-class v0, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    return-object v0
.end method

.method public static values()[Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;
    .locals 1

    sget-object v0, Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->$VALUES:[Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    invoke-virtual {v0}, [Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/konka/factory/CustomerIDActivity$MEMBER_CUSTOMER_ID;

    return-object v0
.end method
