.class public Lcom/konka/factory/USBUpgradeThread;
.super Ljava/lang/Object;
.source "USBUpgradeThread.java"


# static fields
.field static UPGRATE_END_FAIL:I

.field static UPGRATE_END_FILE_NOT_FOUND:I

.field static UPGRATE_END_SUCCESS:I

.field static UPGRATE_START:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/konka/factory/USBUpgradeThread;->UPGRATE_END_FAIL:I

    const/4 v0, 0x1

    sput v0, Lcom/konka/factory/USBUpgradeThread;->UPGRATE_END_SUCCESS:I

    const/4 v0, 0x2

    sput v0, Lcom/konka/factory/USBUpgradeThread;->UPGRATE_END_FILE_NOT_FOUND:I

    const/4 v0, 0x3

    sput v0, Lcom/konka/factory/USBUpgradeThread;->UPGRATE_START:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static Upgrade6M30()Z
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/factory/USBUpgradeThread$2;

    invoke-direct {v1}, Lcom/konka/factory/USBUpgradeThread$2;-><init>()V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    const/4 v0, 0x1

    return v0
.end method

.method public static UpgradeMboot()Z
    .locals 2

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/factory/USBUpgradeThread$1;

    invoke-direct {v1}, Lcom/konka/factory/USBUpgradeThread$1;-><init>()V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    const/4 v0, 0x1

    return v0
.end method
