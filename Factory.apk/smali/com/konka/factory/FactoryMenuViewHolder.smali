.class public Lcom/konka/factory/FactoryMenuViewHolder;
.super Ljava/lang/Object;
.source "FactoryMenuViewHolder.java"


# instance fields
.field protected burninmodeIndex:I

.field private burninmodeStrs:[Ljava/lang/String;

.field private commondesk:Lcom/konka/kkinterface/tv/CommonDesk;

.field private factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

.field private factoryactivity:Lcom/konka/factory/MainmenuActivity;

.field protected linear_factory_adc:Landroid/widget/LinearLayout;

.field protected linear_factory_burninmode:Landroid/widget/LinearLayout;

.field protected linear_factory_customerspecialsettings:Landroid/widget/LinearLayout;

.field protected linear_factory_defaultconfig:Landroid/widget/LinearLayout;

.field protected linear_factory_energy:Landroid/widget/LinearLayout;

.field protected linear_factory_mountconfig:Landroid/widget/LinearLayout;

.field protected linear_factory_netfactory:Landroid/widget/LinearLayout;

.field protected linear_factory_non_linear:Landroid/widget/LinearLayout;

.field protected linear_factory_nonstandard:Landroid/widget/LinearLayout;

.field protected linear_factory_otheroption:Landroid/widget/LinearLayout;

.field protected linear_factory_overscan:Landroid/widget/LinearLayout;

.field protected linear_factory_peq:Landroid/widget/LinearLayout;

.field protected linear_factory_picturemode:Landroid/widget/LinearLayout;

.field protected linear_factory_pqtableupdate:Landroid/widget/LinearLayout;

.field protected linear_factory_serialprint:Landroid/widget/LinearLayout;

.field protected linear_factory_soundmode:Landroid/widget/LinearLayout;

.field protected linear_factory_ssc:Landroid/widget/LinearLayout;

.field protected linear_factory_update:Landroid/widget/LinearLayout;

.field protected linear_factory_whitebalance:Landroid/widget/LinearLayout;

.field protected lockkeypadIndex:I

.field private lockkeypadStrs:[Ljava/lang/String;

.field protected mountConfigIndex:I

.field private mountConfigStrs:[Ljava/lang/String;

.field protected pqTableUpdateIndex:I

.field private pqTableUpdateStrs:[Ljava/lang/String;

.field protected text_factory_burninmode_val:Landroid/widget/TextView;

.field private text_factory_head_code:Landroid/widget/TextView;

.field private text_factory_head_date:Landroid/widget/TextView;

.field private text_factory_head_panel:Landroid/widget/TextView;

.field private text_factory_head_platform:Landroid/widget/TextView;

.field private text_factory_head_version:Landroid/widget/TextView;

.field protected text_factory_lockkeypad_val:Landroid/widget/TextView;

.field protected text_factory_mountconfig_val:Landroid/widget/TextView;

.field protected text_factory_pqtableupdate_val:Landroid/widget/TextView;

.field private textview_factory_head_mboot:Landroid/widget/TextView;

.field private textview_factory_head_mboot_content:Landroid/widget/TextView;

.field private textview_factory_head_version:Landroid/widget/TextView;

.field private textview_factory_head_version_content:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V
    .locals 1
    .param p1    # Lcom/konka/factory/MainmenuActivity;
    .param p2    # Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->burninmodeIndex:I

    iput v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->mountConfigIndex:I

    iput v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->pqTableUpdateIndex:I

    iput v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->lockkeypadIndex:I

    iput-object p1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    iput-object p2, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    return-void
.end method

.method private isKeypadLocked()Z
    .locals 13

    const/4 v2, 0x0

    const/4 v12, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x0

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v0}, Lcom/konka/factory/MainmenuActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://com.konka.hotelmenu/"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    if-eqz v7, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "factory"

    const-string v1, "LockKeypadFrom"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v8, v11

    :goto_0
    const-string v0, "OnOff"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v11, v0, :cond_3

    move v9, v11

    :goto_1
    const-string v0, "LockKeypad"

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v11, v0, :cond_4

    move v6, v11

    :goto_2
    if-eqz v6, :cond_5

    if-nez v8, :cond_0

    if-eqz v9, :cond_5

    :cond_0
    move v10, v11

    :cond_1
    :goto_3
    return v10

    :cond_2
    move v8, v12

    goto :goto_0

    :cond_3
    move v9, v12

    goto :goto_1

    :cond_4
    move v6, v12

    goto :goto_2

    :cond_5
    move v10, v12

    goto :goto_3
.end method

.method private lockKeypad()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/konka/factory/FactoryMenuViewHolder;->updateKeypadLockState(I)V

    return-void
.end method

.method private unLockKeypad()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/konka/factory/FactoryMenuViewHolder;->updateKeypadLockState(I)V

    return-void
.end method

.method private updateKeypadLockState(I)V
    .locals 6
    .param p1    # I

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string v2, "LockKeypadFrom"

    const-string v3, "factory"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "LockKeypad"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :try_start_0
    iget-object v2, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v2}, Lcom/konka/factory/MainmenuActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "content://com.konka.hotelmenu/"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v1, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method findView()V
    .locals 3

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0084    # com.konka.factory.R.id.linearlayout_factory_adc

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->linear_factory_adc:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0085    # com.konka.factory.R.id.linearlayout_factory_whitebalance

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->linear_factory_whitebalance:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0087    # com.konka.factory.R.id.linearlayout_factory_picturemode

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->linear_factory_picturemode:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0089    # com.konka.factory.R.id.linearlayout_factory_soundmode

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->linear_factory_soundmode:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a008b    # com.konka.factory.R.id.linearlayout_factory_energy

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->linear_factory_energy:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a008d    # com.konka.factory.R.id.linearlayout_factory_nonstandard

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->linear_factory_nonstandard:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a008f    # com.konka.factory.R.id.linearlayout_factory_non_linear

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->linear_factory_non_linear:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0091    # com.konka.factory.R.id.linearlayout_factory_overscan

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->linear_factory_overscan:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0093    # com.konka.factory.R.id.linearlayout_factory_ssc

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->linear_factory_ssc:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0095    # com.konka.factory.R.id.linearlayout_factory_peq

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->linear_factory_peq:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a007c    # com.konka.factory.R.id.linearlayout_factory_update

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->linear_factory_update:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a007a    # com.konka.factory.R.id.linearlayout_factory_netfactory

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->linear_factory_netfactory:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0082    # com.konka.factory.R.id.linearlayout_factory_serialprint

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->linear_factory_serialprint:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0080    # com.konka.factory.R.id.linearlayout_factory_defaultconfig

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->linear_factory_defaultconfig:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0097    # com.konka.factory.R.id.linearlayout_factory_burninmode

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->linear_factory_burninmode:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a009a    # com.konka.factory.R.id.linearlayout_factory_mountconfig

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->linear_factory_mountconfig:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a009d    # com.konka.factory.R.id.linearlayout_factory_pqtableupdate

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->linear_factory_pqtableupdate:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a00a3    # com.konka.factory.R.id.linearlayout_factory_otheroption

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->linear_factory_otheroption:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a007e    # com.konka.factory.R.id.linearlayout_factory_customerspecialsettings

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->linear_factory_customerspecialsettings:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a006f    # com.konka.factory.R.id.textview_factory_adc

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->text_factory_head_platform:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0070    # com.konka.factory.R.id.textview_factory_header_softwarenumber

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->text_factory_head_code:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0072    # com.konka.factory.R.id.textview_factory_header_versionnumber

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->text_factory_head_version:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0073    # com.konka.factory.R.id.textview_factory_header_screentips

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->text_factory_head_panel:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0074    # com.konka.factory.R.id.textview_factory_header_compiledate

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->text_factory_head_date:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0075    # com.konka.factory.R.id.textview_factory_mboot

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->textview_factory_head_mboot:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0076    # com.konka.factory.R.id.textview_factory_mboot_content

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->textview_factory_head_mboot_content:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0077    # com.konka.factory.R.id.textview_factory_version

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->textview_factory_head_version:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0078    # com.konka.factory.R.id.textview_factory_version_content

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->textview_factory_head_version_content:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0099    # com.konka.factory.R.id.textview_factory_burninmode_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->text_factory_burninmode_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a009c    # com.konka.factory.R.id.textview_factory_mountconfig_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->text_factory_mountconfig_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a009f    # com.konka.factory.R.id.textview_factory_pqtableupdate_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->text_factory_pqtableupdate_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a00a2    # com.konka.factory.R.id.textview_factory_lockkeypad_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->text_factory_lockkeypad_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v0}, Lcom/konka/factory/MainmenuActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090006    # com.konka.factory.R.array.str_factory_burninmode_val

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->burninmodeStrs:[Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v0}, Lcom/konka/factory/MainmenuActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090007    # com.konka.factory.R.array.str_factory_mountconfig_val

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->mountConfigStrs:[Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v0}, Lcom/konka/factory/MainmenuActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090008    # com.konka.factory.R.array.str_factory_pqtableupdate_val

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->pqTableUpdateStrs:[Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v0}, Lcom/konka/factory/MainmenuActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090009    # com.konka.factory.R.array.str_factory_lockkeypad_val

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->lockkeypadStrs:[Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->text_factory_mountconfig_val:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->mountConfigStrs:[Ljava/lang/String;

    iget v2, p0, Lcom/konka/factory/FactoryMenuViewHolder;->mountConfigIndex:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->text_factory_pqtableupdate_val:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->pqTableUpdateStrs:[Ljava/lang/String;

    iget v2, p0, Lcom/konka/factory/FactoryMenuViewHolder;->pqTableUpdateIndex:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/konka/factory/FactoryMenuViewHolder;->isKeypadLocked()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->lockkeypadIndex:I

    iget-object v0, p0, Lcom/konka/factory/FactoryMenuViewHolder;->text_factory_lockkeypad_val:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->lockkeypadStrs:[Ljava/lang/String;

    iget v2, p0, Lcom/konka/factory/FactoryMenuViewHolder;->lockkeypadIndex:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 6
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v5, 0x0

    const/4 v4, 0x1

    iget-object v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v1}, Lcom/konka/factory/MainmenuActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return v5

    :pswitch_0
    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "in the burninmode rightkeydown"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->burninmodeIndex:I

    add-int/lit8 v1, v1, 0x1

    rem-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->burninmodeIndex:I

    iget-object v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->text_factory_burninmode_val:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/factory/FactoryMenuViewHolder;->burninmodeStrs:[Ljava/lang/String;

    iget v3, p0, Lcom/konka/factory/FactoryMenuViewHolder;->burninmodeIndex:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->burninmodeIndex:I

    if-ne v1, v4, :cond_0

    iget-object v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v1, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setBurnInMode(Z)Z

    goto :goto_0

    :sswitch_1
    iget v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->mountConfigIndex:I

    add-int/lit8 v1, v1, 0x1

    rem-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->mountConfigIndex:I

    iget-object v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->text_factory_mountconfig_val:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/factory/FactoryMenuViewHolder;->mountConfigStrs:[Ljava/lang/String;

    iget v3, p0, Lcom/konka/factory/FactoryMenuViewHolder;->mountConfigIndex:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :sswitch_2
    iget v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->pqTableUpdateIndex:I

    add-int/lit8 v1, v1, 0x1

    rem-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->pqTableUpdateIndex:I

    iget-object v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->text_factory_pqtableupdate_val:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/factory/FactoryMenuViewHolder;->pqTableUpdateStrs:[Ljava/lang/String;

    iget v3, p0, Lcom/konka/factory/FactoryMenuViewHolder;->pqTableUpdateIndex:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v1, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setBurnInMode(Z)Z

    goto :goto_0

    :sswitch_3
    iget v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->lockkeypadIndex:I

    add-int/lit8 v1, v1, 0x1

    rem-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->lockkeypadIndex:I

    iget-object v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->text_factory_lockkeypad_val:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/factory/FactoryMenuViewHolder;->lockkeypadStrs:[Ljava/lang/String;

    iget v3, p0, Lcom/konka/factory/FactoryMenuViewHolder;->lockkeypadIndex:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->lockkeypadIndex:I

    if-ne v4, v1, :cond_1

    invoke-direct {p0}, Lcom/konka/factory/FactoryMenuViewHolder;->lockKeypad()V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/konka/factory/FactoryMenuViewHolder;->unLockKeypad()V

    goto :goto_0

    :pswitch_1
    sparse-switch v0, :sswitch_data_1

    goto :goto_0

    :sswitch_4
    iget v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->burninmodeIndex:I

    if-eqz v1, :cond_4

    iget v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->burninmodeIndex:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->burninmodeIndex:I

    :goto_1
    iget-object v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->text_factory_burninmode_val:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/factory/FactoryMenuViewHolder;->burninmodeStrs:[Ljava/lang/String;

    iget v3, p0, Lcom/konka/factory/FactoryMenuViewHolder;->burninmodeIndex:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->burninmodeIndex:I

    if-ne v1, v4, :cond_5

    iget-object v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v1, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setBurnInMode(Z)Z

    goto/16 :goto_0

    :sswitch_5
    iget v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->mountConfigIndex:I

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->mountConfigIndex:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->mountConfigIndex:I

    :goto_2
    iget-object v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->text_factory_mountconfig_val:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/factory/FactoryMenuViewHolder;->mountConfigStrs:[Ljava/lang/String;

    iget v3, p0, Lcom/konka/factory/FactoryMenuViewHolder;->mountConfigIndex:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_2
    iput v4, p0, Lcom/konka/factory/FactoryMenuViewHolder;->mountConfigIndex:I

    goto :goto_2

    :sswitch_6
    iget v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->pqTableUpdateIndex:I

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->pqTableUpdateIndex:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->pqTableUpdateIndex:I

    :goto_3
    iget-object v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->text_factory_pqtableupdate_val:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/factory/FactoryMenuViewHolder;->pqTableUpdateStrs:[Ljava/lang/String;

    iget v3, p0, Lcom/konka/factory/FactoryMenuViewHolder;->pqTableUpdateIndex:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_3
    const/4 v1, 0x3

    iput v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->pqTableUpdateIndex:I

    goto :goto_3

    :cond_4
    iput v4, p0, Lcom/konka/factory/FactoryMenuViewHolder;->burninmodeIndex:I

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v1, v5}, Lcom/konka/factory/desk/IFactoryDesk;->setBurnInMode(Z)Z

    goto/16 :goto_0

    :sswitch_7
    iget v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->lockkeypadIndex:I

    if-eqz v1, :cond_6

    iget v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->lockkeypadIndex:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->lockkeypadIndex:I

    :goto_4
    iget-object v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->text_factory_lockkeypad_val:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/factory/FactoryMenuViewHolder;->lockkeypadStrs:[Ljava/lang/String;

    iget v3, p0, Lcom/konka/factory/FactoryMenuViewHolder;->lockkeypadIndex:I

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v1, p0, Lcom/konka/factory/FactoryMenuViewHolder;->lockkeypadIndex:I

    if-ne v4, v1, :cond_7

    invoke-direct {p0}, Lcom/konka/factory/FactoryMenuViewHolder;->lockKeypad()V

    goto/16 :goto_0

    :cond_6
    iput v4, p0, Lcom/konka/factory/FactoryMenuViewHolder;->lockkeypadIndex:I

    goto :goto_4

    :cond_7
    invoke-direct {p0}, Lcom/konka/factory/FactoryMenuViewHolder;->unLockKeypad()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x15
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x7f0a0097 -> :sswitch_0    # com.konka.factory.R.id.linearlayout_factory_burninmode
        0x7f0a009a -> :sswitch_1    # com.konka.factory.R.id.linearlayout_factory_mountconfig
        0x7f0a009d -> :sswitch_2    # com.konka.factory.R.id.linearlayout_factory_pqtableupdate
        0x7f0a00a0 -> :sswitch_3    # com.konka.factory.R.id.linearlayout_factory_lockkeypad
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x7f0a0097 -> :sswitch_4    # com.konka.factory.R.id.linearlayout_factory_burninmode
        0x7f0a009a -> :sswitch_5    # com.konka.factory.R.id.linearlayout_factory_mountconfig
        0x7f0a009d -> :sswitch_6    # com.konka.factory.R.id.linearlayout_factory_pqtableupdate
        0x7f0a00a0 -> :sswitch_7    # com.konka.factory.R.id.linearlayout_factory_lockkeypad
    .end sparse-switch
.end method

.method oncreate()V
    .locals 6

    iget-object v3, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryactivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v3}, Lcom/konka/factory/MainmenuActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/konka/factory/TvRootApp;

    invoke-virtual {v0}, Lcom/konka/factory/TvRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v3

    iput-object v3, p0, Lcom/konka/factory/FactoryMenuViewHolder;->commondesk:Lcom/konka/kkinterface/tv/CommonDesk;

    iget-object v3, p0, Lcom/konka/factory/FactoryMenuViewHolder;->commondesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v3}, Lcom/konka/kkinterface/tv/CommonDesk;->getSystemBoardInfo()Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    move-result-object v1

    iget-object v3, p0, Lcom/konka/factory/FactoryMenuViewHolder;->text_factory_head_platform:Landroid/widget/TextView;

    iget-object v4, v1, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strPlatform:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/FactoryMenuViewHolder;->text_factory_head_code:Landroid/widget/TextView;

    iget-object v4, v1, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strCode:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/FactoryMenuViewHolder;->text_factory_head_version:Landroid/widget/TextView;

    iget-object v4, v1, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strVersion:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/FactoryMenuViewHolder;->text_factory_head_panel:Landroid/widget/TextView;

    iget-object v4, v1, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strPanelName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/FactoryMenuViewHolder;->text_factory_head_date:Landroid/widget/TextView;

    iget-object v4, v1, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strDate:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/FactoryMenuViewHolder;->textview_factory_head_mboot:Landroid/widget/TextView;

    const v4, 0x7f08010a    # com.konka.factory.R.string.str_mboot

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    iget-object v3, p0, Lcom/konka/factory/FactoryMenuViewHolder;->textview_factory_head_mboot_content:Landroid/widget/TextView;

    iget-object v4, v1, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strVersionMboot:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/FactoryMenuViewHolder;->textview_factory_head_version:Landroid/widget/TextView;

    const v4, 0x7f08010b    # com.konka.factory.R.string.str_version

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    iget-object v3, p0, Lcom/konka/factory/FactoryMenuViewHolder;->textview_factory_head_version_content:Landroid/widget/TextView;

    iget-object v4, v1, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strVersion6m30:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/factory/FactoryMenuViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v3}, Lcom/konka/factory/desk/IFactoryDesk;->getBurnInMode()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    iput v3, p0, Lcom/konka/factory/FactoryMenuViewHolder;->burninmodeIndex:I

    :goto_0
    iget-object v3, p0, Lcom/konka/factory/FactoryMenuViewHolder;->text_factory_burninmode_val:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/konka/factory/FactoryMenuViewHolder;->burninmodeStrs:[Ljava/lang/String;

    iget v5, p0, Lcom/konka/factory/FactoryMenuViewHolder;->burninmodeIndex:I

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    const/4 v3, 0x0

    iput v3, p0, Lcom/konka/factory/FactoryMenuViewHolder;->burninmodeIndex:I

    goto :goto_0
.end method
