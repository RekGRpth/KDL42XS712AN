.class public Lcom/konka/factory/EnergyAjustViewHolder;
.super Ljava/lang/Object;
.source "EnergyAjustViewHolder.java"


# instance fields
.field private energyActivity:Lcom/konka/factory/MainmenuActivity;

.field energyswitch:Z

.field private factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

.field percentage:I

.field protected text_factory_energy_energyswitch_val:Landroid/widget/TextView;

.field protected text_factory_energy_percent_val:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V
    .locals 1
    .param p1    # Lcom/konka/factory/MainmenuActivity;
    .param p2    # Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/konka/factory/EnergyAjustViewHolder;->energyswitch:Z

    iput v0, p0, Lcom/konka/factory/EnergyAjustViewHolder;->percentage:I

    iput-object p1, p0, Lcom/konka/factory/EnergyAjustViewHolder;->energyActivity:Lcom/konka/factory/MainmenuActivity;

    iput-object p2, p0, Lcom/konka/factory/EnergyAjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    return-void
.end method


# virtual methods
.method findview()V
    .locals 2

    iget-object v0, p0, Lcom/konka/factory/EnergyAjustViewHolder;->energyActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a006b    # com.konka.factory.R.id.textview_factory_energy_energyswitch_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/EnergyAjustViewHolder;->text_factory_energy_energyswitch_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/EnergyAjustViewHolder;->energyActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a006e    # com.konka.factory.R.id.textview_factory_energy_pecent_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/EnergyAjustViewHolder;->text_factory_energy_percent_val:Landroid/widget/TextView;

    return-void
.end method

.method onCreate()Z
    .locals 3

    iget-object v0, p0, Lcom/konka/factory/EnergyAjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getEnergyEnable()Z

    move-result v0

    iput-boolean v0, p0, Lcom/konka/factory/EnergyAjustViewHolder;->energyswitch:Z

    iget-object v0, p0, Lcom/konka/factory/EnergyAjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v0}, Lcom/konka/factory/desk/IFactoryDesk;->getEnergyPercent()S

    move-result v0

    iput v0, p0, Lcom/konka/factory/EnergyAjustViewHolder;->percentage:I

    iget-boolean v0, p0, Lcom/konka/factory/EnergyAjustViewHolder;->energyswitch:Z

    invoke-virtual {p0, v0}, Lcom/konka/factory/EnergyAjustViewHolder;->setenergyswitch(Z)V

    iget-object v0, p0, Lcom/konka/factory/EnergyAjustViewHolder;->text_factory_energy_percent_val:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v2, p0, Lcom/konka/factory/EnergyAjustViewHolder;->percentage:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x1

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 6
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v5, 0x64

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x1

    iget-object v4, p0, Lcom/konka/factory/EnergyAjustViewHolder;->energyActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v4}, Lcom/konka/factory/MainmenuActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v1

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    iget-boolean v4, p0, Lcom/konka/factory/EnergyAjustViewHolder;->energyswitch:Z

    if-nez v4, :cond_0

    :goto_1
    iput-boolean v2, p0, Lcom/konka/factory/EnergyAjustViewHolder;->energyswitch:Z

    iget-boolean v2, p0, Lcom/konka/factory/EnergyAjustViewHolder;->energyswitch:Z

    invoke-virtual {p0, v2}, Lcom/konka/factory/EnergyAjustViewHolder;->setenergyswitch(Z)V

    iget-object v2, p0, Lcom/konka/factory/EnergyAjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget-boolean v3, p0, Lcom/konka/factory/EnergyAjustViewHolder;->energyswitch:Z

    invoke-interface {v2, v3}, Lcom/konka/factory/desk/IFactoryDesk;->setEnergyEnable(Z)Z

    goto :goto_0

    :cond_0
    move v2, v3

    goto :goto_1

    :pswitch_2
    iget v2, p0, Lcom/konka/factory/EnergyAjustViewHolder;->percentage:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/factory/EnergyAjustViewHolder;->percentage:I

    iget v2, p0, Lcom/konka/factory/EnergyAjustViewHolder;->percentage:I

    if-le v2, v5, :cond_1

    iput v3, p0, Lcom/konka/factory/EnergyAjustViewHolder;->percentage:I

    :cond_1
    iget-object v2, p0, Lcom/konka/factory/EnergyAjustViewHolder;->text_factory_energy_percent_val:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p0, Lcom/konka/factory/EnergyAjustViewHolder;->percentage:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/EnergyAjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v3, p0, Lcom/konka/factory/EnergyAjustViewHolder;->percentage:I

    int-to-short v3, v3

    invoke-interface {v2, v3}, Lcom/konka/factory/desk/IFactoryDesk;->setEnergyPercent(S)Z

    goto :goto_0

    :sswitch_1
    packed-switch v1, :pswitch_data_1

    :pswitch_3
    goto :goto_0

    :pswitch_4
    iget-boolean v4, p0, Lcom/konka/factory/EnergyAjustViewHolder;->energyswitch:Z

    if-nez v4, :cond_2

    :goto_2
    iput-boolean v2, p0, Lcom/konka/factory/EnergyAjustViewHolder;->energyswitch:Z

    iget-boolean v2, p0, Lcom/konka/factory/EnergyAjustViewHolder;->energyswitch:Z

    invoke-virtual {p0, v2}, Lcom/konka/factory/EnergyAjustViewHolder;->setenergyswitch(Z)V

    iget-object v2, p0, Lcom/konka/factory/EnergyAjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget-boolean v3, p0, Lcom/konka/factory/EnergyAjustViewHolder;->energyswitch:Z

    invoke-interface {v2, v3}, Lcom/konka/factory/desk/IFactoryDesk;->setEnergyEnable(Z)Z

    goto :goto_0

    :cond_2
    move v2, v3

    goto :goto_2

    :pswitch_5
    iget v2, p0, Lcom/konka/factory/EnergyAjustViewHolder;->percentage:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/factory/EnergyAjustViewHolder;->percentage:I

    iget v2, p0, Lcom/konka/factory/EnergyAjustViewHolder;->percentage:I

    if-gez v2, :cond_3

    iput v5, p0, Lcom/konka/factory/EnergyAjustViewHolder;->percentage:I

    :cond_3
    iget-object v2, p0, Lcom/konka/factory/EnergyAjustViewHolder;->text_factory_energy_percent_val:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p0, Lcom/konka/factory/EnergyAjustViewHolder;->percentage:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/EnergyAjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v3, p0, Lcom/konka/factory/EnergyAjustViewHolder;->percentage:I

    int-to-short v3, v3

    invoke-interface {v2, v3}, Lcom/konka/factory/desk/IFactoryDesk;->setEnergyPercent(S)Z

    goto/16 :goto_0

    :sswitch_2
    iget-object v2, p0, Lcom/konka/factory/EnergyAjustViewHolder;->energyActivity:Lcom/konka/factory/MainmenuActivity;

    const/16 v3, 0x9

    invoke-virtual {v2, v3}, Lcom/konka/factory/MainmenuActivity;->returnRoot(I)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_2
        0x15 -> :sswitch_1
        0x16 -> :sswitch_0
        0x52 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x7f0a0069
        :pswitch_1    # com.konka.factory.R.id.linearlayout_factory_energy_energyswitch
        :pswitch_0    # com.konka.factory.R.id.textview_factory_energy_energyswitch
        :pswitch_0    # com.konka.factory.R.id.textview_factory_energy_energyswitch_val
        :pswitch_2    # com.konka.factory.R.id.linearlayout_factory_energy_percent
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x7f0a0069
        :pswitch_4    # com.konka.factory.R.id.linearlayout_factory_energy_energyswitch
        :pswitch_3    # com.konka.factory.R.id.textview_factory_energy_energyswitch
        :pswitch_3    # com.konka.factory.R.id.textview_factory_energy_energyswitch_val
        :pswitch_5    # com.konka.factory.R.id.linearlayout_factory_energy_percent
    .end packed-switch
.end method

.method setenergyswitch(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/konka/factory/EnergyAjustViewHolder;->text_factory_energy_energyswitch_val:Landroid/widget/TextView;

    const v1, 0x7f080044    # com.konka.factory.R.string.str_factory_energy_energyswitch_on

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/factory/EnergyAjustViewHolder;->text_factory_energy_energyswitch_val:Landroid/widget/TextView;

    const v1, 0x7f080045    # com.konka.factory.R.string.str_factory_energy_energyswitch_off

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method
