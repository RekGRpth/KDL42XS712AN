.class public Lcom/konka/factory/TvRootApp;
.super Landroid/app/Application;
.source "TvRootApp.java"


# static fields
.field private static commonSkin:Lcom/mstar/android/tv/TvCommonManager;


# instance fields
.field protected handler:Landroid/os/Handler;

.field private tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/factory/TvRootApp;->commonSkin:Lcom/mstar/android/tv/TvCommonManager;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/factory/TvRootApp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    new-instance v0, Lcom/konka/factory/TvRootApp$1;

    invoke-direct {v0, p0}, Lcom/konka/factory/TvRootApp$1;-><init>(Lcom/konka/factory/TvRootApp;)V

    iput-object v0, p0, Lcom/konka/factory/TvRootApp;->handler:Landroid/os/Handler;

    return-void
.end method

.method public static getCommonSkin()Lcom/mstar/android/tv/TvCommonManager;
    .locals 1

    sget-object v0, Lcom/konka/factory/TvRootApp;->commonSkin:Lcom/mstar/android/tv/TvCommonManager;

    return-object v0
.end method


# virtual methods
.method public getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;
    .locals 1

    iget-object v0, p0, Lcom/konka/factory/TvRootApp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    invoke-static {p0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/factory/TvRootApp;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getInstance()Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v0

    sput-object v0, Lcom/konka/factory/TvRootApp;->commonSkin:Lcom/mstar/android/tv/TvCommonManager;

    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    return-void
.end method

.method public onTerminate()V
    .locals 0

    invoke-super {p0}, Landroid/app/Application;->onTerminate()V

    return-void
.end method
