.class public Lcom/konka/factory/OtherOptionAdjustViewHolder;
.super Ljava/lang/Object;
.source "OtherOptionAdjustViewHolder.java"


# instance fields
.field private audioprescaleval:I

.field private dtvavdelayenable:[Ljava/lang/String;

.field private dtvavdelayindex:I

.field private factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

.field private initialchannel:[Ljava/lang/String;

.field private initialchannelindex:I

.field protected linear_factory_otheroption_mstv_tool:Landroid/widget/LinearLayout;

.field protected linear_factory_otheroption_pq_update:Landroid/widget/LinearLayout;

.field private listener:Landroid/view/View$OnClickListener;

.field private otheroptionActivity:Lcom/konka/factory/MainmenuActivity;

.field private panelswingval:I

.field private poweronmodeindex:I

.field private pq_update_state:Z

.field private restoretodefaultindex:I

.field private restoretodefenable:[Ljava/lang/String;

.field private str_board:Ljava/lang/String;

.field private str_build:Ljava/lang/String;

.field private str_changelist:Ljava/lang/String;

.field private str_date:Ljava/lang/String;

.field private str_main_pq_version:Ljava/lang/String;

.field private str_name:Ljava/lang/String;

.field private str_panel:Ljava/lang/String;

.field private str_sub_pq_version:Ljava/lang/String;

.field private str_version:Ljava/lang/String;

.field private testPatternType:[Ljava/lang/String;

.field private testpartternindex:I

.field protected text_factory_otheroption_3d_selfadaptive_detectlevel_val:Landroid/widget/TextView;

.field protected text_factory_otheroption_audioprescale_val:Landroid/widget/TextView;

.field protected text_factory_otheroption_board_val:Landroid/widget/TextView;

.field protected text_factory_otheroption_build_val:Landroid/widget/TextView;

.field protected text_factory_otheroption_changelist_val:Landroid/widget/TextView;

.field protected text_factory_otheroption_date_val:Landroid/widget/TextView;

.field protected text_factory_otheroption_dtvavdelay_val:Landroid/widget/TextView;

.field protected text_factory_otheroption_initialfunction_val:Landroid/widget/TextView;

.field protected text_factory_otheroption_mainpq_version_val:Landroid/widget/TextView;

.field protected text_factory_otheroption_name_val:Landroid/widget/TextView;

.field protected text_factory_otheroption_panel_val:Landroid/widget/TextView;

.field protected text_factory_otheroption_panelswing_val:Landroid/widget/TextView;

.field protected text_factory_otheroption_pq_updagte_state:Landroid/widget/TextView;

.field protected text_factory_otheroption_restoretodefault_val:Landroid/widget/TextView;

.field protected text_factory_otheroption_subpq_version_val:Landroid/widget/TextView;

.field protected text_factory_otheroption_testpattern_val:Landroid/widget/TextView;

.field protected text_factory_otheroption_uartenable_val:Landroid/widget/TextView;

.field protected text_factory_otheroption_upgrade6m30_val:Landroid/widget/TextView;

.field protected text_factory_otheroption_upgrademain_val:Landroid/widget/TextView;

.field protected text_factory_otheroption_upgrademboot_val:Landroid/widget/TextView;

.field protected text_factory_otheroption_version_val:Landroid/widget/TextView;

.field protected text_factory_otheroption_watchdog_val:Landroid/widget/TextView;

.field private threeDselfadaptivelevel:[Ljava/lang/String;

.field private threeDselfadaptivelevelindex:I

.field private uartenable:[Ljava/lang/String;

.field private uartenableindex:I

.field private upgrade6m30:I

.field private upgradeStatus:[Ljava/lang/String;

.field private upgrademboot:I

.field private watchdogenable:[Ljava/lang/String;

.field private watchdogindex:I


# direct methods
.method public constructor <init>(Lcom/konka/factory/MainmenuActivity;Lcom/konka/factory/desk/IFactoryDesk;)V
    .locals 8
    .param p1    # Lcom/konka/factory/MainmenuActivity;
    .param p2    # Lcom/konka/factory/desk/IFactoryDesk;

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->watchdogindex:I

    iput v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->testpartternindex:I

    iput v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->restoretodefaultindex:I

    iput v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->poweronmodeindex:I

    iput v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->uartenableindex:I

    iput v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->dtvavdelayindex:I

    iput v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->initialchannelindex:I

    iput v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->panelswingval:I

    iput v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->audioprescaleval:I

    iput v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->threeDselfadaptivelevelindex:I

    iput v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->upgrademboot:I

    iput v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->upgrade6m30:I

    iput-boolean v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->pq_update_state:Z

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "Off"

    aput-object v1, v0, v3

    const-string v1, "On"

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->watchdogenable:[Ljava/lang/String;

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Off"

    aput-object v1, v0, v3

    const-string v1, "White"

    aput-object v1, v0, v4

    const-string v1, "Red"

    aput-object v1, v0, v5

    const-string v1, "Green"

    aput-object v1, v0, v6

    const-string v1, "Blue"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "Black"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->testPatternType:[Ljava/lang/String;

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "Error!"

    aput-object v1, v0, v3

    const-string v1, "Sucess!"

    aput-object v1, v0, v4

    const-string v1, "Fail!"

    aput-object v1, v0, v5

    const-string v1, "File Not Found!"

    aput-object v1, v0, v6

    const-string v1, "Please Plug USB!"

    aput-object v1, v0, v7

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->upgradeStatus:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "On"

    aput-object v1, v0, v3

    const-string v1, "Off"

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->dtvavdelayenable:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "On"

    aput-object v1, v0, v3

    const-string v1, "Off"

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->restoretodefenable:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "Off"

    aput-object v1, v0, v3

    const-string v1, "On"

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->uartenable:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "DTV"

    aput-object v1, v0, v3

    const-string v1, "ATV"

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->initialchannel:[Ljava/lang/String;

    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "LOW"

    aput-object v1, v0, v3

    const-string v1, "MIDDLE"

    aput-object v1, v0, v4

    const-string v1, "HIGH"

    aput-object v1, v0, v5

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->threeDselfadaptivelevel:[Ljava/lang/String;

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->str_version:Ljava/lang/String;

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->str_name:Ljava/lang/String;

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->str_build:Ljava/lang/String;

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->str_changelist:Ljava/lang/String;

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->str_board:Ljava/lang/String;

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->str_panel:Ljava/lang/String;

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->str_date:Ljava/lang/String;

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->str_main_pq_version:Ljava/lang/String;

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0}, Ljava/lang/String;-><init>()V

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->str_sub_pq_version:Ljava/lang/String;

    new-instance v0, Lcom/konka/factory/OtherOptionAdjustViewHolder$7;

    invoke-direct {v0, p0}, Lcom/konka/factory/OtherOptionAdjustViewHolder$7;-><init>(Lcom/konka/factory/OtherOptionAdjustViewHolder;)V

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->listener:Landroid/view/View$OnClickListener;

    iput-object p1, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->otheroptionActivity:Lcom/konka/factory/MainmenuActivity;

    iput-object p2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    return-void
.end method

.method static synthetic access$1000(Lcom/konka/factory/OtherOptionAdjustViewHolder;)V
    .locals 0
    .param p0    # Lcom/konka/factory/OtherOptionAdjustViewHolder;

    invoke-direct {p0}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->openLayoutDialog6M30()V

    return-void
.end method

.method static synthetic access$1100(Lcom/konka/factory/OtherOptionAdjustViewHolder;)Lcom/konka/factory/desk/IFactoryDesk;
    .locals 1
    .param p0    # Lcom/konka/factory/OtherOptionAdjustViewHolder;

    iget-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/konka/factory/OtherOptionAdjustViewHolder;)Z
    .locals 1
    .param p0    # Lcom/konka/factory/OtherOptionAdjustViewHolder;

    iget-boolean v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->pq_update_state:Z

    return v0
.end method

.method static synthetic access$1202(Lcom/konka/factory/OtherOptionAdjustViewHolder;Z)Z
    .locals 0
    .param p0    # Lcom/konka/factory/OtherOptionAdjustViewHolder;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->pq_update_state:Z

    return p1
.end method

.method static synthetic access$700(Lcom/konka/factory/OtherOptionAdjustViewHolder;)Lcom/konka/factory/MainmenuActivity;
    .locals 1
    .param p0    # Lcom/konka/factory/OtherOptionAdjustViewHolder;

    iget-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->otheroptionActivity:Lcom/konka/factory/MainmenuActivity;

    return-object v0
.end method

.method static synthetic access$800(Lcom/konka/factory/OtherOptionAdjustViewHolder;)V
    .locals 0
    .param p0    # Lcom/konka/factory/OtherOptionAdjustViewHolder;

    invoke-direct {p0}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->openLayoutDialog()V

    return-void
.end method

.method static synthetic access$900(Lcom/konka/factory/OtherOptionAdjustViewHolder;)V
    .locals 0
    .param p0    # Lcom/konka/factory/OtherOptionAdjustViewHolder;

    invoke-direct {p0}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->openLayoutDialogMain()V

    return-void
.end method

.method private chmodFile(Ljava/io/File;)V
    .locals 4

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "chmod 666 "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "zyl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "command = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "zyl"

    const-string v2, "chmod fail!!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private copyFile(Ljava/io/File;Ljava/io/File;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "OtherOption"

    const-string v2, "~src file not exits~"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->isFile()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "OtherOption"

    const-string v2, "~src file is not a file~"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Ljava/io/File;->canRead()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "OtherOption"

    const-string v2, "~src file can  not read~"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "OtherOption"

    const-string v2, "~dest file not exits~"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {p2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    :cond_3
    invoke-virtual {p2}, Ljava/io/File;->createNewFile()Z

    :cond_4
    invoke-virtual {p2}, Ljava/io/File;->canRead()Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "OtherOption"

    const-string v2, "~dest file can  not read~"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_5
    const-string v1, "OtherOption"

    const-string v2, "~src file OK~"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    new-instance v2, Ljava/io/BufferedInputStream;

    invoke-direct {v2, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    new-instance v4, Ljava/io/BufferedOutputStream;

    invoke-direct {v4, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    const v5, 0x1f4000

    new-array v5, v5, [B

    :goto_1
    invoke-virtual {v2, v5}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_6

    invoke-virtual {v4, v5, v0, v6}, Ljava/io/BufferedOutputStream;->write([BII)V

    goto :goto_1

    :cond_6
    invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->flush()V

    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V

    invoke-virtual {v4}, Ljava/io/BufferedOutputStream;->close()V

    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    const-string v0, "OtherOption"

    const-string v1, "~chmod dest file OK~"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method private openLayoutDialog()V
    .locals 3

    new-instance v0, Lcom/konka/factory/DialogMenu;

    iget-object v1, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->otheroptionActivity:Lcom/konka/factory/MainmenuActivity;

    const v2, 0x7f0a0161    # com.konka.factory.R.id.textview_factory_otheroption_upgrademboot_val

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/DialogMenu;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0}, Lcom/konka/factory/DialogMenu;->show()V

    return-void
.end method

.method private openLayoutDialog6M30()V
    .locals 3

    new-instance v0, Lcom/konka/factory/DialogMenu;

    iget-object v1, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->otheroptionActivity:Lcom/konka/factory/MainmenuActivity;

    const v2, 0x7f0a0167    # com.konka.factory.R.id.textview_factory_otheroption_upgrade6m30_val

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/DialogMenu;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0}, Lcom/konka/factory/DialogMenu;->show()V

    return-void
.end method

.method private openLayoutDialogMain()V
    .locals 3

    new-instance v0, Lcom/konka/factory/DialogMenu;

    iget-object v1, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->otheroptionActivity:Lcom/konka/factory/MainmenuActivity;

    const v2, 0x7f0a0164    # com.konka.factory.R.id.textview_factory_otheroption_upgrademain_val

    invoke-direct {v0, v1, v2}, Lcom/konka/factory/DialogMenu;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0}, Lcom/konka/factory/DialogMenu;->show()V

    return-void
.end method

.method private registerListeners()V
    .locals 2

    iget-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_upgrademboot_val:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_upgrademain_val:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_upgrade6m30_val:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->linear_factory_otheroption_mstv_tool:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->linear_factory_otheroption_pq_update:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->listener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method findView()V
    .locals 2

    iget-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->otheroptionActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0140    # com.konka.factory.R.id.textview_factory_otheroption_version_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_version_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->otheroptionActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0146    # com.konka.factory.R.id.textview_factory_otheroption_build_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_build_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->otheroptionActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0143    # com.konka.factory.R.id.textview_factory_otheroption_name_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_name_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->otheroptionActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0149    # com.konka.factory.R.id.textview_factory_otheroption_changelist_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_changelist_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->otheroptionActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a014c    # com.konka.factory.R.id.textview_factory_otheroption_board_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_board_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->otheroptionActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a014f    # com.konka.factory.R.id.textview_factory_otheroption_panel_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_panel_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->otheroptionActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0152    # com.konka.factory.R.id.textview_factory_otheroption_date_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_date_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->otheroptionActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0155    # com.konka.factory.R.id.textview_factory_otheroption_pq_version_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_mainpq_version_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->otheroptionActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0158    # com.konka.factory.R.id.textview_factory_otheroption_subpq_version_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_subpq_version_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->otheroptionActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a015b    # com.konka.factory.R.id.textview_factory_otheroption_watchdog_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_watchdog_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->otheroptionActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a015e    # com.konka.factory.R.id.textview_factory_otheroption_testpattern_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_testpattern_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->otheroptionActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0161    # com.konka.factory.R.id.textview_factory_otheroption_upgrademboot_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_upgrademboot_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->otheroptionActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0164    # com.konka.factory.R.id.textview_factory_otheroption_upgrademain_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_upgrademain_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->otheroptionActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0167    # com.konka.factory.R.id.textview_factory_otheroption_upgrade6m30_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_upgrade6m30_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->otheroptionActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a016a    # com.konka.factory.R.id.textview_factory_otheroption_restoretodefault_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_restoretodefault_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->otheroptionActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a016d    # com.konka.factory.R.id.textview_factory_otheroption_uartenable_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_uartenable_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->otheroptionActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0170    # com.konka.factory.R.id.textview_factory_otheroption_dtvavdelay_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_dtvavdelay_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->otheroptionActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0173    # com.konka.factory.R.id.textview_factory_otheroption_initialfunction_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_initialfunction_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->otheroptionActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0176    # com.konka.factory.R.id.textview_factory_otheroption_panelswing_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_panelswing_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->otheroptionActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0179    # com.konka.factory.R.id.textview_factory_otheroption_audioprescale_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_audioprescale_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->otheroptionActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a017c    # com.konka.factory.R.id.textview_factory_otheroption_3d_selfadaptive_detectlevel_val

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_3d_selfadaptive_detectlevel_val:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->otheroptionActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a0181    # com.konka.factory.R.id.textview_factory_otheroption_pq_update_state

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_pq_updagte_state:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->otheroptionActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a017d    # com.konka.factory.R.id.linearlayout_factory_otheroption_mstv_tool

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->linear_factory_otheroption_mstv_tool:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->otheroptionActivity:Lcom/konka/factory/MainmenuActivity;

    const v1, 0x7f0a017f    # com.konka.factory.R.id.linearlayout_factory_otheroption_pq_update

    invoke-virtual {v0, v1}, Lcom/konka/factory/MainmenuActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->linear_factory_otheroption_pq_update:Landroid/widget/LinearLayout;

    return-void
.end method

.method public onCreate()Z
    .locals 8

    const/4 v5, 0x1

    const/4 v6, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentDtvRoute()I

    move-result v4

    int-to-short v1, v4

    sget-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvDemodType;->E_DEMOD_DVB_C:Lcom/mstar/android/tvapi/dtv/vo/DtvDemodType;

    packed-switch v1, :pswitch_data_0

    :goto_0
    new-instance v2, Lcom/mstar/android/tvapi/dtv/vo/DtvDemodVersion;

    invoke-direct {v2}, Lcom/mstar/android/tvapi/dtv/vo/DtvDemodVersion;-><init>()V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v4

    check-cast v4, Lcom/mstar/android/tvapi/impl/PlayerImpl;

    invoke-virtual {v4, v0}, Lcom/mstar/android/tvapi/impl/PlayerImpl;->getDTVDemodVersion(Lcom/mstar/android/tvapi/dtv/vo/DtvDemodType;)Lcom/mstar/android/tvapi/dtv/vo/DtvDemodVersion;

    move-result-object v2

    new-instance v4, Ljava/lang/String;

    iget-object v7, v2, Lcom/mstar/android/tvapi/dtv/vo/DtvDemodVersion;->name:[C

    invoke-direct {v4, v7}, Ljava/lang/String;-><init>([C)V

    iput-object v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->str_name:Ljava/lang/String;

    new-instance v4, Ljava/lang/String;

    iget-object v7, v2, Lcom/mstar/android/tvapi/dtv/vo/DtvDemodVersion;->version:[C

    invoke-direct {v4, v7}, Ljava/lang/String;-><init>([C)V

    iput-object v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->str_version:Ljava/lang/String;

    new-instance v4, Ljava/lang/String;

    iget-object v7, v2, Lcom/mstar/android/tvapi/dtv/vo/DtvDemodVersion;->build:[C

    invoke-direct {v4, v7}, Ljava/lang/String;-><init>([C)V

    iput-object v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->str_build:Ljava/lang/String;

    new-instance v4, Ljava/lang/String;

    iget-object v7, v2, Lcom/mstar/android/tvapi/dtv/vo/DtvDemodVersion;->changelist:[C

    invoke-direct {v4, v7}, Ljava/lang/String;-><init>([C)V

    iput-object v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->str_changelist:Ljava/lang/String;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v4}, Lcom/konka/factory/desk/IFactoryDesk;->getBoardType()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->str_board:Ljava/lang/String;

    iget-object v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v4}, Lcom/konka/factory/desk/IFactoryDesk;->getPanelType()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->str_panel:Ljava/lang/String;

    const-string v4, "2011-10-23 12:00"

    iput-object v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->str_date:Ljava/lang/String;

    iget-object v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v4, v6}, Lcom/konka/factory/desk/IFactoryDesk;->getPQVersion(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->str_main_pq_version:Ljava/lang/String;

    iget-object v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v4, v5}, Lcom/konka/factory/desk/IFactoryDesk;->getPQVersion(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->str_sub_pq_version:Ljava/lang/String;

    iget-object v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v4}, Lcom/konka/factory/desk/IFactoryDesk;->getWatchDogMode()S

    move-result v4

    iput v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->watchdogindex:I

    iget-object v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v4}, Lcom/konka/factory/desk/IFactoryDesk;->getTestPattern()I

    move-result v4

    iput v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->testpartternindex:I

    iput v6, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->restoretodefaultindex:I

    iget-object v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v4}, Lcom/konka/factory/desk/IFactoryDesk;->getUartOnOff()Z

    move-result v4

    if-eqz v4, :cond_1

    move v4, v5

    :goto_2
    iput v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->uartenableindex:I

    iget-object v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v4}, Lcom/konka/factory/desk/IFactoryDesk;->getDtvAvAbnormalDelay()Z

    move-result v4

    if-eqz v4, :cond_0

    move v6, v5

    :cond_0
    iput v6, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->dtvavdelayindex:I

    iget-object v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v4}, Lcom/konka/factory/desk/IFactoryDesk;->getFactoryPreSetFeature()I

    move-result v4

    iput v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->initialchannelindex:I

    iget-object v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v4}, Lcom/konka/factory/desk/IFactoryDesk;->getPanelSwing()S

    move-result v4

    iput v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->panelswingval:I

    iget-object v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v4}, Lcom/konka/factory/desk/IFactoryDesk;->getAudioPrescale()S

    move-result v4

    iput v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->audioprescaleval:I

    iget-object v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v4}, Lcom/konka/factory/desk/IFactoryDesk;->get3DSelfAdaptiveLevel()I

    move-result v4

    iput v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->threeDselfadaptivelevelindex:I

    iget-object v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_watchdog_val:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->watchdogenable:[Ljava/lang/String;

    iget v7, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->watchdogindex:I

    aget-object v6, v6, v7

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_version_val:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->str_version:Ljava/lang/String;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_name_val:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->str_name:Ljava/lang/String;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_build_val:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->str_build:Ljava/lang/String;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_changelist_val:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->str_changelist:Ljava/lang/String;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_board_val:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->str_board:Ljava/lang/String;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_panel_val:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->str_panel:Ljava/lang/String;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_mainpq_version_val:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->str_main_pq_version:Ljava/lang/String;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_subpq_version_val:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->str_sub_pq_version:Ljava/lang/String;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_panelswing_val:Landroid/widget/TextView;

    iget v6, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->panelswingval:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_audioprescale_val:Landroid/widget/TextView;

    iget v6, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->audioprescaleval:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_3d_selfadaptive_detectlevel_val:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->threeDselfadaptivelevel:[Ljava/lang/String;

    iget v7, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->threeDselfadaptivelevelindex:I

    aget-object v6, v6, v7

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_uartenable_val:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->uartenable:[Ljava/lang/String;

    iget v7, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->uartenableindex:I

    aget-object v6, v6, v7

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->registerListeners()V

    return v5

    :pswitch_0
    :try_start_1
    sget-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvDemodType;->E_DEMOD_DTMB:Lcom/mstar/android/tvapi/dtv/vo/DtvDemodType;

    goto/16 :goto_0

    :pswitch_1
    sget-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvDemodType;->E_DEMOD_DVB_C:Lcom/mstar/android/tvapi/dtv/vo/DtvDemodType;

    goto/16 :goto_0

    :pswitch_2
    sget-object v0, Lcom/mstar/android/tvapi/dtv/vo/DtvDemodType;->E_DEMOD_DVB_T:Lcom/mstar/android/tvapi/dtv/vo/DtvDemodType;
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_1

    :cond_1
    move v4, v6

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v7, 0x64

    const/4 v6, 0x5

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->otheroptionActivity:Lcom/konka/factory/MainmenuActivity;

    invoke-virtual {v2}, Lcom/konka/factory/MainmenuActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v1

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :sswitch_0
    sparse-switch v1, :sswitch_data_1

    goto :goto_0

    :sswitch_1
    iget v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->watchdogindex:I

    if-eq v2, v3, :cond_0

    iget v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->watchdogindex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->watchdogindex:I

    :goto_1
    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_watchdog_val:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->watchdogenable:[Ljava/lang/String;

    iget v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->watchdogindex:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->watchdogindex:I

    int-to-short v3, v3

    invoke-interface {v2, v3}, Lcom/konka/factory/desk/IFactoryDesk;->setWatchDogMode(S)Z

    goto :goto_0

    :cond_0
    iput v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->watchdogindex:I

    goto :goto_1

    :sswitch_2
    iget v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->testpartternindex:I

    if-eq v2, v6, :cond_1

    iget v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->testpartternindex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->testpartternindex:I

    :goto_2
    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_testpattern_val:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->testPatternType:[Ljava/lang/String;

    iget v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->testpartternindex:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->testpartternindex:I

    invoke-interface {v2, v3}, Lcom/konka/factory/desk/IFactoryDesk;->setTestPattern(I)Z

    goto :goto_0

    :cond_1
    iput v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->testpartternindex:I

    goto :goto_2

    :sswitch_3
    iget v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->restoretodefaultindex:I

    if-eq v2, v3, :cond_2

    iget v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->restoretodefaultindex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->restoretodefaultindex:I

    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v2}, Lcom/konka/factory/desk/IFactoryDesk;->restoreToDefault()Z

    :goto_3
    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_restoretodefault_val:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->restoretodefenable:[Ljava/lang/String;

    iget v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->restoretodefaultindex:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    iput v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->restoretodefaultindex:I

    goto :goto_3

    :sswitch_4
    iget v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->uartenableindex:I

    if-nez v2, :cond_3

    iput v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->uartenableindex:I

    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v2, v3}, Lcom/konka/factory/desk/IFactoryDesk;->setUartOnOff(Z)Z

    :goto_4
    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_uartenable_val:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->uartenable:[Ljava/lang/String;

    iget v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->uartenableindex:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    iput v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->uartenableindex:I

    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v2, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setUartOnOff(Z)Z

    goto :goto_4

    :sswitch_5
    iget v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->dtvavdelayindex:I

    if-eq v2, v3, :cond_4

    iget v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->dtvavdelayindex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->dtvavdelayindex:I

    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v2, v3}, Lcom/konka/factory/desk/IFactoryDesk;->setDtvAvAbnormalDelay(Z)Z

    :goto_5
    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_dtvavdelay_val:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->dtvavdelayenable:[Ljava/lang/String;

    iget v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->dtvavdelayindex:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_4
    iput v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->dtvavdelayindex:I

    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v2, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setDtvAvAbnormalDelay(Z)Z

    goto :goto_5

    :sswitch_6
    iget v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->initialchannelindex:I

    if-eq v2, v3, :cond_5

    iget v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->initialchannelindex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->initialchannelindex:I

    :goto_6
    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_initialfunction_val:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->initialchannel:[Ljava/lang/String;

    iget v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->initialchannelindex:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->initialchannelindex:I

    invoke-interface {v2, v3}, Lcom/konka/factory/desk/IFactoryDesk;->setFactoryPreSetFeature(I)Z

    goto/16 :goto_0

    :cond_5
    iput v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->initialchannelindex:I

    goto :goto_6

    :sswitch_7
    iget v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->panelswingval:I

    if-eq v2, v7, :cond_6

    iget v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->panelswingval:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->panelswingval:I

    :cond_6
    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_panelswing_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->panelswingval:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->panelswingval:I

    int-to-short v3, v3

    invoke-interface {v2, v3}, Lcom/konka/factory/desk/IFactoryDesk;->setPanelSwing(S)Z

    goto/16 :goto_0

    :sswitch_8
    iget v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->audioprescaleval:I

    if-eq v2, v7, :cond_7

    iget v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->audioprescaleval:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->audioprescaleval:I

    :cond_7
    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_audioprescale_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->audioprescaleval:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->audioprescaleval:I

    int-to-short v3, v3

    invoke-interface {v2, v3}, Lcom/konka/factory/desk/IFactoryDesk;->setAudioPrescale(S)Z

    goto/16 :goto_0

    :sswitch_9
    iget v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->threeDselfadaptivelevelindex:I

    if-eq v2, v5, :cond_8

    iget v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->threeDselfadaptivelevelindex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->threeDselfadaptivelevelindex:I

    :goto_7
    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_3d_selfadaptive_detectlevel_val:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->threeDselfadaptivelevel:[Ljava/lang/String;

    iget v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->threeDselfadaptivelevelindex:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->threeDselfadaptivelevelindex:I

    invoke-interface {v2, v3}, Lcom/konka/factory/desk/IFactoryDesk;->set3DSelfAdaptiveLevel(I)Z

    goto/16 :goto_0

    :cond_8
    iput v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->threeDselfadaptivelevelindex:I

    goto :goto_7

    :sswitch_a
    sparse-switch v1, :sswitch_data_2

    goto/16 :goto_0

    :sswitch_b
    iget v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->watchdogindex:I

    if-eqz v2, :cond_9

    iget v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->watchdogindex:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->watchdogindex:I

    :goto_8
    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_watchdog_val:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->watchdogenable:[Ljava/lang/String;

    iget v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->watchdogindex:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->watchdogindex:I

    int-to-short v3, v3

    invoke-interface {v2, v3}, Lcom/konka/factory/desk/IFactoryDesk;->setWatchDogMode(S)Z

    goto/16 :goto_0

    :cond_9
    iput v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->watchdogindex:I

    goto :goto_8

    :sswitch_c
    iget v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->testpartternindex:I

    if-eqz v2, :cond_a

    iget v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->testpartternindex:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->testpartternindex:I

    :goto_9
    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_testpattern_val:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->testPatternType:[Ljava/lang/String;

    iget v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->testpartternindex:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->testpartternindex:I

    invoke-interface {v2, v3}, Lcom/konka/factory/desk/IFactoryDesk;->setTestPattern(I)Z

    goto/16 :goto_0

    :cond_a
    iput v6, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->testpartternindex:I

    goto :goto_9

    :sswitch_d
    iget v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->restoretodefaultindex:I

    if-eqz v2, :cond_b

    iget v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->restoretodefaultindex:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->restoretodefaultindex:I

    :goto_a
    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_restoretodefault_val:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->restoretodefenable:[Ljava/lang/String;

    iget v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->restoretodefaultindex:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_b
    iput v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->restoretodefaultindex:I

    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v2}, Lcom/konka/factory/desk/IFactoryDesk;->restoreToDefault()Z

    goto :goto_a

    :sswitch_e
    iget v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->uartenableindex:I

    if-ne v2, v3, :cond_c

    iput v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->uartenableindex:I

    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v2, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setUartOnOff(Z)Z

    :goto_b
    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_uartenable_val:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->uartenable:[Ljava/lang/String;

    iget v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->uartenableindex:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_c
    iput v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->uartenableindex:I

    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v2, v3}, Lcom/konka/factory/desk/IFactoryDesk;->setUartOnOff(Z)Z

    goto :goto_b

    :sswitch_f
    iget v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->dtvavdelayindex:I

    if-eqz v2, :cond_d

    iget v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->dtvavdelayindex:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->dtvavdelayindex:I

    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v2, v4}, Lcom/konka/factory/desk/IFactoryDesk;->setDtvAvAbnormalDelay(Z)Z

    :goto_c
    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_dtvavdelay_val:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->dtvavdelayenable:[Ljava/lang/String;

    iget v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->dtvavdelayindex:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_d
    iput v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->dtvavdelayindex:I

    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v2, v3}, Lcom/konka/factory/desk/IFactoryDesk;->setDtvAvAbnormalDelay(Z)Z

    goto :goto_c

    :sswitch_10
    iget v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->initialchannelindex:I

    if-eqz v2, :cond_e

    iget v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->initialchannelindex:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->initialchannelindex:I

    :goto_d
    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_initialfunction_val:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->initialchannel:[Ljava/lang/String;

    iget v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->initialchannelindex:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->initialchannelindex:I

    invoke-interface {v2, v3}, Lcom/konka/factory/desk/IFactoryDesk;->setFactoryPreSetFeature(I)Z

    goto/16 :goto_0

    :cond_e
    iput v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->initialchannelindex:I

    goto :goto_d

    :sswitch_11
    iget v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->panelswingval:I

    if-eqz v2, :cond_f

    iget v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->panelswingval:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->panelswingval:I

    :cond_f
    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_panelswing_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->panelswingval:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->panelswingval:I

    int-to-short v3, v3

    invoke-interface {v2, v3}, Lcom/konka/factory/desk/IFactoryDesk;->setPanelSwing(S)Z

    goto/16 :goto_0

    :sswitch_12
    iget v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->audioprescaleval:I

    if-eqz v2, :cond_10

    iget v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->audioprescaleval:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->audioprescaleval:I

    :cond_10
    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_audioprescale_val:Landroid/widget/TextView;

    iget v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->audioprescaleval:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    iget v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->audioprescaleval:I

    int-to-short v3, v3

    invoke-interface {v2, v3}, Lcom/konka/factory/desk/IFactoryDesk;->setAudioPrescale(S)Z

    goto/16 :goto_0

    :sswitch_13
    iget v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->threeDselfadaptivelevelindex:I

    if-eqz v2, :cond_11

    iget v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->threeDselfadaptivelevelindex:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->threeDselfadaptivelevelindex:I

    :goto_e
    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_3d_selfadaptive_detectlevel_val:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->threeDselfadaptivelevel:[Ljava/lang/String;

    iget v4, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->threeDselfadaptivelevelindex:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_11
    iput v5, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->threeDselfadaptivelevelindex:I

    goto :goto_e

    :sswitch_14
    sparse-switch v1, :sswitch_data_3

    goto/16 :goto_0

    :sswitch_15
    invoke-direct {p0}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->openLayoutDialog()V

    goto/16 :goto_0

    :sswitch_16
    invoke-direct {p0}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->openLayoutDialogMain()V

    goto/16 :goto_0

    :sswitch_17
    invoke-direct {p0}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->openLayoutDialog6M30()V

    goto/16 :goto_0

    :sswitch_18
    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_pq_updagte_state:Landroid/widget/TextView;

    const-string v3, "Please Click!"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->otheroptionActivity:Lcom/konka/factory/MainmenuActivity;

    const/16 v3, 0x9

    invoke-virtual {v2, v3}, Lcom/konka/factory/MainmenuActivity;->returnRoot(I)V

    iget-object v2, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->otheroptionActivity:Lcom/konka/factory/MainmenuActivity;

    const/16 v3, 0x13

    invoke-virtual {v2, v3}, Lcom/konka/factory/MainmenuActivity;->returnRoot(I)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_18
        0x15 -> :sswitch_a
        0x16 -> :sswitch_0
        0x42 -> :sswitch_14
        0x52 -> :sswitch_18
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x7f0a0159 -> :sswitch_1    # com.konka.factory.R.id.linearlayout_factory_otheroption_watchdog
        0x7f0a015c -> :sswitch_2    # com.konka.factory.R.id.linearlayout_factory_otheroption_testpattern
        0x7f0a0168 -> :sswitch_3    # com.konka.factory.R.id.linearlayout_factory_otheroption_restoretodefault
        0x7f0a016b -> :sswitch_4    # com.konka.factory.R.id.linearlayout_factory_otheroption_uartenable
        0x7f0a016e -> :sswitch_5    # com.konka.factory.R.id.linearlayout_factory_otheroption_dtvavdelay
        0x7f0a0171 -> :sswitch_6    # com.konka.factory.R.id.linearlayout_factory_otheroption_initialfunction
        0x7f0a0174 -> :sswitch_7    # com.konka.factory.R.id.linearlayout_factory_otheroption_panelswing
        0x7f0a0177 -> :sswitch_8    # com.konka.factory.R.id.linearlayout_factory_otheroption_audioprescale
        0x7f0a017a -> :sswitch_9    # com.konka.factory.R.id.linearlayout_factory_otheroption_3d_selfadaptive_detectlevel
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x7f0a0159 -> :sswitch_b    # com.konka.factory.R.id.linearlayout_factory_otheroption_watchdog
        0x7f0a015c -> :sswitch_c    # com.konka.factory.R.id.linearlayout_factory_otheroption_testpattern
        0x7f0a0168 -> :sswitch_d    # com.konka.factory.R.id.linearlayout_factory_otheroption_restoretodefault
        0x7f0a016b -> :sswitch_e    # com.konka.factory.R.id.linearlayout_factory_otheroption_uartenable
        0x7f0a016e -> :sswitch_f    # com.konka.factory.R.id.linearlayout_factory_otheroption_dtvavdelay
        0x7f0a0171 -> :sswitch_10    # com.konka.factory.R.id.linearlayout_factory_otheroption_initialfunction
        0x7f0a0174 -> :sswitch_11    # com.konka.factory.R.id.linearlayout_factory_otheroption_panelswing
        0x7f0a0177 -> :sswitch_12    # com.konka.factory.R.id.linearlayout_factory_otheroption_audioprescale
        0x7f0a017a -> :sswitch_13    # com.konka.factory.R.id.linearlayout_factory_otheroption_3d_selfadaptive_detectlevel
    .end sparse-switch

    :sswitch_data_3
    .sparse-switch
        0x7f0a015f -> :sswitch_15    # com.konka.factory.R.id.linearlayout_factory_otheroption_upgrademboot
        0x7f0a0162 -> :sswitch_16    # com.konka.factory.R.id.linearlayout_factory_otheroption_upgrademain
        0x7f0a0165 -> :sswitch_17    # com.konka.factory.R.id.linearlayout_factory_otheroption_upgrade6m30
    .end sparse-switch
.end method

.method public updatePqFile()V
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2}, Ljava/lang/String;-><init>()V

    const-string v2, "/Customer/DLC/DLC.ini"

    const-string v3, "OtherOption"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "....1....destPath is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".........."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/io/File;

    const-string v4, "/mnt/usb/sda1/pqbin/DLC.ini"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_0
    invoke-direct {p0, v3, v4}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->copyFile(Ljava/io/File;Ljava/io/File;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    :cond_0
    :goto_0
    const-string v2, "/Customer/ColorMatrix/ColorMatrix.ini"

    const-string v3, "OtherOption"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "....2....destPath is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".........."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/io/File;

    const-string v4, "/mnt/usb/sda1/pqbin/ColorMatrix.ini"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_1
    invoke-direct {p0, v3, v4}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->copyFile(Ljava/io/File;Ljava/io/File;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    :cond_1
    :goto_1
    const-string v2, "/Customer/pq/Bandwidth_RegTable.bin"

    const-string v3, "OtherOption"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "....3....destPath is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".........."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/io/File;

    const-string v4, "/mnt/usb/sda1/pqbin/Bandwidth_RegTable.bin"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_2
    invoke-direct {p0, v3, v4}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->copyFile(Ljava/io/File;Ljava/io/File;)Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    :cond_2
    :goto_2
    const-string v2, "/Customer/pq/Main.bin"

    const-string v3, "OtherOption"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "....4....destPath is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".........."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/io/File;

    const-string v4, "/mnt/usb/sda1/pqbin/Main.bin"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_3
    invoke-direct {p0, v3, v4}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->copyFile(Ljava/io/File;Ljava/io/File;)Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    move-result v2

    if-eqz v2, :cond_3

    move v0, v1

    :cond_3
    :goto_3
    const-string v2, "/Customer/pq/Main_Text.bin"

    const-string v3, "OtherOption"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "....5....destPath is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".........."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/io/File;

    const-string v4, "/mnt/usb/sda1/pqbin/Main_Text.bin"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_4
    invoke-direct {p0, v3, v4}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->copyFile(Ljava/io/File;Ljava/io/File;)Z
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    move-result v2

    if-eqz v2, :cond_4

    move v0, v1

    :cond_4
    :goto_4
    const-string v2, "/Customer/pq/Main_Ex.bin"

    const-string v3, "OtherOption"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "....6....destPath is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".........."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/io/File;

    const-string v4, "/mnt/usb/sda1/pqbin/Main_Ex.bin"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_5
    invoke-direct {p0, v3, v4}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->copyFile(Ljava/io/File;Ljava/io/File;)Z
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5

    move-result v2

    if-eqz v2, :cond_5

    move v0, v1

    :cond_5
    :goto_5
    const-string v2, "/Customer/pq/Main_Ex_Text.bin"

    const-string v3, "OtherOption"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "....7....destPath is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".........."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/io/File;

    const-string v4, "/mnt/usb/sda1/pqbin/Main_Ex_Text.bin"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_6
    invoke-direct {p0, v3, v4}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->copyFile(Ljava/io/File;Ljava/io/File;)Z
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_6

    move-result v2

    if-eqz v2, :cond_6

    move v0, v1

    :cond_6
    :goto_6
    const-string v2, "/Customer/pq/Sub.bin"

    const-string v3, "OtherOption"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "....8....destPath is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".........."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/io/File;

    const-string v4, "/mnt/usb/sda1/pqbin/Sub.bin"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_7
    invoke-direct {p0, v3, v4}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->copyFile(Ljava/io/File;Ljava/io/File;)Z
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_7

    move-result v2

    if-eqz v2, :cond_7

    move v0, v1

    :cond_7
    :goto_7
    const-string v2, "/Customer/pq/Sub_Text.bin"

    const-string v3, "OtherOption"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "....9....destPath is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".........."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/io/File;

    const-string v4, "/mnt/usb/sda1/pqbin/Sub_Text.bin"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_8
    invoke-direct {p0, v3, v4}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->copyFile(Ljava/io/File;Ljava/io/File;)Z
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_8

    move-result v2

    if-eqz v2, :cond_8

    move v0, v1

    :cond_8
    :goto_8
    const-string v2, "/Customer/pq/Sub_Ex.bin"

    const-string v3, "OtherOption"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "....10....destPath is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".........."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/io/File;

    const-string v4, "/mnt/usb/sda1/pqbin/Sub_Ex.bin"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_9
    invoke-direct {p0, v3, v4}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->copyFile(Ljava/io/File;Ljava/io/File;)Z
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_9

    move-result v2

    if-eqz v2, :cond_9

    move v0, v1

    :cond_9
    :goto_9
    const-string v2, "/Customer/pq/Sub_Ex_Text.bin"

    const-string v3, "OtherOption"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "....11....destPath is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".........."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/io/File;

    const-string v4, "/mnt/usb/sda1/pqbin/Sub_Ex_Text.bin"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_a
    invoke-direct {p0, v3, v4}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->copyFile(Ljava/io/File;Ljava/io/File;)Z
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_a

    move-result v2

    if-eqz v2, :cond_a

    move v0, v1

    :cond_a
    :goto_a
    const-string v2, "/Customer/pq/gamma0.ini"

    const-string v3, "OtherOption"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "....12....destPath is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".........."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/io/File;

    const-string v4, "/mnt/usb/sda1/pqbin/gamma0.ini"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_b
    invoke-direct {p0, v3, v4}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->copyFile(Ljava/io/File;Ljava/io/File;)Z

    move-result v2

    if-eqz v2, :cond_b

    move v0, v1

    :cond_b
    invoke-direct {p0, v4}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->chmodFile(Ljava/io/File;)V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_b

    :goto_b
    const-string v2, "/customercfg/panel/Main_Color.bin"

    const-string v3, "OtherOption"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "....13....destPath is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".........."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/io/File;

    const-string v4, "/mnt/usb/sda1/pqbin/Main_Color.bin"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_c
    invoke-direct {p0, v3, v4}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->copyFile(Ljava/io/File;Ljava/io/File;)Z

    move-result v2

    if-eqz v2, :cond_c

    move v0, v1

    :cond_c
    invoke-direct {p0, v4}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->chmodFile(Ljava/io/File;)V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_c

    :goto_c
    const-string v2, "/customercfg/panel/Sub_Color.bin"

    const-string v3, "OtherOption"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "....14....destPath is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".........."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/io/File;

    const-string v4, "/mnt/usb/sda1/pqbin/Sub_Color.bin"

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_d
    invoke-direct {p0, v3, v4}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->copyFile(Ljava/io/File;Ljava/io/File;)Z

    move-result v2

    if-eqz v2, :cond_d

    move v0, v1

    :cond_d
    invoke-direct {p0, v4}, Lcom/konka/factory/OtherOptionAdjustViewHolder;->chmodFile(Ljava/io/File;)V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_d

    :goto_d
    iget-object v1, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->factoryManager:Lcom/konka/factory/desk/IFactoryDesk;

    invoke-interface {v1}, Lcom/konka/factory/desk/IFactoryDesk;->updatePqIniFiles()V

    const-string v1, "OtherOption"

    const-string v2, ".......updatePqIniFiles OK!!!!!!!!!!.........."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_pq_updagte_state:Landroid/widget/TextView;

    const-string v1, "OK!!!"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_e
    return-void

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0

    :catch_1
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_1

    :catch_2
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_2

    :catch_3
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    :catch_4
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_4

    :catch_5
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_5

    :catch_6
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_6

    :catch_7
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_7

    :catch_8
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_8

    :catch_9
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_9

    :catch_a
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_a

    :catch_b
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_b

    :catch_c
    move-exception v2

    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_c

    :catch_d
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_d

    :cond_e
    iget-object v0, p0, Lcom/konka/factory/OtherOptionAdjustViewHolder;->text_factory_otheroption_pq_updagte_state:Landroid/widget/TextView;

    const-string v1, "No File On sdcard!"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_e
.end method
