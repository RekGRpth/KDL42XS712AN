.class final Lcom/konka/factory/USBUpgradeThread$2;
.super Ljava/lang/Object;
.source "USBUpgradeThread.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/factory/USBUpgradeThread;->Upgrade6M30()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    invoke-static {}, Lcom/konka/factory/DialogMenu;->getHandler()Landroid/os/Handler;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/konka/factory/DialogMenu;->getHandler()Landroid/os/Handler;

    move-result-object v2

    sget v3, Lcom/konka/factory/USBUpgradeThread;->UPGRATE_START:I

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    invoke-static {}, Lcom/konka/factory/DialogMenu;->Upgrade6M30Fun()I

    move-result v1

    const-wide/16 v2, 0x3e8

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    sget-object v2, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;->E_UPGRADE_SUCCESS:Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;

    invoke-virtual {v2}, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;->ordinal()I

    move-result v2

    if-ne v1, v2, :cond_1

    invoke-static {}, Lcom/konka/factory/DialogMenu;->getHandler()Landroid/os/Handler;

    move-result-object v2

    sget v3, Lcom/konka/factory/USBUpgradeThread;->UPGRATE_END_SUCCESS:I

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_0
    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;->E_UPGRADE_FILE_NOT_FOUND:Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;

    invoke-virtual {v2}, Lcom/konka/factory/DialogMenu$EnumUpgradeStatus;->ordinal()I

    move-result v2

    if-ne v1, v2, :cond_2

    invoke-static {}, Lcom/konka/factory/DialogMenu;->getHandler()Landroid/os/Handler;

    move-result-object v2

    sget v3, Lcom/konka/factory/USBUpgradeThread;->UPGRATE_END_FILE_NOT_FOUND:I

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    :cond_2
    invoke-static {}, Lcom/konka/factory/DialogMenu;->getHandler()Landroid/os/Handler;

    move-result-object v2

    sget v3, Lcom/konka/factory/USBUpgradeThread;->UPGRATE_END_FAIL:I

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1
.end method
