.class public final Lcom/android/systemui/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/systemui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final DeadZone:[I

.field public static final KeyButtonView:[I

.field public static final NotificationLinearLayout:[I

.field public static final NotificationRowLayout:[I

.field public static final RecentsPanelView:[I

.field public static final ToggleSlider:[I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/systemui/R$styleable;->DeadZone:[I

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/systemui/R$styleable;->KeyButtonView:[I

    new-array v0, v3, [I

    const v1, 0x7f010004    # com.android.systemui.R.attr.insetLeft

    aput v1, v0, v2

    sput-object v0, Lcom/android/systemui/R$styleable;->NotificationLinearLayout:[I

    new-array v0, v3, [I

    const v1, 0x7f010005    # com.android.systemui.R.attr.rowHeight

    aput v1, v0, v2

    sput-object v0, Lcom/android/systemui/R$styleable;->NotificationRowLayout:[I

    new-array v0, v3, [I

    const v1, 0x7f010006    # com.android.systemui.R.attr.recentItemLayout

    aput v1, v0, v2

    sput-object v0, Lcom/android/systemui/R$styleable;->RecentsPanelView:[I

    new-array v0, v3, [I

    const v1, 0x7f010003    # com.android.systemui.R.attr.text

    aput v1, v0, v2

    sput-object v0, Lcom/android/systemui/R$styleable;->ToggleSlider:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f010007    # com.android.systemui.R.attr.minSize
        0x7f010008    # com.android.systemui.R.attr.maxSize
        0x7f010009    # com.android.systemui.R.attr.holdTime
        0x7f01000a    # com.android.systemui.R.attr.decayTime
        0x7f01000b    # com.android.systemui.R.attr.orientation
    .end array-data

    :array_1
    .array-data 4
        0x7f010000    # com.android.systemui.R.attr.keyCode
        0x7f010001    # com.android.systemui.R.attr.keyRepeat
        0x7f010002    # com.android.systemui.R.attr.glowBackground
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
