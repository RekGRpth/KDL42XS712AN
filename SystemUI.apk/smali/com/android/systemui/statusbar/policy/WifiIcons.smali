.class Lcom/android/systemui/statusbar/policy/WifiIcons;
.super Ljava/lang/Object;
.source "WifiIcons.java"


# static fields
.field static final QS_WIFI_SIGNAL_STRENGTH:[[I

.field static final WIFI_LEVEL_COUNT:I

.field static final WIFI_SIGNAL_STRENGTH:[[I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x5

    new-array v0, v5, [[I

    new-array v1, v2, [I

    fill-array-data v1, :array_0

    aput-object v1, v0, v3

    new-array v1, v2, [I

    fill-array-data v1, :array_1

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/systemui/statusbar/policy/WifiIcons;->WIFI_SIGNAL_STRENGTH:[[I

    new-array v0, v5, [[I

    new-array v1, v2, [I

    fill-array-data v1, :array_2

    aput-object v1, v0, v3

    new-array v1, v2, [I

    fill-array-data v1, :array_3

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/systemui/statusbar/policy/WifiIcons;->QS_WIFI_SIGNAL_STRENGTH:[[I

    sget-object v0, Lcom/android/systemui/statusbar/policy/WifiIcons;->WIFI_SIGNAL_STRENGTH:[[I

    aget-object v0, v0, v3

    array-length v0, v0

    sput v0, Lcom/android/systemui/statusbar/policy/WifiIcons;->WIFI_LEVEL_COUNT:I

    return-void

    :array_0
    .array-data 4
        0x7f020145    # com.android.systemui.R.drawable.stat_sys_wifi_signal_0
        0x7f020146    # com.android.systemui.R.drawable.stat_sys_wifi_signal_1
        0x7f020148    # com.android.systemui.R.drawable.stat_sys_wifi_signal_2
        0x7f02014a    # com.android.systemui.R.drawable.stat_sys_wifi_signal_3
        0x7f02014c    # com.android.systemui.R.drawable.stat_sys_wifi_signal_4
    .end array-data

    :array_1
    .array-data 4
        0x7f020145    # com.android.systemui.R.drawable.stat_sys_wifi_signal_0
        0x7f020147    # com.android.systemui.R.drawable.stat_sys_wifi_signal_1_fully
        0x7f020149    # com.android.systemui.R.drawable.stat_sys_wifi_signal_2_fully
        0x7f02014b    # com.android.systemui.R.drawable.stat_sys_wifi_signal_3_fully
        0x7f02014d    # com.android.systemui.R.drawable.stat_sys_wifi_signal_4_fully
    .end array-data

    :array_2
    .array-data 4
        0x7f020077    # com.android.systemui.R.drawable.ic_qs_wifi_0
        0x7f020078    # com.android.systemui.R.drawable.ic_qs_wifi_1
        0x7f020079    # com.android.systemui.R.drawable.ic_qs_wifi_2
        0x7f02007a    # com.android.systemui.R.drawable.ic_qs_wifi_3
        0x7f02007b    # com.android.systemui.R.drawable.ic_qs_wifi_4
    .end array-data

    :array_3
    .array-data 4
        0x7f020077    # com.android.systemui.R.drawable.ic_qs_wifi_0
        0x7f02007c    # com.android.systemui.R.drawable.ic_qs_wifi_full_1
        0x7f02007d    # com.android.systemui.R.drawable.ic_qs_wifi_full_2
        0x7f02007e    # com.android.systemui.R.drawable.ic_qs_wifi_full_3
        0x7f02007f    # com.android.systemui.R.drawable.ic_qs_wifi_full_4
    .end array-data
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
