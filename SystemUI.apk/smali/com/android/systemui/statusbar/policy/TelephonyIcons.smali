.class Lcom/android/systemui/statusbar/policy/TelephonyIcons;
.super Ljava/lang/Object;
.source "TelephonyIcons.java"


# static fields
.field static final DATA_1X:[[I

.field static final DATA_3G:[[I

.field static final DATA_4G:[[I

.field static final DATA_E:[[I

.field static final DATA_G:[[I

.field static final DATA_H:[[I

.field static final DATA_SIGNAL_STRENGTH:[[I

.field static final QS_TELEPHONY_SIGNAL_STRENGTH:[[I

.field static final TELEPHONY_SIGNAL_STRENGTH:[[I

.field static final TELEPHONY_SIGNAL_STRENGTH_ROAMING:[[I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x5

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x4

    new-array v0, v5, [[I

    new-array v1, v6, [I

    fill-array-data v1, :array_0

    aput-object v1, v0, v3

    new-array v1, v6, [I

    fill-array-data v1, :array_1

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->TELEPHONY_SIGNAL_STRENGTH:[[I

    new-array v0, v5, [[I

    new-array v1, v6, [I

    fill-array-data v1, :array_2

    aput-object v1, v0, v3

    new-array v1, v6, [I

    fill-array-data v1, :array_3

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->QS_TELEPHONY_SIGNAL_STRENGTH:[[I

    new-array v0, v5, [[I

    new-array v1, v6, [I

    fill-array-data v1, :array_4

    aput-object v1, v0, v3

    new-array v1, v6, [I

    fill-array-data v1, :array_5

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->TELEPHONY_SIGNAL_STRENGTH_ROAMING:[[I

    sget-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->TELEPHONY_SIGNAL_STRENGTH:[[I

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->DATA_SIGNAL_STRENGTH:[[I

    new-array v0, v5, [[I

    new-array v1, v2, [I

    fill-array-data v1, :array_6

    aput-object v1, v0, v3

    new-array v1, v2, [I

    fill-array-data v1, :array_7

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->DATA_G:[[I

    new-array v0, v5, [[I

    new-array v1, v2, [I

    fill-array-data v1, :array_8

    aput-object v1, v0, v3

    new-array v1, v2, [I

    fill-array-data v1, :array_9

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->DATA_3G:[[I

    new-array v0, v5, [[I

    new-array v1, v2, [I

    fill-array-data v1, :array_a

    aput-object v1, v0, v3

    new-array v1, v2, [I

    fill-array-data v1, :array_b

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->DATA_E:[[I

    new-array v0, v5, [[I

    new-array v1, v2, [I

    fill-array-data v1, :array_c

    aput-object v1, v0, v3

    new-array v1, v2, [I

    fill-array-data v1, :array_d

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->DATA_H:[[I

    new-array v0, v5, [[I

    new-array v1, v2, [I

    fill-array-data v1, :array_e

    aput-object v1, v0, v3

    new-array v1, v2, [I

    fill-array-data v1, :array_f

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->DATA_1X:[[I

    new-array v0, v5, [[I

    new-array v1, v2, [I

    fill-array-data v1, :array_10

    aput-object v1, v0, v3

    new-array v1, v2, [I

    fill-array-data v1, :array_11

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/systemui/statusbar/policy/TelephonyIcons;->DATA_4G:[[I

    return-void

    :array_0
    .array-data 4
        0x7f020130    # com.android.systemui.R.drawable.stat_sys_signal_0
        0x7f020132    # com.android.systemui.R.drawable.stat_sys_signal_1
        0x7f020134    # com.android.systemui.R.drawable.stat_sys_signal_2
        0x7f020136    # com.android.systemui.R.drawable.stat_sys_signal_3
        0x7f020138    # com.android.systemui.R.drawable.stat_sys_signal_4
    .end array-data

    :array_1
    .array-data 4
        0x7f020131    # com.android.systemui.R.drawable.stat_sys_signal_0_fully
        0x7f020133    # com.android.systemui.R.drawable.stat_sys_signal_1_fully
        0x7f020135    # com.android.systemui.R.drawable.stat_sys_signal_2_fully
        0x7f020137    # com.android.systemui.R.drawable.stat_sys_signal_3_fully
        0x7f020139    # com.android.systemui.R.drawable.stat_sys_signal_4_fully
    .end array-data

    :array_2
    .array-data 4
        0x7f020059    # com.android.systemui.R.drawable.ic_qs_signal_0
        0x7f02005a    # com.android.systemui.R.drawable.ic_qs_signal_1
        0x7f02005c    # com.android.systemui.R.drawable.ic_qs_signal_2
        0x7f02005d    # com.android.systemui.R.drawable.ic_qs_signal_3
        0x7f02005f    # com.android.systemui.R.drawable.ic_qs_signal_4
    .end array-data

    :array_3
    .array-data 4
        0x7f020062    # com.android.systemui.R.drawable.ic_qs_signal_full_0
        0x7f020063    # com.android.systemui.R.drawable.ic_qs_signal_full_1
        0x7f020065    # com.android.systemui.R.drawable.ic_qs_signal_full_2
        0x7f020066    # com.android.systemui.R.drawable.ic_qs_signal_full_3
        0x7f020068    # com.android.systemui.R.drawable.ic_qs_signal_full_4
    .end array-data

    :array_4
    .array-data 4
        0x7f020130    # com.android.systemui.R.drawable.stat_sys_signal_0
        0x7f020132    # com.android.systemui.R.drawable.stat_sys_signal_1
        0x7f020134    # com.android.systemui.R.drawable.stat_sys_signal_2
        0x7f020136    # com.android.systemui.R.drawable.stat_sys_signal_3
        0x7f020138    # com.android.systemui.R.drawable.stat_sys_signal_4
    .end array-data

    :array_5
    .array-data 4
        0x7f020131    # com.android.systemui.R.drawable.stat_sys_signal_0_fully
        0x7f020133    # com.android.systemui.R.drawable.stat_sys_signal_1_fully
        0x7f020135    # com.android.systemui.R.drawable.stat_sys_signal_2_fully
        0x7f020137    # com.android.systemui.R.drawable.stat_sys_signal_3_fully
        0x7f020139    # com.android.systemui.R.drawable.stat_sys_signal_4_fully
    .end array-data

    :array_6
    .array-data 4
        0x7f020114    # com.android.systemui.R.drawable.stat_sys_data_connected_g
        0x7f020114    # com.android.systemui.R.drawable.stat_sys_data_connected_g
        0x7f020114    # com.android.systemui.R.drawable.stat_sys_data_connected_g
        0x7f020114    # com.android.systemui.R.drawable.stat_sys_data_connected_g
    .end array-data

    :array_7
    .array-data 4
        0x7f02011b    # com.android.systemui.R.drawable.stat_sys_data_fully_connected_g
        0x7f02011b    # com.android.systemui.R.drawable.stat_sys_data_fully_connected_g
        0x7f02011b    # com.android.systemui.R.drawable.stat_sys_data_fully_connected_g
        0x7f02011b    # com.android.systemui.R.drawable.stat_sys_data_fully_connected_g
    .end array-data

    :array_8
    .array-data 4
        0x7f020111    # com.android.systemui.R.drawable.stat_sys_data_connected_3g
        0x7f020111    # com.android.systemui.R.drawable.stat_sys_data_connected_3g
        0x7f020111    # com.android.systemui.R.drawable.stat_sys_data_connected_3g
        0x7f020111    # com.android.systemui.R.drawable.stat_sys_data_connected_3g
    .end array-data

    :array_9
    .array-data 4
        0x7f020118    # com.android.systemui.R.drawable.stat_sys_data_fully_connected_3g
        0x7f020118    # com.android.systemui.R.drawable.stat_sys_data_fully_connected_3g
        0x7f020118    # com.android.systemui.R.drawable.stat_sys_data_fully_connected_3g
        0x7f020118    # com.android.systemui.R.drawable.stat_sys_data_fully_connected_3g
    .end array-data

    :array_a
    .array-data 4
        0x7f020113    # com.android.systemui.R.drawable.stat_sys_data_connected_e
        0x7f020113    # com.android.systemui.R.drawable.stat_sys_data_connected_e
        0x7f020113    # com.android.systemui.R.drawable.stat_sys_data_connected_e
        0x7f020113    # com.android.systemui.R.drawable.stat_sys_data_connected_e
    .end array-data

    :array_b
    .array-data 4
        0x7f02011a    # com.android.systemui.R.drawable.stat_sys_data_fully_connected_e
        0x7f02011a    # com.android.systemui.R.drawable.stat_sys_data_fully_connected_e
        0x7f02011a    # com.android.systemui.R.drawable.stat_sys_data_fully_connected_e
        0x7f02011a    # com.android.systemui.R.drawable.stat_sys_data_fully_connected_e
    .end array-data

    :array_c
    .array-data 4
        0x7f020115    # com.android.systemui.R.drawable.stat_sys_data_connected_h
        0x7f020115    # com.android.systemui.R.drawable.stat_sys_data_connected_h
        0x7f020115    # com.android.systemui.R.drawable.stat_sys_data_connected_h
        0x7f020115    # com.android.systemui.R.drawable.stat_sys_data_connected_h
    .end array-data

    :array_d
    .array-data 4
        0x7f02011c    # com.android.systemui.R.drawable.stat_sys_data_fully_connected_h
        0x7f02011c    # com.android.systemui.R.drawable.stat_sys_data_fully_connected_h
        0x7f02011c    # com.android.systemui.R.drawable.stat_sys_data_fully_connected_h
        0x7f02011c    # com.android.systemui.R.drawable.stat_sys_data_fully_connected_h
    .end array-data

    :array_e
    .array-data 4
        0x7f020110    # com.android.systemui.R.drawable.stat_sys_data_connected_1x
        0x7f020110    # com.android.systemui.R.drawable.stat_sys_data_connected_1x
        0x7f020110    # com.android.systemui.R.drawable.stat_sys_data_connected_1x
        0x7f020110    # com.android.systemui.R.drawable.stat_sys_data_connected_1x
    .end array-data

    :array_f
    .array-data 4
        0x7f020117    # com.android.systemui.R.drawable.stat_sys_data_fully_connected_1x
        0x7f020117    # com.android.systemui.R.drawable.stat_sys_data_fully_connected_1x
        0x7f020117    # com.android.systemui.R.drawable.stat_sys_data_fully_connected_1x
        0x7f020117    # com.android.systemui.R.drawable.stat_sys_data_fully_connected_1x
    .end array-data

    :array_10
    .array-data 4
        0x7f020112    # com.android.systemui.R.drawable.stat_sys_data_connected_4g
        0x7f020112    # com.android.systemui.R.drawable.stat_sys_data_connected_4g
        0x7f020112    # com.android.systemui.R.drawable.stat_sys_data_connected_4g
        0x7f020112    # com.android.systemui.R.drawable.stat_sys_data_connected_4g
    .end array-data

    :array_11
    .array-data 4
        0x7f020119    # com.android.systemui.R.drawable.stat_sys_data_fully_connected_4g
        0x7f020119    # com.android.systemui.R.drawable.stat_sys_data_fully_connected_4g
        0x7f020119    # com.android.systemui.R.drawable.stat_sys_data_fully_connected_4g
        0x7f020119    # com.android.systemui.R.drawable.stat_sys_data_fully_connected_4g
    .end array-data
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
