.class public Lcom/android/systemui/statusbar/policy/AccessibilityContentDescriptions;
.super Ljava/lang/Object;
.source "AccessibilityContentDescriptions.java"


# static fields
.field static final DATA_CONNECTION_STRENGTH:[I

.field static final PHONE_SIGNAL_STRENGTH:[I

.field static final WIFI_CONNECTION_STRENGTH:[I

.field static final WIMAX_CONNECTION_STRENGTH:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x5

    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/systemui/statusbar/policy/AccessibilityContentDescriptions;->PHONE_SIGNAL_STRENGTH:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/systemui/statusbar/policy/AccessibilityContentDescriptions;->DATA_CONNECTION_STRENGTH:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/android/systemui/statusbar/policy/AccessibilityContentDescriptions;->WIFI_CONNECTION_STRENGTH:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/android/systemui/statusbar/policy/AccessibilityContentDescriptions;->WIMAX_CONNECTION_STRENGTH:[I

    return-void

    :array_0
    .array-data 4
        0x7f0b0049    # com.android.systemui.R.string.accessibility_no_phone
        0x7f0b004a    # com.android.systemui.R.string.accessibility_phone_one_bar
        0x7f0b004b    # com.android.systemui.R.string.accessibility_phone_two_bars
        0x7f0b004c    # com.android.systemui.R.string.accessibility_phone_three_bars
        0x7f0b004d    # com.android.systemui.R.string.accessibility_phone_signal_full
    .end array-data

    :array_1
    .array-data 4
        0x7f0b004e    # com.android.systemui.R.string.accessibility_no_data
        0x7f0b004f    # com.android.systemui.R.string.accessibility_data_one_bar
        0x7f0b0050    # com.android.systemui.R.string.accessibility_data_two_bars
        0x7f0b0051    # com.android.systemui.R.string.accessibility_data_three_bars
        0x7f0b0052    # com.android.systemui.R.string.accessibility_data_signal_full
    .end array-data

    :array_2
    .array-data 4
        0x7f0b0054    # com.android.systemui.R.string.accessibility_no_wifi
        0x7f0b0055    # com.android.systemui.R.string.accessibility_wifi_one_bar
        0x7f0b0056    # com.android.systemui.R.string.accessibility_wifi_two_bars
        0x7f0b0057    # com.android.systemui.R.string.accessibility_wifi_three_bars
        0x7f0b0058    # com.android.systemui.R.string.accessibility_wifi_signal_full
    .end array-data

    :array_3
    .array-data 4
        0x7f0b0059    # com.android.systemui.R.string.accessibility_no_wimax
        0x7f0b005a    # com.android.systemui.R.string.accessibility_wimax_one_bar
        0x7f0b005b    # com.android.systemui.R.string.accessibility_wimax_two_bars
        0x7f0b005c    # com.android.systemui.R.string.accessibility_wimax_three_bars
        0x7f0b005d    # com.android.systemui.R.string.accessibility_wimax_signal_full
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
