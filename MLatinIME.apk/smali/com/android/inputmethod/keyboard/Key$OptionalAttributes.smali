.class final Lcom/android/inputmethod/keyboard/Key$OptionalAttributes;
.super Ljava/lang/Object;
.source "Key.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/keyboard/Key;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "OptionalAttributes"
.end annotation


# instance fields
.field public final mAltCode:I

.field public final mDisabledIconId:I

.field public final mOutputText:Ljava/lang/String;

.field public final mPreviewIconId:I

.field public final mVisualInsetsLeft:I

.field public final mVisualInsetsRight:I


# direct methods
.method public constructor <init>(Ljava/lang/String;IIIII)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/inputmethod/keyboard/Key$OptionalAttributes;->mOutputText:Ljava/lang/String;

    iput p2, p0, Lcom/android/inputmethod/keyboard/Key$OptionalAttributes;->mAltCode:I

    iput p3, p0, Lcom/android/inputmethod/keyboard/Key$OptionalAttributes;->mDisabledIconId:I

    iput p4, p0, Lcom/android/inputmethod/keyboard/Key$OptionalAttributes;->mPreviewIconId:I

    iput p5, p0, Lcom/android/inputmethod/keyboard/Key$OptionalAttributes;->mVisualInsetsLeft:I

    iput p6, p0, Lcom/android/inputmethod/keyboard/Key$OptionalAttributes;->mVisualInsetsRight:I

    return-void
.end method
