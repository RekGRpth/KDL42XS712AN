.class public final Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;
.super Ljava/lang/Object;
.source "GestureStroke.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/keyboard/internal/GestureStroke;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "GestureStrokeParams"
.end annotation


# static fields
.field public static final DEFAULT:Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;

.field public static final FOR_TEST:Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;


# instance fields
.field public final mDetectFastMoveSpeedThreshold:F

.field public final mDynamicDistanceThresholdFrom:F

.field public final mDynamicDistanceThresholdTo:F

.field public final mDynamicThresholdDecayDuration:I

.field public final mDynamicTimeThresholdFrom:I

.field public final mDynamicTimeThresholdTo:I

.field public final mRecognitionMinimumTime:I

.field public final mRecognitionSpeedThreshold:F

.field public final mSamplingMinimumDistance:F

.field public final mStaticTimeThresholdAfterFastTyping:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;

    invoke-direct {v0}, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;-><init>()V

    sput-object v0, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->FOR_TEST:Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;

    sget-object v0, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->FOR_TEST:Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;

    sput-object v0, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->DEFAULT:Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x15e

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mStaticTimeThresholdAfterFastTyping:I

    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mDetectFastMoveSpeedThreshold:F

    const/16 v0, 0x1c2

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mDynamicThresholdDecayDuration:I

    const/16 v0, 0x12c

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mDynamicTimeThresholdFrom:I

    const/16 v0, 0x14

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mDynamicTimeThresholdTo:I

    const/high16 v0, 0x40c00000    # 6.0f

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mDynamicDistanceThresholdFrom:F

    const v0, 0x3eb33333    # 0.35f

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mDynamicDistanceThresholdTo:F

    const v0, 0x3e2aaaab

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mSamplingMinimumDistance:F

    const/16 v0, 0x64

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mRecognitionMinimumTime:I

    const/high16 v0, 0x40b00000    # 5.5f

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mRecognitionSpeedThreshold:F

    return-void
.end method

.method public constructor <init>(Landroid/content/res/TypedArray;)V
    .locals 2
    .param p1    # Landroid/content/res/TypedArray;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x14

    sget-object v1, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->DEFAULT:Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;

    iget v1, v1, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mStaticTimeThresholdAfterFastTyping:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mStaticTimeThresholdAfterFastTyping:I

    const/16 v0, 0x15

    sget-object v1, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->DEFAULT:Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;

    iget v1, v1, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mDetectFastMoveSpeedThreshold:F

    invoke-static {p1, v0, v1}, Lcom/android/inputmethod/latin/ResourceUtils;->getFraction(Landroid/content/res/TypedArray;IF)F

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mDetectFastMoveSpeedThreshold:F

    const/16 v0, 0x16

    sget-object v1, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->DEFAULT:Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;

    iget v1, v1, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mDynamicThresholdDecayDuration:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mDynamicThresholdDecayDuration:I

    const/16 v0, 0x17

    sget-object v1, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->DEFAULT:Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;

    iget v1, v1, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mDynamicTimeThresholdFrom:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mDynamicTimeThresholdFrom:I

    const/16 v0, 0x18

    sget-object v1, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->DEFAULT:Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;

    iget v1, v1, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mDynamicTimeThresholdTo:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mDynamicTimeThresholdTo:I

    const/16 v0, 0x19

    sget-object v1, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->DEFAULT:Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;

    iget v1, v1, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mDynamicDistanceThresholdFrom:F

    invoke-static {p1, v0, v1}, Lcom/android/inputmethod/latin/ResourceUtils;->getFraction(Landroid/content/res/TypedArray;IF)F

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mDynamicDistanceThresholdFrom:F

    const/16 v0, 0x1a

    sget-object v1, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->DEFAULT:Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;

    iget v1, v1, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mDynamicDistanceThresholdTo:F

    invoke-static {p1, v0, v1}, Lcom/android/inputmethod/latin/ResourceUtils;->getFraction(Landroid/content/res/TypedArray;IF)F

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mDynamicDistanceThresholdTo:F

    const/16 v0, 0x1b

    sget-object v1, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->DEFAULT:Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;

    iget v1, v1, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mSamplingMinimumDistance:F

    invoke-static {p1, v0, v1}, Lcom/android/inputmethod/latin/ResourceUtils;->getFraction(Landroid/content/res/TypedArray;IF)F

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mSamplingMinimumDistance:F

    const/16 v0, 0x1c

    sget-object v1, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->DEFAULT:Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;

    iget v1, v1, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mRecognitionMinimumTime:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mRecognitionMinimumTime:I

    const/16 v0, 0x1d

    sget-object v1, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->DEFAULT:Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;

    iget v1, v1, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mRecognitionSpeedThreshold:F

    invoke-static {p1, v0, v1}, Lcom/android/inputmethod/latin/ResourceUtils;->getFraction(Landroid/content/res/TypedArray;IF)F

    move-result v0

    iput v0, p0, Lcom/android/inputmethod/keyboard/internal/GestureStroke$GestureStrokeParams;->mRecognitionSpeedThreshold:F

    return-void
.end method
