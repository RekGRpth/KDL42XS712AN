.class public final Lcom/android/inputmethod/latin/Settings;
.super Lcom/android/inputmethodcommon/InputMethodSettingsFragment;
.source "Settings.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# instance fields
.field private mAutoCorrectionThresholdPreference:Landroid/preference/ListPreference;

.field private mBigramPrediction:Landroid/preference/CheckBoxPreference;

.field private mDebugSettingsPreference:Landroid/preference/Preference;

.field private mKeyPreviewPopupDismissDelay:Landroid/preference/ListPreference;

.field private mKeypressSoundVolumeSettingsPref:Landroid/preference/PreferenceScreen;

.field private mKeypressSoundVolumeSettingsTextView:Landroid/widget/TextView;

.field private mKeypressVibrationDurationSettingsPref:Landroid/preference/PreferenceScreen;

.field private mKeypressVibrationDurationSettingsTextView:Landroid/widget/TextView;

.field private mShowCorrectionSuggestionsPreference:Landroid/preference/ListPreference;

.field private mVoicePreference:Landroid/preference/ListPreference;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/inputmethodcommon/InputMethodSettingsFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/android/inputmethod/latin/Settings;)V
    .locals 0
    .param p0    # Lcom/android/inputmethod/latin/Settings;

    invoke-direct {p0}, Lcom/android/inputmethod/latin/Settings;->showKeypressVibrationDurationSettingsDialog()V

    return-void
.end method

.method static synthetic access$100(Lcom/android/inputmethod/latin/Settings;)V
    .locals 0
    .param p0    # Lcom/android/inputmethod/latin/Settings;

    invoke-direct {p0}, Lcom/android/inputmethod/latin/Settings;->showKeypressSoundVolumeSettingDialog()V

    return-void
.end method

.method static synthetic access$200(Lcom/android/inputmethod/latin/Settings;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/android/inputmethod/latin/Settings;

    iget-object v0, p0, Lcom/android/inputmethod/latin/Settings;->mKeypressVibrationDurationSettingsTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/inputmethod/latin/Settings;Landroid/content/SharedPreferences;Landroid/content/res/Resources;)V
    .locals 0
    .param p0    # Lcom/android/inputmethod/latin/Settings;
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Landroid/content/res/Resources;

    invoke-direct {p0, p1, p2}, Lcom/android/inputmethod/latin/Settings;->updateKeypressVibrationDurationSettingsSummary(Landroid/content/SharedPreferences;Landroid/content/res/Resources;)V

    return-void
.end method

.method static synthetic access$400(Lcom/android/inputmethod/latin/Settings;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/android/inputmethod/latin/Settings;

    iget-object v0, p0, Lcom/android/inputmethod/latin/Settings;->mKeypressSoundVolumeSettingsTextView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/inputmethod/latin/Settings;Landroid/content/SharedPreferences;Landroid/content/res/Resources;)V
    .locals 0
    .param p0    # Lcom/android/inputmethod/latin/Settings;
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Landroid/content/res/Resources;

    invoke-direct {p0, p1, p2}, Lcom/android/inputmethod/latin/Settings;->updateKeypressSoundVolumeSummary(Landroid/content/SharedPreferences;Landroid/content/res/Resources;)V

    return-void
.end method

.method private ensureConsistencyOfAutoCorrectionSettings()V
    .locals 4

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/Settings;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b000d    # com.android.inputmethod.latin.R.string.auto_correction_threshold_mode_index_off

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/android/inputmethod/latin/Settings;->mAutoCorrectionThresholdPreference:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/android/inputmethod/latin/Settings;->mBigramPrediction:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {v3, v2}, Lcom/android/inputmethod/latin/Settings;->setPreferenceEnabled(Landroid/preference/Preference;Z)V

    return-void

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private refreshEnablingsOfKeypressSoundAndVibrationSettings(Landroid/content/SharedPreferences;Landroid/content/res/Resources;)V
    .locals 5
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Landroid/content/res/Resources;

    iget-object v3, p0, Lcom/android/inputmethod/latin/Settings;->mKeypressVibrationDurationSettingsPref:Landroid/preference/PreferenceScreen;

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/Settings;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/android/inputmethod/latin/VibratorUtils;->getInstance(Landroid/content/Context;)Lcom/android/inputmethod/latin/VibratorUtils;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/inputmethod/latin/VibratorUtils;->hasVibrator()Z

    move-result v0

    const-string v3, "vibrate_on"

    const v4, 0x7f090008    # com.android.inputmethod.latin.R.bool.config_default_vibration_enabled

    invoke-virtual {p2, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    invoke-interface {p1, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iget-object v4, p0, Lcom/android/inputmethod/latin/Settings;->mKeypressVibrationDurationSettingsPref:Landroid/preference/PreferenceScreen;

    if-eqz v0, :cond_2

    if-eqz v2, :cond_2

    const/4 v3, 0x1

    :goto_0
    invoke-static {v4, v3}, Lcom/android/inputmethod/latin/Settings;->setPreferenceEnabled(Landroid/preference/Preference;Z)V

    :cond_0
    iget-object v3, p0, Lcom/android/inputmethod/latin/Settings;->mKeypressSoundVolumeSettingsPref:Landroid/preference/PreferenceScreen;

    if-eqz v3, :cond_1

    const-string v3, "sound_on"

    const v4, 0x7f090007    # com.android.inputmethod.latin.R.bool.config_default_sound_enabled

    invoke-virtual {p2, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    invoke-interface {p1, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iget-object v3, p0, Lcom/android/inputmethod/latin/Settings;->mKeypressSoundVolumeSettingsPref:Landroid/preference/PreferenceScreen;

    invoke-static {v3, v1}, Lcom/android/inputmethod/latin/Settings;->setPreferenceEnabled(Landroid/preference/Preference;Z)V

    :cond_1
    return-void

    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private static setPreferenceEnabled(Landroid/preference/Preference;Z)V
    .locals 0
    .param p0    # Landroid/preference/Preference;
    .param p1    # Z

    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, Landroid/preference/Preference;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method private showKeypressSoundVolumeSettingDialog()V
    .locals 11

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/Settings;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v8, "audio"

    invoke-virtual {v2, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/Settings;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v8

    invoke-virtual {v8}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v6

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v8, 0x7f0b00bb    # com.android.inputmethod.latin.R.string.prefs_keypress_sound_volume_settings

    invoke-virtual {v1, v8}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v8, 0x104000a    # android.R.string.ok

    new-instance v9, Lcom/android/inputmethod/latin/Settings$6;

    invoke-direct {v9, p0, v6, v4}, Lcom/android/inputmethod/latin/Settings$6;-><init>(Lcom/android/inputmethod/latin/Settings;Landroid/content/SharedPreferences;Landroid/content/res/Resources;)V

    invoke-virtual {v1, v8, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/high16 v8, 0x1040000    # android.R.string.cancel

    new-instance v9, Lcom/android/inputmethod/latin/Settings$7;

    invoke-direct {v9, p0}, Lcom/android/inputmethod/latin/Settings$7;-><init>(Lcom/android/inputmethod/latin/Settings;)V

    invoke-virtual {v1, v8, v9}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v8

    const v9, 0x7f04000a    # com.android.inputmethod.latin.R.layout.sound_effect_volume_dialog

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    invoke-static {v6, v4}, Lcom/android/inputmethod/latin/SettingsValues;->getCurrentKeypressSoundVolume(Landroid/content/SharedPreferences;Landroid/content/res/Resources;)F

    move-result v8

    const/high16 v9, 0x42c80000    # 100.0f

    mul-float/2addr v8, v9

    float-to-int v3, v8

    const v8, 0x7f08004a    # com.android.inputmethod.latin.R.id.sound_effect_volume_value

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lcom/android/inputmethod/latin/Settings;->mKeypressSoundVolumeSettingsTextView:Landroid/widget/TextView;

    const v8, 0x7f08004b    # com.android.inputmethod.latin.R.id.sound_effect_volume_bar

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/SeekBar;

    new-instance v8, Lcom/android/inputmethod/latin/Settings$8;

    invoke-direct {v8, p0, v0}, Lcom/android/inputmethod/latin/Settings$8;-><init>(Lcom/android/inputmethod/latin/Settings;Landroid/media/AudioManager;)V

    invoke-virtual {v5, v8}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    invoke-virtual {v5, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v8, p0, Lcom/android/inputmethod/latin/Settings;->mKeypressSoundVolumeSettingsTextView:Landroid/widget/TextView;

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v7}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private showKeypressVibrationDurationSettingsDialog()V
    .locals 10

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/Settings;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v7

    invoke-virtual {v7}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/Settings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v7, 0x7f0b00ba    # com.android.inputmethod.latin.R.string.prefs_keypress_vibration_duration_settings

    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v7, 0x104000a    # android.R.string.ok

    new-instance v8, Lcom/android/inputmethod/latin/Settings$3;

    invoke-direct {v8, p0, v5, v3}, Lcom/android/inputmethod/latin/Settings$3;-><init>(Lcom/android/inputmethod/latin/Settings;Landroid/content/SharedPreferences;Landroid/content/res/Resources;)V

    invoke-virtual {v0, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const/high16 v7, 0x1040000    # android.R.string.cancel

    new-instance v8, Lcom/android/inputmethod/latin/Settings$4;

    invoke-direct {v8, p0}, Lcom/android/inputmethod/latin/Settings$4;-><init>(Lcom/android/inputmethod/latin/Settings;)V

    invoke-virtual {v0, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    const v8, 0x7f040010    # com.android.inputmethod.latin.R.layout.vibration_settings_dialog

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/Settings;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v7

    invoke-virtual {v7}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v7

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/Settings;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/android/inputmethod/latin/SettingsValues;->getCurrentVibrationDuration(Landroid/content/SharedPreferences;Landroid/content/res/Resources;)I

    move-result v2

    const v7, 0x7f08004d    # com.android.inputmethod.latin.R.id.vibration_value

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/android/inputmethod/latin/Settings;->mKeypressVibrationDurationSettingsTextView:Landroid/widget/TextView;

    const v7, 0x7f08004e    # com.android.inputmethod.latin.R.id.vibration_settings

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/SeekBar;

    new-instance v7, Lcom/android/inputmethod/latin/Settings$5;

    invoke-direct {v7, p0, v1}, Lcom/android/inputmethod/latin/Settings$5;-><init>(Lcom/android/inputmethod/latin/Settings;Landroid/content/Context;)V

    invoke-virtual {v4, v7}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    invoke-virtual {v4, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v7, p0, Lcom/android/inputmethod/latin/Settings;->mKeypressVibrationDurationSettingsTextView:Landroid/widget/TextView;

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v6}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private updateCustomInputStylesSummary()V
    .locals 11

    const-string v10, "custom_input_styles"

    invoke-virtual {p0, v10}, Lcom/android/inputmethod/latin/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/PreferenceScreen;

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/Settings;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v10

    invoke-virtual {v10}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/Settings;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/android/inputmethod/latin/SettingsValues;->getPrefAdditionalSubtypes(Landroid/content/SharedPreferences;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/android/inputmethod/latin/AdditionalSubtype;->createAdditionalSubtypesArray(Ljava/lang/String;)[Landroid/view/inputmethod/InputMethodSubtype;

    move-result-object v9

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move-object v0, v9

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v8, v0, v2

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->length()I

    move-result v10

    if-lez v10, :cond_0

    const-string v10, ", "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-static {v8, v6}, Lcom/android/inputmethod/latin/SubtypeLocale;->getSubtypeDisplayName(Landroid/view/inputmethod/InputMethodSubtype;Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v1, v7}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private updateKeyPreviewPopupDelaySummary()V
    .locals 3

    iget-object v1, p0, Lcom/android/inputmethod/latin/Settings;->mKeyPreviewPopupDismissDelay:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v2, v0

    if-gtz v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v1}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v2

    aget-object v2, v0, v2

    invoke-virtual {v1, v2}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateKeypressSoundVolumeSummary(Landroid/content/SharedPreferences;Landroid/content/res/Resources;)V
    .locals 3
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/android/inputmethod/latin/Settings;->mKeypressSoundVolumeSettingsPref:Landroid/preference/PreferenceScreen;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/Settings;->mKeypressSoundVolumeSettingsPref:Landroid/preference/PreferenceScreen;

    invoke-static {p1, p2}, Lcom/android/inputmethod/latin/SettingsValues;->getCurrentKeypressSoundVolume(Landroid/content/SharedPreferences;Landroid/content/res/Resources;)F

    move-result v1

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method private updateKeypressVibrationDurationSettingsSummary(Landroid/content/SharedPreferences;Landroid/content/res/Resources;)V
    .locals 3
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/android/inputmethod/latin/Settings;->mKeypressVibrationDurationSettingsPref:Landroid/preference/PreferenceScreen;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/inputmethod/latin/Settings;->mKeypressVibrationDurationSettingsPref:Landroid/preference/PreferenceScreen;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1, p2}, Lcom/android/inputmethod/latin/SettingsValues;->getCurrentVibrationDuration(Landroid/content/SharedPreferences;Landroid/content/res/Resources;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f0b0027    # com.android.inputmethod.latin.R.string.settings_ms

    invoke-virtual {p2, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method private updateShowCorrectionSuggestionsSummary()V
    .locals 4

    iget-object v0, p0, Lcom/android/inputmethod/latin/Settings;->mShowCorrectionSuggestionsPreference:Landroid/preference/ListPreference;

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/Settings;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0002    # com.android.inputmethod.latin.R.array.prefs_suggestion_visibilities

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/inputmethod/latin/Settings;->mShowCorrectionSuggestionsPreference:Landroid/preference/ListPreference;

    iget-object v3, p0, Lcom/android/inputmethod/latin/Settings;->mShowCorrectionSuggestionsPreference:Landroid/preference/ListPreference;

    invoke-virtual {v3}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private updateVoiceModeSummary()V
    .locals 4

    iget-object v0, p0, Lcom/android/inputmethod/latin/Settings;->mVoicePreference:Landroid/preference/ListPreference;

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/Settings;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0007    # com.android.inputmethod.latin.R.array.voice_input_modes_summary

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/inputmethod/latin/Settings;->mVoicePreference:Landroid/preference/ListPreference;

    iget-object v3, p0, Lcom/android/inputmethod/latin/Settings;->mVoicePreference:Landroid/preference/ListPreference;

    invoke-virtual {v3}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 26
    .param p1    # Landroid/os/Bundle;

    invoke-super/range {p0 .. p1}, Lcom/android/inputmethodcommon/InputMethodSettingsFragment;->onCreate(Landroid/os/Bundle;)V

    const v22, 0x7f0b008c    # com.android.inputmethod.latin.R.string.language_selection_title

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/Settings;->setInputMethodSettingsCategoryTitle(I)V

    const v22, 0x7f0b009d    # com.android.inputmethod.latin.R.string.select_language

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/Settings;->setSubtypeEnablerTitle(I)V

    const v22, 0x7f060054    # com.android.inputmethod.latin.R.xml.prefs

    move-object/from16 v0, p0

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/Settings;->addPreferencesFromResource(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/android/inputmethod/latin/Settings;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    invoke-virtual/range {p0 .. p0}, Lcom/android/inputmethod/latin/Settings;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Lcom/android/inputmethod/latin/SubtypeLocale;->init(Landroid/content/Context;)V

    const-string v22, "voice_mode"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v22

    check-cast v22, Landroid/preference/ListPreference;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/inputmethod/latin/Settings;->mVoicePreference:Landroid/preference/ListPreference;

    const-string v22, "show_suggestions_setting"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v22

    check-cast v22, Landroid/preference/ListPreference;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/inputmethod/latin/Settings;->mShowCorrectionSuggestionsPreference:Landroid/preference/ListPreference;

    invoke-virtual/range {p0 .. p0}, Lcom/android/inputmethod/latin/Settings;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    const-string v22, "auto_correction_threshold"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v22

    check-cast v22, Landroid/preference/ListPreference;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/inputmethod/latin/Settings;->mAutoCorrectionThresholdPreference:Landroid/preference/ListPreference;

    const-string v22, "next_word_prediction"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v22

    check-cast v22, Landroid/preference/CheckBoxPreference;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/inputmethod/latin/Settings;->mBigramPrediction:Landroid/preference/CheckBoxPreference;

    invoke-direct/range {p0 .. p0}, Lcom/android/inputmethod/latin/Settings;->ensureConsistencyOfAutoCorrectionSettings()V

    const-string v22, "general_settings"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v7

    check-cast v7, Landroid/preference/PreferenceGroup;

    const-string v22, "correction_settings"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v21

    check-cast v21, Landroid/preference/PreferenceGroup;

    const-string v22, "gesture_typing_settings"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v12

    check-cast v12, Landroid/preference/PreferenceGroup;

    const-string v22, "misc_settings"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v14

    check-cast v14, Landroid/preference/PreferenceGroup;

    const-string v22, "debug_settings"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v22

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/inputmethod/latin/Settings;->mDebugSettingsPreference:Landroid/preference/Preference;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/Settings;->mDebugSettingsPreference:Landroid/preference/Preference;

    move-object/from16 v22, v0

    if-eqz v22, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/Settings;->mDebugSettingsPreference:Landroid/preference/Preference;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v14, v0}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :cond_0
    const v22, 0x7f090002    # com.android.inputmethod.latin.R.bool.config_enable_show_voice_key_option

    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v20

    if-nez v20, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/Settings;->mVoicePreference:Landroid/preference/ListPreference;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v7, v0}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :cond_1
    const-string v22, "pref_advanced_settings"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/PreferenceGroup;

    invoke-static {v4}, Lcom/android/inputmethod/latin/VibratorUtils;->getInstance(Landroid/content/Context;)Lcom/android/inputmethod/latin/VibratorUtils;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/android/inputmethod/latin/VibratorUtils;->hasVibrator()Z

    move-result v22

    if-nez v22, :cond_2

    const-string v22, "vibrate_on"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v7, v0}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    if-eqz v3, :cond_2

    const-string v22, "pref_vibration_duration_settings"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :cond_2
    const v22, 0x7f090003    # com.android.inputmethod.latin.R.bool.config_enable_show_popup_on_keypress_option

    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v19

    const-string v22, "pref_key_preview_popup_dismiss_delay"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v22

    check-cast v22, Landroid/preference/ListPreference;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/inputmethod/latin/Settings;->mKeyPreviewPopupDismissDelay:Landroid/preference/ListPreference;

    if-nez v19, :cond_7

    const-string v22, "popup_on"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v7, v0}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    if-eqz v3, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/Settings;->mKeyPreviewPopupDismissDelay:Landroid/preference/ListPreference;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v3, v0}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :cond_3
    :goto_0
    const-string v22, "pref_include_other_imes_in_language_switch_list"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v22

    invoke-static/range {v17 .. v17}, Lcom/android/inputmethod/latin/SettingsValues;->showsLanguageSwitchKey(Landroid/content/SharedPreferences;)Z

    move-result v23

    invoke-static/range {v22 .. v23}, Lcom/android/inputmethod/latin/Settings;->setPreferenceEnabled(Landroid/preference/Preference;Z)V

    const-string v22, "configure_dictionaries_key"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v5

    check-cast v5, Landroid/preference/PreferenceScreen;

    invoke-virtual {v5}, Landroid/preference/PreferenceScreen;->getIntent()Landroid/content/Intent;

    move-result-object v13

    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v22

    const/16 v23, 0x0

    move-object/from16 v0, v22

    move/from16 v1, v23

    invoke-virtual {v0, v13, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v22

    invoke-interface/range {v22 .. v22}, Ljava/util/List;->size()I

    move-result v15

    if-gtz v15, :cond_4

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    :cond_4
    const v22, 0x7f09000d    # com.android.inputmethod.latin.R.bool.config_gesture_input_enabled_by_build_config

    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v9

    const-string v22, "pref_gesture_preview_trail"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v11

    const-string v22, "pref_gesture_floating_preview_text"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v8

    if-nez v9, :cond_9

    const-string v22, "gesture_input"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v12, v0}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    invoke-virtual {v12, v11}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    invoke-virtual {v12, v8}, Landroid/preference/PreferenceGroup;->removePreference(Landroid/preference/Preference;)Z

    invoke-virtual/range {p0 .. p0}, Lcom/android/inputmethod/latin/Settings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v0, v12}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :goto_1
    const-string v22, "pref_vibration_duration_settings"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v22

    check-cast v22, Landroid/preference/PreferenceScreen;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/inputmethod/latin/Settings;->mKeypressVibrationDurationSettingsPref:Landroid/preference/PreferenceScreen;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/Settings;->mKeypressVibrationDurationSettingsPref:Landroid/preference/PreferenceScreen;

    move-object/from16 v22, v0

    if-eqz v22, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/Settings;->mKeypressVibrationDurationSettingsPref:Landroid/preference/PreferenceScreen;

    move-object/from16 v22, v0

    new-instance v23, Lcom/android/inputmethod/latin/Settings$1;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/inputmethod/latin/Settings$1;-><init>(Lcom/android/inputmethod/latin/Settings;)V

    invoke-virtual/range {v22 .. v23}, Landroid/preference/PreferenceScreen;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/android/inputmethod/latin/Settings;->updateKeypressVibrationDurationSettingsSummary(Landroid/content/SharedPreferences;Landroid/content/res/Resources;)V

    :cond_5
    const-string v22, "pref_keypress_sound_volume"

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v22

    check-cast v22, Landroid/preference/PreferenceScreen;

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/inputmethod/latin/Settings;->mKeypressSoundVolumeSettingsPref:Landroid/preference/PreferenceScreen;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/Settings;->mKeypressSoundVolumeSettingsPref:Landroid/preference/PreferenceScreen;

    move-object/from16 v22, v0

    if-eqz v22, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/Settings;->mKeypressSoundVolumeSettingsPref:Landroid/preference/PreferenceScreen;

    move-object/from16 v22, v0

    new-instance v23, Lcom/android/inputmethod/latin/Settings$2;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/inputmethod/latin/Settings$2;-><init>(Lcom/android/inputmethod/latin/Settings;)V

    invoke-virtual/range {v22 .. v23}, Landroid/preference/PreferenceScreen;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/android/inputmethod/latin/Settings;->updateKeypressSoundVolumeSummary(Landroid/content/SharedPreferences;Landroid/content/res/Resources;)V

    :cond_6
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/android/inputmethod/latin/Settings;->refreshEnablingsOfKeypressSoundAndVibrationSettings(Landroid/content/SharedPreferences;Landroid/content/res/Resources;)V

    return-void

    :cond_7
    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v6, v0, [Ljava/lang/String;

    const/16 v22, 0x0

    const v23, 0x7f0b0042    # com.android.inputmethod.latin.R.string.key_preview_popup_dismiss_no_delay

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v23

    aput-object v23, v6, v22

    const/16 v22, 0x1

    const v23, 0x7f0b0043    # com.android.inputmethod.latin.R.string.key_preview_popup_dismiss_default_delay

    move-object/from16 v0, v18

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v23

    aput-object v23, v6, v22

    const v22, 0x7f0a000b    # com.android.inputmethod.latin.R.integer.config_key_preview_linger_timeout

    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v22

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/Settings;->mKeyPreviewPopupDismissDelay:Landroid/preference/ListPreference;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/Settings;->mKeyPreviewPopupDismissDelay:Landroid/preference/ListPreference;

    move-object/from16 v22, v0

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const-string v25, "0"

    aput-object v25, v23, v24

    const/16 v24, 0x1

    aput-object v16, v23, v24

    invoke-virtual/range {v22 .. v23}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/Settings;->mKeyPreviewPopupDismissDelay:Landroid/preference/ListPreference;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v22

    if-nez v22, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/Settings;->mKeyPreviewPopupDismissDelay:Landroid/preference/ListPreference;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/inputmethod/latin/Settings;->mKeyPreviewPopupDismissDelay:Landroid/preference/ListPreference;

    move-object/from16 v22, v0

    invoke-static/range {v17 .. v18}, Lcom/android/inputmethod/latin/SettingsValues;->isKeyPreviewPopupEnabled(Landroid/content/SharedPreferences;Landroid/content/res/Resources;)Z

    move-result v23

    invoke-static/range {v22 .. v23}, Lcom/android/inputmethod/latin/Settings;->setPreferenceEnabled(Landroid/preference/Preference;Z)V

    goto/16 :goto_0

    :cond_9
    const-string v22, "gesture_input"

    const/16 v23, 0x1

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v10

    invoke-static {v11, v10}, Lcom/android/inputmethod/latin/Settings;->setPreferenceEnabled(Landroid/preference/Preference;Z)V

    invoke-static {v8, v10}, Lcom/android/inputmethod/latin/Settings;->setPreferenceEnabled(Landroid/preference/Preference;Z)V

    goto/16 :goto_1
.end method

.method public onDestroy()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/Settings;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    invoke-super {p0}, Lcom/android/inputmethodcommon/InputMethodSettingsFragment;->onDestroy()V

    return-void
.end method

.method public onResume()V
    .locals 3

    invoke-super {p0}, Lcom/android/inputmethodcommon/InputMethodSettingsFragment;->onResume()V

    invoke-static {}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->getInstance()Lcom/android/inputmethod/latin/SubtypeSwitcher;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/SubtypeSwitcher;->isShortcutImeEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/inputmethod/latin/Settings;->updateVoiceModeSummary()V

    :goto_0
    invoke-direct {p0}, Lcom/android/inputmethod/latin/Settings;->updateShowCorrectionSuggestionsSummary()V

    invoke-direct {p0}, Lcom/android/inputmethod/latin/Settings;->updateKeyPreviewPopupDelaySummary()V

    invoke-direct {p0}, Lcom/android/inputmethod/latin/Settings;->updateCustomInputStylesSummary()V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/inputmethod/latin/Settings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    iget-object v2, p0, Lcom/android/inputmethod/latin/Settings;->mVoicePreference:Landroid/preference/ListPreference;

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 5
    .param p1    # Landroid/content/SharedPreferences;
    .param p2    # Ljava/lang/String;

    const/4 v4, 0x1

    new-instance v2, Landroid/app/backup/BackupManager;

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/Settings;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/backup/BackupManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Landroid/app/backup/BackupManager;->dataChanged()V

    const-string v2, "popup_on"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "pref_key_preview_popup_dismiss_delay"

    invoke-virtual {p0, v2}, Lcom/android/inputmethod/latin/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    const-string v3, "popup_on"

    invoke-interface {p1, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-static {v2, v3}, Lcom/android/inputmethod/latin/Settings;->setPreferenceEnabled(Landroid/preference/Preference;Z)V

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/android/inputmethod/latin/Settings;->ensureConsistencyOfAutoCorrectionSettings()V

    invoke-direct {p0}, Lcom/android/inputmethod/latin/Settings;->updateVoiceModeSummary()V

    invoke-direct {p0}, Lcom/android/inputmethod/latin/Settings;->updateShowCorrectionSuggestionsSummary()V

    invoke-direct {p0}, Lcom/android/inputmethod/latin/Settings;->updateKeyPreviewPopupDelaySummary()V

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/Settings;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {p0, p1, v2}, Lcom/android/inputmethod/latin/Settings;->refreshEnablingsOfKeypressSoundAndVibrationSettings(Landroid/content/SharedPreferences;Landroid/content/res/Resources;)V

    return-void

    :cond_1
    const-string v2, "pref_show_language_switch_key"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "pref_include_other_imes_in_language_switch_list"

    invoke-virtual {p0, v2}, Lcom/android/inputmethod/latin/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    invoke-static {p1}, Lcom/android/inputmethod/latin/SettingsValues;->showsLanguageSwitchKey(Landroid/content/SharedPreferences;)Z

    move-result v3

    invoke-static {v2, v3}, Lcom/android/inputmethod/latin/Settings;->setPreferenceEnabled(Landroid/preference/Preference;Z)V

    goto :goto_0

    :cond_2
    const-string v2, "gesture_input"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/Settings;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09000d    # com.android.inputmethod.latin.R.bool.config_gesture_input_enabled_by_build_config

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v2, "gesture_input"

    invoke-interface {p1, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    const-string v2, "pref_gesture_preview_trail"

    invoke-virtual {p0, v2}, Lcom/android/inputmethod/latin/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/android/inputmethod/latin/Settings;->setPreferenceEnabled(Landroid/preference/Preference;Z)V

    const-string v2, "pref_gesture_floating_preview_text"

    invoke-virtual {p0, v2}, Lcom/android/inputmethod/latin/Settings;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/android/inputmethod/latin/Settings;->setPreferenceEnabled(Landroid/preference/Preference;Z)V

    goto :goto_0
.end method
