.class public final Lcom/android/inputmethod/latin/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final Keyboard:[I

.field public static final KeyboardLayoutSet_Element:[I

.field public static final KeyboardTheme:[I

.field public static final KeyboardView:[I

.field public static final Keyboard_Case:[I

.field public static final Keyboard_Include:[I

.field public static final Keyboard_Key:[I

.field public static final Keyboard_KeyStyle:[I

.field public static final MainKeyboardView:[I

.field public static final SuggestionStripView:[I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v3, 0xe

    const/16 v0, 0x1c

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/inputmethod/latin/R$styleable;->Keyboard:[I

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/inputmethod/latin/R$styleable;->KeyboardLayoutSet_Element:[I

    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/android/inputmethod/latin/R$styleable;->KeyboardTheme:[I

    const/16 v0, 0x1e

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/android/inputmethod/latin/R$styleable;->KeyboardView:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/android/inputmethod/latin/R$styleable;->Keyboard_Case:[I

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f010096    # com.android.inputmethod.latin.R.attr.keyboardLayout

    aput v2, v0, v1

    sput-object v0, Lcom/android/inputmethod/latin/R$styleable;->Keyboard_Include:[I

    const/16 v0, 0x24

    new-array v0, v0, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/android/inputmethod/latin/R$styleable;->Keyboard_Key:[I

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/android/inputmethod/latin/R$styleable;->Keyboard_KeyStyle:[I

    const/16 v0, 0x1f

    new-array v0, v0, [I

    fill-array-data v0, :array_7

    sput-object v0, Lcom/android/inputmethod/latin/R$styleable;->MainKeyboardView:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_8

    sput-object v0, Lcom/android/inputmethod/latin/R$styleable;->SuggestionStripView:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f010056    # com.android.inputmethod.latin.R.attr.themeId
        0x7f010057    # com.android.inputmethod.latin.R.attr.touchPositionCorrectionData
        0x7f010058    # com.android.inputmethod.latin.R.attr.keyboardHeight
        0x7f010059    # com.android.inputmethod.latin.R.attr.maxKeyboardHeight
        0x7f01005a    # com.android.inputmethod.latin.R.attr.minKeyboardHeight
        0x7f01005b    # com.android.inputmethod.latin.R.attr.keyboardTopPadding
        0x7f01005c    # com.android.inputmethod.latin.R.attr.keyboardBottomPadding
        0x7f01005d    # com.android.inputmethod.latin.R.attr.keyboardHorizontalEdgesPadding
        0x7f01005e    # com.android.inputmethod.latin.R.attr.rowHeight
        0x7f01005f    # com.android.inputmethod.latin.R.attr.horizontalGap
        0x7f010060    # com.android.inputmethod.latin.R.attr.verticalGap
        0x7f010061    # com.android.inputmethod.latin.R.attr.moreKeysTemplate
        0x7f010062    # com.android.inputmethod.latin.R.attr.iconShiftKey
        0x7f010063    # com.android.inputmethod.latin.R.attr.iconDeleteKey
        0x7f010064    # com.android.inputmethod.latin.R.attr.iconSettingsKey
        0x7f010065    # com.android.inputmethod.latin.R.attr.iconSpaceKey
        0x7f010066    # com.android.inputmethod.latin.R.attr.iconEnterKey
        0x7f010067    # com.android.inputmethod.latin.R.attr.iconSearchKey
        0x7f010068    # com.android.inputmethod.latin.R.attr.iconTabKey
        0x7f010069    # com.android.inputmethod.latin.R.attr.iconShortcutKey
        0x7f01006a    # com.android.inputmethod.latin.R.attr.iconShortcutForLabel
        0x7f01006b    # com.android.inputmethod.latin.R.attr.iconSpaceKeyForNumberLayout
        0x7f01006c    # com.android.inputmethod.latin.R.attr.iconShiftKeyShifted
        0x7f01006d    # com.android.inputmethod.latin.R.attr.iconShortcutKeyDisabled
        0x7f01006e    # com.android.inputmethod.latin.R.attr.iconTabKeyPreview
        0x7f01006f    # com.android.inputmethod.latin.R.attr.iconLanguageSwitchKey
        0x7f010070    # com.android.inputmethod.latin.R.attr.iconZwnjKey
        0x7f010071    # com.android.inputmethod.latin.R.attr.iconZwjKey
    .end array-data

    :array_1
    .array-data 4
        0x7f0100a7    # com.android.inputmethod.latin.R.attr.elementName
        0x7f0100a8    # com.android.inputmethod.latin.R.attr.elementKeyboard
        0x7f0100a9    # com.android.inputmethod.latin.R.attr.enableProximityCharsCorrection
    .end array-data

    :array_2
    .array-data 4
        0x7f010000    # com.android.inputmethod.latin.R.attr.keyboardStyle
        0x7f010001    # com.android.inputmethod.latin.R.attr.keyboardViewStyle
        0x7f010002    # com.android.inputmethod.latin.R.attr.mainKeyboardViewStyle
        0x7f010003    # com.android.inputmethod.latin.R.attr.moreKeysKeyboardStyle
        0x7f010004    # com.android.inputmethod.latin.R.attr.moreKeysKeyboardViewStyle
        0x7f010005    # com.android.inputmethod.latin.R.attr.moreKeysKeyboardPanelStyle
        0x7f010006    # com.android.inputmethod.latin.R.attr.suggestionsStripBackgroundStyle
        0x7f010007    # com.android.inputmethod.latin.R.attr.suggestionStripViewStyle
        0x7f010008    # com.android.inputmethod.latin.R.attr.moreSuggestionsViewStyle
        0x7f010009    # com.android.inputmethod.latin.R.attr.suggestionBackgroundStyle
        0x7f01000a    # com.android.inputmethod.latin.R.attr.suggestionPreviewBackgroundStyle
    .end array-data

    :array_3
    .array-data 4
        0x7f01000b    # com.android.inputmethod.latin.R.attr.keyBackground
        0x7f01000c    # com.android.inputmethod.latin.R.attr.keyLabelHorizontalPadding
        0x7f01000d    # com.android.inputmethod.latin.R.attr.keyHintLetterPadding
        0x7f01000e    # com.android.inputmethod.latin.R.attr.keyPopupHintLetterPadding
        0x7f01000f    # com.android.inputmethod.latin.R.attr.keyShiftedLetterHintPadding
        0x7f010010    # com.android.inputmethod.latin.R.attr.keyTextShadowRadius
        0x7f010011    # com.android.inputmethod.latin.R.attr.keyPreviewLayout
        0x7f010012    # com.android.inputmethod.latin.R.attr.state_left_edge
        0x7f010013    # com.android.inputmethod.latin.R.attr.state_right_edge
        0x7f010014    # com.android.inputmethod.latin.R.attr.state_has_morekeys
        0x7f010015    # com.android.inputmethod.latin.R.attr.keyPreviewOffset
        0x7f010016    # com.android.inputmethod.latin.R.attr.keyPreviewHeight
        0x7f010017    # com.android.inputmethod.latin.R.attr.keyPreviewLingerTimeout
        0x7f010018    # com.android.inputmethod.latin.R.attr.verticalCorrection
        0x7f010019    # com.android.inputmethod.latin.R.attr.moreKeysLayout
        0x7f01001a    # com.android.inputmethod.latin.R.attr.backgroundDimAlpha
        0x7f01001b    # com.android.inputmethod.latin.R.attr.gestureFloatingPreviewTextSize
        0x7f01001c    # com.android.inputmethod.latin.R.attr.gestureFloatingPreviewTextColor
        0x7f01001d    # com.android.inputmethod.latin.R.attr.gestureFloatingPreviewTextOffset
        0x7f01001e    # com.android.inputmethod.latin.R.attr.gestureFloatingPreviewColor
        0x7f01001f    # com.android.inputmethod.latin.R.attr.gestureFloatingPreviewHorizontalPadding
        0x7f010020    # com.android.inputmethod.latin.R.attr.gestureFloatingPreviewVerticalPadding
        0x7f010021    # com.android.inputmethod.latin.R.attr.gestureFloatingPreviewRoundRadius
        0x7f010022    # com.android.inputmethod.latin.R.attr.gestureFloatingPreviewTextLingerTimeout
        0x7f010023    # com.android.inputmethod.latin.R.attr.gesturePreviewTrailFadeoutStartDelay
        0x7f010024    # com.android.inputmethod.latin.R.attr.gesturePreviewTrailFadeoutDuration
        0x7f010025    # com.android.inputmethod.latin.R.attr.gesturePreviewTrailUpdateInterval
        0x7f010026    # com.android.inputmethod.latin.R.attr.gesturePreviewTrailColor
        0x7f010027    # com.android.inputmethod.latin.R.attr.gesturePreviewTrailStartWidth
        0x7f010028    # com.android.inputmethod.latin.R.attr.gesturePreviewTrailEndWidth
    .end array-data

    :array_4
    .array-data 4
        0x7f010097    # com.android.inputmethod.latin.R.attr.keyboardLayoutSetElement
        0x7f010098    # com.android.inputmethod.latin.R.attr.mode
        0x7f010099    # com.android.inputmethod.latin.R.attr.navigateNext
        0x7f01009a    # com.android.inputmethod.latin.R.attr.navigatePrevious
        0x7f01009b    # com.android.inputmethod.latin.R.attr.passwordInput
        0x7f01009c    # com.android.inputmethod.latin.R.attr.clobberSettingsKey
        0x7f01009d    # com.android.inputmethod.latin.R.attr.shortcutKeyEnabled
        0x7f01009e    # com.android.inputmethod.latin.R.attr.hasShortcutKey
        0x7f01009f    # com.android.inputmethod.latin.R.attr.languageSwitchKeyEnabled
        0x7f0100a0    # com.android.inputmethod.latin.R.attr.isMultiLine
        0x7f0100a1    # com.android.inputmethod.latin.R.attr.imeAction
        0x7f0100a2    # com.android.inputmethod.latin.R.attr.localeCode
        0x7f0100a3    # com.android.inputmethod.latin.R.attr.languageCode
        0x7f0100a4    # com.android.inputmethod.latin.R.attr.countryCode
    .end array-data

    :array_5
    .array-data 4
        0x7f010072    # com.android.inputmethod.latin.R.attr.code
        0x7f010073    # com.android.inputmethod.latin.R.attr.altCode
        0x7f010074    # com.android.inputmethod.latin.R.attr.moreKeys
        0x7f010075    # com.android.inputmethod.latin.R.attr.additionalMoreKeys
        0x7f010076    # com.android.inputmethod.latin.R.attr.maxMoreKeysColumn
        0x7f010077    # com.android.inputmethod.latin.R.attr.backgroundType
        0x7f010078    # com.android.inputmethod.latin.R.attr.keyActionFlags
        0x7f010079    # com.android.inputmethod.latin.R.attr.keyOutputText
        0x7f01007a    # com.android.inputmethod.latin.R.attr.keyLabel
        0x7f01007b    # com.android.inputmethod.latin.R.attr.keyHintLabel
        0x7f01007c    # com.android.inputmethod.latin.R.attr.keyLabelFlags
        0x7f01007d    # com.android.inputmethod.latin.R.attr.keyIcon
        0x7f01007e    # com.android.inputmethod.latin.R.attr.keyIconDisabled
        0x7f01007f    # com.android.inputmethod.latin.R.attr.keyIconPreview
        0x7f010080    # com.android.inputmethod.latin.R.attr.keyStyle
        0x7f010081    # com.android.inputmethod.latin.R.attr.visualInsetsLeft
        0x7f010082    # com.android.inputmethod.latin.R.attr.visualInsetsRight
        0x7f010083    # com.android.inputmethod.latin.R.attr.keyWidth
        0x7f010084    # com.android.inputmethod.latin.R.attr.keyXPos
        0x7f010085    # com.android.inputmethod.latin.R.attr.keyTypeface
        0x7f010086    # com.android.inputmethod.latin.R.attr.keyLetterSize
        0x7f010087    # com.android.inputmethod.latin.R.attr.keyLabelSize
        0x7f010088    # com.android.inputmethod.latin.R.attr.keyLargeLetterRatio
        0x7f010089    # com.android.inputmethod.latin.R.attr.keyLargeLabelRatio
        0x7f01008a    # com.android.inputmethod.latin.R.attr.keyHintLetterRatio
        0x7f01008b    # com.android.inputmethod.latin.R.attr.keyHintLabelRatio
        0x7f01008c    # com.android.inputmethod.latin.R.attr.keyShiftedLetterHintRatio
        0x7f01008d    # com.android.inputmethod.latin.R.attr.keyTextColor
        0x7f01008e    # com.android.inputmethod.latin.R.attr.keyTextShadowColor
        0x7f01008f    # com.android.inputmethod.latin.R.attr.keyTextInactivatedColor
        0x7f010090    # com.android.inputmethod.latin.R.attr.keyHintLetterColor
        0x7f010091    # com.android.inputmethod.latin.R.attr.keyHintLabelColor
        0x7f010092    # com.android.inputmethod.latin.R.attr.keyShiftedLetterHintInactivatedColor
        0x7f010093    # com.android.inputmethod.latin.R.attr.keyShiftedLetterHintActivatedColor
        0x7f010094    # com.android.inputmethod.latin.R.attr.keyPreviewTextColor
        0x7f010095    # com.android.inputmethod.latin.R.attr.keyPreviewTextRatio
    .end array-data

    :array_6
    .array-data 4
        0x7f0100a5    # com.android.inputmethod.latin.R.attr.styleName
        0x7f0100a6    # com.android.inputmethod.latin.R.attr.parentStyle
    .end array-data

    :array_7
    .array-data 4
        0x7f010029    # com.android.inputmethod.latin.R.attr.autoCorrectionSpacebarLedEnabled
        0x7f01002a    # com.android.inputmethod.latin.R.attr.autoCorrectionSpacebarLedIcon
        0x7f01002b    # com.android.inputmethod.latin.R.attr.spacebarTextRatio
        0x7f01002c    # com.android.inputmethod.latin.R.attr.spacebarTextColor
        0x7f01002d    # com.android.inputmethod.latin.R.attr.spacebarTextShadowColor
        0x7f01002e    # com.android.inputmethod.latin.R.attr.languageOnSpacebarFinalAlpha
        0x7f01002f    # com.android.inputmethod.latin.R.attr.languageOnSpacebarFadeoutAnimator
        0x7f010030    # com.android.inputmethod.latin.R.attr.altCodeKeyWhileTypingFadeoutAnimator
        0x7f010031    # com.android.inputmethod.latin.R.attr.altCodeKeyWhileTypingFadeinAnimator
        0x7f010032    # com.android.inputmethod.latin.R.attr.keyHysteresisDistance
        0x7f010033    # com.android.inputmethod.latin.R.attr.keyHysteresisDistanceForSlidingModifier
        0x7f010034    # com.android.inputmethod.latin.R.attr.touchNoiseThresholdTime
        0x7f010035    # com.android.inputmethod.latin.R.attr.touchNoiseThresholdDistance
        0x7f010036    # com.android.inputmethod.latin.R.attr.slidingKeyInputEnable
        0x7f010037    # com.android.inputmethod.latin.R.attr.keyRepeatStartTimeout
        0x7f010038    # com.android.inputmethod.latin.R.attr.keyRepeatInterval
        0x7f010039    # com.android.inputmethod.latin.R.attr.longPressKeyTimeout
        0x7f01003a    # com.android.inputmethod.latin.R.attr.longPressShiftKeyTimeout
        0x7f01003b    # com.android.inputmethod.latin.R.attr.ignoreAltCodeKeyTimeout
        0x7f01003c    # com.android.inputmethod.latin.R.attr.showMoreKeysKeyboardAtTouchedPoint
        0x7f01003d    # com.android.inputmethod.latin.R.attr.gestureStaticTimeThresholdAfterFastTyping
        0x7f01003e    # com.android.inputmethod.latin.R.attr.gestureDetectFastMoveSpeedThreshold
        0x7f01003f    # com.android.inputmethod.latin.R.attr.gestureDynamicThresholdDecayDuration
        0x7f010040    # com.android.inputmethod.latin.R.attr.gestureDynamicTimeThresholdFrom
        0x7f010041    # com.android.inputmethod.latin.R.attr.gestureDynamicTimeThresholdTo
        0x7f010042    # com.android.inputmethod.latin.R.attr.gestureDynamicDistanceThresholdFrom
        0x7f010043    # com.android.inputmethod.latin.R.attr.gestureDynamicDistanceThresholdTo
        0x7f010044    # com.android.inputmethod.latin.R.attr.gestureSamplingMinimumDistance
        0x7f010045    # com.android.inputmethod.latin.R.attr.gestureRecognitionMinimumTime
        0x7f010046    # com.android.inputmethod.latin.R.attr.gestureRecognitionSpeedThreshold
        0x7f010047    # com.android.inputmethod.latin.R.attr.suppressKeyPreviewAfterBatchInputDuration
    .end array-data

    :array_8
    .array-data 4
        0x7f010048    # com.android.inputmethod.latin.R.attr.suggestionStripOption
        0x7f010049    # com.android.inputmethod.latin.R.attr.colorValidTypedWord
        0x7f01004a    # com.android.inputmethod.latin.R.attr.colorTypedWord
        0x7f01004b    # com.android.inputmethod.latin.R.attr.colorAutoCorrect
        0x7f01004c    # com.android.inputmethod.latin.R.attr.colorSuggested
        0x7f01004d    # com.android.inputmethod.latin.R.attr.alphaValidTypedWord
        0x7f01004e    # com.android.inputmethod.latin.R.attr.alphaTypedWord
        0x7f01004f    # com.android.inputmethod.latin.R.attr.alphaAutoCorrect
        0x7f010050    # com.android.inputmethod.latin.R.attr.alphaSuggested
        0x7f010051    # com.android.inputmethod.latin.R.attr.alphaObsoleted
        0x7f010052    # com.android.inputmethod.latin.R.attr.suggestionsCountInStrip
        0x7f010053    # com.android.inputmethod.latin.R.attr.centerSuggestionPercentile
        0x7f010054    # com.android.inputmethod.latin.R.attr.maxMoreSuggestionsRow
        0x7f010055    # com.android.inputmethod.latin.R.attr.minMoreSuggestionsWidth
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
