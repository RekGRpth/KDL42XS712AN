.class public final Lcom/android/inputmethod/latin/InputPointers;
.super Ljava/lang/Object;
.source "InputPointers.java"


# instance fields
.field private final mDefaultCapacity:I

.field private final mPointerIds:Lcom/android/inputmethod/latin/ResizableIntArray;

.field private final mTimes:Lcom/android/inputmethod/latin/ResizableIntArray;

.field private final mXCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

.field private final mYCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/inputmethod/latin/InputPointers;->mDefaultCapacity:I

    new-instance v0, Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-direct {v0, p1}, Lcom/android/inputmethod/latin/ResizableIntArray;-><init>(I)V

    iput-object v0, p0, Lcom/android/inputmethod/latin/InputPointers;->mXCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    new-instance v0, Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-direct {v0, p1}, Lcom/android/inputmethod/latin/ResizableIntArray;-><init>(I)V

    iput-object v0, p0, Lcom/android/inputmethod/latin/InputPointers;->mYCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    new-instance v0, Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-direct {v0, p1}, Lcom/android/inputmethod/latin/ResizableIntArray;-><init>(I)V

    iput-object v0, p0, Lcom/android/inputmethod/latin/InputPointers;->mPointerIds:Lcom/android/inputmethod/latin/ResizableIntArray;

    new-instance v0, Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-direct {v0, p1}, Lcom/android/inputmethod/latin/ResizableIntArray;-><init>(I)V

    iput-object v0, p0, Lcom/android/inputmethod/latin/InputPointers;->mTimes:Lcom/android/inputmethod/latin/ResizableIntArray;

    return-void
.end method


# virtual methods
.method public addPointer(IIII)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v0, p0, Lcom/android/inputmethod/latin/InputPointers;->mXCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, p1}, Lcom/android/inputmethod/latin/ResizableIntArray;->add(I)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/InputPointers;->mYCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, p2}, Lcom/android/inputmethod/latin/ResizableIntArray;->add(I)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/InputPointers;->mPointerIds:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, p3}, Lcom/android/inputmethod/latin/ResizableIntArray;->add(I)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/InputPointers;->mTimes:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, p4}, Lcom/android/inputmethod/latin/ResizableIntArray;->add(I)V

    return-void
.end method

.method public addPointer(IIIII)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iget-object v0, p0, Lcom/android/inputmethod/latin/InputPointers;->mXCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, p1, p2}, Lcom/android/inputmethod/latin/ResizableIntArray;->add(II)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/InputPointers;->mYCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, p1, p3}, Lcom/android/inputmethod/latin/ResizableIntArray;->add(II)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/InputPointers;->mPointerIds:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, p1, p4}, Lcom/android/inputmethod/latin/ResizableIntArray;->add(II)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/InputPointers;->mTimes:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, p1, p5}, Lcom/android/inputmethod/latin/ResizableIntArray;->add(II)V

    return-void
.end method

.method public append(ILcom/android/inputmethod/latin/ResizableIntArray;Lcom/android/inputmethod/latin/ResizableIntArray;Lcom/android/inputmethod/latin/ResizableIntArray;II)V
    .locals 2
    .param p1    # I
    .param p2    # Lcom/android/inputmethod/latin/ResizableIntArray;
    .param p3    # Lcom/android/inputmethod/latin/ResizableIntArray;
    .param p4    # Lcom/android/inputmethod/latin/ResizableIntArray;
    .param p5    # I
    .param p6    # I

    if-nez p6, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/latin/InputPointers;->mXCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, p3, p5, p6}, Lcom/android/inputmethod/latin/ResizableIntArray;->append(Lcom/android/inputmethod/latin/ResizableIntArray;II)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/InputPointers;->mYCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, p4, p5, p6}, Lcom/android/inputmethod/latin/ResizableIntArray;->append(Lcom/android/inputmethod/latin/ResizableIntArray;II)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/InputPointers;->mPointerIds:Lcom/android/inputmethod/latin/ResizableIntArray;

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputPointers;->mPointerIds:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v1}, Lcom/android/inputmethod/latin/ResizableIntArray;->getLength()I

    move-result v1

    invoke-virtual {v0, p1, v1, p6}, Lcom/android/inputmethod/latin/ResizableIntArray;->fill(III)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/InputPointers;->mTimes:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, p2, p5, p6}, Lcom/android/inputmethod/latin/ResizableIntArray;->append(Lcom/android/inputmethod/latin/ResizableIntArray;II)V

    goto :goto_0
.end method

.method public append(Lcom/android/inputmethod/latin/InputPointers;II)V
    .locals 2
    .param p1    # Lcom/android/inputmethod/latin/InputPointers;
    .param p2    # I
    .param p3    # I

    if-nez p3, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/inputmethod/latin/InputPointers;->mXCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    iget-object v1, p1, Lcom/android/inputmethod/latin/InputPointers;->mXCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, v1, p2, p3}, Lcom/android/inputmethod/latin/ResizableIntArray;->append(Lcom/android/inputmethod/latin/ResizableIntArray;II)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/InputPointers;->mYCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    iget-object v1, p1, Lcom/android/inputmethod/latin/InputPointers;->mYCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, v1, p2, p3}, Lcom/android/inputmethod/latin/ResizableIntArray;->append(Lcom/android/inputmethod/latin/ResizableIntArray;II)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/InputPointers;->mPointerIds:Lcom/android/inputmethod/latin/ResizableIntArray;

    iget-object v1, p1, Lcom/android/inputmethod/latin/InputPointers;->mPointerIds:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, v1, p2, p3}, Lcom/android/inputmethod/latin/ResizableIntArray;->append(Lcom/android/inputmethod/latin/ResizableIntArray;II)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/InputPointers;->mTimes:Lcom/android/inputmethod/latin/ResizableIntArray;

    iget-object v1, p1, Lcom/android/inputmethod/latin/InputPointers;->mTimes:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, v1, p2, p3}, Lcom/android/inputmethod/latin/ResizableIntArray;->append(Lcom/android/inputmethod/latin/ResizableIntArray;II)V

    goto :goto_0
.end method

.method public copy(Lcom/android/inputmethod/latin/InputPointers;)V
    .locals 2
    .param p1    # Lcom/android/inputmethod/latin/InputPointers;

    iget-object v0, p0, Lcom/android/inputmethod/latin/InputPointers;->mXCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    iget-object v1, p1, Lcom/android/inputmethod/latin/InputPointers;->mXCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/ResizableIntArray;->copy(Lcom/android/inputmethod/latin/ResizableIntArray;)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/InputPointers;->mYCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    iget-object v1, p1, Lcom/android/inputmethod/latin/InputPointers;->mYCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/ResizableIntArray;->copy(Lcom/android/inputmethod/latin/ResizableIntArray;)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/InputPointers;->mPointerIds:Lcom/android/inputmethod/latin/ResizableIntArray;

    iget-object v1, p1, Lcom/android/inputmethod/latin/InputPointers;->mPointerIds:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/ResizableIntArray;->copy(Lcom/android/inputmethod/latin/ResizableIntArray;)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/InputPointers;->mTimes:Lcom/android/inputmethod/latin/ResizableIntArray;

    iget-object v1, p1, Lcom/android/inputmethod/latin/InputPointers;->mTimes:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/ResizableIntArray;->copy(Lcom/android/inputmethod/latin/ResizableIntArray;)V

    return-void
.end method

.method public getPointerIds()[I
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/InputPointers;->mPointerIds:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/ResizableIntArray;->getPrimitiveArray()[I

    move-result-object v0

    return-object v0
.end method

.method public getPointerSize()I
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/InputPointers;->mXCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/ResizableIntArray;->getLength()I

    move-result v0

    return v0
.end method

.method public getTimes()[I
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/InputPointers;->mTimes:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/ResizableIntArray;->getPrimitiveArray()[I

    move-result-object v0

    return-object v0
.end method

.method public getXCoordinates()[I
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/InputPointers;->mXCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/ResizableIntArray;->getPrimitiveArray()[I

    move-result-object v0

    return-object v0
.end method

.method public getYCoordinates()[I
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/InputPointers;->mYCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0}, Lcom/android/inputmethod/latin/ResizableIntArray;->getPrimitiveArray()[I

    move-result-object v0

    return-object v0
.end method

.method public reset()V
    .locals 2

    iget v0, p0, Lcom/android/inputmethod/latin/InputPointers;->mDefaultCapacity:I

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputPointers;->mXCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v1, v0}, Lcom/android/inputmethod/latin/ResizableIntArray;->reset(I)V

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputPointers;->mYCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v1, v0}, Lcom/android/inputmethod/latin/ResizableIntArray;->reset(I)V

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputPointers;->mPointerIds:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v1, v0}, Lcom/android/inputmethod/latin/ResizableIntArray;->reset(I)V

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputPointers;->mTimes:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v1, v0}, Lcom/android/inputmethod/latin/ResizableIntArray;->reset(I)V

    return-void
.end method

.method public set(Lcom/android/inputmethod/latin/InputPointers;)V
    .locals 2
    .param p1    # Lcom/android/inputmethod/latin/InputPointers;

    iget-object v0, p0, Lcom/android/inputmethod/latin/InputPointers;->mXCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    iget-object v1, p1, Lcom/android/inputmethod/latin/InputPointers;->mXCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/ResizableIntArray;->set(Lcom/android/inputmethod/latin/ResizableIntArray;)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/InputPointers;->mYCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    iget-object v1, p1, Lcom/android/inputmethod/latin/InputPointers;->mYCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/ResizableIntArray;->set(Lcom/android/inputmethod/latin/ResizableIntArray;)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/InputPointers;->mPointerIds:Lcom/android/inputmethod/latin/ResizableIntArray;

    iget-object v1, p1, Lcom/android/inputmethod/latin/InputPointers;->mPointerIds:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/ResizableIntArray;->set(Lcom/android/inputmethod/latin/ResizableIntArray;)V

    iget-object v0, p0, Lcom/android/inputmethod/latin/InputPointers;->mTimes:Lcom/android/inputmethod/latin/ResizableIntArray;

    iget-object v1, p1, Lcom/android/inputmethod/latin/InputPointers;->mTimes:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, v1}, Lcom/android/inputmethod/latin/ResizableIntArray;->set(Lcom/android/inputmethod/latin/ResizableIntArray;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/inputmethod/latin/InputPointers;->getPointerSize()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputPointers;->mPointerIds:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " time="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputPointers;->mTimes:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " x="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputPointers;->mXCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " y="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/inputmethod/latin/InputPointers;->mYCoordinates:Lcom/android/inputmethod/latin/ResizableIntArray;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
