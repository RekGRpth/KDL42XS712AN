.class public final Lcom/android/inputmethod/latin/makedict/CharGroupInfo;
.super Ljava/lang/Object;
.source "CharGroupInfo.java"


# instance fields
.field public final mBigrams:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/makedict/PendingAttribute;",
            ">;"
        }
    .end annotation
.end field

.field public final mCharacters:[I

.field public final mChildrenAddress:I

.field public final mEndAddress:I

.field public final mFlags:I

.field public final mFrequency:I

.field public final mOriginalAddress:I

.field public final mParentAddress:I

.field public final mShortcutTargets:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(III[IIIILjava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # [I
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III[IIII",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/makedict/PendingAttribute;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/inputmethod/latin/makedict/CharGroupInfo;->mOriginalAddress:I

    iput p2, p0, Lcom/android/inputmethod/latin/makedict/CharGroupInfo;->mEndAddress:I

    iput p3, p0, Lcom/android/inputmethod/latin/makedict/CharGroupInfo;->mFlags:I

    iput-object p4, p0, Lcom/android/inputmethod/latin/makedict/CharGroupInfo;->mCharacters:[I

    iput p5, p0, Lcom/android/inputmethod/latin/makedict/CharGroupInfo;->mFrequency:I

    iput p6, p0, Lcom/android/inputmethod/latin/makedict/CharGroupInfo;->mParentAddress:I

    iput p7, p0, Lcom/android/inputmethod/latin/makedict/CharGroupInfo;->mChildrenAddress:I

    iput-object p8, p0, Lcom/android/inputmethod/latin/makedict/CharGroupInfo;->mShortcutTargets:Ljava/util/ArrayList;

    iput-object p9, p0, Lcom/android/inputmethod/latin/makedict/CharGroupInfo;->mBigrams:Ljava/util/ArrayList;

    return-void
.end method
