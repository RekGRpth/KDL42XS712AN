.class public final Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;
.super Ljava/lang/Object;
.source "FusionDictionary.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/makedict/FusionDictionary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "CharGroup"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field mBigrams:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;",
            ">;"
        }
    .end annotation
.end field

.field mCachedAddress:I

.field mCachedSize:I

.field final mChars:[I

.field mChildren:Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

.field mFrequency:I

.field mIsBlacklistEntry:Z

.field mIsNotAWord:Z

.field mShortcutTargets:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>([ILjava/util/ArrayList;Ljava/util/ArrayList;IZZ)V
    .locals 1
    .param p1    # [I
    .param p4    # I
    .param p5    # Z
    .param p6    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;",
            ">;IZZ)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChars:[I

    iput p4, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mFrequency:I

    iput-object p2, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mShortcutTargets:Ljava/util/ArrayList;

    iput-object p3, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mBigrams:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChildren:Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    iput-boolean p5, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mIsNotAWord:Z

    iput-boolean p6, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mIsBlacklistEntry:Z

    return-void
.end method

.method public constructor <init>([ILjava/util/ArrayList;Ljava/util/ArrayList;IZZLcom/android/inputmethod/latin/makedict/FusionDictionary$Node;)V
    .locals 0
    .param p1    # [I
    .param p4    # I
    .param p5    # Z
    .param p6    # Z
    .param p7    # Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;",
            ">;IZZ",
            "Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChars:[I

    iput p4, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mFrequency:I

    iput-object p2, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mShortcutTargets:Ljava/util/ArrayList;

    iput-object p3, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mBigrams:Ljava/util/ArrayList;

    iput-object p7, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChildren:Lcom/android/inputmethod/latin/makedict/FusionDictionary$Node;

    iput-boolean p5, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mIsNotAWord:Z

    iput-boolean p6, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mIsBlacklistEntry:Z

    return-void
.end method


# virtual methods
.method public addBigram(Ljava/lang/String;I)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # I

    iget-object v1, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mBigrams:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mBigrams:Ljava/util/ArrayList;

    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->getBigram(Ljava/lang/String;)Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;

    move-result-object v0

    if-eqz v0, :cond_1

    iput p2, v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;->mFrequency:I

    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;

    invoke-direct {v0, p1, p2}, Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;-><init>(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mBigrams:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getBigram(Ljava/lang/String;)Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;
    .locals 4
    .param p1    # Ljava/lang/String;

    iget-object v3, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mBigrams:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mBigrams:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v3, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mBigrams:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;

    iget-object v3, v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;->mWord:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getShortcut(Ljava/lang/String;)Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;
    .locals 4
    .param p1    # Ljava/lang/String;

    iget-object v3, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mShortcutTargets:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mShortcutTargets:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    iget-object v3, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mShortcutTargets:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;

    iget-object v3, v1, Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;->mWord:Ljava/lang/String;

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_1
    return-object v1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public hasSeveralChars()Z
    .locals 2

    const/4 v0, 0x1

    sget-boolean v1, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChars:[I

    array-length v1, v1

    if-gtz v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mChars:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isTerminal()Z
    .locals 2

    const/4 v0, -0x1

    iget v1, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mFrequency:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public update(ILjava/util/ArrayList;Ljava/util/ArrayList;ZZ)V
    .locals 8
    .param p1    # I
    .param p4    # Z
    .param p5    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;",
            ">;ZZ)V"
        }
    .end annotation

    iget v6, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mFrequency:I

    if-le p1, v6, :cond_0

    iput p1, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mFrequency:I

    :cond_0
    if-eqz p2, :cond_1

    iget-object v6, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mShortcutTargets:Ljava/util/ArrayList;

    if-nez v6, :cond_3

    iput-object p2, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mShortcutTargets:Ljava/util/ArrayList;

    :cond_1
    if-eqz p3, :cond_2

    iget-object v6, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mBigrams:Ljava/util/ArrayList;

    if-nez v6, :cond_6

    iput-object p3, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mBigrams:Ljava/util/ArrayList;

    :cond_2
    iput-boolean p4, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mIsNotAWord:Z

    iput-boolean p5, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mIsBlacklistEntry:Z

    return-void

    :cond_3
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v5, :cond_1

    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;

    iget-object v6, v4, Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;->mWord:Ljava/lang/String;

    invoke-virtual {p0, v6}, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->getShortcut(Ljava/lang/String;)Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;

    move-result-object v2

    if-nez v2, :cond_5

    iget-object v6, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mShortcutTargets:Ljava/util/ArrayList;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_5
    iget v6, v2, Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;->mFrequency:I

    iget v7, v4, Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;->mFrequency:I

    if-ge v6, v7, :cond_4

    iget v6, v4, Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;->mFrequency:I

    iput v6, v2, Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;->mFrequency:I

    goto :goto_1

    :cond_6
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v3, 0x0

    :goto_2
    if-ge v3, v5, :cond_2

    invoke-virtual {p3, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;

    iget-object v6, v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;->mWord:Ljava/lang/String;

    invoke-virtual {p0, v6}, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->getBigram(Ljava/lang/String;)Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;

    move-result-object v1

    if-nez v1, :cond_8

    iget-object v6, p0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$CharGroup;->mBigrams:Ljava/util/ArrayList;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_7
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_8
    iget v6, v1, Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;->mFrequency:I

    iget v7, v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;->mFrequency:I

    if-ge v6, v7, :cond_7

    iget v6, v0, Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;->mFrequency:I

    iput v6, v1, Lcom/android/inputmethod/latin/makedict/FusionDictionary$WeightedString;->mFrequency:I

    goto :goto_3
.end method
