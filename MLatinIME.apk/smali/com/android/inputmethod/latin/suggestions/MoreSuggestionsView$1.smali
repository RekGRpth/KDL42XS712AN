.class Lcom/android/inputmethod/latin/suggestions/MoreSuggestionsView$1;
.super Lcom/android/inputmethod/keyboard/KeyboardActionListener$Adapter;
.source "MoreSuggestionsView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/inputmethod/latin/suggestions/MoreSuggestionsView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/inputmethod/latin/suggestions/MoreSuggestionsView;


# direct methods
.method constructor <init>(Lcom/android/inputmethod/latin/suggestions/MoreSuggestionsView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/inputmethod/latin/suggestions/MoreSuggestionsView$1;->this$0:Lcom/android/inputmethod/latin/suggestions/MoreSuggestionsView;

    invoke-direct {p0}, Lcom/android/inputmethod/keyboard/KeyboardActionListener$Adapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancelInput()V
    .locals 1

    iget-object v0, p0, Lcom/android/inputmethod/latin/suggestions/MoreSuggestionsView$1;->this$0:Lcom/android/inputmethod/latin/suggestions/MoreSuggestionsView;

    iget-object v0, v0, Lcom/android/inputmethod/latin/suggestions/MoreSuggestionsView;->mListener:Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    invoke-interface {v0}, Lcom/android/inputmethod/keyboard/KeyboardActionListener;->onCancelInput()V

    return-void
.end method

.method public onCodeInput(III)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I

    add-int/lit16 v0, p1, -0x400

    if-ltz v0, :cond_0

    const/16 v1, 0x12

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/inputmethod/latin/suggestions/MoreSuggestionsView$1;->this$0:Lcom/android/inputmethod/latin/suggestions/MoreSuggestionsView;

    iget-object v1, v1, Lcom/android/inputmethod/latin/suggestions/MoreSuggestionsView;->mListener:Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    invoke-interface {v1, v0}, Lcom/android/inputmethod/keyboard/KeyboardActionListener;->onCustomRequest(I)Z

    :cond_0
    return-void
.end method

.method public onPressKey(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/inputmethod/latin/suggestions/MoreSuggestionsView$1;->this$0:Lcom/android/inputmethod/latin/suggestions/MoreSuggestionsView;

    iget-object v0, v0, Lcom/android/inputmethod/latin/suggestions/MoreSuggestionsView;->mListener:Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    invoke-interface {v0, p1}, Lcom/android/inputmethod/keyboard/KeyboardActionListener;->onPressKey(I)V

    return-void
.end method

.method public onReleaseKey(IZ)V
    .locals 1
    .param p1    # I
    .param p2    # Z

    iget-object v0, p0, Lcom/android/inputmethod/latin/suggestions/MoreSuggestionsView$1;->this$0:Lcom/android/inputmethod/latin/suggestions/MoreSuggestionsView;

    iget-object v0, v0, Lcom/android/inputmethod/latin/suggestions/MoreSuggestionsView;->mListener:Lcom/android/inputmethod/keyboard/KeyboardActionListener;

    invoke-interface {v0, p1, p2}, Lcom/android/inputmethod/keyboard/KeyboardActionListener;->onReleaseKey(IZ)V

    return-void
.end method
