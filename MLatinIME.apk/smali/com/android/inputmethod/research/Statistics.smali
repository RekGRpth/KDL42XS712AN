.class public Lcom/android/inputmethod/research/Statistics;
.super Ljava/lang/Object;
.source "Statistics.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;
    }
.end annotation


# static fields
.field private static final sInstance:Lcom/android/inputmethod/research/Statistics;


# instance fields
.field final mAfterDeleteKeyCounter:Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

.field final mBeforeDeleteKeyCounter:Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

.field mCharCount:I

.field mDeleteKeyCount:I

.field final mDuringRepeatedDeleteKeysCounter:Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

.field mIsEmptinessStateKnown:Z

.field mIsEmptyUponStarting:Z

.field mIsLastKeyDeleteKey:Z

.field final mKeyCounter:Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

.field private mLastTapTime:J

.field mLetterCount:I

.field mNumberCount:I

.field mSpaceCount:I

.field mWordCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/inputmethod/research/Statistics;

    invoke-direct {v0}, Lcom/android/inputmethod/research/Statistics;-><init>()V

    sput-object v0, Lcom/android/inputmethod/research/Statistics;->sInstance:Lcom/android/inputmethod/research/Statistics;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

    invoke-direct {v0}, Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/research/Statistics;->mKeyCounter:Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

    new-instance v0, Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

    invoke-direct {v0}, Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/research/Statistics;->mBeforeDeleteKeyCounter:Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

    new-instance v0, Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

    invoke-direct {v0}, Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/research/Statistics;->mDuringRepeatedDeleteKeysCounter:Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

    new-instance v0, Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

    invoke-direct {v0}, Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;-><init>()V

    iput-object v0, p0, Lcom/android/inputmethod/research/Statistics;->mAfterDeleteKeyCounter:Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

    invoke-virtual {p0}, Lcom/android/inputmethod/research/Statistics;->reset()V

    return-void
.end method

.method public static getInstance()Lcom/android/inputmethod/research/Statistics;
    .locals 1

    sget-object v0, Lcom/android/inputmethod/research/Statistics;->sInstance:Lcom/android/inputmethod/research/Statistics;

    return-object v0
.end method


# virtual methods
.method public reset()V
    .locals 3

    const/4 v2, 0x0

    iput v2, p0, Lcom/android/inputmethod/research/Statistics;->mCharCount:I

    iput v2, p0, Lcom/android/inputmethod/research/Statistics;->mLetterCount:I

    iput v2, p0, Lcom/android/inputmethod/research/Statistics;->mNumberCount:I

    iput v2, p0, Lcom/android/inputmethod/research/Statistics;->mSpaceCount:I

    iput v2, p0, Lcom/android/inputmethod/research/Statistics;->mDeleteKeyCount:I

    iput v2, p0, Lcom/android/inputmethod/research/Statistics;->mWordCount:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/inputmethod/research/Statistics;->mIsEmptyUponStarting:Z

    iput-boolean v2, p0, Lcom/android/inputmethod/research/Statistics;->mIsEmptinessStateKnown:Z

    iget-object v0, p0, Lcom/android/inputmethod/research/Statistics;->mKeyCounter:Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

    invoke-virtual {v0}, Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;->reset()V

    iget-object v0, p0, Lcom/android/inputmethod/research/Statistics;->mBeforeDeleteKeyCounter:Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

    invoke-virtual {v0}, Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;->reset()V

    iget-object v0, p0, Lcom/android/inputmethod/research/Statistics;->mDuringRepeatedDeleteKeysCounter:Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

    invoke-virtual {v0}, Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;->reset()V

    iget-object v0, p0, Lcom/android/inputmethod/research/Statistics;->mAfterDeleteKeyCounter:Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;

    invoke-virtual {v0}, Lcom/android/inputmethod/research/Statistics$AverageTimeCounter;->reset()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/android/inputmethod/research/Statistics;->mLastTapTime:J

    iput-boolean v2, p0, Lcom/android/inputmethod/research/Statistics;->mIsLastKeyDeleteKey:Z

    return-void
.end method
