.class public Lcom/android/contacts/model/account/BaseAccountType$PhoneActionInflater;
.super Lcom/android/contacts/model/account/BaseAccountType$CommonInflater;
.source "BaseAccountType.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/contacts/model/account/BaseAccountType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PhoneActionInflater"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/contacts/model/account/BaseAccountType$CommonInflater;-><init>()V

    return-void
.end method


# virtual methods
.method protected getTypeLabelResource(Ljava/lang/Integer;)I
    .locals 2
    .param p1    # Ljava/lang/Integer;

    const v0, 0x7f0b0117    # com.android.contacts.R.string.call_other

    if-nez p1, :cond_0

    :goto_0
    :pswitch_0
    return v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    const v0, 0x7f0b0110    # com.android.contacts.R.string.call_custom

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0b0111    # com.android.contacts.R.string.call_home

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0b0112    # com.android.contacts.R.string.call_mobile

    goto :goto_0

    :pswitch_3
    const v0, 0x7f0b0113    # com.android.contacts.R.string.call_work

    goto :goto_0

    :pswitch_4
    const v0, 0x7f0b0114    # com.android.contacts.R.string.call_fax_work

    goto :goto_0

    :pswitch_5
    const v0, 0x7f0b0115    # com.android.contacts.R.string.call_fax_home

    goto :goto_0

    :pswitch_6
    const v0, 0x7f0b0116    # com.android.contacts.R.string.call_pager

    goto :goto_0

    :pswitch_7
    const v0, 0x7f0b0118    # com.android.contacts.R.string.call_callback

    goto :goto_0

    :pswitch_8
    const v0, 0x7f0b0119    # com.android.contacts.R.string.call_car

    goto :goto_0

    :pswitch_9
    const v0, 0x7f0b011a    # com.android.contacts.R.string.call_company_main

    goto :goto_0

    :pswitch_a
    const v0, 0x7f0b011b    # com.android.contacts.R.string.call_isdn

    goto :goto_0

    :pswitch_b
    const v0, 0x7f0b011c    # com.android.contacts.R.string.call_main

    goto :goto_0

    :pswitch_c
    const v0, 0x7f0b011d    # com.android.contacts.R.string.call_other_fax

    goto :goto_0

    :pswitch_d
    const v0, 0x7f0b011e    # com.android.contacts.R.string.call_radio

    goto :goto_0

    :pswitch_e
    const v0, 0x7f0b011f    # com.android.contacts.R.string.call_telex

    goto :goto_0

    :pswitch_f
    const v0, 0x7f0b0120    # com.android.contacts.R.string.call_tty_tdd

    goto :goto_0

    :pswitch_10
    const v0, 0x7f0b0121    # com.android.contacts.R.string.call_work_mobile

    goto :goto_0

    :pswitch_11
    const v0, 0x7f0b0122    # com.android.contacts.R.string.call_work_pager

    goto :goto_0

    :pswitch_12
    const v0, 0x7f0b0123    # com.android.contacts.R.string.call_assistant

    goto :goto_0

    :pswitch_13
    const v0, 0x7f0b0124    # com.android.contacts.R.string.call_mms

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch
.end method

.method protected isCustom(Ljava/lang/Integer;)Z
    .locals 2
    .param p1    # Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/16 v1, 0x13

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
