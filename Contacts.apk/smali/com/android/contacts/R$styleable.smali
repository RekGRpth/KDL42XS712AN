.class public final Lcom/android/contacts/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/contacts/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final CallLog:[I

.field public static final ContactBrowser:[I

.field public static final ContactListItemView:[I

.field public static final EdgeTriggerView:[I

.field public static final Favorites:[I

.field public static final InterpolatingLayout_Layout:[I

.field public static final Mapping:[I

.field public static final ProportionalLayout:[I

.field public static final VoicemailStatus:[I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x5

    const/4 v3, 0x2

    new-array v0, v4, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/contacts/R$styleable;->CallLog:[I

    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/contacts/R$styleable;->ContactBrowser:[I

    const/16 v0, 0x1b

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/android/contacts/R$styleable;->ContactListItemView:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/android/contacts/R$styleable;->EdgeTriggerView:[I

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f01003c    # com.android.contacts.R.attr.favorites_padding_bottom

    aput v2, v0, v1

    sput-object v0, Lcom/android/contacts/R$styleable;->Favorites:[I

    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/android/contacts/R$styleable;->InterpolatingLayout_Layout:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/android/contacts/R$styleable;->Mapping:[I

    new-array v0, v3, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/android/contacts/R$styleable;->ProportionalLayout:[I

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_7

    sput-object v0, Lcom/android/contacts/R$styleable;->VoicemailStatus:[I

    return-void

    :array_0
    .array-data 4
        0x7f010033    # com.android.contacts.R.attr.call_log_primary_text_color
        0x7f010034    # com.android.contacts.R.attr.call_log_primary_background_color
        0x7f010035    # com.android.contacts.R.attr.call_log_secondary_text_color
        0x7f010036    # com.android.contacts.R.attr.call_log_secondary_background_color
        0x7f010037    # com.android.contacts.R.attr.call_log_header_color
    .end array-data

    :array_1
    .array-data 4
        0x7f010015    # com.android.contacts.R.attr.contact_browser_list_padding_left
        0x7f010016    # com.android.contacts.R.attr.contact_browser_list_padding_right
        0x7f010017    # com.android.contacts.R.attr.contact_browser_background
    .end array-data

    :array_2
    .array-data 4
        0x7f010018    # com.android.contacts.R.attr.list_item_height
        0x7f010019    # com.android.contacts.R.attr.list_section_header_height
        0x7f01001a    # com.android.contacts.R.attr.activated_background
        0x7f01001b    # com.android.contacts.R.attr.section_header_background
        0x7f01001c    # com.android.contacts.R.attr.list_item_divider
        0x7f01001d    # com.android.contacts.R.attr.list_item_padding_top
        0x7f01001e    # com.android.contacts.R.attr.list_item_padding_right
        0x7f01001f    # com.android.contacts.R.attr.list_item_padding_bottom
        0x7f010020    # com.android.contacts.R.attr.list_item_padding_left
        0x7f010021    # com.android.contacts.R.attr.list_item_gap_between_image_and_text
        0x7f010022    # com.android.contacts.R.attr.list_item_gap_between_label_and_data
        0x7f010023    # com.android.contacts.R.attr.list_item_presence_icon_margin
        0x7f010024    # com.android.contacts.R.attr.list_item_presence_icon_size
        0x7f010025    # com.android.contacts.R.attr.list_item_photo_size
        0x7f010026    # com.android.contacts.R.attr.list_item_profile_photo_size
        0x7f010027    # com.android.contacts.R.attr.list_item_prefix_highlight_color
        0x7f010028    # com.android.contacts.R.attr.list_item_header_text_indent
        0x7f010029    # com.android.contacts.R.attr.list_item_header_text_color
        0x7f01002a    # com.android.contacts.R.attr.list_item_header_text_size
        0x7f01002b    # com.android.contacts.R.attr.list_item_header_height
        0x7f01002c    # com.android.contacts.R.attr.list_item_header_underline_height
        0x7f01002d    # com.android.contacts.R.attr.list_item_header_underline_color
        0x7f01002e    # com.android.contacts.R.attr.list_item_contacts_count_text_color
        0x7f01002f    # com.android.contacts.R.attr.list_item_text_indent
        0x7f010030    # com.android.contacts.R.attr.list_item_contacts_count_text_size
        0x7f010031    # com.android.contacts.R.attr.list_item_data_width_weight
        0x7f010032    # com.android.contacts.R.attr.list_item_label_width_weight
    .end array-data

    :array_3
    .array-data 4
        0x7f010005    # com.android.contacts.R.attr.edgeWidth
        0x7f010006    # com.android.contacts.R.attr.listenEdges
    .end array-data

    :array_4
    .array-data 4
        0x7f010007    # com.android.contacts.R.attr.layout_narrowParentWidth
        0x7f010008    # com.android.contacts.R.attr.layout_narrowWidth
        0x7f010009    # com.android.contacts.R.attr.layout_narrowMarginLeft
        0x7f01000a    # com.android.contacts.R.attr.layout_narrowMarginRight
        0x7f01000b    # com.android.contacts.R.attr.layout_narrowPaddingLeft
        0x7f01000c    # com.android.contacts.R.attr.layout_narrowPaddingRight
        0x7f01000d    # com.android.contacts.R.attr.layout_wideParentWidth
        0x7f01000e    # com.android.contacts.R.attr.layout_wideWidth
        0x7f01000f    # com.android.contacts.R.attr.layout_wideMarginLeft
        0x7f010010    # com.android.contacts.R.attr.layout_wideMarginRight
        0x7f010011    # com.android.contacts.R.attr.layout_widePaddingLeft
        0x7f010012    # com.android.contacts.R.attr.layout_widePaddingRight
    .end array-data

    :array_5
    .array-data 4
        0x7f010000    # com.android.contacts.R.attr.mimeType
        0x7f010001    # com.android.contacts.R.attr.remoteViews
        0x7f010002    # com.android.contacts.R.attr.icon
        0x7f010003    # com.android.contacts.R.attr.summaryColumn
        0x7f010004    # com.android.contacts.R.attr.detailColumn
    .end array-data

    :array_6
    .array-data 4
        0x7f010013    # com.android.contacts.R.attr.direction
        0x7f010014    # com.android.contacts.R.attr.ratio
    .end array-data

    :array_7
    .array-data 4
        0x7f010038    # com.android.contacts.R.attr.call_log_voicemail_status_height
        0x7f010039    # com.android.contacts.R.attr.call_log_voicemail_status_background_color
        0x7f01003a    # com.android.contacts.R.attr.call_log_voicemail_status_text_color
        0x7f01003b    # com.android.contacts.R.attr.call_log_voicemail_status_action_text_color
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
