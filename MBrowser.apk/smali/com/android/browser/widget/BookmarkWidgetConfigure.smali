.class public Lcom/android/browser/widget/BookmarkWidgetConfigure;
.super Landroid/app/ListActivity;
.source "BookmarkWidgetConfigure.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/browser/widget/BookmarkWidgetConfigure$AccountsLoader;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/app/ListActivity;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private mAccountAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Lcom/android/browser/AddBookmarkPage$BookmarkAccount;",
            ">;"
        }
    .end annotation
.end field

.field private mAppWidgetId:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/browser/widget/BookmarkWidgetConfigure;->mAppWidgetId:I

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    invoke-virtual {p0}, Lcom/android/browser/widget/BookmarkWidgetConfigure;->finish()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x0

    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0, v4}, Lcom/android/browser/widget/BookmarkWidgetConfigure;->setResult(I)V

    invoke-virtual {p0, v4}, Lcom/android/browser/widget/BookmarkWidgetConfigure;->setVisible(Z)V

    const v2, 0x7f040039    # com.android.browser.R.layout.widget_account_selection

    invoke-virtual {p0, v2}, Lcom/android/browser/widget/BookmarkWidgetConfigure;->setContentView(I)V

    const v2, 0x7f0d0038    # com.android.browser.R.id.cancel

    invoke-virtual {p0, v2}, Lcom/android/browser/widget/BookmarkWidgetConfigure;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v2, Landroid/widget/ArrayAdapter;

    const v3, 0x1090003    # android.R.layout.simple_list_item_1

    invoke-direct {v2, p0, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/android/browser/widget/BookmarkWidgetConfigure;->mAccountAdapter:Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lcom/android/browser/widget/BookmarkWidgetConfigure;->mAccountAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {p0, v2}, Lcom/android/browser/widget/BookmarkWidgetConfigure;->setListAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Lcom/android/browser/widget/BookmarkWidgetConfigure;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v2, "appWidgetId"

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/android/browser/widget/BookmarkWidgetConfigure;->mAppWidgetId:I

    :cond_0
    iget v2, p0, Lcom/android/browser/widget/BookmarkWidgetConfigure;->mAppWidgetId:I

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/android/browser/widget/BookmarkWidgetConfigure;->finish()V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/browser/widget/BookmarkWidgetConfigure;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    goto :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 1
    .param p1    # I
    .param p2    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/android/browser/widget/BookmarkWidgetConfigure$AccountsLoader;

    invoke-direct {v0, p0}, Lcom/android/browser/widget/BookmarkWidgetConfigure$AccountsLoader;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method protected onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 3
    .param p1    # Landroid/widget/ListView;
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J

    iget-object v1, p0, Lcom/android/browser/widget/BookmarkWidgetConfigure;->mAccountAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, p3}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/browser/AddBookmarkPage$BookmarkAccount;

    iget-wide v1, v0, Lcom/android/browser/AddBookmarkPage$BookmarkAccount;->rootFolderId:J

    invoke-virtual {p0, v1, v2}, Lcom/android/browser/widget/BookmarkWidgetConfigure;->pickAccount(J)V

    return-void
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 3
    .param p2    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    const/4 v2, 0x1

    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ge v0, v2, :cond_1

    :cond_0
    const-wide/16 v0, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/android/browser/widget/BookmarkWidgetConfigure;->pickAccount(J)V

    :goto_0
    invoke-virtual {p0}, Lcom/android/browser/widget/BookmarkWidgetConfigure;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/LoaderManager;->destroyLoader(I)V

    return-void

    :cond_1
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v0, v2, :cond_2

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    const/4 v0, 0x2

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/android/browser/widget/BookmarkWidgetConfigure;->pickAccount(J)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/browser/widget/BookmarkWidgetConfigure;->mAccountAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    :goto_1
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/browser/widget/BookmarkWidgetConfigure;->mAccountAdapter:Landroid/widget/ArrayAdapter;

    new-instance v1, Lcom/android/browser/AddBookmarkPage$BookmarkAccount;

    invoke-direct {v1, p0, p2}, Lcom/android/browser/AddBookmarkPage$BookmarkAccount;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    goto :goto_1

    :cond_3
    invoke-virtual {p0, v2}, Lcom/android/browser/widget/BookmarkWidgetConfigure;->setVisible(Z)V

    goto :goto_0
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/content/Loader;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/android/browser/widget/BookmarkWidgetConfigure;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.method pickAccount(J)V
    .locals 3
    .param p1    # J

    iget v1, p0, Lcom/android/browser/widget/BookmarkWidgetConfigure;->mAppWidgetId:I

    invoke-static {p0, v1, p1, p2}, Lcom/android/browser/widget/BookmarkThumbnailWidgetService;->setupWidgetState(Landroid/content/Context;IJ)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "appWidgetId"

    iget v2, p0, Lcom/android/browser/widget/BookmarkWidgetConfigure;->mAppWidgetId:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/android/browser/widget/BookmarkWidgetConfigure;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/android/browser/widget/BookmarkWidgetConfigure;->finish()V

    return-void
.end method
