.class public Lcom/android/browser/NavTabScroller;
.super Lcom/android/browser/view/ScrollerView;
.source "NavTabScroller.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/browser/NavTabScroller$ContentLayout;,
        Lcom/android/browser/NavTabScroller$OnLayoutListener;,
        Lcom/android/browser/NavTabScroller$OnRemoveListener;
    }
.end annotation


# static fields
.field static final PULL_FACTOR:[F


# instance fields
.field private mAdapter:Landroid/widget/BaseAdapter;

.field private mAnimator:Landroid/animation/AnimatorSet;

.field private mContentView:Lcom/android/browser/NavTabScroller$ContentLayout;

.field mCubic:Landroid/view/animation/DecelerateInterpolator;

.field private mFlingVelocity:F

.field private mGap:I

.field private mGapAnimator:Landroid/animation/ObjectAnimator;

.field private mGapPosition:I

.field private mLayoutListener:Lcom/android/browser/NavTabScroller$OnLayoutListener;

.field private mNeedsScroll:Z

.field mPullValue:I

.field private mRemoveListener:Lcom/android/browser/NavTabScroller$OnRemoveListener;

.field private mScrollPosition:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/browser/NavTabScroller;->PULL_FACTOR:[F

    return-void

    nop

    :array_0
    .array-data 4
        0x40200000    # 2.5f
        0x3f666666    # 0.9f
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/android/browser/view/ScrollerView;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, p1}, Lcom/android/browser/NavTabScroller;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/browser/view/ScrollerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p1}, Lcom/android/browser/NavTabScroller;->init(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/android/browser/view/ScrollerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p1}, Lcom/android/browser/NavTabScroller;->init(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$000(Lcom/android/browser/NavTabScroller;)Lcom/android/browser/NavTabScroller$OnRemoveListener;
    .locals 1
    .param p0    # Lcom/android/browser/NavTabScroller;

    iget-object v0, p0, Lcom/android/browser/NavTabScroller;->mRemoveListener:Lcom/android/browser/NavTabScroller$OnRemoveListener;

    return-object v0
.end method

.method static synthetic access$102(Lcom/android/browser/NavTabScroller;Landroid/animation/AnimatorSet;)Landroid/animation/AnimatorSet;
    .locals 0
    .param p0    # Lcom/android/browser/NavTabScroller;
    .param p1    # Landroid/animation/AnimatorSet;

    iput-object p1, p0, Lcom/android/browser/NavTabScroller;->mAnimator:Landroid/animation/AnimatorSet;

    return-object p1
.end method

.method static synthetic access$202(Lcom/android/browser/NavTabScroller;I)I
    .locals 0
    .param p0    # Lcom/android/browser/NavTabScroller;
    .param p1    # I

    iput p1, p0, Lcom/android/browser/NavTabScroller;->mGapPosition:I

    return p1
.end method

.method static synthetic access$302(Lcom/android/browser/NavTabScroller;I)I
    .locals 0
    .param p0    # Lcom/android/browser/NavTabScroller;
    .param p1    # I

    iput p1, p0, Lcom/android/browser/NavTabScroller;->mGap:I

    return p1
.end method

.method private adjustViewGap(Landroid/view/View;I)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # I

    iget v0, p0, Lcom/android/browser/NavTabScroller;->mGap:I

    if-gez v0, :cond_0

    iget v0, p0, Lcom/android/browser/NavTabScroller;->mGapPosition:I

    if-gt p2, v0, :cond_1

    :cond_0
    iget v0, p0, Lcom/android/browser/NavTabScroller;->mGap:I

    if-lez v0, :cond_2

    iget v0, p0, Lcom/android/browser/NavTabScroller;->mGapPosition:I

    if-ge p2, v0, :cond_2

    :cond_1
    iget-boolean v0, p0, Lcom/android/browser/NavTabScroller;->mHorizontal:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/android/browser/NavTabScroller;->mGap:I

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationX(F)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget v0, p0, Lcom/android/browser/NavTabScroller;->mGap:I

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_0
.end method

.method private animateOut(Landroid/view/View;F)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # F

    iget-boolean v1, p0, Lcom/android/browser/NavTabScroller;->mHorizontal:Z

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTranslationY()F

    move-result v0

    :goto_0
    invoke-direct {p0, p1, p2, v0}, Lcom/android/browser/NavTabScroller;->animateOut(Landroid/view/View;FF)V

    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result v0

    goto :goto_0
.end method

.method private animateOut(Landroid/view/View;FF)V
    .locals 26
    .param p1    # Landroid/view/View;
    .param p2    # F
    .param p3    # F

    if-eqz p1, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/browser/NavTabScroller;->mAnimator:Landroid/animation/AnimatorSet;

    move-object/from16 v22, v0

    if-eqz v22, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/browser/NavTabScroller;->mContentView:Lcom/android/browser/NavTabScroller$ContentLayout;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/android/browser/NavTabScroller$ContentLayout;->indexOfChild(Landroid/view/View;)I

    move-result v13

    const/16 v18, 0x0

    const/16 v22, 0x0

    cmpg-float v22, p2, v22

    if-gez v22, :cond_7

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/browser/NavTabScroller;->mHorizontal:Z

    move/from16 v22, v0

    if-eqz v22, :cond_6

    invoke-virtual/range {p0 .. p0}, Lcom/android/browser/NavTabScroller;->getHeight()I

    move-result v22

    move/from16 v0, v22

    neg-int v0, v0

    move/from16 v18, v0

    :goto_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/browser/NavTabScroller;->mHorizontal:Z

    move/from16 v22, v0

    if-eqz v22, :cond_9

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTop()I

    move-result v22

    :goto_2
    sub-int v6, v18, v22

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v22

    move/from16 v0, v22

    mul-int/lit16 v0, v0, 0x3e8

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    invoke-static/range {p2 .. p2}, Ljava/lang/Math;->abs(F)F

    move-result v23

    div-float v22, v22, v23

    move/from16 v0, v22

    float-to-long v7, v0

    const/4 v14, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/browser/NavTabScroller;->mHorizontal:Z

    move/from16 v22, v0

    if-eqz v22, :cond_a

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getWidth()I

    move-result v10

    :goto_3
    invoke-direct/range {p0 .. p1}, Lcom/android/browser/NavTabScroller;->getViewCenter(Landroid/view/View;)I

    move-result v5

    invoke-direct/range {p0 .. p0}, Lcom/android/browser/NavTabScroller;->getScreenCenter()I

    move-result v4

    const/4 v11, -0x1

    div-int/lit8 v22, v10, 0x2

    sub-int v22, v4, v22

    move/from16 v0, v22

    if-ge v5, v0, :cond_c

    sub-int v22, v4, v5

    sub-int v22, v22, v10

    move/from16 v0, v22

    neg-int v14, v0

    if-lez v13, :cond_b

    move/from16 v21, v10

    :goto_4
    move v11, v13

    :cond_2
    :goto_5
    move-object/from16 v0, p0

    iput v13, v0, Lcom/android/browser/NavTabScroller;->mGapPosition:I

    move v12, v11

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/browser/NavTabScroller;->mHorizontal:Z

    move/from16 v22, v0

    if-eqz v22, :cond_f

    sget-object v22, Lcom/android/browser/NavTabScroller;->TRANSLATION_Y:Landroid/util/Property;

    :goto_6
    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [F

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput p3, v23, v24

    const/16 v24, 0x1

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v25, v0

    aput v25, v23, v24

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v19

    sget-object v22, Lcom/android/browser/NavTabScroller;->ALPHA:Landroid/util/Property;

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [F

    move-object/from16 v23, v0

    const/16 v24, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/android/browser/NavTabScroller;->getAlpha(Landroid/view/View;F)F

    move-result v25

    aput v25, v23, v24

    const/16 v24, 0x1

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v25

    invoke-direct {v0, v1, v2}, Lcom/android/browser/NavTabScroller;->getAlpha(Landroid/view/View;F)F

    move-result v25

    aput v25, v23, v24

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    new-instance v16, Landroid/animation/AnimatorSet;

    invoke-direct/range {v16 .. v16}, Landroid/animation/AnimatorSet;-><init>()V

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [Landroid/animation/Animator;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object v19, v22, v23

    const/16 v23, 0x1

    aput-object v3, v22, v23

    move-object/from16 v0, v16

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v7, v8}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    new-instance v22, Landroid/animation/AnimatorSet;

    invoke-direct/range {v22 .. v22}, Landroid/animation/AnimatorSet;-><init>()V

    move-object/from16 v0, v22

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/browser/NavTabScroller;->mAnimator:Landroid/animation/AnimatorSet;

    const/16 v20, 0x0

    const/4 v15, 0x0

    if-eqz v14, :cond_3

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/browser/NavTabScroller;->mHorizontal:Z

    move/from16 v22, v0

    if-eqz v22, :cond_10

    const-string v22, "scrollX"

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [I

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/android/browser/NavTabScroller;->getScrollX()I

    move-result v25

    aput v25, v23, v24

    const/16 v24, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/android/browser/NavTabScroller;->getScrollX()I

    move-result v25

    add-int v25, v25, v14

    aput v25, v23, v24

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v15

    :cond_3
    :goto_7
    if-eqz v21, :cond_4

    const-string v22, "gap"

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [I

    move-object/from16 v23, v0

    const/16 v24, 0x0

    const/16 v25, 0x0

    aput v25, v23, v24

    const/16 v24, 0x1

    aput v21, v23, v24

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v20

    :cond_4
    const/16 v9, 0xc8

    if-eqz v15, :cond_12

    if-eqz v20, :cond_11

    new-instance v17, Landroid/animation/AnimatorSet;

    invoke-direct/range {v17 .. v17}, Landroid/animation/AnimatorSet;-><init>()V

    const/16 v22, 0x2

    move/from16 v0, v22

    new-array v0, v0, [Landroid/animation/Animator;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    aput-object v15, v22, v23

    const/16 v23, 0x1

    aput-object v20, v22, v23

    move-object/from16 v0, v17

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    const-wide/16 v22, 0xc8

    move-object/from16 v0, v17

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/browser/NavTabScroller;->mAnimator:Landroid/animation/AnimatorSet;

    move-object/from16 v22, v0

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [Landroid/animation/Animator;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput-object v16, v23, v24

    const/16 v24, 0x1

    aput-object v17, v23, v24

    invoke-virtual/range {v22 .. v23}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    :cond_5
    :goto_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/browser/NavTabScroller;->mAnimator:Landroid/animation/AnimatorSet;

    move-object/from16 v22, v0

    new-instance v23, Lcom/android/browser/NavTabScroller$2;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v13, v12}, Lcom/android/browser/NavTabScroller$2;-><init>(Lcom/android/browser/NavTabScroller;II)V

    invoke-virtual/range {v22 .. v23}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/browser/NavTabScroller;->mAnimator:Landroid/animation/AnimatorSet;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/animation/AnimatorSet;->start()V

    goto/16 :goto_0

    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/android/browser/NavTabScroller;->getWidth()I

    move-result v22

    move/from16 v0, v22

    neg-int v0, v0

    move/from16 v18, v0

    goto/16 :goto_1

    :cond_7
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/android/browser/NavTabScroller;->mHorizontal:Z

    move/from16 v22, v0

    if-eqz v22, :cond_8

    invoke-virtual/range {p0 .. p0}, Lcom/android/browser/NavTabScroller;->getHeight()I

    move-result v18

    :goto_9
    goto/16 :goto_1

    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/android/browser/NavTabScroller;->getWidth()I

    move-result v18

    goto :goto_9

    :cond_9
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getLeft()I

    move-result v22

    goto/16 :goto_2

    :cond_a
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getHeight()I

    move-result v10

    goto/16 :goto_3

    :cond_b
    const/16 v21, 0x0

    goto/16 :goto_4

    :cond_c
    div-int/lit8 v22, v10, 0x2

    add-int v22, v22, v4

    move/from16 v0, v22

    if-le v5, v0, :cond_d

    add-int v22, v4, v10

    sub-int v22, v22, v5

    move/from16 v0, v22

    neg-int v14, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/browser/NavTabScroller;->mAdapter:Landroid/widget/BaseAdapter;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v22

    add-int/lit8 v22, v22, -0x1

    move/from16 v0, v22

    if-ge v13, v0, :cond_2

    neg-int v0, v10

    move/from16 v21, v0

    goto/16 :goto_5

    :cond_d
    sub-int v22, v4, v5

    move/from16 v0, v22

    neg-int v14, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/browser/NavTabScroller;->mAdapter:Landroid/widget/BaseAdapter;

    move-object/from16 v22, v0

    invoke-virtual/range {v22 .. v22}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v22

    add-int/lit8 v22, v22, -0x1

    move/from16 v0, v22

    if-ge v13, v0, :cond_e

    neg-int v0, v10

    move/from16 v21, v0

    goto/16 :goto_5

    :cond_e
    sub-int/2addr v14, v10

    goto/16 :goto_5

    :cond_f
    sget-object v22, Lcom/android/browser/NavTabScroller;->TRANSLATION_X:Landroid/util/Property;

    goto/16 :goto_6

    :cond_10
    const-string v22, "scrollY"

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [I

    move-object/from16 v23, v0

    const/16 v24, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/android/browser/NavTabScroller;->getScrollY()I

    move-result v25

    aput v25, v23, v24

    const/16 v24, 0x1

    invoke-virtual/range {p0 .. p0}, Lcom/android/browser/NavTabScroller;->getScrollY()I

    move-result v25

    add-int v25, v25, v14

    aput v25, v23, v24

    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move-object/from16 v2, v23

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v15

    goto/16 :goto_7

    :cond_11
    const-wide/16 v22, 0xc8

    move-wide/from16 v0, v22

    invoke-virtual {v15, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/browser/NavTabScroller;->mAnimator:Landroid/animation/AnimatorSet;

    move-object/from16 v22, v0

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [Landroid/animation/Animator;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput-object v16, v23, v24

    const/16 v24, 0x1

    aput-object v15, v23, v24

    invoke-virtual/range {v22 .. v23}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    goto/16 :goto_8

    :cond_12
    if-eqz v20, :cond_5

    const-wide/16 v22, 0xc8

    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-virtual {v0, v1, v2}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/browser/NavTabScroller;->mAnimator:Landroid/animation/AnimatorSet;

    move-object/from16 v22, v0

    const/16 v23, 0x2

    move/from16 v0, v23

    new-array v0, v0, [Landroid/animation/Animator;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aput-object v16, v23, v24

    const/16 v24, 0x1

    aput-object v20, v23, v24

    invoke-virtual/range {v22 .. v23}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    goto/16 :goto_8
.end method

.method private calcPadding()V
    .locals 5

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/android/browser/NavTabScroller;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v2}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/android/browser/NavTabScroller;->mContentView:Lcom/android/browser/NavTabScroller$ContentLayout;

    invoke-virtual {v2, v4}, Lcom/android/browser/NavTabScroller$ContentLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    iget-boolean v2, p0, Lcom/android/browser/NavTabScroller;->mHorizontal:Z

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/android/browser/NavTabScroller;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/lit8 v0, v2, 0x2

    iget-object v2, p0, Lcom/android/browser/NavTabScroller;->mContentView:Lcom/android/browser/NavTabScroller$ContentLayout;

    invoke-virtual {v2, v0, v4, v0, v4}, Lcom/android/browser/NavTabScroller$ContentLayout;->setPadding(IIII)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/browser/NavTabScroller;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/lit8 v0, v2, 0x2

    iget-object v2, p0, Lcom/android/browser/NavTabScroller;->mContentView:Lcom/android/browser/NavTabScroller$ContentLayout;

    invoke-virtual {v2, v4, v0, v4, v0}, Lcom/android/browser/NavTabScroller$ContentLayout;->setPadding(IIII)V

    goto :goto_0
.end method

.method private ease(Landroid/view/animation/DecelerateInterpolator;FFFF)F
    .locals 1
    .param p1    # Landroid/view/animation/DecelerateInterpolator;
    .param p2    # F
    .param p3    # F
    .param p4    # F
    .param p5    # F

    div-float v0, p2, p5

    invoke-virtual {p1, v0}, Landroid/view/animation/DecelerateInterpolator;->getInterpolation(F)F

    move-result v0

    mul-float/2addr v0, p4

    add-float/2addr v0, p3

    return v0
.end method

.method private getAlpha(Landroid/view/View;F)F
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # F

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget-boolean v0, p0, Lcom/android/browser/NavTabScroller;->mHorizontal:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    :goto_0
    int-to-float v0, v0

    div-float v0, v2, v0

    sub-float v0, v1, v0

    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    goto :goto_0
.end method

.method private getScreenCenter()I
    .locals 2

    iget-boolean v0, p0, Lcom/android/browser/NavTabScroller;->mHorizontal:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/browser/NavTabScroller;->getScrollX()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/browser/NavTabScroller;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/android/browser/NavTabScroller;->getScrollY()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/browser/NavTabScroller;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method private getViewCenter(Landroid/view/View;)I
    .locals 2
    .param p1    # Landroid/view/View;

    iget-boolean v0, p0, Lcom/android/browser/NavTabScroller;->mHorizontal:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method private init(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    const/4 v3, -0x1

    const/4 v2, 0x0

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x3fc00000    # 1.5f

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v0, p0, Lcom/android/browser/NavTabScroller;->mCubic:Landroid/view/animation/DecelerateInterpolator;

    iput v3, p0, Lcom/android/browser/NavTabScroller;->mGapPosition:I

    invoke-virtual {p0, v2}, Lcom/android/browser/NavTabScroller;->setHorizontalScrollBarEnabled(Z)V

    invoke-virtual {p0, v2}, Lcom/android/browser/NavTabScroller;->setVerticalScrollBarEnabled(Z)V

    new-instance v0, Lcom/android/browser/NavTabScroller$ContentLayout;

    invoke-direct {v0, p1, p0}, Lcom/android/browser/NavTabScroller$ContentLayout;-><init>(Landroid/content/Context;Lcom/android/browser/NavTabScroller;)V

    iput-object v0, p0, Lcom/android/browser/NavTabScroller;->mContentView:Lcom/android/browser/NavTabScroller$ContentLayout;

    iget-object v0, p0, Lcom/android/browser/NavTabScroller;->mContentView:Lcom/android/browser/NavTabScroller$ContentLayout;

    invoke-virtual {v0, v2}, Lcom/android/browser/NavTabScroller$ContentLayout;->setOrientation(I)V

    iget-object v0, p0, Lcom/android/browser/NavTabScroller;->mContentView:Lcom/android/browser/NavTabScroller$ContentLayout;

    invoke-virtual {p0, v0}, Lcom/android/browser/NavTabScroller;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/android/browser/NavTabScroller;->mContentView:Lcom/android/browser/NavTabScroller$ContentLayout;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/android/browser/NavTabScroller$ContentLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0}, Lcom/android/browser/NavTabScroller;->getGap()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/browser/NavTabScroller;->setGap(I)V

    invoke-virtual {p0}, Lcom/android/browser/NavTabScroller;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const v1, 0x44bb8000    # 1500.0f

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/android/browser/NavTabScroller;->mFlingVelocity:F

    return-void
.end method

.method private offsetView(Landroid/view/View;F)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # F

    invoke-direct {p0, p1, p2}, Lcom/android/browser/NavTabScroller;->getAlpha(Landroid/view/View;F)F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    iget-boolean v0, p0, Lcom/android/browser/NavTabScroller;->mHorizontal:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1, p2}, Landroid/view/View;->setTranslationY(F)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1, p2}, Landroid/view/View;->setTranslationX(F)V

    goto :goto_0
.end method


# virtual methods
.method adjustGap()V
    .locals 3

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/android/browser/NavTabScroller;->mContentView:Lcom/android/browser/NavTabScroller$ContentLayout;

    invoke-virtual {v2}, Lcom/android/browser/NavTabScroller$ContentLayout;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lcom/android/browser/NavTabScroller;->mContentView:Lcom/android/browser/NavTabScroller$ContentLayout;

    invoke-virtual {v2, v1}, Lcom/android/browser/NavTabScroller$ContentLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lcom/android/browser/NavTabScroller;->adjustViewGap(Landroid/view/View;I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected animateOut(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/android/browser/NavTabScroller;->mFlingVelocity:F

    neg-float v0, v0

    invoke-direct {p0, p1, v0}, Lcom/android/browser/NavTabScroller;->animateOut(Landroid/view/View;F)V

    goto :goto_0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1    # Landroid/graphics/Canvas;

    iget v0, p0, Lcom/android/browser/NavTabScroller;->mGapPosition:I

    const/4 v1, -0x1

    if-le v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/browser/NavTabScroller;->adjustGap()V

    :cond_0
    invoke-super {p0, p1}, Lcom/android/browser/view/ScrollerView;->draw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method protected findViewAt(II)Landroid/view/View;
    .locals 4
    .param p1    # I
    .param p2    # I

    iget v3, p0, Lcom/android/browser/NavTabScroller;->mScrollX:I

    add-int/2addr p1, v3

    iget v3, p0, Lcom/android/browser/NavTabScroller;->mScrollY:I

    add-int/2addr p2, v3

    iget-object v3, p0, Lcom/android/browser/NavTabScroller;->mContentView:Lcom/android/browser/NavTabScroller$ContentLayout;

    invoke-virtual {v3}, Lcom/android/browser/NavTabScroller$ContentLayout;->getChildCount()I

    move-result v1

    add-int/lit8 v2, v1, -0x1

    :goto_0
    if-ltz v2, :cond_1

    iget-object v3, p0, Lcom/android/browser/NavTabScroller;->mContentView:Lcom/android/browser/NavTabScroller$ContentLayout;

    invoke-virtual {v3, v2}, Lcom/android/browser/NavTabScroller$ContentLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v3

    if-lt p1, v3, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v3

    if-ge p1, v3, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v3

    if-lt p2, v3, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v3

    if-ge p2, v3, :cond_0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected finishScroller()V
    .locals 2

    iget-object v0, p0, Lcom/android/browser/NavTabScroller;->mScroller:Landroid/widget/OverScroller;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/OverScroller;->forceFinished(Z)V

    return-void
.end method

.method public getGap()I
    .locals 1

    iget v0, p0, Lcom/android/browser/NavTabScroller;->mGap:I

    return v0
.end method

.method protected getScrollValue()I
    .locals 1

    iget-boolean v0, p0, Lcom/android/browser/NavTabScroller;->mHorizontal:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/browser/NavTabScroller;->mScrollX:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/android/browser/NavTabScroller;->mScrollY:I

    goto :goto_0
.end method

.method protected getTabView(I)Lcom/android/browser/NavTabView;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/browser/NavTabScroller;->mContentView:Lcom/android/browser/NavTabScroller$ContentLayout;

    invoke-virtual {v0, p1}, Lcom/android/browser/NavTabScroller$ContentLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/browser/NavTabView;

    return-object v0
.end method

.method protected handleDataChanged()V
    .locals 1

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/android/browser/NavTabScroller;->handleDataChanged(I)V

    return-void
.end method

.method handleDataChanged(I)V
    .locals 10
    .param p1    # I

    const/4 v5, 0x1

    const/4 v9, -0x1

    const/4 v8, -0x2

    invoke-virtual {p0}, Lcom/android/browser/NavTabScroller;->getScrollValue()I

    move-result v2

    iget-object v4, p0, Lcom/android/browser/NavTabScroller;->mGapAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/browser/NavTabScroller;->mGapAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v4}, Landroid/animation/ObjectAnimator;->cancel()V

    :cond_0
    iget-object v4, p0, Lcom/android/browser/NavTabScroller;->mContentView:Lcom/android/browser/NavTabScroller$ContentLayout;

    invoke-virtual {v4}, Lcom/android/browser/NavTabScroller$ContentLayout;->removeAllViews()V

    const/4 v0, 0x0

    :goto_0
    iget-object v4, p0, Lcom/android/browser/NavTabScroller;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v4}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v4

    if-ge v0, v4, :cond_3

    iget-object v4, p0, Lcom/android/browser/NavTabScroller;->mAdapter:Landroid/widget/BaseAdapter;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/android/browser/NavTabScroller;->mContentView:Lcom/android/browser/NavTabScroller$ContentLayout;

    invoke-virtual {v4, v0, v6, v7}, Landroid/widget/BaseAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v8, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-boolean v4, p0, Lcom/android/browser/NavTabScroller;->mHorizontal:Z

    if-eqz v4, :cond_2

    const/16 v4, 0x10

    :goto_1
    iput v4, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    iget-object v4, p0, Lcom/android/browser/NavTabScroller;->mContentView:Lcom/android/browser/NavTabScroller$ContentLayout;

    invoke-virtual {v4, v3, v1}, Lcom/android/browser/NavTabScroller$ContentLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget v4, p0, Lcom/android/browser/NavTabScroller;->mGapPosition:I

    if-le v4, v9, :cond_1

    invoke-direct {p0, v3, v0}, Lcom/android/browser/NavTabScroller;->adjustViewGap(Landroid/view/View;I)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v4, v5

    goto :goto_1

    :cond_3
    if-le p1, v9, :cond_4

    iget-object v4, p0, Lcom/android/browser/NavTabScroller;->mAdapter:Landroid/widget/BaseAdapter;

    invoke-virtual {v4}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-static {v4, p1}, Ljava/lang/Math;->min(II)I

    move-result p1

    iput-boolean v5, p0, Lcom/android/browser/NavTabScroller;->mNeedsScroll:Z

    iput p1, p0, Lcom/android/browser/NavTabScroller;->mScrollPosition:I

    invoke-virtual {p0}, Lcom/android/browser/NavTabScroller;->requestLayout()V

    :goto_2
    return-void

    :cond_4
    invoke-virtual {p0, v2}, Lcom/android/browser/NavTabScroller;->setScrollValue(I)V

    goto :goto_2
.end method

.method protected isHorizontal()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/browser/NavTabScroller;->mHorizontal:Z

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 3
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v2, 0x0

    invoke-super/range {p0 .. p5}, Lcom/android/browser/view/ScrollerView;->onLayout(ZIIII)V

    iget-boolean v0, p0, Lcom/android/browser/NavTabScroller;->mNeedsScroll:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/browser/NavTabScroller;->mScroller:Landroid/widget/OverScroller;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/OverScroller;->forceFinished(Z)V

    iget v0, p0, Lcom/android/browser/NavTabScroller;->mScrollPosition:I

    invoke-virtual {p0, v0, v2}, Lcom/android/browser/NavTabScroller;->snapToSelected(IZ)V

    iput-boolean v2, p0, Lcom/android/browser/NavTabScroller;->mNeedsScroll:Z

    :cond_0
    iget-object v0, p0, Lcom/android/browser/NavTabScroller;->mLayoutListener:Lcom/android/browser/NavTabScroller$OnLayoutListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/browser/NavTabScroller;->mLayoutListener:Lcom/android/browser/NavTabScroller$OnLayoutListener;

    invoke-interface {v0, p2, p3, p4, p5}, Lcom/android/browser/NavTabScroller$OnLayoutListener;->onLayout(IIII)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/browser/NavTabScroller;->mLayoutListener:Lcom/android/browser/NavTabScroller$OnLayoutListener;

    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    invoke-super {p0, p1, p2}, Lcom/android/browser/view/ScrollerView;->onMeasure(II)V

    invoke-direct {p0}, Lcom/android/browser/NavTabScroller;->calcPadding()V

    return-void
.end method

.method protected onOrthoDrag(Landroid/view/View;F)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # F

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/browser/NavTabScroller;->mAnimator:Landroid/animation/AnimatorSet;

    if-nez v0, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/android/browser/NavTabScroller;->offsetView(Landroid/view/View;F)V

    :cond_0
    return-void
.end method

.method protected onOrthoDragFinished(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget-object v1, p0, Lcom/android/browser/NavTabScroller;->mAnimator:Landroid/animation/AnimatorSet;

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v1, p0, Lcom/android/browser/NavTabScroller;->mIsOrthoDragged:Z

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    iget-boolean v1, p0, Lcom/android/browser/NavTabScroller;->mHorizontal:Z

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getTranslationY()F

    move-result v0

    :goto_1
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget-boolean v1, p0, Lcom/android/browser/NavTabScroller;->mHorizontal:Z

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    :goto_2
    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    cmpl-float v1, v2, v1

    if-lez v1, :cond_4

    invoke-static {v0}, Ljava/lang/Math;->signum(F)F

    move-result v1

    iget v2, p0, Lcom/android/browser/NavTabScroller;->mFlingVelocity:F

    mul-float/2addr v1, v2

    invoke-direct {p0, p1, v1, v0}, Lcom/android/browser/NavTabScroller;->animateOut(Landroid/view/View;FF)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getTranslationX()F

    move-result v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/android/browser/NavTabScroller;->offsetView(Landroid/view/View;F)V

    goto :goto_0
.end method

.method protected onOrthoFling(Landroid/view/View;F)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # F

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/browser/NavTabScroller;->mAnimator:Landroid/animation/AnimatorSet;

    if-nez v0, :cond_1

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/android/browser/NavTabScroller;->mFlingVelocity:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    invoke-direct {p0, p1, p2}, Lcom/android/browser/NavTabScroller;->animateOut(Landroid/view/View;F)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/browser/NavTabScroller;->offsetView(Landroid/view/View;F)V

    goto :goto_0
.end method

.method protected onPull(I)V
    .locals 20
    .param p1    # I

    const/4 v13, 0x0

    const/4 v8, 0x2

    if-nez p1, :cond_1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/browser/NavTabScroller;->mPullValue:I

    if-nez v1, :cond_1

    :cond_0
    return-void

    :cond_1
    if-nez p1, :cond_a

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/browser/NavTabScroller;->mPullValue:I

    if-eqz v1, :cond_a

    const/4 v11, 0x0

    :goto_0
    if-ge v11, v8, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/browser/NavTabScroller;->mContentView:Lcom/android/browser/NavTabScroller$ContentLayout;

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/browser/NavTabScroller;->mPullValue:I

    if-gez v1, :cond_4

    move v1, v11

    :goto_1
    invoke-virtual {v2, v1}, Lcom/android/browser/NavTabScroller$ContentLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    if-nez v7, :cond_5

    :cond_2
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lcom/android/browser/NavTabScroller;->mPullValue:I

    :goto_2
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/browser/NavTabScroller;->mHorizontal:Z

    if-eqz v1, :cond_c

    invoke-virtual/range {p0 .. p0}, Lcom/android/browser/NavTabScroller;->getWidth()I

    move-result v10

    :goto_3
    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/browser/NavTabScroller;->mPullValue:I

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v14

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/browser/NavTabScroller;->mPullValue:I

    if-gtz v1, :cond_d

    const/4 v9, 0x1

    :goto_4
    const/4 v11, 0x0

    :goto_5
    if-ge v11, v8, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/browser/NavTabScroller;->mContentView:Lcom/android/browser/NavTabScroller$ContentLayout;

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/browser/NavTabScroller;->mPullValue:I

    if-gez v1, :cond_e

    move v1, v11

    :goto_6
    invoke-virtual {v2, v1}, Lcom/android/browser/NavTabScroller$ContentLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    if-eqz v7, :cond_0

    if-eqz v13, :cond_3

    :cond_3
    sget-object v1, Lcom/android/browser/NavTabScroller;->PULL_FACTOR:[F

    aget v12, v1, v11

    neg-int v1, v9

    int-to-float v0, v1

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/browser/NavTabScroller;->mCubic:Landroid/view/animation/DecelerateInterpolator;

    int-to-float v3, v14

    const/4 v4, 0x0

    const/high16 v1, 0x40000000    # 2.0f

    mul-float v5, v12, v1

    int-to-float v6, v10

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/android/browser/NavTabScroller;->ease(Landroid/view/animation/DecelerateInterpolator;FFFF)F

    move-result v1

    mul-float v15, v19, v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/browser/NavTabScroller;->mCubic:Landroid/view/animation/DecelerateInterpolator;

    int-to-float v3, v14

    const/4 v4, 0x0

    const/high16 v1, 0x41a00000    # 20.0f

    mul-float v5, v12, v1

    int-to-float v6, v10

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/android/browser/NavTabScroller;->ease(Landroid/view/animation/DecelerateInterpolator;FFFF)F

    move-result v1

    float-to-int v1, v1

    mul-int v18, v9, v1

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/browser/NavTabScroller;->mHorizontal:Z

    if-eqz v1, :cond_f

    move/from16 v0, v18

    int-to-float v1, v0

    invoke-virtual {v7, v1}, Landroid/view/View;->setTranslationX(F)V

    :goto_7
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/browser/NavTabScroller;->mHorizontal:Z

    if-eqz v1, :cond_10

    neg-float v1, v15

    invoke-virtual {v7, v1}, Landroid/view/View;->setRotationY(F)V

    :goto_8
    add-int/lit8 v11, v11, 0x1

    goto :goto_5

    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/browser/NavTabScroller;->mContentView:Lcom/android/browser/NavTabScroller$ContentLayout;

    invoke-virtual {v1}, Lcom/android/browser/NavTabScroller$ContentLayout;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    sub-int/2addr v1, v11

    goto/16 :goto_1

    :cond_5
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/browser/NavTabScroller;->mHorizontal:Z

    if-eqz v1, :cond_6

    const-string v1, "translationX"

    :goto_9
    const/4 v2, 0x2

    new-array v3, v2, [F

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/browser/NavTabScroller;->mHorizontal:Z

    if-eqz v2, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/android/browser/NavTabScroller;->getTranslationX()F

    move-result v2

    :goto_a
    aput v2, v3, v4

    const/4 v2, 0x1

    const/4 v4, 0x0

    aput v4, v3, v2

    invoke-static {v7, v1, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v17

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/android/browser/NavTabScroller;->mHorizontal:Z

    if-eqz v1, :cond_8

    const-string v1, "rotationY"

    :goto_b
    const/4 v2, 0x2

    new-array v3, v2, [F

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/android/browser/NavTabScroller;->mHorizontal:Z

    if-eqz v2, :cond_9

    invoke-virtual/range {p0 .. p0}, Lcom/android/browser/NavTabScroller;->getRotationY()F

    move-result v2

    :goto_c
    aput v2, v3, v4

    const/4 v2, 0x1

    const/4 v4, 0x0

    aput v4, v3, v2

    invoke-static {v7, v1, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v15

    new-instance v16, Landroid/animation/AnimatorSet;

    invoke-direct/range {v16 .. v16}, Landroid/animation/AnimatorSet;-><init>()V

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/animation/Animator;

    const/4 v2, 0x0

    aput-object v17, v1, v2

    const/4 v2, 0x1

    aput-object v15, v1, v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    const-wide/16 v1, 0x64

    move-object/from16 v0, v16

    invoke-virtual {v0, v1, v2}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    invoke-virtual/range {v16 .. v16}, Landroid/animation/AnimatorSet;->start()V

    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_0

    :cond_6
    const-string v1, "translationY"

    goto :goto_9

    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/android/browser/NavTabScroller;->getTranslationY()F

    move-result v2

    goto :goto_a

    :cond_8
    const-string v1, "rotationX"

    goto :goto_b

    :cond_9
    invoke-virtual/range {p0 .. p0}, Lcom/android/browser/NavTabScroller;->getRotationX()F

    move-result v2

    goto :goto_c

    :cond_a
    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/browser/NavTabScroller;->mPullValue:I

    if-nez v1, :cond_b

    const/4 v13, 0x1

    :cond_b
    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/browser/NavTabScroller;->mPullValue:I

    add-int v1, v1, p1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/android/browser/NavTabScroller;->mPullValue:I

    goto/16 :goto_2

    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/android/browser/NavTabScroller;->getHeight()I

    move-result v10

    goto/16 :goto_3

    :cond_d
    const/4 v9, -0x1

    goto/16 :goto_4

    :cond_e
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/browser/NavTabScroller;->mContentView:Lcom/android/browser/NavTabScroller$ContentLayout;

    invoke-virtual {v1}, Lcom/android/browser/NavTabScroller$ContentLayout;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    sub-int/2addr v1, v11

    goto/16 :goto_6

    :cond_f
    move/from16 v0, v18

    int-to-float v1, v0

    invoke-virtual {v7, v1}, Landroid/view/View;->setTranslationY(F)V

    goto/16 :goto_7

    :cond_10
    invoke-virtual {v7, v15}, Landroid/view/View;->setRotationX(F)V

    goto/16 :goto_8
.end method

.method protected setAdapter(Landroid/widget/BaseAdapter;I)V
    .locals 2
    .param p1    # Landroid/widget/BaseAdapter;
    .param p2    # I

    iput-object p1, p0, Lcom/android/browser/NavTabScroller;->mAdapter:Landroid/widget/BaseAdapter;

    iget-object v0, p0, Lcom/android/browser/NavTabScroller;->mAdapter:Landroid/widget/BaseAdapter;

    new-instance v1, Lcom/android/browser/NavTabScroller$1;

    invoke-direct {v1, p0}, Lcom/android/browser/NavTabScroller$1;-><init>(Lcom/android/browser/NavTabScroller;)V

    invoke-virtual {v0, v1}, Landroid/widget/BaseAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    invoke-virtual {p0, p2}, Lcom/android/browser/NavTabScroller;->handleDataChanged(I)V

    return-void
.end method

.method public setGap(I)V
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/android/browser/NavTabScroller;->mGapPosition:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iput p1, p0, Lcom/android/browser/NavTabScroller;->mGap:I

    invoke-virtual {p0}, Lcom/android/browser/NavTabScroller;->postInvalidate()V

    :cond_0
    return-void
.end method

.method public setOnLayoutListener(Lcom/android/browser/NavTabScroller$OnLayoutListener;)V
    .locals 0
    .param p1    # Lcom/android/browser/NavTabScroller$OnLayoutListener;

    iput-object p1, p0, Lcom/android/browser/NavTabScroller;->mLayoutListener:Lcom/android/browser/NavTabScroller$OnLayoutListener;

    return-void
.end method

.method public setOnRemoveListener(Lcom/android/browser/NavTabScroller$OnRemoveListener;)V
    .locals 0
    .param p1    # Lcom/android/browser/NavTabScroller$OnRemoveListener;

    iput-object p1, p0, Lcom/android/browser/NavTabScroller;->mRemoveListener:Lcom/android/browser/NavTabScroller$OnRemoveListener;

    return-void
.end method

.method public setOrientation(I)V
    .locals 4
    .param p1    # I

    const/4 v3, -0x1

    const/4 v2, -0x2

    iget-object v0, p0, Lcom/android/browser/NavTabScroller;->mContentView:Lcom/android/browser/NavTabScroller$ContentLayout;

    invoke-virtual {v0, p1}, Lcom/android/browser/NavTabScroller$ContentLayout;->setOrientation(I)V

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/android/browser/NavTabScroller;->mContentView:Lcom/android/browser/NavTabScroller$ContentLayout;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/android/browser/NavTabScroller$ContentLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_0
    invoke-super {p0, p1}, Lcom/android/browser/view/ScrollerView;->setOrientation(I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/browser/NavTabScroller;->mContentView:Lcom/android/browser/NavTabScroller$ContentLayout;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v3, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/android/browser/NavTabScroller$ContentLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method protected setScrollValue(I)V
    .locals 3
    .param p1    # I

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/android/browser/NavTabScroller;->mHorizontal:Z

    if-eqz v0, :cond_0

    move v0, p1

    :goto_0
    iget-boolean v2, p0, Lcom/android/browser/NavTabScroller;->mHorizontal:Z

    if-eqz v2, :cond_1

    :goto_1
    invoke-virtual {p0, v0, v1}, Lcom/android/browser/NavTabScroller;->scrollTo(II)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v1, p1

    goto :goto_1
.end method

.method snapToSelected(IZ)V
    .locals 5
    .param p1    # I
    .param p2    # Z

    if-gez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/android/browser/NavTabScroller;->mContentView:Lcom/android/browser/NavTabScroller$ContentLayout;

    invoke-virtual {v3, p1}, Lcom/android/browser/NavTabScroller$ContentLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    const/4 v1, 0x0

    iget-boolean v3, p0, Lcom/android/browser/NavTabScroller;->mHorizontal:Z

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v3

    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/android/browser/NavTabScroller;->getWidth()I

    move-result v4

    sub-int/2addr v3, v4

    div-int/lit8 v0, v3, 0x2

    :goto_1
    iget v3, p0, Lcom/android/browser/NavTabScroller;->mScrollX:I

    if-ne v0, v3, :cond_2

    iget v3, p0, Lcom/android/browser/NavTabScroller;->mScrollY:I

    if-eq v1, v3, :cond_0

    :cond_2
    if-eqz p2, :cond_4

    invoke-virtual {p0, v0, v1}, Lcom/android/browser/NavTabScroller;->smoothScrollTo(II)V

    goto :goto_0

    :cond_3
    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/android/browser/NavTabScroller;->getHeight()I

    move-result v4

    sub-int/2addr v3, v4

    div-int/lit8 v1, v3, 0x2

    goto :goto_1

    :cond_4
    invoke-virtual {p0, v0, v1}, Lcom/android/browser/NavTabScroller;->scrollTo(II)V

    goto :goto_0
.end method
