.class public Lcom/android/browser/AutofillHandler;
.super Ljava/lang/Object;
.source "AutofillHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/browser/AutofillHandler$1;,
        Lcom/android/browser/AutofillHandler$DeleteProfileFromDbTask;,
        Lcom/android/browser/AutofillHandler$SaveProfileToDbTask;,
        Lcom/android/browser/AutofillHandler$AutoFillProfileDbTask;,
        Lcom/android/browser/AutofillHandler$LoadFromDb;
    }
.end annotation


# instance fields
.field private mAutoFillActiveProfileId:I

.field private mAutoFillProfile:Landroid/webkit/WebSettingsClassic$AutoFillProfile;

.field private mContext:Landroid/content/Context;

.field private mLoaded:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/android/browser/AutofillHandler;->mLoaded:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/android/browser/AutofillHandler;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$100(Lcom/android/browser/AutofillHandler;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/android/browser/AutofillHandler;

    iget-object v0, p0, Lcom/android/browser/AutofillHandler;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/browser/AutofillHandler;)I
    .locals 1
    .param p0    # Lcom/android/browser/AutofillHandler;

    iget v0, p0, Lcom/android/browser/AutofillHandler;->mAutoFillActiveProfileId:I

    return v0
.end method

.method static synthetic access$202(Lcom/android/browser/AutofillHandler;I)I
    .locals 0
    .param p0    # Lcom/android/browser/AutofillHandler;
    .param p1    # I

    iput p1, p0, Lcom/android/browser/AutofillHandler;->mAutoFillActiveProfileId:I

    return p1
.end method

.method static synthetic access$300(Lcom/android/browser/AutofillHandler;)Landroid/webkit/WebSettingsClassic$AutoFillProfile;
    .locals 1
    .param p0    # Lcom/android/browser/AutofillHandler;

    iget-object v0, p0, Lcom/android/browser/AutofillHandler;->mAutoFillProfile:Landroid/webkit/WebSettingsClassic$AutoFillProfile;

    return-object v0
.end method

.method static synthetic access$302(Lcom/android/browser/AutofillHandler;Landroid/webkit/WebSettingsClassic$AutoFillProfile;)Landroid/webkit/WebSettingsClassic$AutoFillProfile;
    .locals 0
    .param p0    # Lcom/android/browser/AutofillHandler;
    .param p1    # Landroid/webkit/WebSettingsClassic$AutoFillProfile;

    iput-object p1, p0, Lcom/android/browser/AutofillHandler;->mAutoFillProfile:Landroid/webkit/WebSettingsClassic$AutoFillProfile;

    return-object p1
.end method

.method static synthetic access$400(Lcom/android/browser/AutofillHandler;)Ljava/util/concurrent/CountDownLatch;
    .locals 1
    .param p0    # Lcom/android/browser/AutofillHandler;

    iget-object v0, p0, Lcom/android/browser/AutofillHandler;->mLoaded:Ljava/util/concurrent/CountDownLatch;

    return-object v0
.end method

.method private declared-synchronized setActiveAutoFillProfileId(I)V
    .locals 2
    .param p1    # I

    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/android/browser/AutofillHandler;->mAutoFillActiveProfileId:I

    iget-object v1, p0, Lcom/android/browser/AutofillHandler;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "autofill_active_profile_id"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private waitForLoad()V
    .locals 3

    :try_start_0
    iget-object v1, p0, Lcom/android/browser/AutofillHandler;->mLoaded:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "AutofillHandler"

    const-string v2, "Caught exception while waiting for AutofillProfile to load."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public asyncLoadFromDb()V
    .locals 2

    new-instance v0, Lcom/android/browser/AutofillHandler$LoadFromDb;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/browser/AutofillHandler$LoadFromDb;-><init>(Lcom/android/browser/AutofillHandler;Lcom/android/browser/AutofillHandler$1;)V

    invoke-virtual {v0}, Lcom/android/browser/AutofillHandler$LoadFromDb;->start()V

    return-void
.end method

.method public declared-synchronized getAutoFillProfile()Landroid/webkit/WebSettingsClassic$AutoFillProfile;
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/android/browser/AutofillHandler;->waitForLoad()V

    iget-object v0, p0, Lcom/android/browser/AutofillHandler;->mAutoFillProfile:Landroid/webkit/WebSettingsClassic$AutoFillProfile;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setAutoFillProfile(Landroid/webkit/WebSettingsClassic$AutoFillProfile;Landroid/os/Message;)V
    .locals 5
    .param p1    # Landroid/webkit/WebSettingsClassic$AutoFillProfile;
    .param p2    # Landroid/os/Message;

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/android/browser/AutofillHandler;->waitForLoad()V

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/webkit/WebSettingsClassic$AutoFillProfile;->getUniqueId()I

    move-result v0

    new-instance v1, Lcom/android/browser/AutofillHandler$SaveProfileToDbTask;

    invoke-direct {v1, p0, p2}, Lcom/android/browser/AutofillHandler$SaveProfileToDbTask;-><init>(Lcom/android/browser/AutofillHandler;Landroid/os/Message;)V

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/webkit/WebSettingsClassic$AutoFillProfile;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v1, v2}, Lcom/android/browser/AutofillHandler$SaveProfileToDbTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/android/browser/AutofillHandler;->mAutoFillProfile:Landroid/webkit/WebSettingsClassic$AutoFillProfile;

    invoke-direct {p0, v0}, Lcom/android/browser/AutofillHandler;->setActiveAutoFillProfileId(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/android/browser/AutofillHandler;->mAutoFillProfile:Landroid/webkit/WebSettingsClassic$AutoFillProfile;

    if-eqz v1, :cond_0

    new-instance v1, Lcom/android/browser/AutofillHandler$DeleteProfileFromDbTask;

    invoke-direct {v1, p0, p2}, Lcom/android/browser/AutofillHandler$DeleteProfileFromDbTask;-><init>(Lcom/android/browser/AutofillHandler;Landroid/os/Message;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Integer;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/android/browser/AutofillHandler;->mAutoFillProfile:Landroid/webkit/WebSettingsClassic$AutoFillProfile;

    invoke-virtual {v4}, Landroid/webkit/WebSettingsClassic$AutoFillProfile;->getUniqueId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/android/browser/AutofillHandler$DeleteProfileFromDbTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
