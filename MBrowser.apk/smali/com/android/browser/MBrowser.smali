.class public Lcom/android/browser/MBrowser;
.super Landroid/app/Application;
.source "MBrowser.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 1

    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    invoke-static {p0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    invoke-virtual {p0}, Lcom/android/browser/MBrowser;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/browser/BrowserSettings;->initialize(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/android/browser/MBrowser;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/browser/Preloader;->initialize(Landroid/content/Context;)V

    return-void
.end method
