.class Lcom/android/browser/Controller$4;
.super Landroid/os/Handler;
.source "Controller.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/browser/Controller;->startHandler()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/browser/Controller;


# direct methods
.method constructor <init>(Lcom/android/browser/Controller;)V
    .locals 0

    iput-object p1, p0, Lcom/android/browser/Controller$4;->this$0:Lcom/android/browser/Controller;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 23
    .param p1    # Landroid/os/Message;

    move-object/from16 v0, p1

    iget v1, v0, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/browser/Controller$4;->this$0:Lcom/android/browser/Controller;

    sget-object v3, Lcom/android/browser/UI$ComboViews;->Bookmarks:Lcom/android/browser/UI$ComboViews;

    invoke-virtual {v1, v3}, Lcom/android/browser/Controller;->bookmarksOrHistoryPicker(Lcom/android/browser/UI$ComboViews;)V

    goto :goto_0

    :sswitch_1
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "url"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "title"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "src"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/lang/String;

    const-string v1, ""

    if-ne v2, v1, :cond_1

    move-object/from16 v2, v19

    :cond_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v17, v0

    check-cast v17, Ljava/util/HashMap;

    const-string v1, "webview"

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Landroid/webkit/WebView;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/browser/Controller$4;->this$0:Lcom/android/browser/Controller;

    invoke-virtual {v1}, Lcom/android/browser/Controller;->getCurrentTopWebView()Landroid/webkit/WebView;

    move-result-object v1

    move-object/from16 v0, v22

    if-ne v1, v0, :cond_0

    move-object/from16 v0, p1

    iget v1, v0, Landroid/os/Message;->arg1:I

    sparse-switch v1, :sswitch_data_1

    goto :goto_0

    :sswitch_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/browser/Controller$4;->this$0:Lcom/android/browser/Controller;

    invoke-virtual {v1, v2}, Lcom/android/browser/Controller;->loadUrlFromContext(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/browser/Controller$4;->this$0:Lcom/android/browser/Controller;

    move-object/from16 v0, v19

    invoke-virtual {v1, v0}, Lcom/android/browser/Controller;->loadUrlFromContext(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/browser/Controller$4;->this$0:Lcom/android/browser/Controller;

    # getter for: Lcom/android/browser/Controller;->mTabControl:Lcom/android/browser/TabControl;
    invoke-static {v1}, Lcom/android/browser/Controller;->access$000(Lcom/android/browser/Controller;)Lcom/android/browser/TabControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/browser/TabControl;->getCurrentTab()Lcom/android/browser/Tab;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/browser/Controller$4;->this$0:Lcom/android/browser/Controller;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/browser/Controller$4;->this$0:Lcom/android/browser/Controller;

    # getter for: Lcom/android/browser/Controller;->mSettings:Lcom/android/browser/BrowserSettings;
    invoke-static {v1}, Lcom/android/browser/Controller;->access$300(Lcom/android/browser/Controller;)Lcom/android/browser/BrowserSettings;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/browser/BrowserSettings;->openInBackground()Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    const/4 v4, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v3, v2, v0, v1, v4}, Lcom/android/browser/Controller;->openTab(Ljava/lang/String;Lcom/android/browser/Tab;ZZ)Lcom/android/browser/Tab;

    goto/16 :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    :sswitch_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/browser/Controller$4;->this$0:Lcom/android/browser/Controller;

    # invokes: Lcom/android/browser/Controller;->copy(Ljava/lang/CharSequence;)V
    invoke-static {v1, v2}, Lcom/android/browser/Controller;->access$400(Lcom/android/browser/Controller;Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual/range {v22 .. v22}, Landroid/webkit/WebView;->getUrl()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/browser/Controller$4;->this$0:Lcom/android/browser/Controller;

    # getter for: Lcom/android/browser/Controller;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/android/browser/Controller;->access$200(Lcom/android/browser/Controller;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual/range {v22 .. v22}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v3

    invoke-virtual {v3}, Landroid/webkit/WebSettings;->getUserAgentString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v22 .. v22}, Landroid/webkit/WebView;->isPrivateBrowsingEnabled()Z

    move-result v7

    invoke-static/range {v1 .. v7}, Lcom/android/browser/DownloadHandler;->onDownloadStartNoStream(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_0

    :sswitch_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/browser/Controller$4;->this$0:Lcom/android/browser/Controller;

    move-object/from16 v0, p1

    iget-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Lcom/android/browser/Controller;->loadUrlFromContext(Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/browser/Controller$4;->this$0:Lcom/android/browser/Controller;

    invoke-virtual {v1}, Lcom/android/browser/Controller;->stopLoading()V

    goto/16 :goto_0

    :sswitch_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/browser/Controller$4;->this$0:Lcom/android/browser/Controller;

    # getter for: Lcom/android/browser/Controller;->mWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v1}, Lcom/android/browser/Controller;->access$500(Lcom/android/browser/Controller;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/browser/Controller$4;->this$0:Lcom/android/browser/Controller;

    # getter for: Lcom/android/browser/Controller;->mWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v1}, Lcom/android/browser/Controller;->access$500(Lcom/android/browser/Controller;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v1

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/browser/Controller$4;->this$0:Lcom/android/browser/Controller;

    # getter for: Lcom/android/browser/Controller;->mWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v1}, Lcom/android/browser/Controller;->access$500(Lcom/android/browser/Controller;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/browser/Controller$4;->this$0:Lcom/android/browser/Controller;

    # getter for: Lcom/android/browser/Controller;->mTabControl:Lcom/android/browser/TabControl;
    invoke-static {v1}, Lcom/android/browser/Controller;->access$000(Lcom/android/browser/Controller;)Lcom/android/browser/TabControl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/browser/TabControl;->stopAllLoading()V

    goto/16 :goto_0

    :sswitch_a
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v20, v0

    check-cast v20, Lcom/android/browser/Tab;

    if-eqz v20, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/browser/Controller$4;->this$0:Lcom/android/browser/Controller;

    move-object/from16 v0, v20

    # invokes: Lcom/android/browser/Controller;->updateScreenshot(Lcom/android/browser/Tab;)V
    invoke-static {v1, v0}, Lcom/android/browser/Controller;->access$600(Lcom/android/browser/Controller;Lcom/android/browser/Tab;)V

    goto/16 :goto_0

    :sswitch_b
    move-object/from16 v0, p1

    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object/from16 v16, v0

    check-cast v16, Lcom/android/browser/Controller$DownloadInfo;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/android/browser/Controller$4;->this$0:Lcom/android/browser/Controller;

    move-object/from16 v0, v16

    iget-object v8, v0, Lcom/android/browser/Controller$DownloadInfo;->tab:Lcom/android/browser/Tab;

    move-object/from16 v0, v16

    iget-object v9, v0, Lcom/android/browser/Controller$DownloadInfo;->url:Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v10, v0, Lcom/android/browser/Controller$DownloadInfo;->userAgent:Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v11, v0, Lcom/android/browser/Controller$DownloadInfo;->contentDisposition:Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v12, v0, Lcom/android/browser/Controller$DownloadInfo;->mimetype:Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v13, v0, Lcom/android/browser/Controller$DownloadInfo;->RefererUrl:Ljava/lang/String;

    move-object/from16 v0, v16

    iget-wide v14, v0, Lcom/android/browser/Controller$DownloadInfo;->contentLength:J

    # invokes: Lcom/android/browser/Controller;->doDownloading(Lcom/android/browser/Tab;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    invoke-static/range {v7 .. v15}, Lcom/android/browser/Controller;->access$700(Lcom/android/browser/Controller;Lcom/android/browser/Tab;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x66 -> :sswitch_1
        0x6b -> :sswitch_9
        0x6c -> :sswitch_a
        0xc9 -> :sswitch_0
        0xca -> :sswitch_b
        0x3e9 -> :sswitch_7
        0x3ea -> :sswitch_8
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x7f0d0021 -> :sswitch_2    # com.android.browser.R.id.open_context_menu_id
        0x7f0d00d6 -> :sswitch_4    # com.android.browser.R.id.open_newtab_context_menu_id
        0x7f0d00d7 -> :sswitch_6    # com.android.browser.R.id.save_link_context_menu_id
        0x7f0d00d8 -> :sswitch_5    # com.android.browser.R.id.copy_link_context_menu_id
        0x7f0d00da -> :sswitch_6    # com.android.browser.R.id.download_context_menu_id
        0x7f0d00db -> :sswitch_3    # com.android.browser.R.id.view_image_context_menu_id
    .end sparse-switch
.end method
