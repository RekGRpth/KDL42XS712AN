.class public abstract Lcom/android/browser/util/ThreadedCursorAdapter;
.super Landroid/widget/BaseAdapter;
.source "ThreadedCursorAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/browser/util/ThreadedCursorAdapter$LoadContainer;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/widget/BaseAdapter;"
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCursorAdapter:Landroid/widget/CursorAdapter;

.field private mCursorLock:Ljava/lang/Object;

.field private mGeneration:J

.field private mHandler:Landroid/os/Handler;

.field private mHasCursor:Z

.field private mLoadHandler:Landroid/os/Handler;

.field private mLoadingObject:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private mSize:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/database/Cursor;

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/android/browser/util/ThreadedCursorAdapter;->mCursorLock:Ljava/lang/Object;

    iput-object p1, p0, Lcom/android/browser/util/ThreadedCursorAdapter;->mContext:Landroid/content/Context;

    if-eqz p2, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/android/browser/util/ThreadedCursorAdapter;->mHasCursor:Z

    new-instance v1, Lcom/android/browser/util/ThreadedCursorAdapter$1;

    invoke-direct {v1, p0, p1, p2, v2}, Lcom/android/browser/util/ThreadedCursorAdapter$1;-><init>(Lcom/android/browser/util/ThreadedCursorAdapter;Landroid/content/Context;Landroid/database/Cursor;I)V

    iput-object v1, p0, Lcom/android/browser/util/ThreadedCursorAdapter;->mCursorAdapter:Landroid/widget/CursorAdapter;

    iget-object v1, p0, Lcom/android/browser/util/ThreadedCursorAdapter;->mCursorAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {v1}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v1

    iput v1, p0, Lcom/android/browser/util/ThreadedCursorAdapter;->mSize:I

    new-instance v0, Landroid/os/HandlerThread;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "threaded_adapter_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    new-instance v1, Lcom/android/browser/util/ThreadedCursorAdapter$2;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/android/browser/util/ThreadedCursorAdapter$2;-><init>(Lcom/android/browser/util/ThreadedCursorAdapter;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/android/browser/util/ThreadedCursorAdapter;->mLoadHandler:Landroid/os/Handler;

    new-instance v1, Lcom/android/browser/util/ThreadedCursorAdapter$3;

    invoke-direct {v1, p0}, Lcom/android/browser/util/ThreadedCursorAdapter$3;-><init>(Lcom/android/browser/util/ThreadedCursorAdapter;)V

    iput-object v1, p0, Lcom/android/browser/util/ThreadedCursorAdapter;->mHandler:Landroid/os/Handler;

    return-void

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method static synthetic access$002(Lcom/android/browser/util/ThreadedCursorAdapter;I)I
    .locals 0
    .param p0    # Lcom/android/browser/util/ThreadedCursorAdapter;
    .param p1    # I

    iput p1, p0, Lcom/android/browser/util/ThreadedCursorAdapter;->mSize:I

    return p1
.end method

.method static synthetic access$100(Lcom/android/browser/util/ThreadedCursorAdapter;)J
    .locals 2
    .param p0    # Lcom/android/browser/util/ThreadedCursorAdapter;

    iget-wide v0, p0, Lcom/android/browser/util/ThreadedCursorAdapter;->mGeneration:J

    return-wide v0
.end method

.method static synthetic access$108(Lcom/android/browser/util/ThreadedCursorAdapter;)J
    .locals 4
    .param p0    # Lcom/android/browser/util/ThreadedCursorAdapter;

    iget-wide v0, p0, Lcom/android/browser/util/ThreadedCursorAdapter;->mGeneration:J

    const-wide/16 v2, 0x1

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/android/browser/util/ThreadedCursorAdapter;->mGeneration:J

    return-wide v0
.end method

.method static synthetic access$200(Lcom/android/browser/util/ThreadedCursorAdapter;ILcom/android/browser/util/ThreadedCursorAdapter$LoadContainer;)V
    .locals 0
    .param p0    # Lcom/android/browser/util/ThreadedCursorAdapter;
    .param p1    # I
    .param p2    # Lcom/android/browser/util/ThreadedCursorAdapter$LoadContainer;

    invoke-direct {p0, p1, p2}, Lcom/android/browser/util/ThreadedCursorAdapter;->loadRowObject(ILcom/android/browser/util/ThreadedCursorAdapter$LoadContainer;)V

    return-void
.end method

.method private cachedLoadObject()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/browser/util/ThreadedCursorAdapter;->mLoadingObject:Ljava/lang/Object;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/browser/util/ThreadedCursorAdapter;->getLoadingObject()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/android/browser/util/ThreadedCursorAdapter;->mLoadingObject:Ljava/lang/Object;

    :cond_0
    iget-object v0, p0, Lcom/android/browser/util/ThreadedCursorAdapter;->mLoadingObject:Ljava/lang/Object;

    return-object v0
.end method

.method private loadRowObject(ILcom/android/browser/util/ThreadedCursorAdapter$LoadContainer;)V
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/android/browser/util/ThreadedCursorAdapter",
            "<TT;>.",
            "LoadContainer;",
            ")V"
        }
    .end annotation

    if-eqz p2, :cond_0

    iget v1, p2, Lcom/android/browser/util/ThreadedCursorAdapter$LoadContainer;->position:I

    if-ne v1, p1, :cond_0

    iget-object v1, p2, Lcom/android/browser/util/ThreadedCursorAdapter$LoadContainer;->owner:Landroid/widget/Adapter;

    if-ne v1, p0, :cond_0

    iget-object v1, p2, Lcom/android/browser/util/ThreadedCursorAdapter$LoadContainer;->view:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/android/browser/util/ThreadedCursorAdapter;->mCursorLock:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/android/browser/util/ThreadedCursorAdapter;->mCursorAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {v1, p1}, Landroid/widget/CursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_3
    :try_start_1
    iget-object v1, p2, Lcom/android/browser/util/ThreadedCursorAdapter$LoadContainer;->bind_object:Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/android/browser/util/ThreadedCursorAdapter;->getRowObject(Landroid/database/Cursor;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p2, Lcom/android/browser/util/ThreadedCursorAdapter$LoadContainer;->bind_object:Ljava/lang/Object;

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v1, p0, Lcom/android/browser/util/ThreadedCursorAdapter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method


# virtual methods
.method public abstract bindView(Landroid/view/View;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "TT;)V"
        }
    .end annotation
.end method

.method public changeCursor(Landroid/database/Cursor;)V
    .locals 2
    .param p1    # Landroid/database/Cursor;

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/android/browser/util/ThreadedCursorAdapter;->mLoadHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/android/browser/util/ThreadedCursorAdapter;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/android/browser/util/ThreadedCursorAdapter;->mCursorLock:Ljava/lang/Object;

    monitor-enter v1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    :try_start_0
    iput-boolean v0, p0, Lcom/android/browser/util/ThreadedCursorAdapter;->mHasCursor:Z

    iget-object v0, p0, Lcom/android/browser/util/ThreadedCursorAdapter;->mCursorAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {v0, p1}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    monitor-exit v1

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getCount()I
    .locals 1

    iget v0, p0, Lcom/android/browser/util/ThreadedCursorAdapter;->mSize:I

    return v0
.end method

.method public getItem(I)Landroid/database/Cursor;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/browser/util/ThreadedCursorAdapter;->mCursorAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {v0, p1}, Landroid/widget/CursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    return-object v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/android/browser/util/ThreadedCursorAdapter;->getItem(I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 4
    .param p1    # I

    iget-object v1, p0, Lcom/android/browser/util/ThreadedCursorAdapter;->mCursorLock:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/android/browser/util/ThreadedCursorAdapter;->getItem(I)Landroid/database/Cursor;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/browser/util/ThreadedCursorAdapter;->getItemId(Landroid/database/Cursor;)J

    move-result-wide v2

    monitor-exit v1

    return-wide v2

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected abstract getItemId(Landroid/database/Cursor;)J
.end method

.method public abstract getLoadingObject()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public abstract getRowObject(Landroid/database/Cursor;Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "TT;)TT;"
        }
    .end annotation
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const v2, 0x7f0d0005    # com.android.browser.R.id.load_object

    if-nez p2, :cond_0

    iget-object v1, p0, Lcom/android/browser/util/ThreadedCursorAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v1, p3}, Lcom/android/browser/util/ThreadedCursorAdapter;->newView(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_0
    invoke-virtual {p2, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/browser/util/ThreadedCursorAdapter$LoadContainer;

    if-nez v0, :cond_1

    new-instance v0, Lcom/android/browser/util/ThreadedCursorAdapter$LoadContainer;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/browser/util/ThreadedCursorAdapter$LoadContainer;-><init>(Lcom/android/browser/util/ThreadedCursorAdapter;Lcom/android/browser/util/ThreadedCursorAdapter$1;)V

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, v0, Lcom/android/browser/util/ThreadedCursorAdapter$LoadContainer;->view:Ljava/lang/ref/WeakReference;

    invoke-virtual {p2, v2, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    :cond_1
    iget v1, v0, Lcom/android/browser/util/ThreadedCursorAdapter$LoadContainer;->position:I

    if-ne v1, p1, :cond_3

    iget-object v1, v0, Lcom/android/browser/util/ThreadedCursorAdapter$LoadContainer;->owner:Landroid/widget/Adapter;

    if-ne v1, p0, :cond_3

    iget-boolean v1, v0, Lcom/android/browser/util/ThreadedCursorAdapter$LoadContainer;->loaded:Z

    if-eqz v1, :cond_3

    iget-wide v1, v0, Lcom/android/browser/util/ThreadedCursorAdapter$LoadContainer;->generation:J

    iget-wide v3, p0, Lcom/android/browser/util/ThreadedCursorAdapter;->mGeneration:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_3

    iget-object v1, v0, Lcom/android/browser/util/ThreadedCursorAdapter$LoadContainer;->bind_object:Ljava/lang/Object;

    invoke-virtual {p0, p2, v1}, Lcom/android/browser/util/ThreadedCursorAdapter;->bindView(Landroid/view/View;Ljava/lang/Object;)V

    :cond_2
    :goto_0
    return-object p2

    :cond_3
    invoke-direct {p0}, Lcom/android/browser/util/ThreadedCursorAdapter;->cachedLoadObject()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, p2, v1}, Lcom/android/browser/util/ThreadedCursorAdapter;->bindView(Landroid/view/View;Ljava/lang/Object;)V

    iget-boolean v1, p0, Lcom/android/browser/util/ThreadedCursorAdapter;->mHasCursor:Z

    if-eqz v1, :cond_2

    iput p1, v0, Lcom/android/browser/util/ThreadedCursorAdapter$LoadContainer;->position:I

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/android/browser/util/ThreadedCursorAdapter$LoadContainer;->loaded:Z

    iput-object p0, v0, Lcom/android/browser/util/ThreadedCursorAdapter$LoadContainer;->owner:Landroid/widget/Adapter;

    iget-wide v1, p0, Lcom/android/browser/util/ThreadedCursorAdapter;->mGeneration:J

    iput-wide v1, v0, Lcom/android/browser/util/ThreadedCursorAdapter$LoadContainer;->generation:J

    iget-object v1, p0, Lcom/android/browser/util/ThreadedCursorAdapter;->mLoadHandler:Landroid/os/Handler;

    invoke-virtual {v1, p1, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method

.method public abstract newView(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
.end method
