.class public Lcom/android/browser/ShortcutActivity;
.super Landroid/app/Activity;
.source "ShortcutActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/android/browser/BookmarksPageCallbacks;


# instance fields
.field private mBookmarks:Lcom/android/browser/BrowserBookmarksPage;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onBookmarkSelected(Landroid/database/Cursor;Z)Z
    .locals 2
    .param p1    # Landroid/database/Cursor;
    .param p2    # Z

    if-eqz p2, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-static {p0, p1}, Lcom/android/browser/BrowserBookmarksPage;->createShortcutIntent(Landroid/content/Context;Landroid/database/Cursor;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/android/browser/ShortcutActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/android/browser/ShortcutActivity;->finish()V

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/android/browser/ShortcutActivity;->finish()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f0d0038
        :pswitch_0    # com.android.browser.R.id.cancel
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v1, 0x7f0c0052    # com.android.browser.R.string.shortcut_bookmark_title

    invoke-virtual {p0, v1}, Lcom/android/browser/ShortcutActivity;->setTitle(I)V

    const v1, 0x7f040023    # com.android.browser.R.layout.pick_bookmark

    invoke-virtual {p0, v1}, Lcom/android/browser/ShortcutActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/android/browser/ShortcutActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const v2, 0x7f0d005b    # com.android.browser.R.id.bookmarks

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/android/browser/BrowserBookmarksPage;

    iput-object v1, p0, Lcom/android/browser/ShortcutActivity;->mBookmarks:Lcom/android/browser/BrowserBookmarksPage;

    iget-object v1, p0, Lcom/android/browser/ShortcutActivity;->mBookmarks:Lcom/android/browser/BrowserBookmarksPage;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/browser/BrowserBookmarksPage;->setEnableContextMenu(Z)V

    iget-object v1, p0, Lcom/android/browser/ShortcutActivity;->mBookmarks:Lcom/android/browser/BrowserBookmarksPage;

    invoke-virtual {v1, p0}, Lcom/android/browser/BrowserBookmarksPage;->setCallbackListener(Lcom/android/browser/BookmarksPageCallbacks;)V

    const v1, 0x7f0d0038    # com.android.browser.R.id.cancel

    invoke-virtual {p0, v1}, Lcom/android/browser/ShortcutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method public varargs onOpenInNewWindow([Ljava/lang/String;)Z
    .locals 1
    .param p1    # [Ljava/lang/String;

    const/4 v0, 0x0

    return v0
.end method
