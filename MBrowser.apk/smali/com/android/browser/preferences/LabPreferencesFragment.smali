.class public Lcom/android/browser/preferences/LabPreferencesFragment;
.super Landroid/preference/PreferenceFragment;
.source "LabPreferencesFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f060007    # com.android.browser.R.xml.lab_preferences

    invoke-virtual {p0, v0}, Lcom/android/browser/preferences/LabPreferencesFragment;->addPreferencesFromResource(I)V

    return-void
.end method
