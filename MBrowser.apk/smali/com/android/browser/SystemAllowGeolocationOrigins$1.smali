.class Lcom/android/browser/SystemAllowGeolocationOrigins$1;
.super Ljava/lang/Object;
.source "SystemAllowGeolocationOrigins.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/browser/SystemAllowGeolocationOrigins;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/browser/SystemAllowGeolocationOrigins;


# direct methods
.method constructor <init>(Lcom/android/browser/SystemAllowGeolocationOrigins;)V
    .locals 0

    iput-object p1, p0, Lcom/android/browser/SystemAllowGeolocationOrigins$1;->this$0:Lcom/android/browser/SystemAllowGeolocationOrigins;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    iget-object v7, p0, Lcom/android/browser/SystemAllowGeolocationOrigins$1;->this$0:Lcom/android/browser/SystemAllowGeolocationOrigins;

    # invokes: Lcom/android/browser/SystemAllowGeolocationOrigins;->getSystemSetting()Ljava/lang/String;
    invoke-static {v7}, Lcom/android/browser/SystemAllowGeolocationOrigins;->access$000(Lcom/android/browser/SystemAllowGeolocationOrigins;)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Lcom/android/browser/BrowserSettings;->getInstance()Lcom/android/browser/BrowserSettings;

    move-result-object v7

    invoke-virtual {v7}, Lcom/android/browser/BrowserSettings;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v5

    const-string v7, "last_read_allow_geolocation_origins"

    const-string v8, ""

    invoke-interface {v5, v7, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    const-string v8, "last_read_allow_geolocation_origins"

    invoke-interface {v7, v8, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->apply()V

    # invokes: Lcom/android/browser/SystemAllowGeolocationOrigins;->parseAllowGeolocationOrigins(Ljava/lang/String;)Ljava/util/HashSet;
    invoke-static {v1}, Lcom/android/browser/SystemAllowGeolocationOrigins;->access$100(Ljava/lang/String;)Ljava/util/HashSet;

    move-result-object v4

    # invokes: Lcom/android/browser/SystemAllowGeolocationOrigins;->parseAllowGeolocationOrigins(Ljava/lang/String;)Ljava/util/HashSet;
    invoke-static {v3}, Lcom/android/browser/SystemAllowGeolocationOrigins;->access$100(Ljava/lang/String;)Ljava/util/HashSet;

    move-result-object v2

    iget-object v7, p0, Lcom/android/browser/SystemAllowGeolocationOrigins$1;->this$0:Lcom/android/browser/SystemAllowGeolocationOrigins;

    # invokes: Lcom/android/browser/SystemAllowGeolocationOrigins;->setMinus(Ljava/util/Set;Ljava/util/Set;)Ljava/util/Set;
    invoke-static {v7, v2, v4}, Lcom/android/browser/SystemAllowGeolocationOrigins;->access$200(Lcom/android/browser/SystemAllowGeolocationOrigins;Ljava/util/Set;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iget-object v7, p0, Lcom/android/browser/SystemAllowGeolocationOrigins$1;->this$0:Lcom/android/browser/SystemAllowGeolocationOrigins;

    # invokes: Lcom/android/browser/SystemAllowGeolocationOrigins;->setMinus(Ljava/util/Set;Ljava/util/Set;)Ljava/util/Set;
    invoke-static {v7, v4, v2}, Lcom/android/browser/SystemAllowGeolocationOrigins;->access$200(Lcom/android/browser/SystemAllowGeolocationOrigins;Ljava/util/Set;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v6

    iget-object v7, p0, Lcom/android/browser/SystemAllowGeolocationOrigins$1;->this$0:Lcom/android/browser/SystemAllowGeolocationOrigins;

    # invokes: Lcom/android/browser/SystemAllowGeolocationOrigins;->removeOrigins(Ljava/util/Set;)V
    invoke-static {v7, v6}, Lcom/android/browser/SystemAllowGeolocationOrigins;->access$300(Lcom/android/browser/SystemAllowGeolocationOrigins;Ljava/util/Set;)V

    iget-object v7, p0, Lcom/android/browser/SystemAllowGeolocationOrigins$1;->this$0:Lcom/android/browser/SystemAllowGeolocationOrigins;

    # invokes: Lcom/android/browser/SystemAllowGeolocationOrigins;->addOrigins(Ljava/util/Set;)V
    invoke-static {v7, v0}, Lcom/android/browser/SystemAllowGeolocationOrigins;->access$400(Lcom/android/browser/SystemAllowGeolocationOrigins;Ljava/util/Set;)V

    goto :goto_0
.end method
