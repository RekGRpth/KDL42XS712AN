.class public Lcom/android/browser/BrowserUtil;
.super Ljava/lang/Object;
.source "BrowserUtil.java"


# instance fields
.field private mToolLibControlAble:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/browser/BrowserUtil;->mToolLibControlAble:Z

    :try_start_0
    const-string v1, "browserutil_jni"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "BROWSERTOOL"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not load native library: jni_browserUtil"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/android/browser/BrowserUtil;->mToolLibControlAble:Z

    goto :goto_0
.end method

.method private native nativeCloseInput()V
.end method

.method private native nativeMouseLeftClick()V
.end method

.method private native nativeMoveCursor(II)V
.end method


# virtual methods
.method public closeInputDevice()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/browser/BrowserUtil;->mToolLibControlAble:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/browser/BrowserUtil;->nativeCloseInput()V

    :cond_0
    return-void
.end method

.method public mouseLeftClick()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/browser/BrowserUtil;->mToolLibControlAble:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/browser/BrowserUtil;->nativeMouseLeftClick()V

    :cond_0
    return-void
.end method

.method public moveCursor(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    const-string v0, "zb.wu"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "moveCursor: x:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "   y:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/android/browser/BrowserUtil;->mToolLibControlAble:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/android/browser/BrowserUtil;->nativeMoveCursor(II)V

    :cond_0
    return-void
.end method
