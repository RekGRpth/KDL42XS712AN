.class public Lcom/android/browser/BrowserBookmarksAdapter;
.super Lcom/android/browser/util/ThreadedCursorAdapter;
.source "BrowserBookmarksAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/android/browser/util/ThreadedCursorAdapter",
        "<",
        "Lcom/android/browser/BrowserBookmarksAdapterItem;",
        ">;"
    }
.end annotation


# instance fields
.field mContext:Landroid/content/Context;

.field mInflater:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/browser/util/ThreadedCursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/android/browser/BrowserBookmarksAdapter;->mInflater:Landroid/view/LayoutInflater;

    iput-object p1, p0, Lcom/android/browser/BrowserBookmarksAdapter;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method bindGridView(Landroid/view/View;Landroid/content/Context;Lcom/android/browser/BrowserBookmarksAdapterItem;)V
    .locals 5
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Lcom/android/browser/BrowserBookmarksAdapterItem;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0025    # com.android.browser.R.dimen.combo_horizontalSpacing

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    invoke-virtual {p1, v0, v3, v0, v4}, Landroid/view/View;->setPadding(IIII)V

    const v3, 0x7f0d001e    # com.android.browser.R.id.thumb

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    const v3, 0x7f0d0018    # com.android.browser.R.id.label

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v3, p3, Lcom/android/browser/BrowserBookmarksAdapterItem;->title:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v3, p3, Lcom/android/browser/BrowserBookmarksAdapterItem;->is_folder:Z

    if-eqz v3, :cond_0

    const v3, 0x7f02005a    # com.android.browser.R.drawable.thumb_bookmark_widget_folder_holo

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    sget-object v3, Landroid/widget/ImageView$ScaleType;->FIT_END:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void

    :cond_0
    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v3, p3, Lcom/android/browser/BrowserBookmarksAdapterItem;->thumbnail:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v3, :cond_1

    iget-boolean v3, p3, Lcom/android/browser/BrowserBookmarksAdapterItem;->has_thumbnail:Z

    if-nez v3, :cond_2

    :cond_1
    const v3, 0x7f02000e    # com.android.browser.R.drawable.browser_thumbnail

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_1
    const v3, 0x7f02000c    # com.android.browser.R.drawable.border_thumb_bookmarks_widget_holo

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    :cond_2
    iget-object v3, p3, Lcom/android/browser/BrowserBookmarksAdapterItem;->thumbnail:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method public bindView(Landroid/view/View;Lcom/android/browser/BrowserBookmarksAdapterItem;)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/android/browser/BrowserBookmarksAdapterItem;

    move-object v0, p1

    check-cast v0, Lcom/android/browser/view/BookmarkContainer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/browser/view/BookmarkContainer;->setIgnoreRequestLayout(Z)V

    iget-object v1, p0, Lcom/android/browser/BrowserBookmarksAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p0, p1, v1, p2}, Lcom/android/browser/BrowserBookmarksAdapter;->bindGridView(Landroid/view/View;Landroid/content/Context;Lcom/android/browser/BrowserBookmarksAdapterItem;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/browser/view/BookmarkContainer;->setIgnoreRequestLayout(Z)V

    return-void
.end method

.method public bridge synthetic bindView(Landroid/view/View;Ljava/lang/Object;)V
    .locals 0
    .param p1    # Landroid/view/View;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Lcom/android/browser/BrowserBookmarksAdapterItem;

    invoke-virtual {p0, p1, p2}, Lcom/android/browser/BrowserBookmarksAdapter;->bindView(Landroid/view/View;Lcom/android/browser/BrowserBookmarksAdapterItem;)V

    return-void
.end method

.method protected getItemId(Landroid/database/Cursor;)J
    .locals 2
    .param p1    # Landroid/database/Cursor;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getLoadingObject()Lcom/android/browser/BrowserBookmarksAdapterItem;
    .locals 1

    new-instance v0, Lcom/android/browser/BrowserBookmarksAdapterItem;

    invoke-direct {v0}, Lcom/android/browser/BrowserBookmarksAdapterItem;-><init>()V

    return-object v0
.end method

.method public bridge synthetic getLoadingObject()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/android/browser/BrowserBookmarksAdapter;->getLoadingObject()Lcom/android/browser/BrowserBookmarksAdapterItem;

    move-result-object v0

    return-object v0
.end method

.method public getRowObject(Landroid/database/Cursor;Lcom/android/browser/BrowserBookmarksAdapterItem;)Lcom/android/browser/BrowserBookmarksAdapterItem;
    .locals 5
    .param p1    # Landroid/database/Cursor;
    .param p2    # Lcom/android/browser/BrowserBookmarksAdapterItem;

    const/4 v3, 0x0

    const/4 v2, 0x1

    if-nez p2, :cond_0

    new-instance p2, Lcom/android/browser/BrowserBookmarksAdapterItem;

    invoke-direct {p2}, Lcom/android/browser/BrowserBookmarksAdapterItem;-><init>()V

    :cond_0
    iget-object v1, p2, Lcom/android/browser/BrowserBookmarksAdapterItem;->thumbnail:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_4

    iget-object v1, p2, Lcom/android/browser/BrowserBookmarksAdapterItem;->thumbnail:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    const/4 v1, 0x4

    invoke-static {p1, v1, v0}, Lcom/android/browser/BrowserBookmarksPage;->getBitmap(Landroid/database/Cursor;ILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_5

    move v1, v2

    :goto_1
    iput-boolean v1, p2, Lcom/android/browser/BrowserBookmarksAdapterItem;->has_thumbnail:Z

    if-eqz v0, :cond_2

    iget-object v1, p2, Lcom/android/browser/BrowserBookmarksAdapterItem;->thumbnail:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_1

    iget-object v1, p2, Lcom/android/browser/BrowserBookmarksAdapterItem;->thumbnail:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eq v1, v0, :cond_2

    :cond_1
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v4, p0, Lcom/android/browser/BrowserBookmarksAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v1, v4, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v1, p2, Lcom/android/browser/BrowserBookmarksAdapterItem;->thumbnail:Landroid/graphics/drawable/BitmapDrawable;

    :cond_2
    const/4 v1, 0x6

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-eqz v1, :cond_3

    move v3, v2

    :cond_3
    iput-boolean v3, p2, Lcom/android/browser/BrowserBookmarksAdapterItem;->is_folder:Z

    invoke-virtual {p0, p1}, Lcom/android/browser/BrowserBookmarksAdapter;->getTitle(Landroid/database/Cursor;)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p2, Lcom/android/browser/BrowserBookmarksAdapterItem;->title:Ljava/lang/CharSequence;

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, Lcom/android/browser/BrowserBookmarksAdapterItem;->url:Ljava/lang/String;

    return-object p2

    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    :cond_5
    move v1, v3

    goto :goto_1
.end method

.method public bridge synthetic getRowObject(Landroid/database/Cursor;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Landroid/database/Cursor;
    .param p2    # Ljava/lang/Object;

    check-cast p2, Lcom/android/browser/BrowserBookmarksAdapterItem;

    invoke-virtual {p0, p1, p2}, Lcom/android/browser/BrowserBookmarksAdapter;->getRowObject(Landroid/database/Cursor;Lcom/android/browser/BrowserBookmarksAdapterItem;)Lcom/android/browser/BrowserBookmarksAdapterItem;

    move-result-object v0

    return-object v0
.end method

.method getTitle(Landroid/database/Cursor;)Ljava/lang/CharSequence;
    .locals 3
    .param p1    # Landroid/database/Cursor;

    const/16 v1, 0x9

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v1, 0x2

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :pswitch_0
    iget-object v1, p0, Lcom/android/browser/BrowserBookmarksAdapter;->mContext:Landroid/content/Context;

    const v2, 0x7f0c0142    # com.android.browser.R.string.other_bookmarks

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public newView(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/android/browser/BrowserBookmarksAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f040009    # com.android.browser.R.layout.bookmark_thumbnail

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
