.class Lcom/kehdev/SaveGameTouchEvent$3;
.super Ljava/lang/Object;
.source "SaveGameTouchEvent.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/kehdev/SaveGameTouchEvent;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kehdev/SaveGameTouchEvent;


# direct methods
.method constructor <init>(Lcom/kehdev/SaveGameTouchEvent;)V
    .locals 0

    iput-object p1, p0, Lcom/kehdev/SaveGameTouchEvent$3;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1    # Landroid/view/View;

    const/4 v8, -0x1

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/kehdev/SaveGameTouchEvent$3;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    # getter for: Lcom/kehdev/SaveGameTouchEvent;->mSqliteOperation:Lcom/kehdev/SqliteOperation;
    invoke-static {v0}, Lcom/kehdev/SaveGameTouchEvent;->access$100(Lcom/kehdev/SaveGameTouchEvent;)Lcom/kehdev/SqliteOperation;

    move-result-object v0

    iget-object v1, p0, Lcom/kehdev/SaveGameTouchEvent$3;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    # getter for: Lcom/kehdev/SaveGameTouchEvent;->completePkgName:Ljava/lang/String;
    invoke-static {v1}, Lcom/kehdev/SaveGameTouchEvent;->access$400(Lcom/kehdev/SaveGameTouchEvent;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/kehdev/SqliteOperation;->checkIfTheGameInDatabase(Ljava/lang/String;)I

    move-result v0

    if-ne v8, v0, :cond_0

    iget-object v0, p0, Lcom/kehdev/SaveGameTouchEvent$3;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    # getter for: Lcom/kehdev/SaveGameTouchEvent;->mSqliteOperation:Lcom/kehdev/SqliteOperation;
    invoke-static {v0}, Lcom/kehdev/SaveGameTouchEvent;->access$100(Lcom/kehdev/SaveGameTouchEvent;)Lcom/kehdev/SqliteOperation;

    move-result-object v0

    iget-object v1, p0, Lcom/kehdev/SaveGameTouchEvent$3;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    # getter for: Lcom/kehdev/SaveGameTouchEvent;->completePkgName:Ljava/lang/String;
    invoke-static {v1}, Lcom/kehdev/SaveGameTouchEvent;->access$400(Lcom/kehdev/SaveGameTouchEvent;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v7}, Lcom/kehdev/SqliteOperation;->insertNewAppToDatebase(Ljava/lang/String;I)Z

    :cond_0
    iget-object v0, p0, Lcom/kehdev/SaveGameTouchEvent$3;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    # getter for: Lcom/kehdev/SaveGameTouchEvent;->mSqliteOperation:Lcom/kehdev/SqliteOperation;
    invoke-static {v0}, Lcom/kehdev/SaveGameTouchEvent;->access$100(Lcom/kehdev/SaveGameTouchEvent;)Lcom/kehdev/SqliteOperation;

    move-result-object v0

    iget-object v1, p0, Lcom/kehdev/SaveGameTouchEvent$3;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    # getter for: Lcom/kehdev/SaveGameTouchEvent;->gameId:I
    invoke-static {v1}, Lcom/kehdev/SaveGameTouchEvent;->access$500(Lcom/kehdev/SaveGameTouchEvent;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    mul-int/lit8 v1, v1, 0x19

    iget-object v2, p0, Lcom/kehdev/SaveGameTouchEvent$3;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    iget v2, v2, Lcom/kehdev/SaveGameTouchEvent;->sltKeyIndex:I

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/kehdev/SqliteOperation;->ifTouchHasSupport(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/kehdev/SaveGameTouchEvent$3;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    # getter for: Lcom/kehdev/SaveGameTouchEvent;->mSqliteOperation:Lcom/kehdev/SqliteOperation;
    invoke-static {v0}, Lcom/kehdev/SaveGameTouchEvent;->access$100(Lcom/kehdev/SaveGameTouchEvent;)Lcom/kehdev/SqliteOperation;

    move-result-object v0

    iget-object v1, p0, Lcom/kehdev/SaveGameTouchEvent$3;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    # getter for: Lcom/kehdev/SaveGameTouchEvent;->gameId:I
    invoke-static {v1}, Lcom/kehdev/SaveGameTouchEvent;->access$500(Lcom/kehdev/SaveGameTouchEvent;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    mul-int/lit8 v1, v1, 0x19

    iget-object v2, p0, Lcom/kehdev/SaveGameTouchEvent$3;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    iget v2, v2, Lcom/kehdev/SaveGameTouchEvent;->sltKeyIndex:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/kehdev/SaveGameTouchEvent$3;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    # getter for: Lcom/kehdev/SaveGameTouchEvent;->sltTypeIndex:I
    invoke-static {v2}, Lcom/kehdev/SaveGameTouchEvent;->access$600(Lcom/kehdev/SaveGameTouchEvent;)I

    move-result v2

    iget-object v3, p0, Lcom/kehdev/SaveGameTouchEvent$3;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    # getter for: Lcom/kehdev/SaveGameTouchEvent;->sltKeyMessage:Ljava/lang/String;
    invoke-static {v3}, Lcom/kehdev/SaveGameTouchEvent;->access$700(Lcom/kehdev/SaveGameTouchEvent;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/kehdev/SaveGameTouchEvent$3;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    # getter for: Lcom/kehdev/SaveGameTouchEvent;->sltCoordinateX:[I
    invoke-static {v4}, Lcom/kehdev/SaveGameTouchEvent;->access$800(Lcom/kehdev/SaveGameTouchEvent;)[I

    move-result-object v4

    iget-object v5, p0, Lcom/kehdev/SaveGameTouchEvent$3;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    # getter for: Lcom/kehdev/SaveGameTouchEvent;->sltCoordinateY:[I
    invoke-static {v5}, Lcom/kehdev/SaveGameTouchEvent;->access$900(Lcom/kehdev/SaveGameTouchEvent;)[I

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/kehdev/SqliteOperation;->updateTouchEvent(IILjava/lang/String;[I[I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/kehdev/SaveGameTouchEvent$3;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    invoke-virtual {v0}, Lcom/kehdev/SaveGameTouchEvent;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/kehdev/SaveGameTouchEvent$3;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    const v2, 0x7f07003d    # com.kehdev.R.string.updateKeySucc

    invoke-virtual {v1, v2}, Lcom/kehdev/SaveGameTouchEvent;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_0
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    const-string v0, "sltKeyMessage"

    iget-object v1, p0, Lcom/kehdev/SaveGameTouchEvent$3;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    # getter for: Lcom/kehdev/SaveGameTouchEvent;->sltKeyMessage:Ljava/lang/String;
    invoke-static {v1}, Lcom/kehdev/SaveGameTouchEvent;->access$700(Lcom/kehdev/SaveGameTouchEvent;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/kehdev/SaveGameTouchEvent$3;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    invoke-virtual {v0, v8, v6}, Lcom/kehdev/SaveGameTouchEvent;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/kehdev/SaveGameTouchEvent$3;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    invoke-virtual {v0}, Lcom/kehdev/SaveGameTouchEvent;->finish()V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/kehdev/SaveGameTouchEvent$3;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    invoke-virtual {v0}, Lcom/kehdev/SaveGameTouchEvent;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/kehdev/SaveGameTouchEvent$3;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    const v2, 0x7f07003e    # com.kehdev.R.string.updateKeyFail

    invoke-virtual {v1, v2}, Lcom/kehdev/SaveGameTouchEvent;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/kehdev/SaveGameTouchEvent$3;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    # getter for: Lcom/kehdev/SaveGameTouchEvent;->mSqliteOperation:Lcom/kehdev/SqliteOperation;
    invoke-static {v0}, Lcom/kehdev/SaveGameTouchEvent;->access$100(Lcom/kehdev/SaveGameTouchEvent;)Lcom/kehdev/SqliteOperation;

    move-result-object v0

    iget-object v1, p0, Lcom/kehdev/SaveGameTouchEvent$3;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    # getter for: Lcom/kehdev/SaveGameTouchEvent;->gameId:I
    invoke-static {v1}, Lcom/kehdev/SaveGameTouchEvent;->access$500(Lcom/kehdev/SaveGameTouchEvent;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    mul-int/lit8 v1, v1, 0x19

    iget-object v2, p0, Lcom/kehdev/SaveGameTouchEvent$3;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    iget v2, v2, Lcom/kehdev/SaveGameTouchEvent;->sltKeyIndex:I

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/kehdev/SaveGameTouchEvent$3;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    # getter for: Lcom/kehdev/SaveGameTouchEvent;->sltTypeIndex:I
    invoke-static {v2}, Lcom/kehdev/SaveGameTouchEvent;->access$600(Lcom/kehdev/SaveGameTouchEvent;)I

    move-result v2

    iget-object v3, p0, Lcom/kehdev/SaveGameTouchEvent$3;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    # getter for: Lcom/kehdev/SaveGameTouchEvent;->sltKeyMessage:Ljava/lang/String;
    invoke-static {v3}, Lcom/kehdev/SaveGameTouchEvent;->access$700(Lcom/kehdev/SaveGameTouchEvent;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/kehdev/SaveGameTouchEvent$3;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    # getter for: Lcom/kehdev/SaveGameTouchEvent;->sltCoordinateX:[I
    invoke-static {v4}, Lcom/kehdev/SaveGameTouchEvent;->access$800(Lcom/kehdev/SaveGameTouchEvent;)[I

    move-result-object v4

    iget-object v5, p0, Lcom/kehdev/SaveGameTouchEvent$3;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    # getter for: Lcom/kehdev/SaveGameTouchEvent;->sltCoordinateY:[I
    invoke-static {v5}, Lcom/kehdev/SaveGameTouchEvent;->access$900(Lcom/kehdev/SaveGameTouchEvent;)[I

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/kehdev/SqliteOperation;->insertTouchEvent(IILjava/lang/String;[I[I)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/kehdev/SaveGameTouchEvent$3;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    invoke-virtual {v0}, Lcom/kehdev/SaveGameTouchEvent;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/kehdev/SaveGameTouchEvent$3;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    const v2, 0x7f07003f    # com.kehdev.R.string.insertKeySucc

    invoke-virtual {v1, v2}, Lcom/kehdev/SaveGameTouchEvent;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/kehdev/SaveGameTouchEvent$3;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    invoke-virtual {v0}, Lcom/kehdev/SaveGameTouchEvent;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/kehdev/SaveGameTouchEvent$3;->this$0:Lcom/kehdev/SaveGameTouchEvent;

    const v2, 0x7f070040    # com.kehdev.R.string.insertKeyFail

    invoke-virtual {v1, v2}, Lcom/kehdev/SaveGameTouchEvent;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0
.end method
