.class Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;
.super Ljava/lang/Object;
.source "ReadKeyStatus.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kehdev/ReadKeyStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "readKeyStatusWorker"
.end annotation


# instance fields
.field mute_flag:Z

.field final synthetic this$0:Lcom/kehdev/ReadKeyStatus;


# direct methods
.method constructor <init>(Lcom/kehdev/ReadKeyStatus;)V
    .locals 1

    iput-object p1, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->mute_flag:Z

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    const/16 v11, 0x14

    const/16 v10, 0x13

    const/4 v9, 0x4

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v0, 0x0

    const/4 v3, 0x0

    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget-object v5, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    invoke-virtual {v5}, Lcom/kehdev/ReadKeyStatus;->startReadKeyService()I

    move-result v5

    iput v5, v4, Lcom/kehdev/ReadKeyStatus;->bing:I

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget-object v5, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v5, v5, Lcom/kehdev/ReadKeyStatus;->bing:I

    and-int/lit16 v5, v5, 0xff

    iput v5, v4, Lcom/kehdev/ReadKeyStatus;->g_state:I

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget-object v5, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v5, v5, Lcom/kehdev/ReadKeyStatus;->bing:I

    shr-int/lit8 v5, v5, 0x8

    and-int/lit16 v5, v5, 0xff

    iput v5, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget-object v5, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v5, v5, Lcom/kehdev/ReadKeyStatus;->bing:I

    shr-int/lit8 v5, v5, 0x10

    and-int/lit16 v5, v5, 0xff

    iput v5, v4, Lcom/kehdev/ReadKeyStatus;->g_keyValue:I

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->bing:I

    shr-int/lit8 v4, v4, 0x18

    and-int/lit16 v0, v4, 0xff

    const/4 v4, 0x2

    if-ne v0, v4, :cond_2

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_keyValue:I

    const/16 v5, 0x12

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_1

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    invoke-virtual {v4}, Lcom/kehdev/ReadKeyStatus;->openVoiceControl()V

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_keyValue:I

    const/16 v5, 0xe

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    sget-object v5, Lcom/kehdev/GetRunningAppName;->runningAppPkgName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/kehdev/ReadKeyStatus;->openRunningGameManual(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_state:I

    const/16 v5, 0xff

    if-eq v4, v5, :cond_0

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_state:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_4

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget-boolean v4, v4, Lcom/kehdev/ReadKeyStatus;->hadOneKeyDown:Z

    if-nez v4, :cond_3

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget-boolean v4, v4, Lcom/kehdev/ReadKeyStatus;->hadTwoKeyDown:Z

    if-eqz v4, :cond_4

    :cond_3
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    invoke-virtual {v4}, Lcom/kehdev/ReadKeyStatus;->releaseTouch()Z

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    invoke-virtual {v4}, Lcom/kehdev/ReadKeyStatus;->initTouchKey()V

    goto/16 :goto_0

    :cond_4
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_state:I

    const/16 v5, 0x20

    if-ne v4, v5, :cond_0

    new-instance v1, Landroid/app/Instrumentation;

    invoke-direct {v1}, Landroid/app/Instrumentation;-><init>()V

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_keyValue:I

    const/16 v5, 0x90

    if-ne v4, v5, :cond_7

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-ne v4, v7, :cond_6

    iget-boolean v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->mute_flag:Z

    if-eqz v4, :cond_6

    new-instance v2, Landroid/view/KeyEvent;

    const/16 v4, 0xa4

    invoke-direct {v2, v8, v4}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    iput-boolean v8, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->mute_flag:Z

    :cond_5
    :goto_1
    const-wide/16 v4, 0x64

    invoke-static {v4, v5}, Landroid/os/SystemClock;->sleep(J)V

    goto/16 :goto_0

    :cond_6
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-nez v4, :cond_5

    new-instance v2, Landroid/view/KeyEvent;

    const/16 v4, 0xa4

    invoke-direct {v2, v7, v4}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    iput-boolean v7, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->mute_flag:Z

    goto :goto_1

    :cond_7
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_keyValue:I

    const/16 v5, 0x85

    if-ne v4, v5, :cond_9

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-ne v4, v7, :cond_8

    new-instance v2, Landroid/view/KeyEvent;

    const/16 v4, 0x18

    invoke-direct {v2, v8, v4}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    goto/16 :goto_0

    :cond_8
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-nez v4, :cond_0

    new-instance v2, Landroid/view/KeyEvent;

    const/16 v4, 0x18

    invoke-direct {v2, v7, v4}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    goto/16 :goto_0

    :cond_9
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_keyValue:I

    const/16 v5, 0x86

    if-ne v4, v5, :cond_b

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-ne v4, v7, :cond_a

    new-instance v2, Landroid/view/KeyEvent;

    const/16 v4, 0x19

    invoke-direct {v2, v8, v4}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    goto/16 :goto_0

    :cond_a
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-nez v4, :cond_0

    new-instance v2, Landroid/view/KeyEvent;

    const/16 v4, 0x19

    invoke-direct {v2, v7, v4}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    goto/16 :goto_0

    :cond_b
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_keyValue:I

    const/16 v5, 0x8e

    if-ne v4, v5, :cond_d

    sget-object v4, Lcom/kehdev/GetRunningAppName;->runningAppPkgName:Ljava/lang/String;

    const-string v5, "com.app.xjiajia"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_d

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-ne v4, v7, :cond_c

    add-int/lit8 v3, v3, 0x1

    const/16 v4, 0x8

    if-ne v3, v4, :cond_0

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    sget-object v5, Lcom/kehdev/GetRunningAppName;->runningAppPkgName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/kehdev/ReadKeyStatus;->openRunningGameManual(Ljava/lang/String;)V

    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_c
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-nez v4, :cond_0

    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_d
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_keyValue:I

    const/16 v5, 0x87

    if-eq v4, v5, :cond_e

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_keyValue:I

    const/16 v5, 0x88

    if-eq v4, v5, :cond_e

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_keyValue:I

    const/16 v5, 0x89

    if-eq v4, v5, :cond_e

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_keyValue:I

    const/16 v5, 0x8d

    if-eq v4, v5, :cond_e

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_keyValue:I

    const/16 v5, 0x8b

    if-eq v4, v5, :cond_e

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_keyValue:I

    const/16 v5, 0x8f

    if-ne v4, v5, :cond_f

    :cond_e
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-eq v4, v7, :cond_0

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    invoke-virtual {v4}, Lcom/kehdev/ReadKeyStatus;->ToastShow()V

    goto/16 :goto_0

    :cond_f
    sget-object v4, Lcom/kehdev/GetRunningAppName;->runningAppPkgName:Ljava/lang/String;

    const-string v5, "com.trans.pingpang2"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_10

    sget-object v4, Lcom/kehdev/GetRunningAppName;->runningAppPkgName:Ljava/lang/String;

    const-string v5, "com.trans.tennis"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_10

    sget-object v4, Lcom/kehdev/GetRunningAppName;->runningAppPkgName:Ljava/lang/String;

    const-string v5, "com.trans.beatpenguin"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1c

    :cond_10
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_keyValue:I

    const/16 v5, 0x8c

    if-ne v4, v5, :cond_12

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-ne v4, v7, :cond_11

    new-instance v2, Landroid/view/KeyEvent;

    const/16 v4, 0x42

    invoke-direct {v2, v8, v4}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    goto/16 :goto_0

    :cond_11
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-nez v4, :cond_0

    new-instance v2, Landroid/view/KeyEvent;

    const/16 v4, 0x42

    invoke-direct {v2, v7, v4}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    goto/16 :goto_0

    :cond_12
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_keyValue:I

    sget-object v5, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_RIGHT:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    invoke-virtual {v5}, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->ordinal()I

    move-result v5

    add-int/lit16 v5, v5, 0x80

    if-ne v4, v5, :cond_14

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-ne v4, v7, :cond_13

    new-instance v2, Landroid/view/KeyEvent;

    const/16 v4, 0x16

    invoke-direct {v2, v8, v4}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    goto/16 :goto_0

    :cond_13
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-nez v4, :cond_0

    new-instance v2, Landroid/view/KeyEvent;

    const/16 v4, 0x16

    invoke-direct {v2, v7, v4}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    goto/16 :goto_0

    :cond_14
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_keyValue:I

    sget-object v5, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_LEFT:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    invoke-virtual {v5}, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->ordinal()I

    move-result v5

    add-int/lit16 v5, v5, 0x80

    if-ne v4, v5, :cond_16

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-ne v4, v7, :cond_15

    new-instance v2, Landroid/view/KeyEvent;

    const/16 v4, 0x15

    invoke-direct {v2, v8, v4}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    goto/16 :goto_0

    :cond_15
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-nez v4, :cond_0

    new-instance v2, Landroid/view/KeyEvent;

    const/16 v4, 0x15

    invoke-direct {v2, v7, v4}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    goto/16 :goto_0

    :cond_16
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_keyValue:I

    sget-object v5, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_UP:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    invoke-virtual {v5}, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->ordinal()I

    move-result v5

    add-int/lit16 v5, v5, 0x80

    if-ne v4, v5, :cond_18

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-ne v4, v7, :cond_17

    new-instance v2, Landroid/view/KeyEvent;

    invoke-direct {v2, v8, v10}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    goto/16 :goto_0

    :cond_17
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-nez v4, :cond_0

    new-instance v2, Landroid/view/KeyEvent;

    invoke-direct {v2, v7, v10}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    goto/16 :goto_0

    :cond_18
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_keyValue:I

    sget-object v5, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_DOWN:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    invoke-virtual {v5}, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->ordinal()I

    move-result v5

    add-int/lit16 v5, v5, 0x80

    if-ne v4, v5, :cond_1a

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-ne v4, v7, :cond_19

    new-instance v2, Landroid/view/KeyEvent;

    invoke-direct {v2, v8, v11}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    goto/16 :goto_0

    :cond_19
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-nez v4, :cond_0

    new-instance v2, Landroid/view/KeyEvent;

    invoke-direct {v2, v7, v11}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    goto/16 :goto_0

    :cond_1a
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_keyValue:I

    const/16 v5, 0x91

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-ne v4, v7, :cond_1b

    new-instance v2, Landroid/view/KeyEvent;

    invoke-direct {v2, v8, v9}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    goto/16 :goto_0

    :cond_1b
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-nez v4, :cond_0

    new-instance v2, Landroid/view/KeyEvent;

    invoke-direct {v2, v7, v9}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    goto/16 :goto_0

    :cond_1c
    sget-object v4, Lcom/kehdev/GetRunningAppName;->runningAppPkgName:Ljava/lang/String;

    const-string v5, "com.app.xjiajia"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2a

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_keyValue:I

    const/16 v5, 0x8c

    if-ne v4, v5, :cond_1e

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-ne v4, v7, :cond_1d

    new-instance v2, Landroid/view/KeyEvent;

    const/16 v4, 0x42

    invoke-direct {v2, v8, v4}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    goto/16 :goto_0

    :cond_1d
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-nez v4, :cond_0

    new-instance v2, Landroid/view/KeyEvent;

    const/16 v4, 0x42

    invoke-direct {v2, v7, v4}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    goto/16 :goto_0

    :cond_1e
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_keyValue:I

    const/16 v5, 0x91

    if-ne v4, v5, :cond_20

    const-string v4, "readKeyStatus"

    const-string v5, "press key B"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-ne v4, v7, :cond_1f

    new-instance v2, Landroid/view/KeyEvent;

    invoke-direct {v2, v8, v9}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    goto/16 :goto_0

    :cond_1f
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-nez v4, :cond_0

    new-instance v2, Landroid/view/KeyEvent;

    invoke-direct {v2, v7, v9}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    goto/16 :goto_0

    :cond_20
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_keyValue:I

    sget-object v5, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_RIGHT:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    invoke-virtual {v5}, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->ordinal()I

    move-result v5

    add-int/lit16 v5, v5, 0x80

    if-ne v4, v5, :cond_22

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-ne v4, v7, :cond_21

    new-instance v2, Landroid/view/KeyEvent;

    const/16 v4, 0x16

    invoke-direct {v2, v8, v4}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    goto/16 :goto_0

    :cond_21
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-nez v4, :cond_0

    new-instance v2, Landroid/view/KeyEvent;

    const/16 v4, 0x16

    invoke-direct {v2, v7, v4}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    goto/16 :goto_0

    :cond_22
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_keyValue:I

    sget-object v5, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_LEFT:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    invoke-virtual {v5}, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->ordinal()I

    move-result v5

    add-int/lit16 v5, v5, 0x80

    if-ne v4, v5, :cond_24

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-ne v4, v7, :cond_23

    new-instance v2, Landroid/view/KeyEvent;

    const/16 v4, 0x15

    invoke-direct {v2, v8, v4}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    goto/16 :goto_0

    :cond_23
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-nez v4, :cond_0

    new-instance v2, Landroid/view/KeyEvent;

    const/16 v4, 0x15

    invoke-direct {v2, v7, v4}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    goto/16 :goto_0

    :cond_24
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_keyValue:I

    sget-object v5, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_UP:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    invoke-virtual {v5}, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->ordinal()I

    move-result v5

    add-int/lit16 v5, v5, 0x80

    if-ne v4, v5, :cond_26

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-ne v4, v7, :cond_25

    new-instance v2, Landroid/view/KeyEvent;

    invoke-direct {v2, v8, v10}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    goto/16 :goto_0

    :cond_25
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-nez v4, :cond_0

    new-instance v2, Landroid/view/KeyEvent;

    invoke-direct {v2, v7, v10}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    goto/16 :goto_0

    :cond_26
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_keyValue:I

    sget-object v5, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->KEY_VALUE_DOWN:Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;

    invoke-virtual {v5}, Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;->ordinal()I

    move-result v5

    add-int/lit16 v5, v5, 0x80

    if-ne v4, v5, :cond_28

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-ne v4, v7, :cond_27

    new-instance v2, Landroid/view/KeyEvent;

    invoke-direct {v2, v8, v11}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    goto/16 :goto_0

    :cond_27
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-nez v4, :cond_0

    new-instance v2, Landroid/view/KeyEvent;

    invoke-direct {v2, v7, v11}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    goto/16 :goto_0

    :cond_28
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_keyValue:I

    const/16 v5, 0x8e

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-ne v4, v7, :cond_29

    new-instance v2, Landroid/view/KeyEvent;

    const/16 v4, 0x52

    invoke-direct {v2, v8, v4}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    goto/16 :goto_0

    :cond_29
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-nez v4, :cond_0

    new-instance v2, Landroid/view/KeyEvent;

    const/16 v4, 0x52

    invoke-direct {v2, v7, v4}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/app/Instrumentation;->sendKeySync(Landroid/view/KeyEvent;)V

    goto/16 :goto_0

    :cond_2a
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_keyValue:I

    const/16 v5, 0x8d

    if-eq v4, v5, :cond_0

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    invoke-virtual {v4}, Lcom/kehdev/ReadKeyStatus;->checkActiveKey()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    # setter for: Lcom/kehdev/ReadKeyStatus;->getKeyMessInTime:Z
    invoke-static {v4, v7}, Lcom/kehdev/ReadKeyStatus;->access$002(Lcom/kehdev/ReadKeyStatus;Z)Z

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-ne v4, v7, :cond_2b

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_keyValue:I

    iget-object v5, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v5, v5, Lcom/kehdev/ReadKeyStatus;->rem_fstKeyValue:I

    if-eq v4, v5, :cond_0

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_keyValue:I

    iget-object v5, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v5, v5, Lcom/kehdev/ReadKeyStatus;->rem_secKeyValue:I

    if-eq v4, v5, :cond_0

    :cond_2b
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget-object v5, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    invoke-virtual {v5}, Lcom/kehdev/ReadKeyStatus;->findEventType()I

    move-result v5

    iput v5, v4, Lcom/kehdev/ReadKeyStatus;->touchType:I

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget-object v5, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v5, v5, Lcom/kehdev/ReadKeyStatus;->touchType:I

    iput v5, v4, Lcom/kehdev/ReadKeyStatus;->preTouchTypeData:I

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget-object v5, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v5, v5, Lcom/kehdev/ReadKeyStatus;->g_remValue:I

    iput v5, v4, Lcom/kehdev/ReadKeyStatus;->g_preValue:I

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    sget-object v5, Lcom/kehdev/GetRunningAppName;->runningAppPkgName:Ljava/lang/String;

    iput-object v5, v4, Lcom/kehdev/ReadKeyStatus;->g_preRunAppName:Ljava/lang/String;

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->touchType:I

    packed-switch v4, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    invoke-virtual {v4}, Lcom/kehdev/ReadKeyStatus;->chang2TouchEvent()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    invoke-virtual {v4}, Lcom/kehdev/ReadKeyStatus;->sendTouchEvent()Z

    goto/16 :goto_0

    :pswitch_1
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    invoke-virtual {v4}, Lcom/kehdev/ReadKeyStatus;->sendMoveSingle()Z

    goto/16 :goto_0

    :pswitch_2
    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    const-wide/16 v5, 0x0

    iput-wide v5, v4, Lcom/kehdev/ReadKeyStatus;->begin:J

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    iget v4, v4, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;->this$0:Lcom/kehdev/ReadKeyStatus;

    invoke-virtual {v4}, Lcom/kehdev/ReadKeyStatus;->sendMoveDouble()Z

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
