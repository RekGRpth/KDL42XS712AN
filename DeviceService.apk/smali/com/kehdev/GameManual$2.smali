.class Lcom/kehdev/GameManual$2;
.super Ljava/lang/Object;
.source "GameManual.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/kehdev/GameManual;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kehdev/GameManual;


# direct methods
.method constructor <init>(Lcom/kehdev/GameManual;)V
    .locals 0

    iput-object p1, p0, Lcom/kehdev/GameManual$2;->this$0:Lcom/kehdev/GameManual;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/kehdev/GameManual$2;->this$0:Lcom/kehdev/GameManual;

    # setter for: Lcom/kehdev/GameManual;->saveSltItem:I
    invoke-static {v0, p3}, Lcom/kehdev/GameManual;->access$502(Lcom/kehdev/GameManual;I)I

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    packed-switch p3, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/kehdev/GameManual$2;->this$0:Lcom/kehdev/GameManual;

    const-class v2, Lcom/kehdev/AxisSelect;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string v1, "AppPkgCompleteName"

    iget-object v2, p0, Lcom/kehdev/GameManual$2;->this$0:Lcom/kehdev/GameManual;

    # getter for: Lcom/kehdev/GameManual;->completePkgName:Ljava/lang/String;
    invoke-static {v2}, Lcom/kehdev/GameManual;->access$400(Lcom/kehdev/GameManual;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/kehdev/GameManual$2;->this$0:Lcom/kehdev/GameManual;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/kehdev/GameManual;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
