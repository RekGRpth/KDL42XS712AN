.class Lcom/kehdev/DeviceService$5;
.super Ljava/lang/Object;
.source "DeviceService.java"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/kehdev/DeviceService;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kehdev/DeviceService;


# direct methods
.method constructor <init>(Lcom/kehdev/DeviceService;)V
    .locals 0

    iput-object p1, p0, Lcom/kehdev/DeviceService$5;->this$0:Lcom/kehdev/DeviceService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 3
    .param p1    # Landroid/widget/RadioGroup;
    .param p2    # I

    iget-object v0, p0, Lcom/kehdev/DeviceService$5;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->systemVoiceControl:Landroid/widget/RadioButton;
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$400(Lcom/kehdev/DeviceService;)Landroid/widget/RadioButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/RadioButton;->getId()I

    move-result v0

    if-ne v0, p2, :cond_0

    iget-object v0, p0, Lcom/kehdev/DeviceService$5;->this$0:Lcom/kehdev/DeviceService;

    sget v1, Lcom/kehdev/ReadKeyStatus;->select_talk:I

    # setter for: Lcom/kehdev/DeviceService;->remStateBeforeSystemVoice:I
    invoke-static {v0, v1}, Lcom/kehdev/DeviceService;->access$502(Lcom/kehdev/DeviceService;I)I

    const/4 v0, -0x1

    sput v0, Lcom/kehdev/ReadKeyStatus;->select_talk:I

    iget-object v0, p0, Lcom/kehdev/DeviceService$5;->this$0:Lcom/kehdev/DeviceService;

    iget-object v0, v0, Lcom/kehdev/DeviceService;->googlevoiceSearchButton:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/kehdev/DeviceService$5;->this$0:Lcom/kehdev/DeviceService;

    iget-object v0, v0, Lcom/kehdev/DeviceService;->googlevoiceSearchButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/kehdev/DeviceService$5;->this$0:Lcom/kehdev/DeviceService;

    invoke-virtual {v1}, Lcom/kehdev/DeviceService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02003d    # com.kehdev.R.drawable.voice_search_disable

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/kehdev/DeviceService$5;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->remStateBeforeSystemVoice:I
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$500(Lcom/kehdev/DeviceService;)I

    move-result v0

    sput v0, Lcom/kehdev/ReadKeyStatus;->select_talk:I

    iget-object v0, p0, Lcom/kehdev/DeviceService$5;->this$0:Lcom/kehdev/DeviceService;

    invoke-virtual {v0}, Lcom/kehdev/DeviceService;->voiceControl()V

    goto :goto_0
.end method
