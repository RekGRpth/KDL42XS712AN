.class Lcom/kehdev/DeviceService$7;
.super Ljava/lang/Object;
.source "DeviceService.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/kehdev/DeviceService;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kehdev/DeviceService;


# direct methods
.method constructor <init>(Lcom/kehdev/DeviceService;)V
    .locals 0

    iput-object p1, p0, Lcom/kehdev/DeviceService$7;->this$0:Lcom/kehdev/DeviceService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    packed-switch p3, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/kehdev/DeviceService$7;->this$0:Lcom/kehdev/DeviceService;

    invoke-virtual {v0}, Lcom/kehdev/DeviceService;->voiceControl()V

    iget-object v0, p0, Lcom/kehdev/DeviceService$7;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->viewFlip:Landroid/widget/ViewFlipper;
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$700(Lcom/kehdev/DeviceService;)Landroid/widget/ViewFlipper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/kehdev/DeviceService$7;->this$0:Lcom/kehdev/DeviceService;

    invoke-virtual {v0}, Lcom/kehdev/DeviceService;->setupSensorGameViews()V

    iget-object v0, p0, Lcom/kehdev/DeviceService$7;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->viewFlip:Landroid/widget/ViewFlipper;
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$700(Lcom/kehdev/DeviceService;)Landroid/widget/ViewFlipper;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/kehdev/DeviceService$7;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->viewFlip:Landroid/widget/ViewFlipper;
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$700(Lcom/kehdev/DeviceService;)Landroid/widget/ViewFlipper;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/kehdev/DeviceService$7;->this$0:Lcom/kehdev/DeviceService;

    invoke-virtual {v0}, Lcom/kehdev/DeviceService;->MatchCode()V

    iget-object v0, p0, Lcom/kehdev/DeviceService$7;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->viewFlip:Landroid/widget/ViewFlipper;
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$700(Lcom/kehdev/DeviceService;)Landroid/widget/ViewFlipper;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/kehdev/DeviceService$7;->this$0:Lcom/kehdev/DeviceService;

    invoke-virtual {v0}, Lcom/kehdev/DeviceService;->softandupdate()V

    iget-object v0, p0, Lcom/kehdev/DeviceService$7;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->viewFlip:Landroid/widget/ViewFlipper;
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$700(Lcom/kehdev/DeviceService;)Landroid/widget/ViewFlipper;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
