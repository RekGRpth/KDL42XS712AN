.class public Lcom/kehdev/SaveGameTouchEvent;
.super Landroid/app/Activity;
.source "SaveGameTouchEvent.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private bindSucc:Z

.field private completePkgName:Ljava/lang/String;

.field private final coorItemOffset:I

.field private gameId:I

.field private getSltAppBitmap:Landroid/graphics/Bitmap;

.field private getSltLableName:Ljava/lang/String;

.field private indexToKeyTitle:[Ljava/lang/String;

.field private indexkeyToKeyType:[Ljava/lang/String;

.field private intentGet:Landroid/content/Intent;

.field private keyMessageListView:Landroid/widget/ListView;

.field private mRunningAppName:Lcom/kehdev/GetRunningAppName;

.field private mRunningAppNameConn:Landroid/content/ServiceConnection;

.field private mSqliteOperation:Lcom/kehdev/SqliteOperation;

.field private final maxKeyPerGame:I

.field private p1_end_active:Z

.field private p1_start_active:Z

.field private p2_end_active:Z

.field private p2_start_active:Z

.field private saveButt:Landroid/widget/Button;

.field private sltCoordinateX:[I

.field private sltCoordinateY:[I

.field private sltItemIndex:I

.field public sltKeyIndex:I

.field private sltKeyMessage:Ljava/lang/String;

.field private sltTypeIndex:I


# direct methods
.method public constructor <init>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v1, -0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const-string v0, "SaveGameTouchEvent"

    iput-object v0, p0, Lcom/kehdev/SaveGameTouchEvent;->TAG:Ljava/lang/String;

    iput-object v3, p0, Lcom/kehdev/SaveGameTouchEvent;->keyMessageListView:Landroid/widget/ListView;

    iput-object v3, p0, Lcom/kehdev/SaveGameTouchEvent;->getSltLableName:Ljava/lang/String;

    iput-object v3, p0, Lcom/kehdev/SaveGameTouchEvent;->getSltAppBitmap:Landroid/graphics/Bitmap;

    iput-object v3, p0, Lcom/kehdev/SaveGameTouchEvent;->intentGet:Landroid/content/Intent;

    iput-object v3, p0, Lcom/kehdev/SaveGameTouchEvent;->completePkgName:Ljava/lang/String;

    iput-object v3, p0, Lcom/kehdev/SaveGameTouchEvent;->mRunningAppName:Lcom/kehdev/GetRunningAppName;

    iput v1, p0, Lcom/kehdev/SaveGameTouchEvent;->gameId:I

    iput v1, p0, Lcom/kehdev/SaveGameTouchEvent;->sltKeyIndex:I

    iput v1, p0, Lcom/kehdev/SaveGameTouchEvent;->sltItemIndex:I

    const/16 v0, 0x19

    iput v0, p0, Lcom/kehdev/SaveGameTouchEvent;->maxKeyPerGame:I

    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "A4"

    aput-object v1, v0, v4

    const/4 v1, 0x1

    const-string v2, "A6"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "A3"

    aput-object v2, v0, v1

    const-string v1, "A5"

    aput-object v1, v0, v5

    const-string v1, "A2"

    aput-object v1, v0, v6

    const/4 v1, 0x5

    const-string v2, "A15"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "A17"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "A16"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "A18"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "A14"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/kehdev/SaveGameTouchEvent;->indexToKeyTitle:[Ljava/lang/String;

    new-array v0, v5, [Ljava/lang/String;

    iput-object v0, p0, Lcom/kehdev/SaveGameTouchEvent;->indexkeyToKeyType:[Ljava/lang/String;

    iput v4, p0, Lcom/kehdev/SaveGameTouchEvent;->sltTypeIndex:I

    const-string v0, "null"

    iput-object v0, p0, Lcom/kehdev/SaveGameTouchEvent;->sltKeyMessage:Ljava/lang/String;

    new-array v0, v6, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/kehdev/SaveGameTouchEvent;->sltCoordinateX:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/kehdev/SaveGameTouchEvent;->sltCoordinateY:[I

    iput v5, p0, Lcom/kehdev/SaveGameTouchEvent;->coorItemOffset:I

    iput-object v3, p0, Lcom/kehdev/SaveGameTouchEvent;->saveButt:Landroid/widget/Button;

    iput-boolean v4, p0, Lcom/kehdev/SaveGameTouchEvent;->bindSucc:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/kehdev/SaveGameTouchEvent;->p1_start_active:Z

    iput-boolean v4, p0, Lcom/kehdev/SaveGameTouchEvent;->p1_end_active:Z

    iput-boolean v4, p0, Lcom/kehdev/SaveGameTouchEvent;->p2_start_active:Z

    iput-boolean v4, p0, Lcom/kehdev/SaveGameTouchEvent;->p2_end_active:Z

    new-instance v0, Lcom/kehdev/SaveGameTouchEvent$1;

    invoke-direct {v0, p0}, Lcom/kehdev/SaveGameTouchEvent$1;-><init>(Lcom/kehdev/SaveGameTouchEvent;)V

    iput-object v0, p0, Lcom/kehdev/SaveGameTouchEvent;->mRunningAppNameConn:Landroid/content/ServiceConnection;

    return-void

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method static synthetic access$000(Lcom/kehdev/SaveGameTouchEvent;)Lcom/kehdev/GetRunningAppName;
    .locals 1
    .param p0    # Lcom/kehdev/SaveGameTouchEvent;

    iget-object v0, p0, Lcom/kehdev/SaveGameTouchEvent;->mRunningAppName:Lcom/kehdev/GetRunningAppName;

    return-object v0
.end method

.method static synthetic access$002(Lcom/kehdev/SaveGameTouchEvent;Lcom/kehdev/GetRunningAppName;)Lcom/kehdev/GetRunningAppName;
    .locals 0
    .param p0    # Lcom/kehdev/SaveGameTouchEvent;
    .param p1    # Lcom/kehdev/GetRunningAppName;

    iput-object p1, p0, Lcom/kehdev/SaveGameTouchEvent;->mRunningAppName:Lcom/kehdev/GetRunningAppName;

    return-object p1
.end method

.method static synthetic access$100(Lcom/kehdev/SaveGameTouchEvent;)Lcom/kehdev/SqliteOperation;
    .locals 1
    .param p0    # Lcom/kehdev/SaveGameTouchEvent;

    iget-object v0, p0, Lcom/kehdev/SaveGameTouchEvent;->mSqliteOperation:Lcom/kehdev/SqliteOperation;

    return-object v0
.end method

.method static synthetic access$102(Lcom/kehdev/SaveGameTouchEvent;Lcom/kehdev/SqliteOperation;)Lcom/kehdev/SqliteOperation;
    .locals 0
    .param p0    # Lcom/kehdev/SaveGameTouchEvent;
    .param p1    # Lcom/kehdev/SqliteOperation;

    iput-object p1, p0, Lcom/kehdev/SaveGameTouchEvent;->mSqliteOperation:Lcom/kehdev/SqliteOperation;

    return-object p1
.end method

.method static synthetic access$202(Lcom/kehdev/SaveGameTouchEvent;I)I
    .locals 0
    .param p0    # Lcom/kehdev/SaveGameTouchEvent;
    .param p1    # I

    iput p1, p0, Lcom/kehdev/SaveGameTouchEvent;->sltItemIndex:I

    return p1
.end method

.method static synthetic access$300(Lcom/kehdev/SaveGameTouchEvent;)Z
    .locals 1
    .param p0    # Lcom/kehdev/SaveGameTouchEvent;

    iget-boolean v0, p0, Lcom/kehdev/SaveGameTouchEvent;->p1_start_active:Z

    return v0
.end method

.method static synthetic access$400(Lcom/kehdev/SaveGameTouchEvent;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/kehdev/SaveGameTouchEvent;

    iget-object v0, p0, Lcom/kehdev/SaveGameTouchEvent;->completePkgName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/kehdev/SaveGameTouchEvent;)I
    .locals 1
    .param p0    # Lcom/kehdev/SaveGameTouchEvent;

    iget v0, p0, Lcom/kehdev/SaveGameTouchEvent;->gameId:I

    return v0
.end method

.method static synthetic access$600(Lcom/kehdev/SaveGameTouchEvent;)I
    .locals 1
    .param p0    # Lcom/kehdev/SaveGameTouchEvent;

    iget v0, p0, Lcom/kehdev/SaveGameTouchEvent;->sltTypeIndex:I

    return v0
.end method

.method static synthetic access$700(Lcom/kehdev/SaveGameTouchEvent;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/kehdev/SaveGameTouchEvent;

    iget-object v0, p0, Lcom/kehdev/SaveGameTouchEvent;->sltKeyMessage:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800(Lcom/kehdev/SaveGameTouchEvent;)[I
    .locals 1
    .param p0    # Lcom/kehdev/SaveGameTouchEvent;

    iget-object v0, p0, Lcom/kehdev/SaveGameTouchEvent;->sltCoordinateX:[I

    return-object v0
.end method

.method static synthetic access$900(Lcom/kehdev/SaveGameTouchEvent;)[I
    .locals 1
    .param p0    # Lcom/kehdev/SaveGameTouchEvent;

    iget-object v0, p0, Lcom/kehdev/SaveGameTouchEvent;->sltCoordinateY:[I

    return-object v0
.end method


# virtual methods
.method public checkAllMessage()Z
    .locals 10

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    iget-object v2, p0, Lcom/kehdev/SaveGameTouchEvent;->mSqliteOperation:Lcom/kehdev/SqliteOperation;

    iget-object v3, p0, Lcom/kehdev/SaveGameTouchEvent;->completePkgName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/kehdev/SqliteOperation;->checkIfTheGameInDatabase(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/kehdev/SaveGameTouchEvent;->gameId:I

    const/4 v2, -0x1

    iget v3, p0, Lcom/kehdev/SaveGameTouchEvent;->gameId:I

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/kehdev/SaveGameTouchEvent;->mSqliteOperation:Lcom/kehdev/SqliteOperation;

    sget v3, Lcom/kehdev/ReadKeyStatus;->screenHeight:I

    iget v4, p0, Lcom/kehdev/SaveGameTouchEvent;->sltKeyIndex:I

    iget v5, p0, Lcom/kehdev/SaveGameTouchEvent;->gameId:I

    add-int/lit8 v5, v5, -0x1

    mul-int/lit8 v5, v5, 0x19

    add-int/2addr v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/kehdev/SqliteOperation;->checkScreenTouchMess(II)[I

    move-result-object v1

    if-eqz v1, :cond_0

    aget v2, v1, v7

    iput v2, p0, Lcom/kehdev/SaveGameTouchEvent;->sltTypeIndex:I

    iget-object v2, p0, Lcom/kehdev/SaveGameTouchEvent;->sltCoordinateX:[I

    aget v3, v1, v6

    aput v3, v2, v7

    iget-object v2, p0, Lcom/kehdev/SaveGameTouchEvent;->sltCoordinateY:[I

    aget v3, v1, v8

    aput v3, v2, v7

    iget-object v2, p0, Lcom/kehdev/SaveGameTouchEvent;->sltCoordinateX:[I

    aget v3, v1, v9

    aput v3, v2, v8

    iget-object v2, p0, Lcom/kehdev/SaveGameTouchEvent;->sltCoordinateY:[I

    const/4 v3, 0x4

    aget v3, v1, v3

    aput v3, v2, v8

    iget-object v2, p0, Lcom/kehdev/SaveGameTouchEvent;->sltCoordinateX:[I

    const/4 v3, 0x5

    aget v3, v1, v3

    aput v3, v2, v6

    iget-object v2, p0, Lcom/kehdev/SaveGameTouchEvent;->sltCoordinateY:[I

    const/4 v3, 0x6

    aget v3, v1, v3

    aput v3, v2, v6

    iget-object v2, p0, Lcom/kehdev/SaveGameTouchEvent;->sltCoordinateX:[I

    const/4 v3, 0x7

    aget v3, v1, v3

    aput v3, v2, v9

    iget-object v2, p0, Lcom/kehdev/SaveGameTouchEvent;->sltCoordinateY:[I

    const/16 v3, 0x8

    aget v3, v1, v3

    aput v3, v2, v9

    :cond_0
    iget-object v2, p0, Lcom/kehdev/SaveGameTouchEvent;->mSqliteOperation:Lcom/kehdev/SqliteOperation;

    iget v3, p0, Lcom/kehdev/SaveGameTouchEvent;->sltKeyIndex:I

    iget v4, p0, Lcom/kehdev/SaveGameTouchEvent;->gameId:I

    add-int/lit8 v4, v4, -0x1

    mul-int/lit8 v4, v4, 0x19

    add-int/2addr v3, v4

    invoke-virtual {v2, v3}, Lcom/kehdev/SqliteOperation;->findTouchDescription(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iput-object v0, p0, Lcom/kehdev/SaveGameTouchEvent;->sltKeyMessage:Ljava/lang/String;

    :cond_1
    return v6
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 7
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    const/4 v4, -0x1

    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    const/4 v1, 0x0

    if-nez p1, :cond_0

    if-ne p2, v4, :cond_0

    iget v2, p0, Lcom/kehdev/SaveGameTouchEvent;->sltItemIndex:I

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    const/4 v0, 0x0

    const-string v2, "sltTypeIndex"

    iget v3, p0, Lcom/kehdev/SaveGameTouchEvent;->sltTypeIndex:I

    invoke-virtual {p3, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/kehdev/SaveGameTouchEvent;->sltTypeIndex:I

    iget v2, p0, Lcom/kehdev/SaveGameTouchEvent;->sltTypeIndex:I

    if-eq v2, v4, :cond_1

    :cond_1
    iget v2, p0, Lcom/kehdev/SaveGameTouchEvent;->sltTypeIndex:I

    invoke-virtual {p0, v2}, Lcom/kehdev/SaveGameTouchEvent;->setNoActiveItem(I)V

    goto :goto_0

    :pswitch_2
    iget-object v2, p0, Lcom/kehdev/SaveGameTouchEvent;->sltCoordinateX:[I

    iget v3, p0, Lcom/kehdev/SaveGameTouchEvent;->sltItemIndex:I

    add-int/lit8 v3, v3, -0x3

    const-string v4, "sltCoordinateX"

    iget-object v5, p0, Lcom/kehdev/SaveGameTouchEvent;->sltCoordinateX:[I

    iget v6, p0, Lcom/kehdev/SaveGameTouchEvent;->sltItemIndex:I

    add-int/lit8 v6, v6, -0x3

    aget v5, v5, v6

    invoke-virtual {p3, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    aput v4, v2, v3

    iget-object v2, p0, Lcom/kehdev/SaveGameTouchEvent;->sltCoordinateY:[I

    iget v3, p0, Lcom/kehdev/SaveGameTouchEvent;->sltItemIndex:I

    add-int/lit8 v3, v3, -0x3

    const-string v4, "sltCoordinateY"

    iget-object v5, p0, Lcom/kehdev/SaveGameTouchEvent;->sltCoordinateY:[I

    iget v6, p0, Lcom/kehdev/SaveGameTouchEvent;->sltItemIndex:I

    add-int/lit8 v6, v6, -0x3

    aget v5, v5, v6

    invoke-virtual {p3, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    aput v4, v2, v3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "< "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/kehdev/SaveGameTouchEvent;->sltCoordinateX:[I

    iget v4, p0, Lcom/kehdev/SaveGameTouchEvent;->sltItemIndex:I

    add-int/lit8 v4, v4, -0x3

    aget v3, v3, v4

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/kehdev/SaveGameTouchEvent;->sltCoordinateY:[I

    iget v4, p0, Lcom/kehdev/SaveGameTouchEvent;->sltItemIndex:I

    add-int/lit8 v4, v4, -0x3

    aget v3, v3, v4

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " >"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/kehdev/SaveGameTouchEvent;->sltCoordinateX:[I

    iget v3, p0, Lcom/kehdev/SaveGameTouchEvent;->sltItemIndex:I

    add-int/lit8 v3, v3, -0x3

    aget v2, v2, v3

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/kehdev/SaveGameTouchEvent;->sltCoordinateY:[I

    iget v3, p0, Lcom/kehdev/SaveGameTouchEvent;->sltItemIndex:I

    add-int/lit8 v3, v3, -0x3

    aget v2, v2, v3

    if-eqz v2, :cond_0

    goto/16 :goto_0

    :pswitch_3
    const-string v2, "sltKeyMessage"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/kehdev/SaveGameTouchEvent;->sltKeyMessage:Ljava/lang/String;

    iget-object v2, p0, Lcom/kehdev/SaveGameTouchEvent;->sltKeyMessage:Ljava/lang/String;

    if-eqz v2, :cond_2

    :goto_1
    iget-object v2, p0, Lcom/kehdev/SaveGameTouchEvent;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sltKeyMessage :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/kehdev/SaveGameTouchEvent;->sltKeyMessage:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_2
    const-string v2, "null"

    iput-object v2, p0, Lcom/kehdev/SaveGameTouchEvent;->sltKeyMessage:Ljava/lang/String;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0, v3}, Lcom/kehdev/SaveGameTouchEvent;->requestWindowFeature(I)Z

    const v0, 0x7f030012    # com.kehdev.R.layout.save_game_touch_event_layout

    invoke-virtual {p0, v0}, Lcom/kehdev/SaveGameTouchEvent;->setContentView(I)V

    iget-object v0, p0, Lcom/kehdev/SaveGameTouchEvent;->indexkeyToKeyType:[Ljava/lang/String;

    const/4 v1, 0x0

    const v2, 0x7f070027    # com.kehdev.R.string.press

    invoke-virtual {p0, v2}, Lcom/kehdev/SaveGameTouchEvent;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/kehdev/SaveGameTouchEvent;->indexkeyToKeyType:[Ljava/lang/String;

    const v1, 0x7f070028    # com.kehdev.R.string.singleSlide

    invoke-virtual {p0, v1}, Lcom/kehdev/SaveGameTouchEvent;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    iget-object v0, p0, Lcom/kehdev/SaveGameTouchEvent;->indexkeyToKeyType:[Ljava/lang/String;

    const/4 v1, 0x2

    const v2, 0x7f070029    # com.kehdev.R.string.doubleSlide

    invoke-virtual {p0, v2}, Lcom/kehdev/SaveGameTouchEvent;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/kehdev/GetRunningAppName;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/kehdev/SaveGameTouchEvent;->mRunningAppNameConn:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0, v1, v3}, Lcom/kehdev/SaveGameTouchEvent;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/kehdev/SaveGameTouchEvent;->bindSucc:Z

    const v0, 0x7f090051    # com.kehdev.R.id.keyMessageListV

    invoke-virtual {p0, v0}, Lcom/kehdev/SaveGameTouchEvent;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/kehdev/SaveGameTouchEvent;->keyMessageListView:Landroid/widget/ListView;

    const v0, 0x7f090052    # com.kehdev.R.id.saveButt

    invoke-virtual {p0, v0}, Lcom/kehdev/SaveGameTouchEvent;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/kehdev/SaveGameTouchEvent;->saveButt:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/kehdev/SaveGameTouchEvent;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/kehdev/SaveGameTouchEvent;->intentGet:Landroid/content/Intent;

    iget-object v0, p0, Lcom/kehdev/SaveGameTouchEvent;->intentGet:Landroid/content/Intent;

    const-string v1, "AppPkgCompleteName"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kehdev/SaveGameTouchEvent;->completePkgName:Ljava/lang/String;

    iget-object v0, p0, Lcom/kehdev/SaveGameTouchEvent;->intentGet:Landroid/content/Intent;

    const-string v1, "AppIconImage"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/kehdev/SaveGameTouchEvent;->getSltAppBitmap:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/kehdev/SaveGameTouchEvent;->intentGet:Landroid/content/Intent;

    const-string v1, "AppName"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kehdev/SaveGameTouchEvent;->getSltLableName:Ljava/lang/String;

    iget-object v0, p0, Lcom/kehdev/SaveGameTouchEvent;->intentGet:Landroid/content/Intent;

    const-string v1, "sltKeyIndex"

    iget v2, p0, Lcom/kehdev/SaveGameTouchEvent;->sltKeyIndex:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/kehdev/SaveGameTouchEvent;->sltKeyIndex:I

    iget-object v0, p0, Lcom/kehdev/SaveGameTouchEvent;->keyMessageListView:Landroid/widget/ListView;

    new-instance v1, Lcom/kehdev/SaveGameTouchEvent$2;

    invoke-direct {v1, p0}, Lcom/kehdev/SaveGameTouchEvent$2;-><init>(Lcom/kehdev/SaveGameTouchEvent;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/kehdev/SaveGameTouchEvent;->saveButt:Landroid/widget/Button;

    new-instance v1, Lcom/kehdev/SaveGameTouchEvent$3;

    invoke-direct {v1, p0}, Lcom/kehdev/SaveGameTouchEvent$3;-><init>(Lcom/kehdev/SaveGameTouchEvent;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onStop()V
    .locals 1

    iget-boolean v0, p0, Lcom/kehdev/SaveGameTouchEvent;->bindSucc:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/kehdev/SaveGameTouchEvent;->mRunningAppNameConn:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/kehdev/SaveGameTouchEvent;->unbindService(Landroid/content/ServiceConnection;)V

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method public setMessageInCorr()Z
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x1

    return v1
.end method

.method public setNoActiveItem(I)V
    .locals 4
    .param p1    # I

    const/4 v3, 0x0

    const/4 v1, 0x0

    const-string v0, ""

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/kehdev/SaveGameTouchEvent;->p1_start_active:Z

    iput-boolean v3, p0, Lcom/kehdev/SaveGameTouchEvent;->p1_end_active:Z

    iput-boolean v3, p0, Lcom/kehdev/SaveGameTouchEvent;->p2_start_active:Z

    iput-boolean v3, p0, Lcom/kehdev/SaveGameTouchEvent;->p2_end_active:Z

    return-void
.end method
