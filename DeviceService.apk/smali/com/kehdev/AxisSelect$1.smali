.class Lcom/kehdev/AxisSelect$1;
.super Ljava/lang/Object;
.source "AxisSelect.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kehdev/AxisSelect;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kehdev/AxisSelect;


# direct methods
.method constructor <init>(Lcom/kehdev/AxisSelect;)V
    .locals 0

    iput-object p1, p0, Lcom/kehdev/AxisSelect$1;->this$0:Lcom/kehdev/AxisSelect;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 7
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    const v6, 0x7f020036    # com.kehdev.R.drawable.trans_background

    const/16 v5, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/kehdev/AxisSelect$1;->this$0:Lcom/kehdev/AxisSelect;

    check-cast p2, Lcom/kehdev/GetRunningAppName$LocalBinder;

    invoke-virtual {p2}, Lcom/kehdev/GetRunningAppName$LocalBinder;->getService()Lcom/kehdev/GetRunningAppName;

    move-result-object v1

    # setter for: Lcom/kehdev/AxisSelect;->mRunningAppName:Lcom/kehdev/GetRunningAppName;
    invoke-static {v0, v1}, Lcom/kehdev/AxisSelect;->access$002(Lcom/kehdev/AxisSelect;Lcom/kehdev/GetRunningAppName;)Lcom/kehdev/GetRunningAppName;

    iget-object v0, p0, Lcom/kehdev/AxisSelect$1;->this$0:Lcom/kehdev/AxisSelect;

    # getter for: Lcom/kehdev/AxisSelect;->mRunningAppName:Lcom/kehdev/GetRunningAppName;
    invoke-static {v0}, Lcom/kehdev/AxisSelect;->access$000(Lcom/kehdev/AxisSelect;)Lcom/kehdev/GetRunningAppName;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/kehdev/AxisSelect$1;->this$0:Lcom/kehdev/AxisSelect;

    iget-object v1, p0, Lcom/kehdev/AxisSelect$1;->this$0:Lcom/kehdev/AxisSelect;

    # getter for: Lcom/kehdev/AxisSelect;->mRunningAppName:Lcom/kehdev/GetRunningAppName;
    invoke-static {v1}, Lcom/kehdev/AxisSelect;->access$000(Lcom/kehdev/AxisSelect;)Lcom/kehdev/GetRunningAppName;

    move-result-object v1

    iget-object v1, v1, Lcom/kehdev/GetRunningAppName;->mSqliteOperation:Lcom/kehdev/SqliteOperation;

    # setter for: Lcom/kehdev/AxisSelect;->mSqliteOperation:Lcom/kehdev/SqliteOperation;
    invoke-static {v0, v1}, Lcom/kehdev/AxisSelect;->access$102(Lcom/kehdev/AxisSelect;Lcom/kehdev/SqliteOperation;)Lcom/kehdev/SqliteOperation;

    iget-object v0, p0, Lcom/kehdev/AxisSelect$1;->this$0:Lcom/kehdev/AxisSelect;

    # getter for: Lcom/kehdev/AxisSelect;->mSqliteOperation:Lcom/kehdev/SqliteOperation;
    invoke-static {v0}, Lcom/kehdev/AxisSelect;->access$100(Lcom/kehdev/AxisSelect;)Lcom/kehdev/SqliteOperation;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/kehdev/AxisSelect$1;->this$0:Lcom/kehdev/AxisSelect;

    # setter for: Lcom/kehdev/AxisSelect;->bindSucc:Z
    invoke-static {v0, v4}, Lcom/kehdev/AxisSelect;->access$202(Lcom/kehdev/AxisSelect;Z)Z

    iget-object v0, p0, Lcom/kehdev/AxisSelect$1;->this$0:Lcom/kehdev/AxisSelect;

    iget-object v1, p0, Lcom/kehdev/AxisSelect$1;->this$0:Lcom/kehdev/AxisSelect;

    # getter for: Lcom/kehdev/AxisSelect;->mSqliteOperation:Lcom/kehdev/SqliteOperation;
    invoke-static {v1}, Lcom/kehdev/AxisSelect;->access$100(Lcom/kehdev/AxisSelect;)Lcom/kehdev/SqliteOperation;

    move-result-object v1

    iget-object v2, p0, Lcom/kehdev/AxisSelect$1;->this$0:Lcom/kehdev/AxisSelect;

    # getter for: Lcom/kehdev/AxisSelect;->completePkgName:Ljava/lang/String;
    invoke-static {v2}, Lcom/kehdev/AxisSelect;->access$400(Lcom/kehdev/AxisSelect;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/kehdev/SqliteOperation;->checkIfTheGameInDatabase(Ljava/lang/String;)I

    move-result v1

    # setter for: Lcom/kehdev/AxisSelect;->gameId:I
    invoke-static {v0, v1}, Lcom/kehdev/AxisSelect;->access$302(Lcom/kehdev/AxisSelect;I)I

    iget-object v0, p0, Lcom/kehdev/AxisSelect$1;->this$0:Lcom/kehdev/AxisSelect;

    # getter for: Lcom/kehdev/AxisSelect;->gameId:I
    invoke-static {v0}, Lcom/kehdev/AxisSelect;->access$300(Lcom/kehdev/AxisSelect;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/kehdev/AxisSelect$1;->this$0:Lcom/kehdev/AxisSelect;

    iget-object v1, p0, Lcom/kehdev/AxisSelect$1;->this$0:Lcom/kehdev/AxisSelect;

    # getter for: Lcom/kehdev/AxisSelect;->mSqliteOperation:Lcom/kehdev/SqliteOperation;
    invoke-static {v1}, Lcom/kehdev/AxisSelect;->access$100(Lcom/kehdev/AxisSelect;)Lcom/kehdev/SqliteOperation;

    move-result-object v1

    iget-object v2, p0, Lcom/kehdev/AxisSelect$1;->this$0:Lcom/kehdev/AxisSelect;

    # getter for: Lcom/kehdev/AxisSelect;->completePkgName:Ljava/lang/String;
    invoke-static {v2}, Lcom/kehdev/AxisSelect;->access$400(Lcom/kehdev/AxisSelect;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/kehdev/SqliteOperation;->findGameAxis(Ljava/lang/String;)I

    move-result v1

    # setter for: Lcom/kehdev/AxisSelect;->axisSlts:I
    invoke-static {v0, v1}, Lcom/kehdev/AxisSelect;->access$502(Lcom/kehdev/AxisSelect;I)I

    :goto_1
    iget-object v0, p0, Lcom/kehdev/AxisSelect$1;->this$0:Lcom/kehdev/AxisSelect;

    # getter for: Lcom/kehdev/AxisSelect;->axisSlts:I
    invoke-static {v0}, Lcom/kehdev/AxisSelect;->access$500(Lcom/kehdev/AxisSelect;)I

    move-result v0

    if-ne v0, v4, :cond_3

    iget-object v0, p0, Lcom/kehdev/AxisSelect$1;->this$0:Lcom/kehdev/AxisSelect;

    # getter for: Lcom/kehdev/AxisSelect;->status2:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/kehdev/AxisSelect;->access$600(Lcom/kehdev/AxisSelect;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/kehdev/AxisSelect$1;->this$0:Lcom/kehdev/AxisSelect;

    # getter for: Lcom/kehdev/AxisSelect;->status1:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/kehdev/AxisSelect;->access$700(Lcom/kehdev/AxisSelect;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/kehdev/AxisSelect$1;->this$0:Lcom/kehdev/AxisSelect;

    # getter for: Lcom/kehdev/AxisSelect;->rela1:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/kehdev/AxisSelect;->access$800(Lcom/kehdev/AxisSelect;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/kehdev/AxisSelect$1;->this$0:Lcom/kehdev/AxisSelect;

    # getter for: Lcom/kehdev/AxisSelect;->rela2:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/kehdev/AxisSelect;->access$900(Lcom/kehdev/AxisSelect;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/kehdev/AxisSelect$1;->this$0:Lcom/kehdev/AxisSelect;

    # setter for: Lcom/kehdev/AxisSelect;->axisSlts:I
    invoke-static {v0, v3}, Lcom/kehdev/AxisSelect;->access$502(Lcom/kehdev/AxisSelect;I)I

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/kehdev/AxisSelect$1;->this$0:Lcom/kehdev/AxisSelect;

    # getter for: Lcom/kehdev/AxisSelect;->status1:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/kehdev/AxisSelect;->access$700(Lcom/kehdev/AxisSelect;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/kehdev/AxisSelect$1;->this$0:Lcom/kehdev/AxisSelect;

    # getter for: Lcom/kehdev/AxisSelect;->status2:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/kehdev/AxisSelect;->access$600(Lcom/kehdev/AxisSelect;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/kehdev/AxisSelect$1;->this$0:Lcom/kehdev/AxisSelect;

    # getter for: Lcom/kehdev/AxisSelect;->rela2:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/kehdev/AxisSelect;->access$900(Lcom/kehdev/AxisSelect;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/kehdev/AxisSelect$1;->this$0:Lcom/kehdev/AxisSelect;

    # getter for: Lcom/kehdev/AxisSelect;->rela1:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/kehdev/AxisSelect;->access$800(Lcom/kehdev/AxisSelect;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto/16 :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;

    iget-object v0, p0, Lcom/kehdev/AxisSelect$1;->this$0:Lcom/kehdev/AxisSelect;

    const/4 v1, 0x0

    # setter for: Lcom/kehdev/AxisSelect;->mRunningAppName:Lcom/kehdev/GetRunningAppName;
    invoke-static {v0, v1}, Lcom/kehdev/AxisSelect;->access$002(Lcom/kehdev/AxisSelect;Lcom/kehdev/GetRunningAppName;)Lcom/kehdev/GetRunningAppName;

    return-void
.end method
