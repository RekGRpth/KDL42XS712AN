.class public Lcom/kehdev/SetTouchCoorView;
.super Landroid/view/SurfaceView;
.source "SetTouchCoorView.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# static fields
.field public static mtviewRemX:I

.field public static mtviewRemY:I


# instance fields
.field private colors:[I

.field private height:I

.field private scale:F

.field private textPaint:Landroid/graphics/Paint;

.field private touchPaints:[Landroid/graphics/Paint;

.field private width:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/kehdev/SetTouchCoorView;->mtviewRemX:I

    sput v0, Lcom/kehdev/SetTouchCoorView;->mtviewRemY:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    const/16 v3, 0xa

    const/4 v2, 0x1

    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/kehdev/SetTouchCoorView;->textPaint:Landroid/graphics/Paint;

    new-array v1, v3, [Landroid/graphics/Paint;

    iput-object v1, p0, Lcom/kehdev/SetTouchCoorView;->touchPaints:[Landroid/graphics/Paint;

    new-array v1, v3, [I

    iput-object v1, p0, Lcom/kehdev/SetTouchCoorView;->colors:[I

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/kehdev/SetTouchCoorView;->scale:F

    invoke-virtual {p0}, Lcom/kehdev/SetTouchCoorView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    const/4 v1, -0x2

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setFormat(I)V

    invoke-virtual {p0, v2}, Lcom/kehdev/SetTouchCoorView;->setFocusable(Z)V

    invoke-virtual {p0, v2}, Lcom/kehdev/SetTouchCoorView;->setFocusableInTouchMode(Z)V

    invoke-direct {p0}, Lcom/kehdev/SetTouchCoorView;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/16 v3, 0xa

    const/4 v2, 0x1

    invoke-direct {p0, p1, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/kehdev/SetTouchCoorView;->textPaint:Landroid/graphics/Paint;

    new-array v1, v3, [Landroid/graphics/Paint;

    iput-object v1, p0, Lcom/kehdev/SetTouchCoorView;->touchPaints:[Landroid/graphics/Paint;

    new-array v1, v3, [I

    iput-object v1, p0, Lcom/kehdev/SetTouchCoorView;->colors:[I

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/kehdev/SetTouchCoorView;->scale:F

    invoke-virtual {p0}, Lcom/kehdev/SetTouchCoorView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    const/4 v1, -0x2

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setFormat(I)V

    invoke-virtual {p0, v2}, Lcom/kehdev/SetTouchCoorView;->setFocusable(Z)V

    invoke-virtual {p0, v2}, Lcom/kehdev/SetTouchCoorView;->setFocusableInTouchMode(Z)V

    invoke-direct {p0}, Lcom/kehdev/SetTouchCoorView;->init()V

    return-void
.end method

.method private drawCircle(IILandroid/graphics/Paint;Landroid/graphics/Canvas;)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/graphics/Paint;
    .param p4    # Landroid/graphics/Canvas;

    return-void
.end method

.method private drawCrosshairsAndText(IILandroid/graphics/Paint;IILandroid/graphics/Canvas;)V
    .locals 9
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/graphics/Paint;
    .param p4    # I
    .param p5    # I
    .param p6    # Landroid/graphics/Canvas;

    const/4 v1, 0x0

    int-to-float v2, p2

    iget v0, p0, Lcom/kehdev/SetTouchCoorView;->width:I

    int-to-float v3, v0

    int-to-float v4, p2

    move-object v0, p6

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    int-to-float v3, p1

    int-to-float v5, p1

    iget v0, p0, Lcom/kehdev/SetTouchCoorView;->height:I

    int-to-float v6, v0

    move-object v2, p6

    move v4, v1

    move-object v7, p3

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    mul-int/lit8 v0, p4, 0x14

    add-int/lit8 v0, v0, 0xf

    int-to-float v0, v0

    iget v1, p0, Lcom/kehdev/SetTouchCoorView;->scale:F

    mul-float/2addr v0, v1

    float-to-int v8, v0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/high16 v1, 0x41200000    # 10.0f

    iget v2, p0, Lcom/kehdev/SetTouchCoorView;->scale:F

    mul-float/2addr v1, v2

    int-to-float v2, v8

    iget-object v3, p0, Lcom/kehdev/SetTouchCoorView;->textPaint:Landroid/graphics/Paint;

    invoke-virtual {p6, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "y"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/high16 v1, 0x428c0000    # 70.0f

    iget v2, p0, Lcom/kehdev/SetTouchCoorView;->scale:F

    mul-float/2addr v1, v2

    int-to-float v2, v8

    iget-object v3, p0, Lcom/kehdev/SetTouchCoorView;->textPaint:Landroid/graphics/Paint;

    invoke-virtual {p6, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/kehdev/SetTouchCoorView;->width:I

    int-to-float v1, v1

    const/high16 v2, 0x425c0000    # 55.0f

    iget v3, p0, Lcom/kehdev/SetTouchCoorView;->scale:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    int-to-float v2, v8

    iget-object v3, p0, Lcom/kehdev/SetTouchCoorView;->textPaint:Landroid/graphics/Paint;

    invoke-virtual {p6, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method private init()V
    .locals 5

    const/4 v4, -0x1

    iget-object v1, p0, Lcom/kehdev/SetTouchCoorView;->textPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/kehdev/SetTouchCoorView;->colors:[I

    const/4 v2, 0x0

    const v3, -0xffff01

    aput v3, v1, v2

    iget-object v1, p0, Lcom/kehdev/SetTouchCoorView;->colors:[I

    const/4 v2, 0x1

    const/high16 v3, -0x10000

    aput v3, v1, v2

    iget-object v1, p0, Lcom/kehdev/SetTouchCoorView;->colors:[I

    const/4 v2, 0x2

    const v3, -0xff0100

    aput v3, v1, v2

    iget-object v1, p0, Lcom/kehdev/SetTouchCoorView;->colors:[I

    const/4 v2, 0x3

    const/16 v3, -0x100

    aput v3, v1, v2

    iget-object v1, p0, Lcom/kehdev/SetTouchCoorView;->colors:[I

    const/4 v2, 0x4

    const v3, -0xff0001

    aput v3, v1, v2

    iget-object v1, p0, Lcom/kehdev/SetTouchCoorView;->colors:[I

    const/4 v2, 0x5

    const v3, -0xff01

    aput v3, v1, v2

    iget-object v1, p0, Lcom/kehdev/SetTouchCoorView;->colors:[I

    const/4 v2, 0x6

    const v3, -0xbbbbbc

    aput v3, v1, v2

    iget-object v1, p0, Lcom/kehdev/SetTouchCoorView;->colors:[I

    const/4 v2, 0x7

    aput v4, v1, v2

    iget-object v1, p0, Lcom/kehdev/SetTouchCoorView;->colors:[I

    const/16 v2, 0x8

    const v3, -0x333334

    aput v3, v1, v2

    iget-object v1, p0, Lcom/kehdev/SetTouchCoorView;->colors:[I

    const/16 v2, 0x9

    const v3, -0x777778

    aput v3, v1, v2

    const/4 v0, 0x0

    :goto_0
    const/16 v1, 0xa

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/kehdev/SetTouchCoorView;->touchPaints:[Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    aput-object v2, v1, v0

    iget-object v1, p0, Lcom/kehdev/SetTouchCoorView;->touchPaints:[Landroid/graphics/Paint;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/kehdev/SetTouchCoorView;->colors:[I

    aget v2, v2, v0

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public drawer(Landroid/view/MotionEvent;Landroid/graphics/Canvas;)V
    .locals 8
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Landroid/graphics/Canvas;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v7

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v7, :cond_0

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v5

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    float-to-int v1, v0

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    float-to-int v2, v0

    iget-object v0, p0, Lcom/kehdev/SetTouchCoorView;->touchPaints:[Landroid/graphics/Paint;

    aget-object v3, v0, v5

    move-object v0, p0

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/kehdev/SetTouchCoorView;->drawCrosshairsAndText(IILandroid/graphics/Paint;IILandroid/graphics/Canvas;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_1
    if-ge v4, v7, :cond_1

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v5

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    float-to-int v1, v0

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    float-to-int v2, v0

    iget-object v0, p0, Lcom/kehdev/SetTouchCoorView;->touchPaints:[Landroid/graphics/Paint;

    aget-object v0, v0, v5

    invoke-direct {p0, v1, v2, v0, p2}, Lcom/kehdev/SetTouchCoorView;->drawCircle(IILandroid/graphics/Paint;Landroid/graphics/Canvas;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1    # Landroid/view/MotionEvent;

    const/4 v9, 0x1

    const/4 v3, 0x0

    sput v3, Lcom/kehdev/SetTouchCoorView;->mtviewRemX:I

    sput v3, Lcom/kehdev/SetTouchCoorView;->mtviewRemY:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v7

    const/16 v0, 0xa

    if-le v7, v0, :cond_0

    const/16 v7, 0xa

    :cond_0
    invoke-virtual {p0}, Lcom/kehdev/SetTouchCoorView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/SurfaceHolder;->lockCanvas()Landroid/graphics/Canvas;

    move-result-object v6

    if-eqz v6, :cond_3

    sget-object v0, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v6, v3, v0}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v3, 0x6

    if-ne v0, v3, :cond_4

    const-string v0, "UP"

    const-string v3, "event.getAction() ACTION_POINTER_1_UP"

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v7, :cond_1

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v5

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    float-to-int v1, v0

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    float-to-int v2, v0

    iget-object v0, p0, Lcom/kehdev/SetTouchCoorView;->touchPaints:[Landroid/graphics/Paint;

    aget-object v3, v0, v5

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/kehdev/SetTouchCoorView;->drawCrosshairsAndText(IILandroid/graphics/Paint;IILandroid/graphics/Canvas;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v5

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    float-to-int v1, v0

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    float-to-int v2, v0

    iget-object v0, p0, Lcom/kehdev/SetTouchCoorView;->touchPaints:[Landroid/graphics/Paint;

    aget-object v0, v0, v5

    invoke-direct {p0, v1, v2, v0, v6}, Lcom/kehdev/SetTouchCoorView;->drawCircle(IILandroid/graphics/Paint;Landroid/graphics/Canvas;)V

    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/kehdev/SetTouchCoorView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0, v6}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    :cond_3
    return v9

    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/16 v3, 0x106

    if-ne v0, v3, :cond_6

    const-string v0, "UP"

    const-string v3, "event.getAction() ACTION_POINTER_2_UP"

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x0

    :goto_2
    if-ge v4, v7, :cond_5

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v5

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    float-to-int v1, v0

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    float-to-int v2, v0

    iget-object v0, p0, Lcom/kehdev/SetTouchCoorView;->touchPaints:[Landroid/graphics/Paint;

    aget-object v3, v0, v5

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/kehdev/SetTouchCoorView;->drawCrosshairsAndText(IILandroid/graphics/Paint;IILandroid/graphics/Canvas;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_5
    const/4 v4, 0x0

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v5

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    float-to-int v1, v0

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    float-to-int v2, v0

    iget-object v0, p0, Lcom/kehdev/SetTouchCoorView;->touchPaints:[Landroid/graphics/Paint;

    aget-object v0, v0, v5

    invoke-direct {p0, v1, v2, v0, v6}, Lcom/kehdev/SetTouchCoorView;->drawCircle(IILandroid/graphics/Paint;Landroid/graphics/Canvas;)V

    goto :goto_1

    :cond_6
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v9, :cond_7

    const-string v0, "UP"

    const-string v3, "key ACTION_UP..."

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, p1, v6}, Lcom/kehdev/SetTouchCoorView;->drawer(Landroid/view/MotionEvent;Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/kehdev/SetTouchCoorView;->mtviewRemX:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcom/kehdev/SetTouchCoorView;->mtviewRemY:I

    const-string v0, "tag"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mtview rem x:"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v8, Lcom/kehdev/SetTouchCoorView;->mtviewRemX:I

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "tag"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mtview rem y:"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v8, Lcom/kehdev/SetTouchCoorView;->mtviewRemY:I

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_7
    const-string v0, "DOWN"

    const-string v3, "key down..."

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x0

    :goto_3
    if-ge v4, v7, :cond_8

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v5

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    float-to-int v1, v0

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    float-to-int v2, v0

    iget-object v0, p0, Lcom/kehdev/SetTouchCoorView;->touchPaints:[Landroid/graphics/Paint;

    aget-object v3, v0, v5

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/kehdev/SetTouchCoorView;->drawCrosshairsAndText(IILandroid/graphics/Paint;IILandroid/graphics/Canvas;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_8
    const/4 v4, 0x0

    :goto_4
    if-ge v4, v7, :cond_2

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v5

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    float-to-int v1, v0

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    float-to-int v2, v0

    iget-object v0, p0, Lcom/kehdev/SetTouchCoorView;->touchPaints:[Landroid/graphics/Paint;

    aget-object v0, v0, v5

    invoke-direct {p0, v1, v2, v0, v6}, Lcom/kehdev/SetTouchCoorView;->drawCircle(IILandroid/graphics/Paint;Landroid/graphics/Canvas;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_4
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 7
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const v6, 0x7f07003c    # com.kehdev.R.string.savaTouchCoorTip

    const/high16 v3, 0x43f00000    # 480.0f

    const/4 v5, 0x0

    iput p3, p0, Lcom/kehdev/SetTouchCoorView;->width:I

    iput p4, p0, Lcom/kehdev/SetTouchCoorView;->height:I

    if-le p3, p4, :cond_1

    int-to-float v2, p3

    div-float/2addr v2, v3

    iput v2, p0, Lcom/kehdev/SetTouchCoorView;->scale:F

    :goto_0
    iget-object v2, p0, Lcom/kehdev/SetTouchCoorView;->textPaint:Landroid/graphics/Paint;

    const/high16 v3, 0x41600000    # 14.0f

    iget v4, p0, Lcom/kehdev/SetTouchCoorView;->scale:F

    mul-float/2addr v3, v4

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    invoke-virtual {p0}, Lcom/kehdev/SetTouchCoorView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/SurfaceHolder;->lockCanvas()Landroid/graphics/Canvas;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v5, v5, v5, v5}, Landroid/graphics/Canvas;->drawARGB(IIII)V

    iget-object v2, p0, Lcom/kehdev/SetTouchCoorView;->textPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/kehdev/SetTouchCoorView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v1

    iget-object v2, p0, Lcom/kehdev/SetTouchCoorView;->textPaint:Landroid/graphics/Paint;

    const v3, -0x777778

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p0}, Lcom/kehdev/SetTouchCoorView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    div-int/lit8 v3, p3, 0x2

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v1, v4

    sub-float/2addr v3, v4

    div-int/lit8 v4, p4, 0x2

    int-to-float v4, v4

    iget-object v5, p0, Lcom/kehdev/SetTouchCoorView;->textPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    invoke-virtual {p0}, Lcom/kehdev/SetTouchCoorView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v2

    invoke-interface {v2, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    :cond_0
    return-void

    :cond_1
    int-to-float v2, p4

    div-float/2addr v2, v3

    iput v2, p0, Lcom/kehdev/SetTouchCoorView;->scale:F

    goto :goto_0
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1    # Landroid/view/SurfaceHolder;

    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1    # Landroid/view/SurfaceHolder;

    return-void
.end method
