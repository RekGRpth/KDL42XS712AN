.class public Lcom/kehdev/ListSupportGame;
.super Landroid/app/Activity;
.source "ListSupportGame.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field public bindSuccRet:Z

.field private mRunningAppName:Lcom/kehdev/GetRunningAppName;

.field private mRunningAppNameConn:Landroid/content/ServiceConnection;

.field private mSqliteOperation:Lcom/kehdev/SqliteOperation;

.field private supportGameDownloadAddr:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private supportGameName:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private supportGameSpecific:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public supprotGameListView:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-object v1, p0, Lcom/kehdev/ListSupportGame;->supprotGameListView:Landroid/widget/ListView;

    const-string v0, "ListSupportGame"

    iput-object v0, p0, Lcom/kehdev/ListSupportGame;->TAG:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/kehdev/ListSupportGame;->bindSuccRet:Z

    iput-object v1, p0, Lcom/kehdev/ListSupportGame;->mRunningAppName:Lcom/kehdev/GetRunningAppName;

    iput-object v1, p0, Lcom/kehdev/ListSupportGame;->mSqliteOperation:Lcom/kehdev/SqliteOperation;

    iput-object v1, p0, Lcom/kehdev/ListSupportGame;->supportGameName:Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/kehdev/ListSupportGame;->supportGameSpecific:Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/kehdev/ListSupportGame;->supportGameDownloadAddr:Ljava/util/ArrayList;

    new-instance v0, Lcom/kehdev/ListSupportGame$1;

    invoke-direct {v0, p0}, Lcom/kehdev/ListSupportGame$1;-><init>(Lcom/kehdev/ListSupportGame;)V

    iput-object v0, p0, Lcom/kehdev/ListSupportGame;->mRunningAppNameConn:Landroid/content/ServiceConnection;

    return-void
.end method

.method static synthetic access$000(Lcom/kehdev/ListSupportGame;)Lcom/kehdev/GetRunningAppName;
    .locals 1
    .param p0    # Lcom/kehdev/ListSupportGame;

    iget-object v0, p0, Lcom/kehdev/ListSupportGame;->mRunningAppName:Lcom/kehdev/GetRunningAppName;

    return-object v0
.end method

.method static synthetic access$002(Lcom/kehdev/ListSupportGame;Lcom/kehdev/GetRunningAppName;)Lcom/kehdev/GetRunningAppName;
    .locals 0
    .param p0    # Lcom/kehdev/ListSupportGame;
    .param p1    # Lcom/kehdev/GetRunningAppName;

    iput-object p1, p0, Lcom/kehdev/ListSupportGame;->mRunningAppName:Lcom/kehdev/GetRunningAppName;

    return-object p1
.end method

.method static synthetic access$100(Lcom/kehdev/ListSupportGame;)Lcom/kehdev/SqliteOperation;
    .locals 1
    .param p0    # Lcom/kehdev/ListSupportGame;

    iget-object v0, p0, Lcom/kehdev/ListSupportGame;->mSqliteOperation:Lcom/kehdev/SqliteOperation;

    return-object v0
.end method

.method static synthetic access$102(Lcom/kehdev/ListSupportGame;Lcom/kehdev/SqliteOperation;)Lcom/kehdev/SqliteOperation;
    .locals 0
    .param p0    # Lcom/kehdev/ListSupportGame;
    .param p1    # Lcom/kehdev/SqliteOperation;

    iput-object p1, p0, Lcom/kehdev/ListSupportGame;->mSqliteOperation:Lcom/kehdev/SqliteOperation;

    return-object p1
.end method

.method static synthetic access$200(Lcom/kehdev/ListSupportGame;)Ljava/util/ArrayList;
    .locals 1
    .param p0    # Lcom/kehdev/ListSupportGame;

    iget-object v0, p0, Lcom/kehdev/ListSupportGame;->supportGameDownloadAddr:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lcom/kehdev/ListSupportGame;->requestWindowFeature(I)Z

    const v0, 0x7f03000a    # com.kehdev.R.layout.list_support_game_layout

    invoke-virtual {p0, v0}, Lcom/kehdev/ListSupportGame;->setContentView(I)V

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/kehdev/ListSupportGame;->supportGameName:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/kehdev/ListSupportGame;->supportGameSpecific:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/kehdev/ListSupportGame;->supportGameDownloadAddr:Ljava/util/ArrayList;

    const v0, 0x7f09001a    # com.kehdev.R.id.listSupportGameListView

    invoke-virtual {p0, v0}, Lcom/kehdev/ListSupportGame;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/kehdev/ListSupportGame;->supprotGameListView:Landroid/widget/ListView;

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/kehdev/ListSupportGame;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/kehdev/GetRunningAppName;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/kehdev/ListSupportGame;->mRunningAppNameConn:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0, v1, v3}, Lcom/kehdev/ListSupportGame;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/kehdev/ListSupportGame;->bindSuccRet:Z

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-boolean v0, p0, Lcom/kehdev/ListSupportGame;->bindSuccRet:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/kehdev/ListSupportGame;->mRunningAppNameConn:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/kehdev/ListSupportGame;->unbindService(Landroid/content/ServiceConnection;)V

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public openWebSite(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.android.browser"

    const-string v2, "com.android.browser.BrowserActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/kehdev/ListSupportGame;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public setListViewContent()V
    .locals 2

    iget-object v0, p0, Lcom/kehdev/ListSupportGame;->supprotGameListView:Landroid/widget/ListView;

    new-instance v1, Lcom/kehdev/ListSupportGame$2;

    invoke-direct {v1, p0}, Lcom/kehdev/ListSupportGame$2;-><init>(Lcom/kehdev/ListSupportGame;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void
.end method
