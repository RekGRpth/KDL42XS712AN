.class public Lcom/kehdev/ReadKeyStatus;
.super Landroid/app/Service;
.source "ReadKeyStatus.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;,
        Lcom/kehdev/ReadKeyStatus$monitorKeyReleaseWorker;,
        Lcom/kehdev/ReadKeyStatus$getscreen_Thread;,
        Lcom/kehdev/ReadKeyStatus$LocalBinder;,
        Lcom/kehdev/ReadKeyStatus$OSI_KEY_VALUE;
    }
.end annotation


# static fields
.field public static screenHeight:I

.field public static screenWidth:I

.field public static select_talk:I


# instance fields
.field private final HID_FAVOR:I

.field private final HID_HOME:I

.field private final HID_MEDIA_MUTE:I

.field private final HID_MEDIA_NEXT:I

.field private final HID_MEDIA_PAUSE:I

.field private final HID_MEDIA_PRE:I

.field private final HID_MEDIA_STOP:I

.field private final HID_MENU:I

.field private final HID_MIC_LONGPRESS:I

.field public KeyCount:I

.field private final OSI_KEY_VALUE_BASE:I

.field private final OSI_KEY_VALUE_BASES:I

.field private final TAG:Ljava/lang/String;

.field public active_x:I

.field public active_y:I

.field public begin:J

.field public bing:I

.field public bx1:I

.field public bx2:I

.field public by1:I

.field public by2:I

.field public downHadSendFlg:Z

.field public end:J

.field public ex1:I

.field public ex2:I

.field public ey1:I

.field public ey2:I

.field public g_keyValue:I

.field public g_keyValue_save:I

.field public g_preRunAppName:Ljava/lang/String;

.field public g_preValue:I

.field public g_remState:I

.field public g_remValue:I

.field public g_state:I

.field public g_status:I

.field public g_status_save:I

.field public gameId:I

.field private getKeyMessInTime:Z

.field public hadOneKeyDown:Z

.field public hadOnekeyrelease:Z

.field public hadTwoKeyDown:Z

.field handler:Landroid/os/Handler;

.field private iKeyBoardMode:I

.field public final keyDown:I

.field public keyMap:[I

.field public keyTimeOut:I

.field public final keyUp:I

.field private final mBinder:Landroid/os/IBinder;

.field private mReadKeyStatus:Lcom/kehdev/ReadKeyStatus;

.field private mRunningAppNameConn_1:Landroid/content/ServiceConnection;

.field public mRunningAppName_1:Lcom/kehdev/GetRunningAppName;

.field public mSqliteOperation:Lcom/kehdev/SqliteOperation;

.field private mView:Lcom/kehdev/DeviceService;

.field public final maxKeyNum:I

.field public needSendFlg:Z

.field public newKeyFlg:Z

.field public oneKeyHadUp:Z

.field private pm:Landroid/content/pm/PackageManager;

.field public preTouchTypeData:I

.field public rem_fstKeyValue:I

.field public rem_fstKeyX:I

.field public rem_fstKeyY:I

.field public rem_secKeyValue:I

.field public rem_secKeyX:I

.field public rem_secKeyY:I

.field public screenTouchId:I

.field public startTime:D

.field public timeOutFlg:Z

.field public touchType:I

.field public touchTypeData:I

.field public twoKeyHadUp:Z

.field private windowManager:Landroid/view/IWindowManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x438

    sput v0, Lcom/kehdev/ReadKeyStatus;->screenWidth:I

    const/16 v0, 0x2d0

    sput v0, Lcom/kehdev/ReadKeyStatus;->screenHeight:I

    const/4 v0, 0x1

    sput v0, Lcom/kehdev/ReadKeyStatus;->select_talk:I

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    const/16 v5, 0x20

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    iput-object v2, p0, Lcom/kehdev/ReadKeyStatus;->mRunningAppName_1:Lcom/kehdev/GetRunningAppName;

    iput-object v2, p0, Lcom/kehdev/ReadKeyStatus;->mSqliteOperation:Lcom/kehdev/SqliteOperation;

    iput v1, p0, Lcom/kehdev/ReadKeyStatus;->touchType:I

    iput v1, p0, Lcom/kehdev/ReadKeyStatus;->gameId:I

    iput v1, p0, Lcom/kehdev/ReadKeyStatus;->screenTouchId:I

    const-string v0, "readKeyStatus"

    iput-object v0, p0, Lcom/kehdev/ReadKeyStatus;->TAG:Ljava/lang/String;

    new-instance v0, Lcom/kehdev/ReadKeyStatus$LocalBinder;

    invoke-direct {v0, p0}, Lcom/kehdev/ReadKeyStatus$LocalBinder;-><init>(Lcom/kehdev/ReadKeyStatus;)V

    iput-object v0, p0, Lcom/kehdev/ReadKeyStatus;->mBinder:Landroid/os/IBinder;

    iput-object v2, p0, Lcom/kehdev/ReadKeyStatus;->mView:Lcom/kehdev/DeviceService;

    iput v1, p0, Lcom/kehdev/ReadKeyStatus;->touchTypeData:I

    iput v3, p0, Lcom/kehdev/ReadKeyStatus;->g_preValue:I

    iput v1, p0, Lcom/kehdev/ReadKeyStatus;->preTouchTypeData:I

    const-string v0, ""

    iput-object v0, p0, Lcom/kehdev/ReadKeyStatus;->g_preRunAppName:Ljava/lang/String;

    const/16 v0, 0x80

    iput v0, p0, Lcom/kehdev/ReadKeyStatus;->OSI_KEY_VALUE_BASE:I

    const/16 v0, 0x80

    iput v0, p0, Lcom/kehdev/ReadKeyStatus;->OSI_KEY_VALUE_BASES:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/kehdev/ReadKeyStatus;->end:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/kehdev/ReadKeyStatus;->begin:J

    iput-object v2, p0, Lcom/kehdev/ReadKeyStatus;->windowManager:Landroid/view/IWindowManager;

    iput v4, p0, Lcom/kehdev/ReadKeyStatus;->keyDown:I

    iput v3, p0, Lcom/kehdev/ReadKeyStatus;->keyUp:I

    iput v3, p0, Lcom/kehdev/ReadKeyStatus;->KeyCount:I

    iput v5, p0, Lcom/kehdev/ReadKeyStatus;->maxKeyNum:I

    iput-boolean v4, p0, Lcom/kehdev/ReadKeyStatus;->getKeyMessInTime:Z

    iput-object v2, p0, Lcom/kehdev/ReadKeyStatus;->mReadKeyStatus:Lcom/kehdev/ReadKeyStatus;

    iput v3, p0, Lcom/kehdev/ReadKeyStatus;->iKeyBoardMode:I

    new-array v0, v5, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/kehdev/ReadKeyStatus;->keyMap:[I

    const/16 v0, 0x4a

    iput v0, p0, Lcom/kehdev/ReadKeyStatus;->HID_HOME:I

    const/16 v0, 0x3b

    iput v0, p0, Lcom/kehdev/ReadKeyStatus;->HID_MENU:I

    const/16 v0, 0x73

    iput v0, p0, Lcom/kehdev/ReadKeyStatus;->HID_FAVOR:I

    const/16 v0, 0x6d

    iput v0, p0, Lcom/kehdev/ReadKeyStatus;->HID_MEDIA_NEXT:I

    const/16 v0, 0x6c

    iput v0, p0, Lcom/kehdev/ReadKeyStatus;->HID_MEDIA_PRE:I

    const/16 v0, 0xe8

    iput v0, p0, Lcom/kehdev/ReadKeyStatus;->HID_MEDIA_PAUSE:I

    const/16 v0, 0x69

    iput v0, p0, Lcom/kehdev/ReadKeyStatus;->HID_MEDIA_STOP:I

    const/16 v0, 0x7f

    iput v0, p0, Lcom/kehdev/ReadKeyStatus;->HID_MEDIA_MUTE:I

    const/16 v0, 0x76

    iput v0, p0, Lcom/kehdev/ReadKeyStatus;->HID_MIC_LONGPRESS:I

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/kehdev/ReadKeyStatus;->handler:Landroid/os/Handler;

    new-instance v0, Lcom/kehdev/ReadKeyStatus$2;

    invoke-direct {v0, p0}, Lcom/kehdev/ReadKeyStatus$2;-><init>(Lcom/kehdev/ReadKeyStatus;)V

    iput-object v0, p0, Lcom/kehdev/ReadKeyStatus;->mRunningAppNameConn_1:Landroid/content/ServiceConnection;

    iput-object v2, p0, Lcom/kehdev/ReadKeyStatus;->pm:Landroid/content/pm/PackageManager;

    return-void

    :array_0
    .array-data 4
        0x13
        0x14
        0x16
        0x15
        0x17
        0x57
        0x58
        0x56
        0x5b
        0x55
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method static synthetic access$000(Lcom/kehdev/ReadKeyStatus;)Z
    .locals 1
    .param p0    # Lcom/kehdev/ReadKeyStatus;

    iget-boolean v0, p0, Lcom/kehdev/ReadKeyStatus;->getKeyMessInTime:Z

    return v0
.end method

.method static synthetic access$002(Lcom/kehdev/ReadKeyStatus;Z)Z
    .locals 0
    .param p0    # Lcom/kehdev/ReadKeyStatus;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/kehdev/ReadKeyStatus;->getKeyMessInTime:Z

    return p1
.end method


# virtual methods
.method public ToastShow()V
    .locals 2

    iget-object v0, p0, Lcom/kehdev/ReadKeyStatus;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/kehdev/ReadKeyStatus$1;

    invoke-direct {v1, p0}, Lcom/kehdev/ReadKeyStatus$1;-><init>(Lcom/kehdev/ReadKeyStatus;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public chang2TouchEvent()Z
    .locals 2

    const/4 v1, -0x1

    iget v0, p0, Lcom/kehdev/ReadKeyStatus;->bx1:I

    iput v0, p0, Lcom/kehdev/ReadKeyStatus;->active_x:I

    iget v0, p0, Lcom/kehdev/ReadKeyStatus;->by1:I

    iput v0, p0, Lcom/kehdev/ReadKeyStatus;->active_y:I

    iget v0, p0, Lcom/kehdev/ReadKeyStatus;->active_y:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/kehdev/ReadKeyStatus;->active_x:I

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public checkActiveKey()Z
    .locals 3

    const/4 v0, 0x1

    iget v1, p0, Lcom/kehdev/ReadKeyStatus;->g_keyValue:I

    const/16 v2, 0x7f

    if-le v1, v2, :cond_1

    iget v1, p0, Lcom/kehdev/ReadKeyStatus;->g_keyValue:I

    const/16 v2, 0x100

    if-ge v1, v2, :cond_1

    iget v1, p0, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lcom/kehdev/ReadKeyStatus;->g_status:I

    if-nez v1, :cond_1

    :cond_0
    iget v1, p0, Lcom/kehdev/ReadKeyStatus;->g_status:I

    iput v1, p0, Lcom/kehdev/ReadKeyStatus;->g_remState:I

    iget v1, p0, Lcom/kehdev/ReadKeyStatus;->g_keyValue:I

    iput v1, p0, Lcom/kehdev/ReadKeyStatus;->g_remValue:I

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public findEventType()I
    .locals 5

    const/4 v1, -0x1

    iget v2, p0, Lcom/kehdev/ReadKeyStatus;->preTouchTypeData:I

    if-nez v2, :cond_1

    iget v2, p0, Lcom/kehdev/ReadKeyStatus;->g_preValue:I

    iget v3, p0, Lcom/kehdev/ReadKeyStatus;->g_remValue:I

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/kehdev/ReadKeyStatus;->g_preRunAppName:Ljava/lang/String;

    sget-object v3, Lcom/kehdev/GetRunningAppName;->runningAppPkgName:Ljava/lang/String;

    if-ne v2, v3, :cond_1

    iget v1, p0, Lcom/kehdev/ReadKeyStatus;->preTouchTypeData:I

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v2, p0, Lcom/kehdev/ReadKeyStatus;->mSqliteOperation:Lcom/kehdev/SqliteOperation;

    sget-object v3, Lcom/kehdev/GetRunningAppName;->runningAppPkgName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/kehdev/SqliteOperation;->checkIfTheGameInDatabase(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/kehdev/ReadKeyStatus;->gameId:I

    iget v2, p0, Lcom/kehdev/ReadKeyStatus;->gameId:I

    if-eq v2, v1, :cond_0

    invoke-virtual {p0}, Lcom/kehdev/ReadKeyStatus;->findScreenTouchId()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/kehdev/ReadKeyStatus;->mSqliteOperation:Lcom/kehdev/SqliteOperation;

    iget v3, p0, Lcom/kehdev/ReadKeyStatus;->gameId:I

    iget v4, p0, Lcom/kehdev/ReadKeyStatus;->screenTouchId:I

    invoke-virtual {v2, v3, v4}, Lcom/kehdev/SqliteOperation;->checkScreenTouchMess(II)[I

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    aget v1, v0, v1

    iput v1, p0, Lcom/kehdev/ReadKeyStatus;->touchTypeData:I

    const/4 v1, 0x1

    aget v1, v0, v1

    iput v1, p0, Lcom/kehdev/ReadKeyStatus;->bx1:I

    const/4 v1, 0x2

    aget v1, v0, v1

    iput v1, p0, Lcom/kehdev/ReadKeyStatus;->by1:I

    const/4 v1, 0x3

    aget v1, v0, v1

    iput v1, p0, Lcom/kehdev/ReadKeyStatus;->bx2:I

    const/4 v1, 0x4

    aget v1, v0, v1

    iput v1, p0, Lcom/kehdev/ReadKeyStatus;->by2:I

    const/4 v1, 0x5

    aget v1, v0, v1

    iput v1, p0, Lcom/kehdev/ReadKeyStatus;->ex1:I

    const/4 v1, 0x6

    aget v1, v0, v1

    iput v1, p0, Lcom/kehdev/ReadKeyStatus;->ey1:I

    const/4 v1, 0x7

    aget v1, v0, v1

    iput v1, p0, Lcom/kehdev/ReadKeyStatus;->ex2:I

    const/16 v1, 0x8

    aget v1, v0, v1

    iput v1, p0, Lcom/kehdev/ReadKeyStatus;->ey2:I

    iget v1, p0, Lcom/kehdev/ReadKeyStatus;->touchTypeData:I

    goto :goto_0
.end method

.method public findScreenTouchId()Z
    .locals 2

    iget v0, p0, Lcom/kehdev/ReadKeyStatus;->gameId:I

    add-int/lit8 v0, v0, -0x1

    mul-int/lit8 v0, v0, 0x19

    iget v1, p0, Lcom/kehdev/ReadKeyStatus;->g_remValue:I

    add-int/lit8 v1, v1, -0x80

    add-int/2addr v0, v1

    iput v0, p0, Lcom/kehdev/ReadKeyStatus;->screenTouchId:I

    const/4 v0, 0x1

    return v0
.end method

.method public getRunningAppResolveInfo(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/pm/ResolveInfo;
    .locals 7
    .param p1    # Landroid/content/pm/PackageManager;
    .param p2    # Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v1, 0x0

    new-instance v2, Landroid/content/Intent;

    const-string v6, "android.intent.action.MAIN"

    invoke-direct {v2, v6, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v6, "android.intent.category.LAUNCHER"

    invoke-virtual {v2, v6}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const/4 v6, 0x0

    invoke-virtual {p1, v2, v6}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    iget-object v6, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v6, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    :goto_0
    return-object v3

    :cond_1
    move-object v3, v5

    goto :goto_0
.end method

.method public initSqlite()V
    .locals 4

    const/4 v3, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SELECT * from games where gameStr = \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/kehdev/GetRunningAppName;->runningAppPkgName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput v3, p0, Lcom/kehdev/ReadKeyStatus;->screenTouchId:I

    iput v3, p0, Lcom/kehdev/ReadKeyStatus;->gameId:I

    iput v3, p0, Lcom/kehdev/ReadKeyStatus;->ey2:I

    iput v3, p0, Lcom/kehdev/ReadKeyStatus;->ex2:I

    iput v3, p0, Lcom/kehdev/ReadKeyStatus;->ey1:I

    iput v3, p0, Lcom/kehdev/ReadKeyStatus;->ex1:I

    iput v3, p0, Lcom/kehdev/ReadKeyStatus;->by2:I

    iput v3, p0, Lcom/kehdev/ReadKeyStatus;->bx2:I

    iput v3, p0, Lcom/kehdev/ReadKeyStatus;->by1:I

    iput v3, p0, Lcom/kehdev/ReadKeyStatus;->bx1:I

    return-void
.end method

.method public initTouchKey()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/kehdev/ReadKeyStatus;->hadOneKeyDown:Z

    iput-boolean v0, p0, Lcom/kehdev/ReadKeyStatus;->hadTwoKeyDown:Z

    iput-boolean v0, p0, Lcom/kehdev/ReadKeyStatus;->oneKeyHadUp:Z

    iput-boolean v0, p0, Lcom/kehdev/ReadKeyStatus;->twoKeyHadUp:Z

    iput-boolean v0, p0, Lcom/kehdev/ReadKeyStatus;->hadOnekeyrelease:Z

    iput v0, p0, Lcom/kehdev/ReadKeyStatus;->rem_fstKeyValue:I

    iput v0, p0, Lcom/kehdev/ReadKeyStatus;->rem_secKeyValue:I

    iput v0, p0, Lcom/kehdev/ReadKeyStatus;->rem_fstKeyX:I

    iput v0, p0, Lcom/kehdev/ReadKeyStatus;->rem_secKeyX:I

    iput v0, p0, Lcom/kehdev/ReadKeyStatus;->rem_fstKeyY:I

    iput v0, p0, Lcom/kehdev/ReadKeyStatus;->rem_secKeyY:I

    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/kehdev/ReadKeyStatus;->mBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    new-instance v0, Landroid/app/Notification;

    const-string v1, "F-dolphin Service"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct {v0, v4, v1, v2, v3}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/kehdev/DeviceService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p0, v4, v1, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    const-string v2, "F-dolphin Service"

    const-string v3, ""

    invoke-virtual {v0, p0, v2, v3, v1}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    const/16 v1, 0x10

    iput v1, v0, Landroid/app/Notification;->flags:I

    const/16 v1, 0x4d3

    invoke-virtual {p0, v1, v0}, Lcom/kehdev/ReadKeyStatus;->startForeground(ILandroid/app/Notification;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/kehdev/GetRunningAppName;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/kehdev/ReadKeyStatus;->mRunningAppNameConn_1:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0, v1, v6}, Lcom/kehdev/ReadKeyStatus;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    const-string v0, "window"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v0

    iput-object v0, p0, Lcom/kehdev/ReadKeyStatus;->windowManager:Landroid/view/IWindowManager;

    const/16 v0, 0x190

    iput v0, p0, Lcom/kehdev/ReadKeyStatus;->keyTimeOut:I

    iput v4, p0, Lcom/kehdev/ReadKeyStatus;->g_keyValue:I

    iput v4, p0, Lcom/kehdev/ReadKeyStatus;->g_status:I

    iput v4, p0, Lcom/kehdev/ReadKeyStatus;->g_keyValue_save:I

    iput v4, p0, Lcom/kehdev/ReadKeyStatus;->g_status_save:I

    iput v4, p0, Lcom/kehdev/ReadKeyStatus;->g_remValue:I

    iput v4, p0, Lcom/kehdev/ReadKeyStatus;->g_remState:I

    iput-boolean v6, p0, Lcom/kehdev/ReadKeyStatus;->newKeyFlg:Z

    iput-boolean v4, p0, Lcom/kehdev/ReadKeyStatus;->needSendFlg:Z

    iput-boolean v4, p0, Lcom/kehdev/ReadKeyStatus;->downHadSendFlg:Z

    iput-boolean v4, p0, Lcom/kehdev/ReadKeyStatus;->timeOutFlg:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/kehdev/ReadKeyStatus;->startTime:D

    invoke-virtual {p0}, Lcom/kehdev/ReadKeyStatus;->initTouchKey()V

    invoke-virtual {p0}, Lcom/kehdev/ReadKeyStatus;->initSqlite()V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;

    invoke-direct {v1, p0}, Lcom/kehdev/ReadKeyStatus$readKeyStatusWorker;-><init>(Lcom/kehdev/ReadKeyStatus;)V

    const-string v2, "readKeyStatus"

    invoke-direct {v0, v5, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/kehdev/ReadKeyStatus$monitorKeyReleaseWorker;

    invoke-direct {v1, p0}, Lcom/kehdev/ReadKeyStatus$monitorKeyReleaseWorker;-><init>(Lcom/kehdev/ReadKeyStatus;)V

    const-string v2, "monitorKeyRelease"

    invoke-direct {v0, v5, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/kehdev/ReadKeyStatus$getscreen_Thread;

    invoke-direct {v1, p0}, Lcom/kehdev/ReadKeyStatus$getscreen_Thread;-><init>(Lcom/kehdev/ReadKeyStatus;)V

    const-string v2, "getscreen_Thread"

    invoke-direct {v0, v5, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/kehdev/ReadKeyStatus;->mRunningAppNameConn_1:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/kehdev/ReadKeyStatus;->mRunningAppNameConn_1:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/kehdev/ReadKeyStatus;->unbindService(Landroid/content/ServiceConnection;)V

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/kehdev/ReadKeyStatus;->stopForeground(Z)V

    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 0
    .param p1    # Landroid/content/Intent;
    .param p2    # I

    invoke-super {p0, p1, p2}, Landroid/app/Service;->onStart(Landroid/content/Intent;I)V

    return-void
.end method

.method public openRunningGameManual(Ljava/lang/String;)V
    .locals 2

    const-string v0, "com.android.launcher3"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    const-string v0, "com.android.launcher"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string v0, "readKeyStatus"

    const-string v1, "186186 runningAppPkgName==null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/kehdev/ReadKeyStatus;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/kehdev/ReadKeyStatus;->getRunningAppResolveInfo(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "readKeyStatus"

    const-string v1, "186186 getRunningAppResolveInfo IS  NULL"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "AppPkgCompleteName"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/high16 v1, 0x100000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-class v1, Lcom/kehdev/AxisSelect;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/kehdev/ReadKeyStatus;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public openVoiceAndGesture(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    new-instance v0, Landroid/content/ComponentName;

    invoke-direct {v0, p1, p2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const/high16 v3, 0x20000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    :try_start_0
    invoke-virtual {p0, v2}, Lcom/kehdev/ReadKeyStatus;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v3, 0x1

    :goto_0
    return v3

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    invoke-virtual {p0}, Lcom/kehdev/ReadKeyStatus;->sendBroadCastToUi()V

    const/4 v3, 0x0

    goto :goto_0
.end method

.method public openVoiceControl()V
    .locals 2

    sget v0, Lcom/kehdev/ReadKeyStatus;->select_talk:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget v0, Lcom/kehdev/ReadKeyStatus;->select_talk:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    const-string v0, "com.iflytek.viafly"

    const-string v1, "com.iflytek.viafly.ui.model.activity.SpeechDialog"

    invoke-virtual {p0, v0, v1}, Lcom/kehdev/ReadKeyStatus;->openVoiceAndGesture(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0

    :cond_2
    const-string v0, "com.google.android.voicesearch"

    const-string v1, "com.google.android.voicesearch.RecognitionActivity"

    invoke-virtual {p0, v0, v1}, Lcom/kehdev/ReadKeyStatus;->openVoiceAndGesture(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.google.android.googlequicksearchbox"

    const-string v1, "com.google.android.googlequicksearchbox.VoiceSearchActivity"

    invoke-virtual {p0, v0, v1}, Lcom/kehdev/ReadKeyStatus;->openVoiceAndGesture(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "com.google.android.googlequicksearchbox"

    const-string v1, "com.google.android.googlequicksearchbox.SearchActivity"

    invoke-virtual {p0, v0, v1}, Lcom/kehdev/ReadKeyStatus;->openVoiceAndGesture(Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public releaseTouch()Z
    .locals 21

    new-instance v18, Landroid/app/Instrumentation;

    invoke-direct/range {v18 .. v18}, Landroid/app/Instrumentation;-><init>()V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_fstKeyX:I

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_fstKeyY:I

    if-eqz v2, :cond_0

    new-instance v19, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct/range {v19 .. v19}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_fstKeyX:I

    int-to-float v2, v2

    move-object/from16 v0, v19

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->x:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_fstKeyY:I

    int-to-float v2, v2

    move-object/from16 v0, v19

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->y:F

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    const/4 v6, 0x1

    const/4 v7, 0x1

    const/4 v8, 0x1

    new-array v8, v8, [I

    const/4 v9, 0x0

    const/4 v10, 0x0

    aput v10, v8, v9

    const/4 v9, 0x1

    new-array v9, v9, [Landroid/view/MotionEvent$PointerCoords;

    const/4 v10, 0x0

    aput-object v19, v9, v10

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-static/range {v2 .. v16}, Landroid/view/MotionEvent;->obtain(JJII[I[Landroid/view/MotionEvent$PointerCoords;IFFIIII)Landroid/view/MotionEvent;

    move-result-object v17

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V

    :cond_0
    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_secKeyX:I

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_secKeyY:I

    if-eqz v2, :cond_1

    new-instance v20, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct/range {v20 .. v20}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_secKeyX:I

    int-to-float v2, v2

    move-object/from16 v0, v20

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->x:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_secKeyY:I

    int-to-float v2, v2

    move-object/from16 v0, v20

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->y:F

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    const/4 v6, 0x1

    const/4 v7, 0x1

    const/4 v8, 0x1

    new-array v8, v8, [I

    const/4 v9, 0x0

    const/4 v10, 0x1

    aput v10, v8, v9

    const/4 v9, 0x1

    new-array v9, v9, [Landroid/view/MotionEvent$PointerCoords;

    const/4 v10, 0x0

    aput-object v20, v9, v10

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-static/range {v2 .. v16}, Landroid/view/MotionEvent;->obtain(JJII[I[Landroid/view/MotionEvent$PointerCoords;IFFIIII)Landroid/view/MotionEvent;

    move-result-object v17

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V

    :cond_1
    const/4 v2, 0x1

    return v2
.end method

.method public sendBroadCastToUi()V
    .locals 4

    const-string v0, "com.kehdev.version"

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "remoteVerStr"

    const-string v3, "abc"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "dongleVerStr"

    const-string v3, "abcd"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/kehdev/ReadKeyStatus;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public sendMoveDouble()Z
    .locals 29

    new-instance v18, Landroid/app/Instrumentation;

    invoke-direct/range {v18 .. v18}, Landroid/app/Instrumentation;-><init>()V

    new-instance v19, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct/range {v19 .. v19}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->bx1:I

    int-to-float v2, v2

    move-object/from16 v0, v19

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->x:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->by1:I

    int-to-float v2, v2

    move-object/from16 v0, v19

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->y:F

    new-instance v24, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct/range {v24 .. v24}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->bx2:I

    int-to-float v2, v2

    move-object/from16 v0, v24

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->x:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->by2:I

    int-to-float v2, v2

    move-object/from16 v0, v24

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->y:F

    new-instance v21, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct/range {v21 .. v21}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->bx1:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/kehdev/ReadKeyStatus;->ex1:I

    add-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    move-object/from16 v0, v21

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->x:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->by1:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/kehdev/ReadKeyStatus;->ey1:I

    add-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    move-object/from16 v0, v21

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->y:F

    new-instance v26, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct/range {v26 .. v26}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->bx2:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/kehdev/ReadKeyStatus;->ex2:I

    add-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    move-object/from16 v0, v26

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->x:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->by2:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/kehdev/ReadKeyStatus;->ey2:I

    add-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    move-object/from16 v0, v26

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->y:F

    new-instance v22, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct/range {v22 .. v22}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->bx1:I

    int-to-float v2, v2

    move-object/from16 v0, v21

    iget v3, v0, Landroid/view/MotionEvent$PointerCoords;->x:F

    add-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    move-object/from16 v0, v22

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->x:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->by1:I

    int-to-float v2, v2

    move-object/from16 v0, v21

    iget v3, v0, Landroid/view/MotionEvent$PointerCoords;->y:F

    add-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    move-object/from16 v0, v22

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->y:F

    new-instance v27, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct/range {v27 .. v27}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->bx2:I

    int-to-float v2, v2

    move-object/from16 v0, v26

    iget v3, v0, Landroid/view/MotionEvent$PointerCoords;->x:F

    add-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    move-object/from16 v0, v27

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->x:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->by2:I

    int-to-float v2, v2

    move-object/from16 v0, v26

    iget v3, v0, Landroid/view/MotionEvent$PointerCoords;->y:F

    add-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    move-object/from16 v0, v27

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->y:F

    new-instance v23, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct/range {v23 .. v23}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    move-object/from16 v0, v21

    iget v2, v0, Landroid/view/MotionEvent$PointerCoords;->x:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/kehdev/ReadKeyStatus;->ex1:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    move-object/from16 v0, v23

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->x:F

    move-object/from16 v0, v21

    iget v2, v0, Landroid/view/MotionEvent$PointerCoords;->y:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/kehdev/ReadKeyStatus;->ey1:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    move-object/from16 v0, v23

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->y:F

    new-instance v28, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct/range {v28 .. v28}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    move-object/from16 v0, v26

    iget v2, v0, Landroid/view/MotionEvent$PointerCoords;->x:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/kehdev/ReadKeyStatus;->ex2:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    move-object/from16 v0, v28

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->x:F

    move-object/from16 v0, v26

    iget v2, v0, Landroid/view/MotionEvent$PointerCoords;->y:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/kehdev/ReadKeyStatus;->ey2:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    move-object/from16 v0, v28

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->y:F

    new-instance v20, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct/range {v20 .. v20}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->ex1:I

    int-to-float v2, v2

    move-object/from16 v0, v20

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->x:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->ey1:I

    int-to-float v2, v2

    move-object/from16 v0, v20

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->y:F

    new-instance v25, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct/range {v25 .. v25}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->ex2:I

    int-to-float v2, v2

    move-object/from16 v0, v25

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->x:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->ey2:I

    int-to-float v2, v2

    move-object/from16 v0, v25

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->y:F

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x1

    new-array v8, v8, [I

    const/4 v9, 0x0

    const/4 v10, 0x0

    aput v10, v8, v9

    const/4 v9, 0x1

    new-array v9, v9, [Landroid/view/MotionEvent$PointerCoords;

    const/4 v10, 0x0

    aput-object v19, v9, v10

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-static/range {v2 .. v16}, Landroid/view/MotionEvent;->obtain(JJII[I[Landroid/view/MotionEvent$PointerCoords;IFFIIII)Landroid/view/MotionEvent;

    move-result-object v17

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    const/16 v6, 0x105

    const/4 v7, 0x2

    const/4 v8, 0x2

    new-array v8, v8, [I

    fill-array-data v8, :array_0

    const/4 v9, 0x2

    new-array v9, v9, [Landroid/view/MotionEvent$PointerCoords;

    const/4 v10, 0x0

    aput-object v19, v9, v10

    const/4 v10, 0x1

    aput-object v24, v9, v10

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-static/range {v2 .. v16}, Landroid/view/MotionEvent;->obtain(JJII[I[Landroid/view/MotionEvent$PointerCoords;IFFIIII)Landroid/view/MotionEvent;

    move-result-object v17

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    const/4 v6, 0x2

    const/4 v7, 0x2

    const/4 v8, 0x2

    new-array v8, v8, [I

    fill-array-data v8, :array_1

    const/4 v9, 0x2

    new-array v9, v9, [Landroid/view/MotionEvent$PointerCoords;

    const/4 v10, 0x0

    aput-object v21, v9, v10

    const/4 v10, 0x1

    aput-object v26, v9, v10

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-static/range {v2 .. v16}, Landroid/view/MotionEvent;->obtain(JJII[I[Landroid/view/MotionEvent$PointerCoords;IFFIIII)Landroid/view/MotionEvent;

    move-result-object v17

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    const/4 v6, 0x2

    const/4 v7, 0x2

    const/4 v8, 0x2

    new-array v8, v8, [I

    fill-array-data v8, :array_2

    const/4 v9, 0x2

    new-array v9, v9, [Landroid/view/MotionEvent$PointerCoords;

    const/4 v10, 0x0

    aput-object v22, v9, v10

    const/4 v10, 0x1

    aput-object v27, v9, v10

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-static/range {v2 .. v16}, Landroid/view/MotionEvent;->obtain(JJII[I[Landroid/view/MotionEvent$PointerCoords;IFFIIII)Landroid/view/MotionEvent;

    move-result-object v17

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    const/4 v6, 0x2

    const/4 v7, 0x2

    const/4 v8, 0x2

    new-array v8, v8, [I

    fill-array-data v8, :array_3

    const/4 v9, 0x2

    new-array v9, v9, [Landroid/view/MotionEvent$PointerCoords;

    const/4 v10, 0x0

    aput-object v23, v9, v10

    const/4 v10, 0x1

    aput-object v28, v9, v10

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-static/range {v2 .. v16}, Landroid/view/MotionEvent;->obtain(JJII[I[Landroid/view/MotionEvent$PointerCoords;IFFIIII)Landroid/view/MotionEvent;

    move-result-object v17

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    const/4 v6, 0x2

    const/4 v7, 0x2

    const/4 v8, 0x2

    new-array v8, v8, [I

    fill-array-data v8, :array_4

    const/4 v9, 0x2

    new-array v9, v9, [Landroid/view/MotionEvent$PointerCoords;

    const/4 v10, 0x0

    aput-object v20, v9, v10

    const/4 v10, 0x1

    aput-object v25, v9, v10

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-static/range {v2 .. v16}, Landroid/view/MotionEvent;->obtain(JJII[I[Landroid/view/MotionEvent$PointerCoords;IFFIIII)Landroid/view/MotionEvent;

    move-result-object v17

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    const/4 v6, 0x6

    const/4 v7, 0x2

    const/4 v8, 0x2

    new-array v8, v8, [I

    fill-array-data v8, :array_5

    const/4 v9, 0x2

    new-array v9, v9, [Landroid/view/MotionEvent$PointerCoords;

    const/4 v10, 0x0

    aput-object v20, v9, v10

    const/4 v10, 0x1

    aput-object v25, v9, v10

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-static/range {v2 .. v16}, Landroid/view/MotionEvent;->obtain(JJII[I[Landroid/view/MotionEvent$PointerCoords;IFFIIII)Landroid/view/MotionEvent;

    move-result-object v17

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    const/4 v6, 0x1

    const/4 v7, 0x1

    const/4 v8, 0x1

    new-array v8, v8, [I

    const/4 v9, 0x0

    const/4 v10, 0x1

    aput v10, v8, v9

    const/4 v9, 0x1

    new-array v9, v9, [Landroid/view/MotionEvent$PointerCoords;

    const/4 v10, 0x0

    aput-object v25, v9, v10

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-static/range {v2 .. v16}, Landroid/view/MotionEvent;->obtain(JJII[I[Landroid/view/MotionEvent$PointerCoords;IFFIIII)Landroid/view/MotionEvent;

    move-result-object v17

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V

    const/4 v2, 0x1

    return v2

    :array_0
    .array-data 4
        0x0
        0x1
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x1
    .end array-data

    :array_2
    .array-data 4
        0x0
        0x1
    .end array-data

    :array_3
    .array-data 4
        0x0
        0x1
    .end array-data

    :array_4
    .array-data 4
        0x0
        0x1
    .end array-data

    :array_5
    .array-data 4
        0x0
        0x1
    .end array-data
.end method

.method public sendMoveSingle()Z
    .locals 22

    new-instance v17, Landroid/app/Instrumentation;

    invoke-direct/range {v17 .. v17}, Landroid/app/Instrumentation;-><init>()V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget v7, v0, Lcom/kehdev/ReadKeyStatus;->bx1:I

    int-to-float v7, v7

    move-object/from16 v0, p0

    iget v8, v0, Lcom/kehdev/ReadKeyStatus;->by1:I

    int-to-float v8, v8

    const/high16 v9, 0x3f800000    # 1.0f

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v11, 0x1

    const/high16 v12, 0x3f800000    # 1.0f

    const/high16 v13, 0x3f800000    # 1.0f

    const/4 v14, 0x1

    const/4 v15, 0x1

    invoke-static/range {v2 .. v15}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v16

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V

    const/16 v19, 0xa

    const/16 v18, 0x0

    :goto_0
    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->bx1:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/kehdev/ReadKeyStatus;->ex1:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/kehdev/ReadKeyStatus;->bx1:I

    sub-int/2addr v3, v4

    mul-int v3, v3, v18

    div-int v3, v3, v19

    add-int v20, v2, v3

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->by1:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/kehdev/ReadKeyStatus;->ey1:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/kehdev/ReadKeyStatus;->by1:I

    sub-int/2addr v3, v4

    mul-int v3, v3, v18

    div-int v3, v3, v19

    add-int v21, v2, v3

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    const/4 v6, 0x2

    move/from16 v0, v20

    int-to-float v7, v0

    move/from16 v0, v21

    int-to-float v8, v0

    const/high16 v9, 0x3f800000    # 1.0f

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v11, 0x1

    const/high16 v12, 0x3f800000    # 1.0f

    const/high16 v13, 0x3f800000    # 1.0f

    const/4 v14, 0x1

    const/4 v15, 0x1

    invoke-static/range {v2 .. v15}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v16

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V

    add-int/lit8 v18, v18, 0x1

    goto :goto_0

    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget v7, v0, Lcom/kehdev/ReadKeyStatus;->ex1:I

    int-to-float v7, v7

    move-object/from16 v0, p0

    iget v8, v0, Lcom/kehdev/ReadKeyStatus;->ey1:I

    int-to-float v8, v8

    const/high16 v9, 0x3f800000    # 1.0f

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v11, 0x1

    const/high16 v12, 0x3f800000    # 1.0f

    const/high16 v13, 0x3f800000    # 1.0f

    const/4 v14, 0x1

    const/4 v15, 0x1

    invoke-static/range {v2 .. v15}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v16

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V

    const/4 v2, 0x1

    return v2
.end method

.method public sendTouchEvent()Z
    .locals 22

    new-instance v18, Landroid/app/Instrumentation;

    invoke-direct/range {v18 .. v18}, Landroid/app/Instrumentation;-><init>()V

    const/16 v21, 0x0

    new-instance v19, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct/range {v19 .. v19}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    new-instance v20, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct/range {v20 .. v20}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/kehdev/ReadKeyStatus;->hadOneKeyDown:Z

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->g_remState:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_fstKeyValue:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/kehdev/ReadKeyStatus;->g_remValue:I

    if-ne v2, v3, :cond_0

    const/4 v2, 0x0

    :goto_0
    return v2

    :cond_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/kehdev/ReadKeyStatus;->hadTwoKeyDown:Z

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->g_remState:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/kehdev/ReadKeyStatus;->hadTwoKeyDown:Z

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->g_remState:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    invoke-virtual/range {p0 .. p0}, Lcom/kehdev/ReadKeyStatus;->releaseTouch()Z

    invoke-virtual/range {p0 .. p0}, Lcom/kehdev/ReadKeyStatus;->initTouchKey()V

    :cond_2
    :goto_1
    const/4 v2, 0x1

    goto :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/kehdev/ReadKeyStatus;->hadOnekeyrelease:Z

    const/4 v3, 0x1

    if-ne v2, v3, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/kehdev/ReadKeyStatus;->releaseTouch()Z

    invoke-virtual/range {p0 .. p0}, Lcom/kehdev/ReadKeyStatus;->initTouchKey()V

    goto :goto_1

    :cond_4
    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->g_remValue:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/kehdev/ReadKeyStatus;->rem_fstKeyValue:I

    if-ne v2, v3, :cond_5

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_fstKeyX:I

    int-to-float v2, v2

    move-object/from16 v0, v19

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->x:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_fstKeyY:I

    int-to-float v2, v2

    move-object/from16 v0, v19

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->y:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_secKeyX:I

    int-to-float v2, v2

    move-object/from16 v0, v20

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->x:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_secKeyY:I

    int-to-float v2, v2

    move-object/from16 v0, v20

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->y:F

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    const/4 v6, 0x6

    const/4 v7, 0x2

    const/4 v8, 0x2

    new-array v8, v8, [I

    fill-array-data v8, :array_0

    const/4 v9, 0x2

    new-array v9, v9, [Landroid/view/MotionEvent$PointerCoords;

    const/4 v10, 0x0

    aput-object v19, v9, v10

    const/4 v10, 0x1

    aput-object v20, v9, v10

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-static/range {v2 .. v16}, Landroid/view/MotionEvent;->obtain(JJII[I[Landroid/view/MotionEvent$PointerCoords;IFFIIII)Landroid/view/MotionEvent;

    move-result-object v17

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/kehdev/ReadKeyStatus;->oneKeyHadUp:Z

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/kehdev/ReadKeyStatus;->hadOnekeyrelease:Z

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_secKeyX:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_fstKeyX:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_secKeyY:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_fstKeyY:I

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_secKeyX:I

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_secKeyY:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_secKeyValue:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_fstKeyValue:I

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_secKeyValue:I

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/kehdev/ReadKeyStatus;->hadTwoKeyDown:Z

    goto/16 :goto_1

    :cond_5
    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->g_remValue:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/kehdev/ReadKeyStatus;->rem_secKeyValue:I

    if-ne v2, v3, :cond_2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_fstKeyX:I

    int-to-float v2, v2

    move-object/from16 v0, v19

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->x:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_fstKeyY:I

    int-to-float v2, v2

    move-object/from16 v0, v19

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->y:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_secKeyX:I

    int-to-float v2, v2

    move-object/from16 v0, v20

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->x:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_secKeyY:I

    int-to-float v2, v2

    move-object/from16 v0, v20

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->y:F

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    const/16 v6, 0x106

    const/4 v7, 0x2

    const/4 v8, 0x2

    new-array v8, v8, [I

    fill-array-data v8, :array_1

    const/4 v9, 0x2

    new-array v9, v9, [Landroid/view/MotionEvent$PointerCoords;

    const/4 v10, 0x0

    aput-object v19, v9, v10

    const/4 v10, 0x1

    aput-object v20, v9, v10

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-static/range {v2 .. v16}, Landroid/view/MotionEvent;->obtain(JJII[I[Landroid/view/MotionEvent$PointerCoords;IFFIIII)Landroid/view/MotionEvent;

    move-result-object v17

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_secKeyX:I

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_secKeyY:I

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_secKeyValue:I

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/kehdev/ReadKeyStatus;->twoKeyHadUp:Z

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/kehdev/ReadKeyStatus;->hadOnekeyrelease:Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/kehdev/ReadKeyStatus;->hadTwoKeyDown:Z

    goto/16 :goto_1

    :cond_6
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/kehdev/ReadKeyStatus;->hadOneKeyDown:Z

    if-eqz v2, :cond_9

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->g_remState:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_7

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->g_remValue:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/kehdev/ReadKeyStatus;->rem_fstKeyValue:I

    if-eq v2, v3, :cond_7

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/kehdev/ReadKeyStatus;->hadTwoKeyDown:Z

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->g_remValue:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_secKeyValue:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->active_x:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_secKeyX:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->active_y:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_secKeyY:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_fstKeyX:I

    int-to-float v2, v2

    move-object/from16 v0, v19

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->x:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_fstKeyY:I

    int-to-float v2, v2

    move-object/from16 v0, v19

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->y:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_secKeyX:I

    int-to-float v2, v2

    move-object/from16 v0, v20

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->x:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_secKeyY:I

    int-to-float v2, v2

    move-object/from16 v0, v20

    iput v2, v0, Landroid/view/MotionEvent$PointerCoords;->y:F

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    const/16 v6, 0x105

    const/4 v7, 0x2

    const/4 v8, 0x2

    new-array v8, v8, [I

    fill-array-data v8, :array_2

    const/4 v9, 0x2

    new-array v9, v9, [Landroid/view/MotionEvent$PointerCoords;

    const/4 v10, 0x0

    aput-object v19, v9, v10

    const/4 v10, 0x1

    aput-object v20, v9, v10

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-static/range {v2 .. v16}, Landroid/view/MotionEvent;->obtain(JJII[I[Landroid/view/MotionEvent$PointerCoords;IFFIIII)Landroid/view/MotionEvent;

    move-result-object v17

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/kehdev/ReadKeyStatus;->hadOnekeyrelease:Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/kehdev/ReadKeyStatus;->oneKeyHadUp:Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/kehdev/ReadKeyStatus;->twoKeyHadUp:Z

    goto/16 :goto_1

    :cond_7
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/kehdev/ReadKeyStatus;->oneKeyHadUp:Z

    if-eqz v2, :cond_8

    const/16 v21, 0x1

    :goto_2
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    const/4 v6, 0x1

    const/4 v7, 0x1

    const/4 v8, 0x1

    new-array v8, v8, [I

    const/4 v9, 0x0

    aput v21, v8, v9

    const/4 v9, 0x1

    new-array v9, v9, [Landroid/view/MotionEvent$PointerCoords;

    const/4 v10, 0x0

    aput-object v19, v9, v10

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-static/range {v2 .. v16}, Landroid/view/MotionEvent;->obtain(JJII[I[Landroid/view/MotionEvent$PointerCoords;IFFIIII)Landroid/view/MotionEvent;

    move-result-object v17

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V

    invoke-virtual/range {p0 .. p0}, Lcom/kehdev/ReadKeyStatus;->initTouchKey()V

    goto/16 :goto_1

    :cond_8
    const/16 v21, 0x0

    goto :goto_2

    :cond_9
    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->g_remState:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_a

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/kehdev/ReadKeyStatus;->hadOneKeyDown:Z

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->g_remValue:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_fstKeyValue:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->active_x:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_fstKeyX:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/kehdev/ReadKeyStatus;->active_y:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/kehdev/ReadKeyStatus;->rem_fstKeyY:I

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget v7, v0, Lcom/kehdev/ReadKeyStatus;->rem_fstKeyX:I

    int-to-float v7, v7

    move-object/from16 v0, p0

    iget v8, v0, Lcom/kehdev/ReadKeyStatus;->rem_fstKeyY:I

    int-to-float v8, v8

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v17

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/app/Instrumentation;->sendPointerSync(Landroid/view/MotionEvent;)V

    goto/16 :goto_1

    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/kehdev/ReadKeyStatus;->releaseTouch()Z

    invoke-virtual/range {p0 .. p0}, Lcom/kehdev/ReadKeyStatus;->initTouchKey()V

    goto/16 :goto_1

    nop

    :array_0
    .array-data 4
        0x0
        0x1
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x1
    .end array-data

    :array_2
    .array-data 4
        0x0
        0x1
    .end array-data
.end method

.method public setView(Lcom/kehdev/DeviceService;)V
    .locals 0
    .param p1    # Lcom/kehdev/DeviceService;

    iput-object p1, p0, Lcom/kehdev/ReadKeyStatus;->mView:Lcom/kehdev/DeviceService;

    return-void
.end method

.method public startReadKeyService()I
    .locals 1

    invoke-static {}, Lcom/kehdev/DeviceService;->_readRemoteKeyStatus()I

    move-result v0

    return v0
.end method
