.class public Lcom/kehdev/AddGameToSupport;
.super Landroid/app/Activity;
.source "AddGameToSupport.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kehdev/AddGameToSupport$MyAdapter;
    }
.end annotation


# instance fields
.field private TAG:Ljava/lang/String;

.field private adapter:Lcom/kehdev/AddGameToSupport$MyAdapter;

.field private installedApkListView:Landroid/widget/ListView;

.field items:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private mResolveInfo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation
.end field

.field private pm:Landroid/content/pm/PackageManager;

.field private savePkgNameMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/kehdev/AddGameToSupport;->items:Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/kehdev/AddGameToSupport;->savePkgNameMap:Ljava/util/HashMap;

    const-string v0, "PackageManagerActivity"

    iput-object v0, p0, Lcom/kehdev/AddGameToSupport;->TAG:Ljava/lang/String;

    iput-object v1, p0, Lcom/kehdev/AddGameToSupport;->pm:Landroid/content/pm/PackageManager;

    iput-object v1, p0, Lcom/kehdev/AddGameToSupport;->mResolveInfo:Ljava/util/List;

    return-void
.end method

.method static synthetic access$000(Lcom/kehdev/AddGameToSupport;)Landroid/widget/ListView;
    .locals 1
    .param p0    # Lcom/kehdev/AddGameToSupport;

    iget-object v0, p0, Lcom/kehdev/AddGameToSupport;->installedApkListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/kehdev/AddGameToSupport;)Lcom/kehdev/AddGameToSupport$MyAdapter;
    .locals 1
    .param p0    # Lcom/kehdev/AddGameToSupport;

    iget-object v0, p0, Lcom/kehdev/AddGameToSupport;->adapter:Lcom/kehdev/AddGameToSupport$MyAdapter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/kehdev/AddGameToSupport;)Ljava/util/HashMap;
    .locals 1
    .param p0    # Lcom/kehdev/AddGameToSupport;

    iget-object v0, p0, Lcom/kehdev/AddGameToSupport;->savePkgNameMap:Ljava/util/HashMap;

    return-object v0
.end method


# virtual methods
.method public checkIfLabelInList(Ljava/lang/String;Ljava/util/List;)Z
    .locals 3
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)Z"
        }
    .end annotation

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    sget-object v2, Lcom/kehdev/DeviceService;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v1, v2}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 13
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/kehdev/AddGameToSupport;->requestWindowFeature(I)Z

    const/high16 v0, 0x7f030000    # com.kehdev.R.layout.add_game_to_support_layout

    invoke-virtual {p0, v0}, Lcom/kehdev/AddGameToSupport;->setContentView(I)V

    const/high16 v0, 0x7f090000    # com.kehdev.R.id.installedApkListView

    invoke-virtual {p0, v0}, Lcom/kehdev/AddGameToSupport;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/kehdev/AddGameToSupport;->installedApkListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lcom/kehdev/AddGameToSupport;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/kehdev/AddGameToSupport;->pm:Landroid/content/pm/PackageManager;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/kehdev/AddGameToSupport;->savePkgNameMap:Ljava/util/HashMap;

    new-instance v8, Landroid/content/Intent;

    const-string v0, "android.intent.action.MAIN"

    const/4 v1, 0x0

    invoke-direct {v8, v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v0, "android.intent.category.LAUNCHER"

    invoke-virtual {v8, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/kehdev/AddGameToSupport;->pm:Landroid/content/pm/PackageManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v8, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/kehdev/AddGameToSupport;->mResolveInfo:Ljava/util/List;

    invoke-virtual {p0}, Lcom/kehdev/AddGameToSupport;->saveLabelAndComPkgName()V

    iget-object v0, p0, Lcom/kehdev/AddGameToSupport;->pm:Landroid/content/pm/PackageManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/content/pm/PackageInfo;

    iget-object v0, v12, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v1, p0, Lcom/kehdev/AddGameToSupport;->pm:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, v1}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    sget-object v0, Lcom/kehdev/DeviceService;->noSupportAppList:Ljava/util/List;

    invoke-virtual {p0, v10, v0}, Lcom/kehdev/AddGameToSupport;->checkIfLabelInList(Ljava/lang/String;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    const-string v0, "icon"

    iget-object v1, v12, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v2, p0, Lcom/kehdev/AddGameToSupport;->pm:Landroid/content/pm/PackageManager;

    invoke-virtual {v1, v2}, Landroid/content/pm/ApplicationInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "appName"

    iget-object v1, v12, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v2, p0, Lcom/kehdev/AddGameToSupport;->pm:Landroid/content/pm/PackageManager;

    invoke-virtual {v1, v2}, Landroid/content/pm/ApplicationInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "packageName"

    iget-object v1, v12, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v9, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/kehdev/AddGameToSupport;->items:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/kehdev/AddGameToSupport$MyAdapter;

    iget-object v3, p0, Lcom/kehdev/AddGameToSupport;->items:Ljava/util/ArrayList;

    const v4, 0x7f030001    # com.kehdev.R.layout.add_game_to_support_listview_layout

    const/4 v1, 0x3

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "icon"

    aput-object v2, v5, v1

    const/4 v1, 0x1

    const-string v2, "appName"

    aput-object v2, v5, v1

    const/4 v1, 0x2

    const-string v2, "packageName"

    aput-object v2, v5, v1

    const/4 v1, 0x3

    new-array v6, v1, [I

    fill-array-data v6, :array_0

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/kehdev/AddGameToSupport$MyAdapter;-><init>(Lcom/kehdev/AddGameToSupport;Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    iput-object v0, p0, Lcom/kehdev/AddGameToSupport;->adapter:Lcom/kehdev/AddGameToSupport$MyAdapter;

    iget-object v0, p0, Lcom/kehdev/AddGameToSupport;->installedApkListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/kehdev/AddGameToSupport;->adapter:Lcom/kehdev/AddGameToSupport$MyAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/kehdev/AddGameToSupport;->installedApkListView:Landroid/widget/ListView;

    new-instance v1, Lcom/kehdev/AddGameToSupport$1;

    invoke-direct {v1, p0}, Lcom/kehdev/AddGameToSupport$1;-><init>(Lcom/kehdev/AddGameToSupport;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    return-void

    nop

    :array_0
    .array-data 4
        0x7f090001    # com.kehdev.R.id.icon
        0x7f090002    # com.kehdev.R.id.appName
        0x7f090003    # com.kehdev.R.id.packageName
    .end array-data
.end method

.method public saveLabelAndComPkgName()V
    .locals 6

    iget-object v4, p0, Lcom/kehdev/AddGameToSupport;->mResolveInfo:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/ResolveInfo;

    iget-object v4, v2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v4, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    move-object v0, v3

    iget-object v4, p0, Lcom/kehdev/AddGameToSupport;->savePkgNameMap:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/kehdev/AddGameToSupport;->pm:Landroid/content/pm/PackageManager;

    invoke-virtual {v2, v5}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method
