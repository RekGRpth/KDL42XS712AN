.class Lcom/kehdev/DeviceService$10;
.super Landroid/os/Handler;
.source "DeviceService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kehdev/DeviceService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kehdev/DeviceService;


# direct methods
.method constructor <init>(Lcom/kehdev/DeviceService;)V
    .locals 0

    iput-object p1, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1    # Landroid/os/Message;

    const v5, 0x7f02001b    # com.kehdev.R.drawable.match_forbid

    const v4, 0x7f02001e    # com.kehdev.R.drawable.match_open_disable

    const v3, 0x7f02001c    # com.kehdev.R.drawable.match_forbid_disable

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    iget-object v0, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->userText:[Landroid/widget/TextView;
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$800(Lcom/kehdev/DeviceService;)[Landroid/widget/TextView;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg2:I

    aget-object v0, v0, v1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "00"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->arg2:I

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->IDText:[Landroid/widget/TextView;
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$900(Lcom/kehdev/DeviceService;)[Landroid/widget/TextView;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg2:I

    aget-object v0, v0, v1

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->TypeText:[Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$1000(Lcom/kehdev/DeviceService;)[Landroid/widget/ImageView;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg2:I

    aget-object v0, v0, v1

    const v1, 0x7f020016    # com.kehdev.R.drawable.jihuo

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->userText:[Landroid/widget/TextView;
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$800(Lcom/kehdev/DeviceService;)[Landroid/widget/TextView;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg2:I

    aget-object v0, v0, v1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "00"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->arg2:I

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->IDText:[Landroid/widget/TextView;
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$900(Lcom/kehdev/DeviceService;)[Landroid/widget/TextView;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg2:I

    aget-object v0, v0, v1

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->TypeText:[Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$1000(Lcom/kehdev/DeviceService;)[Landroid/widget/ImageView;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg2:I

    aget-object v0, v0, v1

    const v1, 0x7f02003e    # com.kehdev.R.drawable.weijihuo

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->userText:[Landroid/widget/TextView;
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$800(Lcom/kehdev/DeviceService;)[Landroid/widget/TextView;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg2:I

    aget-object v0, v0, v1

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->IDText:[Landroid/widget/TextView;
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$900(Lcom/kehdev/DeviceService;)[Landroid/widget/TextView;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg2:I

    aget-object v0, v0, v1

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->TypeText:[Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$1000(Lcom/kehdev/DeviceService;)[Landroid/widget/ImageView;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg2:I

    aget-object v0, v0, v1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->openMatchCode:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$1100(Lcom/kehdev/DeviceService;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->closeMatchCode:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$1200(Lcom/kehdev/DeviceService;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->openMatchCode:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$1100(Lcom/kehdev/DeviceService;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    invoke-virtual {v1}, Lcom/kehdev/DeviceService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->closeMatchCode:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$1200(Lcom/kehdev/DeviceService;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    invoke-virtual {v1}, Lcom/kehdev/DeviceService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->openMatchCode:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$1100(Lcom/kehdev/DeviceService;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->closeMatchCode:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$1200(Lcom/kehdev/DeviceService;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->openMatchCode:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$1100(Lcom/kehdev/DeviceService;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    invoke-virtual {v1}, Lcom/kehdev/DeviceService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02001d    # com.kehdev.R.drawable.match_open

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->closeMatchCode:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$1200(Lcom/kehdev/DeviceService;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    invoke-virtual {v1}, Lcom/kehdev/DeviceService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->openMatchCode:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$1100(Lcom/kehdev/DeviceService;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->closeMatchCode:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$1200(Lcom/kehdev/DeviceService;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->openMatchCode:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$1100(Lcom/kehdev/DeviceService;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    invoke-virtual {v1}, Lcom/kehdev/DeviceService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->closeMatchCode:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$1200(Lcom/kehdev/DeviceService;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    invoke-virtual {v1}, Lcom/kehdev/DeviceService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->openMatchCode:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$1100(Lcom/kehdev/DeviceService;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->closeMatchCode:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$1200(Lcom/kehdev/DeviceService;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->openMatchCode:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$1100(Lcom/kehdev/DeviceService;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    invoke-virtual {v1}, Lcom/kehdev/DeviceService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02001d    # com.kehdev.R.drawable.match_open

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->closeMatchCode:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$1200(Lcom/kehdev/DeviceService;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    invoke-virtual {v1}, Lcom/kehdev/DeviceService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->openMatchCode:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$1100(Lcom/kehdev/DeviceService;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->closeMatchCode:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$1200(Lcom/kehdev/DeviceService;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->openMatchCode:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$1100(Lcom/kehdev/DeviceService;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    invoke-virtual {v1}, Lcom/kehdev/DeviceService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->closeMatchCode:Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/kehdev/DeviceService;->access$1200(Lcom/kehdev/DeviceService;)Landroid/widget/ImageButton;

    move-result-object v0

    iget-object v1, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    invoke-virtual {v1}, Lcom/kehdev/DeviceService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lcom/kehdev/DeviceService$10;->this$0:Lcom/kehdev/DeviceService;

    iget-object v0, v0, Lcom/kehdev/DeviceService;->pBar:Landroid/app/ProgressDialog;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->incrementProgressBy(I)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_0
        0x30 -> :sswitch_1
        0x50 -> :sswitch_2
        0x60 -> :sswitch_3
        0x70 -> :sswitch_4
        0x80 -> :sswitch_5
        0x90 -> :sswitch_6
        0x99 -> :sswitch_7
        0x1100 -> :sswitch_8
    .end sparse-switch
.end method
