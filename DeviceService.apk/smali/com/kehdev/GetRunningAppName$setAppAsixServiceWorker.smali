.class Lcom/kehdev/GetRunningAppName$setAppAsixServiceWorker;
.super Ljava/lang/Object;
.source "GetRunningAppName.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kehdev/GetRunningAppName;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "setAppAsixServiceWorker"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kehdev/GetRunningAppName;


# direct methods
.method constructor <init>(Lcom/kehdev/GetRunningAppName;)V
    .locals 0

    iput-object p1, p0, Lcom/kehdev/GetRunningAppName$setAppAsixServiceWorker;->this$0:Lcom/kehdev/GetRunningAppName;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    :goto_0
    iget-object v0, p0, Lcom/kehdev/GetRunningAppName$setAppAsixServiceWorker;->this$0:Lcom/kehdev/GetRunningAppName;

    # invokes: Lcom/kehdev/GetRunningAppName;->getCurrentApp()Ljava/lang/String;
    invoke-static {v0}, Lcom/kehdev/GetRunningAppName;->access$000(Lcom/kehdev/GetRunningAppName;)Ljava/lang/String;

    iget-object v0, p0, Lcom/kehdev/GetRunningAppName$setAppAsixServiceWorker;->this$0:Lcom/kehdev/GetRunningAppName;

    # invokes: Lcom/kehdev/GetRunningAppName;->checkRunningAppChange()Z
    invoke-static {v0}, Lcom/kehdev/GetRunningAppName;->access$100(Lcom/kehdev/GetRunningAppName;)Z

    move-result v0

    if-nez v0, :cond_0

    const-wide/16 v0, 0x64

    invoke-static {v0, v1}, Landroid/os/SystemClock;->sleep(J)V

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/kehdev/GetRunningAppName;->g_runningAppName:Ljava/lang/String;

    sput-object v0, Lcom/kehdev/GetRunningAppName;->g_preAppName:Ljava/lang/String;

    iget-object v0, p0, Lcom/kehdev/GetRunningAppName$setAppAsixServiceWorker;->this$0:Lcom/kehdev/GetRunningAppName;

    invoke-virtual {v0}, Lcom/kehdev/GetRunningAppName;->checkRunningApp()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/kehdev/GetRunningAppName$setAppAsixServiceWorker;->this$0:Lcom/kehdev/GetRunningAppName;

    invoke-virtual {v0}, Lcom/kehdev/GetRunningAppName;->setAppAsix()I

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/kehdev/GetRunningAppName$setAppAsixServiceWorker;->this$0:Lcom/kehdev/GetRunningAppName;

    const/4 v1, -0x1

    iput v1, v0, Lcom/kehdev/GetRunningAppName;->appType:I

    iget-object v0, p0, Lcom/kehdev/GetRunningAppName$setAppAsixServiceWorker;->this$0:Lcom/kehdev/GetRunningAppName;

    invoke-virtual {v0}, Lcom/kehdev/GetRunningAppName;->setAppAsix()I

    goto :goto_0
.end method
