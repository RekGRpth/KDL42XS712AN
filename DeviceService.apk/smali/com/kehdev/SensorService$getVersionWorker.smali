.class Lcom/kehdev/SensorService$getVersionWorker;
.super Ljava/lang/Object;
.source "SensorService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kehdev/SensorService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "getVersionWorker"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kehdev/SensorService;


# direct methods
.method constructor <init>(Lcom/kehdev/SensorService;)V
    .locals 0

    iput-object p1, p0, Lcom/kehdev/SensorService$getVersionWorker;->this$0:Lcom/kehdev/SensorService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    :cond_0
    :goto_0
    const-wide/16 v2, 0x7d0

    invoke-static {v2, v3}, Landroid/os/SystemClock;->sleep(J)V

    iget-object v2, p0, Lcom/kehdev/SensorService$getVersionWorker;->this$0:Lcom/kehdev/SensorService;

    iget-object v2, v2, Lcom/kehdev/SensorService;->versionPkg:[I

    invoke-static {v2}, Lcom/kehdev/DeviceService;->_getRemoteAndDongleVersion([I)I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    :try_start_0
    iget-object v2, p0, Lcom/kehdev/SensorService$getVersionWorker;->this$0:Lcom/kehdev/SensorService;

    # getter for: Lcom/kehdev/SensorService;->serverService:Lcom/aidlServer/IAIDLServerService;
    invoke-static {v2}, Lcom/kehdev/SensorService;->access$000(Lcom/kehdev/SensorService;)Lcom/aidlServer/IAIDLServerService;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v1, "NO get version yet , please wait!"

    iget-object v2, p0, Lcom/kehdev/SensorService$getVersionWorker;->this$0:Lcom/kehdev/SensorService;

    # getter for: Lcom/kehdev/SensorService;->serverService:Lcom/aidlServer/IAIDLServerService;
    invoke-static {v2}, Lcom/kehdev/SensorService;->access$000(Lcom/kehdev/SensorService;)Lcom/aidlServer/IAIDLServerService;

    move-result-object v2

    invoke-interface {v2, v1, v1, v1}, Lcom/aidlServer/IAIDLServerService;->setAllVersion(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/kehdev/SensorService$getVersionWorker;->this$0:Lcom/kehdev/SensorService;

    iget-object v2, v2, Lcom/kehdev/SensorService;->versionPkg:[I

    invoke-static {v2}, Lcom/kehdev/DeviceService;->_getRemoteAndDongleVersion([I)I

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/kehdev/SensorService$getVersionWorker;->this$0:Lcom/kehdev/SensorService;

    iget-object v3, p0, Lcom/kehdev/SensorService$getVersionWorker;->this$0:Lcom/kehdev/SensorService;

    iget-object v3, v3, Lcom/kehdev/SensorService;->versionPkg:[I

    invoke-virtual {v2, v3}, Lcom/kehdev/SensorService;->changeBuff2Display([I)Ljava/lang/String;

    :try_start_1
    iget-object v2, p0, Lcom/kehdev/SensorService$getVersionWorker;->this$0:Lcom/kehdev/SensorService;

    # getter for: Lcom/kehdev/SensorService;->serverService:Lcom/aidlServer/IAIDLServerService;
    invoke-static {v2}, Lcom/kehdev/SensorService;->access$000(Lcom/kehdev/SensorService;)Lcom/aidlServer/IAIDLServerService;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/kehdev/SensorService$getVersionWorker;->this$0:Lcom/kehdev/SensorService;

    # getter for: Lcom/kehdev/SensorService;->serverService:Lcom/aidlServer/IAIDLServerService;
    invoke-static {v2}, Lcom/kehdev/SensorService;->access$000(Lcom/kehdev/SensorService;)Lcom/aidlServer/IAIDLServerService;

    move-result-object v2

    iget-object v3, p0, Lcom/kehdev/SensorService$getVersionWorker;->this$0:Lcom/kehdev/SensorService;

    sget-object v4, Lcom/kehdev/SensorService;->remoteVerStr:Ljava/lang/String;

    # invokes: Lcom/kehdev/SensorService;->divRemoteVerToCanDisplay(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v3, v4}, Lcom/kehdev/SensorService;->access$100(Lcom/kehdev/SensorService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/kehdev/SensorService;->dongleVerStr:Ljava/lang/String;

    sget-object v5, Lcom/kehdev/DeviceService;->driverVersion:Ljava/lang/String;

    invoke-interface {v2, v3, v4, v5}, Lcom/aidlServer/IAIDLServerService;->setAllVersion(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
