.class public Lcom/kehdev/AxisSelect;
.super Landroid/app/Activity;
.source "AxisSelect.java"


# static fields
.field private static aixisflag:I


# instance fields
.field private TAG:Ljava/lang/String;

.field private axisSltListview:Landroid/widget/ListView;

.field private axisSltListviewAdapterAList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private axisSlts:I

.field private bindSucc:Z

.field private completePkgName:Ljava/lang/String;

.field private gameId:I

.field private imageView1:Landroid/widget/ImageView;

.field private imageView2:Landroid/widget/ImageView;

.field private intentGet:Landroid/content/Intent;

.field private mRunningAppName:Lcom/kehdev/GetRunningAppName;

.field private mRunningAppNameConn:Landroid/content/ServiceConnection;

.field private mSqliteOperation:Lcom/kehdev/SqliteOperation;

.field private rela1:Landroid/widget/RelativeLayout;

.field private rela2:Landroid/widget/RelativeLayout;

.field private status1:Landroid/widget/ImageView;

.field private status2:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/kehdev/AxisSelect;->aixisflag:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    iput-boolean v0, p0, Lcom/kehdev/AxisSelect;->bindSucc:Z

    iput v0, p0, Lcom/kehdev/AxisSelect;->axisSlts:I

    iput v0, p0, Lcom/kehdev/AxisSelect;->gameId:I

    const-string v0, "AxisSelect"

    iput-object v0, p0, Lcom/kehdev/AxisSelect;->TAG:Ljava/lang/String;

    iput-object v1, p0, Lcom/kehdev/AxisSelect;->axisSltListview:Landroid/widget/ListView;

    iput-object v1, p0, Lcom/kehdev/AxisSelect;->axisSltListviewAdapterAList:Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/kehdev/AxisSelect;->intentGet:Landroid/content/Intent;

    iput-object v1, p0, Lcom/kehdev/AxisSelect;->completePkgName:Ljava/lang/String;

    iput-object v1, p0, Lcom/kehdev/AxisSelect;->mRunningAppName:Lcom/kehdev/GetRunningAppName;

    iput-object v1, p0, Lcom/kehdev/AxisSelect;->mSqliteOperation:Lcom/kehdev/SqliteOperation;

    new-instance v0, Lcom/kehdev/AxisSelect$1;

    invoke-direct {v0, p0}, Lcom/kehdev/AxisSelect$1;-><init>(Lcom/kehdev/AxisSelect;)V

    iput-object v0, p0, Lcom/kehdev/AxisSelect;->mRunningAppNameConn:Landroid/content/ServiceConnection;

    return-void
.end method

.method static synthetic access$000(Lcom/kehdev/AxisSelect;)Lcom/kehdev/GetRunningAppName;
    .locals 1
    .param p0    # Lcom/kehdev/AxisSelect;

    iget-object v0, p0, Lcom/kehdev/AxisSelect;->mRunningAppName:Lcom/kehdev/GetRunningAppName;

    return-object v0
.end method

.method static synthetic access$002(Lcom/kehdev/AxisSelect;Lcom/kehdev/GetRunningAppName;)Lcom/kehdev/GetRunningAppName;
    .locals 0
    .param p0    # Lcom/kehdev/AxisSelect;
    .param p1    # Lcom/kehdev/GetRunningAppName;

    iput-object p1, p0, Lcom/kehdev/AxisSelect;->mRunningAppName:Lcom/kehdev/GetRunningAppName;

    return-object p1
.end method

.method static synthetic access$100(Lcom/kehdev/AxisSelect;)Lcom/kehdev/SqliteOperation;
    .locals 1
    .param p0    # Lcom/kehdev/AxisSelect;

    iget-object v0, p0, Lcom/kehdev/AxisSelect;->mSqliteOperation:Lcom/kehdev/SqliteOperation;

    return-object v0
.end method

.method static synthetic access$102(Lcom/kehdev/AxisSelect;Lcom/kehdev/SqliteOperation;)Lcom/kehdev/SqliteOperation;
    .locals 0
    .param p0    # Lcom/kehdev/AxisSelect;
    .param p1    # Lcom/kehdev/SqliteOperation;

    iput-object p1, p0, Lcom/kehdev/AxisSelect;->mSqliteOperation:Lcom/kehdev/SqliteOperation;

    return-object p1
.end method

.method static synthetic access$202(Lcom/kehdev/AxisSelect;Z)Z
    .locals 0
    .param p0    # Lcom/kehdev/AxisSelect;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/kehdev/AxisSelect;->bindSucc:Z

    return p1
.end method

.method static synthetic access$300(Lcom/kehdev/AxisSelect;)I
    .locals 1
    .param p0    # Lcom/kehdev/AxisSelect;

    iget v0, p0, Lcom/kehdev/AxisSelect;->gameId:I

    return v0
.end method

.method static synthetic access$302(Lcom/kehdev/AxisSelect;I)I
    .locals 0
    .param p0    # Lcom/kehdev/AxisSelect;
    .param p1    # I

    iput p1, p0, Lcom/kehdev/AxisSelect;->gameId:I

    return p1
.end method

.method static synthetic access$400(Lcom/kehdev/AxisSelect;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/kehdev/AxisSelect;

    iget-object v0, p0, Lcom/kehdev/AxisSelect;->completePkgName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/kehdev/AxisSelect;)I
    .locals 1
    .param p0    # Lcom/kehdev/AxisSelect;

    iget v0, p0, Lcom/kehdev/AxisSelect;->axisSlts:I

    return v0
.end method

.method static synthetic access$502(Lcom/kehdev/AxisSelect;I)I
    .locals 0
    .param p0    # Lcom/kehdev/AxisSelect;
    .param p1    # I

    iput p1, p0, Lcom/kehdev/AxisSelect;->axisSlts:I

    return p1
.end method

.method static synthetic access$600(Lcom/kehdev/AxisSelect;)Landroid/widget/ImageView;
    .locals 1
    .param p0    # Lcom/kehdev/AxisSelect;

    iget-object v0, p0, Lcom/kehdev/AxisSelect;->status2:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/kehdev/AxisSelect;)Landroid/widget/ImageView;
    .locals 1
    .param p0    # Lcom/kehdev/AxisSelect;

    iget-object v0, p0, Lcom/kehdev/AxisSelect;->status1:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/kehdev/AxisSelect;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0    # Lcom/kehdev/AxisSelect;

    iget-object v0, p0, Lcom/kehdev/AxisSelect;->rela1:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$900(Lcom/kehdev/AxisSelect;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0    # Lcom/kehdev/AxisSelect;

    iget-object v0, p0, Lcom/kehdev/AxisSelect;->rela2:Landroid/widget/RelativeLayout;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/kehdev/AxisSelect;->requestWindowFeature(I)Z

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030004    # com.kehdev.R.layout.dialog

    invoke-virtual {p0, v0}, Lcom/kehdev/AxisSelect;->setContentView(I)V

    const v0, 0x7f090008    # com.kehdev.R.id.img1

    invoke-virtual {p0, v0}, Lcom/kehdev/AxisSelect;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/kehdev/AxisSelect;->imageView1:Landroid/widget/ImageView;

    const v0, 0x7f09000b    # com.kehdev.R.id.img2

    invoke-virtual {p0, v0}, Lcom/kehdev/AxisSelect;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/kehdev/AxisSelect;->imageView2:Landroid/widget/ImageView;

    const v0, 0x7f090009    # com.kehdev.R.id.status1

    invoke-virtual {p0, v0}, Lcom/kehdev/AxisSelect;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/kehdev/AxisSelect;->status1:Landroid/widget/ImageView;

    const v0, 0x7f09000c    # com.kehdev.R.id.status2

    invoke-virtual {p0, v0}, Lcom/kehdev/AxisSelect;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/kehdev/AxisSelect;->status2:Landroid/widget/ImageView;

    const v0, 0x7f090007    # com.kehdev.R.id.rela1

    invoke-virtual {p0, v0}, Lcom/kehdev/AxisSelect;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/kehdev/AxisSelect;->rela1:Landroid/widget/RelativeLayout;

    const v0, 0x7f09000a    # com.kehdev.R.id.rela2

    invoke-virtual {p0, v0}, Lcom/kehdev/AxisSelect;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/kehdev/AxisSelect;->rela2:Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/kehdev/AxisSelect;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/kehdev/AxisSelect;->intentGet:Landroid/content/Intent;

    iget-object v0, p0, Lcom/kehdev/AxisSelect;->intentGet:Landroid/content/Intent;

    const-string v1, "AppPkgCompleteName"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/kehdev/AxisSelect;->completePkgName:Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/kehdev/GetRunningAppName;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/kehdev/AxisSelect;->mRunningAppNameConn:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0, v1, v2}, Lcom/kehdev/AxisSelect;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    iget-object v0, p0, Lcom/kehdev/AxisSelect;->rela1:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/kehdev/AxisSelect$2;

    invoke-direct {v1, p0}, Lcom/kehdev/AxisSelect$2;-><init>(Lcom/kehdev/AxisSelect;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/kehdev/AxisSelect;->rela2:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/kehdev/AxisSelect$3;

    invoke-direct {v1, p0}, Lcom/kehdev/AxisSelect$3;-><init>(Lcom/kehdev/AxisSelect;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-boolean v0, p0, Lcom/kehdev/AxisSelect;->bindSucc:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/kehdev/AxisSelect;->mRunningAppNameConn:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/kehdev/AxisSelect;->mRunningAppNameConn:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/kehdev/AxisSelect;->unbindService(Landroid/content/ServiceConnection;)V

    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const v3, 0x7f020036    # com.kehdev.R.drawable.trans_background

    const/16 v2, 0x8

    const/4 v1, 0x0

    const/16 v0, 0x13

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/kehdev/AxisSelect;->status2:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/kehdev/AxisSelect;->status1:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/kehdev/AxisSelect;->rela1:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/kehdev/AxisSelect;->rela2:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    const/4 v0, 0x1

    sput v0, Lcom/kehdev/AxisSelect;->aixisflag:I

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0

    :cond_1
    const/16 v0, 0x14

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/kehdev/AxisSelect;->status1:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/kehdev/AxisSelect;->status2:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/kehdev/AxisSelect;->rela2:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/kehdev/AxisSelect;->rela1:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    sput v1, Lcom/kehdev/AxisSelect;->aixisflag:I

    goto :goto_0

    :cond_2
    const/16 v0, 0x42

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/kehdev/AxisSelect;->mSqliteOperation:Lcom/kehdev/SqliteOperation;

    iget-object v1, p0, Lcom/kehdev/AxisSelect;->completePkgName:Ljava/lang/String;

    sget v2, Lcom/kehdev/AxisSelect;->aixisflag:I

    invoke-virtual {v0, v1, v2}, Lcom/kehdev/SqliteOperation;->saveAxisToDatabase(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_3
    invoke-virtual {p0}, Lcom/kehdev/AxisSelect;->finish()V

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onStop()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method
