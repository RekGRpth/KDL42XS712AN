.class public Lcom/kehdev/SetTouchCoorMess;
.super Landroid/app/Activity;
.source "SetTouchCoorMess.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private setButt:Landroid/widget/Button;

.field x:I

.field y:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const-string v0, "SetTouchCoorMess"

    iput-object v0, p0, Lcom/kehdev/SetTouchCoorMess;->TAG:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/kehdev/SetTouchCoorMess;->setButt:Landroid/widget/Button;

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    const/16 v1, 0x400

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/kehdev/SetTouchCoorMess;->requestWindowFeature(I)Z

    const v0, 0x7f030015    # com.kehdev.R.layout.set_touch_coor_mess

    invoke-virtual {p0, v0}, Lcom/kehdev/SetTouchCoorMess;->setContentView(I)V

    invoke-virtual {p0}, Lcom/kehdev/SetTouchCoorMess;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    const v0, 0x7f090058    # com.kehdev.R.id.setButt

    invoke-virtual {p0, v0}, Lcom/kehdev/SetTouchCoorMess;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/kehdev/SetTouchCoorMess;->setButt:Landroid/widget/Button;

    iget-object v0, p0, Lcom/kehdev/SetTouchCoorMess;->setButt:Landroid/widget/Button;

    new-instance v1, Lcom/kehdev/SetTouchCoorMess$1;

    invoke-direct {v1, p0}, Lcom/kehdev/SetTouchCoorMess$1;-><init>(Lcom/kehdev/SetTouchCoorMess;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onStop()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/kehdev/SetTouchCoorMess;->x:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/kehdev/SetTouchCoorMess;->y:I

    const/4 v0, 0x1

    return v0
.end method
