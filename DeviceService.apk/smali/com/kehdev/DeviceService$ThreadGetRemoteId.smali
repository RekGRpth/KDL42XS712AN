.class Lcom/kehdev/DeviceService$ThreadGetRemoteId;
.super Ljava/lang/Object;
.source "DeviceService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kehdev/DeviceService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ThreadGetRemoteId"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kehdev/DeviceService;


# direct methods
.method constructor <init>(Lcom/kehdev/DeviceService;)V
    .locals 0

    iput-object p1, p0, Lcom/kehdev/DeviceService$ThreadGetRemoteId;->this$0:Lcom/kehdev/DeviceService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    const-wide/16 v9, 0x0

    const/4 v8, 0x1

    const/4 v1, 0x1

    :cond_0
    if-eqz v1, :cond_6

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v8, :cond_2

    iget-object v6, p0, Lcom/kehdev/DeviceService$ThreadGetRemoteId;->this$0:Lcom/kehdev/DeviceService;

    iget-object v6, v6, Lcom/kehdev/DeviceService;->RemoteId:[I

    invoke-static {v2}, Lcom/kehdev/DeviceService;->_GetRemoteID(I)I

    move-result v7

    aput v7, v6, v2

    iget-object v6, p0, Lcom/kehdev/DeviceService$ThreadGetRemoteId;->this$0:Lcom/kehdev/DeviceService;

    iget-object v6, v6, Lcom/kehdev/DeviceService;->RemoteId:[I

    aget v6, v6, v2

    if-lez v6, :cond_1

    iget-object v6, p0, Lcom/kehdev/DeviceService$ThreadGetRemoteId;->this$0:Lcom/kehdev/DeviceService;

    iget-object v6, v6, Lcom/kehdev/DeviceService;->RemoteIsOnline:[Z

    invoke-static {v2}, Lcom/kehdev/DeviceService;->_isARemoteInline(I)Z

    move-result v7

    aput-boolean v7, v6, v2

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    :goto_1
    if-ge v3, v8, :cond_5

    new-instance v5, Landroid/os/Message;

    invoke-direct {v5}, Landroid/os/Message;-><init>()V

    iget-object v6, p0, Lcom/kehdev/DeviceService$ThreadGetRemoteId;->this$0:Lcom/kehdev/DeviceService;

    iget-object v6, v6, Lcom/kehdev/DeviceService;->RemoteId:[I

    aget v6, v6, v3

    if-lez v6, :cond_3

    iget-object v6, p0, Lcom/kehdev/DeviceService$ThreadGetRemoteId;->this$0:Lcom/kehdev/DeviceService;

    iget-object v6, v6, Lcom/kehdev/DeviceService;->RemoteId:[I

    aget v6, v6, v3

    iput v6, v5, Landroid/os/Message;->arg1:I

    iput v3, v5, Landroid/os/Message;->arg2:I

    iget-object v6, p0, Lcom/kehdev/DeviceService$ThreadGetRemoteId;->this$0:Lcom/kehdev/DeviceService;

    iget-object v6, v6, Lcom/kehdev/DeviceService;->RemoteIsOnline:[Z

    aget-boolean v6, v6, v3

    if-eqz v6, :cond_4

    const/16 v6, 0x20

    iput v6, v5, Landroid/os/Message;->what:I

    iget-object v6, p0, Lcom/kehdev/DeviceService$ThreadGetRemoteId;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->mMsgReciver:Landroid/os/Handler;
    invoke-static {v6}, Lcom/kehdev/DeviceService;->access$300(Lcom/kehdev/DeviceService;)Landroid/os/Handler;

    move-result-object v6

    invoke-virtual {v6, v5, v9, v10}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_3
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    const/16 v6, 0x30

    iput v6, v5, Landroid/os/Message;->what:I

    iget-object v6, p0, Lcom/kehdev/DeviceService$ThreadGetRemoteId;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->mMsgReciver:Landroid/os/Handler;
    invoke-static {v6}, Lcom/kehdev/DeviceService;->access$300(Lcom/kehdev/DeviceService;)Landroid/os/Handler;

    move-result-object v6

    invoke-virtual {v6, v5, v9, v10}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_2

    :cond_5
    const-wide/16 v6, 0x7d0

    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_3
    iget-object v6, p0, Lcom/kehdev/DeviceService$ThreadGetRemoteId;->this$0:Lcom/kehdev/DeviceService;

    iget-object v6, v6, Lcom/kehdev/DeviceService;->RemoteId:[I

    const/4 v7, 0x0

    aget v6, v6, v7

    if-nez v6, :cond_0

    const/4 v4, 0x0

    :goto_4
    if-ge v4, v8, :cond_0

    new-instance v5, Landroid/os/Message;

    invoke-direct {v5}, Landroid/os/Message;-><init>()V

    iget-object v6, p0, Lcom/kehdev/DeviceService$ThreadGetRemoteId;->this$0:Lcom/kehdev/DeviceService;

    iget-object v6, v6, Lcom/kehdev/DeviceService;->RemoteId:[I

    aget v6, v6, v4

    iput v6, v5, Landroid/os/Message;->arg1:I

    iput v4, v5, Landroid/os/Message;->arg2:I

    const/16 v6, 0x50

    iput v6, v5, Landroid/os/Message;->what:I

    iget-object v6, p0, Lcom/kehdev/DeviceService$ThreadGetRemoteId;->this$0:Lcom/kehdev/DeviceService;

    # getter for: Lcom/kehdev/DeviceService;->mMsgReciver:Landroid/os/Handler;
    invoke-static {v6}, Lcom/kehdev/DeviceService;->access$300(Lcom/kehdev/DeviceService;)Landroid/os/Handler;

    move-result-object v6

    invoke-virtual {v6, v5, v9, v10}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    :cond_6
    return-void
.end method
