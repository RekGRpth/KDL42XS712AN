.class public Lcom/kehdev/MySqlHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "MySqlHelper.java"


# static fields
.field private static TAG:Ljava/lang/String;

.field public static final initDatas:[Ljava/lang/String;

.field public static final initTouch1080p:[Ljava/lang/String;

.field private static mDbName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v0, "MySqlHelper"

    sput-object v0, Lcom/kehdev/MySqlHelper;->TAG:Ljava/lang/String;

    const-string v0, "kehdevDevice.db"

    sput-object v0, Lcom/kehdev/MySqlHelper;->mDbName:Ljava/lang/String;

    const/16 v0, 0x7c

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "INSERT INTO versions (versionID,updateType,updateTime,versionName,versionNO) values(1,\'DataBase\',0,\'20120909\',\'20120909\')"

    aput-object v1, v0, v3

    const-string v1, "INSERT INTO versions (versionID,updateType,updateTime,versionName,versionNO) values(2,\'Device\',0,\'AnD000400R120816X\',\'1.0\')"

    aput-object v1, v0, v4

    const-string v1, "INSERT INTO axisOrn (ID,description) values(0,\'TYPE_AXIS_NORMAL\')"

    aput-object v1, v0, v5

    const-string v1, "INSERT INTO axisOrn (ID,description) values(1,\'TYPE_AXIS_pZ_ANTICLK_90\')"

    aput-object v1, v0, v6

    const-string v1, "INSERT INTO axisOrn (ID,description) values(2,\'TYPE_AXIS_pZ_ANTICLK_180\')"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "INSERT INTO axisOrn (ID,description) values(3,\'TYPE_AXIS_pZ_ANTICLK_270\')"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "INSERT INTO axisOrn (ID,description) values(4,\'TYPE_AXIS_nZ_Yrvs_ANTICLK_00\')"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "INSERT INTO axisOrn (ID,description) values(5,\'TYPE_AXIS_nZ_Yrvs_ANTICLK_90\')"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "INSERT INTO axisOrn (ID,description) values(6,\'TYPE_AXIS_nZ_Yrvs_ANTICLK_180\')"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "INSERT INTO axisOrn (ID,description) values(7,\'TYPE_AXIS_nZ_Yrvs_ANTICLK_270\')"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "INSERT INTO games (ID,gameStr,name,axisID) values(1,\'com.lotuz.sensortest\',\'SensorTest\',0)"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "INSERT INTO games (ID,gameStr,name,axisID) values(2,\'com.vectorunit.blue\',\'\u5feb\u8247\',1)"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "INSERT INTO games (ID,gameStr,name,axisID) values(3,\'com.ezone.Snowboard\',\'\u6ed1\u96ea\',1)"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "INSERT INTO games (ID,gameStr,name,axisID) values(4,\'nl.dotsightsoftware.pacificfighter.release\',\'\u592a\u5e73\u6d0b\u6d77\u6218\',1)"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "INSERT INTO games (ID,gameStr,name,axisID) values(5,\'se.illusionlabs.labyrinth.full\',\'\u91cd\u529b\u611f\u5e94\u8ff7\u5bab\',1)"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "INSERT INTO games (ID,gameStr,name,axisID) values(6,\'net.osaris.turbofly\',\'\u8d85\u97f3\u901f\u98de\u884c\',1)"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "INSERT INTO games (ID,gameStr,name,axisID) values(7,\'com.zerracsoft.steamball\',\'\u84b8\u6c7d\u6eda\u7403\',0)"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "INSERT INTO games (ID,gameStr,name,axisID) values(8,\'com.trans.pingpang2\',\'pingpang\',0)"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "INSERT INTO games (ID,gameStr,name,axisID) values(9,\'com.robotinvader.knightmare\',\'wind-up knight\',0)"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "INSERT INTO games (ID,gameStr,name,axisID) values(10,\'com.trans.beatpenguin\',\'BeatPenguin\',0)"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "INSERT INTO games (ID,gameStr,name,axisID) values(11,\'com.wireddevelopments.wipeoutdash\',\'\u6eda\u7403\u8df3\u8dc3\',1)"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "INSERT INTO games (ID,gameStr,name,axisID) values(12,\'com.trans.tennis\',\'Tennis\',0)"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "INSERT INTO games (ID,gameStr,name,axisID) values(13,\'com.realarcade.DOJ\',\'\u662f\u7537\u4eba\u5c31\u4e0a100\u5c42\',1)"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "INSERT INTO games (ID,gameStr,name,axisID) values(14,\'com.galapagossoft.trial\',\'3D\u6781\u9650\u6469\u6258\',1)"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "INSERT INTO games (ID,gameStr,name,axisID) values(15,\'com.polarbit.rthunder2play\',\'\u96f7\u9706\u8d5b\u8f662\',1)"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "INSERT INTO games (ID,gameStr,name,axisID) values(16,\'com.rpg90.pipi_cn_ad\',\'\u7ed2\u7ed2\u7403VS\u5fcd\u8005\',0)"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "INSERT INTO games (ID,gameStr,name,axisID) values(17,\'com.sticksports.stickcricket\',\'\u677f\u7403\u8fd0\u52a8\',0)"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "INSERT INTO games (ID,gameStr,name,axisID) values(18,\'com.sinyee.babybus.kartRacing\',\'\u718a\u732b\u5361\u4e01\u8f66\',0)"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string v2, "INSERT INTO games (ID,gameStr,name,axisID) values(19,\'com.project9.bpuzzle\',\'\u94f6\u5e01\u730e\u4eba\',0)"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string v2, "INSERT INTO games (ID,gameStr,name,axisID) values(20,\'com.wavecade.mypaperplane_x_lite\',\'\u7eb8\u98de\u673a2\',1)"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string v2, "INSERT INTO games (ID,gameStr,name,axisID) values(21,\'com.app.xjiajia\',\'\u8fd0\u52a8\u52a0\u52a0\',0)"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string v2, "INSERT INTO games (ID,gameStr,name,axisID) values(22,\'com.bengigi.noogranuts\',\'\u7b28\u677e\u9f20\',1)"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string v2, "INSERT INTO games (ID,gameStr,name,axisID) values(23,\'com.gamelion.speedx3d\',\'3D\u96a7\u9053\',1)"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string v2, "INSERT INTO games (ID,gameStr,name,axisID) values(24,\'com.polarbit.krazyracers\',\'\u75af\u72c2\u5361\u4e01\u8f66\',0)"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string v2, "INSERT INTO RemoteActions (ID, description) values(0,\'KEY_VALUE_UP\')"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string v2, "INSERT INTO RemoteActions (ID, description) values(1,\'KEY_VALUE_DOWN\')"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string v2, "INSERT INTO RemoteActions (ID, description) values(2,\'KEY_VALUE_RIGHT\')"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string v2, "INSERT INTO RemoteActions (ID, description) values(3,\'KEY_VALUE_LEFT\')"

    aput-object v2, v0, v1

    const/16 v1, 0x26

    const-string v2, "INSERT INTO RemoteActions (ID, description) values(4,\'KEY_VALUE_HOME\')"

    aput-object v2, v0, v1

    const/16 v1, 0x27

    const-string v2, "INSERT INTO RemoteActions (ID, description) values(5,\'KEY_VALUE_NEXT\')"

    aput-object v2, v0, v1

    const/16 v1, 0x28

    const-string v2, "INSERT INTO RemoteActions (ID, description) values(6,\'KEY_VALUE_PREV\')"

    aput-object v2, v0, v1

    const/16 v1, 0x29

    const-string v2, "INSERT INTO RemoteActions (ID, description) values(7,\'KEY_VALUE_STOP\')"

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    const-string v2, "INSERT INTO RemoteActions (ID, description) values(8,\'KEY_VALUE_VOL\')"

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    const-string v2, "INSERT INTO RemoteActions (ID, description) values(9,\'KEY_VALUE_PP\')"

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    const-string v2, "INSERT INTO RemoteActions (ID, description) values(10,\'KEY_VALUE_MR\')"

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    const-string v2, "INSERT INTO RemoteActions (ID, description) values(11,\'KEY_VALUE_TASK\')"

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    const-string v2, "INSERT INTO RemoteActions (ID, description) values(12,\'KEY_VALUE_ENTER\')"

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    const-string v2, "INSERT INTO RemoteActions (ID, description) values(13,\'KEY_VALUE_SP\')"

    aput-object v2, v0, v1

    const/16 v1, 0x30

    const-string v2, "INSERT INTO RemoteActions (ID, description) values(14,\'KEY_VALUE_ML\')"

    aput-object v2, v0, v1

    const/16 v1, 0x31

    const-string v2, "INSERT INTO RemoteActions (ID, description) values(15,\'KEY_VALUE_FUC\')"

    aput-object v2, v0, v1

    const/16 v1, 0x32

    const-string v2, "INSERT INTO RemoteActions (ID, description) values(16,\'KEY_VALUE_SPWD\')"

    aput-object v2, v0, v1

    const/16 v1, 0x33

    const-string v2, "INSERT INTO RemoteActions (ID, description) values(17,\'KEY_VALUE_DEV\')"

    aput-object v2, v0, v1

    const/16 v1, 0x34

    const-string v2, "INSERT INTO RemoteActions (ID, description) values(18,\'KEY_VALUE_LASER\')"

    aput-object v2, v0, v1

    const/16 v1, 0x35

    const-string v2, "INSERT INTO RemoteActions (ID, description) values(19,\'KEY_VALUE_RESERVE\')"

    aput-object v2, v0, v1

    const/16 v1, 0x36

    const-string v2, "INSERT INTO RemoteActions (ID, description) values(20,\'KEY_A_SENSOR_FRONT\')"

    aput-object v2, v0, v1

    const/16 v1, 0x37

    const-string v2, "INSERT INTO RemoteActions (ID, description) values(21,\'KEY_A_SENSOR_BEHIND\')"

    aput-object v2, v0, v1

    const/16 v1, 0x38

    const-string v2, "INSERT INTO RemoteActions (ID, description) values(22,\'KEY_A_SENSOR_LEFT\')"

    aput-object v2, v0, v1

    const/16 v1, 0x39

    const-string v2, "INSERT INTO RemoteActions (ID, description) values(23,\'KEY_A_SENSOR_RIGHT\')"

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    const-string v2, "INSERT INTO RemoteActions (ID, description) values(24,\'KEY_A_SENSOR_RESERVE\')"

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(29, 0, \'brake\', 58, 630)"

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(37, 0, \'injection\', 640, 340)"

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(38, 0, \'stop\', 640, 30)"

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(46, 0, \'back\', 50, 630)"

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1, bY1, eX1, eY1, bX2, bY2, eX2, eY2) values(25, 2, \'skill1\',  200, 600, 100, 600,  1000, 600, 900, 600)"

    aput-object v2, v0, v1

    const/16 v1, 0x40

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1, bY1, eX1, eY1, bX2, bY2, eX2, eY2) values(26, 2, \'skill2\',  100, 600, 200, 600,  900, 600, 1000, 600)"

    aput-object v2, v0, v1

    const/16 v1, 0x41

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1, bY1, eX1, eY1, bX2, bY2, eX2, eY2) values(27, 2, \'skill3\',  200, 650, 200, 550,  1080, 650, 1080, 550)"

    aput-object v2, v0, v1

    const/16 v1, 0x42

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1, bY1, eX1, eY1, bX2, bY2, eX2, eY2) values(28, 2, \'skill4\',  200, 550, 200, 650,  1080, 550, 1080, 650)"

    aput-object v2, v0, v1

    const/16 v1, 0x43

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1, bY1, eX1, eY1, bX2, bY2, eX2, eY2) values(30, 2, \'skill5\', 100, 600, 200, 600,  1000, 600, 900, 600)"

    aput-object v2, v0, v1

    const/16 v1, 0x44

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1, bY1, eX1, eY1, bX2, bY2, eX2, eY2) values(31, 2, \'skill6\', 200, 600, 100, 600,  900, 600, 1000, 600)"

    aput-object v2, v0, v1

    const/16 v1, 0x45

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1, bY1, eX1, eY1, bX2, bY2, eX2, eY2) values(32, 2, \'skill7\', 200, 650, 200, 550,  900, 550, 900, 650)"

    aput-object v2, v0, v1

    const/16 v1, 0x46

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1, bY1, eX1, eY1, bX2, bY2, eX2, eY2) values(33, 2, \'skill8\', 200, 550, 200, 650,  900, 650, 900, 550)"

    aput-object v2, v0, v1

    const/16 v1, 0x47

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1, bY1, eX1, eY1, bX2, bY2, eX2, eY2) values(45, 2, \'Injection speed\', 1143, 124, 1154, 190,  1180, 326, 1180, 240)"

    aput-object v2, v0, v1

    const/16 v1, 0x48

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(50, 0, \'turn left\', 45, 451)"

    aput-object v2, v0, v1

    const/16 v1, 0x49

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(51, 0, \'turn right\',195, 447 )"

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(52, 0, \'turn forward\', 107, 385)"

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(53, 0, \'turn back\', 117, 519)"

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(55, 0, \'skill1\', 1110, 400)"

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(56, 0, \'skill2\', 1206, 494)"

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(57, 0, \'skill3\', 1210, 397)"

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(58, 0, \'skill4\', 1112, 505)"

    aput-object v2, v0, v1

    const/16 v1, 0x50

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(137, 0, \'launch\', 1175, 358)"

    aput-object v2, v0, v1

    const/16 v1, 0x51

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1,eX1,eY1) values(150, 1, \'Left perspective\', 640, 360, 620, 360)"

    aput-object v2, v0, v1

    const/16 v1, 0x52

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1,eX1,eY1) values(151, 1, \'Right perspective\', 640, 360, 660, 360)"

    aput-object v2, v0, v1

    const/16 v1, 0x53

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1,eX1,eY1) values(152, 1, \'Front perspective\', 640, 360, 640, 380)"

    aput-object v2, v0, v1

    const/16 v1, 0x54

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1,eX1,eY1) values(153, 1, \'Behind perspective\', 640, 360, 640, 340)"

    aput-object v2, v0, v1

    const/16 v1, 0x55

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1,eX1,eY1) values(175, 1, \'turn left\', 500, 360, 300, 360)"

    aput-object v2, v0, v1

    const/16 v1, 0x56

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1,eX1,eY1) values(176, 1, \'turn right\', 500, 302, 700, 302)"

    aput-object v2, v0, v1

    const/16 v1, 0x57

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1,eX1,eY1) values(178, 1, \'jump\', 300, 300, 300, 500)"

    aput-object v2, v0, v1

    const/16 v1, 0x58

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1,eX1,eY1) values(177, 1, \'squat\', 300, 600, 300, 300)"

    aput-object v2, v0, v1

    const/16 v1, 0x59

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(184, 0, \'start1\', 609, 471)"

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1, eX1,eY1) values(183, 1, \'start2\', 609, 471,  609, 553)"

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1, eX1,eY1) values(182, 1, \'start3\', 609, 471,  609, 553)"

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(204, 0, \'start\', 629, 358)"

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(202, 0, \'jump\', 1159, 582)"

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(209, 0, \'sword\', 940, 611)"

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(203, 0, \'down\', 336, 624)"

    aput-object v2, v0, v1

    const/16 v1, 0x60

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(205, 0, \'shield\', 118, 574)"

    aput-object v2, v0, v1

    const/16 v1, 0x61

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(206, 0, \'none\', 0, 0)"

    aput-object v2, v0, v1

    const/16 v1, 0x62

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(207, 0, \'stop\', 1194,33)"

    aput-object v2, v0, v1

    const/16 v1, 0x63

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(208, 0, \'none\',0 ,0 )"

    aput-object v2, v0, v1

    const/16 v1, 0x64

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(254, 0, \'retry\', 955, 444)"

    aput-object v2, v0, v1

    const/16 v1, 0x65

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(259, 0, \'again\', 635, 677)"

    aput-object v2, v0, v1

    const/16 v1, 0x66

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(257, 0, \'next\', 1165, 687)"

    aput-object v2, v0, v1

    const/16 v1, 0x67

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(279, 0, \'start\', 548, 635)"

    aput-object v2, v0, v1

    const/16 v1, 0x68

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(280, 0, \'coin in\', 418, 641)"

    aput-object v2, v0, v1

    const/16 v1, 0x69

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(284, 0, \'action\', 1025, 618)"

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(282, 0, \'jump\', 1177, 618)"

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(277, 0, \'up\', 151, 47)"

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(278, 0, \'down\', 145, 242)"

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(295, 0, \'up\', 151, 47)"

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(296, 0, \'down\', 145, 242)"

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(276, 0, \'right\', 237, 148)"

    aput-object v2, v0, v1

    const/16 v1, 0x70

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(275, 0, \'left\', 55, 166)"

    aput-object v2, v0, v1

    const/16 v1, 0x71

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(328, 0, \'back\', 946, 617)"

    aput-object v2, v0, v1

    const/16 v1, 0x72

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(327, 0, \'forward\', 1130, 615)"

    aput-object v2, v0, v1

    const/16 v1, 0x73

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(354, 0, \'speed\', 1129, 418)"

    aput-object v2, v0, v1

    const/16 v1, 0x74

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(359, 0, \'break\', 109, 599)"

    aput-object v2, v0, v1

    const/16 v1, 0x75

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(384, 0, \'shoot\', 823, 270)"

    aput-object v2, v0, v1

    const/16 v1, 0x76

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(404, 0, \'left\', 224, 489)"

    aput-object v2, v0, v1

    const/16 v1, 0x77

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(409, 0, \'right\', 1055, 483)"

    aput-object v2, v0, v1

    const/16 v1, 0x78

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(309, 0, \'shoot\', 823, 270)"

    aput-object v2, v0, v1

    const/16 v1, 0x79

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(534, 0, \'jump\', 857,575)"

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(579, 0, \'kill1\', 1246,581)"

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(584, 0, \'kill2\', 1218,663)"

    aput-object v2, v0, v1

    sput-object v0, Lcom/kehdev/MySqlHelper;->initDatas:[Ljava/lang/String;

    const/16 v0, 0x1a

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(20037, 0, \'injection\', 1855, 416)"

    aput-object v1, v0, v3

    const-string v1, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(20046, 0, \'back\', 304, 384)"

    aput-object v1, v0, v4

    const-string v1, "INSERT INTO ScreenTouch (ID, touchType, description,bX1, bY1, eX1, eY1, bX2, bY2, eX2, eY2) values(20025, 2, \'skill\',  100, 400, 1100, 400,  100, 600, 1100, 600)"

    aput-object v1, v0, v5

    const-string v1, "INSERT INTO ScreenTouch (ID, touchType, description,bX1, bY1, eX1, eY1, bX2, bY2, eX2, eY2) values(20026, 2, \'skill\',  100, 600, 1100, 600,  100, 400, 1100, 400)"

    aput-object v1, v0, v6

    const-string v1, "INSERT INTO ScreenTouch (ID, touchType, description,bX1, bY1, eX1, eY1, bX2, bY2, eX2, eY2) values(20027, 2, \'skill\',  100, 600, 1100, 400,  100, 400, 1100, 600)"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1, bY1, eX1, eY1, bX2, bY2, eX2, eY2) values(20028, 2, \'skill\',  100, 400, 1100, 600,  100, 600, 1100, 400)"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1, bY1, eX1, eY1, bX2, bY2, eX2, eY2) values(20030, 2, \'skill\', 400, 600, 900, 600, 200, 600, 1100, 600)"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1, bY1, eX1, eY1, bX2, bY2, eX2, eY2) values(20031, 2, \'skill\', 200, 600, 1100, 600, 400, 600, 900, 600)"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1, bY1, eX1, eY1, bX2, bY2, eX2, eY2) values(20032, 2, \'skill\', 400, 600, 1100, 600, 200, 600, 900, 600)"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1, bY1, eX1, eY1, bX2, bY2, eX2, eY2) values(20033, 2, \'skill\', 400, 600, 900, 600, 600, 600, 1100, 600)"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1, bY1, eX1, eY1, bX2, bY2, eX2, eY2) values(20045, 2, \'Injection speed\', 1810, 209, 1830, 282,  1855, 383, 1862, 471)"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(20050, 0, \'turn\', 42, 880)"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(20051, 0, \'turn\',189, 829 )"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(20052, 0, \'turn\', 107, 794)"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(20053, 0, \'turn\', 142, 938)"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(20055, 0, \'skill\', 1782, 792)"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(20056, 0, \'skill\', 1751, 903)"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(20057, 0, \'skill\', 1865, 927)"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(20058, 0, \'skill\', 1845, 927)"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(20137, 0, \'skill\', 1733, 587)"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1,eX1,eY1) values(20150, 1, \'switchview\', 640, 360, 620, 360)"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1,eX1,eY1) values(20151, 1, \'switchview\', 640, 360, 660, 360)"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1,eX1,eY1) values(20152, 1, \'switchview\', 640, 360, 640, 380)"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1,eX1,eY1) values(20153, 1, \'switchview\', 640, 360, 640, 340)"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(20328, 0, \'back\', 1426, 936)"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "INSERT INTO ScreenTouch (ID, touchType, description,bX1,bY1) values(20327, 0, \'forward\', 1759, 916)"

    aput-object v2, v0, v1

    sput-object v0, Lcom/kehdev/MySqlHelper;->initTouch1080p:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    sget-object v0, Lcom/kehdev/MySqlHelper;->mDbName:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/database/sqlite/SQLiteDatabase$CursorFactory;
    .param p4    # I

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;

    :try_start_0
    const-string v2, "CREATE TABLE IF NOT EXISTS games (ID integer NOT NULL UNIQUE DEFAULT 0,gameStr varchar(100) NOT NULL PRIMARY KEY UNIQUE,name varchar(100),axisID integer DEFAULT 0)"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "CREATE TABLE IF NOT EXISTS axisOrn (ID smallint NOT NULL PRIMARY KEY DEFAULT 0,description varchar(100) NOT NULL)"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "CREATE TABLE IF NOT EXISTS remoteActions (ID integer PRIMARY KEY AUTOINCREMENT DEFAULT 0,description varchar(100) NOT NULL)"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "CREATE TABLE IF NOT EXISTS screenTouch (ID integer PRIMARY KEY AUTOINCREMENT DEFAULT 0,touchType smallint NOT NULL DEFAULT -1,description varchar(200) NOT NULL,bX1 smallint DEFAULT 0,bY1 smallint DEFAULT 0,eX1 smallint DEFAULT 0,eY1 smallint DEFAULT 0,bX2 smallint DEFAULT 0,bY2 smallint DEFAULT 0,eX2 smallint DEFAULT 0,eY2 smallint DEFAULT 0)"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "CREATE TABLE IF NOT EXISTS gameActions (gameID integer NOT NULL,remoteAction integer NOT NULL,screenTouch integer NOT NULL,description varchar(100) NOT NULL,PRIMARY KEY (gameID,remoteAction,screenTouch))"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "CREATE TABLE IF NOT EXISTS supportGameMess (ID integer NOT NULL PRIMARY KEY AUTOINCREMENT , gameName varchar(100) NOT NULL, downloadAddress varchar(100) NOT NULL, description varchar(200))"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const-string v2, "CREATE TABLE versions (versionID integer PRIMARY KEY AUTOINCREMENT DEFAULT 0,updateType varchar(20),updateTime time DEFAULT 0, versionName varchar(20), versionNO varchar(20) DEFAULT 1.0)"

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const/4 v1, 0x0

    :goto_0
    sget-object v2, Lcom/kehdev/MySqlHelper;->initDatas:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_0

    sget-object v2, Lcom/kehdev/MySqlHelper;->initDatas:[Ljava/lang/String;

    aget-object v2, v2, v1

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_1
    sget-object v2, Lcom/kehdev/MySqlHelper;->initTouch1080p:[Ljava/lang/String;

    array-length v2, v2

    if-ge v1, v2, :cond_1

    sget-object v2, Lcom/kehdev/MySqlHelper;->initTouch1080p:[Ljava/lang/String;

    aget-object v2, v2, v1

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    :cond_1
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1
    .param p1    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2    # I
    .param p3    # I

    const-string v0, "DROP TABLE IF EXISTS games"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lcom/kehdev/MySqlHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    return-void
.end method
