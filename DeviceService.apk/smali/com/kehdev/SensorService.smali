.class public Lcom/kehdev/SensorService;
.super Landroid/app/Service;
.source "SensorService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/kehdev/SensorService$getVersionWorker;,
        Lcom/kehdev/SensorService$LocalBinder;
    }
.end annotation


# static fields
.field private static TAG:Ljava/lang/String;

.field public static dongleVerStr:Ljava/lang/String;

.field public static remoteVerStr:Ljava/lang/String;


# instance fields
.field private final SIZEVERSION:I

.field public flg:I

.field private sc:Landroid/content/ServiceConnection;

.field private serverService:Lcom/aidlServer/IAIDLServerService;

.field public versionPkg:[I

.field private windowManager:Landroid/view/IWindowManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "SensorService"

    sput-object v0, Lcom/kehdev/SensorService;->TAG:Ljava/lang/String;

    const-string v0, ""

    sput-object v0, Lcom/kehdev/SensorService;->remoteVerStr:Ljava/lang/String;

    const-string v0, ""

    sput-object v0, Lcom/kehdev/SensorService;->dongleVerStr:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/16 v1, 0x14

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/kehdev/SensorService;->flg:I

    iput v1, p0, Lcom/kehdev/SensorService;->SIZEVERSION:I

    new-array v0, v1, [I

    iput-object v0, p0, Lcom/kehdev/SensorService;->versionPkg:[I

    iput-object v2, p0, Lcom/kehdev/SensorService;->serverService:Lcom/aidlServer/IAIDLServerService;

    iput-object v2, p0, Lcom/kehdev/SensorService;->windowManager:Landroid/view/IWindowManager;

    new-instance v0, Lcom/kehdev/SensorService$1;

    invoke-direct {v0, p0}, Lcom/kehdev/SensorService$1;-><init>(Lcom/kehdev/SensorService;)V

    iput-object v0, p0, Lcom/kehdev/SensorService;->sc:Landroid/content/ServiceConnection;

    return-void
.end method

.method static synthetic access$000(Lcom/kehdev/SensorService;)Lcom/aidlServer/IAIDLServerService;
    .locals 1
    .param p0    # Lcom/kehdev/SensorService;

    iget-object v0, p0, Lcom/kehdev/SensorService;->serverService:Lcom/aidlServer/IAIDLServerService;

    return-object v0
.end method

.method static synthetic access$002(Lcom/kehdev/SensorService;Lcom/aidlServer/IAIDLServerService;)Lcom/aidlServer/IAIDLServerService;
    .locals 0
    .param p0    # Lcom/kehdev/SensorService;
    .param p1    # Lcom/aidlServer/IAIDLServerService;

    iput-object p1, p0, Lcom/kehdev/SensorService;->serverService:Lcom/aidlServer/IAIDLServerService;

    return-object p1
.end method

.method static synthetic access$100(Lcom/kehdev/SensorService;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/kehdev/SensorService;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/kehdev/SensorService;->divRemoteVerToCanDisplay(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private divRemoteVerToCanDisplay(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;

    const-string v1, ""

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p1, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v3, "B"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v2, v1

    :goto_1
    return-object v2

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method


# virtual methods
.method public changeBuff2Display([I)Ljava/lang/String;
    .locals 7
    .param p1    # [I

    const/16 v6, 0x50

    const/4 v5, 0x4

    new-array v2, v5, [C

    new-array v3, v6, [C

    new-array v4, v6, [C

    const/4 v0, 0x0

    :goto_0
    const/16 v5, 0x14

    if-ge v0, v5, :cond_0

    aget v5, p1, v0

    invoke-virtual {p0, v5}, Lcom/kehdev/SensorService;->int2charString(I)[C

    move-result-object v2

    mul-int/lit8 v5, v0, 0x4

    const/4 v6, 0x3

    aget-char v6, v2, v6

    aput-char v6, v3, v5

    mul-int/lit8 v5, v0, 0x4

    add-int/lit8 v5, v5, 0x1

    const/4 v6, 0x2

    aget-char v6, v2, v6

    aput-char v6, v3, v5

    mul-int/lit8 v5, v0, 0x4

    add-int/lit8 v5, v5, 0x2

    const/4 v6, 0x1

    aget-char v6, v2, v6

    aput-char v6, v3, v5

    mul-int/lit8 v5, v0, 0x4

    add-int/lit8 v5, v5, 0x3

    const/4 v6, 0x0

    aget-char v6, v2, v6

    aput-char v6, v3, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {p0, v1}, Lcom/kehdev/SensorService;->divStrToNeed(Ljava/lang/String;)V

    return-object v1
.end method

.method public divStrToNeed(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    const-string v2, ""

    sput-object v2, Lcom/kehdev/SensorService;->remoteVerStr:Ljava/lang/String;

    const-string v2, ""

    sput-object v2, Lcom/kehdev/SensorService;->dongleVerStr:Ljava/lang/String;

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_3

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v3, "|"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    add-int/lit8 v0, v0, 0x1

    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    if-nez v0, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/kehdev/SensorService;->remoteVerStr:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p1, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/kehdev/SensorService;->remoteVerStr:Ljava/lang/String;

    goto :goto_1

    :cond_2
    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/kehdev/SensorService;->dongleVerStr:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {p1, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/kehdev/SensorService;->dongleVerStr:Ljava/lang/String;

    goto :goto_1

    :cond_3
    return-void
.end method

.method public int2charString(I)[C
    .locals 3
    .param p1    # I

    const/4 v1, 0x4

    new-array v0, v1, [C

    const/4 v1, 0x3

    and-int/lit16 v2, p1, 0xff

    int-to-char v2, v2

    aput-char v2, v0, v1

    const/4 v1, 0x2

    const v2, 0xff00

    and-int/2addr v2, p1

    shr-int/lit8 v2, v2, 0x8

    int-to-char v2, v2

    aput-char v2, v0, v1

    const/4 v1, 0x1

    const/high16 v2, 0xff0000

    and-int/2addr v2, p1

    shr-int/lit8 v2, v2, 0x10

    int-to-char v2, v2

    aput-char v2, v0, v1

    const/4 v1, 0x0

    const/high16 v2, -0x1000000

    and-int/2addr v2, p1

    shr-int/lit8 v2, v2, 0x18

    int-to-char v2, v2

    aput-char v2, v0, v1

    return-object v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 6

    const/4 v5, 0x0

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    new-instance v0, Landroid/app/Notification;

    const v1, 0x7f020011    # com.kehdev.R.drawable.icon

    const-string v2, "F-dolphin Service"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/kehdev/DeviceService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p0, v5, v1, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    const-string v2, "F-dolphin Service"

    const-string v3, ""

    invoke-virtual {v0, p0, v2, v3, v1}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    const/16 v1, 0x10

    iput v1, v0, Landroid/app/Notification;->flags:I

    const/16 v1, 0x4d3

    invoke-virtual {p0, v1, v0}, Lcom/kehdev/SensorService;->startForeground(ILandroid/app/Notification;)V

    const-string v0, "window"

    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Landroid/view/IWindowManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/view/IWindowManager;

    move-result-object v0

    iput-object v0, p0, Lcom/kehdev/SensorService;->windowManager:Landroid/view/IWindowManager;

    :try_start_0
    iget-object v0, p0, Lcom/kehdev/SensorService;->windowManager:Landroid/view/IWindowManager;

    const/4 v1, -0x1

    invoke-interface {v0, v1}, Landroid/view/IWindowManager;->freezeRotation(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-virtual {p0}, Lcom/kehdev/SensorService;->startkehdevService()I

    new-instance v0, Ljava/lang/Thread;

    const/4 v1, 0x0

    new-instance v2, Lcom/kehdev/SensorService$getVersionWorker;

    invoke-direct {v2, p0}, Lcom/kehdev/SensorService$getVersionWorker;-><init>(Lcom/kehdev/SensorService;)V

    const-string v3, "getVersion"

    invoke-direct {v0, v1, v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void

    :catch_0
    move-exception v0

    sget-object v0, Lcom/kehdev/SensorService;->TAG:Ljava/lang/String;

    const-string v1, "Unable to save auto-rotate setting"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/kehdev/SensorService;->stopForeground(Z)V

    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 0
    .param p1    # Landroid/content/Intent;
    .param p2    # I

    invoke-super {p0, p1, p2}, Landroid/app/Service;->onStart(Landroid/content/Intent;I)V

    return-void
.end method

.method public startkehdevService()I
    .locals 1

    invoke-static {}, Lcom/kehdev/DeviceService;->_startService()I

    move-result v0

    return v0
.end method
