.class Lcom/kehdev/AddGameToSupport$MyAdapter;
.super Landroid/widget/SimpleAdapter;
.source "AddGameToSupport.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kehdev/AddGameToSupport;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MyAdapter"
.end annotation


# instance fields
.field private appData:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;>;"
        }
    .end annotation
.end field

.field private appFrom:[Ljava/lang/String;

.field private appInflater:Landroid/view/LayoutInflater;

.field private appResource:I

.field private appTo:[I

.field private appViewBinder:Landroid/widget/SimpleAdapter$ViewBinder;

.field final synthetic this$0:Lcom/kehdev/AddGameToSupport;


# direct methods
.method public constructor <init>(Lcom/kehdev/AddGameToSupport;Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V
    .locals 6
    .param p2    # Landroid/content/Context;
    .param p4    # I
    .param p5    # [Ljava/lang/String;
    .param p6    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<+",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;>;I[",
            "Ljava/lang/String;",
            "[I)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/kehdev/AddGameToSupport$MyAdapter;->this$0:Lcom/kehdev/AddGameToSupport;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    iput-object p3, p0, Lcom/kehdev/AddGameToSupport$MyAdapter;->appData:Ljava/util/List;

    iput p4, p0, Lcom/kehdev/AddGameToSupport$MyAdapter;->appResource:I

    iput-object p5, p0, Lcom/kehdev/AddGameToSupport$MyAdapter;->appFrom:[Ljava/lang/String;

    iput-object p6, p0, Lcom/kehdev/AddGameToSupport$MyAdapter;->appTo:[I

    const-string v0, "layout_inflater"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/kehdev/AddGameToSupport$MyAdapter;->appInflater:Landroid/view/LayoutInflater;

    return-void
.end method

.method private bindView(ILandroid/view/View;)V
    .locals 14
    .param p1    # I
    .param p2    # Landroid/view/View;

    iget-object v11, p0, Lcom/kehdev/AddGameToSupport$MyAdapter;->appData:Ljava/util/List;

    invoke-interface {v11, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map;

    if-nez v4, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/kehdev/AddGameToSupport$MyAdapter;->appViewBinder:Landroid/widget/SimpleAdapter$ViewBinder;

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, [Landroid/view/View;

    move-object v6, v11

    check-cast v6, [Landroid/view/View;

    iget-object v5, p0, Lcom/kehdev/AddGameToSupport$MyAdapter;->appFrom:[Ljava/lang/String;

    iget-object v9, p0, Lcom/kehdev/AddGameToSupport$MyAdapter;->appTo:[I

    array-length v2, v9

    const/4 v7, 0x0

    :goto_0
    if-ge v7, v2, :cond_0

    aget-object v10, v6, v7

    if-eqz v10, :cond_4

    aget-object v11, v5, v7

    invoke-interface {v4, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_5

    const-string v8, ""

    :goto_1
    if-nez v8, :cond_2

    const-string v8, ""

    :cond_2
    const/4 v1, 0x0

    if-eqz v0, :cond_3

    invoke-interface {v0, v10, v3, v8}, Landroid/widget/SimpleAdapter$ViewBinder;->setViewValue(Landroid/view/View;Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v1

    :cond_3
    if-nez v1, :cond_4

    instance-of v11, v10, Landroid/widget/TextView;

    if-eqz v11, :cond_6

    check-cast v10, Landroid/widget/TextView;

    invoke-virtual {p0, v10, v8}, Lcom/kehdev/AddGameToSupport$MyAdapter;->setViewText(Landroid/widget/TextView;Ljava/lang/String;)V

    :cond_4
    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_5
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    goto :goto_1

    :cond_6
    instance-of v11, v10, Landroid/widget/ImageView;

    if-eqz v11, :cond_7

    check-cast v10, Landroid/widget/ImageView;

    check-cast v3, Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v10, v3}, Lcom/kehdev/AddGameToSupport$MyAdapter;->setViewImage(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    :cond_7
    new-instance v11, Ljava/lang/IllegalStateException;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " is not a "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "view that can be bounds by this SimpleAdapter"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v11
.end method

.method private createViewFromResource(ILandroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 7
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;
    .param p4    # I

    if-nez p2, :cond_1

    iget-object v5, p0, Lcom/kehdev/AddGameToSupport$MyAdapter;->appInflater:Landroid/view/LayoutInflater;

    const/4 v6, 0x0

    invoke-virtual {v5, p4, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    iget-object v3, p0, Lcom/kehdev/AddGameToSupport$MyAdapter;->appTo:[I

    array-length v0, v3

    new-array v1, v0, [Landroid/view/View;

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    aget v5, v3, v2

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    aput-object v5, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v4, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_1
    invoke-direct {p0, p1, v4}, Lcom/kehdev/AddGameToSupport$MyAdapter;->bindView(ILandroid/view/View;)V

    return-object v4

    :cond_1
    move-object v4, p2

    goto :goto_1
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    iget v0, p0, Lcom/kehdev/AddGameToSupport$MyAdapter;->appResource:I

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/kehdev/AddGameToSupport$MyAdapter;->createViewFromResource(ILandroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public setViewImage(Landroid/widget/ImageView;Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1    # Landroid/widget/ImageView;
    .param p2    # Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method
