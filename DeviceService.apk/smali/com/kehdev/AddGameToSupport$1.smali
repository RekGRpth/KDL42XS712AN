.class Lcom/kehdev/AddGameToSupport$1;
.super Ljava/lang/Object;
.source "AddGameToSupport.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/kehdev/AddGameToSupport;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kehdev/AddGameToSupport;


# direct methods
.method constructor <init>(Lcom/kehdev/AddGameToSupport;)V
    .locals 0

    iput-object p1, p0, Lcom/kehdev/AddGameToSupport$1;->this$0:Lcom/kehdev/AddGameToSupport;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/kehdev/AddGameToSupport$1;->this$0:Lcom/kehdev/AddGameToSupport;

    # getter for: Lcom/kehdev/AddGameToSupport;->installedApkListView:Landroid/widget/ListView;
    invoke-static {v0}, Lcom/kehdev/AddGameToSupport;->access$000(Lcom/kehdev/AddGameToSupport;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->requestFocusFromTouch()Z

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, Lcom/kehdev/AddGameToSupport$1;->this$0:Lcom/kehdev/AddGameToSupport;

    const-class v1, Lcom/kehdev/GameManual;

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/kehdev/AddGameToSupport$1;->this$0:Lcom/kehdev/AddGameToSupport;

    # getter for: Lcom/kehdev/AddGameToSupport;->adapter:Lcom/kehdev/AddGameToSupport$MyAdapter;
    invoke-static {v0}, Lcom/kehdev/AddGameToSupport;->access$100(Lcom/kehdev/AddGameToSupport;)Lcom/kehdev/AddGameToSupport$MyAdapter;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/kehdev/AddGameToSupport$MyAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    const-string v1, "icon"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/Drawable;

    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    const-string v3, "appIconBitmap"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "appName"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "appSltLable"

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "completePkgName"

    iget-object v3, p0, Lcom/kehdev/AddGameToSupport$1;->this$0:Lcom/kehdev/AddGameToSupport;

    # getter for: Lcom/kehdev/AddGameToSupport;->savePkgNameMap:Ljava/util/HashMap;
    invoke-static {v3}, Lcom/kehdev/AddGameToSupport;->access$200(Lcom/kehdev/AddGameToSupport;)Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/kehdev/AddGameToSupport$1;->this$0:Lcom/kehdev/AddGameToSupport;

    invoke-virtual {v0, v2}, Lcom/kehdev/AddGameToSupport;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/kehdev/AddGameToSupport$1;->this$0:Lcom/kehdev/AddGameToSupport;

    invoke-virtual {v0}, Lcom/kehdev/AddGameToSupport;->finish()V

    return-void
.end method
