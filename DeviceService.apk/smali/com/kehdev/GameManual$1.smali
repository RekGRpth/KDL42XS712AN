.class Lcom/kehdev/GameManual$1;
.super Ljava/lang/Object;
.source "GameManual.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/kehdev/GameManual;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/kehdev/GameManual;


# direct methods
.method constructor <init>(Lcom/kehdev/GameManual;)V
    .locals 0

    iput-object p1, p0, Lcom/kehdev/GameManual$1;->this$0:Lcom/kehdev/GameManual;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    iget-object v0, p0, Lcom/kehdev/GameManual$1;->this$0:Lcom/kehdev/GameManual;

    check-cast p2, Lcom/kehdev/GetRunningAppName$LocalBinder;

    invoke-virtual {p2}, Lcom/kehdev/GetRunningAppName$LocalBinder;->getService()Lcom/kehdev/GetRunningAppName;

    move-result-object v1

    # setter for: Lcom/kehdev/GameManual;->mRunningAppName:Lcom/kehdev/GetRunningAppName;
    invoke-static {v0, v1}, Lcom/kehdev/GameManual;->access$002(Lcom/kehdev/GameManual;Lcom/kehdev/GetRunningAppName;)Lcom/kehdev/GetRunningAppName;

    iget-object v0, p0, Lcom/kehdev/GameManual$1;->this$0:Lcom/kehdev/GameManual;

    # getter for: Lcom/kehdev/GameManual;->mRunningAppName:Lcom/kehdev/GetRunningAppName;
    invoke-static {v0}, Lcom/kehdev/GameManual;->access$000(Lcom/kehdev/GameManual;)Lcom/kehdev/GetRunningAppName;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/kehdev/GameManual$1;->this$0:Lcom/kehdev/GameManual;

    iget-object v1, p0, Lcom/kehdev/GameManual$1;->this$0:Lcom/kehdev/GameManual;

    # getter for: Lcom/kehdev/GameManual;->mRunningAppName:Lcom/kehdev/GetRunningAppName;
    invoke-static {v1}, Lcom/kehdev/GameManual;->access$000(Lcom/kehdev/GameManual;)Lcom/kehdev/GetRunningAppName;

    move-result-object v1

    iget-object v1, v1, Lcom/kehdev/GetRunningAppName;->mSqliteOperation:Lcom/kehdev/SqliteOperation;

    # setter for: Lcom/kehdev/GameManual;->mSqliteOperation:Lcom/kehdev/SqliteOperation;
    invoke-static {v0, v1}, Lcom/kehdev/GameManual;->access$102(Lcom/kehdev/GameManual;Lcom/kehdev/SqliteOperation;)Lcom/kehdev/SqliteOperation;

    iget-object v0, p0, Lcom/kehdev/GameManual$1;->this$0:Lcom/kehdev/GameManual;

    # getter for: Lcom/kehdev/GameManual;->mSqliteOperation:Lcom/kehdev/SqliteOperation;
    invoke-static {v0}, Lcom/kehdev/GameManual;->access$100(Lcom/kehdev/GameManual;)Lcom/kehdev/SqliteOperation;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/kehdev/GameManual$1;->this$0:Lcom/kehdev/GameManual;

    const/4 v1, 0x1

    # setter for: Lcom/kehdev/GameManual;->bindSucc:Z
    invoke-static {v0, v1}, Lcom/kehdev/GameManual;->access$202(Lcom/kehdev/GameManual;Z)Z

    iget-object v0, p0, Lcom/kehdev/GameManual$1;->this$0:Lcom/kehdev/GameManual;

    iget-object v1, p0, Lcom/kehdev/GameManual$1;->this$0:Lcom/kehdev/GameManual;

    # getter for: Lcom/kehdev/GameManual;->mSqliteOperation:Lcom/kehdev/SqliteOperation;
    invoke-static {v1}, Lcom/kehdev/GameManual;->access$100(Lcom/kehdev/GameManual;)Lcom/kehdev/SqliteOperation;

    move-result-object v1

    iget-object v2, p0, Lcom/kehdev/GameManual$1;->this$0:Lcom/kehdev/GameManual;

    # getter for: Lcom/kehdev/GameManual;->completePkgName:Ljava/lang/String;
    invoke-static {v2}, Lcom/kehdev/GameManual;->access$400(Lcom/kehdev/GameManual;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/kehdev/SqliteOperation;->checkIfTheGameInDatabase(Ljava/lang/String;)I

    move-result v1

    # setter for: Lcom/kehdev/GameManual;->gameId:I
    invoke-static {v0, v1}, Lcom/kehdev/GameManual;->access$302(Lcom/kehdev/GameManual;I)I

    iget-object v0, p0, Lcom/kehdev/GameManual$1;->this$0:Lcom/kehdev/GameManual;

    # getter for: Lcom/kehdev/GameManual;->gameId:I
    invoke-static {v0}, Lcom/kehdev/GameManual;->access$300(Lcom/kehdev/GameManual;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/kehdev/GameManual$1;->this$0:Lcom/kehdev/GameManual;

    invoke-virtual {v0}, Lcom/kehdev/GameManual;->checkOutAllMessage()Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/kehdev/GameManual$1;->this$0:Lcom/kehdev/GameManual;

    # getter for: Lcom/kehdev/GameManual;->mSqliteOperation:Lcom/kehdev/SqliteOperation;
    invoke-static {v0}, Lcom/kehdev/GameManual;->access$100(Lcom/kehdev/GameManual;)Lcom/kehdev/SqliteOperation;

    move-result-object v0

    iget-object v1, p0, Lcom/kehdev/GameManual$1;->this$0:Lcom/kehdev/GameManual;

    # getter for: Lcom/kehdev/GameManual;->completePkgName:Ljava/lang/String;
    invoke-static {v1}, Lcom/kehdev/GameManual;->access$400(Lcom/kehdev/GameManual;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/kehdev/SqliteOperation;->insertNewAppToDatebase(Ljava/lang/String;I)Z

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;

    iget-object v0, p0, Lcom/kehdev/GameManual$1;->this$0:Lcom/kehdev/GameManual;

    const/4 v1, 0x0

    # setter for: Lcom/kehdev/GameManual;->mRunningAppName:Lcom/kehdev/GetRunningAppName;
    invoke-static {v0, v1}, Lcom/kehdev/GameManual;->access$002(Lcom/kehdev/GameManual;Lcom/kehdev/GetRunningAppName;)Lcom/kehdev/GetRunningAppName;

    return-void
.end method
