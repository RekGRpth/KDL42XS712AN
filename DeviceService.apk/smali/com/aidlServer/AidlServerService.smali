.class public Lcom/aidlServer/AidlServerService;
.super Landroid/app/Service;
.source "AidlServerService.java"


# instance fields
.field private final SIZEVERSION:I

.field public TAG:Ljava/lang/String;

.field public bindServiceFlg:Z

.field public driverVerStr:Ljava/lang/String;

.field public getMouseResoFlg:Z

.field public mBinder:Lcom/aidlServer/IAIDLServerService$Stub;

.field public mGetResoValue:I

.field public mRunningAppNameConn:Landroid/content/ServiceConnection;

.field public mSensorService:Lcom/kehdev/SensorService;

.field public mSetResoValue:I

.field public mVersion:Lcom/aidlServer/Version;

.field public setMouseResoFlg:Z

.field public versionPkg:[I


# direct methods
.method public constructor <init>()V
    .locals 3

    const/16 v2, 0x14

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    const-string v0, "AidlServerService"

    iput-object v0, p0, Lcom/aidlServer/AidlServerService;->TAG:Ljava/lang/String;

    iput v2, p0, Lcom/aidlServer/AidlServerService;->SIZEVERSION:I

    new-array v0, v2, [I

    iput-object v0, p0, Lcom/aidlServer/AidlServerService;->versionPkg:[I

    const-string v0, ""

    iput-object v0, p0, Lcom/aidlServer/AidlServerService;->driverVerStr:Ljava/lang/String;

    new-instance v0, Lcom/aidlServer/Version;

    invoke-direct {v0}, Lcom/aidlServer/Version;-><init>()V

    iput-object v0, p0, Lcom/aidlServer/AidlServerService;->mVersion:Lcom/aidlServer/Version;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/aidlServer/AidlServerService;->mSensorService:Lcom/kehdev/SensorService;

    iput v1, p0, Lcom/aidlServer/AidlServerService;->mSetResoValue:I

    iput v1, p0, Lcom/aidlServer/AidlServerService;->mGetResoValue:I

    iput-boolean v1, p0, Lcom/aidlServer/AidlServerService;->setMouseResoFlg:Z

    iput-boolean v1, p0, Lcom/aidlServer/AidlServerService;->getMouseResoFlg:Z

    iput-boolean v1, p0, Lcom/aidlServer/AidlServerService;->bindServiceFlg:Z

    new-instance v0, Lcom/aidlServer/AidlServerService$1;

    invoke-direct {v0, p0}, Lcom/aidlServer/AidlServerService$1;-><init>(Lcom/aidlServer/AidlServerService;)V

    iput-object v0, p0, Lcom/aidlServer/AidlServerService;->mRunningAppNameConn:Landroid/content/ServiceConnection;

    new-instance v0, Lcom/aidlServer/AidlServerService$2;

    invoke-direct {v0, p0}, Lcom/aidlServer/AidlServerService$2;-><init>(Lcom/aidlServer/AidlServerService;)V

    iput-object v0, p0, Lcom/aidlServer/AidlServerService;->mBinder:Lcom/aidlServer/IAIDLServerService$Stub;

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/aidlServer/AidlServerService;->mBinder:Lcom/aidlServer/IAIDLServerService$Stub;

    return-object v0
.end method

.method public onDestroy()V
    .locals 3

    iget-boolean v0, p0, Lcom/aidlServer/AidlServerService;->bindServiceFlg:Z

    if-eqz v0, :cond_0

    const-string v0, "bindServiceFlg"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "bindServiceFlg = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/aidlServer/AidlServerService;->bindServiceFlg:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/aidlServer/AidlServerService;->mRunningAppNameConn:Landroid/content/ServiceConnection;

    invoke-virtual {p0, v0}, Lcom/aidlServer/AidlServerService;->unbindService(Landroid/content/ServiceConnection;)V

    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method
