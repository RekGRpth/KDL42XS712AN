.class Lcom/aidlServer/AidlServerService$1;
.super Ljava/lang/Object;
.source "AidlServerService.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/aidlServer/AidlServerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/aidlServer/AidlServerService;


# direct methods
.method constructor <init>(Lcom/aidlServer/AidlServerService;)V
    .locals 0

    iput-object p1, p0, Lcom/aidlServer/AidlServerService$1;->this$0:Lcom/aidlServer/AidlServerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    iget-object v0, p0, Lcom/aidlServer/AidlServerService$1;->this$0:Lcom/aidlServer/AidlServerService;

    iget-object v0, v0, Lcom/aidlServer/AidlServerService;->TAG:Ljava/lang/String;

    const-string v1, "onServiceConnected "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/aidlServer/AidlServerService$1;->this$0:Lcom/aidlServer/AidlServerService;

    check-cast p2, Lcom/kehdev/SensorService$LocalBinder;

    invoke-virtual {p2}, Lcom/kehdev/SensorService$LocalBinder;->getService()Lcom/kehdev/SensorService;

    move-result-object v1

    iput-object v1, v0, Lcom/aidlServer/AidlServerService;->mSensorService:Lcom/kehdev/SensorService;

    iget-object v0, p0, Lcom/aidlServer/AidlServerService$1;->this$0:Lcom/aidlServer/AidlServerService;

    iget-object v0, v0, Lcom/aidlServer/AidlServerService;->mSensorService:Lcom/kehdev/SensorService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/aidlServer/AidlServerService$1;->this$0:Lcom/aidlServer/AidlServerService;

    iget-object v0, v0, Lcom/aidlServer/AidlServerService;->TAG:Ljava/lang/String;

    const-string v1, "onServiceConnected: null != mSensorService!!!!!!!!!!!! "

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/aidlServer/AidlServerService$1;->this$0:Lcom/aidlServer/AidlServerService;

    iget-object v0, v0, Lcom/aidlServer/AidlServerService;->TAG:Ljava/lang/String;

    const-string v1, "onServiceConnected: null == mSensorService!!!!!!!!!!!! "

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;

    iget-object v0, p0, Lcom/aidlServer/AidlServerService$1;->this$0:Lcom/aidlServer/AidlServerService;

    iget-object v0, v0, Lcom/aidlServer/AidlServerService;->TAG:Ljava/lang/String;

    const-string v1, "onServiceDisconnected "

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
