.class public Lcom/android/quicksearchbox/ListSuggestionCursorNoDuplicates;
.super Lcom/android/quicksearchbox/ListSuggestionCursor;
.source "ListSuggestionCursorNoDuplicates.java"


# instance fields
.field private final mSuggestionKeys:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/quicksearchbox/ListSuggestionCursor;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/quicksearchbox/ListSuggestionCursorNoDuplicates;->mSuggestionKeys:Ljava/util/HashSet;

    return-void
.end method


# virtual methods
.method public add(Lcom/android/quicksearchbox/Suggestion;)Z
    .locals 2
    .param p1    # Lcom/android/quicksearchbox/Suggestion;

    invoke-static {p1}, Lcom/android/quicksearchbox/SuggestionUtils;->getSuggestionKey(Lcom/android/quicksearchbox/Suggestion;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/quicksearchbox/ListSuggestionCursorNoDuplicates;->mSuggestionKeys:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-super {p0, p1}, Lcom/android/quicksearchbox/ListSuggestionCursor;->add(Lcom/android/quicksearchbox/Suggestion;)Z

    move-result v1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
