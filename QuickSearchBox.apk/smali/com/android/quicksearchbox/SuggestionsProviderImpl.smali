.class public Lcom/android/quicksearchbox/SuggestionsProviderImpl;
.super Ljava/lang/Object;
.source "SuggestionsProviderImpl.java"

# interfaces
.implements Lcom/android/quicksearchbox/SuggestionsProvider;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/quicksearchbox/SuggestionsProviderImpl$SuggestionCursorReceiver;
    }
.end annotation


# instance fields
.field private mBatchingExecutor:Lcom/android/quicksearchbox/util/BatchingNamedTaskExecutor;

.field private final mConfig:Lcom/android/quicksearchbox/Config;

.field private final mLogger:Lcom/android/quicksearchbox/Logger;

.field private final mPublishThread:Landroid/os/Handler;

.field private final mQueryExecutor:Lcom/android/quicksearchbox/util/NamedTaskExecutor;

.field private final mShouldQueryStrategy:Lcom/android/quicksearchbox/ShouldQueryStrategy;


# direct methods
.method public constructor <init>(Lcom/android/quicksearchbox/Config;Lcom/android/quicksearchbox/util/NamedTaskExecutor;Landroid/os/Handler;Lcom/android/quicksearchbox/Logger;)V
    .locals 2
    .param p1    # Lcom/android/quicksearchbox/Config;
    .param p2    # Lcom/android/quicksearchbox/util/NamedTaskExecutor;
    .param p3    # Landroid/os/Handler;
    .param p4    # Lcom/android/quicksearchbox/Logger;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/quicksearchbox/SuggestionsProviderImpl;->mConfig:Lcom/android/quicksearchbox/Config;

    iput-object p2, p0, Lcom/android/quicksearchbox/SuggestionsProviderImpl;->mQueryExecutor:Lcom/android/quicksearchbox/util/NamedTaskExecutor;

    iput-object p3, p0, Lcom/android/quicksearchbox/SuggestionsProviderImpl;->mPublishThread:Landroid/os/Handler;

    iput-object p4, p0, Lcom/android/quicksearchbox/SuggestionsProviderImpl;->mLogger:Lcom/android/quicksearchbox/Logger;

    new-instance v0, Lcom/android/quicksearchbox/ShouldQueryStrategy;

    iget-object v1, p0, Lcom/android/quicksearchbox/SuggestionsProviderImpl;->mConfig:Lcom/android/quicksearchbox/Config;

    invoke-direct {v0, v1}, Lcom/android/quicksearchbox/ShouldQueryStrategy;-><init>(Lcom/android/quicksearchbox/Config;)V

    iput-object v0, p0, Lcom/android/quicksearchbox/SuggestionsProviderImpl;->mShouldQueryStrategy:Lcom/android/quicksearchbox/ShouldQueryStrategy;

    return-void
.end method

.method static synthetic access$100(Lcom/android/quicksearchbox/SuggestionsProviderImpl;Lcom/android/quicksearchbox/CorpusResult;)V
    .locals 0
    .param p0    # Lcom/android/quicksearchbox/SuggestionsProviderImpl;
    .param p1    # Lcom/android/quicksearchbox/CorpusResult;

    invoke-direct {p0, p1}, Lcom/android/quicksearchbox/SuggestionsProviderImpl;->updateShouldQueryStrategy(Lcom/android/quicksearchbox/CorpusResult;)V

    return-void
.end method

.method static synthetic access$200(Lcom/android/quicksearchbox/SuggestionsProviderImpl;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/quicksearchbox/SuggestionsProviderImpl;

    iget-object v0, p0, Lcom/android/quicksearchbox/SuggestionsProviderImpl;->mPublishThread:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/android/quicksearchbox/SuggestionsProviderImpl;)Lcom/android/quicksearchbox/Logger;
    .locals 1
    .param p0    # Lcom/android/quicksearchbox/SuggestionsProviderImpl;

    iget-object v0, p0, Lcom/android/quicksearchbox/SuggestionsProviderImpl;->mLogger:Lcom/android/quicksearchbox/Logger;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/quicksearchbox/SuggestionsProviderImpl;)Lcom/android/quicksearchbox/Config;
    .locals 1
    .param p0    # Lcom/android/quicksearchbox/SuggestionsProviderImpl;

    iget-object v0, p0, Lcom/android/quicksearchbox/SuggestionsProviderImpl;->mConfig:Lcom/android/quicksearchbox/Config;

    return-object v0
.end method

.method private cancelPendingTasks()V
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/SuggestionsProviderImpl;->mBatchingExecutor:Lcom/android/quicksearchbox/util/BatchingNamedTaskExecutor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/quicksearchbox/SuggestionsProviderImpl;->mBatchingExecutor:Lcom/android/quicksearchbox/util/BatchingNamedTaskExecutor;

    invoke-virtual {v0}, Lcom/android/quicksearchbox/util/BatchingNamedTaskExecutor;->cancelPendingTasks()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/quicksearchbox/SuggestionsProviderImpl;->mBatchingExecutor:Lcom/android/quicksearchbox/util/BatchingNamedTaskExecutor;

    :cond_0
    return-void
.end method

.method private countDefaultCorpora(Ljava/util/List;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/android/quicksearchbox/Corpus;",
            ">;)I"
        }
    .end annotation

    const/4 v1, 0x0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/quicksearchbox/Corpus;

    invoke-interface {v0}, Lcom/android/quicksearchbox/Corpus;->isCorpusDefaultEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method

.method private filterCorpora(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 5
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/android/quicksearchbox/Corpus;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/android/quicksearchbox/Corpus;",
            ">;"
        }
    .end annotation

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-gt v3, v4, :cond_0

    :goto_0
    return-object p2

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/quicksearchbox/Corpus;

    invoke-virtual {p0, v1, p1}, Lcom/android/quicksearchbox/SuggestionsProviderImpl;->shouldQueryCorpus(Lcom/android/quicksearchbox/Corpus;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move-object p2, v0

    goto :goto_0
.end method

.method private shouldDisplayResults(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/quicksearchbox/SuggestionsProviderImpl;->mConfig:Lcom/android/quicksearchbox/Config;

    invoke-virtual {v0}, Lcom/android/quicksearchbox/Config;->showSuggestionsForZeroQuery()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private updateShouldQueryStrategy(Lcom/android/quicksearchbox/CorpusResult;)V
    .locals 3
    .param p1    # Lcom/android/quicksearchbox/CorpusResult;

    invoke-interface {p1}, Lcom/android/quicksearchbox/CorpusResult;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/quicksearchbox/SuggestionsProviderImpl;->mShouldQueryStrategy:Lcom/android/quicksearchbox/ShouldQueryStrategy;

    invoke-interface {p1}, Lcom/android/quicksearchbox/CorpusResult;->getCorpus()Lcom/android/quicksearchbox/Corpus;

    move-result-object v1

    invoke-interface {p1}, Lcom/android/quicksearchbox/CorpusResult;->getUserQuery()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/android/quicksearchbox/ShouldQueryStrategy;->onZeroResults(Lcom/android/quicksearchbox/Corpus;Ljava/lang/String;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public close()V
    .locals 0

    invoke-direct {p0}, Lcom/android/quicksearchbox/SuggestionsProviderImpl;->cancelPendingTasks()V

    return-void
.end method

.method public getSuggestions(Ljava/lang/String;Ljava/util/List;)Lcom/android/quicksearchbox/Suggestions;
    .locals 16
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/android/quicksearchbox/Corpus;",
            ">;)",
            "Lcom/android/quicksearchbox/Suggestions;"
        }
    .end annotation

    invoke-direct/range {p0 .. p2}, Lcom/android/quicksearchbox/SuggestionsProviderImpl;->filterCorpora(Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object p2

    new-instance v5, Lcom/android/quicksearchbox/Suggestions;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {v5, v0, v1}, Lcom/android/quicksearchbox/Suggestions;-><init>(Ljava/lang/String;Ljava/util/List;)V

    const-string v3, "QSB.SuggestionsProviderImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "chars:"

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v9, ",corpora:"

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_0

    :goto_0
    return-object v5

    :cond_0
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/android/quicksearchbox/SuggestionsProviderImpl;->countDefaultCorpora(Ljava/util/List;)I

    move-result v6

    if-nez v6, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/quicksearchbox/SuggestionsProviderImpl;->mConfig:Lcom/android/quicksearchbox/Config;

    invoke-virtual {v3}, Lcom/android/quicksearchbox/Config;->getNumPromotedSources()I

    move-result v6

    :cond_1
    new-instance v3, Lcom/android/quicksearchbox/util/BatchingNamedTaskExecutor;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/quicksearchbox/SuggestionsProviderImpl;->mQueryExecutor:Lcom/android/quicksearchbox/util/NamedTaskExecutor;

    invoke-direct {v3, v4}, Lcom/android/quicksearchbox/util/BatchingNamedTaskExecutor;-><init>(Lcom/android/quicksearchbox/util/NamedTaskExecutor;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/android/quicksearchbox/SuggestionsProviderImpl;->mBatchingExecutor:Lcom/android/quicksearchbox/util/BatchingNamedTaskExecutor;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/quicksearchbox/SuggestionsProviderImpl;->mConfig:Lcom/android/quicksearchbox/Config;

    invoke-virtual {v3}, Lcom/android/quicksearchbox/Config;->getPublishResultDelayMillis()J

    move-result-wide v7

    invoke-direct/range {p0 .. p1}, Lcom/android/quicksearchbox/SuggestionsProviderImpl;->shouldDisplayResults(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    new-instance v2, Lcom/android/quicksearchbox/SuggestionsProviderImpl$SuggestionCursorReceiver;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/android/quicksearchbox/SuggestionsProviderImpl;->mBatchingExecutor:Lcom/android/quicksearchbox/util/BatchingNamedTaskExecutor;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v8}, Lcom/android/quicksearchbox/SuggestionsProviderImpl$SuggestionCursorReceiver;-><init>(Lcom/android/quicksearchbox/SuggestionsProviderImpl;Lcom/android/quicksearchbox/util/BatchingNamedTaskExecutor;Lcom/android/quicksearchbox/Suggestions;IJ)V

    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/quicksearchbox/SuggestionsProviderImpl;->mConfig:Lcom/android/quicksearchbox/Config;

    invoke-virtual {v3}, Lcom/android/quicksearchbox/Config;->getMaxResultsPerSource()I

    move-result v10

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/android/quicksearchbox/SuggestionsProviderImpl;->mBatchingExecutor:Lcom/android/quicksearchbox/util/BatchingNamedTaskExecutor;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/android/quicksearchbox/SuggestionsProviderImpl;->mPublishThread:Landroid/os/Handler;

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    const/4 v15, 0x1

    :goto_2
    move-object/from16 v9, p1

    move-object/from16 v11, p2

    move-object v14, v2

    invoke-static/range {v9 .. v15}, Lcom/android/quicksearchbox/QueryTask;->startQueries(Ljava/lang/String;ILjava/lang/Iterable;Lcom/android/quicksearchbox/util/NamedTaskExecutor;Landroid/os/Handler;Lcom/android/quicksearchbox/util/Consumer;Z)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/quicksearchbox/SuggestionsProviderImpl;->mBatchingExecutor:Lcom/android/quicksearchbox/util/BatchingNamedTaskExecutor;

    invoke-virtual {v3, v6}, Lcom/android/quicksearchbox/util/BatchingNamedTaskExecutor;->executeNextBatch(I)V

    goto :goto_0

    :cond_2
    new-instance v2, Lcom/android/quicksearchbox/util/NoOpConsumer;

    invoke-direct {v2}, Lcom/android/quicksearchbox/util/NoOpConsumer;-><init>()V

    invoke-virtual {v5}, Lcom/android/quicksearchbox/Suggestions;->done()V

    goto :goto_1

    :cond_3
    const/4 v15, 0x0

    goto :goto_2
.end method

.method protected shouldQueryCorpus(Lcom/android/quicksearchbox/Corpus;Ljava/lang/String;)Z
    .locals 1
    .param p1    # Lcom/android/quicksearchbox/Corpus;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/quicksearchbox/SuggestionsProviderImpl;->mShouldQueryStrategy:Lcom/android/quicksearchbox/ShouldQueryStrategy;

    invoke-virtual {v0, p1, p2}, Lcom/android/quicksearchbox/ShouldQueryStrategy;->shouldQueryCorpus(Lcom/android/quicksearchbox/Corpus;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
