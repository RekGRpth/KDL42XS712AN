.class public Lcom/android/quicksearchbox/SearchActivity;
.super Landroid/app/Activity;
.source "SearchActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/quicksearchbox/SearchActivity$OnDestroyListener;,
        Lcom/android/quicksearchbox/SearchActivity$CorporaObserver;,
        Lcom/android/quicksearchbox/SearchActivity$CorpusSelectorDismissListener;,
        Lcom/android/quicksearchbox/SearchActivity$ClickHandler;
    }
.end annotation


# instance fields
.field private mAppSearchData:Landroid/os/Bundle;

.field private mCorporaObserver:Lcom/android/quicksearchbox/SearchActivity$CorporaObserver;

.field private mDestroyListener:Lcom/android/quicksearchbox/SearchActivity$OnDestroyListener;

.field private final mHandler:Landroid/os/Handler;

.field private mOnCreateLatency:I

.field private mOnCreateTracker:Lcom/android/quicksearchbox/LatencyTracker;

.field private mSearchActivityView:Lcom/android/quicksearchbox/ui/SearchActivityView;

.field private final mShowInputMethodTask:Ljava/lang/Runnable;

.field private mStartLatencyTracker:Lcom/android/quicksearchbox/LatencyTracker;

.field private mStarting:Z

.field private mTookAction:Z

.field private mTraceStartUp:Z

.field private final mUpdateSuggestionsTask:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/quicksearchbox/SearchActivity;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/android/quicksearchbox/SearchActivity$1;

    invoke-direct {v0, p0}, Lcom/android/quicksearchbox/SearchActivity$1;-><init>(Lcom/android/quicksearchbox/SearchActivity;)V

    iput-object v0, p0, Lcom/android/quicksearchbox/SearchActivity;->mUpdateSuggestionsTask:Ljava/lang/Runnable;

    new-instance v0, Lcom/android/quicksearchbox/SearchActivity$2;

    invoke-direct {v0, p0}, Lcom/android/quicksearchbox/SearchActivity$2;-><init>(Lcom/android/quicksearchbox/SearchActivity;)V

    iput-object v0, p0, Lcom/android/quicksearchbox/SearchActivity;->mShowInputMethodTask:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/android/quicksearchbox/SearchActivity;)Lcom/android/quicksearchbox/ui/SearchActivityView;
    .locals 1
    .param p0    # Lcom/android/quicksearchbox/SearchActivity;

    iget-object v0, p0, Lcom/android/quicksearchbox/SearchActivity;->mSearchActivityView:Lcom/android/quicksearchbox/ui/SearchActivityView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/quicksearchbox/SearchActivity;)V
    .locals 0
    .param p0    # Lcom/android/quicksearchbox/SearchActivity;

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->updateSuggestionsBuffered()V

    return-void
.end method

.method static synthetic access$500(Lcom/android/quicksearchbox/SearchActivity;Lcom/android/quicksearchbox/ui/SuggestionsAdapter;J)Z
    .locals 1
    .param p0    # Lcom/android/quicksearchbox/SearchActivity;
    .param p1    # Lcom/android/quicksearchbox/ui/SuggestionsAdapter;
    .param p2    # J

    invoke-direct {p0, p1, p2, p3}, Lcom/android/quicksearchbox/SearchActivity;->launchSuggestion(Lcom/android/quicksearchbox/ui/SuggestionsAdapter;J)Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/android/quicksearchbox/SearchActivity;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/android/quicksearchbox/SearchActivity;

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getCorpusName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/quicksearchbox/SearchActivity;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/android/quicksearchbox/SearchActivity;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/quicksearchbox/SearchActivity;->setCorpus(Ljava/lang/String;)V

    return-void
.end method

.method private getConfig()Lcom/android/quicksearchbox/Config;
    .locals 1

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getQsbApplication()Lcom/android/quicksearchbox/QsbApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/quicksearchbox/QsbApplication;->getConfig()Lcom/android/quicksearchbox/Config;

    move-result-object v0

    return-object v0
.end method

.method private getCorpora()Lcom/android/quicksearchbox/Corpora;
    .locals 1

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getQsbApplication()Lcom/android/quicksearchbox/QsbApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/quicksearchbox/QsbApplication;->getCorpora()Lcom/android/quicksearchbox/Corpora;

    move-result-object v0

    return-object v0
.end method

.method private getCorporaToQuery(Lcom/android/quicksearchbox/util/Consumer;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/quicksearchbox/util/Consumer",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/android/quicksearchbox/Corpus;",
            ">;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getCorpus()Lcom/android/quicksearchbox/Corpus;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getCorpusRanker()Lcom/android/quicksearchbox/CorpusRanker;

    move-result-object v3

    iget-object v4, p0, Lcom/android/quicksearchbox/SearchActivity;->mHandler:Landroid/os/Handler;

    invoke-static {v4, p1}, Lcom/android/quicksearchbox/util/Consumers;->createAsyncConsumer(Landroid/os/Handler;Lcom/android/quicksearchbox/util/Consumer;)Lcom/android/quicksearchbox/util/Consumer;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/android/quicksearchbox/CorpusRanker;->getCorporaInAll(Lcom/android/quicksearchbox/util/Consumer;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/android/quicksearchbox/SearchActivity;->getSearchCorpus()Lcom/android/quicksearchbox/Corpus;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-interface {p1, v0}, Lcom/android/quicksearchbox/util/Consumer;->consume(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private getCorpus()Lcom/android/quicksearchbox/Corpus;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/SearchActivity;->mSearchActivityView:Lcom/android/quicksearchbox/ui/SearchActivityView;

    invoke-virtual {v0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->getCorpus()Lcom/android/quicksearchbox/Corpus;

    move-result-object v0

    return-object v0
.end method

.method private getCorpusName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/SearchActivity;->mSearchActivityView:Lcom/android/quicksearchbox/ui/SearchActivityView;

    invoke-virtual {v0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->getCorpusName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getCorpusNameFromUri(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .param p1    # Landroid/net/Uri;

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-string v1, "qsb.corpus"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getCorpusRanker()Lcom/android/quicksearchbox/CorpusRanker;
    .locals 1

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getQsbApplication()Lcom/android/quicksearchbox/QsbApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/quicksearchbox/QsbApplication;->getCorpusRanker()Lcom/android/quicksearchbox/CorpusRanker;

    move-result-object v0

    return-object v0
.end method

.method private getLogger()Lcom/android/quicksearchbox/Logger;
    .locals 1

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getQsbApplication()Lcom/android/quicksearchbox/QsbApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/quicksearchbox/QsbApplication;->getLogger()Lcom/android/quicksearchbox/Logger;

    move-result-object v0

    return-object v0
.end method

.method private getQsbApplication()Lcom/android/quicksearchbox/QsbApplication;
    .locals 1

    invoke-static {p0}, Lcom/android/quicksearchbox/QsbApplication;->get(Landroid/content/Context;)Lcom/android/quicksearchbox/QsbApplication;

    move-result-object v0

    return-object v0
.end method

.method private getShortcutRepository()Lcom/android/quicksearchbox/ShortcutRepository;
    .locals 1

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getQsbApplication()Lcom/android/quicksearchbox/QsbApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/quicksearchbox/QsbApplication;->getShortcutRepository()Lcom/android/quicksearchbox/ShortcutRepository;

    move-result-object v0

    return-object v0
.end method

.method private getSuggestionsProvider()Lcom/android/quicksearchbox/SuggestionsProvider;
    .locals 1

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getQsbApplication()Lcom/android/quicksearchbox/QsbApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/quicksearchbox/QsbApplication;->getSuggestionsProvider()Lcom/android/quicksearchbox/SuggestionsProvider;

    move-result-object v0

    return-object v0
.end method

.method private gotSuggestions(Lcom/android/quicksearchbox/Suggestions;)V
    .locals 6
    .param p1    # Lcom/android/quicksearchbox/Suggestions;

    iget-boolean v0, p0, Lcom/android/quicksearchbox/SearchActivity;->mStarting:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/quicksearchbox/SearchActivity;->mStarting:Z

    invoke-virtual {p0}, Lcom/android/quicksearchbox/SearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "source"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/android/quicksearchbox/SearchActivity;->mStartLatencyTracker:Lcom/android/quicksearchbox/LatencyTracker;

    invoke-virtual {v0}, Lcom/android/quicksearchbox/LatencyTracker;->getLatency()I

    move-result v2

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getLogger()Lcom/android/quicksearchbox/Logger;

    move-result-object v0

    iget v1, p0, Lcom/android/quicksearchbox/SearchActivity;->mOnCreateLatency:I

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getCorpus()Lcom/android/quicksearchbox/Corpus;

    move-result-object v4

    if-nez p1, :cond_1

    const/4 v5, 0x0

    :goto_0
    invoke-interface/range {v0 .. v5}, Lcom/android/quicksearchbox/Logger;->logStart(IILjava/lang/String;Lcom/android/quicksearchbox/Corpus;Ljava/util/List;)V

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getQsbApplication()Lcom/android/quicksearchbox/QsbApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/quicksearchbox/QsbApplication;->onStartupComplete()V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/android/quicksearchbox/Suggestions;->getExpectedCorpora()Ljava/util/List;

    move-result-object v5

    goto :goto_0
.end method

.method private launchSuggestion(Lcom/android/quicksearchbox/ui/SuggestionsAdapter;J)Z
    .locals 8
    .param p2    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/quicksearchbox/ui/SuggestionsAdapter",
            "<*>;J)Z"
        }
    .end annotation

    const/4 v7, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0, p1, p2, p3}, Lcom/android/quicksearchbox/SearchActivity;->getCurrentSuggestions(Lcom/android/quicksearchbox/ui/SuggestionsAdapter;J)Lcom/android/quicksearchbox/SuggestionPosition;

    move-result-object v6

    if-nez v6, :cond_0

    :goto_0
    return v5

    :cond_0
    iput-boolean v7, p0, Lcom/android/quicksearchbox/SearchActivity;->mTookAction:Z

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getLogger()Lcom/android/quicksearchbox/Logger;

    move-result-object v0

    invoke-virtual {v6}, Lcom/android/quicksearchbox/SuggestionPosition;->getCursor()Lcom/android/quicksearchbox/SuggestionCursor;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/quicksearchbox/SearchActivity;->getCurrentIncludedCorpora()Ljava/util/Set;

    move-result-object v4

    move-wide v1, p2

    invoke-interface/range {v0 .. v5}, Lcom/android/quicksearchbox/Logger;->logSuggestionClick(JLcom/android/quicksearchbox/SuggestionCursor;Ljava/util/Collection;I)V

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getShortcutRepository()Lcom/android/quicksearchbox/ShortcutRepository;

    move-result-object v0

    invoke-virtual {v6}, Lcom/android/quicksearchbox/SuggestionPosition;->getCursor()Lcom/android/quicksearchbox/SuggestionCursor;

    move-result-object v1

    invoke-virtual {v6}, Lcom/android/quicksearchbox/SuggestionPosition;->getPosition()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/android/quicksearchbox/ShortcutRepository;->reportClick(Lcom/android/quicksearchbox/SuggestionCursor;I)V

    invoke-virtual {v6}, Lcom/android/quicksearchbox/SuggestionPosition;->getCursor()Lcom/android/quicksearchbox/SuggestionCursor;

    move-result-object v0

    invoke-virtual {v6}, Lcom/android/quicksearchbox/SuggestionPosition;->getPosition()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/quicksearchbox/SearchActivity;->launchSuggestion(Lcom/android/quicksearchbox/SuggestionCursor;I)V

    move v5, v7

    goto :goto_0
.end method

.method private recordOnCreateDone()V
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/SearchActivity;->mOnCreateTracker:Lcom/android/quicksearchbox/LatencyTracker;

    invoke-virtual {v0}, Lcom/android/quicksearchbox/LatencyTracker;->getLatency()I

    move-result v0

    iput v0, p0, Lcom/android/quicksearchbox/SearchActivity;->mOnCreateLatency:I

    return-void
.end method

.method private recordStartTime()V
    .locals 1

    new-instance v0, Lcom/android/quicksearchbox/LatencyTracker;

    invoke-direct {v0}, Lcom/android/quicksearchbox/LatencyTracker;-><init>()V

    iput-object v0, p0, Lcom/android/quicksearchbox/SearchActivity;->mStartLatencyTracker:Lcom/android/quicksearchbox/LatencyTracker;

    new-instance v0, Lcom/android/quicksearchbox/LatencyTracker;

    invoke-direct {v0}, Lcom/android/quicksearchbox/LatencyTracker;-><init>()V

    iput-object v0, p0, Lcom/android/quicksearchbox/SearchActivity;->mOnCreateTracker:Lcom/android/quicksearchbox/LatencyTracker;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/quicksearchbox/SearchActivity;->mStarting:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/quicksearchbox/SearchActivity;->mTookAction:Z

    return-void
.end method

.method private setCorpus(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/quicksearchbox/SearchActivity;->mSearchActivityView:Lcom/android/quicksearchbox/ui/SearchActivityView;

    invoke-virtual {v0, p1}, Lcom/android/quicksearchbox/ui/SearchActivityView;->setCorpus(Ljava/lang/String;)V

    return-void
.end method

.method private setupFromIntent(Landroid/content/Intent;)V
    .locals 6
    .param p1    # Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/android/quicksearchbox/SearchActivity;->getCorpusNameFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    const-string v4, "query"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "app_data"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "select_query"

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    invoke-direct {p0, v1}, Lcom/android/quicksearchbox/SearchActivity;->setCorpus(Ljava/lang/String;)V

    invoke-virtual {p0, v2, v3}, Lcom/android/quicksearchbox/SearchActivity;->setQuery(Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/android/quicksearchbox/SearchActivity;->mAppSearchData:Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/android/quicksearchbox/SearchActivity;->startedIntoCorpusSelectionDialog()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/android/quicksearchbox/SearchActivity;->mSearchActivityView:Lcom/android/quicksearchbox/ui/SearchActivityView;

    invoke-virtual {v4}, Lcom/android/quicksearchbox/ui/SearchActivityView;->showCorpusSelectionDialog()V

    :cond_0
    return-void
.end method

.method private updateSuggestionsBuffered()V
    .locals 4

    iget-object v2, p0, Lcom/android/quicksearchbox/SearchActivity;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/android/quicksearchbox/SearchActivity;->mUpdateSuggestionsTask:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getConfig()Lcom/android/quicksearchbox/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/quicksearchbox/Config;->getTypingUpdateSuggestionsDelayMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/android/quicksearchbox/SearchActivity;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/android/quicksearchbox/SearchActivity;->mUpdateSuggestionsTask:Ljava/lang/Runnable;

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method


# virtual methods
.method public clearStartedIntoCorpusSelectionDialog()V
    .locals 4

    invoke-virtual {p0}, Lcom/android/quicksearchbox/SearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "com.android.quicksearchbox.action.QSB_AND_SELECT_CORPUS"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const-string v2, "android.search.action.GLOBAL_SEARCH"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/android/quicksearchbox/SearchActivity;->setIntent(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method protected clickedQuickContact(Lcom/android/quicksearchbox/ui/SuggestionsAdapter;J)V
    .locals 7
    .param p2    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/quicksearchbox/ui/SuggestionsAdapter",
            "<*>;J)V"
        }
    .end annotation

    invoke-virtual {p0, p1, p2, p3}, Lcom/android/quicksearchbox/SearchActivity;->getCurrentSuggestions(Lcom/android/quicksearchbox/ui/SuggestionsAdapter;J)Lcom/android/quicksearchbox/SuggestionPosition;

    move-result-object v6

    if-nez v6, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/quicksearchbox/SearchActivity;->mTookAction:Z

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getLogger()Lcom/android/quicksearchbox/Logger;

    move-result-object v0

    invoke-virtual {v6}, Lcom/android/quicksearchbox/SuggestionPosition;->getCursor()Lcom/android/quicksearchbox/SuggestionCursor;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/quicksearchbox/SearchActivity;->getCurrentIncludedCorpora()Ljava/util/Set;

    move-result-object v4

    const/4 v5, 0x2

    move-wide v1, p2

    invoke-interface/range {v0 .. v5}, Lcom/android/quicksearchbox/Logger;->logSuggestionClick(JLcom/android/quicksearchbox/SuggestionCursor;Ljava/util/Collection;I)V

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getShortcutRepository()Lcom/android/quicksearchbox/ShortcutRepository;

    move-result-object v0

    invoke-virtual {v6}, Lcom/android/quicksearchbox/SuggestionPosition;->getCursor()Lcom/android/quicksearchbox/SuggestionCursor;

    move-result-object v1

    invoke-virtual {v6}, Lcom/android/quicksearchbox/SuggestionPosition;->getPosition()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/android/quicksearchbox/ShortcutRepository;->reportClick(Lcom/android/quicksearchbox/SuggestionCursor;I)V

    goto :goto_0
.end method

.method protected createCorpusSelectionDialog()Lcom/android/quicksearchbox/CorpusSelectionDialog;
    .locals 2

    new-instance v0, Lcom/android/quicksearchbox/CorpusSelectionDialog;

    invoke-virtual {p0}, Lcom/android/quicksearchbox/SearchActivity;->getSettings()Lcom/android/quicksearchbox/SearchSettings;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/android/quicksearchbox/CorpusSelectionDialog;-><init>(Landroid/content/Context;Lcom/android/quicksearchbox/SearchSettings;)V

    return-object v0
.end method

.method public createMenuItems(Landroid/view/Menu;Z)V
    .locals 2
    .param p1    # Landroid/view/Menu;
    .param p2    # Z

    invoke-virtual {p0}, Lcom/android/quicksearchbox/SearchActivity;->getSettings()Lcom/android/quicksearchbox/SearchSettings;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/android/quicksearchbox/SearchSettings;->addMenuItems(Landroid/view/Menu;Z)V

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getQsbApplication()Lcom/android/quicksearchbox/QsbApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/quicksearchbox/QsbApplication;->getHelp()Lcom/android/quicksearchbox/Help;

    move-result-object v0

    const-string v1, "search"

    invoke-virtual {v0, p1, v1}, Lcom/android/quicksearchbox/Help;->addHelpMenuItem(Landroid/view/Menu;Ljava/lang/String;)V

    return-void
.end method

.method public getCorpusSelectionDialog()Lcom/android/quicksearchbox/CorpusSelectionDialog;
    .locals 3

    invoke-virtual {p0}, Lcom/android/quicksearchbox/SearchActivity;->createCorpusSelectionDialog()Lcom/android/quicksearchbox/CorpusSelectionDialog;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/android/quicksearchbox/CorpusSelectionDialog;->setOwnerActivity(Landroid/app/Activity;)V

    new-instance v1, Lcom/android/quicksearchbox/SearchActivity$CorpusSelectorDismissListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/android/quicksearchbox/SearchActivity$CorpusSelectorDismissListener;-><init>(Lcom/android/quicksearchbox/SearchActivity;Lcom/android/quicksearchbox/SearchActivity$1;)V

    invoke-virtual {v0, v1}, Lcom/android/quicksearchbox/CorpusSelectionDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    return-object v0
.end method

.method protected getCurrentIncludedCorpora()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/android/quicksearchbox/Corpus;",
            ">;"
        }
    .end annotation

    iget-object v1, p0, Lcom/android/quicksearchbox/SearchActivity;->mSearchActivityView:Lcom/android/quicksearchbox/ui/SearchActivityView;

    invoke-virtual {v1}, Lcom/android/quicksearchbox/ui/SearchActivityView;->getSuggestions()Lcom/android/quicksearchbox/Suggestions;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Lcom/android/quicksearchbox/Suggestions;->getIncludedCorpora()Ljava/util/Set;

    move-result-object v1

    goto :goto_0
.end method

.method protected getCurrentSuggestions()Lcom/android/quicksearchbox/SuggestionCursor;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/SearchActivity;->mSearchActivityView:Lcom/android/quicksearchbox/ui/SearchActivityView;

    invoke-virtual {v0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->getCurrentPromotedSuggestions()Lcom/android/quicksearchbox/SuggestionCursor;

    move-result-object v0

    return-object v0
.end method

.method protected getCurrentSuggestions(Lcom/android/quicksearchbox/ui/SuggestionsAdapter;J)Lcom/android/quicksearchbox/SuggestionPosition;
    .locals 8
    .param p2    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/quicksearchbox/ui/SuggestionsAdapter",
            "<*>;J)",
            "Lcom/android/quicksearchbox/SuggestionPosition;"
        }
    .end annotation

    const/4 v4, 0x0

    invoke-interface {p1, p2, p3}, Lcom/android/quicksearchbox/ui/SuggestionsAdapter;->getSuggestion(J)Lcom/android/quicksearchbox/SuggestionPosition;

    move-result-object v1

    if-nez v1, :cond_0

    move-object v1, v4

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v1}, Lcom/android/quicksearchbox/SuggestionPosition;->getCursor()Lcom/android/quicksearchbox/SuggestionCursor;

    move-result-object v3

    invoke-virtual {v1}, Lcom/android/quicksearchbox/SuggestionPosition;->getPosition()I

    move-result v2

    if-nez v3, :cond_1

    move-object v1, v4

    goto :goto_0

    :cond_1
    invoke-interface {v3}, Lcom/android/quicksearchbox/SuggestionCursor;->getCount()I

    move-result v0

    if-ltz v2, :cond_2

    if-lt v2, v0, :cond_3

    :cond_2
    const-string v5, "QSB.SearchActivity"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Invalid suggestion position "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", count = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v4

    goto :goto_0

    :cond_3
    invoke-interface {v3, v2}, Lcom/android/quicksearchbox/SuggestionCursor;->moveTo(I)V

    goto :goto_0
.end method

.method protected getQuery()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/SearchActivity;->mSearchActivityView:Lcom/android/quicksearchbox/ui/SearchActivityView;

    invoke-virtual {v0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->getQuery()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getSearchCorpus()Lcom/android/quicksearchbox/Corpus;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/SearchActivity;->mSearchActivityView:Lcom/android/quicksearchbox/ui/SearchActivityView;

    invoke-virtual {v0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->getSearchCorpus()Lcom/android/quicksearchbox/Corpus;

    move-result-object v0

    return-object v0
.end method

.method protected getSettings()Lcom/android/quicksearchbox/SearchSettings;
    .locals 1

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getQsbApplication()Lcom/android/quicksearchbox/QsbApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/quicksearchbox/QsbApplication;->getSettings()Lcom/android/quicksearchbox/SearchSettings;

    move-result-object v0

    return-object v0
.end method

.method protected getShortcutsForQuery(Ljava/lang/String;Ljava/util/Collection;Lcom/android/quicksearchbox/Suggestions;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p3    # Lcom/android/quicksearchbox/Suggestions;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/quicksearchbox/Corpus;",
            ">;",
            "Lcom/android/quicksearchbox/Suggestions;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getShortcutRepository()Lcom/android/quicksearchbox/ShortcutRepository;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_2

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getConfig()Lcom/android/quicksearchbox/Config;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/quicksearchbox/Config;->showShortcutsForZeroQuery()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    iget-object v2, p0, Lcom/android/quicksearchbox/SearchActivity;->mHandler:Landroid/os/Handler;

    new-instance v3, Lcom/android/quicksearchbox/SearchActivity$8;

    invoke-direct {v3, p0, p3}, Lcom/android/quicksearchbox/SearchActivity$8;-><init>(Lcom/android/quicksearchbox/SearchActivity;Lcom/android/quicksearchbox/Suggestions;)V

    invoke-static {v2, v3}, Lcom/android/quicksearchbox/util/Consumers;->createAsyncCloseableConsumer(Landroid/os/Handler;Lcom/android/quicksearchbox/util/Consumer;)Lcom/android/quicksearchbox/util/Consumer;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/quicksearchbox/SearchActivity;->getSettings()Lcom/android/quicksearchbox/SearchSettings;

    move-result-object v2

    invoke-interface {v2}, Lcom/android/quicksearchbox/SearchSettings;->allowWebSearchShortcuts()Z

    move-result v2

    invoke-interface {v1, p1, p2, v2, v0}, Lcom/android/quicksearchbox/ShortcutRepository;->getShortcutsForQuery(Ljava/lang/String;Ljava/util/Collection;ZLcom/android/quicksearchbox/util/Consumer;)V

    goto :goto_0
.end method

.method protected launchIntent(Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Intent;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/android/quicksearchbox/SearchActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "QSB.SearchActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to start "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected launchSuggestion(Lcom/android/quicksearchbox/SuggestionCursor;I)V
    .locals 2
    .param p1    # Lcom/android/quicksearchbox/SuggestionCursor;
    .param p2    # I

    invoke-interface {p1, p2}, Lcom/android/quicksearchbox/SuggestionCursor;->moveTo(I)V

    iget-object v1, p0, Lcom/android/quicksearchbox/SearchActivity;->mAppSearchData:Landroid/os/Bundle;

    invoke-static {p1, v1}, Lcom/android/quicksearchbox/SuggestionUtils;->getSuggestionIntent(Lcom/android/quicksearchbox/SuggestionCursor;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/quicksearchbox/SearchActivity;->launchIntent(Landroid/content/Intent;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/android/quicksearchbox/SearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "trace_start_up"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/android/quicksearchbox/SearchActivity;->mTraceStartUp:Z

    iget-boolean v3, p0, Lcom/android/quicksearchbox/SearchActivity;->mTraceStartUp:Z

    if-eqz v3, :cond_0

    new-instance v3, Ljava/io/File;

    const-string v4, "traces"

    const/4 v5, 0x0

    invoke-virtual {p0, v4, v5}, Lcom/android/quicksearchbox/SearchActivity;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v4

    const-string v5, "qsb-start.trace"

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    const-string v3, "QSB.SearchActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Writing start-up trace to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v2}, Landroid/os/Debug;->startMethodTracing(Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->recordStartTime()V

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Lcom/android/quicksearchbox/QsbApplication;->get(Landroid/content/Context;)Lcom/android/quicksearchbox/QsbApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/quicksearchbox/QsbApplication;->getSearchBaseUrlHelper()Lcom/android/quicksearchbox/google/SearchBaseUrlHelper;

    invoke-virtual {p0}, Lcom/android/quicksearchbox/SearchActivity;->setupContentView()Lcom/android/quicksearchbox/ui/SearchActivityView;

    move-result-object v3

    iput-object v3, p0, Lcom/android/quicksearchbox/SearchActivity;->mSearchActivityView:Lcom/android/quicksearchbox/ui/SearchActivityView;

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getConfig()Lcom/android/quicksearchbox/Config;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/quicksearchbox/Config;->showScrollingSuggestions()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/android/quicksearchbox/SearchActivity;->mSearchActivityView:Lcom/android/quicksearchbox/ui/SearchActivityView;

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getConfig()Lcom/android/quicksearchbox/Config;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/quicksearchbox/Config;->getMaxPromotedSuggestions()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/android/quicksearchbox/ui/SearchActivityView;->setMaxPromotedSuggestions(I)V

    :goto_0
    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getConfig()Lcom/android/quicksearchbox/Config;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/quicksearchbox/Config;->showScrollingResults()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/android/quicksearchbox/SearchActivity;->mSearchActivityView:Lcom/android/quicksearchbox/ui/SearchActivityView;

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getConfig()Lcom/android/quicksearchbox/Config;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/quicksearchbox/Config;->getMaxPromotedResults()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/android/quicksearchbox/ui/SearchActivityView;->setMaxPromotedResults(I)V

    :goto_1
    iget-object v3, p0, Lcom/android/quicksearchbox/SearchActivity;->mSearchActivityView:Lcom/android/quicksearchbox/ui/SearchActivityView;

    new-instance v4, Lcom/android/quicksearchbox/SearchActivity$3;

    invoke-direct {v4, p0}, Lcom/android/quicksearchbox/SearchActivity$3;-><init>(Lcom/android/quicksearchbox/SearchActivity;)V

    invoke-virtual {v3, v4}, Lcom/android/quicksearchbox/ui/SearchActivityView;->setSearchClickListener(Lcom/android/quicksearchbox/ui/SearchActivityView$SearchClickListener;)V

    iget-object v3, p0, Lcom/android/quicksearchbox/SearchActivity;->mSearchActivityView:Lcom/android/quicksearchbox/ui/SearchActivityView;

    new-instance v4, Lcom/android/quicksearchbox/SearchActivity$4;

    invoke-direct {v4, p0}, Lcom/android/quicksearchbox/SearchActivity$4;-><init>(Lcom/android/quicksearchbox/SearchActivity;)V

    invoke-virtual {v3, v4}, Lcom/android/quicksearchbox/ui/SearchActivityView;->setQueryListener(Lcom/android/quicksearchbox/ui/SearchActivityView$QueryListener;)V

    iget-object v3, p0, Lcom/android/quicksearchbox/SearchActivity;->mSearchActivityView:Lcom/android/quicksearchbox/ui/SearchActivityView;

    new-instance v4, Lcom/android/quicksearchbox/SearchActivity$ClickHandler;

    invoke-direct {v4, p0, v6}, Lcom/android/quicksearchbox/SearchActivity$ClickHandler;-><init>(Lcom/android/quicksearchbox/SearchActivity;Lcom/android/quicksearchbox/SearchActivity$1;)V

    invoke-virtual {v3, v4}, Lcom/android/quicksearchbox/ui/SearchActivityView;->setSuggestionClickListener(Lcom/android/quicksearchbox/ui/SuggestionClickListener;)V

    iget-object v3, p0, Lcom/android/quicksearchbox/SearchActivity;->mSearchActivityView:Lcom/android/quicksearchbox/ui/SearchActivityView;

    new-instance v4, Lcom/android/quicksearchbox/SearchActivity$5;

    invoke-direct {v4, p0}, Lcom/android/quicksearchbox/SearchActivity$5;-><init>(Lcom/android/quicksearchbox/SearchActivity;)V

    invoke-virtual {v3, v4}, Lcom/android/quicksearchbox/ui/SearchActivityView;->setVoiceSearchButtonClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/android/quicksearchbox/SearchActivity$6;

    invoke-direct {v0, p0}, Lcom/android/quicksearchbox/SearchActivity$6;-><init>(Lcom/android/quicksearchbox/SearchActivity;)V

    iget-object v3, p0, Lcom/android/quicksearchbox/SearchActivity;->mSearchActivityView:Lcom/android/quicksearchbox/ui/SearchActivityView;

    invoke-virtual {v3, v0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->setExitClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/android/quicksearchbox/SearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/android/quicksearchbox/SearchActivity;->setupFromIntent(Landroid/content/Intent;)V

    invoke-virtual {p0, p1}, Lcom/android/quicksearchbox/SearchActivity;->restoreInstanceState(Landroid/os/Bundle;)V

    iget-object v3, p0, Lcom/android/quicksearchbox/SearchActivity;->mSearchActivityView:Lcom/android/quicksearchbox/ui/SearchActivityView;

    invoke-virtual {v3}, Lcom/android/quicksearchbox/ui/SearchActivityView;->start()V

    new-instance v3, Lcom/android/quicksearchbox/SearchActivity$CorporaObserver;

    invoke-direct {v3, p0, v6}, Lcom/android/quicksearchbox/SearchActivity$CorporaObserver;-><init>(Lcom/android/quicksearchbox/SearchActivity;Lcom/android/quicksearchbox/SearchActivity$1;)V

    iput-object v3, p0, Lcom/android/quicksearchbox/SearchActivity;->mCorporaObserver:Lcom/android/quicksearchbox/SearchActivity$CorporaObserver;

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getCorpora()Lcom/android/quicksearchbox/Corpora;

    move-result-object v3

    iget-object v4, p0, Lcom/android/quicksearchbox/SearchActivity;->mCorporaObserver:Lcom/android/quicksearchbox/SearchActivity$CorporaObserver;

    invoke-interface {v3, v4}, Lcom/android/quicksearchbox/Corpora;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->recordOnCreateDone()V

    return-void

    :cond_1
    iget-object v3, p0, Lcom/android/quicksearchbox/SearchActivity;->mSearchActivityView:Lcom/android/quicksearchbox/ui/SearchActivityView;

    invoke-virtual {v3}, Lcom/android/quicksearchbox/ui/SearchActivityView;->limitSuggestionsToViewHeight()V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/android/quicksearchbox/SearchActivity;->mSearchActivityView:Lcom/android/quicksearchbox/ui/SearchActivityView;

    invoke-virtual {v3}, Lcom/android/quicksearchbox/ui/SearchActivityView;->limitResultsToViewHeight()V

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 2

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getCorpora()Lcom/android/quicksearchbox/Corpora;

    move-result-object v0

    iget-object v1, p0, Lcom/android/quicksearchbox/SearchActivity;->mCorporaObserver:Lcom/android/quicksearchbox/SearchActivity$CorporaObserver;

    invoke-interface {v0, v1}, Lcom/android/quicksearchbox/Corpora;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    iget-object v0, p0, Lcom/android/quicksearchbox/SearchActivity;->mSearchActivityView:Lcom/android/quicksearchbox/ui/SearchActivityView;

    invoke-virtual {v0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->destroy()V

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-object v0, p0, Lcom/android/quicksearchbox/SearchActivity;->mDestroyListener:Lcom/android/quicksearchbox/SearchActivity$OnDestroyListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/quicksearchbox/SearchActivity;->mDestroyListener:Lcom/android/quicksearchbox/SearchActivity$OnDestroyListener;

    invoke-interface {v0}, Lcom/android/quicksearchbox/SearchActivity$OnDestroyListener;->onDestroyed()V

    :cond_0
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0
    .param p1    # Landroid/content/Intent;

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->recordStartTime()V

    invoke-virtual {p0, p1}, Lcom/android/quicksearchbox/SearchActivity;->setIntent(Landroid/content/Intent;)V

    invoke-direct {p0, p1}, Lcom/android/quicksearchbox/SearchActivity;->setupFromIntent(Landroid/content/Intent;)V

    return-void
.end method

.method protected onPause()V
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/SearchActivity;->mSearchActivityView:Lcom/android/quicksearchbox/ui/SearchActivityView;

    invoke-virtual {v0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->onPause()V

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1    # Landroid/view/Menu;

    const/4 v0, 0x1

    invoke-interface {p1}, Landroid/view/Menu;->clear()V

    invoke-virtual {p0, p1, v0}, Lcom/android/quicksearchbox/SearchActivity;->createMenuItems(Landroid/view/Menu;Z)V

    return v0
.end method

.method protected onRestart()V
    .locals 0

    invoke-super {p0}, Landroid/app/Activity;->onRestart()V

    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->updateSuggestionsBuffered()V

    iget-object v0, p0, Lcom/android/quicksearchbox/SearchActivity;->mSearchActivityView:Lcom/android/quicksearchbox/ui/SearchActivityView;

    invoke-virtual {v0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->onResume()V

    iget-boolean v0, p0, Lcom/android/quicksearchbox/SearchActivity;->mTraceStartUp:Z

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/Debug;->stopMethodTracing()V

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string v0, "corpus"

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getCorpusName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "query"

    invoke-virtual {p0}, Lcom/android/quicksearchbox/SearchActivity;->getQuery()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method protected onSearchClicked(I)Z
    .locals 7
    .param p1    # I

    const/4 v3, 0x1

    const/4 v2, 0x0

    sget-object v4, Lcom/google/common/base/CharMatcher;->WHITESPACE:Lcom/google/common/base/CharMatcher;

    invoke-virtual {p0}, Lcom/android/quicksearchbox/SearchActivity;->getQuery()Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x20

    invoke-virtual {v4, v5, v6}, Lcom/google/common/base/CharMatcher;->trimAndCollapseFrom(Ljava/lang/CharSequence;C)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-virtual {p0}, Lcom/android/quicksearchbox/SearchActivity;->getSearchCorpus()Lcom/android/quicksearchbox/Corpus;

    move-result-object v1

    if-eqz v1, :cond_0

    iput-boolean v3, p0, Lcom/android/quicksearchbox/SearchActivity;->mTookAction:Z

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getLogger()Lcom/android/quicksearchbox/Logger;

    move-result-object v2

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getCorpus()Lcom/android/quicksearchbox/Corpus;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-interface {v2, v4, p1, v5}, Lcom/android/quicksearchbox/Logger;->logSearch(Lcom/android/quicksearchbox/Corpus;II)V

    invoke-virtual {p0, v1, v0}, Lcom/android/quicksearchbox/SearchActivity;->startSearch(Lcom/android/quicksearchbox/Corpus;Ljava/lang/String;)V

    move v2, v3

    goto :goto_0
.end method

.method protected onStop()V
    .locals 3

    iget-boolean v0, p0, Lcom/android/quicksearchbox/SearchActivity;->mTookAction:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getLogger()Lcom/android/quicksearchbox/Logger;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/quicksearchbox/SearchActivity;->getCurrentSuggestions()Lcom/android/quicksearchbox/SuggestionCursor;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/quicksearchbox/SearchActivity;->getQuery()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/android/quicksearchbox/Logger;->logExit(Lcom/android/quicksearchbox/SuggestionCursor;I)V

    :cond_0
    iget-object v0, p0, Lcom/android/quicksearchbox/SearchActivity;->mSearchActivityView:Lcom/android/quicksearchbox/ui/SearchActivityView;

    invoke-virtual {v0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->clearSuggestions()V

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getQsbApplication()Lcom/android/quicksearchbox/QsbApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/quicksearchbox/QsbApplication;->getShortcutRefresher()Lcom/android/quicksearchbox/ShortcutRefresher;

    move-result-object v0

    invoke-interface {v0}, Lcom/android/quicksearchbox/ShortcutRefresher;->reset()V

    iget-object v0, p0, Lcom/android/quicksearchbox/SearchActivity;->mSearchActivityView:Lcom/android/quicksearchbox/ui/SearchActivityView;

    invoke-virtual {v0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->onStop()V

    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    return-void
.end method

.method protected onVoiceSearchClicked()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/quicksearchbox/SearchActivity;->getSearchCorpus()Lcom/android/quicksearchbox/Corpus;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/quicksearchbox/SearchActivity;->mTookAction:Z

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getLogger()Lcom/android/quicksearchbox/Logger;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/android/quicksearchbox/Logger;->logVoiceSearch(Lcom/android/quicksearchbox/Corpus;)V

    iget-object v2, p0, Lcom/android/quicksearchbox/SearchActivity;->mAppSearchData:Landroid/os/Bundle;

    invoke-interface {v1, v2}, Lcom/android/quicksearchbox/Corpus;->createVoiceSearchIntent(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/quicksearchbox/SearchActivity;->launchIntent(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 4
    .param p1    # Z

    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/quicksearchbox/SearchActivity;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/android/quicksearchbox/SearchActivity;->mShowInputMethodTask:Ljava/lang/Runnable;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    return-void
.end method

.method protected refineSuggestion(Lcom/android/quicksearchbox/ui/SuggestionsAdapter;J)V
    .locals 9
    .param p2    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/quicksearchbox/ui/SuggestionsAdapter",
            "<*>;J)V"
        }
    .end annotation

    invoke-virtual {p0, p1, p2, p3}, Lcom/android/quicksearchbox/SearchActivity;->getCurrentSuggestions(Lcom/android/quicksearchbox/ui/SuggestionsAdapter;J)Lcom/android/quicksearchbox/SuggestionPosition;

    move-result-object v8

    if-nez v8, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v8}, Lcom/android/quicksearchbox/SuggestionPosition;->getSuggestionQuery()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getLogger()Lcom/android/quicksearchbox/Logger;

    move-result-object v0

    invoke-virtual {v8}, Lcom/android/quicksearchbox/SuggestionPosition;->getCursor()Lcom/android/quicksearchbox/SuggestionCursor;

    move-result-object v3

    invoke-virtual {p0}, Lcom/android/quicksearchbox/SearchActivity;->getCurrentIncludedCorpora()Ljava/util/Set;

    move-result-object v4

    const/4 v5, 0x1

    move-wide v1, p2

    invoke-interface/range {v0 .. v5}, Lcom/android/quicksearchbox/Logger;->logSuggestionClick(JLcom/android/quicksearchbox/SuggestionCursor;Ljava/util/Collection;I)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v0, 0x0

    invoke-virtual {p0, v7, v0}, Lcom/android/quicksearchbox/SearchActivity;->setQuery(Ljava/lang/String;Z)V

    invoke-virtual {p0}, Lcom/android/quicksearchbox/SearchActivity;->updateSuggestions()V

    iget-object v0, p0, Lcom/android/quicksearchbox/SearchActivity;->mSearchActivityView:Lcom/android/quicksearchbox/ui/SearchActivityView;

    invoke-virtual {v0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->focusQueryTextView()V

    goto :goto_0
.end method

.method protected removeFromHistory(Lcom/android/quicksearchbox/SuggestionCursor;I)V
    .locals 1
    .param p1    # Lcom/android/quicksearchbox/SuggestionCursor;
    .param p2    # I

    invoke-virtual {p0, p1, p2}, Lcom/android/quicksearchbox/SearchActivity;->removeShortcut(Lcom/android/quicksearchbox/SuggestionCursor;I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/quicksearchbox/SearchActivity;->removeFromHistoryDone(Z)V

    return-void
.end method

.method protected removeFromHistory(Lcom/android/quicksearchbox/ui/SuggestionsAdapter;J)V
    .locals 3
    .param p2    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/quicksearchbox/ui/SuggestionsAdapter",
            "<*>;J)V"
        }
    .end annotation

    invoke-virtual {p0, p1, p2, p3}, Lcom/android/quicksearchbox/SearchActivity;->getCurrentSuggestions(Lcom/android/quicksearchbox/ui/SuggestionsAdapter;J)Lcom/android/quicksearchbox/SuggestionPosition;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/android/quicksearchbox/SuggestionPosition;->getCursor()Lcom/android/quicksearchbox/SuggestionCursor;

    move-result-object v1

    invoke-virtual {v0}, Lcom/android/quicksearchbox/SuggestionPosition;->getPosition()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/android/quicksearchbox/SearchActivity;->removeFromHistory(Lcom/android/quicksearchbox/SuggestionCursor;I)V

    goto :goto_0
.end method

.method protected removeFromHistoryClicked(Lcom/android/quicksearchbox/ui/SuggestionsAdapter;J)V
    .locals 6
    .param p2    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/quicksearchbox/ui/SuggestionsAdapter",
            "<*>;J)V"
        }
    .end annotation

    invoke-virtual {p0, p1, p2, p3}, Lcom/android/quicksearchbox/SearchActivity;->getCurrentSuggestions(Lcom/android/quicksearchbox/ui/SuggestionsAdapter;J)Lcom/android/quicksearchbox/SuggestionPosition;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v1}, Lcom/android/quicksearchbox/SuggestionPosition;->getSuggestionText1()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f090009    # com.android.quicksearchbox.R.string.remove_from_history

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x104000a    # android.R.string.ok

    new-instance v5, Lcom/android/quicksearchbox/SearchActivity$7;

    invoke-direct {v5, p0, p1, p2, p3}, Lcom/android/quicksearchbox/SearchActivity$7;-><init>(Lcom/android/quicksearchbox/SearchActivity;Lcom/android/quicksearchbox/ui/SuggestionsAdapter;J)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const/high16 v4, 0x1040000    # android.R.string.cancel

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method protected removeFromHistoryDone(Z)V
    .locals 3
    .param p1    # Z

    const-string v0, "QSB.SearchActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Removed query from history, success="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->updateSuggestionsBuffered()V

    if-nez p1, :cond_0

    const v0, 0x7f09000a    # com.android.quicksearchbox.R.string.remove_from_history_failed

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method

.method protected removeShortcut(Lcom/android/quicksearchbox/SuggestionCursor;I)V
    .locals 1
    .param p1    # Lcom/android/quicksearchbox/SuggestionCursor;
    .param p2    # I

    invoke-interface {p1}, Lcom/android/quicksearchbox/SuggestionCursor;->isSuggestionShortcut()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getShortcutRepository()Lcom/android/quicksearchbox/ShortcutRepository;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/android/quicksearchbox/ShortcutRepository;->removeFromHistory(Lcom/android/quicksearchbox/SuggestionCursor;I)V

    :cond_0
    return-void
.end method

.method protected restoreInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v2, "corpus"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "query"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0}, Lcom/android/quicksearchbox/SearchActivity;->setCorpus(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/android/quicksearchbox/SearchActivity;->setQuery(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public setOnDestroyListener(Lcom/android/quicksearchbox/SearchActivity$OnDestroyListener;)V
    .locals 0
    .param p1    # Lcom/android/quicksearchbox/SearchActivity$OnDestroyListener;

    iput-object p1, p0, Lcom/android/quicksearchbox/SearchActivity;->mDestroyListener:Lcom/android/quicksearchbox/SearchActivity$OnDestroyListener;

    return-void
.end method

.method protected setQuery(Ljava/lang/String;Z)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    iget-object v0, p0, Lcom/android/quicksearchbox/SearchActivity;->mSearchActivityView:Lcom/android/quicksearchbox/ui/SearchActivityView;

    invoke-virtual {v0, p1, p2}, Lcom/android/quicksearchbox/ui/SearchActivityView;->setQuery(Ljava/lang/String;Z)V

    return-void
.end method

.method protected setupContentView()Lcom/android/quicksearchbox/ui/SearchActivityView;
    .locals 1

    const v0, 0x7f040005    # com.android.quicksearchbox.R.layout.search_activity

    invoke-virtual {p0, v0}, Lcom/android/quicksearchbox/SearchActivity;->setContentView(I)V

    const v0, 0x7f0f0013    # com.android.quicksearchbox.R.id.search_activity_view

    invoke-virtual {p0, v0}, Lcom/android/quicksearchbox/SearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/quicksearchbox/ui/SearchActivityView;

    return-object v0
.end method

.method protected showSuggestions(Lcom/android/quicksearchbox/Suggestions;)V
    .locals 1
    .param p1    # Lcom/android/quicksearchbox/Suggestions;

    iget-object v0, p0, Lcom/android/quicksearchbox/SearchActivity;->mSearchActivityView:Lcom/android/quicksearchbox/ui/SearchActivityView;

    invoke-virtual {v0, p1}, Lcom/android/quicksearchbox/ui/SearchActivityView;->setSuggestions(Lcom/android/quicksearchbox/Suggestions;)V

    return-void
.end method

.method protected startSearch(Lcom/android/quicksearchbox/Corpus;Ljava/lang/String;)V
    .locals 2
    .param p1    # Lcom/android/quicksearchbox/Corpus;
    .param p2    # Ljava/lang/String;

    iget-object v1, p0, Lcom/android/quicksearchbox/SearchActivity;->mAppSearchData:Landroid/os/Bundle;

    invoke-interface {p1, p2, v1}, Lcom/android/quicksearchbox/Corpus;->createSearchIntent(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/quicksearchbox/SearchActivity;->launchIntent(Landroid/content/Intent;)V

    return-void
.end method

.method public startedIntoCorpusSelectionDialog()Z
    .locals 2

    const-string v0, "com.android.quicksearchbox.action.QSB_AND_SELECT_CORPUS"

    invoke-virtual {p0}, Lcom/android/quicksearchbox/SearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public updateSuggestions()V
    .locals 3

    sget-object v1, Lcom/google/common/base/CharMatcher;->WHITESPACE:Lcom/google/common/base/CharMatcher;

    invoke-virtual {p0}, Lcom/android/quicksearchbox/SearchActivity;->getQuery()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/common/base/CharMatcher;->trimLeadingFrom(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getQsbApplication()Lcom/android/quicksearchbox/QsbApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/quicksearchbox/QsbApplication;->getSourceTaskExecutor()Lcom/android/quicksearchbox/util/NamedTaskExecutor;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/quicksearchbox/util/NamedTaskExecutor;->cancelPendingTasks()V

    new-instance v1, Lcom/android/quicksearchbox/SearchActivity$9;

    invoke-direct {v1, p0, v0}, Lcom/android/quicksearchbox/SearchActivity$9;-><init>(Lcom/android/quicksearchbox/SearchActivity;Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/android/quicksearchbox/SearchActivity;->getCorporaToQuery(Lcom/android/quicksearchbox/util/Consumer;)V

    return-void
.end method

.method protected updateSuggestions(Ljava/lang/String;Ljava/util/List;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/android/quicksearchbox/Corpus;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Lcom/android/quicksearchbox/SearchActivity;->getSuggestionsProvider()Lcom/android/quicksearchbox/SuggestionsProvider;

    move-result-object v1

    invoke-interface {v1, p1, p2}, Lcom/android/quicksearchbox/SuggestionsProvider;->getSuggestions(Ljava/lang/String;Ljava/util/List;)Lcom/android/quicksearchbox/Suggestions;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/quicksearchbox/SearchActivity;->getShortcutsForQuery(Ljava/lang/String;Ljava/util/Collection;Lcom/android/quicksearchbox/Suggestions;)V

    invoke-direct {p0, v0}, Lcom/android/quicksearchbox/SearchActivity;->gotSuggestions(Lcom/android/quicksearchbox/Suggestions;)V

    invoke-virtual {p0, v0}, Lcom/android/quicksearchbox/SearchActivity;->showSuggestions(Lcom/android/quicksearchbox/Suggestions;)V

    return-void
.end method
