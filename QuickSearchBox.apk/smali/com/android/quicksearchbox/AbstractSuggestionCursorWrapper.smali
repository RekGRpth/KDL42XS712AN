.class public abstract Lcom/android/quicksearchbox/AbstractSuggestionCursorWrapper;
.super Lcom/android/quicksearchbox/AbstractSuggestionWrapper;
.source "AbstractSuggestionCursorWrapper.java"

# interfaces
.implements Lcom/android/quicksearchbox/SuggestionCursor;


# instance fields
.field private final mUserQuery:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0}, Lcom/android/quicksearchbox/AbstractSuggestionWrapper;-><init>()V

    iput-object p1, p0, Lcom/android/quicksearchbox/AbstractSuggestionCursorWrapper;->mUserQuery:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getUserQuery()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/AbstractSuggestionCursorWrapper;->mUserQuery:Ljava/lang/String;

    return-object v0
.end method
