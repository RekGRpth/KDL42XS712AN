.class public abstract Lcom/android/quicksearchbox/preferences/SettingsFragmentBase;
.super Landroid/preference/PreferenceFragment;
.source "SettingsFragmentBase.java"


# instance fields
.field private mController:Lcom/android/quicksearchbox/preferences/PreferenceController;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected createController()Lcom/android/quicksearchbox/preferences/PreferenceControllerFactory;
    .locals 2

    invoke-virtual {p0}, Lcom/android/quicksearchbox/preferences/SettingsFragmentBase;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/android/quicksearchbox/QsbApplication;->get(Landroid/content/Context;)Lcom/android/quicksearchbox/QsbApplication;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/quicksearchbox/preferences/SettingsFragmentBase;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/quicksearchbox/QsbApplication;->createPreferenceControllerFactory(Landroid/app/Activity;)Lcom/android/quicksearchbox/preferences/PreferenceControllerFactory;

    move-result-object v1

    return-object v1
.end method

.method protected getController()Lcom/android/quicksearchbox/preferences/PreferenceController;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/preferences/SettingsFragmentBase;->mController:Lcom/android/quicksearchbox/preferences/PreferenceController;

    return-object v0
.end method

.method protected getPreferencesName()Ljava/lang/String;
    .locals 1

    const-string v0, "SearchSettings"

    return-object v0
.end method

.method protected abstract getPreferencesResourceId()I
.end method

.method protected handlePreferenceGroup(Landroid/preference/PreferenceGroup;)V
    .locals 4
    .param p1    # Landroid/preference/PreferenceGroup;

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v1

    instance-of v2, v1, Landroid/preference/PreferenceCategory;

    if-eqz v2, :cond_0

    check-cast v1, Landroid/preference/PreferenceCategory;

    invoke-virtual {p0, v1}, Lcom/android/quicksearchbox/preferences/SettingsFragmentBase;->handlePreferenceGroup(Landroid/preference/PreferenceGroup;)V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/android/quicksearchbox/preferences/SettingsFragmentBase;->mController:Lcom/android/quicksearchbox/preferences/PreferenceController;

    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/android/quicksearchbox/preferences/PreferenceController;->handlePreference(Landroid/preference/Preference;)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/quicksearchbox/preferences/SettingsFragmentBase;->createController()Lcom/android/quicksearchbox/preferences/PreferenceControllerFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/android/quicksearchbox/preferences/SettingsFragmentBase;->mController:Lcom/android/quicksearchbox/preferences/PreferenceController;

    invoke-virtual {p0}, Lcom/android/quicksearchbox/preferences/SettingsFragmentBase;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/quicksearchbox/preferences/SettingsFragmentBase;->getPreferencesName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceManager;->setSharedPreferencesName(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/quicksearchbox/preferences/SettingsFragmentBase;->getPreferencesResourceId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/quicksearchbox/preferences/SettingsFragmentBase;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/android/quicksearchbox/preferences/SettingsFragmentBase;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/quicksearchbox/preferences/SettingsFragmentBase;->handlePreferenceGroup(Landroid/preference/PreferenceGroup;)V

    iget-object v0, p0, Lcom/android/quicksearchbox/preferences/SettingsFragmentBase;->mController:Lcom/android/quicksearchbox/preferences/PreferenceController;

    invoke-interface {v0}, Lcom/android/quicksearchbox/preferences/PreferenceController;->onCreateComplete()V

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/preferences/SettingsFragmentBase;->mController:Lcom/android/quicksearchbox/preferences/PreferenceController;

    invoke-interface {v0}, Lcom/android/quicksearchbox/preferences/PreferenceController;->onDestroy()V

    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onDestroy()V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    iget-object v0, p0, Lcom/android/quicksearchbox/preferences/SettingsFragmentBase;->mController:Lcom/android/quicksearchbox/preferences/PreferenceController;

    invoke-interface {v0}, Lcom/android/quicksearchbox/preferences/PreferenceController;->onResume()V

    return-void
.end method

.method public onStop()V
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/preferences/SettingsFragmentBase;->mController:Lcom/android/quicksearchbox/preferences/PreferenceController;

    invoke-interface {v0}, Lcom/android/quicksearchbox/preferences/PreferenceController;->onStop()V

    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onStop()V

    return-void
.end method
