.class public Lcom/android/quicksearchbox/preferences/SearchableItemsFragment;
.super Lcom/android/quicksearchbox/preferences/SettingsFragmentBase;
.source "SearchableItemsFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/android/quicksearchbox/preferences/SettingsFragmentBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected getPreferencesResourceId()I
    .locals 1

    const v0, 0x7f060004    # com.android.quicksearchbox.R.xml.preferences_searchable_items

    return v0
.end method

.method protected handlePreferenceGroup(Landroid/preference/PreferenceGroup;)V
    .locals 1
    .param p1    # Landroid/preference/PreferenceGroup;

    invoke-virtual {p0}, Lcom/android/quicksearchbox/preferences/SearchableItemsFragment;->getController()Lcom/android/quicksearchbox/preferences/PreferenceController;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/android/quicksearchbox/preferences/PreferenceController;->handlePreference(Landroid/preference/Preference;)V

    return-void
.end method
