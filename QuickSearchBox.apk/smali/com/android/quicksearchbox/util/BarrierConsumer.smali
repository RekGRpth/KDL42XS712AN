.class public Lcom/android/quicksearchbox/util/BarrierConsumer;
.super Ljava/lang/Object;
.source "BarrierConsumer.java"

# interfaces
.implements Lcom/android/quicksearchbox/util/Consumer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<A:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/android/quicksearchbox/util/Consumer",
        "<TA;>;"
    }
.end annotation


# instance fields
.field private final mExpectedCount:I

.field private final mLock:Ljava/util/concurrent/locks/Lock;

.field private final mNotFull:Ljava/util/concurrent/locks/Condition;

.field private mValues:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<TA;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/android/quicksearchbox/util/BarrierConsumer;->mLock:Ljava/util/concurrent/locks/Lock;

    iget-object v0, p0, Lcom/android/quicksearchbox/util/BarrierConsumer;->mLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/android/quicksearchbox/util/BarrierConsumer;->mNotFull:Ljava/util/concurrent/locks/Condition;

    iput p1, p0, Lcom/android/quicksearchbox/util/BarrierConsumer;->mExpectedCount:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/android/quicksearchbox/util/BarrierConsumer;->mValues:Ljava/util/ArrayList;

    return-void
.end method

.method private isFull()Z
    .locals 2

    iget-object v0, p0, Lcom/android/quicksearchbox/util/BarrierConsumer;->mValues:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/android/quicksearchbox/util/BarrierConsumer;->mExpectedCount:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public consume(Ljava/lang/Object;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TA;)Z"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/quicksearchbox/util/BarrierConsumer;->mLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    :try_start_0
    iget-object v0, p0, Lcom/android/quicksearchbox/util/BarrierConsumer;->mValues:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/quicksearchbox/util/BarrierConsumer;->isFull()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/android/quicksearchbox/util/BarrierConsumer;->mLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_0
    return v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/android/quicksearchbox/util/BarrierConsumer;->mValues:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/android/quicksearchbox/util/BarrierConsumer;->isFull()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/quicksearchbox/util/BarrierConsumer;->mNotFull:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signal()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/android/quicksearchbox/util/BarrierConsumer;->mLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/android/quicksearchbox/util/BarrierConsumer;->mLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public getValues()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<TA;>;"
        }
    .end annotation

    iget-object v1, p0, Lcom/android/quicksearchbox/util/BarrierConsumer;->mLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    :goto_0
    :try_start_0
    invoke-direct {p0}, Lcom/android/quicksearchbox/util/BarrierConsumer;->isFull()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/android/quicksearchbox/util/BarrierConsumer;->mNotFull:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Condition;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v1

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/quicksearchbox/util/BarrierConsumer;->mValues:Ljava/util/ArrayList;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/android/quicksearchbox/util/BarrierConsumer;->mValues:Ljava/util/ArrayList;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v1, p0, Lcom/android/quicksearchbox/util/BarrierConsumer;->mLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object v0

    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/android/quicksearchbox/util/BarrierConsumer;->mLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v1
.end method
