.class public Lcom/android/quicksearchbox/util/BatchingNamedTaskExecutor;
.super Ljava/lang/Object;
.source "BatchingNamedTaskExecutor.java"

# interfaces
.implements Lcom/android/quicksearchbox/util/NamedTaskExecutor;


# instance fields
.field private final mExecutor:Lcom/android/quicksearchbox/util/NamedTaskExecutor;

.field private final mQueuedTasks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/quicksearchbox/util/NamedTask;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/android/quicksearchbox/util/NamedTaskExecutor;)V
    .locals 1
    .param p1    # Lcom/android/quicksearchbox/util/NamedTaskExecutor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/quicksearchbox/util/BatchingNamedTaskExecutor;->mQueuedTasks:Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/android/quicksearchbox/util/BatchingNamedTaskExecutor;->mExecutor:Lcom/android/quicksearchbox/util/NamedTaskExecutor;

    return-void
.end method

.method private dispatch(Lcom/android/quicksearchbox/util/NamedTask;)V
    .locals 1
    .param p1    # Lcom/android/quicksearchbox/util/NamedTask;

    iget-object v0, p0, Lcom/android/quicksearchbox/util/BatchingNamedTaskExecutor;->mExecutor:Lcom/android/quicksearchbox/util/NamedTaskExecutor;

    invoke-interface {v0, p1}, Lcom/android/quicksearchbox/util/NamedTaskExecutor;->execute(Lcom/android/quicksearchbox/util/NamedTask;)V

    return-void
.end method


# virtual methods
.method public cancelPendingTasks()V
    .locals 2

    iget-object v1, p0, Lcom/android/quicksearchbox/util/BatchingNamedTaskExecutor;->mQueuedTasks:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/quicksearchbox/util/BatchingNamedTaskExecutor;->mQueuedTasks:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public close()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/quicksearchbox/util/BatchingNamedTaskExecutor;->cancelPendingTasks()V

    iget-object v0, p0, Lcom/android/quicksearchbox/util/BatchingNamedTaskExecutor;->mExecutor:Lcom/android/quicksearchbox/util/NamedTaskExecutor;

    invoke-interface {v0}, Lcom/android/quicksearchbox/util/NamedTaskExecutor;->close()V

    return-void
.end method

.method public execute(Lcom/android/quicksearchbox/util/NamedTask;)V
    .locals 2
    .param p1    # Lcom/android/quicksearchbox/util/NamedTask;

    iget-object v1, p0, Lcom/android/quicksearchbox/util/BatchingNamedTaskExecutor;->mQueuedTasks:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/quicksearchbox/util/BatchingNamedTaskExecutor;->mQueuedTasks:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public executeNextBatch(I)V
    .locals 11
    .param p1    # I

    const/4 v8, 0x0

    new-array v2, v8, [Lcom/android/quicksearchbox/util/NamedTask;

    iget-object v9, p0, Lcom/android/quicksearchbox/util/BatchingNamedTaskExecutor;->mQueuedTasks:Ljava/util/ArrayList;

    monitor-enter v9

    :try_start_0
    iget-object v8, p0, Lcom/android/quicksearchbox/util/BatchingNamedTaskExecutor;->mQueuedTasks:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-static {v8, p1}, Ljava/lang/Math;->min(II)I

    move-result v3

    iget-object v8, p0, Lcom/android/quicksearchbox/util/BatchingNamedTaskExecutor;->mQueuedTasks:Ljava/util/ArrayList;

    const/4 v10, 0x0

    invoke-virtual {v8, v10, v3}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    move-object v0, v8

    check-cast v0, [Lcom/android/quicksearchbox/util/NamedTask;

    move-object v2, v0

    invoke-interface {v6}, Ljava/util/List;->clear()V

    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v1, v2

    array-length v5, v1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v7, v1, v4

    invoke-direct {p0, v7}, Lcom/android/quicksearchbox/util/BatchingNamedTaskExecutor;->dispatch(Lcom/android/quicksearchbox/util/NamedTask;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :catchall_0
    move-exception v8

    :try_start_1
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v8

    :cond_0
    return-void
.end method
