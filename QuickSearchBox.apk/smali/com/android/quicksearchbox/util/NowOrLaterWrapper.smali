.class public abstract Lcom/android/quicksearchbox/util/NowOrLaterWrapper;
.super Ljava/lang/Object;
.source "NowOrLaterWrapper.java"

# interfaces
.implements Lcom/android/quicksearchbox/util/NowOrLater;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<A:",
        "Ljava/lang/Object;",
        "B:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/android/quicksearchbox/util/NowOrLater",
        "<TB;>;"
    }
.end annotation


# instance fields
.field private final mWrapped:Lcom/android/quicksearchbox/util/NowOrLater;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/quicksearchbox/util/NowOrLater",
            "<TA;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/android/quicksearchbox/util/NowOrLater;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/quicksearchbox/util/NowOrLater",
            "<TA;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/quicksearchbox/util/NowOrLaterWrapper;->mWrapped:Lcom/android/quicksearchbox/util/NowOrLater;

    return-void
.end method


# virtual methods
.method public abstract get(Ljava/lang/Object;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TA;)TB;"
        }
    .end annotation
.end method

.method public getLater(Lcom/android/quicksearchbox/util/Consumer;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/quicksearchbox/util/Consumer",
            "<-TB;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/quicksearchbox/util/NowOrLaterWrapper;->mWrapped:Lcom/android/quicksearchbox/util/NowOrLater;

    new-instance v1, Lcom/android/quicksearchbox/util/NowOrLaterWrapper$1;

    invoke-direct {v1, p0, p1}, Lcom/android/quicksearchbox/util/NowOrLaterWrapper$1;-><init>(Lcom/android/quicksearchbox/util/NowOrLaterWrapper;Lcom/android/quicksearchbox/util/Consumer;)V

    invoke-interface {v0, v1}, Lcom/android/quicksearchbox/util/NowOrLater;->getLater(Lcom/android/quicksearchbox/util/Consumer;)V

    return-void
.end method

.method public getNow()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TB;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/quicksearchbox/util/NowOrLaterWrapper;->mWrapped:Lcom/android/quicksearchbox/util/NowOrLater;

    invoke-interface {v0}, Lcom/android/quicksearchbox/util/NowOrLater;->getNow()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/quicksearchbox/util/NowOrLaterWrapper;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public haveNow()Z
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/util/NowOrLaterWrapper;->mWrapped:Lcom/android/quicksearchbox/util/NowOrLater;

    invoke-interface {v0}, Lcom/android/quicksearchbox/util/NowOrLater;->haveNow()Z

    move-result v0

    return v0
.end method
