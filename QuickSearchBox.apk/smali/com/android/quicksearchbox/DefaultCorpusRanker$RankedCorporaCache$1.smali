.class Lcom/android/quicksearchbox/DefaultCorpusRanker$RankedCorporaCache$1;
.super Ljava/lang/Object;
.source "DefaultCorpusRanker.java"

# interfaces
.implements Lcom/android/quicksearchbox/util/Consumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/quicksearchbox/DefaultCorpusRanker$RankedCorporaCache;->create()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/android/quicksearchbox/util/Consumer",
        "<",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/android/quicksearchbox/DefaultCorpusRanker$RankedCorporaCache;


# direct methods
.method constructor <init>(Lcom/android/quicksearchbox/DefaultCorpusRanker$RankedCorporaCache;)V
    .locals 0

    iput-object p1, p0, Lcom/android/quicksearchbox/DefaultCorpusRanker$RankedCorporaCache$1;->this$1:Lcom/android/quicksearchbox/DefaultCorpusRanker$RankedCorporaCache;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic consume(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Ljava/util/Map;

    invoke-virtual {p0, p1}, Lcom/android/quicksearchbox/DefaultCorpusRanker$RankedCorporaCache$1;->consume(Ljava/util/Map;)Z

    move-result v0

    return v0
.end method

.method public consume(Ljava/util/Map;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation

    iget-object v2, p0, Lcom/android/quicksearchbox/DefaultCorpusRanker$RankedCorporaCache$1;->this$1:Lcom/android/quicksearchbox/DefaultCorpusRanker$RankedCorporaCache;

    iget-object v2, v2, Lcom/android/quicksearchbox/DefaultCorpusRanker$RankedCorporaCache;->this$0:Lcom/android/quicksearchbox/DefaultCorpusRanker;

    # getter for: Lcom/android/quicksearchbox/DefaultCorpusRanker;->mCorpora:Lcom/android/quicksearchbox/Corpora;
    invoke-static {v2}, Lcom/android/quicksearchbox/DefaultCorpusRanker;->access$200(Lcom/android/quicksearchbox/DefaultCorpusRanker;)Lcom/android/quicksearchbox/Corpora;

    move-result-object v2

    invoke-interface {v2}, Lcom/android/quicksearchbox/Corpora;->getCorporaInAll()Ljava/util/List;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v2, Lcom/android/quicksearchbox/DefaultCorpusRanker$CorpusComparator;

    invoke-direct {v2, p1}, Lcom/android/quicksearchbox/DefaultCorpusRanker$CorpusComparator;-><init>(Ljava/util/Map;)V

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iget-object v2, p0, Lcom/android/quicksearchbox/DefaultCorpusRanker$RankedCorporaCache$1;->this$1:Lcom/android/quicksearchbox/DefaultCorpusRanker$RankedCorporaCache;

    # invokes: Lcom/android/quicksearchbox/DefaultCorpusRanker$RankedCorporaCache;->store(Ljava/lang/Object;)V
    invoke-static {v2, v1}, Lcom/android/quicksearchbox/DefaultCorpusRanker$RankedCorporaCache;->access$300(Lcom/android/quicksearchbox/DefaultCorpusRanker$RankedCorporaCache;Ljava/lang/Object;)V

    const/4 v2, 0x1

    return v2
.end method
