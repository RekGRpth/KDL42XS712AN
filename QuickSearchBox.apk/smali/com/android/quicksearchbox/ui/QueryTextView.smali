.class public Lcom/android/quicksearchbox/ui/QueryTextView;
.super Landroid/widget/EditText;
.source "QueryTextView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/quicksearchbox/ui/QueryTextView$CommitCompletionListener;
    }
.end annotation


# instance fields
.field private mCommitCompletionListener:Lcom/android/quicksearchbox/ui/QueryTextView$CommitCompletionListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private getInputMethodManager()Landroid/view/inputmethod/InputMethodManager;
    .locals 2

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/QueryTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    return-object v0
.end method


# virtual methods
.method public hideInputMethod()V
    .locals 3

    invoke-direct {p0}, Lcom/android/quicksearchbox/ui/QueryTextView;->getInputMethodManager()Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/QueryTextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    :cond_0
    return-void
.end method

.method public onCommitCompletion(Landroid/view/inputmethod/CompletionInfo;)V
    .locals 2
    .param p1    # Landroid/view/inputmethod/CompletionInfo;

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/QueryTextView;->hideInputMethod()V

    invoke-virtual {p1}, Landroid/view/inputmethod/CompletionInfo;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/quicksearchbox/ui/QueryTextView;->replaceText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/QueryTextView;->mCommitCompletionListener:Lcom/android/quicksearchbox/ui/QueryTextView$CommitCompletionListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/QueryTextView;->mCommitCompletionListener:Lcom/android/quicksearchbox/ui/QueryTextView$CommitCompletionListener;

    invoke-virtual {p1}, Landroid/view/inputmethod/CompletionInfo;->getPosition()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/android/quicksearchbox/ui/QueryTextView$CommitCompletionListener;->onCommitCompletion(I)V

    :cond_0
    return-void
.end method

.method protected replaceText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/QueryTextView;->clearComposingText()V

    invoke-virtual {p0, p1}, Lcom/android/quicksearchbox/ui/QueryTextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/quicksearchbox/ui/QueryTextView;->setTextSelection(Z)V

    return-void
.end method

.method public setCommitCompletionListener(Lcom/android/quicksearchbox/ui/QueryTextView$CommitCompletionListener;)V
    .locals 0
    .param p1    # Lcom/android/quicksearchbox/ui/QueryTextView$CommitCompletionListener;

    iput-object p1, p0, Lcom/android/quicksearchbox/ui/QueryTextView;->mCommitCompletionListener:Lcom/android/quicksearchbox/ui/QueryTextView$CommitCompletionListener;

    return-void
.end method

.method public setTextSelection(Z)V
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/QueryTextView;->selectAll()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/QueryTextView;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/quicksearchbox/ui/QueryTextView;->setSelection(I)V

    goto :goto_0
.end method

.method public showInputMethod()V
    .locals 2

    invoke-direct {p0}, Lcom/android/quicksearchbox/ui/QueryTextView;->getInputMethodManager()Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    :cond_0
    return-void
.end method
