.class public Lcom/android/quicksearchbox/ui/WebSearchSuggestionView;
.super Lcom/android/quicksearchbox/ui/BaseSuggestionView;
.source "WebSearchSuggestionView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/quicksearchbox/ui/WebSearchSuggestionView$Factory;,
        Lcom/android/quicksearchbox/ui/WebSearchSuggestionView$KeyListener;
    }
.end annotation


# instance fields
.field private final mSuggestionFormatter:Lcom/android/quicksearchbox/SuggestionFormatter;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/quicksearchbox/ui/BaseSuggestionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-static {p1}, Lcom/android/quicksearchbox/QsbApplication;->get(Landroid/content/Context;)Lcom/android/quicksearchbox/QsbApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/quicksearchbox/QsbApplication;->getSuggestionFormatter()Lcom/android/quicksearchbox/SuggestionFormatter;

    move-result-object v0

    iput-object v0, p0, Lcom/android/quicksearchbox/ui/WebSearchSuggestionView;->mSuggestionFormatter:Lcom/android/quicksearchbox/SuggestionFormatter;

    return-void
.end method

.method private setIsHistorySuggestion(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/WebSearchSuggestionView;->mIcon1:Landroid/widget/ImageView;

    const v1, 0x7f020033    # com.android.quicksearchbox.R.drawable.ic_history_suggestion

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/WebSearchSuggestionView;->mIcon1:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/quicksearchbox/ui/WebSearchSuggestionView;->mIcon1:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public bindAsSuggestion(Lcom/android/quicksearchbox/Suggestion;Ljava/lang/String;)V
    .locals 3
    .param p1    # Lcom/android/quicksearchbox/Suggestion;
    .param p2    # Ljava/lang/String;

    invoke-super {p0, p1, p2}, Lcom/android/quicksearchbox/ui/BaseSuggestionView;->bindAsSuggestion(Lcom/android/quicksearchbox/Suggestion;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/android/quicksearchbox/ui/WebSearchSuggestionView;->mSuggestionFormatter:Lcom/android/quicksearchbox/SuggestionFormatter;

    invoke-interface {p1}, Lcom/android/quicksearchbox/Suggestion;->getSuggestionText1()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p2, v2}, Lcom/android/quicksearchbox/SuggestionFormatter;->formatSuggestion(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/quicksearchbox/ui/WebSearchSuggestionView;->setText1(Ljava/lang/CharSequence;)V

    invoke-interface {p1}, Lcom/android/quicksearchbox/Suggestion;->isHistorySuggestion()Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/android/quicksearchbox/ui/WebSearchSuggestionView;->setIsHistorySuggestion(Z)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    invoke-super {p0}, Lcom/android/quicksearchbox/ui/BaseSuggestionView;->onFinishInflate()V

    new-instance v0, Lcom/android/quicksearchbox/ui/WebSearchSuggestionView$KeyListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/quicksearchbox/ui/WebSearchSuggestionView$KeyListener;-><init>(Lcom/android/quicksearchbox/ui/WebSearchSuggestionView;Lcom/android/quicksearchbox/ui/WebSearchSuggestionView$1;)V

    invoke-virtual {p0, v0}, Lcom/android/quicksearchbox/ui/WebSearchSuggestionView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v1, p0, Lcom/android/quicksearchbox/ui/WebSearchSuggestionView;->mIcon2:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v1, p0, Lcom/android/quicksearchbox/ui/WebSearchSuggestionView;->mIcon2:Landroid/widget/ImageView;

    new-instance v2, Lcom/android/quicksearchbox/ui/WebSearchSuggestionView$1;

    invoke-direct {v2, p0}, Lcom/android/quicksearchbox/ui/WebSearchSuggestionView$1;-><init>(Lcom/android/quicksearchbox/ui/WebSearchSuggestionView;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/android/quicksearchbox/ui/WebSearchSuggestionView;->mIcon2:Landroid/widget/ImageView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setFocusable(Z)V

    return-void
.end method
