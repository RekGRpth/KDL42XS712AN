.class public Lcom/android/quicksearchbox/ui/SuggestionsView;
.super Landroid/widget/ListView;
.source "SuggestionsView.java"

# interfaces
.implements Lcom/android/quicksearchbox/ui/SuggestionsListView;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ListView;",
        "Lcom/android/quicksearchbox/ui/SuggestionsListView",
        "<",
        "Landroid/widget/ListAdapter;",
        ">;"
    }
.end annotation


# instance fields
.field private mLimitSuggestionsToViewHeight:Z

.field private mSuggestionsAdapter:Lcom/android/quicksearchbox/ui/SuggestionsAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/quicksearchbox/ui/SuggestionsAdapter",
            "<",
            "Landroid/widget/ListAdapter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private setMaxPromotedByHeight()V
    .locals 6

    iget-object v3, p0, Lcom/android/quicksearchbox/ui/SuggestionsView;->mSuggestionsAdapter:Lcom/android/quicksearchbox/ui/SuggestionsAdapter;

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SuggestionsView;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    instance-of v3, v3, Landroid/widget/FrameLayout;

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SuggestionsView;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    int-to-float v0, v3

    :goto_0
    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SuggestionsView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0001    # com.android.quicksearchbox.R.dimen.suggestion_view_height

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    const/4 v3, 0x0

    cmpl-float v3, v1, v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    div-float v4, v0, v1

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v4, v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget-object v3, p0, Lcom/android/quicksearchbox/ui/SuggestionsView;->mSuggestionsAdapter:Lcom/android/quicksearchbox/ui/SuggestionsAdapter;

    invoke-interface {v3, v2}, Lcom/android/quicksearchbox/ui/SuggestionsAdapter;->setMaxPromoted(I)V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SuggestionsView;->getHeight()I

    move-result v3

    int-to-float v0, v3

    goto :goto_0
.end method


# virtual methods
.method public getSuggestionsAdapter()Lcom/android/quicksearchbox/ui/SuggestionsAdapter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/android/quicksearchbox/ui/SuggestionsAdapter",
            "<",
            "Landroid/widget/ListAdapter;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsView;->mSuggestionsAdapter:Lcom/android/quicksearchbox/ui/SuggestionsAdapter;

    return-object v0
.end method

.method public onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/ListView;->onFinishInflate()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/quicksearchbox/ui/SuggestionsView;->setItemsCanFocus(Z)V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-boolean v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsView;->mLimitSuggestionsToViewHeight:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/quicksearchbox/ui/SuggestionsView;->setMaxPromotedByHeight()V

    :cond_0
    return-void
.end method

.method public setLimitSuggestionsToViewHeight(Z)V
    .locals 1
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/quicksearchbox/ui/SuggestionsView;->mLimitSuggestionsToViewHeight:Z

    iget-boolean v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsView;->mLimitSuggestionsToViewHeight:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/quicksearchbox/ui/SuggestionsView;->setMaxPromotedByHeight()V

    :cond_0
    return-void
.end method

.method public setSuggestionsAdapter(Lcom/android/quicksearchbox/ui/SuggestionsAdapter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/quicksearchbox/ui/SuggestionsAdapter",
            "<",
            "Landroid/widget/ListAdapter;",
            ">;)V"
        }
    .end annotation

    if-nez p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-super {p0, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iput-object p1, p0, Lcom/android/quicksearchbox/ui/SuggestionsView;->mSuggestionsAdapter:Lcom/android/quicksearchbox/ui/SuggestionsAdapter;

    iget-boolean v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsView;->mLimitSuggestionsToViewHeight:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/quicksearchbox/ui/SuggestionsView;->setMaxPromotedByHeight()V

    :cond_0
    return-void

    :cond_1
    invoke-interface {p1}, Lcom/android/quicksearchbox/ui/SuggestionsAdapter;->getListAdapter()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    goto :goto_0
.end method
