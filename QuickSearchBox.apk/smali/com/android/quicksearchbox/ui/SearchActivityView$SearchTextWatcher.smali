.class Lcom/android/quicksearchbox/ui/SearchActivityView$SearchTextWatcher;
.super Ljava/lang/Object;
.source "SearchActivityView.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/quicksearchbox/ui/SearchActivityView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SearchTextWatcher"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/quicksearchbox/ui/SearchActivityView;


# direct methods
.method private constructor <init>(Lcom/android/quicksearchbox/ui/SearchActivityView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/quicksearchbox/ui/SearchActivityView$SearchTextWatcher;->this$0:Lcom/android/quicksearchbox/ui/SearchActivityView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/quicksearchbox/ui/SearchActivityView;Lcom/android/quicksearchbox/ui/SearchActivityView$1;)V
    .locals 0
    .param p1    # Lcom/android/quicksearchbox/ui/SearchActivityView;
    .param p2    # Lcom/android/quicksearchbox/ui/SearchActivityView$1;

    invoke-direct {p0, p1}, Lcom/android/quicksearchbox/ui/SearchActivityView$SearchTextWatcher;-><init>(Lcom/android/quicksearchbox/ui/SearchActivityView;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2
    .param p1    # Landroid/text/Editable;

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    if-nez v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/android/quicksearchbox/ui/SearchActivityView$SearchTextWatcher;->this$0:Lcom/android/quicksearchbox/ui/SearchActivityView;

    iget-boolean v1, v1, Lcom/android/quicksearchbox/ui/SearchActivityView;->mQueryWasEmpty:Z

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/quicksearchbox/ui/SearchActivityView$SearchTextWatcher;->this$0:Lcom/android/quicksearchbox/ui/SearchActivityView;

    iput-boolean v0, v1, Lcom/android/quicksearchbox/ui/SearchActivityView;->mQueryWasEmpty:Z

    iget-object v1, p0, Lcom/android/quicksearchbox/ui/SearchActivityView$SearchTextWatcher;->this$0:Lcom/android/quicksearchbox/ui/SearchActivityView;

    invoke-virtual {v1, v0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->updateUi(Z)V

    :cond_0
    iget-object v1, p0, Lcom/android/quicksearchbox/ui/SearchActivityView$SearchTextWatcher;->this$0:Lcom/android/quicksearchbox/ui/SearchActivityView;

    # getter for: Lcom/android/quicksearchbox/ui/SearchActivityView;->mUpdateSuggestions:Z
    invoke-static {v1}, Lcom/android/quicksearchbox/ui/SearchActivityView;->access$800(Lcom/android/quicksearchbox/ui/SearchActivityView;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/quicksearchbox/ui/SearchActivityView$SearchTextWatcher;->this$0:Lcom/android/quicksearchbox/ui/SearchActivityView;

    # getter for: Lcom/android/quicksearchbox/ui/SearchActivityView;->mQueryListener:Lcom/android/quicksearchbox/ui/SearchActivityView$QueryListener;
    invoke-static {v1}, Lcom/android/quicksearchbox/ui/SearchActivityView;->access$900(Lcom/android/quicksearchbox/ui/SearchActivityView;)Lcom/android/quicksearchbox/ui/SearchActivityView$QueryListener;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/quicksearchbox/ui/SearchActivityView$SearchTextWatcher;->this$0:Lcom/android/quicksearchbox/ui/SearchActivityView;

    # getter for: Lcom/android/quicksearchbox/ui/SearchActivityView;->mQueryListener:Lcom/android/quicksearchbox/ui/SearchActivityView$QueryListener;
    invoke-static {v1}, Lcom/android/quicksearchbox/ui/SearchActivityView;->access$900(Lcom/android/quicksearchbox/ui/SearchActivityView;)Lcom/android/quicksearchbox/ui/SearchActivityView$QueryListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/quicksearchbox/ui/SearchActivityView$QueryListener;->onQueryChanged()V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method
