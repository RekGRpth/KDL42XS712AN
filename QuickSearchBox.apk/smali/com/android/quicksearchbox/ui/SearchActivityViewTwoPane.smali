.class public Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;
.super Lcom/android/quicksearchbox/ui/SearchActivityView;
.source "SearchActivityViewTwoPane.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane$ResultsObserver;
    }
.end annotation


# instance fields
.field private mJustCreated:Z

.field private mMenuButton:Landroid/widget/ImageView;

.field private mResultsAdapter:Lcom/android/quicksearchbox/ui/SuggestionsAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/quicksearchbox/ui/SuggestionsAdapter",
            "<",
            "Landroid/widget/ExpandableListAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private mResultsHeader:Landroid/view/View;

.field private mResultsView:Lcom/android/quicksearchbox/ui/ClusteredSuggestionsView;

.field private mSearchPlate:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/android/quicksearchbox/ui/SearchActivityView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/quicksearchbox/ui/SearchActivityView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/android/quicksearchbox/ui/SearchActivityView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method static synthetic access$000(Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;)V
    .locals 0
    .param p0    # Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;

    invoke-direct {p0}, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->showPopupMenu()V

    return-void
.end method

.method static synthetic access$100(Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;)Lcom/android/quicksearchbox/ui/ClusteredSuggestionsView;
    .locals 1
    .param p0    # Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mResultsView:Lcom/android/quicksearchbox/ui/ClusteredSuggestionsView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;)Landroid/view/View;
    .locals 1
    .param p0    # Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mSearchPlate:Landroid/view/View;

    return-object v0
.end method

.method private checkHideResultsHeader()V
    .locals 2

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mResultsHeader:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mResultsAdapter:Lcom/android/quicksearchbox/ui/SuggestionsAdapter;

    invoke-interface {v0}, Lcom/android/quicksearchbox/ui/SuggestionsAdapter;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mResultsHeader:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mResultsHeader:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private setupEntryAnimations()V
    .locals 9

    const-wide/16 v7, 0x96

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c0006    # com.android.quicksearchbox.R.dimen.APKTOOL_DUMMY_0006

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v4, v1

    const/high16 v5, 0x3f000000    # 0.5f

    mul-float/2addr v4, v5

    float-to-int v3, v4

    iget-object v4, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mSearchPlate:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v4, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mSearchPlate:Landroid/view/View;

    invoke-virtual {v4, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v4, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mSearchPlate:Landroid/view/View;

    const-string v5, "alpha"

    const/4 v6, 0x2

    new-array v6, v6, [I

    fill-array-data v6, :array_0

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0, v7, v8}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    move-object v4, v0

    check-cast v4, Landroid/animation/ValueAnimator;

    new-instance v5, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane$4;

    invoke-direct {v5, p0, v3, v1}, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane$4;-><init>(Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;II)V

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    invoke-virtual {v0, v7, v8}, Landroid/animation/Animator;->setStartDelay(J)V

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    return-void

    :array_0
    .array-data 4
        0x0
        0xff
    .end array-data
.end method

.method private showPopupMenu()V
    .locals 4

    new-instance v1, Landroid/widget/PopupMenu;

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mMenuButton:Landroid/widget/ImageView;

    invoke-direct {v1, v2, v3}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->getActivity()Lcom/android/quicksearchbox/SearchActivity;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Lcom/android/quicksearchbox/SearchActivity;->createMenuItems(Landroid/view/Menu;Z)V

    invoke-virtual {v1}, Landroid/widget/PopupMenu;->show()V

    return-void
.end method


# virtual methods
.method public clearSuggestions()V
    .locals 2

    invoke-super {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->clearSuggestions()V

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mResultsAdapter:Lcom/android/quicksearchbox/ui/SuggestionsAdapter;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/android/quicksearchbox/ui/SuggestionsAdapter;->setSuggestions(Lcom/android/quicksearchbox/Suggestions;)V

    return-void
.end method

.method public considerHidingInputMethod()V
    .locals 0

    return-void
.end method

.method protected createClusteredSuggestionsAdapter()Lcom/android/quicksearchbox/ui/SuggestionsAdapter;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/android/quicksearchbox/ui/SuggestionsAdapter",
            "<",
            "Landroid/widget/ExpandableListAdapter;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/android/quicksearchbox/ui/DelayingSuggestionsAdapter;

    new-instance v1, Lcom/android/quicksearchbox/ui/ClusteredSuggestionsAdapter;

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->getQsbApplication()Lcom/android/quicksearchbox/QsbApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/quicksearchbox/QsbApplication;->getSuggestionViewFactory()Lcom/android/quicksearchbox/ui/SuggestionViewFactory;

    move-result-object v2

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/android/quicksearchbox/ui/ClusteredSuggestionsAdapter;-><init>(Lcom/android/quicksearchbox/ui/SuggestionViewFactory;Landroid/content/Context;)V

    invoke-direct {v0, v1}, Lcom/android/quicksearchbox/ui/DelayingSuggestionsAdapter;-><init>(Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;)V

    return-object v0
.end method

.method protected createResultsPromoter()Lcom/android/quicksearchbox/Promoter;
    .locals 2

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->getCorpus()Lcom/android/quicksearchbox/Corpus;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->getQsbApplication()Lcom/android/quicksearchbox/QsbApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/quicksearchbox/QsbApplication;->createResultsPromoter()Lcom/android/quicksearchbox/Promoter;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->getQsbApplication()Lcom/android/quicksearchbox/QsbApplication;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/quicksearchbox/QsbApplication;->createSingleCorpusResultsPromoter(Lcom/android/quicksearchbox/Corpus;)Lcom/android/quicksearchbox/Promoter;

    move-result-object v1

    goto :goto_0
.end method

.method protected createSuggestionsPromoter()Lcom/android/quicksearchbox/Promoter;
    .locals 1

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->getQsbApplication()Lcom/android/quicksearchbox/QsbApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/quicksearchbox/QsbApplication;->createWebPromoter()Lcom/android/quicksearchbox/Promoter;

    move-result-object v0

    return-object v0
.end method

.method public destroy()V
    .locals 2

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mResultsView:Lcom/android/quicksearchbox/ui/ClusteredSuggestionsView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/quicksearchbox/ui/ClusteredSuggestionsView;->setSuggestionsAdapter(Lcom/android/quicksearchbox/ui/SuggestionsAdapter;)V

    invoke-super {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->destroy()V

    return-void
.end method

.method public getSearchCorpus()Lcom/android/quicksearchbox/Corpus;
    .locals 1

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->getWebCorpus()Lcom/android/quicksearchbox/Corpus;

    move-result-object v0

    return-object v0
.end method

.method protected getVoiceSearchIcon()Landroid/graphics/drawable/Drawable;
    .locals 6

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->getVoiceSearch()Lcom/android/quicksearchbox/VoiceSearch;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/quicksearchbox/VoiceSearch;->getComponent()Landroid/content/ComponentName;

    move-result-object v4

    if-eqz v4, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/16 v5, 0x80

    invoke-virtual {v2, v4, v5}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object v5

    iget-object v1, v5, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    if-eqz v1, :cond_0

    const-string v5, "com.android.launcher.toolbar_icon"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v2, v4}, Landroid/content/pm/PackageManager;->getResourcesForActivity(Landroid/content/ComponentName;)Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    :goto_0
    return-object v5

    :catch_0
    move-exception v5

    :cond_0
    invoke-super {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->getVoiceSearchIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    goto :goto_0
.end method

.method public limitResultsToViewHeight()V
    .locals 2

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mResultsView:Lcom/android/quicksearchbox/ui/ClusteredSuggestionsView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/quicksearchbox/ui/ClusteredSuggestionsView;->setLimitSuggestionsToViewHeight(Z)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 4

    const/4 v3, 0x1

    invoke-super {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->onFinishInflate()V

    const v1, 0x7f0f0023    # com.android.quicksearchbox.R.id.menu_button

    invoke-virtual {p0, v1}, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mMenuButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mMenuButton:Landroid/widget/ImageView;

    new-instance v2, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane$1;

    invoke-direct {v2, p0}, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane$1;-><init>(Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f0f0021    # com.android.quicksearchbox.R.id.shortcuts

    invoke-virtual {p0, v1}, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/quicksearchbox/ui/ClusteredSuggestionsView;

    iput-object v1, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mResultsView:Lcom/android/quicksearchbox/ui/ClusteredSuggestionsView;

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->createClusteredSuggestionsAdapter()Lcom/android/quicksearchbox/ui/SuggestionsAdapter;

    move-result-object v1

    iput-object v1, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mResultsAdapter:Lcom/android/quicksearchbox/ui/SuggestionsAdapter;

    iget-object v1, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mResultsAdapter:Lcom/android/quicksearchbox/ui/SuggestionsAdapter;

    invoke-interface {v1}, Lcom/android/quicksearchbox/ui/SuggestionsAdapter;->getListAdapter()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ExpandableListAdapter;

    new-instance v2, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane$2;

    invoke-direct {v2, p0}, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane$2;-><init>(Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;)V

    invoke-interface {v1, v2}, Landroid/widget/ExpandableListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    iget-object v1, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mResultsView:Lcom/android/quicksearchbox/ui/ClusteredSuggestionsView;

    new-instance v2, Lcom/android/quicksearchbox/ui/SearchActivityView$SuggestionsViewKeyListener;

    invoke-direct {v2, p0}, Lcom/android/quicksearchbox/ui/SearchActivityView$SuggestionsViewKeyListener;-><init>(Lcom/android/quicksearchbox/ui/SearchActivityView;)V

    invoke-virtual {v1, v2}, Lcom/android/quicksearchbox/ui/ClusteredSuggestionsView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v1, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mResultsView:Lcom/android/quicksearchbox/ui/ClusteredSuggestionsView;

    invoke-virtual {v1, v3}, Lcom/android/quicksearchbox/ui/ClusteredSuggestionsView;->setFocusable(Z)V

    const v1, 0x7f0f0022    # com.android.quicksearchbox.R.id.shortcut_title

    invoke-virtual {p0, v1}, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mResultsHeader:Landroid/view/View;

    const v1, 0x7f0f001b    # com.android.quicksearchbox.R.id.left_pane

    invoke-virtual {p0, v1}, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mSearchPlate:Landroid/view/View;

    iput-boolean v3, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mJustCreated:Z

    const v1, 0x7f0f0019    # com.android.quicksearchbox.R.id.dismiss_bg

    invoke-virtual {p0, v1}, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane$3;

    invoke-direct {v1, p0}, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane$3;-><init>(Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onPause()V
    .locals 3

    invoke-super {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->onPause()V

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->getActivity()Lcom/android/quicksearchbox/SearchActivity;

    move-result-object v0

    const v1, 0x7f050002    # com.android.quicksearchbox.R.anim.fade_in_fast

    const v2, 0x7f050003    # com.android.quicksearchbox.R.anim.fade_out_fast

    invoke-virtual {v0, v1, v2}, Lcom/android/quicksearchbox/SearchActivity;->overridePendingTransition(II)V

    return-void
.end method

.method protected onResultsChanged()V
    .locals 0

    invoke-direct {p0}, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->checkHideResultsHeader()V

    return-void
.end method

.method public onResume()V
    .locals 1

    iget-boolean v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mJustCreated:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->setupEntryAnimations()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mJustCreated:Z

    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 0

    return-void
.end method

.method protected setCorpus(Lcom/android/quicksearchbox/Corpus;)V
    .locals 2
    .param p1    # Lcom/android/quicksearchbox/Corpus;

    invoke-super {p0, p1}, Lcom/android/quicksearchbox/ui/SearchActivityView;->setCorpus(Lcom/android/quicksearchbox/Corpus;)V

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mResultsAdapter:Lcom/android/quicksearchbox/ui/SuggestionsAdapter;

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->createResultsPromoter()Lcom/android/quicksearchbox/Promoter;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/android/quicksearchbox/ui/SuggestionsAdapter;->setPromoter(Lcom/android/quicksearchbox/Promoter;)V

    return-void
.end method

.method public setMaxPromotedResults(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mResultsView:Lcom/android/quicksearchbox/ui/ClusteredSuggestionsView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/quicksearchbox/ui/ClusteredSuggestionsView;->setLimitSuggestionsToViewHeight(Z)V

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mResultsAdapter:Lcom/android/quicksearchbox/ui/SuggestionsAdapter;

    invoke-interface {v0, p1}, Lcom/android/quicksearchbox/ui/SuggestionsAdapter;->setMaxPromoted(I)V

    return-void
.end method

.method public setSuggestionClickListener(Lcom/android/quicksearchbox/ui/SuggestionClickListener;)V
    .locals 1
    .param p1    # Lcom/android/quicksearchbox/ui/SuggestionClickListener;

    invoke-super {p0, p1}, Lcom/android/quicksearchbox/ui/SearchActivityView;->setSuggestionClickListener(Lcom/android/quicksearchbox/ui/SuggestionClickListener;)V

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mResultsAdapter:Lcom/android/quicksearchbox/ui/SuggestionsAdapter;

    invoke-interface {v0, p1}, Lcom/android/quicksearchbox/ui/SuggestionsAdapter;->setSuggestionClickListener(Lcom/android/quicksearchbox/ui/SuggestionClickListener;)V

    return-void
.end method

.method public setSuggestions(Lcom/android/quicksearchbox/Suggestions;)V
    .locals 1
    .param p1    # Lcom/android/quicksearchbox/Suggestions;

    invoke-super {p0, p1}, Lcom/android/quicksearchbox/ui/SearchActivityView;->setSuggestions(Lcom/android/quicksearchbox/Suggestions;)V

    invoke-virtual {p1}, Lcom/android/quicksearchbox/Suggestions;->acquire()V

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mResultsAdapter:Lcom/android/quicksearchbox/ui/SuggestionsAdapter;

    invoke-interface {v0, p1}, Lcom/android/quicksearchbox/ui/SuggestionsAdapter;->setSuggestions(Lcom/android/quicksearchbox/Suggestions;)V

    return-void
.end method

.method public showCorpusSelectionDialog()V
    .locals 0

    return-void
.end method

.method public start()V
    .locals 2

    invoke-super {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->start()V

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mResultsAdapter:Lcom/android/quicksearchbox/ui/SuggestionsAdapter;

    invoke-interface {v0}, Lcom/android/quicksearchbox/ui/SuggestionsAdapter;->getListAdapter()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ExpandableListAdapter;

    new-instance v1, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane$ResultsObserver;

    invoke-direct {v1, p0}, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane$ResultsObserver;-><init>(Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;)V

    invoke-interface {v0, v1}, Landroid/widget/ExpandableListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mResultsView:Lcom/android/quicksearchbox/ui/ClusteredSuggestionsView;

    iget-object v1, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mResultsAdapter:Lcom/android/quicksearchbox/ui/SuggestionsAdapter;

    invoke-virtual {v0, v1}, Lcom/android/quicksearchbox/ui/ClusteredSuggestionsView;->setSuggestionsAdapter(Lcom/android/quicksearchbox/ui/SuggestionsAdapter;)V

    return-void
.end method

.method protected updateQueryTextView(Z)V
    .locals 2
    .param p1    # Z

    invoke-super {p0, p1}, Lcom/android/quicksearchbox/ui/SearchActivityView;->updateQueryTextView(Z)V

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mSearchCloseButton:Landroid/widget/ImageButton;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mSearchCloseButton:Landroid/widget/ImageButton;

    const v1, 0x7f02002b    # com.android.quicksearchbox.R.drawable.ic_clear_disabled

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityViewTwoPane;->mSearchCloseButton:Landroid/widget/ImageButton;

    const v1, 0x7f02002a    # com.android.quicksearchbox.R.drawable.ic_clear

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0
.end method
