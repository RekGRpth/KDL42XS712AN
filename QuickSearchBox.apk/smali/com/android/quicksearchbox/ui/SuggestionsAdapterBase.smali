.class public abstract Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;
.super Ljava/lang/Object;
.source "SuggestionsAdapterBase.java"

# interfaces
.implements Lcom/android/quicksearchbox/ui/SuggestionsAdapter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase$1;,
        Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase$SuggestionViewClickListener;,
        Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase$MySuggestionsObserver;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<A:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/android/quicksearchbox/ui/SuggestionsAdapter",
        "<TA;>;"
    }
.end annotation


# instance fields
.field private mClosed:Z

.field private mDataSetObserver:Landroid/database/DataSetObserver;

.field private mMaxPromoted:I

.field private mOnFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

.field private mPromotedSuggestions:Lcom/android/quicksearchbox/SuggestionCursor;

.field private mPromoter:Lcom/android/quicksearchbox/Promoter;

.field private mSuggestionClickListener:Lcom/android/quicksearchbox/ui/SuggestionClickListener;

.field private mSuggestions:Lcom/android/quicksearchbox/Suggestions;

.field private final mViewFactory:Lcom/android/quicksearchbox/ui/SuggestionViewFactory;

.field private final mViewTypeMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lcom/android/quicksearchbox/ui/SuggestionViewFactory;)V
    .locals 4
    .param p1    # Lcom/android/quicksearchbox/ui/SuggestionViewFactory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mClosed:Z

    iput-object p1, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mViewFactory:Lcom/android/quicksearchbox/ui/SuggestionViewFactory;

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mViewTypeMap:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mViewFactory:Lcom/android/quicksearchbox/ui/SuggestionViewFactory;

    invoke-interface {v2}, Lcom/android/quicksearchbox/ui/SuggestionViewFactory;->getSuggestionViewTypes()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mViewTypeMap:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mViewTypeMap:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mViewTypeMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-void
.end method

.method private changePromoted(Lcom/android/quicksearchbox/SuggestionCursor;)V
    .locals 1
    .param p1    # Lcom/android/quicksearchbox/SuggestionCursor;

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mPromotedSuggestions:Lcom/android/quicksearchbox/SuggestionCursor;

    if-ne p1, v0, :cond_1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->notifyDataSetChanged()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mPromotedSuggestions:Lcom/android/quicksearchbox/SuggestionCursor;

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mPromotedSuggestions:Lcom/android/quicksearchbox/SuggestionCursor;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->notifyDataSetChanged()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->notifyDataSetInvalidated()V

    goto :goto_0
.end method

.method private suggestionViewType(Lcom/android/quicksearchbox/Suggestion;)Ljava/lang/String;
    .locals 4
    .param p1    # Lcom/android/quicksearchbox/Suggestion;

    iget-object v1, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mViewFactory:Lcom/android/quicksearchbox/ui/SuggestionViewFactory;

    invoke-interface {v1, p1}, Lcom/android/quicksearchbox/ui/SuggestionViewFactory;->getViewType(Lcom/android/quicksearchbox/Suggestion;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mViewTypeMap:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown viewType "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    return-object v0
.end method


# virtual methods
.method public getCurrentPromotedSuggestions()Lcom/android/quicksearchbox/SuggestionCursor;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mPromotedSuggestions:Lcom/android/quicksearchbox/SuggestionCursor;

    return-object v0
.end method

.method public abstract getListAdapter()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TA;"
        }
    .end annotation
.end method

.method protected getPromoted(Lcom/android/quicksearchbox/Suggestions;)Lcom/android/quicksearchbox/SuggestionCursor;
    .locals 2
    .param p1    # Lcom/android/quicksearchbox/Suggestions;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mPromoter:Lcom/android/quicksearchbox/Promoter;

    iget v1, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mMaxPromoted:I

    invoke-virtual {p1, v0, v1}, Lcom/android/quicksearchbox/Suggestions;->getPromoted(Lcom/android/quicksearchbox/Promoter;I)Lcom/android/quicksearchbox/SuggestionCursor;

    move-result-object v0

    goto :goto_0
.end method

.method protected getPromotedCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mPromotedSuggestions:Lcom/android/quicksearchbox/SuggestionCursor;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mPromotedSuggestions:Lcom/android/quicksearchbox/SuggestionCursor;

    invoke-interface {v0}, Lcom/android/quicksearchbox/SuggestionCursor;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method protected getPromotedSuggestion(I)Lcom/android/quicksearchbox/SuggestionPosition;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mPromotedSuggestions:Lcom/android/quicksearchbox/SuggestionCursor;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/android/quicksearchbox/SuggestionPosition;

    iget-object v1, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mPromotedSuggestions:Lcom/android/quicksearchbox/SuggestionCursor;

    invoke-direct {v0, v1, p1}, Lcom/android/quicksearchbox/SuggestionPosition;-><init>(Lcom/android/quicksearchbox/SuggestionCursor;I)V

    goto :goto_0
.end method

.method public abstract getSuggestion(J)Lcom/android/quicksearchbox/SuggestionPosition;
.end method

.method protected getSuggestionViewType(Lcom/android/quicksearchbox/SuggestionCursor;I)I
    .locals 2
    .param p1    # Lcom/android/quicksearchbox/SuggestionCursor;
    .param p2    # I

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-interface {p1, p2}, Lcom/android/quicksearchbox/SuggestionCursor;->moveTo(I)V

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mViewTypeMap:Ljava/util/HashMap;

    invoke-direct {p0, p1}, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->suggestionViewType(Lcom/android/quicksearchbox/Suggestion;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method protected getSuggestionViewTypeCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mViewTypeMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    return v0
.end method

.method public getSuggestions()Lcom/android/quicksearchbox/Suggestions;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mSuggestions:Lcom/android/quicksearchbox/Suggestions;

    return-object v0
.end method

.method protected getView(Lcom/android/quicksearchbox/SuggestionCursor;IJLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # Lcom/android/quicksearchbox/SuggestionCursor;
    .param p2    # I
    .param p3    # J
    .param p5    # Landroid/view/View;
    .param p6    # Landroid/view/ViewGroup;

    invoke-interface {p1, p2}, Lcom/android/quicksearchbox/SuggestionCursor;->moveTo(I)V

    iget-object v2, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mViewFactory:Lcom/android/quicksearchbox/ui/SuggestionViewFactory;

    invoke-interface {p1}, Lcom/android/quicksearchbox/SuggestionCursor;->getUserQuery()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, p1, v3, p5, p6}, Lcom/android/quicksearchbox/ui/SuggestionViewFactory;->getView(Lcom/android/quicksearchbox/SuggestionCursor;Ljava/lang/String;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    instance-of v2, v1, Lcom/android/quicksearchbox/ui/SuggestionView;

    if-eqz v2, :cond_1

    move-object v2, v1

    check-cast v2, Lcom/android/quicksearchbox/ui/SuggestionView;

    invoke-interface {v2, p0, p3, p4}, Lcom/android/quicksearchbox/ui/SuggestionView;->bindAdapter(Lcom/android/quicksearchbox/ui/SuggestionsAdapter;J)V

    :goto_0
    iget-object v2, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mOnFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mOnFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    :cond_0
    return-object v1

    :cond_1
    new-instance v0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase$SuggestionViewClickListener;

    invoke-direct {v0, p0, p3, p4}, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase$SuggestionViewClickListener;-><init>(Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;J)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public isClosed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mClosed:Z

    return v0
.end method

.method public abstract isEmpty()Z
.end method

.method protected abstract notifyDataSetChanged()V
.end method

.method protected abstract notifyDataSetInvalidated()V
.end method

.method public onSuggestionClicked(J)V
    .locals 2
    .param p1    # J

    iget-boolean v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mClosed:Z

    if-eqz v0, :cond_1

    const-string v0, "QSB.SuggestionsAdapter"

    const-string v1, "onSuggestionClicked after close"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mSuggestionClickListener:Lcom/android/quicksearchbox/ui/SuggestionClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mSuggestionClickListener:Lcom/android/quicksearchbox/ui/SuggestionClickListener;

    invoke-interface {v0, p0, p1, p2}, Lcom/android/quicksearchbox/ui/SuggestionClickListener;->onSuggestionClicked(Lcom/android/quicksearchbox/ui/SuggestionsAdapter;J)V

    goto :goto_0
.end method

.method public onSuggestionQueryRefineClicked(J)V
    .locals 2
    .param p1    # J

    iget-boolean v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mClosed:Z

    if-eqz v0, :cond_1

    const-string v0, "QSB.SuggestionsAdapter"

    const-string v1, "onSuggestionQueryRefineClicked after close"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mSuggestionClickListener:Lcom/android/quicksearchbox/ui/SuggestionClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mSuggestionClickListener:Lcom/android/quicksearchbox/ui/SuggestionClickListener;

    invoke-interface {v0, p0, p1, p2}, Lcom/android/quicksearchbox/ui/SuggestionClickListener;->onSuggestionQueryRefineClicked(Lcom/android/quicksearchbox/ui/SuggestionsAdapter;J)V

    goto :goto_0
.end method

.method public onSuggestionQuickContactClicked(J)V
    .locals 2
    .param p1    # J

    iget-boolean v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mClosed:Z

    if-eqz v0, :cond_1

    const-string v0, "QSB.SuggestionsAdapter"

    const-string v1, "onSuggestionQuickContactClicked after close"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mSuggestionClickListener:Lcom/android/quicksearchbox/ui/SuggestionClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mSuggestionClickListener:Lcom/android/quicksearchbox/ui/SuggestionClickListener;

    invoke-interface {v0, p0, p1, p2}, Lcom/android/quicksearchbox/ui/SuggestionClickListener;->onSuggestionQuickContactClicked(Lcom/android/quicksearchbox/ui/SuggestionsAdapter;J)V

    goto :goto_0
.end method

.method public onSuggestionRemoveFromHistoryClicked(J)V
    .locals 2
    .param p1    # J

    iget-boolean v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mClosed:Z

    if-eqz v0, :cond_1

    const-string v0, "QSB.SuggestionsAdapter"

    const-string v1, "onSuggestionRemoveFromHistoryClicked after close"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mSuggestionClickListener:Lcom/android/quicksearchbox/ui/SuggestionClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mSuggestionClickListener:Lcom/android/quicksearchbox/ui/SuggestionClickListener;

    invoke-interface {v0, p0, p1, p2}, Lcom/android/quicksearchbox/ui/SuggestionClickListener;->onSuggestionRemoveFromHistoryClicked(Lcom/android/quicksearchbox/ui/SuggestionsAdapter;J)V

    goto :goto_0
.end method

.method protected onSuggestionsChanged()V
    .locals 2

    iget-object v1, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mSuggestions:Lcom/android/quicksearchbox/Suggestions;

    invoke-virtual {p0, v1}, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->getPromoted(Lcom/android/quicksearchbox/Suggestions;)Lcom/android/quicksearchbox/SuggestionCursor;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->changePromoted(Lcom/android/quicksearchbox/SuggestionCursor;)V

    return-void
.end method

.method public setMaxPromoted(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mMaxPromoted:I

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->onSuggestionsChanged()V

    return-void
.end method

.method public setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V
    .locals 0
    .param p1    # Landroid/view/View$OnFocusChangeListener;

    iput-object p1, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mOnFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    return-void
.end method

.method public setPromoter(Lcom/android/quicksearchbox/Promoter;)V
    .locals 0
    .param p1    # Lcom/android/quicksearchbox/Promoter;

    iput-object p1, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mPromoter:Lcom/android/quicksearchbox/Promoter;

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->onSuggestionsChanged()V

    return-void
.end method

.method public setSuggestionClickListener(Lcom/android/quicksearchbox/ui/SuggestionClickListener;)V
    .locals 0
    .param p1    # Lcom/android/quicksearchbox/ui/SuggestionClickListener;

    iput-object p1, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mSuggestionClickListener:Lcom/android/quicksearchbox/ui/SuggestionClickListener;

    return-void
.end method

.method public setSuggestions(Lcom/android/quicksearchbox/Suggestions;)V
    .locals 2
    .param p1    # Lcom/android/quicksearchbox/Suggestions;

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mSuggestions:Lcom/android/quicksearchbox/Suggestions;

    if-ne v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mClosed:Z

    if-eqz v0, :cond_2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/android/quicksearchbox/Suggestions;->release()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mDataSetObserver:Landroid/database/DataSetObserver;

    if-nez v0, :cond_3

    new-instance v0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase$MySuggestionsObserver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase$MySuggestionsObserver;-><init>(Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase$1;)V

    iput-object v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mDataSetObserver:Landroid/database/DataSetObserver;

    :cond_3
    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mSuggestions:Lcom/android/quicksearchbox/Suggestions;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mSuggestions:Lcom/android/quicksearchbox/Suggestions;

    iget-object v1, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mDataSetObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/android/quicksearchbox/Suggestions;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mSuggestions:Lcom/android/quicksearchbox/Suggestions;

    invoke-virtual {v0}, Lcom/android/quicksearchbox/Suggestions;->release()V

    :cond_4
    iput-object p1, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mSuggestions:Lcom/android/quicksearchbox/Suggestions;

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mSuggestions:Lcom/android/quicksearchbox/Suggestions;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mSuggestions:Lcom/android/quicksearchbox/Suggestions;

    iget-object v1, p0, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->mDataSetObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/android/quicksearchbox/Suggestions;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    :cond_5
    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;->onSuggestionsChanged()V

    goto :goto_0
.end method

.method public abstract willPublishNonPromotedSuggestions()Z
.end method
