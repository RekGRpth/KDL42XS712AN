.class public abstract Lcom/android/quicksearchbox/ui/SearchActivityView;
.super Landroid/widget/RelativeLayout;
.source "SearchActivityView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/quicksearchbox/ui/SearchActivityView$CloseClickListener;,
        Lcom/android/quicksearchbox/ui/SearchActivityView$SearchClickListener;,
        Lcom/android/quicksearchbox/ui/SearchActivityView$QueryListener;,
        Lcom/android/quicksearchbox/ui/SearchActivityView$SuggestionsObserver;,
        Lcom/android/quicksearchbox/ui/SearchActivityView$QueryTextViewFocusListener;,
        Lcom/android/quicksearchbox/ui/SearchActivityView$SuggestListFocusListener;,
        Lcom/android/quicksearchbox/ui/SearchActivityView$ButtonsKeyListener;,
        Lcom/android/quicksearchbox/ui/SearchActivityView$QueryTextEditorActionListener;,
        Lcom/android/quicksearchbox/ui/SearchActivityView$SearchGoButtonClickListener;,
        Lcom/android/quicksearchbox/ui/SearchActivityView$InputMethodCloser;,
        Lcom/android/quicksearchbox/ui/SearchActivityView$SuggestionsViewKeyListener;,
        Lcom/android/quicksearchbox/ui/SearchActivityView$SearchTextWatcher;
    }
.end annotation


# instance fields
.field protected mButtonsKeyListener:Lcom/android/quicksearchbox/ui/SearchActivityView$ButtonsKeyListener;

.field private mCorpus:Lcom/android/quicksearchbox/Corpus;

.field protected mExitClickListener:Landroid/view/View$OnClickListener;

.field private mQueryListener:Lcom/android/quicksearchbox/ui/SearchActivityView$QueryListener;

.field protected mQueryTextEmptyBg:Landroid/graphics/drawable/Drawable;

.field protected mQueryTextNotEmptyBg:Landroid/graphics/drawable/Drawable;

.field protected mQueryTextView:Lcom/android/quicksearchbox/ui/QueryTextView;

.field protected mQueryWasEmpty:Z

.field private mSearchClickListener:Lcom/android/quicksearchbox/ui/SearchActivityView$SearchClickListener;

.field protected mSearchCloseButton:Landroid/widget/ImageButton;

.field protected mSearchGoButton:Landroid/widget/ImageButton;

.field protected mSuggestionsAdapter:Lcom/android/quicksearchbox/ui/SuggestionsAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/quicksearchbox/ui/SuggestionsAdapter",
            "<",
            "Landroid/widget/ListAdapter;",
            ">;"
        }
    .end annotation
.end field

.field protected mSuggestionsView:Lcom/android/quicksearchbox/ui/SuggestionsListView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/android/quicksearchbox/ui/SuggestionsListView",
            "<",
            "Landroid/widget/ListAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private mUpdateSuggestions:Z

.field protected mVoiceSearchButton:Landroid/widget/ImageButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mQueryWasEmpty:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mQueryWasEmpty:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mQueryWasEmpty:Z

    return-void
.end method

.method static synthetic access$1000(Lcom/android/quicksearchbox/ui/SearchActivityView;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p0    # Lcom/android/quicksearchbox/ui/SearchActivityView;
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    invoke-direct {p0, p1, p2}, Lcom/android/quicksearchbox/ui/SearchActivityView;->forwardKeyToQueryTextView(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/android/quicksearchbox/ui/SearchActivityView;)Z
    .locals 1
    .param p0    # Lcom/android/quicksearchbox/ui/SearchActivityView;

    iget-boolean v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mUpdateSuggestions:Z

    return v0
.end method

.method static synthetic access$900(Lcom/android/quicksearchbox/ui/SearchActivityView;)Lcom/android/quicksearchbox/ui/SearchActivityView$QueryListener;
    .locals 1
    .param p0    # Lcom/android/quicksearchbox/ui/SearchActivityView;

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mQueryListener:Lcom/android/quicksearchbox/ui/SearchActivityView$QueryListener;

    return-object v0
.end method

.method private forwardKeyToQueryTextView(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isSystem()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/android/quicksearchbox/ui/SearchActivityView;->shouldForwardToQueryTextView(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mQueryTextView:Lcom/android/quicksearchbox/ui/QueryTextView;

    invoke-virtual {v0}, Lcom/android/quicksearchbox/ui/QueryTextView;->requestFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mQueryTextView:Lcom/android/quicksearchbox/ui/QueryTextView;

    invoke-virtual {v0, p2}, Lcom/android/quicksearchbox/ui/QueryTextView;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private shouldForwardToQueryTextView(I)Z
    .locals 1
    .param p1    # I

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :sswitch_0
    const/4 v0, 0x0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_0
        0x15 -> :sswitch_0
        0x16 -> :sswitch_0
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
        0x54 -> :sswitch_0
    .end sparse-switch
.end method

.method private updateSearchGoButton(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mSearchGoButton:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mSearchGoButton:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method private webSuggestionsToCompletions(Lcom/android/quicksearchbox/Suggestions;)[Landroid/view/inputmethod/CompletionInfo;
    .locals 9
    .param p1    # Lcom/android/quicksearchbox/Suggestions;

    invoke-virtual {p1}, Lcom/android/quicksearchbox/Suggestions;->getWebResult()Lcom/android/quicksearchbox/CorpusResult;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v6, 0x0

    :goto_0
    return-object v6

    :cond_0
    invoke-interface {v2}, Lcom/android/quicksearchbox/CorpusResult;->getCount()I

    move-result v1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->isSearchCorpusWeb()Z

    move-result v5

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v1, :cond_3

    invoke-interface {v2, v3}, Lcom/android/quicksearchbox/CorpusResult;->moveTo(I)V

    if-eqz v5, :cond_1

    invoke-interface {v2}, Lcom/android/quicksearchbox/CorpusResult;->isWebSearchSuggestion()Z

    move-result v6

    if-eqz v6, :cond_2

    :cond_1
    invoke-interface {v2}, Lcom/android/quicksearchbox/CorpusResult;->getSuggestionText1()Ljava/lang/String;

    move-result-object v4

    new-instance v6, Landroid/view/inputmethod/CompletionInfo;

    int-to-long v7, v3

    invoke-direct {v6, v7, v8, v3, v4}, Landroid/view/inputmethod/CompletionInfo;-><init>(JILjava/lang/CharSequence;)V

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v6, v6, [Landroid/view/inputmethod/CompletionInfo;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Landroid/view/inputmethod/CompletionInfo;

    goto :goto_0
.end method


# virtual methods
.method public clearSuggestions()V
    .locals 2

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mSuggestionsAdapter:Lcom/android/quicksearchbox/ui/SuggestionsAdapter;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/android/quicksearchbox/ui/SuggestionsAdapter;->setSuggestions(Lcom/android/quicksearchbox/Suggestions;)V

    return-void
.end method

.method public abstract considerHidingInputMethod()V
.end method

.method protected createSuggestionsAdapter()Lcom/android/quicksearchbox/ui/SuggestionsAdapter;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/android/quicksearchbox/ui/SuggestionsAdapter",
            "<",
            "Landroid/widget/ListAdapter;",
            ">;"
        }
    .end annotation

    new-instance v0, Lcom/android/quicksearchbox/ui/DelayingSuggestionsAdapter;

    new-instance v1, Lcom/android/quicksearchbox/ui/SuggestionsListAdapter;

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->getQsbApplication()Lcom/android/quicksearchbox/QsbApplication;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/quicksearchbox/QsbApplication;->getSuggestionViewFactory()Lcom/android/quicksearchbox/ui/SuggestionViewFactory;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/quicksearchbox/ui/SuggestionsListAdapter;-><init>(Lcom/android/quicksearchbox/ui/SuggestionViewFactory;)V

    invoke-direct {v0, v1}, Lcom/android/quicksearchbox/ui/DelayingSuggestionsAdapter;-><init>(Lcom/android/quicksearchbox/ui/SuggestionsAdapterBase;)V

    return-object v0
.end method

.method protected abstract createSuggestionsPromoter()Lcom/android/quicksearchbox/Promoter;
.end method

.method public destroy()V
    .locals 2

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mSuggestionsView:Lcom/android/quicksearchbox/ui/SuggestionsListView;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/android/quicksearchbox/ui/SuggestionsListView;->setSuggestionsAdapter(Lcom/android/quicksearchbox/ui/SuggestionsAdapter;)V

    return-void
.end method

.method public dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z
    .locals 5
    .param p1    # Landroid/view/KeyEvent;

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->getActivity()Lcom/android/quicksearchbox/SearchActivity;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_1

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->isQueryEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1, p1, p0}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    :goto_0
    return v2

    :cond_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-ne v3, v2, :cond_1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v1, p1}, Landroid/view/KeyEvent$DispatcherState;->isTracking(Landroid/view/KeyEvent;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->hideInputMethod()V

    invoke-virtual {v0}, Lcom/android/quicksearchbox/SearchActivity;->onBackPressed()V

    goto :goto_0

    :cond_1
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z

    move-result v2

    goto :goto_0
.end method

.method public focusQueryTextView()V
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mQueryTextView:Lcom/android/quicksearchbox/ui/QueryTextView;

    invoke-virtual {v0}, Lcom/android/quicksearchbox/ui/QueryTextView;->requestFocus()Z

    return-void
.end method

.method protected getActivity()Lcom/android/quicksearchbox/SearchActivity;
    .locals 2

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v1, v0, Lcom/android/quicksearchbox/SearchActivity;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/android/quicksearchbox/SearchActivity;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getCorpora()Lcom/android/quicksearchbox/Corpora;
    .locals 1

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->getQsbApplication()Lcom/android/quicksearchbox/QsbApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/quicksearchbox/QsbApplication;->getCorpora()Lcom/android/quicksearchbox/Corpora;

    move-result-object v0

    return-object v0
.end method

.method public getCorpus()Lcom/android/quicksearchbox/Corpus;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mCorpus:Lcom/android/quicksearchbox/Corpus;

    return-object v0
.end method

.method protected getCorpus(Ljava/lang/String;)Lcom/android/quicksearchbox/Corpus;
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    if-nez p1, :cond_1

    move-object v0, v1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->getCorpora()Lcom/android/quicksearchbox/Corpora;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/android/quicksearchbox/Corpora;->getCorpus(Ljava/lang/String;)Lcom/android/quicksearchbox/Corpus;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v2, "QSB.SearchActivityView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown corpus "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    goto :goto_0
.end method

.method public getCorpusName()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->getCorpus()Lcom/android/quicksearchbox/Corpus;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-interface {v0}, Lcom/android/quicksearchbox/Corpus;->getName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getCurrentPromotedSuggestions()Lcom/android/quicksearchbox/SuggestionCursor;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mSuggestionsAdapter:Lcom/android/quicksearchbox/ui/SuggestionsAdapter;

    invoke-interface {v0}, Lcom/android/quicksearchbox/ui/SuggestionsAdapter;->getCurrentPromotedSuggestions()Lcom/android/quicksearchbox/SuggestionCursor;

    move-result-object v0

    return-object v0
.end method

.method protected getQsbApplication()Lcom/android/quicksearchbox/QsbApplication;
    .locals 1

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/quicksearchbox/QsbApplication;->get(Landroid/content/Context;)Lcom/android/quicksearchbox/QsbApplication;

    move-result-object v0

    return-object v0
.end method

.method public getQuery()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mQueryTextView:Lcom/android/quicksearchbox/ui/QueryTextView;

    invoke-virtual {v1}, Lcom/android/quicksearchbox/ui/QueryTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v1, ""

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public abstract getSearchCorpus()Lcom/android/quicksearchbox/Corpus;
.end method

.method public getSuggestions()Lcom/android/quicksearchbox/Suggestions;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mSuggestionsAdapter:Lcom/android/quicksearchbox/ui/SuggestionsAdapter;

    invoke-interface {v0}, Lcom/android/quicksearchbox/ui/SuggestionsAdapter;->getSuggestions()Lcom/android/quicksearchbox/Suggestions;

    move-result-object v0

    return-object v0
.end method

.method protected getVoiceSearch()Lcom/android/quicksearchbox/VoiceSearch;
    .locals 1

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->getQsbApplication()Lcom/android/quicksearchbox/QsbApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/quicksearchbox/QsbApplication;->getVoiceSearch()Lcom/android/quicksearchbox/VoiceSearch;

    move-result-object v0

    return-object v0
.end method

.method protected getVoiceSearchIcon()Landroid/graphics/drawable/Drawable;
    .locals 2

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020029    # com.android.quicksearchbox.R.drawable.ic_btn_speak_now

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public getWebCorpus()Lcom/android/quicksearchbox/Corpus;
    .locals 3

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->getCorpora()Lcom/android/quicksearchbox/Corpora;

    move-result-object v1

    invoke-interface {v1}, Lcom/android/quicksearchbox/Corpora;->getWebCorpus()Lcom/android/quicksearchbox/Corpus;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v1, "QSB.SearchActivityView"

    const-string v2, "No web corpus"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-object v0
.end method

.method protected hideInputMethod()V
    .locals 3

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    :cond_0
    return-void
.end method

.method public isQueryEmpty()Z
    .locals 1

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->getQuery()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method protected isSearchCorpusWeb()Z
    .locals 2

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->getSearchCorpus()Lcom/android/quicksearchbox/Corpus;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/android/quicksearchbox/Corpus;->isWebCorpus()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public limitResultsToViewHeight()V
    .locals 0

    return-void
.end method

.method public limitSuggestionsToViewHeight()V
    .locals 2

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mSuggestionsView:Lcom/android/quicksearchbox/ui/SuggestionsListView;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/android/quicksearchbox/ui/SuggestionsListView;->setLimitSuggestionsToViewHeight(Z)V

    return-void
.end method

.method public onCorpusSelected(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/android/quicksearchbox/ui/SearchActivityView;->setCorpus(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->focusQueryTextView()V

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->showInputMethodForQuery()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    const/4 v2, 0x0

    const v0, 0x7f0f0016    # com.android.quicksearchbox.R.id.search_src_text

    invoke-virtual {p0, v0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/quicksearchbox/ui/QueryTextView;

    iput-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mQueryTextView:Lcom/android/quicksearchbox/ui/QueryTextView;

    const v0, 0x7f0f0014    # com.android.quicksearchbox.R.id.suggestions

    invoke-virtual {p0, v0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/quicksearchbox/ui/SuggestionsView;

    iput-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mSuggestionsView:Lcom/android/quicksearchbox/ui/SuggestionsListView;

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mSuggestionsView:Lcom/android/quicksearchbox/ui/SuggestionsListView;

    new-instance v1, Lcom/android/quicksearchbox/ui/SearchActivityView$InputMethodCloser;

    invoke-direct {v1, p0, v2}, Lcom/android/quicksearchbox/ui/SearchActivityView$InputMethodCloser;-><init>(Lcom/android/quicksearchbox/ui/SearchActivityView;Lcom/android/quicksearchbox/ui/SearchActivityView$1;)V

    invoke-interface {v0, v1}, Lcom/android/quicksearchbox/ui/SuggestionsListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mSuggestionsView:Lcom/android/quicksearchbox/ui/SuggestionsListView;

    new-instance v1, Lcom/android/quicksearchbox/ui/SearchActivityView$SuggestionsViewKeyListener;

    invoke-direct {v1, p0}, Lcom/android/quicksearchbox/ui/SearchActivityView$SuggestionsViewKeyListener;-><init>(Lcom/android/quicksearchbox/ui/SearchActivityView;)V

    invoke-interface {v0, v1}, Lcom/android/quicksearchbox/ui/SuggestionsListView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mSuggestionsView:Lcom/android/quicksearchbox/ui/SuggestionsListView;

    new-instance v1, Lcom/android/quicksearchbox/ui/SearchActivityView$SuggestListFocusListener;

    invoke-direct {v1, p0, v2}, Lcom/android/quicksearchbox/ui/SearchActivityView$SuggestListFocusListener;-><init>(Lcom/android/quicksearchbox/ui/SearchActivityView;Lcom/android/quicksearchbox/ui/SearchActivityView$1;)V

    invoke-interface {v0, v1}, Lcom/android/quicksearchbox/ui/SuggestionsListView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->createSuggestionsAdapter()Lcom/android/quicksearchbox/ui/SuggestionsAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mSuggestionsAdapter:Lcom/android/quicksearchbox/ui/SuggestionsAdapter;

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mSuggestionsAdapter:Lcom/android/quicksearchbox/ui/SuggestionsAdapter;

    new-instance v1, Lcom/android/quicksearchbox/ui/SearchActivityView$SuggestListFocusListener;

    invoke-direct {v1, p0, v2}, Lcom/android/quicksearchbox/ui/SearchActivityView$SuggestListFocusListener;-><init>(Lcom/android/quicksearchbox/ui/SearchActivityView;Lcom/android/quicksearchbox/ui/SearchActivityView$1;)V

    invoke-interface {v0, v1}, Lcom/android/quicksearchbox/ui/SuggestionsAdapter;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    const v0, 0x7f0f001f    # com.android.quicksearchbox.R.id.search_close_btn

    invoke-virtual {p0, v0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mSearchCloseButton:Landroid/widget/ImageButton;

    const v0, 0x7f0f0017    # com.android.quicksearchbox.R.id.search_go_btn

    invoke-virtual {p0, v0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mSearchGoButton:Landroid/widget/ImageButton;

    const v0, 0x7f0f0018    # com.android.quicksearchbox.R.id.search_voice_btn

    invoke-virtual {p0, v0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mVoiceSearchButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mVoiceSearchButton:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->getVoiceSearchIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mQueryTextView:Lcom/android/quicksearchbox/ui/QueryTextView;

    new-instance v1, Lcom/android/quicksearchbox/ui/SearchActivityView$SearchTextWatcher;

    invoke-direct {v1, p0, v2}, Lcom/android/quicksearchbox/ui/SearchActivityView$SearchTextWatcher;-><init>(Lcom/android/quicksearchbox/ui/SearchActivityView;Lcom/android/quicksearchbox/ui/SearchActivityView$1;)V

    invoke-virtual {v0, v1}, Lcom/android/quicksearchbox/ui/QueryTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mQueryTextView:Lcom/android/quicksearchbox/ui/QueryTextView;

    new-instance v1, Lcom/android/quicksearchbox/ui/SearchActivityView$QueryTextEditorActionListener;

    invoke-direct {v1, p0, v2}, Lcom/android/quicksearchbox/ui/SearchActivityView$QueryTextEditorActionListener;-><init>(Lcom/android/quicksearchbox/ui/SearchActivityView;Lcom/android/quicksearchbox/ui/SearchActivityView$1;)V

    invoke-virtual {v0, v1}, Lcom/android/quicksearchbox/ui/QueryTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mQueryTextView:Lcom/android/quicksearchbox/ui/QueryTextView;

    new-instance v1, Lcom/android/quicksearchbox/ui/SearchActivityView$QueryTextViewFocusListener;

    invoke-direct {v1, p0, v2}, Lcom/android/quicksearchbox/ui/SearchActivityView$QueryTextViewFocusListener;-><init>(Lcom/android/quicksearchbox/ui/SearchActivityView;Lcom/android/quicksearchbox/ui/SearchActivityView$1;)V

    invoke-virtual {v0, v1}, Lcom/android/quicksearchbox/ui/QueryTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mQueryTextView:Lcom/android/quicksearchbox/ui/QueryTextView;

    invoke-virtual {v0}, Lcom/android/quicksearchbox/ui/QueryTextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mQueryTextEmptyBg:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mSearchGoButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/android/quicksearchbox/ui/SearchActivityView$SearchGoButtonClickListener;

    invoke-direct {v1, p0, v2}, Lcom/android/quicksearchbox/ui/SearchActivityView$SearchGoButtonClickListener;-><init>(Lcom/android/quicksearchbox/ui/SearchActivityView;Lcom/android/quicksearchbox/ui/SearchActivityView$1;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/android/quicksearchbox/ui/SearchActivityView$ButtonsKeyListener;

    invoke-direct {v0, p0, v2}, Lcom/android/quicksearchbox/ui/SearchActivityView$ButtonsKeyListener;-><init>(Lcom/android/quicksearchbox/ui/SearchActivityView;Lcom/android/quicksearchbox/ui/SearchActivityView$1;)V

    iput-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mButtonsKeyListener:Lcom/android/quicksearchbox/ui/SearchActivityView$ButtonsKeyListener;

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mSearchGoButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mButtonsKeyListener:Lcom/android/quicksearchbox/ui/SearchActivityView$ButtonsKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mVoiceSearchButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mButtonsKeyListener:Lcom/android/quicksearchbox/ui/SearchActivityView$ButtonsKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mSearchCloseButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mSearchCloseButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mButtonsKeyListener:Lcom/android/quicksearchbox/ui/SearchActivityView$ButtonsKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mSearchCloseButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/android/quicksearchbox/ui/SearchActivityView$CloseClickListener;

    invoke-direct {v1, p0, v2}, Lcom/android/quicksearchbox/ui/SearchActivityView$CloseClickListener;-><init>(Lcom/android/quicksearchbox/ui/SearchActivityView;Lcom/android/quicksearchbox/ui/SearchActivityView$1;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mUpdateSuggestions:Z

    return-void
.end method

.method public onPause()V
    .locals 0

    return-void
.end method

.method public abstract onResume()V
.end method

.method protected onSearchClicked(I)Z
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mSearchClickListener:Lcom/android/quicksearchbox/ui/SearchActivityView$SearchClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mSearchClickListener:Lcom/android/quicksearchbox/ui/SearchActivityView$SearchClickListener;

    invoke-interface {v0, p1}, Lcom/android/quicksearchbox/ui/SearchActivityView$SearchClickListener;->onSearchClicked(I)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract onStop()V
.end method

.method protected onSuggestionKeyDown(Lcom/android/quicksearchbox/ui/SuggestionsAdapter;JILandroid/view/KeyEvent;)Z
    .locals 2
    .param p2    # J
    .param p4    # I
    .param p5    # Landroid/view/KeyEvent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/quicksearchbox/ui/SuggestionsAdapter",
            "<*>;JI",
            "Landroid/view/KeyEvent;",
            ")Z"
        }
    .end annotation

    const/4 v0, 0x0

    const/16 v1, 0x42

    if-eq p4, v1, :cond_0

    const/16 v1, 0x54

    if-eq p4, v1, :cond_0

    const/16 v1, 0x17

    if-ne p4, v1, :cond_1

    :cond_0
    if-eqz p1, :cond_1

    invoke-interface {p1, p2, p3}, Lcom/android/quicksearchbox/ui/SuggestionsAdapter;->onSuggestionClicked(J)V

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method protected onSuggestionsChanged()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->updateInputMethodSuggestions()V

    return-void
.end method

.method protected setCorpus(Lcom/android/quicksearchbox/Corpus;)V
    .locals 3
    .param p1    # Lcom/android/quicksearchbox/Corpus;

    iput-object p1, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mCorpus:Lcom/android/quicksearchbox/Corpus;

    iget-object v1, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mSuggestionsAdapter:Lcom/android/quicksearchbox/ui/SuggestionsAdapter;

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->createSuggestionsPromoter()Lcom/android/quicksearchbox/Promoter;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/android/quicksearchbox/ui/SuggestionsAdapter;->setPromoter(Lcom/android/quicksearchbox/Promoter;)V

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->getSuggestions()Lcom/android/quicksearchbox/Suggestions;

    move-result-object v0

    if-eqz p1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/android/quicksearchbox/Suggestions;->expectsCorpus(Lcom/android/quicksearchbox/Corpus;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->getActivity()Lcom/android/quicksearchbox/SearchActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/quicksearchbox/SearchActivity;->updateSuggestions()V

    :cond_1
    return-void
.end method

.method public setCorpus(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/android/quicksearchbox/ui/SearchActivityView;->getCorpus(Ljava/lang/String;)Lcom/android/quicksearchbox/Corpus;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->setCorpus(Lcom/android/quicksearchbox/Corpus;)V

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->updateUi()V

    return-void
.end method

.method public setExitClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1    # Landroid/view/View$OnClickListener;

    iput-object p1, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mExitClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public setMaxPromotedResults(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public setMaxPromotedSuggestions(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mSuggestionsView:Lcom/android/quicksearchbox/ui/SuggestionsListView;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/android/quicksearchbox/ui/SuggestionsListView;->setLimitSuggestionsToViewHeight(Z)V

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mSuggestionsAdapter:Lcom/android/quicksearchbox/ui/SuggestionsAdapter;

    invoke-interface {v0, p1}, Lcom/android/quicksearchbox/ui/SuggestionsAdapter;->setMaxPromoted(I)V

    return-void
.end method

.method public setQuery(Ljava/lang/String;Z)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mUpdateSuggestions:Z

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mQueryTextView:Lcom/android/quicksearchbox/ui/QueryTextView;

    invoke-virtual {v0, p1}, Lcom/android/quicksearchbox/ui/QueryTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mQueryTextView:Lcom/android/quicksearchbox/ui/QueryTextView;

    invoke-virtual {v0, p2}, Lcom/android/quicksearchbox/ui/QueryTextView;->setTextSelection(Z)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mUpdateSuggestions:Z

    return-void
.end method

.method public setQueryListener(Lcom/android/quicksearchbox/ui/SearchActivityView$QueryListener;)V
    .locals 0
    .param p1    # Lcom/android/quicksearchbox/ui/SearchActivityView$QueryListener;

    iput-object p1, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mQueryListener:Lcom/android/quicksearchbox/ui/SearchActivityView$QueryListener;

    return-void
.end method

.method public setSearchClickListener(Lcom/android/quicksearchbox/ui/SearchActivityView$SearchClickListener;)V
    .locals 0
    .param p1    # Lcom/android/quicksearchbox/ui/SearchActivityView$SearchClickListener;

    iput-object p1, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mSearchClickListener:Lcom/android/quicksearchbox/ui/SearchActivityView$SearchClickListener;

    return-void
.end method

.method public setSuggestionClickListener(Lcom/android/quicksearchbox/ui/SuggestionClickListener;)V
    .locals 2
    .param p1    # Lcom/android/quicksearchbox/ui/SuggestionClickListener;

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mSuggestionsAdapter:Lcom/android/quicksearchbox/ui/SuggestionsAdapter;

    invoke-interface {v0, p1}, Lcom/android/quicksearchbox/ui/SuggestionsAdapter;->setSuggestionClickListener(Lcom/android/quicksearchbox/ui/SuggestionClickListener;)V

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mQueryTextView:Lcom/android/quicksearchbox/ui/QueryTextView;

    new-instance v1, Lcom/android/quicksearchbox/ui/SearchActivityView$1;

    invoke-direct {v1, p0}, Lcom/android/quicksearchbox/ui/SearchActivityView$1;-><init>(Lcom/android/quicksearchbox/ui/SearchActivityView;)V

    invoke-virtual {v0, v1}, Lcom/android/quicksearchbox/ui/QueryTextView;->setCommitCompletionListener(Lcom/android/quicksearchbox/ui/QueryTextView$CommitCompletionListener;)V

    return-void
.end method

.method public setSuggestions(Lcom/android/quicksearchbox/Suggestions;)V
    .locals 1
    .param p1    # Lcom/android/quicksearchbox/Suggestions;

    invoke-virtual {p1}, Lcom/android/quicksearchbox/Suggestions;->acquire()V

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mSuggestionsAdapter:Lcom/android/quicksearchbox/ui/SuggestionsAdapter;

    invoke-interface {v0, p1}, Lcom/android/quicksearchbox/ui/SuggestionsAdapter;->setSuggestions(Lcom/android/quicksearchbox/Suggestions;)V

    return-void
.end method

.method public setVoiceSearchButtonClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mVoiceSearchButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mVoiceSearchButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method protected shouldShowVoiceSearch(Z)Z
    .locals 0
    .param p1    # Z

    return p1
.end method

.method public abstract showCorpusSelectionDialog()V
.end method

.method public showInputMethodForQuery()V
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mQueryTextView:Lcom/android/quicksearchbox/ui/QueryTextView;

    invoke-virtual {v0}, Lcom/android/quicksearchbox/ui/QueryTextView;->showInputMethod()V

    return-void
.end method

.method public start()V
    .locals 2

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mSuggestionsAdapter:Lcom/android/quicksearchbox/ui/SuggestionsAdapter;

    invoke-interface {v0}, Lcom/android/quicksearchbox/ui/SuggestionsAdapter;->getListAdapter()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    new-instance v1, Lcom/android/quicksearchbox/ui/SearchActivityView$SuggestionsObserver;

    invoke-direct {v1, p0}, Lcom/android/quicksearchbox/ui/SearchActivityView$SuggestionsObserver;-><init>(Lcom/android/quicksearchbox/ui/SearchActivityView;)V

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mSuggestionsView:Lcom/android/quicksearchbox/ui/SuggestionsListView;

    iget-object v1, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mSuggestionsAdapter:Lcom/android/quicksearchbox/ui/SuggestionsAdapter;

    invoke-interface {v0, v1}, Lcom/android/quicksearchbox/ui/SuggestionsListView;->setSuggestionsAdapter(Lcom/android/quicksearchbox/ui/SuggestionsAdapter;)V

    return-void
.end method

.method protected updateInputMethodSuggestions()V
    .locals 5

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "input_method"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodManager;->isFullscreenMode()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mSuggestionsAdapter:Lcom/android/quicksearchbox/ui/SuggestionsAdapter;

    invoke-interface {v3}, Lcom/android/quicksearchbox/ui/SuggestionsAdapter;->getSuggestions()Lcom/android/quicksearchbox/Suggestions;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-direct {p0, v2}, Lcom/android/quicksearchbox/ui/SearchActivityView;->webSuggestionsToCompletions(Lcom/android/quicksearchbox/Suggestions;)[Landroid/view/inputmethod/CompletionInfo;

    move-result-object v0

    iget-object v3, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mQueryTextView:Lcom/android/quicksearchbox/ui/QueryTextView;

    invoke-virtual {v1, v3, v0}, Landroid/view/inputmethod/InputMethodManager;->displayCompletions(Landroid/view/View;[Landroid/view/inputmethod/CompletionInfo;)V

    goto :goto_0
.end method

.method protected updateQueryTextView(Z)V
    .locals 3
    .param p1    # Z

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->isSearchCorpusWeb()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mQueryTextView:Lcom/android/quicksearchbox/ui/QueryTextView;

    iget-object v2, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mQueryTextEmptyBg:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Lcom/android/quicksearchbox/ui/QueryTextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mQueryTextView:Lcom/android/quicksearchbox/ui/QueryTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/android/quicksearchbox/ui/QueryTextView;->setHint(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mQueryTextNotEmptyBg:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020053    # com.android.quicksearchbox.R.drawable.textfield_search_empty

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mQueryTextNotEmptyBg:Landroid/graphics/drawable/Drawable;

    :cond_1
    iget-object v1, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mQueryTextView:Lcom/android/quicksearchbox/ui/QueryTextView;

    iget-object v2, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mQueryTextNotEmptyBg:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Lcom/android/quicksearchbox/ui/QueryTextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->getCorpus()Lcom/android/quicksearchbox/Corpus;

    move-result-object v0

    iget-object v2, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mQueryTextView:Lcom/android/quicksearchbox/ui/QueryTextView;

    if-nez v0, :cond_2

    const-string v1, ""

    :goto_1
    invoke-virtual {v2, v1}, Lcom/android/quicksearchbox/ui/QueryTextView;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    invoke-interface {v0}, Lcom/android/quicksearchbox/Corpus;->getHint()Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mQueryTextView:Lcom/android/quicksearchbox/ui/QueryTextView;

    const v2, 0x7f020051    # com.android.quicksearchbox.R.drawable.textfield_search

    invoke-virtual {v1, v2}, Lcom/android/quicksearchbox/ui/QueryTextView;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method protected updateUi()V
    .locals 1

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->isQueryEmpty()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->updateUi(Z)V

    return-void
.end method

.method protected updateUi(Z)V
    .locals 0
    .param p1    # Z

    invoke-virtual {p0, p1}, Lcom/android/quicksearchbox/ui/SearchActivityView;->updateQueryTextView(Z)V

    invoke-direct {p0, p1}, Lcom/android/quicksearchbox/ui/SearchActivityView;->updateSearchGoButton(Z)V

    invoke-virtual {p0, p1}, Lcom/android/quicksearchbox/ui/SearchActivityView;->updateVoiceSearchButton(Z)V

    return-void
.end method

.method protected updateVoiceSearchButton(Z)V
    .locals 2
    .param p1    # Z

    invoke-virtual {p0, p1}, Lcom/android/quicksearchbox/ui/SearchActivityView;->shouldShowVoiceSearch(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->getVoiceSearch()Lcom/android/quicksearchbox/VoiceSearch;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ui/SearchActivityView;->getCorpus()Lcom/android/quicksearchbox/Corpus;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/quicksearchbox/VoiceSearch;->shouldShowVoiceSearch(Lcom/android/quicksearchbox/Corpus;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mVoiceSearchButton:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mQueryTextView:Lcom/android/quicksearchbox/ui/QueryTextView;

    const-string v1, "nm"

    invoke-virtual {v0, v1}, Lcom/android/quicksearchbox/ui/QueryTextView;->setPrivateImeOptions(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mVoiceSearchButton:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/android/quicksearchbox/ui/SearchActivityView;->mQueryTextView:Lcom/android/quicksearchbox/ui/QueryTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/quicksearchbox/ui/QueryTextView;->setPrivateImeOptions(Ljava/lang/String;)V

    goto :goto_0
.end method
