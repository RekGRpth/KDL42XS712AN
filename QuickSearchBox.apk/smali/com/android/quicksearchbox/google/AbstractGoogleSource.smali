.class public abstract Lcom/android/quicksearchbox/google/AbstractGoogleSource;
.super Lcom/android/quicksearchbox/AbstractInternalSource;
.source "AbstractGoogleSource.java"

# interfaces
.implements Lcom/android/quicksearchbox/google/GoogleSource;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/quicksearchbox/util/NamedTaskExecutor;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/os/Handler;
    .param p3    # Lcom/android/quicksearchbox/util/NamedTaskExecutor;

    invoke-direct {p0, p1, p2, p3}, Lcom/android/quicksearchbox/AbstractInternalSource;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/android/quicksearchbox/util/NamedTaskExecutor;)V

    return-void
.end method

.method private emptyIfNull(Lcom/android/quicksearchbox/SourceResult;Ljava/lang/String;)Lcom/android/quicksearchbox/SourceResult;
    .locals 0
    .param p1    # Lcom/android/quicksearchbox/SourceResult;
    .param p2    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance p1, Lcom/android/quicksearchbox/CursorBackedSourceResult;

    invoke-direct {p1, p0, p2}, Lcom/android/quicksearchbox/CursorBackedSourceResult;-><init>(Lcom/android/quicksearchbox/Source;Ljava/lang/String;)V

    :cond_0
    return-object p1
.end method


# virtual methods
.method public createVoiceSearchIntent(Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-virtual {p0, p1}, Lcom/android/quicksearchbox/google/AbstractGoogleSource;->createVoiceWebSearchIntent(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultIntentAction()Ljava/lang/String;
    .locals 1

    const-string v0, "android.intent.action.WEB_SEARCH"

    return-object v0
.end method

.method public getHint()Ljava/lang/CharSequence;
    .locals 2

    invoke-virtual {p0}, Lcom/android/quicksearchbox/google/AbstractGoogleSource;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f090021    # com.android.quicksearchbox.R.string.google_search_hint

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLabel()Ljava/lang/CharSequence;
    .locals 2

    invoke-virtual {p0}, Lcom/android/quicksearchbox/google/AbstractGoogleSource;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f09001f    # com.android.quicksearchbox.R.string.google_search_label

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMaxShortcuts(Lcom/android/quicksearchbox/Config;)I
    .locals 1
    .param p1    # Lcom/android/quicksearchbox/Config;

    invoke-virtual {p1}, Lcom/android/quicksearchbox/Config;->getMaxShortcutsPerWebSource()I

    move-result v0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "com.android.quicksearchbox/.google.GoogleSearch"

    return-object v0
.end method

.method public getSettingsDescription()Ljava/lang/CharSequence;
    .locals 2

    invoke-virtual {p0}, Lcom/android/quicksearchbox/google/AbstractGoogleSource;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f090022    # com.android.quicksearchbox.R.string.google_search_description

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getSourceIconResource()I
    .locals 1

    const/high16 v0, 0x7f030000    # com.android.quicksearchbox.R.mipmap.google_icon

    return v0
.end method

.method public getSuggestions(Ljava/lang/String;IZ)Lcom/android/quicksearchbox/SourceResult;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Z

    invoke-virtual {p0, p1}, Lcom/android/quicksearchbox/google/AbstractGoogleSource;->queryInternal(Ljava/lang/String;)Lcom/android/quicksearchbox/SourceResult;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/android/quicksearchbox/google/AbstractGoogleSource;->emptyIfNull(Lcom/android/quicksearchbox/SourceResult;Ljava/lang/String;)Lcom/android/quicksearchbox/SourceResult;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getSuggestions(Ljava/lang/String;IZ)Lcom/android/quicksearchbox/SuggestionCursor;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Z

    invoke-virtual {p0, p1, p2, p3}, Lcom/android/quicksearchbox/google/AbstractGoogleSource;->getSuggestions(Ljava/lang/String;IZ)Lcom/android/quicksearchbox/SourceResult;

    move-result-object v0

    return-object v0
.end method

.method public includeInAll()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public abstract queryInternal(Ljava/lang/String;)Lcom/android/quicksearchbox/SourceResult;
.end method

.method public voiceSearchEnabled()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
