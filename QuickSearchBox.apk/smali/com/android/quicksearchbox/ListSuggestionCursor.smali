.class public Lcom/android/quicksearchbox/ListSuggestionCursor;
.super Lcom/android/quicksearchbox/AbstractSuggestionCursorWrapper;
.source "ListSuggestionCursor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/quicksearchbox/ListSuggestionCursor$Entry;
    }
.end annotation


# instance fields
.field private final mDataSetObservable:Landroid/database/DataSetObservable;

.field private mExtraColumns:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mPos:I

.field private final mSuggestions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/quicksearchbox/ListSuggestionCursor$Entry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const/16 v0, 0x10

    invoke-direct {p0, p1, v0}, Lcom/android/quicksearchbox/ListSuggestionCursor;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-direct {p0, p1}, Lcom/android/quicksearchbox/AbstractSuggestionCursorWrapper;-><init>(Ljava/lang/String;)V

    new-instance v0, Landroid/database/DataSetObservable;

    invoke-direct {v0}, Landroid/database/DataSetObservable;-><init>()V

    iput-object v0, p0, Lcom/android/quicksearchbox/ListSuggestionCursor;->mDataSetObservable:Landroid/database/DataSetObservable;

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/quicksearchbox/ListSuggestionCursor;->mPos:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/android/quicksearchbox/ListSuggestionCursor;->mSuggestions:Ljava/util/ArrayList;

    return-void
.end method

.method public varargs constructor <init>(Ljava/lang/String;[Lcom/android/quicksearchbox/Suggestion;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # [Lcom/android/quicksearchbox/Suggestion;

    array-length v4, p2

    invoke-direct {p0, p1, v4}, Lcom/android/quicksearchbox/ListSuggestionCursor;-><init>(Ljava/lang/String;I)V

    move-object v0, p2

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    invoke-virtual {p0, v3}, Lcom/android/quicksearchbox/ListSuggestionCursor;->add(Lcom/android/quicksearchbox/Suggestion;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public add(Lcom/android/quicksearchbox/Suggestion;)Z
    .locals 2
    .param p1    # Lcom/android/quicksearchbox/Suggestion;

    iget-object v0, p0, Lcom/android/quicksearchbox/ListSuggestionCursor;->mSuggestions:Ljava/util/ArrayList;

    new-instance v1, Lcom/android/quicksearchbox/ListSuggestionCursor$Entry;

    invoke-direct {v1, p1}, Lcom/android/quicksearchbox/ListSuggestionCursor$Entry;-><init>(Lcom/android/quicksearchbox/Suggestion;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    return v0
.end method

.method public close()V
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/ListSuggestionCursor;->mSuggestions:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method protected current()Lcom/android/quicksearchbox/Suggestion;
    .locals 2

    iget-object v0, p0, Lcom/android/quicksearchbox/ListSuggestionCursor;->mSuggestions:Ljava/util/ArrayList;

    iget v1, p0, Lcom/android/quicksearchbox/ListSuggestionCursor;->mPos:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/quicksearchbox/ListSuggestionCursor$Entry;

    invoke-virtual {v0}, Lcom/android/quicksearchbox/ListSuggestionCursor$Entry;->get()Lcom/android/quicksearchbox/Suggestion;

    move-result-object v0

    return-object v0
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/ListSuggestionCursor;->mSuggestions:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getExtraColumns()Ljava/util/Collection;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/android/quicksearchbox/ListSuggestionCursor;->mExtraColumns:Ljava/util/HashSet;

    if-nez v7, :cond_2

    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    iput-object v7, p0, Lcom/android/quicksearchbox/ListSuggestionCursor;->mExtraColumns:Ljava/util/HashSet;

    iget-object v7, p0, Lcom/android/quicksearchbox/ListSuggestionCursor;->mSuggestions:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/quicksearchbox/ListSuggestionCursor$Entry;

    invoke-virtual {v1}, Lcom/android/quicksearchbox/ListSuggestionCursor$Entry;->getExtras()Lcom/android/quicksearchbox/SuggestionExtras;

    move-result-object v3

    if-nez v3, :cond_1

    move-object v2, v6

    :goto_0
    if-eqz v2, :cond_0

    invoke-interface {v3}, Lcom/android/quicksearchbox/SuggestionExtras;->getExtraColumnNames()Ljava/util/Collection;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v7, p0, Lcom/android/quicksearchbox/ListSuggestionCursor;->mExtraColumns:Ljava/util/HashSet;

    invoke-virtual {v7, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    invoke-interface {v3}, Lcom/android/quicksearchbox/SuggestionExtras;->getExtraColumnNames()Ljava/util/Collection;

    move-result-object v2

    goto :goto_0

    :cond_2
    iget-object v7, p0, Lcom/android/quicksearchbox/ListSuggestionCursor;->mExtraColumns:Ljava/util/HashSet;

    invoke-virtual {v7}, Ljava/util/HashSet;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_3

    :goto_2
    return-object v6

    :cond_3
    iget-object v6, p0, Lcom/android/quicksearchbox/ListSuggestionCursor;->mExtraColumns:Ljava/util/HashSet;

    goto :goto_2
.end method

.method public getExtras()Lcom/android/quicksearchbox/SuggestionExtras;
    .locals 2

    iget-object v0, p0, Lcom/android/quicksearchbox/ListSuggestionCursor;->mSuggestions:Ljava/util/ArrayList;

    iget v1, p0, Lcom/android/quicksearchbox/ListSuggestionCursor;->mPos:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/quicksearchbox/ListSuggestionCursor$Entry;

    invoke-virtual {v0}, Lcom/android/quicksearchbox/ListSuggestionCursor$Entry;->getExtras()Lcom/android/quicksearchbox/SuggestionExtras;

    move-result-object v0

    return-object v0
.end method

.method public getPosition()I
    .locals 1

    iget v0, p0, Lcom/android/quicksearchbox/ListSuggestionCursor;->mPos:I

    return v0
.end method

.method public moveTo(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/quicksearchbox/ListSuggestionCursor;->mPos:I

    return-void
.end method

.method public moveToNext()Z
    .locals 3

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/quicksearchbox/ListSuggestionCursor;->mSuggestions:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v2, p0, Lcom/android/quicksearchbox/ListSuggestionCursor;->mPos:I

    if-lt v2, v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget v2, p0, Lcom/android/quicksearchbox/ListSuggestionCursor;->mPos:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/android/quicksearchbox/ListSuggestionCursor;->mPos:I

    iget v2, p0, Lcom/android/quicksearchbox/ListSuggestionCursor;->mPos:I

    if-ge v2, v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method protected notifyDataSetChanged()V
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/ListSuggestionCursor;->mDataSetObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1    # Landroid/database/DataSetObserver;

    iget-object v0, p0, Lcom/android/quicksearchbox/ListSuggestionCursor;->mDataSetObservable:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->registerObserver(Ljava/lang/Object;)V

    return-void
.end method

.method public removeRow()V
    .locals 2

    iget-object v0, p0, Lcom/android/quicksearchbox/ListSuggestionCursor;->mSuggestions:Ljava/util/ArrayList;

    iget v1, p0, Lcom/android/quicksearchbox/ListSuggestionCursor;->mPos:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    return-void
.end method

.method public replaceRow(Lcom/android/quicksearchbox/Suggestion;)V
    .locals 3
    .param p1    # Lcom/android/quicksearchbox/Suggestion;

    iget-object v0, p0, Lcom/android/quicksearchbox/ListSuggestionCursor;->mSuggestions:Ljava/util/ArrayList;

    iget v1, p0, Lcom/android/quicksearchbox/ListSuggestionCursor;->mPos:I

    new-instance v2, Lcom/android/quicksearchbox/ListSuggestionCursor$Entry;

    invoke-direct {v2, p1}, Lcom/android/quicksearchbox/ListSuggestionCursor$Entry;-><init>(Lcom/android/quicksearchbox/Suggestion;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "{["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ListSuggestionCursor;->getUserQuery()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/android/quicksearchbox/ListSuggestionCursor;->mSuggestions:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
