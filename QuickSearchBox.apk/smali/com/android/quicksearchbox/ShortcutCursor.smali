.class public Lcom/android/quicksearchbox/ShortcutCursor;
.super Lcom/android/quicksearchbox/ListSuggestionCursor;
.source "ShortcutCursor.java"


# instance fields
.field private mClosed:Z

.field private final mRefreshed:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/android/quicksearchbox/SuggestionCursor;",
            ">;"
        }
    .end annotation
.end field

.field private final mRefresher:Lcom/android/quicksearchbox/ShortcutRefresher;

.field private final mShortcutRepo:Lcom/android/quicksearchbox/ShortcutRepository;

.field private final mShortcuts:Lcom/android/quicksearchbox/SuggestionCursor;

.field private final mUiThread:Landroid/os/Handler;


# direct methods
.method constructor <init>(Lcom/android/quicksearchbox/SuggestionCursor;)V
    .locals 6
    .param p1    # Lcom/android/quicksearchbox/SuggestionCursor;

    const/4 v3, 0x0

    const/4 v2, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/android/quicksearchbox/ShortcutCursor;-><init>(Lcom/android/quicksearchbox/SuggestionCursor;ZLandroid/os/Handler;Lcom/android/quicksearchbox/ShortcutRefresher;Lcom/android/quicksearchbox/ShortcutRepository;)V

    return-void
.end method

.method public constructor <init>(Lcom/android/quicksearchbox/SuggestionCursor;ZLandroid/os/Handler;Lcom/android/quicksearchbox/ShortcutRefresher;Lcom/android/quicksearchbox/ShortcutRepository;)V
    .locals 8
    .param p1    # Lcom/android/quicksearchbox/SuggestionCursor;
    .param p2    # Z
    .param p3    # Landroid/os/Handler;
    .param p4    # Lcom/android/quicksearchbox/ShortcutRefresher;
    .param p5    # Lcom/android/quicksearchbox/ShortcutRepository;

    invoke-interface {p1}, Lcom/android/quicksearchbox/SuggestionCursor;->getUserQuery()Ljava/lang/String;

    move-result-object v1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/android/quicksearchbox/ShortcutCursor;-><init>(Ljava/lang/String;Lcom/android/quicksearchbox/SuggestionCursor;Landroid/os/Handler;Lcom/android/quicksearchbox/ShortcutRefresher;Lcom/android/quicksearchbox/ShortcutRepository;)V

    invoke-interface {p1}, Lcom/android/quicksearchbox/SuggestionCursor;->getCount()I

    move-result v6

    const/4 v7, 0x0

    :goto_0
    if-ge v7, v6, :cond_2

    invoke-interface {p1, v7}, Lcom/android/quicksearchbox/SuggestionCursor;->moveTo(I)V

    invoke-interface {p1}, Lcom/android/quicksearchbox/SuggestionCursor;->getSuggestionSource()Lcom/android/quicksearchbox/Source;

    move-result-object v0

    if-eqz v0, :cond_1

    if-nez p2, :cond_0

    invoke-interface {p1}, Lcom/android/quicksearchbox/SuggestionCursor;->isWebSearchSuggestion()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Lcom/android/quicksearchbox/SuggestionPosition;

    invoke-direct {v0, p1}, Lcom/android/quicksearchbox/SuggestionPosition;-><init>(Lcom/android/quicksearchbox/SuggestionCursor;)V

    invoke-virtual {p0, v0}, Lcom/android/quicksearchbox/ShortcutCursor;->add(Lcom/android/quicksearchbox/Suggestion;)Z

    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Landroid/os/Handler;Lcom/android/quicksearchbox/ShortcutRefresher;Lcom/android/quicksearchbox/ShortcutRepository;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/os/Handler;
    .param p3    # Lcom/android/quicksearchbox/ShortcutRefresher;
    .param p4    # Lcom/android/quicksearchbox/ShortcutRepository;

    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/android/quicksearchbox/ShortcutCursor;-><init>(Ljava/lang/String;Lcom/android/quicksearchbox/SuggestionCursor;Landroid/os/Handler;Lcom/android/quicksearchbox/ShortcutRefresher;Lcom/android/quicksearchbox/ShortcutRepository;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lcom/android/quicksearchbox/SuggestionCursor;Landroid/os/Handler;Lcom/android/quicksearchbox/ShortcutRefresher;Lcom/android/quicksearchbox/ShortcutRepository;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/android/quicksearchbox/SuggestionCursor;
    .param p3    # Landroid/os/Handler;
    .param p4    # Lcom/android/quicksearchbox/ShortcutRefresher;
    .param p5    # Lcom/android/quicksearchbox/ShortcutRepository;

    invoke-direct {p0, p1}, Lcom/android/quicksearchbox/ListSuggestionCursor;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/quicksearchbox/ShortcutCursor;->mClosed:Z

    iput-object p2, p0, Lcom/android/quicksearchbox/ShortcutCursor;->mShortcuts:Lcom/android/quicksearchbox/SuggestionCursor;

    iput-object p3, p0, Lcom/android/quicksearchbox/ShortcutCursor;->mUiThread:Landroid/os/Handler;

    iput-object p4, p0, Lcom/android/quicksearchbox/ShortcutCursor;->mRefresher:Lcom/android/quicksearchbox/ShortcutRefresher;

    iput-object p5, p0, Lcom/android/quicksearchbox/ShortcutCursor;->mShortcutRepo:Lcom/android/quicksearchbox/ShortcutRepository;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/quicksearchbox/ShortcutCursor;->mRefreshed:Ljava/util/HashSet;

    return-void
.end method

.method static synthetic access$000(Lcom/android/quicksearchbox/ShortcutCursor;)Lcom/android/quicksearchbox/ShortcutRepository;
    .locals 1
    .param p0    # Lcom/android/quicksearchbox/ShortcutCursor;

    iget-object v0, p0, Lcom/android/quicksearchbox/ShortcutCursor;->mShortcutRepo:Lcom/android/quicksearchbox/ShortcutRepository;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/quicksearchbox/ShortcutCursor;Lcom/android/quicksearchbox/Source;Ljava/lang/String;Lcom/android/quicksearchbox/SuggestionCursor;)V
    .locals 0
    .param p0    # Lcom/android/quicksearchbox/ShortcutCursor;
    .param p1    # Lcom/android/quicksearchbox/Source;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/android/quicksearchbox/SuggestionCursor;

    invoke-direct {p0, p1, p2, p3}, Lcom/android/quicksearchbox/ShortcutCursor;->refresh(Lcom/android/quicksearchbox/Source;Ljava/lang/String;Lcom/android/quicksearchbox/SuggestionCursor;)V

    return-void
.end method

.method static synthetic access$200(Lcom/android/quicksearchbox/ShortcutCursor;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/quicksearchbox/ShortcutCursor;

    iget-object v0, p0, Lcom/android/quicksearchbox/ShortcutCursor;->mUiThread:Landroid/os/Handler;

    return-object v0
.end method

.method private refresh(Lcom/android/quicksearchbox/Source;Ljava/lang/String;Lcom/android/quicksearchbox/SuggestionCursor;)V
    .locals 2
    .param p1    # Lcom/android/quicksearchbox/Source;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/android/quicksearchbox/SuggestionCursor;

    iget-boolean v1, p0, Lcom/android/quicksearchbox/ShortcutCursor;->mClosed:Z

    if-eqz v1, :cond_1

    if-eqz p3, :cond_0

    invoke-interface {p3}, Lcom/android/quicksearchbox/SuggestionCursor;->close()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p3, :cond_2

    iget-object v1, p0, Lcom/android/quicksearchbox/ShortcutCursor;->mRefreshed:Ljava/util/HashSet;

    invoke-virtual {v1, p3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_2
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p0}, Lcom/android/quicksearchbox/ShortcutCursor;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/android/quicksearchbox/ShortcutCursor;->moveTo(I)V

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ShortcutCursor;->getShortcutId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ShortcutCursor;->getSuggestionSource()Lcom/android/quicksearchbox/Source;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    if-eqz p3, :cond_3

    invoke-interface {p3}, Lcom/android/quicksearchbox/SuggestionCursor;->getCount()I

    move-result v1

    if-lez v1, :cond_3

    new-instance v1, Lcom/android/quicksearchbox/SuggestionPosition;

    invoke-direct {v1, p3}, Lcom/android/quicksearchbox/SuggestionPosition;-><init>(Lcom/android/quicksearchbox/SuggestionCursor;)V

    invoke-virtual {p0, v1}, Lcom/android/quicksearchbox/ShortcutCursor;->replaceRow(Lcom/android/quicksearchbox/Suggestion;)V

    :goto_2
    invoke-virtual {p0}, Lcom/android/quicksearchbox/ShortcutCursor;->notifyDataSetChanged()V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/android/quicksearchbox/ShortcutCursor;->removeRow()V

    goto :goto_2

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public close()V
    .locals 4

    iget-boolean v2, p0, Lcom/android/quicksearchbox/ShortcutCursor;->mClosed:Z

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "double close"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/android/quicksearchbox/ShortcutCursor;->mClosed:Z

    iget-object v2, p0, Lcom/android/quicksearchbox/ShortcutCursor;->mShortcuts:Lcom/android/quicksearchbox/SuggestionCursor;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/android/quicksearchbox/ShortcutCursor;->mShortcuts:Lcom/android/quicksearchbox/SuggestionCursor;

    invoke-interface {v2}, Lcom/android/quicksearchbox/SuggestionCursor;->close()V

    :cond_1
    iget-object v2, p0, Lcom/android/quicksearchbox/ShortcutCursor;->mRefreshed:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/quicksearchbox/SuggestionCursor;

    invoke-interface {v0}, Lcom/android/quicksearchbox/SuggestionCursor;->close()V

    goto :goto_0

    :cond_2
    invoke-super {p0}, Lcom/android/quicksearchbox/ListSuggestionCursor;->close()V

    return-void
.end method

.method public isSuggestionShortcut()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public refresh(Lcom/android/quicksearchbox/Suggestion;)V
    .locals 2
    .param p1    # Lcom/android/quicksearchbox/Suggestion;

    iget-object v0, p0, Lcom/android/quicksearchbox/ShortcutCursor;->mRefresher:Lcom/android/quicksearchbox/ShortcutRefresher;

    new-instance v1, Lcom/android/quicksearchbox/ShortcutCursor$1;

    invoke-direct {v1, p0}, Lcom/android/quicksearchbox/ShortcutCursor$1;-><init>(Lcom/android/quicksearchbox/ShortcutCursor;)V

    invoke-interface {v0, p1, v1}, Lcom/android/quicksearchbox/ShortcutRefresher;->refresh(Lcom/android/quicksearchbox/Suggestion;Lcom/android/quicksearchbox/ShortcutRefresher$Listener;)V

    return-void
.end method
