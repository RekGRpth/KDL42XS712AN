.class public Lcom/android/quicksearchbox/SingleSourceCorpus;
.super Lcom/android/quicksearchbox/AbstractCorpus;
.source "SingleSourceCorpus.java"


# instance fields
.field private final mSource:Lcom/android/quicksearchbox/Source;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/quicksearchbox/Config;Lcom/android/quicksearchbox/Source;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/quicksearchbox/Config;
    .param p3    # Lcom/android/quicksearchbox/Source;

    invoke-direct {p0, p1, p2}, Lcom/android/quicksearchbox/AbstractCorpus;-><init>(Landroid/content/Context;Lcom/android/quicksearchbox/Config;)V

    iput-object p3, p0, Lcom/android/quicksearchbox/SingleSourceCorpus;->mSource:Lcom/android/quicksearchbox/Source;

    return-void
.end method


# virtual methods
.method public createSearchIntent(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/os/Bundle;

    iget-object v0, p0, Lcom/android/quicksearchbox/SingleSourceCorpus;->mSource:Lcom/android/quicksearchbox/Source;

    invoke-interface {v0, p1, p2}, Lcom/android/quicksearchbox/Source;->createSearchIntent(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public createVoiceSearchIntent(Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 1
    .param p1    # Landroid/os/Bundle;

    iget-object v0, p0, Lcom/android/quicksearchbox/SingleSourceCorpus;->mSource:Lcom/android/quicksearchbox/Source;

    invoke-interface {v0, p1}, Lcom/android/quicksearchbox/Source;->createVoiceSearchIntent(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public getCorpusIcon()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/SingleSourceCorpus;->mSource:Lcom/android/quicksearchbox/Source;

    invoke-interface {v0}, Lcom/android/quicksearchbox/Source;->getSourceIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public getHint()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/SingleSourceCorpus;->mSource:Lcom/android/quicksearchbox/Source;

    invoke-interface {v0}, Lcom/android/quicksearchbox/Source;->getHint()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getLabel()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/SingleSourceCorpus;->mSource:Lcom/android/quicksearchbox/Source;

    invoke-interface {v0}, Lcom/android/quicksearchbox/Source;->getLabel()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/SingleSourceCorpus;->mSource:Lcom/android/quicksearchbox/Source;

    invoke-interface {v0}, Lcom/android/quicksearchbox/Source;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getQueryThreshold()I
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/SingleSourceCorpus;->mSource:Lcom/android/quicksearchbox/Source;

    invoke-interface {v0}, Lcom/android/quicksearchbox/Source;->getQueryThreshold()I

    move-result v0

    return v0
.end method

.method public getSettingsDescription()Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/SingleSourceCorpus;->mSource:Lcom/android/quicksearchbox/Source;

    invoke-interface {v0}, Lcom/android/quicksearchbox/Source;->getSettingsDescription()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getSources()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/quicksearchbox/Source;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/quicksearchbox/SingleSourceCorpus;->mSource:Lcom/android/quicksearchbox/Source;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSuggestions(Ljava/lang/String;IZ)Lcom/android/quicksearchbox/CorpusResult;
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Z

    new-instance v1, Lcom/android/quicksearchbox/LatencyTracker;

    invoke-direct {v1}, Lcom/android/quicksearchbox/LatencyTracker;-><init>()V

    iget-object v3, p0, Lcom/android/quicksearchbox/SingleSourceCorpus;->mSource:Lcom/android/quicksearchbox/Source;

    const/4 v4, 0x1

    invoke-interface {v3, p1, p2, v4}, Lcom/android/quicksearchbox/Source;->getSuggestions(Ljava/lang/String;IZ)Lcom/android/quicksearchbox/SourceResult;

    move-result-object v2

    invoke-virtual {v1}, Lcom/android/quicksearchbox/LatencyTracker;->getLatency()I

    move-result v0

    new-instance v3, Lcom/android/quicksearchbox/SingleSourceCorpusResult;

    invoke-direct {v3, p0, p1, v2, v0}, Lcom/android/quicksearchbox/SingleSourceCorpusResult;-><init>(Lcom/android/quicksearchbox/Corpus;Ljava/lang/String;Lcom/android/quicksearchbox/SuggestionCursor;I)V

    return-object v3
.end method

.method public bridge synthetic getSuggestions(Ljava/lang/String;IZ)Lcom/android/quicksearchbox/SuggestionCursor;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # Z

    invoke-virtual {p0, p1, p2, p3}, Lcom/android/quicksearchbox/SingleSourceCorpus;->getSuggestions(Ljava/lang/String;IZ)Lcom/android/quicksearchbox/CorpusResult;

    move-result-object v0

    return-object v0
.end method

.method public includeInAll()Z
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/SingleSourceCorpus;->mSource:Lcom/android/quicksearchbox/Source;

    invoke-interface {v0}, Lcom/android/quicksearchbox/Source;->includeInAll()Z

    move-result v0

    return v0
.end method

.method public isWebCorpus()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public queryAfterZeroResults()Z
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/SingleSourceCorpus;->mSource:Lcom/android/quicksearchbox/Source;

    invoke-interface {v0}, Lcom/android/quicksearchbox/Source;->queryAfterZeroResults()Z

    move-result v0

    return v0
.end method

.method public voiceSearchEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/SingleSourceCorpus;->mSource:Lcom/android/quicksearchbox/Source;

    invoke-interface {v0}, Lcom/android/quicksearchbox/Source;->voiceSearchEnabled()Z

    move-result v0

    return v0
.end method
