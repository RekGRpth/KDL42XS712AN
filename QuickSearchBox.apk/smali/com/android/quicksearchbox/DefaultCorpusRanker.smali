.class public Lcom/android/quicksearchbox/DefaultCorpusRanker;
.super Ljava/lang/Object;
.source "DefaultCorpusRanker.java"

# interfaces
.implements Lcom/android/quicksearchbox/CorpusRanker;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/quicksearchbox/DefaultCorpusRanker$1;,
        Lcom/android/quicksearchbox/DefaultCorpusRanker$CorpusComparator;,
        Lcom/android/quicksearchbox/DefaultCorpusRanker$RankedCorporaCache;,
        Lcom/android/quicksearchbox/DefaultCorpusRanker$CorporaObserver;
    }
.end annotation


# instance fields
.field private final mCorpora:Lcom/android/quicksearchbox/Corpora;

.field private final mRankedCorpora:Lcom/android/quicksearchbox/DefaultCorpusRanker$RankedCorporaCache;

.field private final mShortcuts:Lcom/android/quicksearchbox/ShortcutRepository;


# direct methods
.method public constructor <init>(Lcom/android/quicksearchbox/Corpora;Lcom/android/quicksearchbox/ShortcutRepository;)V
    .locals 3
    .param p1    # Lcom/android/quicksearchbox/Corpora;
    .param p2    # Lcom/android/quicksearchbox/ShortcutRepository;

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/quicksearchbox/DefaultCorpusRanker;->mCorpora:Lcom/android/quicksearchbox/Corpora;

    iget-object v0, p0, Lcom/android/quicksearchbox/DefaultCorpusRanker;->mCorpora:Lcom/android/quicksearchbox/Corpora;

    new-instance v1, Lcom/android/quicksearchbox/DefaultCorpusRanker$CorporaObserver;

    invoke-direct {v1, p0, v2}, Lcom/android/quicksearchbox/DefaultCorpusRanker$CorporaObserver;-><init>(Lcom/android/quicksearchbox/DefaultCorpusRanker;Lcom/android/quicksearchbox/DefaultCorpusRanker$1;)V

    invoke-interface {v0, v1}, Lcom/android/quicksearchbox/Corpora;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    iput-object p2, p0, Lcom/android/quicksearchbox/DefaultCorpusRanker;->mShortcuts:Lcom/android/quicksearchbox/ShortcutRepository;

    new-instance v0, Lcom/android/quicksearchbox/DefaultCorpusRanker$RankedCorporaCache;

    invoke-direct {v0, p0, v2}, Lcom/android/quicksearchbox/DefaultCorpusRanker$RankedCorporaCache;-><init>(Lcom/android/quicksearchbox/DefaultCorpusRanker;Lcom/android/quicksearchbox/DefaultCorpusRanker$1;)V

    iput-object v0, p0, Lcom/android/quicksearchbox/DefaultCorpusRanker;->mRankedCorpora:Lcom/android/quicksearchbox/DefaultCorpusRanker$RankedCorporaCache;

    return-void
.end method

.method static synthetic access$200(Lcom/android/quicksearchbox/DefaultCorpusRanker;)Lcom/android/quicksearchbox/Corpora;
    .locals 1
    .param p0    # Lcom/android/quicksearchbox/DefaultCorpusRanker;

    iget-object v0, p0, Lcom/android/quicksearchbox/DefaultCorpusRanker;->mCorpora:Lcom/android/quicksearchbox/Corpora;

    return-object v0
.end method

.method static synthetic access$400(Lcom/android/quicksearchbox/DefaultCorpusRanker;)Lcom/android/quicksearchbox/ShortcutRepository;
    .locals 1
    .param p0    # Lcom/android/quicksearchbox/DefaultCorpusRanker;

    iget-object v0, p0, Lcom/android/quicksearchbox/DefaultCorpusRanker;->mShortcuts:Lcom/android/quicksearchbox/ShortcutRepository;

    return-object v0
.end method


# virtual methods
.method public clear()V
    .locals 1

    iget-object v0, p0, Lcom/android/quicksearchbox/DefaultCorpusRanker;->mRankedCorpora:Lcom/android/quicksearchbox/DefaultCorpusRanker$RankedCorporaCache;

    invoke-virtual {v0}, Lcom/android/quicksearchbox/DefaultCorpusRanker$RankedCorporaCache;->clear()V

    return-void
.end method

.method public getCorporaInAll(Lcom/android/quicksearchbox/util/Consumer;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/quicksearchbox/util/Consumer",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/android/quicksearchbox/Corpus;",
            ">;>;)V"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/quicksearchbox/DefaultCorpusRanker;->mRankedCorpora:Lcom/android/quicksearchbox/DefaultCorpusRanker$RankedCorporaCache;

    invoke-virtual {v0, p1}, Lcom/android/quicksearchbox/DefaultCorpusRanker$RankedCorporaCache;->getLater(Lcom/android/quicksearchbox/util/Consumer;)V

    return-void
.end method
