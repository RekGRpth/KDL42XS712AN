.class public Lcom/android/quicksearchbox/Help;
.super Ljava/lang/Object;
.source "Help.java"


# instance fields
.field private final mConfig:Lcom/android/quicksearchbox/Config;

.field private final mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/quicksearchbox/Config;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/quicksearchbox/Config;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/quicksearchbox/Help;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/android/quicksearchbox/Help;->mConfig:Lcom/android/quicksearchbox/Config;

    return-void
.end method

.method private getHelpIntent(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/android/quicksearchbox/Help;->mConfig:Lcom/android/quicksearchbox/Config;

    invoke-virtual {v1, p1}, Lcom/android/quicksearchbox/Config;->getHelpUrl(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_0
.end method


# virtual methods
.method public addHelpMenuItem(Landroid/view/Menu;Ljava/lang/String;)V
    .locals 1
    .param p1    # Landroid/view/Menu;
    .param p2    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/android/quicksearchbox/Help;->addHelpMenuItem(Landroid/view/Menu;Ljava/lang/String;Z)V

    return-void
.end method

.method public addHelpMenuItem(Landroid/view/Menu;Ljava/lang/String;Z)V
    .locals 4
    .param p1    # Landroid/view/Menu;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    invoke-direct {p0, p2}, Lcom/android/quicksearchbox/Help;->getHelpIntent(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Landroid/view/MenuInflater;

    iget-object v3, p0, Lcom/android/quicksearchbox/Help;->mContext:Landroid/content/Context;

    invoke-direct {v1, v3}, Landroid/view/MenuInflater;-><init>(Landroid/content/Context;)V

    const/high16 v3, 0x7f0e0000    # com.android.quicksearchbox.R.menu.help

    invoke-virtual {v1, v3, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    const v3, 0x7f0f002b    # com.android.quicksearchbox.R.id.menu_help

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v0}, Landroid/view/MenuItem;->setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;

    if-eqz p3, :cond_0

    const/4 v3, 0x2

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setShowAsAction(I)V

    :cond_0
    return-void
.end method
