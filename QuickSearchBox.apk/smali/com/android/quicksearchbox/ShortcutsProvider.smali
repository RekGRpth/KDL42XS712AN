.class public Lcom/android/quicksearchbox/ShortcutsProvider;
.super Landroid/content/ContentProvider;
.source "ShortcutsProvider.java"


# instance fields
.field private mUriMatcher:Landroid/content/UriMatcher;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/android/quicksearchbox/ShortcutsProvider;Landroid/content/ComponentName;Landroid/content/ContentValues;)V
    .locals 0
    .param p0    # Lcom/android/quicksearchbox/ShortcutsProvider;
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/content/ContentValues;

    invoke-direct {p0, p1, p2}, Lcom/android/quicksearchbox/ShortcutsProvider;->storeShortcut(Landroid/content/ComponentName;Landroid/content/ContentValues;)V

    return-void
.end method

.method private addShortcut(Landroid/content/ContentValues;)V
    .locals 6
    .param p1    # Landroid/content/ContentValues;

    const-string v3, "shortcut_source"

    invoke-virtual {p1, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "QSB.ExternalShortcutReceiver"

    const-string v4, "Missing shortcut_source"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v3, "suggest_intent_action"

    invoke-virtual {p1, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "android.intent.action.WEB_SEARCH"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v2}, Landroid/content/ComponentName;->unflattenFromString(Ljava/lang/String;)Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/android/quicksearchbox/ShortcutsProvider;->checkCallingPackage(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "QSB.ExternalShortcutReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Got shortcut for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " from a different process"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/android/quicksearchbox/ShortcutsProvider;->getQsbApplication()Lcom/android/quicksearchbox/QsbApplication;

    move-result-object v3

    new-instance v4, Lcom/android/quicksearchbox/ShortcutsProvider$1;

    invoke-direct {v4, p0, v1, p1}, Lcom/android/quicksearchbox/ShortcutsProvider$1;-><init>(Lcom/android/quicksearchbox/ShortcutsProvider;Landroid/content/ComponentName;Landroid/content/ContentValues;)V

    invoke-virtual {v3, v4}, Lcom/android/quicksearchbox/QsbApplication;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private buildUriMatcher()Landroid/content/UriMatcher;
    .locals 4

    invoke-direct {p0}, Lcom/android/quicksearchbox/ShortcutsProvider;->getAuthority()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/UriMatcher;

    const/4 v2, -0x1

    invoke-direct {v1, v2}, Landroid/content/UriMatcher;-><init>(I)V

    const-string v2, "shortcuts"

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    return-object v1
.end method

.method private checkCallingPackage(Ljava/lang/String;)Z
    .locals 9
    .param p1    # Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ShortcutsProvider;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_1

    :cond_0
    :goto_0
    return v7

    :cond_1
    move-object v0, v6

    array-length v3, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_0

    aget-object v5, v0, v2

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v7, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private getAuthority()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ShortcutsProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".shortcuts"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getCorpora()Lcom/android/quicksearchbox/Corpora;
    .locals 1

    invoke-direct {p0}, Lcom/android/quicksearchbox/ShortcutsProvider;->getQsbApplication()Lcom/android/quicksearchbox/QsbApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/quicksearchbox/QsbApplication;->getCorpora()Lcom/android/quicksearchbox/Corpora;

    move-result-object v0

    return-object v0
.end method

.method private getQsbApplication()Lcom/android/quicksearchbox/QsbApplication;
    .locals 1

    invoke-virtual {p0}, Lcom/android/quicksearchbox/ShortcutsProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/android/quicksearchbox/QsbApplication;->get(Landroid/content/Context;)Lcom/android/quicksearchbox/QsbApplication;

    move-result-object v0

    return-object v0
.end method

.method private getShortcutRepository()Lcom/android/quicksearchbox/ShortcutRepository;
    .locals 1

    invoke-direct {p0}, Lcom/android/quicksearchbox/ShortcutsProvider;->getQsbApplication()Lcom/android/quicksearchbox/QsbApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/quicksearchbox/QsbApplication;->getShortcutRepository()Lcom/android/quicksearchbox/ShortcutRepository;

    move-result-object v0

    return-object v0
.end method

.method private makeSuggestion(Lcom/android/quicksearchbox/Source;Landroid/content/ContentValues;)Lcom/android/quicksearchbox/SuggestionData;
    .locals 16
    .param p1    # Lcom/android/quicksearchbox/Source;
    .param p2    # Landroid/content/ContentValues;

    const-string v14, "suggest_format"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v14, "suggest_text_1"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    const-string v14, "suggest_text_2"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    const-string v14, "suggest_text_2_url"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    const-string v14, "suggest_icon_1"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v14, "suggest_icon_2"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v14, "suggest_shortcut_id"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v14, "suggest_spinner_while_refreshing"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/ContentValues;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v14

    const/4 v15, 0x0

    invoke-static {v14, v15}, Lcom/android/quicksearchbox/ShortcutsProvider;->unboxBoolean(Ljava/lang/Boolean;Z)Z

    move-result v9

    const-string v14, "suggest_intent_action"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v14, "suggest_intent_data"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v14, "suggest_intent_extra_data"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v14, "suggest_intent_query"

    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    new-instance v10, Lcom/android/quicksearchbox/SuggestionData;

    move-object/from16 v0, p1

    invoke-direct {v10, v0}, Lcom/android/quicksearchbox/SuggestionData;-><init>(Lcom/android/quicksearchbox/Source;)V

    invoke-virtual {v10, v1}, Lcom/android/quicksearchbox/SuggestionData;->setFormat(Ljava/lang/String;)Lcom/android/quicksearchbox/SuggestionData;

    invoke-virtual {v10, v11}, Lcom/android/quicksearchbox/SuggestionData;->setText1(Ljava/lang/String;)Lcom/android/quicksearchbox/SuggestionData;

    invoke-virtual {v10, v12}, Lcom/android/quicksearchbox/SuggestionData;->setText2(Ljava/lang/String;)Lcom/android/quicksearchbox/SuggestionData;

    invoke-virtual {v10, v13}, Lcom/android/quicksearchbox/SuggestionData;->setText2Url(Ljava/lang/String;)Lcom/android/quicksearchbox/SuggestionData;

    invoke-virtual {v10, v2}, Lcom/android/quicksearchbox/SuggestionData;->setIcon1(Ljava/lang/String;)Lcom/android/quicksearchbox/SuggestionData;

    invoke-virtual {v10, v3}, Lcom/android/quicksearchbox/SuggestionData;->setIcon2(Ljava/lang/String;)Lcom/android/quicksearchbox/SuggestionData;

    invoke-virtual {v10, v8}, Lcom/android/quicksearchbox/SuggestionData;->setShortcutId(Ljava/lang/String;)Lcom/android/quicksearchbox/SuggestionData;

    invoke-virtual {v10, v9}, Lcom/android/quicksearchbox/SuggestionData;->setSpinnerWhileRefreshing(Z)Lcom/android/quicksearchbox/SuggestionData;

    invoke-virtual {v10, v4}, Lcom/android/quicksearchbox/SuggestionData;->setIntentAction(Ljava/lang/String;)Lcom/android/quicksearchbox/SuggestionData;

    invoke-virtual {v10, v5}, Lcom/android/quicksearchbox/SuggestionData;->setIntentData(Ljava/lang/String;)Lcom/android/quicksearchbox/SuggestionData;

    invoke-virtual {v10, v6}, Lcom/android/quicksearchbox/SuggestionData;->setIntentExtraData(Ljava/lang/String;)Lcom/android/quicksearchbox/SuggestionData;

    invoke-virtual {v10, v7}, Lcom/android/quicksearchbox/SuggestionData;->setSuggestionQuery(Ljava/lang/String;)Lcom/android/quicksearchbox/SuggestionData;

    return-object v10
.end method

.method private storeShortcut(Landroid/content/ComponentName;Landroid/content/ContentValues;)V
    .locals 6
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/content/ContentValues;

    invoke-direct {p0}, Lcom/android/quicksearchbox/ShortcutsProvider;->getCorpora()Lcom/android/quicksearchbox/Corpora;

    move-result-object v3

    invoke-virtual {p1}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/android/quicksearchbox/Corpora;->getSource(Ljava/lang/String;)Lcom/android/quicksearchbox/Source;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v3, "QSB.ExternalShortcutReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unknown shortcut source "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const-string v3, "user_query"

    invoke-virtual {p2, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v2, ""

    :cond_1
    new-instance v0, Lcom/android/quicksearchbox/ListSuggestionCursor;

    invoke-direct {v0, v2}, Lcom/android/quicksearchbox/ListSuggestionCursor;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1, p2}, Lcom/android/quicksearchbox/ShortcutsProvider;->makeSuggestion(Lcom/android/quicksearchbox/Source;Landroid/content/ContentValues;)Lcom/android/quicksearchbox/SuggestionData;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/android/quicksearchbox/ListSuggestionCursor;->add(Lcom/android/quicksearchbox/Suggestion;)Z

    invoke-direct {p0}, Lcom/android/quicksearchbox/ShortcutsProvider;->getShortcutRepository()Lcom/android/quicksearchbox/ShortcutRepository;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v0, v4}, Lcom/android/quicksearchbox/ShortcutRepository;->reportClick(Lcom/android/quicksearchbox/SuggestionCursor;I)V

    goto :goto_0
.end method

.method private static unboxBoolean(Ljava/lang/Boolean;Z)Z
    .locals 0
    .param p0    # Ljava/lang/Boolean;
    .param p1    # Z

    if-nez p0, :cond_0

    :goto_0
    return p1

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    goto :goto_0
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Ljava/lang/String;
    .param p3    # [Ljava/lang/String;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3
    .param p1    # Landroid/net/Uri;

    iget-object v0, p0, Lcom/android/quicksearchbox/ShortcutsProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown URI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const-string v0, "vnd.android.cursor.dir/vnd.android.search.suggest"

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 3
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;

    iget-object v0, p0, Lcom/android/quicksearchbox/ShortcutsProvider;->mUriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown URI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    invoke-direct {p0, p2}, Lcom/android/quicksearchbox/ShortcutsProvider;->addShortcut(Landroid/content/ContentValues;)V

    const/4 v0, 0x0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate()Z
    .locals 1

    invoke-direct {p0}, Lcom/android/quicksearchbox/ShortcutsProvider;->buildUriMatcher()Landroid/content/UriMatcher;

    move-result-object v0

    iput-object v0, p0, Lcom/android/quicksearchbox/ShortcutsProvider;->mUriMatcher:Landroid/content/UriMatcher;

    const/4 v0, 0x1

    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # [Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1    # Landroid/net/Uri;
    .param p2    # Landroid/content/ContentValues;
    .param p3    # Ljava/lang/String;
    .param p4    # [Ljava/lang/String;

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
