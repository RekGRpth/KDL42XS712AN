.class public Lcom/android/quicksearchbox/Config;
.super Ljava/lang/Object;
.source "Config.java"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mDefaultCorpora:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDefaultCorporaSuggestUris:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mHiddenCorpora:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/quicksearchbox/Config;->mContext:Landroid/content/Context;

    return-void
.end method

.method private loadResourceStringSet(I)Ljava/util/HashSet;
    .locals 7
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    iget-object v6, p0, Lcom/android/quicksearchbox/Config;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, p1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    array-length v5, v0

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v2, v0, v4

    invoke-virtual {v3, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    return-object v3
.end method


# virtual methods
.method public close()V
    .locals 0

    return-void
.end method

.method public getHelpUrl(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getHttpConnectTimeout()I
    .locals 1

    const/16 v0, 0xfa0

    return v0
.end method

.method public getLatencyLogFrequency()I
    .locals 1

    const/16 v0, 0x3e8

    return v0
.end method

.method public getMaxPromotedResults()I
    .locals 2

    iget-object v0, p0, Lcom/android/quicksearchbox/Config;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0002    # com.android.quicksearchbox.R.integer.max_promoted_results

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    return v0
.end method

.method public getMaxPromotedSuggestions()I
    .locals 2

    iget-object v0, p0, Lcom/android/quicksearchbox/Config;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0001    # com.android.quicksearchbox.R.integer.max_promoted_suggestions

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    return v0
.end method

.method public getMaxResultsPerSource()I
    .locals 1

    const/16 v0, 0x32

    return v0
.end method

.method public getMaxShortcuts(Ljava/lang/String;)I
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0}, Lcom/android/quicksearchbox/Config;->getMaxShortcutsPerNonWebSource()I

    move-result v0

    return v0
.end method

.method public getMaxShortcutsPerNonWebSource()I
    .locals 2

    iget-object v0, p0, Lcom/android/quicksearchbox/Config;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0004    # com.android.quicksearchbox.R.integer.max_shortcuts_per_non_web_source

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    return v0
.end method

.method public getMaxShortcutsPerWebSource()I
    .locals 2

    iget-object v0, p0, Lcom/android/quicksearchbox/Config;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0003    # com.android.quicksearchbox.R.integer.max_shortcuts_per_web_source

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    return v0
.end method

.method public getMaxStatAgeMillis()J
    .locals 2

    const-wide v0, 0x9a7ec800L

    return-wide v0
.end method

.method public getMinClicksForSourceRanking()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public getNumPromotedSources()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public getNumSuggestionsAboveKeyboard()I
    .locals 2

    iget-object v0, p0, Lcom/android/quicksearchbox/Config;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0a0000    # com.android.quicksearchbox.R.integer.num_suggestions_above_keyboard

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    return v0
.end method

.method public getNumWebCorpusThreads()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public getPublishResultDelayMillis()J
    .locals 2

    const-wide/16 v0, 0xc8

    return-wide v0
.end method

.method public getQueryThreadPriority()I
    .locals 1

    const/16 v0, 0x9

    return v0
.end method

.method public getTypingUpdateSuggestionsDelayMillis()J
    .locals 2

    const-wide/16 v0, 0x64

    return-wide v0
.end method

.method public getUserAgent()Ljava/lang/String;
    .locals 1

    const-string v0, "Android/1.0"

    return-object v0
.end method

.method public declared-synchronized isCorpusEnabledByDefault(Lcom/android/quicksearchbox/Corpus;)Z
    .locals 6
    .param p1    # Lcom/android/quicksearchbox/Corpus;

    const/4 v3, 0x1

    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/android/quicksearchbox/Config;->mDefaultCorpora:Ljava/util/HashSet;

    if-nez v4, :cond_0

    const/high16 v4, 0x7f080000    # com.android.quicksearchbox.R.array.default_corpora

    invoke-direct {p0, v4}, Lcom/android/quicksearchbox/Config;->loadResourceStringSet(I)Ljava/util/HashSet;

    move-result-object v4

    iput-object v4, p0, Lcom/android/quicksearchbox/Config;->mDefaultCorpora:Ljava/util/HashSet;

    :cond_0
    iget-object v4, p0, Lcom/android/quicksearchbox/Config;->mDefaultCorpora:Ljava/util/HashSet;

    invoke-interface {p1}, Lcom/android/quicksearchbox/Corpus;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-eqz v4, :cond_1

    :goto_0
    monitor-exit p0

    return v3

    :cond_1
    :try_start_1
    iget-object v4, p0, Lcom/android/quicksearchbox/Config;->mDefaultCorporaSuggestUris:Ljava/util/HashSet;

    if-nez v4, :cond_2

    const v4, 0x7f080001    # com.android.quicksearchbox.R.array.default_corpora_suggest_uris

    invoke-direct {p0, v4}, Lcom/android/quicksearchbox/Config;->loadResourceStringSet(I)Ljava/util/HashSet;

    move-result-object v4

    iput-object v4, p0, Lcom/android/quicksearchbox/Config;->mDefaultCorporaSuggestUris:Ljava/util/HashSet;

    :cond_2
    invoke-interface {p1}, Lcom/android/quicksearchbox/Corpus;->getSources()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/quicksearchbox/Source;

    invoke-interface {v1}, Lcom/android/quicksearchbox/Source;->getSuggestUri()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/android/quicksearchbox/Config;->mDefaultCorporaSuggestUris:Ljava/util/HashSet;

    invoke-virtual {v4, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v4

    if-eqz v4, :cond_3

    goto :goto_0

    :cond_4
    const/4 v3, 0x0

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public declared-synchronized isCorpusHidden(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/quicksearchbox/Config;->mHiddenCorpora:Ljava/util/HashSet;

    if-nez v0, :cond_0

    const v0, 0x7f080002    # com.android.quicksearchbox.R.array.hidden_corpora

    invoke-direct {p0, v0}, Lcom/android/quicksearchbox/Config;->loadResourceStringSet(I)Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/quicksearchbox/Config;->mHiddenCorpora:Ljava/util/HashSet;

    :cond_0
    iget-object v0, p0, Lcom/android/quicksearchbox/Config;->mHiddenCorpora:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public showScrollingResults()Z
    .locals 2

    iget-object v0, p0, Lcom/android/quicksearchbox/Config;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0003    # com.android.quicksearchbox.R.bool.show_scrolling_results

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public showScrollingSuggestions()Z
    .locals 2

    iget-object v0, p0, Lcom/android/quicksearchbox/Config;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0002    # com.android.quicksearchbox.R.bool.show_scrolling_suggestions

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public showShortcutsForZeroQuery()Z
    .locals 2

    iget-object v0, p0, Lcom/android/quicksearchbox/Config;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0001    # com.android.quicksearchbox.R.bool.show_zero_query_shortcuts

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public showSuggestionsForZeroQuery()Z
    .locals 2

    iget-object v0, p0, Lcom/android/quicksearchbox/Config;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v1, 0x7f0b0000    # com.android.quicksearchbox.R.bool.show_zero_query_suggestions

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    return v0
.end method
