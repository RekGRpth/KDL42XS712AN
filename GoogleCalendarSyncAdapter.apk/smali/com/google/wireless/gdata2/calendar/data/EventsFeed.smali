.class public Lcom/google/wireless/gdata2/calendar/data/EventsFeed;
.super Lcom/google/wireless/gdata2/data/Feed;
.source "EventsFeed.java"


# instance fields
.field private timezone:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/wireless/gdata2/data/Feed;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/wireless/gdata2/calendar/data/EventsFeed;->timezone:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getTimezone()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/calendar/data/EventsFeed;->timezone:Ljava/lang/String;

    return-object v0
.end method

.method public setTimezone(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/calendar/data/EventsFeed;->timezone:Ljava/lang/String;

    return-void
.end method
