.class public Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;
.super Lcom/google/wireless/gdata2/data/Entry;
.source "CalendarEntry.java"


# instance fields
.field private accessLevel:B

.field private color:Ljava/lang/String;

.field private eventsUri:Ljava/lang/String;

.field private hidden:Z

.field private overrideName:Ljava/lang/String;

.field private selected:Z

.field private selfUri:Ljava/lang/String;

.field private timezone:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/wireless/gdata2/data/Entry;-><init>()V

    iput-byte v2, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->accessLevel:B

    iput-object v1, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->color:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->hidden:Z

    iput-boolean v2, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->selected:Z

    iput-object v1, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->timezone:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->overrideName:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->selfUri:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->eventsUri:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/wireless/gdata2/data/Entry;->clear()V

    iput-byte v2, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->accessLevel:B

    iput-object v1, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->color:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->hidden:Z

    iput-boolean v2, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->selected:Z

    iput-object v1, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->timezone:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->overrideName:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->selfUri:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->eventsUri:Ljava/lang/String;

    return-void
.end method

.method public getAccessLevel()B
    .locals 1

    iget-byte v0, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->accessLevel:B

    return v0
.end method

.method public getColor()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->color:Ljava/lang/String;

    return-object v0
.end method

.method public getEventsUri()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->eventsUri:Ljava/lang/String;

    return-object v0
.end method

.method public getOverrideName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->overrideName:Ljava/lang/String;

    return-object v0
.end method

.method public getSelfUri()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->selfUri:Ljava/lang/String;

    return-object v0
.end method

.method public getTimezone()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->timezone:Ljava/lang/String;

    return-object v0
.end method

.method public isHidden()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->hidden:Z

    return v0
.end method

.method public isSelected()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->selected:Z

    return v0
.end method

.method public setAccessLevel(B)V
    .locals 0
    .param p1    # B

    iput-byte p1, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->accessLevel:B

    return-void
.end method

.method public setColor(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->color:Ljava/lang/String;

    return-void
.end method

.method public setEventsUri(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->eventsUri:Ljava/lang/String;

    return-void
.end method

.method public setHidden(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->hidden:Z

    return-void
.end method

.method public setOverrideName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->overrideName:Ljava/lang/String;

    return-void
.end method

.method public setSelected(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->selected:Z

    return-void
.end method

.method public setSelfUri(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->selfUri:Ljava/lang/String;

    return-void
.end method

.method public setTimezone(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->timezone:Ljava/lang/String;

    return-void
.end method

.method public toString(Ljava/lang/StringBuffer;)V
    .locals 3
    .param p1    # Ljava/lang/StringBuffer;

    const/16 v2, 0xa

    const-string v0, "ACCESS LEVEL: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-byte v0, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->accessLevel:B

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    const-string v0, "SELF URI"

    iget-object v1, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->selfUri:Ljava/lang/String;

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->appendIfNotNull(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "EDIT URI"

    invoke-virtual {p0}, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->getEditUri()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->appendIfNotNull(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "EVENTS URI"

    iget-object v1, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->eventsUri:Ljava/lang/String;

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->appendIfNotNull(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "COLOR"

    iget-object v1, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->color:Ljava/lang/String;

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->appendIfNotNull(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "HIDDEN: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-boolean v0, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->hidden:Z

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    const-string v0, "SELECTED: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-boolean v0, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->selected:Z

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    const-string v0, "TIMEZONE"

    iget-object v1, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->timezone:Ljava/lang/String;

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->appendIfNotNull(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "OVERRIDE NAME"

    iget-object v1, p0, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->overrideName:Ljava/lang/String;

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/wireless/gdata2/calendar/data/CalendarEntry;->appendIfNotNull(Ljava/lang/StringBuffer;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
