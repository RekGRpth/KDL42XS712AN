.class public Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;
.super Lcom/google/wireless/gdata2/serializer/xml/XmlEntryGDataSerializer;
.source "XmlEventEntryGDataSerializer.java"


# static fields
.field private static final EMPTY_EVENT_ENTRY:Lcom/google/wireless/gdata2/calendar/data/EventEntry;


# instance fields
.field private serializeAttendees:Z

.field private serializeExtendedProperties:Z

.field private serializeReminders:Z

.field private serializeWhens:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, -0x1

    new-instance v0, Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    invoke-direct {v0}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;-><init>()V

    sput-object v0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->EMPTY_EVENT_ENTRY:Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    sget-object v0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->EMPTY_EVENT_ENTRY:Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    invoke-virtual {v0, v1}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->setStatus(B)V

    sget-object v0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->EMPTY_EVENT_ENTRY:Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    invoke-virtual {v0, v1}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->setVisibility(B)V

    sget-object v0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->EMPTY_EVENT_ENTRY:Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    invoke-virtual {v0, v1}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->setTransparency(B)V

    return-void
.end method

.method public constructor <init>(Lcom/google/wireless/gdata2/parser/xml/XmlParserFactory;Lcom/google/wireless/gdata2/calendar/data/EventEntry;)V
    .locals 4
    .param p1    # Lcom/google/wireless/gdata2/parser/xml/XmlParserFactory;
    .param p2    # Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    const/4 v3, 0x1

    const/4 v2, 0x0

    sget-object v1, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->EMPTY_EVENT_ENTRY:Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    invoke-direct {p0, p1, p2, v1}, Lcom/google/wireless/gdata2/serializer/xml/XmlEntryGDataSerializer;-><init>(Lcom/google/wireless/gdata2/parser/xml/XmlParserFactory;Lcom/google/wireless/gdata2/data/Entry;Lcom/google/wireless/gdata2/data/Entry;)V

    iput-boolean v2, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->serializeAttendees:Z

    iput-boolean v2, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->serializeReminders:Z

    iput-boolean v2, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->serializeWhens:Z

    iput-boolean v2, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->serializeExtendedProperties:Z

    iput-boolean v3, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->serializeAttendees:Z

    iput-boolean v3, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->serializeExtendedProperties:Z

    invoke-virtual {p2}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->getRecurrence()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iput-boolean v3, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->serializeReminders:Z

    :goto_0
    return-void

    :cond_0
    iput-boolean v3, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->serializeWhens:Z

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/wireless/gdata2/parser/xml/XmlParserFactory;Lcom/google/wireless/gdata2/calendar/data/EventEntry;Lcom/google/wireless/gdata2/calendar/data/EventEntry;)V
    .locals 15
    .param p1    # Lcom/google/wireless/gdata2/parser/xml/XmlParserFactory;
    .param p2    # Lcom/google/wireless/gdata2/calendar/data/EventEntry;
    .param p3    # Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    if-nez p3, :cond_0

    sget-object p3, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->EMPTY_EVENT_ENTRY:Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    :cond_0
    invoke-direct/range {p0 .. p3}, Lcom/google/wireless/gdata2/serializer/xml/XmlEntryGDataSerializer;-><init>(Lcom/google/wireless/gdata2/parser/xml/XmlParserFactory;Lcom/google/wireless/gdata2/data/Entry;Lcom/google/wireless/gdata2/data/Entry;)V

    const/4 v13, 0x0

    iput-boolean v13, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->serializeAttendees:Z

    const/4 v13, 0x0

    iput-boolean v13, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->serializeReminders:Z

    const/4 v13, 0x0

    iput-boolean v13, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->serializeWhens:Z

    const/4 v13, 0x0

    iput-boolean v13, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->serializeExtendedProperties:Z

    iget-object v0, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->oldEntry:Lcom/google/wireless/gdata2/data/Entry;

    move-object/from16 p3, v0

    check-cast p3, Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->getContent()Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->getContent()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->equals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_1

    sget-object p3, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->EMPTY_EVENT_ENTRY:Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    move-object/from16 v0, p3

    iput-object v0, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->oldEntry:Lcom/google/wireless/gdata2/data/Entry;

    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->getAttendees()Ljava/util/Set;

    move-result-object v1

    iget-object v13, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->oldEntry:Lcom/google/wireless/gdata2/data/Entry;

    check-cast v13, Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    invoke-virtual {v13}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->getAttendees()Ljava/util/Set;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_2

    const-string v13, "gd:who"

    invoke-interface {v4, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v13, 0x1

    iput-boolean v13, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->serializeAttendees:Z

    :cond_2
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->getReminders()Ljava/util/Set;

    move-result-object v11

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->getReminders()Ljava/util/Set;

    move-result-object v8

    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->getRecurrence()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_8

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->getRecurrence()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v10, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    invoke-virtual {v11, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_4

    :cond_3
    const-string v13, "gd:reminder"

    invoke-interface {v4, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v13, "gd:when"

    invoke-interface {v4, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v13, 0x1

    iput-boolean v13, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->serializeReminders:Z

    :cond_4
    :goto_0
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->getExtendedProperties()Ljava/util/Map;

    move-result-object v3

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->getExtendedProperties()Ljava/util/Map;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_5

    const-string v13, "gd:extendedProperty"

    invoke-interface {v4, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v13, 0x1

    iput-boolean v13, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->serializeExtendedProperties:Z

    :cond_5
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->getWhere()Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->getWhere()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->equals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_6

    const-string v13, "gd:where"

    invoke-interface {v4, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    invoke-virtual {p0}, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->isPartial()Z

    move-result v13

    if-eqz v13, :cond_7

    const-string v13, ","

    invoke-static {v13, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->setFields(Ljava/lang/String;)V

    :cond_7
    return-void

    :cond_8
    invoke-virtual/range {p2 .. p2}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->getWhens()Ljava/util/List;

    move-result-object v12

    invoke-virtual/range {p3 .. p3}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->getWhens()Ljava/util/List;

    move-result-object v9

    invoke-virtual {v12, v9}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_9

    invoke-virtual {v11, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_4

    :cond_9
    const-string v13, "gd:reminder"

    invoke-interface {v4, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v13, "gd:when"

    invoke-interface {v4, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v13, 0x1

    iput-boolean v13, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->serializeWhens:Z

    goto :goto_0
.end method

.method private serializeAttendees(Lorg/xmlpull/v1/XmlSerializer;)V
    .locals 4
    .param p1    # Lorg/xmlpull/v1/XmlSerializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/wireless/gdata2/parser/ParseException;
        }
    .end annotation

    iget-boolean v3, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->serializeAttendees:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->entry:Lcom/google/wireless/gdata2/data/Entry;

    check-cast v3, Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    invoke-virtual {v3}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->getAttendees()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/wireless/gdata2/calendar/data/Who;

    invoke-static {p1, v2}, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->serializeWho(Lorg/xmlpull/v1/XmlSerializer;Lcom/google/wireless/gdata2/calendar/data/Who;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static serializeExtendedProperty(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0    # Lorg/xmlpull/v1/XmlSerializer;
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v2, 0x0

    const-string v0, "http://schemas.google.com/g/2005"

    const-string v1, "extendedProperty"

    invoke-interface {p0, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    const-string v0, "name"

    invoke-interface {p0, v2, v0, p1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    const-string v0, "value"

    invoke-interface {p0, v2, v0, p2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    const-string v0, "http://schemas.google.com/g/2005"

    const-string v1, "extendedProperty"

    invoke-interface {p0, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    return-void
.end method

.method private serializeOriginalEvent(Lorg/xmlpull/v1/XmlSerializer;)V
    .locals 9
    .param p1    # Lorg/xmlpull/v1/XmlSerializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v8, 0x0

    iget-object v6, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->entry:Lcom/google/wireless/gdata2/data/Entry;

    check-cast v6, Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    invoke-virtual {v6}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->getOriginalEventId()Ljava/lang/String;

    move-result-object v4

    iget-object v6, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->entry:Lcom/google/wireless/gdata2/data/Entry;

    check-cast v6, Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    invoke-virtual {v6}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->getOriginalEventStartTime()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->oldEntry:Lcom/google/wireless/gdata2/data/Entry;

    check-cast v6, Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    invoke-virtual {v6}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->getOriginalEventId()Ljava/lang/String;

    move-result-object v2

    iget-object v6, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->oldEntry:Lcom/google/wireless/gdata2/data/Entry;

    check-cast v6, Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    invoke-virtual {v6}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->getOriginalEventStartTime()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4}, Lcom/google/wireless/gdata2/data/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-static {v5}, Lcom/google/wireless/gdata2/data/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v6, "http://schemas.google.com/g/2005"

    const-string v7, "originalEvent"

    invoke-interface {p1, v6, v7}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    const/16 v6, 0x2f

    invoke-virtual {v4, v6}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    const/4 v6, -0x1

    if-eq v1, v6, :cond_2

    add-int/lit8 v6, v1, 0x1

    invoke-virtual {v4, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/wireless/gdata2/data/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "id"

    invoke-interface {p1, v8, v6, v0}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    :cond_2
    const-string v6, "href"

    invoke-interface {p1, v8, v6, v4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    const-string v6, "http://schemas.google.com/g/2005"

    const-string v7, "when"

    invoke-interface {p1, v6, v7}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    const-string v6, "startTime"

    invoke-interface {p1, v8, v6, v5}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    const-string v6, "http://schemas.google.com/g/2005"

    const-string v7, "when"

    invoke-interface {p1, v6, v7}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    const-string v6, "http://schemas.google.com/g/2005"

    const-string v7, "originalEvent"

    invoke-interface {p1, v6, v7}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0
.end method

.method private serializeQuickAdd(Lorg/xmlpull/v1/XmlSerializer;)V
    .locals 4
    .param p1    # Lorg/xmlpull/v1/XmlSerializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->entry:Lcom/google/wireless/gdata2/data/Entry;

    check-cast v1, Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    invoke-virtual {v1}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->isQuickAdd()Z

    move-result v0

    iget-object v1, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->oldEntry:Lcom/google/wireless/gdata2/data/Entry;

    check-cast v1, Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    invoke-virtual {v1}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->isQuickAdd()Z

    move-result v1

    if-eq v0, v1, :cond_0

    const-string v1, "gCal"

    const-string v2, "quickadd"

    invoke-interface {p1, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    const/4 v1, 0x0

    const-string v2, "value"

    const-string v3, "true"

    invoke-interface {p1, v1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    const-string v1, "gCal"

    const-string v2, "quickadd"

    invoke-interface {p1, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    :cond_0
    return-void
.end method

.method private serializeRecurrence(Lorg/xmlpull/v1/XmlSerializer;)V
    .locals 3
    .param p1    # Lorg/xmlpull/v1/XmlSerializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->entry:Lcom/google/wireless/gdata2/data/Entry;

    check-cast v1, Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    invoke-virtual {v1}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->getRecurrence()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->oldEntry:Lcom/google/wireless/gdata2/data/Entry;

    check-cast v1, Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    invoke-virtual {v1}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->getRecurrence()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->equals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v1, "http://schemas.google.com/g/2005"

    const-string v2, "recurrence"

    invoke-interface {p1, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    invoke-interface {p1, v0}, Lorg/xmlpull/v1/XmlSerializer;->text(Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    const-string v1, "http://schemas.google.com/g/2005"

    const-string v2, "recurrence"

    invoke-interface {p1, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0
.end method

.method private static serializeReminder(Lorg/xmlpull/v1/XmlSerializer;Lcom/google/wireless/gdata2/calendar/data/Reminder;)V
    .locals 6
    .param p0    # Lorg/xmlpull/v1/XmlSerializer;
    .param p1    # Lcom/google/wireless/gdata2/calendar/data/Reminder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v5, 0x0

    const-string v3, "http://schemas.google.com/g/2005"

    const-string v4, "reminder"

    invoke-interface {p0, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    invoke-virtual {p1}, Lcom/google/wireless/gdata2/calendar/data/Reminder;->getMethod()B

    move-result v0

    const/4 v1, 0x0

    packed-switch v0, :pswitch_data_0

    :goto_0
    if-eqz v1, :cond_0

    const-string v3, "method"

    invoke-interface {p0, v5, v3, v1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    :cond_0
    invoke-virtual {p1}, Lcom/google/wireless/gdata2/calendar/data/Reminder;->getMinutes()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    const-string v3, "minutes"

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p0, v5, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    :cond_1
    const-string v3, "http://schemas.google.com/g/2005"

    const-string v4, "reminder"

    invoke-interface {p0, v3, v4}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    return-void

    :pswitch_0
    const-string v1, "alert"

    goto :goto_0

    :pswitch_1
    const-string v1, "email"

    goto :goto_0

    :pswitch_2
    const-string v1, "sms"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private serializeStatus(Lorg/xmlpull/v1/XmlSerializer;)V
    .locals 4
    .param p1    # Lorg/xmlpull/v1/XmlSerializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v2, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->entry:Lcom/google/wireless/gdata2/data/Entry;

    check-cast v2, Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    invoke-virtual {v2}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->getStatus()B

    move-result v0

    iget-object v2, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->oldEntry:Lcom/google/wireless/gdata2/data/Entry;

    check-cast v2, Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    invoke-virtual {v2}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->getStatus()B

    move-result v2

    if-ne v0, v2, :cond_0

    :goto_0
    return-void

    :cond_0
    packed-switch v0, :pswitch_data_0

    const-string v1, "http://schemas.google.com/g/2005#event.tentative"

    :goto_1
    const-string v2, "http://schemas.google.com/g/2005"

    const-string v3, "eventStatus"

    invoke-interface {p1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    const/4 v2, 0x0

    const-string v3, "value"

    invoke-interface {p1, v2, v3, v1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    const-string v2, "http://schemas.google.com/g/2005"

    const-string v3, "eventStatus"

    invoke-interface {p1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0

    :pswitch_0
    const-string v1, "http://schemas.google.com/g/2005#event.tentative"

    goto :goto_1

    :pswitch_1
    const-string v1, "http://schemas.google.com/g/2005#event.canceled"

    goto :goto_1

    :pswitch_2
    const-string v1, "http://schemas.google.com/g/2005#event.confirmed"

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private serializeTransparency(Lorg/xmlpull/v1/XmlSerializer;)V
    .locals 4
    .param p1    # Lorg/xmlpull/v1/XmlSerializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v2, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->entry:Lcom/google/wireless/gdata2/data/Entry;

    check-cast v2, Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    invoke-virtual {v2}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->getTransparency()B

    move-result v0

    iget-object v2, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->oldEntry:Lcom/google/wireless/gdata2/data/Entry;

    check-cast v2, Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    invoke-virtual {v2}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->getTransparency()B

    move-result v2

    if-ne v0, v2, :cond_0

    :goto_0
    return-void

    :cond_0
    packed-switch v0, :pswitch_data_0

    const-string v1, "http://schemas.google.com/g/2005#event.transparent"

    :goto_1
    const-string v2, "http://schemas.google.com/g/2005"

    const-string v3, "transparency"

    invoke-interface {p1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    const/4 v2, 0x0

    const-string v3, "value"

    invoke-interface {p1, v2, v3, v1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    const-string v2, "http://schemas.google.com/g/2005"

    const-string v3, "transparency"

    invoke-interface {p1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0

    :pswitch_0
    const-string v1, "http://schemas.google.com/g/2005#event.opaque"

    goto :goto_1

    :pswitch_1
    const-string v1, "http://schemas.google.com/g/2005#event.transparent"

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private serializeVisibility(Lorg/xmlpull/v1/XmlSerializer;)V
    .locals 4
    .param p1    # Lorg/xmlpull/v1/XmlSerializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v2, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->entry:Lcom/google/wireless/gdata2/data/Entry;

    check-cast v2, Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    invoke-virtual {v2}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->getVisibility()B

    move-result v0

    iget-object v2, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->oldEntry:Lcom/google/wireless/gdata2/data/Entry;

    check-cast v2, Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    invoke-virtual {v2}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->getVisibility()B

    move-result v2

    if-ne v0, v2, :cond_0

    :goto_0
    return-void

    :cond_0
    packed-switch v0, :pswitch_data_0

    const-string v1, "http://schemas.google.com/g/2005#event.default"

    :goto_1
    const-string v2, "http://schemas.google.com/g/2005"

    const-string v3, "visibility"

    invoke-interface {p1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    const/4 v2, 0x0

    const-string v3, "value"

    invoke-interface {p1, v2, v3, v1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    const-string v2, "http://schemas.google.com/g/2005"

    const-string v3, "visibility"

    invoke-interface {p1, v2, v3}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0

    :pswitch_0
    const-string v1, "http://schemas.google.com/g/2005#event.default"

    goto :goto_1

    :pswitch_1
    const-string v1, "http://schemas.google.com/g/2005#event.confidential"

    goto :goto_1

    :pswitch_2
    const-string v1, "http://schemas.google.com/g/2005#event.private"

    goto :goto_1

    :pswitch_3
    const-string v1, "http://schemas.google.com/g/2005#event.public"

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private serializeWhen(Lorg/xmlpull/v1/XmlSerializer;Lcom/google/wireless/gdata2/calendar/data/When;)V
    .locals 7
    .param p1    # Lorg/xmlpull/v1/XmlSerializer;
    .param p2    # Lcom/google/wireless/gdata2/calendar/data/When;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v6, 0x0

    invoke-virtual {p2}, Lcom/google/wireless/gdata2/calendar/data/When;->getStartTime()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/wireless/gdata2/calendar/data/When;->getEndTime()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/wireless/gdata2/calendar/data/When;->getStartTime()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/wireless/gdata2/data/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string v4, "http://schemas.google.com/g/2005"

    const-string v5, "when"

    invoke-interface {p1, v4, v5}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    const-string v4, "startTime"

    invoke-interface {p1, v6, v4, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    invoke-static {v0}, Lcom/google/wireless/gdata2/data/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "endTime"

    invoke-interface {p1, v6, v4, v0}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    :cond_1
    iget-object v4, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->entry:Lcom/google/wireless/gdata2/data/Entry;

    check-cast v4, Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    invoke-virtual {v4}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->getReminders()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/wireless/gdata2/calendar/data/Reminder;

    invoke-static {p1, v2}, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->serializeReminder(Lorg/xmlpull/v1/XmlSerializer;Lcom/google/wireless/gdata2/calendar/data/Reminder;)V

    goto :goto_1

    :cond_2
    const-string v4, "http://schemas.google.com/g/2005"

    const-string v5, "when"

    invoke-interface {p1, v4, v5}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0
.end method

.method private serializeWhere(Lorg/xmlpull/v1/XmlSerializer;)V
    .locals 3
    .param p1    # Lorg/xmlpull/v1/XmlSerializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget-object v1, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->entry:Lcom/google/wireless/gdata2/data/Entry;

    check-cast v1, Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    invoke-virtual {v1}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->getWhere()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    iget-object v1, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->oldEntry:Lcom/google/wireless/gdata2/data/Entry;

    sget-object v2, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->EMPTY_EVENT_ENTRY:Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->oldEntry:Lcom/google/wireless/gdata2/data/Entry;

    check-cast v1, Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    invoke-virtual {v1}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->getWhere()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->equals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_0
    return-void

    :cond_1
    const-string v1, "http://schemas.google.com/g/2005"

    const-string v2, "where"

    invoke-interface {p1, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    const/4 v1, 0x0

    const-string v2, "valueString"

    invoke-interface {p1, v1, v2, v0}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    const-string v1, "http://schemas.google.com/g/2005"

    const-string v2, "where"

    invoke-interface {p1, v1, v2}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    goto :goto_0
.end method

.method private static serializeWho(Lorg/xmlpull/v1/XmlSerializer;Lcom/google/wireless/gdata2/calendar/data/Who;)V
    .locals 8
    .param p0    # Lorg/xmlpull/v1/XmlSerializer;
    .param p1    # Lcom/google/wireless/gdata2/calendar/data/Who;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/wireless/gdata2/parser/ParseException;
        }
    .end annotation

    const/4 v7, 0x0

    const-string v5, "http://schemas.google.com/g/2005"

    const-string v6, "who"

    invoke-interface {p0, v5, v6}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    invoke-virtual {p1}, Lcom/google/wireless/gdata2/calendar/data/Who;->getEmail()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/wireless/gdata2/data/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "email"

    invoke-interface {p0, v7, v5, v0}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    :cond_0
    invoke-virtual {p1}, Lcom/google/wireless/gdata2/calendar/data/Who;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/wireless/gdata2/data/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "valueString"

    invoke-interface {p0, v7, v5, v4}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/google/wireless/gdata2/calendar/data/Who;->getRelationship()B

    move-result v5

    packed-switch v5, :pswitch_data_0

    new-instance v5, Lcom/google/wireless/gdata2/parser/ParseException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unexpected rel: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/google/wireless/gdata2/calendar/data/Who;->getRelationship()B

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/wireless/gdata2/parser/ParseException;-><init>(Ljava/lang/String;)V

    throw v5

    :pswitch_0
    const-string v1, "http://schemas.google.com/g/2005#event.attendee"

    :goto_0
    :pswitch_1
    invoke-static {v1}, Lcom/google/wireless/gdata2/data/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "rel"

    invoke-interface {p0, v7, v5, v1}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    :cond_2
    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/wireless/gdata2/calendar/data/Who;->getStatus()B

    move-result v5

    packed-switch v5, :pswitch_data_1

    new-instance v5, Lcom/google/wireless/gdata2/parser/ParseException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unexpected status: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/google/wireless/gdata2/calendar/data/Who;->getStatus()B

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/wireless/gdata2/parser/ParseException;-><init>(Ljava/lang/String;)V

    throw v5

    :pswitch_2
    const-string v1, "http://schemas.google.com/g/2005#event.organizer"

    goto :goto_0

    :pswitch_3
    const-string v1, "http://schemas.google.com/g/2005#event.performer"

    goto :goto_0

    :pswitch_4
    const-string v1, "http://schemas.google.com/g/2005#event.speaker"

    goto :goto_0

    :pswitch_5
    const-string v2, "http://schemas.google.com/g/2005#event.accepted"

    :goto_1
    :pswitch_6
    invoke-static {v2}, Lcom/google/wireless/gdata2/data/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    const-string v5, "http://schemas.google.com/g/2005"

    const-string v6, "attendeeStatus"

    invoke-interface {p0, v5, v6}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    const-string v5, "value"

    invoke-interface {p0, v7, v5, v2}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    const-string v5, "http://schemas.google.com/g/2005"

    const-string v6, "attendeeStatus"

    invoke-interface {p0, v5, v6}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    :cond_3
    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/wireless/gdata2/calendar/data/Who;->getType()B

    move-result v5

    packed-switch v5, :pswitch_data_2

    new-instance v5, Lcom/google/wireless/gdata2/parser/ParseException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Unexpected type: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Lcom/google/wireless/gdata2/calendar/data/Who;->getType()B

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/wireless/gdata2/parser/ParseException;-><init>(Ljava/lang/String;)V

    throw v5

    :pswitch_7
    const-string v2, "http://schemas.google.com/g/2005#event.declined"

    goto :goto_1

    :pswitch_8
    const-string v2, "http://schemas.google.com/g/2005#event.invited"

    goto :goto_1

    :pswitch_9
    const-string v2, "http://schemas.google.com/g/2005#event.tentative"

    goto :goto_1

    :pswitch_a
    const-string v3, "http://schemas.google.com/g/2005#event.required"

    :goto_2
    :pswitch_b
    invoke-static {v3}, Lcom/google/wireless/gdata2/data/StringUtils;->isEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    const-string v5, "http://schemas.google.com/g/2005"

    const-string v6, "attendeeType"

    invoke-interface {p0, v5, v6}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    const-string v5, "value"

    invoke-interface {p0, v7, v5, v3}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    const-string v5, "http://schemas.google.com/g/2005"

    const-string v6, "attendeeType"

    invoke-interface {p0, v5, v6}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    :cond_4
    const-string v5, "http://schemas.google.com/g/2005"

    const-string v6, "who"

    invoke-interface {p0, v5, v6}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    return-void

    :pswitch_c
    const-string v3, "http://schemas.google.com/g/2005#event.optional"

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_b
        :pswitch_c
        :pswitch_a
    .end packed-switch
.end method


# virtual methods
.method protected declareExtraEntryNamespaces(Lorg/xmlpull/v1/XmlSerializer;)V
    .locals 2
    .param p1    # Lorg/xmlpull/v1/XmlSerializer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const-string v0, "gCal"

    const-string v1, "http://schemas.google.com/gCal/2005"

    invoke-interface {p1, v0, v1}, Lorg/xmlpull/v1/XmlSerializer;->setPrefix(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public isPartial()Z
    .locals 2

    iget-object v0, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->oldEntry:Lcom/google/wireless/gdata2/data/Entry;

    sget-object v1, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->EMPTY_EVENT_ENTRY:Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected serializeExtraEntryContents(Lorg/xmlpull/v1/XmlSerializer;I)V
    .locals 11
    .param p1    # Lorg/xmlpull/v1/XmlSerializer;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/wireless/gdata2/parser/ParseException;
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->serializeStatus(Lorg/xmlpull/v1/XmlSerializer;)V

    invoke-direct {p0, p1}, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->serializeTransparency(Lorg/xmlpull/v1/XmlSerializer;)V

    invoke-direct {p0, p1}, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->serializeVisibility(Lorg/xmlpull/v1/XmlSerializer;)V

    iget-object v8, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->entry:Lcom/google/wireless/gdata2/data/Entry;

    check-cast v8, Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    invoke-virtual {v8}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->getSendEventNotifications()Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v8, "http://schemas.google.com/gCal/2005"

    const-string v9, "sendEventNotifications"

    invoke-interface {p1, v8, v9}, Lorg/xmlpull/v1/XmlSerializer;->startTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    const/4 v8, 0x0

    const-string v9, "value"

    const-string v10, "true"

    invoke-interface {p1, v8, v9, v10}, Lorg/xmlpull/v1/XmlSerializer;->attribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    const-string v8, "http://schemas.google.com/gCal/2005"

    const-string v9, "sendEventNotifications"

    invoke-interface {p1, v8, v9}, Lorg/xmlpull/v1/XmlSerializer;->endTag(Ljava/lang/String;Ljava/lang/String;)Lorg/xmlpull/v1/XmlSerializer;

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->serializeAttendees(Lorg/xmlpull/v1/XmlSerializer;)V

    invoke-direct {p0, p1}, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->serializeRecurrence(Lorg/xmlpull/v1/XmlSerializer;)V

    iget-boolean v8, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->serializeReminders:Z

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->entry:Lcom/google/wireless/gdata2/data/Entry;

    check-cast v8, Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    invoke-virtual {v8}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->getReminders()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/wireless/gdata2/calendar/data/Reminder;

    invoke-static {p1, v3}, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->serializeReminder(Lorg/xmlpull/v1/XmlSerializer;Lcom/google/wireless/gdata2/calendar/data/Reminder;)V

    goto :goto_0

    :cond_1
    iget-boolean v8, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->serializeWhens:Z

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->entry:Lcom/google/wireless/gdata2/data/Entry;

    check-cast v8, Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    invoke-virtual {v8}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->getWhens()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/wireless/gdata2/calendar/data/When;

    invoke-direct {p0, p1, v6}, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->serializeWhen(Lorg/xmlpull/v1/XmlSerializer;Lcom/google/wireless/gdata2/calendar/data/When;)V

    goto :goto_1

    :cond_2
    invoke-direct {p0, p1}, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->serializeOriginalEvent(Lorg/xmlpull/v1/XmlSerializer;)V

    invoke-direct {p0, p1}, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->serializeWhere(Lorg/xmlpull/v1/XmlSerializer;)V

    iget-boolean v8, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->serializeExtendedProperties:Z

    if-eqz v8, :cond_3

    iget-object v8, p0, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->entry:Lcom/google/wireless/gdata2/data/Entry;

    check-cast v8, Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    invoke-virtual {v8}, Lcom/google/wireless/gdata2/calendar/data/EventEntry;->getExtendedProperties()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    invoke-static {p1, v8, v9}, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->serializeExtendedProperty(Lorg/xmlpull/v1/XmlSerializer;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    invoke-direct {p0, p1}, Lcom/google/wireless/gdata2/calendar/serializer/xml/XmlEventEntryGDataSerializer;->serializeQuickAdd(Lorg/xmlpull/v1/XmlSerializer;)V

    return-void
.end method
