.class public Lcom/google/android/syncadapters/EntityReader$EntryEntityItem;
.super Ljava/lang/Object;
.source "EntityReader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/syncadapters/EntityReader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EntryEntityItem"
.end annotation


# instance fields
.field public final entity:Landroid/content/Entity;

.field public final entry:Lcom/google/wireless/gdata2/data/Entry;

.field public final entryIndex:I


# direct methods
.method public constructor <init>(Lcom/google/wireless/gdata2/data/Entry;ILandroid/content/Entity;)V
    .locals 0
    .param p1    # Lcom/google/wireless/gdata2/data/Entry;
    .param p2    # I
    .param p3    # Landroid/content/Entity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/syncadapters/EntityReader$EntryEntityItem;->entry:Lcom/google/wireless/gdata2/data/Entry;

    iput-object p3, p0, Lcom/google/android/syncadapters/EntityReader$EntryEntityItem;->entity:Landroid/content/Entity;

    iput p2, p0, Lcom/google/android/syncadapters/EntityReader$EntryEntityItem;->entryIndex:I

    return-void
.end method
