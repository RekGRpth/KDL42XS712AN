.class public Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;
.super Ljava/lang/Object;
.source "CalendarSyncAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Operation"
.end annotation


# instance fields
.field private entry:Lcom/google/wireless/gdata2/data/Entry;

.field private etag:Ljava/lang/String;

.field public oldEntry:Lcom/google/wireless/gdata2/data/Entry;

.field private type:I

.field private url:Ljava/lang/String;


# direct methods
.method private constructor <init>(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;->type:I

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;)I
    .locals 1
    .param p0    # Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;

    iget v0, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;->type:I

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;I)I
    .locals 0
    .param p0    # Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;
    .param p1    # I

    iput p1, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;->type:I

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;)Lcom/google/wireless/gdata2/data/Entry;
    .locals 1
    .param p0    # Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;

    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;->entry:Lcom/google/wireless/gdata2/data/Entry;

    return-object v0
.end method

.method static synthetic access$202(Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;Lcom/google/wireless/gdata2/data/Entry;)Lcom/google/wireless/gdata2/data/Entry;
    .locals 0
    .param p0    # Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;
    .param p1    # Lcom/google/wireless/gdata2/data/Entry;

    iput-object p1, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;->entry:Lcom/google/wireless/gdata2/data/Entry;

    return-object p1
.end method

.method static synthetic access$300(Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;

    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;->url:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;

    iget-object v0, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;->etag:Ljava/lang/String;

    return-object v0
.end method

.method public static newDelete(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    if-nez p0, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "null url"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    new-instance v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;-><init>(I)V

    iput-object p0, v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;->url:Ljava/lang/String;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;->entry:Lcom/google/wireless/gdata2/data/Entry;

    iput-object p1, v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;->etag:Ljava/lang/String;

    return-object v0
.end method

.method public static newInsert(Ljava/lang/String;Lcom/google/wireless/gdata2/data/Entry;)Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Lcom/google/wireless/gdata2/data/Entry;

    if-nez p0, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "null url"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    new-instance v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;-><init>(I)V

    iput-object p0, v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;->url:Ljava/lang/String;

    iput-object p1, v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;->entry:Lcom/google/wireless/gdata2/data/Entry;

    return-object v0
.end method

.method public static newUpdate(Ljava/lang/String;Lcom/google/wireless/gdata2/data/Entry;)Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;
    .locals 1
    .param p0    # Ljava/lang/String;
    .param p1    # Lcom/google/wireless/gdata2/data/Entry;

    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;->newUpdate(Ljava/lang/String;Lcom/google/wireless/gdata2/data/Entry;Lcom/google/wireless/gdata2/calendar/data/EventEntry;)Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;

    move-result-object v0

    return-object v0
.end method

.method public static newUpdate(Ljava/lang/String;Lcom/google/wireless/gdata2/data/Entry;Lcom/google/wireless/gdata2/calendar/data/EventEntry;)Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;
    .locals 3
    .param p0    # Ljava/lang/String;
    .param p1    # Lcom/google/wireless/gdata2/data/Entry;
    .param p2    # Lcom/google/wireless/gdata2/calendar/data/EventEntry;

    if-nez p0, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "null url"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    new-instance v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;-><init>(I)V

    iput-object p1, v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;->entry:Lcom/google/wireless/gdata2/data/Entry;

    iput-object p2, v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;->oldEntry:Lcom/google/wireless/gdata2/data/Entry;

    iput-object p0, v0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;->url:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public getType()I
    .locals 1

    iget v0, p0, Lcom/google/android/syncadapters/calendar/CalendarSyncAdapter$Operation;->type:I

    return v0
.end method
