.class public Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;
.super Landroid/database/DataSetObserver;
.source "SectionedAlbumDataAdapter.java"

# interfaces
.implements Landroid/widget/ListAdapter;


# instance fields
.field private final mAlbumData:Lcom/android/dreams/phototable/AlbumDataAdapter;

.field private final mContext:Landroid/content/Context;

.field private final mInflater:Landroid/view/LayoutInflater;

.field private final mLayout:I

.field private sections:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;IILjava/util/List;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/SharedPreferences;
    .param p3    # I
    .param p4    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/content/SharedPreferences;",
            "II",
            "Ljava/util/List",
            "<",
            "Lcom/android/dreams/phototable/PhotoSource$AlbumData;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    iput p3, p0, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->mLayout:I

    iput-object p1, p0, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->mContext:Landroid/content/Context;

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->mInflater:Landroid/view/LayoutInflater;

    new-instance v0, Lcom/android/dreams/phototable/AlbumDataAdapter;

    invoke-direct {v0, p1, p2, p4, p5}, Lcom/android/dreams/phototable/AlbumDataAdapter;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;ILjava/util/List;)V

    iput-object v0, p0, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->mAlbumData:Lcom/android/dreams/phototable/AlbumDataAdapter;

    iget-object v0, p0, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->mAlbumData:Lcom/android/dreams/phototable/AlbumDataAdapter;

    new-instance v1, Lcom/android/dreams/phototable/AlbumDataAdapter$AccountComparator;

    invoke-direct {v1}, Lcom/android/dreams/phototable/AlbumDataAdapter$AccountComparator;-><init>()V

    invoke-virtual {v0, v1}, Lcom/android/dreams/phototable/AlbumDataAdapter;->sort(Ljava/util/Comparator;)V

    invoke-virtual {p0}, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->onChanged()V

    iget-object v0, p0, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->mAlbumData:Lcom/android/dreams/phototable/AlbumDataAdapter;

    invoke-virtual {v0, p0}, Lcom/android/dreams/phototable/AlbumDataAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    return-void
.end method

.method private internalPosition(I)I
    .locals 2
    .param p1    # I

    iget-object v1, p0, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->sections:[I

    invoke-static {v1, p1}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v0

    if-gez v0, :cond_0

    add-int/lit8 v1, v0, 0x1

    neg-int v0, v1

    :cond_0
    sub-int v1, p1, v0

    return v1
.end method

.method private isHeader(I)Z
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->sections:[I

    invoke-static {v0, p1}, Ljava/util/Arrays;->binarySearch([II)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->mAlbumData:Lcom/android/dreams/phototable/AlbumDataAdapter;

    invoke-virtual {v0}, Lcom/android/dreams/phototable/AlbumDataAdapter;->areAllItemsEnabled()Z

    move-result v0

    return v0
.end method

.method public getCount()I
    .locals 2

    iget-object v0, p0, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->mAlbumData:Lcom/android/dreams/phototable/AlbumDataAdapter;

    invoke-virtual {v0}, Lcom/android/dreams/phototable/AlbumDataAdapter;->getCount()I

    move-result v0

    iget-object v1, p0, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->sections:[I

    array-length v1, v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->isHeader(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->mAlbumData:Lcom/android/dreams/phototable/AlbumDataAdapter;

    add-int/lit8 v1, p1, 0x1

    invoke-direct {p0, v1}, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->internalPosition(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/dreams/phototable/AlbumDataAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/dreams/phototable/PhotoSource$AlbumData;

    iget-object v0, v0, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->account:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->mAlbumData:Lcom/android/dreams/phototable/AlbumDataAdapter;

    invoke-direct {p0, p1}, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->internalPosition(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/dreams/phototable/AlbumDataAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 2
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->isHeader(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->mAlbumData:Lcom/android/dreams/phototable/AlbumDataAdapter;

    invoke-virtual {v0}, Lcom/android/dreams/phototable/AlbumDataAdapter;->getViewTypeCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->mAlbumData:Lcom/android/dreams/phototable/AlbumDataAdapter;

    invoke-direct {p0, p1}, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->internalPosition(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/dreams/phototable/AlbumDataAdapter;->getItemViewType(I)I

    move-result v0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    invoke-direct {p0, p1}, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->isHeader(I)Z

    move-result v4

    if-eqz v4, :cond_2

    move-object v0, p2

    if-nez v0, :cond_0

    iget-object v4, p0, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->mInflater:Landroid/view/LayoutInflater;

    iget v5, p0, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->mLayout:I

    const/4 v6, 0x0

    invoke-virtual {v4, v5, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    :cond_0
    const v4, 0x7f0b0004    # com.android.dreams.phototable.R.id.title

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_1

    instance-of v4, v3, Landroid/widget/TextView;

    if-eqz v4, :cond_1

    move-object v2, v3

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    move-object v1, v0

    :goto_0
    return-object v1

    :cond_2
    iget-object v4, p0, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->mAlbumData:Lcom/android/dreams/phototable/AlbumDataAdapter;

    invoke-direct {p0, p1}, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->internalPosition(I)I

    move-result v5

    invoke-virtual {v4, v5, p2, p3}, Lcom/android/dreams/phototable/AlbumDataAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    iget-object v0, p0, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->mAlbumData:Lcom/android/dreams/phototable/AlbumDataAdapter;

    invoke-virtual {v0}, Lcom/android/dreams/phototable/AlbumDataAdapter;->getViewTypeCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    iget-object v0, p0, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->mAlbumData:Lcom/android/dreams/phototable/AlbumDataAdapter;

    invoke-virtual {v0}, Lcom/android/dreams/phototable/AlbumDataAdapter;->hasStableIds()Z

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->mAlbumData:Lcom/android/dreams/phototable/AlbumDataAdapter;

    invoke-virtual {v0}, Lcom/android/dreams/phototable/AlbumDataAdapter;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 2
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->isHeader(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->mAlbumData:Lcom/android/dreams/phototable/AlbumDataAdapter;

    invoke-direct {p0, p1}, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->internalPosition(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/android/dreams/phototable/AlbumDataAdapter;->isEnabled(I)Z

    move-result v0

    goto :goto_0
.end method

.method public onChanged()V
    .locals 6

    const/4 v2, 0x0

    const-string v3, ""

    const/4 v0, 0x0

    :goto_0
    iget-object v4, p0, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->mAlbumData:Lcom/android/dreams/phototable/AlbumDataAdapter;

    invoke-virtual {v4}, Lcom/android/dreams/phototable/AlbumDataAdapter;->getCount()I

    move-result v4

    if-ge v0, v4, :cond_2

    iget-object v4, p0, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->mAlbumData:Lcom/android/dreams/phototable/AlbumDataAdapter;

    invoke-virtual {v4, v0}, Lcom/android/dreams/phototable/AlbumDataAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/dreams/phototable/PhotoSource$AlbumData;

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, v1, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->account:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    iget-object v3, v1, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->account:Ljava/lang/String;

    add-int/lit8 v2, v2, 0x1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    new-array v4, v2, [I

    iput-object v4, p0, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->sections:[I

    const/4 v2, 0x0

    const-string v3, ""

    const/4 v0, 0x0

    :goto_1
    iget-object v4, p0, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->mAlbumData:Lcom/android/dreams/phototable/AlbumDataAdapter;

    invoke-virtual {v4}, Lcom/android/dreams/phototable/AlbumDataAdapter;->getCount()I

    move-result v4

    if-ge v0, v4, :cond_5

    iget-object v4, p0, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->mAlbumData:Lcom/android/dreams/phototable/AlbumDataAdapter;

    invoke-virtual {v4, v0}, Lcom/android/dreams/phototable/AlbumDataAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/dreams/phototable/PhotoSource$AlbumData;

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, v1, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->account:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    :cond_3
    iget-object v3, v1, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->account:Ljava/lang/String;

    iget-object v4, p0, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->sections:[I

    aput v0, v4, v2

    add-int/lit8 v2, v2, 0x1

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    :goto_2
    iget-object v4, p0, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->sections:[I

    array-length v4, v4

    if-ge v0, v4, :cond_6

    iget-object v4, p0, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->sections:[I

    aget v5, v4, v0

    add-int/2addr v5, v0

    aput v5, v4, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_6
    return-void
.end method

.method public onInvalidated()V
    .locals 0

    invoke-virtual {p0}, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->onChanged()V

    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1    # Landroid/database/DataSetObserver;

    iget-object v0, p0, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->mAlbumData:Lcom/android/dreams/phototable/AlbumDataAdapter;

    invoke-virtual {v0, p1}, Lcom/android/dreams/phototable/AlbumDataAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1    # Landroid/database/DataSetObserver;

    iget-object v0, p0, Lcom/android/dreams/phototable/SectionedAlbumDataAdapter;->mAlbumData:Lcom/android/dreams/phototable/AlbumDataAdapter;

    invoke-virtual {v0, p1}, Lcom/android/dreams/phototable/AlbumDataAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    return-void
.end method
