.class public Lcom/android/dreams/phototable/LocalSource;
.super Lcom/android/dreams/phototable/PhotoSource;
.source "LocalSource.java"


# instance fields
.field private mFoundAlbumIds:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mLocalSourceName:Ljava/lang/String;

.field private mNextPosition:I

.field private final mUnknownAlbumName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/SharedPreferences;

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-direct {p0, p1, p2}, Lcom/android/dreams/phototable/PhotoSource;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    iget-object v0, p0, Lcom/android/dreams/phototable/LocalSource;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0c0007    # com.android.dreams.phototable.R.string.local_source_name

    new-array v2, v5, [Ljava/lang/Object;

    const-string v3, "Photos on Device"

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dreams/phototable/LocalSource;->mLocalSourceName:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/dreams/phototable/LocalSource;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0c0005    # com.android.dreams.phototable.R.string.unknown_album_name

    new-array v2, v5, [Ljava/lang/Object;

    const-string v3, "Unknown"

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/dreams/phototable/LocalSource;->mUnknownAlbumName:Ljava/lang/String;

    const-string v0, "PhotoTable.LocalSource"

    iput-object v0, p0, Lcom/android/dreams/phototable/LocalSource;->mSourceName:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/dreams/phototable/LocalSource;->mNextPosition:I

    invoke-virtual {p0}, Lcom/android/dreams/phototable/LocalSource;->fillQueue()V

    return-void
.end method

.method private getFoundAlbums()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/android/dreams/phototable/LocalSource;->mFoundAlbumIds:Ljava/util/Set;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/dreams/phototable/LocalSource;->findAlbums()Ljava/util/Collection;

    :cond_0
    iget-object v0, p0, Lcom/android/dreams/phototable/LocalSource;->mFoundAlbumIds:Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method public findAlbums()Ljava/util/Collection;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/dreams/phototable/PhotoSource$AlbumData;",
            ">;"
        }
    .end annotation

    const-string v1, "PhotoTable.LocalSource"

    const-string v2, "finding albums"

    invoke-static {v1, v2}, Lcom/android/dreams/phototable/LocalSource;->log(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    const/4 v1, 0x4

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_data"

    aput-object v2, v3, v1

    const/4 v1, 0x1

    const-string v2, "bucket_id"

    aput-object v2, v3, v1

    const/4 v1, 0x2

    const-string v2, "bucket_display_name"

    aput-object v2, v3, v1

    const/4 v1, 0x3

    const-string v2, "datetaken"

    aput-object v2, v3, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/dreams/phototable/LocalSource;->mResolver:Landroid/content/ContentResolver;

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    const-string v1, "_data"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    const-string v1, "bucket_id"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    const-string v1, "bucket_display_name"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    const-string v1, "datetaken"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    if-gez v7, :cond_2

    const-string v1, "PhotoTable.LocalSource"

    const-string v2, "can\'t find the ID column!"

    invoke-static {v1, v2}, Lcom/android/dreams/phototable/LocalSource;->log(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_1
    const-string v1, "PhotoTable.LocalSource"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "found "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v11}, Ljava/util/HashMap;->size()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " items."

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/dreams/phototable/LocalSource;->log(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v11}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/android/dreams/phototable/LocalSource;->mFoundAlbumIds:Ljava/util/Set;

    invoke-virtual {v11}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    return-object v1

    :cond_2
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PhotoTable.LocalSource:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v8, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/dreams/phototable/PhotoSource$AlbumData;

    invoke-virtual {v11, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_4

    new-instance v9, Lcom/android/dreams/phototable/PhotoSource$AlbumData;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Lcom/android/dreams/phototable/PhotoSource$AlbumData;-><init>(Lcom/android/dreams/phototable/PhotoSource;)V

    iput-object v12, v9, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->id:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/dreams/phototable/LocalSource;->mLocalSourceName:Ljava/lang/String;

    iput-object v1, v9, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->account:Ljava/lang/String;

    if-ltz v10, :cond_3

    invoke-interface {v8, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v9, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->thumbnailUrl:Ljava/lang/String;

    :cond_3
    if-ltz v13, :cond_6

    invoke-interface {v8, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v9, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->title:Ljava/lang/String;

    :goto_1
    const-string v1, "PhotoTable.LocalSource"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v9, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->title:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " found"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/dreams/phototable/LocalSource;->log(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v11, v12, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    if-ltz v16, :cond_5

    move/from16 v0, v16

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    iget-wide v1, v9, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->updated:J

    const-wide/16 v4, 0x0

    cmp-long v1, v1, v4

    if-nez v1, :cond_7

    :goto_2
    iput-wide v14, v9, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->updated:J

    :cond_5
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/dreams/phototable/LocalSource;->mUnknownAlbumName:Ljava/lang/String;

    iput-object v1, v9, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->title:Ljava/lang/String;

    goto :goto_1

    :cond_7
    iget-wide v1, v9, Lcom/android/dreams/phototable/PhotoSource$AlbumData;->updated:J

    invoke-static {v1, v2, v14, v15}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v14

    goto :goto_2
.end method

.method protected findImages(I)Ljava/util/Collection;
    .locals 17
    .param p1    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Collection",
            "<",
            "Lcom/android/dreams/phototable/PhotoSource$ImageData;",
            ">;"
        }
    .end annotation

    const-string v1, "PhotoTable.LocalSource"

    const-string v2, "finding images"

    invoke-static {v1, v2}, Lcom/android/dreams/phototable/LocalSource;->log(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v11, Ljava/util/LinkedList;

    invoke-direct {v11}, Ljava/util/LinkedList;-><init>()V

    const/4 v1, 0x4

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_data"

    aput-object v2, v3, v1

    const/4 v1, 0x1

    const-string v2, "orientation"

    aput-object v2, v3, v1

    const/4 v1, 0x2

    const-string v2, "bucket_id"

    aput-object v2, v3, v1

    const/4 v1, 0x3

    const-string v2, "bucket_display_name"

    aput-object v2, v3, v1

    const-string v4, ""

    invoke-direct/range {p0 .. p0}, Lcom/android/dreams/phototable/LocalSource;->getFoundAlbums()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_0
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/dreams/phototable/LocalSource;->mSettings:Lcom/android/dreams/phototable/AlbumSettings;

    invoke-virtual {v1, v13}, Lcom/android/dreams/phototable/AlbumSettings;->isAlbumEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, ":"

    invoke-virtual {v13, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    array-length v1, v0

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " OR "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "bucket_id = \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x1

    aget-object v2, v16, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_2
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    :goto_1
    return-object v11

    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/dreams/phototable/LocalSource;->mResolver:Landroid/content/ContentResolver;

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    if-eqz v8, :cond_7

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v1

    move/from16 v0, p1

    if-le v1, v0, :cond_4

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/dreams/phototable/LocalSource;->mNextPosition:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/dreams/phototable/LocalSource;->mRNG:Ljava/util/Random;

    invoke-virtual {v1}, Ljava/util/Random;->nextInt()I

    move-result v1

    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v2

    sub-int v2, v2, p1

    rem-int/2addr v1, v2

    move-object/from16 v0, p0

    iput v1, v0, Lcom/android/dreams/phototable/LocalSource;->mNextPosition:I

    :cond_4
    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/dreams/phototable/LocalSource;->mNextPosition:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_5

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lcom/android/dreams/phototable/LocalSource;->mNextPosition:I

    :cond_5
    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/dreams/phototable/LocalSource;->mNextPosition:I

    invoke-interface {v8, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    const-string v1, "_data"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    const-string v1, "orientation"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    const-string v1, "bucket_id"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    const-string v1, "bucket_display_name"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    if-gez v10, :cond_8

    const-string v1, "PhotoTable.LocalSource"

    const-string v2, "can\'t find the DATA column!"

    invoke-static {v1, v2}, Lcom/android/dreams/phototable/LocalSource;->log(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    :goto_2
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_7
    const-string v1, "PhotoTable.LocalSource"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "found "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v11}, Ljava/util/LinkedList;->size()I

    move-result v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " items."

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/android/dreams/phototable/LocalSource;->log(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_8
    :goto_3
    invoke-virtual {v11}, Ljava/util/LinkedList;->size()I

    move-result v1

    move/from16 v0, p1

    if-ge v1, v0, :cond_9

    invoke-interface {v8}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_9

    new-instance v9, Lcom/android/dreams/phototable/PhotoSource$ImageData;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, Lcom/android/dreams/phototable/PhotoSource$ImageData;-><init>(Lcom/android/dreams/phototable/PhotoSource;)V

    invoke-interface {v8, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v9, Lcom/android/dreams/phototable/PhotoSource$ImageData;->url:Ljava/lang/String;

    invoke-interface {v8, v15}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v9, Lcom/android/dreams/phototable/PhotoSource$ImageData;->orientation:I

    invoke-virtual {v11, v9}, Ljava/util/LinkedList;->offer(Ljava/lang/Object;)Z

    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_8

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/dreams/phototable/LocalSource;->mNextPosition:I

    add-int/lit8 v1, v1, 0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/android/dreams/phototable/LocalSource;->mNextPosition:I

    goto :goto_3

    :cond_9
    invoke-interface {v8}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lcom/android/dreams/phototable/LocalSource;->mNextPosition:I

    goto :goto_2
.end method

.method protected getStream(Lcom/android/dreams/phototable/PhotoSource$ImageData;I)Ljava/io/InputStream;
    .locals 5
    .param p1    # Lcom/android/dreams/phototable/PhotoSource$ImageData;
    .param p2    # I

    const/4 v1, 0x0

    :try_start_0
    const-string v2, "PhotoTable.LocalSource"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "opening:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->url:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/dreams/phototable/LocalSource;->log(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/io/FileInputStream;

    iget-object v2, p1, Lcom/android/dreams/phototable/PhotoSource$ImageData;->url:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const-string v2, "PhotoTable.LocalSource"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/android/dreams/phototable/LocalSource;->log(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    goto :goto_0
.end method
