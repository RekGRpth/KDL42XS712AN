.class public Lcom/android/dreams/phototable/PhotoTableDream;
.super Landroid/service/dreams/DreamService;
.source "PhotoTableDream.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/service/dreams/DreamService;-><init>()V

    return-void
.end method


# virtual methods
.method public onAttachedToWindow()V
    .locals 10

    const/4 v9, 0x0

    const/4 v8, 0x1

    invoke-super {p0}, Landroid/service/dreams/DreamService;->onAttachedToWindow()V

    const-string v6, "layout_inflater"

    invoke-virtual {p0, v6}, Lcom/android/dreams/phototable/PhotoTableDream;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const-string v6, "PhotoTableDream"

    const/4 v7, 0x0

    invoke-virtual {p0, v6, v7}, Lcom/android/dreams/phototable/PhotoTableDream;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    invoke-static {v6}, Lcom/android/dreams/phototable/AlbumSettings;->getAlbumSettings(Landroid/content/SharedPreferences;)Lcom/android/dreams/phototable/AlbumSettings;

    move-result-object v3

    invoke-virtual {v3}, Lcom/android/dreams/phototable/AlbumSettings;->isConfigured()Z

    move-result v6

    if-eqz v6, :cond_0

    const v6, 0x7f040006    # com.android.dreams.phototable.R.layout.table

    invoke-virtual {v1, v6, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    const v6, 0x7f0b000c    # com.android.dreams.phototable.R.id.table

    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/android/dreams/phototable/PhotoTable;

    invoke-virtual {v4, p0}, Lcom/android/dreams/phototable/PhotoTable;->setDream(Landroid/service/dreams/DreamService;)V

    invoke-virtual {p0, v5}, Lcom/android/dreams/phototable/PhotoTableDream;->setContentView(Landroid/view/View;)V

    :goto_0
    invoke-virtual {p0, v8}, Lcom/android/dreams/phototable/PhotoTableDream;->setFullscreen(Z)V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/android/dreams/phototable/PhotoTableDream;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f040001    # com.android.dreams.phototable.R.layout.bummer

    invoke-virtual {v1, v6, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    const v6, 0x7f0b0006    # com.android.dreams.phototable.R.id.bummer

    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/dreams/phototable/BummerView;

    const/high16 v6, 0x7f080000    # com.android.dreams.phototable.R.integer.table_drop_period

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v6

    const v7, 0x7f080001    # com.android.dreams.phototable.R.integer.fast_drop

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v7

    invoke-virtual {v0, v8, v6, v7}, Lcom/android/dreams/phototable/BummerView;->setAnimationParams(ZII)V

    invoke-virtual {p0, v5}, Lcom/android/dreams/phototable/PhotoTableDream;->setContentView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public onDreamingStarted()V
    .locals 1

    invoke-super {p0}, Landroid/service/dreams/DreamService;->onDreamingStarted()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/dreams/phototable/PhotoTableDream;->setInteractive(Z)V

    return-void
.end method
