.class Lgreendroid/widget/PagedDialog$2;
.super Landroid/database/DataSetObserver;
.source "PagedDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lgreendroid/widget/PagedDialog;->setupContent()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lgreendroid/widget/PagedDialog;


# direct methods
.method constructor <init>(Lgreendroid/widget/PagedDialog;)V
    .locals 0

    iput-object p1, p0, Lgreendroid/widget/PagedDialog$2;->this$0:Lgreendroid/widget/PagedDialog;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method

.method private updatePageIndicator()V
    .locals 4

    iget-object v2, p0, Lgreendroid/widget/PagedDialog$2;->this$0:Lgreendroid/widget/PagedDialog;

    # getter for: Lgreendroid/widget/PagedDialog;->mAdapter:Lgreendroid/widget/PagedAdapter;
    invoke-static {v2}, Lgreendroid/widget/PagedDialog;->access$8(Lgreendroid/widget/PagedDialog;)Lgreendroid/widget/PagedAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lgreendroid/widget/PagedAdapter;->getCount()I

    move-result v0

    iget-object v2, p0, Lgreendroid/widget/PagedDialog$2;->this$0:Lgreendroid/widget/PagedDialog;

    # getter for: Lgreendroid/widget/PagedDialog;->mPageIndicator:Lgreendroid/widget/PageIndicator;
    invoke-static {v2}, Lgreendroid/widget/PagedDialog;->access$9(Lgreendroid/widget/PagedDialog;)Lgreendroid/widget/PageIndicator;

    move-result-object v2

    invoke-virtual {v2}, Lgreendroid/widget/PageIndicator;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_1

    if-lez v0, :cond_1

    iget-object v2, p0, Lgreendroid/widget/PagedDialog$2;->this$0:Lgreendroid/widget/PagedDialog;

    # getter for: Lgreendroid/widget/PagedDialog;->mPageIndicator:Lgreendroid/widget/PageIndicator;
    invoke-static {v2}, Lgreendroid/widget/PagedDialog;->access$9(Lgreendroid/widget/PagedDialog;)Lgreendroid/widget/PageIndicator;

    move-result-object v2

    invoke-virtual {v2, v0}, Lgreendroid/widget/PageIndicator;->setDotCount(I)V

    iget-object v2, p0, Lgreendroid/widget/PagedDialog$2;->this$0:Lgreendroid/widget/PagedDialog;

    # getter for: Lgreendroid/widget/PagedDialog;->mPageIndicator:Lgreendroid/widget/PageIndicator;
    invoke-static {v2}, Lgreendroid/widget/PagedDialog;->access$9(Lgreendroid/widget/PagedDialog;)Lgreendroid/widget/PageIndicator;

    move-result-object v2

    iget-object v3, p0, Lgreendroid/widget/PagedDialog$2;->this$0:Lgreendroid/widget/PagedDialog;

    # getter for: Lgreendroid/widget/PagedDialog;->mPagedView:Lgreendroid/widget/PagedView;
    invoke-static {v3}, Lgreendroid/widget/PagedDialog;->access$10(Lgreendroid/widget/PagedDialog;)Lgreendroid/widget/PagedView;

    move-result-object v3

    invoke-virtual {v3}, Lgreendroid/widget/PagedView;->getCurrentPage()I

    move-result v3

    invoke-virtual {v2, v3}, Lgreendroid/widget/PageIndicator;->setActiveDot(I)V

    iget-object v2, p0, Lgreendroid/widget/PagedDialog$2;->this$0:Lgreendroid/widget/PagedDialog;

    # getter for: Lgreendroid/widget/PagedDialog;->mPageIndicator:Lgreendroid/widget/PageIndicator;
    invoke-static {v2}, Lgreendroid/widget/PagedDialog;->access$9(Lgreendroid/widget/PagedDialog;)Lgreendroid/widget/PageIndicator;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lgreendroid/widget/PageIndicator;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez v1, :cond_0

    if-gtz v0, :cond_0

    iget-object v2, p0, Lgreendroid/widget/PagedDialog$2;->this$0:Lgreendroid/widget/PagedDialog;

    # getter for: Lgreendroid/widget/PagedDialog;->mPageIndicator:Lgreendroid/widget/PageIndicator;
    invoke-static {v2}, Lgreendroid/widget/PagedDialog;->access$9(Lgreendroid/widget/PagedDialog;)Lgreendroid/widget/PageIndicator;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lgreendroid/widget/PageIndicator;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public onChanged()V
    .locals 0

    invoke-direct {p0}, Lgreendroid/widget/PagedDialog$2;->updatePageIndicator()V

    return-void
.end method

.method public onInvalidated()V
    .locals 0

    invoke-direct {p0}, Lgreendroid/widget/PagedDialog$2;->updatePageIndicator()V

    return-void
.end method
