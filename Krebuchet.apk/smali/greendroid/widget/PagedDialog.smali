.class public Lgreendroid/widget/PagedDialog;
.super Landroid/app/Dialog;
.source "PagedDialog.java"

# interfaces
.implements Lgreendroid/widget/PagedView$OnPagedViewChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgreendroid/widget/PagedDialog$AlertParams;,
        Lgreendroid/widget/PagedDialog$Builder;,
        Lgreendroid/widget/PagedDialog$ButtonHandler;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "PagedDialog"


# instance fields
.field private mAdapter:Lgreendroid/widget/PagedAdapter;

.field private mButtonHandler:Landroid/view/View$OnClickListener;

.field private mButtonNegative:Landroid/widget/Button;

.field private mButtonNegativeMessage:Landroid/os/Message;

.field private mButtonNegativeText:Ljava/lang/CharSequence;

.field private mButtonPositive:Landroid/widget/Button;

.field private mButtonPositiveMessage:Landroid/os/Message;

.field private mButtonPositiveText:Ljava/lang/CharSequence;

.field private mContentHeight:I

.field private mContentWidth:I

.field private mHandler:Landroid/os/Handler;

.field private mIcon:Landroid/graphics/drawable/Drawable;

.field private mIconId:I

.field private mIconView:Landroid/widget/ImageView;

.field private mPageIndicator:Lgreendroid/widget/PageIndicator;

.field private mPagedView:Lgreendroid/widget/PagedView;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mTitleText:Ljava/lang/CharSequence;

.field private mTitleView:Landroid/widget/TextView;


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lgreendroid/widget/PagedDialog;-><init>(Landroid/content/Context;I)V

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # I

    const/4 v1, -0x1

    invoke-direct {p0, p1, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    const/4 v0, 0x0

    iput v0, p0, Lgreendroid/widget/PagedDialog;->mIconId:I

    iput v1, p0, Lgreendroid/widget/PagedDialog;->mContentWidth:I

    iput v1, p0, Lgreendroid/widget/PagedDialog;->mContentHeight:I

    new-instance v0, Lgreendroid/widget/PagedDialog$1;

    invoke-direct {v0, p0}, Lgreendroid/widget/PagedDialog$1;-><init>(Lgreendroid/widget/PagedDialog;)V

    iput-object v0, p0, Lgreendroid/widget/PagedDialog;->mButtonHandler:Landroid/view/View$OnClickListener;

    new-instance v0, Lgreendroid/widget/PagedDialog$ButtonHandler;

    invoke-direct {v0, p0}, Lgreendroid/widget/PagedDialog$ButtonHandler;-><init>(Landroid/content/DialogInterface;)V

    iput-object v0, p0, Lgreendroid/widget/PagedDialog;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$0(Lgreendroid/widget/PagedDialog;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lgreendroid/widget/PagedDialog;->mButtonPositive:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$1(Lgreendroid/widget/PagedDialog;)Landroid/os/Message;
    .locals 1

    iget-object v0, p0, Lgreendroid/widget/PagedDialog;->mButtonPositiveMessage:Landroid/os/Message;

    return-object v0
.end method

.method static synthetic access$10(Lgreendroid/widget/PagedDialog;)Lgreendroid/widget/PagedView;
    .locals 1

    iget-object v0, p0, Lgreendroid/widget/PagedDialog;->mPagedView:Lgreendroid/widget/PagedView;

    return-object v0
.end method

.method static synthetic access$2(Lgreendroid/widget/PagedDialog;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lgreendroid/widget/PagedDialog;->mButtonNegative:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$3(Lgreendroid/widget/PagedDialog;)Landroid/os/Message;
    .locals 1

    iget-object v0, p0, Lgreendroid/widget/PagedDialog;->mButtonNegativeMessage:Landroid/os/Message;

    return-object v0
.end method

.method static synthetic access$4(Lgreendroid/widget/PagedDialog;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lgreendroid/widget/PagedDialog;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$5(Lgreendroid/widget/PagedDialog;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    invoke-direct {p0, p1}, Lgreendroid/widget/PagedDialog;->setIcon(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method static synthetic access$6(Lgreendroid/widget/PagedDialog;I)V
    .locals 0

    invoke-direct {p0, p1}, Lgreendroid/widget/PagedDialog;->setIcon(I)V

    return-void
.end method

.method static synthetic access$7(Lgreendroid/widget/PagedDialog;ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lgreendroid/widget/PagedDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V

    return-void
.end method

.method static synthetic access$8(Lgreendroid/widget/PagedDialog;)Lgreendroid/widget/PagedAdapter;
    .locals 1

    iget-object v0, p0, Lgreendroid/widget/PagedDialog;->mAdapter:Lgreendroid/widget/PagedAdapter;

    return-object v0
.end method

.method static synthetic access$9(Lgreendroid/widget/PagedDialog;)Lgreendroid/widget/PageIndicator;
    .locals 1

    iget-object v0, p0, Lgreendroid/widget/PagedDialog;->mPageIndicator:Lgreendroid/widget/PageIndicator;

    return-object v0
.end method

.method private centerButton(Landroid/widget/Button;)V
    .locals 2
    .param p1    # Landroid/widget/Button;

    invoke-virtual {p1}, Landroid/widget/Button;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, 0x1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    const/high16 v1, 0x3f000000    # 0.5f

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private handleKeyEvent(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v1, 0x0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lgreendroid/widget/PagedDialog;->mPagedView:Lgreendroid/widget/PagedView;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v2, p0, Lgreendroid/widget/PagedDialog;->mPagedView:Lgreendroid/widget/PagedView;

    invoke-virtual {v2}, Lgreendroid/widget/PagedView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lgreendroid/widget/PagedDialog;->mPagedView:Lgreendroid/widget/PagedView;

    invoke-virtual {v1, p1, p2}, Lgreendroid/widget/PagedView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_0

    :cond_2
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    const/16 v3, 0x13

    if-ne v2, v3, :cond_0

    iget-object v1, p0, Lgreendroid/widget/PagedDialog;->mPagedView:Lgreendroid/widget/PagedView;

    const/16 v2, 0x82

    invoke-virtual {v1, v2}, Lgreendroid/widget/PagedView;->requestFocus(I)Z

    move-result v1

    goto :goto_0
.end method

.method private setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V
    .locals 2
    .param p1    # I
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Landroid/content/DialogInterface$OnClickListener;
    .param p4    # Landroid/os/Message;

    if-nez p4, :cond_0

    if-eqz p3, :cond_0

    iget-object v0, p0, Lgreendroid/widget/PagedDialog;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, p1, p3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p4

    :cond_0
    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Button does not exist"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iput-object p2, p0, Lgreendroid/widget/PagedDialog;->mButtonPositiveText:Ljava/lang/CharSequence;

    iput-object p4, p0, Lgreendroid/widget/PagedDialog;->mButtonPositiveMessage:Landroid/os/Message;

    :goto_0
    return-void

    :pswitch_1
    iput-object p2, p0, Lgreendroid/widget/PagedDialog;->mButtonNegativeText:Ljava/lang/CharSequence;

    iput-object p4, p0, Lgreendroid/widget/PagedDialog;->mButtonNegativeMessage:Landroid/os/Message;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private setIcon(I)V
    .locals 2
    .param p1    # I

    iput p1, p0, Lgreendroid/widget/PagedDialog;->mIconId:I

    iget-object v0, p0, Lgreendroid/widget/PagedDialog;->mIconView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    if-lez p1, :cond_1

    iget-object v0, p0, Lgreendroid/widget/PagedDialog;->mIconView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_0

    iget-object v0, p0, Lgreendroid/widget/PagedDialog;->mIconView:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;

    iput-object p1, p0, Lgreendroid/widget/PagedDialog;->mIcon:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lgreendroid/widget/PagedDialog;->mIconView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lgreendroid/widget/PagedDialog;->mIconView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method private setupButtons()Z
    .locals 7

    const/16 v6, 0x8

    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x2

    const/4 v2, 0x0

    const v3, 0x7f0d004b    # com.konka.avenger.R.id.button1

    invoke-virtual {p0, v3}, Lgreendroid/widget/PagedDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lgreendroid/widget/PagedDialog;->mButtonPositive:Landroid/widget/Button;

    iget-object v3, p0, Lgreendroid/widget/PagedDialog;->mButtonPositive:Landroid/widget/Button;

    iget-object v5, p0, Lgreendroid/widget/PagedDialog;->mButtonHandler:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lgreendroid/widget/PagedDialog;->mButtonPositiveText:Ljava/lang/CharSequence;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lgreendroid/widget/PagedDialog;->mButtonPositive:Landroid/widget/Button;

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setVisibility(I)V

    :goto_0
    const v3, 0x7f0d004a    # com.konka.avenger.R.id.button2

    invoke-virtual {p0, v3}, Lgreendroid/widget/PagedDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lgreendroid/widget/PagedDialog;->mButtonNegative:Landroid/widget/Button;

    iget-object v3, p0, Lgreendroid/widget/PagedDialog;->mButtonNegative:Landroid/widget/Button;

    iget-object v5, p0, Lgreendroid/widget/PagedDialog;->mButtonHandler:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lgreendroid/widget/PagedDialog;->mButtonNegativeText:Ljava/lang/CharSequence;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lgreendroid/widget/PagedDialog;->mButtonNegative:Landroid/widget/Button;

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setVisibility(I)V

    :goto_1
    if-ne v2, v1, :cond_3

    iget-object v3, p0, Lgreendroid/widget/PagedDialog;->mButtonPositive:Landroid/widget/Button;

    invoke-direct {p0, v3}, Lgreendroid/widget/PagedDialog;->centerButton(Landroid/widget/Button;)V

    :cond_0
    :goto_2
    if-eqz v2, :cond_4

    const/4 v3, 0x1

    :goto_3
    return v3

    :cond_1
    iget-object v3, p0, Lgreendroid/widget/PagedDialog;->mButtonPositive:Landroid/widget/Button;

    iget-object v5, p0, Lgreendroid/widget/PagedDialog;->mButtonPositiveText:Ljava/lang/CharSequence;

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lgreendroid/widget/PagedDialog;->mButtonPositive:Landroid/widget/Button;

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setVisibility(I)V

    or-int/2addr v2, v1

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lgreendroid/widget/PagedDialog;->mButtonNegative:Landroid/widget/Button;

    iget-object v5, p0, Lgreendroid/widget/PagedDialog;->mButtonNegativeText:Ljava/lang/CharSequence;

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lgreendroid/widget/PagedDialog;->mButtonNegative:Landroid/widget/Button;

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setVisibility(I)V

    or-int/2addr v2, v0

    goto :goto_1

    :cond_3
    if-ne v2, v0, :cond_0

    iget-object v3, p0, Lgreendroid/widget/PagedDialog;->mButtonNegative:Landroid/widget/Button;

    invoke-direct {p0, v3}, Lgreendroid/widget/PagedDialog;->centerButton(Landroid/widget/Button;)V

    goto :goto_2

    :cond_4
    move v3, v4

    goto :goto_3
.end method

.method private setupContent()V
    .locals 5

    const/4 v4, 0x4

    const v1, 0x7f0d001f    # com.konka.avenger.R.id.apps_customize_progress_bar

    invoke-virtual {p0, v1}, Lgreendroid/widget/PagedDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lgreendroid/widget/PagedDialog;->mProgressBar:Landroid/widget/ProgressBar;

    iget-object v1, p0, Lgreendroid/widget/PagedDialog;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    const v1, 0x7f0d0048    # com.konka.avenger.R.id.paged_view

    invoke-virtual {p0, v1}, Lgreendroid/widget/PagedDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lgreendroid/widget/PagedView;

    iput-object v1, p0, Lgreendroid/widget/PagedDialog;->mPagedView:Lgreendroid/widget/PagedView;

    iget v1, p0, Lgreendroid/widget/PagedDialog;->mContentWidth:I

    if-lez v1, :cond_0

    iget v1, p0, Lgreendroid/widget/PagedDialog;->mContentHeight:I

    if-lez v1, :cond_0

    iget-object v1, p0, Lgreendroid/widget/PagedDialog;->mPagedView:Lgreendroid/widget/PagedView;

    invoke-virtual {v1}, Lgreendroid/widget/PagedView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const-string v2, "PagedDialog"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v1, "pagedDialog viewgroup params "

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez v0, :cond_2

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " null"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "PagedDialog"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "pagedDialog mContentWidth = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lgreendroid/widget/PagedDialog;->mContentWidth:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mContentHeight="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lgreendroid/widget/PagedDialog;->mContentHeight:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v0, :cond_3

    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    iget v1, p0, Lgreendroid/widget/PagedDialog;->mContentWidth:I

    iget v2, p0, Lgreendroid/widget/PagedDialog;->mContentHeight:I

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    :goto_1
    iget-object v1, p0, Lgreendroid/widget/PagedDialog;->mPagedView:Lgreendroid/widget/PagedView;

    invoke-virtual {v1, v0}, Lgreendroid/widget/PagedView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    const v1, 0x7f0d0047    # com.konka.avenger.R.id.page_indicator

    invoke-virtual {p0, v1}, Lgreendroid/widget/PagedDialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lgreendroid/widget/PageIndicator;

    iput-object v1, p0, Lgreendroid/widget/PagedDialog;->mPageIndicator:Lgreendroid/widget/PageIndicator;

    iget-object v1, p0, Lgreendroid/widget/PagedDialog;->mAdapter:Lgreendroid/widget/PagedAdapter;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lgreendroid/widget/PagedDialog;->mPagedView:Lgreendroid/widget/PagedView;

    iget-object v2, p0, Lgreendroid/widget/PagedDialog;->mAdapter:Lgreendroid/widget/PagedAdapter;

    invoke-virtual {v1, v2}, Lgreendroid/widget/PagedView;->setAdapter(Lgreendroid/widget/PagedAdapter;)V

    iget-object v1, p0, Lgreendroid/widget/PagedDialog;->mPagedView:Lgreendroid/widget/PagedView;

    invoke-virtual {v1, p0}, Lgreendroid/widget/PagedView;->setOnPageChangeListener(Lgreendroid/widget/PagedView$OnPagedViewChangeListener;)V

    iget-object v1, p0, Lgreendroid/widget/PagedDialog;->mAdapter:Lgreendroid/widget/PagedAdapter;

    new-instance v2, Lgreendroid/widget/PagedDialog$2;

    invoke-direct {v2, p0}, Lgreendroid/widget/PagedDialog$2;-><init>(Lgreendroid/widget/PagedDialog;)V

    invoke-virtual {v1, v2}, Lgreendroid/widget/PagedAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    iget-object v1, p0, Lgreendroid/widget/PagedDialog;->mAdapter:Lgreendroid/widget/PagedAdapter;

    invoke-virtual {v1}, Lgreendroid/widget/PagedAdapter;->getCount()I

    move-result v1

    if-lez v1, :cond_4

    iget-object v1, p0, Lgreendroid/widget/PagedDialog;->mPageIndicator:Lgreendroid/widget/PageIndicator;

    iget-object v2, p0, Lgreendroid/widget/PagedDialog;->mAdapter:Lgreendroid/widget/PagedAdapter;

    invoke-virtual {v2}, Lgreendroid/widget/PagedAdapter;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Lgreendroid/widget/PageIndicator;->setDotCount(I)V

    iget-object v1, p0, Lgreendroid/widget/PagedDialog;->mPageIndicator:Lgreendroid/widget/PageIndicator;

    iget-object v2, p0, Lgreendroid/widget/PagedDialog;->mPagedView:Lgreendroid/widget/PagedView;

    invoke-virtual {v2}, Lgreendroid/widget/PagedView;->getCurrentPage()I

    move-result v2

    invoke-virtual {v1, v2}, Lgreendroid/widget/PageIndicator;->setActiveDot(I)V

    :cond_1
    :goto_2
    return-void

    :cond_2
    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_3
    iget v1, p0, Lgreendroid/widget/PagedDialog;->mContentWidth:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v1, p0, Lgreendroid/widget/PagedDialog;->mContentHeight:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lgreendroid/widget/PagedDialog;->mPageIndicator:Lgreendroid/widget/PageIndicator;

    invoke-virtual {v1, v4}, Lgreendroid/widget/PageIndicator;->setVisibility(I)V

    goto :goto_2
.end method

.method private setupTitle(Landroid/widget/LinearLayout;)Z
    .locals 9
    .param p1    # Landroid/widget/LinearLayout;

    const/16 v8, 0x8

    const/4 v1, 0x1

    iget-object v3, p0, Lgreendroid/widget/PagedDialog;->mTitleText:Ljava/lang/CharSequence;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v0, 0x0

    :goto_0
    const v3, 0x7f0d0042    # com.konka.avenger.R.id.icon

    invoke-virtual {p0, v3}, Lgreendroid/widget/PagedDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lgreendroid/widget/PagedDialog;->mIconView:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    const v3, 0x7f0d0043    # com.konka.avenger.R.id.pagedTitle

    invoke-virtual {p0, v3}, Lgreendroid/widget/PagedDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lgreendroid/widget/PagedDialog;->mTitleView:Landroid/widget/TextView;

    iget-object v3, p0, Lgreendroid/widget/PagedDialog;->mTitleView:Landroid/widget/TextView;

    iget-object v4, p0, Lgreendroid/widget/PagedDialog;->mTitleText:Ljava/lang/CharSequence;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v3, p0, Lgreendroid/widget/PagedDialog;->mIconId:I

    if-lez v3, :cond_2

    iget-object v3, p0, Lgreendroid/widget/PagedDialog;->mIconView:Landroid/widget/ImageView;

    iget v4, p0, Lgreendroid/widget/PagedDialog;->mIconId:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_0
    :goto_1
    return v1

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lgreendroid/widget/PagedDialog;->mIcon:Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lgreendroid/widget/PagedDialog;->mIconView:Landroid/widget/ImageView;

    iget-object v4, p0, Lgreendroid/widget/PagedDialog;->mIcon:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    :cond_3
    iget v3, p0, Lgreendroid/widget/PagedDialog;->mIconId:I

    if-nez v3, :cond_0

    iget-object v3, p0, Lgreendroid/widget/PagedDialog;->mTitleView:Landroid/widget/TextView;

    iget-object v4, p0, Lgreendroid/widget/PagedDialog;->mIconView:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getPaddingLeft()I

    move-result v4

    iget-object v5, p0, Lgreendroid/widget/PagedDialog;->mIconView:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getPaddingTop()I

    move-result v5

    iget-object v6, p0, Lgreendroid/widget/PagedDialog;->mIconView:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getPaddingRight()I

    move-result v6

    iget-object v7, p0, Lgreendroid/widget/PagedDialog;->mIconView:Landroid/widget/ImageView;

    invoke-virtual {v7}, Landroid/widget/ImageView;->getPaddingBottom()I

    move-result v7

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object v3, p0, Lgreendroid/widget/PagedDialog;->mIconView:Landroid/widget/ImageView;

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    :cond_4
    const v3, 0x7f0d0041    # com.konka.avenger.R.id.title_template

    invoke-virtual {p0, v3}, Lgreendroid/widget/PagedDialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lgreendroid/widget/PagedDialog;->mIconView:Landroid/widget/ImageView;

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {p1, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const/4 v1, 0x0

    goto :goto_1
.end method


# virtual methods
.method public getAdapter()Lgreendroid/widget/PagedAdapter;
    .locals 1

    iget-object v0, p0, Lgreendroid/widget/PagedDialog;->mAdapter:Lgreendroid/widget/PagedAdapter;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;

    const/high16 v5, 0x20000

    const/4 v7, 0x1

    invoke-super {p0, p1}, Landroid/app/Dialog;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lgreendroid/widget/PagedDialog;->getWindow()Landroid/view/Window;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/view/Window;->requestFeature(I)Z

    invoke-virtual {v4, v5, v5}, Landroid/view/Window;->setFlags(II)V

    const v5, 0x7f03001b    # com.konka.avenger.R.layout.paged_dialog

    invoke-virtual {p0, v5}, Lgreendroid/widget/PagedDialog;->setContentView(I)V

    iget v5, p0, Lgreendroid/widget/PagedDialog;->mContentWidth:I

    if-lez v5, :cond_0

    iget v5, p0, Lgreendroid/widget/PagedDialog;->mContentWidth:I

    add-int/lit8 v5, v5, 0x20

    const/4 v6, -0x2

    invoke-virtual {v4, v5, v6}, Landroid/view/Window;->setLayout(II)V

    :cond_0
    const v5, 0x7f0d003f    # com.konka.avenger.R.id.topPanel

    invoke-virtual {p0, v5}, Lgreendroid/widget/PagedDialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    invoke-direct {p0, v3}, Lgreendroid/widget/PagedDialog;->setupTitle(Landroid/widget/LinearLayout;)Z

    move-result v2

    invoke-direct {p0}, Lgreendroid/widget/PagedDialog;->setupContent()V

    invoke-direct {p0}, Lgreendroid/widget/PagedDialog;->setupButtons()Z

    move-result v1

    const v5, 0x7f0d0049    # com.konka.avenger.R.id.buttonPanel

    invoke-virtual {v4, v5}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v1, :cond_1

    const/16 v5, 0x8

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v4, v7}, Landroid/view/Window;->setCloseOnTouchOutsideIfNotSet(Z)V

    :cond_1
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    invoke-direct {p0, p1, p2}, Lgreendroid/widget/PagedDialog;->handleKeyEvent(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    invoke-direct {p0, p1, p2}, Lgreendroid/widget/PagedDialog;->handleKeyEvent(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPageChanged(Lgreendroid/widget/PagedView;II)V
    .locals 1
    .param p1    # Lgreendroid/widget/PagedView;
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lgreendroid/widget/PagedDialog;->mPageIndicator:Lgreendroid/widget/PageIndicator;

    invoke-virtual {v0, p3}, Lgreendroid/widget/PageIndicator;->setActiveDot(I)V

    return-void
.end method

.method public onStartTracking(Lgreendroid/widget/PagedView;)V
    .locals 0
    .param p1    # Lgreendroid/widget/PagedView;

    return-void
.end method

.method public onStopTracking(Lgreendroid/widget/PagedView;)V
    .locals 0
    .param p1    # Lgreendroid/widget/PagedView;

    return-void
.end method

.method public setAdapter(Lgreendroid/widget/PagedAdapter;)V
    .locals 0
    .param p1    # Lgreendroid/widget/PagedAdapter;

    iput-object p1, p0, Lgreendroid/widget/PagedDialog;->mAdapter:Lgreendroid/widget/PagedAdapter;

    return-void
.end method

.method public setButton(ILjava/lang/CharSequence;Landroid/os/Message;)V
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/CharSequence;
    .param p3    # Landroid/os/Message;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, Lgreendroid/widget/PagedDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V

    return-void
.end method

.method public setContentLayout(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    iput p1, p0, Lgreendroid/widget/PagedDialog;->mContentWidth:I

    iput p2, p0, Lgreendroid/widget/PagedDialog;->mContentHeight:I

    return-void
.end method

.method public setShowProgress(Z)V
    .locals 3
    .param p1    # Z

    const/4 v1, 0x0

    iget-object v2, p0, Lgreendroid/widget/PagedDialog;->mProgressBar:Landroid/widget/ProgressBar;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lgreendroid/widget/PagedDialog;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v2}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v2, 0x1

    :goto_0
    xor-int/2addr v2, p1

    if-eqz v2, :cond_1

    iget-object v2, p0, Lgreendroid/widget/PagedDialog;->mProgressBar:Landroid/widget/ProgressBar;

    if-nez v0, :cond_0

    const/16 v1, 0x8

    :cond_0
    invoke-virtual {v2, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :cond_1
    return-void

    :cond_2
    move v2, v1

    goto :goto_0
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    invoke-super {p0, p1}, Landroid/app/Dialog;->setTitle(Ljava/lang/CharSequence;)V

    iput-object p1, p0, Lgreendroid/widget/PagedDialog;->mTitleText:Ljava/lang/CharSequence;

    iget-object v0, p0, Lgreendroid/widget/PagedDialog;->mTitleView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgreendroid/widget/PagedDialog;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method
