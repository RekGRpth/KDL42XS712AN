.class public Lgreendroid/widget/PageIndicator;
.super Landroid/view/View;
.source "PageIndicator.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgreendroid/widget/PageIndicator$DotType;,
        Lgreendroid/widget/PageIndicator$SavedState;
    }
.end annotation


# static fields
.field private static final MIN_DOT_COUNT:I = 0x1

.field public static final NO_ACTIVE_DOT:I = -0x1

.field private static sInRect:Landroid/graphics/Rect;

.field private static sOutRect:Landroid/graphics/Rect;


# instance fields
.field private mActiveDot:I

.field private mDotCount:I

.field private mDotDrawable:Landroid/graphics/drawable/Drawable;

.field private mDotSpacing:I

.field private mDotType:I

.field private mExtraState:[I

.field private mGravity:I

.field private mInitializing:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lgreendroid/widget/PageIndicator;->sInRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lgreendroid/widget/PageIndicator;->sOutRect:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lgreendroid/widget/PageIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lgreendroid/widget/PageIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0}, Lgreendroid/widget/PageIndicator;->initPageIndicator()V

    iput-boolean v2, p0, Lgreendroid/widget/PageIndicator;->mInitializing:Z

    sget-object v1, Lcom/konka/avenger/R$styleable;->PageIndicator:[I

    invoke-virtual {p1, p2, v1, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    iget v1, p0, Lgreendroid/widget/PageIndicator;->mDotCount:I

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lgreendroid/widget/PageIndicator;->setDotCount(I)V

    iget v1, p0, Lgreendroid/widget/PageIndicator;->mActiveDot:I

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lgreendroid/widget/PageIndicator;->setActiveDot(I)V

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p0, v1}, Lgreendroid/widget/PageIndicator;->setDotDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v1, 0x3

    iget v2, p0, Lgreendroid/widget/PageIndicator;->mDotSpacing:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lgreendroid/widget/PageIndicator;->setDotSpacing(I)V

    const/4 v1, 0x4

    iget v2, p0, Lgreendroid/widget/PageIndicator;->mGravity:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lgreendroid/widget/PageIndicator;->setGravity(I)V

    const/4 v1, 0x5

    iget v2, p0, Lgreendroid/widget/PageIndicator;->mDotType:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lgreendroid/widget/PageIndicator;->setDotType(I)V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    iput-boolean v3, p0, Lgreendroid/widget/PageIndicator;->mInitializing:Z

    return-void
.end method

.method private initPageIndicator()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iput v2, p0, Lgreendroid/widget/PageIndicator;->mDotCount:I

    const/16 v0, 0x11

    iput v0, p0, Lgreendroid/widget/PageIndicator;->mGravity:I

    iput v1, p0, Lgreendroid/widget/PageIndicator;->mActiveDot:I

    iput v1, p0, Lgreendroid/widget/PageIndicator;->mDotSpacing:I

    iput v1, p0, Lgreendroid/widget/PageIndicator;->mDotType:I

    invoke-virtual {p0, v2}, Lgreendroid/widget/PageIndicator;->onCreateDrawableState(I)[I

    move-result-object v0

    iput-object v0, p0, Lgreendroid/widget/PageIndicator;->mExtraState:[I

    iget-object v0, p0, Lgreendroid/widget/PageIndicator;->mExtraState:[I

    sget-object v1, Lgreendroid/widget/PageIndicator;->SELECTED_STATE_SET:[I

    invoke-static {v0, v1}, Lgreendroid/widget/PageIndicator;->mergeDrawableStates([I[I)[I

    return-void
.end method


# virtual methods
.method protected drawableStateChanged()V
    .locals 2

    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lgreendroid/widget/PageIndicator;->onCreateDrawableState(I)[I

    move-result-object v0

    iput-object v0, p0, Lgreendroid/widget/PageIndicator;->mExtraState:[I

    iget-object v0, p0, Lgreendroid/widget/PageIndicator;->mExtraState:[I

    sget-object v1, Lgreendroid/widget/PageIndicator;->SELECTED_STATE_SET:[I

    invoke-static {v0, v1}, Lgreendroid/widget/PageIndicator;->mergeDrawableStates([I[I)[I

    invoke-virtual {p0}, Lgreendroid/widget/PageIndicator;->invalidate()V

    return-void
.end method

.method public getActiveDot()I
    .locals 1

    iget v0, p0, Lgreendroid/widget/PageIndicator;->mActiveDot:I

    return v0
.end method

.method public getDotCount()I
    .locals 1

    iget v0, p0, Lgreendroid/widget/PageIndicator;->mDotCount:I

    return v0
.end method

.method public getDotDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lgreendroid/widget/PageIndicator;->mDotDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getDotSpacing()I
    .locals 1

    iget v0, p0, Lgreendroid/widget/PageIndicator;->mDotSpacing:I

    return v0
.end method

.method public getDotType()I
    .locals 1

    iget v0, p0, Lgreendroid/widget/PageIndicator;->mDotType:I

    return v0
.end method

.method public getGravity()I
    .locals 1

    iget v0, p0, Lgreendroid/widget/PageIndicator;->mGravity:I

    return v0
.end method

.method public invalidate()V
    .locals 1

    iget-boolean v0, p0, Lgreendroid/widget/PageIndicator;->mInitializing:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/view/View;->invalidate()V

    :cond_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 13
    .param p1    # Landroid/graphics/Canvas;

    iget-object v1, p0, Lgreendroid/widget/PageIndicator;->mDotDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    iget v10, p0, Lgreendroid/widget/PageIndicator;->mDotType:I

    if-nez v10, :cond_1

    iget v0, p0, Lgreendroid/widget/PageIndicator;->mDotCount:I

    :goto_0
    if-gtz v0, :cond_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget v0, p0, Lgreendroid/widget/PageIndicator;->mActiveDot:I

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    const/4 v10, 0x0

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v11

    iget v12, p0, Lgreendroid/widget/PageIndicator;->mDotSpacing:I

    add-int/2addr v11, v12

    mul-int/2addr v11, v0

    iget v12, p0, Lgreendroid/widget/PageIndicator;->mDotSpacing:I

    sub-int/2addr v11, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->max(II)I

    move-result v9

    invoke-virtual {p0}, Lgreendroid/widget/PageIndicator;->getPaddingRight()I

    move-result v6

    invoke-virtual {p0}, Lgreendroid/widget/PageIndicator;->getPaddingLeft()I

    move-result v5

    invoke-virtual {p0}, Lgreendroid/widget/PageIndicator;->getPaddingTop()I

    move-result v7

    invoke-virtual {p0}, Lgreendroid/widget/PageIndicator;->getPaddingBottom()I

    move-result v4

    sget-object v10, Lgreendroid/widget/PageIndicator;->sInRect:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lgreendroid/widget/PageIndicator;->getWidth()I

    move-result v11

    sub-int/2addr v11, v6

    invoke-virtual {p0}, Lgreendroid/widget/PageIndicator;->getHeight()I

    move-result v12

    sub-int/2addr v12, v4

    invoke-virtual {v10, v5, v7, v11, v12}, Landroid/graphics/Rect;->set(IIII)V

    iget v10, p0, Lgreendroid/widget/PageIndicator;->mGravity:I

    sget-object v11, Lgreendroid/widget/PageIndicator;->sInRect:Landroid/graphics/Rect;

    sget-object v12, Lgreendroid/widget/PageIndicator;->sOutRect:Landroid/graphics/Rect;

    invoke-static {v10, v9, v2, v11, v12}, Landroid/view/Gravity;->apply(IIILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    sget-object v10, Lgreendroid/widget/PageIndicator;->sOutRect:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    int-to-float v10, v10

    sget-object v11, Lgreendroid/widget/PageIndicator;->sOutRect:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->top:I

    int-to-float v11, v11

    invoke-virtual {p1, v10, v11}, Landroid/graphics/Canvas;->translate(FF)V

    const/4 v3, 0x0

    :goto_2
    if-lt v3, v0, :cond_3

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_1

    :cond_3
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-virtual {p0}, Lgreendroid/widget/PageIndicator;->getDrawableState()[I

    move-result-object v8

    iget v10, p0, Lgreendroid/widget/PageIndicator;->mDotType:I

    const/4 v11, 0x1

    if-eq v10, v11, :cond_4

    iget v10, p0, Lgreendroid/widget/PageIndicator;->mActiveDot:I

    if-ne v3, v10, :cond_5

    :cond_4
    iget-object v8, p0, Lgreendroid/widget/PageIndicator;->mExtraState:[I

    :cond_5
    const/4 v10, 0x0

    invoke-virtual {v1, v10}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    invoke-virtual {v1, v8}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    invoke-virtual {v1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    :cond_6
    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    iget v10, p0, Lgreendroid/widget/PageIndicator;->mDotSpacing:I

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v11

    add-int/2addr v10, v11

    int-to-float v10, v10

    const/4 v11, 0x0

    invoke-virtual {p1, v10, v11}, Landroid/graphics/Canvas;->translate(FF)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method

.method protected onMeasure(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lgreendroid/widget/PageIndicator;->mDotDrawable:Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget v3, p0, Lgreendroid/widget/PageIndicator;->mDotCount:I

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    iget v5, p0, Lgreendroid/widget/PageIndicator;->mDotSpacing:I

    add-int/2addr v4, v5

    mul-int/2addr v3, v4

    iget v4, p0, Lgreendroid/widget/PageIndicator;->mDotSpacing:I

    sub-int v2, v3, v4

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    :cond_0
    invoke-virtual {p0}, Lgreendroid/widget/PageIndicator;->getPaddingRight()I

    move-result v3

    invoke-virtual {p0}, Lgreendroid/widget/PageIndicator;->getPaddingLeft()I

    move-result v4

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    invoke-virtual {p0}, Lgreendroid/widget/PageIndicator;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p0}, Lgreendroid/widget/PageIndicator;->getPaddingTop()I

    move-result v4

    add-int/2addr v3, v4

    add-int/2addr v1, v3

    invoke-static {v2, p1}, Lgreendroid/widget/PageIndicator;->resolveSize(II)I

    move-result v3

    invoke-static {v1, p2}, Lgreendroid/widget/PageIndicator;->resolveSize(II)I

    move-result v4

    invoke-virtual {p0, v3, v4}, Lgreendroid/widget/PageIndicator;->setMeasuredDimension(II)V

    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1    # Landroid/os/Parcelable;

    move-object v0, p1

    check-cast v0, Lgreendroid/widget/PageIndicator$SavedState;

    invoke-virtual {v0}, Lgreendroid/widget/PageIndicator$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget v1, v0, Lgreendroid/widget/PageIndicator$SavedState;->activeDot:I

    iput v1, p0, Lgreendroid/widget/PageIndicator;->mActiveDot:I

    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    new-instance v0, Lgreendroid/widget/PageIndicator$SavedState;

    invoke-direct {v0, v1}, Lgreendroid/widget/PageIndicator$SavedState;-><init>(Landroid/os/Parcelable;)V

    iget v2, p0, Lgreendroid/widget/PageIndicator;->mActiveDot:I

    iput v2, v0, Lgreendroid/widget/PageIndicator$SavedState;->activeDot:I

    return-object v0
.end method

.method public requestLayout()V
    .locals 1

    iget-boolean v0, p0, Lgreendroid/widget/PageIndicator;->mInitializing:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/view/View;->requestLayout()V

    :cond_0
    return-void
.end method

.method public setActiveDot(I)V
    .locals 1
    .param p1    # I

    if-gez p1, :cond_0

    const/4 p1, -0x1

    :cond_0
    iget v0, p0, Lgreendroid/widget/PageIndicator;->mDotType:I

    packed-switch v0, :pswitch_data_0

    :cond_1
    :goto_0
    iput p1, p0, Lgreendroid/widget/PageIndicator;->mActiveDot:I

    invoke-virtual {p0}, Lgreendroid/widget/PageIndicator;->invalidate()V

    return-void

    :pswitch_0
    iget v0, p0, Lgreendroid/widget/PageIndicator;->mDotCount:I

    add-int/lit8 v0, v0, -0x1

    if-le p1, v0, :cond_1

    const/4 p1, -0x1

    goto :goto_0

    :pswitch_1
    iget v0, p0, Lgreendroid/widget/PageIndicator;->mDotCount:I

    if-le p1, v0, :cond_1

    const/4 p1, -0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setDotCount(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    if-ge p1, v0, :cond_0

    const/4 p1, 0x1

    :cond_0
    iget v0, p0, Lgreendroid/widget/PageIndicator;->mDotCount:I

    if-eq v0, p1, :cond_1

    iput p1, p0, Lgreendroid/widget/PageIndicator;->mDotCount:I

    invoke-virtual {p0}, Lgreendroid/widget/PageIndicator;->requestLayout()V

    invoke-virtual {p0}, Lgreendroid/widget/PageIndicator;->invalidate()V

    :cond_1
    return-void
.end method

.method public setDotDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 4
    .param p1    # Landroid/graphics/drawable/Drawable;

    const/4 v3, 0x0

    const/4 v2, -0x1

    iget-object v0, p0, Lgreendroid/widget/PageIndicator;->mDotDrawable:Landroid/graphics/drawable/Drawable;

    if-eq p1, v0, :cond_1

    iget-object v0, p0, Lgreendroid/widget/PageIndicator;->mDotDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgreendroid/widget/PageIndicator;->mDotDrawable:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    :cond_0
    iput-object p1, p0, Lgreendroid/widget/PageIndicator;->mDotDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    if-eq v0, v2, :cond_1

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    if-ne v0, v2, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-virtual {p1, v3, v3, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lgreendroid/widget/PageIndicator;->getDrawableState()[I

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    :cond_3
    invoke-virtual {p0}, Lgreendroid/widget/PageIndicator;->requestLayout()V

    invoke-virtual {p0}, Lgreendroid/widget/PageIndicator;->invalidate()V

    goto :goto_0
.end method

.method public setDotSpacing(I)V
    .locals 3
    .param p1    # I

    const-string v0, "PageIndicator"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setDotSpacing to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lgreendroid/widget/PageIndicator;->mDotSpacing:I

    if-eq p1, v0, :cond_0

    iput p1, p0, Lgreendroid/widget/PageIndicator;->mDotSpacing:I

    invoke-virtual {p0}, Lgreendroid/widget/PageIndicator;->requestLayout()V

    invoke-virtual {p0}, Lgreendroid/widget/PageIndicator;->invalidate()V

    :cond_0
    return-void
.end method

.method public setDotType(I)V
    .locals 1
    .param p1    # I

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    :cond_0
    iget v0, p0, Lgreendroid/widget/PageIndicator;->mDotType:I

    if-eq v0, p1, :cond_1

    iput p1, p0, Lgreendroid/widget/PageIndicator;->mDotType:I

    invoke-virtual {p0}, Lgreendroid/widget/PageIndicator;->invalidate()V

    :cond_1
    return-void
.end method

.method public setGravity(I)V
    .locals 1
    .param p1    # I

    iget v0, p0, Lgreendroid/widget/PageIndicator;->mGravity:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lgreendroid/widget/PageIndicator;->mGravity:I

    invoke-virtual {p0}, Lgreendroid/widget/PageIndicator;->invalidate()V

    :cond_0
    return-void
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;

    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgreendroid/widget/PageIndicator;->mDotDrawable:Landroid/graphics/drawable/Drawable;

    if-eq p1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
