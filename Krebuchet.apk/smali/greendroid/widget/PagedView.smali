.class public Lgreendroid/widget/PagedView;
.super Landroid/view/ViewGroup;
.source "PagedView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgreendroid/widget/PagedView$OnPagedViewChangeListener;,
        Lgreendroid/widget/PagedView$SavedState;
    }
.end annotation


# static fields
.field private static final FRAME_RATE:I = 0x10

.field private static final INVALID_PAGE:I = -0x1

.field private static final LOG_TAG:Ljava/lang/String;

.field private static final MINIMUM_PAGE_CHANGE_VELOCITY:I = 0x1f4

.field private static final VELOCITY_UNITS:I = 0x3e8


# instance fields
.field private mActiveViews:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mAdapter:Lgreendroid/widget/PagedAdapter;

.field private mCurrentPage:I

.field private mDataSetObserver:Landroid/database/DataSetObserver;

.field private final mHandler:Landroid/os/Handler;

.field private mIsBeingDragged:Z

.field private mMaximumVelocity:I

.field private mMinimumVelocity:I

.field private mOffsetX:I

.field private mOnPageChangeListener:Lgreendroid/widget/PagedView$OnPagedViewChangeListener;

.field private mPageCount:I

.field private mPageSlop:I

.field private mPagingTouchSlop:I

.field private mRecycler:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mScroller:Landroid/widget/Scroller;

.field private mScrollerRunnable:Ljava/lang/Runnable;

.field private mStartMotionX:I

.field private mStartOffsetX:I

.field private mTargetPage:I

.field private mVelocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lgreendroid/widget/PagedView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lgreendroid/widget/PagedView;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lgreendroid/widget/PagedView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lgreendroid/widget/PagedView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lgreendroid/widget/PagedView;->mHandler:Landroid/os/Handler;

    const/4 v0, -0x1

    iput v0, p0, Lgreendroid/widget/PagedView;->mTargetPage:I

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lgreendroid/widget/PagedView;->mActiveViews:Landroid/util/SparseArray;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lgreendroid/widget/PagedView;->mRecycler:Ljava/util/Queue;

    new-instance v0, Lgreendroid/widget/PagedView$1;

    invoke-direct {v0, p0}, Lgreendroid/widget/PagedView$1;-><init>(Lgreendroid/widget/PagedView;)V

    iput-object v0, p0, Lgreendroid/widget/PagedView;->mDataSetObserver:Landroid/database/DataSetObserver;

    new-instance v0, Lgreendroid/widget/PagedView$2;

    invoke-direct {v0, p0}, Lgreendroid/widget/PagedView$2;-><init>(Lgreendroid/widget/PagedView;)V

    iput-object v0, p0, Lgreendroid/widget/PagedView;->mScrollerRunnable:Ljava/lang/Runnable;

    invoke-direct {p0}, Lgreendroid/widget/PagedView;->initPagedView()V

    return-void
.end method

.method static synthetic access$0(Lgreendroid/widget/PagedView;)I
    .locals 1

    iget v0, p0, Lgreendroid/widget/PagedView;->mCurrentPage:I

    return v0
.end method

.method static synthetic access$1(Lgreendroid/widget/PagedView;)Lgreendroid/widget/PagedAdapter;
    .locals 1

    iget-object v0, p0, Lgreendroid/widget/PagedView;->mAdapter:Lgreendroid/widget/PagedAdapter;

    return-object v0
.end method

.method static synthetic access$2(Lgreendroid/widget/PagedView;I)V
    .locals 0

    iput p1, p0, Lgreendroid/widget/PagedView;->mCurrentPage:I

    return-void
.end method

.method static synthetic access$3(Lgreendroid/widget/PagedView;I)I
    .locals 1

    invoke-direct {p0, p1}, Lgreendroid/widget/PagedView;->getOffsetForPage(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$4(Lgreendroid/widget/PagedView;I)V
    .locals 0

    invoke-direct {p0, p1}, Lgreendroid/widget/PagedView;->setOffsetX(I)V

    return-void
.end method

.method static synthetic access$5(Lgreendroid/widget/PagedView;)Landroid/widget/Scroller;
    .locals 1

    iget-object v0, p0, Lgreendroid/widget/PagedView;->mScroller:Landroid/widget/Scroller;

    return-object v0
.end method

.method static synthetic access$6(Lgreendroid/widget/PagedView;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lgreendroid/widget/PagedView;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$7(Lgreendroid/widget/PagedView;)I
    .locals 1

    iget v0, p0, Lgreendroid/widget/PagedView;->mTargetPage:I

    return v0
.end method

.method static synthetic access$8(Lgreendroid/widget/PagedView;I)V
    .locals 0

    invoke-direct {p0, p1}, Lgreendroid/widget/PagedView;->performPageChange(I)V

    return-void
.end method

.method private getActualCurrentPage()I
    .locals 2

    iget v0, p0, Lgreendroid/widget/PagedView;->mTargetPage:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lgreendroid/widget/PagedView;->mTargetPage:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lgreendroid/widget/PagedView;->mCurrentPage:I

    goto :goto_0
.end method

.method private getOffsetForPage(I)I
    .locals 1
    .param p1    # I

    invoke-virtual {p0}, Lgreendroid/widget/PagedView;->getWidth()I

    move-result v0

    mul-int/2addr v0, p1

    neg-int v0, v0

    return v0
.end method

.method private getPageForOffset(I)I
    .locals 2
    .param p1    # I

    neg-int v0, p1

    invoke-virtual {p0}, Lgreendroid/widget/PagedView;->getWidth()I

    move-result v1

    div-int/2addr v0, v1

    return v0
.end method

.method private handleKeyEvent(ILandroid/view/KeyEvent;)Z
    .locals 9
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/16 v8, 0x15

    const/4 v6, 0x0

    const/4 v5, 0x1

    iget-object v4, p0, Lgreendroid/widget/PagedView;->mActiveViews:Landroid/util/SparseArray;

    invoke-virtual {p0}, Lgreendroid/widget/PagedView;->getCurrentPage()I

    move-result v7

    invoke-virtual {v4, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    if-nez v4, :cond_0

    if-nez v1, :cond_1

    :cond_0
    move v4, v6

    :goto_0
    return v4

    :cond_1
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_2
    move v4, v6

    goto :goto_0

    :sswitch_0
    invoke-virtual {v1}, Landroid/view/View;->findFocus()Landroid/view/View;

    move-result-object v0

    if-ne v0, p0, :cond_3

    const/4 v0, 0x0

    :cond_3
    const/4 v3, 0x0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v4

    if-ne v4, v8, :cond_5

    const/16 v2, 0x11

    :goto_1
    instance-of v4, v1, Landroid/view/ViewGroup;

    if-eqz v4, :cond_4

    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v7

    move-object v4, v1

    check-cast v4, Landroid/view/ViewGroup;

    invoke-virtual {v7, v4, v0, v2}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    :cond_4
    if-eqz v3, :cond_6

    if-eq v3, v1, :cond_6

    invoke-virtual {v3, v2}, Landroid/view/View;->requestFocus(I)Z

    move-result v4

    if-eqz v4, :cond_6

    move v4, v5

    goto :goto_0

    :cond_5
    const/16 v2, 0x42

    goto :goto_1

    :cond_6
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v4

    if-ne v4, v8, :cond_7

    invoke-virtual {p0}, Lgreendroid/widget/PagedView;->smoothScrollToPrevious()V

    move v4, v5

    goto :goto_0

    :cond_7
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v4

    const/16 v7, 0x16

    if-ne v4, v7, :cond_2

    invoke-virtual {p0}, Lgreendroid/widget/PagedView;->smoothScrollToNext()V

    move v4, v5

    goto :goto_0

    :sswitch_1
    invoke-virtual {p0}, Lgreendroid/widget/PagedView;->smoothScrollToNext()V

    move v4, v5

    goto :goto_0

    :sswitch_2
    invoke-virtual {p0}, Lgreendroid/widget/PagedView;->smoothScrollToPrevious()V

    move v4, v5

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x15 -> :sswitch_0
        0x16 -> :sswitch_0
        0x5c -> :sswitch_2
        0x5d -> :sswitch_1
    .end sparse-switch
.end method

.method private initPagedView()V
    .locals 5

    invoke-virtual {p0}, Lgreendroid/widget/PagedView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v3, Landroid/widget/Scroller;

    new-instance v4, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v4}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-direct {v3, v1, v4}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v3, p0, Lgreendroid/widget/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    iput v3, p0, Lgreendroid/widget/PagedView;->mPagingTouchSlop:I

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v3

    iput v3, p0, Lgreendroid/widget/PagedView;->mMaximumVelocity:I

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v3, v2, Landroid/util/DisplayMetrics;->density:F

    const/high16 v4, 0x43fa0000    # 500.0f

    mul-float/2addr v3, v4

    const/high16 v4, 0x3f000000    # 0.5f

    add-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, p0, Lgreendroid/widget/PagedView;->mMinimumVelocity:I

    return-void
.end method

.method private obtainView(I)Landroid/view/View;
    .locals 4
    .param p1    # I

    iget-object v2, p0, Lgreendroid/widget/PagedView;->mRecycler:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    iget-object v2, p0, Lgreendroid/widget/PagedView;->mAdapter:Lgreendroid/widget/PagedAdapter;

    invoke-virtual {v2, p1, v1, p0}, Lgreendroid/widget/PagedAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "PagedAdapter.getView must return a non-null View"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    if-eqz v1, :cond_1

    if-eq v0, v1, :cond_1

    :cond_1
    invoke-virtual {p0, v0}, Lgreendroid/widget/PagedView;->addView(Landroid/view/View;)V

    iget-object v2, p0, Lgreendroid/widget/PagedView;->mActiveViews:Landroid/util/SparseArray;

    invoke-virtual {v2, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    return-object v0
.end method

.method private performPageChange(I)V
    .locals 2
    .param p1    # I

    iget v0, p0, Lgreendroid/widget/PagedView;->mCurrentPage:I

    if-eq v0, p1, :cond_1

    iget-object v0, p0, Lgreendroid/widget/PagedView;->mOnPageChangeListener:Lgreendroid/widget/PagedView$OnPagedViewChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgreendroid/widget/PagedView;->mOnPageChangeListener:Lgreendroid/widget/PagedView$OnPagedViewChangeListener;

    iget v1, p0, Lgreendroid/widget/PagedView;->mCurrentPage:I

    invoke-interface {v0, p0, v1, p1}, Lgreendroid/widget/PagedView$OnPagedViewChangeListener;->onPageChanged(Lgreendroid/widget/PagedView;II)V

    :cond_0
    iput p1, p0, Lgreendroid/widget/PagedView;->mCurrentPage:I

    :cond_1
    return-void
.end method

.method private performStartTracking(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lgreendroid/widget/PagedView;->mOnPageChangeListener:Lgreendroid/widget/PagedView$OnPagedViewChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgreendroid/widget/PagedView;->mOnPageChangeListener:Lgreendroid/widget/PagedView$OnPagedViewChangeListener;

    invoke-interface {v0, p0}, Lgreendroid/widget/PagedView$OnPagedViewChangeListener;->onStartTracking(Lgreendroid/widget/PagedView;)V

    :cond_0
    iput p1, p0, Lgreendroid/widget/PagedView;->mStartMotionX:I

    iget v0, p0, Lgreendroid/widget/PagedView;->mOffsetX:I

    iput v0, p0, Lgreendroid/widget/PagedView;->mStartOffsetX:I

    return-void
.end method

.method private recycleViews(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lgreendroid/widget/PagedView;->mActiveViews:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v1, :cond_0

    return-void

    :cond_0
    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    if-lt v3, p1, :cond_1

    if-le v3, p2, :cond_2

    :cond_1
    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    invoke-virtual {p0, v4}, Lgreendroid/widget/PagedView;->removeView(Landroid/view/View;)V

    iget-object v5, p0, Lgreendroid/widget/PagedView;->mRecycler:Ljava/util/Queue;

    invoke-interface {v5, v4}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->delete(I)V

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private scrollToPage(IZ)V
    .locals 5
    .param p1    # I
    .param p2    # Z

    const/4 v4, 0x0

    iget v2, p0, Lgreendroid/widget/PagedView;->mPageCount:I

    add-int/lit8 v2, v2, -0x1

    invoke-static {p1, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result p1

    invoke-direct {p0, p1}, Lgreendroid/widget/PagedView;->getOffsetForPage(I)I

    move-result v1

    iget v2, p0, Lgreendroid/widget/PagedView;->mOffsetX:I

    sub-int v0, v1, v2

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lgreendroid/widget/PagedView;->performPageChange(I)V

    :goto_0
    return-void

    :cond_0
    if-eqz p2, :cond_1

    iput p1, p0, Lgreendroid/widget/PagedView;->mTargetPage:I

    iget-object v2, p0, Lgreendroid/widget/PagedView;->mScroller:Landroid/widget/Scroller;

    iget v3, p0, Lgreendroid/widget/PagedView;->mOffsetX:I

    invoke-virtual {v2, v3, v4, v0, v4}, Landroid/widget/Scroller;->startScroll(IIII)V

    iget-object v2, p0, Lgreendroid/widget/PagedView;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lgreendroid/widget/PagedView;->mScrollerRunnable:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_1
    invoke-direct {p0, v1}, Lgreendroid/widget/PagedView;->setOffsetX(I)V

    invoke-direct {p0, p1}, Lgreendroid/widget/PagedView;->performPageChange(I)V

    goto :goto_0
.end method

.method private setOffsetX(I)V
    .locals 6
    .param p1    # I

    iget v5, p0, Lgreendroid/widget/PagedView;->mOffsetX:I

    if-ne p1, v5, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lgreendroid/widget/PagedView;->getPageForOffset(I)I

    move-result v4

    invoke-virtual {p0}, Lgreendroid/widget/PagedView;->getWidth()I

    move-result v5

    sub-int v5, p1, v5

    add-int/lit8 v5, v5, 0x1

    invoke-direct {p0, v5}, Lgreendroid/widget/PagedView;->getPageForOffset(I)I

    move-result v1

    invoke-direct {p0, v4, v1}, Lgreendroid/widget/PagedView;->recycleViews(II)V

    iget v5, p0, Lgreendroid/widget/PagedView;->mOffsetX:I

    sub-int v3, p1, v5

    move v2, v4

    :goto_1
    if-le v2, v1, :cond_1

    iput p1, p0, Lgreendroid/widget/PagedView;->mOffsetX:I

    invoke-virtual {p0}, Lgreendroid/widget/PagedView;->invalidate()V

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lgreendroid/widget/PagedView;->mActiveViews:Landroid/util/SparseArray;

    invoke-virtual {v5, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_2

    invoke-direct {p0, v2}, Lgreendroid/widget/PagedView;->obtainView(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0, v2}, Lgreendroid/widget/PagedView;->setupView(Landroid/view/View;I)V

    :cond_2
    invoke-virtual {v0, v3}, Landroid/view/View;->offsetLeftAndRight(I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private setupView(Landroid/view/View;I)V
    .locals 9
    .param p1    # Landroid/view/View;
    .param p2    # I

    const/high16 v8, 0x40000000    # 2.0f

    const/4 v7, -0x1

    const/4 v6, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-ne v4, v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    if-nez v3, :cond_2

    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v3, v7, v7}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    :cond_2
    invoke-virtual {p0}, Lgreendroid/widget/PagedView;->getWidth()I

    move-result v4

    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    iget v5, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {v4, v6, v5}, Lgreendroid/widget/PagedView;->getChildMeasureSpec(III)I

    move-result v2

    invoke-virtual {p0}, Lgreendroid/widget/PagedView;->getHeight()I

    move-result v4

    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    iget v5, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v4, v6, v5}, Lgreendroid/widget/PagedView;->getChildMeasureSpec(III)I

    move-result v0

    invoke-virtual {p1, v2, v0}, Landroid/view/View;->measure(II)V

    iget v4, p0, Lgreendroid/widget/PagedView;->mOffsetX:I

    invoke-direct {p0, p2}, Lgreendroid/widget/PagedView;->getOffsetForPage(I)I

    move-result v5

    sub-int v1, v4, v5

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    invoke-virtual {p1, v1, v6, v4, v5}, Landroid/view/View;->layout(IIII)V

    goto :goto_0
.end method


# virtual methods
.method public getCurrentPage()I
    .locals 1

    iget v0, p0, Lgreendroid/widget/PagedView;->mCurrentPage:I

    return v0
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1    # Landroid/view/MotionEvent;

    const/4 v2, 0x1

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v1

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    :goto_0
    return v2

    :pswitch_0
    iget-boolean v1, p0, Lgreendroid/widget/PagedView;->mIsBeingDragged:Z

    if-nez v1, :cond_0

    const/16 v1, 0x9

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v0

    cmpl-float v1, v0, v4

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lgreendroid/widget/PagedView;->getCurrentPage()I

    move-result v3

    cmpg-float v1, v0, v4

    if-gez v1, :cond_1

    move v1, v2

    :goto_1
    add-int/2addr v1, v3

    invoke-virtual {p0, v1}, Lgreendroid/widget/PagedView;->smoothScrollToPage(I)V

    goto :goto_0

    :cond_1
    const/4 v1, -0x1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
    .end packed-switch
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1    # Landroid/view/MotionEvent;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v5, 0x2

    if-ne v0, v5, :cond_0

    iget-boolean v5, p0, Lgreendroid/widget/PagedView;->mIsBeingDragged:Z

    if-eqz v5, :cond_0

    :goto_0
    return v4

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    float-to-int v1, v5

    packed-switch v0, :pswitch_data_0

    :cond_1
    :goto_1
    iget-boolean v4, p0, Lgreendroid/widget/PagedView;->mIsBeingDragged:Z

    goto :goto_0

    :pswitch_0
    iput v1, p0, Lgreendroid/widget/PagedView;->mStartMotionX:I

    iget-object v5, p0, Lgreendroid/widget/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v5}, Landroid/widget/Scroller;->isFinished()Z

    move-result v5

    if-eqz v5, :cond_2

    :goto_2
    iput-boolean v3, p0, Lgreendroid/widget/PagedView;->mIsBeingDragged:Z

    iget-boolean v3, p0, Lgreendroid/widget/PagedView;->mIsBeingDragged:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lgreendroid/widget/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v3, v4}, Landroid/widget/Scroller;->forceFinished(Z)V

    iget-object v3, p0, Lgreendroid/widget/PagedView;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lgreendroid/widget/PagedView;->mScrollerRunnable:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_1

    :cond_2
    move v3, v4

    goto :goto_2

    :pswitch_1
    iget v3, p0, Lgreendroid/widget/PagedView;->mStartMotionX:I

    sub-int v3, v1, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v2

    iget v3, p0, Lgreendroid/widget/PagedView;->mPagingTouchSlop:I

    if-le v2, v3, :cond_1

    iput-boolean v4, p0, Lgreendroid/widget/PagedView;->mIsBeingDragged:Z

    invoke-direct {p0, v1}, Lgreendroid/widget/PagedView;->performStartTracking(I)V

    goto :goto_1

    :pswitch_2
    iput-boolean v3, p0, Lgreendroid/widget/PagedView;->mIsBeingDragged:Z

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    invoke-direct {p0, p1, p2}, Lgreendroid/widget/PagedView;->handleKeyEvent(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    invoke-direct {p0, p1, p2}, Lgreendroid/widget/PagedView;->handleKeyEvent(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 6
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iget v4, p0, Lgreendroid/widget/PagedView;->mPageCount:I

    if-gtz v4, :cond_1

    :cond_0
    return-void

    :cond_1
    iget v4, p0, Lgreendroid/widget/PagedView;->mOffsetX:I

    invoke-direct {p0, v4}, Lgreendroid/widget/PagedView;->getPageForOffset(I)I

    move-result v3

    iget v4, p0, Lgreendroid/widget/PagedView;->mOffsetX:I

    invoke-virtual {p0}, Lgreendroid/widget/PagedView;->getWidth()I

    move-result v5

    sub-int/2addr v4, v5

    add-int/lit8 v4, v4, 0x1

    invoke-direct {p0, v4}, Lgreendroid/widget/PagedView;->getPageForOffset(I)I

    move-result v1

    invoke-direct {p0, v3, v1}, Lgreendroid/widget/PagedView;->recycleViews(II)V

    move v2, v3

    :goto_0
    if-gt v2, v1, :cond_0

    iget-object v4, p0, Lgreendroid/widget/PagedView;->mActiveViews:Landroid/util/SparseArray;

    invoke-virtual {v4, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_2

    invoke-direct {p0, v2}, Lgreendroid/widget/PagedView;->obtainView(I)Landroid/view/View;

    move-result-object v0

    :cond_2
    invoke-direct {p0, v0, v2}, Lgreendroid/widget/PagedView;->setupView(Landroid/view/View;I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 9
    .param p1    # I
    .param p2    # I

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v6

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v8, p0, Lgreendroid/widget/PagedView;->mAdapter:Lgreendroid/widget/PagedAdapter;

    if-nez v8, :cond_4

    const/4 v5, 0x0

    :goto_0
    if-lez v5, :cond_3

    if-eqz v6, :cond_0

    if-nez v3, :cond_1

    :cond_0
    iget v8, p0, Lgreendroid/widget/PagedView;->mCurrentPage:I

    invoke-direct {p0, v8}, Lgreendroid/widget/PagedView;->obtainView(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, Lgreendroid/widget/PagedView;->measureChild(Landroid/view/View;II)V

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    :cond_1
    if-nez v6, :cond_2

    move v7, v2

    :cond_2
    if-nez v3, :cond_3

    move v4, v1

    :cond_3
    invoke-virtual {p0, v7, v4}, Lgreendroid/widget/PagedView;->setMeasuredDimension(II)V

    return-void

    :cond_4
    iget-object v8, p0, Lgreendroid/widget/PagedView;->mAdapter:Lgreendroid/widget/PagedAdapter;

    invoke-virtual {v8}, Lgreendroid/widget/PagedAdapter;->getCount()I

    move-result v5

    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1    # Landroid/os/Parcelable;

    move-object v0, p1

    check-cast v0, Lgreendroid/widget/PagedView$SavedState;

    invoke-virtual {v0}, Lgreendroid/widget/PagedView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget v1, v0, Lgreendroid/widget/PagedView$SavedState;->currentPage:I

    iput v1, p0, Lgreendroid/widget/PagedView;->mCurrentPage:I

    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    new-instance v0, Lgreendroid/widget/PagedView$SavedState;

    invoke-direct {v0, v1}, Lgreendroid/widget/PagedView$SavedState;-><init>(Landroid/os/Parcelable;)V

    iget v2, p0, Lgreendroid/widget/PagedView;->mCurrentPage:I

    iput v2, v0, Lgreendroid/widget/PagedView$SavedState;->currentPage:I

    return-object v0
.end method

.method protected onSizeChanged(IIII)V
    .locals 4
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onSizeChanged(IIII)V

    int-to-double v0, p1

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, Lgreendroid/widget/PagedView;->mPageSlop:I

    iget v0, p0, Lgreendroid/widget/PagedView;->mCurrentPage:I

    invoke-direct {p0, v0}, Lgreendroid/widget/PagedView;->getOffsetForPage(I)I

    move-result v0

    iput v0, p0, Lgreendroid/widget/PagedView;->mOffsetX:I

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1    # Landroid/view/MotionEvent;

    const/4 v7, -0x1

    const/4 v6, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    float-to-int v5, v8

    iget-object v8, p0, Lgreendroid/widget/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v8, :cond_0

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v8

    iput-object v8, p0, Lgreendroid/widget/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    :cond_0
    iget-object v8, p0, Lgreendroid/widget/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v8, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    packed-switch v0, :pswitch_data_0

    :cond_1
    :goto_0
    return v6

    :pswitch_0
    iget-object v7, p0, Lgreendroid/widget/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v7}, Landroid/widget/Scroller;->isFinished()Z

    move-result v7

    if-nez v7, :cond_2

    iget-object v7, p0, Lgreendroid/widget/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v7, v6}, Landroid/widget/Scroller;->forceFinished(Z)V

    iget-object v7, p0, Lgreendroid/widget/PagedView;->mHandler:Landroid/os/Handler;

    iget-object v8, p0, Lgreendroid/widget/PagedView;->mScrollerRunnable:Ljava/lang/Runnable;

    invoke-virtual {v7, v8}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :cond_2
    invoke-direct {p0, v5}, Lgreendroid/widget/PagedView;->performStartTracking(I)V

    goto :goto_0

    :pswitch_1
    iget v7, p0, Lgreendroid/widget/PagedView;->mStartOffsetX:I

    iget v8, p0, Lgreendroid/widget/PagedView;->mStartMotionX:I

    sub-int/2addr v8, v5

    sub-int v3, v7, v8

    if-gtz v3, :cond_3

    iget v7, p0, Lgreendroid/widget/PagedView;->mPageCount:I

    add-int/lit8 v7, v7, -0x1

    invoke-direct {p0, v7}, Lgreendroid/widget/PagedView;->getOffsetForPage(I)I

    move-result v7

    if-ge v3, v7, :cond_4

    :cond_3
    iget v7, p0, Lgreendroid/widget/PagedView;->mOffsetX:I

    iput v7, p0, Lgreendroid/widget/PagedView;->mStartOffsetX:I

    iput v5, p0, Lgreendroid/widget/PagedView;->mStartMotionX:I

    goto :goto_0

    :cond_4
    invoke-direct {p0, v3}, Lgreendroid/widget/PagedView;->setOffsetX(I)V

    goto :goto_0

    :pswitch_2
    iget v8, p0, Lgreendroid/widget/PagedView;->mStartOffsetX:I

    iget v9, p0, Lgreendroid/widget/PagedView;->mStartMotionX:I

    sub-int/2addr v9, v5

    sub-int/2addr v8, v9

    invoke-direct {p0, v8}, Lgreendroid/widget/PagedView;->setOffsetX(I)V

    const/4 v1, 0x0

    iget v8, p0, Lgreendroid/widget/PagedView;->mStartMotionX:I

    sub-int v4, v8, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v8

    iget v9, p0, Lgreendroid/widget/PagedView;->mPageSlop:I

    if-le v8, v9, :cond_8

    if-lez v4, :cond_7

    move v1, v6

    :cond_5
    :goto_1
    iget-object v7, p0, Lgreendroid/widget/PagedView;->mOnPageChangeListener:Lgreendroid/widget/PagedView$OnPagedViewChangeListener;

    if-eqz v7, :cond_6

    iget-object v7, p0, Lgreendroid/widget/PagedView;->mOnPageChangeListener:Lgreendroid/widget/PagedView$OnPagedViewChangeListener;

    invoke-interface {v7, p0}, Lgreendroid/widget/PagedView$OnPagedViewChangeListener;->onStopTracking(Lgreendroid/widget/PagedView;)V

    :cond_6
    invoke-direct {p0}, Lgreendroid/widget/PagedView;->getActualCurrentPage()I

    move-result v7

    add-int/2addr v7, v1

    invoke-virtual {p0, v7}, Lgreendroid/widget/PagedView;->smoothScrollToPage(I)V

    iget-object v7, p0, Lgreendroid/widget/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v7, :cond_1

    iget-object v7, p0, Lgreendroid/widget/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v7}, Landroid/view/VelocityTracker;->recycle()V

    const/4 v7, 0x0

    iput-object v7, p0, Lgreendroid/widget/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    goto :goto_0

    :cond_7
    move v1, v7

    goto :goto_1

    :cond_8
    iget-object v8, p0, Lgreendroid/widget/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    const/16 v9, 0x3e8

    iget v10, p0, Lgreendroid/widget/PagedView;->mMaximumVelocity:I

    int-to-float v10, v10

    invoke-virtual {v8, v9, v10}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    iget-object v8, p0, Lgreendroid/widget/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v8}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v8

    float-to-int v2, v8

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v8

    iget v9, p0, Lgreendroid/widget/PagedView;->mMinimumVelocity:I

    if-le v8, v9, :cond_5

    if-lez v2, :cond_9

    move v1, v7

    :goto_2
    goto :goto_1

    :cond_9
    move v1, v6

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public scrollToNext()V
    .locals 1

    invoke-direct {p0}, Lgreendroid/widget/PagedView;->getActualCurrentPage()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lgreendroid/widget/PagedView;->scrollToPage(I)V

    return-void
.end method

.method public scrollToPage(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lgreendroid/widget/PagedView;->scrollToPage(IZ)V

    return-void
.end method

.method public scrollToPrevious()V
    .locals 1

    invoke-direct {p0}, Lgreendroid/widget/PagedView;->getActualCurrentPage()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lgreendroid/widget/PagedView;->scrollToPage(I)V

    return-void
.end method

.method public setAdapter(Lgreendroid/widget/PagedAdapter;)V
    .locals 3
    .param p1    # Lgreendroid/widget/PagedAdapter;

    const/4 v2, 0x0

    iget-object v0, p0, Lgreendroid/widget/PagedView;->mAdapter:Lgreendroid/widget/PagedAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgreendroid/widget/PagedView;->mAdapter:Lgreendroid/widget/PagedAdapter;

    iget-object v1, p0, Lgreendroid/widget/PagedView;->mDataSetObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lgreendroid/widget/PagedAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    :cond_0
    iget-object v0, p0, Lgreendroid/widget/PagedView;->mRecycler:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    iget-object v0, p0, Lgreendroid/widget/PagedView;->mActiveViews:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    invoke-virtual {p0}, Lgreendroid/widget/PagedView;->removeAllViews()V

    iput-object p1, p0, Lgreendroid/widget/PagedView;->mAdapter:Lgreendroid/widget/PagedAdapter;

    const/4 v0, -0x1

    iput v0, p0, Lgreendroid/widget/PagedView;->mTargetPage:I

    iput v2, p0, Lgreendroid/widget/PagedView;->mCurrentPage:I

    iput v2, p0, Lgreendroid/widget/PagedView;->mOffsetX:I

    iget-object v0, p0, Lgreendroid/widget/PagedView;->mAdapter:Lgreendroid/widget/PagedAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgreendroid/widget/PagedView;->mAdapter:Lgreendroid/widget/PagedAdapter;

    iget-object v1, p0, Lgreendroid/widget/PagedView;->mDataSetObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lgreendroid/widget/PagedAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    iget-object v0, p0, Lgreendroid/widget/PagedView;->mAdapter:Lgreendroid/widget/PagedAdapter;

    invoke-virtual {v0}, Lgreendroid/widget/PagedAdapter;->getCount()I

    move-result v0

    iput v0, p0, Lgreendroid/widget/PagedView;->mPageCount:I

    :cond_1
    invoke-virtual {p0}, Lgreendroid/widget/PagedView;->requestLayout()V

    invoke-virtual {p0}, Lgreendroid/widget/PagedView;->invalidate()V

    return-void
.end method

.method public setOnPageChangeListener(Lgreendroid/widget/PagedView$OnPagedViewChangeListener;)V
    .locals 0
    .param p1    # Lgreendroid/widget/PagedView$OnPagedViewChangeListener;

    iput-object p1, p0, Lgreendroid/widget/PagedView;->mOnPageChangeListener:Lgreendroid/widget/PagedView$OnPagedViewChangeListener;

    return-void
.end method

.method public smoothScrollToNext()V
    .locals 1

    invoke-direct {p0}, Lgreendroid/widget/PagedView;->getActualCurrentPage()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lgreendroid/widget/PagedView;->smoothScrollToPage(I)V

    return-void
.end method

.method public smoothScrollToPage(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lgreendroid/widget/PagedView;->scrollToPage(IZ)V

    return-void
.end method

.method public smoothScrollToPrevious()V
    .locals 1

    invoke-direct {p0}, Lgreendroid/widget/PagedView;->getActualCurrentPage()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lgreendroid/widget/PagedView;->smoothScrollToPage(I)V

    return-void
.end method
