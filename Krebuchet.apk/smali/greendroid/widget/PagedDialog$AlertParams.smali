.class Lgreendroid/widget/PagedDialog$AlertParams;
.super Ljava/lang/Object;
.source "PagedDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgreendroid/widget/PagedDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AlertParams"
.end annotation


# instance fields
.field public mAdapter:Lgreendroid/widget/PagedAdapter;

.field public mCancelable:Z

.field public mContentHeight:I

.field public mContentWidth:I

.field public final mContext:Landroid/content/Context;

.field public mIcon:Landroid/graphics/drawable/Drawable;

.field public mIconId:I

.field public final mInflater:Landroid/view/LayoutInflater;

.field public mNegativeButtonListener:Landroid/content/DialogInterface$OnClickListener;

.field public mNegativeButtonText:Ljava/lang/CharSequence;

.field public mOnCancelListener:Landroid/content/DialogInterface$OnCancelListener;

.field public mOnKeyListener:Landroid/content/DialogInterface$OnKeyListener;

.field public mPositiveButtonListener:Landroid/content/DialogInterface$OnClickListener;

.field public mPositiveButtonText:Ljava/lang/CharSequence;

.field public mTitle:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lgreendroid/widget/PagedDialog$AlertParams;->mIconId:I

    iput-object p1, p0, Lgreendroid/widget/PagedDialog$AlertParams;->mContext:Landroid/content/Context;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lgreendroid/widget/PagedDialog$AlertParams;->mCancelable:Z

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lgreendroid/widget/PagedDialog$AlertParams;->mInflater:Landroid/view/LayoutInflater;

    return-void
.end method


# virtual methods
.method public apply(Lgreendroid/widget/PagedDialog;)V
    .locals 4
    .param p1    # Lgreendroid/widget/PagedDialog;

    const/4 v3, 0x0

    iget-object v0, p0, Lgreendroid/widget/PagedDialog$AlertParams;->mTitle:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgreendroid/widget/PagedDialog$AlertParams;->mTitle:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0}, Lgreendroid/widget/PagedDialog;->setTitle(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v0, p0, Lgreendroid/widget/PagedDialog$AlertParams;->mIcon:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgreendroid/widget/PagedDialog$AlertParams;->mIcon:Landroid/graphics/drawable/Drawable;

    # invokes: Lgreendroid/widget/PagedDialog;->setIcon(Landroid/graphics/drawable/Drawable;)V
    invoke-static {p1, v0}, Lgreendroid/widget/PagedDialog;->access$5(Lgreendroid/widget/PagedDialog;Landroid/graphics/drawable/Drawable;)V

    :cond_1
    iget v0, p0, Lgreendroid/widget/PagedDialog$AlertParams;->mIconId:I

    if-ltz v0, :cond_2

    iget v0, p0, Lgreendroid/widget/PagedDialog$AlertParams;->mIconId:I

    # invokes: Lgreendroid/widget/PagedDialog;->setIcon(I)V
    invoke-static {p1, v0}, Lgreendroid/widget/PagedDialog;->access$6(Lgreendroid/widget/PagedDialog;I)V

    :cond_2
    iget v0, p0, Lgreendroid/widget/PagedDialog$AlertParams;->mContentWidth:I

    if-lez v0, :cond_3

    iget v0, p0, Lgreendroid/widget/PagedDialog$AlertParams;->mContentHeight:I

    if-lez v0, :cond_3

    iget v0, p0, Lgreendroid/widget/PagedDialog$AlertParams;->mContentWidth:I

    iget v1, p0, Lgreendroid/widget/PagedDialog$AlertParams;->mContentHeight:I

    invoke-virtual {p1, v0, v1}, Lgreendroid/widget/PagedDialog;->setContentLayout(II)V

    :cond_3
    iget-object v0, p0, Lgreendroid/widget/PagedDialog$AlertParams;->mPositiveButtonText:Ljava/lang/CharSequence;

    if-eqz v0, :cond_4

    const/4 v0, -0x1

    iget-object v1, p0, Lgreendroid/widget/PagedDialog$AlertParams;->mPositiveButtonText:Ljava/lang/CharSequence;

    iget-object v2, p0, Lgreendroid/widget/PagedDialog$AlertParams;->mPositiveButtonListener:Landroid/content/DialogInterface$OnClickListener;

    # invokes: Lgreendroid/widget/PagedDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V
    invoke-static {p1, v0, v1, v2, v3}, Lgreendroid/widget/PagedDialog;->access$7(Lgreendroid/widget/PagedDialog;ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V

    :cond_4
    iget-object v0, p0, Lgreendroid/widget/PagedDialog$AlertParams;->mNegativeButtonText:Ljava/lang/CharSequence;

    if-eqz v0, :cond_5

    const/4 v0, -0x2

    iget-object v1, p0, Lgreendroid/widget/PagedDialog$AlertParams;->mNegativeButtonText:Ljava/lang/CharSequence;

    iget-object v2, p0, Lgreendroid/widget/PagedDialog$AlertParams;->mNegativeButtonListener:Landroid/content/DialogInterface$OnClickListener;

    # invokes: Lgreendroid/widget/PagedDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V
    invoke-static {p1, v0, v1, v2, v3}, Lgreendroid/widget/PagedDialog;->access$7(Lgreendroid/widget/PagedDialog;ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;Landroid/os/Message;)V

    :cond_5
    iget-object v0, p0, Lgreendroid/widget/PagedDialog$AlertParams;->mAdapter:Lgreendroid/widget/PagedAdapter;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lgreendroid/widget/PagedDialog$AlertParams;->mAdapter:Lgreendroid/widget/PagedAdapter;

    invoke-virtual {p1, v0}, Lgreendroid/widget/PagedDialog;->setAdapter(Lgreendroid/widget/PagedAdapter;)V

    :cond_6
    return-void
.end method
