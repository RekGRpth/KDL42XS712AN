.class public interface abstract Lgreendroid/widget/PagedView$OnPagedViewChangeListener;
.super Ljava/lang/Object;
.source "PagedView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgreendroid/widget/PagedView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnPagedViewChangeListener"
.end annotation


# virtual methods
.method public abstract onPageChanged(Lgreendroid/widget/PagedView;II)V
.end method

.method public abstract onStartTracking(Lgreendroid/widget/PagedView;)V
.end method

.method public abstract onStopTracking(Lgreendroid/widget/PagedView;)V
.end method
