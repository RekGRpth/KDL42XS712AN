.class public Lnet/londatiga/android/ArrowedPopupWindow;
.super Lnet/londatiga/android/PopupWindows;
.source "ArrowedPopupWindow.java"

# interfaces
.implements Landroid/widget/PopupWindow$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/londatiga/android/ArrowedPopupWindow$OnDismissListener;
    }
.end annotation


# static fields
.field public static final ANIM_AUTO:I = 0x5

.field public static final ANIM_GROW_FROM_CENTER:I = 0x3

.field public static final ANIM_GROW_FROM_LEFT:I = 0x1

.field public static final ANIM_GROW_FROM_RIGHT:I = 0x2

.field public static final ANIM_REFLECT:I = 0x4

.field private static final TAG:Ljava/lang/String; = "ArrowedPopupWindow"


# instance fields
.field private mAnimStyle:I

.field private mArrowDown:Landroid/widget/ImageView;

.field private mArrowUp:Landroid/widget/ImageView;

.field private mContainer:Landroid/widget/FrameLayout;

.field protected mContentView:Landroid/view/ViewGroup;

.field protected mDismissListener:Lnet/londatiga/android/ArrowedPopupWindow$OnDismissListener;

.field protected mInflater:Landroid/view/LayoutInflater;

.field private mRootView:Landroid/view/ViewGroup;

.field private rootWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I

    invoke-direct {p0, p1}, Lnet/londatiga/android/PopupWindows;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput v0, p0, Lnet/londatiga/android/ArrowedPopupWindow;->rootWidth:I

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mInflater:Landroid/view/LayoutInflater;

    const v0, 0x7f03000a    # com.konka.avenger.R.layout.arrowed_popup_window

    invoke-direct {p0, v0}, Lnet/londatiga/android/ArrowedPopupWindow;->setRootViewId(I)V

    iput p2, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mAnimStyle:I

    return-void
.end method

.method private setAnimationStyle(IIZ)V
    .locals 7
    .param p1    # I
    .param p2    # I
    .param p3    # Z

    const v1, 0x7f0e0020    # com.konka.avenger.R.style.Animations_QuickAction_PopUpMenu_Left

    const v4, 0x7f0e001f    # com.konka.avenger.R.style.Animations_QuickAction_PopUpMenu_Center

    const v3, 0x7f0e001c    # com.konka.avenger.R.style.Animations_QuickAction_PopDownMenu_Right

    const v2, 0x7f0e001b    # com.konka.avenger.R.style.Animations_QuickAction_PopDownMenu_Left

    const v5, 0x7f0e001a    # com.konka.avenger.R.style.Animations_QuickAction_PopDownMenu_Center

    iget-object v6, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mArrowUp:Landroid/widget/ImageView;

    invoke-virtual {v6}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    sub-int v0, p2, v6

    iget v6, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mAnimStyle:I

    packed-switch v6, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v3, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mWindow:Landroid/widget/PopupWindow;

    if-eqz p3, :cond_0

    :goto_1
    invoke-virtual {v3, v1}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    goto :goto_0

    :cond_0
    move v1, v2

    goto :goto_1

    :pswitch_1
    iget-object v2, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mWindow:Landroid/widget/PopupWindow;

    if-eqz p3, :cond_1

    const v1, 0x7f0e0021    # com.konka.avenger.R.style.Animations_QuickAction_PopUpMenu_Right

    :goto_2
    invoke-virtual {v2, v1}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    goto :goto_0

    :cond_1
    move v1, v3

    goto :goto_2

    :pswitch_2
    iget-object v2, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mWindow:Landroid/widget/PopupWindow;

    if-eqz p3, :cond_2

    move v1, v4

    :goto_3
    invoke-virtual {v2, v1}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    goto :goto_0

    :cond_2
    move v1, v5

    goto :goto_3

    :pswitch_3
    iget-object v2, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mWindow:Landroid/widget/PopupWindow;

    if-eqz p3, :cond_3

    const v1, 0x7f0e0022    # com.konka.avenger.R.style.Animations_QuickAction_PopUpMenu_Reflect

    :goto_4
    invoke-virtual {v2, v1}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    goto :goto_0

    :cond_3
    const v1, 0x7f0e001d    # com.konka.avenger.R.style.Animations_QuickAction_PopDownMenu_Reflect

    goto :goto_4

    :pswitch_4
    div-int/lit8 v6, p1, 0x4

    if-gt v0, v6, :cond_5

    iget-object v3, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mWindow:Landroid/widget/PopupWindow;

    if-eqz p3, :cond_4

    :goto_5
    invoke-virtual {v3, v1}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_5

    :cond_5
    div-int/lit8 v1, p1, 0x4

    if-le v0, v1, :cond_7

    div-int/lit8 v1, p1, 0x4

    mul-int/lit8 v1, v1, 0x3

    if-ge v0, v1, :cond_7

    iget-object v1, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mWindow:Landroid/widget/PopupWindow;

    if-eqz p3, :cond_6

    :goto_6
    invoke-virtual {v1, v4}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    goto :goto_0

    :cond_6
    move v4, v5

    goto :goto_6

    :cond_7
    iget-object v1, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mWindow:Landroid/widget/PopupWindow;

    if-eqz p3, :cond_8

    const v3, 0x7f0e0021    # com.konka.avenger.R.style.Animations_QuickAction_PopUpMenu_Right

    :cond_8
    invoke-virtual {v1, v3}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private setRootViewId(I)V
    .locals 3
    .param p1    # I

    const/4 v2, -0x2

    iget-object v0, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mInflater:Landroid/view/LayoutInflater;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mRootView:Landroid/view/ViewGroup;

    iget-object v0, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mRootView:Landroid/view/ViewGroup;

    const v1, 0x7f0d0025    # com.konka.avenger.R.id.arrow_down

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mArrowDown:Landroid/widget/ImageView;

    iget-object v0, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mRootView:Landroid/view/ViewGroup;

    const v1, 0x7f0d0024    # com.konka.avenger.R.id.arrow_up

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mArrowUp:Landroid/widget/ImageView;

    iget-object v0, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mRootView:Landroid/view/ViewGroup;

    const v1, 0x7f0d0023    # com.konka.avenger.R.id.container

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mContainer:Landroid/widget/FrameLayout;

    iget-object v0, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mRootView:Landroid/view/ViewGroup;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mRootView:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lnet/londatiga/android/ArrowedPopupWindow;->setRootView(Landroid/view/View;)V

    return-void
.end method

.method private showArrow(II)V
    .locals 5
    .param p1    # I
    .param p2    # I

    const v4, 0x7f0d0024    # com.konka.avenger.R.id.arrow_up

    if-ne p1, v4, :cond_0

    iget-object v3, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mArrowUp:Landroid/widget/ImageView;

    :goto_0
    if-ne p1, v4, :cond_1

    iget-object v1, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mArrowDown:Landroid/widget/ImageView;

    :goto_1
    iget-object v4, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mArrowUp:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v0

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    div-int/lit8 v4, v0, 0x2

    sub-int v4, p2, v4

    iput v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    const/4 v4, 0x4

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    iget-object v3, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mArrowDown:Landroid/widget/ImageView;

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mArrowUp:Landroid/widget/ImageView;

    goto :goto_1
.end method


# virtual methods
.method public onDismiss()V
    .locals 1

    iget-object v0, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mDismissListener:Lnet/londatiga/android/ArrowedPopupWindow$OnDismissListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mDismissListener:Lnet/londatiga/android/ArrowedPopupWindow$OnDismissListener;

    invoke-interface {v0}, Lnet/londatiga/android/ArrowedPopupWindow$OnDismissListener;->onDismiss()V

    :cond_0
    return-void
.end method

.method public setAnimStyle(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mAnimStyle:I

    return-void
.end method

.method public setContentView(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lnet/londatiga/android/ArrowedPopupWindow;->setContentView(ILandroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public setContentView(ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 3
    .param p1    # I
    .param p2    # Landroid/view/ViewGroup$LayoutParams;

    iget-object v1, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mInflater:Landroid/view/LayoutInflater;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p0, v0, p2}, Lnet/londatiga/android/ArrowedPopupWindow;->setContentView(Landroid/view/ViewGroup;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public setContentView(Landroid/view/ViewGroup;)V
    .locals 1
    .param p1    # Landroid/view/ViewGroup;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lnet/londatiga/android/ArrowedPopupWindow;->setContentView(Landroid/view/ViewGroup;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public setContentView(Landroid/view/ViewGroup;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/view/ViewGroup$LayoutParams;

    iget-object v0, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mContainer:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    :cond_0
    if-eqz p2, :cond_1

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_1
    iget-object v0, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    iput-object p1, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mContentView:Landroid/view/ViewGroup;

    return-void
.end method

.method public setOnDismissListener(Lnet/londatiga/android/ArrowedPopupWindow$OnDismissListener;)V
    .locals 0
    .param p1    # Lnet/londatiga/android/ArrowedPopupWindow$OnDismissListener;

    invoke-virtual {p0, p0}, Lnet/londatiga/android/ArrowedPopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    iput-object p1, p0, Lnet/londatiga/android/ArrowedPopupWindow;->mDismissListener:Lnet/londatiga/android/ArrowedPopupWindow$OnDismissListener;

    return-void
.end method

.method public show(Landroid/view/View;)V
    .locals 24
    .param p1    # Landroid/view/View;

    invoke-virtual/range {p0 .. p0}, Lnet/londatiga/android/ArrowedPopupWindow;->preShow()V

    const/16 v19, 0x2

    move/from16 v0, v19

    new-array v10, v0, [I

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/view/View;->getLocationOnScreen([I)V

    new-instance v5, Landroid/graphics/Rect;

    const/16 v19, 0x0

    aget v19, v10, v19

    const/16 v20, 0x1

    aget v20, v10, v20

    const/16 v21, 0x0

    aget v21, v10, v21

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getWidth()I

    move-result v22

    add-int v21, v21, v22

    const/16 v22, 0x1

    aget v22, v10, v22

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getHeight()I

    move-result v23

    add-int v22, v22, v23

    move/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-direct {v5, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/londatiga/android/ArrowedPopupWindow;->mRootView:Landroid/view/ViewGroup;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const/16 v21, 0x0

    invoke-static/range {v20 .. v21}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v20

    const/16 v21, 0x0

    const/16 v22, 0x0

    invoke-static/range {v21 .. v22}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v21

    invoke-virtual/range {v19 .. v21}, Landroid/view/ViewGroup;->measure(II)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/londatiga/android/ArrowedPopupWindow;->mRootView:Landroid/view/ViewGroup;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v14

    const-string v19, "ArrowedPopupWindow"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "measuredHeight="

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget v0, v0, Lnet/londatiga/android/ArrowedPopupWindow;->rootWidth:I

    move/from16 v19, v0

    if-nez v19, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/londatiga/android/ArrowedPopupWindow;->mRootView:Landroid/view/ViewGroup;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lnet/londatiga/android/ArrowedPopupWindow;->rootWidth:I

    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/londatiga/android/ArrowedPopupWindow;->mWindowManager:Landroid/view/WindowManager;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/view/Display;->getWidth()I

    move-result v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/londatiga/android/ArrowedPopupWindow;->mWindowManager:Landroid/view/WindowManager;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/view/Display;->getHeight()I

    move-result v15

    iget v0, v5, Landroid/graphics/Rect;->left:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lnet/londatiga/android/ArrowedPopupWindow;->rootWidth:I

    move/from16 v20, v0

    add-int v19, v19, v20

    move/from16 v0, v19

    move/from16 v1, v16

    if-le v0, v1, :cond_3

    move-object/from16 v0, p0

    iget v0, v0, Lnet/londatiga/android/ArrowedPopupWindow;->rootWidth:I

    move/from16 v19, v0

    sub-int v19, v16, v19

    add-int/lit8 v17, v19, -0x1

    if-gez v17, :cond_1

    const/16 v17, 0x0

    :cond_1
    invoke-virtual {v5}, Landroid/graphics/Rect;->centerX()I

    move-result v19

    sub-int v6, v19, v17

    :goto_0
    const-string v19, "ArrowedPopupWindow"

    new-instance v20, Ljava/lang/StringBuilder;

    const-string v21, "xpos="

    invoke-direct/range {v20 .. v21}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v20

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", measuredWidth="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget v0, v0, Lnet/londatiga/android/ArrowedPopupWindow;->rootWidth:I

    move/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ", rightmost="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, p0

    iget v0, v0, Lnet/londatiga/android/ArrowedPopupWindow;->rootWidth:I

    move/from16 v21, v0

    add-int v21, v21, v17

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v8, v5, Landroid/graphics/Rect;->top:I

    iget v0, v5, Landroid/graphics/Rect;->bottom:I

    move/from16 v19, v0

    sub-int v7, v15, v19

    if-le v8, v7, :cond_5

    const/4 v13, 0x1

    :goto_1
    if-eqz v13, :cond_7

    if-le v14, v8, :cond_6

    const/16 v18, 0xf

    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/londatiga/android/ArrowedPopupWindow;->mContainer:Landroid/widget/FrameLayout;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getHeight()I

    move-result v19

    sub-int v19, v8, v19

    move/from16 v0, v19

    iput v0, v9, Landroid/view/ViewGroup$LayoutParams;->height:I

    :cond_2
    :goto_2
    if-eqz v13, :cond_8

    const v19, 0x7f0d0025    # com.konka.avenger.R.id.arrow_down

    :goto_3
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1, v6}, Lnet/londatiga/android/ArrowedPopupWindow;->showArrow(II)V

    invoke-virtual {v5}, Landroid/graphics/Rect;->centerX()I

    move-result v19

    move-object/from16 v0, p0

    move/from16 v1, v16

    move/from16 v2, v19

    invoke-direct {v0, v1, v2, v13}, Lnet/londatiga/android/ArrowedPopupWindow;->setAnimationStyle(IIZ)V

    move-object/from16 v0, p0

    iget v0, v0, Lnet/londatiga/android/ArrowedPopupWindow;->rootWidth:I

    move/from16 v19, v0

    move/from16 v0, v19

    move/from16 v1, v16

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v12

    invoke-static {v14, v15}, Ljava/lang/Math;->min(II)I

    move-result v11

    const/high16 v19, 0x40000000    # 2.0f

    move/from16 v0, v19

    invoke-static {v12, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v19

    const/high16 v20, 0x40000000    # 2.0f

    move/from16 v0, v20

    invoke-static {v11, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v20

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lnet/londatiga/android/ArrowedPopupWindow;->setMeasureSpecs(II)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/londatiga/android/ArrowedPopupWindow;->mWindow:Landroid/widget/PopupWindow;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    move/from16 v2, v20

    move/from16 v3, v17

    move/from16 v4, v18

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/londatiga/android/ArrowedPopupWindow;->mContainer:Landroid/widget/FrameLayout;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/widget/FrameLayout;->requestFocus()Z

    return-void

    :cond_3
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getWidth()I

    move-result v19

    move-object/from16 v0, p0

    iget v0, v0, Lnet/londatiga/android/ArrowedPopupWindow;->rootWidth:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-le v0, v1, :cond_4

    invoke-virtual {v5}, Landroid/graphics/Rect;->centerX()I

    move-result v19

    move-object/from16 v0, p0

    iget v0, v0, Lnet/londatiga/android/ArrowedPopupWindow;->rootWidth:I

    move/from16 v20, v0

    div-int/lit8 v20, v20, 0x2

    sub-int v17, v19, v20

    :goto_4
    invoke-virtual {v5}, Landroid/graphics/Rect;->centerX()I

    move-result v19

    sub-int v6, v19, v17

    goto/16 :goto_0

    :cond_4
    iget v0, v5, Landroid/graphics/Rect;->left:I

    move/from16 v17, v0

    goto :goto_4

    :cond_5
    const/4 v13, 0x0

    goto/16 :goto_1

    :cond_6
    iget v0, v5, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    sub-int v18, v19, v14

    goto/16 :goto_2

    :cond_7
    iget v0, v5, Landroid/graphics/Rect;->bottom:I

    move/from16 v18, v0

    if-le v14, v7, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/londatiga/android/ArrowedPopupWindow;->mContainer:Landroid/widget/FrameLayout;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    iput v7, v9, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto/16 :goto_2

    :cond_8
    const v19, 0x7f0d0024    # com.konka.avenger.R.id.arrow_up

    goto/16 :goto_3
.end method
