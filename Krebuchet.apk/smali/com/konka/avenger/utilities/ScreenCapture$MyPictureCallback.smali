.class final Lcom/konka/avenger/utilities/ScreenCapture$MyPictureCallback;
.super Ljava/lang/Object;
.source "ScreenCapture.java"

# interfaces
.implements Landroid/hardware/Camera$PictureCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/avenger/utilities/ScreenCapture;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "MyPictureCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/avenger/utilities/ScreenCapture;


# direct methods
.method constructor <init>(Lcom/konka/avenger/utilities/ScreenCapture;)V
    .locals 3

    iput-object p1, p0, Lcom/konka/avenger/utilities/ScreenCapture$MyPictureCallback;->this$0:Lcom/konka/avenger/utilities/ScreenCapture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v1, "ScreenCapture"

    const-string v2, "==========DWinCamera init!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x3

    invoke-static {v1}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v1

    iput-object v1, p1, Lcom/konka/avenger/utilities/ScreenCapture;->mCameraDevice:Landroid/hardware/Camera;

    iget-object v1, p1, Lcom/konka/avenger/utilities/ScreenCapture;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    const/16 v1, 0x280

    const/16 v2, 0x168

    invoke-virtual {v0, v1, v2}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    iget-object v1, p1, Lcom/konka/avenger/utilities/ScreenCapture;->mCameraDevice:Landroid/hardware/Camera;

    invoke-virtual {v1, v0}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    return-void
.end method


# virtual methods
.method public onPictureTaken([BLandroid/hardware/Camera;)V
    .locals 10
    .param p1    # [B
    .param p2    # Landroid/hardware/Camera;

    :try_start_0
    const-string v7, "ScreenCapture"

    const-string v8, "catch picture data -----------------------"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    sget-object v7, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v7, v2, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    const/4 v7, 0x0

    array-length v8, p1

    invoke-static {p1, v7, v8, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    const-string v7, "ScreenCapture"

    const-string v8, "start to update the widget background -----------------------"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    iget-object v7, p0, Lcom/konka/avenger/utilities/ScreenCapture$MyPictureCallback;->this$0:Lcom/konka/avenger/utilities/ScreenCapture;

    iget-wide v7, v7, Lcom/konka/avenger/utilities/ScreenCapture;->time1:J

    sub-long v3, v5, v7

    const-string v7, "ScreenCapture"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "------------------------>end onPreviewFrame time = "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
