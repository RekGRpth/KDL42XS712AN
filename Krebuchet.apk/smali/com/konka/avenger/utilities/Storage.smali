.class public Lcom/konka/avenger/utilities/Storage;
.super Ljava/lang/Object;
.source "Storage.java"


# static fields
.field static final TAG:Ljava/lang/String; = "Storage"


# instance fields
.field private mDataFileStats:Landroid/os/StatFs;

.field private mFreeStorage:J

.field private mSDCardFileStats:Landroid/os/StatFs;

.field private mTotalStorage:J

.field private mUsedStorage:J


# direct methods
.method public constructor <init>()V
    .locals 2

    const-wide/16 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v0, p0, Lcom/konka/avenger/utilities/Storage;->mTotalStorage:J

    iput-wide v0, p0, Lcom/konka/avenger/utilities/Storage;->mFreeStorage:J

    iput-wide v0, p0, Lcom/konka/avenger/utilities/Storage;->mUsedStorage:J

    return-void
.end method


# virtual methods
.method public getDiskStorage()Lcom/konka/avenger/utilities/StorageInfor;
    .locals 8

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v1, "Storage"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "the disk storage path is ==="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "the status is ==="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/os/StatFs;

    invoke-direct {v1, v7}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/konka/avenger/utilities/Storage;->mSDCardFileStats:Landroid/os/StatFs;

    iget-object v1, p0, Lcom/konka/avenger/utilities/Storage;->mSDCardFileStats:Landroid/os/StatFs;

    invoke-virtual {v1, v7}, Landroid/os/StatFs;->restat(Ljava/lang/String;)V

    :try_start_0
    iget-object v1, p0, Lcom/konka/avenger/utilities/Storage;->mSDCardFileStats:Landroid/os/StatFs;

    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockCount()I

    move-result v1

    int-to-long v1, v1

    iget-object v3, p0, Lcom/konka/avenger/utilities/Storage;->mSDCardFileStats:Landroid/os/StatFs;

    invoke-virtual {v3}, Landroid/os/StatFs;->getBlockSize()I

    move-result v3

    int-to-long v3, v3

    mul-long/2addr v1, v3

    iput-wide v1, p0, Lcom/konka/avenger/utilities/Storage;->mTotalStorage:J

    iget-object v1, p0, Lcom/konka/avenger/utilities/Storage;->mSDCardFileStats:Landroid/os/StatFs;

    invoke-virtual {v1}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v1

    int-to-long v1, v1

    iget-object v3, p0, Lcom/konka/avenger/utilities/Storage;->mSDCardFileStats:Landroid/os/StatFs;

    invoke-virtual {v3}, Landroid/os/StatFs;->getBlockSize()I

    move-result v3

    int-to-long v3, v3

    mul-long/2addr v1, v3

    iput-wide v1, p0, Lcom/konka/avenger/utilities/Storage;->mFreeStorage:J

    iget-wide v1, p0, Lcom/konka/avenger/utilities/Storage;->mTotalStorage:J

    iget-wide v3, p0, Lcom/konka/avenger/utilities/Storage;->mFreeStorage:J

    sub-long/2addr v1, v3

    iput-wide v1, p0, Lcom/konka/avenger/utilities/Storage;->mUsedStorage:J
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v0, Lcom/konka/avenger/utilities/StorageInfor;

    iget-wide v1, p0, Lcom/konka/avenger/utilities/Storage;->mTotalStorage:J

    iget-wide v3, p0, Lcom/konka/avenger/utilities/Storage;->mFreeStorage:J

    iget-wide v5, p0, Lcom/konka/avenger/utilities/Storage;->mUsedStorage:J

    invoke-direct/range {v0 .. v6}, Lcom/konka/avenger/utilities/StorageInfor;-><init>(JJJ)V

    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public getInternalStorage()Lcom/konka/avenger/utilities/StorageInfor;
    .locals 7

    new-instance v1, Landroid/os/StatFs;

    const-string v2, "/data"

    invoke-direct {v1, v2}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/konka/avenger/utilities/Storage;->mDataFileStats:Landroid/os/StatFs;

    iget-object v1, p0, Lcom/konka/avenger/utilities/Storage;->mDataFileStats:Landroid/os/StatFs;

    const-string v2, "/data"

    invoke-virtual {v1, v2}, Landroid/os/StatFs;->restat(Ljava/lang/String;)V

    :try_start_0
    iget-object v1, p0, Lcom/konka/avenger/utilities/Storage;->mDataFileStats:Landroid/os/StatFs;

    invoke-virtual {v1}, Landroid/os/StatFs;->getBlockCount()I

    move-result v1

    int-to-long v1, v1

    iget-object v3, p0, Lcom/konka/avenger/utilities/Storage;->mDataFileStats:Landroid/os/StatFs;

    invoke-virtual {v3}, Landroid/os/StatFs;->getBlockSize()I

    move-result v3

    int-to-long v3, v3

    mul-long/2addr v1, v3

    iput-wide v1, p0, Lcom/konka/avenger/utilities/Storage;->mTotalStorage:J

    iget-object v1, p0, Lcom/konka/avenger/utilities/Storage;->mDataFileStats:Landroid/os/StatFs;

    invoke-virtual {v1}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v1

    int-to-long v1, v1

    iget-object v3, p0, Lcom/konka/avenger/utilities/Storage;->mDataFileStats:Landroid/os/StatFs;

    invoke-virtual {v3}, Landroid/os/StatFs;->getBlockSize()I

    move-result v3

    int-to-long v3, v3

    mul-long/2addr v1, v3

    iput-wide v1, p0, Lcom/konka/avenger/utilities/Storage;->mFreeStorage:J

    iget-wide v1, p0, Lcom/konka/avenger/utilities/Storage;->mTotalStorage:J

    iget-wide v3, p0, Lcom/konka/avenger/utilities/Storage;->mFreeStorage:J

    sub-long/2addr v1, v3

    iput-wide v1, p0, Lcom/konka/avenger/utilities/Storage;->mUsedStorage:J
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v0, Lcom/konka/avenger/utilities/StorageInfor;

    iget-wide v1, p0, Lcom/konka/avenger/utilities/Storage;->mTotalStorage:J

    iget-wide v3, p0, Lcom/konka/avenger/utilities/Storage;->mFreeStorage:J

    iget-wide v5, p0, Lcom/konka/avenger/utilities/Storage;->mUsedStorage:J

    invoke-direct/range {v0 .. v6}, Lcom/konka/avenger/utilities/StorageInfor;-><init>(JJJ)V

    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public isSDMounted()Z
    .locals 2

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mounted"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
