.class public final Lcom/konka/avenger/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/avenger/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final accessibility_all_apps_button:I = 0x7f0a0032

.field public static final accessibility_delete_button:I = 0x7f0a0033

.field public static final accessibility_search_button:I = 0x7f0a0030

.field public static final accessibility_voice_search_button:I = 0x7f0a0031

.field public static final activity_not_found:I = 0x7f0a000b

.field public static final all_apps_button_label:I = 0x7f0a0028

.field public static final all_apps_cling_add_item:I = 0x7f0a005b

.field public static final all_apps_cling_title:I = 0x7f0a005a

.field public static final all_apps_home_button_label:I = 0x7f0a0029

.field public static final all_apps_sort_cling_add_item:I = 0x7f0a005d

.field public static final all_apps_sort_cling_title:I = 0x7f0a005c

.field public static final app_can_not_uninstall:I = 0x7f0a00a7

.field public static final application_copyright:I = 0x7f0a0003

.field public static final application_name:I = 0x7f0a0002

.field public static final application_version:I = 0x7f0a0004

.field public static final apps_customize_apps_scroll_format:I = 0x7f0a0055

.field public static final apps_customize_widgets_scroll_format:I = 0x7f0a0056

.field public static final cab_app_selection_text:I = 0x7f0a0043

.field public static final cab_folder_selection_text:I = 0x7f0a0045

.field public static final cab_menu_app_info:I = 0x7f0a0042

.field public static final cab_menu_delete_app:I = 0x7f0a0041

.field public static final cab_shortcut_selection_text:I = 0x7f0a0046

.field public static final cab_widget_selection_text:I = 0x7f0a0044

.field public static final cancel_action:I = 0x7f0a001a

.field public static final cancel_target_label:I = 0x7f0a002d

.field public static final chooser_wallpaper:I = 0x7f0a0008

.field public static final click_to_add:I = 0x7f0a00a9

.field public static final cling_dismiss:I = 0x7f0a0061

.field public static final config_drawerDefaultTransitionEffect:I = 0x7f0a0000

.field public static final config_workspaceDefaultTransitionEffect:I = 0x7f0a0001

.field public static final default_scroll_format:I = 0x7f0a0053

.field public static final delete_target_label:I = 0x7f0a002c

.field public static final delete_target_uninstall_label:I = 0x7f0a002e

.field public static final delete_zone_label_all_apps:I = 0x7f0a002b

.field public static final delete_zone_label_all_apps_system_app:I = 0x7f0a0034

.field public static final delete_zone_label_workspace:I = 0x7f0a002a

.field public static final dialog_dismiss:I = 0x7f0a000e

.field public static final dialog_positive:I = 0x7f0a000d

.field public static final dream_name:I = 0x7f0a0051

.field public static final external_drop_widget_error:I = 0x7f0a0015

.field public static final external_drop_widget_pick_format:I = 0x7f0a0014

.field public static final external_drop_widget_pick_title:I = 0x7f0a0016

.field public static final folder:I = 0x7f0a000f

.field public static final folder_cling_create_folder:I = 0x7f0a0060

.field public static final folder_cling_move_item:I = 0x7f0a005f

.field public static final folder_cling_title:I = 0x7f0a005e

.field public static final folder_closed:I = 0x7f0a0065

.field public static final folder_delete:I = 0x7f0a00bc

.field public static final folder_hint_text:I = 0x7f0a0052

.field public static final folder_name:I = 0x7f0a0007

.field public static final folder_name_format:I = 0x7f0a0067

.field public static final folder_noempty:I = 0x7f0a00bd

.field public static final folder_noroom:I = 0x7f0a00bb

.field public static final folder_opened:I = 0x7f0a0062

.field public static final folder_renamed:I = 0x7f0a0066

.field public static final folder_tap_to_close:I = 0x7f0a0063

.field public static final folder_tap_to_rename:I = 0x7f0a0064

.field public static final gadget_error_text:I = 0x7f0a004f

.field public static final group_applications:I = 0x7f0a001c

.field public static final group_shortcuts:I = 0x7f0a001d

.field public static final group_wallpapers:I = 0x7f0a001f

.field public static final group_widgets:I = 0x7f0a001e

.field public static final help_url:I = 0x7f0a0040

.field public static final hint_all_apps:I = 0x7f0a00ac

.field public static final hint_change_wallpaper:I = 0x7f0a00ad

.field public static final hint_homescreen_add:I = 0x7f0a00ab

.field public static final hint_kk_search:I = 0x7f0a00ae

.field public static final hint_latest_app:I = 0x7f0a00b3

.field public static final hint_market:I = 0x7f0a00b4

.field public static final hint_network_wired:I = 0x7f0a00b1

.field public static final hint_network_wireless:I = 0x7f0a00b2

.field public static final hint_settings:I = 0x7f0a00af

.field public static final hint_usb_indicator:I = 0x7f0a00b0

.field public static final hint_user_center:I = 0x7f0a00aa

.field public static final info_target_label:I = 0x7f0a002f

.field public static final invalid_hotseat_item:I = 0x7f0a0022

.field public static final lang_flag:I = 0x7f0a00b8

.field public static final latest_app:I = 0x7f0a00c1

.field public static final long_press_widget_to_add:I = 0x7f0a0010

.field public static final market:I = 0x7f0a0011

.field public static final menu:I = 0x7f0a0012

.field public static final menu_add:I = 0x7f0a0035

.field public static final menu_apps_sort_install_date:I = 0x7f0a003f

.field public static final menu_apps_sort_title:I = 0x7f0a003e

.field public static final menu_help:I = 0x7f0a003d

.field public static final menu_item_add_item:I = 0x7f0a001b

.field public static final menu_manage_apps:I = 0x7f0a0036

.field public static final menu_market:I = 0x7f0a0037

.field public static final menu_notifications:I = 0x7f0a003a

.field public static final menu_preferences:I = 0x7f0a003c

.field public static final menu_search:I = 0x7f0a0039

.field public static final menu_settings:I = 0x7f0a003b

.field public static final menu_wallpaper:I = 0x7f0a0038

.field public static final more_apps:I = 0x7f0a00a8

.field public static final not_allowed_add:I = 0x7f0a0021

.field public static final out_of_space:I = 0x7f0a0020

.field public static final permdesc_install_shortcut:I = 0x7f0a0048

.field public static final permdesc_read_settings:I = 0x7f0a004c

.field public static final permdesc_uninstall_shortcut:I = 0x7f0a004a

.field public static final permdesc_write_settings:I = 0x7f0a004e

.field public static final permlab_install_shortcut:I = 0x7f0a0047

.field public static final permlab_read_settings:I = 0x7f0a004b

.field public static final permlab_uninstall_shortcut:I = 0x7f0a0049

.field public static final permlab_write_settings:I = 0x7f0a004d

.field public static final pick_wallpaper:I = 0x7f0a000a

.field public static final preferences_application_title:I = 0x7f0a006a

.field public static final preferences_interface_dock_title:I = 0x7f0a009b

.field public static final preferences_interface_drawer_indicator_category:I = 0x7f0a0096

.field public static final preferences_interface_drawer_indicator_enable_summary:I = 0x7f0a0098

.field public static final preferences_interface_drawer_indicator_enable_title:I = 0x7f0a0097

.field public static final preferences_interface_drawer_indicator_fade_summary:I = 0x7f0a009a

.field public static final preferences_interface_drawer_indicator_fade_title:I = 0x7f0a0099

.field public static final preferences_interface_drawer_scrolling_category:I = 0x7f0a0091

.field public static final preferences_interface_drawer_scrolling_fade_adjacent_screens_summary:I = 0x7f0a0095

.field public static final preferences_interface_drawer_scrolling_fade_adjacent_screens_title:I = 0x7f0a0094

.field public static final preferences_interface_drawer_scrolling_transition_effect_summary:I = 0x7f0a0093

.field public static final preferences_interface_drawer_scrolling_transition_effect_title:I = 0x7f0a0092

.field public static final preferences_interface_drawer_title:I = 0x7f0a008d

.field public static final preferences_interface_drawer_widgets_category:I = 0x7f0a008e

.field public static final preferences_interface_drawer_widgets_join_apps_summary:I = 0x7f0a0090

.field public static final preferences_interface_drawer_widgets_join_apps_title:I = 0x7f0a008f

.field public static final preferences_interface_general_orientation_title:I = 0x7f0a009e

.field public static final preferences_interface_general_title:I = 0x7f0a009d

.field public static final preferences_interface_homescreen_general_category:I = 0x7f0a006c

.field public static final preferences_interface_homescreen_general_default_screen_summary:I = 0x7f0a0070

.field public static final preferences_interface_homescreen_general_default_screen_title:I = 0x7f0a006f

.field public static final preferences_interface_homescreen_general_grid_columns_title:I = 0x7f0a0074

.field public static final preferences_interface_homescreen_general_grid_rows_title:I = 0x7f0a0073

.field public static final preferences_interface_homescreen_general_grid_summary:I = 0x7f0a0072

.field public static final preferences_interface_homescreen_general_grid_title:I = 0x7f0a0071

.field public static final preferences_interface_homescreen_general_hide_icon_labels_summary:I = 0x7f0a007e

.field public static final preferences_interface_homescreen_general_hide_icon_labels_title:I = 0x7f0a007d

.field public static final preferences_interface_homescreen_general_resize_any_widget_summary:I = 0x7f0a007c

.field public static final preferences_interface_homescreen_general_resize_any_widget_title:I = 0x7f0a007b

.field public static final preferences_interface_homescreen_general_screen_padding_horizontal_summary:I = 0x7f0a0078

.field public static final preferences_interface_homescreen_general_screen_padding_horizontal_title:I = 0x7f0a0077

.field public static final preferences_interface_homescreen_general_screen_padding_vertical_summary:I = 0x7f0a0076

.field public static final preferences_interface_homescreen_general_screen_padding_vertical_title:I = 0x7f0a0075

.field public static final preferences_interface_homescreen_general_screens_summary:I = 0x7f0a006e

.field public static final preferences_interface_homescreen_general_screens_title:I = 0x7f0a006d

.field public static final preferences_interface_homescreen_general_search_summary:I = 0x7f0a007a

.field public static final preferences_interface_homescreen_general_search_title:I = 0x7f0a0079

.field public static final preferences_interface_homescreen_indicator_background_summary:I = 0x7f0a008c

.field public static final preferences_interface_homescreen_indicator_background_title:I = 0x7f0a008b

.field public static final preferences_interface_homescreen_indicator_category:I = 0x7f0a0086

.field public static final preferences_interface_homescreen_indicator_enable_summary:I = 0x7f0a0088

.field public static final preferences_interface_homescreen_indicator_enable_title:I = 0x7f0a0087

.field public static final preferences_interface_homescreen_indicator_fade_summary:I = 0x7f0a008a

.field public static final preferences_interface_homescreen_indicator_fade_title:I = 0x7f0a0089

.field public static final preferences_interface_homescreen_scrolling_category:I = 0x7f0a007f

.field public static final preferences_interface_homescreen_scrolling_fade_adjacent_screens_summary:I = 0x7f0a0083

.field public static final preferences_interface_homescreen_scrolling_fade_adjacent_screens_title:I = 0x7f0a0082

.field public static final preferences_interface_homescreen_scrolling_scroll_wallpaper_summary:I = 0x7f0a0085

.field public static final preferences_interface_homescreen_scrolling_scroll_wallpaper_title:I = 0x7f0a0084

.field public static final preferences_interface_homescreen_scrolling_transition_effect_summary:I = 0x7f0a0081

.field public static final preferences_interface_homescreen_scrolling_transition_effect_title:I = 0x7f0a0080

.field public static final preferences_interface_homescreen_title:I = 0x7f0a006b

.field public static final preferences_interface_icons_title:I = 0x7f0a009c

.field public static final preferences_interface_title:I = 0x7f0a0069

.field public static final preferences_title:I = 0x7f0a0068

.field public static final quick_action_add_to_homepage:I = 0x7f0a00a0

.field public static final quick_action_attribute:I = 0x7f0a00a1

.field public static final quick_action_delete:I = 0x7f0a00a4

.field public static final quick_action_move:I = 0x7f0a00a3

.field public static final quick_action_open:I = 0x7f0a00a5

.field public static final quick_action_rename:I = 0x7f0a00a6

.field public static final quick_action_run:I = 0x7f0a009f

.field public static final quick_action_uninstall:I = 0x7f0a00a2

.field public static final rename_action:I = 0x7f0a0019

.field public static final rename_folder_label:I = 0x7f0a0017

.field public static final rename_folder_title:I = 0x7f0a0018

.field public static final shortcut_duplicate:I = 0x7f0a0025

.field public static final shortcut_installed:I = 0x7f0a0023

.field public static final shortcut_uninstalled:I = 0x7f0a0024

.field public static final str_dialog_mes:I = 0x7f0a00b7

.field public static final str_dialog_title:I = 0x7f0a00b6

.field public static final supersky_canused:I = 0x7f0a00ba

.field public static final supersky_used:I = 0x7f0a00b9

.field public static final target_is_locked:I = 0x7f0a00b5

.field public static final title_select_application:I = 0x7f0a0027

.field public static final title_select_shortcut:I = 0x7f0a0026

.field public static final uid_name:I = 0x7f0a0006

.field public static final uninstall_system_app_text:I = 0x7f0a0050

.field public static final versionFormatter:I = 0x7f0a0005

.field public static final wallpaper_cancel:I = 0x7f0a00c0

.field public static final wallpaper_chooser_empty:I = 0x7f0a00bf

.field public static final wallpaper_dialog_title:I = 0x7f0a00be

.field public static final wallpaper_instructions:I = 0x7f0a0009

.field public static final widget_dims_format:I = 0x7f0a0013

.field public static final widgets_tab_label:I = 0x7f0a000c

.field public static final workspace_cling_move_item:I = 0x7f0a0058

.field public static final workspace_cling_open_all_apps:I = 0x7f0a0059

.field public static final workspace_cling_title:I = 0x7f0a0057

.field public static final workspace_scroll_format:I = 0x7f0a0054


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
