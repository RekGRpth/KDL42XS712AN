.class public final Lcom/konka/avenger/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/avenger/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final add_guide:I = 0x7f0d000a

.field public static final add_guide_btn:I = 0x7f0d000c

.field public static final add_guide_img:I = 0x7f0d000b

.field public static final all_apps_button:I = 0x7f0d0055

.field public static final all_apps_cling:I = 0x7f0d001c

.field public static final all_apps_cling_add_item:I = 0x7f0d000e

.field public static final all_apps_cling_title:I = 0x7f0d000d

.field public static final all_apps_sort_cling:I = 0x7f0d001d

.field public static final all_apps_sort_cling_add_item:I = 0x7f0d0011

.field public static final all_apps_sort_cling_title:I = 0x7f0d0010

.field public static final animation_buffer:I = 0x7f0d001a

.field public static final application_icon:I = 0x7f0d0012

.field public static final applist_latest:I = 0x7f0d0018

.field public static final apps_customize_content:I = 0x7f0d0013

.field public static final apps_customize_pane:I = 0x7f0d0038

.field public static final apps_customize_pane_content:I = 0x7f0d0019

.field public static final apps_customize_progress_bar:I = 0x7f0d001f

.field public static final apps_sort_install_date:I = 0x7f0d0070

.field public static final apps_sort_title:I = 0x7f0d006f

.field public static final arrow_down:I = 0x7f0d0025

.field public static final arrow_up:I = 0x7f0d0024

.field public static final bottom:I = 0x7f0d0001

.field public static final button1:I = 0x7f0d004b

.field public static final button2:I = 0x7f0d004a

.field public static final buttonPanel:I = 0x7f0d0049

.field public static final center:I = 0x7f0d0006

.field public static final center_horizontal:I = 0x7f0d0005

.field public static final center_vertical:I = 0x7f0d0004

.field public static final cling_dismiss:I = 0x7f0d000f

.field public static final container:I = 0x7f0d0023

.field public static final contentPanel:I = 0x7f0d0045

.field public static final delete_target_text:I = 0x7f0d0050

.field public static final dock_divider:I = 0x7f0d0035

.field public static final drag_layer:I = 0x7f0d0032

.field public static final drag_target_bar:I = 0x7f0d004f

.field public static final folder_cling:I = 0x7f0d003a

.field public static final folder_cling_create_folder:I = 0x7f0d002d

.field public static final folder_cling_title:I = 0x7f0d002c

.field public static final folder_content:I = 0x7f0d0065

.field public static final folder_icon_name:I = 0x7f0d002f

.field public static final folder_name:I = 0x7f0d005d

.field public static final folder_surface:I = 0x7f0d0064

.field public static final gallery:I = 0x7f0d0066

.field public static final hint_text:I = 0x7f0d0009

.field public static final homescreen_add_button:I = 0x7f0d0054

.field public static final hotseat:I = 0x7f0d0036

.field public static final icon:I = 0x7f0d0042

.field public static final info_target_text:I = 0x7f0d0051

.field public static final kk_search_button:I = 0x7f0d0057

.field public static final label:I = 0x7f0d005c

.field public static final layout:I = 0x7f0d0031

.field public static final left:I = 0x7f0d0002

.field public static final memsize_info:I = 0x7f0d0017

.field public static final multiple:I = 0x7f0d0008

.field public static final network_status_indicator:I = 0x7f0d005a

.field public static final number_picker:I = 0x7f0d003c

.field public static final number_picker_1:I = 0x7f0d0027

.field public static final number_picker_2:I = 0x7f0d0029

.field public static final page_indicator:I = 0x7f0d0047

.field public static final pagedPanel:I = 0x7f0d0046

.field public static final pagedTitle:I = 0x7f0d0043

.field public static final paged_view:I = 0x7f0d0048

.field public static final paged_view_checkbox:I = 0x7f0d004d

.field public static final paged_view_icon:I = 0x7f0d004c

.field public static final paged_view_indicator:I = 0x7f0d001b

.field public static final parentPanel:I = 0x7f0d003e

.field public static final picker_title_1:I = 0x7f0d0026

.field public static final picker_title_2:I = 0x7f0d0028

.field public static final preview_background:I = 0x7f0d002e

.field public static final provider:I = 0x7f0d002b

.field public static final provider_icon:I = 0x7f0d002a

.field public static final qsb_bar:I = 0x7f0d0037

.field public static final qsb_divider:I = 0x7f0d0034

.field public static final qsb_search_bar:I = 0x7f0d004e

.field public static final qsb_static_bar:I = 0x7f0d0052

.field public static final quick_entry_bar:I = 0x7f0d0016

.field public static final release_version:I = 0x7f0d001e

.field public static final right:I = 0x7f0d0003

.field public static final scroller:I = 0x7f0d003d

.field public static final search_button:I = 0x7f0d005e

.field public static final search_button_container:I = 0x7f0d0061

.field public static final search_divider:I = 0x7f0d005f

.field public static final set:I = 0x7f0d0067

.field public static final settings_button:I = 0x7f0d0058

.field public static final single:I = 0x7f0d0007

.field public static final tab_title_all_app:I = 0x7f0d0015

.field public static final tabs_container:I = 0x7f0d0014

.field public static final titleDivider:I = 0x7f0d0044

.field public static final titleDividerTop:I = 0x7f0d0040

.field public static final title_template:I = 0x7f0d0041

.field public static final top:I = 0x7f0d0000

.field public static final topPanel:I = 0x7f0d003f

.field public static final tracks:I = 0x7f0d0030

.field public static final tv_title:I = 0x7f0d005b

.field public static final tv_window:I = 0x7f0d003b

.field public static final tvwindow_widget:I = 0x7f0d0063

.field public static final usb_status_indicator:I = 0x7f0d0059

.field public static final user_center_button:I = 0x7f0d0053

.field public static final voice_button:I = 0x7f0d0060

.field public static final voice_button_container:I = 0x7f0d0062

.field public static final wallpaper_change_button:I = 0x7f0d0056

.field public static final wallpaper_chooser_fragment:I = 0x7f0d0068

.field public static final wallpaper_image:I = 0x7f0d0069

.field public static final widget_dims:I = 0x7f0d0021

.field public static final widget_name:I = 0x7f0d0020

.field public static final widget_preview:I = 0x7f0d0022

.field public static final workspace:I = 0x7f0d0033

.field public static final workspace_cling:I = 0x7f0d0039

.field public static final workspace_cling_move_item:I = 0x7f0d006b

.field public static final workspace_cling_open_all_apps:I = 0x7f0d006c

.field public static final workspace_cling_title:I = 0x7f0d006a

.field public static final workspace_next_page_button:I = 0x7f0d006e

.field public static final workspace_prev_page_button:I = 0x7f0d006d


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
