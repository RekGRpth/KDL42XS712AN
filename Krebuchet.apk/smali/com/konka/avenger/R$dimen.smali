.class public final Lcom/konka/avenger/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/avenger/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final TVWindow_Height:I = 0x7f0c0044

.field public static final TVWindow_Width:I = 0x7f0c0043

.field public static final TVWindow_x:I = 0x7f0c0045

.field public static final TVWindow_y:I = 0x7f0c0046

.field public static final alert_dialog_content_inset:I = 0x7f0c0060

.field public static final allAppsSmallScreenVerticalMarginLandscape:I = 0x7f0c0030

.field public static final allAppsSmallScreenVerticalMarginPortrait:I = 0x7f0c0031

.field public static final all_apps_button_vertical_padding:I = 0x7f0c005c

.field public static final app_icon_padding_top:I = 0x7f0c001a

.field public static final app_icon_size:I = 0x7f0c0022

.field public static final app_widget_padding_bottom:I = 0x7f0c003b

.field public static final app_widget_padding_left:I = 0x7f0c0038

.field public static final app_widget_padding_right:I = 0x7f0c0039

.field public static final app_widget_padding_top:I = 0x7f0c003a

.field public static final app_widget_preview_padding_left:I = 0x7f0c003c

.field public static final app_widget_preview_padding_top:I = 0x7f0c003d

.field public static final apps_customize_cell_height:I = 0x7f0c0024

.field public static final apps_customize_cell_width:I = 0x7f0c0023

.field public static final apps_customize_indicator_margin_bottom:I = 0x7f0c0021

.field public static final apps_customize_indicator_padding:I = 0x7f0c005a

.field public static final apps_customize_max_gap:I = 0x7f0c0025

.field public static final apps_customize_pageLayoutHeightGap:I = 0x7f0c0050

.field public static final apps_customize_pageLayoutPaddingBottom:I = 0x7f0c0052

.field public static final apps_customize_pageLayoutPaddingLeft:I = 0x7f0c0053

.field public static final apps_customize_pageLayoutPaddingRight:I = 0x7f0c0054

.field public static final apps_customize_pageLayoutPaddingTop:I = 0x7f0c0051

.field public static final apps_customize_pageLayoutWidthGap:I = 0x7f0c004f

.field public static final apps_customize_tab_bar_height:I = 0x7f0c001f

.field public static final apps_customize_tab_bar_margin_top:I = 0x7f0c0020

.field public static final apps_customize_widget_cell_height_gap:I = 0x7f0c0027

.field public static final apps_customize_widget_cell_width_gap:I = 0x7f0c0026

.field public static final button_bar_height:I = 0x7f0c0029

.field public static final button_bar_height_bottom_padding:I = 0x7f0c002b

.field public static final button_bar_height_plus_padding:I = 0x7f0c002e

.field public static final button_bar_height_top_padding:I = 0x7f0c002a

.field public static final button_bar_width_left_padding:I = 0x7f0c002c

.field public static final button_bar_width_right_padding:I = 0x7f0c002d

.field public static final clingPunchThroughGraphicCenterRadius:I = 0x7f0c0000

.field public static final cling_text_block_offset_x:I = 0x7f0c0002

.field public static final cling_text_block_offset_y:I = 0x7f0c0003

.field public static final delete_zone_drawable_padding:I = 0x7f0c0033

.field public static final dragViewOffsetX:I = 0x7f0c0036

.field public static final dragViewOffsetY:I = 0x7f0c0037

.field public static final drop_target_drag_padding:I = 0x7f0c0032

.field public static final external_drop_icon_rect_radius:I = 0x7f0c005b

.field public static final folderClingMarginTop:I = 0x7f0c0001

.field public static final folder_cell_height:I = 0x7f0c0010

.field public static final folder_cell_width:I = 0x7f0c000f

.field public static final folder_height_gap:I = 0x7f0c0042

.field public static final folder_name_padding:I = 0x7f0c0040

.field public static final folder_preview_padding:I = 0x7f0c003f

.field public static final folder_preview_size:I = 0x7f0c003e

.field public static final folder_width_gap:I = 0x7f0c0041

.field public static final hotseat_bottom_padding:I = 0x7f0c0056

.field public static final hotseat_cell_height:I = 0x7f0c0016

.field public static final hotseat_cell_width:I = 0x7f0c0015

.field public static final hotseat_height_gap:I = 0x7f0c0018

.field public static final hotseat_left_padding:I = 0x7f0c0057

.field public static final hotseat_right_padding:I = 0x7f0c0058

.field public static final hotseat_top_padding:I = 0x7f0c0055

.field public static final hotseat_width_gap:I = 0x7f0c0017

.field public static final live_wallpaper_grid_item_padding:I = 0x7f0c005f

.field public static final paged_dialog_button_bar_height:I = 0x7f0c0048

.field public static final paged_dialog_title_height:I = 0x7f0c0047

.field public static final qsb_bar_height:I = 0x7f0c0006

.field public static final qsb_bar_height_inset:I = 0x7f0c0004

.field public static final qsb_bar_hidden_inset:I = 0x7f0c0005

.field public static final qsb_padding_left:I = 0x7f0c0007

.field public static final qsb_padding_right:I = 0x7f0c0008

.field public static final quickaction_text_size:I = 0x7f0c0049

.field public static final scroll_zone:I = 0x7f0c0035

.field public static final search_bar_height:I = 0x7f0c0009

.field public static final smallScreenExtraSpacing:I = 0x7f0c002f

.field public static final status_bar_height:I = 0x7f0c0034

.field public static final title_texture_width:I = 0x7f0c0028

.field public static final toolbar_button_horizontal_padding:I = 0x7f0c001c

.field public static final toolbar_button_vertical_padding:I = 0x7f0c001b

.field public static final toolbar_external_icon_height:I = 0x7f0c001e

.field public static final toolbar_external_icon_width:I = 0x7f0c001d

.field public static final wallpaper_chooser_grid_height:I = 0x7f0c005e

.field public static final wallpaper_chooser_grid_width:I = 0x7f0c005d

.field public static final workspace_bottom_padding:I = 0x7f0c004d

.field public static final workspace_cell_height:I = 0x7f0c000b

.field public static final workspace_cell_width:I = 0x7f0c000a

.field public static final workspace_content_large_only_top_margin:I = 0x7f0c0059

.field public static final workspace_divider_padding_bottom:I = 0x7f0c0014

.field public static final workspace_divider_padding_left:I = 0x7f0c0011

.field public static final workspace_divider_padding_right:I = 0x7f0c0012

.field public static final workspace_divider_padding_top:I = 0x7f0c0013

.field public static final workspace_height_gap:I = 0x7f0c000d

.field public static final workspace_left_padding:I = 0x7f0c004a

.field public static final workspace_max_gap:I = 0x7f0c000e

.field public static final workspace_overscroll_drawable_padding:I = 0x7f0c0019

.field public static final workspace_page_spacing:I = 0x7f0c004e

.field public static final workspace_right_padding:I = 0x7f0c004b

.field public static final workspace_top_padding:I = 0x7f0c004c

.field public static final workspace_width_gap:I = 0x7f0c000c


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
