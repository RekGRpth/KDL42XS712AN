.class public final Lcom/konka/avenger/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/avenger/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final add_guide:I = 0x7f020000

.field public static final add_guide_btn:I = 0x7f020001

.field public static final add_guide_btn_sel:I = 0x7f020002

.field public static final add_guide_btn_unsel:I = 0x7f020003

.field public static final all_app_bg:I = 0x7f020004

.field public static final all_apps_button_icon:I = 0x7f020005

.field public static final app_hover:I = 0x7f020006

.field public static final app_icon_more:I = 0x7f020007

.field public static final apps_customize_bg:I = 0x7f020008

.field public static final apps_icon_color_selector:I = 0x7f020009

.field public static final bg_appwidget_error:I = 0x7f02000a

.field public static final bg_cling1:I = 0x7f02000b

.field public static final bg_cling2:I = 0x7f02000c

.field public static final bg_cling3:I = 0x7f02000d

.field public static final bg_cling4:I = 0x7f02000e

.field public static final btn_appshop_indicator_selector:I = 0x7f02000f

.field public static final btn_cling_normal:I = 0x7f020010

.field public static final btn_cling_pressed:I = 0x7f020011

.field public static final btn_recentapp_indicator_selector:I = 0x7f020012

.field public static final button_bg:I = 0x7f020013

.field public static final button_hover:I = 0x7f020014

.field public static final cling:I = 0x7f020015

.field public static final cling_button_bg:I = 0x7f020016

.field public static final com_icon_file_s:I = 0x7f020017

.field public static final com_icon_file_uns:I = 0x7f020018

.field public static final com_scrollbar:I = 0x7f020019

.field public static final conn_stat_eth_connected:I = 0x7f02001a

.field public static final conn_stat_eth_connected_selected:I = 0x7f02001b

.field public static final conn_stat_eth_disconnected:I = 0x7f02001c

.field public static final conn_stat_eth_disconnected_selected:I = 0x7f02001d

.field public static final conn_stat_eth_unreachable:I = 0x7f02001e

.field public static final conn_stat_eth_unreachable_selected:I = 0x7f02001f

.field public static final conn_stat_usb_connected:I = 0x7f020020

.field public static final conn_stat_usb_connected_selected:I = 0x7f020021

.field public static final conn_stat_usb_disconnected:I = 0x7f020022

.field public static final conn_stat_usb_disconnected_selected:I = 0x7f020023

.field public static final conn_stat_wifi_signal_0:I = 0x7f020024

.field public static final conn_stat_wifi_signal_0_fully:I = 0x7f020025

.field public static final conn_stat_wifi_signal_1:I = 0x7f020026

.field public static final conn_stat_wifi_signal_1_fully:I = 0x7f020027

.field public static final conn_stat_wifi_signal_2:I = 0x7f020028

.field public static final conn_stat_wifi_signal_2_fully:I = 0x7f020029

.field public static final conn_stat_wifi_signal_3:I = 0x7f02002a

.field public static final conn_stat_wifi_signal_3_fully:I = 0x7f02002b

.field public static final conn_stat_wifi_signal_4:I = 0x7f02002c

.field public static final conn_stat_wifi_signal_4_fully:I = 0x7f02002d

.field public static final default_widget_preview_holo:I = 0x7f02002e

.field public static final divider_launcher_holo:I = 0x7f02002f

.field public static final dotted_indicator:I = 0x7f020030

.field public static final file_popmenu1_bj:I = 0x7f020031

.field public static final file_popmenu2_bj:I = 0x7f020032

.field public static final file_popmenu3_bj:I = 0x7f020033

.field public static final file_popmenu_bj:I = 0x7f020034

.field public static final flying_icon_bg:I = 0x7f020035

.field public static final flying_icon_bg_pressed:I = 0x7f020036

.field public static final focusable_view_bg:I = 0x7f020037

.field public static final focused_bg:I = 0x7f020038

.field public static final gardening_crosshairs:I = 0x7f020039

.field public static final gd_page_indicator_dot:I = 0x7f02003a

.field public static final gd_page_indicator_dot_normal_focused:I = 0x7f02003b

.field public static final gd_page_indicator_dot_normal_normal:I = 0x7f02003c

.field public static final gd_page_indicator_dot_normal_pressed:I = 0x7f02003d

.field public static final gd_page_indicator_dot_selected_focused:I = 0x7f02003e

.field public static final gd_page_indicator_dot_selected_normal:I = 0x7f02003f

.field public static final gd_page_indicator_dot_selected_pressed:I = 0x7f020040

.field public static final grid_focused:I = 0x7f020041

.field public static final grid_pressed:I = 0x7f020042

.field public static final grid_selected:I = 0x7f020043

.field public static final hand:I = 0x7f020044

.field public static final hint_popup:I = 0x7f020045

.field public static final home_default_tv:I = 0x7f020046

.field public static final home_hint_list1_uns:I = 0x7f020047

.field public static final home_indicator:I = 0x7f020048

.field public static final home_press:I = 0x7f020049

.field public static final home_selectbox1_s:I = 0x7f02004a

.field public static final home_selectbox_s:I = 0x7f02004b

.field public static final home_widget_hover_box_s:I = 0x7f02004c

.field public static final homescreen_blue_normal_holo:I = 0x7f02004d

.field public static final homescreen_blue_strong_holo:I = 0x7f02004e

.field public static final hotseat_bg_panel:I = 0x7f02004f

.field public static final hotseat_scrubber_holo:I = 0x7f020050

.field public static final hotseat_track_holo:I = 0x7f020051

.field public static final ic_add_apps:I = 0x7f020052

.field public static final ic_add_folder:I = 0x7f020053

.field public static final ic_add_shortcut:I = 0x7f020054

.field public static final ic_add_widget:I = 0x7f020055

.field public static final ic_allapps:I = 0x7f020056

.field public static final ic_allapps_pressed:I = 0x7f020057

.field public static final ic_dotted_scroller_normal:I = 0x7f020058

.field public static final ic_dotted_scroller_pressed:I = 0x7f020059

.field public static final ic_dotted_scroller_selected:I = 0x7f02005a

.field public static final ic_home_add:I = 0x7f02005b

.field public static final ic_home_add_holo_dark:I = 0x7f02005c

.field public static final ic_home_add_selected:I = 0x7f02005d

.field public static final ic_home_all_apps:I = 0x7f02005e

.field public static final ic_home_all_apps_holo_dark:I = 0x7f02005f

.field public static final ic_home_all_apps_selected:I = 0x7f020060

.field public static final ic_home_change_wallpaper:I = 0x7f020061

.field public static final ic_home_change_wallpaper_selected:I = 0x7f020062

.field public static final ic_home_scroller_normal:I = 0x7f020063

.field public static final ic_home_scroller_pressed:I = 0x7f020064

.field public static final ic_home_scroller_selected:I = 0x7f020065

.field public static final ic_home_search_normal:I = 0x7f020066

.field public static final ic_home_search_normal_holo:I = 0x7f020067

.field public static final ic_home_search_normal_selected:I = 0x7f020068

.field public static final ic_home_settings:I = 0x7f020069

.field public static final ic_home_settings_selected:I = 0x7f02006a

.field public static final ic_home_user_center:I = 0x7f02006b

.field public static final ic_home_user_center_loggedin:I = 0x7f02006c

.field public static final ic_home_user_center_selected:I = 0x7f02006d

.field public static final ic_home_voice_search_holo:I = 0x7f02006e

.field public static final ic_launcher_application:I = 0x7f02006f

.field public static final ic_launcher_clear_active_holo:I = 0x7f020070

.field public static final ic_launcher_clear_normal_holo:I = 0x7f020071

.field public static final ic_launcher_home:I = 0x7f020072

.field public static final ic_launcher_info_active_holo:I = 0x7f020073

.field public static final ic_launcher_info_normal_holo:I = 0x7f020074

.field public static final ic_launcher_market_holo:I = 0x7f020075

.field public static final ic_launcher_trashcan_active_holo:I = 0x7f020076

.field public static final ic_launcher_trashcan_normal_holo:I = 0x7f020077

.field public static final ic_launcher_wallpaper:I = 0x7f020078

.field public static final ic_menu_overflow:I = 0x7f020079

.field public static final ic_no_applications:I = 0x7f02007a

.field public static final ic_search_scroller_normal:I = 0x7f02007b

.field public static final ic_search_scroller_pressed:I = 0x7f02007c

.field public static final ic_search_scroller_selected:I = 0x7f02007d

.field public static final info_target_selector:I = 0x7f02007e

.field public static final kk_button_normal:I = 0x7f02007f

.field public static final kk_button_selected:I = 0x7f020080

.field public static final kk_button_selector:I = 0x7f020081

.field public static final kk_button_text_color:I = 0x7f020082

.field public static final kk_hover_bg:I = 0x7f020083

.field public static final list_button_recentlyused_s:I = 0x7f020084

.field public static final list_button_recentlyused_uns:I = 0x7f020085

.field public static final list_button_shop_s:I = 0x7f020086

.field public static final list_button_shop_uns:I = 0x7f020087

.field public static final next_page_button:I = 0x7f020088

.field public static final next_page_button_hover:I = 0x7f020089

.field public static final next_page_button_press:I = 0x7f02008a

.field public static final overscroll_glow_left:I = 0x7f02008b

.field public static final overscroll_glow_right:I = 0x7f02008c

.field public static final page_hover_left_holo:I = 0x7f02008d

.field public static final page_hover_right_holo:I = 0x7f02008e

.field public static final paged_dialog_bg:I = 0x7f02008f

.field public static final paged_dialog_checkbox:I = 0x7f020090

.field public static final paged_dialog_checkbox_on:I = 0x7f020091

.field public static final paged_view_indicator:I = 0x7f020092

.field public static final portal_container_holo:I = 0x7f020093

.field public static final portal_ring_inner_holo:I = 0x7f020094

.field public static final portal_ring_inner_nolip_holo:I = 0x7f020095

.field public static final portal_ring_outer_holo:I = 0x7f020096

.field public static final portal_ring_rest:I = 0x7f020097

.field public static final prev_page_button:I = 0x7f020098

.field public static final prev_page_button_hover:I = 0x7f020099

.field public static final prev_page_button_press:I = 0x7f02009a

.field public static final qsb_add_quickaction_selector:I = 0x7f02009b

.field public static final qsb_all_apps:I = 0x7f02009c

.field public static final qsb_btn_bg:I = 0x7f02009d

.field public static final qsb_btn_text_color:I = 0x7f02009e

.field public static final qsb_change_wallpaper:I = 0x7f02009f

.field public static final qsb_home_add:I = 0x7f0200a0

.field public static final qsb_network_icon_eth_connected:I = 0x7f0200a1

.field public static final qsb_network_icon_eth_disconnected:I = 0x7f0200a2

.field public static final qsb_network_icon_eth_unreachable:I = 0x7f0200a3

.field public static final qsb_network_icon_wifi_signal_0:I = 0x7f0200a4

.field public static final qsb_network_icon_wifi_signal_1:I = 0x7f0200a5

.field public static final qsb_network_icon_wifi_signal_2:I = 0x7f0200a6

.field public static final qsb_network_icon_wifi_signal_3:I = 0x7f0200a7

.field public static final qsb_network_icon_wifi_signal_4:I = 0x7f0200a8

.field public static final qsb_search_icon:I = 0x7f0200a9

.field public static final qsb_seperator:I = 0x7f0200aa

.field public static final qsb_settings_icon:I = 0x7f0200ab

.field public static final qsb_usb_icon_connected:I = 0x7f0200ac

.field public static final qsb_usb_icon_disconnected:I = 0x7f0200ad

.field public static final qsb_user_center_loggedin:I = 0x7f0200ae

.field public static final qsb_user_center_normal:I = 0x7f0200af

.field public static final quick_entry_indicator_selector:I = 0x7f0200b0

.field public static final quickaction_arrow_down:I = 0x7f0200b1

.field public static final quickaction_arrow_up:I = 0x7f0200b2

.field public static final quickaction_item_btn:I = 0x7f0200b3

.field public static final quickaction_item_selected:I = 0x7f0200b4

.field public static final quickaction_popup:I = 0x7f0200b5

.field public static final quickaction_qsb_selected:I = 0x7f0200b6

.field public static final quickaction_text_color:I = 0x7f0200b7

.field public static final remove_target_selector:I = 0x7f0200b8

.field public static final search_bg_panel:I = 0x7f0200b9

.field public static final search_frame:I = 0x7f0200ba

.field public static final search_indicator:I = 0x7f0200bb

.field public static final tab_selected_focused_holo:I = 0x7f0200bc

.field public static final tab_selected_holo:I = 0x7f0200bd

.field public static final tab_selected_pressed_focused_holo:I = 0x7f0200be

.field public static final tab_selected_pressed_holo:I = 0x7f0200bf

.field public static final tab_unselected_focused_holo:I = 0x7f0200c0

.field public static final tab_unselected_holo:I = 0x7f0200c1

.field public static final tab_unselected_pressed_focused_holo:I = 0x7f0200c2

.field public static final tab_unselected_pressed_holo:I = 0x7f0200c3

.field public static final tab_widget_indicator_selector:I = 0x7f0200c4

.field public static final tv_widget_bg:I = 0x7f0200c5

.field public static final tv_widget_hover:I = 0x7f0200c6

.field public static final uninstall_target_selector:I = 0x7f0200c7

.field public static final wallpaper_0:I = 0x7f0200c8

.field public static final wallpaper_0_small:I = 0x7f0200c9

.field public static final wallpaper_1:I = 0x7f0200ca

.field public static final wallpaper_1_small:I = 0x7f0200cb

.field public static final wallpaper_2:I = 0x7f0200cc

.field public static final wallpaper_2_small:I = 0x7f0200cd

.field public static final wallpaper_3:I = 0x7f0200ce

.field public static final wallpaper_3_small:I = 0x7f0200cf

.field public static final wallpaper_4:I = 0x7f0200d0

.field public static final wallpaper_4_small:I = 0x7f0200d1

.field public static final wallpaper_5:I = 0x7f0200d2

.field public static final wallpaper_5_small:I = 0x7f0200d3

.field public static final wallpaper_gallery_background:I = 0x7f0200d4

.field public static final wallpaper_gallery_item:I = 0x7f0200d5

.field public static final wallpaper_picker_preview:I = 0x7f0200d6

.field public static final widget_resize_frame_holo:I = 0x7f0200d7

.field public static final widget_resize_handle_bottom:I = 0x7f0200d8

.field public static final widget_resize_handle_left:I = 0x7f0200d9

.field public static final widget_resize_handle_right:I = 0x7f0200da

.field public static final widget_resize_handle_top:I = 0x7f0200db


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
