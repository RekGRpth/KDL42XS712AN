.class public Lcom/konka/avenger/tv/TVView;
.super Landroid/view/SurfaceView;
.source "TVView.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/avenger/tv/TVView$InputSourceThread;
    }
.end annotation


# instance fields
.field private final CLOSE_TV:I

.field private final SHOW_TV:I

.field private TAG:Ljava/lang/String;

.field private bSync:Ljava/lang/Boolean;

.field inputSourceThread:Lcom/konka/avenger/tv/TVView$InputSourceThread;

.field private isSetStorageInput:Ljava/lang/Boolean;

.field private isSetTVInput:Ljava/lang/Boolean;

.field private mConditionChanged:Z

.field private mContext:Landroid/content/Context;

.field private mCreateFromLayout:Z

.field private mExpectState:Z

.field private final mHandler:Landroid/os/Handler;

.field private mRealState:Z

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;

.field private mWM:Landroid/view/WindowManager;

.field private mWindowHeight:I

.field private mWindowLeft:I

.field private mWindowTop:I

.field private mWindowWidth:I

.field private surfaceParams:Landroid/view/WindowManager$LayoutParams;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    const-string v0, "TVWindow"

    iput-object v0, p0, Lcom/konka/avenger/tv/TVView;->TAG:Ljava/lang/String;

    const/16 v0, 0x2711

    iput v0, p0, Lcom/konka/avenger/tv/TVView;->SHOW_TV:I

    const/16 v0, 0x2712

    iput v0, p0, Lcom/konka/avenger/tv/TVView;->CLOSE_TV:I

    iput-boolean v1, p0, Lcom/konka/avenger/tv/TVView;->mRealState:Z

    iput-boolean v1, p0, Lcom/konka/avenger/tv/TVView;->mExpectState:Z

    iput-boolean v1, p0, Lcom/konka/avenger/tv/TVView;->mConditionChanged:Z

    iput-object v2, p0, Lcom/konka/avenger/tv/TVView;->mContext:Landroid/content/Context;

    iput v1, p0, Lcom/konka/avenger/tv/TVView;->mWindowTop:I

    iput v1, p0, Lcom/konka/avenger/tv/TVView;->mWindowLeft:I

    iput v1, p0, Lcom/konka/avenger/tv/TVView;->mWindowWidth:I

    iput v1, p0, Lcom/konka/avenger/tv/TVView;->mWindowHeight:I

    iput-object v2, p0, Lcom/konka/avenger/tv/TVView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    iput-object v2, p0, Lcom/konka/avenger/tv/TVView;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iput-boolean v1, p0, Lcom/konka/avenger/tv/TVView;->mCreateFromLayout:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/avenger/tv/TVView;->isSetTVInput:Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/avenger/tv/TVView;->isSetStorageInput:Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/avenger/tv/TVView;->bSync:Ljava/lang/Boolean;

    new-instance v0, Lcom/konka/avenger/tv/TVView$1;

    invoke-direct {v0, p0}, Lcom/konka/avenger/tv/TVView$1;-><init>(Lcom/konka/avenger/tv/TVView;)V

    iput-object v0, p0, Lcom/konka/avenger/tv/TVView;->mHandler:Landroid/os/Handler;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/avenger/tv/TVView;->mCreateFromLayout:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IIII)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    const-string v0, "TVWindow"

    iput-object v0, p0, Lcom/konka/avenger/tv/TVView;->TAG:Ljava/lang/String;

    const/16 v0, 0x2711

    iput v0, p0, Lcom/konka/avenger/tv/TVView;->SHOW_TV:I

    const/16 v0, 0x2712

    iput v0, p0, Lcom/konka/avenger/tv/TVView;->CLOSE_TV:I

    iput-boolean v1, p0, Lcom/konka/avenger/tv/TVView;->mRealState:Z

    iput-boolean v1, p0, Lcom/konka/avenger/tv/TVView;->mExpectState:Z

    iput-boolean v1, p0, Lcom/konka/avenger/tv/TVView;->mConditionChanged:Z

    iput-object v2, p0, Lcom/konka/avenger/tv/TVView;->mContext:Landroid/content/Context;

    iput v1, p0, Lcom/konka/avenger/tv/TVView;->mWindowTop:I

    iput v1, p0, Lcom/konka/avenger/tv/TVView;->mWindowLeft:I

    iput v1, p0, Lcom/konka/avenger/tv/TVView;->mWindowWidth:I

    iput v1, p0, Lcom/konka/avenger/tv/TVView;->mWindowHeight:I

    iput-object v2, p0, Lcom/konka/avenger/tv/TVView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    iput-object v2, p0, Lcom/konka/avenger/tv/TVView;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iput-boolean v1, p0, Lcom/konka/avenger/tv/TVView;->mCreateFromLayout:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/avenger/tv/TVView;->isSetTVInput:Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/avenger/tv/TVView;->isSetStorageInput:Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/avenger/tv/TVView;->bSync:Ljava/lang/Boolean;

    new-instance v0, Lcom/konka/avenger/tv/TVView$1;

    invoke-direct {v0, p0}, Lcom/konka/avenger/tv/TVView$1;-><init>(Lcom/konka/avenger/tv/TVView;)V

    iput-object v0, p0, Lcom/konka/avenger/tv/TVView;->mHandler:Landroid/os/Handler;

    iput p3, p0, Lcom/konka/avenger/tv/TVView;->mWindowTop:I

    iput p2, p0, Lcom/konka/avenger/tv/TVView;->mWindowLeft:I

    iput p4, p0, Lcom/konka/avenger/tv/TVView;->mWindowWidth:I

    iput p5, p0, Lcom/konka/avenger/tv/TVView;->mWindowHeight:I

    iput-object p1, p0, Lcom/konka/avenger/tv/TVView;->mContext:Landroid/content/Context;

    iput-boolean v1, p0, Lcom/konka/avenger/tv/TVView;->mCreateFromLayout:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const-string v0, "TVWindow"

    iput-object v0, p0, Lcom/konka/avenger/tv/TVView;->TAG:Ljava/lang/String;

    const/16 v0, 0x2711

    iput v0, p0, Lcom/konka/avenger/tv/TVView;->SHOW_TV:I

    const/16 v0, 0x2712

    iput v0, p0, Lcom/konka/avenger/tv/TVView;->CLOSE_TV:I

    iput-boolean v1, p0, Lcom/konka/avenger/tv/TVView;->mRealState:Z

    iput-boolean v1, p0, Lcom/konka/avenger/tv/TVView;->mExpectState:Z

    iput-boolean v1, p0, Lcom/konka/avenger/tv/TVView;->mConditionChanged:Z

    iput-object v2, p0, Lcom/konka/avenger/tv/TVView;->mContext:Landroid/content/Context;

    iput v1, p0, Lcom/konka/avenger/tv/TVView;->mWindowTop:I

    iput v1, p0, Lcom/konka/avenger/tv/TVView;->mWindowLeft:I

    iput v1, p0, Lcom/konka/avenger/tv/TVView;->mWindowWidth:I

    iput v1, p0, Lcom/konka/avenger/tv/TVView;->mWindowHeight:I

    iput-object v2, p0, Lcom/konka/avenger/tv/TVView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    iput-object v2, p0, Lcom/konka/avenger/tv/TVView;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iput-boolean v1, p0, Lcom/konka/avenger/tv/TVView;->mCreateFromLayout:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/avenger/tv/TVView;->isSetTVInput:Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/avenger/tv/TVView;->isSetStorageInput:Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/avenger/tv/TVView;->bSync:Ljava/lang/Boolean;

    new-instance v0, Lcom/konka/avenger/tv/TVView$1;

    invoke-direct {v0, p0}, Lcom/konka/avenger/tv/TVView$1;-><init>(Lcom/konka/avenger/tv/TVView;)V

    iput-object v0, p0, Lcom/konka/avenger/tv/TVView;->mHandler:Landroid/os/Handler;

    iput-object p1, p0, Lcom/konka/avenger/tv/TVView;->mContext:Landroid/content/Context;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/avenger/tv/TVView;->mCreateFromLayout:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const-string v0, "TVWindow"

    iput-object v0, p0, Lcom/konka/avenger/tv/TVView;->TAG:Ljava/lang/String;

    const/16 v0, 0x2711

    iput v0, p0, Lcom/konka/avenger/tv/TVView;->SHOW_TV:I

    const/16 v0, 0x2712

    iput v0, p0, Lcom/konka/avenger/tv/TVView;->CLOSE_TV:I

    iput-boolean v1, p0, Lcom/konka/avenger/tv/TVView;->mRealState:Z

    iput-boolean v1, p0, Lcom/konka/avenger/tv/TVView;->mExpectState:Z

    iput-boolean v1, p0, Lcom/konka/avenger/tv/TVView;->mConditionChanged:Z

    iput-object v2, p0, Lcom/konka/avenger/tv/TVView;->mContext:Landroid/content/Context;

    iput v1, p0, Lcom/konka/avenger/tv/TVView;->mWindowTop:I

    iput v1, p0, Lcom/konka/avenger/tv/TVView;->mWindowLeft:I

    iput v1, p0, Lcom/konka/avenger/tv/TVView;->mWindowWidth:I

    iput v1, p0, Lcom/konka/avenger/tv/TVView;->mWindowHeight:I

    iput-object v2, p0, Lcom/konka/avenger/tv/TVView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    iput-object v2, p0, Lcom/konka/avenger/tv/TVView;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iput-boolean v1, p0, Lcom/konka/avenger/tv/TVView;->mCreateFromLayout:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/avenger/tv/TVView;->isSetTVInput:Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/avenger/tv/TVView;->isSetStorageInput:Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/avenger/tv/TVView;->bSync:Ljava/lang/Boolean;

    new-instance v0, Lcom/konka/avenger/tv/TVView$1;

    invoke-direct {v0, p0}, Lcom/konka/avenger/tv/TVView$1;-><init>(Lcom/konka/avenger/tv/TVView;)V

    iput-object v0, p0, Lcom/konka/avenger/tv/TVView;->mHandler:Landroid/os/Handler;

    iput-object p1, p0, Lcom/konka/avenger/tv/TVView;->mContext:Landroid/content/Context;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/avenger/tv/TVView;->mCreateFromLayout:Z

    return-void
.end method

.method static synthetic access$0(Lcom/konka/avenger/tv/TVView;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/avenger/tv/TVView;->open()V

    return-void
.end method

.method static synthetic access$1(Lcom/konka/avenger/tv/TVView;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/avenger/tv/TVView;->close()V

    return-void
.end method

.method static synthetic access$2(Lcom/konka/avenger/tv/TVView;)Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/konka/avenger/tv/TVView;->bSync:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/avenger/tv/TVView;)Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/konka/avenger/tv/TVView;->isSetTVInput:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/avenger/tv/TVView;Ljava/lang/Boolean;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/avenger/tv/TVView;->isSetTVInput:Ljava/lang/Boolean;

    return-void
.end method

.method static synthetic access$5(Lcom/konka/avenger/tv/TVView;)Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lcom/konka/avenger/tv/TVView;->isSetStorageInput:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/avenger/tv/TVView;Ljava/lang/Boolean;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/avenger/tv/TVView;->isSetStorageInput:Ljava/lang/Boolean;

    return-void
.end method

.method static synthetic access$7(Lcom/konka/avenger/tv/TVView;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/avenger/tv/TVView;->setInputSource2Storage()V

    return-void
.end method

.method private close()V
    .locals 2

    iget-boolean v0, p0, Lcom/konka/avenger/tv/TVView;->mRealState:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/konka/avenger/tv/TVView;->mRealState:Z

    iget-boolean v1, p0, Lcom/konka/avenger/tv/TVView;->mExpectState:Z

    if-eq v0, v1, :cond_2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/avenger/tv/TVView;->mRealState:Z

    iget-boolean v0, p0, Lcom/konka/avenger/tv/TVView;->mCreateFromLayout:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/konka/avenger/tv/TVView;->mWM:Landroid/view/WindowManager;

    invoke-interface {v0, p0}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    iget-object v0, p0, Lcom/konka/avenger/tv/TVView;->TAG:Ljava/lang/String;

    const-string v1, "rm view ++++++++++++"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    invoke-direct {p0}, Lcom/konka/avenger/tv/TVView;->isTVorKTVOnTop()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/konka/avenger/tv/TVView;->setStorageInput()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/konka/avenger/tv/TVView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/konka/avenger/tv/TVView;->TAG:Ljava/lang/String;

    const-string v1, "the window is already closed"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private closeViewImmediate()V
    .locals 1

    iget-boolean v0, p0, Lcom/konka/avenger/tv/TVView;->mCreateFromLayout:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/konka/avenger/tv/TVView;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method private getCurInputsoureFromDB()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .locals 8

    const/4 v2, 0x0

    sget-object v7, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iget-object v1, p0, Lcom/konka/avenger/tv/TVView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/systemsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    const-string v2, "enInputSourceType"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    aget-object v7, v1, v2

    :cond_0
    return-object v7
.end method

.method private getMicStatusFromDB()I
    .locals 7

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/konka/avenger/tv/TVView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/systemsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/avenger/tv/TVView;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "******In db mic status is :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "bMicOn"

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "bMicOn"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/konka/avenger/tv/TVView;->TAG:Ljava/lang/String;

    const-string v2, "Fetch db fail"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    goto :goto_0
.end method

.method private initTVWindow()V
    .locals 3

    iget-object v0, p0, Lcom/konka/avenger/tv/TVView;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    if-nez v0, :cond_4

    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    iput-object v0, p0, Lcom/konka/avenger/tv/TVView;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iget v0, p0, Lcom/konka/avenger/tv/TVView;->mWindowLeft:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/konka/avenger/tv/TVView;->mWindowLeft:I

    iget v1, p0, Lcom/konka/avenger/tv/TVView;->mWindowWidth:I

    add-int/2addr v0, v1

    const/16 v1, 0x780

    if-le v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/konka/avenger/tv/TVView;->TAG:Ljava/lang/String;

    const-string v1, "For test force value mWindowLeft,mWindowWidth 1203,654"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x4b3

    iput v0, p0, Lcom/konka/avenger/tv/TVView;->mWindowLeft:I

    const/16 v0, 0x28e

    iput v0, p0, Lcom/konka/avenger/tv/TVView;->mWindowWidth:I

    :cond_1
    iget v0, p0, Lcom/konka/avenger/tv/TVView;->mWindowTop:I

    if-ltz v0, :cond_2

    iget v0, p0, Lcom/konka/avenger/tv/TVView;->mWindowTop:I

    iget v1, p0, Lcom/konka/avenger/tv/TVView;->mWindowHeight:I

    add-int/2addr v0, v1

    const/16 v1, 0x438

    if-le v0, v1, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/konka/avenger/tv/TVView;->TAG:Ljava/lang/String;

    const-string v1, "For test force value mWindowLeft,mWindowWidth 108,426"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v0, 0x6c

    iput v0, p0, Lcom/konka/avenger/tv/TVView;->mWindowTop:I

    const/16 v0, 0x1aa

    iput v0, p0, Lcom/konka/avenger/tv/TVView;->mWindowHeight:I

    :cond_3
    iget-object v0, p0, Lcom/konka/avenger/tv/TVView;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lcom/konka/avenger/tv/TVView;->mWindowLeft:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    iget-object v0, p0, Lcom/konka/avenger/tv/TVView;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lcom/konka/avenger/tv/TVView;->mWindowTop:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    iget-object v0, p0, Lcom/konka/avenger/tv/TVView;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lcom/konka/avenger/tv/TVView;->mWindowWidth:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    iget-object v0, p0, Lcom/konka/avenger/tv/TVView;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    iget v1, p0, Lcom/konka/avenger/tv/TVView;->mWindowHeight:I

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    iget-object v0, p0, Lcom/konka/avenger/tv/TVView;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    const/4 v1, 0x2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    iget-object v0, p0, Lcom/konka/avenger/tv/TVView;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x18

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    iget-object v0, p0, Lcom/konka/avenger/tv/TVView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "openSurfaceView==="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/avenger/tv/TVView;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/avenger/tv/TVView;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x33

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    iget-object v0, p0, Lcom/konka/avenger/tv/TVView;->mContext:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/konka/avenger/tv/TVView;->mWM:Landroid/view/WindowManager;

    :cond_4
    invoke-direct {p0}, Lcom/konka/avenger/tv/TVView;->initVideoView()V

    return-void
.end method

.method private initVideoView()V
    .locals 2

    iget-object v0, p0, Lcom/konka/avenger/tv/TVView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/konka/avenger/tv/TVView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/avenger/tv/TVView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    iget-object v0, p0, Lcom/konka/avenger/tv/TVView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    iget-object v0, p0, Lcom/konka/avenger/tv/TVView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    :cond_0
    return-void
.end method

.method private isTVorKTVOnTop()Z
    .locals 8

    const/4 v3, 0x0

    const/4 v7, 0x2

    const/4 v4, 0x1

    iget-object v2, p0, Lcom/konka/avenger/tv/TVView;->mContext:Landroid/content/Context;

    const-string v5, "activity"

    invoke-virtual {v2, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0, v7}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/konka/avenger/tv/TVView;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "the top activity=========="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "com.iflytek.vaf"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "com.konka.hotkey"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    invoke-virtual {v0, v7}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v4, :cond_1

    invoke-virtual {v0, v7}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v2, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/konka/avenger/tv/TVView;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "the second activity=========="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "com.konka.tvsettings"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v5, "com.konka.karaoke"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    iget-object v2, p0, Lcom/konka/avenger/tv/TVView;->TAG:Ljava/lang/String;

    const-string v3, "the media app is on the top task list,no need to switch source"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v4

    :goto_0
    return v2

    :cond_3
    move v2, v3

    goto :goto_0
.end method

.method private open()V
    .locals 2

    iget-boolean v0, p0, Lcom/konka/avenger/tv/TVView;->mRealState:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/konka/avenger/tv/TVView;->mRealState:Z

    iget-boolean v1, p0, Lcom/konka/avenger/tv/TVView;->mExpectState:Z

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/avenger/tv/TVView;->mRealState:Z

    invoke-virtual {p0}, Lcom/konka/avenger/tv/TVView;->init()V

    iget-boolean v0, p0, Lcom/konka/avenger/tv/TVView;->mCreateFromLayout:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/avenger/tv/TVView;->mWM:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/konka/avenger/tv/TVView;->surfaceParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, p0, v1}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/konka/avenger/tv/TVView;->TAG:Ljava/lang/String;

    const-string v1, "add view ++++++++++++"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/konka/avenger/tv/TVView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/konka/avenger/tv/TVView;->TAG:Ljava/lang/String;

    const-string v1, "the window is already open"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private scaleFullWindow()V
    .locals 4

    const v2, 0xffff

    new-instance v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;

    invoke-direct {v1}, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;-><init>()V

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->height:I

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->width:I

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->x:I

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->y:I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->selectWindow(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/mstar/android/tvapi/common/PictureManager;->setDisplayWindow(Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/PictureManager;->scaleWindow()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private setInputSource2Storage()V
    .locals 6

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0}, Lcom/konka/avenger/tv/TVView;->getMicStatusFromDB()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_KTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    :goto_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    if-eq v2, v1, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v1, v3, v4, v5}, Lcom/mstar/android/tvapi/common/TvManager;->setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;ZZZ)Z

    iget-object v2, p0, Lcom/konka/avenger/tv/TVView;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Force (mic) switch to "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_1
    return-void

    :cond_1
    sget-object v1, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/konka/avenger/tv/TVView;->TAG:Ljava/lang/String;

    const-string v3, "Do not need to switch to MM, because it is already here!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method public init()V
    .locals 1

    iget-boolean v0, p0, Lcom/konka/avenger/tv/TVView;->mCreateFromLayout:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/konka/avenger/tv/TVView;->initVideoView()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/konka/avenger/tv/TVView;->initTVWindow()V

    goto :goto_0
.end method

.method public resetState()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/avenger/tv/TVView;->mConditionChanged:Z

    iput-boolean v0, p0, Lcom/konka/avenger/tv/TVView;->mExpectState:Z

    iput-boolean v0, p0, Lcom/konka/avenger/tv/TVView;->mRealState:Z

    return-void
.end method

.method public scaleSmallWindow()V
    .locals 8

    const/4 v6, 0x0

    iget-object v5, p0, Lcom/konka/avenger/tv/TVView;->TAG:Ljava/lang/String;

    const-string v7, "thread scaleSmallWindow =================="

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/konka/avenger/tv/TVView;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/konka/kkimplements/tv/mstar/CommonDeskImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v0

    new-instance v4, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;

    invoke-direct {v4}, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;-><init>()V

    iget v5, p0, Lcom/konka/avenger/tv/TVView;->mWindowLeft:I

    if-ltz v5, :cond_0

    iget v5, p0, Lcom/konka/avenger/tv/TVView;->mWindowLeft:I

    iget v7, p0, Lcom/konka/avenger/tv/TVView;->mWindowWidth:I

    add-int/2addr v5, v7

    const/16 v7, 0x780

    if-le v5, v7, :cond_1

    :cond_0
    iget-object v5, p0, Lcom/konka/avenger/tv/TVView;->TAG:Ljava/lang/String;

    const-string v7, "For test force value mWindowLeft,mWindowWidth 1203,654"

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v5, 0x4b3

    iput v5, p0, Lcom/konka/avenger/tv/TVView;->mWindowLeft:I

    const/16 v5, 0x28e

    iput v5, p0, Lcom/konka/avenger/tv/TVView;->mWindowWidth:I

    :cond_1
    iget v5, p0, Lcom/konka/avenger/tv/TVView;->mWindowTop:I

    if-ltz v5, :cond_2

    iget v5, p0, Lcom/konka/avenger/tv/TVView;->mWindowTop:I

    iget v7, p0, Lcom/konka/avenger/tv/TVView;->mWindowHeight:I

    add-int/2addr v5, v7

    const/16 v7, 0x438

    if-le v5, v7, :cond_3

    :cond_2
    iget-object v5, p0, Lcom/konka/avenger/tv/TVView;->TAG:Ljava/lang/String;

    const-string v7, "For test force value mWindowLeft,mWindowWidth 108,426"

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v5, 0x6c

    iput v5, p0, Lcom/konka/avenger/tv/TVView;->mWindowTop:I

    const/16 v5, 0x1aa

    iput v5, p0, Lcom/konka/avenger/tv/TVView;->mWindowHeight:I

    :cond_3
    iget v7, p0, Lcom/konka/avenger/tv/TVView;->mWindowLeft:I

    iget-object v5, p0, Lcom/konka/avenger/tv/TVView;->mContext:Landroid/content/Context;

    invoke-interface {v0, v5}, Lcom/konka/kkinterface/tv/CommonDesk;->is1280x720(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_8

    const/16 v5, 0x35

    :goto_0
    add-int/2addr v5, v7

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->x:I

    iget v5, p0, Lcom/konka/avenger/tv/TVView;->mWindowTop:I

    iget-object v7, p0, Lcom/konka/avenger/tv/TVView;->mContext:Landroid/content/Context;

    invoke-interface {v0, v7}, Lcom/konka/kkinterface/tv/CommonDesk;->is1280x720(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_4

    :cond_4
    add-int/lit8 v5, v5, 0x0

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->y:I

    iget v7, p0, Lcom/konka/avenger/tv/TVView;->mWindowWidth:I

    iget-object v5, p0, Lcom/konka/avenger/tv/TVView;->mContext:Landroid/content/Context;

    invoke-interface {v0, v5}, Lcom/konka/kkinterface/tv/CommonDesk;->is1280x720(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_9

    const/16 v5, 0x1d

    :goto_1
    add-int/2addr v5, v7

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->width:I

    iget v5, p0, Lcom/konka/avenger/tv/TVView;->mWindowHeight:I

    iget-object v7, p0, Lcom/konka/avenger/tv/TVView;->mContext:Landroid/content/Context;

    invoke-interface {v0, v7}, Lcom/konka/kkinterface/tv/CommonDesk;->is1280x720(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_5

    const/16 v6, 0x18

    :cond_5
    add-int/2addr v5, v6

    iput v5, v4, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->height:I

    iget-object v5, p0, Lcom/konka/avenger/tv/TVView;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "[[[[[[[[[[[[[[the [x,y][w,h]=====["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, v4, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->x:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v4, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->y:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v4, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->width:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v4, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->height:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    if-eqz v5, :cond_7

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v5

    if-eqz v5, :cond_7

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v5

    sget-object v6, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-virtual {v5, v6}, Lcom/mstar/android/tvapi/common/PictureManager;->selectWindow(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/mstar/android/tvapi/common/PictureManager;->setDisplayWindow(Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/PictureManager;->scaleWindow()Z

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/CommonDesk;->getLocalDimmingInfo()Lcom/konka/kkinterface/tv/CommonDesk$LocalDimmingInfo;

    move-result-object v2

    iget-object v5, p0, Lcom/konka/avenger/tv/TVView;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "get localdimminginfo enable = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, v2, Lcom/konka/kkinterface/tv/CommonDesk$LocalDimmingInfo;->localdimmingenable:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v5, v2, Lcom/konka/kkinterface/tv/CommonDesk$LocalDimmingInfo;->localdimmingenable:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_6

    iget-object v5, p0, Lcom/konka/avenger/tv/TVView;->TAG:Ljava/lang/String;

    const-string v6, "launcher tv smallwindow settting backlight value is 100. "

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v5

    const/16 v6, 0x64

    invoke-virtual {v5, v6}, Lcom/mstar/android/tvapi/common/PictureManager;->setBacklight(I)V

    :cond_6
    iget-object v5, p0, Lcom/konka/avenger/tv/TVView;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;

    move-result-object v5

    invoke-virtual {v5}, Lcom/konka/kkimplements/tv/mstar/DataBaseDeskImpl;->IsSystemLocked()Z

    move-result v5

    if-nez v5, :cond_7

    iget-object v5, p0, Lcom/konka/avenger/tv/TVView;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->getChannelMgrInstance(Landroid/content/Context;)Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;

    move-result-object v5

    invoke-virtual {v5}, Lcom/konka/kkimplements/tv/mstar/ChannelDeskImpl;->getCurrProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v3

    iget-object v5, p0, Lcom/konka/avenger/tv/TVView;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "luncher tvwidget current programinfo islock = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v7, v3, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->isLock:Z

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v5, v3, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->isLock:Z

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/konka/avenger/tv/TVView;->TAG:Ljava/lang/String;

    const-string v6, "luncher tvwidget unlockChannel."

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getDvbPlayerManager()Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;

    move-result-object v5

    invoke-interface {v5}, Lcom/mstar/android/tvapi/dtv/dvb/DvbPlayer;->unlockChannel()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_7
    :goto_2
    return-void

    :cond_8
    move v5, v6

    goto/16 :goto_0

    :cond_9
    move v5, v6

    goto/16 :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2
.end method

.method public setInputSource2TV()V
    .locals 8

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :cond_0
    :goto_0
    iget-object v5, p0, Lcom/konka/avenger/tv/TVView;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "================getCurrentInputSource==== "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v5

    sget-object v6, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v6

    if-ge v5, v6, :cond_2

    iget-object v5, p0, Lcom/konka/avenger/tv/TVView;->TAG:Ljava/lang/String;

    const-string v6, "the current input source is TV source,do not switch!"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/konka/avenger/tv/TVView;->getCurInputsoureFromDB()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    iget-object v5, p0, Lcom/konka/avenger/tv/TVView;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "=====================get the input source from DB ==== "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v5

    sget-object v6, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_STORAGE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v6

    if-lt v5, v6, :cond_3

    iget-object v5, p0, Lcom/konka/avenger/tv/TVView;->TAG:Ljava/lang/String;

    const-string v6, "the current input source is not tv source,so switch to ATV"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    :cond_3
    sget-object v5, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v3, v5, :cond_4

    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    sget-object v6, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v5, v6}, Lcom/mstar/android/tvapi/common/TvManager;->setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/ChannelManager;->getCurrChannelNumber()I

    move-result v4

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v5

    sget-object v6, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->E_SERVICETYPE_ATV:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    invoke-virtual {v6}, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->ordinal()I

    move-result v6

    int-to-short v6, v6

    const/4 v7, 0x0

    invoke-virtual {v5, v4, v6, v7}, Lcom/mstar/android/tvapi/common/ChannelManager;->selectProgram(ISI)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1

    :cond_4
    sget-object v5, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v3, v5, :cond_5

    :try_start_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    sget-object v6, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v5, v6}, Lcom/mstar/android/tvapi/common/TvManager;->setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getInstance()Lcom/mstar/android/tv/TvChannelManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/mstar/android/tv/TvChannelManager;->playDtvCurrentProgram()Z
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_1

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_1

    :cond_5
    :try_start_3
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v5

    invoke-virtual {v5, v3}, Lcom/mstar/android/tvapi/common/TvManager;->setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_1

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_1
.end method

.method public declared-synchronized setState(Z)V
    .locals 1
    .param p1    # Z

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/konka/avenger/tv/TVView;->mConditionChanged:Z

    iput-boolean p1, p0, Lcom/konka/avenger/tv/TVView;->mExpectState:Z

    if-nez p1, :cond_0

    invoke-direct {p0}, Lcom/konka/avenger/tv/TVView;->closeViewImmediate()V

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setStorageInput()V
    .locals 2

    iget-object v1, p0, Lcom/konka/avenger/tv/TVView;->bSync:Ljava/lang/Boolean;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/avenger/tv/TVView;->isSetStorageInput:Ljava/lang/Boolean;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setTVInput()V
    .locals 2

    iget-object v1, p0, Lcom/konka/avenger/tv/TVView;->bSync:Ljava/lang/Boolean;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/avenger/tv/TVView;->isSetTVInput:Ljava/lang/Boolean;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public startInputSourceThread()V
    .locals 1

    invoke-virtual {p0}, Lcom/konka/avenger/tv/TVView;->stopInputSourceThread()V

    new-instance v0, Lcom/konka/avenger/tv/TVView$InputSourceThread;

    invoke-direct {v0, p0}, Lcom/konka/avenger/tv/TVView$InputSourceThread;-><init>(Lcom/konka/avenger/tv/TVView;)V

    iput-object v0, p0, Lcom/konka/avenger/tv/TVView;->inputSourceThread:Lcom/konka/avenger/tv/TVView$InputSourceThread;

    iget-object v0, p0, Lcom/konka/avenger/tv/TVView;->inputSourceThread:Lcom/konka/avenger/tv/TVView$InputSourceThread;

    invoke-virtual {v0}, Lcom/konka/avenger/tv/TVView$InputSourceThread;->start()V

    return-void
.end method

.method public stopInputSourceThread()V
    .locals 2

    iget-object v0, p0, Lcom/konka/avenger/tv/TVView;->inputSourceThread:Lcom/konka/avenger/tv/TVView$InputSourceThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/avenger/tv/TVView;->inputSourceThread:Lcom/konka/avenger/tv/TVView$InputSourceThread;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/konka/avenger/tv/TVView$InputSourceThread;->access$0(Lcom/konka/avenger/tv/TVView$InputSourceThread;Z)V

    :cond_0
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 3
    .param p1    # Landroid/view/SurfaceHolder;

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Surface;->isValid()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/konka/avenger/tv/TVView;->getY()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/konka/avenger/tv/TVView;->mWindowTop:I

    invoke-virtual {p0}, Lcom/konka/avenger/tv/TVView;->getX()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/konka/avenger/tv/TVView;->mWindowLeft:I

    invoke-virtual {p0}, Lcom/konka/avenger/tv/TVView;->getWidth()I

    move-result v1

    iput v1, p0, Lcom/konka/avenger/tv/TVView;->mWindowWidth:I

    invoke-virtual {p0}, Lcom/konka/avenger/tv/TVView;->getHeight()I

    move-result v1

    iput v1, p0, Lcom/konka/avenger/tv/TVView;->mWindowHeight:I

    iget-object v1, p0, Lcom/konka/avenger/tv/TVView;->TAG:Ljava/lang/String;

    const-string v2, "***********************surfaceCreated!!!!!!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/avenger/tv/TVView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v1, v2}, Lcom/mstar/android/tvapi/common/TvPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/konka/avenger/tv/TVView;->setTVInput()V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/konka/avenger/tv/TVView;->setBackgroundColor(I)V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1    # Landroid/view/SurfaceHolder;

    return-void
.end method

.method public declared-synchronized updateState()V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/konka/avenger/tv/TVView;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "update state===="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/konka/avenger/tv/TVView;->mExpectState:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/konka/avenger/tv/TVView;->mExpectState:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/avenger/tv/TVView;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x2711

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/konka/avenger/tv/TVView;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x2712

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized waitForStateChange()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    monitor-enter p0

    :goto_0
    :try_start_0
    iget-boolean v0, p0, Lcom/konka/avenger/tv/TVView;->mConditionChanged:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/konka/avenger/tv/TVView;->mExpectState:Z

    iget-boolean v1, p0, Lcom/konka/avenger/tv/TVView;->mRealState:Z

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/avenger/tv/TVView;->mConditionChanged:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
