.class Lcom/konka/avenger/tv/TVWatchingThread;
.super Ljava/lang/Object;
.source "TVWindowManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private tvWindow:Lcom/konka/avenger/tv/TVView;


# direct methods
.method public constructor <init>(Lcom/konka/avenger/tv/TVView;)V
    .locals 0
    .param p1    # Lcom/konka/avenger/tv/TVView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/konka/avenger/tv/TVWatchingThread;->tvWindow:Lcom/konka/avenger/tv/TVView;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    :goto_0
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_1
    return-void

    :cond_0
    const-string v1, "TVWindowManager"

    const-string v2, "waiting in the thread!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/avenger/tv/TVWatchingThread;->tvWindow:Lcom/konka/avenger/tv/TVView;

    invoke-virtual {v1}, Lcom/konka/avenger/tv/TVView;->waitForStateChange()V

    const-string v1, "TVWindowManager"

    const-string v2, "runing in the thread!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/avenger/tv/TVWatchingThread;->tvWindow:Lcom/konka/avenger/tv/TVView;

    invoke-virtual {v1}, Lcom/konka/avenger/tv/TVView;->updateState()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1
.end method
