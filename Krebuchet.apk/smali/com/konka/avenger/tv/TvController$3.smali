.class Lcom/konka/avenger/tv/TvController$3;
.super Ljava/lang/Thread;
.source "TvController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/avenger/tv/TvController;->setInputSource2TV()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/avenger/tv/TvController;


# direct methods
.method constructor <init>(Lcom/konka/avenger/tv/TvController;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/avenger/tv/TvController$3;->this$0:Lcom/konka/avenger/tv/TvController;

    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v2, p0, Lcom/konka/avenger/tv/TvController$3;->this$0:Lcom/konka/avenger/tv/TvController;

    # getter for: Lcom/konka/avenger/tv/TvController;->mSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    invoke-static {v2}, Lcom/konka/avenger/tv/TvController;->access$4(Lcom/konka/avenger/tv/TvController;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v2, v3, :cond_0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/TvManager;->setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/ChannelManager;->getCurrChannelNumber()I

    move-result v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v2

    sget-object v3, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->E_SERVICETYPE_ATV:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->ordinal()I

    move-result v3

    int-to-short v3, v3

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v3, v4}, Lcom/mstar/android/tvapi/common/ChannelManager;->selectProgram(ISI)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/konka/avenger/tv/TvController$3;->this$0:Lcom/konka/avenger/tv/TvController;

    # getter for: Lcom/konka/avenger/tv/TvController;->mSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    invoke-static {v2}, Lcom/konka/avenger/tv/TvController;->access$4(Lcom/konka/avenger/tv/TvController;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v2, v3, :cond_1

    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/TvManager;->setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/ChannelManager;->getCurrChannelNumber()I

    move-result v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v2

    sget-object v3, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->E_SERVICETYPE_DTV:Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;

    invoke-virtual {v3}, Lcom/konka/kkinterface/tv/DtvInterface$MEMBER_SERVICETYPE;->ordinal()I

    move-result v3

    int-to-short v3, v3

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v3, v4}, Lcom/mstar/android/tvapi/common/ChannelManager;->selectProgram(ISI)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_1
    :try_start_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/avenger/tv/TvController$3;->this$0:Lcom/konka/avenger/tv/TvController;

    # getter for: Lcom/konka/avenger/tv/TvController;->mSource:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    invoke-static {v3}, Lcom/konka/avenger/tv/TvController;->access$4(Lcom/konka/avenger/tv/TvController;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/TvManager;->setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method
