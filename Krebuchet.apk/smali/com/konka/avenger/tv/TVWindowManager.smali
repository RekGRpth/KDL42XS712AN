.class public Lcom/konka/avenger/tv/TVWindowManager;
.super Ljava/lang/Object;
.source "TVWindowManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;
    }
.end annotation


# instance fields
.field private TAG:Ljava/lang/String;

.field private mTVWindow:Lcom/konka/avenger/tv/TVView;

.field private mThread:Ljava/lang/Thread;


# direct methods
.method public constructor <init>(Landroid/content/Context;IIIILcom/konka/avenger/tv/TVView;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # Lcom/konka/avenger/tv/TVView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "TVWindowManager"

    iput-object v0, p0, Lcom/konka/avenger/tv/TVWindowManager;->TAG:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/avenger/tv/TVWindowManager;->mTVWindow:Lcom/konka/avenger/tv/TVView;

    iget-object v0, p0, Lcom/konka/avenger/tv/TVWindowManager;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "create the window ===("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",mTVWindow = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p6, :cond_0

    new-instance v0, Lcom/konka/avenger/tv/TVView;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/konka/avenger/tv/TVView;-><init>(Landroid/content/Context;IIII)V

    iput-object v0, p0, Lcom/konka/avenger/tv/TVWindowManager;->mTVWindow:Lcom/konka/avenger/tv/TVView;

    :goto_0
    iget-object v0, p0, Lcom/konka/avenger/tv/TVWindowManager;->mTVWindow:Lcom/konka/avenger/tv/TVView;

    const v1, 0x7f020046    # com.konka.avenger.R.drawable.home_default_tv

    invoke-virtual {v0, v1}, Lcom/konka/avenger/tv/TVView;->setBackgroundResource(I)V

    return-void

    :cond_0
    iput-object p6, p0, Lcom/konka/avenger/tv/TVWindowManager;->mTVWindow:Lcom/konka/avenger/tv/TVView;

    iget-object v0, p0, Lcom/konka/avenger/tv/TVWindowManager;->mTVWindow:Lcom/konka/avenger/tv/TVView;

    invoke-virtual {v0}, Lcom/konka/avenger/tv/TVView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/widget/FrameLayout$LayoutParams;

    iput p4, v6, Landroid/widget/FrameLayout$LayoutParams;->width:I

    iput p5, v6, Landroid/widget/FrameLayout$LayoutParams;->height:I

    iput p2, v6, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iput p3, v6, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    iget-object v0, p0, Lcom/konka/avenger/tv/TVWindowManager;->mTVWindow:Lcom/konka/avenger/tv/TVView;

    invoke-virtual {v0, v6}, Lcom/konka/avenger/tv/TVView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method


# virtual methods
.method public hideBackground()V
    .locals 2

    iget-object v0, p0, Lcom/konka/avenger/tv/TVWindowManager;->mTVWindow:Lcom/konka/avenger/tv/TVView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/konka/avenger/tv/TVView;->setBackgroundColor(I)V

    return-void
.end method

.method public isResetCreate()Z
    .locals 3

    iget-object v1, p0, Lcom/konka/avenger/tv/TVWindowManager;->mTVWindow:Lcom/konka/avenger/tv/TVView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/avenger/tv/TVWindowManager;->mTVWindow:Lcom/konka/avenger/tv/TVView;

    invoke-virtual {v1}, Lcom/konka/avenger/tv/TVView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    if-nez v1, :cond_0

    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    if-nez v1, :cond_0

    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    if-nez v1, :cond_0

    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/konka/avenger/tv/TVWindowManager;->TAG:Ljava/lang/String;

    const-string v2, "isResetCreate true"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public resetTVWindow()V
    .locals 1

    iget-object v0, p0, Lcom/konka/avenger/tv/TVWindowManager;->mTVWindow:Lcom/konka/avenger/tv/TVView;

    invoke-virtual {v0}, Lcom/konka/avenger/tv/TVView;->scaleSmallWindow()V

    return-void
.end method

.method public setState(Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;)V
    .locals 2
    .param p1    # Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;

    iget-object v0, p0, Lcom/konka/avenger/tv/TVWindowManager;->TAG:Ljava/lang/String;

    const-string v1, "set state"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;->OPEN:Lcom/konka/avenger/tv/TVWindowManager$TV_STATE;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/konka/avenger/tv/TVWindowManager;->mTVWindow:Lcom/konka/avenger/tv/TVView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/konka/avenger/tv/TVView;->setState(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/avenger/tv/TVWindowManager;->mTVWindow:Lcom/konka/avenger/tv/TVView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/konka/avenger/tv/TVView;->setState(Z)V

    goto :goto_0
.end method

.method public showBackground()V
    .locals 2

    iget-object v0, p0, Lcom/konka/avenger/tv/TVWindowManager;->mTVWindow:Lcom/konka/avenger/tv/TVView;

    const v1, 0x7f020046    # com.konka.avenger.R.drawable.home_default_tv

    invoke-virtual {v0, v1}, Lcom/konka/avenger/tv/TVView;->setBackgroundResource(I)V

    return-void
.end method

.method public start()V
    .locals 3

    iget-object v0, p0, Lcom/konka/avenger/tv/TVWindowManager;->TAG:Ljava/lang/String;

    const-string v1, "start thread"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/avenger/tv/TVWindowManager;->mTVWindow:Lcom/konka/avenger/tv/TVView;

    invoke-virtual {v0}, Lcom/konka/avenger/tv/TVView;->resetState()V

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/konka/avenger/tv/TVWatchingThread;

    iget-object v2, p0, Lcom/konka/avenger/tv/TVWindowManager;->mTVWindow:Lcom/konka/avenger/tv/TVView;

    invoke-direct {v1, v2}, Lcom/konka/avenger/tv/TVWatchingThread;-><init>(Lcom/konka/avenger/tv/TVView;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/konka/avenger/tv/TVWindowManager;->mThread:Ljava/lang/Thread;

    iget-object v0, p0, Lcom/konka/avenger/tv/TVWindowManager;->mThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public stop()V
    .locals 2

    iget-object v0, p0, Lcom/konka/avenger/tv/TVWindowManager;->TAG:Ljava/lang/String;

    const-string v1, "stop thread"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/avenger/tv/TVWindowManager;->mThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    return-void
.end method
