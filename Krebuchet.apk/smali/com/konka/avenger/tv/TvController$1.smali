.class Lcom/konka/avenger/tv/TvController$1;
.super Ljava/lang/Object;
.source "TvController.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/avenger/tv/TvController;->createTvWidgetView(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/avenger/tv/TvController;


# direct methods
.method constructor <init>(Lcom/konka/avenger/tv/TvController;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/avenger/tv/TvController$1;->this$0:Lcom/konka/avenger/tv/TvController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 3
    .param p1    # Landroid/view/SurfaceHolder;

    iget-object v1, p0, Lcom/konka/avenger/tv/TvController$1;->this$0:Lcom/konka/avenger/tv/TvController;

    invoke-virtual {v1}, Lcom/konka/avenger/tv/TvController;->isTVServiceAlive()Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "TvWindowController"

    const-string v2, "can not Created surface"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Surface;->isValid()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "TvWindowController"

    const-string v2, "***********************surfaceCreated!!!!!!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    # getter for: Lcom/konka/avenger/tv/TvController;->mtvplayer:Lcom/mstar/android/tvapi/common/TvPlayer;
    invoke-static {}, Lcom/konka/avenger/tv/TvController;->access$0()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/common/TvPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v1, p0, Lcom/konka/avenger/tv/TvController$1;->this$0:Lcom/konka/avenger/tv/TvController;

    # invokes: Lcom/konka/avenger/tv/TvController;->scaleSmallWindow()V
    invoke-static {v1}, Lcom/konka/avenger/tv/TvController;->access$1(Lcom/konka/avenger/tv/TvController;)V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1    # Landroid/view/SurfaceHolder;

    const-string v0, "TvWindowController"

    const-string v1, "***********************surfaceDestroyed!!!!!!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
