.class public Lcom/konka/avenger/tv/TVWindowWidgetProvider;
.super Landroid/appwidget/AppWidgetProvider;
.source "TVWindowWidgetProvider.java"


# static fields
.field static final TAG:Ljava/lang/String; = "TVWindowWidgetProvider"

.field private static mAppWidgeManger:Landroid/appwidget/AppWidgetManager;

.field private static mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    return-void
.end method

.method public static udpateBackground(Landroid/graphics/Bitmap;)V
    .locals 4
    .param p0    # Landroid/graphics/Bitmap;

    sget-object v2, Lcom/konka/avenger/tv/TVWindowWidgetProvider;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/konka/avenger/tv/TVWindowWidgetProvider;->mAppWidgeManger:Landroid/appwidget/AppWidgetManager;

    if-eqz v2, :cond_0

    new-instance v0, Landroid/widget/RemoteViews;

    sget-object v2, Lcom/konka/avenger/tv/TVWindowWidgetProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f03002b    # com.konka.avenger.R.layout.tvwindow_widget_main

    invoke-direct {v0, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    new-instance v1, Landroid/content/ComponentName;

    sget-object v2, Lcom/konka/avenger/tv/TVWindowWidgetProvider;->mContext:Landroid/content/Context;

    const-class v3, Lcom/konka/avenger/tv/TVWindowWidgetProvider;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const v2, 0x7f0d0063    # com.konka.avenger.R.id.tvwindow_widget

    const-string v3, "setImageBitmap"

    invoke-virtual {v0, v2, v3, p0}, Landroid/widget/RemoteViews;->setBitmap(ILjava/lang/String;Landroid/graphics/Bitmap;)V

    sget-object v2, Lcom/konka/avenger/tv/TVWindowWidgetProvider;->mAppWidgeManger:Landroid/appwidget/AppWidgetManager;

    invoke-virtual {v2, v1, v0}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(Landroid/content/ComponentName;Landroid/widget/RemoteViews;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public onDeleted(Landroid/content/Context;[I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # [I

    const-string v0, "TVWindowWidgetProvider"

    const-string v1, "onDeleted"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onDeleted(Landroid/content/Context;[I)V

    return-void
.end method

.method public onDisabled(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const-string v0, "TVWindowWidgetProvider"

    const-string v1, "onDisabled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onDisabled(Landroid/content/Context;)V

    return-void
.end method

.method public onEnabled(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const-string v0, "TVWindowWidgetProvider"

    const-string v1, "onEnabled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onEnabled(Landroid/content/Context;)V

    sput-object p1, Lcom/konka/avenger/tv/TVWindowWidgetProvider;->mContext:Landroid/content/Context;

    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    sput-object p1, Lcom/konka/avenger/tv/TVWindowWidgetProvider;->mContext:Landroid/content/Context;

    return-void
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/appwidget/AppWidgetManager;
    .param p3    # [I

    invoke-virtual {p0, p1, p2}, Lcom/konka/avenger/tv/TVWindowWidgetProvider;->updateAppWidget(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;)V

    invoke-super {p0, p1, p2, p3}, Landroid/appwidget/AppWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    return-void
.end method

.method public updateAppWidget(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/appwidget/AppWidgetManager;

    const/4 v6, 0x0

    const-string v4, "TVWindowWidgetProvider"

    const-string v5, "updateAppWidget"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sput-object p2, Lcom/konka/avenger/tv/TVWindowWidgetProvider;->mAppWidgeManger:Landroid/appwidget/AppWidgetManager;

    new-instance v1, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f03002b    # com.konka.avenger.R.layout.tvwindow_widget_main

    invoke-direct {v1, v4, v5}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    new-instance v2, Landroid/content/ComponentName;

    const-class v4, Lcom/konka/avenger/tv/TVWindowWidgetProvider;

    invoke-direct {v2, p1, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v4, "com.konka.tvsettings"

    const-string v5, "com.konka.tvsettings.RootActivity"

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "android.intent.action.MAIN"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v4, 0x10200000

    invoke-virtual {v0, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-static {p1, v6, v0, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    const v4, 0x7f0d0063    # com.konka.avenger.R.id.tvwindow_widget

    invoke-virtual {v1, v4, v3}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    invoke-virtual {p2, v2, v1}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(Landroid/content/ComponentName;Landroid/widget/RemoteViews;)V

    return-void
.end method
