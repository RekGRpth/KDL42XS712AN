.class public Lcom/konka/passport/service/PassportInfo;
.super Ljava/lang/Object;
.source "PassportInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/konka/passport/service/PassportInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private HeadPic:Landroid/graphics/Bitmap;

.field private IpAddress:Ljava/lang/String;

.field private PassportId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/konka/passport/service/PassportInfo$1;

    invoke-direct {v0}, Lcom/konka/passport/service/PassportInfo$1;-><init>()V

    sput-object v0, Lcom/konka/passport/service/PassportInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/passport/service/PassportInfo;->PassportId:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/passport/service/PassportInfo;->IpAddress:Ljava/lang/String;

    const-class v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/konka/passport/service/PassportInfo;->HeadPic:Landroid/graphics/Bitmap;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/konka/passport/service/PassportInfo;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/passport/service/PassportInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/graphics/Bitmap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/konka/passport/service/PassportInfo;->PassportId:Ljava/lang/String;

    iput-object p2, p0, Lcom/konka/passport/service/PassportInfo;->IpAddress:Ljava/lang/String;

    iput-object p3, p0, Lcom/konka/passport/service/PassportInfo;->HeadPic:Landroid/graphics/Bitmap;

    return-void
.end method


# virtual methods
.method public GetHeadPic()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/konka/passport/service/PassportInfo;->HeadPic:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public GetIpAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/passport/service/PassportInfo;->IpAddress:Ljava/lang/String;

    return-object v0
.end method

.method public GetPassportId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/passport/service/PassportInfo;->PassportId:Ljava/lang/String;

    return-object v0
.end method

.method public SetHeadPic(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1    # Landroid/graphics/Bitmap;

    iput-object p1, p0, Lcom/konka/passport/service/PassportInfo;->HeadPic:Landroid/graphics/Bitmap;

    return-void
.end method

.method public SetIpAddress(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/passport/service/PassportInfo;->IpAddress:Ljava/lang/String;

    return-void
.end method

.method public SetPassportId(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/passport/service/PassportInfo;->PassportId:Ljava/lang/String;

    return-void
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/passport/service/PassportInfo;->PassportId:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/passport/service/PassportInfo;->IpAddress:Ljava/lang/String;

    const-class v0, Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/konka/passport/service/PassportInfo;->HeadPic:Landroid/graphics/Bitmap;

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/konka/passport/service/PassportInfo;->PassportId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/passport/service/PassportInfo;->IpAddress:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/passport/service/PassportInfo;->HeadPic:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
