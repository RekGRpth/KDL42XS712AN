.class Lcom/konka/folder/OpenAction$1;
.super Ljava/lang/Object;
.source "OpenAction.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/folder/OpenAction;->addActionItem(Lnet/londatiga/android/ActionItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/folder/OpenAction;

.field private final synthetic val$actionId:I

.field private final synthetic val$pos:I


# direct methods
.method constructor <init>(Lcom/konka/folder/OpenAction;II)V
    .locals 0

    iput-object p1, p0, Lcom/konka/folder/OpenAction$1;->this$0:Lcom/konka/folder/OpenAction;

    iput p2, p0, Lcom/konka/folder/OpenAction$1;->val$pos:I

    iput p3, p0, Lcom/konka/folder/OpenAction$1;->val$actionId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/konka/folder/OpenAction$1;->this$0:Lcom/konka/folder/OpenAction;

    # getter for: Lcom/konka/folder/OpenAction;->mItemClickListener:Lcom/konka/folder/OpenAction$OnActionItemClickListener;
    invoke-static {v0}, Lcom/konka/folder/OpenAction;->access$0(Lcom/konka/folder/OpenAction;)Lcom/konka/folder/OpenAction$OnActionItemClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/folder/OpenAction$1;->this$0:Lcom/konka/folder/OpenAction;

    # getter for: Lcom/konka/folder/OpenAction;->mItemClickListener:Lcom/konka/folder/OpenAction$OnActionItemClickListener;
    invoke-static {v0}, Lcom/konka/folder/OpenAction;->access$0(Lcom/konka/folder/OpenAction;)Lcom/konka/folder/OpenAction$OnActionItemClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/folder/OpenAction$1;->this$0:Lcom/konka/folder/OpenAction;

    iget v2, p0, Lcom/konka/folder/OpenAction$1;->val$pos:I

    iget v3, p0, Lcom/konka/folder/OpenAction$1;->val$actionId:I

    invoke-interface {v0, v1, v2, v3}, Lcom/konka/folder/OpenAction$OnActionItemClickListener;->onItemClick(Lcom/konka/folder/OpenAction;II)V

    :cond_0
    iget-object v0, p0, Lcom/konka/folder/OpenAction$1;->this$0:Lcom/konka/folder/OpenAction;

    iget v1, p0, Lcom/konka/folder/OpenAction$1;->val$pos:I

    invoke-virtual {v0, v1}, Lcom/konka/folder/OpenAction;->getActionItem(I)Lnet/londatiga/android/ActionItem;

    move-result-object v0

    invoke-virtual {v0}, Lnet/londatiga/android/ActionItem;->isSticky()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/konka/folder/OpenAction$1;->this$0:Lcom/konka/folder/OpenAction;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/konka/folder/OpenAction;->access$1(Lcom/konka/folder/OpenAction;Z)V

    iget-object v0, p0, Lcom/konka/folder/OpenAction$1;->this$0:Lcom/konka/folder/OpenAction;

    invoke-virtual {v0}, Lcom/konka/folder/OpenAction;->dismiss()V

    :cond_1
    return-void
.end method
