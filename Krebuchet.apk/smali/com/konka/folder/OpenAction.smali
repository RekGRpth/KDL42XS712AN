.class public Lcom/konka/folder/OpenAction;
.super Lnet/londatiga/android/ArrowedPopupWindow;
.source "OpenAction.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/folder/OpenAction$OnActionItemClickListener;
    }
.end annotation


# static fields
.field public static final HORIZONTAL:I = 0x0

.field public static final VERTICAL:I = 0x1


# instance fields
.field private actionItems:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/londatiga/android/ActionItem;",
            ">;"
        }
    .end annotation
.end field

.field private mChildPos:I

.field private mDidAction:Z

.field private mInsertPos:I

.field private mItemClickListener:Lcom/konka/folder/OpenAction$OnActionItemClickListener;

.field private mOrientation:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/konka/folder/OpenAction;-><init>(Landroid/content/Context;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I

    const/4 v0, 0x5

    invoke-direct {p0, p1, p2, v0}, Lcom/konka/folder/OpenAction;-><init>(Landroid/content/Context;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # I

    invoke-direct {p0, p1, p3}, Lnet/londatiga/android/ArrowedPopupWindow;-><init>(Landroid/content/Context;I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/konka/folder/OpenAction;->actionItems:Ljava/util/List;

    if-nez p2, :cond_0

    const v0, 0x7f030011    # com.konka.avenger.R.layout.folderaction_popup_horizontal

    invoke-virtual {p0, v0}, Lcom/konka/folder/OpenAction;->setContentView(I)V

    :goto_0
    iput p2, p0, Lcom/konka/folder/OpenAction;->mOrientation:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/folder/OpenAction;->mChildPos:I

    return-void

    :cond_0
    const v0, 0x7f030024    # com.konka.avenger.R.layout.quickaction_popup_vertical

    invoke-virtual {p0, v0}, Lcom/konka/folder/OpenAction;->setContentView(I)V

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/konka/folder/OpenAction;)Lcom/konka/folder/OpenAction$OnActionItemClickListener;
    .locals 1

    iget-object v0, p0, Lcom/konka/folder/OpenAction;->mItemClickListener:Lcom/konka/folder/OpenAction$OnActionItemClickListener;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/folder/OpenAction;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/folder/OpenAction;->mDidAction:Z

    return-void
.end method


# virtual methods
.method public addActionItem(Lnet/londatiga/android/ActionItem;)V
    .locals 14
    .param p1    # Lnet/londatiga/android/ActionItem;

    const/4 v13, 0x5

    const/4 v12, 0x0

    const/4 v11, 0x1

    const/4 v9, 0x0

    iget-object v8, p0, Lcom/konka/folder/OpenAction;->actionItems:Ljava/util/List;

    invoke-interface {v8, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Lnet/londatiga/android/ActionItem;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1}, Lnet/londatiga/android/ActionItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iget v8, p0, Lcom/konka/folder/OpenAction;->mOrientation:I

    if-nez v8, :cond_2

    iget-object v8, p0, Lcom/konka/folder/OpenAction;->mInflater:Landroid/view/LayoutInflater;

    const v10, 0x7f030021    # com.konka.avenger.R.layout.quickaction_item_horizontal

    invoke-virtual {v8, v10, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    :goto_0
    move-object v6, v1

    check-cast v6, Landroid/widget/TextView;

    if-eqz v2, :cond_0

    iget v8, p0, Lcom/konka/folder/OpenAction;->mOrientation:I

    if-ne v8, v11, :cond_3

    move-object v8, v2

    :goto_1
    iget v10, p0, Lcom/konka/folder/OpenAction;->mOrientation:I

    if-nez v10, :cond_4

    :goto_2
    invoke-virtual {v6, v8, v2, v9, v9}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    :cond_0
    if-eqz v7, :cond_5

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_3
    iget v4, p0, Lcom/konka/folder/OpenAction;->mChildPos:I

    invoke-virtual {p1}, Lnet/londatiga/android/ActionItem;->getActionId()I

    move-result v0

    new-instance v8, Lcom/konka/folder/OpenAction$1;

    invoke-direct {v8, p0, v4, v0}, Lcom/konka/folder/OpenAction$1;-><init>(Lcom/konka/folder/OpenAction;II)V

    invoke-virtual {v1, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1, v11}, Landroid/view/View;->setFocusable(Z)V

    invoke-virtual {v1, v11}, Landroid/view/View;->setClickable(Z)V

    iget v8, p0, Lcom/konka/folder/OpenAction;->mOrientation:I

    if-nez v8, :cond_1

    iget v8, p0, Lcom/konka/folder/OpenAction;->mChildPos:I

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/konka/folder/OpenAction;->mInflater:Landroid/view/LayoutInflater;

    const v10, 0x7f030020    # com.konka.avenger.R.layout.quickaction_horiz_separator

    invoke-virtual {v8, v10, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v8, -0x2

    const/4 v9, -0x1

    invoke-direct {v3, v8, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v5, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v5, v13, v12, v13, v12}, Landroid/view/View;->setPadding(IIII)V

    iget-object v8, p0, Lcom/konka/folder/OpenAction;->mContentView:Landroid/view/ViewGroup;

    iget v9, p0, Lcom/konka/folder/OpenAction;->mInsertPos:I

    invoke-virtual {v8, v5, v9}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    iget v8, p0, Lcom/konka/folder/OpenAction;->mInsertPos:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/konka/folder/OpenAction;->mInsertPos:I

    :cond_1
    iget-object v8, p0, Lcom/konka/folder/OpenAction;->mContentView:Landroid/view/ViewGroup;

    iget v9, p0, Lcom/konka/folder/OpenAction;->mInsertPos:I

    invoke-virtual {v8, v1, v9}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    iget v8, p0, Lcom/konka/folder/OpenAction;->mChildPos:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/konka/folder/OpenAction;->mChildPos:I

    iget v8, p0, Lcom/konka/folder/OpenAction;->mInsertPos:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/konka/folder/OpenAction;->mInsertPos:I

    return-void

    :cond_2
    iget-object v8, p0, Lcom/konka/folder/OpenAction;->mInflater:Landroid/view/LayoutInflater;

    const v10, 0x7f030022    # com.konka.avenger.R.layout.quickaction_item_vertical

    invoke-virtual {v8, v10, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    :cond_3
    move-object v8, v9

    goto :goto_1

    :cond_4
    move-object v2, v9

    goto :goto_2

    :cond_5
    const/16 v8, 0x8

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3
.end method

.method public getActionItem(I)Lnet/londatiga/android/ActionItem;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/folder/OpenAction;->actionItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/londatiga/android/ActionItem;

    return-object v0
.end method

.method public onDismiss()V
    .locals 1

    iget-boolean v0, p0, Lcom/konka/folder/OpenAction;->mDidAction:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/folder/OpenAction;->mDismissListener:Lnet/londatiga/android/ArrowedPopupWindow$OnDismissListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/folder/OpenAction;->mDismissListener:Lnet/londatiga/android/ArrowedPopupWindow$OnDismissListener;

    invoke-interface {v0}, Lnet/londatiga/android/ArrowedPopupWindow$OnDismissListener;->onDismiss()V

    :cond_0
    return-void
.end method

.method public setOnActionItemClickListener(Lcom/konka/folder/OpenAction$OnActionItemClickListener;)V
    .locals 0
    .param p1    # Lcom/konka/folder/OpenAction$OnActionItemClickListener;

    iput-object p1, p0, Lcom/konka/folder/OpenAction;->mItemClickListener:Lcom/konka/folder/OpenAction$OnActionItemClickListener;

    return-void
.end method
