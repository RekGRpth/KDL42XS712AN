.class public final enum Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;
.super Ljava/lang/Enum;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkinterface/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EN_CI_FUNCTION"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_APPINFO:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_APPMMI:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_AUTH:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_CAS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_CC:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_CU:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_DEBUG_COUNT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_DEFAULT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_DT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_HC:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_HLC:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_HSS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_LSC:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_MMI:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_OP:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_PMT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_RM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

.field public static final enum EN_CI_FUNCTION_SAS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_RM"

    invoke-direct {v0, v1, v3}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_RM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_APPINFO"

    invoke-direct {v0, v1, v4}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_APPINFO:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_CAS"

    invoke-direct {v0, v1, v5}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_CAS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_HC"

    invoke-direct {v0, v1, v6}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_HC:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_DT"

    invoke-direct {v0, v1, v7}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_DT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_MMI"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_MMI:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_LSC"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_LSC:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_CC"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_CC:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_HLC"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_HLC:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_CU"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_CU:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_OP"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_OP:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_SAS"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_SAS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_APPMMI"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_APPMMI:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_PMT"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_PMT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_HSS"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_HSS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_AUTH"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_AUTH:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_DEFAULT"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_DEFAULT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    new-instance v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    const-string v1, "EN_CI_FUNCTION_DEBUG_COUNT"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_DEBUG_COUNT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    const/16 v0, 0x12

    new-array v0, v0, [Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_RM:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_APPINFO:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_CAS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v1, v0, v5

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_HC:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v1, v0, v6

    sget-object v1, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_DT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_MMI:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_LSC:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_CC:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_HLC:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_CU:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_OP:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_SAS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_APPMMI:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_PMT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_HSS:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_AUTH:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_DEFAULT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->EN_CI_FUNCTION_DEBUG_COUNT:Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    aput-object v2, v0, v1

    sput-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->ENUM$VALUES:[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;
    .locals 1

    const-class v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    return-object v0
.end method

.method public static values()[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;->ENUM$VALUES:[Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/kkinterface/tv/DataBaseDesk$EN_CI_FUNCTION;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
