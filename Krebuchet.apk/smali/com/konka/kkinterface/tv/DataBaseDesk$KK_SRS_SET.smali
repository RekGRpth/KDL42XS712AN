.class public Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;
.super Ljava/lang/Object;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkinterface/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "KK_SRS_SET"
.end annotation


# instance fields
.field public srs_DCControl:I

.field public srs_DefinitionControl:I

.field public srs_InputGain:I

.field public srs_SpeakerAnalysis:I

.field public srs_SpeakerAudio:I

.field public srs_SurrLevelControl:I

.field public srs_TrubassControl:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x7

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x6

    iput v0, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_InputGain:I

    iput v1, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_SurrLevelControl:I

    const/16 v0, 0x9

    iput v0, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_SpeakerAudio:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_SpeakerAnalysis:I

    const/4 v0, 0x5

    iput v0, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_TrubassControl:I

    iput v1, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_DCControl:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/konka/kkinterface/tv/DataBaseDesk$KK_SRS_SET;->srs_DefinitionControl:I

    return-void
.end method
