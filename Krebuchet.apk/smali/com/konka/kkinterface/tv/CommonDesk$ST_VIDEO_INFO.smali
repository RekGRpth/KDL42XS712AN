.class public Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;
.super Ljava/lang/Object;
.source "CommonDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/kkinterface/tv/CommonDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ST_VIDEO_INFO"
.end annotation


# instance fields
.field public enScanType:Lcom/konka/kkinterface/tv/CommonDesk$EN_SCAN_TYPE;

.field public s16FrameRate:S

.field public s16HResolution:S

.field public s16ModeIndex:S

.field public s16VResolution:S


# direct methods
.method public constructor <init>(SSSSLcom/konka/kkinterface/tv/CommonDesk$EN_SCAN_TYPE;)V
    .locals 0
    .param p1    # S
    .param p2    # S
    .param p3    # S
    .param p4    # S
    .param p5    # Lcom/konka/kkinterface/tv/CommonDesk$EN_SCAN_TYPE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-short p1, p0, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16HResolution:S

    iput-short p2, p0, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16VResolution:S

    iput-short p3, p0, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16FrameRate:S

    iput-object p5, p0, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->enScanType:Lcom/konka/kkinterface/tv/CommonDesk$EN_SCAN_TYPE;

    iput-short p4, p0, Lcom/konka/kkinterface/tv/CommonDesk$ST_VIDEO_INFO;->s16ModeIndex:S

    return-void
.end method
