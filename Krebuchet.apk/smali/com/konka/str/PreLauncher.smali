.class public Lcom/konka/str/PreLauncher;
.super Landroid/app/Activity;
.source "PreLauncher.java"


# instance fields
.field public final FLAG_CLOSE:I

.field public final FLAG_OPEN:I

.field private handler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/konka/str/PreLauncher;->FLAG_OPEN:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/str/PreLauncher;->FLAG_CLOSE:I

    new-instance v0, Lcom/konka/str/PreLauncher$1;

    invoke-direct {v0, p0}, Lcom/konka/str/PreLauncher$1;-><init>(Lcom/konka/str/PreLauncher;)V

    iput-object v0, p0, Lcom/konka/str/PreLauncher;->handler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/str/PreLauncher;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/str/PreLauncher;->open()V

    return-void
.end method

.method private open()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/konka/str/PreLauncher;->showDialog(I)V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/konka/str/PreLauncher;->removeDialog(I)V

    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0a00b6    # com.konka.avenger.R.string.str_dialog_title

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    invoke-virtual {p0}, Lcom/konka/str/PreLauncher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00b7    # com.konka.avenger.R.string.str_dialog_mes

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    iget-object v0, p0, Lcom/konka/str/PreLauncher;->handler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-object v0, p0, Lcom/konka/str/PreLauncher;->handler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method
