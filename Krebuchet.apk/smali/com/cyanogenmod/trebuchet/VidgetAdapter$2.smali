.class Lcom/cyanogenmod/trebuchet/VidgetAdapter$2;
.super Ljava/lang/Object;
.source "VidgetAdapter.java"

# interfaces
.implements Lcom/cyanogenmod/trebuchet/AsyncTaskCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cyanogenmod/trebuchet/VidgetAdapter;->prepareLoadWidgetPreviewsTask(ILjava/util/ArrayList;IIILcom/cyanogenmod/trebuchet/PagedDialogGridView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/VidgetAdapter;

.field private final synthetic val$currentView:Lcom/cyanogenmod/trebuchet/PagedDialogGridView;


# direct methods
.method constructor <init>(Lcom/cyanogenmod/trebuchet/VidgetAdapter;Lcom/cyanogenmod/trebuchet/PagedDialogGridView;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter$2;->this$0:Lcom/cyanogenmod/trebuchet/VidgetAdapter;

    iput-object p2, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter$2;->val$currentView:Lcom/cyanogenmod/trebuchet/PagedDialogGridView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run(Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;)V
    .locals 2
    .param p1    # Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;
    .param p2    # Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;

    :try_start_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter$2;->this$0:Lcom/cyanogenmod/trebuchet/VidgetAdapter;

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mRunningTasks:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->isCancelled()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->isCancelled()Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;->cleanup(Z)V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter$2;->this$0:Lcom/cyanogenmod/trebuchet/VidgetAdapter;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter$2;->val$currentView:Lcom/cyanogenmod/trebuchet/PagedDialogGridView;

    # invokes: Lcom/cyanogenmod/trebuchet/VidgetAdapter;->onSyncWidgetPageItems(Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;Lcom/cyanogenmod/trebuchet/PagedDialogGridView;)V
    invoke-static {v0, p2, v1}, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->access$1(Lcom/cyanogenmod/trebuchet/VidgetAdapter;Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;Lcom/cyanogenmod/trebuchet/PagedDialogGridView;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->isCancelled()Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;->cleanup(Z)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->isCancelled()Z

    move-result v1

    invoke-virtual {p2, v1}, Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;->cleanup(Z)V

    throw v0
.end method
