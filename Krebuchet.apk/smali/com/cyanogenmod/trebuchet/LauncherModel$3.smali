.class Lcom/cyanogenmod/trebuchet/LauncherModel$3;
.super Ljava/lang/Object;
.source "LauncherModel.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cyanogenmod/trebuchet/LauncherModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/cyanogenmod/trebuchet/ApplicationInfo;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Lcom/cyanogenmod/trebuchet/ApplicationInfo;Lcom/cyanogenmod/trebuchet/ApplicationInfo;)I
    .locals 4
    .param p1    # Lcom/cyanogenmod/trebuchet/ApplicationInfo;
    .param p2    # Lcom/cyanogenmod/trebuchet/ApplicationInfo;

    iget v1, p1, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->sortOrderID:I

    iget v2, p2, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->sortOrderID:I

    if-le v1, v2, :cond_1

    const/4 v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p1, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->sortOrderID:I

    iget v2, p2, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->sortOrderID:I

    if-ge v1, v2, :cond_2

    const/4 v0, -0x1

    goto :goto_0

    :cond_2
    iget v1, p1, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->sortOrderID:I

    iget v2, p2, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->sortOrderID:I

    if-ne v1, v2, :cond_3

    iget v1, p1, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->sortOrderID:I

    const/16 v2, 0x63

    if-ne v1, v2, :cond_3

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->sCollator:Ljava/text/Collator;
    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$0()Ljava/text/Collator;

    move-result-object v1

    iget-object v2, p1, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->title:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p2, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->title:Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v1, p1, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->componentName:Landroid/content/ComponentName;

    iget-object v2, p2, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->componentName:Landroid/content/ComponentName;

    invoke-virtual {v1, v2}, Landroid/content/ComponentName;->compareTo(Landroid/content/ComponentName;)I

    move-result v0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/cyanogenmod/trebuchet/ApplicationInfo;

    check-cast p2, Lcom/cyanogenmod/trebuchet/ApplicationInfo;

    invoke-virtual {p0, p1, p2}, Lcom/cyanogenmod/trebuchet/LauncherModel$3;->compare(Lcom/cyanogenmod/trebuchet/ApplicationInfo;Lcom/cyanogenmod/trebuchet/ApplicationInfo;)I

    move-result v0

    return v0
.end method
