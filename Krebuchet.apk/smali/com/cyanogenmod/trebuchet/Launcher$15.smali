.class Lcom/cyanogenmod/trebuchet/Launcher$15;
.super Lcom/cyanogenmod/trebuchet/LauncherAnimatorUpdateListener;
.source "Launcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cyanogenmod/trebuchet/Launcher;->showAppsCustomizeHelper(ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/Launcher;

.field private final synthetic val$scale:F

.field private final synthetic val$toView:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/cyanogenmod/trebuchet/Launcher;Landroid/view/View;F)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Launcher$15;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    iput-object p2, p0, Lcom/cyanogenmod/trebuchet/Launcher$15;->val$toView:Landroid/view/View;

    iput p3, p0, Lcom/cyanogenmod/trebuchet/Launcher$15;->val$scale:F

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/LauncherAnimatorUpdateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(FF)V
    .locals 4
    .param p1    # F
    .param p2    # F

    const/high16 v3, 0x3f800000    # 1.0f

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher$15;->val$toView:Landroid/view/View;

    iget v1, p0, Lcom/cyanogenmod/trebuchet/Launcher$15;->val$scale:F

    mul-float/2addr v1, p1

    mul-float v2, p2, v3

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setScaleX(F)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher$15;->val$toView:Landroid/view/View;

    iget v1, p0, Lcom/cyanogenmod/trebuchet/Launcher$15;->val$scale:F

    mul-float/2addr v1, p1

    mul-float v2, p2, v3

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setScaleY(F)V

    return-void
.end method
