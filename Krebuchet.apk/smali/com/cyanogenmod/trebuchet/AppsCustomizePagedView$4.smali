.class Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$4;
.super Ljava/lang/Object;
.source "AppsCustomizePagedView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->onTabChanged(Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;

.field private final synthetic val$duration:I

.field private final synthetic val$type:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;


# direct methods
.method constructor <init>(Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;I)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$4;->this$0:Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;

    iput-object p2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$4;->val$type:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    iput p3, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$4;->val$duration:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$4;)Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$4;->this$0:Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 14

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$4;->this$0:Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;

    invoke-virtual {v10}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getMeasuredWidth()I

    move-result v10

    if-lez v10, :cond_0

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$4;->this$0:Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;

    invoke-virtual {v10}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getMeasuredHeight()I

    move-result v10

    if-gtz v10, :cond_1

    :cond_0
    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$4;->this$0:Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;

    invoke-virtual {v10}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->reloadCurrentPage()V

    :goto_0
    return-void

    :cond_1
    const/4 v10, 0x2

    new-array v8, v10, [I

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$4;->this$0:Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;

    invoke-virtual {v10, v8}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getVisiblePages([I)V

    const/4 v10, 0x0

    aget v10, v8, v10

    const/4 v11, -0x1

    if-ne v10, v11, :cond_2

    const/4 v10, 0x1

    aget v10, v8, v10

    const/4 v11, -0x1

    if-ne v10, v11, :cond_2

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$4;->this$0:Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;

    invoke-virtual {v10}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->reloadCurrentPage()V

    goto :goto_0

    :cond_2
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    const/4 v10, 0x0

    aget v3, v8, v10

    :goto_1
    const/4 v10, 0x1

    aget v10, v8, v10

    if-le v3, v10, :cond_3

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$4;->this$0:Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;

    # invokes: Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getTabHost()Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;
    invoke-static {v10}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->access$2(Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;)Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    move-result-object v10

    const v11, 0x7f0d001a    # com.konka.avenger.R.id.animation_buffer

    invoke-virtual {v10, v11}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$4;->this$0:Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;

    # invokes: Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getTabHost()Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;
    invoke-static {v10}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->access$2(Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;)Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    move-result-object v10

    const v11, 0x7f0d0019    # com.konka.avenger.R.id.apps_customize_pane_content

    invoke-virtual {v10, v11}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$4;->this$0:Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;

    invoke-virtual {v10}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getScrollX()I

    move-result v10

    const/4 v11, 0x0

    invoke-virtual {v1, v10, v11}, Landroid/widget/FrameLayout;->scrollTo(II)V

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v10

    add-int/lit8 v3, v10, -0x1

    :goto_2
    if-gez v3, :cond_4

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$4;->this$0:Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->hideScrollingIndicator(Z)V

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$4;->this$0:Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;

    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$4;->val$type:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    invoke-virtual {v10, v11}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->setContentType(Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;)V

    const-string v10, "alpha"

    const/4 v11, 0x1

    new-array v11, v11, [F

    const/4 v12, 0x0

    const/4 v13, 0x0

    aput v13, v11, v12

    invoke-static {v1, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    new-instance v10, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$4$1;

    invoke-direct {v10, p0, v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$4$1;-><init>(Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$4;Landroid/widget/FrameLayout;)V

    invoke-virtual {v5, v10}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    const-string v10, "alpha"

    const/4 v11, 0x1

    new-array v11, v11, [F

    const/4 v12, 0x0

    const/high16 v13, 0x3f800000    # 1.0f

    aput v13, v11, v12

    invoke-static {v7, v10, v11}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    new-instance v10, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$4$2;

    invoke-direct {v10, p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$4$2;-><init>(Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$4;)V

    invoke-virtual {v4, v10}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    const/4 v10, 0x2

    new-array v10, v10, [Landroid/animation/Animator;

    const/4 v11, 0x0

    aput-object v5, v10, v11

    const/4 v11, 0x1

    aput-object v4, v10, v11

    invoke-virtual {v0, v10}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    iget v10, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$4;->val$duration:I

    int-to-long v10, v10

    invoke-virtual {v0, v10, v11}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto/16 :goto_0

    :cond_3
    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$4;->this$0:Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;

    invoke-virtual {v10, v3}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    :cond_4
    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    instance-of v10, v2, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;

    if-eqz v10, :cond_6

    move-object v10, v2

    check-cast v10, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;

    invoke-virtual {v10}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->resetChildrenOnKeyListeners()V

    :cond_5
    :goto_3
    const/4 v10, 0x0

    invoke-static {v10}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->setDeletePreviewsWhenDetachedFromWindow(Z)V

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView$4;->this$0:Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;

    invoke-virtual {v10, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;->removeView(Landroid/view/View;)V

    const/4 v10, 0x1

    invoke-static {v10}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->setDeletePreviewsWhenDetachedFromWindow(Z)V

    const/high16 v10, 0x3f800000    # 1.0f

    invoke-virtual {v1, v10}, Landroid/widget/FrameLayout;->setAlpha(F)V

    const/4 v10, 0x0

    invoke-virtual {v1, v10}, Landroid/widget/FrameLayout;->setVisibility(I)V

    new-instance v6, Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v10

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v11

    invoke-direct {v6, v10, v11}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v10

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v6, v10, v11, v12, v13}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {v1, v2, v6}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v3, v3, -0x1

    goto/16 :goto_2

    :cond_6
    instance-of v10, v2, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;

    if-eqz v10, :cond_5

    move-object v10, v2

    check-cast v10, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;

    invoke-virtual {v10}, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;->resetChildrenOnKeyListeners()V

    goto :goto_3
.end method
