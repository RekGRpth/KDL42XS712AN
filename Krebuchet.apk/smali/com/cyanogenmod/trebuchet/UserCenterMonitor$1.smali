.class Lcom/cyanogenmod/trebuchet/UserCenterMonitor$1;
.super Ljava/lang/Object;
.source "UserCenterMonitor.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cyanogenmod/trebuchet/UserCenterMonitor;-><init>(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/UserCenterMonitor$IUserCenterUpdateListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/UserCenterMonitor;


# direct methods
.method constructor <init>(Lcom/cyanogenmod/trebuchet/UserCenterMonitor;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/UserCenterMonitor$1;->this$0:Lcom/cyanogenmod/trebuchet/UserCenterMonitor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public declared-synchronized onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/UserCenterMonitor$1;->this$0:Lcom/cyanogenmod/trebuchet/UserCenterMonitor;

    invoke-static {p2}, Lcom/konka/passport/service/UserInfo$Stub;->asInterface(Landroid/os/IBinder;)Lcom/konka/passport/service/UserInfo;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/cyanogenmod/trebuchet/UserCenterMonitor;->access$2(Lcom/cyanogenmod/trebuchet/UserCenterMonitor;Lcom/konka/passport/service/UserInfo;)V

    new-instance v0, Lcom/cyanogenmod/trebuchet/UserCenterMonitor$QueryTask;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/UserCenterMonitor$1;->this$0:Lcom/cyanogenmod/trebuchet/UserCenterMonitor;

    invoke-direct {v0, v1}, Lcom/cyanogenmod/trebuchet/UserCenterMonitor$QueryTask;-><init>(Lcom/cyanogenmod/trebuchet/UserCenterMonitor;)V

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/konka/passport/service/UserInfo;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/UserCenterMonitor$1;->this$0:Lcom/cyanogenmod/trebuchet/UserCenterMonitor;

    # getter for: Lcom/cyanogenmod/trebuchet/UserCenterMonitor;->mUserInfo:Lcom/konka/passport/service/UserInfo;
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/UserCenterMonitor;->access$3(Lcom/cyanogenmod/trebuchet/UserCenterMonitor;)Lcom/konka/passport/service/UserInfo;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/UserCenterMonitor$QueryTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/UserCenterMonitor$1;->this$0:Lcom/cyanogenmod/trebuchet/UserCenterMonitor;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/cyanogenmod/trebuchet/UserCenterMonitor;->access$2(Lcom/cyanogenmod/trebuchet/UserCenterMonitor;Lcom/konka/passport/service/UserInfo;)V

    return-void
.end method
