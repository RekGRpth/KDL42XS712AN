.class Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;
.super Ljava/lang/Object;
.source "Launcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cyanogenmod/trebuchet/Launcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RenameFolder"
.end annotation


# instance fields
.field private mInput:Landroid/widget/EditText;

.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/Launcher;


# direct methods
.method private constructor <init>(Lcom/cyanogenmod/trebuchet/Launcher;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/cyanogenmod/trebuchet/Launcher;Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;)V

    return-void
.end method

.method static synthetic access$0(Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;)V
    .locals 0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;->cleanup()V

    return-void
.end method

.method static synthetic access$1(Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;)V
    .locals 0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;->changeFolderName()V

    return-void
.end method

.method static synthetic access$2(Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;->mInput:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$4(Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;)Lcom/cyanogenmod/trebuchet/Launcher;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    return-object v0
.end method

.method private changeFolderName()V
    .locals 7

    const/4 v6, 0x0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;->mInput:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # getter for: Lcom/cyanogenmod/trebuchet/Launcher;->sFolders:Ljava/util/HashMap;
    invoke-static {}, Lcom/cyanogenmod/trebuchet/Launcher;->access$12()Ljava/util/HashMap;

    move-result-object v2

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # getter for: Lcom/cyanogenmod/trebuchet/Launcher;->mFolderInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;
    invoke-static {v4}, Lcom/cyanogenmod/trebuchet/Launcher;->access$13(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/cyanogenmod/trebuchet/FolderInfo;

    move-result-object v4

    iget-wide v4, v4, Lcom/cyanogenmod/trebuchet/FolderInfo;->id:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cyanogenmod/trebuchet/FolderInfo;

    invoke-static {v3, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->access$14(Lcom/cyanogenmod/trebuchet/Launcher;Lcom/cyanogenmod/trebuchet/FolderInfo;)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # getter for: Lcom/cyanogenmod/trebuchet/Launcher;->mFolderInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->access$13(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/cyanogenmod/trebuchet/FolderInfo;

    move-result-object v2

    iput-object v1, v2, Lcom/cyanogenmod/trebuchet/FolderInfo;->title:Ljava/lang/CharSequence;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # getter for: Lcom/cyanogenmod/trebuchet/Launcher;->mFolderInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/Launcher;->access$13(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/cyanogenmod/trebuchet/FolderInfo;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/cyanogenmod/trebuchet/LauncherModel;->updateItemInDatabase(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/ItemInfo;)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # getter for: Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspaceLoading:Z
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->access$15(Lcom/cyanogenmod/trebuchet/Launcher;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->lockAllApps()V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # getter for: Lcom/cyanogenmod/trebuchet/Launcher;->mModel:Lcom/cyanogenmod/trebuchet/LauncherModel;
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->access$16(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/cyanogenmod/trebuchet/LauncherModel;

    move-result-object v2

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v2, v3, v6}, Lcom/cyanogenmod/trebuchet/LauncherModel;->startLoader(Landroid/content/Context;Z)V

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;->cleanup()V

    return-void

    :cond_1
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # getter for: Lcom/cyanogenmod/trebuchet/Launcher;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->access$0(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/cyanogenmod/trebuchet/Workspace;

    move-result-object v2

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # getter for: Lcom/cyanogenmod/trebuchet/Launcher;->mFolderInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/Launcher;->access$13(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/cyanogenmod/trebuchet/FolderInfo;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/cyanogenmod/trebuchet/Workspace;->getViewForTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/FolderIcon;

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->getWorkspace()Lcom/cyanogenmod/trebuchet/Workspace;

    move-result-object v2

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Workspace;->requestLayout()V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->lockAllApps()V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/cyanogenmod/trebuchet/Launcher;->access$17(Lcom/cyanogenmod/trebuchet/Launcher;Z)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # getter for: Lcom/cyanogenmod/trebuchet/Launcher;->mModel:Lcom/cyanogenmod/trebuchet/LauncherModel;
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->access$16(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/cyanogenmod/trebuchet/LauncherModel;

    move-result-object v2

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v2, v3, v6}, Lcom/cyanogenmod/trebuchet/LauncherModel;->startLoader(Landroid/content/Context;Z)V

    goto :goto_0
.end method

.method private cleanup()V
    .locals 2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->dismissDialog(I)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->access$11(Lcom/cyanogenmod/trebuchet/Launcher;Z)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->access$14(Lcom/cyanogenmod/trebuchet/Launcher;Lcom/cyanogenmod/trebuchet/FolderInfo;)V

    return-void
.end method


# virtual methods
.method createDialog()Landroid/app/Dialog;
    .locals 6

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    const v4, 0x7f030025    # com.konka.avenger.R.layout.rename_folder

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    const v3, 0x7f0d005d    # com.konka.avenger.R.id.folder_name

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;->mInput:Landroid/widget/EditText;

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    const v4, 0x7f0a0018    # com.konka.avenger.R.string.rename_folder_title

    invoke-virtual {v3, v4}, Lcom/cyanogenmod/trebuchet/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    new-instance v3, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder$1;

    invoke-direct {v3, p0}, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder$1;-><init>(Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;)V

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    const v4, 0x7f0a001a    # com.konka.avenger.R.string.cancel_action

    invoke-virtual {v3, v4}, Lcom/cyanogenmod/trebuchet/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder$2;

    invoke-direct {v4, p0}, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder$2;-><init>(Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    const v4, 0x7f0a0019    # com.konka.avenger.R.string.rename_action

    invoke-virtual {v3, v4}, Lcom/cyanogenmod/trebuchet/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder$3;

    invoke-direct {v4, p0}, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder$3;-><init>(Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;)V

    invoke-virtual {v0, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    new-instance v3, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder$4;

    invoke-direct {v3, p0}, Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder$4;-><init>(Lcom/cyanogenmod/trebuchet/Launcher$RenameFolder;)V

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    return-object v1
.end method
