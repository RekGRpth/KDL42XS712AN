.class public Lcom/cyanogenmod/trebuchet/SpringLoadedDragController;
.super Ljava/lang/Object;
.source "SpringLoadedDragController.java"

# interfaces
.implements Lcom/cyanogenmod/trebuchet/OnAlarmListener;


# instance fields
.field final ENTER_SPRING_LOAD_CANCEL_HOVER_TIME:J

.field final ENTER_SPRING_LOAD_HOVER_TIME:J

.field mAlarm:Lcom/cyanogenmod/trebuchet/Alarm;

.field private mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

.field private mScreen:Lcom/cyanogenmod/trebuchet/CellLayout;


# direct methods
.method public constructor <init>(Lcom/cyanogenmod/trebuchet/Launcher;)V
    .locals 2
    .param p1    # Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x226

    iput-wide v0, p0, Lcom/cyanogenmod/trebuchet/SpringLoadedDragController;->ENTER_SPRING_LOAD_HOVER_TIME:J

    const-wide/16 v0, 0x3b6

    iput-wide v0, p0, Lcom/cyanogenmod/trebuchet/SpringLoadedDragController;->ENTER_SPRING_LOAD_CANCEL_HOVER_TIME:J

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/SpringLoadedDragController;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    new-instance v0, Lcom/cyanogenmod/trebuchet/Alarm;

    invoke-direct {v0}, Lcom/cyanogenmod/trebuchet/Alarm;-><init>()V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/SpringLoadedDragController;->mAlarm:Lcom/cyanogenmod/trebuchet/Alarm;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SpringLoadedDragController;->mAlarm:Lcom/cyanogenmod/trebuchet/Alarm;

    invoke-virtual {v0, p0}, Lcom/cyanogenmod/trebuchet/Alarm;->setOnAlarmListener(Lcom/cyanogenmod/trebuchet/OnAlarmListener;)V

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SpringLoadedDragController;->mAlarm:Lcom/cyanogenmod/trebuchet/Alarm;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Alarm;->cancelAlarm()V

    return-void
.end method

.method public onAlarm(Lcom/cyanogenmod/trebuchet/Alarm;)V
    .locals 3
    .param p1    # Lcom/cyanogenmod/trebuchet/Alarm;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/SpringLoadedDragController;->mScreen:Lcom/cyanogenmod/trebuchet/CellLayout;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/SpringLoadedDragController;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->getWorkspace()Lcom/cyanogenmod/trebuchet/Workspace;

    move-result-object v1

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/SpringLoadedDragController;->mScreen:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v1, v2}, Lcom/cyanogenmod/trebuchet/Workspace;->indexOfChild(Landroid/view/View;)I

    move-result v0

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Workspace;->getCurrentPage()I

    move-result v2

    if-eq v0, v2, :cond_0

    invoke-virtual {v1, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->snapToPage(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/SpringLoadedDragController;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->getDragController()Lcom/cyanogenmod/trebuchet/DragController;

    move-result-object v2

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/DragController;->cancelDrag()V

    goto :goto_0
.end method

.method public setAlarm(Lcom/cyanogenmod/trebuchet/CellLayout;)V
    .locals 3
    .param p1    # Lcom/cyanogenmod/trebuchet/CellLayout;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SpringLoadedDragController;->mAlarm:Lcom/cyanogenmod/trebuchet/Alarm;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Alarm;->cancelAlarm()V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/SpringLoadedDragController;->mAlarm:Lcom/cyanogenmod/trebuchet/Alarm;

    if-nez p1, :cond_0

    const-wide/16 v0, 0x3b6

    :goto_0
    invoke-virtual {v2, v0, v1}, Lcom/cyanogenmod/trebuchet/Alarm;->setAlarm(J)V

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/SpringLoadedDragController;->mScreen:Lcom/cyanogenmod/trebuchet/CellLayout;

    return-void

    :cond_0
    const-wide/16 v0, 0x226

    goto :goto_0
.end method
