.class final Lcom/cyanogenmod/trebuchet/NetworkMonitor$WifiHandler;
.super Landroid/os/Handler;
.source "NetworkMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cyanogenmod/trebuchet/NetworkMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "WifiHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/NetworkMonitor;


# direct methods
.method private constructor <init>(Lcom/cyanogenmod/trebuchet/NetworkMonitor;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor$WifiHandler;->this$0:Lcom/cyanogenmod/trebuchet/NetworkMonitor;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/cyanogenmod/trebuchet/NetworkMonitor;Lcom/cyanogenmod/trebuchet/NetworkMonitor$WifiHandler;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/NetworkMonitor$WifiHandler;-><init>(Lcom/cyanogenmod/trebuchet/NetworkMonitor;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1    # Landroid/os/Message;

    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor$WifiHandler;->this$0:Lcom/cyanogenmod/trebuchet/NetworkMonitor;

    # getter for: Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mWifiChannel:Lcom/android/internal/util/AsyncChannel;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->access$9(Lcom/cyanogenmod/trebuchet/NetworkMonitor;)Lcom/android/internal/util/AsyncChannel;

    move-result-object v0

    const v1, 0x11001

    invoke-static {p0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/internal/util/AsyncChannel;->sendMessage(Landroid/os/Message;)V

    goto :goto_0

    :cond_1
    const-string v0, "Krebuchet.NetworkMonitor"

    const-string v1, "Failed to connect to wifi"

    invoke-static {v0, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :sswitch_1
    iget v0, p1, Landroid/os/Message;->arg1:I

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor$WifiHandler;->this$0:Lcom/cyanogenmod/trebuchet/NetworkMonitor;

    # getter for: Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mWifiActivity:I
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->access$10(Lcom/cyanogenmod/trebuchet/NetworkMonitor;)I

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor$WifiHandler;->this$0:Lcom/cyanogenmod/trebuchet/NetworkMonitor;

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0, v1}, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->access$11(Lcom/cyanogenmod/trebuchet/NetworkMonitor;I)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor$WifiHandler;->this$0:Lcom/cyanogenmod/trebuchet/NetworkMonitor;

    # getter for: Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mListener:Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->access$7(Lcom/cyanogenmod/trebuchet/NetworkMonitor;)Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor$WifiHandler;->this$0:Lcom/cyanogenmod/trebuchet/NetworkMonitor;

    # getter for: Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mListener:Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->access$7(Lcom/cyanogenmod/trebuchet/NetworkMonitor;)Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;

    move-result-object v0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor$WifiHandler;->this$0:Lcom/cyanogenmod/trebuchet/NetworkMonitor;

    # getter for: Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mWifiActivity:I
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->access$10(Lcom/cyanogenmod/trebuchet/NetworkMonitor;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;->onUpdateWifiActivity(I)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x11000 -> :sswitch_0
    .end sparse-switch
.end method
