.class public Lcom/cyanogenmod/trebuchet/DragController;
.super Ljava/lang/Object;
.source "DragController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cyanogenmod/trebuchet/DragController$DragListener;,
        Lcom/cyanogenmod/trebuchet/DragController$ScrollRunnable;
    }
.end annotation


# static fields
.field public static DRAG_ACTION_COPY:I = 0x0

.field public static DRAG_ACTION_MOVE:I = 0x0

.field private static final PROFILE_DRAWING_DURING_DRAG:Z = false

.field private static final SCROLL_DELAY:I = 0x258

.field static final SCROLL_LEFT:I = 0x0

.field static final SCROLL_NONE:I = -0x1

.field private static final SCROLL_OUTSIDE_ZONE:I = 0x0

.field static final SCROLL_RIGHT:I = 0x1

.field private static final SCROLL_WAITING_IN_ZONE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "Launcher.DragController"

.field private static final VIBRATE_DURATION:I = 0x23

.field static final VIRTUAL_MOTION_MOVE:I = 0x1

.field static final VIRTUAL_MOTION_MOVE_CANCEL:I = 0x3

.field static final VIRTUAL_MOTION_MOVE_CONFIRM:I = 0x2

.field static final VIRTUAL_MOTION_PRESS_DOWN:I


# instance fields
.field private final mCoordinatesTemp:[I

.field private mDistanceSinceScroll:I

.field private mDragLayerRect:Landroid/graphics/Rect;

.field private mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

.field private mDragScroller:Lcom/cyanogenmod/trebuchet/DragScroller;

.field private mDragging:Z

.field private mDropTargets:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/DropTarget;",
            ">;"
        }
    .end annotation
.end field

.field private mForceScroll:Z

.field private mHandler:Landroid/os/Handler;

.field private mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

.field private final mIsDebugMoving:Z

.field private mLastDropTarget:Lcom/cyanogenmod/trebuchet/DropTarget;

.field private mLastTouch:[I

.field private mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

.field private mListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/DragController$DragListener;",
            ">;"
        }
    .end annotation
.end field

.field private mMotionDownX:I

.field private mMotionDownY:I

.field private mMoveTarget:Landroid/view/View;

.field private mRectTemp:Landroid/graphics/Rect;

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mScrollRunnable:Lcom/cyanogenmod/trebuchet/DragController$ScrollRunnable;

.field private mScrollState:I

.field private mScrollView:Landroid/view/View;

.field private mScrollZone:I

.field private mTmpPoint:[I

.field private final mVibrator:Landroid/os/Vibrator;

.field private mWindowToken:Landroid/os/IBinder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/cyanogenmod/trebuchet/DragController;->DRAG_ACTION_MOVE:I

    const/4 v0, 0x1

    sput v0, Lcom/cyanogenmod/trebuchet/DragController;->DRAG_ACTION_COPY:I

    return-void
.end method

.method public constructor <init>(Lcom/cyanogenmod/trebuchet/Launcher;)V
    .locals 4
    .param p1    # Lcom/cyanogenmod/trebuchet/Launcher;

    const/4 v3, 0x2

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v2, p0, Lcom/cyanogenmod/trebuchet/DragController;->mIsDebugMoving:Z

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/DragController;->mRectTemp:Landroid/graphics/Rect;

    new-array v1, v3, [I

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/DragController;->mCoordinatesTemp:[I

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDropTargets:Ljava/util/ArrayList;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/DragController;->mListeners:Ljava/util/ArrayList;

    iput v2, p0, Lcom/cyanogenmod/trebuchet/DragController;->mScrollState:I

    new-instance v1, Lcom/cyanogenmod/trebuchet/DragController$ScrollRunnable;

    invoke-direct {v1, p0}, Lcom/cyanogenmod/trebuchet/DragController$ScrollRunnable;-><init>(Lcom/cyanogenmod/trebuchet/DragController;)V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/DragController;->mScrollRunnable:Lcom/cyanogenmod/trebuchet/DragController$ScrollRunnable;

    new-array v1, v3, [I

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/DragController;->mLastTouch:[I

    iput v2, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDistanceSinceScroll:I

    iput-boolean v2, p0, Lcom/cyanogenmod/trebuchet/DragController;->mForceScroll:Z

    new-array v1, v3, [I

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/DragController;->mTmpPoint:[I

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragLayerRect:Landroid/graphics/Rect;

    const-string v1, "vibrator"

    invoke-virtual {p1, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Vibrator;

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/DragController;->mVibrator:Landroid/os/Vibrator;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/DragController;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/DragController;->mHandler:Landroid/os/Handler;

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0035    # com.konka.avenger.R.dimen.scroll_zone

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/DragController;->mScrollZone:I

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/Launcher;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v1, p0, Lcom/cyanogenmod/trebuchet/DragController;->mScreenWidth:I

    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v1, p0, Lcom/cyanogenmod/trebuchet/DragController;->mScreenHeight:I

    return-void
.end method

.method static synthetic access$0(Lcom/cyanogenmod/trebuchet/DragController;)Lcom/cyanogenmod/trebuchet/DragScroller;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragScroller:Lcom/cyanogenmod/trebuchet/DragScroller;

    return-object v0
.end method

.method static synthetic access$1(Lcom/cyanogenmod/trebuchet/DragController;I)V
    .locals 0

    iput p1, p0, Lcom/cyanogenmod/trebuchet/DragController;->mScrollState:I

    return-void
.end method

.method static synthetic access$2(Lcom/cyanogenmod/trebuchet/DragController;I)V
    .locals 0

    iput p1, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDistanceSinceScroll:I

    return-void
.end method

.method static synthetic access$3(Lcom/cyanogenmod/trebuchet/DragController;FF)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/cyanogenmod/trebuchet/DragController;->drop(FF)V

    return-void
.end method

.method static synthetic access$4(Lcom/cyanogenmod/trebuchet/DragController;)V
    .locals 0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/DragController;->endDrag()V

    return-void
.end method

.method private canDrop(FF)Z
    .locals 4
    .param p1    # F
    .param p2    # F

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragController;->mCoordinatesTemp:[I

    float-to-int v2, p1

    float-to-int v3, p2

    invoke-direct {p0, v2, v3, v0}, Lcom/cyanogenmod/trebuchet/DragController;->findDropTarget(II[I)Lcom/cyanogenmod/trebuchet/DropTarget;

    move-result-object v1

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    invoke-interface {v1, v2}, Lcom/cyanogenmod/trebuchet/DropTarget;->acceptDrop(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)Z

    move-result v2

    return v2
.end method

.method private drop(FF)V
    .locals 6
    .param p1    # F
    .param p2    # F

    const/4 v5, 0x1

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/DragController;->mCoordinatesTemp:[I

    float-to-int v3, p1

    float-to-int v4, p2

    invoke-direct {p0, v3, v4, v1}, Lcom/cyanogenmod/trebuchet/DragController;->findDropTarget(II[I)Lcom/cyanogenmod/trebuchet/DropTarget;

    move-result-object v2

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    const/4 v4, 0x0

    aget v4, v1, v4

    iput v4, v3, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->x:I

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    aget v4, v1, v5

    iput v4, v3, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->y:I

    const/4 v0, 0x0

    if-eqz v2, :cond_0

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    iput-boolean v5, v3, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragComplete:Z

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    invoke-interface {v2, v3}, Lcom/cyanogenmod/trebuchet/DropTarget;->onDragExit(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    invoke-interface {v2, v3}, Lcom/cyanogenmod/trebuchet/DropTarget;->acceptDrop(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    invoke-interface {v2, v3}, Lcom/cyanogenmod/trebuchet/DropTarget;->onDrop(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V

    const/4 v0, 0x1

    :cond_0
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    iget-object v3, v3, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragSource:Lcom/cyanogenmod/trebuchet/DragSource;

    check-cast v2, Landroid/view/View;

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    invoke-interface {v3, v2, v4, v0}, Lcom/cyanogenmod/trebuchet/DragSource;->onDropCompleted(Landroid/view/View;Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;Z)V

    return-void
.end method

.method private endDrag()V
    .locals 3

    iget-boolean v1, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragging:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragging:Z

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/DragController;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    iget-object v1, v1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    iget-object v1, v1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/DragView;->remove()V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    :cond_0
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/DragController;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->refreshAddGuide()V

    :cond_1
    return-void

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/DragController$DragListener;

    invoke-interface {v0}, Lcom/cyanogenmod/trebuchet/DragController$DragListener;->onDragEnd()V

    goto :goto_0
.end method

.method private findDropTarget(II[I)Lcom/cyanogenmod/trebuchet/DropTarget;
    .locals 11
    .param p1    # I
    .param p2    # I
    .param p3    # [I

    const/4 v10, 0x1

    const/4 v9, 0x0

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mRectTemp:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDropTargets:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v3, v0, -0x1

    :goto_0
    if-gez v3, :cond_0

    const/4 v5, 0x0

    :goto_1
    return-object v5

    :cond_0
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/cyanogenmod/trebuchet/DropTarget;

    invoke-interface {v5}, Lcom/cyanogenmod/trebuchet/DropTarget;->isDropEnabled()Z

    move-result v6

    if-nez v6, :cond_2

    :cond_1
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    :cond_2
    invoke-interface {v5, v4}, Lcom/cyanogenmod/trebuchet/DropTarget;->getHitRect(Landroid/graphics/Rect;)V

    invoke-interface {v5, p3}, Lcom/cyanogenmod/trebuchet/DropTarget;->getLocationInDragLayer([I)V

    aget v6, p3, v9

    invoke-interface {v5}, Lcom/cyanogenmod/trebuchet/DropTarget;->getLeft()I

    move-result v7

    sub-int/2addr v6, v7

    aget v7, p3, v10

    invoke-interface {v5}, Lcom/cyanogenmod/trebuchet/DropTarget;->getTop()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-virtual {v4, v6, v7}, Landroid/graphics/Rect;->offset(II)V

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    iput p1, v6, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->x:I

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    iput p2, v6, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->y:I

    invoke-virtual {v4, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    invoke-interface {v5, v6}, Lcom/cyanogenmod/trebuchet/DropTarget;->getDropTargetDelegate(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)Lcom/cyanogenmod/trebuchet/DropTarget;

    move-result-object v1

    if-eqz v1, :cond_3

    move-object v5, v1

    invoke-interface {v5, p3}, Lcom/cyanogenmod/trebuchet/DropTarget;->getLocationInDragLayer([I)V

    :cond_3
    aget v6, p3, v9

    sub-int v6, p1, v6

    aput v6, p3, v9

    aget v6, p3, v10

    sub-int v6, p2, v6

    aput v6, p3, v10

    goto :goto_1
.end method

.method private getClampedDragLayerPos(FF)[I
    .locals 4
    .param p1    # F
    .param p2    # F

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragController;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->getDragLayer()Lcom/cyanogenmod/trebuchet/DragLayer;

    move-result-object v0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragLayerRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/DragLayer;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragController;->mTmpPoint:[I

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragLayerRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragLayerRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    invoke-static {p1, v3}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    float-to-int v2, v2

    aput v2, v0, v1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragController;->mTmpPoint:[I

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragLayerRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragLayerRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    invoke-static {p2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    float-to-int v2, v2

    aput v2, v0, v1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragController;->mTmpPoint:[I

    return-object v0
.end method

.method private handleDragViewKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 14
    .param p1    # Landroid/view/KeyEvent;

    const/4 v13, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v10

    if-eq v10, v9, :cond_0

    :goto_0
    return v8

    :cond_0
    new-array v5, v13, [I

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/DragController;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v10}, Lcom/cyanogenmod/trebuchet/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0c000a    # com.konka.avenger.R.dimen.workspace_cell_width

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/DragController;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v11}, Lcom/cyanogenmod/trebuchet/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0c000c    # com.konka.avenger.R.dimen.workspace_width_gap

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    add-int v1, v10, v11

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/DragController;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v10}, Lcom/cyanogenmod/trebuchet/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0c000b    # com.konka.avenger.R.dimen.workspace_cell_height

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/DragController;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v11}, Lcom/cyanogenmod/trebuchet/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0c000d    # com.konka.avenger.R.dimen.workspace_height_gap

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    add-int v0, v10, v11

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/DragController;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v10}, Lcom/cyanogenmod/trebuchet/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0c0022    # com.konka.avenger.R.dimen.app_icon_size

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    iget-object v10, v10, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    invoke-virtual {v10}, Lcom/cyanogenmod/trebuchet/DragView;->getWidth()I

    move-result v3

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    iget-object v10, v10, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    invoke-virtual {v10}, Lcom/cyanogenmod/trebuchet/DragView;->getHeight()I

    move-result v2

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    iget-object v10, v10, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    invoke-virtual {v10, v5}, Lcom/cyanogenmod/trebuchet/DragView;->getMotionPosition([I)[I

    const/4 v10, 0x3

    aget v8, v5, v8

    aget v11, v5, v9

    invoke-virtual {p0, v10, v8, v11}, Lcom/cyanogenmod/trebuchet/DragController;->onKeyEvent(III)Z

    :cond_1
    :goto_1
    move v8, v9

    goto :goto_0

    :sswitch_1
    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    iget-object v10, v10, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    invoke-virtual {v10, v5}, Lcom/cyanogenmod/trebuchet/DragView;->getMotionPosition([I)[I

    aget v10, v5, v8

    div-int/lit8 v11, v4, 0x2

    add-int/2addr v10, v11

    iget v11, p0, Lcom/cyanogenmod/trebuchet/DragController;->mScreenWidth:I

    if-le v10, v11, :cond_3

    aget v8, v5, v8

    sub-int/2addr v8, v1

    div-int/lit8 v10, v4, 0x2

    add-int v6, v8, v10

    :cond_2
    :goto_2
    aget v7, v5, v9

    invoke-virtual {p0, v9, v6, v7}, Lcom/cyanogenmod/trebuchet/DragController;->onKeyEvent(III)Z

    goto :goto_1

    :cond_3
    aget v10, v5, v8

    div-int/lit8 v11, v4, 0x2

    sub-int/2addr v10, v11

    if-gez v10, :cond_4

    aget v6, v5, v8

    iput-boolean v9, p0, Lcom/cyanogenmod/trebuchet/DragController;->mForceScroll:Z

    goto :goto_2

    :cond_4
    aget v8, v5, v8

    sub-int v6, v8, v1

    if-gez v6, :cond_2

    div-int/lit8 v8, v4, 0x2

    add-int/2addr v6, v8

    goto :goto_2

    :sswitch_2
    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    iget-object v10, v10, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    invoke-virtual {v10, v5}, Lcom/cyanogenmod/trebuchet/DragView;->getMotionPosition([I)[I

    aget v10, v5, v8

    div-int/lit8 v11, v4, 0x2

    add-int/2addr v10, v11

    iget v11, p0, Lcom/cyanogenmod/trebuchet/DragController;->mScreenWidth:I

    if-le v10, v11, :cond_6

    aget v6, v5, v8

    iput-boolean v9, p0, Lcom/cyanogenmod/trebuchet/DragController;->mForceScroll:Z

    :cond_5
    :goto_3
    aget v7, v5, v9

    invoke-virtual {p0, v9, v6, v7}, Lcom/cyanogenmod/trebuchet/DragController;->onKeyEvent(III)Z

    goto :goto_1

    :cond_6
    aget v10, v5, v8

    div-int/lit8 v11, v4, 0x2

    sub-int/2addr v10, v11

    if-gez v10, :cond_7

    aget v8, v5, v8

    add-int/2addr v8, v1

    div-int/lit8 v10, v4, 0x2

    sub-int v6, v8, v10

    goto :goto_3

    :cond_7
    aget v8, v5, v8

    add-int v6, v8, v1

    iget v8, p0, Lcom/cyanogenmod/trebuchet/DragController;->mScreenWidth:I

    if-le v6, v8, :cond_5

    div-int/lit8 v8, v4, 0x2

    sub-int/2addr v6, v8

    goto :goto_3

    :sswitch_3
    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    iget-object v10, v10, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    invoke-virtual {v10, v5}, Lcom/cyanogenmod/trebuchet/DragView;->getMotionPosition([I)[I

    aget v6, v5, v8

    aget v8, v5, v9

    sub-int v7, v8, v0

    if-ltz v7, :cond_1

    invoke-virtual {p0, v9, v6, v7}, Lcom/cyanogenmod/trebuchet/DragController;->onKeyEvent(III)Z

    goto :goto_1

    :sswitch_4
    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    iget-object v10, v10, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    invoke-virtual {v10, v5}, Lcom/cyanogenmod/trebuchet/DragView;->getMotionPosition([I)[I

    aget v6, v5, v8

    aget v8, v5, v9

    add-int v7, v8, v0

    div-int/lit8 v8, v2, 0x2

    add-int/2addr v8, v7

    iget v10, p0, Lcom/cyanogenmod/trebuchet/DragController;->mScreenHeight:I

    if-gt v8, v10, :cond_1

    invoke-virtual {p0, v9, v6, v7}, Lcom/cyanogenmod/trebuchet/DragController;->onKeyEvent(III)Z

    goto/16 :goto_1

    :sswitch_5
    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    iget-object v10, v10, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    invoke-virtual {v10, v5}, Lcom/cyanogenmod/trebuchet/DragView;->getMotionPosition([I)[I

    aget v8, v5, v8

    aget v10, v5, v9

    invoke-virtual {p0, v13, v8, v10}, Lcom/cyanogenmod/trebuchet/DragController;->onKeyEvent(III)Z

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x13 -> :sswitch_3
        0x14 -> :sswitch_4
        0x15 -> :sswitch_1
        0x16 -> :sswitch_2
        0x17 -> :sswitch_5
        0x42 -> :sswitch_5
    .end sparse-switch
.end method

.method private handleMoveEvent(II)V
    .locals 12
    .param p1    # I
    .param p2    # I

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    iget-object v4, v4, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    invoke-virtual {v4, p1, p2}, Lcom/cyanogenmod/trebuchet/DragView;->move(II)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragController;->mCoordinatesTemp:[I

    invoke-direct {p0, p1, p2, v0}, Lcom/cyanogenmod/trebuchet/DragController;->findDropTarget(II[I)Lcom/cyanogenmod/trebuchet/DropTarget;

    move-result-object v2

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    const/4 v5, 0x0

    aget v5, v0, v5

    iput v5, v4, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->x:I

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    const/4 v5, 0x1

    aget v5, v0, v5

    iput v5, v4, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->y:I

    if-eqz v2, :cond_6

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    invoke-interface {v2, v4}, Lcom/cyanogenmod/trebuchet/DropTarget;->getDropTargetDelegate(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)Lcom/cyanogenmod/trebuchet/DropTarget;

    move-result-object v1

    if-eqz v1, :cond_0

    move-object v2, v1

    :cond_0
    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mLastDropTarget:Lcom/cyanogenmod/trebuchet/DropTarget;

    if-eq v4, v2, :cond_2

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mLastDropTarget:Lcom/cyanogenmod/trebuchet/DropTarget;

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mLastDropTarget:Lcom/cyanogenmod/trebuchet/DropTarget;

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    invoke-interface {v4, v5}, Lcom/cyanogenmod/trebuchet/DropTarget;->onDragExit(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V

    :cond_1
    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    invoke-interface {v2, v4}, Lcom/cyanogenmod/trebuchet/DropTarget;->onDragEnter(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V

    :cond_2
    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    invoke-interface {v2, v4}, Lcom/cyanogenmod/trebuchet/DropTarget;->onDragOver(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V

    :cond_3
    :goto_0
    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/DragController;->mLastDropTarget:Lcom/cyanogenmod/trebuchet/DropTarget;

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-static {v4}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewConfiguration;->getScaledWindowTouchSlop()I

    move-result v3

    iget v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDistanceSinceScroll:I

    int-to-double v4, v4

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/DragController;->mLastTouch:[I

    const/4 v7, 0x0

    aget v6, v6, v7

    sub-int/2addr v6, p1

    int-to-double v6, v6

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/DragController;->mLastTouch:[I

    const/4 v9, 0x1

    aget v8, v8, v9

    sub-int/2addr v8, p2

    int-to-double v8, v8

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v8

    add-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    add-double/2addr v4, v6

    double-to-int v4, v4

    iput v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDistanceSinceScroll:I

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mLastTouch:[I

    const/4 v5, 0x0

    aput p1, v4, v5

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mLastTouch:[I

    const/4 v5, 0x1

    aput p2, v4, v5

    iget v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mScrollZone:I

    if-ge p1, v4, :cond_7

    iget v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mScrollState:I

    if-nez v4, :cond_5

    iget v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDistanceSinceScroll:I

    if-gt v4, v3, :cond_4

    iget-boolean v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mForceScroll:Z

    if-eqz v4, :cond_5

    :cond_4
    const/4 v4, 0x1

    iput v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mScrollState:I

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mForceScroll:Z

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragScroller:Lcom/cyanogenmod/trebuchet/DragScroller;

    const/4 v5, 0x0

    invoke-interface {v4, p1, p2, v5}, Lcom/cyanogenmod/trebuchet/DragScroller;->onEnterScrollArea(III)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mScrollRunnable:Lcom/cyanogenmod/trebuchet/DragController$ScrollRunnable;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/cyanogenmod/trebuchet/DragController$ScrollRunnable;->setDirection(I)V

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/DragController;->mScrollRunnable:Lcom/cyanogenmod/trebuchet/DragController$ScrollRunnable;

    const-wide/16 v6, 0x258

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_5
    :goto_1
    return-void

    :cond_6
    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mLastDropTarget:Lcom/cyanogenmod/trebuchet/DropTarget;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mLastDropTarget:Lcom/cyanogenmod/trebuchet/DropTarget;

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    invoke-interface {v4, v5}, Lcom/cyanogenmod/trebuchet/DropTarget;->onDragExit(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V

    goto :goto_0

    :cond_7
    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mScrollView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    iget v5, p0, Lcom/cyanogenmod/trebuchet/DragController;->mScrollZone:I

    sub-int/2addr v4, v5

    if-le p1, v4, :cond_9

    iget v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mScrollState:I

    if-nez v4, :cond_5

    iget v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDistanceSinceScroll:I

    if-gt v4, v3, :cond_8

    iget-boolean v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mForceScroll:Z

    if-eqz v4, :cond_5

    :cond_8
    const/4 v4, 0x1

    iput v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mScrollState:I

    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mForceScroll:Z

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragScroller:Lcom/cyanogenmod/trebuchet/DragScroller;

    const/4 v5, 0x1

    invoke-interface {v4, p1, p2, v5}, Lcom/cyanogenmod/trebuchet/DragScroller;->onEnterScrollArea(III)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mScrollRunnable:Lcom/cyanogenmod/trebuchet/DragController$ScrollRunnable;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/cyanogenmod/trebuchet/DragController$ScrollRunnable;->setDirection(I)V

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/DragController;->mScrollRunnable:Lcom/cyanogenmod/trebuchet/DragController$ScrollRunnable;

    const-wide/16 v6, 0x258

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    :cond_9
    iget v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mScrollState:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_5

    const/4 v4, 0x0

    iput v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mScrollState:I

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mScrollRunnable:Lcom/cyanogenmod/trebuchet/DragController$ScrollRunnable;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/cyanogenmod/trebuchet/DragController$ScrollRunnable;->setDirection(I)V

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/DragController;->mScrollRunnable:Lcom/cyanogenmod/trebuchet/DragController$ScrollRunnable;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragScroller:Lcom/cyanogenmod/trebuchet/DragScroller;

    invoke-interface {v4}, Lcom/cyanogenmod/trebuchet/DragScroller;->onExitScrollArea()Z

    goto :goto_1
.end method


# virtual methods
.method public addDragListener(Lcom/cyanogenmod/trebuchet/DragController$DragListener;)V
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/DragController$DragListener;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragController;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addDropTarget(Lcom/cyanogenmod/trebuchet/DropTarget;)V
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/DropTarget;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDropTargets:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public cancelDrag()V
    .locals 4

    const/4 v2, 0x1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragging:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragController;->mLastDropTarget:Lcom/cyanogenmod/trebuchet/DropTarget;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragController;->mLastDropTarget:Lcom/cyanogenmod/trebuchet/DropTarget;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    invoke-interface {v0, v1}, Lcom/cyanogenmod/trebuchet/DropTarget;->onDragExit(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    iput-boolean v2, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->cancelled:Z

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    iput-boolean v2, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragComplete:Z

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragSource:Lcom/cyanogenmod/trebuchet/DragSource;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/cyanogenmod/trebuchet/DragSource;->onDropCompleted(Landroid/view/View;Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;Z)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragController;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->refreshAddGuide()V

    :cond_1
    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/DragController;->endDrag()V

    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # Landroid/view/KeyEvent;

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragging:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    iget-boolean v0, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->isInTouchMode:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/DragController;->handleDragViewKeyEvent(Landroid/view/KeyEvent;)Z

    :cond_0
    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragging:Z

    return v0
.end method

.method public dispatchUnhandledMove(Landroid/view/View;I)Z
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragController;->mMoveTarget:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragController;->mMoveTarget:Landroid/view/View;

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->dispatchUnhandledMove(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dragging()Z
    .locals 1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragging:Z

    return v0
.end method

.method getDragView()Lcom/cyanogenmod/trebuchet/DragView;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    return-object v0
.end method

.method getViewBitmap(Landroid/view/View;)Landroid/graphics/Bitmap;
    .locals 6
    .param p1    # Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/view/View;->clearFocus()V

    invoke-virtual {p1, v5}, Landroid/view/View;->setPressed(Z)V

    invoke-virtual {p1}, Landroid/view/View;->willNotCacheDrawing()Z

    move-result v4

    invoke-virtual {p1, v5}, Landroid/view/View;->setWillNotCacheDrawing(Z)V

    invoke-virtual {p1}, Landroid/view/View;->getDrawingCacheBackgroundColor()I

    move-result v3

    invoke-virtual {p1, v5}, Landroid/view/View;->setDrawingCacheBackgroundColor(I)V

    invoke-virtual {p1}, Landroid/view/View;->getAlpha()F

    move-result v0

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {p1, v5}, Landroid/view/View;->setAlpha(F)V

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->destroyDrawingCache()V

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->buildDrawingCache()V

    invoke-virtual {p1}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v2

    if-nez v2, :cond_1

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_1
    invoke-static {v2}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->destroyDrawingCache()V

    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {p1, v4}, Landroid/view/View;->setWillNotCacheDrawing(Z)V

    invoke-virtual {p1, v3}, Landroid/view/View;->setDrawingCacheBackgroundColor(I)V

    goto :goto_0
.end method

.method public isDragging()Z
    .locals 1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragging:Z

    return v0
.end method

.method public onAppsRemoved(Ljava/util/ArrayList;Landroid/content/Context;)V
    .locals 6
    .param p2    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/ApplicationInfo;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    iget-object v2, v3, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragInfo:Ljava/lang/Object;

    instance-of v3, v2, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    if-eqz v3, :cond_1

    move-object v0, v2

    check-cast v0, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/ApplicationInfo;

    iget-object v4, v1, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->intent:Landroid/content/Intent;

    invoke-virtual {v4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v4

    iget-object v5, v0, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->intent:Landroid/content/Intent;

    invoke-virtual {v5}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/DragController;->cancelDrag()V

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    invoke-direct {p0, v4, v5}, Lcom/cyanogenmod/trebuchet/DragController;->getClampedDragLayerPos(FF)[I

    move-result-object v1

    const/4 v4, 0x0

    aget v2, v1, v4

    const/4 v4, 0x1

    aget v3, v1, v4

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    iget-boolean v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragging:Z

    return v4

    :pswitch_1
    iput v2, p0, Lcom/cyanogenmod/trebuchet/DragController;->mMotionDownX:I

    iput v3, p0, Lcom/cyanogenmod/trebuchet/DragController;->mMotionDownY:I

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mLastDropTarget:Lcom/cyanogenmod/trebuchet/DropTarget;

    goto :goto_0

    :pswitch_2
    iget-boolean v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragging:Z

    if-eqz v4, :cond_0

    int-to-float v4, v2

    int-to-float v5, v3

    invoke-direct {p0, v4, v5}, Lcom/cyanogenmod/trebuchet/DragController;->drop(FF)V

    :cond_0
    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/DragController;->endDrag()V

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/DragController;->cancelDrag()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public onKeyEvent(III)Z
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragging:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    packed-switch p1, :pswitch_data_0

    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_0
    iput p2, p0, Lcom/cyanogenmod/trebuchet/DragController;->mMotionDownX:I

    iput p3, p0, Lcom/cyanogenmod/trebuchet/DragController;->mMotionDownY:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/DragController;->mLastDropTarget:Lcom/cyanogenmod/trebuchet/DropTarget;

    goto :goto_1

    :pswitch_1
    invoke-direct {p0, p2, p3}, Lcom/cyanogenmod/trebuchet/DragController;->handleMoveEvent(II)V

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragController;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/DragController;->mScrollRunnable:Lcom/cyanogenmod/trebuchet/DragController$ScrollRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragging:Z

    if-eqz v0, :cond_1

    int-to-float v0, p2

    int-to-float v1, p3

    invoke-direct {p0, v0, v1}, Lcom/cyanogenmod/trebuchet/DragController;->canDrop(FF)Z

    move-result v0

    if-eqz v0, :cond_2

    int-to-float v0, p2

    int-to-float v1, p3

    invoke-direct {p0, v0, v1}, Lcom/cyanogenmod/trebuchet/DragController;->drop(FF)V

    :cond_1
    :goto_2
    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/DragController;->endDrag()V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/DragController;->cancelDrag()V

    goto :goto_2

    :pswitch_3
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/DragController;->cancelDrag()V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 15
    .param p1    # Landroid/view/MotionEvent;

    iget-boolean v11, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragging:Z

    if-nez v11, :cond_0

    const/4 v11, 0x0

    :goto_0
    return v11

    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v11

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v12

    invoke-direct {p0, v11, v12}, Lcom/cyanogenmod/trebuchet/DragController;->getClampedDragLayerPos(FF)[I

    move-result-object v6

    const/4 v11, 0x0

    aget v7, v6, v11

    const/4 v11, 0x1

    aget v8, v6, v11

    const-string v11, "Launcher.DragController"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "[[[onTouchEvent]]]the Action===="

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "=======the position===["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ","

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "]"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch v0, :pswitch_data_0

    :cond_1
    :goto_1
    const/4 v11, 0x1

    goto :goto_0

    :pswitch_0
    iput v7, p0, Lcom/cyanogenmod/trebuchet/DragController;->mMotionDownX:I

    iput v8, p0, Lcom/cyanogenmod/trebuchet/DragController;->mMotionDownY:I

    iget v11, p0, Lcom/cyanogenmod/trebuchet/DragController;->mScrollZone:I

    if-lt v7, v11, :cond_2

    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/DragController;->mScrollView:Landroid/view/View;

    invoke-virtual {v11}, Landroid/view/View;->getWidth()I

    move-result v11

    iget v12, p0, Lcom/cyanogenmod/trebuchet/DragController;->mScrollZone:I

    sub-int/2addr v11, v12

    if-le v7, v11, :cond_3

    :cond_2
    const/4 v11, 0x1

    iput v11, p0, Lcom/cyanogenmod/trebuchet/DragController;->mScrollState:I

    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/DragController;->mHandler:Landroid/os/Handler;

    iget-object v12, p0, Lcom/cyanogenmod/trebuchet/DragController;->mScrollRunnable:Lcom/cyanogenmod/trebuchet/DragController$ScrollRunnable;

    const-wide/16 v13, 0x258

    invoke-virtual {v11, v12, v13, v14}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    :cond_3
    const/4 v11, 0x0

    iput v11, p0, Lcom/cyanogenmod/trebuchet/DragController;->mScrollState:I

    goto :goto_1

    :pswitch_1
    invoke-direct {p0, v7, v8}, Lcom/cyanogenmod/trebuchet/DragController;->handleMoveEvent(II)V

    goto :goto_1

    :pswitch_2
    invoke-direct {p0, v7, v8}, Lcom/cyanogenmod/trebuchet/DragController;->handleMoveEvent(II)V

    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/DragController;->mHandler:Landroid/os/Handler;

    iget-object v12, p0, Lcom/cyanogenmod/trebuchet/DragController;->mScrollRunnable:Lcom/cyanogenmod/trebuchet/DragController$ScrollRunnable;

    invoke-virtual {v11, v12}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-boolean v11, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragging:Z

    if-eqz v11, :cond_1

    int-to-float v11, v7

    int-to-float v12, v8

    invoke-direct {p0, v11, v12}, Lcom/cyanogenmod/trebuchet/DragController;->canDrop(FF)Z

    move-result v11

    if-eqz v11, :cond_6

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragController;->mCoordinatesTemp:[I

    invoke-direct {p0, v7, v8, v4}, Lcom/cyanogenmod/trebuchet/DragController;->findDropTarget(II[I)Lcom/cyanogenmod/trebuchet/DropTarget;

    move-result-object v9

    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    iget-object v11, v11, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragInfo:Ljava/lang/Object;

    instance-of v11, v11, Lcom/cyanogenmod/trebuchet/FolderInfo;

    if-eqz v11, :cond_5

    instance-of v11, v9, Lcom/cyanogenmod/trebuchet/DeleteDropTarget;

    if-eqz v11, :cond_5

    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    iget-object v10, v11, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragInfo:Ljava/lang/Object;

    check-cast v10, Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-object v11, v10, Lcom/cyanogenmod/trebuchet/FolderInfo;->contents:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_4

    new-instance v3, Landroid/app/AlertDialog$Builder;

    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/DragController;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-direct {v3, v11}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/DragController;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v11}, Lcom/cyanogenmod/trebuchet/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0a00bc    # com.konka.avenger.R.string.folder_delete

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v11}, Landroid/app/AlertDialog;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/DragController;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v11}, Lcom/cyanogenmod/trebuchet/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0a00bd    # com.konka.avenger.R.string.folder_noempty

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v11}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    const/4 v11, -0x1

    iget-object v12, p0, Lcom/cyanogenmod/trebuchet/DragController;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v12}, Lcom/cyanogenmod/trebuchet/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0a000d    # com.konka.avenger.R.string.dialog_positive

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    new-instance v13, Lcom/cyanogenmod/trebuchet/DragController$1;

    invoke-direct {v13, p0, v7, v8}, Lcom/cyanogenmod/trebuchet/DragController$1;-><init>(Lcom/cyanogenmod/trebuchet/DragController;II)V

    invoke-virtual {v5, v11, v12, v13}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    const/4 v11, -0x2

    iget-object v12, p0, Lcom/cyanogenmod/trebuchet/DragController;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v12}, Lcom/cyanogenmod/trebuchet/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f0a000e    # com.konka.avenger.R.string.dialog_dismiss

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    new-instance v13, Lcom/cyanogenmod/trebuchet/DragController$2;

    invoke-direct {v13, p0, v5}, Lcom/cyanogenmod/trebuchet/DragController$2;-><init>(Lcom/cyanogenmod/trebuchet/DragController;Landroid/app/AlertDialog;)V

    invoke-virtual {v5, v11, v12, v13}, Landroid/app/AlertDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    invoke-virtual {v5}, Landroid/app/AlertDialog;->show()V

    const/4 v11, -0x1

    invoke-virtual {v5, v11}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v2

    const/4 v11, -0x2

    invoke-virtual {v5, v11}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Button;->requestFocus()Z

    const/4 v11, 0x1

    invoke-virtual {v1, v11}, Landroid/widget/Button;->setFocusableInTouchMode(Z)V

    invoke-virtual {v2}, Landroid/widget/Button;->clearFocus()V

    goto/16 :goto_1

    :cond_4
    int-to-float v11, v7

    int-to-float v12, v8

    invoke-direct {p0, v11, v12}, Lcom/cyanogenmod/trebuchet/DragController;->drop(FF)V

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/DragController;->endDrag()V

    goto/16 :goto_1

    :cond_5
    int-to-float v11, v7

    int-to-float v12, v8

    invoke-direct {p0, v11, v12}, Lcom/cyanogenmod/trebuchet/DragController;->drop(FF)V

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/DragController;->endDrag()V

    goto/16 :goto_1

    :cond_6
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/DragController;->cancelDrag()V

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/DragController;->endDrag()V

    goto/16 :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/DragController;->cancelDrag()V

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public removeDragListener(Lcom/cyanogenmod/trebuchet/DragController$DragListener;)V
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/DragController$DragListener;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragController;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public removeDropTarget(Lcom/cyanogenmod/trebuchet/DropTarget;)V
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/DropTarget;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDropTargets:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public setDragScoller(Lcom/cyanogenmod/trebuchet/DragScroller;)V
    .locals 0
    .param p1    # Lcom/cyanogenmod/trebuchet/DragScroller;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/DragController;->mDragScroller:Lcom/cyanogenmod/trebuchet/DragScroller;

    return-void
.end method

.method setMoveTarget(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/DragController;->mMoveTarget:Landroid/view/View;

    return-void
.end method

.method public setScrollView(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/DragController;->mScrollView:Landroid/view/View;

    return-void
.end method

.method public setWindowToken(Landroid/os/IBinder;)V
    .locals 0
    .param p1    # Landroid/os/IBinder;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/DragController;->mWindowToken:Landroid/os/IBinder;

    return-void
.end method

.method public startDrag(Landroid/graphics/Bitmap;IILcom/cyanogenmod/trebuchet/DragSource;Ljava/lang/Object;ILandroid/graphics/Point;Landroid/graphics/Rect;Z)V
    .locals 20
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # I
    .param p3    # I
    .param p4    # Lcom/cyanogenmod/trebuchet/DragSource;
    .param p5    # Ljava/lang/Object;
    .param p6    # I
    .param p7    # Landroid/graphics/Point;
    .param p8    # Landroid/graphics/Rect;
    .param p9    # Z

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cyanogenmod/trebuchet/DragController;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    if-nez v5, :cond_0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cyanogenmod/trebuchet/DragController;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    const-string v6, "input_method"

    invoke-virtual {v5, v6}, Lcom/cyanogenmod/trebuchet/Launcher;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/inputmethod/InputMethodManager;

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/cyanogenmod/trebuchet/DragController;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    :cond_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cyanogenmod/trebuchet/DragController;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/cyanogenmod/trebuchet/DragController;->mWindowToken:Landroid/os/IBinder;

    const/4 v9, 0x0

    invoke-virtual {v5, v6, v9}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    if-nez p9, :cond_1

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v17

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v16

    div-int/lit8 v5, v17, 0x2

    add-int v5, v5, p2

    int-to-float v5, v5

    div-int/lit8 v6, v16, 0x2

    add-int v6, v6, p3

    int-to-float v6, v6

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v6}, Lcom/cyanogenmod/trebuchet/DragController;->getClampedDragLayerPos(FF)[I

    move-result-object v13

    const/4 v5, 0x0

    const/4 v6, 0x0

    aget v6, v13, v6

    const/4 v9, 0x1

    aget v9, v13, v9

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v6, v9}, Lcom/cyanogenmod/trebuchet/DragController;->onKeyEvent(III)Z

    :cond_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cyanogenmod/trebuchet/DragController;->mListeners:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/cyanogenmod/trebuchet/DragController;->mMotionDownX:I

    sub-int v7, v5, p2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/cyanogenmod/trebuchet/DragController;->mMotionDownY:I

    sub-int v8, v5, p3

    if-nez p8, :cond_5

    const/4 v14, 0x0

    :goto_1
    if-nez p8, :cond_6

    const/4 v15, 0x0

    :goto_2
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/cyanogenmod/trebuchet/DragController;->mDragging:Z

    new-instance v5, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    invoke-direct {v5}, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;-><init>()V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    const/4 v6, 0x0

    iput-boolean v6, v5, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragComplete:Z

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/cyanogenmod/trebuchet/DragController;->mMotionDownX:I

    add-int v9, p2, v14

    sub-int/2addr v6, v9

    iput v6, v5, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->xOffset:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/cyanogenmod/trebuchet/DragController;->mMotionDownY:I

    add-int v9, p3, v15

    sub-int/2addr v6, v9

    iput v6, v5, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->yOffset:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    move-object/from16 v0, p4

    iput-object v0, v5, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragSource:Lcom/cyanogenmod/trebuchet/DragSource;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    move-object/from16 v0, p5

    iput-object v0, v5, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragInfo:Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    move/from16 v0, p9

    iput-boolean v0, v5, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->isInTouchMode:Z

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cyanogenmod/trebuchet/DragController;->mVibrator:Landroid/os/Vibrator;

    const-wide/16 v9, 0x23

    invoke-virtual {v5, v9, v10}, Landroid/os/Vibrator;->vibrate(J)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    move-object/from16 v19, v0

    new-instance v4, Lcom/cyanogenmod/trebuchet/DragView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cyanogenmod/trebuchet/DragController;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    move-object/from16 v6, p1

    invoke-direct/range {v4 .. v12}, Lcom/cyanogenmod/trebuchet/DragView;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;Landroid/graphics/Bitmap;IIIIII)V

    move-object/from16 v0, v19

    iput-object v4, v0, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    if-eqz p7, :cond_2

    new-instance v5, Landroid/graphics/Point;

    move-object/from16 v0, p7

    invoke-direct {v5, v0}, Landroid/graphics/Point;-><init>(Landroid/graphics/Point;)V

    invoke-virtual {v4, v5}, Lcom/cyanogenmod/trebuchet/DragView;->setDragVisualizeOffset(Landroid/graphics/Point;)V

    :cond_2
    if-eqz p8, :cond_3

    new-instance v5, Landroid/graphics/Rect;

    move-object/from16 v0, p8

    invoke-direct {v5, v0}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    invoke-virtual {v4, v5}, Lcom/cyanogenmod/trebuchet/DragView;->setDragRegion(Landroid/graphics/Rect;)V

    :cond_3
    move-object/from16 v0, p0

    iget v5, v0, Lcom/cyanogenmod/trebuchet/DragController;->mMotionDownX:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/cyanogenmod/trebuchet/DragController;->mMotionDownY:I

    invoke-virtual {v4, v5, v6}, Lcom/cyanogenmod/trebuchet/DragView;->show(II)V

    if-nez p9, :cond_7

    const-string v5, "Launcher.DragController"

    const-string v6, "It is not in touch mode!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    iget-object v5, v5, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/cyanogenmod/trebuchet/DragView;->setFocusable(Z)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    iget-object v5, v5, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lcom/cyanogenmod/trebuchet/DragView;->setFocusableInTouchMode(Z)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cyanogenmod/trebuchet/DragController;->mDragObject:Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    iget-object v5, v5, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/DragView;->requestFocus()Z

    :goto_3
    move-object/from16 v0, p0

    iget v5, v0, Lcom/cyanogenmod/trebuchet/DragController;->mMotionDownX:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/cyanogenmod/trebuchet/DragController;->mMotionDownY:I

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v6}, Lcom/cyanogenmod/trebuchet/DragController;->handleMoveEvent(II)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cyanogenmod/trebuchet/DragController;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/Launcher;->hideAddGuide()V

    return-void

    :cond_4
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/cyanogenmod/trebuchet/DragController$DragListener;

    move-object/from16 v0, v18

    move-object/from16 v1, p4

    move-object/from16 v2, p5

    move/from16 v3, p6

    invoke-interface {v0, v1, v2, v3}, Lcom/cyanogenmod/trebuchet/DragController$DragListener;->onDragStart(Lcom/cyanogenmod/trebuchet/DragSource;Ljava/lang/Object;I)V

    goto/16 :goto_0

    :cond_5
    move-object/from16 v0, p8

    iget v14, v0, Landroid/graphics/Rect;->left:I

    goto/16 :goto_1

    :cond_6
    move-object/from16 v0, p8

    iget v15, v0, Landroid/graphics/Rect;->top:I

    goto/16 :goto_2

    :cond_7
    const-string v5, "Launcher.DragController"

    const-string v6, "It is in touch mode!!!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method

.method public startDrag(Landroid/graphics/Bitmap;IILcom/cyanogenmod/trebuchet/DragSource;Ljava/lang/Object;IZ)V
    .locals 10
    .param p1    # Landroid/graphics/Bitmap;
    .param p2    # I
    .param p3    # I
    .param p4    # Lcom/cyanogenmod/trebuchet/DragSource;
    .param p5    # Ljava/lang/Object;
    .param p6    # I
    .param p7    # Z

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move/from16 v6, p6

    move/from16 v9, p7

    invoke-virtual/range {v0 .. v9}, Lcom/cyanogenmod/trebuchet/DragController;->startDrag(Landroid/graphics/Bitmap;IILcom/cyanogenmod/trebuchet/DragSource;Ljava/lang/Object;ILandroid/graphics/Point;Landroid/graphics/Rect;Z)V

    return-void
.end method

.method public startDrag(Landroid/view/View;Landroid/graphics/Bitmap;Lcom/cyanogenmod/trebuchet/DragSource;Ljava/lang/Object;ILandroid/graphics/Rect;Z)V
    .locals 12
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/graphics/Bitmap;
    .param p3    # Lcom/cyanogenmod/trebuchet/DragSource;
    .param p4    # Ljava/lang/Object;
    .param p5    # I
    .param p6    # Landroid/graphics/Rect;
    .param p7    # Z

    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/DragController;->mCoordinatesTemp:[I

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/DragController;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->getDragLayer()Lcom/cyanogenmod/trebuchet/DragLayer;

    move-result-object v1

    invoke-virtual {v1, p1, v11}, Lcom/cyanogenmod/trebuchet/DragLayer;->getLocationInDragLayer(Landroid/view/View;[I)V

    const/4 v1, 0x0

    aget v3, v11, v1

    const/4 v1, 0x1

    aget v4, v11, v1

    const/4 v8, 0x0

    move-object v1, p0

    move-object v2, p2

    move-object v5, p3

    move-object/from16 v6, p4

    move/from16 v7, p5

    move-object/from16 v9, p6

    move/from16 v10, p7

    invoke-virtual/range {v1 .. v10}, Lcom/cyanogenmod/trebuchet/DragController;->startDrag(Landroid/graphics/Bitmap;IILcom/cyanogenmod/trebuchet/DragSource;Ljava/lang/Object;ILandroid/graphics/Point;Landroid/graphics/Rect;Z)V

    sget v1, Lcom/cyanogenmod/trebuchet/DragController;->DRAG_ACTION_MOVE:I

    move/from16 v0, p5

    if-ne v0, v1, :cond_0

    const/16 v1, 0x8

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public startDrag(Landroid/view/View;Lcom/cyanogenmod/trebuchet/DragSource;Ljava/lang/Object;ILandroid/graphics/Rect;Z)V
    .locals 11
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/cyanogenmod/trebuchet/DragSource;
    .param p3    # Ljava/lang/Object;
    .param p4    # I
    .param p5    # Landroid/graphics/Rect;
    .param p6    # Z

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/DragController;->getViewBitmap(Landroid/view/View;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/DragController;->mCoordinatesTemp:[I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragController;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->getDragLayer()Lcom/cyanogenmod/trebuchet/DragLayer;

    move-result-object v0

    invoke-virtual {v0, p1, v10}, Lcom/cyanogenmod/trebuchet/DragLayer;->getLocationInDragLayer(Landroid/view/View;[I)V

    const/4 v0, 0x0

    aget v2, v10, v0

    const/4 v0, 0x1

    aget v3, v10, v0

    const/4 v7, 0x0

    move-object v0, p0

    move-object v4, p2

    move-object v5, p3

    move v6, p4

    move-object/from16 v8, p5

    move/from16 v9, p6

    invoke-virtual/range {v0 .. v9}, Lcom/cyanogenmod/trebuchet/DragController;->startDrag(Landroid/graphics/Bitmap;IILcom/cyanogenmod/trebuchet/DragSource;Ljava/lang/Object;ILandroid/graphics/Point;Landroid/graphics/Rect;Z)V

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    sget v0, Lcom/cyanogenmod/trebuchet/DragController;->DRAG_ACTION_MOVE:I

    if-ne p4, v0, :cond_0

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public startDrag(Landroid/view/View;Lcom/cyanogenmod/trebuchet/DragSource;Ljava/lang/Object;IZ)V
    .locals 7
    .param p1    # Landroid/view/View;
    .param p2    # Lcom/cyanogenmod/trebuchet/DragSource;
    .param p3    # Ljava/lang/Object;
    .param p4    # I
    .param p5    # Z

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/cyanogenmod/trebuchet/DragController;->startDrag(Landroid/view/View;Lcom/cyanogenmod/trebuchet/DragSource;Ljava/lang/Object;ILandroid/graphics/Rect;Z)V

    return-void
.end method
