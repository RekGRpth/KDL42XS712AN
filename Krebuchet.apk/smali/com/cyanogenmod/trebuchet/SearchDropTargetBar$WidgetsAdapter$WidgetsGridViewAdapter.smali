.class Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$WidgetsGridViewAdapter;
.super Lcom/cyanogenmod/trebuchet/VidgetAdapter$VidgetGridAdapter;
.source "SearchDropTargetBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WidgetsGridViewAdapter"
.end annotation


# instance fields
.field private mHoverListener:Landroid/view/View$OnHoverListener;

.field final synthetic this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;


# direct methods
.method private constructor <init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;)V
    .locals 1

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$WidgetsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/VidgetAdapter$VidgetGridAdapter;-><init>(Lcom/cyanogenmod/trebuchet/VidgetAdapter;)V

    new-instance v0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$WidgetsGridViewAdapter$1;

    invoke-direct {v0, p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$WidgetsGridViewAdapter$1;-><init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$WidgetsGridViewAdapter;)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$WidgetsGridViewAdapter;->mHoverListener:Landroid/view/View$OnHoverListener;

    return-void
.end method

.method synthetic constructor <init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$WidgetsGridViewAdapter;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$WidgetsGridViewAdapter;-><init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;)V

    return-void
.end method


# virtual methods
.method public getItem(I)Ljava/lang/Object;
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$WidgetsGridViewAdapter;->mCount:I

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$WidgetsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->access$0(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;)Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    move-result-object v0

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWidgets:Ljava/util/List;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->access$7(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)Ljava/util/List;

    move-result-object v0

    iget v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$WidgetsGridViewAdapter;->mStartIdx:I

    add-int/2addr v1, p1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v9, 0x0

    const/4 v6, 0x0

    iget v5, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$WidgetsGridViewAdapter;->mStartIdx:I

    add-int/2addr v5, p1

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$WidgetsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;
    invoke-static {v7}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->access$0(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;)Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    move-result-object v7

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWidgets:Ljava/util/List;
    invoke-static {v7}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->access$7(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-lt v5, v7, :cond_0

    move-object v5, v6

    :goto_0
    return-object v5

    :cond_0
    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$WidgetsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;
    invoke-static {v5}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->access$0(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;)Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    move-result-object v5

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWidgets:Ljava/util/List;
    invoke-static {v5}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->access$7(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)Ljava/util/List;

    move-result-object v5

    iget v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$WidgetsGridViewAdapter;->mStartIdx:I

    add-int/2addr v7, p1

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/appwidget/AppWidgetProviderInfo;

    if-nez p2, :cond_1

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$WidgetsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;

    iget-object v5, v5, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v7, 0x7f03001e    # com.konka.avenger.R.layout.paged_dialog_widget_preview

    invoke-virtual {v5, v7, v6, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/cyanogenmod/trebuchet/PagedViewWidget;

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$WidgetsGridViewAdapter;->mHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {p2, v5}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    new-instance v3, Landroid/widget/AbsListView$LayoutParams;

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$WidgetsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;

    iget v5, v5, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->mVidgetHSpan:I

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$WidgetsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;

    iget v7, v7, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->mVidgetCellWidth:I

    mul-int/2addr v5, v7

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$WidgetsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;

    iget v7, v7, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->mVidgetVSpan:I

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$WidgetsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;

    iget v8, v8, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->mVidgetCellHeight:I

    mul-int/2addr v7, v8

    invoke-direct {v3, v5, v7}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    move-object v5, p2

    check-cast v5, Lcom/cyanogenmod/trebuchet/PagedViewWidget;

    invoke-virtual {v5, v3}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$ViewHolder;

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$WidgetsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;

    invoke-direct {v1, v5, v6}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$ViewHolder;-><init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$ViewHolder;)V

    new-instance v5, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;

    invoke-direct {v5, v2, v6, v6}, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;-><init>(Landroid/appwidget/AppWidgetProviderInfo;Ljava/lang/String;Landroid/os/Parcelable;)V

    iput-object v5, v1, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$ViewHolder;->mInfo:Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;

    const v5, 0x7f0d0020    # com.konka.avenger.R.id.widget_name

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v1, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$ViewHolder;->mNameView:Landroid/widget/TextView;

    const v5, 0x7f0d0021    # com.konka.avenger.R.id.widget_dims

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, v1, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$ViewHolder;->mDimsView:Landroid/widget/TextView;

    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$ViewHolder;

    iget-object v4, v1, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$ViewHolder;->mInfo:Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$WidgetsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;

    iget-object v5, v5, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v5, v2, v6}, Lcom/cyanogenmod/trebuchet/Launcher;->getSpanForWidget(Landroid/appwidget/AppWidgetProviderInfo;[I)[I

    move-result-object v0

    aget v5, v0, v9

    iput v5, v4, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;->spanX:I

    const/4 v5, 0x1

    aget v5, v0, v5

    iput v5, v4, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;->spanY:I

    move-object v5, p2

    check-cast v5, Lcom/cyanogenmod/trebuchet/PagedViewWidget;

    const/4 v6, -0x1

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$WidgetsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;

    iget-object v7, v7, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->mHolographicOutlineHelper:Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

    invoke-virtual {v5, v2, v6, v0, v7}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->applyFromAppWidgetProviderInfo(Landroid/appwidget/AppWidgetProviderInfo;I[ILcom/cyanogenmod/trebuchet/HolographicOutlineHelper;)V

    move-object v5, p2

    goto/16 :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$ViewHolder;

    iget-object v2, v1, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$ViewHolder;->mInfo:Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$WidgetsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;

    iget-object v3, v3, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Launcher;->getWorkspace()Lcom/cyanogenmod/trebuchet/Workspace;

    move-result-object v3

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Workspace;->getCurrentDropLayout()Lcom/cyanogenmod/trebuchet/CellLayout;

    move-result-object v0

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$WidgetsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->access$0(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;)Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    move-result-object v3

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWidgetsDialog:Lgreendroid/widget/PagedDialog;
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->access$8(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)Lgreendroid/widget/PagedDialog;

    move-result-object v3

    invoke-virtual {v3}, Lgreendroid/widget/PagedDialog;->dismiss()V

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$WidgetsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;

    iget-object v3, v3, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v3, v2, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->addExternalItemToScreen(Lcom/cyanogenmod/trebuchet/ItemInfo;Lcom/cyanogenmod/trebuchet/CellLayout;)V

    return-void
.end method
