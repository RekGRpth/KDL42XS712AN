.class Lcom/cyanogenmod/trebuchet/UserCenterMonitor$QueryTask;
.super Landroid/os/AsyncTask;
.source "UserCenterMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cyanogenmod/trebuchet/UserCenterMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "QueryTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/konka/passport/service/UserInfo;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final mMonitorRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/cyanogenmod/trebuchet/UserCenterMonitor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/cyanogenmod/trebuchet/UserCenterMonitor;)V
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/UserCenterMonitor;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/UserCenterMonitor$QueryTask;->mMonitorRef:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/konka/passport/service/UserInfo;)Ljava/lang/Boolean;
    .locals 5
    .param p1    # [Lcom/konka/passport/service/UserInfo;

    const/4 v4, 0x0

    aget-object v1, p1, v4

    :try_start_0
    invoke-interface {v1}, Lcom/konka/passport/service/UserInfo;->GetPassportId()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "wzd_usercenter"

    const-string v3, "GetPassportInfo failed:"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v2, "wzd_usercenter"

    const-string v3, "GetPassportInfo failed:"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    goto :goto_0
.end method

.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Lcom/konka/passport/service/UserInfo;

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/UserCenterMonitor$QueryTask;->doInBackground([Lcom/konka/passport/service/UserInfo;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 4
    .param p1    # Ljava/lang/Boolean;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/UserCenterMonitor$QueryTask;->mMonitorRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/UserCenterMonitor;

    const-string v1, "wzd_usercenter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "query login == "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/cyanogenmod/trebuchet/UserCenterMonitor;->access$0(Lcom/cyanogenmod/trebuchet/UserCenterMonitor;Z)V

    # invokes: Lcom/cyanogenmod/trebuchet/UserCenterMonitor;->updateUserCenterState()V
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/UserCenterMonitor;->access$1(Lcom/cyanogenmod/trebuchet/UserCenterMonitor;)V

    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/UserCenterMonitor$QueryTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
