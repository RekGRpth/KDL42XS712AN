.class Lcom/cyanogenmod/trebuchet/Workspace$15;
.super Ljava/lang/Object;
.source "Workspace.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cyanogenmod/trebuchet/Workspace;->onItemClick(Lnet/londatiga/android/QuickAction;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/Workspace;

.field private final synthetic val$folderInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;


# direct methods
.method constructor <init>(Lcom/cyanogenmod/trebuchet/Workspace;Lcom/cyanogenmod/trebuchet/FolderInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Workspace$15;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    iput-object p2, p0, Lcom/cyanogenmod/trebuchet/Workspace$15;->val$folderInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace$15;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace$15;->val$folderInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->removeFolder(Lcom/cyanogenmod/trebuchet/FolderInfo;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace$15;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace$15;->val$folderInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    invoke-static {v0, v1}, Lcom/cyanogenmod/trebuchet/LauncherModel;->deleteFolderContentsFromDatabase(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/FolderInfo;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace$15;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    # getter for: Lcom/cyanogenmod/trebuchet/Workspace;->mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/Workspace;->access$35(Lcom/cyanogenmod/trebuchet/Workspace;)Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/CellLayout;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace$15;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    # getter for: Lcom/cyanogenmod/trebuchet/Workspace;->mCurSelectCell:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/Workspace;->access$35(Lcom/cyanogenmod/trebuchet/Workspace;)Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    move-result-object v1

    iget-object v1, v1, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->removeView(Landroid/view/View;)V

    return-void
.end method
