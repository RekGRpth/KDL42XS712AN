.class Lcom/cyanogenmod/trebuchet/Workspace$FolderCreationAlarmListener;
.super Ljava/lang/Object;
.source "Workspace.java"

# interfaces
.implements Lcom/cyanogenmod/trebuchet/OnAlarmListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cyanogenmod/trebuchet/Workspace;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "FolderCreationAlarmListener"
.end annotation


# instance fields
.field cellX:I

.field cellY:I

.field layout:Lcom/cyanogenmod/trebuchet/CellLayout;

.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/Workspace;


# direct methods
.method public constructor <init>(Lcom/cyanogenmod/trebuchet/Workspace;Lcom/cyanogenmod/trebuchet/CellLayout;II)V
    .locals 0
    .param p2    # Lcom/cyanogenmod/trebuchet/CellLayout;
    .param p3    # I
    .param p4    # I

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Workspace$FolderCreationAlarmListener;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/cyanogenmod/trebuchet/Workspace$FolderCreationAlarmListener;->layout:Lcom/cyanogenmod/trebuchet/CellLayout;

    iput p3, p0, Lcom/cyanogenmod/trebuchet/Workspace$FolderCreationAlarmListener;->cellX:I

    iput p4, p0, Lcom/cyanogenmod/trebuchet/Workspace$FolderCreationAlarmListener;->cellY:I

    return-void
.end method


# virtual methods
.method public onAlarm(Lcom/cyanogenmod/trebuchet/Alarm;)V
    .locals 4
    .param p1    # Lcom/cyanogenmod/trebuchet/Alarm;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace$FolderCreationAlarmListener;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    # getter for: Lcom/cyanogenmod/trebuchet/Workspace;->mDragFolderRingAnimator:Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/Workspace;->access$2(Lcom/cyanogenmod/trebuchet/Workspace;)Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace$FolderCreationAlarmListener;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    new-instance v1, Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace$FolderCreationAlarmListener;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    iget-object v2, v2, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;Lcom/cyanogenmod/trebuchet/FolderIcon;)V

    invoke-static {v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->access$3(Lcom/cyanogenmod/trebuchet/Workspace;Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;)V

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace$FolderCreationAlarmListener;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    # getter for: Lcom/cyanogenmod/trebuchet/Workspace;->mDragFolderRingAnimator:Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/Workspace;->access$2(Lcom/cyanogenmod/trebuchet/Workspace;)Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;

    move-result-object v0

    iget v1, p0, Lcom/cyanogenmod/trebuchet/Workspace$FolderCreationAlarmListener;->cellX:I

    iget v2, p0, Lcom/cyanogenmod/trebuchet/Workspace$FolderCreationAlarmListener;->cellY:I

    invoke-virtual {v0, v1, v2}, Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;->setCell(II)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace$FolderCreationAlarmListener;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    # getter for: Lcom/cyanogenmod/trebuchet/Workspace;->mDragFolderRingAnimator:Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/Workspace;->access$2(Lcom/cyanogenmod/trebuchet/Workspace;)Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace$FolderCreationAlarmListener;->layout:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;->setCellLayout(Lcom/cyanogenmod/trebuchet/CellLayout;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace$FolderCreationAlarmListener;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    # getter for: Lcom/cyanogenmod/trebuchet/Workspace;->mDragFolderRingAnimator:Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/Workspace;->access$2(Lcom/cyanogenmod/trebuchet/Workspace;)Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;->animateToAcceptState()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace$FolderCreationAlarmListener;->layout:Lcom/cyanogenmod/trebuchet/CellLayout;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Workspace$FolderCreationAlarmListener;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    # getter for: Lcom/cyanogenmod/trebuchet/Workspace;->mDragFolderRingAnimator:Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/Workspace;->access$2(Lcom/cyanogenmod/trebuchet/Workspace;)Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->showFolderAccept(Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace$FolderCreationAlarmListener;->layout:Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->clearDragOutlines()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace$FolderCreationAlarmListener;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->access$4(Lcom/cyanogenmod/trebuchet/Workspace;Z)V

    return-void
.end method
