.class public abstract Lcom/cyanogenmod/trebuchet/VidgetAdapter;
.super Lgreendroid/widget/PagedAdapter;
.source "VidgetAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cyanogenmod/trebuchet/VidgetAdapter$VidgetGridAdapter;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "VidgetAdapter"

.field private static final sPageSleepDelay:I = 0xc8


# instance fields
.field protected mAdapters:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/VidgetAdapter$VidgetGridAdapter;",
            ">;"
        }
    .end annotation
.end field

.field protected mAppIconSize:I

.field protected mDefaultWidgetBackground:Landroid/graphics/drawable/Drawable;

.field protected mDragViewMultiplyColor:I

.field protected mHolographicOutlineHelper:Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

.field protected mIconCache:Lcom/cyanogenmod/trebuchet/IconCache;

.field protected final mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

.field protected mLayoutInflater:Landroid/view/LayoutInflater;

.field protected final mPackageManager:Landroid/content/pm/PackageManager;

.field protected mRunningTasks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;",
            ">;"
        }
    .end annotation
.end field

.field protected final mVidgetCellHeight:I

.field protected final mVidgetCellWidth:I

.field protected mVidgetHSpan:I

.field protected mVidgetVSpan:I

.field protected final sWidgetPreviewIconPaddingPercentage:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/Launcher;IIII)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/cyanogenmod/trebuchet/Launcher;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I

    invoke-direct {p0}, Lgreendroid/widget/PagedAdapter;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mAdapters:Ljava/util/ArrayList;

    const/high16 v1, 0x3e800000    # 0.25f

    iput v1, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->sWidgetPreviewIconPaddingPercentage:F

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    new-instance v1, Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

    invoke-direct {v1}, Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;-><init>()V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mHolographicOutlineHelper:Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

    iput p3, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mVidgetCellWidth:I

    iput p4, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mVidgetCellHeight:I

    iput p5, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mVidgetHSpan:I

    iput p6, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mVidgetVSpan:I

    iput-object p2, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mPackageManager:Landroid/content/pm/PackageManager;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mRunningTasks:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/LauncherApplication;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->getIconCache()Lcom/cyanogenmod/trebuchet/IconCache;

    move-result-object v1

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mIconCache:Lcom/cyanogenmod/trebuchet/IconCache;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02002e    # com.konka.avenger.R.drawable.default_widget_preview_holo

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mDefaultWidgetBackground:Landroid/graphics/drawable/Drawable;

    const v1, 0x7f0c0022    # com.konka.avenger.R.dimen.app_icon_size

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mAppIconSize:I

    const v1, 0x7f080002    # com.konka.avenger.R.color.drag_view_multiply_color

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mDragViewMultiplyColor:I

    return-void
.end method

.method static synthetic access$0(Lcom/cyanogenmod/trebuchet/VidgetAdapter;Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->loadWidgetPreviewsInBackground(Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;)V

    return-void
.end method

.method static synthetic access$1(Lcom/cyanogenmod/trebuchet/VidgetAdapter;Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;Lcom/cyanogenmod/trebuchet/PagedDialogGridView;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->onSyncWidgetPageItems(Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;Lcom/cyanogenmod/trebuchet/PagedDialogGridView;)V

    return-void
.end method

.method private getShortcutPreview(Landroid/content/pm/ResolveInfo;)Landroid/graphics/Bitmap;
    .locals 8
    .param p1    # Landroid/content/pm/ResolveInfo;

    const/4 v3, 0x0

    iget v7, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mAppIconSize:I

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v7, v7, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mIconCache:Lcom/cyanogenmod/trebuchet/IconCache;

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/IconCache;->getFullResIcon(Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget v5, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mAppIconSize:I

    iget v6, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mAppIconSize:I

    move-object v0, p0

    move v4, v3

    invoke-direct/range {v0 .. v6}, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->renderDrawableToBitmap(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;IIII)V

    return-object v2
.end method

.method private getSleepForPage(II)I
    .locals 3
    .param p1    # I
    .param p2    # I

    invoke-direct {p0, p1, p2}, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->getWidgetPageLoadPriority(II)I

    move-result v0

    const/4 v1, 0x0

    mul-int/lit16 v2, v0, 0xc8

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    return v1
.end method

.method private static getThreadPriorityForPage(II)I
    .locals 2
    .param p0    # I
    .param p1    # I

    sub-int v1, p0, p1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v1, -0x2

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    if-gt v0, v1, :cond_1

    const/4 v1, -0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getWidgetPageLoadPriority(II)I
    .locals 6
    .param p1    # I
    .param p2    # I

    move v4, p2

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mRunningTasks:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const v1, 0x7fffffff

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_0

    sub-int v5, p1, v4

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v2

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v5

    sub-int v5, v2, v5

    return v5

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;

    iget v5, v3, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->page:I

    sub-int/2addr v5, v4

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v1

    goto :goto_0
.end method

.method private getWidgetPreview(Landroid/content/ComponentName;IIIIII)Landroid/graphics/Bitmap;
    .locals 23
    .param p1    # Landroid/content/ComponentName;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I

    invoke-virtual/range {p1 .. p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v18

    if-gez p6, :cond_0

    const p6, 0x7fffffff

    :cond_0
    if-gez p7, :cond_1

    const p7, 0x7fffffff

    :cond_1
    const/4 v3, 0x0

    if-eqz p2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v5, 0x0

    move-object/from16 v0, v18

    move/from16 v1, p2

    invoke-virtual {v2, v0, v1, v5}, Landroid/content/pm/PackageManager;->getDrawable(Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    if-nez v3, :cond_2

    const-string v2, "VidgetAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Can\'t load widget preview drawable 0x"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " for provider: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    if-eqz v3, :cond_7

    const/16 v22, 0x1

    :goto_0
    if-eqz v22, :cond_8

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v8

    move-object/from16 v0, p0

    iget v2, v0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mVidgetCellWidth:I

    mul-int v2, v2, p4

    move/from16 v0, p6

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result p6

    move-object/from16 v0, p0

    iget v2, v0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mVidgetCellHeight:I

    mul-int v2, v2, p5

    move/from16 v0, p7

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result p7

    :cond_3
    :goto_1
    const/high16 v20, 0x3f800000    # 1.0f

    move/from16 v0, p6

    if-le v7, v0, :cond_4

    move/from16 v0, p6

    int-to-float v2, v0

    int-to-float v5, v7

    div-float v20, v2, v5

    :cond_4
    int-to-float v2, v8

    mul-float v2, v2, v20

    move/from16 v0, p7

    int-to-float v5, v0

    cmpl-float v2, v2, v5

    if-lez v2, :cond_5

    move/from16 v0, p7

    int-to-float v2, v0

    int-to-float v5, v8

    div-float v20, v2, v5

    :cond_5
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v20, v2

    if-eqz v2, :cond_6

    int-to-float v2, v7

    mul-float v2, v2, v20

    float-to-int v7, v2

    int-to-float v2, v8

    mul-float v2, v2, v20

    float-to-int v8, v2

    :cond_6
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v7, v8, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    if-eqz v22, :cond_a

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->renderDrawableToBitmap(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;IIII)V

    :goto_2
    return-object v4

    :cond_7
    const/16 v22, 0x0

    goto :goto_0

    :cond_8
    move-object/from16 v0, p0

    iget v2, v0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mVidgetCellWidth:I

    mul-int v7, p4, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mVidgetCellHeight:I

    mul-int v8, p5, v2

    move/from16 v0, p4

    move/from16 v1, p5

    if-ne v0, v1, :cond_3

    move-object/from16 v0, p0

    iget v2, v0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mAppIconSize:I

    int-to-float v2, v2

    const/high16 v5, 0x3e800000    # 0.25f

    mul-float/2addr v2, v5

    float-to-int v0, v2

    move/from16 v17, v0

    const/4 v2, 0x1

    move/from16 v0, p4

    if-gt v0, v2, :cond_9

    move-object/from16 v0, p0

    iget v2, v0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mAppIconSize:I

    mul-int/lit8 v5, v17, 0x2

    add-int v8, v2, v5

    move v7, v8

    goto :goto_1

    :cond_9
    move-object/from16 v0, p0

    iget v2, v0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mAppIconSize:I

    mul-int/lit8 v5, v17, 0x4

    add-int v8, v2, v5

    move v7, v8

    goto :goto_1

    :cond_a
    move-object/from16 v0, p0

    iget v2, v0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mAppIconSize:I

    int-to-float v2, v2

    const/high16 v5, 0x3e800000    # 0.25f

    mul-float/2addr v2, v5

    float-to-int v0, v2

    move/from16 v17, v0

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v21

    move/from16 v0, v21

    int-to-float v2, v0

    move-object/from16 v0, p0

    iget v5, v0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mAppIconSize:I

    mul-int/lit8 v6, v17, 0x2

    add-int/2addr v5, v6

    int-to-float v5, v5

    div-float/2addr v2, v5

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-static {v2, v5}, Ljava/lang/Math;->min(FF)F

    move-result v16

    const/4 v2, 0x1

    move/from16 v0, p4

    if-ne v0, v2, :cond_b

    const/4 v2, 0x1

    move/from16 v0, p5

    if-eq v0, v2, :cond_c

    :cond_b
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mDefaultWidgetBackground:Landroid/graphics/drawable/Drawable;

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v9, p0

    move-object v11, v4

    move v14, v7

    move v15, v8

    invoke-direct/range {v9 .. v15}, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->renderDrawableToBitmap(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;IIII)V

    :cond_c
    const/4 v10, 0x0

    :try_start_0
    div-int/lit8 v2, v7, 0x2

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mAppIconSize:I

    int-to-float v5, v5

    mul-float v5, v5, v16

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    sub-float/2addr v2, v5

    float-to-int v12, v2

    div-int/lit8 v2, v8, 0x2

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mAppIconSize:I

    int-to-float v5, v5

    mul-float v5, v5, v16

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    sub-float/2addr v2, v5

    float-to-int v13, v2

    if-lez p3, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mIconCache:Lcom/cyanogenmod/trebuchet/IconCache;

    move-object/from16 v0, v18

    move/from16 v1, p3

    invoke-virtual {v2, v0, v1}, Lcom/cyanogenmod/trebuchet/IconCache;->getFullResIcon(Ljava/lang/String;I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    if-nez v10, :cond_e

    const v2, 0x7f02006f    # com.konka.avenger.R.drawable.ic_launcher_application

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    :cond_e
    move-object/from16 v0, p0

    iget v2, v0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mAppIconSize:I

    int-to-float v2, v2

    mul-float v2, v2, v16

    float-to-int v14, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mAppIconSize:I

    int-to-float v2, v2

    mul-float v2, v2, v16

    float-to-int v15, v2

    move-object/from16 v9, p0

    move-object v11, v4

    invoke-direct/range {v9 .. v15}, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->renderDrawableToBitmap(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;IIII)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_2

    :catch_0
    move-exception v2

    goto/16 :goto_2
.end method

.method private loadWidgetPreviewsInBackground(Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;)V
    .locals 16
    .param p1    # Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;
    .param p2    # Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;

    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->syncThreadPriority()V

    :cond_0
    move-object/from16 v0, p2

    iget-object v14, v0, Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;->items:Ljava/util/ArrayList;

    move-object/from16 v0, p2

    iget-object v11, v0, Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;->generatedImages:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_1
    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    return-void

    :cond_3
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    if-eqz p1, :cond_4

    invoke-virtual/range {p1 .. p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual/range {p1 .. p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->syncThreadPriority()V

    :cond_4
    instance-of v1, v13, Landroid/appwidget/AppWidgetProviderInfo;

    if-eqz v1, :cond_5

    move-object v12, v13

    check-cast v12, Landroid/appwidget/AppWidgetProviderInfo;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    const/4 v2, 0x0

    invoke-virtual {v1, v12, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->getSpanForWidget(Landroid/appwidget/AppWidgetProviderInfo;[I)[I

    move-result-object v10

    iget-object v2, v12, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    iget v3, v12, Landroid/appwidget/AppWidgetProviderInfo;->previewImage:I

    iget v4, v12, Landroid/appwidget/AppWidgetProviderInfo;->icon:I

    const/4 v1, 0x0

    aget v5, v10, v1

    const/4 v1, 0x1

    aget v6, v10, v1

    move-object/from16 v0, p2

    iget v7, v0, Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;->maxImageWidth:I

    move-object/from16 v0, p2

    iget v8, v0, Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;->maxImageHeight:I

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v8}, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->getWidgetPreview(Landroid/content/ComponentName;IIIIII)Landroid/graphics/Bitmap;

    move-result-object v9

    invoke-virtual {v11, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_5
    instance-of v1, v13, Landroid/content/pm/ResolveInfo;

    if-eqz v1, :cond_1

    move-object v12, v13

    check-cast v12, Landroid/content/pm/ResolveInfo;

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->getShortcutPreview(Landroid/content/pm/ResolveInfo;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private onSyncWidgetPageItems(Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;Lcom/cyanogenmod/trebuchet/PagedDialogGridView;)V
    .locals 10
    .param p1    # Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;
    .param p2    # Lcom/cyanogenmod/trebuchet/PagedDialogGridView;

    iget v3, p1, Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;->page:I

    iget-object v2, p1, Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;->items:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v0, :cond_0

    invoke-virtual {p2}, Lcom/cyanogenmod/trebuchet/PagedDialogGridView;->invalidate()V

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mRunningTasks:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_2

    return-void

    :cond_0
    invoke-virtual {p2, v1}, Lcom/cyanogenmod/trebuchet/PagedDialogGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/cyanogenmod/trebuchet/PagedViewWidget;

    if-eqz v7, :cond_1

    iget-object v8, p1, Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;->generatedImages:Ljava/util/ArrayList;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Bitmap;

    new-instance v8, Lcom/cyanogenmod/trebuchet/FastBitmapDrawable;

    invoke-direct {v8, v5}, Lcom/cyanogenmod/trebuchet/FastBitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v7, v8}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->applyPreview(Lcom/cyanogenmod/trebuchet/FastBitmapDrawable;)V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;

    iget v4, v6, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->page:I

    const/4 v9, 0x0

    invoke-static {v4, v9}, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->getThreadPriorityForPage(II)I

    move-result v9

    invoke-virtual {v6, v9}, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->setThreadPriority(I)V

    goto :goto_1
.end method

.method private renderDrawableToBitmap(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;IIII)V
    .locals 9
    .param p1    # Landroid/graphics/drawable/Drawable;
    .param p2    # Landroid/graphics/Bitmap;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v8, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v8}, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->renderDrawableToBitmap(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;IIIIFI)V

    return-void
.end method

.method private renderDrawableToBitmap(Landroid/graphics/drawable/Drawable;Landroid/graphics/Bitmap;IIIIFI)V
    .locals 4
    .param p1    # Landroid/graphics/drawable/Drawable;
    .param p2    # Landroid/graphics/Bitmap;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # F
    .param p8    # I

    if-eqz p2, :cond_1

    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, p2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, p7, p7}, Landroid/graphics/Canvas;->scale(FF)V

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->copyBounds()Landroid/graphics/Rect;

    move-result-object v1

    add-int v2, p3, p5

    add-int v3, p4, p6

    invoke-virtual {p1, p3, p4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    const/4 v2, -0x1

    if-eq p8, v2, :cond_0

    iget v2, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mDragViewMultiplyColor:I

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method protected prepareLoadWidgetPreviewsTask(ILjava/util/ArrayList;IIILcom/cyanogenmod/trebuchet/PagedDialogGridView;)V
    .locals 13
    .param p1    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # Lcom/cyanogenmod/trebuchet/PagedDialogGridView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;III",
            "Lcom/cyanogenmod/trebuchet/PagedDialogGridView;",
            ")V"
        }
    .end annotation

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mRunningTasks:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    move/from16 v0, p5

    invoke-direct {p0, p1, v0}, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->getSleepForPage(II)I

    move-result v9

    new-instance v1, Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;

    new-instance v6, Lcom/cyanogenmod/trebuchet/VidgetAdapter$1;

    invoke-direct {v6, p0, v9}, Lcom/cyanogenmod/trebuchet/VidgetAdapter$1;-><init>(Lcom/cyanogenmod/trebuchet/VidgetAdapter;I)V

    new-instance v7, Lcom/cyanogenmod/trebuchet/VidgetAdapter$2;

    move-object/from16 v0, p6

    invoke-direct {v7, p0, v0}, Lcom/cyanogenmod/trebuchet/VidgetAdapter$2;-><init>(Lcom/cyanogenmod/trebuchet/VidgetAdapter;Lcom/cyanogenmod/trebuchet/PagedDialogGridView;)V

    move v2, p1

    move-object v3, p2

    move/from16 v4, p3

    move/from16 v5, p4

    invoke-direct/range {v1 .. v7}, Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;-><init>(ILjava/util/ArrayList;IILcom/cyanogenmod/trebuchet/AsyncTaskCallback;Lcom/cyanogenmod/trebuchet/AsyncTaskCallback;)V

    new-instance v10, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;

    sget-object v2, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;->Widgets:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    sget-object v3, Lcom/cyanogenmod/trebuchet/AsyncTaskPageData$Type;->LoadWidgetPreviewData:Lcom/cyanogenmod/trebuchet/AsyncTaskPageData$Type;

    invoke-direct {v10, p1, v2, v3}, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;-><init>(ILcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;Lcom/cyanogenmod/trebuchet/AsyncTaskPageData$Type;)V

    move/from16 v0, p5

    invoke-static {p1, v0}, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->getThreadPriorityForPage(II)I

    move-result v2

    invoke-virtual {v10, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->setThreadPriority(I)V

    const-string v2, "VidgetAdapter"

    const-string v3, "new asynctask"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v2, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/cyanogenmod/trebuchet/AsyncTaskPageData;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-virtual {v10, v2, v3}, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->mRunningTasks:Ljava/util/ArrayList;

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void

    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;

    iget v12, v11, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->page:I

    if-ltz v12, :cond_1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->getCount()I

    move-result v2

    if-lt v12, v2, :cond_2

    :cond_1
    const/4 v2, 0x0

    invoke-virtual {v11, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->cancel(Z)Z

    invoke-interface {v8}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_2
    move/from16 v0, p5

    invoke-static {v12, v0}, Lcom/cyanogenmod/trebuchet/VidgetAdapter;->getThreadPriorityForPage(II)I

    move-result v2

    invoke-virtual {v11, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizeAsyncTask;->setThreadPriority(I)V

    goto :goto_0
.end method
