.class public Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;
.super Landroid/widget/FrameLayout;
.source "SearchDropTargetBar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/view/View$OnHoverListener;
.implements Lcom/cyanogenmod/trebuchet/DragController$DragListener;
.implements Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;
.implements Lcom/cyanogenmod/trebuchet/UsbMonitor$IUsbUpdateListener;
.implements Lcom/cyanogenmod/trebuchet/UserCenterMonitor$IUserCenterUpdateListener;
.implements Lnet/londatiga/android/QuickAction$OnActionItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;,
        Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;,
        Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;
    }
.end annotation


# static fields
.field private static final ID_QUICKACTION_ADD_APPS:I = 0x0

.field private static final ID_QUICKACTION_ADD_FOLDER:I = 0x2

.field private static final ID_QUICKACTION_ADD_SHORTCUT:I = 0x3

.field private static final ID_QUICKACTION_ADD_WIDGET:I = 0x1

.field private static final NUM_COLUMNS_APP:I = 0x7

.field private static final NUM_COLUMNS_SHORTCUT:I = 0x4

.field private static final NUM_COLUMNS_WIDGET:I = 0x4

.field private static final NUM_ROWS_APP:I = 0x3

.field private static final NUM_ROWS_SHORTCUT:I = 0x2

.field private static final NUM_ROWS_WIDGET:I = 0x2

.field private static final NUM_SHORTCUT_HSPAN:I = 0x3

.field private static final NUM_SHORTCUT_VSPAN:I = 0x3

.field private static final NUM_WIDGET_HSPAN:I = 0x3

.field private static final NUM_WIDGET_VSPAN:I = 0x3

.field private static final TAG:Ljava/lang/String; = "SearchDropTargetBar"

.field private static final WIFI_SIGNAL_STRENGTH:[[I

.field private static final sTransitionInDuration:I = 0xc8

.field private static final sTransitionOutDuration:I = 0xaf


# instance fields
.field private final mAddQuickAction:Lnet/londatiga/android/QuickAction;

.field private mAllAppsButton:Landroid/widget/ImageView;

.field private mApps:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/ApplicationInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mAppsDialog:Lgreendroid/widget/PagedDialog;

.field private mBarHeight:I

.field private mCellHeight:I

.field private mCellWidth:I

.field private final mChangeWallpaperWindow:Lnet/londatiga/android/ArrowedPopupWindow;

.field private mCurrentInterfaceId:I

.field private mCurrentInterfaceText:Ljava/lang/String;

.field private mDeferOnDragEnd:Z

.field private mDeleteDropTarget:Lcom/cyanogenmod/trebuchet/ButtonDropTarget;

.field private mDropTargetBar:Landroid/view/View;

.field private mDropTargetBarFadeInAnim:Landroid/animation/AnimatorSet;

.field private mDropTargetBarFadeOutAnim:Landroid/animation/AnimatorSet;

.field private mHomescreenAddButton:Landroid/widget/ImageView;

.field private mImages:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mInfoDropTarget:Lcom/cyanogenmod/trebuchet/ButtonDropTarget;

.field private mInitAddQuickAction:Z

.field private mInitChangeWallpaperWindow:Z

.field private mInitPackages:Z

.field private mIsSearchBarHidden:Z

.field private mKKSearchButton:Landroid/widget/ImageView;

.field private mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

.field private mNetworkStatusIndicator:Landroid/widget/ImageView;

.field private final mPackageManager:Landroid/content/pm/PackageManager;

.field private mPreviousBackground:Landroid/graphics/drawable/Drawable;

.field private mQSBSearchBar:Landroid/view/View;

.field private mQSBSearchBarFadeInAnim:Landroid/animation/ObjectAnimator;

.field private mQSBSearchBarFadeOutAnim:Landroid/animation/ObjectAnimator;

.field private mQSBStaticBar:Landroid/view/View;

.field private final mResources:Landroid/content/res/Resources;

.field private mSettingsButton:Landroid/widget/ImageView;

.field private mShortcuts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mShortcutsDialog:Lgreendroid/widget/PagedDialog;

.field private mShowQSBSearchBar:Z

.field private mSortMode:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

.field private final mTextFocusColor:I

.field private final mTextNormalColor:I

.field private mThumbs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mUsbStatusIndicator:Landroid/widget/TextView;

.field private mUserCenterButton:Landroid/widget/TextView;

.field private mWallpaperChangeButton:Landroid/widget/ImageView;

.field private mWidgetBlackList:[Ljava/lang/String;

.field private mWidgets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/appwidget/AppWidgetProviderInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mWidgetsDialog:Lgreendroid/widget/PagedDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x5

    const/4 v0, 0x2

    new-array v0, v0, [[I

    const/4 v1, 0x0

    new-array v2, v3, [I

    fill-array-data v2, :array_0

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-array v2, v3, [I

    fill-array-data v2, :array_1

    aput-object v2, v0, v1

    sput-object v0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->WIFI_SIGNAL_STRENGTH:[[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0200a4    # com.konka.avenger.R.drawable.qsb_network_icon_wifi_signal_0
        0x7f0200a5    # com.konka.avenger.R.drawable.qsb_network_icon_wifi_signal_1
        0x7f0200a6    # com.konka.avenger.R.drawable.qsb_network_icon_wifi_signal_2
        0x7f0200a7    # com.konka.avenger.R.drawable.qsb_network_icon_wifi_signal_3
        0x7f0200a8    # com.konka.avenger.R.drawable.qsb_network_icon_wifi_signal_4
    .end array-data

    :array_1
    .array-data 4
        0x7f0200a4    # com.konka.avenger.R.drawable.qsb_network_icon_wifi_signal_0
        0x7f0200a5    # com.konka.avenger.R.drawable.qsb_network_icon_wifi_signal_1
        0x7f0200a6    # com.konka.avenger.R.drawable.qsb_network_icon_wifi_signal_2
        0x7f0200a7    # com.konka.avenger.R.drawable.qsb_network_icon_wifi_signal_3
        0x7f0200a8    # com.konka.avenger.R.drawable.qsb_network_icon_wifi_signal_4
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/16 v3, 0x18

    const/4 v2, 0x3

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-boolean v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mDeferOnDragEnd:Z

    iput v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mCurrentInterfaceId:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mThumbs:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mImages:Ljava/util/ArrayList;

    sget-object v0, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;->Title:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mSortMode:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    invoke-static {p1}, Lcom/cyanogenmod/trebuchet/preference/PreferencesProvider$Interface$Homescreen;->getShowSearchBar(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mShowQSBSearchBar:Z

    new-instance v0, Lnet/londatiga/android/ArrowedPopupWindow;

    invoke-direct {v0, p1, v2}, Lnet/londatiga/android/ArrowedPopupWindow;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mChangeWallpaperWindow:Lnet/londatiga/android/ArrowedPopupWindow;

    new-instance v0, Lnet/londatiga/android/QuickAction;

    invoke-direct {v0, p1, v1, v2}, Lnet/londatiga/android/QuickAction;-><init>(Landroid/content/Context;II)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mAddQuickAction:Lnet/londatiga/android/QuickAction;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mPackageManager:Landroid/content/pm/PackageManager;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mApps:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWidgets:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mShortcuts:Ljava/util/List;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mResources:Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f080007    # com.konka.avenger.R.color.konka_orange

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mTextFocusColor:I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mResources:Landroid/content/res/Resources;

    const v1, 0x106000c    # android.R.color.black

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mTextNormalColor:I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0c0023    # com.konka.avenger.R.dimen.apps_customize_cell_width

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mCellWidth:I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0c0024    # com.konka.avenger.R.dimen.apps_customize_cell_height

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mCellHeight:I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0a00b1    # com.konka.avenger.R.string.hint_network_wired

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mCurrentInterfaceText:Ljava/lang/String;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f070006    # com.konka.avenger.R.array.widget_blacklist

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWidgetBlackList:[Ljava/lang/String;

    return-void
.end method

.method static synthetic access$0(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)I
    .locals 1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mTextNormalColor:I

    return v0
.end method

.method static synthetic access$1(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)I
    .locals 1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mTextFocusColor:I

    return v0
.end method

.method static synthetic access$10(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)Lnet/londatiga/android/ArrowedPopupWindow;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mChangeWallpaperWindow:Lnet/londatiga/android/ArrowedPopupWindow;

    return-object v0
.end method

.method static synthetic access$11(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)Lcom/cyanogenmod/trebuchet/Launcher;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    return-object v0
.end method

.method static synthetic access$12(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)Lgreendroid/widget/PagedDialog;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mAppsDialog:Lgreendroid/widget/PagedDialog;

    return-object v0
.end method

.method static synthetic access$13(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mDropTargetBar:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$14(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBStaticBar:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$15(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBSearchBar:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mApps:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)I
    .locals 1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mCellWidth:I

    return v0
.end method

.method static synthetic access$4(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)I
    .locals 1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mCellHeight:I

    return v0
.end method

.method static synthetic access$5(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mShortcuts:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$6(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)Lgreendroid/widget/PagedDialog;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mShortcutsDialog:Lgreendroid/widget/PagedDialog;

    return-object v0
.end method

.method static synthetic access$7(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWidgets:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$8(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)Lgreendroid/widget/PagedDialog;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWidgetsDialog:Lgreendroid/widget/PagedDialog;

    return-object v0
.end method

.method static synthetic access$9(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mImages:Ljava/util/ArrayList;

    return-object v0
.end method

.method private addAppsWithoutInvalidate(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/ApplicationInfo;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    return-void

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/ApplicationInfo;

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mSortMode:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    sget-object v4, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;->Title:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mApps:Ljava/util/ArrayList;

    sget-object v4, Lcom/cyanogenmod/trebuchet/LauncherModel;->APP_NAME_COMPARATOR:Ljava/util/Comparator;

    invoke-static {v3, v1, v4}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v0

    :cond_2
    :goto_1
    if-gez v0, :cond_0

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mApps:Ljava/util/ArrayList;

    add-int/lit8 v4, v0, 0x1

    neg-int v4, v4

    invoke-virtual {v3, v4, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mSortMode:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    sget-object v4, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;->InstallDate:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mApps:Ljava/util/ArrayList;

    sget-object v4, Lcom/cyanogenmod/trebuchet/LauncherModel;->APP_INSTALL_TIME_COMPARATOR:Ljava/util/Comparator;

    invoke-static {v3, v1, v4}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v0

    goto :goto_1
.end method

.method private addFolderInCurrentScreen()Z
    .locals 10

    const/4 v6, 0x1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->getWorkspace()Lcom/cyanogenmod/trebuchet/Workspace;

    move-result-object v9

    invoke-virtual {v9}, Lcom/cyanogenmod/trebuchet/Workspace;->getCurrentDropLayout()Lcom/cyanogenmod/trebuchet/CellLayout;

    move-result-object v1

    const/4 v0, 0x2

    new-array v7, v0, [I

    invoke-virtual {v1, v7, v6, v6}, Lcom/cyanogenmod/trebuchet/CellLayout;->findCellForSpan([III)Z

    move-result v8

    if-eqz v8, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    const-wide/16 v2, -0x64

    invoke-virtual {v9, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->indexOfChild(Landroid/view/View;)I

    move-result v4

    const/4 v5, 0x0

    aget v5, v7, v5

    aget v6, v7, v6

    invoke-virtual/range {v0 .. v6}, Lcom/cyanogenmod/trebuchet/Launcher;->addFolder(Lcom/cyanogenmod/trebuchet/CellLayout;JIII)Lcom/cyanogenmod/trebuchet/FolderIcon;

    :goto_0
    return v8

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->showOutOfSpaceMessage()V

    goto :goto_0
.end method

.method private cancelAnimations()V
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mDropTargetBarFadeInAnim:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mDropTargetBarFadeOutAnim:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBSearchBarFadeInAnim:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBSearchBarFadeOutAnim:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    return-void
.end method

.method private static filterAppWidget(Ljava/util/List;[Ljava/lang/String;)V
    .locals 8
    .param p1    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/appwidget/AppWidgetProviderInfo;",
            ">;[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v7, 0x2

    new-array v0, v7, [Ljava/lang/String;

    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    array-length v7, p1

    if-gtz v7, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v3, 0x0

    :goto_0
    array-length v7, p1

    if-ge v3, v7, :cond_0

    aget-object v7, p1, v3

    invoke-static {v7, v0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->unflattenFromString(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_3

    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v6

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v6, :cond_2

    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/appwidget/AppWidgetProviderInfo;

    iget-object v2, v7, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    const/4 v7, 0x0

    aget-object v7, v0, v7

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    const/4 v7, 0x1

    aget-object v7, v0, v7

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {p0, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    add-int/lit8 v4, v4, -0x1

    add-int/lit8 v6, v6, -0x1

    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method private findAppByComponent(Ljava/util/List;Lcom/cyanogenmod/trebuchet/ApplicationInfo;)I
    .locals 5
    .param p2    # Lcom/cyanogenmod/trebuchet/ApplicationInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/cyanogenmod/trebuchet/ApplicationInfo;",
            ">;",
            "Lcom/cyanogenmod/trebuchet/ApplicationInfo;",
            ")I"
        }
    .end annotation

    iget-object v4, p2, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->intent:Landroid/content/Intent;

    invoke-virtual {v4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v2, :cond_1

    const/4 v0, -0x1

    :cond_0
    return v0

    :cond_1
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/ApplicationInfo;

    iget-object v4, v1, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->intent:Landroid/content/Intent;

    invoke-virtual {v4}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private get3DFlag()I
    .locals 3

    new-instance v0, Lcom/konka/kkimplements/common/IniEditor;

    invoke-direct {v0}, Lcom/konka/kkimplements/common/IniEditor;-><init>()V

    const-string v1, "/customercfg/panel/panel.ini"

    invoke-virtual {v0, v1}, Lcom/konka/kkimplements/common/IniEditor;->loadFile(Ljava/lang/String;)Z

    const-string v1, "panel:b3DPanel"

    const-string v2, "0"

    invoke-virtual {v0, v1, v2}, Lcom/konka/kkimplements/common/IniEditor;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    return v1
.end method

.method private getAppsDialog()Lgreendroid/widget/PagedDialog;
    .locals 9

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mAppsDialog:Lgreendroid/widget/PagedDialog;

    if-nez v6, :cond_0

    new-instance v1, Lgreendroid/widget/PagedDialog$Builder;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v1, v6}, Lgreendroid/widget/PagedDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0a001b    # com.konka.avenger.R.string.menu_item_add_item

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f0a000d    # com.konka.avenger.R.string.dialog_positive

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v6, 0x7f0a000e    # com.konka.avenger.R.string.dialog_dismiss

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v0, p0, v6}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;-><init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;Landroid/content/Context;)V

    invoke-virtual {v1, v5}, Lgreendroid/widget/PagedDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lgreendroid/widget/PagedDialog$Builder;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Lgreendroid/widget/PagedDialog$Builder;->setCancelable(Z)Lgreendroid/widget/PagedDialog$Builder;

    move-result-object v6

    iget v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mCellWidth:I

    mul-int/lit8 v7, v7, 0x7

    add-int/lit8 v7, v7, 0x5a

    iget v8, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mCellHeight:I

    mul-int/lit8 v8, v8, 0x3

    add-int/lit8 v8, v8, 0x46

    invoke-virtual {v6, v7, v8}, Lgreendroid/widget/PagedDialog$Builder;->setContentLayout(II)Lgreendroid/widget/PagedDialog$Builder;

    move-result-object v6

    invoke-virtual {v6, v0}, Lgreendroid/widget/PagedDialog$Builder;->setAdapter(Lgreendroid/widget/PagedAdapter;)Lgreendroid/widget/PagedDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$3;

    invoke-direct {v7, p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$3;-><init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)V

    invoke-virtual {v6, v2, v7}, Lgreendroid/widget/PagedDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lgreendroid/widget/PagedDialog$Builder;

    move-result-object v6

    new-instance v7, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$4;

    invoke-direct {v7, p0, v0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$4;-><init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;)V

    invoke-virtual {v6, v3, v7}, Lgreendroid/widget/PagedDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lgreendroid/widget/PagedDialog$Builder;

    invoke-virtual {v1}, Lgreendroid/widget/PagedDialog$Builder;->create()Lgreendroid/widget/PagedDialog;

    move-result-object v6

    iput-object v6, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mAppsDialog:Lgreendroid/widget/PagedDialog;

    new-instance v6, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$5;

    invoke-direct {v6, p0, v0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$5;-><init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;)V

    invoke-virtual {v0, v6}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mAppsDialog:Lgreendroid/widget/PagedDialog;

    new-instance v7, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$6;

    invoke-direct {v7, p0, v0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$6;-><init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;)V

    invoke-virtual {v6, v7}, Lgreendroid/widget/PagedDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mAppsDialog:Lgreendroid/widget/PagedDialog;

    invoke-virtual {v6, v0}, Lgreendroid/widget/PagedDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    :cond_0
    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mAppsDialog:Lgreendroid/widget/PagedDialog;

    return-object v6
.end method

.method private getShortcutsDialog()Lgreendroid/widget/PagedDialog;
    .locals 14

    const/4 v6, 0x3

    const/4 v13, 0x1

    iget-boolean v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mInitPackages:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->onPackagesUpdated()V

    iput-boolean v13, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mInitPackages:Z

    :cond_0
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mShortcutsDialog:Lgreendroid/widget/PagedDialog;

    if-nez v1, :cond_1

    iget v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mCellWidth:I

    div-int/lit8 v4, v1, 0x2

    iget v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mCellHeight:I

    div-int/lit8 v5, v1, 0x2

    mul-int/lit8 v12, v4, 0x3

    mul-int/lit8 v11, v5, 0x3

    new-instance v8, Lgreendroid/widget/PagedDialog$Builder;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v8, v1}, Lgreendroid/widget/PagedDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v1, 0x7f0a001d    # com.konka.avenger.R.string.group_shortcuts

    invoke-virtual {v9, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    new-instance v0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    move-object v1, p0

    move v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;-><init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;Landroid/content/Context;Lcom/cyanogenmod/trebuchet/Launcher;IIII)V

    invoke-virtual {v8, v10}, Lgreendroid/widget/PagedDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lgreendroid/widget/PagedDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v13}, Lgreendroid/widget/PagedDialog$Builder;->setCancelable(Z)Lgreendroid/widget/PagedDialog$Builder;

    move-result-object v1

    mul-int/lit8 v2, v12, 0x4

    add-int/lit8 v2, v2, 0x3c

    mul-int/lit8 v3, v11, 0x2

    add-int/lit8 v3, v3, 0x32

    invoke-virtual {v1, v2, v3}, Lgreendroid/widget/PagedDialog$Builder;->setContentLayout(II)Lgreendroid/widget/PagedDialog$Builder;

    invoke-virtual {v8, v0}, Lgreendroid/widget/PagedDialog$Builder;->setAdapter(Lgreendroid/widget/PagedAdapter;)Lgreendroid/widget/PagedDialog$Builder;

    invoke-virtual {v8}, Lgreendroid/widget/PagedDialog$Builder;->create()Lgreendroid/widget/PagedDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mShortcutsDialog:Lgreendroid/widget/PagedDialog;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mShortcutsDialog:Lgreendroid/widget/PagedDialog;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lgreendroid/widget/PagedDialog;->setShowProgress(Z)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mShortcutsDialog:Lgreendroid/widget/PagedDialog;

    new-instance v2, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$7;

    invoke-direct {v2, p0, v0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$7;-><init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;)V

    invoke-virtual {v1, v2}, Lgreendroid/widget/PagedDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mShortcutsDialog:Lgreendroid/widget/PagedDialog;

    new-instance v2, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$8;

    invoke-direct {v2, p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$8;-><init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)V

    invoke-virtual {v1, v2}, Lgreendroid/widget/PagedDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    :cond_1
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mShortcutsDialog:Lgreendroid/widget/PagedDialog;

    return-object v1
.end method

.method private getWidgetsDialog()Lgreendroid/widget/PagedDialog;
    .locals 14

    const/4 v6, 0x3

    const/4 v13, 0x1

    iget-boolean v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mInitPackages:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->onPackagesUpdated()V

    iput-boolean v13, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mInitPackages:Z

    :cond_0
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWidgetsDialog:Lgreendroid/widget/PagedDialog;

    if-nez v1, :cond_1

    iget v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mCellWidth:I

    div-int/lit8 v4, v1, 0x2

    iget v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mCellHeight:I

    div-int/lit8 v5, v1, 0x2

    mul-int/lit8 v12, v4, 0x3

    mul-int/lit8 v11, v5, 0x3

    new-instance v8, Lgreendroid/widget/PagedDialog$Builder;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v8, v1}, Lgreendroid/widget/PagedDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v1, 0x7f0a001e    # com.konka.avenger.R.string.group_widgets

    invoke-virtual {v9, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    new-instance v0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    move-object v1, p0

    move v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;-><init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;Landroid/content/Context;Lcom/cyanogenmod/trebuchet/Launcher;IIII)V

    invoke-virtual {v8, v10}, Lgreendroid/widget/PagedDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Lgreendroid/widget/PagedDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v13}, Lgreendroid/widget/PagedDialog$Builder;->setCancelable(Z)Lgreendroid/widget/PagedDialog$Builder;

    move-result-object v1

    mul-int/lit8 v2, v12, 0x4

    add-int/lit8 v2, v2, 0x3c

    mul-int/lit8 v3, v11, 0x2

    add-int/lit8 v3, v3, 0x32

    invoke-virtual {v1, v2, v3}, Lgreendroid/widget/PagedDialog$Builder;->setContentLayout(II)Lgreendroid/widget/PagedDialog$Builder;

    invoke-virtual {v8, v0}, Lgreendroid/widget/PagedDialog$Builder;->setAdapter(Lgreendroid/widget/PagedAdapter;)Lgreendroid/widget/PagedDialog$Builder;

    invoke-virtual {v8}, Lgreendroid/widget/PagedDialog$Builder;->create()Lgreendroid/widget/PagedDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWidgetsDialog:Lgreendroid/widget/PagedDialog;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWidgetsDialog:Lgreendroid/widget/PagedDialog;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lgreendroid/widget/PagedDialog;->setShowProgress(Z)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWidgetsDialog:Lgreendroid/widget/PagedDialog;

    new-instance v2, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$9;

    invoke-direct {v2, p0, v0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$9;-><init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;)V

    invoke-virtual {v1, v2}, Lgreendroid/widget/PagedDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWidgetsDialog:Lgreendroid/widget/PagedDialog;

    new-instance v2, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$10;

    invoke-direct {v2, p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$10;-><init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)V

    invoke-virtual {v1, v2}, Lgreendroid/widget/PagedDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    :cond_1
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWidgetsDialog:Lgreendroid/widget/PagedDialog;

    return-object v1
.end method

.method private hideHint(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/DragLayer;

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/DragLayer;->hideHint(Landroid/view/View;)V

    return-void
.end method

.method private initAddQuickAction()V
    .locals 14

    const/4 v13, 0x1

    const v12, 0x7f02009b    # com.konka.avenger.R.drawable.qsb_add_quickaction_selector

    const v11, 0x106000b    # android.R.color.white

    iget-boolean v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mInitAddQuickAction:Z

    if-eqz v7, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-instance v0, Lnet/londatiga/android/ActionItem;

    const/4 v7, 0x0

    const v8, 0x7f0a001c    # com.konka.avenger.R.string.group_applications

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f020052    # com.konka.avenger.R.drawable.ic_add_apps

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-direct {v0, v7, v8, v9}, Lnet/londatiga/android/ActionItem;-><init>(ILjava/lang/String;Landroid/graphics/drawable/Drawable;)V

    new-instance v6, Lnet/londatiga/android/ActionItem;

    const v7, 0x7f0a001e    # com.konka.avenger.R.string.group_widgets

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f020055    # com.konka.avenger.R.drawable.ic_add_widget

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-direct {v6, v13, v7, v8}, Lnet/londatiga/android/ActionItem;-><init>(ILjava/lang/String;Landroid/graphics/drawable/Drawable;)V

    new-instance v2, Lnet/londatiga/android/ActionItem;

    const/4 v7, 0x2

    const v8, 0x7f0a000f    # com.konka.avenger.R.string.folder

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f020053    # com.konka.avenger.R.drawable.ic_add_folder

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-direct {v2, v7, v8, v9}, Lnet/londatiga/android/ActionItem;-><init>(ILjava/lang/String;Landroid/graphics/drawable/Drawable;)V

    new-instance v4, Lnet/londatiga/android/ActionItem;

    const/4 v7, 0x3

    const v8, 0x7f0a001d    # com.konka.avenger.R.string.group_shortcuts

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f020054    # com.konka.avenger.R.drawable.ic_add_shortcut

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    invoke-direct {v4, v7, v8, v9}, Lnet/londatiga/android/ActionItem;-><init>(ILjava/lang/String;Landroid/graphics/drawable/Drawable;)V

    const v1, 0x7f02009b    # com.konka.avenger.R.drawable.qsb_add_quickaction_selector

    const v5, 0x106000b    # android.R.color.white

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mAddQuickAction:Lnet/londatiga/android/QuickAction;

    invoke-virtual {v7, v0, v12, v11}, Lnet/londatiga/android/QuickAction;->addActionItem(Lnet/londatiga/android/ActionItem;II)V

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mAddQuickAction:Lnet/londatiga/android/QuickAction;

    invoke-virtual {v7, v6, v12, v11}, Lnet/londatiga/android/QuickAction;->addActionItem(Lnet/londatiga/android/ActionItem;II)V

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mAddQuickAction:Lnet/londatiga/android/QuickAction;

    invoke-virtual {v7, v2, v12, v11}, Lnet/londatiga/android/QuickAction;->addActionItem(Lnet/londatiga/android/ActionItem;II)V

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mAddQuickAction:Lnet/londatiga/android/QuickAction;

    invoke-virtual {v7, v4, v12, v11}, Lnet/londatiga/android/QuickAction;->addActionItem(Lnet/londatiga/android/ActionItem;II)V

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mAddQuickAction:Lnet/londatiga/android/QuickAction;

    invoke-virtual {v7, p0}, Lnet/londatiga/android/QuickAction;->setOnActionItemClickListener(Lnet/londatiga/android/QuickAction$OnActionItemClickListener;)V

    iput-boolean v13, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mInitAddQuickAction:Z

    goto/16 :goto_0
.end method

.method private initChangeWallpaperWindow()V
    .locals 13

    const/4 v12, 0x0

    const/4 v11, 0x1

    iget-boolean v8, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mInitChangeWallpaperWindow:Z

    if-eqz v8, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v8, 0x7f03002f    # com.konka.avenger.R.layout.wallpaper_chooser_popup

    invoke-virtual {v2, v8, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/GridLayout;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->getContext()Landroid/content/Context;

    move-result-object v8

    iget-object v9, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mThumbs:Ljava/util/ArrayList;

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mImages:Ljava/util/ArrayList;

    invoke-static {v8, v9, v10}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->findWallpapers(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    new-instance v7, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$1;

    invoke-direct {v7, p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$1;-><init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)V

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mThumbs:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    :goto_1
    if-lt v0, v4, :cond_1

    new-instance v3, Landroid/widget/GridLayout$LayoutParams;

    invoke-direct {v3}, Landroid/widget/GridLayout$LayoutParams;-><init>()V

    const/4 v8, -0x2

    iput v8, v3, Landroid/widget/GridLayout$LayoutParams;->height:I

    iput v8, v3, Landroid/widget/GridLayout$LayoutParams;->width:I

    invoke-virtual {v3, v11}, Landroid/widget/GridLayout$LayoutParams;->setGravity(I)V

    invoke-virtual {v6, v3}, Landroid/widget/GridLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mChangeWallpaperWindow:Lnet/londatiga/android/ArrowedPopupWindow;

    invoke-virtual {v8, v6}, Lnet/londatiga/android/ArrowedPopupWindow;->setContentView(Landroid/view/ViewGroup;)V

    iput-boolean v11, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mInitChangeWallpaperWindow:Z

    goto :goto_0

    :cond_1
    const v8, 0x7f030030    # com.konka.avenger.R.layout.wallpaper_item

    invoke-virtual {v2, v8, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    const v8, 0x7f0d0069    # com.konka.avenger.R.id.wallpaper_image

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mThumbs:Ljava/util/ArrayList;

    invoke-static {v1, v0, v8}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->setPreviewView(Landroid/widget/ImageView;ILjava/util/ArrayList;)V

    invoke-virtual {v5, v11}, Landroid/view/View;->setFocusable(Z)V

    add-int/lit8 v8, v4, -0x1

    if-eq v0, v8, :cond_2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v5, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_2
    invoke-virtual {v6, v5}, Landroid/widget/GridLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    new-instance v8, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$2;

    invoke-direct {v8, p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$2;-><init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)V

    invoke-virtual {v5, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2
.end method

.method private removeAppsWithoutInvalidate(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/ApplicationInfo;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    return-void

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/ApplicationInfo;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mApps:Ljava/util/ArrayList;

    invoke-direct {p0, v3, v0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->findAppByComponent(Ljava/util/List;Lcom/cyanogenmod/trebuchet/ApplicationInfo;)I

    move-result v1

    const/4 v3, -0x1

    if-le v1, v3, :cond_0

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mApps:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0
.end method

.method private showHint(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    const-wide/16 v3, 0x3e8

    const v6, 0x7f0d0009    # com.konka.avenger.R.id.hint_text

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/DragLayer;

    iget v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mCurrentInterfaceId:I

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mResources:Landroid/content/res/Resources;

    const v5, 0x7f0a00b1    # com.konka.avenger.R.string.hint_network_wired

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mCurrentInterfaceText:Ljava/lang/String;

    :goto_0
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mNetworkStatusIndicator:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mCurrentInterfaceText:Ljava/lang/String;

    invoke-virtual {v1, v6, v5}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    invoke-virtual {p1, v6}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object v1, p1

    move-wide v5, v3

    invoke-virtual/range {v0 .. v6}, Lcom/cyanogenmod/trebuchet/DragLayer;->showDelayedHint(Landroid/view/View;Ljava/lang/String;JJ)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mResources:Landroid/content/res/Resources;

    const v5, 0x7f0a00b2    # com.konka.avenger.R.string.hint_network_wireless

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mCurrentInterfaceText:Ljava/lang/String;

    goto :goto_0
.end method

.method private static unflattenFromString(Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 5
    .param p0    # Ljava/lang/String;
    .param p1    # [Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/16 v3, 0x2f

    invoke-virtual {p0, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ltz v0, :cond_0

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v3, v4, :cond_0

    array-length v3, p1

    const/4 v4, 0x2

    if-ge v3, v4, :cond_2

    :cond_0
    move v1, v2

    :cond_1
    :goto_0
    return v1

    :cond_2
    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    aput-object v3, p1, v2

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, p1, v1

    aget-object v3, p1, v1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    aget-object v3, p1, v1

    invoke-virtual {v3, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x2e

    if-ne v3, v4, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    aget-object v2, p1, v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v2, p1, v1

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, p1, v1

    goto :goto_0
.end method


# virtual methods
.method public addApps(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/ApplicationInfo;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->addAppsWithoutInvalidate(Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mAppsDialog:Lgreendroid/widget/PagedDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mAppsDialog:Lgreendroid/widget/PagedDialog;

    invoke-virtual {v0}, Lgreendroid/widget/PagedDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mAppsDialog:Lgreendroid/widget/PagedDialog;

    invoke-virtual {v0}, Lgreendroid/widget/PagedDialog;->getAdapter()Lgreendroid/widget/PagedAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lgreendroid/widget/PagedAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public deferOnDragEnd()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mDeferOnDragEnd:Z

    return-void
.end method

.method public getSearchBarBounds()Landroid/graphics/Rect;
    .locals 7

    const/4 v6, 0x1

    const/4 v4, 0x0

    const/high16 v5, 0x3f000000    # 0.5f

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBSearchBar:Landroid/view/View;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBSearchBar:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getCompatibilityInfo()Landroid/content/res/CompatibilityInfo;

    move-result-object v3

    iget v0, v3, Landroid/content/res/CompatibilityInfo;->applicationScale:F

    const/4 v3, 0x2

    new-array v1, v3, [I

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBSearchBar:Landroid/view/View;

    invoke-virtual {v3, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    aget v3, v1, v4

    int-to-float v3, v3

    mul-float/2addr v3, v0

    add-float/2addr v3, v5

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Rect;->left:I

    aget v3, v1, v6

    int-to-float v3, v3

    mul-float/2addr v3, v0

    add-float/2addr v3, v5

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Rect;->top:I

    aget v3, v1, v4

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBSearchBar:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    add-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v3, v0

    add-float/2addr v3, v5

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Rect;->right:I

    aget v3, v1, v6

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBSearchBar:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v3, v0

    add-float/2addr v3, v5

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getSortMode()Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mSortMode:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    return-object v0
.end method

.method public getTransitionInDuration()I
    .locals 1

    const/16 v0, 0xc8

    return v0
.end method

.method public getTransitionOutDuration()I
    .locals 1

    const/16 v0, 0xaf

    return v0
.end method

.method public hideSearchBar(Z)V
    .locals 2
    .param p1    # Z

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->cancelAnimations()V

    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mShowQSBSearchBar:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBSearchBarFadeOutAnim:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    :cond_0
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mIsSearchBarHidden:Z

    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mShowQSBSearchBar:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBSearchBar:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBSearchBar:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mHomescreenAddButton:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->onClickHomescreenAddButton()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWallpaperChangeButton:Landroid/widget/ImageView;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->onClickWallpaperChangeButton()V

    goto :goto_0
.end method

.method public onClickHomescreenAddButton()V
    .locals 4

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->getWorkspace()Lcom/cyanogenmod/trebuchet/Workspace;

    move-result-object v1

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Workspace;->getCurrentDropLayout()Lcom/cyanogenmod/trebuchet/CellLayout;

    move-result-object v0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->getWorkspace()Lcom/cyanogenmod/trebuchet/Workspace;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/cyanogenmod/trebuchet/Workspace;->isDefaultHomeScreen(Lcom/cyanogenmod/trebuchet/CellLayout;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->showNotAllowedAddMessage()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->initAddQuickAction()V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mAddQuickAction:Lnet/londatiga/android/QuickAction;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mHomescreenAddButton:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Lnet/londatiga/android/QuickAction;->show(Landroid/view/View;)V

    goto :goto_0
.end method

.method public onClickNetworkStatusIndicator(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    iget v2, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mCurrentInterfaceId:I

    if-nez v2, :cond_0

    const-string v0, "net_lan"

    :goto_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.konka.systemsetting.action.SHOW_MENU"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "menu_name"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v2, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->sendBroadcast(Landroid/content/Intent;)V

    return-void

    :cond_0
    const-string v0, "net_wireless"

    goto :goto_0
.end method

.method public onClickWallpaperChangeButton()V
    .locals 2

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->initChangeWallpaperWindow()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mChangeWallpaperWindow:Lnet/londatiga/android/ArrowedPopupWindow;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWallpaperChangeButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Lnet/londatiga/android/ArrowedPopupWindow;->show(Landroid/view/View;)V

    return-void
.end method

.method public onDragEnd()V
    .locals 3

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mDeferOnDragEnd:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBStaticBar:Landroid/view/View;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBStaticBar:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->buildLayer()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mDropTargetBarFadeInAnim:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mDropTargetBarFadeOutAnim:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mIsSearchBarHidden:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mShowQSBSearchBar:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBSearchBarFadeOutAnim:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBSearchBarFadeInAnim:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mDeferOnDragEnd:Z

    goto :goto_0
.end method

.method public onDragStart(Lcom/cyanogenmod/trebuchet/DragSource;Ljava/lang/Object;I)V
    .locals 4
    .param p1    # Lcom/cyanogenmod/trebuchet/DragSource;
    .param p2    # Ljava/lang/Object;
    .param p3    # I

    check-cast p1, Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mDropTargetBar:Landroid/view/View;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mDropTargetBar:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->buildLayer()V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mDropTargetBarFadeOutAnim:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->cancel()V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mDropTargetBarFadeInAnim:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->start()V

    :cond_0
    iget-boolean v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mIsSearchBarHidden:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mShowQSBSearchBar:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBSearchBarFadeInAnim:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->cancel()V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBSearchBarFadeOutAnim:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    :cond_1
    return-void
.end method

.method protected onFinishInflate()V
    .locals 15

    const/high16 v14, 0x3f800000    # 1.0f

    const/4 v13, 0x0

    const v10, 0x7f0d0009    # com.konka.avenger.R.id.hint_text

    const/4 v12, 0x1

    const/4 v11, 0x0

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    const v7, 0x7f0d0052    # com.konka.avenger.R.id.qsb_static_bar

    invoke-virtual {p0, v7}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBStaticBar:Landroid/view/View;

    const v7, 0x7f0d004e    # com.konka.avenger.R.id.qsb_search_bar

    invoke-virtual {p0, v7}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBSearchBar:Landroid/view/View;

    const v7, 0x7f0d004f    # com.konka.avenger.R.id.drag_target_bar

    invoke-virtual {p0, v7}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mDropTargetBar:Landroid/view/View;

    const v7, 0x7f0d0053    # com.konka.avenger.R.id.user_center_button

    invoke-virtual {p0, v7}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mUserCenterButton:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mUserCenterButton:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mResources:Landroid/content/res/Resources;

    const v9, 0x7f0a00aa    # com.konka.avenger.R.string.hint_user_center

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v10, v8}, Landroid/widget/TextView;->setTag(ILjava/lang/Object;)V

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mUserCenterButton:Landroid/widget/TextView;

    invoke-virtual {v7, p0}, Landroid/widget/TextView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mUserCenterButton:Landroid/widget/TextView;

    invoke-virtual {v7, p0}, Landroid/widget/TextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    const v7, 0x7f0d0054    # com.konka.avenger.R.id.homescreen_add_button

    invoke-virtual {p0, v7}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mHomescreenAddButton:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mHomescreenAddButton:Landroid/widget/ImageView;

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mResources:Landroid/content/res/Resources;

    const v9, 0x7f0a00ab    # com.konka.avenger.R.string.hint_homescreen_add

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v10, v8}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mHomescreenAddButton:Landroid/widget/ImageView;

    invoke-virtual {v7, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mHomescreenAddButton:Landroid/widget/ImageView;

    invoke-virtual {v7, p0}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mHomescreenAddButton:Landroid/widget/ImageView;

    invoke-virtual {v7, p0}, Landroid/widget/ImageView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    const v7, 0x7f0d0055    # com.konka.avenger.R.id.all_apps_button

    invoke-virtual {p0, v7}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mAllAppsButton:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mAllAppsButton:Landroid/widget/ImageView;

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mResources:Landroid/content/res/Resources;

    const v9, 0x7f0a00ac    # com.konka.avenger.R.string.hint_all_apps

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v10, v8}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mAllAppsButton:Landroid/widget/ImageView;

    invoke-virtual {v7, p0}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mAllAppsButton:Landroid/widget/ImageView;

    invoke-virtual {v7, p0}, Landroid/widget/ImageView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    const v7, 0x7f0d0056    # com.konka.avenger.R.id.wallpaper_change_button

    invoke-virtual {p0, v7}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWallpaperChangeButton:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWallpaperChangeButton:Landroid/widget/ImageView;

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mResources:Landroid/content/res/Resources;

    const v9, 0x7f0a00ad    # com.konka.avenger.R.string.hint_change_wallpaper

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v10, v8}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWallpaperChangeButton:Landroid/widget/ImageView;

    invoke-virtual {v7, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWallpaperChangeButton:Landroid/widget/ImageView;

    invoke-virtual {v7, p0}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWallpaperChangeButton:Landroid/widget/ImageView;

    invoke-virtual {v7, p0}, Landroid/widget/ImageView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    const v7, 0x7f0d0057    # com.konka.avenger.R.id.kk_search_button

    invoke-virtual {p0, v7}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mKKSearchButton:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mKKSearchButton:Landroid/widget/ImageView;

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mResources:Landroid/content/res/Resources;

    const v9, 0x7f0a00ae    # com.konka.avenger.R.string.hint_kk_search

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v10, v8}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mKKSearchButton:Landroid/widget/ImageView;

    invoke-virtual {v7, p0}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mKKSearchButton:Landroid/widget/ImageView;

    invoke-virtual {v7, p0}, Landroid/widget/ImageView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    const v7, 0x7f0d0058    # com.konka.avenger.R.id.settings_button

    invoke-virtual {p0, v7}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mSettingsButton:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mSettingsButton:Landroid/widget/ImageView;

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mResources:Landroid/content/res/Resources;

    const v9, 0x7f0a00af    # com.konka.avenger.R.string.hint_settings

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v10, v8}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mSettingsButton:Landroid/widget/ImageView;

    invoke-virtual {v7, p0}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mSettingsButton:Landroid/widget/ImageView;

    invoke-virtual {v7, p0}, Landroid/widget/ImageView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    const v7, 0x7f0d0059    # com.konka.avenger.R.id.usb_status_indicator

    invoke-virtual {p0, v7}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iput-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mUsbStatusIndicator:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mUsbStatusIndicator:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mResources:Landroid/content/res/Resources;

    const v9, 0x7f0a00b0    # com.konka.avenger.R.string.hint_usb_indicator

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v10, v8}, Landroid/widget/TextView;->setTag(ILjava/lang/Object;)V

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mUsbStatusIndicator:Landroid/widget/TextView;

    invoke-virtual {v7, p0}, Landroid/widget/TextView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mUsbStatusIndicator:Landroid/widget/TextView;

    invoke-virtual {v7, p0}, Landroid/widget/TextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    const v7, 0x7f0d005a    # com.konka.avenger.R.id.network_status_indicator

    invoke-virtual {p0, v7}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    iput-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mNetworkStatusIndicator:Landroid/widget/ImageView;

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mNetworkStatusIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7, p0}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mNetworkStatusIndicator:Landroid/widget/ImageView;

    invoke-virtual {v7, p0}, Landroid/widget/ImageView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mDropTargetBar:Landroid/view/View;

    const v8, 0x7f0d0051    # com.konka.avenger.R.id.info_target_text

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/cyanogenmod/trebuchet/ButtonDropTarget;

    iput-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mInfoDropTarget:Lcom/cyanogenmod/trebuchet/ButtonDropTarget;

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mDropTargetBar:Landroid/view/View;

    const v8, 0x7f0d0050    # com.konka.avenger.R.id.delete_target_text

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Lcom/cyanogenmod/trebuchet/ButtonDropTarget;

    iput-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mDeleteDropTarget:Lcom/cyanogenmod/trebuchet/ButtonDropTarget;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0c0006    # com.konka.avenger.R.dimen.qsb_bar_height

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    iput v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mBarHeight:I

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mInfoDropTarget:Lcom/cyanogenmod/trebuchet/ButtonDropTarget;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mInfoDropTarget:Lcom/cyanogenmod/trebuchet/ButtonDropTarget;

    invoke-virtual {v7, p0}, Lcom/cyanogenmod/trebuchet/ButtonDropTarget;->setSearchDropTargetBar(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)V

    :cond_0
    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mDeleteDropTarget:Lcom/cyanogenmod/trebuchet/ButtonDropTarget;

    invoke-virtual {v7, p0}, Lcom/cyanogenmod/trebuchet/ButtonDropTarget;->setSearchDropTargetBar(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f090007    # com.konka.avenger.R.bool.config_useDropTargetDownTransition

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    iget-boolean v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mShowQSBSearchBar:Z

    if-nez v7, :cond_1

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBSearchBar:Landroid/view/View;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mDropTargetBar:Landroid/view/View;

    invoke-virtual {v7, v13}, Landroid/view/View;->setAlpha(F)V

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mDropTargetBar:Landroid/view/View;

    const-string v8, "alpha"

    new-array v9, v12, [F

    aput v14, v9, v11

    invoke-static {v7, v8, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    new-instance v7, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v7}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v7}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v7, Landroid/animation/AnimatorSet;

    invoke-direct {v7}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mDropTargetBarFadeInAnim:Landroid/animation/AnimatorSet;

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mDropTargetBarFadeInAnim:Landroid/animation/AnimatorSet;

    invoke-virtual {v7, v1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v2

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBStaticBar:Landroid/view/View;

    const-string v8, "alpha"

    new-array v9, v12, [F

    aput v13, v9, v11

    invoke-static {v7, v8, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    new-instance v7, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v7}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v6, v7}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {v2, v6}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    if-eqz v0, :cond_2

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mDropTargetBar:Landroid/view/View;

    iget v8, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mBarHeight:I

    neg-int v8, v8

    int-to-float v8, v8

    invoke-virtual {v7, v8}, Landroid/view/View;->setTranslationY(F)V

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mDropTargetBar:Landroid/view/View;

    const-string v8, "translationY"

    new-array v9, v12, [F

    aput v13, v9, v11

    invoke-static {v7, v8, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBStaticBar:Landroid/view/View;

    const-string v8, "translationY"

    new-array v9, v12, [F

    iget v10, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mBarHeight:I

    neg-int v10, v10

    int-to-float v10, v10

    aput v10, v9, v11

    invoke-static {v7, v8, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v7

    invoke-virtual {v2, v7}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    :cond_2
    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mDropTargetBarFadeInAnim:Landroid/animation/AnimatorSet;

    const-wide/16 v8, 0xc8

    invoke-virtual {v7, v8, v9}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mDropTargetBarFadeInAnim:Landroid/animation/AnimatorSet;

    new-instance v8, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$11;

    invoke-direct {v8, p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$11;-><init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)V

    invoke-virtual {v7, v8}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mDropTargetBar:Landroid/view/View;

    const-string v8, "alpha"

    new-array v9, v12, [F

    aput v13, v9, v11

    invoke-static {v7, v8, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    new-instance v7, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v7}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v3, v7}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v7, Landroid/animation/AnimatorSet;

    invoke-direct {v7}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mDropTargetBarFadeOutAnim:Landroid/animation/AnimatorSet;

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mDropTargetBarFadeOutAnim:Landroid/animation/AnimatorSet;

    invoke-virtual {v7, v3}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v4

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBStaticBar:Landroid/view/View;

    const-string v8, "alpha"

    new-array v9, v12, [F

    aput v14, v9, v11

    invoke-static {v7, v8, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    new-instance v7, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v7}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v5, v7}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {v4, v5}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    if-eqz v0, :cond_3

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mDropTargetBar:Landroid/view/View;

    const-string v8, "translationY"

    new-array v9, v12, [F

    iget v10, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mBarHeight:I

    neg-int v10, v10

    int-to-float v10, v10

    aput v10, v9, v11

    invoke-static {v7, v8, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBStaticBar:Landroid/view/View;

    const-string v8, "translationY"

    new-array v9, v12, [F

    aput v13, v9, v11

    invoke-static {v7, v8, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    :cond_3
    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mDropTargetBarFadeOutAnim:Landroid/animation/AnimatorSet;

    const-wide/16 v8, 0xaf

    invoke-virtual {v7, v8, v9}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mDropTargetBarFadeOutAnim:Landroid/animation/AnimatorSet;

    new-instance v8, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$12;

    invoke-direct {v8, p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$12;-><init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)V

    invoke-virtual {v7, v8}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBSearchBar:Landroid/view/View;

    const-string v8, "alpha"

    new-array v9, v12, [F

    aput v14, v9, v11

    invoke-static {v7, v8, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v7

    iput-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBSearchBarFadeInAnim:Landroid/animation/ObjectAnimator;

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBSearchBarFadeInAnim:Landroid/animation/ObjectAnimator;

    const-wide/16 v8, 0xc8

    invoke-virtual {v7, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBSearchBarFadeInAnim:Landroid/animation/ObjectAnimator;

    new-instance v8, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$13;

    invoke-direct {v8, p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$13;-><init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)V

    invoke-virtual {v7, v8}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBSearchBar:Landroid/view/View;

    const-string v8, "alpha"

    new-array v9, v12, [F

    aput v13, v9, v11

    invoke-static {v7, v8, v9}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v7

    iput-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBSearchBarFadeOutAnim:Landroid/animation/ObjectAnimator;

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBSearchBarFadeOutAnim:Landroid/animation/ObjectAnimator;

    const-wide/16 v8, 0xaf

    invoke-virtual {v7, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBSearchBarFadeOutAnim:Landroid/animation/ObjectAnimator;

    new-instance v8, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$14;

    invoke-direct {v8, p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$14;-><init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)V

    invoke-virtual {v7, v8}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Z

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->getWorkspace()Lcom/cyanogenmod/trebuchet/Workspace;

    move-result-object v1

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Workspace;->getOpenFolder()Lcom/cyanogenmod/trebuchet/Folder;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Folder;->isEditingName()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Folder;->dismissEditingName()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Folder;->setFocusOnFirstChild()V

    goto :goto_0

    :cond_1
    if-nez p2, :cond_2

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->hideHint(Landroid/view/View;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->showHint(Landroid/view/View;)V

    goto :goto_0
.end method

.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->isWorkspaceVisible()Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-virtual {p1, p2}, Landroid/view/View;->onHoverEvent(Landroid/view/MotionEvent;)Z

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->showHint(Landroid/view/View;)V

    move v0, v1

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1, p2}, Landroid/view/View;->onHoverEvent(Landroid/view/MotionEvent;)Z

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->hideHint(Landroid/view/View;)V

    move v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onItemClick(Lnet/londatiga/android/QuickAction;II)V
    .locals 1
    .param p1    # Lnet/londatiga/android/QuickAction;
    .param p2    # I
    .param p3    # I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mAddQuickAction:Lnet/londatiga/android/QuickAction;

    if-ne p1, v0, :cond_0

    packed-switch p3, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->getAppsDialog()Lgreendroid/widget/PagedDialog;

    move-result-object v0

    invoke-virtual {v0}, Lgreendroid/widget/PagedDialog;->show()V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->getWidgetsDialog()Lgreendroid/widget/PagedDialog;

    move-result-object v0

    invoke-virtual {v0}, Lgreendroid/widget/PagedDialog;->show()V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->addFolderInCurrentScreen()Z

    goto :goto_0

    :pswitch_3
    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->getShortcutsDialog()Lgreendroid/widget/PagedDialog;

    move-result-object v0

    invoke-virtual {v0}, Lgreendroid/widget/PagedDialog;->show()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onPackagesUpdated()V
    .locals 3

    new-instance v0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$15;

    invoke-direct {v0, p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$15;-><init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)V

    const-wide/16 v1, 0x1f4

    invoke-virtual {p0, v0, v1, v2}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public onSearchPackagesChanged(ZZ)V
    .locals 3
    .param p1    # Z
    .param p2    # Z

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBSearchBar:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBSearchBar:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    if-nez p2, :cond_1

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mPreviousBackground:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBSearchBar:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mPreviousBackground:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    if-nez p1, :cond_2

    if-eqz p2, :cond_0

    :cond_2
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBSearchBar:Landroid/view/View;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mPreviousBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public onUpdateNetworkConnectivity(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x0

    const-string v3, "NetworkMonitor.interfaceId"

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mCurrentInterfaceId:I

    const-string v3, "NetworkMonitor.Status"

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iget v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mCurrentInterfaceId:I

    packed-switch v3, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    packed-switch v1, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mNetworkStatusIndicator:Landroid/widget/ImageView;

    const v4, 0x7f0200a2    # com.konka.avenger.R.drawable.qsb_network_icon_eth_disconnected

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :pswitch_2
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mNetworkStatusIndicator:Landroid/widget/ImageView;

    const v4, 0x7f0200a1    # com.konka.avenger.R.drawable.qsb_network_icon_eth_connected

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :pswitch_3
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mNetworkStatusIndicator:Landroid/widget/ImageView;

    const v4, 0x7f0200a3    # com.konka.avenger.R.drawable.qsb_network_icon_eth_unreachable

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :pswitch_4
    const-string v3, "NetworkMonitor.wifi.Condiction"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const-string v3, "NetworkMonitor.wifi.level"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mNetworkStatusIndicator:Landroid/widget/ImageView;

    sget-object v4, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->WIFI_SIGNAL_STRENGTH:[[I

    aget-object v4, v4, v0

    aget v4, v4, v2

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public onUpdateUsbConnectivity(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;

    const/4 v4, 0x0

    const-string v2, "UsbMonitor.isUsbConnected"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mUsbStatusIndicator:Landroid/widget/TextView;

    const v3, 0x7f0200ac    # com.konka.avenger.R.drawable.qsb_usb_icon_connected

    invoke-virtual {v2, v3, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    const-string v2, "UsbMonitor.usbCount"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mUsbStatusIndicator:Landroid/widget/TextView;

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mUsbStatusIndicator:Landroid/widget/TextView;

    const v3, 0x7f0200ad    # com.konka.avenger.R.drawable.qsb_usb_icon_disconnected

    invoke-virtual {v2, v3, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mUsbStatusIndicator:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onUpdateUserCenterConnectivity(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    const/4 v3, 0x0

    const-string v2, "UserCenterMonitor.isLoggedIn"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    const-string v2, "UserCenterMonitor.msgCount"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    if-eqz v0, :cond_2

    if-lez v1, :cond_1

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mUserCenterButton:Landroid/widget/TextView;

    const/16 v3, -0xa

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mUserCenterButton:Landroid/widget/TextView;

    const/16 v2, 0xa

    if-le v1, v2, :cond_0

    const-string v2, "10+"

    :goto_0
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    return-void

    :cond_0
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mUserCenterButton:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mUserCenterButton:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mUserCenterButton:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mUserCenterButton:Landroid/widget/TextView;

    const-string v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public onUpdateWifiActivity(I)V
    .locals 0
    .param p1    # I

    return-void
.end method

.method public removeApps(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/ApplicationInfo;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->removeAppsWithoutInvalidate(Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mAppsDialog:Lgreendroid/widget/PagedDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mAppsDialog:Lgreendroid/widget/PagedDialog;

    invoke-virtual {v0}, Lgreendroid/widget/PagedDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mAppsDialog:Lgreendroid/widget/PagedDialog;

    invoke-virtual {v0}, Lgreendroid/widget/PagedDialog;->getAdapter()Lgreendroid/widget/PagedAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lgreendroid/widget/PagedAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public setApps(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/ApplicationInfo;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mApps:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mSortMode:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    sget-object v1, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;->Title:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mApps:Ljava/util/ArrayList;

    sget-object v1, Lcom/cyanogenmod/trebuchet/LauncherModel;->APP_NAME_COMPARATOR:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mAppsDialog:Lgreendroid/widget/PagedDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mAppsDialog:Lgreendroid/widget/PagedDialog;

    invoke-virtual {v0}, Lgreendroid/widget/PagedDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mAppsDialog:Lgreendroid/widget/PagedDialog;

    invoke-virtual {v0}, Lgreendroid/widget/PagedDialog;->getAdapter()Lgreendroid/widget/PagedAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lgreendroid/widget/PagedAdapter;->notifyDataSetChanged()V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mSortMode:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    sget-object v1, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;->InstallDate:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mApps:Ljava/util/ArrayList;

    sget-object v1, Lcom/cyanogenmod/trebuchet/LauncherModel;->APP_INSTALL_TIME_COMPARATOR:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method public setSortMode(Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;)V
    .locals 2
    .param p1    # Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mSortMode:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    if-ne v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mSortMode:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mSortMode:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    sget-object v1, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;->Title:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mApps:Ljava/util/ArrayList;

    sget-object v1, Lcom/cyanogenmod/trebuchet/LauncherModel;->APP_NAME_COMPARATOR:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mAppsDialog:Lgreendroid/widget/PagedDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mAppsDialog:Lgreendroid/widget/PagedDialog;

    invoke-virtual {v0}, Lgreendroid/widget/PagedDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mAppsDialog:Lgreendroid/widget/PagedDialog;

    invoke-virtual {v0}, Lgreendroid/widget/PagedDialog;->getAdapter()Lgreendroid/widget/PagedAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lgreendroid/widget/PagedAdapter;->notifyDataSetChanged()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mSortMode:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    sget-object v1, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;->InstallDate:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$SortMode;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mApps:Ljava/util/ArrayList;

    sget-object v1, Lcom/cyanogenmod/trebuchet/LauncherModel;->APP_INSTALL_TIME_COMPARATOR:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_1
.end method

.method public setup(Lcom/cyanogenmod/trebuchet/Launcher;Lcom/cyanogenmod/trebuchet/DragController;)V
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/Launcher;
    .param p2    # Lcom/cyanogenmod/trebuchet/DragController;

    invoke-virtual {p2, p0}, Lcom/cyanogenmod/trebuchet/DragController;->addDragListener(Lcom/cyanogenmod/trebuchet/DragController$DragListener;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mInfoDropTarget:Lcom/cyanogenmod/trebuchet/ButtonDropTarget;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mInfoDropTarget:Lcom/cyanogenmod/trebuchet/ButtonDropTarget;

    invoke-virtual {p2, v0}, Lcom/cyanogenmod/trebuchet/DragController;->addDragListener(Lcom/cyanogenmod/trebuchet/DragController$DragListener;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mInfoDropTarget:Lcom/cyanogenmod/trebuchet/ButtonDropTarget;

    invoke-virtual {p2, v0}, Lcom/cyanogenmod/trebuchet/DragController;->addDropTarget(Lcom/cyanogenmod/trebuchet/DropTarget;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mInfoDropTarget:Lcom/cyanogenmod/trebuchet/ButtonDropTarget;

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/ButtonDropTarget;->setLauncher(Lcom/cyanogenmod/trebuchet/Launcher;)V

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mDeleteDropTarget:Lcom/cyanogenmod/trebuchet/ButtonDropTarget;

    invoke-virtual {p2, v0}, Lcom/cyanogenmod/trebuchet/DragController;->addDragListener(Lcom/cyanogenmod/trebuchet/DragController$DragListener;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mDeleteDropTarget:Lcom/cyanogenmod/trebuchet/ButtonDropTarget;

    invoke-virtual {p2, v0}, Lcom/cyanogenmod/trebuchet/DragController;->addDropTarget(Lcom/cyanogenmod/trebuchet/DropTarget;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mDeleteDropTarget:Lcom/cyanogenmod/trebuchet/ButtonDropTarget;

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/ButtonDropTarget;->setLauncher(Lcom/cyanogenmod/trebuchet/Launcher;)V

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    return-void
.end method

.method public showSearchBar(Z)V
    .locals 3
    .param p1    # Z

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->cancelAnimations()V

    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mShowQSBSearchBar:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBSearchBarFadeInAnim:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    :cond_0
    :goto_0
    iput-boolean v2, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mIsSearchBarHidden:Z

    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mShowQSBSearchBar:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBSearchBar:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mQSBSearchBar:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method

.method public updateApps(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/ApplicationInfo;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->removeAppsWithoutInvalidate(Ljava/util/ArrayList;)V

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->addAppsWithoutInvalidate(Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mAppsDialog:Lgreendroid/widget/PagedDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mAppsDialog:Lgreendroid/widget/PagedDialog;

    invoke-virtual {v0}, Lgreendroid/widget/PagedDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mAppsDialog:Lgreendroid/widget/PagedDialog;

    invoke-virtual {v0}, Lgreendroid/widget/PagedDialog;->getAdapter()Lgreendroid/widget/PagedAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lgreendroid/widget/PagedAdapter;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public updatePackages()V
    .locals 6

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWidgets:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-static {v3}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/appwidget/AppWidgetManager;->getInstalledProviders()Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWidgets:Ljava/util/List;

    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWidgets:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v0, v3, :cond_2

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWidgets:Ljava/util/List;

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWidgetBlackList:[Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->filterAppWidget(Ljava/util/List;[Ljava/lang/String;)V

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWidgets:Ljava/util/List;

    new-instance v4, Lcom/cyanogenmod/trebuchet/LauncherModel$WidgetAndShortcutNameComparator;

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-direct {v4, v5}, Lcom/cyanogenmod/trebuchet/LauncherModel$WidgetAndShortcutNameComparator;-><init>(Landroid/content/pm/PackageManager;)V

    invoke-static {v3, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    const-string v3, "SearchDropTargetBar"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onPackagesUpdated: widgets num="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWidgets:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWidgets:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_4

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mShortcuts:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.CREATE_SHORTCUT"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mPackageManager:Landroid/content/pm/PackageManager;

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v3

    iput-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mShortcuts:Ljava/util/List;

    const-string v3, "SearchDropTargetBar"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "onPackagesUpdated: shortcuts num="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mShortcuts:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mShortcuts:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_5

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->get3DFlag()I

    move-result v3

    if-nez v3, :cond_0

    const/4 v0, 0x0

    :goto_3
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mShortcuts:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v0, v3, :cond_6

    :cond_0
    :goto_4
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mShortcuts:Ljava/util/List;

    new-instance v4, Lcom/cyanogenmod/trebuchet/LauncherModel$WidgetAndShortcutNameComparator;

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mPackageManager:Landroid/content/pm/PackageManager;

    invoke-direct {v4, v5}, Lcom/cyanogenmod/trebuchet/LauncherModel$WidgetAndShortcutNameComparator;-><init>(Landroid/content/pm/PackageManager;)V

    invoke-static {v3, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWidgetsDialog:Lgreendroid/widget/PagedDialog;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWidgetsDialog:Lgreendroid/widget/PagedDialog;

    invoke-virtual {v3}, Lgreendroid/widget/PagedDialog;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWidgetsDialog:Lgreendroid/widget/PagedDialog;

    invoke-virtual {v3}, Lgreendroid/widget/PagedDialog;->getAdapter()Lgreendroid/widget/PagedAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lgreendroid/widget/PagedAdapter;->notifyDataSetChanged()V

    :cond_1
    :goto_5
    return-void

    :cond_2
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWidgets:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/appwidget/AppWidgetProviderInfo;

    iget-object v3, v3, Landroid/appwidget/AppWidgetProviderInfo;->label:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "KonkaTvWidget"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWidgets:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/appwidget/AppWidgetProviderInfo;

    const-string v4, "SearchDropTargetBar"

    iget-object v5, v1, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v5}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_5
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    const-string v4, "SearchDropTargetBar"

    iget-object v5, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_6
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mShortcuts:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/ResolveInfo;

    iget-object v3, v3, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    const-string v4, "com.konka.tvsettings.shortcuts.ThreeDSet"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mShortcuts:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_4

    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_3

    :cond_8
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mShortcutsDialog:Lgreendroid/widget/PagedDialog;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mShortcutsDialog:Lgreendroid/widget/PagedDialog;

    invoke-virtual {v3}, Lgreendroid/widget/PagedDialog;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mShortcutsDialog:Lgreendroid/widget/PagedDialog;

    invoke-virtual {v3}, Lgreendroid/widget/PagedDialog;->getAdapter()Lgreendroid/widget/PagedAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lgreendroid/widget/PagedAdapter;->notifyDataSetChanged()V

    goto :goto_5
.end method
