.class public abstract Lcom/cyanogenmod/trebuchet/PagedView;
.super Landroid/view/ViewGroup;
.source "PagedView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cyanogenmod/trebuchet/PagedView$PageSwitchListener;,
        Lcom/cyanogenmod/trebuchet/PagedView$SavedState;,
        Lcom/cyanogenmod/trebuchet/PagedView$ScrollInterpolator;
    }
.end annotation


# static fields
.field protected static final CHOICE_MODE_MULTIPLE:I = 0x2

.field protected static final CHOICE_MODE_NONE:I = 0x0

.field protected static final CHOICE_MODE_SINGLE:I = 0x1

.field private static final DEBUG:Z = false

.field protected static final INVALID_PAGE:I = -0x1

.field protected static final INVALID_POINTER:I = -0x1

.field private static final MINIMUM_SNAP_VELOCITY:I = 0x898

.field private static final MIN_FLING_VELOCITY:I = 0xfa

.field private static final MIN_LENGTH_FOR_FLING:I = 0x19

.field private static final OVERSCROLL_ACCELERATE_FACTOR:F = 2.0f

.field private static final OVERSCROLL_DAMP_FACTOR:F = 0.14f

.field private static final PAGE_SNAP_ANIMATION_DURATION:I = 0x226

.field private static final RETURN_TO_ORIGINAL_PAGE_THRESHOLD:F = 0.33f

.field private static final SIGNIFICANT_MOVE_THRESHOLD:F = 0.4f

.field private static final TAG:Ljava/lang/String; = "PagedView"

.field protected static final TOUCH_STATE_NEXT_PAGE:I = 0x3

.field protected static final TOUCH_STATE_PREV_PAGE:I = 0x2

.field protected static final TOUCH_STATE_REST:I = 0x0

.field protected static final TOUCH_STATE_SCROLLING:I = 0x1

.field protected static final sScrollIndicatorFadeInDuration:I = 0x96

.field protected static final sScrollIndicatorFadeOutDuration:I = 0x28a

.field protected static final sScrollIndicatorFadeOutShortDuration:I = 0x96

.field protected static final sScrollIndicatorFlashDuration:I = 0x28a


# instance fields
.field hideScrollingIndicatorRunnable:Ljava/lang/Runnable;

.field private mActionMode:Landroid/view/ActionMode;

.field protected mActivePointerId:I

.field protected mAllowLongPress:Z

.field protected mAllowOverScroll:Z

.field protected mCellCountX:I

.field protected mCellCountY:I

.field protected mCenterPagesVertically:Z

.field private mChildOffsets:[I

.field private mChildOffsetsWithLayoutScale:[I

.field private mChildRelativeOffsets:[I

.field protected mChoiceMode:I

.field protected mClearDirtyPages:Z

.field protected mContentIsRefreshable:Z

.field protected mCurrentPage:I

.field private mDeferLoadAssociatedPagesUntilScrollCompletes:Z

.field protected mDeferScrollUpdate:Z

.field protected mDensity:F

.field protected mDirtyPageContent:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mDownMotionX:F

.field protected mFadeInAdjacentScreens:Z

.field protected mFirstLayout:Z

.field protected mForceScreenScrolled:Z

.field protected mHandleFadeInAdjacentScreens:Z

.field private mHasScrollIndicator:Z

.field protected mIsDataReady:Z

.field protected mIsPageMoving:Z

.field protected mLastMotionX:F

.field protected mLastMotionXRemainder:F

.field protected mLastMotionY:F

.field private mLastScreenScroll:I

.field protected mLayoutScale:F

.field protected mLongClickListener:Landroid/view/View$OnLongClickListener;

.field protected mMaxScrollX:I

.field private mMaximumVelocity:I

.field private mMinimumWidth:I

.field protected mNextPage:I

.field protected mOverScrollX:I

.field protected mPageLayoutHeightGap:I

.field protected mPageLayoutPaddingBottom:I

.field protected mPageLayoutPaddingLeft:I

.field protected mPageLayoutPaddingRight:I

.field protected mPageLayoutPaddingTop:I

.field protected mPageLayoutWidthGap:I

.field protected mPageSpacing:I

.field private mPageSwitchListener:Lcom/cyanogenmod/trebuchet/PagedView$PageSwitchListener;

.field private mPagingTouchSlop:I

.field private mScrollIndicator:Landroid/view/View;

.field protected mScroller:Landroid/widget/Scroller;

.field protected mSnapVelocity:I

.field protected mTempVisiblePagesRange:[I

.field protected mTotalMotionX:F

.field protected mTouchSlop:I

.field protected mTouchState:I

.field protected mTouchX:F

.field protected mUnboundedScrollX:I

.field protected mUsePagingTouchSlop:Z

.field private mVelocityTracker:Landroid/view/VelocityTracker;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/cyanogenmod/trebuchet/PagedView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/cyanogenmod/trebuchet/PagedView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v5, 0x2

    const/4 v4, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/16 v1, 0x1f4

    iput v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mSnapVelocity:I

    iput-boolean v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mFirstLayout:Z

    iput v4, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mNextPage:I

    iput v4, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mLastScreenScroll:I

    iput v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mTouchState:I

    iput-boolean v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mForceScreenScrolled:Z

    iput-boolean v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mAllowLongPress:Z

    iput v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCellCountX:I

    iput v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCellCountY:I

    iput-boolean v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mAllowOverScroll:Z

    new-array v1, v5, [I

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mTempVisiblePagesRange:[I

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mLayoutScale:F

    iput v4, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mActivePointerId:I

    iput-boolean v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mContentIsRefreshable:Z

    iput-boolean v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mFadeInAdjacentScreens:Z

    iput-boolean v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mHandleFadeInAdjacentScreens:Z

    iput-boolean v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mUsePagingTouchSlop:Z

    iput-boolean v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mDeferScrollUpdate:Z

    iput-boolean v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mIsPageMoving:Z

    iput-boolean v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mIsDataReady:Z

    iput-boolean v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mClearDirtyPages:Z

    iput-boolean v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mHasScrollIndicator:Z

    new-instance v1, Lcom/cyanogenmod/trebuchet/PagedView$1;

    invoke-direct {v1, p0}, Lcom/cyanogenmod/trebuchet/PagedView$1;-><init>(Lcom/cyanogenmod/trebuchet/PagedView;)V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->hideScrollingIndicatorRunnable:Ljava/lang/Runnable;

    iput v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mChoiceMode:I

    sget-object v1, Lcom/konka/avenger/R$styleable;->PagedView:[I

    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/PagedView;->setPageSpacing(I)V

    invoke-virtual {v0, v5, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mPageLayoutPaddingTop:I

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mPageLayoutPaddingBottom:I

    const/4 v1, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mPageLayoutPaddingLeft:I

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mPageLayoutPaddingRight:I

    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mPageLayoutWidthGap:I

    invoke-virtual {v0, v3, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mPageLayoutHeightGap:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/PagedView;->setHapticFeedbackEnabled(Z)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->init()V

    return-void
.end method

.method private acquireVelocityTrackerAndAddMovement(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-nez v0, :cond_0

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    return-void
.end method

.method private onSecondaryPointerUp(Landroid/view/MotionEvent;)V
    .locals 5
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    const v4, 0xff00

    and-int/2addr v3, v4

    shr-int/lit8 v2, v3, 0x8

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    iget v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mActivePointerId:I

    if-ne v1, v3, :cond_0

    if-nez v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    iput v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mDownMotionX:F

    iput v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mLastMotionX:F

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    iput v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mLastMotionY:F

    const/4 v3, 0x0

    iput v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mLastMotionXRemainder:F

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v3

    iput v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mActivePointerId:I

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v3}, Landroid/view/VelocityTracker;->clear()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private overScrollInfluenceCurve(F)F
    .locals 2
    .param p1    # F

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr p1, v1

    mul-float v0, p1, p1

    mul-float/2addr v0, p1

    add-float/2addr v0, v1

    return v0
.end method

.method private releaseVelocityTracker()V
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    :cond_0
    return-void
.end method


# virtual methods
.method protected acceleratedOverScroll(F)V
    .locals 6
    .param p1    # F

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getMeasuredWidth()I

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    int-to-float v4, v2

    div-float v4, p1, v4

    mul-float v0, v3, v4

    cmpl-float v3, v0, v5

    if-nez v3, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v3

    div-float/2addr v0, v3

    :cond_1
    int-to-float v3, v2

    mul-float/2addr v3, v0

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v1

    cmpg-float v3, p1, v5

    if-gez v3, :cond_2

    iput v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mOverScrollX:I

    const/4 v3, 0x0

    iput v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollX:I

    :goto_1
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->invalidate()V

    goto :goto_0

    :cond_2
    iget v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mMaxScrollX:I

    add-int/2addr v3, v1

    iput v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mOverScrollX:I

    iget v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mMaxScrollX:I

    iput v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollX:I

    goto :goto_1
.end method

.method public addFocusables(Ljava/util/ArrayList;II)V
    .locals 2
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;II)V"
        }
    .end annotation

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;I)V

    :cond_0
    const/16 v0, 0x11

    if-ne p2, v0, :cond_2

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    if-lez v0, :cond_1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/16 v0, 0x42

    if-ne p2, v0, :cond_1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;I)V

    goto :goto_0
.end method

.method public allowLongPress()Z
    .locals 1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mAllowLongPress:Z

    return v0
.end method

.method protected animateClickFeedback(Landroid/view/View;Ljava/lang/Runnable;)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Ljava/lang/Runnable;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mContext:Landroid/content/Context;

    const v2, 0x7f040002    # com.konka.avenger.R.anim.paged_view_click_feedback

    invoke-static {v1, v2}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v0

    check-cast v0, Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p1}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    new-instance v1, Lcom/cyanogenmod/trebuchet/PagedView$2;

    invoke-direct {v1, p0, p2}, Lcom/cyanogenmod/trebuchet/PagedView$2;-><init>(Lcom/cyanogenmod/trebuchet/PagedView;Ljava/lang/Runnable;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    return-void
.end method

.method protected cancelCurrentPageLongPress()V
    .locals 2

    iget-boolean v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mAllowLongPress:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mAllowLongPress:Z

    iget v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->cancelLongPress()V

    :cond_0
    return-void
.end method

.method protected cancelScrollingIndicatorAnimations()V
    .locals 4

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollIndicator:Landroid/view/View;

    if-eqz v2, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollIndicator:Landroid/view/View;

    check-cast v1, Lcom/cyanogenmod/trebuchet/ScrollIndicator;

    invoke-interface {v1}, Lcom/cyanogenmod/trebuchet/ScrollIndicator;->cancelAnimations()V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v2, "PagedView"

    const-string v3, "scroll indicator should implement ScrollIndicator interface!"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public computeScroll()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, -0x1

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v1

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollX:I

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrX()I

    move-result v2

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollY:I

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrY()I

    move-result v2

    if-eq v1, v2, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getCurrX()I

    move-result v1

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v2}, Landroid/widget/Scroller;->getCurrY()I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/cyanogenmod/trebuchet/PagedView;->scrollTo(II)V

    :cond_1
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->invalidate()V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mNextPage:I

    if-eq v1, v3, :cond_2

    iget v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mNextPage:I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    iput v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mNextPage:I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->notifyPageSwitchListener()V

    iget-boolean v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mDeferLoadAssociatedPagesUntilScrollCompletes:Z

    if-eqz v1, :cond_4

    iget v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/PagedView;->loadAssociatedPages(I)V

    iput-boolean v4, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mDeferLoadAssociatedPagesUntilScrollCompletes:Z

    :cond_4
    iget v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mTouchState:I

    if-nez v1, :cond_5

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->pageEndMoving()V

    :cond_5
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v1, 0x1000

    invoke-static {v1}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getCurrentPageDescription()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_0
.end method

.method protected dampedOverScroll(F)V
    .locals 6
    .param p1    # F

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getMeasuredWidth()I

    move-result v2

    int-to-float v3, v2

    div-float v0, p1, v3

    cmpl-float v3, v0, v5

    if-nez v3, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v3

    div-float v3, v0, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v4

    invoke-direct {p0, v4}, Lcom/cyanogenmod/trebuchet/PagedView;->overScrollInfluenceCurve(F)F

    move-result v4

    mul-float v0, v3, v4

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v3

    div-float/2addr v0, v3

    :cond_1
    const v3, 0x3e0f5c29    # 0.14f

    mul-float/2addr v3, v0

    int-to-float v4, v2

    mul-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v1

    cmpg-float v3, p1, v5

    if-gez v3, :cond_2

    iput v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mOverScrollX:I

    const/4 v3, 0x0

    iput v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollX:I

    :goto_1
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->invalidate()V

    goto :goto_0

    :cond_2
    iget v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mMaxScrollX:I

    add-int/2addr v3, v1

    iput v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mOverScrollX:I

    iget v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mMaxScrollX:I

    iput v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollX:I

    goto :goto_1
.end method

.method protected determineScrollingStart(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, p1, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->determineScrollingStart(Landroid/view/MotionEvent;F)V

    return-void
.end method

.method protected determineScrollingStart(Landroid/view/MotionEvent;F)V
    .locals 11
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # F

    const/4 v8, 0x0

    const/4 v9, 0x1

    iget v10, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mActivePointerId:I

    invoke-virtual {p1, v10}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    const/4 v10, -0x1

    if-ne v0, v10, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v6

    iget v10, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mLastMotionX:F

    sub-float v10, v2, v10

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v10

    float-to-int v3, v10

    iget v10, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mLastMotionY:F

    sub-float v10, v6, v10

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v10

    float-to-int v7, v10

    iget v10, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mTouchSlop:I

    int-to-float v10, v10

    mul-float/2addr v10, p2

    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    move-result v1

    iget v10, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mPagingTouchSlop:I

    if-le v3, v10, :cond_5

    move v5, v9

    :goto_1
    if-le v3, v1, :cond_6

    move v4, v9

    :goto_2
    if-le v7, v1, :cond_2

    move v8, v9

    :cond_2
    if-nez v4, :cond_3

    if-nez v5, :cond_3

    if-eqz v8, :cond_0

    :cond_3
    iget-boolean v10, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mUsePagingTouchSlop:Z

    if-eqz v10, :cond_7

    if-eqz v5, :cond_4

    :goto_3
    iput v9, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mTouchState:I

    iget v9, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mTotalMotionX:F

    iget v10, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mLastMotionX:F

    sub-float/2addr v10, v2

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v10

    add-float/2addr v9, v10

    iput v9, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mTotalMotionX:F

    iput v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mLastMotionX:F

    const/4 v9, 0x0

    iput v9, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mLastMotionXRemainder:F

    iget v9, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollX:I

    int-to-float v9, v9

    iput v9, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mTouchX:F

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->pageBeginMoving()V

    :cond_4
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->cancelCurrentPageLongPress()V

    goto :goto_0

    :cond_5
    move v5, v8

    goto :goto_1

    :cond_6
    move v4, v8

    goto :goto_2

    :cond_7
    if-eqz v4, :cond_4

    goto :goto_3
.end method

.method protected disableScrollingIndicator()V
    .locals 2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollIndicator:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollIndicator:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mHasScrollIndicator:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollIndicator:Landroid/view/View;

    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 11
    .param p1    # Landroid/graphics/Canvas;

    const/4 v9, 0x0

    const/4 v8, -0x1

    iget v6, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mOverScrollX:I

    iget v7, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mLastScreenScroll:I

    if-ne v6, v7, :cond_0

    iget-boolean v6, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mForceScreenScrolled:Z

    if-eqz v6, :cond_1

    :cond_0
    iget v6, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mOverScrollX:I

    invoke-virtual {p0, v6}, Lcom/cyanogenmod/trebuchet/PagedView;->screenScrolled(I)V

    iget v6, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mOverScrollX:I

    iput v6, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mLastScreenScroll:I

    iput-boolean v9, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mForceScreenScrolled:Z

    :cond_1
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v4

    if-lez v4, :cond_2

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mTempVisiblePagesRange:[I

    invoke-virtual {p0, v6}, Lcom/cyanogenmod/trebuchet/PagedView;->getVisiblePages([I)V

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mTempVisiblePagesRange:[I

    aget v3, v6, v9

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mTempVisiblePagesRange:[I

    const/4 v7, 0x1

    aget v5, v6, v7

    if-eq v3, v8, :cond_2

    if-eq v5, v8, :cond_2

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getDrawingTime()J

    move-result-wide v0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget v6, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollX:I

    iget v7, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollY:I

    iget v8, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollX:I

    iget v9, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mRight:I

    add-int/2addr v8, v9

    iget v9, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mLeft:I

    sub-int/2addr v8, v9

    iget v9, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollY:I

    iget v10, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mBottom:I

    add-int/2addr v9, v10

    iget v10, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mTop:I

    sub-int/2addr v9, v10

    invoke-virtual {p1, v6, v7, v8, v9}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    move v2, v5

    :goto_0
    if-ge v2, v3, :cond_3

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_2
    return-void

    :cond_3
    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {p0, p1, v6, v0, v1}, Lcom/cyanogenmod/trebuchet/PagedView;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    add-int/lit8 v2, v2, -0x1

    goto :goto_0
.end method

.method public dispatchUnhandledMove(Landroid/view/View;I)Z
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I

    const/4 v0, 0x1

    const/16 v1, 0x11

    if-ne p2, v1, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getCurrentPage()I

    move-result v1

    if-lez v1, :cond_1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getCurrentPage()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/PagedView;->snapToPage(I)V

    :goto_0
    return v0

    :cond_0
    const/16 v1, 0x42

    if-ne p2, v1, :cond_1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getCurrentPage()I

    move-result v1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getCurrentPage()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/PagedView;->snapToPage(I)V

    goto :goto_0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->dispatchUnhandledMove(Landroid/view/View;I)Z

    move-result v0

    goto :goto_0
.end method

.method distanceInfluenceForSnapDuration(F)F
    .locals 4
    .param p1    # F

    const/high16 v0, 0x3f000000    # 0.5f

    sub-float/2addr p1, v0

    float-to-double v0, p1

    const-wide v2, 0x3fde28c7460698c7L    # 0.4712389167638204

    mul-double/2addr v0, v2

    double-to-float p1, v0

    float-to-double v0, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method protected enableScrollingIndicator()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mHasScrollIndicator:Z

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getScrollingIndicator()Landroid/view/View;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollIndicator:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollIndicator:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public endChoiceMode()V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/PagedView;->isChoiceMode(I)Z

    move-result v0

    if-nez v0, :cond_1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mChoiceMode:I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->resetCheckedGrandchildren()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mActionMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mActionMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mActionMode:Landroid/view/ActionMode;

    :cond_1
    return-void
.end method

.method protected flashScrollingIndicator(Z)V
    .locals 3
    .param p1    # Z

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->hideScrollingIndicatorRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->removeCallbacks(Ljava/lang/Runnable;)Z

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->showScrollingIndicator(Z)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->hideScrollingIndicatorRunnable:Ljava/lang/Runnable;

    const-wide/16 v1, 0x28a

    invoke-virtual {p0, v0, v1, v2}, Lcom/cyanogenmod/trebuchet/PagedView;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public focusableViewAvailable(Landroid/view/View;)V
    .locals 4
    .param p1    # Landroid/view/View;

    iget v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    invoke-virtual {p0, v3}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    move-object v2, p1

    :goto_0
    if-ne v2, v0, :cond_1

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->focusableViewAvailable(Landroid/view/View;)V

    :cond_0
    return-void

    :cond_1
    if-eq v2, p0, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    instance-of v3, v1, Landroid/view/View;

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    goto :goto_0
.end method

.method protected getAssociatedLowerPageBound(I)I
    .locals 2
    .param p1    # I

    const/4 v0, 0x0

    add-int/lit8 v1, p1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method protected getAssociatedUpperPageBound(I)I
    .locals 3
    .param p1    # I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v0

    add-int/lit8 v1, p1, 0x1

    add-int/lit8 v2, v0, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    return v1
.end method

.method protected getCheckedGrandchildren()Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/Checkable;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v1

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v1, :cond_0

    return-object v0

    :cond_0
    invoke-virtual {p0, v3}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/cyanogenmod/trebuchet/Page;

    invoke-interface {v5}, Lcom/cyanogenmod/trebuchet/Page;->getPageChildCount()I

    move-result v2

    const/4 v4, 0x0

    :goto_1
    if-lt v4, v2, :cond_1

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    invoke-interface {v5, v4}, Lcom/cyanogenmod/trebuchet/Page;->getChildOnPageAt(I)Landroid/view/View;

    move-result-object v6

    instance-of v7, v6, Landroid/widget/Checkable;

    if-eqz v7, :cond_2

    move-object v7, v6

    check-cast v7, Landroid/widget/Checkable;

    invoke-interface {v7}, Landroid/widget/Checkable;->isChecked()Z

    move-result v7

    if-eqz v7, :cond_2

    check-cast v6, Landroid/widget/Checkable;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method protected getChildIndexForRelativeOffset(I)I
    .locals 5
    .param p1    # I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v0, :cond_1

    const/4 v1, -0x1

    :cond_0
    return v1

    :cond_1
    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/PagedView;->getRelativeChildOffset(I)I

    move-result v2

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/cyanogenmod/trebuchet/PagedView;->getScaledMeasuredWidth(Landroid/view/View;)I

    move-result v4

    add-int v3, v2, v4

    if-gt v2, p1, :cond_2

    if-le p1, v3, :cond_0

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected getChildOffset(I)I
    .locals 5
    .param p1    # I

    const/4 v2, 0x0

    iget v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mLayoutScale:F

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v3, v4}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-nez v3, :cond_1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mChildOffsets:[I

    :goto_0
    if-eqz v0, :cond_2

    aget v3, v0, p1

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    aget v2, v0, p1

    :cond_0
    :goto_1
    return v2

    :cond_1
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mChildOffsetsWithLayoutScale:[I

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/PagedView;->getRelativeChildOffset(I)I

    move-result v2

    const/4 v1, 0x0

    :goto_2
    if-lt v1, p1, :cond_3

    if-eqz v0, :cond_0

    aput v2, v0, p1

    goto :goto_1

    :cond_3
    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/cyanogenmod/trebuchet/PagedView;->getScaledMeasuredWidth(Landroid/view/View;)I

    move-result v3

    iget v4, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mPageSpacing:I

    add-int/2addr v3, v4

    add-int/2addr v2, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method protected getChildWidth(I)I
    .locals 3
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mMinimumWidth:I

    if-le v1, v0, :cond_0

    :goto_0
    return v1

    :cond_0
    move v1, v0

    goto :goto_0
.end method

.method getCurrentPage()I
    .locals 1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    return v0
.end method

.method protected getCurrentPageDescription()Ljava/lang/String;
    .locals 5

    iget v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mNextPage:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mNextPage:I

    :goto_0
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mContext:Landroid/content/Context;

    const v2, 0x7f0a0053    # com.konka.avenger.R.string.default_scroll_format

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    add-int/lit8 v4, v0, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_0
    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    goto :goto_0
.end method

.method getPageAt(I)Landroid/view/View;
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method getPageCount()I
    .locals 1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v0

    return v0
.end method

.method public getPageForView(Landroid/view/View;)I
    .locals 5
    .param p1    # Landroid/view/View;

    const/4 v2, -0x1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v0, :cond_2

    :cond_0
    move v1, v2

    :cond_1
    return v1

    :cond_2
    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v4

    if-eq v3, v4, :cond_1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method getPageNearestToCenterOfScreen()I
    .locals 12

    const v7, 0x7fffffff

    const/4 v8, -0x1

    iget v10, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollX:I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getMeasuredWidth()I

    move-result v11

    div-int/lit8 v11, v11, 0x2

    add-int v9, v10, v11

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v1

    const/4 v5, 0x0

    :goto_0
    if-lt v5, v1, :cond_0

    return v8

    :cond_0
    invoke-virtual {p0, v5}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/cyanogenmod/trebuchet/PagedView;->getScaledMeasuredWidth(Landroid/view/View;)I

    move-result v2

    div-int/lit8 v4, v2, 0x2

    invoke-virtual {p0, v5}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildOffset(I)I

    move-result v10

    add-int v0, v10, v4

    sub-int v10, v0, v9

    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    move-result v3

    if-ge v3, v7, :cond_1

    move v7, v3

    move v8, v5

    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method

.method protected getRelativeChildOffset(I)I
    .locals 5
    .param p1    # I

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mChildRelativeOffsets:[I

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mChildRelativeOffsets:[I

    array-length v2, v2

    if-ge p1, v2, :cond_1

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mChildRelativeOffsets:[I

    aget v2, v2, p1

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mChildRelativeOffsets:[I

    aget v0, v2, p1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mPaddingLeft:I

    iget v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mPaddingRight:I

    add-int v1, v2, v3

    iget v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mPaddingLeft:I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getMeasuredWidth()I

    move-result v3

    sub-int/2addr v3, v1

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildWidth(I)I

    move-result v4

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    add-int v0, v2, v3

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mChildRelativeOffsets:[I

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mChildRelativeOffsets:[I

    aput v0, v2, p1

    goto :goto_0
.end method

.method protected getScaledMeasuredWidth(Landroid/view/View;)I
    .locals 5
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mMinimumWidth:I

    if-le v2, v1, :cond_0

    move v0, v2

    :goto_0
    int-to-float v3, v0

    iget v4, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mLayoutScale:F

    mul-float/2addr v3, v4

    const/high16 v4, 0x3f000000    # 0.5f

    add-float/2addr v3, v4

    float-to-int v3, v3

    return v3

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method protected getScaledRelativeChildOffset(I)I
    .locals 5
    .param p1    # I

    iget v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mPaddingLeft:I

    iget v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mPaddingRight:I

    add-int v1, v2, v3

    iget v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mPaddingLeft:I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getMeasuredWidth()I

    move-result v3

    sub-int/2addr v3, v1

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/cyanogenmod/trebuchet/PagedView;->getScaledMeasuredWidth(Landroid/view/View;)I

    move-result v4

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    add-int v0, v2, v3

    return v0
.end method

.method protected getScrollProgress(ILandroid/view/View;I)F
    .locals 8
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # I

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getMeasuredWidth()I

    move-result v5

    div-int/lit8 v1, v5, 0x2

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getMeasuredWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    add-int v2, p1, v5

    invoke-virtual {p0, p2}, Lcom/cyanogenmod/trebuchet/PagedView;->getScaledMeasuredWidth(Landroid/view/View;)I

    move-result v5

    iget v6, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mPageSpacing:I

    add-int v4, v5, v6

    invoke-virtual {p0, p3}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildOffset(I)I

    move-result v5

    invoke-virtual {p0, p3}, Lcom/cyanogenmod/trebuchet/PagedView;->getRelativeChildOffset(I)I

    move-result v6

    sub-int/2addr v5, v6

    add-int/2addr v5, v1

    sub-int v0, v2, v5

    int-to-float v5, v0

    int-to-float v6, v4

    mul-float/2addr v6, v7

    div-float v3, v5, v6

    invoke-static {v3, v7}, Ljava/lang/Math;->min(FF)F

    move-result v3

    const/high16 v5, -0x40800000    # -1.0f

    invoke-static {v3, v5}, Ljava/lang/Math;->max(FF)F

    move-result v3

    return v3
.end method

.method protected getScrollingIndicator()Landroid/view/View;
    .locals 6

    const/4 v4, 0x0

    iget-boolean v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mHasScrollIndicator:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollIndicator:Landroid/view/View;

    if-nez v3, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    const v3, 0x7f0d001b    # com.konka.avenger.R.id.paged_view_indicator

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollIndicator:Landroid/view/View;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollIndicator:Landroid/view/View;

    if-eqz v3, :cond_1

    const/4 v3, 0x1

    :goto_0
    iput-boolean v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mHasScrollIndicator:Z

    iget-boolean v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mHasScrollIndicator:Z

    if-eqz v3, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollIndicator:Landroid/view/View;

    check-cast v1, Lcom/cyanogenmod/trebuchet/ScrollIndicator;

    invoke-interface {v1, p0}, Lcom/cyanogenmod/trebuchet/ScrollIndicator;->init(Lcom/cyanogenmod/trebuchet/PagedView;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollIndicator:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollIndicator:Landroid/view/View;

    return-object v3

    :cond_1
    move v3, v4

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v3, "PagedView"

    const-string v5, "scroll indicator should implement ScrollIndicator interface!"

    invoke-static {v3, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method protected getSingleCheckedGrandchild()Landroid/widget/Checkable;
    .locals 8

    iget v6, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mChoiceMode:I

    const/4 v7, 0x2

    if-eq v6, v7, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v0

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v0, :cond_1

    :cond_0
    const/4 v5, 0x0

    :goto_1
    return-object v5

    :cond_1
    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/cyanogenmod/trebuchet/Page;

    invoke-interface {v4}, Lcom/cyanogenmod/trebuchet/Page;->getPageChildCount()I

    move-result v1

    const/4 v3, 0x0

    :goto_2
    if-lt v3, v1, :cond_2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    invoke-interface {v4, v3}, Lcom/cyanogenmod/trebuchet/Page;->getChildOnPageAt(I)Landroid/view/View;

    move-result-object v5

    instance-of v6, v5, Landroid/widget/Checkable;

    if-eqz v6, :cond_3

    move-object v6, v5

    check-cast v6, Landroid/widget/Checkable;

    invoke-interface {v6}, Landroid/widget/Checkable;->isChecked()Z

    move-result v6

    if-eqz v6, :cond_3

    check-cast v5, Landroid/widget/Checkable;

    goto :goto_1

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method

.method protected getVisiblePages([I)V
    .locals 10
    .param p1    # [I

    const/4 v9, 0x1

    const/4 v6, -0x1

    const/4 v8, 0x0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v1

    if-lez v1, :cond_4

    invoke-virtual {p0, v8}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/cyanogenmod/trebuchet/PagedView;->getScaledMeasuredWidth(Landroid/view/View;)I

    move-result v2

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getMeasuredWidth()I

    move-result v4

    invoke-virtual {p0, v8}, Lcom/cyanogenmod/trebuchet/PagedView;->getScaledRelativeChildOffset(I)I

    move-result v6

    add-int v5, v6, v2

    const/4 v0, 0x0

    const/4 v3, 0x0

    :goto_0
    iget v6, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollX:I

    if-gt v5, v6, :cond_0

    add-int/lit8 v6, v1, -0x1

    if-lt v0, v6, :cond_2

    :cond_0
    move v3, v0

    :goto_1
    iget v6, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollX:I

    add-int/2addr v6, v4

    if-ge v5, v6, :cond_1

    add-int/lit8 v6, v1, -0x1

    if-lt v3, v6, :cond_3

    :cond_1
    aput v0, p1, v8

    aput v3, p1, v9

    :goto_2
    return-void

    :cond_2
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/cyanogenmod/trebuchet/PagedView;->getScaledMeasuredWidth(Landroid/view/View;)I

    move-result v6

    iget v7, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mPageSpacing:I

    add-int/2addr v6, v7

    add-int/2addr v5, v6

    goto :goto_0

    :cond_3
    add-int/lit8 v3, v3, 0x1

    invoke-virtual {p0, v3}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/cyanogenmod/trebuchet/PagedView;->getScaledMeasuredWidth(Landroid/view/View;)I

    move-result v6

    iget v7, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mPageSpacing:I

    add-int/2addr v6, v7

    add-int/2addr v5, v6

    goto :goto_1

    :cond_4
    aput v6, p1, v8

    aput v6, p1, v9

    goto :goto_2
.end method

.method protected hasElasticScrollIndicator()Z
    .locals 5

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollIndicator:Landroid/view/View;

    if-nez v3, :cond_0

    :goto_0
    return v2

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollIndicator:Landroid/view/View;

    check-cast v1, Lcom/cyanogenmod/trebuchet/ScrollIndicator;

    invoke-interface {v1}, Lcom/cyanogenmod/trebuchet/ScrollIndicator;->isElasticScrollIndicator()Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v3, "PagedView"

    const-string v4, "scroll indicator should implement ScrollIndicator interface!"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected hideScrollingIndicator(Z)V
    .locals 1
    .param p1    # Z

    const/16 v0, 0x28a

    invoke-virtual {p0, p1, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->hideScrollingIndicator(ZI)V

    return-void
.end method

.method protected hideScrollingIndicator(ZI)V
    .locals 4
    .param p1    # Z
    .param p2    # I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->isScrollingIndicatorEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getScrollingIndicator()Landroid/view/View;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollIndicator:Landroid/view/View;

    if-eqz v2, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollIndicator:Landroid/view/View;

    check-cast v1, Lcom/cyanogenmod/trebuchet/ScrollIndicator;

    invoke-interface {v1, p1, p2}, Lcom/cyanogenmod/trebuchet/ScrollIndicator;->hide(ZI)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "PagedView"

    const-string v3, "scroll indicator should implement ScrollIndicator interface!"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected hitsNextPage(FF)Z
    .locals 2
    .param p1    # F
    .param p2    # F

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/PagedView;->getRelativeChildOffset(I)I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mPageSpacing:I

    add-int/2addr v0, v1

    int-to-float v0, v0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected hitsPreviousPage(FF)Z
    .locals 2
    .param p1    # F
    .param p2    # F

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->getRelativeChildOffset(I)I

    move-result v0

    iget v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mPageSpacing:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected indexToPage(I)I
    .locals 0
    .param p1    # I

    return p1
.end method

.method protected init()V
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mDirtyPageContent:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mDirtyPageContent:Ljava/util/ArrayList;

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->ensureCapacity(I)V

    new-instance v1, Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Lcom/cyanogenmod/trebuchet/PagedView$ScrollInterpolator;

    invoke-direct {v3}, Lcom/cyanogenmod/trebuchet/PagedView$ScrollInterpolator;-><init>()V

    invoke-direct {v1, v2, v3}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScroller:Landroid/widget/Scroller;

    const/4 v1, 0x0

    iput v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCenterPagesVertically:Z

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mTouchSlop:I

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledPagingTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mPagingTouchSlop:I

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mMaximumVelocity:I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    iput v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mDensity:F

    return-void
.end method

.method protected invalidateCachedOffsets()V
    .locals 4

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_1

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mChildOffsets:[I

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mChildRelativeOffsets:[I

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mChildOffsetsWithLayoutScale:[I

    :cond_0
    return-void

    :cond_1
    new-array v2, v0, [I

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mChildOffsets:[I

    new-array v2, v0, [I

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mChildRelativeOffsets:[I

    new-array v2, v0, [I

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mChildOffsetsWithLayoutScale:[I

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mChildOffsets:[I

    aput v3, v2, v1

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mChildRelativeOffsets:[I

    aput v3, v2, v1

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mChildOffsetsWithLayoutScale:[I

    aput v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected invalidatePageData()V
    .locals 2

    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/cyanogenmod/trebuchet/PagedView;->invalidatePageData(IZ)V

    return-void
.end method

.method protected invalidatePageData(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->invalidatePageData(IZ)V

    return-void
.end method

.method protected invalidatePageData(IZ)V
    .locals 7
    .param p1    # I
    .param p2    # Z

    const/high16 v6, 0x40000000    # 2.0f

    const/4 v5, 0x1

    const/4 v4, -0x1

    iget-boolean v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mIsDataReady:Z

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mContentIsRefreshable:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v2, v5}, Landroid/widget/Scroller;->forceFinished(Z)V

    iput v4, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mNextPage:I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->syncPages()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getMeasuredWidth()I

    move-result v2

    invoke-static {v2, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getMeasuredHeight()I

    move-result v3

    invoke-static {v3, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {p0, v2, v3}, Lcom/cyanogenmod/trebuchet/PagedView;->measure(II)V

    if-le p1, v4, :cond_2

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v2, p1}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/PagedView;->setCurrentPage(I)V

    :cond_2
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mDirtyPageContent:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    const/4 v1, 0x0

    :goto_1
    if-lt v1, v0, :cond_3

    iget v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    invoke-virtual {p0, v2, p2}, Lcom/cyanogenmod/trebuchet/PagedView;->loadAssociatedPages(IZ)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->requestLayout()V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mDirtyPageContent:Ljava/util/ArrayList;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method protected isChoiceMode(I)Z
    .locals 1
    .param p1    # I

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mChoiceMode:I

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isDataReady()Z
    .locals 1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mIsDataReady:Z

    return v0
.end method

.method protected isPageMoving()Z
    .locals 1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mIsPageMoving:Z

    return v0
.end method

.method protected isScrollingIndicatorEnabled()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected loadAssociatedPages(I)V
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->loadAssociatedPages(IZ)V

    return-void
.end method

.method protected loadAssociatedPages(IZ)V
    .locals 10
    .param p1    # I
    .param p2    # Z

    const/4 v7, 0x1

    const/4 v8, 0x0

    iget-boolean v6, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mContentIsRefreshable:Z

    if-eqz v6, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v1

    if-ge p1, v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedView;->getAssociatedLowerPageBound(I)I

    move-result v4

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedView;->getAssociatedUpperPageBound(I)I

    move-result v5

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v1, :cond_1

    :cond_0
    return-void

    :cond_1
    if-eq v2, p1, :cond_3

    if-eqz p2, :cond_3

    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/cyanogenmod/trebuchet/Page;

    invoke-interface {v3}, Lcom/cyanogenmod/trebuchet/Page;->getPageChildCount()I

    move-result v0

    if-gt v4, v2, :cond_5

    if-gt v2, v5, :cond_5

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mDirtyPageContent:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_2

    if-ne v2, p1, :cond_4

    if-eqz p2, :cond_4

    move v6, v7

    :goto_2
    invoke-virtual {p0, v2, v6}, Lcom/cyanogenmod/trebuchet/PagedView;->syncPageItems(IZ)V

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mDirtyPageContent:Ljava/util/ArrayList;

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v6, v2, v9}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_4
    move v6, v8

    goto :goto_2

    :cond_5
    iget-boolean v6, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mClearDirtyPages:Z

    if-eqz v6, :cond_2

    if-lez v0, :cond_6

    invoke-interface {v3}, Lcom/cyanogenmod/trebuchet/Page;->removeAllViewsOnPage()V

    :cond_6
    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mDirtyPageContent:Ljava/util/ArrayList;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v6, v2, v9}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method protected notifyPageSwitchListener()V
    .locals 3

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mPageSwitchListener:Lcom/cyanogenmod/trebuchet/PagedView$PageSwitchListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mPageSwitchListener:Lcom/cyanogenmod/trebuchet/PagedView$PageSwitchListener;

    iget v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v1

    iget v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    invoke-interface {v0, v1, v2}, Lcom/cyanogenmod/trebuchet/PagedView$PageSwitchListener;->onPageSwitch(Landroid/view/View;I)V

    :cond_0
    return-void
.end method

.method public onGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1    # Landroid/view/MotionEvent;

    const/16 v4, 0x9

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v2

    and-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    :goto_0
    return v2

    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v2

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_3

    const/4 v1, 0x0

    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v0

    :goto_1
    cmpl-float v2, v0, v3

    if-nez v2, :cond_1

    cmpl-float v2, v1, v3

    if-eqz v2, :cond_0

    :cond_1
    cmpl-float v2, v0, v3

    if-gtz v2, :cond_2

    cmpl-float v2, v1, v3

    if-lez v2, :cond_4

    :cond_2
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->scrollRight()V

    :goto_2
    const/4 v2, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {p1, v4}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v2

    neg-float v1, v2

    const/16 v2, 0xa

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v0

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->scrollLeft()V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
    .end packed-switch
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x1

    return v0
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2
    .param p1    # Landroid/view/accessibility/AccessibilityEvent;

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setScrollable(Z)V

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setFromIndex(I)V

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setToIndex(I)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setItemCount(I)V

    :cond_0
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1
    .param p1    # Landroid/view/accessibility/AccessibilityNodeInfo;

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setScrollable(Z)V

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1    # Landroid/view/MotionEvent;

    const/4 v10, -0x1

    const/4 v8, 0x0

    const/4 v9, 0x2

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedView;->acquireVelocityTrackerAndAddMovement(Landroid/view/MotionEvent;)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v7

    if-gtz v7, :cond_1

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v5

    :cond_0
    :goto_0
    return v5

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-ne v0, v9, :cond_2

    iget v7, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mTouchState:I

    if-eq v7, v5, :cond_0

    :cond_2
    and-int/lit16 v7, v0, 0xff

    packed-switch v7, :pswitch_data_0

    :cond_3
    :goto_1
    :pswitch_0
    iget v7, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mTouchState:I

    if-nez v7, :cond_0

    move v5, v6

    goto :goto_0

    :pswitch_1
    iget v7, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mActivePointerId:I

    if-eq v7, v10, :cond_4

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedView;->determineScrollingStart(Landroid/view/MotionEvent;)V

    goto :goto_1

    :cond_4
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    iput v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mDownMotionX:F

    iput v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mLastMotionX:F

    iput v4, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mLastMotionY:F

    iput v8, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mLastMotionXRemainder:F

    iput v8, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mTotalMotionX:F

    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v7

    iput v7, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mActivePointerId:I

    iput-boolean v5, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mAllowLongPress:Z

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v7}, Landroid/widget/Scroller;->getFinalX()I

    move-result v7

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v8}, Landroid/widget/Scroller;->getCurrX()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v3

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v7}, Landroid/widget/Scroller;->isFinished()Z

    move-result v7

    if-nez v7, :cond_5

    iget v7, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mTouchSlop:I

    if-lt v3, v7, :cond_5

    move v1, v6

    :goto_2
    if-eqz v1, :cond_6

    iput v6, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mTouchState:I

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v7}, Landroid/widget/Scroller;->abortAnimation()V

    :goto_3
    iget v7, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mTouchState:I

    if-eq v7, v9, :cond_3

    iget v7, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mTouchState:I

    const/4 v8, 0x3

    if-eq v7, v8, :cond_3

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v7

    if-lez v7, :cond_3

    invoke-virtual {p0, v2, v4}, Lcom/cyanogenmod/trebuchet/PagedView;->hitsPreviousPage(FF)Z

    move-result v7

    if-eqz v7, :cond_7

    iput v9, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mTouchState:I

    goto :goto_1

    :cond_5
    move v1, v5

    goto :goto_2

    :cond_6
    iput v5, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mTouchState:I

    goto :goto_3

    :cond_7
    invoke-virtual {p0, v2, v4}, Lcom/cyanogenmod/trebuchet/PagedView;->hitsNextPage(FF)Z

    move-result v7

    if-eqz v7, :cond_3

    const/4 v7, 0x3

    iput v7, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mTouchState:I

    goto :goto_1

    :pswitch_3
    iput v6, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mTouchState:I

    iput-boolean v6, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mAllowLongPress:Z

    iput v10, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mActivePointerId:I

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->releaseVelocityTracker()V

    goto :goto_1

    :pswitch_4
    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedView;->onSecondaryPointerUp(Landroid/view/MotionEvent;)V

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->releaseVelocityTracker()V

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 11
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    iget-boolean v9, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mIsDataReady:Z

    if-nez v9, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v9, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mPaddingTop:I

    iget v10, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mPaddingBottom:I

    add-int v8, v9, v10

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v1

    const/4 v3, 0x0

    if-lez v1, :cond_2

    const/4 v9, 0x0

    invoke-virtual {p0, v9}, Lcom/cyanogenmod/trebuchet/PagedView;->getRelativeChildOffset(I)I

    move-result v3

    iget v9, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mPageSpacing:I

    if-gez v9, :cond_2

    sub-int v9, p4, p2

    const/4 v10, 0x0

    invoke-virtual {p0, v10}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    invoke-virtual {v10}, Landroid/view/View;->getMeasuredWidth()I

    move-result v10

    sub-int/2addr v9, v10

    div-int/lit8 v9, v9, 0x2

    invoke-virtual {p0, v9}, Lcom/cyanogenmod/trebuchet/PagedView;->setPageSpacing(I)V

    :cond_2
    const/4 v6, 0x0

    :goto_1
    if-lt v6, v1, :cond_4

    iget-boolean v9, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mFirstLayout:Z

    if-eqz v9, :cond_3

    iget v9, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    if-ltz v9, :cond_3

    iget v9, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v10

    if-ge v9, v10, :cond_3

    const/4 v9, 0x0

    invoke-virtual {p0, v9}, Lcom/cyanogenmod/trebuchet/PagedView;->setHorizontalScrollBarEnabled(Z)V

    iget v9, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    invoke-virtual {p0, v9}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildOffset(I)I

    move-result v9

    iget v10, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    invoke-virtual {p0, v10}, Lcom/cyanogenmod/trebuchet/PagedView;->getRelativeChildOffset(I)I

    move-result v10

    sub-int v7, v9, v10

    const/4 v9, 0x0

    invoke-virtual {p0, v7, v9}, Lcom/cyanogenmod/trebuchet/PagedView;->scrollTo(II)V

    iget-object v9, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v9, v7}, Landroid/widget/Scroller;->setFinalX(I)V

    const/4 v9, 0x1

    invoke-virtual {p0, v9}, Lcom/cyanogenmod/trebuchet/PagedView;->setHorizontalScrollBarEnabled(Z)V

    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mFirstLayout:Z

    :cond_3
    iget-boolean v9, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mFirstLayout:Z

    if-eqz v9, :cond_0

    iget v9, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    if-ltz v9, :cond_0

    iget v9, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v10

    if-ge v9, v10, :cond_0

    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mFirstLayout:Z

    goto :goto_0

    :cond_4
    invoke-virtual {p0, v6}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v9

    const/16 v10, 0x8

    if-eq v9, v10, :cond_6

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->getScaledMeasuredWidth(Landroid/view/View;)I

    move-result v5

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    iget v4, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mPaddingTop:I

    iget-boolean v9, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCenterPagesVertically:Z

    if-eqz v9, :cond_5

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getMeasuredHeight()I

    move-result v9

    sub-int/2addr v9, v8

    sub-int/2addr v9, v2

    div-int/lit8 v9, v9, 0x2

    add-int/2addr v4, v9

    :cond_5
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    add-int/2addr v9, v3

    add-int v10, v4, v2

    invoke-virtual {v0, v3, v4, v9, v10}, Landroid/view/View;->layout(IIII)V

    iget v9, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mPageSpacing:I

    add-int/2addr v9, v5

    add-int/2addr v3, v9

    :cond_6
    add-int/lit8 v6, v6, 0x1

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 21
    .param p1    # I
    .param p2    # I

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mIsDataReady:Z

    move/from16 v19, v0

    if-nez v19, :cond_0

    invoke-super/range {p0 .. p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    :goto_0
    return-void

    :cond_0
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v17

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v18

    const/high16 v19, 0x40000000    # 2.0f

    move/from16 v0, v17

    move/from16 v1, v19

    if-eq v0, v1, :cond_1

    new-instance v19, Ljava/lang/IllegalStateException;

    const-string v20, "Workspace can only be used in EXACTLY mode."

    invoke-direct/range {v19 .. v20}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v19

    :cond_1
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v9

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v10

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mPaddingTop:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mPaddingBottom:I

    move/from16 v20, v0

    add-int v16, v19, v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mPaddingLeft:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mPaddingRight:I

    move/from16 v20, v0

    add-int v11, v19, v20

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v3

    const/4 v12, 0x0

    :goto_1
    if-lt v12, v3, :cond_4

    const/high16 v19, -0x80000000

    move/from16 v0, v19

    if-ne v9, v0, :cond_2

    add-int v10, v15, v16

    :cond_2
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1, v10}, Lcom/cyanogenmod/trebuchet/PagedView;->setMeasuredDimension(II)V

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/PagedView;->invalidateCachedOffsets()V

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/PagedView;->isScrollingIndicatorEnabled()Z

    move-result v19

    if-eqz v19, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollIndicator:Landroid/view/View;

    move-object/from16 v19, v0

    if-eqz v19, :cond_3

    :try_start_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollIndicator:Landroid/view/View;

    check-cast v13, Lcom/cyanogenmod/trebuchet/ScrollIndicator;

    invoke-interface {v13}, Lcom/cyanogenmod/trebuchet/ScrollIndicator;->update()V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_2
    if-lez v3, :cond_7

    add-int/lit8 v19, v3, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildOffset(I)I

    move-result v19

    add-int/lit8 v20, v3, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/PagedView;->getRelativeChildOffset(I)I

    move-result v20

    sub-int v19, v19, v20

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/cyanogenmod/trebuchet/PagedView;->mMaxScrollX:I

    goto/16 :goto_0

    :cond_4
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v14

    iget v0, v14, Landroid/view/ViewGroup$LayoutParams;->width:I

    move/from16 v19, v0

    const/16 v20, -0x2

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_5

    const/high16 v7, -0x80000000

    :goto_3
    iget v0, v14, Landroid/view/ViewGroup$LayoutParams;->height:I

    move/from16 v19, v0

    const/16 v20, -0x2

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_6

    const/high16 v5, -0x80000000

    :goto_4
    sub-int v19, v18, v11

    move/from16 v0, v19

    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    sub-int v19, v10, v16

    move/from16 v0, v19

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v6, v4}, Landroid/view/View;->measure(II)V

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v19

    move/from16 v0, v19

    invoke-static {v15, v0}, Ljava/lang/Math;->max(II)I

    move-result v15

    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_1

    :cond_5
    const/high16 v7, 0x40000000    # 2.0f

    goto :goto_3

    :cond_6
    const/high16 v5, 0x40000000    # 2.0f

    goto :goto_4

    :catch_0
    move-exception v8

    const-string v19, "PagedView"

    const-string v20, "scroll indicator should implement ScrollIndicator interface!"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-static {v0, v1, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    :cond_7
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/cyanogenmod/trebuchet/PagedView;->mMaxScrollX:I

    goto/16 :goto_0
.end method

.method protected onPageBeginMoving()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->showScrollingIndicator(Z)V

    return-void
.end method

.method protected onPageEndMoving()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->hideScrollingIndicator(Z)V

    return-void
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/graphics/Rect;

    iget v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mNextPage:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mNextPage:I

    :goto_0
    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v2

    :goto_1
    return v2

    :cond_0
    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 20
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v17

    if-gtz v17, :cond_0

    invoke-super/range {p0 .. p1}, Landroid/view/ViewGroup;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v17

    :goto_0
    return v17

    :cond_0
    invoke-direct/range {p0 .. p1}, Lcom/cyanogenmod/trebuchet/PagedView;->acquireVelocityTrackerAndAddMovement(Landroid/view/MotionEvent;)V

    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    and-int/lit16 v0, v3, 0xff

    move/from16 v17, v0

    packed-switch v17, :pswitch_data_0

    :cond_1
    :goto_1
    :pswitch_0
    const/16 v17, 0x1

    goto :goto_0

    :pswitch_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mScroller:Landroid/widget/Scroller;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/Scroller;->isFinished()Z

    move-result v17

    if-nez v17, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mScroller:Landroid/widget/Scroller;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/Scroller;->abortAnimation()V

    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v17

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/cyanogenmod/trebuchet/PagedView;->mLastMotionX:F

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/cyanogenmod/trebuchet/PagedView;->mDownMotionX:F

    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/cyanogenmod/trebuchet/PagedView;->mLastMotionXRemainder:F

    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/cyanogenmod/trebuchet/PagedView;->mTotalMotionX:F

    const/16 v17, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v17

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/cyanogenmod/trebuchet/PagedView;->mActivePointerId:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mTouchState:I

    move/from16 v17, v0

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_1

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/PagedView;->pageBeginMoving()V

    goto :goto_1

    :pswitch_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mTouchState:I

    move/from16 v17, v0

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mActivePointerId:I

    move/from16 v17, v0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/view/MotionEvent;->getX(I)F

    move-result v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mLastMotionX:F

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mLastMotionXRemainder:F

    move/from16 v18, v0

    add-float v17, v17, v18

    sub-float v5, v17, v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mTotalMotionX:F

    move/from16 v17, v0

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v18

    add-float v17, v17, v18

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/cyanogenmod/trebuchet/PagedView;->mTotalMotionX:F

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v17

    const/high16 v18, 0x3f800000    # 1.0f

    cmpl-float v17, v17, v18

    if-ltz v17, :cond_4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mTouchX:F

    move/from16 v17, v0

    add-float v17, v17, v5

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/cyanogenmod/trebuchet/PagedView;->mTouchX:F

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mDeferScrollUpdate:Z

    move/from16 v17, v0

    if-nez v17, :cond_3

    float-to-int v0, v5

    move/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Lcom/cyanogenmod/trebuchet/PagedView;->scrollBy(II)V

    :goto_2
    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Lcom/cyanogenmod/trebuchet/PagedView;->mLastMotionX:F

    float-to-int v0, v5

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    sub-float v17, v5, v17

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/cyanogenmod/trebuchet/PagedView;->mLastMotionXRemainder:F

    goto/16 :goto_1

    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/PagedView;->invalidate()V

    goto :goto_2

    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/PagedView;->awakenScrollBars()Z

    goto/16 :goto_1

    :cond_5
    invoke-virtual/range {p0 .. p1}, Lcom/cyanogenmod/trebuchet/PagedView;->determineScrollingStart(Landroid/view/MotionEvent;)V

    goto/16 :goto_1

    :pswitch_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mTouchState:I

    move/from16 v17, v0

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_11

    move-object/from16 v0, p0

    iget v4, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mActivePointerId:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/view/MotionEvent;->getX(I)F

    move-result v16

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mVelocityTracker:Landroid/view/VelocityTracker;

    const/16 v17, 0x3e8

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mMaximumVelocity:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v14, v0, v1}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    invoke-virtual {v14, v4}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v17

    move/from16 v0, v17

    float-to-int v15, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mDownMotionX:F

    move/from16 v17, v0

    sub-float v17, v16, v17

    move/from16 v0, v17

    float-to-int v5, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/PagedView;->getScaledMeasuredWidth(Landroid/view/View;)I

    move-result v10

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    int-to-float v0, v10

    move/from16 v18, v0

    const v19, 0x3ecccccd    # 0.4f

    mul-float v18, v18, v19

    cmpl-float v17, v17, v18

    if-lez v17, :cond_9

    const/4 v8, 0x1

    :goto_3
    move-object/from16 v0, p0

    iget v13, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mSnapVelocity:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mTotalMotionX:F

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mLastMotionX:F

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mLastMotionXRemainder:F

    move/from16 v19, v0

    add-float v18, v18, v19

    sub-float v18, v18, v16

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->abs(F)F

    move-result v18

    add-float v17, v17, v18

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/cyanogenmod/trebuchet/PagedView;->mTotalMotionX:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mTotalMotionX:F

    move/from16 v17, v0

    const/high16 v18, 0x41c80000    # 25.0f

    cmpl-float v17, v17, v18

    if-lez v17, :cond_a

    invoke-static {v15}, Ljava/lang/Math;->abs(I)I

    move-result v17

    move/from16 v0, v17

    if-le v0, v13, :cond_a

    const/4 v7, 0x1

    :goto_4
    const/4 v12, 0x0

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    int-to-float v0, v10

    move/from16 v18, v0

    const v19, 0x3ea8f5c3    # 0.33f

    mul-float v18, v18, v19

    cmpl-float v17, v17, v18

    if-lez v17, :cond_6

    int-to-float v0, v15

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Math;->signum(F)F

    move-result v17

    int-to-float v0, v5

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->signum(F)F

    move-result v18

    cmpl-float v17, v17, v18

    if-eqz v17, :cond_6

    if-eqz v7, :cond_6

    const/4 v12, 0x1

    :cond_6
    if-eqz v8, :cond_7

    if-lez v5, :cond_7

    if-eqz v7, :cond_8

    :cond_7
    if-eqz v7, :cond_c

    if-lez v15, :cond_c

    :cond_8
    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    move/from16 v17, v0

    if-lez v17, :cond_c

    if-eqz v12, :cond_b

    move-object/from16 v0, p0

    iget v6, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    :goto_5
    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v15}, Lcom/cyanogenmod/trebuchet/PagedView;->snapToPageWithVelocity(II)V

    :goto_6
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/cyanogenmod/trebuchet/PagedView;->mTouchState:I

    const/16 v17, -0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/cyanogenmod/trebuchet/PagedView;->mActivePointerId:I

    invoke-direct/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/PagedView;->releaseVelocityTracker()V

    goto/16 :goto_1

    :cond_9
    const/4 v8, 0x0

    goto/16 :goto_3

    :cond_a
    const/4 v7, 0x0

    goto :goto_4

    :cond_b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    move/from16 v17, v0

    add-int/lit8 v6, v17, -0x1

    goto :goto_5

    :cond_c
    if-eqz v8, :cond_d

    if-gez v5, :cond_d

    if-eqz v7, :cond_e

    :cond_d
    if-eqz v7, :cond_10

    if-gez v15, :cond_10

    :cond_e
    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    move/from16 v17, v0

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v18

    add-int/lit8 v18, v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_10

    if-eqz v12, :cond_f

    move-object/from16 v0, p0

    iget v6, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    :goto_7
    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v15}, Lcom/cyanogenmod/trebuchet/PagedView;->snapToPageWithVelocity(II)V

    goto :goto_6

    :cond_f
    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    move/from16 v17, v0

    add-int/lit8 v6, v17, 0x1

    goto :goto_7

    :cond_10
    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/PagedView;->snapToDestination()V

    goto :goto_6

    :cond_11
    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mTouchState:I

    move/from16 v17, v0

    const/16 v18, 0x2

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_13

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, -0x1

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->max(II)I

    move-result v9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-eq v9, v0, :cond_12

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/cyanogenmod/trebuchet/PagedView;->snapToPage(I)V

    goto/16 :goto_6

    :cond_12
    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/PagedView;->snapToDestination()V

    goto/16 :goto_6

    :cond_13
    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mTouchState:I

    move/from16 v17, v0

    const/16 v18, 0x3

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_15

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v17

    add-int/lit8 v17, v17, -0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    move/from16 v18, v0

    add-int/lit8 v18, v18, 0x1

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->min(II)I

    move-result v9

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-eq v9, v0, :cond_14

    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/cyanogenmod/trebuchet/PagedView;->snapToPage(I)V

    goto/16 :goto_6

    :cond_14
    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/PagedView;->snapToDestination()V

    goto/16 :goto_6

    :cond_15
    invoke-virtual/range {p0 .. p1}, Lcom/cyanogenmod/trebuchet/PagedView;->onUnhandledTap(Landroid/view/MotionEvent;)V

    goto/16 :goto_6

    :pswitch_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/PagedView;->mTouchState:I

    move/from16 v17, v0

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_16

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/PagedView;->snapToDestination()V

    :cond_16
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/cyanogenmod/trebuchet/PagedView;->mTouchState:I

    const/16 v17, -0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/cyanogenmod/trebuchet/PagedView;->mActivePointerId:I

    invoke-direct/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/PagedView;->releaseVelocityTracker()V

    goto/16 :goto_1

    :pswitch_5
    invoke-direct/range {p0 .. p1}, Lcom/cyanogenmod/trebuchet/PagedView;->onSecondaryPointerUp(Landroid/view/MotionEvent;)V

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method protected onUnhandledTap(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1    # Landroid/view/MotionEvent;

    return-void
.end method

.method protected onViewAdded(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onViewAdded(Landroid/view/View;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mForceScreenScrolled:Z

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->invalidate()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->invalidateCachedOffsets()V

    return-void
.end method

.method protected overScroll(F)V
    .locals 0
    .param p1    # F

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedView;->dampedOverScroll(F)V

    return-void
.end method

.method protected pageBeginMoving()V
    .locals 1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mIsPageMoving:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mIsPageMoving:Z

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->onPageBeginMoving()V

    :cond_0
    return-void
.end method

.method protected pageEndMoving()V
    .locals 1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mIsPageMoving:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mIsPageMoving:Z

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->onPageEndMoving()V

    :cond_0
    return-void
.end method

.method public requestChildFocus(Landroid/view/View;Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedView;->indexOfChild(Landroid/view/View;)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/PagedView;->indexToPage(I)I

    move-result v0

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getCurrentPage()I

    move-result v1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->isInTouchMode()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->snapToPage(I)V

    :cond_0
    return-void
.end method

.method public requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/graphics/Rect;
    .param p3    # Z

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedView;->indexOfChild(Landroid/view/View;)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/PagedView;->indexToPage(I)I

    move-result v0

    iget v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->isFinished()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->snapToPage(I)V

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->cancelLongPress()V

    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    return-void
.end method

.method protected resetCheckedGrandchildren()V
    .locals 4

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getCheckedGrandchildren()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    return-void

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Checkable;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Landroid/widget/Checkable;->setChecked(Z)V

    goto :goto_0
.end method

.method protected screenScrolled(I)V
    .locals 6
    .param p1    # I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->isScrollingIndicatorEnabled()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->updateScrollingIndicator()V

    :cond_0
    iget-boolean v4, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mFadeInAdjacentScreens:Z

    if-eqz v4, :cond_1

    iget-boolean v4, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mHandleFadeInAdjacentScreens:Z

    if-nez v4, :cond_1

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v4

    if-lt v2, v4, :cond_2

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->invalidate()V

    :cond_1
    return-void

    :cond_2
    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p0, p1, v1, v2}, Lcom/cyanogenmod/trebuchet/PagedView;->getScrollProgress(ILandroid/view/View;I)F

    move-result v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v5

    sub-float v0, v4, v5

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {v1}, Landroid/view/View;->invalidate()V

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public scrollBy(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mUnboundedScrollX:I

    add-int/2addr v0, p1

    iget v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollY:I

    add-int/2addr v1, p2

    invoke-virtual {p0, v0, v1}, Lcom/cyanogenmod/trebuchet/PagedView;->scrollTo(II)V

    return-void
.end method

.method public scrollLeft()V
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->snapToPage(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mNextPage:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mNextPage:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->snapToPage(I)V

    goto :goto_0
.end method

.method public scrollRight()V
    .locals 2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->snapToPage(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mNextPage:I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mNextPage:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->snapToPage(I)V

    goto :goto_0
.end method

.method public scrollTo(II)V
    .locals 1
    .param p1    # I
    .param p2    # I

    iput p1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mUnboundedScrollX:I

    if-gez p1, :cond_1

    const/4 v0, 0x0

    invoke-super {p0, v0, p2}, Landroid/view/ViewGroup;->scrollTo(II)V

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mAllowOverScroll:Z

    if-eqz v0, :cond_0

    int-to-float v0, p1

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->overScroll(F)V

    :cond_0
    :goto_0
    int-to-float v0, p1

    iput v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mTouchX:F

    return-void

    :cond_1
    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mMaxScrollX:I

    if-le p1, v0, :cond_2

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mMaxScrollX:I

    invoke-super {p0, v0, p2}, Landroid/view/ViewGroup;->scrollTo(II)V

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mAllowOverScroll:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mMaxScrollX:I

    sub-int v0, p1, v0

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->overScroll(F)V

    goto :goto_0

    :cond_2
    iput p1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mOverScrollX:I

    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->scrollTo(II)V

    goto :goto_0
.end method

.method protected scrollToNewPageWithoutMovingPages(I)V
    .locals 7
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildOffset(I)I

    move-result v5

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedView;->getRelativeChildOffset(I)I

    move-result v6

    sub-int v2, v5, v6

    iget v5, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollX:I

    sub-int v0, v2, v5

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v4

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v4, :cond_0

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedView;->setCurrentPage(I)V

    return-void

    :cond_0
    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getX()F

    move-result v5

    int-to-float v6, v0

    add-float/2addr v5, v6

    invoke-virtual {v3, v5}, Landroid/view/View;->setX(F)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setAllowLongPress(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mAllowLongPress:Z

    return-void
.end method

.method setCurrentPage(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    :cond_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_1

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->updateCurrentPageScroll()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->updateScrollingIndicator()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->notifyPageSwitchListener()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->invalidate()V

    goto :goto_0
.end method

.method protected setDataIsReady()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mIsDataReady:Z

    return-void
.end method

.method public setLayoutScale(F)V
    .locals 11
    .param p1    # F

    const/high16 v8, 0x40000000    # 2.0f

    iput p1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mLayoutScale:F

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->invalidateCachedOffsets()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v1

    new-array v2, v1, [F

    new-array v3, v1, [F

    const/4 v5, 0x0

    :goto_0
    if-lt v5, v1, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getMeasuredWidth()I

    move-result v7

    invoke-static {v7, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getMeasuredHeight()I

    move-result v7

    invoke-static {v7, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->requestLayout()V

    invoke-virtual {p0, v6, v4}, Lcom/cyanogenmod/trebuchet/PagedView;->measure(II)V

    iget v7, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mLeft:I

    iget v8, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mTop:I

    iget v9, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mRight:I

    iget v10, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mBottom:I

    invoke-virtual {p0, v7, v8, v9, v10}, Lcom/cyanogenmod/trebuchet/PagedView;->layout(IIII)V

    const/4 v5, 0x0

    :goto_1
    if-lt v5, v1, :cond_1

    iget v7, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    invoke-virtual {p0, v7}, Lcom/cyanogenmod/trebuchet/PagedView;->scrollToNewPageWithoutMovingPages(I)V

    return-void

    :cond_0
    invoke-virtual {p0, v5}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getX()F

    move-result v7

    aput v7, v2, v5

    invoke-virtual {v0}, Landroid/view/View;->getY()F

    move-result v7

    aput v7, v3, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v5}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    aget v7, v2, v5

    invoke-virtual {v0, v7}, Landroid/view/View;->setX(F)V

    aget v7, v3, v5

    invoke-virtual {v0, v7}, Landroid/view/View;->setY(F)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_1
.end method

.method public setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V
    .locals 3
    .param p1    # Landroid/view/View$OnLongClickListener;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setPageSpacing(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mPageSpacing:I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->invalidateCachedOffsets()V

    return-void
.end method

.method public setPageSwitchListener(Lcom/cyanogenmod/trebuchet/PagedView$PageSwitchListener;)V
    .locals 3
    .param p1    # Lcom/cyanogenmod/trebuchet/PagedView$PageSwitchListener;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mPageSwitchListener:Lcom/cyanogenmod/trebuchet/PagedView$PageSwitchListener;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mPageSwitchListener:Lcom/cyanogenmod/trebuchet/PagedView$PageSwitchListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mPageSwitchListener:Lcom/cyanogenmod/trebuchet/PagedView$PageSwitchListener;

    iget v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v1

    iget v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    invoke-interface {v0, v1, v2}, Lcom/cyanogenmod/trebuchet/PagedView$PageSwitchListener;->onPageSwitch(Landroid/view/View;I)V

    :cond_0
    return-void
.end method

.method protected showScrollingIndicator(Z)V
    .locals 1
    .param p1    # Z

    const/16 v0, 0x96

    invoke-virtual {p0, p1, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->showScrollingIndicator(ZI)V

    return-void
.end method

.method protected showScrollingIndicator(ZI)V
    .locals 4
    .param p1    # Z
    .param p2    # I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->isScrollingIndicatorEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getScrollingIndicator()Landroid/view/View;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollIndicator:Landroid/view/View;

    if-eqz v2, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollIndicator:Landroid/view/View;

    check-cast v1, Lcom/cyanogenmod/trebuchet/ScrollIndicator;

    invoke-interface {v1, p1, p2}, Lcom/cyanogenmod/trebuchet/ScrollIndicator;->show(ZI)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "PagedView"

    const-string v3, "scroll indicator should implement ScrollIndicator interface!"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected snapToDestination()V
    .locals 2

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageNearestToCenterOfScreen()I

    move-result v0

    const/16 v1, 0x226

    invoke-virtual {p0, v0, v1}, Lcom/cyanogenmod/trebuchet/PagedView;->snapToPage(II)V

    return-void
.end method

.method protected snapToPage(I)V
    .locals 1
    .param p1    # I

    const/16 v0, 0x226

    invoke-virtual {p0, p1, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->snapToPage(II)V

    return-void
.end method

.method protected snapToPage(II)V
    .locals 5
    .param p1    # I
    .param p2    # I

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-static {p1, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result p1

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildOffset(I)I

    move-result v3

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedView;->getRelativeChildOffset(I)I

    move-result v4

    sub-int v1, v3, v4

    iget v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mUnboundedScrollX:I

    sub-int v0, v1, v2

    invoke-virtual {p0, p1, v0, p2}, Lcom/cyanogenmod/trebuchet/PagedView;->snapToPage(III)V

    return-void
.end method

.method protected snapToPage(III)V
    .locals 7
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v2, 0x0

    iput p1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mNextPage:I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getFocusedChild()Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_0

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    if-eq p1, v0, :cond_0

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    if-ne v6, v0, :cond_0

    invoke-virtual {v6}, Landroid/view/View;->clearFocus()V

    :cond_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->pageBeginMoving()V

    invoke-virtual {p0, p3}, Lcom/cyanogenmod/trebuchet/PagedView;->awakenScrollBars(I)Z

    if-nez p3, :cond_1

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result p3

    :cond_1
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    :cond_2
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScroller:Landroid/widget/Scroller;

    iget v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mUnboundedScrollX:I

    move v3, p2

    move v4, v2

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mDeferScrollUpdate:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mNextPage:I

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->loadAssociatedPages(I)V

    :goto_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->notifyPageSwitchListener()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->invalidate()V

    return-void

    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mDeferLoadAssociatedPagesUntilScrollCompletes:Z

    goto :goto_0
.end method

.method protected snapToPageWithVelocity(II)V
    .locals 9
    .param p1    # I
    .param p2    # I

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-static {p1, v7}, Ljava/lang/Math;->min(II)I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result p1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getMeasuredWidth()I

    move-result v6

    div-int/lit8 v4, v6, 0x2

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildOffset(I)I

    move-result v6

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedView;->getRelativeChildOffset(I)I

    move-result v7

    sub-int v5, v6, v7

    iget v6, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mUnboundedScrollX:I

    sub-int v0, v5, v6

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result v6

    const/16 v7, 0xfa

    if-ge v6, v7, :cond_0

    const/16 v6, 0x226

    invoke-virtual {p0, p1, v6}, Lcom/cyanogenmod/trebuchet/PagedView;->snapToPage(II)V

    :goto_0
    return-void

    :cond_0
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v8

    mul-int/lit8 v7, v4, 0x2

    int-to-float v7, v7

    div-float/2addr v6, v7

    invoke-static {v8, v6}, Ljava/lang/Math;->min(FF)F

    move-result v2

    int-to-float v6, v4

    int-to-float v7, v4

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/PagedView;->distanceInfluenceForSnapDuration(F)F

    move-result v8

    mul-float/2addr v7, v8

    add-float v1, v6, v7

    invoke-static {p2}, Ljava/lang/Math;->abs(I)I

    move-result p2

    const/16 v6, 0x898

    invoke-static {v6, p2}, Ljava/lang/Math;->max(II)I

    move-result p2

    const/high16 v6, 0x447a0000    # 1000.0f

    int-to-float v7, p2

    div-float v7, v1, v7

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    mul-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    mul-int/lit8 v3, v6, 0x4

    invoke-virtual {p0, p1, v0, v3}, Lcom/cyanogenmod/trebuchet/PagedView;->snapToPage(III)V

    goto :goto_0
.end method

.method protected startChoiceMode(ILandroid/view/ActionMode$Callback;)V
    .locals 1
    .param p1    # I
    .param p2    # Landroid/view/ActionMode$Callback;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->isChoiceMode(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iput p1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mChoiceMode:I

    invoke-virtual {p0, p2}, Lcom/cyanogenmod/trebuchet/PagedView;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    move-result-object v0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mActionMode:Landroid/view/ActionMode;

    :cond_0
    return-void
.end method

.method public abstract syncPageItems(IZ)V
.end method

.method public abstract syncPages()V
.end method

.method protected updateCurrentPageScroll()V
    .locals 3

    iget v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildOffset(I)I

    move-result v1

    iget v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mCurrentPage:I

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/PagedView;->getRelativeChildOffset(I)I

    move-result v2

    sub-int v0, v1, v2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/cyanogenmod/trebuchet/PagedView;->scrollTo(II)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v1, v0}, Landroid/widget/Scroller;->setFinalX(I)V

    return-void
.end method

.method protected updateScrollingIndicator()V
    .locals 4

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v2

    const/4 v3, 0x1

    if-gt v2, v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->isScrollingIndicatorEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->getScrollingIndicator()Landroid/view/View;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollIndicator:Landroid/view/View;

    if-eqz v2, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/PagedView;->mScrollIndicator:Landroid/view/View;

    check-cast v1, Lcom/cyanogenmod/trebuchet/ScrollIndicator;

    invoke-interface {v1}, Lcom/cyanogenmod/trebuchet/ScrollIndicator;->update()V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "PagedView"

    const-string v3, "scroll indicator should implement ScrollIndicator interface!"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
