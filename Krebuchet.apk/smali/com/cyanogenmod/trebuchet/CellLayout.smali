.class public Lcom/cyanogenmod/trebuchet/CellLayout;
.super Landroid/view/ViewGroup;
.source "CellLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;,
        Lcom/cyanogenmod/trebuchet/CellLayout$CellLayoutAnimationController;,
        Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;
    }
.end annotation


# static fields
.field static final TAG:Ljava/lang/String; = "CellLayout"


# instance fields
.field private mActiveGlowBackground:Landroid/graphics/drawable/Drawable;

.field private mBackgroundAlpha:F

.field private mBackgroundAlphaMultiplier:F

.field private mBackgroundRect:Landroid/graphics/Rect;

.field private mCellHeight:I

.field private final mCellInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

.field private mCellWidth:I

.field private mChildren:Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

.field private mCountX:I

.field private mCountY:I

.field private mCrosshairsAnimator:Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;

.field private mCrosshairsDrawable:Landroid/graphics/drawable/Drawable;

.field private mCrosshairsVisibility:F

.field private final mDragCell:[I

.field private final mDragCenter:Landroid/graphics/Point;

.field private mDragOutlineAlphas:[F

.field private mDragOutlineAnims:[Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;

.field private mDragOutlineCurrent:I

.field private final mDragOutlinePaint:Landroid/graphics/Paint;

.field private mDragOutlines:[Landroid/graphics/Point;

.field private mDragging:Z

.field private mEaseOutInterpolator:Landroid/animation/TimeInterpolator;

.field private mFolderLeaveBehindCell:[I

.field private mFolderOuterRings:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;",
            ">;"
        }
    .end annotation
.end field

.field private mForegroundAlpha:I

.field private mForegroundPadding:I

.field private mForegroundRect:Landroid/graphics/Rect;

.field private mGuideBtn:Landroid/widget/Button;

.field private mGuideLayout:Landroid/widget/LinearLayout;

.field private mGuideViewGroup:Landroid/view/ViewGroup;

.field private mHeightGap:I

.field private final mInflater:Landroid/view/LayoutInflater;

.field private mInterceptTouchListener:Landroid/view/View$OnTouchListener;

.field private mIsDragOverlapping:Z

.field private mLastDownOnOccupiedCell:Z

.field private mMaxGap:I

.field private mNormalBackground:Landroid/graphics/drawable/Drawable;

.field mOccupied:[[Z

.field private mOriginalCellHeight:I

.field private mOriginalCellWidth:I

.field private mOriginalHeightGap:I

.field private mOriginalWidthGap:I

.field private mOverScrollForegroundDrawable:Landroid/graphics/drawable/Drawable;

.field private mOverScrollLeft:Landroid/graphics/drawable/Drawable;

.field private mOverScrollRight:Landroid/graphics/drawable/Drawable;

.field private mPressedOrFocusedIcon:Lcom/cyanogenmod/trebuchet/BubbleTextView;

.field private final mRect:Landroid/graphics/Rect;

.field private mReorderAnimators:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;",
            "Landroid/animation/ObjectAnimator;",
            ">;"
        }
    .end annotation
.end field

.field private mScrollingTransformsDirty:Z

.field mTempLocation:[I

.field private final mTmpPoint:[I

.field private final mTmpPointF:Landroid/graphics/PointF;

.field private final mTmpXY:[I

.field private mWidthGap:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 17
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct/range {p0 .. p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mScrollingTransformsDirty:Z

    new-instance v12, Landroid/graphics/Rect;

    invoke-direct {v12}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mRect:Landroid/graphics/Rect;

    new-instance v12, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    invoke-direct {v12}, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;-><init>()V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    const/4 v12, 0x2

    new-array v12, v12, [I

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mTmpXY:[I

    const/4 v12, 0x2

    new-array v12, v12, [I

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mTmpPoint:[I

    new-instance v12, Landroid/graphics/PointF;

    invoke-direct {v12}, Landroid/graphics/PointF;-><init>()V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mTmpPointF:Landroid/graphics/PointF;

    const/4 v12, 0x2

    new-array v12, v12, [I

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mTempLocation:[I

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mLastDownOnOccupiedCell:Z

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mFolderOuterRings:Ljava/util/ArrayList;

    const/4 v12, 0x2

    new-array v12, v12, [I

    fill-array-data v12, :array_0

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mFolderLeaveBehindCell:[I

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mForegroundAlpha:I

    const/high16 v12, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iput v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mBackgroundAlphaMultiplier:F

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mIsDragOverlapping:Z

    new-instance v12, Landroid/graphics/Point;

    invoke-direct {v12}, Landroid/graphics/Point;-><init>()V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragCenter:Landroid/graphics/Point;

    const/4 v12, 0x4

    new-array v12, v12, [Landroid/graphics/Point;

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlines:[Landroid/graphics/Point;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlines:[Landroid/graphics/Point;

    array-length v12, v12

    new-array v12, v12, [F

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlineAlphas:[F

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlines:[Landroid/graphics/Point;

    array-length v12, v12

    new-array v12, v12, [Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlineAnims:[Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlineCurrent:I

    new-instance v12, Landroid/graphics/Paint;

    invoke-direct {v12}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlinePaint:Landroid/graphics/Paint;

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCrosshairsDrawable:Landroid/graphics/drawable/Drawable;

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCrosshairsAnimator:Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCrosshairsVisibility:F

    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mReorderAnimators:Ljava/util/HashMap;

    const/4 v12, 0x2

    new-array v12, v12, [I

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragCell:[I

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput-boolean v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragging:Z

    const/4 v12, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/cyanogenmod/trebuchet/CellLayout;->setWillNotDraw(Z)V

    sget-object v12, Lcom/konka/avenger/R$styleable;->CellLayout:[I

    const/4 v13, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v0, v1, v12, v2, v13}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v3

    const-string v12, "layout_inflater"

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/view/LayoutInflater;

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mInflater:Landroid/view/LayoutInflater;

    const/4 v12, 0x0

    const/16 v13, 0xa

    invoke-virtual {v3, v12, v13}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellWidth:I

    move-object/from16 v0, p0

    iput v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOriginalCellWidth:I

    const/4 v12, 0x1

    const/16 v13, 0xa

    invoke-virtual {v3, v12, v13}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellHeight:I

    move-object/from16 v0, p0

    iput v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOriginalCellHeight:I

    const/4 v12, 0x2

    const/4 v13, 0x0

    invoke-virtual {v3, v12, v13}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOriginalWidthGap:I

    move-object/from16 v0, p0

    iput v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mWidthGap:I

    const/4 v12, 0x3

    const/4 v13, 0x0

    invoke-virtual {v3, v12, v13}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOriginalHeightGap:I

    move-object/from16 v0, p0

    iput v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mHeightGap:I

    const/4 v12, 0x4

    const/4 v13, 0x0

    invoke-virtual {v3, v12, v13}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mMaxGap:I

    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherModel;->getCellCountX()I

    move-result v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountX:I

    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherModel;->getCellCountY()I

    move-result v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountY:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountX:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountY:I

    filled-new-array {v12, v13}, [I

    move-result-object v12

    sget-object v13, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v13, v12}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, [[Z

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOccupied:[[Z

    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    const/4 v12, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/cyanogenmod/trebuchet/CellLayout;->setAlwaysDrawnWithCacheEnabled(Z)V

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v12, 0x7f02004d    # com.konka.avenger.R.drawable.homescreen_blue_normal_holo

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mNormalBackground:Landroid/graphics/drawable/Drawable;

    const v12, 0x7f02004e    # com.konka.avenger.R.drawable.homescreen_blue_strong_holo

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mActiveGlowBackground:Landroid/graphics/drawable/Drawable;

    const v12, 0x7f02008b    # com.konka.avenger.R.drawable.overscroll_glow_left

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOverScrollLeft:Landroid/graphics/drawable/Drawable;

    const v12, 0x7f02008c    # com.konka.avenger.R.drawable.overscroll_glow_right

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOverScrollRight:Landroid/graphics/drawable/Drawable;

    const v12, 0x7f0c0019    # com.konka.avenger.R.dimen.workspace_overscroll_drawable_padding

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mForegroundPadding:I

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mNormalBackground:Landroid/graphics/drawable/Drawable;

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mActiveGlowBackground:Landroid/graphics/drawable/Drawable;

    const/4 v13, 0x1

    invoke-virtual {v12, v13}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    const v12, 0x7f020039    # com.konka.avenger.R.drawable.gardening_crosshairs

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCrosshairsDrawable:Landroid/graphics/drawable/Drawable;

    new-instance v12, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v13, 0x40200000    # 2.5f

    invoke-direct {v12, v13}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mEaseOutInterpolator:Landroid/animation/TimeInterpolator;

    const v12, 0x7f0b0016    # com.konka.avenger.R.integer.config_crosshairsFadeInTime

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    new-instance v12, Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;

    int-to-long v13, v5

    const/4 v15, 0x0

    const/high16 v16, 0x3f800000    # 1.0f

    invoke-direct/range {v12 .. v16}, Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;-><init>(JFF)V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCrosshairsAnimator:Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCrosshairsAnimator:Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;

    invoke-virtual {v12}, Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;->getAnimator()Landroid/animation/ValueAnimator;

    move-result-object v12

    new-instance v13, Lcom/cyanogenmod/trebuchet/CellLayout$1;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/cyanogenmod/trebuchet/CellLayout$1;-><init>(Lcom/cyanogenmod/trebuchet/CellLayout;)V

    invoke-virtual {v12, v13}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCrosshairsAnimator:Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;

    invoke-virtual {v12}, Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;->getAnimator()Landroid/animation/ValueAnimator;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mEaseOutInterpolator:Landroid/animation/TimeInterpolator;

    invoke-virtual {v12, v13}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragCell:[I

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragCell:[I

    const/4 v15, 0x1

    const/16 v16, -0x1

    aput v16, v14, v15

    aput v16, v12, v13

    const/4 v8, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlines:[Landroid/graphics/Point;

    array-length v12, v12

    if-lt v8, v12, :cond_0

    const v12, 0x7f0b0018    # com.konka.avenger.R.integer.config_dragOutlineFadeTime

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v6

    const/4 v7, 0x0

    const v12, 0x7f0b0019    # com.konka.avenger.R.integer.config_dragOutlineMaxAlpha

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v12

    int-to-float v11, v12

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlineAlphas:[F

    const/4 v13, 0x0

    invoke-static {v12, v13}, Ljava/util/Arrays;->fill([FF)V

    const/4 v8, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlineAnims:[Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;

    array-length v12, v12

    if-lt v8, v12, :cond_1

    new-instance v12, Landroid/graphics/Rect;

    invoke-direct {v12}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mBackgroundRect:Landroid/graphics/Rect;

    new-instance v12, Landroid/graphics/Rect;

    invoke-direct {v12}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mForegroundRect:Landroid/graphics/Rect;

    new-instance v12, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    move-object/from16 v0, p1

    invoke-direct {v12, v0}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/cyanogenmod/trebuchet/CellLayout;->addView(Landroid/view/View;)V

    return-void

    :cond_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlines:[Landroid/graphics/Point;

    new-instance v13, Landroid/graphics/Point;

    const/4 v14, -0x1

    const/4 v15, -0x1

    invoke-direct {v13, v14, v15}, Landroid/graphics/Point;-><init>(II)V

    aput-object v13, v12, v8

    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_1
    new-instance v4, Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;

    int-to-long v12, v6

    const/4 v14, 0x0

    invoke-direct {v4, v12, v13, v14, v11}, Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;-><init>(JFF)V

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;->getAnimator()Landroid/animation/ValueAnimator;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mEaseOutInterpolator:Landroid/animation/TimeInterpolator;

    invoke-virtual {v12, v13}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    move v10, v8

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;->getAnimator()Landroid/animation/ValueAnimator;

    move-result-object v12

    new-instance v13, Lcom/cyanogenmod/trebuchet/CellLayout$2;

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v4, v10}, Lcom/cyanogenmod/trebuchet/CellLayout$2;-><init>(Lcom/cyanogenmod/trebuchet/CellLayout;Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;I)V

    invoke-virtual {v12, v13}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;->getAnimator()Landroid/animation/ValueAnimator;

    move-result-object v12

    new-instance v13, Lcom/cyanogenmod/trebuchet/CellLayout$3;

    move-object/from16 v0, p0

    invoke-direct {v13, v0, v4}, Lcom/cyanogenmod/trebuchet/CellLayout$3;-><init>(Lcom/cyanogenmod/trebuchet/CellLayout;Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;)V

    invoke-virtual {v12, v13}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlineAnims:[Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;

    aput-object v4, v12, v8

    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    nop

    :array_0
    .array-data 4
        -0x1
        -0x1
    .end array-data
.end method

.method static synthetic access$1(Lcom/cyanogenmod/trebuchet/CellLayout;F)V
    .locals 0

    iput p1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCrosshairsVisibility:F

    return-void
.end method

.method static synthetic access$2(Lcom/cyanogenmod/trebuchet/CellLayout;)[F
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlineAlphas:[F

    return-object v0
.end method

.method static synthetic access$3(Lcom/cyanogenmod/trebuchet/CellLayout;)[Landroid/graphics/Point;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlines:[Landroid/graphics/Point;

    return-object v0
.end method

.method static synthetic access$4(Lcom/cyanogenmod/trebuchet/CellLayout;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mReorderAnimators:Ljava/util/HashMap;

    return-object v0
.end method

.method private clearOccupiedCells()V
    .locals 4

    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountX:I

    if-lt v0, v2, :cond_0

    return-void

    :cond_0
    const/4 v1, 0x0

    :goto_1
    iget v2, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountY:I

    if-lt v1, v2, :cond_1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOccupied:[[Z

    aget-object v2, v2, v0

    const/4 v3, 0x0

    aput-boolean v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private clearTagCellInfo()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, -0x1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    iput v2, v0, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cellX:I

    iput v2, v0, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cellY:I

    iput v3, v0, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->spanX:I

    iput v3, v0, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->spanY:I

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->setTag(Ljava/lang/Object;)V

    return-void
.end method

.method static findVacantCell([IIIII[[Z)Z
    .locals 8
    .param p0    # [I
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # [[Z

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    :goto_0
    if-lt v4, p4, :cond_0

    move v6, v5

    :goto_1
    return v6

    :cond_0
    const/4 v3, 0x0

    :goto_2
    if-lt v3, p3, :cond_1

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    aget-object v7, p5, v3

    aget-boolean v7, v7, v4

    if-eqz v7, :cond_3

    move v0, v5

    :goto_3
    move v1, v3

    :goto_4
    add-int v7, v3, p1

    add-int/lit8 v7, v7, -0x1

    if-ge v1, v7, :cond_2

    if-lt v3, p3, :cond_4

    :cond_2
    if-eqz v0, :cond_8

    aput v3, p0, v5

    aput v4, p0, v6

    goto :goto_1

    :cond_3
    move v0, v6

    goto :goto_3

    :cond_4
    move v2, v4

    :goto_5
    add-int v7, v4, p2

    add-int/lit8 v7, v7, -0x1

    if-ge v2, v7, :cond_5

    if-lt v4, p4, :cond_6

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_6
    if-eqz v0, :cond_7

    aget-object v7, p5, v1

    aget-boolean v7, v7, v2

    if-nez v7, :cond_7

    move v0, v6

    :goto_6
    if-eqz v0, :cond_2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_7
    move v0, v5

    goto :goto_6

    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method

.method static heightInLandscape(Landroid/content/res/Resources;I)I
    .locals 4
    .param p0    # Landroid/content/res/Resources;
    .param p1    # I

    const v2, 0x7f0c000b    # com.konka.avenger.R.dimen.workspace_cell_height

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    const v2, 0x7f0c000c    # com.konka.avenger.R.dimen.workspace_width_gap

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    const v3, 0x7f0c000d    # com.konka.avenger.R.dimen.workspace_height_gap

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    add-int/lit8 v2, p1, -0x1

    mul-int/2addr v2, v1

    mul-int v3, v0, p1

    add-int/2addr v2, v3

    return v2
.end method

.method private invalidateBubbleTextView(Lcom/cyanogenmod/trebuchet/BubbleTextView;)V
    .locals 6
    .param p1    # Lcom/cyanogenmod/trebuchet/BubbleTextView;

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->getPressedOrFocusedBackgroundPadding()I

    move-result v0

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->getLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getPaddingLeft()I

    move-result v2

    add-int/2addr v1, v2

    sub-int/2addr v1, v0

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->getTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getPaddingTop()I

    move-result v3

    add-int/2addr v2, v3

    sub-int/2addr v2, v0

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->getRight()I

    move-result v3

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getPaddingLeft()I

    move-result v4

    add-int/2addr v3, v4

    add-int/2addr v3, v0

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->getBottom()I

    move-result v4

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getPaddingTop()I

    move-result v5

    add-int/2addr v4, v5

    add-int/2addr v4, v0

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/cyanogenmod/trebuchet/CellLayout;->invalidate(IIII)V

    return-void
.end method

.method private markCellsForView(IIIIZ)V
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Z

    move v0, p1

    :goto_0
    add-int v2, p1, p3

    if-ge v0, v2, :cond_0

    iget v2, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountX:I

    if-lt v0, v2, :cond_1

    :cond_0
    return-void

    :cond_1
    move v1, p2

    :goto_1
    add-int v2, p2, p4

    if-ge v1, v2, :cond_2

    iget v2, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountY:I

    if-lt v1, v2, :cond_3

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOccupied:[[Z

    aget-object v2, v2, v0

    aput-boolean p5, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static rectToCell(Landroid/content/res/Resources;II[I)[I
    .locals 9
    .param p0    # Landroid/content/res/Resources;
    .param p1    # I
    .param p2    # I
    .param p3    # [I

    const/4 v8, 0x1

    const/4 v7, 0x0

    const v5, 0x7f0c000a    # com.konka.avenger.R.dimen.workspace_cell_width

    invoke-virtual {p0, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const v5, 0x7f0c000b    # com.konka.avenger.R.dimen.workspace_cell_height

    invoke-virtual {p0, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v2

    int-to-float v5, p1

    int-to-float v6, v2

    div-float/2addr v5, v6

    float-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v5

    double-to-int v3, v5

    int-to-float v5, p2

    int-to-float v6, v2

    div-float/2addr v5, v6

    float-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v5

    double-to-int v4, v5

    if-nez p3, :cond_0

    const/4 v5, 0x2

    new-array p3, v5, [I

    aput v3, p3, v7

    aput v4, p3, v8

    :goto_0
    return-object p3

    :cond_0
    aput v3, p3, v7

    aput v4, p3, v8

    goto :goto_0
.end method

.method private setChildrenAlpha(F)V
    .locals 3
    .param p1    # F

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setAlpha(F)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method static widthInPortrait(Landroid/content/res/Resources;I)I
    .locals 4
    .param p0    # Landroid/content/res/Resources;
    .param p1    # I

    const v2, 0x7f0c000a    # com.konka.avenger.R.dimen.workspace_cell_width

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    const v2, 0x7f0c000c    # com.konka.avenger.R.dimen.workspace_width_gap

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    const v3, 0x7f0c000d    # com.konka.avenger.R.dimen.workspace_height_gap

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    add-int/lit8 v2, p1, -0x1

    mul-int/2addr v2, v1

    mul-int v3, v0, p1

    add-int/2addr v2, v3

    return v2
.end method


# virtual methods
.method public addViewToCellLayout(Landroid/view/View;IILcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;Z)Z
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I
    .param p4    # Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;
    .param p5    # Z

    move-object v0, p4

    iget v1, v0, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellX:I

    if-ltz v1, :cond_3

    iget v1, v0, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellX:I

    iget v2, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountX:I

    add-int/lit8 v2, v2, -0x1

    if-gt v1, v2, :cond_3

    iget v1, v0, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellY:I

    if-ltz v1, :cond_3

    iget v1, v0, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellY:I

    iget v2, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountY:I

    add-int/lit8 v2, v2, -0x1

    if-gt v1, v2, :cond_3

    iget v1, v0, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellHSpan:I

    if-gez v1, :cond_0

    iget v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountX:I

    iput v1, v0, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellHSpan:I

    :cond_0
    iget v1, v0, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellVSpan:I

    if-gez v1, :cond_1

    iget v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountY:I

    iput v1, v0, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellVSpan:I

    :cond_1
    invoke-virtual {p1, p3}, Landroid/view/View;->setId(I)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    invoke-virtual {v1, p1, p2, v0}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    if-eqz p5, :cond_2

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/CellLayout;->markCellsAsOccupiedForView(Landroid/view/View;)V

    :cond_2
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->refreshGuideView()V

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public animateChildToPosition(Landroid/view/View;IIII)Z
    .locals 14
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildrenLayout()Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->indexOfChild(Landroid/view/View;)I

    move-result v11

    const/4 v12, -0x1

    if-eq v11, v12, :cond_1

    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOccupied:[[Z

    aget-object v11, v11, p2

    aget-boolean v11, v11, p3

    if-nez v11, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mReorderAnimators:Ljava/util/HashMap;

    invoke-virtual {v11, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mReorderAnimators:Ljava/util/HashMap;

    invoke-virtual {v11, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/animation/ObjectAnimator;

    invoke-virtual {v11}, Landroid/animation/ObjectAnimator;->cancel()V

    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mReorderAnimators:Ljava/util/HashMap;

    invoke-virtual {v11, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget v7, v3, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->x:I

    iget v8, v3, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->y:I

    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOccupied:[[Z

    iget v12, v3, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellX:I

    aget-object v11, v11, v12

    iget v12, v3, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellY:I

    const/4 v13, 0x0

    aput-boolean v13, v11, v12

    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOccupied:[[Z

    aget-object v11, v11, p2

    const/4 v12, 0x1

    aput-boolean v12, v11, p3

    const/4 v11, 0x1

    iput-boolean v11, v3, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->isLockedToGrid:Z

    move/from16 v0, p2

    iput v0, v2, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellX:I

    move/from16 v0, p2

    iput v0, v3, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellX:I

    move/from16 v0, p3

    iput v0, v2, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellY:I

    move/from16 v0, p3

    iput v0, v3, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellY:I

    invoke-virtual {v1, v3}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->setupLp(Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;)V

    const/4 v11, 0x0

    iput-boolean v11, v3, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->isLockedToGrid:Z

    iget v4, v3, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->x:I

    iget v5, v3, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->y:I

    iput v7, v3, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->x:I

    iput v8, v3, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->y:I

    invoke-virtual {p1}, Landroid/view/View;->requestLayout()V

    const-string v11, "x"

    const/4 v12, 0x2

    new-array v12, v12, [I

    const/4 v13, 0x0

    aput v7, v12, v13

    const/4 v13, 0x1

    aput v4, v12, v13

    invoke-static {v11, v12}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v9

    const-string v11, "y"

    const/4 v12, 0x2

    new-array v12, v12, [I

    const/4 v13, 0x0

    aput v8, v12, v13

    const/4 v13, 0x1

    aput v5, v12, v13

    invoke-static {v11, v12}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v10

    const/4 v11, 0x2

    new-array v11, v11, [Landroid/animation/PropertyValuesHolder;

    const/4 v12, 0x0

    aput-object v9, v11, v12

    const/4 v12, 0x1

    aput-object v10, v11, v12

    invoke-static {v3, v11}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v6

    move/from16 v0, p4

    int-to-long v11, v0

    invoke-virtual {v6, v11, v12}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mReorderAnimators:Ljava/util/HashMap;

    invoke-virtual {v11, v3, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v11, Lcom/cyanogenmod/trebuchet/CellLayout$6;

    invoke-direct {v11, p0, p1}, Lcom/cyanogenmod/trebuchet/CellLayout$6;-><init>(Lcom/cyanogenmod/trebuchet/CellLayout;Landroid/view/View;)V

    invoke-virtual {v6, v11}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-instance v11, Lcom/cyanogenmod/trebuchet/CellLayout$7;

    invoke-direct {v11, p0, v3}, Lcom/cyanogenmod/trebuchet/CellLayout$7;-><init>(Lcom/cyanogenmod/trebuchet/CellLayout;Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;)V

    invoke-virtual {v6, v11}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    move/from16 v0, p5

    int-to-long v11, v0

    invoke-virtual {v6, v11, v12}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    invoke-virtual {v6}, Landroid/animation/ObjectAnimator;->start()V

    const/4 v11, 0x1

    :goto_0
    return v11

    :cond_1
    const/4 v11, 0x0

    goto :goto_0
.end method

.method buildChildrenLayer()V
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->buildLayer()V

    return-void
.end method

.method public calculateSpans(Lcom/cyanogenmod/trebuchet/ItemInfo;)V
    .locals 5
    .param p1    # Lcom/cyanogenmod/trebuchet/ItemInfo;

    const/4 v4, 0x1

    instance-of v3, p1, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;

    if-eqz v3, :cond_0

    move-object v3, p1

    check-cast v3, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;

    iget v1, v3, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->minWidth:I

    move-object v3, p1

    check-cast v3, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;

    iget v0, v3, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->minHeight:I

    :goto_0
    const/4 v3, 0x0

    invoke-virtual {p0, v1, v0, v3}, Lcom/cyanogenmod/trebuchet/CellLayout;->rectToCell(II[I)[I

    move-result-object v2

    const/4 v3, 0x0

    aget v3, v2, v3

    iput v3, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->spanX:I

    aget v3, v2, v4

    iput v3, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->spanY:I

    :goto_1
    return-void

    :cond_0
    instance-of v3, p1, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;

    if-eqz v3, :cond_1

    move-object v3, p1

    check-cast v3, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;

    iget v1, v3, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;->minWidth:I

    move-object v3, p1

    check-cast v3, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;

    iget v0, v3, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;->minHeight:I

    goto :goto_0

    :cond_1
    iput v4, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->spanY:I

    iput v4, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->spanX:I

    goto :goto_1
.end method

.method public cancelLongPress()V
    .locals 3

    invoke-super {p0}, Landroid/view/ViewGroup;->cancelLongPress()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->cancelLongPress()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public cellSpansToSize(II)[I
    .locals 5
    .param p1    # I
    .param p2    # I

    const/4 v1, 0x2

    new-array v0, v1, [I

    const/4 v1, 0x0

    iget v2, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellWidth:I

    mul-int/2addr v2, p1

    add-int/lit8 v3, p1, -0x1

    iget v4, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mWidthGap:I

    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellHeight:I

    mul-int/2addr v2, p2

    add-int/lit8 v3, p2, -0x1

    iget v4, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mHeightGap:I

    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    aput v2, v0, v1

    return-object v0
.end method

.method cellToCenterPoint(II[I)V
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # [I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getPaddingTop()I

    move-result v1

    const/4 v2, 0x0

    iget v3, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellWidth:I

    iget v4, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mWidthGap:I

    add-int/2addr v3, v4

    mul-int/2addr v3, p1

    add-int/2addr v3, v0

    iget v4, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellWidth:I

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    aput v3, p3, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellHeight:I

    iget v4, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mHeightGap:I

    add-int/2addr v3, v4

    mul-int/2addr v3, p2

    add-int/2addr v3, v1

    iget v4, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellHeight:I

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    aput v3, p3, v2

    return-void
.end method

.method cellToPoint(II[I)V
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # [I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getPaddingTop()I

    move-result v1

    const/4 v2, 0x0

    iget v3, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellWidth:I

    iget v4, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mWidthGap:I

    add-int/2addr v3, v4

    mul-int/2addr v3, p1

    add-int/2addr v3, v0

    aput v3, p3, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellHeight:I

    iget v4, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mHeightGap:I

    add-int/2addr v3, v4

    mul-int/2addr v3, p2

    add-int/2addr v3, v1

    aput v3, p3, v2

    return-void
.end method

.method public cellToRect(IIIILandroid/graphics/RectF;)V
    .locals 15
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Landroid/graphics/RectF;

    iget v2, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellWidth:I

    iget v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellHeight:I

    iget v8, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mWidthGap:I

    iget v5, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mHeightGap:I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getPaddingLeft()I

    move-result v3

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getPaddingTop()I

    move-result v6

    mul-int v11, p3, v2

    add-int/lit8 v12, p3, -0x1

    mul-int/2addr v12, v8

    add-int v7, v11, v12

    mul-int v11, p4, v1

    add-int/lit8 v12, p4, -0x1

    mul-int/2addr v12, v5

    add-int v4, v11, v12

    add-int v11, v2, v8

    mul-int v11, v11, p1

    add-int v9, v3, v11

    add-int v11, v1, v5

    mul-int v11, v11, p2

    add-int v10, v6, v11

    int-to-float v11, v9

    int-to-float v12, v10

    add-int v13, v9, v7

    int-to-float v13, v13

    add-int v14, v10, v4

    int-to-float v14, v14

    move-object/from16 v0, p5

    invoke-virtual {v0, v11, v12, v13, v14}, Landroid/graphics/RectF;->set(FFFF)V

    return-void
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1
    .param p1    # Landroid/view/ViewGroup$LayoutParams;

    instance-of v0, p1, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;

    return v0
.end method

.method public clearDragOutlines()V
    .locals 4

    const/4 v3, -0x1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlineCurrent:I

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlineAnims:[Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;->animateOut()V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragCell:[I

    const/4 v2, 0x0

    aput v3, v1, v2

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragCell:[I

    const/4 v2, 0x1

    aput v3, v1, v2

    return-void
.end method

.method public clearFolderLeaveBehind()V
    .locals 3

    const/4 v2, -0x1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mFolderLeaveBehindCell:[I

    const/4 v1, 0x0

    aput v2, v0, v1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mFolderLeaveBehindCell:[I

    const/4 v1, 0x1

    aput v2, v0, v1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->invalidate()V

    return-void
.end method

.method public disableHardwareLayers()V
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->disableHardwareLayers()V

    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1    # Landroid/graphics/Canvas;

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    iget v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mForegroundAlpha:I

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOverScrollForegroundDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mForegroundRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOverScrollForegroundDrawable:Landroid/graphics/drawable/Drawable;

    check-cast v1, Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/NinePatchDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->ADD:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOverScrollForegroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    :cond_0
    return-void
.end method

.method public drawChildren(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1    # Landroid/graphics/Canvas;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->draw(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public enableHardwareLayers()V
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->enableHardwareLayers()V

    return-void
.end method

.method estimateDropCell(IIII[I)V
    .locals 7
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # [I

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountX:I

    iget v2, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountY:I

    invoke-virtual {p0, p1, p2, p5}, Lcom/cyanogenmod/trebuchet/CellLayout;->pointToCellRounded(II[I)V

    aget v4, p5, v5

    add-int/2addr v4, p3

    sub-int v3, v4, v1

    if-lez v3, :cond_0

    aget v4, p5, v5

    sub-int/2addr v4, v3

    aput v4, p5, v5

    :cond_0
    aget v4, p5, v5

    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    aput v4, p5, v5

    aget v4, p5, v6

    add-int/2addr v4, p4

    sub-int v0, v4, v2

    if-lez v0, :cond_1

    aget v4, p5, v6

    sub-int/2addr v4, v0

    aput v4, p5, v6

    :cond_1
    aget v4, p5, v6

    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    aput v4, p5, v6

    return-void
.end method

.method existsEmptyCell()Z
    .locals 2

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1, v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->findCellForSpan([III)Z

    move-result v0

    return v0
.end method

.method findCellForFolderSpan([III)Z
    .locals 7
    .param p1    # [I
    .param p2    # I
    .param p3    # I

    const/4 v4, -0x1

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v5, v4

    invoke-virtual/range {v0 .. v6}, Lcom/cyanogenmod/trebuchet/CellLayout;->findCellForFolderSpanThatIntersectsIgnoring([IIIIILandroid/view/View;)Z

    move-result v0

    return v0
.end method

.method findCellForFolderSpanThatIntersectsIgnoring([IIIIILandroid/view/View;)Z
    .locals 13
    .param p1    # [I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # Landroid/view/View;

    move-object/from16 v0, p6

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->markCellsAsUnoccupiedForView(Landroid/view/View;)V

    const/4 v3, 0x0

    :goto_0
    const/4 v6, 0x0

    iget v10, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountX:I

    add-int/lit8 v11, p2, -0x1

    sub-int v1, v10, v11

    const/4 v7, 0x0

    iget v10, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountY:I

    add-int/lit8 v11, p3, -0x1

    sub-int v2, v10, v11

    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "[wjx]endX =  "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "endY = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "mcountx"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountX:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountY:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    move v9, v7

    :goto_1
    if-ge v9, v2, :cond_0

    if-eqz v3, :cond_1

    :cond_0
    const/4 v10, -0x1

    move/from16 v0, p4

    if-ne v0, v10, :cond_7

    const/4 v10, -0x1

    move/from16 v0, p5

    if-ne v0, v10, :cond_7

    move-object/from16 v0, p6

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->markCellsAsOccupiedForView(Landroid/view/View;)V

    return v3

    :cond_1
    move v8, v6

    :goto_2
    if-lt v8, v1, :cond_2

    :goto_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    :cond_2
    const/4 v4, 0x0

    :goto_4
    if-lt v4, p2, :cond_4

    if-eqz p1, :cond_3

    const/4 v10, 0x0

    aput v8, p1, v10

    const/4 v10, 0x1

    aput v9, p1, v10

    :cond_3
    const/4 v3, 0x1

    goto :goto_3

    :cond_4
    const/4 v5, 0x0

    :goto_5
    move/from16 v0, p3

    if-lt v5, v0, :cond_5

    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :cond_5
    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOccupied:[[Z

    add-int v11, v8, v4

    aget-object v10, v10, v11

    add-int v11, v9, v5

    aget-boolean v10, v10, v11

    if-eqz v10, :cond_6

    add-int/2addr v8, v4

    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    :cond_6
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    :cond_7
    const/16 p4, -0x1

    const/16 p5, -0x1

    goto/16 :goto_0
.end method

.method findCellForSpan([III)Z
    .locals 7
    .param p1    # [I
    .param p2    # I
    .param p3    # I

    const/4 v4, -0x1

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v5, v4

    invoke-virtual/range {v0 .. v6}, Lcom/cyanogenmod/trebuchet/CellLayout;->findCellForSpanThatIntersectsIgnoring([IIIIILandroid/view/View;)Z

    move-result v0

    return v0
.end method

.method findCellForSpanIgnoring([IIILandroid/view/View;)Z
    .locals 7
    .param p1    # [I
    .param p2    # I
    .param p3    # I
    .param p4    # Landroid/view/View;

    const/4 v4, -0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v5, v4

    move-object v6, p4

    invoke-virtual/range {v0 .. v6}, Lcom/cyanogenmod/trebuchet/CellLayout;->findCellForSpanThatIntersectsIgnoring([IIIIILandroid/view/View;)Z

    move-result v0

    return v0
.end method

.method findCellForSpanThatIntersects([IIIII)Z
    .locals 7
    .param p1    # [I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/cyanogenmod/trebuchet/CellLayout;->findCellForSpanThatIntersectsIgnoring([IIIIILandroid/view/View;)Z

    move-result v0

    return v0
.end method

.method findCellForSpanThatIntersectsIgnoring([IIIIILandroid/view/View;)Z
    .locals 12
    .param p1    # [I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # Landroid/view/View;

    move-object/from16 v0, p6

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->markCellsAsUnoccupiedForView(Landroid/view/View;)V

    const/4 v3, 0x0

    :goto_0
    const/4 v6, 0x0

    if-ltz p4, :cond_0

    add-int/lit8 v10, p2, -0x1

    sub-int v10, p4, v10

    invoke-static {v6, v10}, Ljava/lang/Math;->max(II)I

    move-result v6

    :cond_0
    iget v10, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountX:I

    add-int/lit8 v11, p2, -0x1

    sub-int v1, v10, v11

    if-ltz p4, :cond_1

    add-int/lit8 v10, p2, -0x1

    add-int v11, p4, v10

    const/4 v10, 0x1

    if-ne p2, v10, :cond_5

    const/4 v10, 0x1

    :goto_1
    add-int/2addr v10, v11

    invoke-static {v1, v10}, Ljava/lang/Math;->min(II)I

    move-result v1

    :cond_1
    const/4 v7, 0x0

    if-ltz p5, :cond_2

    add-int/lit8 v10, p3, -0x1

    sub-int v10, p5, v10

    invoke-static {v7, v10}, Ljava/lang/Math;->max(II)I

    move-result v7

    :cond_2
    iget v10, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountY:I

    add-int/lit8 v11, p3, -0x1

    sub-int v2, v10, v11

    if-ltz p5, :cond_3

    add-int/lit8 v10, p3, -0x1

    add-int v11, p5, v10

    const/4 v10, 0x1

    if-ne p3, v10, :cond_6

    const/4 v10, 0x1

    :goto_2
    add-int/2addr v10, v11

    invoke-static {v2, v10}, Ljava/lang/Math;->min(II)I

    move-result v2

    :cond_3
    move v9, v7

    :goto_3
    if-ge v9, v2, :cond_4

    if-eqz v3, :cond_7

    :cond_4
    const/4 v10, -0x1

    move/from16 v0, p4

    if-ne v0, v10, :cond_d

    const/4 v10, -0x1

    move/from16 v0, p5

    if-ne v0, v10, :cond_d

    move-object/from16 v0, p6

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->markCellsAsOccupiedForView(Landroid/view/View;)V

    return v3

    :cond_5
    const/4 v10, 0x0

    goto :goto_1

    :cond_6
    const/4 v10, 0x0

    goto :goto_2

    :cond_7
    move v8, v6

    :goto_4
    if-lt v8, v1, :cond_8

    :goto_5
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    :cond_8
    const/4 v4, 0x0

    :goto_6
    if-lt v4, p2, :cond_a

    if-eqz p1, :cond_9

    const/4 v10, 0x0

    aput v8, p1, v10

    const/4 v10, 0x1

    aput v9, p1, v10

    :cond_9
    const/4 v3, 0x1

    goto :goto_5

    :cond_a
    const/4 v5, 0x0

    :goto_7
    if-lt v5, p3, :cond_b

    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    :cond_b
    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOccupied:[[Z

    add-int v11, v8, v4

    aget-object v10, v10, v11

    add-int v11, v9, v5

    aget-boolean v10, v10, v11

    if-eqz v10, :cond_c

    add-int/2addr v8, v4

    add-int/lit8 v8, v8, 0x1

    goto :goto_4

    :cond_c
    add-int/lit8 v5, v5, 0x1

    goto :goto_7

    :cond_d
    const/16 p4, -0x1

    const/16 p5, -0x1

    goto/16 :goto_0
.end method

.method findCellLeftCount()I
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    iget v3, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountX:I

    if-lt v1, v3, :cond_0

    return v0

    :cond_0
    const/4 v2, 0x0

    :goto_1
    iget v3, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountY:I

    if-lt v2, v3, :cond_1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOccupied:[[Z

    aget-object v3, v3, v1

    aget-boolean v3, v3, v2

    if-eqz v3, :cond_2

    add-int/lit8 v0, v0, 0x1

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method findNearestArea(IIIILandroid/view/View;Z[I)[I
    .locals 21
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Landroid/view/View;
    .param p6    # Z
    .param p7    # [I

    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->markCellsAsUnoccupiedForView(Landroid/view/View;)V

    move/from16 v0, p1

    int-to-float v15, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellWidth:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mWidthGap:I

    move/from16 v17, v0

    add-int v16, v16, v17

    add-int/lit8 v17, p3, -0x1

    mul-int v16, v16, v17

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    const/high16 v17, 0x40000000    # 2.0f

    div-float v16, v16, v17

    sub-float v15, v15, v16

    float-to-int v0, v15

    move/from16 p1, v0

    move/from16 v0, p2

    int-to-float v15, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellHeight:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mHeightGap:I

    move/from16 v17, v0

    add-int v16, v16, v17

    add-int/lit8 v17, p4, -0x1

    mul-int v16, v16, v17

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    const/high16 v17, 0x40000000    # 2.0f

    div-float v16, v16, v17

    sub-float v15, v15, v16

    float-to-int v0, v15

    move/from16 p2, v0

    if-eqz p7, :cond_1

    move-object/from16 v4, p7

    :goto_0
    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    move-object/from16 v0, p0

    iget v6, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountX:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountY:I

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOccupied:[[Z

    const/4 v14, 0x0

    :goto_1
    add-int/lit8 v15, p4, -0x1

    sub-int v15, v7, v15

    if-lt v14, v15, :cond_2

    move-object/from16 v0, p0

    move-object/from16 v1, p5

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->markCellsAsOccupiedForView(Landroid/view/View;)V

    const-wide v15, 0x7fefffffffffffffL    # Double.MAX_VALUE

    cmpl-double v15, v2, v15

    if-nez v15, :cond_0

    const/4 v15, 0x0

    const/16 v16, -0x1

    aput v16, v4, v15

    const/4 v15, 0x1

    const/16 v16, -0x1

    aput v16, v4, v15

    :cond_0
    return-object v4

    :cond_1
    const/4 v15, 0x2

    new-array v4, v15, [I

    goto :goto_0

    :cond_2
    const/4 v13, 0x0

    :goto_2
    add-int/lit8 v15, p3, -0x1

    sub-int v15, v6, v15

    if-lt v13, v15, :cond_3

    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    :cond_3
    if-eqz p6, :cond_4

    const/4 v10, 0x0

    :goto_3
    move/from16 v0, p3

    if-lt v10, v0, :cond_6

    :cond_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mTmpXY:[I

    move-object/from16 v0, p0

    invoke-virtual {v0, v13, v14, v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->cellToCenterPoint(II[I)V

    const/4 v15, 0x0

    aget v15, v5, v15

    sub-int v15, v15, p1

    int-to-double v15, v15

    const-wide/high16 v17, 0x4000000000000000L    # 2.0

    invoke-static/range {v15 .. v18}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v15

    const/16 v17, 0x1

    aget v17, v5, v17

    sub-int v17, v17, p2

    move/from16 v0, v17

    int-to-double v0, v0

    move-wide/from16 v17, v0

    const-wide/high16 v19, 0x4000000000000000L    # 2.0

    invoke-static/range {v17 .. v20}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v17

    add-double v15, v15, v17

    invoke-static/range {v15 .. v16}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    cmpg-double v15, v8, v2

    if-gtz v15, :cond_5

    move-wide v2, v8

    const/4 v15, 0x0

    aput v13, v4, v15

    const/4 v15, 0x1

    aput v14, v4, v15

    :cond_5
    :goto_4
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    :cond_6
    const/4 v11, 0x0

    :goto_5
    move/from16 v0, p4

    if-lt v11, v0, :cond_7

    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    :cond_7
    add-int v15, v13, v10

    aget-object v15, v12, v15

    add-int v16, v14, v11

    aget-boolean v15, v15, v16

    if-eqz v15, :cond_8

    add-int/2addr v13, v10

    goto :goto_4

    :cond_8
    add-int/lit8 v11, v11, 0x1

    goto :goto_5
.end method

.method findNearestArea(IIII[I)[I
    .locals 8
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # [I

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Lcom/cyanogenmod/trebuchet/CellLayout;->findNearestArea(IIIILandroid/view/View;Z[I)[I

    move-result-object v0

    return-object v0
.end method

.method findNearestVacantArea(IIIILandroid/view/View;[I)[I
    .locals 8
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Landroid/view/View;
    .param p6    # [I

    const/4 v6, 0x1

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move-object v7, p6

    invoke-virtual/range {v0 .. v7}, Lcom/cyanogenmod/trebuchet/CellLayout;->findNearestArea(IIIILandroid/view/View;Z[I)[I

    move-result-object v0

    return-object v0
.end method

.method findNearestVacantArea(IIII[I)[I
    .locals 7
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # [I

    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/cyanogenmod/trebuchet/CellLayout;->findNearestVacantArea(IIIILandroid/view/View;[I)[I

    move-result-object v0

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2
    .param p1    # Landroid/util/AttributeSet;

    new-instance v0, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1    # Landroid/view/ViewGroup$LayoutParams;

    new-instance v0, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;

    invoke-direct {v0, p1}, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public getBackgroundAlpha()F
    .locals 1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mBackgroundAlpha:F

    return v0
.end method

.method public getBackgroundAlphaMultiplier()F
    .locals 1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mBackgroundAlphaMultiplier:F

    return v0
.end method

.method getCellHeight()I
    .locals 1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellHeight:I

    return v0
.end method

.method getCellWidth()I
    .locals 1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellWidth:I

    return v0
.end method

.method public getChildAt(II)Landroid/view/View;
    .locals 1
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    invoke-virtual {v0, p1, p2}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->getChildAt(II)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getChildrenLayout()Lcom/cyanogenmod/trebuchet/CellLayoutChildren;
    .locals 1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method getContentRect(Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 6
    .param p1    # Landroid/graphics/Rect;

    if-nez p1, :cond_0

    new-instance p1, Landroid/graphics/Rect;

    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    :cond_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getPaddingTop()I

    move-result v3

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getWidth()I

    move-result v4

    add-int/2addr v4, v1

    iget v5, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPaddingLeft:I

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPaddingRight:I

    sub-int v2, v4, v5

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getHeight()I

    move-result v4

    add-int/2addr v4, v3

    iget v5, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPaddingTop:I

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPaddingBottom:I

    sub-int v0, v4, v5

    invoke-virtual {p1, v1, v3, v2, v0}, Landroid/graphics/Rect;->set(IIII)V

    return-object p1
.end method

.method getCountX()I
    .locals 1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountX:I

    return v0
.end method

.method getCountY()I
    .locals 1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountY:I

    return v0
.end method

.method public getDesiredHeight()I
    .locals 3

    iget v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPaddingTop:I

    iget v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPaddingBottom:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountY:I

    iget v2, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellHeight:I

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    iget v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountY:I

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget v2, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mHeightGap:I

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method public getDesiredWidth()I
    .locals 3

    iget v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPaddingLeft:I

    iget v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPaddingRight:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountX:I

    iget v2, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellWidth:I

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    iget v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountX:I

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget v2, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mWidthGap:I

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method public getExpandabilityArrayForView(Landroid/view/View;[I)V
    .locals 10
    .param p1    # Landroid/view/View;
    .param p2    # [I

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;

    aput v6, p2, v6

    iget v4, v1, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellX:I

    add-int/lit8 v2, v4, -0x1

    :goto_0
    if-gez v2, :cond_4

    :cond_0
    aput v6, p2, v7

    iget v4, v1, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellY:I

    add-int/lit8 v3, v4, -0x1

    :goto_1
    if-gez v3, :cond_7

    :cond_1
    aput v6, p2, v8

    iget v4, v1, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellX:I

    iget v5, v1, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellHSpan:I

    add-int v2, v4, v5

    :goto_2
    iget v4, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountX:I

    if-lt v2, v4, :cond_a

    :cond_2
    aput v6, p2, v9

    iget v4, v1, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellY:I

    iget v5, v1, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellVSpan:I

    add-int v3, v4, v5

    :goto_3
    iget v4, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountY:I

    if-lt v3, v4, :cond_d

    :cond_3
    return-void

    :cond_4
    const/4 v0, 0x0

    iget v3, v1, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellY:I

    :goto_4
    iget v4, v1, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellY:I

    iget v5, v1, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellVSpan:I

    add-int/2addr v4, v5

    if-lt v3, v4, :cond_5

    if-nez v0, :cond_0

    aget v4, p2, v6

    add-int/lit8 v4, v4, 0x1

    aput v4, p2, v6

    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    :cond_5
    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOccupied:[[Z

    aget-object v4, v4, v2

    aget-boolean v4, v4, v3

    if-eqz v4, :cond_6

    const/4 v0, 0x1

    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_7
    const/4 v0, 0x0

    iget v2, v1, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellX:I

    :goto_5
    iget v4, v1, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellX:I

    iget v5, v1, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellHSpan:I

    add-int/2addr v4, v5

    if-lt v2, v4, :cond_8

    if-nez v0, :cond_1

    aget v4, p2, v7

    add-int/lit8 v4, v4, 0x1

    aput v4, p2, v7

    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    :cond_8
    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOccupied:[[Z

    aget-object v4, v4, v2

    aget-boolean v4, v4, v3

    if-eqz v4, :cond_9

    const/4 v0, 0x1

    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_a
    const/4 v0, 0x0

    iget v3, v1, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellY:I

    :goto_6
    iget v4, v1, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellY:I

    iget v5, v1, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellVSpan:I

    add-int/2addr v4, v5

    if-lt v3, v4, :cond_b

    if-nez v0, :cond_2

    aget v4, p2, v8

    add-int/lit8 v4, v4, 0x1

    aput v4, p2, v8

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_b
    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOccupied:[[Z

    aget-object v4, v4, v2

    aget-boolean v4, v4, v3

    if-eqz v4, :cond_c

    const/4 v0, 0x1

    :cond_c
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_d
    const/4 v0, 0x0

    iget v2, v1, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellX:I

    :goto_7
    iget v4, v1, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellX:I

    iget v5, v1, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellHSpan:I

    add-int/2addr v4, v5

    if-lt v2, v4, :cond_e

    if-nez v0, :cond_3

    aget v4, p2, v9

    add-int/lit8 v4, v4, 0x1

    aput v4, p2, v9

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_e
    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOccupied:[[Z

    aget-object v4, v4, v2

    aget-boolean v4, v4, v3

    if-eqz v4, :cond_f

    const/4 v0, 0x1

    :cond_f
    add-int/lit8 v2, v2, 0x1

    goto :goto_7
.end method

.method getHeightGap()I
    .locals 1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mHeightGap:I

    return v0
.end method

.method getIsDragOverlapping()Z
    .locals 1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mIsDragOverlapping:Z

    return v0
.end method

.method public getTag()Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;
    .locals 1

    invoke-super {p0}, Landroid/view/ViewGroup;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    return-object v0
.end method

.method public bridge synthetic getTag()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getTag()Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    move-result-object v0

    return-object v0
.end method

.method public getVacantCell([III)Z
    .locals 6
    .param p1    # [I
    .param p2    # I
    .param p3    # I

    iget v3, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountX:I

    iget v4, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountY:I

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOccupied:[[Z

    move-object v0, p1

    move v1, p2

    move v2, p3

    invoke-static/range {v0 .. v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->findVacantCell([IIIII[[Z)Z

    move-result v0

    return v0
.end method

.method getWidthGap()I
    .locals 1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mWidthGap:I

    return v0
.end method

.method public hideFolderAccept(Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;)V
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mFolderOuterRings:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mFolderOuterRings:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->invalidate()V

    return-void
.end method

.method public hideGuideView()V
    .locals 2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mGuideLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mGuideLayout:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public isOccupied(II)Z
    .locals 2
    .param p1    # I
    .param p2    # I

    iget v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountX:I

    if-ge p1, v0, :cond_0

    iget v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountY:I

    if-ge p2, v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOccupied:[[Z

    aget-object v0, v0, p1

    aget-boolean v0, v0, p2

    return v0

    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Position exceeds the bound of this CellLayout"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public lastDownOnOccupiedCell()Z
    .locals 1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mLastDownOnOccupiedCell:Z

    return v0
.end method

.method public markCellsAsOccupiedForView(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    if-eq v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;

    iget v1, v6, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellX:I

    iget v2, v6, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellY:I

    iget v3, v6, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellHSpan:I

    iget v4, v6, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellVSpan:I

    const/4 v5, 0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->markCellsForView(IIIIZ)V

    goto :goto_0
.end method

.method public markCellsAsUnoccupiedForView(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    if-eq v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;

    iget v1, v6, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellX:I

    iget v2, v6, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellY:I

    iget v3, v6, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellHSpan:I

    iget v4, v6, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellVSpan:I

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->markCellsForView(IIIIZ)V

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    iput v0, v1, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->screen:I

    return-void
.end method

.method onDragEnter()V
    .locals 1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragging:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCrosshairsAnimator:Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCrosshairsAnimator:Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;->animateIn()V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragging:Z

    return-void
.end method

.method onDragExit()V
    .locals 4

    const/4 v3, -0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragging:Z

    if-eqz v0, :cond_0

    iput-boolean v2, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragging:Z

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCrosshairsAnimator:Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCrosshairsAnimator:Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;->animateOut()V

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragCell:[I

    aput v3, v0, v2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragCell:[I

    const/4 v1, 0x1

    aput v3, v0, v1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlineAnims:[Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;

    iget v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlineCurrent:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;->animateOut()V

    iget v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlineCurrent:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlineAnims:[Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;

    array-length v1, v1

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlineCurrent:I

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->setIsDragOverlapping(Z)V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 30
    .param p1    # Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mBackgroundAlpha:F

    move/from16 v27, v0

    const/16 v28, 0x0

    cmpl-float v27, v27, v28

    if-lez v27, :cond_0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mIsDragOverlapping:Z

    move/from16 v27, v0

    if-eqz v27, :cond_4

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mActiveGlowBackground:Landroid/graphics/drawable/Drawable;

    :goto_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mBackgroundAlpha:F

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mBackgroundAlphaMultiplier:F

    move/from16 v28, v0

    mul-float v27, v27, v28

    const/high16 v28, 0x437f0000    # 255.0f

    mul-float v27, v27, v28

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    invoke-virtual {v9, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mBackgroundRect:Landroid/graphics/Rect;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    invoke-virtual {v9, v0}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    move-object/from16 v0, p1

    invoke-virtual {v9, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCrosshairsVisibility:F

    move/from16 v27, v0

    const/16 v28, 0x0

    cmpl-float v27, v27, v28

    if-lez v27, :cond_1

    move-object/from16 v0, p0

    iget v13, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountX:I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountY:I

    const v5, 0x3ecccccd    # 0.4f

    const/16 v6, 0x258

    const v4, 0x3b03126f    # 0.002f

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCrosshairsDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v15}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v24

    invoke-virtual {v15}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v18

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getPaddingLeft()I

    move-result v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mWidthGap:I

    move/from16 v28, v0

    div-int/lit8 v28, v28, 0x2

    sub-int v27, v27, v28

    div-int/lit8 v28, v24, 0x2

    sub-int v25, v27, v28

    const/4 v12, 0x0

    :goto_1
    if-le v12, v13, :cond_5

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlinePaint:Landroid/graphics/Paint;

    move-object/from16 v22, v0

    const/16 v19, 0x0

    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlines:[Landroid/graphics/Point;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    array-length v0, v0

    move/from16 v27, v0

    move/from16 v0, v19

    move/from16 v1, v27

    if-lt v0, v1, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPressedOrFocusedIcon:Lcom/cyanogenmod/trebuchet/BubbleTextView;

    move-object/from16 v27, v0

    if-eqz v27, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPressedOrFocusedIcon:Lcom/cyanogenmod/trebuchet/BubbleTextView;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->getPressedOrFocusedBackgroundPadding()I

    move-result v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPressedOrFocusedIcon:Lcom/cyanogenmod/trebuchet/BubbleTextView;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->getPressedOrFocusedBackground()Landroid/graphics/Bitmap;

    move-result-object v8

    if-eqz v8, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPressedOrFocusedIcon:Lcom/cyanogenmod/trebuchet/BubbleTextView;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->getLeft()I

    move-result v27

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getPaddingLeft()I

    move-result v28

    add-int v27, v27, v28

    sub-int v27, v27, v21

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPressedOrFocusedIcon:Lcom/cyanogenmod/trebuchet/BubbleTextView;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->getTop()I

    move-result v28

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getPaddingTop()I

    move-result v29

    add-int v28, v28, v29

    sub-int v28, v28, v21

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    const/16 v29, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v28

    move-object/from16 v3, v29

    invoke-virtual {v0, v8, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_2
    const/16 v19, 0x0

    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mFolderOuterRings:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Ljava/util/ArrayList;->size()I

    move-result v27

    move/from16 v0, v19

    move/from16 v1, v27

    if-lt v0, v1, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mFolderLeaveBehindCell:[I

    move-object/from16 v27, v0

    const/16 v28, 0x0

    aget v27, v27, v28

    if-ltz v27, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mFolderLeaveBehindCell:[I

    move-object/from16 v27, v0

    const/16 v28, 0x1

    aget v27, v27, v28

    if-ltz v27, :cond_3

    sget-object v15, Lcom/cyanogenmod/trebuchet/FolderIcon;->sSharedFolderLeaveBehind:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v15}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v24

    invoke-virtual {v15}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mFolderLeaveBehindCell:[I

    move-object/from16 v27, v0

    const/16 v28, 0x0

    aget v27, v27, v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mFolderLeaveBehindCell:[I

    move-object/from16 v28, v0

    const/16 v29, 0x1

    aget v28, v28, v29

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mTempLocation:[I

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    move/from16 v1, v27

    move/from16 v2, v28

    move-object/from16 v3, v29

    invoke-virtual {v0, v1, v2, v3}, Lcom/cyanogenmod/trebuchet/CellLayout;->cellToPoint(II[I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mTempLocation:[I

    move-object/from16 v27, v0

    const/16 v28, 0x0

    aget v27, v27, v28

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellWidth:I

    move/from16 v28, v0

    div-int/lit8 v28, v28, 0x2

    add-int v10, v27, v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mTempLocation:[I

    move-object/from16 v27, v0

    const/16 v28, 0x1

    aget v27, v27, v28

    sget v28, Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;->sPreviewSize:I

    div-int/lit8 v28, v28, 0x2

    add-int v11, v27, v28

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    div-int/lit8 v27, v24, 0x2

    sub-int v27, v10, v27

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    div-int/lit8 v28, v24, 0x2

    sub-int v28, v11, v28

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    const/16 v27, 0x0

    const/16 v28, 0x0

    move/from16 v0, v27

    move/from16 v1, v28

    move/from16 v2, v24

    move/from16 v3, v18

    invoke-virtual {v15, v0, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    :cond_3
    return-void

    :cond_4
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mNormalBackground:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_0

    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getPaddingTop()I

    move-result v27

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mHeightGap:I

    move/from16 v28, v0

    div-int/lit8 v28, v28, 0x2

    sub-int v27, v27, v28

    div-int/lit8 v28, v18, 0x2

    sub-int v26, v27, v28

    const/16 v23, 0x0

    :goto_4
    move/from16 v0, v23

    if-le v0, v14, :cond_6

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellWidth:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mWidthGap:I

    move/from16 v28, v0

    add-int v27, v27, v28

    add-int v25, v25, v27

    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_1

    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mTmpPointF:Landroid/graphics/PointF;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragCenter:Landroid/graphics/Point;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v28, v0

    sub-int v28, v25, v28

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragCenter:Landroid/graphics/Point;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v29, v0

    sub-int v29, v26, v29

    move/from16 v0, v29

    int-to-float v0, v0

    move/from16 v29, v0

    invoke-virtual/range {v27 .. v29}, Landroid/graphics/PointF;->set(FF)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mTmpPointF:Landroid/graphics/PointF;

    move-object/from16 v27, v0

    invoke-virtual/range {v27 .. v27}, Landroid/graphics/PointF;->length()F

    move-result v16

    const v27, 0x3ecccccd    # 0.4f

    const v28, 0x3b03126f    # 0.002f

    const/high16 v29, 0x44160000    # 600.0f

    sub-float v29, v29, v16

    mul-float v28, v28, v29

    invoke-static/range {v27 .. v28}, Ljava/lang/Math;->min(FF)F

    move-result v7

    const/16 v27, 0x0

    cmpl-float v27, v7, v27

    if-lez v27, :cond_7

    add-int v27, v25, v24

    add-int v28, v26, v18

    move/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v27

    move/from16 v3, v28

    invoke-virtual {v15, v0, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    const/high16 v27, 0x437f0000    # 255.0f

    mul-float v27, v27, v7

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCrosshairsVisibility:F

    move/from16 v28, v0

    mul-float v27, v27, v28

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move/from16 v0, v27

    invoke-virtual {v15, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellHeight:I

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mHeightGap:I

    move/from16 v28, v0

    add-int v27, v27, v28

    add-int v26, v26, v27

    add-int/lit8 v23, v23, 0x1

    goto/16 :goto_4

    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlineAlphas:[F

    move-object/from16 v27, v0

    aget v7, v27, v19

    const/16 v27, 0x0

    cmpl-float v27, v7, v27

    if-lez v27, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlines:[Landroid/graphics/Point;

    move-object/from16 v27, v0

    aget-object v20, v27, v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlineAnims:[Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;

    move-object/from16 v27, v0

    aget-object v27, v27, v19

    invoke-virtual/range {v27 .. v27}, Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;->getTag()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/Bitmap;

    const/high16 v27, 0x3f000000    # 0.5f

    add-float v27, v27, v7

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v27, v0

    move-object/from16 v0, v22

    move/from16 v1, v27

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Point;->x:I

    move/from16 v27, v0

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Point;->y:I

    move/from16 v28, v0

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v28

    move-object/from16 v3, v22

    invoke-virtual {v0, v8, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_9
    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_2

    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mFolderOuterRings:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    move-object/from16 v0, v27

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;

    sget-object v15, Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;->sSharedOuterRingDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual/range {v17 .. v17}, Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;->getOuterRingSize()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v24, v0

    move/from16 v18, v24

    move-object/from16 v0, v17

    iget v0, v0, Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;->mCellX:I

    move/from16 v27, v0

    move-object/from16 v0, v17

    iget v0, v0, Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;->mCellY:I

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mTempLocation:[I

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    move/from16 v1, v27

    move/from16 v2, v28

    move-object/from16 v3, v29

    invoke-virtual {v0, v1, v2, v3}, Lcom/cyanogenmod/trebuchet/CellLayout;->cellToPoint(II[I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mTempLocation:[I

    move-object/from16 v27, v0

    const/16 v28, 0x0

    aget v27, v27, v28

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellWidth:I

    move/from16 v28, v0

    div-int/lit8 v28, v28, 0x2

    add-int v10, v27, v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mTempLocation:[I

    move-object/from16 v27, v0

    const/16 v28, 0x1

    aget v27, v27, v28

    sget v28, Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;->sPreviewSize:I

    div-int/lit8 v28, v28, 0x2

    add-int v11, v27, v28

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    div-int/lit8 v27, v24, 0x2

    sub-int v27, v10, v27

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    div-int/lit8 v28, v18, 0x2

    sub-int v28, v11, v28

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    const/16 v27, 0x0

    const/16 v28, 0x0

    move/from16 v0, v27

    move/from16 v1, v28

    move/from16 v2, v24

    move/from16 v3, v18

    invoke-virtual {v15, v0, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    sget-object v15, Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;->sSharedInnerRingDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual/range {v17 .. v17}, Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;->getInnerRingSize()F

    move-result v27

    move/from16 v0, v27

    float-to-int v0, v0

    move/from16 v24, v0

    move/from16 v18, v24

    move-object/from16 v0, v17

    iget v0, v0, Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;->mCellX:I

    move/from16 v27, v0

    move-object/from16 v0, v17

    iget v0, v0, Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;->mCellY:I

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mTempLocation:[I

    move-object/from16 v29, v0

    move-object/from16 v0, p0

    move/from16 v1, v27

    move/from16 v2, v28

    move-object/from16 v3, v29

    invoke-virtual {v0, v1, v2, v3}, Lcom/cyanogenmod/trebuchet/CellLayout;->cellToPoint(II[I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mTempLocation:[I

    move-object/from16 v27, v0

    const/16 v28, 0x0

    aget v27, v27, v28

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellWidth:I

    move/from16 v28, v0

    div-int/lit8 v28, v28, 0x2

    add-int v10, v27, v28

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mTempLocation:[I

    move-object/from16 v27, v0

    const/16 v28, 0x1

    aget v27, v27, v28

    sget v28, Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;->sPreviewSize:I

    div-int/lit8 v28, v28, 0x2

    add-int v11, v27, v28

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    div-int/lit8 v27, v24, 0x2

    sub-int v27, v10, v27

    move/from16 v0, v27

    int-to-float v0, v0

    move/from16 v27, v0

    div-int/lit8 v28, v24, 0x2

    sub-int v28, v11, v28

    move/from16 v0, v28

    int-to-float v0, v0

    move/from16 v28, v0

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    const/16 v27, 0x0

    const/16 v28, 0x0

    move/from16 v0, v27

    move/from16 v1, v28

    move/from16 v2, v24

    move/from16 v3, v18

    invoke-virtual {v15, v0, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    move-object/from16 v0, p1

    invoke-virtual {v15, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_3
.end method

.method onDropChild(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->dropped:Z

    invoke-virtual {p1}, Landroid/view/View;->requestLayout()V

    :cond_0
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->clearTagCellInfo()V

    :cond_0
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mInterceptTouchListener:Landroid/view/View$OnTouchListener;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mInterceptTouchListener:Landroid/view/View$OnTouchListener;

    invoke-interface {v1, p0, p1}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    if-nez v0, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p0, v1, v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->setTagToCellInfoForPoint(II)V

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 8
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget v3, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPaddingLeft:I

    iget v4, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPaddingTop:I

    sub-int v5, p4, p2

    iget v6, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPaddingRight:I

    sub-int/2addr v5, v6

    sub-int v6, p5, p3

    iget v7, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPaddingBottom:I

    sub-int/2addr v6, v7

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/view/View;->layout(IIII)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 24
    .param p1    # I
    .param p2    # I

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v17

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v18

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v8

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v9

    if-eqz v17, :cond_0

    if-nez v8, :cond_1

    :cond_0
    new-instance v19, Ljava/lang/RuntimeException;

    const-string v20, "CellLayout cannot have UNSPECIFIED dimensions"

    invoke-direct/range {v19 .. v20}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v19

    :cond_1
    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountX:I

    move/from16 v19, v0

    add-int/lit8 v14, v19, -0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountY:I

    move/from16 v19, v0

    add-int/lit8 v13, v19, -0x1

    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->isScreenLarge()Z

    move-result v19

    if-nez v19, :cond_2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPaddingLeft:I

    move/from16 v19, v0

    sub-int v19, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPaddingRight:I

    move/from16 v20, v0

    sub-int v19, v19, v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountX:I

    move/from16 v20, v0

    div-int v19, v19, v20

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/cyanogenmod/trebuchet/CellLayout;->mOriginalCellWidth:I

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellWidth:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPaddingTop:I

    move/from16 v19, v0

    sub-int v19, v9, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPaddingBottom:I

    move/from16 v20, v0

    sub-int v19, v19, v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountY:I

    move/from16 v20, v0

    div-int v19, v19, v20

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/cyanogenmod/trebuchet/CellLayout;->mOriginalCellHeight:I

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellHeight:I

    :cond_2
    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOriginalWidthGap:I

    move/from16 v19, v0

    if-ltz v19, :cond_3

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOriginalHeightGap:I

    move/from16 v19, v0

    if-gez v19, :cond_7

    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPaddingLeft:I

    move/from16 v19, v0

    sub-int v19, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPaddingRight:I

    move/from16 v20, v0

    sub-int v7, v19, v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPaddingTop:I

    move/from16 v19, v0

    sub-int v19, v9, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPaddingBottom:I

    move/from16 v20, v0

    sub-int v16, v19, v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountX:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOriginalCellWidth:I

    move/from16 v20, v0

    mul-int v19, v19, v20

    sub-int v6, v7, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountY:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOriginalCellHeight:I

    move/from16 v20, v0

    mul-int v19, v19, v20

    sub-int v15, v16, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mMaxGap:I

    move/from16 v20, v0

    if-lez v14, :cond_5

    div-int v19, v6, v14

    :goto_0
    move/from16 v0, v20

    move/from16 v1, v19

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/cyanogenmod/trebuchet/CellLayout;->mWidthGap:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mMaxGap:I

    move/from16 v20, v0

    if-lez v13, :cond_6

    div-int v19, v15, v13

    :goto_1
    move/from16 v0, v20

    move/from16 v1, v19

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/cyanogenmod/trebuchet/CellLayout;->mHeightGap:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellWidth:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellHeight:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mWidthGap:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mHeightGap:I

    move/from16 v23, v0

    invoke-virtual/range {v19 .. v23}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->setCellDimensions(IIII)V

    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellWidth:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellHeight:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mWidthGap:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mHeightGap:I

    move/from16 v23, v0

    invoke-virtual/range {v19 .. v23}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->setCellDimensions(IIII)V

    move/from16 v12, v18

    move v11, v9

    const/high16 v19, -0x80000000

    move/from16 v0, v17

    move/from16 v1, v19

    if-ne v0, v1, :cond_4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPaddingLeft:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPaddingRight:I

    move/from16 v20, v0

    add-int v19, v19, v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountX:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellWidth:I

    move/from16 v21, v0

    mul-int v20, v20, v21

    add-int v19, v19, v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountX:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, -0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mWidthGap:I

    move/from16 v21, v0

    mul-int v20, v20, v21

    add-int v12, v19, v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPaddingTop:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPaddingBottom:I

    move/from16 v20, v0

    add-int v19, v19, v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountY:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellHeight:I

    move/from16 v21, v0

    mul-int v20, v20, v21

    add-int v19, v19, v20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountY:I

    move/from16 v20, v0

    add-int/lit8 v20, v20, -0x1

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mHeightGap:I

    move/from16 v21, v0

    mul-int v20, v20, v21

    add-int v11, v19, v20

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v11}, Lcom/cyanogenmod/trebuchet/CellLayout;->setMeasuredDimension(II)V

    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildCount()I

    move-result v5

    const/4 v10, 0x0

    :goto_3
    if-lt v10, v5, :cond_8

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v11}, Lcom/cyanogenmod/trebuchet/CellLayout;->setMeasuredDimension(II)V

    return-void

    :cond_5
    const/16 v19, 0x0

    goto/16 :goto_0

    :cond_6
    const/16 v19, 0x0

    goto/16 :goto_1

    :cond_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOriginalWidthGap:I

    move/from16 v19, v0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/cyanogenmod/trebuchet/CellLayout;->mWidthGap:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOriginalHeightGap:I

    move/from16 v19, v0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/cyanogenmod/trebuchet/CellLayout;->mHeightGap:I

    goto/16 :goto_2

    :cond_8
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPaddingLeft:I

    move/from16 v19, v0

    sub-int v19, v12, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPaddingRight:I

    move/from16 v20, v0

    sub-int v19, v19, v20

    const/high16 v20, 0x40000000    # 2.0f

    invoke-static/range {v19 .. v20}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPaddingTop:I

    move/from16 v19, v0

    sub-int v19, v11, v19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPaddingBottom:I

    move/from16 v20, v0

    sub-int v19, v19, v20

    const/high16 v20, 0x40000000    # 2.0f

    invoke-static/range {v19 .. v20}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/view/View;->measure(II)V

    add-int/lit8 v10, v10, 0x1

    goto :goto_3
.end method

.method public onMove(Landroid/view/View;II)V
    .locals 7
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/CellLayout;->markCellsAsUnoccupiedForView(Landroid/view/View;)V

    iget v3, v6, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellHSpan:I

    iget v4, v6, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellVSpan:I

    const/4 v5, 0x1

    move-object v0, p0

    move v1, p2

    move v2, p3

    invoke-direct/range {v0 .. v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->markCellsForView(IIIIZ)V

    return-void
.end method

.method protected onSetAlpha(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    return v0
.end method

.method protected onSizeChanged(IIII)V
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v1, 0x0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onSizeChanged(IIII)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mBackgroundRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1, v1, p1, p2}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mForegroundRect:Landroid/graphics/Rect;

    iget v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mForegroundPadding:I

    iget v2, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mForegroundPadding:I

    iget v3, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mForegroundPadding:I

    mul-int/lit8 v3, v3, 0x2

    sub-int v3, p1, v3

    iget v4, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mForegroundPadding:I

    mul-int/lit8 v4, v4, 0x2

    sub-int v4, p2, v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    return-void
.end method

.method pointToCellExact(II[I)V
    .locals 9
    .param p1    # I
    .param p2    # I
    .param p3    # [I

    const/4 v8, 0x1

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getPaddingTop()I

    move-result v1

    sub-int v4, p1, v0

    iget v5, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellWidth:I

    iget v6, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mWidthGap:I

    add-int/2addr v5, v6

    div-int/2addr v4, v5

    aput v4, p3, v7

    sub-int v4, p2, v1

    iget v5, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellHeight:I

    iget v6, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mHeightGap:I

    add-int/2addr v5, v6

    div-int/2addr v4, v5

    aput v4, p3, v8

    iget v2, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountX:I

    iget v3, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountY:I

    aget v4, p3, v7

    if-gez v4, :cond_0

    aput v7, p3, v7

    :cond_0
    aget v4, p3, v7

    if-lt v4, v2, :cond_1

    add-int/lit8 v4, v2, -0x1

    aput v4, p3, v7

    :cond_1
    aget v4, p3, v8

    if-gez v4, :cond_2

    aput v7, p3, v8

    :cond_2
    aget v4, p3, v8

    if-lt v4, v3, :cond_3

    add-int/lit8 v4, v3, -0x1

    aput v4, p3, v8

    :cond_3
    return-void
.end method

.method pointToCellRounded(II[I)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # [I

    iget v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellWidth:I

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, p1

    iget v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellHeight:I

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, p2

    invoke-virtual {p0, v0, v1, p3}, Lcom/cyanogenmod/trebuchet/CellLayout;->pointToCellExact(II[I)V

    return-void
.end method

.method public rectToCell(II[I)[I
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # [I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, p1, p2, p3}, Lcom/cyanogenmod/trebuchet/CellLayout;->rectToCell(Landroid/content/res/Resources;II[I)[I

    move-result-object v0

    return-object v0
.end method

.method public refreshGuideView()V
    .locals 2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mGuideLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mGuideLayout:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->getChildCount()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x4

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeAllViews()V
    .locals 1

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->clearOccupiedCells()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->removeAllViews()V

    return-void
.end method

.method public removeAllViewsInLayout()V
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->clearOccupiedCells()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->removeAllViewsInLayout()V

    :cond_0
    return-void
.end method

.method public removeView(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/CellLayout;->markCellsAsUnoccupiedForView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->removeView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->refreshGuideView()V

    return-void
.end method

.method public removeViewAt(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->markCellsAsUnoccupiedForView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->removeViewAt(I)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->refreshGuideView()V

    return-void
.end method

.method public removeViewInLayout(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/CellLayout;->markCellsAsUnoccupiedForView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->removeViewInLayout(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->refreshGuideView()V

    return-void
.end method

.method public removeViewWithoutMarkingCells(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->removeView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->refreshGuideView()V

    return-void
.end method

.method public removeViews(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    move v0, p1

    :goto_0
    add-int v1, p1, p2

    if-lt v0, v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    invoke-virtual {v1, p1, p2}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->removeViews(II)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    invoke-virtual {v1, v0}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->markCellsAsUnoccupiedForView(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public removeViewsInLayout(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    move v0, p1

    :goto_0
    add-int v1, p1, p2

    if-lt v0, v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    invoke-virtual {v1, p1, p2}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->removeViewsInLayout(II)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    invoke-virtual {v1, v0}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->markCellsAsUnoccupiedForView(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public requestFocus(ILandroid/graphics/Rect;)Z
    .locals 4
    .param p1    # I
    .param p2    # Landroid/graphics/Rect;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v1, p0}, Lcom/cyanogenmod/trebuchet/Workspace;->isSearchScreen(Lcom/cyanogenmod/trebuchet/CellLayout;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    invoke-virtual {v2, v3}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    instance-of v2, v2, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    invoke-virtual {v2, v3}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->requestChildFocus()Z

    move-result v2

    :goto_0
    return v2

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v2

    goto :goto_0
.end method

.method protected resetOverscrollTransforms()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mScrollingTransformsDirty:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, v3}, Lcom/cyanogenmod/trebuchet/CellLayout;->setOverscrollTransformsDirty(Z)V

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->setTranslationX(F)V

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->setRotationY(F)V

    const/high16 v0, 0x44a00000    # 1280.0f

    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->getScreenDensity()F

    move-result v1

    mul-float/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->setCameraDistance(F)V

    invoke-virtual {p0, v2, v3}, Lcom/cyanogenmod/trebuchet/CellLayout;->setOverScrollAmount(FZ)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getMeasuredWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->setPivotX(F)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getMeasuredHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->setPivotY(F)V

    :cond_0
    return-void
.end method

.method public setAlpha(F)V
    .locals 0
    .param p1    # F

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/CellLayout;->setChildrenAlpha(F)V

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setAlpha(F)V

    return-void
.end method

.method public setBackgroundAlpha(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mBackgroundAlpha:F

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->invalidate()V

    return-void
.end method

.method public setBackgroundAlphaMultiplier(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mBackgroundAlphaMultiplier:F

    return-void
.end method

.method protected setChildrenDrawingCacheEnabled(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->setChildrenDrawingCacheEnabled(Z)V

    return-void
.end method

.method protected setChildrenDrawnWithCacheEnabled(Z)V
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->setChildrenDrawnWithCacheEnabled(Z)V

    return-void
.end method

.method public setFastBackgroundAlpha(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mBackgroundAlpha:F

    return-void
.end method

.method public setFolderLeaveBehindCell(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mFolderLeaveBehindCell:[I

    const/4 v1, 0x0

    aput p1, v0, v1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mFolderLeaveBehindCell:[I

    const/4 v1, 0x1

    aput p2, v0, v1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->invalidate()V

    return-void
.end method

.method public setGridSize(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    iput p1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountX:I

    iput p2, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountY:I

    iget v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountX:I

    iget v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCountY:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Z

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOccupied:[[Z

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->requestLayout()V

    return-void
.end method

.method setIsDragOverlapping(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mIsDragOverlapping:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mIsDragOverlapping:Z

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->invalidate()V

    :cond_0
    return-void
.end method

.method public setOnInterceptTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 0
    .param p1    # Landroid/view/View$OnTouchListener;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mInterceptTouchListener:Landroid/view/View$OnTouchListener;

    return-void
.end method

.method setOverScrollAmount(FZ)V
    .locals 2
    .param p1    # F
    .param p2    # Z

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOverScrollForegroundDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOverScrollLeft:Landroid/graphics/drawable/Drawable;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOverScrollLeft:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOverScrollForegroundDrawable:Landroid/graphics/drawable/Drawable;

    :cond_0
    :goto_0
    const/high16 v0, 0x437f0000    # 255.0f

    mul-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mForegroundAlpha:I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOverScrollForegroundDrawable:Landroid/graphics/drawable/Drawable;

    iget v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mForegroundAlpha:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->invalidate()V

    return-void

    :cond_1
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOverScrollForegroundDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOverScrollRight:Landroid/graphics/drawable/Drawable;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOverScrollRight:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mOverScrollForegroundDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method protected setOverscrollTransformsDirty(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mScrollingTransformsDirty:Z

    return-void
.end method

.method setPressedOrFocusedIcon(Lcom/cyanogenmod/trebuchet/BubbleTextView;)V
    .locals 2
    .param p1    # Lcom/cyanogenmod/trebuchet/BubbleTextView;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPressedOrFocusedIcon:Lcom/cyanogenmod/trebuchet/BubbleTextView;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPressedOrFocusedIcon:Lcom/cyanogenmod/trebuchet/BubbleTextView;

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->invalidateBubbleTextView(Lcom/cyanogenmod/trebuchet/BubbleTextView;)V

    :cond_0
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPressedOrFocusedIcon:Lcom/cyanogenmod/trebuchet/BubbleTextView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPressedOrFocusedIcon:Lcom/cyanogenmod/trebuchet/BubbleTextView;

    invoke-direct {p0, v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->invalidateBubbleTextView(Lcom/cyanogenmod/trebuchet/BubbleTextView;)V

    :cond_1
    return-void
.end method

.method public setTagToCellInfoForPoint(II)V
    .locals 13
    .param p1    # I
    .param p2    # I

    const/4 v12, 0x1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellInfo:Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mRect:Landroid/graphics/Rect;

    iget v10, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mScrollX:I

    add-int v8, p1, v10

    iget v10, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mScrollY:I

    add-int v9, p2, v10

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    invoke-virtual {v10}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->getChildCount()I

    move-result v3

    const/4 v4, 0x0

    add-int/lit8 v6, v3, -0x1

    :goto_0
    if-gez v6, :cond_1

    :goto_1
    iput-boolean v4, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mLastDownOnOccupiedCell:Z

    if-nez v4, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mTmpXY:[I

    invoke-virtual {p0, v8, v9, v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->pointToCellExact(II[I)V

    const/4 v10, 0x0

    iput-object v10, v0, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    const/4 v10, 0x0

    aget v10, v1, v10

    iput v10, v0, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cellX:I

    aget v10, v1, v12

    iput v10, v0, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cellY:I

    iput v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->spanX:I

    iput v12, v0, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->spanY:I

    :cond_0
    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->setTag(Ljava/lang/Object;)V

    return-void

    :cond_1
    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mChildren:Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    invoke-virtual {v10, v6}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v10

    if-eqz v10, :cond_2

    invoke-virtual {v2}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v10

    if-eqz v10, :cond_3

    :cond_2
    iget-boolean v10, v7, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->isLockedToGrid:Z

    if-eqz v10, :cond_3

    invoke-virtual {v2, v5}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    iget v10, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPaddingLeft:I

    iget v11, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mPaddingTop:I

    invoke-virtual {v5, v10, v11}, Landroid/graphics/Rect;->offset(II)V

    invoke-virtual {v5, v8, v9}, Landroid/graphics/Rect;->contains(II)Z

    move-result v10

    if-eqz v10, :cond_3

    iput-object v2, v0, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cell:Landroid/view/View;

    iget v10, v7, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellX:I

    iput v10, v0, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cellX:I

    iget v10, v7, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellY:I

    iput v10, v0, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->cellY:I

    iget v10, v7, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellHSpan:I

    iput v10, v0, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->spanX:I

    iget v10, v7, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellVSpan:I

    iput v10, v0, Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;->spanY:I

    const/4 v4, 0x1

    goto :goto_1

    :cond_3
    add-int/lit8 v6, v6, -0x1

    goto :goto_0
.end method

.method public setupGuideView(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mInflater:Landroid/view/LayoutInflater;

    const/high16 v2, 0x7f030000    # com.konka.avenger.R.layout.add_guide

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v1, 0x7f0d000a    # com.konka.avenger.R.id.add_guide

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mGuideLayout:Landroid/widget/LinearLayout;

    const v1, 0x7f0d000c    # com.konka.avenger.R.id.add_guide_btn

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mGuideBtn:Landroid/widget/Button;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mGuideBtn:Landroid/widget/Button;

    new-instance v2, Lcom/cyanogenmod/trebuchet/CellLayout$4;

    invoke-direct {v2, p0}, Lcom/cyanogenmod/trebuchet/CellLayout$4;-><init>(Lcom/cyanogenmod/trebuchet/CellLayout;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mGuideBtn:Landroid/widget/Button;

    new-instance v2, Lcom/cyanogenmod/trebuchet/CellLayout$5;

    invoke-direct {v2, p0}, Lcom/cyanogenmod/trebuchet/CellLayout$5;-><init>(Lcom/cyanogenmod/trebuchet/CellLayout;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method public shouldDelayChildPressedState()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public showFolderAccept(Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;)V
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout;->mFolderOuterRings:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method visualizeDropLocation(Landroid/view/View;Landroid/graphics/Bitmap;IIIILandroid/graphics/Point;Landroid/graphics/Rect;)V
    .locals 17
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/graphics/Bitmap;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # Landroid/graphics/Point;
    .param p8    # Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragCell:[I

    const/4 v3, 0x0

    aget v12, v2, v3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragCell:[I

    const/4 v3, 0x1

    aget v13, v2, v3

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragCell:[I

    move-object/from16 v2, p0

    move/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p1

    invoke-virtual/range {v2 .. v8}, Lcom/cyanogenmod/trebuchet/CellLayout;->findNearestVacantArea(IIIILandroid/view/View;[I)[I

    move-result-object v11

    if-eqz p1, :cond_1

    if-nez p7, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragCenter:Landroid/graphics/Point;

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int v3, v3, p3

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int v4, v4, p4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Point;->set(II)V

    :goto_0
    if-nez p2, :cond_2

    if-nez p1, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCrosshairsDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->invalidate()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragCenter:Landroid/graphics/Point;

    move/from16 v0, p3

    move/from16 v1, p4

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    :cond_2
    if-eqz v11, :cond_4

    const/4 v2, 0x0

    aget v2, v11, v2

    if-ne v2, v12, :cond_3

    const/4 v2, 0x1

    aget v2, v11, v2

    if-eq v2, v13, :cond_4

    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mTmpPoint:[I

    move-object/from16 v16, v0

    const/4 v2, 0x0

    aget v2, v11, v2

    const/4 v3, 0x1

    aget v3, v11, v3

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v2, v3, v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->cellToPoint(II[I)V

    const/4 v2, 0x0

    aget v9, v16, v2

    const/4 v2, 0x1

    aget v15, v16, v2

    if-eqz p1, :cond_5

    if-nez p7, :cond_5

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v2, v10, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v9, v2

    iget v2, v10, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v15, v2

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v15, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellWidth:I

    mul-int v2, v2, p5

    add-int/lit8 v3, p5, -0x1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mWidthGap:I

    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v9, v2

    :goto_2
    move-object/from16 v0, p0

    iget v14, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlineCurrent:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlineAnims:[Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;

    aget-object v2, v2, v14

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;->animateOut()V

    add-int/lit8 v2, v14, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlines:[Landroid/graphics/Point;

    array-length v3, v3

    rem-int/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlineCurrent:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlines:[Landroid/graphics/Point;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlineCurrent:I

    aget-object v2, v2, v3

    invoke-virtual {v2, v9, v15}, Landroid/graphics/Point;->set(II)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlineAnims:[Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlineCurrent:I

    aget-object v2, v2, v3

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;->setTag(Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlineAnims:[Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mDragOutlineCurrent:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/InterruptibleInOutAnimator;->animateIn()V

    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCrosshairsDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->invalidate()V

    goto/16 :goto_1

    :cond_5
    if-eqz p7, :cond_6

    if-eqz p8, :cond_6

    move-object/from16 v0, p7

    iget v2, v0, Landroid/graphics/Point;->x:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellWidth:I

    mul-int v3, v3, p5

    add-int/lit8 v4, p5, -0x1

    move-object/from16 v0, p0

    iget v5, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mWidthGap:I

    mul-int/2addr v4, v5

    add-int/2addr v3, v4

    invoke-virtual/range {p8 .. p8}, Landroid/graphics/Rect;->width()I

    move-result v4

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    add-int/2addr v9, v2

    move-object/from16 v0, p7

    iget v2, v0, Landroid/graphics/Point;->y:I

    add-int/2addr v15, v2

    goto :goto_2

    :cond_6
    move-object/from16 v0, p0

    iget v2, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellWidth:I

    mul-int v2, v2, p5

    add-int/lit8 v3, p5, -0x1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mWidthGap:I

    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v9, v2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mCellHeight:I

    mul-int v2, v2, p6

    add-int/lit8 v3, p6, -0x1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/cyanogenmod/trebuchet/CellLayout;->mHeightGap:I

    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v15, v2

    goto/16 :goto_2
.end method
