.class Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;
.super Ljava/lang/Object;
.source "LauncherModel.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cyanogenmod/trebuchet/LauncherModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoaderTask"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mIsLaunching:Z

.field private mLabelCache:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private mLoadAndBindStepFinished:Z

.field private mStopped:Z

.field private mWaitThread:Ljava/lang/Thread;

.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;


# direct methods
.method constructor <init>(Lcom/cyanogenmod/trebuchet/LauncherModel;Landroid/content/Context;Z)V
    .locals 1
    .param p2    # Landroid/content/Context;
    .param p3    # Z

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->mContext:Landroid/content/Context;

    iput-boolean p3, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->mIsLaunching:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->mLabelCache:Ljava/util/HashMap;

    return-void
.end method

.method static synthetic access$0(Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->mLoadAndBindStepFinished:Z

    return-void
.end method

.method private bindWorkspace()V
    .locals 14

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mCallbacks:Ljava/lang/ref/WeakReference;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$4(Lcom/cyanogenmod/trebuchet/LauncherModel;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;

    if-nez v2, :cond_0

    const-string v0, "Launcher.Model"

    const-string v1, "LoaderTask running with no launcher"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mHandler:Lcom/cyanogenmod/trebuchet/DeferredHandler;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$3(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/DeferredHandler;

    move-result-object v0

    new-instance v1, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask$2;

    invoke-direct {v1, p0, v2}, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask$2;-><init>(Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;)V

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/DeferredHandler;->post(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # invokes: Lcom/cyanogenmod/trebuchet/LauncherModel;->unbindWorkspaceItemsOnMainThread()Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$12(Lcom/cyanogenmod/trebuchet/LauncherModel;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/4 v9, 0x0

    :goto_1
    if-lt v9, v6, :cond_1

    new-instance v8, Ljava/util/HashMap;

    sget-object v0, Lcom/cyanogenmod/trebuchet/LauncherModel;->sFolders:Ljava/util/HashMap;

    invoke-direct {v8, v0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mHandler:Lcom/cyanogenmod/trebuchet/DeferredHandler;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$3(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/DeferredHandler;

    move-result-object v0

    new-instance v1, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask$4;

    invoke-direct {v1, p0, v2, v8}, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask$4;-><init>(Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;Ljava/util/HashMap;)V

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/DeferredHandler;->post(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mHandler:Lcom/cyanogenmod/trebuchet/DeferredHandler;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$3(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/DeferredHandler;

    move-result-object v0

    new-instance v1, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask$5;

    invoke-direct {v1, p0}, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask$5;-><init>(Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;)V

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/DeferredHandler;->post(Ljava/lang/Runnable;)V

    invoke-interface {v2}, Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;->getCurrentWorkspaceScreen()I

    move-result v7

    sget-object v0, Lcom/cyanogenmod/trebuchet/LauncherModel;->sAppWidgets:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/4 v9, 0x0

    :goto_2
    if-lt v9, v6, :cond_3

    const/4 v9, 0x0

    :goto_3
    if-lt v9, v6, :cond_5

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mHandler:Lcom/cyanogenmod/trebuchet/DeferredHandler;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$3(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/DeferredHandler;

    move-result-object v0

    new-instance v1, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask$8;

    invoke-direct {v1, p0, v2}, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask$8;-><init>(Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;)V

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/DeferredHandler;->post(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mHandler:Lcom/cyanogenmod/trebuchet/DeferredHandler;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$3(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/DeferredHandler;

    move-result-object v0

    new-instance v1, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask$9;

    invoke-direct {v1, p0, v10, v11}, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask$9;-><init>(Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;J)V

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/DeferredHandler;->post(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_1
    move v4, v9

    add-int/lit8 v0, v9, 0xa

    if-gt v0, v6, :cond_2

    const/16 v5, 0xa

    :goto_4
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mHandler:Lcom/cyanogenmod/trebuchet/DeferredHandler;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$3(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/DeferredHandler;

    move-result-object v13

    new-instance v0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask$3;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask$3;-><init>(Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;Ljava/util/ArrayList;II)V

    invoke-virtual {v13, v0}, Lcom/cyanogenmod/trebuchet/DeferredHandler;->post(Ljava/lang/Runnable;)V

    add-int/lit8 v9, v9, 0xa

    goto :goto_1

    :cond_2
    sub-int v5, v6, v9

    goto :goto_4

    :cond_3
    sget-object v0, Lcom/cyanogenmod/trebuchet/LauncherModel;->sAppWidgets:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;

    iget v0, v12, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->screen:I

    if-ne v0, v7, :cond_4

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mHandler:Lcom/cyanogenmod/trebuchet/DeferredHandler;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$3(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/DeferredHandler;

    move-result-object v0

    new-instance v1, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask$6;

    invoke-direct {v1, p0, v2, v12}, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask$6;-><init>(Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;)V

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/DeferredHandler;->post(Ljava/lang/Runnable;)V

    :cond_4
    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    :cond_5
    sget-object v0, Lcom/cyanogenmod/trebuchet/LauncherModel;->sAppWidgets:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;

    iget v0, v12, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->screen:I

    if-eq v0, v7, :cond_6

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mHandler:Lcom/cyanogenmod/trebuchet/DeferredHandler;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$3(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/DeferredHandler;

    move-result-object v0

    new-instance v1, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask$7;

    invoke-direct {v1, p0, v2, v12}, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask$7;-><init>(Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;)V

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/DeferredHandler;->post(Ljava/lang/Runnable;)V

    :cond_6
    add-int/lit8 v9, v9, 0x1

    goto :goto_3
.end method

.method private checkItemPlacement([[[Lcom/cyanogenmod/trebuchet/ItemInfo;Lcom/cyanogenmod/trebuchet/ItemInfo;)Z
    .locals 10
    .param p1    # [[[Lcom/cyanogenmod/trebuchet/ItemInfo;
    .param p2    # Lcom/cyanogenmod/trebuchet/ItemInfo;

    const/4 v9, 0x7

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget v0, p2, Lcom/cyanogenmod/trebuchet/ItemInfo;->screen:I

    iget-wide v5, p2, Lcom/cyanogenmod/trebuchet/ItemInfo;->container:J

    const-wide/16 v7, -0x65

    cmp-long v5, v5, v7

    if-nez v5, :cond_2

    iget v5, p2, Lcom/cyanogenmod/trebuchet/ItemInfo;->screen:I

    invoke-static {v5}, Lcom/cyanogenmod/trebuchet/Hotseat;->isAllAppsButtonRank(I)Z

    move-result v5

    if-eqz v5, :cond_0

    :goto_0
    return v3

    :cond_0
    aget-object v5, p1, v9

    iget v6, p2, Lcom/cyanogenmod/trebuchet/ItemInfo;->screen:I

    aget-object v5, v5, v6

    aget-object v5, v5, v3

    if-eqz v5, :cond_1

    const-string v4, "Launcher.Model"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Error loading shortcut into hotseat "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " into position ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p2, Lcom/cyanogenmod/trebuchet/ItemInfo;->screen:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p2, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellX:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p2, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellY:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") occupied by "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, p1, v9

    iget v7, p2, Lcom/cyanogenmod/trebuchet/ItemInfo;->screen:I

    aget-object v6, v6, v7

    aget-object v6, v6, v3

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    aget-object v5, p1, v9

    iget v6, p2, Lcom/cyanogenmod/trebuchet/ItemInfo;->screen:I

    aget-object v5, v5, v6

    aput-object p2, v5, v3

    move v3, v4

    goto :goto_0

    :cond_2
    iget-wide v5, p2, Lcom/cyanogenmod/trebuchet/ItemInfo;->container:J

    const-wide/16 v7, -0x64

    cmp-long v5, v5, v7

    if-eqz v5, :cond_3

    move v3, v4

    goto :goto_0

    :cond_3
    iget v1, p2, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellX:I

    :goto_1
    iget v5, p2, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellX:I

    iget v6, p2, Lcom/cyanogenmod/trebuchet/ItemInfo;->spanX:I

    add-int/2addr v5, v6

    if-lt v1, v5, :cond_4

    iget v1, p2, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellX:I

    :goto_2
    iget v3, p2, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellX:I

    iget v5, p2, Lcom/cyanogenmod/trebuchet/ItemInfo;->spanX:I

    add-int/2addr v3, v5

    if-lt v1, v3, :cond_7

    move v3, v4

    goto :goto_0

    :cond_4
    iget v2, p2, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellY:I

    :goto_3
    iget v5, p2, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellY:I

    iget v6, p2, Lcom/cyanogenmod/trebuchet/ItemInfo;->spanY:I

    add-int/2addr v5, v6

    if-lt v2, v5, :cond_5

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_5
    aget-object v5, p1, v0

    aget-object v5, v5, v1

    aget-object v5, v5, v2

    if-eqz v5, :cond_6

    const-string v4, "Launcher.Model"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Error loading shortcut "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " into cell ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p2, Lcom/cyanogenmod/trebuchet/ItemInfo;->screen:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ") occupied by "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    aget-object v6, p1, v0

    aget-object v6, v6, v1

    aget-object v6, v6, v2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_7
    iget v2, p2, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellY:I

    :goto_4
    iget v3, p2, Lcom/cyanogenmod/trebuchet/ItemInfo;->cellY:I

    iget v5, p2, Lcom/cyanogenmod/trebuchet/ItemInfo;->spanY:I

    add-int/2addr v3, v5

    if-lt v2, v3, :cond_8

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_8
    aget-object v3, p1, v0

    aget-object v3, v3, v1

    aput-object p2, v3, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_4
.end method

.method private loadAllAppsByBatch()V
    .locals 28

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    move-object/from16 v23, v0

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mCallbacks:Ljava/lang/ref/WeakReference;
    invoke-static/range {v23 .. v23}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$4(Lcom/cyanogenmod/trebuchet/LauncherModel;)Ljava/lang/ref/WeakReference;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;

    if-nez v12, :cond_1

    const-string v23, "Launcher.Model"

    const-string v24, "LoaderTask running with no launcher (loadAllAppsByBatch)"

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v11, Landroid/content/Intent;

    const-string v23, "android.intent.action.MAIN"

    const/16 v24, 0x0

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-direct {v11, v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v23, "android.intent.category.LAUNCHER"

    move-object/from16 v0, v23

    invoke-virtual {v11, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->mContext:Landroid/content/Context;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v13

    const/4 v6, 0x0

    const v4, 0x7fffffff

    const/4 v9, 0x0

    :goto_1
    if-ge v9, v4, :cond_2

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->mStopped:Z

    move/from16 v23, v0

    if-eqz v23, :cond_3

    :cond_2
    const-string v23, "Launcher.Model"

    new-instance v24, Ljava/lang/StringBuilder;

    const-string v25, "cached all "

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, " apps in "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v25

    sub-long v25, v25, v19

    invoke-virtual/range {v24 .. v26}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "ms"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    if-nez v9, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    move-object/from16 v23, v0

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mAllAppsList:Lcom/cyanogenmod/trebuchet/AllAppsList;
    invoke-static/range {v23 .. v23}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$15(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/AllAppsList;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/cyanogenmod/trebuchet/AllAppsList;->clear()V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v14

    const/16 v23, 0x0

    move/from16 v0, v23

    invoke-virtual {v13, v11, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v6

    const-string v23, "Launcher.Model"

    new-instance v24, Ljava/lang/StringBuilder;

    const-string v25, "queryIntentActivities took "

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v25

    sub-long v25, v25, v14

    invoke-virtual/range {v24 .. v26}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "ms"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v6, :cond_0

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v4

    const-string v23, "Launcher.Model"

    new-instance v24, Ljava/lang/StringBuilder;

    const-string v25, "queryIntentActivities got "

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, " apps"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v4, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v16

    new-instance v23, Lcom/cyanogenmod/trebuchet/LauncherModel$ShortcutNameComparator;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->mLabelCache:Ljava/util/HashMap;

    move-object/from16 v24, v0

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    invoke-direct {v0, v13, v1}, Lcom/cyanogenmod/trebuchet/LauncherModel$ShortcutNameComparator;-><init>(Landroid/content/pm/PackageManager;Ljava/util/HashMap;)V

    move-object/from16 v0, v23

    invoke-static {v6, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    const-string v23, "Launcher.Model"

    new-instance v24, Ljava/lang/StringBuilder;

    const-string v25, "sort took "

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v25

    sub-long v25, v25, v16

    invoke-virtual/range {v24 .. v26}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "ms"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v21

    move/from16 v18, v9

    const/4 v10, 0x0

    :goto_2
    if-ge v9, v4, :cond_5

    if-lt v10, v4, :cond_6

    :cond_5
    if-gt v9, v4, :cond_7

    const/4 v8, 0x1

    :goto_3
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->tryGetCallbacks(Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;)Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    move-object/from16 v23, v0

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mAllAppsList:Lcom/cyanogenmod/trebuchet/AllAppsList;
    invoke-static/range {v23 .. v23}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$15(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/AllAppsList;

    move-result-object v23

    move-object/from16 v0, v23

    iget-object v5, v0, Lcom/cyanogenmod/trebuchet/AllAppsList;->added:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    move-object/from16 v23, v0

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mAllAppsList:Lcom/cyanogenmod/trebuchet/AllAppsList;
    invoke-static/range {v23 .. v23}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$15(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/AllAppsList;

    move-result-object v23

    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    iput-object v0, v1, Lcom/cyanogenmod/trebuchet/AllAppsList;->added:Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    move-object/from16 v23, v0

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mHandler:Lcom/cyanogenmod/trebuchet/DeferredHandler;
    invoke-static/range {v23 .. v23}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$3(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/DeferredHandler;

    move-result-object v23

    new-instance v24, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask$11;

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v7, v8, v5}, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask$11;-><init>(Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;ZLjava/util/ArrayList;)V

    invoke-virtual/range {v23 .. v24}, Lcom/cyanogenmod/trebuchet/DeferredHandler;->post(Ljava/lang/Runnable;)V

    const-string v23, "Launcher.Model"

    new-instance v24, Ljava/lang/StringBuilder;

    const-string v25, "batch of "

    invoke-direct/range {v24 .. v25}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-int v25, v9, v18

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, " icons processed in "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v25

    sub-long v25, v25, v21

    invoke-virtual/range {v24 .. v26}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, "ms"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    move-object/from16 v23, v0

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mAllAppsList:Lcom/cyanogenmod/trebuchet/AllAppsList;
    invoke-static/range {v23 .. v23}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$15(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/AllAppsList;

    move-result-object v24

    new-instance v25, Lcom/cyanogenmod/trebuchet/ApplicationInfo;

    invoke-interface {v6, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Landroid/content/pm/ResolveInfo;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    move-object/from16 v26, v0

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mIconCache:Lcom/cyanogenmod/trebuchet/IconCache;
    invoke-static/range {v26 .. v26}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$16(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/IconCache;

    move-result-object v26

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->mLabelCache:Ljava/util/HashMap;

    move-object/from16 v27, v0

    move-object/from16 v0, v25

    move-object/from16 v1, v23

    move-object/from16 v2, v26

    move-object/from16 v3, v27

    invoke-direct {v0, v13, v1, v2, v3}, Lcom/cyanogenmod/trebuchet/ApplicationInfo;-><init>(Landroid/content/pm/PackageManager;Landroid/content/pm/ResolveInfo;Lcom/cyanogenmod/trebuchet/IconCache;Ljava/util/HashMap;)V

    invoke-virtual/range {v24 .. v25}, Lcom/cyanogenmod/trebuchet/AllAppsList;->add(Lcom/cyanogenmod/trebuchet/ApplicationInfo;)V

    add-int/lit8 v9, v9, 0x1

    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_2

    :cond_7
    const/4 v8, 0x0

    goto/16 :goto_3
.end method

.method private loadAndBindAllApps()V
    .locals 3

    const-string v0, "Launcher.Model"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "loadAndBindAllApps mAllAppsLoaded="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mAllAppsLoaded:Z
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$13(Lcom/cyanogenmod/trebuchet/LauncherModel;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mAllAppsLoaded:Z
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$13(Lcom/cyanogenmod/trebuchet/LauncherModel;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->loadAllAppsByBatch()V

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->mStopped:Z

    if-eqz v0, :cond_0

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$14(Lcom/cyanogenmod/trebuchet/LauncherModel;Z)V

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->onlyBindAllApps()V

    goto :goto_0
.end method

.method private loadAndBindWorkspace()V
    .locals 3

    const-string v0, "Launcher.Model"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "loadAndBindWorkspace mWorkspaceLoaded="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mWorkspaceLoaded:Z
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$1(Lcom/cyanogenmod/trebuchet/LauncherModel;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mWorkspaceLoaded:Z
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$1(Lcom/cyanogenmod/trebuchet/LauncherModel;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->loadWorkspace()V

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->mStopped:Z

    if-eqz v0, :cond_0

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$2(Lcom/cyanogenmod/trebuchet/LauncherModel;Z)V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->bindWorkspace()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private loadWorkspace()V
    .locals 56

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v50

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v13}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v43

    invoke-static {v13}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v53

    invoke-virtual/range {v43 .. v43}, Landroid/content/pm/PackageManager;->isSafeMode()Z

    move-result v36

    sget-object v4, Lcom/cyanogenmod/trebuchet/LauncherModel;->sWorkspaceItems:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    sget-object v4, Lcom/cyanogenmod/trebuchet/LauncherModel;->sAppWidgets:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    sget-object v4, Lcom/cyanogenmod/trebuchet/LauncherModel;->sFolders:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    sget-object v4, Lcom/cyanogenmod/trebuchet/LauncherModel;->sItemsIdMap:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    sget-object v4, Lcom/cyanogenmod/trebuchet/LauncherModel;->sDbIconCache:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->clear()V

    new-instance v39, Ljava/util/ArrayList;

    invoke-direct/range {v39 .. v39}, Ljava/util/ArrayList;-><init>()V

    sget-object v4, Lcom/cyanogenmod/trebuchet/LauncherSettings$Favorites;->CONTENT_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    const/16 v4, 0x8

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mCellCountX:I
    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$8()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mCellCountY:I
    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$9()I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    filled-new-array {v4, v5, v7}, [I

    move-result-object v4

    const-class v5, Lcom/cyanogenmod/trebuchet/ItemInfo;

    invoke-static {v5, v4}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v44

    check-cast v44, [[[Lcom/cyanogenmod/trebuchet/ItemInfo;

    :try_start_0
    const-string v4, "_id"

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v32

    const-string v4, "intent"

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v35

    const-string v4, "title"

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    const-string v4, "iconType"

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v14

    const-string v4, "icon"

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v9

    const-string v4, "iconPackage"

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v15

    const-string v4, "iconResource"

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v16

    const-string v4, "container"

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v26

    const-string v4, "itemType"

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v38

    const-string v4, "appWidgetId"

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v20

    const-string v4, "screen"

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v47

    const-string v4, "cellX"

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v22

    const-string v4, "cellY"

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v23

    const-string v4, "spanX"

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v48

    const-string v4, "spanY"

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v49

    const-string v4, "uri"

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v52

    const-string v4, "displayMode"

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v27

    const-string v4, "lock"

    invoke-interface {v8, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v41

    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->mStopped:Z

    if-nez v4, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-nez v4, :cond_3

    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    invoke-virtual/range {v39 .. v39}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_2

    sget-object v4, Lcom/cyanogenmod/trebuchet/LauncherSettings$Favorites;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;

    move-result-object v24

    invoke-virtual/range {v39 .. v39}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_c

    :cond_2
    const-string v4, "Launcher.Model"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "loaded workspace in "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v11

    sub-long v11, v11, v50

    invoke-virtual {v5, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "ms"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "Launcher.Model"

    const-string v5, "workspace layout: "

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v55, 0x0

    :goto_2
    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mCellCountY:I
    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$9()I

    move-result v4

    move/from16 v0, v55

    if-lt v0, v4, :cond_d

    return-void

    :cond_3
    :try_start_1
    move/from16 v0, v38

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v37

    packed-switch v37, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    move/from16 v0, v35

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v34

    const/4 v4, 0x0

    :try_start_2
    move-object/from16 v0, v34

    invoke-static {v0, v4}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_2
    .catch Ljava/net/URISyntaxException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v6

    if-nez v37, :cond_4

    :try_start_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->mLabelCache:Ljava/util/HashMap;

    move-object/from16 v5, v43

    move-object v7, v13

    invoke-virtual/range {v4 .. v11}, Lcom/cyanogenmod/trebuchet/LauncherModel;->getShortcutInfo(Landroid/content/pm/PackageManager;Landroid/content/Intent;Landroid/content/Context;Landroid/database/Cursor;IILjava/util/HashMap;)Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    move-result-object v33

    :goto_3
    if-eqz v33, :cond_6

    move-object/from16 v0, v33

    iput-object v6, v0, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->intent:Landroid/content/Intent;

    move/from16 v0, v32

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    move-object/from16 v0, v33

    iput-wide v4, v0, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->id:J

    move/from16 v0, v26

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v25

    move/from16 v0, v25

    int-to-long v4, v0

    move-object/from16 v0, v33

    iput-wide v4, v0, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->container:J

    move/from16 v0, v47

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object/from16 v0, v33

    iput v4, v0, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->screen:I

    move/from16 v0, v22

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object/from16 v0, v33

    iput v4, v0, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->cellX:I

    move/from16 v0, v23

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object/from16 v0, v33

    iput v4, v0, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->cellY:I

    move/from16 v0, v41

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-nez v4, :cond_5

    const/4 v4, 0x0

    :goto_4
    move-object/from16 v0, v33

    iput-boolean v4, v0, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->isLock:Z

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    move-object/from16 v2, v33

    invoke-direct {v0, v1, v2}, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->checkItemPlacement([[[Lcom/cyanogenmod/trebuchet/ItemInfo;Lcom/cyanogenmod/trebuchet/ItemInfo;)Z

    move-result v4

    if-eqz v4, :cond_0

    packed-switch v25, :pswitch_data_1

    sget-object v4, Lcom/cyanogenmod/trebuchet/LauncherModel;->sFolders:Ljava/util/HashMap;

    move/from16 v0, v25

    int-to-long v11, v0

    # invokes: Lcom/cyanogenmod/trebuchet/LauncherModel;->findOrMakeFolder(Ljava/util/HashMap;J)Lcom/cyanogenmod/trebuchet/FolderInfo;
    invoke-static {v4, v11, v12}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$11(Ljava/util/HashMap;J)Lcom/cyanogenmod/trebuchet/FolderInfo;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/FolderInfo;->add(Lcom/cyanogenmod/trebuchet/ShortcutInfo;)V

    :goto_5
    sget-object v4, Lcom/cyanogenmod/trebuchet/LauncherModel;->sItemsIdMap:Ljava/util/HashMap;

    move-object/from16 v0, v33

    iget-wide v11, v0, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->id:J

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v33

    invoke-virtual {v4, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    sget-object v5, Lcom/cyanogenmod/trebuchet/LauncherModel;->sDbIconCache:Ljava/util/HashMap;

    move-object/from16 v0, v33

    invoke-virtual {v4, v5, v0, v8, v9}, Lcom/cyanogenmod/trebuchet/LauncherModel;->queueIconToBeChecked(Ljava/util/HashMap;Lcom/cyanogenmod/trebuchet/ShortcutInfo;Landroid/database/Cursor;I)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    :catch_0
    move-exception v28

    :try_start_4
    const-string v4, "Launcher.Model"

    const-string v5, "Desktop items loading interrupted:"

    move-object/from16 v0, v28

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v4

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v4

    :catch_1
    move-exception v28

    goto/16 :goto_0

    :cond_4
    :try_start_5
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    move-object v12, v8

    move/from16 v17, v9

    move/from16 v18, v10

    # invokes: Lcom/cyanogenmod/trebuchet/LauncherModel;->getShortcutInfo(Landroid/database/Cursor;Landroid/content/Context;IIIII)Lcom/cyanogenmod/trebuchet/ShortcutInfo;
    invoke-static/range {v11 .. v18}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$10(Lcom/cyanogenmod/trebuchet/LauncherModel;Landroid/database/Cursor;Landroid/content/Context;IIIII)Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    move-result-object v33

    goto/16 :goto_3

    :cond_5
    const/4 v4, 0x1

    goto :goto_4

    :pswitch_2
    sget-object v4, Lcom/cyanogenmod/trebuchet/LauncherModel;->sWorkspaceItems:Ljava/util/ArrayList;

    move-object/from16 v0, v33

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_6
    move/from16 v0, v32

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v30

    const-string v4, "Launcher.Model"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "Error loading shortcut "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, v30

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ", removing it"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x0

    move-wide/from16 v0, v30

    invoke-static {v0, v1, v4}, Lcom/cyanogenmod/trebuchet/LauncherSettings$Favorites;->getContentUri(JZ)Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v7, 0x0

    invoke-virtual {v3, v4, v5, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_3
    move/from16 v0, v32

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v30

    sget-object v4, Lcom/cyanogenmod/trebuchet/LauncherModel;->sFolders:Ljava/util/HashMap;

    move-wide/from16 v0, v30

    # invokes: Lcom/cyanogenmod/trebuchet/LauncherModel;->findOrMakeFolder(Ljava/util/HashMap;J)Lcom/cyanogenmod/trebuchet/FolderInfo;
    invoke-static {v4, v0, v1}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$11(Ljava/util/HashMap;J)Lcom/cyanogenmod/trebuchet/FolderInfo;

    move-result-object v29

    invoke-interface {v8, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v29

    iput-object v4, v0, Lcom/cyanogenmod/trebuchet/FolderInfo;->title:Ljava/lang/CharSequence;

    move-wide/from16 v0, v30

    move-object/from16 v2, v29

    iput-wide v0, v2, Lcom/cyanogenmod/trebuchet/FolderInfo;->id:J

    move/from16 v0, v26

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v25

    move/from16 v0, v25

    int-to-long v4, v0

    move-object/from16 v0, v29

    iput-wide v4, v0, Lcom/cyanogenmod/trebuchet/FolderInfo;->container:J

    move/from16 v0, v47

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object/from16 v0, v29

    iput v4, v0, Lcom/cyanogenmod/trebuchet/FolderInfo;->screen:I

    move/from16 v0, v22

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object/from16 v0, v29

    iput v4, v0, Lcom/cyanogenmod/trebuchet/FolderInfo;->cellX:I

    move/from16 v0, v23

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object/from16 v0, v29

    iput v4, v0, Lcom/cyanogenmod/trebuchet/FolderInfo;->cellY:I

    move/from16 v0, v41

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-nez v4, :cond_7

    const/4 v4, 0x0

    :goto_6
    move-object/from16 v0, v29

    iput-boolean v4, v0, Lcom/cyanogenmod/trebuchet/FolderInfo;->isLock:Z

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    move-object/from16 v2, v29

    invoke-direct {v0, v1, v2}, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->checkItemPlacement([[[Lcom/cyanogenmod/trebuchet/ItemInfo;Lcom/cyanogenmod/trebuchet/ItemInfo;)Z

    move-result v4

    if-eqz v4, :cond_0

    packed-switch v25, :pswitch_data_2

    :goto_7
    sget-object v4, Lcom/cyanogenmod/trebuchet/LauncherModel;->sItemsIdMap:Ljava/util/HashMap;

    move-object/from16 v0, v29

    iget-wide v11, v0, Lcom/cyanogenmod/trebuchet/FolderInfo;->id:J

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v29

    invoke-virtual {v4, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v4, Lcom/cyanogenmod/trebuchet/LauncherModel;->sFolders:Ljava/util/HashMap;

    move-object/from16 v0, v29

    iget-wide v11, v0, Lcom/cyanogenmod/trebuchet/FolderInfo;->id:J

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v29

    invoke-virtual {v4, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_7
    const/4 v4, 0x1

    goto :goto_6

    :pswitch_4
    sget-object v4, Lcom/cyanogenmod/trebuchet/LauncherModel;->sWorkspaceItems:Ljava/util/ArrayList;

    move-object/from16 v0, v29

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    :pswitch_5
    move/from16 v0, v20

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    move/from16 v0, v32

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v30

    move-object/from16 v0, v53

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/appwidget/AppWidgetManager;->getAppWidgetInfo(I)Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v45

    if-nez v36, :cond_9

    if-eqz v45, :cond_8

    move-object/from16 v0, v45

    iget-object v4, v0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    if-eqz v4, :cond_8

    move-object/from16 v0, v45

    iget-object v4, v0, Landroid/appwidget/AppWidgetProviderInfo;->provider:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_9

    :cond_8
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Deleting widget that isn\'t installed anymore: id="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, v30

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " appWidgetId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v42

    const-string v4, "Launcher.Model"

    move-object/from16 v0, v42

    invoke-static {v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v4, Lcom/cyanogenmod/trebuchet/Launcher;->sDumpLogs:Ljava/util/ArrayList;

    move-object/from16 v0, v42

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v39

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_9
    new-instance v21, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;

    move-object/from16 v0, v21

    move/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;-><init>(I)V

    move-wide/from16 v0, v30

    move-object/from16 v2, v21

    iput-wide v0, v2, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->id:J

    move/from16 v0, v47

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object/from16 v0, v21

    iput v4, v0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->screen:I

    move/from16 v0, v22

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object/from16 v0, v21

    iput v4, v0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->cellX:I

    move/from16 v0, v23

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object/from16 v0, v21

    iput v4, v0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->cellY:I

    move/from16 v0, v48

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object/from16 v0, v21

    iput v4, v0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->spanX:I

    move/from16 v0, v49

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object/from16 v0, v21

    iput v4, v0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->spanY:I

    move/from16 v0, v41

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-nez v4, :cond_a

    const/4 v4, 0x0

    :goto_8
    move-object/from16 v0, v21

    iput-boolean v4, v0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->isLock:Z

    move/from16 v0, v26

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v25

    const/16 v4, -0x64

    move/from16 v0, v25

    if-eq v0, v4, :cond_b

    const/16 v4, -0x65

    move/from16 v0, v25

    if-eq v0, v4, :cond_b

    const-string v4, "Launcher.Model"

    const-string v5, "Widget found where container != CONTAINER_DESKTOP nor CONTAINER_HOTSEAT - ignoring!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_a
    const/4 v4, 0x1

    goto :goto_8

    :cond_b
    move/from16 v0, v26

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    int-to-long v4, v4

    move-object/from16 v0, v21

    iput-wide v4, v0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->container:J

    move-object/from16 v0, p0

    move-object/from16 v1, v44

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->checkItemPlacement([[[Lcom/cyanogenmod/trebuchet/ItemInfo;Lcom/cyanogenmod/trebuchet/ItemInfo;)Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, Lcom/cyanogenmod/trebuchet/LauncherModel;->sItemsIdMap:Ljava/util/HashMap;

    move-object/from16 v0, v21

    iget-wide v11, v0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->id:J

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v21

    invoke-virtual {v4, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v4, Lcom/cyanogenmod/trebuchet/LauncherModel;->sAppWidgets:Ljava/util/ArrayList;

    move-object/from16 v0, v21

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    :cond_c
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v30

    const-string v4, "Launcher.Model"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v11, "Removed id = "

    invoke-direct {v7, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, v30

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v4, 0x0

    :try_start_6
    move-wide/from16 v0, v30

    invoke-static {v0, v1, v4}, Lcom/cyanogenmod/trebuchet/LauncherSettings$Favorites;->getContentUri(JZ)Landroid/net/Uri;

    move-result-object v4

    const/4 v7, 0x0

    const/4 v11, 0x0

    move-object/from16 v0, v24

    invoke-virtual {v0, v4, v7, v11}, Landroid/content/ContentProviderClient;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_2

    goto/16 :goto_1

    :catch_2
    move-exception v28

    const-string v4, "Launcher.Model"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v11, "Could not remove id = "

    invoke-direct {v7, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, v30

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_d
    const-string v40, ""

    const/16 v46, 0x0

    :goto_9
    const/4 v4, 0x7

    move/from16 v0, v46

    if-lt v0, v4, :cond_e

    const-string v4, "Launcher.Model"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "[ "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v40

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " ]"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    add-int/lit8 v55, v55, 0x1

    goto/16 :goto_2

    :cond_e
    if-lez v46, :cond_f

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static/range {v40 .. v40}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, " | "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    :cond_f
    const/16 v54, 0x0

    :goto_a
    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mCellCountX:I
    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$8()I

    move-result v4

    move/from16 v0, v54

    if-lt v0, v4, :cond_10

    add-int/lit8 v46, v46, 0x1

    goto :goto_9

    :cond_10
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static/range {v40 .. v40}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v4, v44, v46

    aget-object v4, v4, v54

    aget-object v4, v4, v55

    if-eqz v4, :cond_11

    const-string v4, "#"

    :goto_b
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v40

    add-int/lit8 v54, v54, 0x1

    goto :goto_a

    :cond_11
    const-string v4, "."

    goto :goto_b

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x65
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch -0x65
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method private onlyBindAllApps()V
    .locals 4

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mCallbacks:Ljava/lang/ref/WeakReference;
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$4(Lcom/cyanogenmod/trebuchet/LauncherModel;)Ljava/lang/ref/WeakReference;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;

    if-nez v1, :cond_0

    const-string v2, "Launcher.Model"

    const-string v3, "LoaderTask running with no launcher (onlyBindAllApps)"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mAllAppsList:Lcom/cyanogenmod/trebuchet/AllAppsList;
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$15(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/AllAppsList;

    move-result-object v2

    iget-object v2, v2, Lcom/cyanogenmod/trebuchet/AllAppsList;->data:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mHandler:Lcom/cyanogenmod/trebuchet/DeferredHandler;
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$3(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/DeferredHandler;

    move-result-object v2

    new-instance v3, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask$10;

    invoke-direct {v3, p0, v1, v0}, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask$10;-><init>(Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;Ljava/util/ArrayList;)V

    invoke-virtual {v2, v3}, Lcom/cyanogenmod/trebuchet/DeferredHandler;->post(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private waitForIdle()V
    .locals 6

    monitor-enter p0

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mHandler:Lcom/cyanogenmod/trebuchet/DeferredHandler;
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$3(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/DeferredHandler;

    move-result-object v2

    new-instance v3, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask$1;

    invoke-direct {v3, p0}, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask$1;-><init>(Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;)V

    invoke-virtual {v2, v3}, Lcom/cyanogenmod/trebuchet/DeferredHandler;->postIdle(Ljava/lang/Runnable;)V

    :goto_0
    iget-boolean v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->mStopped:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->mLoadAndBindStepFinished:Z

    if-eqz v2, :cond_1

    :cond_0
    const-string v2, "Launcher.Model"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "waited "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v0

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ms for previous step to finish binding"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :cond_1
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_0

    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2
.end method


# virtual methods
.method public dumpState()V
    .locals 3

    const-string v0, "Launcher.Model"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mLoaderTask.mContext="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "Launcher.Model"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mLoaderTask.mWaitThread="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->mWaitThread:Ljava/lang/Thread;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "Launcher.Model"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mLoaderTask.mIsLaunching="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->mIsLaunching:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "Launcher.Model"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mLoaderTask.mStopped="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->mStopped:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "Launcher.Model"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mLoaderTask.mLoadAndBindStepFinished="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->mLoadAndBindStepFinished:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "Launcher.Model"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mItems size="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/cyanogenmod/trebuchet/LauncherModel;->sWorkspaceItems:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method isLaunching()Z
    .locals 1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->mIsLaunching:Z

    return v0
.end method

.method public run()V
    .locals 9

    const/4 v8, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mCallbacks:Ljava/lang/ref/WeakReference;
    invoke-static {v4}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$4(Lcom/cyanogenmod/trebuchet/LauncherModel;)Ljava/lang/ref/WeakReference;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;->isAllAppsVisible()Z

    move-result v4

    if-eqz v4, :cond_0

    move v2, v3

    :cond_0
    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mLock:Ljava/lang/Object;
    invoke-static {v4}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$5(Lcom/cyanogenmod/trebuchet/LauncherModel;)Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    :try_start_0
    const-string v6, "Launcher.Model"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v4, "Setting thread priority to "

    invoke-direct {v7, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->mIsLaunching:Z

    if-eqz v4, :cond_3

    const-string v4, "FOREGROUND"

    :goto_0
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v4, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->mIsLaunching:Z

    if-eqz v4, :cond_1

    const/4 v3, -0x2

    :cond_1
    invoke-static {v3}, Landroid/os/Process;->setThreadPriority(I)V

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v2, :cond_4

    const-string v3, "Launcher.Model"

    const-string v4, "step 1: loading workspace"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->loadAndBindWorkspace()V

    :goto_1
    iget-boolean v3, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->mStopped:Z

    if-eqz v3, :cond_5

    :goto_2
    const-string v3, "Launcher.Model"

    const-string v4, "Comparing loaded icons to database icons"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v3, Lcom/cyanogenmod/trebuchet/LauncherModel;->sDbIconCache:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_8

    sget-object v3, Lcom/cyanogenmod/trebuchet/LauncherModel;->sDbIconCache:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    iput-object v8, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mLock:Ljava/lang/Object;
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$5(Lcom/cyanogenmod/trebuchet/LauncherModel;)Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    :try_start_1
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mLoaderTask:Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$6(Lcom/cyanogenmod/trebuchet/LauncherModel;)Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;

    move-result-object v3

    if-ne v3, p0, :cond_2

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    const/4 v5, 0x0

    invoke-static {v3, v5}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$7(Lcom/cyanogenmod/trebuchet/LauncherModel;Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;)V

    :cond_2
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    return-void

    :cond_3
    :try_start_2
    const-string v4, "DEFAULT"

    goto :goto_0

    :catchall_0
    move-exception v3

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v3

    :cond_4
    const-string v3, "Launcher.Model"

    const-string v4, "step 1: special: loading all apps"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->loadAndBindAllApps()V

    goto :goto_1

    :cond_5
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mLock:Ljava/lang/Object;
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$5(Lcom/cyanogenmod/trebuchet/LauncherModel;)Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    :try_start_3
    iget-boolean v3, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->mIsLaunching:Z

    if-eqz v3, :cond_6

    const-string v3, "Launcher.Model"

    const-string v5, "Setting thread priority to BACKGROUND"

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v3, 0xa

    invoke-static {v3}, Landroid/os/Process;->setThreadPriority(I)V

    :cond_6
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->waitForIdle()V

    if-eqz v2, :cond_7

    const-string v3, "Launcher.Model"

    const-string v4, "step 2: loading all apps"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->loadAndBindAllApps()V

    :goto_4
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mLock:Ljava/lang/Object;
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$5(Lcom/cyanogenmod/trebuchet/LauncherModel;)Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    const/4 v3, 0x0

    :try_start_4
    invoke-static {v3}, Landroid/os/Process;->setThreadPriority(I)V

    monitor-exit v4

    goto :goto_2

    :catchall_1
    move-exception v3

    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v3

    :catchall_2
    move-exception v3

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v3

    :cond_7
    const-string v3, "Launcher.Model"

    const-string v4, "step 2: special: loading workspace"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->loadAndBindWorkspace()V

    goto :goto_4

    :cond_8
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->mContext:Landroid/content/Context;

    move-object v3, v1

    check-cast v3, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    sget-object v4, Lcom/cyanogenmod/trebuchet/LauncherModel;->sDbIconCache:Ljava/util/HashMap;

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [B

    invoke-virtual {v6, v7, v3, v4}, Lcom/cyanogenmod/trebuchet/LauncherModel;->updateSavedIcon(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/ShortcutInfo;[B)V

    goto/16 :goto_3

    :catchall_3
    move-exception v3

    :try_start_6
    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    throw v3
.end method

.method public stopLocked()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->mStopped:Z

    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method tryGetCallbacks(Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;)Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;
    .locals 5
    .param p1    # Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$5(Lcom/cyanogenmod/trebuchet/LauncherModel;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    :try_start_0
    iget-boolean v3, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->mStopped:Z

    if-eqz v3, :cond_0

    monitor-exit v2

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mCallbacks:Ljava/lang/ref/WeakReference;
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$4(Lcom/cyanogenmod/trebuchet/LauncherModel;)Ljava/lang/ref/WeakReference;

    move-result-object v3

    if-nez v3, :cond_1

    monitor-exit v2

    move-object v0, v1

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$LoaderTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mCallbacks:Ljava/lang/ref/WeakReference;
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$4(Lcom/cyanogenmod/trebuchet/LauncherModel;)Ljava/lang/ref/WeakReference;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;

    if-eq v0, p1, :cond_2

    monitor-exit v2

    move-object v0, v1

    goto :goto_0

    :cond_2
    if-nez v0, :cond_3

    const-string v3, "Launcher.Model"

    const-string v4, "no mCallbacks"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v2

    move-object v0, v1

    goto :goto_0

    :cond_3
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
