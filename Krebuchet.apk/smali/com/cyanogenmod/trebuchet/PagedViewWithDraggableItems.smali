.class public abstract Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;
.super Lcom/cyanogenmod/trebuchet/PagedView;
.source "PagedViewWithDraggableItems.java"

# interfaces
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private mDragSlopeThreshold:F

.field private mIsDragEnabled:Z

.field private mIsDragging:Z

.field private mLastTouchedItem:Landroid/view/View;

.field private mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Lcom/cyanogenmod/trebuchet/PagedView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    check-cast p1, Lcom/cyanogenmod/trebuchet/Launcher;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    return-void
.end method

.method private handleTouchEvent(Landroid/view/MotionEvent;)V
    .locals 3
    .param p1    # Landroid/view/MotionEvent;

    const/4 v2, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v1, v0, 0xff

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->cancelDragging()V

    iput-boolean v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->mIsDragEnabled:Z

    goto :goto_0

    :pswitch_2
    iget v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->mTouchState:I

    if-eq v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->mIsDragging:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->mIsDragEnabled:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->determineDraggingStart(Landroid/view/MotionEvent;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method protected beginDragging(Landroid/view/View;)Z
    .locals 2
    .param p1    # Landroid/view/View;

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->mIsDragging:Z

    iput-boolean v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->mIsDragging:Z

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    :cond_0
    return v1
.end method

.method protected cancelDragging()V
    .locals 2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->mIsDragging:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->mLastTouchedItem:Landroid/view/View;

    iput-boolean v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->mIsDragEnabled:Z

    return-void
.end method

.method protected determineDraggingStart(Landroid/view/MotionEvent;)V
    .locals 13
    .param p1    # Landroid/view/MotionEvent;

    const/4 v9, 0x1

    const/4 v10, 0x0

    iget v11, p0, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->mActivePointerId:I

    invoke-virtual {p1, v11}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v4

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v6

    iget v11, p0, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->mLastMotionX:F

    sub-float v11, v4, v11

    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v11

    float-to-int v5, v11

    iget v11, p0, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->mLastMotionY:F

    sub-float v11, v6, v11

    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v11

    float-to-int v7, v11

    iget v3, p0, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->mTouchSlop:I

    if-le v7, v3, :cond_1

    move v8, v9

    :goto_0
    int-to-float v11, v7

    int-to-float v12, v5

    div-float/2addr v11, v12

    iget v12, p0, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->mDragSlopeThreshold:F

    cmpl-float v11, v11, v12

    if-lez v11, :cond_2

    move v1, v9

    :goto_1
    if-eqz v1, :cond_0

    if-eqz v8, :cond_0

    iget-object v9, p0, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->mLastTouchedItem:Landroid/view/View;

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->mLastTouchedItem:Landroid/view/View;

    invoke-virtual {p0, v9}, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->beginDragging(Landroid/view/View;)Z

    iget-boolean v9, p0, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->mAllowLongPress:Z

    if-eqz v9, :cond_0

    iput-boolean v10, p0, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->mAllowLongPress:Z

    iget v9, p0, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->mCurrentPage:I

    invoke-virtual {p0, v9}, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->cancelLongPress()V

    :cond_0
    return-void

    :cond_1
    move v8, v10

    goto :goto_0

    :cond_2
    move v1, v10

    goto :goto_1
.end method

.method protected determineScrollingStart(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->mIsDragging:Z

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedView;->determineScrollingStart(Landroid/view/MotionEvent;)V

    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->cancelDragging()V

    invoke-super {p0}, Lcom/cyanogenmod/trebuchet/PagedView;->onDetachedFromWindow()V

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->handleTouchEvent(Landroid/view/MotionEvent;)V

    invoke-super {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 3
    .param p1    # Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/view/View;->isInTouchMode()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->mNextPage:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->isAllAppsCustomizeOpen()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->getWorkspace()Lcom/cyanogenmod/trebuchet/Workspace;

    move-result-object v1

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Workspace;->isSwitchingState()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->beginDragging(Landroid/view/View;)Z

    move-result v0

    goto :goto_0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->mLastTouchedItem:Landroid/view/View;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->mIsDragEnabled:Z

    const/4 v0, 0x0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->handleTouchEvent(Landroid/view/MotionEvent;)V

    invoke-super {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public setDragSlopeThreshold(F)V
    .locals 0
    .param p1    # F

    iput p1, p0, Lcom/cyanogenmod/trebuchet/PagedViewWithDraggableItems;->mDragSlopeThreshold:F

    return-void
.end method
