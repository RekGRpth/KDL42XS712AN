.class Lcom/cyanogenmod/trebuchet/Workspace$6;
.super Lcom/cyanogenmod/trebuchet/LauncherAnimatorUpdateListener;
.source "Workspace.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cyanogenmod/trebuchet/Workspace;->changeState(Lcom/cyanogenmod/trebuchet/Workspace$State;ZI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/Workspace;


# direct methods
.method constructor <init>(Lcom/cyanogenmod/trebuchet/Workspace;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Workspace$6;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/LauncherAnimatorUpdateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(FF)V
    .locals 4
    .param p1    # F
    .param p2    # F

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace$6;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-static {v2, p2}, Lcom/cyanogenmod/trebuchet/Workspace;->access$12(Lcom/cyanogenmod/trebuchet/Workspace;F)V

    const/4 v2, 0x0

    cmpl-float v2, p2, v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace$6;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Workspace;->invalidate()V

    const/4 v1, 0x0

    :goto_1
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace$6;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildCount()I

    move-result v2

    if-lt v1, v2, :cond_1

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace$6;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    # invokes: Lcom/cyanogenmod/trebuchet/Workspace;->syncChildrenLayersEnabledOnVisiblePages()V
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/Workspace;->access$27(Lcom/cyanogenmod/trebuchet/Workspace;)V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace$6;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v2, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->invalidate()V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace$6;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    # getter for: Lcom/cyanogenmod/trebuchet/Workspace;->mOldTranslationXs:[F
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/Workspace;->access$13(Lcom/cyanogenmod/trebuchet/Workspace;)[F

    move-result-object v2

    aget v2, v2, v1

    mul-float/2addr v2, p1

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Workspace$6;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    # getter for: Lcom/cyanogenmod/trebuchet/Workspace;->mNewTranslationXs:[F
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/Workspace;->access$14(Lcom/cyanogenmod/trebuchet/Workspace;)[F

    move-result-object v3

    aget v3, v3, v1

    mul-float/2addr v3, p2

    add-float/2addr v2, v3

    invoke-virtual {v0, v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->setTranslationX(F)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace$6;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    # getter for: Lcom/cyanogenmod/trebuchet/Workspace;->mOldTranslationYs:[F
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/Workspace;->access$15(Lcom/cyanogenmod/trebuchet/Workspace;)[F

    move-result-object v2

    aget v2, v2, v1

    mul-float/2addr v2, p1

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Workspace$6;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    # getter for: Lcom/cyanogenmod/trebuchet/Workspace;->mNewTranslationYs:[F
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/Workspace;->access$16(Lcom/cyanogenmod/trebuchet/Workspace;)[F

    move-result-object v3

    aget v3, v3, v1

    mul-float/2addr v3, p2

    add-float/2addr v2, v3

    invoke-virtual {v0, v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->setTranslationY(F)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace$6;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    # getter for: Lcom/cyanogenmod/trebuchet/Workspace;->mOldScaleXs:[F
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/Workspace;->access$17(Lcom/cyanogenmod/trebuchet/Workspace;)[F

    move-result-object v2

    aget v2, v2, v1

    mul-float/2addr v2, p1

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Workspace$6;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    # getter for: Lcom/cyanogenmod/trebuchet/Workspace;->mNewScaleXs:[F
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/Workspace;->access$18(Lcom/cyanogenmod/trebuchet/Workspace;)[F

    move-result-object v3

    aget v3, v3, v1

    mul-float/2addr v3, p2

    add-float/2addr v2, v3

    invoke-virtual {v0, v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->setScaleX(F)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace$6;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    # getter for: Lcom/cyanogenmod/trebuchet/Workspace;->mOldScaleYs:[F
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/Workspace;->access$19(Lcom/cyanogenmod/trebuchet/Workspace;)[F

    move-result-object v2

    aget v2, v2, v1

    mul-float/2addr v2, p1

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Workspace$6;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    # getter for: Lcom/cyanogenmod/trebuchet/Workspace;->mNewScaleYs:[F
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/Workspace;->access$20(Lcom/cyanogenmod/trebuchet/Workspace;)[F

    move-result-object v3

    aget v3, v3, v1

    mul-float/2addr v3, p2

    add-float/2addr v2, v3

    invoke-virtual {v0, v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->setScaleY(F)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace$6;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    # getter for: Lcom/cyanogenmod/trebuchet/Workspace;->mOldBackgroundAlphas:[F
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/Workspace;->access$21(Lcom/cyanogenmod/trebuchet/Workspace;)[F

    move-result-object v2

    aget v2, v2, v1

    mul-float/2addr v2, p1

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Workspace$6;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    # getter for: Lcom/cyanogenmod/trebuchet/Workspace;->mNewBackgroundAlphas:[F
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/Workspace;->access$22(Lcom/cyanogenmod/trebuchet/Workspace;)[F

    move-result-object v3

    aget v3, v3, v1

    mul-float/2addr v3, p2

    add-float/2addr v2, v3

    invoke-virtual {v0, v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->setFastBackgroundAlpha(F)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace$6;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    # getter for: Lcom/cyanogenmod/trebuchet/Workspace;->mOldBackgroundAlphaMultipliers:[F
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/Workspace;->access$23(Lcom/cyanogenmod/trebuchet/Workspace;)[F

    move-result-object v2

    aget v2, v2, v1

    mul-float/2addr v2, p1

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Workspace$6;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    # getter for: Lcom/cyanogenmod/trebuchet/Workspace;->mNewBackgroundAlphaMultipliers:[F
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/Workspace;->access$24(Lcom/cyanogenmod/trebuchet/Workspace;)[F

    move-result-object v3

    aget v3, v3, v1

    mul-float/2addr v3, p2

    add-float/2addr v2, v3

    invoke-virtual {v0, v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->setBackgroundAlphaMultiplier(F)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Workspace$6;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    # getter for: Lcom/cyanogenmod/trebuchet/Workspace;->mOldAlphas:[F
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/Workspace;->access$25(Lcom/cyanogenmod/trebuchet/Workspace;)[F

    move-result-object v2

    aget v2, v2, v1

    mul-float/2addr v2, p1

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Workspace$6;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    # getter for: Lcom/cyanogenmod/trebuchet/Workspace;->mNewAlphas:[F
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/Workspace;->access$26(Lcom/cyanogenmod/trebuchet/Workspace;)[F

    move-result-object v3

    aget v3, v3, v1

    mul-float/2addr v3, p2

    add-float/2addr v2, v3

    invoke-virtual {v0, v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->setAlpha(F)V

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->invalidate()V

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1
.end method
