.class public Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;
.super Landroid/view/ViewGroup;
.source "PagedViewCellLayoutChildren.java"


# static fields
.field static final TAG:Ljava/lang/String; = "PagedViewCellLayout"


# instance fields
.field private mCellHeight:I

.field private mCellWidth:I

.field private mCenterContent:Z

.field private mHeightGap:I

.field private mWidthGap:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public cancelLongPress()V
    .locals 3

    invoke-super {p0}, Landroid/view/ViewGroup;->cancelLongPress()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->getChildCount()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->cancelLongPress()V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method createHardwareLayer()V
    .locals 2

    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->setLayerType(ILandroid/graphics/Paint;)V

    return-void
.end method

.method destroyHardwareLayer()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->setLayerType(ILandroid/graphics/Paint;)V

    return-void
.end method

.method public enableCenteredContent(Z)V
    .locals 0
    .param p1    # Z

    iput-boolean p1, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->mCenterContent:Z

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 12
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->getChildCount()I

    move-result v3

    const/4 v9, 0x0

    iget-boolean v10, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->mCenterContent:Z

    if-eqz v10, :cond_0

    if-lez v3, :cond_0

    const/4 v7, 0x0

    const v8, 0x7fffffff

    const/4 v4, 0x0

    :goto_0
    if-lt v4, v3, :cond_1

    sub-int v6, v7, v8

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->getMeasuredWidth()I

    move-result v10

    sub-int/2addr v10, v6

    div-int/lit8 v9, v10, 0x2

    :cond_0
    const/4 v4, 0x0

    :goto_1
    if-lt v4, v3, :cond_3

    return-void

    :cond_1
    invoke-virtual {p0, v4}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v10

    const/16 v11, 0x8

    if-eq v10, v11, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout$LayoutParams;

    iget v10, v5, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout$LayoutParams;->x:I

    invoke-static {v8, v10}, Ljava/lang/Math;->min(II)I

    move-result v8

    iget v10, v5, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout$LayoutParams;->x:I

    iget v11, v5, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout$LayoutParams;->width:I

    add-int/2addr v10, v11

    invoke-static {v7, v10}, Ljava/lang/Math;->max(II)I

    move-result v7

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v4}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v10

    const/16 v11, 0x8

    if-eq v10, v11, :cond_4

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout$LayoutParams;

    iget v10, v5, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout$LayoutParams;->x:I

    add-int v1, v9, v10

    iget v2, v5, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout$LayoutParams;->y:I

    iget v10, v5, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout$LayoutParams;->width:I

    add-int/2addr v10, v1

    iget v11, v5, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout$LayoutParams;->height:I

    add-int/2addr v11, v2

    invoke-virtual {v0, v1, v2, v10, v11}, Landroid/view/View;->layout(IIII)V

    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 18
    .param p1    # I
    .param p2    # I

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v16

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v17

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v13

    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v14

    if-eqz v16, :cond_0

    if-nez v13, :cond_1

    :cond_0
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "CellLayout cannot have UNSPECIFIED dimensions"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->getChildCount()I

    move-result v12

    const/4 v15, 0x0

    :goto_0
    if-lt v15, v12, :cond_2

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1, v14}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->setMeasuredDimension(II)V

    return-void

    :cond_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout$LayoutParams;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->mCellWidth:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->mCellHeight:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->mWidthGap:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->mHeightGap:I

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->getPaddingLeft()I

    move-result v7

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->getPaddingTop()I

    move-result v8

    invoke-virtual/range {v2 .. v8}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout$LayoutParams;->setup(IIIIII)V

    iget v3, v2, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout$LayoutParams;->width:I

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    iget v3, v2, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout$LayoutParams;->height:I

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    invoke-virtual {v9, v10, v11}, Landroid/view/View;->measure(II)V

    add-int/lit8 v15, v15, 0x1

    goto :goto_0
.end method

.method public requestChildFocus(Landroid/view/View;Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/View;

    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    if-eqz p1, :cond_0

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p1, v0}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->requestRectangleOnScreen(Landroid/graphics/Rect;)Z

    :cond_0
    return-void
.end method

.method public setCellDimensions(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    iput p1, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->mCellWidth:I

    iput p2, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->mCellHeight:I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->requestLayout()V

    return-void
.end method

.method protected setChildrenDrawingCacheEnabled(Z)V
    .locals 4
    .param p1    # Z

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->getChildCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    invoke-virtual {v2}, Landroid/view/View;->isHardwareAccelerated()Z

    move-result v3

    if-nez v3, :cond_1

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/View;->buildDrawingCache(Z)V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setGap(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    iput p1, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->mWidthGap:I

    iput p2, p0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->mHeightGap:I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;->requestLayout()V

    return-void
.end method
