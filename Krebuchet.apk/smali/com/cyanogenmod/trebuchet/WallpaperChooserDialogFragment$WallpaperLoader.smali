.class Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;
.super Landroid/os/AsyncTask;
.source "WallpaperChooserDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "WallpaperLoader"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field mOptions:Landroid/graphics/BitmapFactory$Options;

.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;


# direct methods
.method constructor <init>(Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;)V
    .locals 2

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;->this$0:Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;->mOptions:Landroid/graphics/BitmapFactory$Options;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;->mOptions:Landroid/graphics/BitmapFactory$Options;

    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inDither:Z

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;->mOptions:Landroid/graphics/BitmapFactory$Options;

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    return-void
.end method


# virtual methods
.method cancel()V
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;->mOptions:Landroid/graphics/BitmapFactory$Options;

    invoke-virtual {v0}, Landroid/graphics/BitmapFactory$Options;->requestCancelDecode()V

    const/4 v0, 0x1

    invoke-super {p0, v0}, Landroid/os/AsyncTask;->cancel(Z)Z

    return-void
.end method

.method protected varargs doInBackground([Ljava/lang/Integer;)Landroid/graphics/Bitmap;
    .locals 5
    .param p1    # [Ljava/lang/Integer;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v1, v2

    :goto_0
    return-object v1

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;->this$0:Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;->this$0:Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;

    # getter for: Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->mImages:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->access$1(Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;)Ljava/util/ArrayList;

    move-result-object v1

    const/4 v4, 0x0

    aget-object v4, p1, v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;->mOptions:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v3, v1, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v1, v2

    goto :goto_0
.end method

.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;->doInBackground([Ljava/lang/Integer;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1    # Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;->mOptions:Landroid/graphics/BitmapFactory$Options;

    iget-boolean v1, v1, Landroid/graphics/BitmapFactory$Options;->mCancel:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;->this$0:Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;

    # getter for: Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->access$2(Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;->this$0:Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;

    # getter for: Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->access$2(Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    :cond_1
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;->this$0:Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;->this$0:Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;

    invoke-static {v1, p1}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->access$3(Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;->this$0:Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;

    # getter for: Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->mWallpaperDrawable:Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperDrawable;
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->access$4(Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;)Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperDrawable;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperDrawable;->setBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {v0}, Landroid/view/View;->postInvalidate()V

    :goto_1
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;->this$0:Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;

    invoke-static {v1, v2}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->access$5(Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;->this$0:Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;

    invoke-static {v1, v2}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->access$3(Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;->this$0:Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;

    # getter for: Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->mWallpaperDrawable:Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperDrawable;
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->access$4(Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;)Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperDrawable;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperDrawable;->setBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment$WallpaperLoader;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method
