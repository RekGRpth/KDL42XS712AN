.class public Lcom/cyanogenmod/trebuchet/ConnectivityService;
.super Landroid/app/Service;
.source "ConnectivityService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cyanogenmod/trebuchet/ConnectivityService$ConnectivityBinder;,
        Lcom/cyanogenmod/trebuchet/ConnectivityService$IConnectivityMonitor;
    }
.end annotation


# static fields
.field public static final KEY_ACTIVE_NETWORK_ID:Ljava/lang/String; = "connectivityService.activeNetwork"

.field private static final WILL_MONITOR:Z = true


# instance fields
.field private final mBinder:Landroid/os/IBinder;

.field private mNetworkListener:Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;

.field private mNetworkMonitor:Lcom/cyanogenmod/trebuchet/NetworkMonitor;

.field private mUsbListener:Lcom/cyanogenmod/trebuchet/UsbMonitor$IUsbUpdateListener;

.field private mUsbMonitor:Lcom/cyanogenmod/trebuchet/UsbMonitor;

.field private mUserCenterListener:Lcom/cyanogenmod/trebuchet/UserCenterMonitor$IUserCenterUpdateListener;

.field private mUserCenterMonitor:Lcom/cyanogenmod/trebuchet/UserCenterMonitor;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Lcom/cyanogenmod/trebuchet/ConnectivityService$ConnectivityBinder;

    invoke-direct {v0, p0}, Lcom/cyanogenmod/trebuchet/ConnectivityService$ConnectivityBinder;-><init>(Lcom/cyanogenmod/trebuchet/ConnectivityService;)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/ConnectivityService;->mBinder:Landroid/os/IBinder;

    return-void
.end method

.method private stopMonitors()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/ConnectivityService;->mUserCenterMonitor:Lcom/cyanogenmod/trebuchet/UserCenterMonitor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/ConnectivityService;->mUserCenterMonitor:Lcom/cyanogenmod/trebuchet/UserCenterMonitor;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/UserCenterMonitor;->stopMonitor()V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/ConnectivityService;->mUserCenterMonitor:Lcom/cyanogenmod/trebuchet/UserCenterMonitor;

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/ConnectivityService;->mNetworkMonitor:Lcom/cyanogenmod/trebuchet/NetworkMonitor;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/ConnectivityService;->mNetworkMonitor:Lcom/cyanogenmod/trebuchet/NetworkMonitor;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->stopMonitor()V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/ConnectivityService;->mNetworkMonitor:Lcom/cyanogenmod/trebuchet/NetworkMonitor;

    :cond_1
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/ConnectivityService;->mUsbMonitor:Lcom/cyanogenmod/trebuchet/UsbMonitor;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/ConnectivityService;->mUsbMonitor:Lcom/cyanogenmod/trebuchet/UsbMonitor;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/UsbMonitor;->stopMonitor()V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/ConnectivityService;->mUsbMonitor:Lcom/cyanogenmod/trebuchet/UsbMonitor;

    :cond_2
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/ConnectivityService;->mBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 0

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    return-void
.end method

.method public onDestroy()V
    .locals 0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/ConnectivityService;->stopMonitors()V

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 1
    .param p1    # Landroid/content/Intent;

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/ConnectivityService;->stopMonitors()V

    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method

.method public registerNetworkListener(Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;)Lcom/cyanogenmod/trebuchet/ConnectivityService;
    .locals 0
    .param p1    # Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/ConnectivityService;->mNetworkListener:Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;

    return-object p0
.end method

.method public registerUsbListener(Lcom/cyanogenmod/trebuchet/UsbMonitor$IUsbUpdateListener;)Lcom/cyanogenmod/trebuchet/ConnectivityService;
    .locals 0
    .param p1    # Lcom/cyanogenmod/trebuchet/UsbMonitor$IUsbUpdateListener;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/ConnectivityService;->mUsbListener:Lcom/cyanogenmod/trebuchet/UsbMonitor$IUsbUpdateListener;

    return-object p0
.end method

.method public registerUserCenterListener(Lcom/cyanogenmod/trebuchet/UserCenterMonitor$IUserCenterUpdateListener;)Lcom/cyanogenmod/trebuchet/ConnectivityService;
    .locals 0
    .param p1    # Lcom/cyanogenmod/trebuchet/UserCenterMonitor$IUserCenterUpdateListener;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/ConnectivityService;->mUserCenterListener:Lcom/cyanogenmod/trebuchet/UserCenterMonitor$IUserCenterUpdateListener;

    return-object p0
.end method

.method public startMonitors()V
    .locals 2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/ConnectivityService;->mNetworkListener:Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/ConnectivityService;->mNetworkMonitor:Lcom/cyanogenmod/trebuchet/NetworkMonitor;

    if-nez v0, :cond_0

    new-instance v0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/ConnectivityService;->mNetworkListener:Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;

    invoke-direct {v0, p0, v1}, Lcom/cyanogenmod/trebuchet/NetworkMonitor;-><init>(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/ConnectivityService;->mNetworkMonitor:Lcom/cyanogenmod/trebuchet/NetworkMonitor;

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/ConnectivityService;->mNetworkMonitor:Lcom/cyanogenmod/trebuchet/NetworkMonitor;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->startMonitor()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/ConnectivityService;->mUsbListener:Lcom/cyanogenmod/trebuchet/UsbMonitor$IUsbUpdateListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/ConnectivityService;->mUsbMonitor:Lcom/cyanogenmod/trebuchet/UsbMonitor;

    if-nez v0, :cond_1

    new-instance v0, Lcom/cyanogenmod/trebuchet/UsbMonitor;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/ConnectivityService;->mUsbListener:Lcom/cyanogenmod/trebuchet/UsbMonitor$IUsbUpdateListener;

    invoke-direct {v0, p0, v1}, Lcom/cyanogenmod/trebuchet/UsbMonitor;-><init>(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/UsbMonitor$IUsbUpdateListener;)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/ConnectivityService;->mUsbMonitor:Lcom/cyanogenmod/trebuchet/UsbMonitor;

    :cond_1
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/ConnectivityService;->mUsbMonitor:Lcom/cyanogenmod/trebuchet/UsbMonitor;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/UsbMonitor;->startMonitor()V

    return-void
.end method
