.class public Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;
.super Landroid/widget/ImageView;
.source "LinearScrollIndicator.java"

# interfaces
.implements Lcom/cyanogenmod/trebuchet/ScrollIndicator;


# instance fields
.field private mAnimator:Landroid/animation/ValueAnimator;

.field private mPaddingLeft:I

.field private mPaddingRight:I

.field private mPagedview:Lcom/cyanogenmod/trebuchet/PagedView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-object v1, Lcom/konka/avenger/R$styleable;->PagedView:[I

    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->mPaddingLeft:I

    const/16 v1, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->mPaddingRight:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private updateScrollingIndicatorPosition()V
    .locals 14

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->mPagedview:Lcom/cyanogenmod/trebuchet/PagedView;

    invoke-virtual {v10}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v6

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->mPagedview:Lcom/cyanogenmod/trebuchet/PagedView;

    invoke-virtual {v10}, Lcom/cyanogenmod/trebuchet/PagedView;->getMeasuredWidth()I

    move-result v8

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->mPagedview:Lcom/cyanogenmod/trebuchet/PagedView;

    invoke-virtual {v11}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    invoke-static {v10, v11}, Ljava/lang/Math;->max(II)I

    move-result v4

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->mPagedview:Lcom/cyanogenmod/trebuchet/PagedView;

    invoke-virtual {v10, v4}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildOffset(I)I

    move-result v10

    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->mPagedview:Lcom/cyanogenmod/trebuchet/PagedView;

    invoke-virtual {v11, v4}, Lcom/cyanogenmod/trebuchet/PagedView;->getRelativeChildOffset(I)I

    move-result v11

    sub-int v5, v10, v11

    iget v10, p0, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->mPaddingLeft:I

    sub-int v10, v8, v10

    iget v11, p0, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->mPaddingRight:I

    sub-int v9, v10, v11

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->getMeasuredWidth()I

    move-result v10

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->getPaddingLeft()I

    move-result v11

    sub-int/2addr v10, v11

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->getPaddingRight()I

    move-result v11

    sub-int v3, v10, v11

    const/4 v10, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    iget-object v12, p0, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->mPagedview:Lcom/cyanogenmod/trebuchet/PagedView;

    invoke-virtual {v12}, Lcom/cyanogenmod/trebuchet/PagedView;->getScrollX()I

    move-result v12

    int-to-float v12, v12

    int-to-float v13, v5

    div-float/2addr v12, v13

    invoke-static {v11, v12}, Ljava/lang/Math;->min(FF)F

    move-result v11

    invoke-static {v10, v11}, Ljava/lang/Math;->max(FF)F

    move-result v7

    div-int v2, v9, v6

    sub-int v10, v9, v2

    int-to-float v10, v10

    mul-float/2addr v10, v7

    float-to-int v10, v10

    iget v11, p0, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->mPaddingLeft:I

    add-int v1, v10, v11

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->isElasticScrollIndicator()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->getMeasuredWidth()I

    move-result v10

    if-eq v10, v2, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    iput v2, v10, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->requestLayout()V

    :cond_0
    :goto_0
    int-to-float v10, v1

    invoke-virtual {p0, v10}, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->setTranslationX(F)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->invalidate()V

    return-void

    :cond_1
    div-int/lit8 v10, v2, 0x2

    div-int/lit8 v11, v3, 0x2

    sub-int v0, v10, v11

    add-int/2addr v1, v0

    goto :goto_0
.end method


# virtual methods
.method public cancelAnimations()V
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->mAnimator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    :cond_0
    return-void
.end method

.method public hide(ZI)V
    .locals 4
    .param p1    # Z
    .param p2    # I

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->updateScrollingIndicatorPosition()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->cancelAnimations()V

    if-eqz p1, :cond_0

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->setVisibility(I)V

    invoke-virtual {p0, v3}, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->setAlpha(F)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "alpha"

    const/4 v1, 0x1

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput v3, v1, v2

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->mAnimator:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->mAnimator:Landroid/animation/ValueAnimator;

    int-to-long v1, p2

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->mAnimator:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator$1;

    invoke-direct {v1, p0}, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator$1;-><init>(Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0
.end method

.method public init(Lcom/cyanogenmod/trebuchet/PagedView;)V
    .locals 0
    .param p1    # Lcom/cyanogenmod/trebuchet/PagedView;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->mPagedview:Lcom/cyanogenmod/trebuchet/PagedView;

    return-void
.end method

.method public isElasticScrollIndicator()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public show(ZI)V
    .locals 4
    .param p1    # Z
    .param p2    # I

    const/4 v3, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->updateScrollingIndicatorPosition()V

    invoke-virtual {p0, v3}, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->cancelAnimations()V

    if-eqz p1, :cond_0

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->setAlpha(F)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "alpha"

    const/4 v1, 0x1

    new-array v1, v1, [F

    aput v2, v1, v3

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->mAnimator:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->mAnimator:Landroid/animation/ValueAnimator;

    int-to-long v1, p2

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0
.end method

.method public update()V
    .locals 0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/LinearScrollIndicator;->updateScrollingIndicatorPosition()V

    return-void
.end method
