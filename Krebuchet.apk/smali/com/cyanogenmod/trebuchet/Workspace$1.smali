.class Lcom/cyanogenmod/trebuchet/Workspace$1;
.super Landroid/animation/AnimatorListenerAdapter;
.source "Workspace.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cyanogenmod/trebuchet/Workspace;->initWorkspace()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/Workspace;


# direct methods
.method constructor <init>(Lcom/cyanogenmod/trebuchet/Workspace;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Workspace$1;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3
    .param p1    # Landroid/animation/Animator;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace$1;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-static {v0, v2}, Lcom/cyanogenmod/trebuchet/Workspace;->access$5(Lcom/cyanogenmod/trebuchet/Workspace;Z)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace$1;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    # getter for: Lcom/cyanogenmod/trebuchet/Workspace;->mScrollWallpaper:Z
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/Workspace;->access$6(Lcom/cyanogenmod/trebuchet/Workspace;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace$1;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mWallpaperOffset:Lcom/cyanogenmod/trebuchet/Workspace$WallpaperOffsetInterpolator;

    invoke-virtual {v0, v2}, Lcom/cyanogenmod/trebuchet/Workspace$WallpaperOffsetInterpolator;->setOverrideHorizontalCatchupConstant(Z)V

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace$1;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->access$7(Lcom/cyanogenmod/trebuchet/Workspace;Landroid/animation/AnimatorSet;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace$1;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    # invokes: Lcom/cyanogenmod/trebuchet/Workspace;->updateChildrenLayersEnabled(Z)V
    invoke-static {v0, v2}, Lcom/cyanogenmod/trebuchet/Workspace;->access$8(Lcom/cyanogenmod/trebuchet/Workspace;Z)V

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2
    .param p1    # Landroid/animation/Animator;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Workspace$1;->this$0:Lcom/cyanogenmod/trebuchet/Workspace;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->access$5(Lcom/cyanogenmod/trebuchet/Workspace;Z)V

    return-void
.end method
