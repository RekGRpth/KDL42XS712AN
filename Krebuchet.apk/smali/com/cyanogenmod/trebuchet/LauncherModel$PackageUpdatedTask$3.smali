.class Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask$3;
.super Ljava/lang/Object;
.source "LauncherModel.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;

.field private final synthetic val$callbacks:Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;

.field private final synthetic val$permanent:Z

.field private final synthetic val$removedFinal:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;Ljava/util/ArrayList;Z)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask$3;->this$1:Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;

    iput-object p2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask$3;->val$callbacks:Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;

    iput-object p3, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask$3;->val$removedFinal:Ljava/util/ArrayList;

    iput-boolean p4, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask$3;->val$permanent:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask$3;->this$1:Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;->access$0(Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;)Lcom/cyanogenmod/trebuchet/LauncherModel;

    move-result-object v1

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mCallbacks:Ljava/lang/ref/WeakReference;
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$4(Lcom/cyanogenmod/trebuchet/LauncherModel;)Ljava/lang/ref/WeakReference;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask$3;->this$1:Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;->this$0:Lcom/cyanogenmod/trebuchet/LauncherModel;
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;->access$0(Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask;)Lcom/cyanogenmod/trebuchet/LauncherModel;

    move-result-object v1

    # getter for: Lcom/cyanogenmod/trebuchet/LauncherModel;->mCallbacks:Ljava/lang/ref/WeakReference;
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/LauncherModel;->access$4(Lcom/cyanogenmod/trebuchet/LauncherModel;)Ljava/lang/ref/WeakReference;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;

    move-object v0, v1

    :goto_0
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask$3;->val$callbacks:Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;

    if-ne v1, v0, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask$3;->val$callbacks:Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask$3;->val$removedFinal:Ljava/util/ArrayList;

    iget-boolean v3, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$PackageUpdatedTask$3;->val$permanent:Z

    invoke-interface {v1, v2, v3}, Lcom/cyanogenmod/trebuchet/LauncherModel$Callbacks;->bindAppsRemoved(Ljava/util/ArrayList;Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
