.class Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$2;
.super Ljava/lang/Object;
.source "RocketLauncher.java"

# interfaces
.implements Landroid/animation/TimeAnimator$TimeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;->reset()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;


# direct methods
.method constructor <init>(Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$2;->this$1:Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTimeUpdate(Landroid/animation/TimeAnimator;JJ)V
    .locals 15
    .param p1    # Landroid/animation/TimeAnimator;
    .param p2    # J
    .param p4    # J

    const/16 v2, 0xbb8

    const-wide/16 v10, 0xbb8

    cmp-long v10, p2, v10

    if-gez v10, :cond_2

    move-wide/from16 v0, p2

    long-to-float v10, v0

    const v11, 0x453b8000    # 3000.0f

    div-float v9, v10, v11

    const/high16 v10, 0x3f800000    # 1.0f

    const/high16 v11, 0x3f800000    # 1.0f

    sub-float v11, v9, v11

    float-to-double v11, v11

    const-wide/high16 v13, 0x4010000000000000L    # 4.0

    invoke-static {v11, v12, v13, v14}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v11

    double-to-float v11, v11

    sub-float v5, v10, v11

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$2;->this$1:Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;

    invoke-virtual {v10, v5}, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;->setScaleX(F)V

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$2;->this$1:Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;

    invoke-virtual {v10, v5}, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;->setScaleY(F)V

    :goto_0
    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$2;->this$1:Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;

    # getter for: Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;->mManeuveringThrusters:Z
    invoke-static {v10}, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;->access$1(Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;)Z

    move-result v10

    if-eqz v10, :cond_3

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$2;->this$1:Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;

    # getter for: Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;->mSpeedScale:F
    invoke-static {v10}, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;->access$2(Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;)F

    move-result v10

    const v11, 0x3dcccccd    # 0.1f

    cmpl-float v10, v10, v11

    if-lez v10, :cond_0

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$2;->this$1:Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;

    # getter for: Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;->mSpeedScale:F
    invoke-static {v10}, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;->access$2(Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;)F

    move-result v11

    const-wide/16 v12, 0x2

    mul-long v12, v12, p4

    long-to-float v12, v12

    const/high16 v13, 0x447a0000    # 1000.0f

    div-float/2addr v12, v13

    sub-float/2addr v11, v12

    invoke-static {v10, v11}, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;->access$3(Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;F)V

    :cond_0
    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$2;->this$1:Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;

    # getter for: Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;->mSpeedScale:F
    invoke-static {v10}, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;->access$2(Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;)F

    move-result v10

    const v11, 0x3dcccccd    # 0.1f

    cmpg-float v10, v10, v11

    if-gez v10, :cond_1

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$2;->this$1:Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;

    const v11, 0x3dcccccd    # 0.1f

    invoke-static {v10, v11}, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;->access$3(Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;F)V

    :cond_1
    :goto_1
    const/4 v3, 0x0

    :goto_2
    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$2;->this$1:Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;

    invoke-virtual {v10}, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;->getChildCount()I

    move-result v10

    if-lt v3, v10, :cond_5

    return-void

    :cond_2
    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$2;->this$1:Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;

    const/high16 v11, 0x3f800000    # 1.0f

    invoke-virtual {v10, v11}, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;->setScaleX(F)V

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$2;->this$1:Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;

    const/high16 v11, 0x3f800000    # 1.0f

    invoke-virtual {v10, v11}, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;->setScaleY(F)V

    goto :goto_0

    :cond_3
    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$2;->this$1:Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;

    # getter for: Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;->mSpeedScale:F
    invoke-static {v10}, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;->access$2(Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;)F

    move-result v10

    const/high16 v11, 0x3f800000    # 1.0f

    cmpg-float v10, v10, v11

    if-gez v10, :cond_4

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$2;->this$1:Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;

    # getter for: Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;->mSpeedScale:F
    invoke-static {v10}, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;->access$2(Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;)F

    move-result v11

    move-wide/from16 v0, p4

    long-to-float v12, v0

    const/high16 v13, 0x447a0000    # 1000.0f

    div-float/2addr v12, v13

    add-float/2addr v11, v12

    invoke-static {v10, v11}, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;->access$3(Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;F)V

    :cond_4
    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$2;->this$1:Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;

    # getter for: Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;->mSpeedScale:F
    invoke-static {v10}, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;->access$2(Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;)F

    move-result v10

    const/high16 v11, 0x3f800000    # 1.0f

    cmpl-float v10, v10, v11

    if-lez v10, :cond_1

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$2;->this$1:Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;

    const/high16 v11, 0x3f800000    # 1.0f

    invoke-static {v10, v11}, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;->access$3(Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;F)V

    goto :goto_1

    :cond_5
    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$2;->this$1:Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;

    invoke-virtual {v10, v3}, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    instance-of v10, v8, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$FlyingIcon;

    if-nez v10, :cond_7

    :cond_6
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_7
    move-object v4, v8

    check-cast v4, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$FlyingIcon;

    move-wide/from16 v0, p4

    long-to-float v10, v0

    const/high16 v11, 0x447a0000    # 1000.0f

    div-float/2addr v10, v11

    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$2;->this$1:Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;

    # getter for: Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;->mSpeedScale:F
    invoke-static {v11}, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;->access$2(Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;)F

    move-result v11

    mul-float/2addr v10, v11

    invoke-virtual {v4, v10}, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$FlyingIcon;->update(F)V

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$FlyingIcon;->getWidth()I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$FlyingIcon;->getScaleX()F

    move-result v11

    mul-float v7, v10, v11

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$FlyingIcon;->getHeight()I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$FlyingIcon;->getScaleY()F

    move-result v11

    mul-float v6, v10, v11

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$FlyingIcon;->getX()F

    move-result v10

    add-float/2addr v10, v7

    const/4 v11, 0x0

    cmpg-float v10, v10, v11

    if-ltz v10, :cond_8

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$FlyingIcon;->getX()F

    move-result v10

    sub-float/2addr v10, v7

    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$2;->this$1:Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;

    invoke-virtual {v11}, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;->getWidth()I

    move-result v11

    int-to-float v11, v11

    cmpl-float v10, v10, v11

    if-gtz v10, :cond_8

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$FlyingIcon;->getY()F

    move-result v10

    add-float/2addr v10, v6

    const/4 v11, 0x0

    cmpg-float v10, v10, v11

    if-ltz v10, :cond_8

    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$FlyingIcon;->getY()F

    move-result v10

    sub-float/2addr v10, v6

    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$2;->this$1:Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;

    invoke-virtual {v11}, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board;->getHeight()I

    move-result v11

    int-to-float v11, v11

    cmpl-float v10, v10, v11

    if-lez v10, :cond_6

    :cond_8
    invoke-virtual {v4}, Lcom/cyanogenmod/trebuchet/RocketLauncher$Board$FlyingIcon;->reset()V

    goto :goto_3
.end method
