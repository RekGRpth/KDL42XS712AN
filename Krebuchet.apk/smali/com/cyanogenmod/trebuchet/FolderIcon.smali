.class public Lcom/cyanogenmod/trebuchet/FolderIcon;
.super Landroid/widget/LinearLayout;
.source "FolderIcon.java"

# interfaces
.implements Lcom/cyanogenmod/trebuchet/FolderInfo$FolderListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;,
        Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;
    }
.end annotation


# static fields
.field private static final CONSUMPTION_ANIMATION_DURATION:I = 0x64

.field private static DRAG_ZERO:I = 0x0

.field private static final DROP_IN_ANIMATION_DURATION:I = 0x190

.field private static final INITIAL_ITEM_ANIMATION_DURATION:I = 0x15e

.field private static final INNER_RING_GROWTH_FACTOR:F = 0.15f

.field private static ITEMNUMBERONE:I = 0x0

.field private static final NUM_ITEMS_IN_PREVIEW:I = 0x8

.field private static final OUTER_RING_GROWTH_FACTOR:F = 0.3f

.field private static final PERSPECTIVE_SCALE_FACTOR:F = 0.35f

.field private static final PERSPECTIVE_SHIFT_FACTOR:F = 0.24f

.field public static sSharedFolderLeaveBehind:Landroid/graphics/drawable/Drawable;

.field private static sStaticValuesDirty:Z


# instance fields
.field private mAnimParams:Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;

.field mAnimating:Z

.field private mAvailableSpaceInPreview:I

.field private mBaselineIconScale:F

.field private mBaselineIconSize:I

.field mFolder:Lcom/cyanogenmod/trebuchet/Folder;

.field private mFolderName:Lcom/cyanogenmod/trebuchet/BubbleTextView;

.field mFolderRingAnimator:Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;

.field mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

.field private mIntrinsicIconSize:I

.field private mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

.field private mMaxPerspectiveShift:F

.field private mParams:Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;

.field public mPreviewBackground:Landroid/widget/ImageView;

.field private mPreviewOffsetX:I

.field private mPreviewOffsetY:I

.field private mTotalWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x1

    sput-boolean v1, Lcom/cyanogenmod/trebuchet/FolderIcon;->sStaticValuesDirty:Z

    const/4 v0, 0x0

    sput-object v0, Lcom/cyanogenmod/trebuchet/FolderIcon;->sSharedFolderLeaveBehind:Landroid/graphics/drawable/Drawable;

    const/4 v0, 0x0

    sput v0, Lcom/cyanogenmod/trebuchet/FolderIcon;->DRAG_ZERO:I

    sput v1, Lcom/cyanogenmod/trebuchet/FolderIcon;->ITEMNUMBERONE:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1    # Landroid/content/Context;

    const/4 v5, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mFolderRingAnimator:Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;

    const/4 v0, -0x1

    iput v0, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mTotalWidth:I

    iput-boolean v5, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mAnimating:Z

    new-instance v0, Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;

    move-object v1, p0

    move v3, v2

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;-><init>(Lcom/cyanogenmod/trebuchet/FolderIcon;FFFI)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mParams:Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;

    new-instance v0, Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;

    move-object v1, p0

    move v3, v2

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;-><init>(Lcom/cyanogenmod/trebuchet/FolderIcon;FFFI)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mAnimParams:Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v5, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mFolderRingAnimator:Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;

    const/4 v0, -0x1

    iput v0, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mTotalWidth:I

    iput-boolean v5, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mAnimating:Z

    new-instance v0, Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;

    move-object v1, p0

    move v3, v2

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;-><init>(Lcom/cyanogenmod/trebuchet/FolderIcon;FFFI)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mParams:Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;

    new-instance v0, Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;

    move-object v1, p0

    move v3, v2

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;-><init>(Lcom/cyanogenmod/trebuchet/FolderIcon;FFFI)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mAnimParams:Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;

    return-void
.end method

.method static synthetic access$0()Z
    .locals 1

    sget-boolean v0, Lcom/cyanogenmod/trebuchet/FolderIcon;->sStaticValuesDirty:Z

    return v0
.end method

.method static synthetic access$1(Z)V
    .locals 0

    sput-boolean p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->sStaticValuesDirty:Z

    return-void
.end method

.method static synthetic access$2(Lcom/cyanogenmod/trebuchet/FolderIcon;)Lcom/cyanogenmod/trebuchet/BubbleTextView;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mFolderName:Lcom/cyanogenmod/trebuchet/BubbleTextView;

    return-object v0
.end method

.method static synthetic access$3(Lcom/cyanogenmod/trebuchet/FolderIcon;)Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mAnimParams:Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;

    return-object v0
.end method

.method private animateFirstItem(Landroid/graphics/drawable/Drawable;I)V
    .locals 7
    .param p1    # Landroid/graphics/drawable/Drawable;
    .param p2    # I

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/FolderIcon;->computePreviewDrawingParams(Landroid/graphics/drawable/Drawable;)V

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct {p0, v5, v6}, Lcom/cyanogenmod/trebuchet/FolderIcon;->computePreviewItemDrawingParams(ILcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;)Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    iget v5, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mAvailableSpaceInPreview:I

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    sub-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    int-to-float v2, v5

    iget v5, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mAvailableSpaceInPreview:I

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    sub-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    int-to-float v3, v5

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mAnimParams:Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;

    iput-object p1, v5, Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;->drawable:Landroid/graphics/drawable/Drawable;

    const/4 v5, 0x2

    new-array v5, v5, [F

    fill-array-data v5, :array_0

    invoke-static {v5}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v4

    new-instance v5, Lcom/cyanogenmod/trebuchet/FolderIcon$4;

    invoke-direct {v5, p0, v2, v0, v3}, Lcom/cyanogenmod/trebuchet/FolderIcon$4;-><init>(Lcom/cyanogenmod/trebuchet/FolderIcon;FLcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;F)V

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    new-instance v5, Lcom/cyanogenmod/trebuchet/FolderIcon$5;

    invoke-direct {v5, p0}, Lcom/cyanogenmod/trebuchet/FolderIcon$5;-><init>(Lcom/cyanogenmod/trebuchet/FolderIcon;)V

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    int-to-long v5, p2

    invoke-virtual {v4, v5, v6}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    invoke-virtual {v4}, Landroid/animation/ValueAnimator;->start()V

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private computePreviewDrawingParams(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    iget v4, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mIntrinsicIconSize:I

    if-ne v4, p1, :cond_0

    iget v4, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mTotalWidth:I

    if-eq v4, p2, :cond_1

    :cond_0
    iput p1, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mIntrinsicIconSize:I

    iput p2, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mTotalWidth:I

    sget v2, Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;->sPreviewSize:I

    sget v1, Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;->sPreviewPadding:I

    mul-int/lit8 v4, v1, 0x2

    sub-int v4, v2, v4

    iput v4, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mAvailableSpaceInPreview:I

    iget v4, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mAvailableSpaceInPreview:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    const v5, 0x3fe66666    # 1.8f

    mul-float/2addr v4, v5

    float-to-int v0, v4

    iget v4, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mIntrinsicIconSize:I

    int-to-float v4, v4

    const v5, 0x3f9eb852    # 1.24f

    mul-float/2addr v4, v5

    float-to-int v3, v4

    const/high16 v4, 0x3f800000    # 1.0f

    int-to-float v5, v0

    mul-float/2addr v4, v5

    int-to-float v5, v3

    div-float/2addr v4, v5

    iput v4, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mBaselineIconScale:F

    iget v4, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mIntrinsicIconSize:I

    int-to-float v4, v4

    iget v5, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mBaselineIconScale:F

    mul-float/2addr v4, v5

    float-to-int v4, v4

    iput v4, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mBaselineIconSize:I

    iget v4, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mBaselineIconSize:I

    int-to-float v4, v4

    const v5, 0x3e75c28f    # 0.24f

    mul-float/2addr v4, v5

    iput v4, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mMaxPerspectiveShift:F

    iget v4, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mTotalWidth:I

    iget v5, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mAvailableSpaceInPreview:I

    sub-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mPreviewOffsetX:I

    iput v1, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mPreviewOffsetY:I

    :cond_1
    return-void
.end method

.method private computePreviewDrawingParams(Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p1    # Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/FolderIcon;->getMeasuredWidth()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/cyanogenmod/trebuchet/FolderIcon;->computePreviewDrawingParams(II)V

    return-void
.end method

.method private computePreviewItemDrawingParams(ILcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;)Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;
    .locals 11
    .param p1    # I
    .param p2    # Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;

    rsub-int/lit8 v0, p1, 0x8

    add-int/lit8 p1, v0, -0x1

    const/high16 v7, 0x3e000000    # 0.125f

    const v8, 0x3e99999a    # 0.3f

    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, v7

    iget v1, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mMaxPerspectiveShift:F

    mul-float v6, v0, v1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mBaselineIconSize:I

    int-to-float v0, v0

    mul-float v10, v8, v0

    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, v8

    iget v1, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mBaselineIconSize:I

    int-to-float v1, v1

    mul-float v9, v0, v1

    const/4 v0, 0x4

    if-lt p1, v0, :cond_1

    invoke-static {}, Lcom/cyanogenmod/trebuchet/Launcher;->getWidthPixels()I

    move-result v0

    const/16 v1, 0x780

    if-ne v0, v1, :cond_0

    const/high16 v3, 0x42810000    # 64.5f

    rsub-int/lit8 v0, p1, 0x7

    mul-int/lit8 v0, v0, 0x14

    add-int/lit8 v0, v0, -0x2

    int-to-float v0, v0

    const/high16 v1, 0x3fc00000    # 1.5f

    mul-float v2, v0, v1

    :goto_0
    iget v0, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mBaselineIconScale:F

    mul-float v4, v0, v8

    const/high16 v0, 0x42a00000    # 80.0f

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, v7

    mul-float/2addr v0, v1

    float-to-int v5, v0

    if-nez p2, :cond_3

    new-instance p2, Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;

    move-object v0, p2

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;-><init>(Lcom/cyanogenmod/trebuchet/FolderIcon;FFFI)V

    :goto_1
    return-object p2

    :cond_0
    const/high16 v3, 0x422c0000    # 43.0f

    rsub-int/lit8 v0, p1, 0x7

    mul-int/lit8 v0, v0, 0x14

    add-int/lit8 v0, v0, -0x2

    int-to-float v2, v0

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/cyanogenmod/trebuchet/Launcher;->getWidthPixels()I

    move-result v0

    const/16 v1, 0x780

    if-ne v0, v1, :cond_2

    const/high16 v3, 0x42d20000    # 105.0f

    rsub-int/lit8 v0, p1, 0x3

    mul-int/lit8 v0, v0, 0x14

    add-int/lit8 v0, v0, -0x2

    int-to-float v0, v0

    const/high16 v1, 0x3fc00000    # 1.5f

    mul-float v2, v0, v1

    goto :goto_0

    :cond_2
    const/high16 v3, 0x428c0000    # 70.0f

    rsub-int/lit8 v0, p1, 0x3

    mul-int/lit8 v0, v0, 0x14

    add-int/lit8 v0, v0, -0x2

    int-to-float v2, v0

    goto :goto_0

    :cond_3
    iput v2, p2, Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;->transX:F

    iput v3, p2, Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;->transY:F

    iput v4, p2, Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;->scale:F

    iput v5, p2, Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;->overlayAlpha:I

    goto :goto_1
.end method

.method private drawPreviewItem(Landroid/graphics/Canvas;Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;)V
    .locals 5
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget v1, p2, Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;->transX:F

    iget v2, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mPreviewOffsetX:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iget v2, p2, Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;->transY:F

    iget v3, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mPreviewOffsetY:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    iget v1, p2, Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;->scale:F

    iget v2, p2, Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;->scale:F

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->scale(FF)V

    iget-object v0, p2, Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;->drawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mIntrinsicIconSize:I

    iget v2, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mIntrinsicIconSize:I

    invoke-virtual {v0, v4, v4, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    iget v1, p2, Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;->overlayAlpha:I

    invoke-static {v1, v4, v4, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->clearColorFilter()V

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method static fromXml(ILcom/cyanogenmod/trebuchet/Launcher;Landroid/view/ViewGroup;Lcom/cyanogenmod/trebuchet/FolderInfo;Lcom/cyanogenmod/trebuchet/IconCache;)Lcom/cyanogenmod/trebuchet/FolderIcon;
    .locals 6
    .param p0    # I
    .param p1    # Lcom/cyanogenmod/trebuchet/Launcher;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Lcom/cyanogenmod/trebuchet/FolderInfo;
    .param p4    # Lcom/cyanogenmod/trebuchet/IconCache;

    const/4 v5, 0x0

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-virtual {v2, p0, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/FolderIcon;

    const v2, 0x7f0d002f    # com.konka.avenger.R.id.folder_icon_name

    invoke-virtual {v1, v2}, Lcom/cyanogenmod/trebuchet/FolderIcon;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/cyanogenmod/trebuchet/BubbleTextView;

    iput-object v2, v1, Lcom/cyanogenmod/trebuchet/FolderIcon;->mFolderName:Lcom/cyanogenmod/trebuchet/BubbleTextView;

    iget-object v2, v1, Lcom/cyanogenmod/trebuchet/FolderIcon;->mFolderName:Lcom/cyanogenmod/trebuchet/BubbleTextView;

    invoke-virtual {v2, v5}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->setFocusable(Z)V

    iget-object v2, v1, Lcom/cyanogenmod/trebuchet/FolderIcon;->mFolderName:Lcom/cyanogenmod/trebuchet/BubbleTextView;

    iget-object v3, p3, Lcom/cyanogenmod/trebuchet/FolderInfo;->title:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->setText(Ljava/lang/CharSequence;)V

    const v2, 0x7f0d002e    # com.konka.avenger.R.id.preview_background

    invoke-virtual {v1, v2}, Lcom/cyanogenmod/trebuchet/FolderIcon;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v1, Lcom/cyanogenmod/trebuchet/FolderIcon;->mPreviewBackground:Landroid/widget/ImageView;

    invoke-virtual {v1, p3}, Lcom/cyanogenmod/trebuchet/FolderIcon;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v1, p1}, Lcom/cyanogenmod/trebuchet/FolderIcon;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-object p3, v1, Lcom/cyanogenmod/trebuchet/FolderIcon;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iput-object p1, v1, Lcom/cyanogenmod/trebuchet/FolderIcon;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    const v2, 0x7f0a0067    # com.konka.avenger.R.string.folder_name_format

    invoke-virtual {p1, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p3, Lcom/cyanogenmod/trebuchet/FolderInfo;->title:Ljava/lang/CharSequence;

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/cyanogenmod/trebuchet/FolderIcon;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-static {p1}, Lcom/cyanogenmod/trebuchet/Folder;->fromXml(Landroid/content/Context;)Lcom/cyanogenmod/trebuchet/Folder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/Launcher;->getDragController()Lcom/cyanogenmod/trebuchet/DragController;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/cyanogenmod/trebuchet/Folder;->setDragController(Lcom/cyanogenmod/trebuchet/DragController;)V

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Folder;->setFolderIcon(Lcom/cyanogenmod/trebuchet/FolderIcon;)V

    invoke-virtual {v0, p3}, Lcom/cyanogenmod/trebuchet/Folder;->bind(Lcom/cyanogenmod/trebuchet/FolderInfo;)V

    iput-object v0, v1, Lcom/cyanogenmod/trebuchet/FolderIcon;->mFolder:Lcom/cyanogenmod/trebuchet/Folder;

    iget-object v2, v1, Lcom/cyanogenmod/trebuchet/FolderIcon;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-object v2, v2, Lcom/cyanogenmod/trebuchet/FolderInfo;->contents:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, v1, Lcom/cyanogenmod/trebuchet/FolderIcon;->mPreviewBackground:Landroid/widget/ImageView;

    const v3, 0x7f020018    # com.konka.avenger.R.drawable.com_icon_file_uns

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    new-instance v2, Lcom/cyanogenmod/trebuchet/FolderIcon$1;

    invoke-direct {v2}, Lcom/cyanogenmod/trebuchet/FolderIcon$1;-><init>()V

    invoke-virtual {v1, v2}, Lcom/cyanogenmod/trebuchet/FolderIcon;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    new-instance v2, Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;

    invoke-direct {v2, p1, v1}, Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;Lcom/cyanogenmod/trebuchet/FolderIcon;)V

    iput-object v2, v1, Lcom/cyanogenmod/trebuchet/FolderIcon;->mFolderRingAnimator:Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;

    invoke-virtual {p3, v1}, Lcom/cyanogenmod/trebuchet/FolderInfo;->addListener(Lcom/cyanogenmod/trebuchet/FolderInfo$FolderListener;)V

    return-object v1

    :cond_0
    iget-object v2, v1, Lcom/cyanogenmod/trebuchet/FolderIcon;->mPreviewBackground:Landroid/widget/ImageView;

    const v3, 0x7f020017    # com.konka.avenger.R.drawable.com_icon_file_s

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private getLocalCenterForIndex(I[I)F
    .locals 6
    .param p1    # I
    .param p2    # [I

    const/high16 v5, 0x40000000    # 2.0f

    const/16 v2, 0x8

    invoke-static {v2, p1}, Ljava/lang/Math;->min(II)I

    move-result v2

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mParams:Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;

    invoke-direct {p0, v2, v3}, Lcom/cyanogenmod/trebuchet/FolderIcon;->computePreviewItemDrawingParams(ILcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;)Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;

    move-result-object v2

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mParams:Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mParams:Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;

    iget v3, v2, Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;->transX:F

    iget v4, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mPreviewOffsetX:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    iput v3, v2, Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;->transX:F

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mParams:Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;

    iget v3, v2, Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;->transY:F

    iget v4, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mPreviewOffsetY:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    iput v3, v2, Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;->transY:F

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mParams:Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;

    iget v2, v2, Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;->transX:F

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mParams:Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;

    iget v3, v3, Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;->scale:F

    iget v4, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mIntrinsicIconSize:I

    int-to-float v4, v4

    mul-float/2addr v3, v4

    div-float/2addr v3, v5

    add-float v0, v2, v3

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mParams:Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;

    iget v2, v2, Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;->transY:F

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mParams:Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;

    iget v3, v3, Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;->scale:F

    iget v4, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mIntrinsicIconSize:I

    int-to-float v4, v4

    mul-float/2addr v3, v4

    div-float/2addr v3, v5

    add-float v1, v2, v3

    const/4 v2, 0x0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    aput v3, p2, v2

    const/4 v2, 0x1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v3

    aput v3, p2, v2

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mParams:Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;

    iget v2, v2, Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;->scale:F

    return v2
.end method

.method private onDrop(Lcom/cyanogenmod/trebuchet/ShortcutInfo;Landroid/view/View;Landroid/graphics/Rect;FILjava/lang/Runnable;)V
    .locals 18
    .param p1    # Lcom/cyanogenmod/trebuchet/ShortcutInfo;
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/graphics/Rect;
    .param p4    # F
    .param p5    # I
    .param p6    # Ljava/lang/Runnable;

    const/4 v3, -0x1

    move-object/from16 v0, p1

    iput v3, v0, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->cellX:I

    const/4 v3, -0x1

    move-object/from16 v0, p1

    iput v3, v0, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->cellY:I

    if-eqz p2, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Launcher;->getDragLayer()Lcom/cyanogenmod/trebuchet/DragLayer;

    move-result-object v2

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p2

    invoke-virtual {v2, v0, v4}, Lcom/cyanogenmod/trebuchet/DragLayer;->getViewRectRelativeToSelf(Landroid/view/View;Landroid/graphics/Rect;)V

    move-object/from16 v5, p3

    if-nez v5, :cond_0

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Launcher;->getWorkspace()Lcom/cyanogenmod/trebuchet/Workspace;

    move-result-object v17

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/FolderIcon;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Lcom/cyanogenmod/trebuchet/CellLayout;

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lcom/cyanogenmod/trebuchet/Workspace;->setFinalTransitionTransform(Lcom/cyanogenmod/trebuchet/CellLayout;)V

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/FolderIcon;->getScaleX()F

    move-result v15

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/FolderIcon;->getScaleY()F

    move-result v16

    const/high16 v3, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/cyanogenmod/trebuchet/FolderIcon;->setScaleX(F)V

    const/high16 v3, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/cyanogenmod/trebuchet/FolderIcon;->setScaleY(F)V

    move-object/from16 v0, p0

    invoke-virtual {v2, v0, v5}, Lcom/cyanogenmod/trebuchet/DragLayer;->getDescendantRectRelativeToSelf(Landroid/view/View;Landroid/graphics/Rect;)F

    move-result p4

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lcom/cyanogenmod/trebuchet/FolderIcon;->setScaleX(F)V

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/FolderIcon;->setScaleY(F)V

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/FolderIcon;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Lcom/cyanogenmod/trebuchet/CellLayout;

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Lcom/cyanogenmod/trebuchet/Workspace;->resetTransitionTransform(Lcom/cyanogenmod/trebuchet/CellLayout;)V

    :cond_0
    const/4 v3, 0x2

    new-array v13, v3, [I

    move-object/from16 v0, p0

    move/from16 v1, p5

    invoke-direct {v0, v1, v13}, Lcom/cyanogenmod/trebuchet/FolderIcon;->getLocalCenterForIndex(I[I)F

    move-result v14

    const/4 v3, 0x0

    const/4 v7, 0x0

    aget v7, v13, v7

    int-to-float v7, v7

    mul-float v7, v7, p4

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    aput v7, v13, v3

    const/4 v3, 0x1

    const/4 v7, 0x1

    aget v7, v13, v7

    int-to-float v7, v7

    mul-float v7, v7, p4

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    aput v7, v13, v3

    const/4 v3, 0x0

    aget v3, v13, v3

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    sub-int/2addr v3, v7

    const/4 v7, 0x1

    aget v7, v13, v7

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    sub-int/2addr v7, v8

    invoke-virtual {v5, v3, v7}, Landroid/graphics/Rect;->offset(II)V

    const/16 v3, 0x8

    move/from16 v0, p5

    if-ge v0, v3, :cond_1

    const/high16 v6, 0x3f000000    # 0.5f

    :goto_0
    mul-float v7, v14, p4

    const/16 v8, 0x190

    new-instance v9, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v3, 0x40000000    # 2.0f

    invoke-direct {v9, v3}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    new-instance v10, Landroid/view/animation/AccelerateInterpolator;

    const/high16 v3, 0x40000000    # 2.0f

    invoke-direct {v10, v3}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    const/4 v12, 0x0

    move-object/from16 v3, p2

    move-object/from16 v11, p6

    invoke-virtual/range {v2 .. v12}, Lcom/cyanogenmod/trebuchet/DragLayer;->animateView(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;FFILandroid/view/animation/Interpolator;Landroid/view/animation/Interpolator;Ljava/lang/Runnable;Z)V

    new-instance v3, Lcom/cyanogenmod/trebuchet/FolderIcon$3;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v3, v0, v1}, Lcom/cyanogenmod/trebuchet/FolderIcon$3;-><init>(Lcom/cyanogenmod/trebuchet/FolderIcon;Lcom/cyanogenmod/trebuchet/ShortcutInfo;)V

    const-wide/16 v7, 0x190

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v7, v8}, Lcom/cyanogenmod/trebuchet/FolderIcon;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_1
    return-void

    :cond_1
    const/4 v6, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual/range {p0 .. p1}, Lcom/cyanogenmod/trebuchet/FolderIcon;->addItem(Lcom/cyanogenmod/trebuchet/ShortcutInfo;)V

    goto :goto_1
.end method

.method private willAcceptItem(Lcom/cyanogenmod/trebuchet/ItemInfo;)Z
    .locals 6
    .param p1    # Lcom/cyanogenmod/trebuchet/ItemInfo;

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget v1, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->itemType:I

    if-ne v1, v4, :cond_2

    move-object v0, p1

    check-cast v0, Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mFolder:Lcom/cyanogenmod/trebuchet/Folder;

    iget-object v5, v0, Lcom/cyanogenmod/trebuchet/FolderInfo;->contents:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/cyanogenmod/trebuchet/Folder;->checkFolderAdd(I)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    if-eq p1, v4, :cond_1

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-boolean v4, v4, Lcom/cyanogenmod/trebuchet/FolderInfo;->opened:Z

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    move v2, v3

    goto :goto_0

    :cond_2
    if-eqz v1, :cond_3

    if-eq v1, v2, :cond_3

    if-ne v1, v4, :cond_4

    :cond_3
    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mFolder:Lcom/cyanogenmod/trebuchet/Folder;

    sget v5, Lcom/cyanogenmod/trebuchet/FolderIcon;->ITEMNUMBERONE:I

    invoke-virtual {v4, v5}, Lcom/cyanogenmod/trebuchet/Folder;->checkFolderAdd(I)Z

    move-result v4

    if-nez v4, :cond_4

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    if-eq p1, v4, :cond_4

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-boolean v4, v4, Lcom/cyanogenmod/trebuchet/FolderInfo;->opened:Z

    if-eqz v4, :cond_0

    :cond_4
    move v2, v3

    goto :goto_0
.end method

.method private willAcceptItems(Lcom/cyanogenmod/trebuchet/ItemInfo;)Z
    .locals 4
    .param p1    # Lcom/cyanogenmod/trebuchet/ItemInfo;

    const/4 v1, 0x1

    iget v0, p1, Lcom/cyanogenmod/trebuchet/ItemInfo;->itemType:I

    if-eqz v0, :cond_0

    if-eq v0, v1, :cond_0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mFolder:Lcom/cyanogenmod/trebuchet/Folder;

    sget v3, Lcom/cyanogenmod/trebuchet/FolderIcon;->DRAG_ZERO:I

    invoke-virtual {v2, v3}, Lcom/cyanogenmod/trebuchet/Folder;->checkFolderAdd(I)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    if-eq p1, v2, :cond_1

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-boolean v2, v2, Lcom/cyanogenmod/trebuchet/FolderInfo;->opened:Z

    if-nez v2, :cond_1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public acceptDrop(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;

    move-object v0, p1

    check-cast v0, Lcom/cyanogenmod/trebuchet/ItemInfo;

    invoke-direct {p0, v0}, Lcom/cyanogenmod/trebuchet/FolderIcon;->willAcceptItem(Lcom/cyanogenmod/trebuchet/ItemInfo;)Z

    move-result v1

    return v1
.end method

.method public addItem(Lcom/cyanogenmod/trebuchet/ShortcutInfo;)V
    .locals 7
    .param p1    # Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/FolderInfo;->add(Lcom/cyanogenmod/trebuchet/ShortcutInfo;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-wide v2, v1, Lcom/cyanogenmod/trebuchet/FolderInfo;->id:J

    const/4 v4, 0x0

    iget v5, p1, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->cellX:I

    iget v6, p1, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->cellY:I

    move-object v1, p1

    invoke-static/range {v0 .. v6}, Lcom/cyanogenmod/trebuchet/LauncherModel;->addOrMoveItemInDatabase(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/ItemInfo;JIII)V

    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1    # Landroid/graphics/Canvas;

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mFolder:Lcom/cyanogenmod/trebuchet/Folder;

    if-nez v5, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mFolder:Lcom/cyanogenmod/trebuchet/Folder;

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/Folder;->getItemCount()I

    move-result v5

    if-nez v5, :cond_2

    iget-boolean v5, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mAnimating:Z

    if-eqz v5, :cond_0

    :cond_2
    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mFolder:Lcom/cyanogenmod/trebuchet/Folder;

    invoke-virtual {v5, v6}, Lcom/cyanogenmod/trebuchet/Folder;->getItemsInReadingOrder(Z)Ljava/util/ArrayList;

    move-result-object v2

    iget-boolean v5, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mAnimating:Z

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mAnimParams:Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;

    iget-object v5, v5, Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;->drawable:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v5}, Lcom/cyanogenmod/trebuchet/FolderIcon;->computePreviewDrawingParams(Landroid/graphics/drawable/Drawable;)V

    :goto_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/16 v6, 0x8

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v3

    iget-boolean v5, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mAnimating:Z

    if-nez v5, :cond_4

    add-int/lit8 v1, v3, -0x1

    :goto_2
    if-ltz v1, :cond_0

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v5

    aget-object v0, v5, v7

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mParams:Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;

    invoke-direct {p0, v1, v5}, Lcom/cyanogenmod/trebuchet/FolderIcon;->computePreviewItemDrawingParams(ILcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;)Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;

    move-result-object v5

    iput-object v5, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mParams:Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mParams:Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;

    iput-object v0, v5, Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;->drawable:Landroid/graphics/drawable/Drawable;

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mParams:Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;

    invoke-direct {p0, p1, v5}, Lcom/cyanogenmod/trebuchet/FolderIcon;->drawPreviewItem(Landroid/graphics/Canvas;Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    :cond_3
    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v5

    aget-object v0, v5, v7

    invoke-direct {p0, v0}, Lcom/cyanogenmod/trebuchet/FolderIcon;->computePreviewDrawingParams(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    :cond_4
    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mAnimParams:Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;

    invoke-direct {p0, p1, v5}, Lcom/cyanogenmod/trebuchet/FolderIcon;->drawPreviewItem(Landroid/graphics/Canvas;Lcom/cyanogenmod/trebuchet/FolderIcon$PreviewItemDrawingParams;)V

    goto :goto_0
.end method

.method public getDropTargetDelegate(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)Lcom/cyanogenmod/trebuchet/DropTarget;
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    const/4 v0, 0x0

    return-object v0
.end method

.method public getTextVisible()Z
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mFolderName:Lcom/cyanogenmod/trebuchet/BubbleTextView;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isDropEnabled()Z
    .locals 4

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/FolderIcon;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Workspace;->isSmall()Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :cond_0
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public onAdd(Lcom/cyanogenmod/trebuchet/ShortcutInfo;)V
    .locals 0
    .param p1    # Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/FolderIcon;->invalidate()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/FolderIcon;->requestLayout()V

    return-void
.end method

.method public onDragEnter(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/cyanogenmod/trebuchet/ItemInfo;

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/FolderIcon;->willAcceptItems(Lcom/cyanogenmod/trebuchet/ItemInfo;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/FolderIcon;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/FolderIcon;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/CellLayout;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mFolderRingAnimator:Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;

    iget v3, v1, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellX:I

    iget v4, v1, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellY:I

    invoke-virtual {v2, v3, v4}, Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;->setCell(II)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mFolderRingAnimator:Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;

    invoke-virtual {v2, v0}, Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;->setCellLayout(Lcom/cyanogenmod/trebuchet/CellLayout;)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mFolderRingAnimator:Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;->animateToAcceptState()V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mFolderRingAnimator:Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;

    invoke-virtual {v0, v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->showFolderAccept(Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;)V

    goto :goto_0
.end method

.method public onDragExit(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;

    check-cast p1, Lcom/cyanogenmod/trebuchet/ItemInfo;

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/FolderIcon;->willAcceptItems(Lcom/cyanogenmod/trebuchet/ItemInfo;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mFolderRingAnimator:Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/FolderIcon$FolderRingAnimator;->animateToNaturalState()V

    goto :goto_0
.end method

.method public onDragOver(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;

    return-void
.end method

.method public onDrop(Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;)V
    .locals 13
    .param p1    # Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    iget-object v0, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragInfo:Ljava/lang/Object;

    instance-of v0, v0, Lcom/cyanogenmod/trebuchet/ApplicationInfo;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragInfo:Ljava/lang/Object;

    check-cast v0, Lcom/cyanogenmod/trebuchet/ApplicationInfo;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->makeShortcut()Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    move-result-object v6

    :goto_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mFolder:Lcom/cyanogenmod/trebuchet/Folder;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Folder;->notifyDrop()V

    iget-object v7, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/FolderInfo;->contents:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v10

    iget-object v11, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->postAnimationRunnable:Ljava/lang/Runnable;

    move-object v5, p0

    move-object v8, v3

    move v9, v4

    invoke-direct/range {v5 .. v11}, Lcom/cyanogenmod/trebuchet/FolderIcon;->onDrop(Lcom/cyanogenmod/trebuchet/ShortcutInfo;Landroid/view/View;Landroid/graphics/Rect;FILjava/lang/Runnable;)V

    :goto_1
    return-void

    :cond_0
    iget-object v0, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragInfo:Ljava/lang/Object;

    instance-of v0, v0, Lcom/cyanogenmod/trebuchet/FolderInfo;

    if-eqz v0, :cond_2

    iget-object v12, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragInfo:Ljava/lang/Object;

    check-cast v12, Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mFolder:Lcom/cyanogenmod/trebuchet/Folder;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Folder;->notifyDrop()V

    iget-object v0, v12, Lcom/cyanogenmod/trebuchet/FolderInfo;->contents:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v0, v12}, Lcom/cyanogenmod/trebuchet/Launcher;->removeFolder(Lcom/cyanogenmod/trebuchet/FolderInfo;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-static {v0, v12}, Lcom/cyanogenmod/trebuchet/LauncherModel;->deleteItemFromDatabase(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/ItemInfo;)V

    goto :goto_1

    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    iget-object v2, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragView:Lcom/cyanogenmod/trebuchet/DragView;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/FolderInfo;->contents:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    iget-object v6, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->postAnimationRunnable:Ljava/lang/Runnable;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/cyanogenmod/trebuchet/FolderIcon;->onDrop(Lcom/cyanogenmod/trebuchet/ShortcutInfo;Landroid/view/View;Landroid/graphics/Rect;FILjava/lang/Runnable;)V

    goto :goto_2

    :cond_2
    iget-object v6, p1, Lcom/cyanogenmod/trebuchet/DropTarget$DragObject;->dragInfo:Ljava/lang/Object;

    check-cast v6, Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    goto :goto_0
.end method

.method public onItemsChanged()V
    .locals 2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mInfo:Lcom/cyanogenmod/trebuchet/FolderInfo;

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/FolderInfo;->contents:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mPreviewBackground:Landroid/widget/ImageView;

    const v1, 0x7f020018    # com.konka.avenger.R.drawable.com_icon_file_uns

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/FolderIcon;->invalidate()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/FolderIcon;->requestLayout()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mPreviewBackground:Landroid/widget/ImageView;

    const v1, 0x7f020017    # com.konka.avenger.R.drawable.com_icon_file_s

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public onRemove(Lcom/cyanogenmod/trebuchet/ShortcutInfo;)V
    .locals 0
    .param p1    # Lcom/cyanogenmod/trebuchet/ShortcutInfo;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/FolderIcon;->invalidate()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/FolderIcon;->requestLayout()V

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/cyanogenmod/trebuchet/FolderIcon;->sStaticValuesDirty:Z

    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method public onTitleChanged(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mFolderName:Lcom/cyanogenmod/trebuchet/BubbleTextView;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mContext:Landroid/content/Context;

    const v1, 0x7f0a0067    # com.konka.avenger.R.string.folder_name_format

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/FolderIcon;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public performCreateAnimation(Lcom/cyanogenmod/trebuchet/ShortcutInfo;Landroid/view/View;Lcom/cyanogenmod/trebuchet/ShortcutInfo;Landroid/view/View;Landroid/graphics/Rect;FLjava/lang/Runnable;)V
    .locals 8
    .param p1    # Lcom/cyanogenmod/trebuchet/ShortcutInfo;
    .param p2    # Landroid/view/View;
    .param p3    # Lcom/cyanogenmod/trebuchet/ShortcutInfo;
    .param p4    # Landroid/view/View;
    .param p5    # Landroid/graphics/Rect;
    .param p6    # F
    .param p7    # Ljava/lang/Runnable;

    const/4 v5, 0x1

    move-object v0, p2

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    aget-object v7, v0, v5

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {p2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/cyanogenmod/trebuchet/FolderIcon;->computePreviewDrawingParams(II)V

    move-object v0, p0

    move-object v1, p3

    move-object v2, p4

    move-object v3, p5

    move v4, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/cyanogenmod/trebuchet/FolderIcon;->onDrop(Lcom/cyanogenmod/trebuchet/ShortcutInfo;Landroid/view/View;Landroid/graphics/Rect;FILjava/lang/Runnable;)V

    const/16 v0, 0x15e

    invoke-direct {p0, v7, v0}, Lcom/cyanogenmod/trebuchet/FolderIcon;->animateFirstItem(Landroid/graphics/drawable/Drawable;I)V

    new-instance v0, Lcom/cyanogenmod/trebuchet/FolderIcon$2;

    invoke-direct {v0, p0, p1}, Lcom/cyanogenmod/trebuchet/FolderIcon$2;-><init>(Lcom/cyanogenmod/trebuchet/FolderIcon;Lcom/cyanogenmod/trebuchet/ShortcutInfo;)V

    const-wide/16 v1, 0x15e

    invoke-virtual {p0, v0, v1, v2}, Lcom/cyanogenmod/trebuchet/FolderIcon;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public setTextVisible(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mFolderName:Lcom/cyanogenmod/trebuchet/BubbleTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/FolderIcon;->mFolderName:Lcom/cyanogenmod/trebuchet/BubbleTextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->setVisibility(I)V

    goto :goto_0
.end method
