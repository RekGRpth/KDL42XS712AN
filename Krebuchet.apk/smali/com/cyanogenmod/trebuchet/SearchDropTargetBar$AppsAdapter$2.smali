.class Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$2;
.super Ljava/lang/Object;
.source "SearchDropTargetBar.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemSelectedListener;"
    }
.end annotation


# instance fields
.field private mSelected:Landroid/view/View;

.field final synthetic this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;


# direct methods
.method constructor <init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$2;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$2;->mSelected:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$2;->mSelected:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$2;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->access$3(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;)Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    move-result-object v1

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mTextNormalColor:I
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->access$0(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$2;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->access$3(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;)Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    move-result-object v1

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mTextFocusColor:I
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->access$1(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iput-object p2, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$2;->mSelected:Landroid/view/View;

    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$2;->mSelected:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$2;->mSelected:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$2;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->access$3(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;)Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    move-result-object v1

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mTextNormalColor:I
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->access$0(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$2;->mSelected:Landroid/view/View;

    return-void
.end method
