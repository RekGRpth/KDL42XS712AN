.class Lcom/cyanogenmod/trebuchet/DragController$1;
.super Ljava/lang/Object;
.source "DragController.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cyanogenmod/trebuchet/DragController;->onTouchEvent(Landroid/view/MotionEvent;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/DragController;

.field private final synthetic val$dragLayerX:I

.field private final synthetic val$dragLayerY:I


# direct methods
.method constructor <init>(Lcom/cyanogenmod/trebuchet/DragController;II)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/DragController$1;->this$0:Lcom/cyanogenmod/trebuchet/DragController;

    iput p2, p0, Lcom/cyanogenmod/trebuchet/DragController$1;->val$dragLayerX:I

    iput p3, p0, Lcom/cyanogenmod/trebuchet/DragController$1;->val$dragLayerY:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragController$1;->this$0:Lcom/cyanogenmod/trebuchet/DragController;

    iget v1, p0, Lcom/cyanogenmod/trebuchet/DragController$1;->val$dragLayerX:I

    int-to-float v1, v1

    iget v2, p0, Lcom/cyanogenmod/trebuchet/DragController$1;->val$dragLayerY:I

    int-to-float v2, v2

    # invokes: Lcom/cyanogenmod/trebuchet/DragController;->drop(FF)V
    invoke-static {v0, v1, v2}, Lcom/cyanogenmod/trebuchet/DragController;->access$3(Lcom/cyanogenmod/trebuchet/DragController;FF)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragController$1;->this$0:Lcom/cyanogenmod/trebuchet/DragController;

    # invokes: Lcom/cyanogenmod/trebuchet/DragController;->endDrag()V
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/DragController;->access$4(Lcom/cyanogenmod/trebuchet/DragController;)V

    return-void
.end method
