.class Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$2;
.super Ljava/lang/Object;
.source "SearchDropTargetBar.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemSelectedListener;"
    }
.end annotation


# instance fields
.field private mSelected:Landroid/view/View;

.field final synthetic this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;


# direct methods
.method constructor <init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$2;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$2;->mSelected:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$2;->mSelected:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$ViewHolder;

    iget-object v1, v0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$ViewHolder;->mNameView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$2;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->access$0(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;)Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    move-result-object v2

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mTextNormalColor:I
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->access$0(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, v0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$ViewHolder;->mDimsView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$2;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->access$0(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;)Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    move-result-object v2

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mTextNormalColor:I
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->access$0(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$ViewHolder;

    iget-object v1, v0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$ViewHolder;->mNameView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$2;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->access$0(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;)Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    move-result-object v2

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mTextFocusColor:I
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->access$1(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, v0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$ViewHolder;->mDimsView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$2;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->access$0(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;)Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    move-result-object v2

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mTextFocusColor:I
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->access$1(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iput-object p2, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$2;->mSelected:Landroid/view/View;

    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$2;->mSelected:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$2;->mSelected:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$ViewHolder;

    iget-object v1, v0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$ViewHolder;->mNameView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$2;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->access$0(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;)Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    move-result-object v2

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mTextNormalColor:I
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->access$0(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, v0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$ViewHolder;->mDimsView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$2;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->access$0(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;)Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    move-result-object v2

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mTextNormalColor:I
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->access$0(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$2;->mSelected:Landroid/view/View;

    return-void
.end method
