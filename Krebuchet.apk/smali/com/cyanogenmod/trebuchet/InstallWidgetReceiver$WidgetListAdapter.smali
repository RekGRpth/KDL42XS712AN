.class public Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetListAdapter;
.super Ljava/lang/Object;
.source "InstallWidgetReceiver.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/widget/ListAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "WidgetListAdapter"
.end annotation


# instance fields
.field private mActivities:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetMimeTypeHandlerData;",
            ">;"
        }
    .end annotation
.end field

.field private mClipData:Landroid/content/ClipData;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

.field private mMimeType:Ljava/lang/String;

.field private mTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

.field private mTargetLayoutPos:[I

.field private mTargetLayoutScreen:I


# direct methods
.method public constructor <init>(Lcom/cyanogenmod/trebuchet/Launcher;Ljava/lang/String;Landroid/content/ClipData;Ljava/util/List;Lcom/cyanogenmod/trebuchet/CellLayout;I[I)V
    .locals 0
    .param p1    # Lcom/cyanogenmod/trebuchet/Launcher;
    .param p2    # Ljava/lang/String;
    .param p3    # Landroid/content/ClipData;
    .param p5    # Lcom/cyanogenmod/trebuchet/CellLayout;
    .param p6    # I
    .param p7    # [I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cyanogenmod/trebuchet/Launcher;",
            "Ljava/lang/String;",
            "Landroid/content/ClipData;",
            "Ljava/util/List",
            "<",
            "Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetMimeTypeHandlerData;",
            ">;",
            "Lcom/cyanogenmod/trebuchet/CellLayout;",
            "I[I)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetListAdapter;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    iput-object p2, p0, Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetListAdapter;->mMimeType:Ljava/lang/String;

    iput-object p3, p0, Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetListAdapter;->mClipData:Landroid/content/ClipData;

    iput-object p4, p0, Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetListAdapter;->mActivities:Ljava/util/List;

    iput-object p5, p0, Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetListAdapter;->mTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    iput p6, p0, Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetListAdapter;->mTargetLayoutScreen:I

    iput-object p7, p0, Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetListAdapter;->mTargetLayoutPos:[I

    return-void
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetListAdapter;->mActivities:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 15
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    invoke-virtual/range {p3 .. p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v6

    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetListAdapter;->mInflater:Landroid/view/LayoutInflater;

    if-nez v11, :cond_0

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v11

    iput-object v11, p0, Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetListAdapter;->mInflater:Landroid/view/LayoutInflater;

    :cond_0
    if-nez p2, :cond_1

    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetListAdapter;->mInflater:Landroid/view/LayoutInflater;

    const v12, 0x7f03000e    # com.konka.avenger.R.layout.external_widget_drop_list_item

    const/4 v13, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v11, v12, v0, v13}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_1
    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetListAdapter;->mActivities:Ljava/util/List;

    move/from16 v0, p1

    invoke-interface {v11, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetMimeTypeHandlerData;

    iget-object v7, v4, Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetMimeTypeHandlerData;->resolveInfo:Landroid/content/pm/ResolveInfo;

    iget-object v9, v4, Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetMimeTypeHandlerData;->widgetInfo:Landroid/appwidget/AppWidgetProviderInfo;

    invoke-virtual {v7, v6}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const v11, 0x7f0d002a    # com.konka.avenger.R.id.provider_icon

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    invoke-virtual {v5, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v7, v6}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v11, 0x2

    new-array v10, v11, [I

    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetListAdapter;->mTargetLayout:Lcom/cyanogenmod/trebuchet/CellLayout;

    iget v12, v9, Landroid/appwidget/AppWidgetProviderInfo;->minWidth:I

    iget v13, v9, Landroid/appwidget/AppWidgetProviderInfo;->minHeight:I

    invoke-virtual {v11, v12, v13, v10}, Lcom/cyanogenmod/trebuchet/CellLayout;->rectToCell(II[I)[I

    const v11, 0x7f0d002b    # com.konka.avenger.R.id.provider

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    const v11, 0x7f0a0014    # com.konka.avenger.R.string.external_drop_widget_pick_format

    const/4 v12, 0x3

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v1, v12, v13

    const/4 v13, 0x1

    const/4 v14, 0x0

    aget v14, v10, v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    const/4 v13, 0x2

    const/4 v14, 0x1

    aget v14, v10, v14

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v2, v11, v12}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-object p2
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetListAdapter;->mActivities:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    return v0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetListAdapter;->mActivities:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetMimeTypeHandlerData;

    iget-object v7, v0, Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetMimeTypeHandlerData;->widgetInfo:Landroid/appwidget/AppWidgetProviderInfo;

    new-instance v1, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetListAdapter;->mMimeType:Ljava/lang/String;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetListAdapter;->mClipData:Landroid/content/ClipData;

    invoke-direct {v1, v7, v0, v2}, Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;-><init>(Landroid/appwidget/AppWidgetProviderInfo;Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetListAdapter;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    const-wide/16 v2, -0x64

    iget v4, p0, Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetListAdapter;->mTargetLayoutScreen:I

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/InstallWidgetReceiver$WidgetListAdapter;->mTargetLayoutPos:[I

    invoke-virtual/range {v0 .. v6}, Lcom/cyanogenmod/trebuchet/Launcher;->addAppWidgetFromDrop(Lcom/cyanogenmod/trebuchet/PendingAddWidgetInfo;JI[I[I)V

    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 0
    .param p1    # Landroid/database/DataSetObserver;

    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 0
    .param p1    # Landroid/database/DataSetObserver;

    return-void
.end method
