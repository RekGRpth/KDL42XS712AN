.class public Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;
.super Landroid/widget/TabHost;
.source "AppsCustomizeTabHost.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/view/View$OnHoverListener;
.implements Landroid/widget/TabHost$OnTabChangeListener;
.implements Lcom/cyanogenmod/trebuchet/LauncherTransitionable;


# static fields
.field private static final APPS_TAB_TAG:Ljava/lang/String; = "APPS"

.field static final LOG_TAG:Ljava/lang/String; = "AppsCustomizeTabHost"

.field private static final WIDGETS_TAB_TAG:Ljava/lang/String; = "WIDGETS"


# instance fields
.field private mAnimationBuffer:Landroid/widget/FrameLayout;

.field private mAppsCustomizePane:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

.field private mContent:Landroid/widget/LinearLayout;

.field private mCurFocusView:Landroid/view/View;

.field private mFadeScrollingIndicator:Z

.field private mInTransition:Z

.field private mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

.field private mLauncherTransition:Landroid/animation/Animator;

.field private final mLayoutInflater:Landroid/view/LayoutInflater;

.field private mResetAfterTransition:Z

.field private final mResources:Landroid/content/res/Resources;

.field private mSuppressContentCallback:Z

.field private mTabs:Landroid/view/ViewGroup;

.field private mTabsContainer:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/TabHost;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mSuppressContentCallback:Z

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mLayoutInflater:Landroid/view/LayoutInflater;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mResources:Landroid/content/res/Resources;

    move-object v0, p1

    check-cast v0, Lcom/cyanogenmod/trebuchet/Launcher;

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-static {p1}, Lcom/cyanogenmod/trebuchet/preference/PreferencesProvider$Interface$Drawer$Indicator;->getFadeScrollingIndicator(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mFadeScrollingIndicator:Z

    return-void
.end method

.method static synthetic access$0(Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;)Landroid/view/ViewGroup;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mTabs:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$1(Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;)Landroid/view/ViewGroup;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mTabsContainer:Landroid/view/ViewGroup;

    return-object v0
.end method

.method private enableAndBuildHardwareLayer()V
    .locals 2

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->isHardwareAccelerated()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->setLayerType(ILandroid/graphics/Paint;)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->buildLayer()V

    :cond_0
    return-void
.end method

.method private hideHint(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/DragLayer;

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/DragLayer;->hideHint(Landroid/view/View;)V

    return-void
.end method

.method private setContentTypeImmediate(Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;)V
    .locals 2
    .param p1    # Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mAppsCustomizePane:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView;->hideIndicator(Z)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mAppsCustomizePane:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    invoke-interface {v0, p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView;->setContentType(Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;)V

    return-void
.end method

.method private showHint(Landroid/view/View;)V
    .locals 7
    .param p1    # Landroid/view/View;

    const-wide/16 v3, 0x3e8

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/DragLayer;

    const v1, 0x7f0d0009    # com.konka.avenger.R.id.hint_text

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object v1, p1

    move-wide v5, v3

    invoke-virtual/range {v0 .. v6}, Lcom/cyanogenmod/trebuchet/DragLayer;->showDelayedHint(Landroid/view/View;Ljava/lang/String;JJ)V

    return-void
.end method


# virtual methods
.method public getAppsCustomizePagedView()Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;
    .locals 1

    const v0, 0x7f0d0019    # com.konka.avenger.R.id.apps_customize_pane_content

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/AppsCustomizePagedView;

    return-object v0
.end method

.method public getContentTypeForTabTag(Ljava/lang/String;)Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "APPS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;->Apps:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "WIDGETS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;->Widgets:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;->Apps:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    goto :goto_0
.end method

.method public getDescendantFocusability()I
    .locals 1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    const/high16 v0, 0x60000

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/widget/TabHost;->getDescendantFocusability()I

    move-result v0

    goto :goto_0
.end method

.method public getTabTagForContentType(Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;)Ljava/lang/String;
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    sget-object v0, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;->Apps:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    if-ne p1, v0, :cond_0

    const-string v0, "APPS"

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;->Widgets:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    if-ne p1, v0, :cond_1

    const-string v0, "WIDGETS"

    goto :goto_0

    :cond_1
    const-string v0, "APPS"

    goto :goto_0
.end method

.method isTransitioning()Z
    .locals 1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mInTransition:Z

    return v0
.end method

.method protected onFinishInflate()V
    .locals 13

    const-string v6, "AppsCustomizeTabHost"

    const-string v7, "onFinishInflate"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const v6, 0x7f0d0014    # com.konka.avenger.R.id.tabs_container

    invoke-virtual {p0, v6}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    const v6, 0x7f0d0019    # com.konka.avenger.R.id.apps_customize_pane_content

    invoke-virtual {p0, v6}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    iput-object v4, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mTabsContainer:Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mAppsCustomizePane:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    const v6, 0x7f0d001a    # com.konka.avenger.R.id.animation_buffer

    invoke-virtual {p0, v6}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/FrameLayout;

    iput-object v6, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mAnimationBuffer:Landroid/widget/FrameLayout;

    const v6, 0x7f0d0013    # com.konka.avenger.R.id.apps_customize_content

    invoke-virtual {p0, v6}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    iput-object v6, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mContent:Landroid/widget/LinearLayout;

    new-instance v3, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabKeyEventListener;

    invoke-direct {v3}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabKeyEventListener;-><init>()V

    const v6, 0x7f0d0018    # com.konka.avenger.R.id.applist_latest

    invoke-virtual {p0, v6}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    const v6, 0x7f0d0009    # com.konka.avenger.R.id.hint_text

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mResources:Landroid/content/res/Resources;

    const v8, 0x7f0a00b3    # com.konka.avenger.R.string.hint_latest_app

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    const v6, 0x7f0d001e    # com.konka.avenger.R.id.release_version

    invoke-virtual {p0, v6}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    :try_start_0
    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    const v7, 0x7f0a0005    # com.konka.avenger.R.string.versionFormatter

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v10}, Lcom/cyanogenmod/trebuchet/Launcher;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v11}, Lcom/cyanogenmod/trebuchet/Launcher;->getPackageName()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v10

    iget-object v10, v10, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    iget-object v10, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v10}, Lcom/cyanogenmod/trebuchet/Launcher;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v10

    iget-object v11, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v11}, Lcom/cyanogenmod/trebuchet/Launcher;->getPackageName()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v10

    iget v10, v10, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Lcom/cyanogenmod/trebuchet/Launcher;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v2

    const-string v6, "AppsCustomizeTabHost"

    const-string v7, "Can\'t find version info:"

    invoke-static {v6, v7, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 0
    .param p1    # Landroid/view/View;
    .param p2    # Z

    if-nez p2, :cond_0

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->hideHint(Landroid/view/View;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->showHint(Landroid/view/View;)V

    goto :goto_0
.end method

.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Launcher;->isWorkspaceVisible()Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-virtual {p1, p2}, Landroid/view/View;->onHoverEvent(Landroid/view/MotionEvent;)Z

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->showHint(Landroid/view/View;)V

    move v0, v1

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1, p2}, Landroid/view/View;->onHoverEvent(Landroid/view/MotionEvent;)Z

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->hideHint(Landroid/view/View;)V

    move v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onLauncherTransitionEnd(Lcom/cyanogenmod/trebuchet/Launcher;Landroid/animation/Animator;Z)V
    .locals 4
    .param p1    # Lcom/cyanogenmod/trebuchet/Launcher;
    .param p2    # Landroid/animation/Animator;
    .param p3    # Z

    const/4 v2, 0x0

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mInTransition:Z

    if-eqz p2, :cond_0

    invoke-virtual {p0, v3, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->setLayerType(ILandroid/graphics/Paint;)V

    :cond_0
    if-nez p3, :cond_2

    invoke-virtual {p1, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->dismissWorkspaceCling(Landroid/view/View;)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mAppsCustomizePane:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    invoke-interface {v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView;->showAllAppsCling()V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mAppsCustomizePane:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    invoke-interface {v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView;->loadContent()V

    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->isScreenLarge()Z

    move-result v2

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mFadeScrollingIndicator:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mAppsCustomizePane:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    invoke-interface {v2, v3}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView;->hideIndicator(Z)V

    :cond_1
    const v2, 0x1020011    # android.R.id.tabcontent

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    const v2, 0x7f0d0018    # com.konka.avenger.R.id.applist_latest

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v1}, Landroid/view/ViewGroup;->hasFocus()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->hasFocus()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1}, Landroid/view/ViewGroup;->requestFocus()Z

    :cond_2
    return-void
.end method

.method public onLauncherTransitionStart(Lcom/cyanogenmod/trebuchet/Launcher;Landroid/animation/Animator;Z)Z
    .locals 6
    .param p1    # Lcom/cyanogenmod/trebuchet/Launcher;
    .param p2    # Landroid/animation/Animator;
    .param p3    # Z

    const/4 v2, 0x1

    const/4 v3, 0x0

    iput-boolean v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mInTransition:Z

    const/4 v1, 0x0

    if-eqz p2, :cond_5

    move v0, v2

    :goto_0
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mLauncherTransition:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mContent:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-ne v4, v5, :cond_0

    iput-object p2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mLauncherTransition:Landroid/animation/Animator;

    const/4 v1, 0x1

    :cond_0
    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mContent:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    if-nez p3, :cond_1

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mAppsCustomizePane:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    invoke-interface {v4, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView;->loadContent(Z)V

    :cond_1
    if-eqz v0, :cond_2

    if-nez v1, :cond_2

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->enableAndBuildHardwareLayer()V

    :cond_2
    if-nez p3, :cond_3

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mAppsCustomizePane:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    invoke-interface {v2, v3}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView;->showIndicator(Z)V

    :cond_3
    iget-boolean v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mResetAfterTransition:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mAppsCustomizePane:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    invoke-interface {v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView;->reset()V

    iput-boolean v3, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mResetAfterTransition:Z

    :cond_4
    return v1

    :cond_5
    move v0, v3

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 1
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Landroid/widget/TabHost;->onLayout(ZIIII)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mLauncherTransition:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->enableAndBuildHardwareLayer()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mLauncherTransition:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mLauncherTransition:Landroid/animation/Animator;

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mTabs:Landroid/view/ViewGroup;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mTabs:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-gtz v2, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-super {p0, p1, p2}, Landroid/widget/TabHost;->onMeasure(II)V

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mAppsCustomizePane:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    check-cast v2, Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mTabs:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-eq v2, v0, :cond_0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mTabs:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput v0, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    new-instance v2, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost$1;

    invoke-direct {v2, p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost$1;-><init>(Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;)V

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->post(Ljava/lang/Runnable;)Z

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/TabHost;->onMeasure(II)V

    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onPause()V
    .locals 5

    const v2, 0x1020011    # android.R.id.tabcontent

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    const v2, 0x7f0d0018    # com.konka.avenger.R.id.applist_latest

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->isFocused()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "AppsCustomizeTabHost"

    const-string v3, "appLatestButton.isFocused()"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mCurFocusView:Landroid/view/View;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v1}, Landroid/view/ViewGroup;->hasFocus()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "AppsCustomizeTabHost"

    const-string v3, "contents.hasFocus()"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mAppsCustomizePane:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    invoke-interface {v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView;->getCurSelectView()Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mCurFocusView:Landroid/view/View;

    goto :goto_0

    :cond_1
    const-string v2, "AppsCustomizeTabHost"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "contents"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mCurFocusView:Landroid/view/View;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mCurFocusView:Landroid/view/View;

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    const-string v1, "AppsCustomizeTabHost"

    const-string v2, "onResume"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mContent:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mAppsCustomizePane:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView;->loadContent(Z)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mAppsCustomizePane:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    invoke-interface {v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView;->loadContent()V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mCurFocusView:Landroid/view/View;

    if-nez v1, :cond_1

    const v1, 0x1020011    # android.R.id.tabcontent

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->hasFocus()Z

    move-result v1

    if-nez v1, :cond_0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mCurFocusView:Landroid/view/View;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mCurFocusView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    goto :goto_0
.end method

.method public onTabChanged(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->getContentTypeForTabTag(Ljava/lang/String;)Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    move-result-object v0

    iget-boolean v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mSuppressContentCallback:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mSuppressContentCallback:Z

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mAppsCustomizePane:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    invoke-interface {v1, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView;->onTabChanged(Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;)V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1    # Landroid/view/MotionEvent;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mAppsCustomizePane:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, v1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/TabHost;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onTrimMemory()V
    .locals 2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mContent:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mAppsCustomizePane:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    invoke-interface {v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView;->clearAllWidgetPreviews()V

    return-void
.end method

.method reset()V
    .locals 1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mInTransition:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mResetAfterTransition:Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mAppsCustomizePane:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    invoke-interface {v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView;->reset()V

    goto :goto_0
.end method

.method selectAppsTab()V
    .locals 1

    sget-object v0, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;->Apps:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    invoke-direct {p0, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->setContentTypeImmediate(Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;)V

    const-string v0, "APPS"

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->setCurrentTabByTag(Ljava/lang/String;)V

    return-void
.end method

.method selectWidgetsTab()V
    .locals 1

    sget-object v0, Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;->Widgets:Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    invoke-direct {p0, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->setContentTypeImmediate(Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mAppsCustomizePane:Lcom/cyanogenmod/trebuchet/AppsCustomizeView;

    invoke-interface {v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizeView;->setCurrentToWidgets()V

    const-string v0, "WIDGETS"

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->setCurrentTabByTag(Ljava/lang/String;)V

    return-void
.end method

.method public setCurrentTabFromContent(Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;)V
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->mSuppressContentCallback:Z

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->getTabTagForContentType(Lcom/cyanogenmod/trebuchet/AppsCustomizeView$ContentType;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->setCurrentTabByTag(Ljava/lang/String;)V

    return-void
.end method
