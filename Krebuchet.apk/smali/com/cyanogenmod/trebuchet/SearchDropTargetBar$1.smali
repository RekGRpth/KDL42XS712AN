.class Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$1;
.super Ljava/lang/Object;
.source "SearchDropTargetBar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->initChangeWallpaperWindow()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;


# direct methods
.method constructor <init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$1;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1    # Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$1;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$1;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mImages:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->access$9(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/cyanogenmod/trebuchet/WallpaperChooserDialogFragment;->setWallpaper(Landroid/content/Context;ILjava/util/ArrayList;)Z

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$1;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mChangeWallpaperWindow:Lnet/londatiga/android/ArrowedPopupWindow;
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->access$10(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)Lnet/londatiga/android/ArrowedPopupWindow;

    move-result-object v1

    invoke-virtual {v1}, Lnet/londatiga/android/ArrowedPopupWindow;->dismiss()V

    return-void
.end method
