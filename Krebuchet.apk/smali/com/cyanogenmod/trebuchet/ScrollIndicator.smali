.class public interface abstract Lcom/cyanogenmod/trebuchet/ScrollIndicator;
.super Ljava/lang/Object;
.source "ScrollIndicator.java"


# virtual methods
.method public abstract cancelAnimations()V
.end method

.method public abstract hide(ZI)V
.end method

.method public abstract init(Lcom/cyanogenmod/trebuchet/PagedView;)V
.end method

.method public abstract isElasticScrollIndicator()Z
.end method

.method public abstract show(ZI)V
.end method

.method public abstract update()V
.end method
