.class Lcom/cyanogenmod/trebuchet/DragLayer$HideHintRunnable;
.super Ljava/lang/Object;
.source "DragLayer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cyanogenmod/trebuchet/DragLayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HideHintRunnable"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/DragLayer;


# direct methods
.method private constructor <init>(Lcom/cyanogenmod/trebuchet/DragLayer;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/DragLayer$HideHintRunnable;->this$0:Lcom/cyanogenmod/trebuchet/DragLayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/cyanogenmod/trebuchet/DragLayer;Lcom/cyanogenmod/trebuchet/DragLayer$HideHintRunnable;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/DragLayer$HideHintRunnable;-><init>(Lcom/cyanogenmod/trebuchet/DragLayer;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer$HideHintRunnable;->this$0:Lcom/cyanogenmod/trebuchet/DragLayer;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/DragLayer$HideHintRunnable;->this$0:Lcom/cyanogenmod/trebuchet/DragLayer;

    # getter for: Lcom/cyanogenmod/trebuchet/DragLayer;->mHintAnchor:Landroid/view/View;
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/DragLayer;->access$3(Lcom/cyanogenmod/trebuchet/DragLayer;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/DragLayer;->hideHint(Landroid/view/View;)V

    return-void
.end method
