.class Lcom/cyanogenmod/trebuchet/Launcher$CreateShortcut;
.super Ljava/lang/Object;
.source "Launcher.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnDismissListener;
.implements Landroid/content/DialogInterface$OnShowListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cyanogenmod/trebuchet/Launcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CreateShortcut"
.end annotation


# instance fields
.field private mAdapter:Lcom/cyanogenmod/trebuchet/AddAdapter;

.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/Launcher;


# direct methods
.method private constructor <init>(Lcom/cyanogenmod/trebuchet/Launcher;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Launcher$CreateShortcut;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/cyanogenmod/trebuchet/Launcher;Lcom/cyanogenmod/trebuchet/Launcher$CreateShortcut;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/Launcher$CreateShortcut;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;)V

    return-void
.end method

.method private cleanup()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher$CreateShortcut;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->dismissDialog(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method createDialog()Landroid/app/Dialog;
    .locals 4

    new-instance v2, Lcom/cyanogenmod/trebuchet/AddAdapter;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/Launcher$CreateShortcut;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-direct {v2, v3}, Lcom/cyanogenmod/trebuchet/AddAdapter;-><init>(Lcom/cyanogenmod/trebuchet/Launcher;)V

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher$CreateShortcut;->mAdapter:Lcom/cyanogenmod/trebuchet/AddAdapter;

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher$CreateShortcut;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    const/4 v3, 0x2

    invoke-direct {v0, v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher$CreateShortcut;->mAdapter:Lcom/cyanogenmod/trebuchet/AddAdapter;

    invoke-virtual {v0, v2, p0}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/app/AlertDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    invoke-virtual {v1, p0}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    invoke-virtual {v1, p0}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    return-object v1
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1    # Landroid/content/DialogInterface;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher$CreateShortcut;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->access$11(Lcom/cyanogenmod/trebuchet/Launcher;Z)V

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher$CreateShortcut;->cleanup()V

    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const/4 v2, 0x1

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher$CreateShortcut;->cleanup()V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher$CreateShortcut;->mAdapter:Lcom/cyanogenmod/trebuchet/AddAdapter;

    invoke-virtual {v1, p2}, Lcom/cyanogenmod/trebuchet/AddAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/AddAdapter$ListItem;

    iget v1, v0, Lcom/cyanogenmod/trebuchet/AddAdapter$ListItem;->actionTag:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher$CreateShortcut;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # getter for: Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeTabHost:Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->access$4(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher$CreateShortcut;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # getter for: Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeTabHost:Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->access$4(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    move-result-object v1

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->selectAppsTab()V

    :cond_0
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher$CreateShortcut;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v1, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->showAllApps(Z)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher$CreateShortcut;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # getter for: Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeTabHost:Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->access$4(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher$CreateShortcut;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # getter for: Lcom/cyanogenmod/trebuchet/Launcher;->mAppsCustomizeTabHost:Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->access$4(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;

    move-result-object v1

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/AppsCustomizeTabHost;->selectWidgetsTab()V

    :cond_1
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher$CreateShortcut;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v1, v2}, Lcom/cyanogenmod/trebuchet/Launcher;->showAllApps(Z)V

    goto :goto_0

    :pswitch_2
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher$CreateShortcut;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # invokes: Lcom/cyanogenmod/trebuchet/Launcher;->startWallpaper()V
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->access$18(Lcom/cyanogenmod/trebuchet/Launcher;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1    # Landroid/content/DialogInterface;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher$CreateShortcut;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->access$11(Lcom/cyanogenmod/trebuchet/Launcher;Z)V

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/Launcher$CreateShortcut;->cleanup()V

    return-void
.end method

.method public onShow(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1    # Landroid/content/DialogInterface;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher$CreateShortcut;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->access$11(Lcom/cyanogenmod/trebuchet/Launcher;Z)V

    return-void
.end method
