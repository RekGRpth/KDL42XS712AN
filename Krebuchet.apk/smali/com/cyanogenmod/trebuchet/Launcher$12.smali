.class Lcom/cyanogenmod/trebuchet/Launcher$12;
.super Ljava/lang/Object;
.source "Launcher.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cyanogenmod/trebuchet/Launcher;->bindConnectivityService()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/Launcher;


# direct methods
.method constructor <init>(Lcom/cyanogenmod/trebuchet/Launcher;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Launcher$12;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher$12;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    check-cast p2, Lcom/cyanogenmod/trebuchet/ConnectivityService$ConnectivityBinder;

    invoke-virtual {p2}, Lcom/cyanogenmod/trebuchet/ConnectivityService$ConnectivityBinder;->getService()Lcom/cyanogenmod/trebuchet/ConnectivityService;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->access$28(Lcom/cyanogenmod/trebuchet/Launcher;Lcom/cyanogenmod/trebuchet/ConnectivityService;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher$12;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # getter for: Lcom/cyanogenmod/trebuchet/Launcher;->mConnectivityServiceBinder:Lcom/cyanogenmod/trebuchet/ConnectivityService;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->access$29(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/cyanogenmod/trebuchet/ConnectivityService;

    move-result-object v0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher$12;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # getter for: Lcom/cyanogenmod/trebuchet/Launcher;->mSearchDropTargetBar:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->access$30(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/ConnectivityService;->registerUserCenterListener(Lcom/cyanogenmod/trebuchet/UserCenterMonitor$IUserCenterUpdateListener;)Lcom/cyanogenmod/trebuchet/ConnectivityService;

    move-result-object v0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher$12;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # getter for: Lcom/cyanogenmod/trebuchet/Launcher;->mSearchDropTargetBar:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->access$30(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/ConnectivityService;->registerNetworkListener(Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;)Lcom/cyanogenmod/trebuchet/ConnectivityService;

    move-result-object v0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher$12;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # getter for: Lcom/cyanogenmod/trebuchet/Launcher;->mSearchDropTargetBar:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/Launcher;->access$30(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/ConnectivityService;->registerUsbListener(Lcom/cyanogenmod/trebuchet/UsbMonitor$IUsbUpdateListener;)Lcom/cyanogenmod/trebuchet/ConnectivityService;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher$12;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # getter for: Lcom/cyanogenmod/trebuchet/Launcher;->mConnectivityServiceBinder:Lcom/cyanogenmod/trebuchet/ConnectivityService;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->access$29(Lcom/cyanogenmod/trebuchet/Launcher;)Lcom/cyanogenmod/trebuchet/ConnectivityService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/ConnectivityService;->startMonitors()V

    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher$12;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->access$28(Lcom/cyanogenmod/trebuchet/Launcher;Lcom/cyanogenmod/trebuchet/ConnectivityService;)V

    return-void
.end method
