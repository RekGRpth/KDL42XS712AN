.class public Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHost;
.super Landroid/appwidget/AppWidgetHost;
.source "LauncherAppWidgetHost.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Landroid/appwidget/AppWidgetHost;-><init>(Landroid/content/Context;I)V

    return-void
.end method


# virtual methods
.method protected onCreateView(Landroid/content/Context;ILandroid/appwidget/AppWidgetProviderInfo;)Landroid/appwidget/AppWidgetHostView;
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # I
    .param p3    # Landroid/appwidget/AppWidgetProviderInfo;

    new-instance v0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;

    invoke-direct {v0, p1, p3}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;-><init>(Landroid/content/Context;Landroid/appwidget/AppWidgetProviderInfo;)V

    return-object v0
.end method

.method public stopListening()V
    .locals 0

    invoke-super {p0}, Landroid/appwidget/AppWidgetHost;->stopListening()V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHost;->clearViews()V

    return-void
.end method
