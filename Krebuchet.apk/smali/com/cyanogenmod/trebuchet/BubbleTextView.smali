.class public Lcom/cyanogenmod/trebuchet/BubbleTextView;
.super Landroid/widget/TextView;
.source "BubbleTextView.java"


# static fields
.field static final PADDING_V:F = 3.0f

.field static final SHADOW_LARGE_COLOUR:I = -0x23000000

.field static final SHADOW_LARGE_RADIUS:F = 4.0f

.field static final SHADOW_SMALL_COLOUR:I = -0x34000000

.field static final SHADOW_SMALL_RADIUS:F = 1.75f

.field static final SHADOW_Y_OFFSET:F = 2.0f


# instance fields
.field private mBackground:Landroid/graphics/drawable/Drawable;

.field private mBackgroundSizeChanged:Z

.field private mBubbleColorAlpha:F

.field private mDidInvalidateForPressedState:Z

.field private mFocusedGlowColor:I

.field private mFocusedOutlineColor:I

.field private final mOutlineHelper:Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

.field private mPaint:Landroid/graphics/Paint;

.field private mPressedGlowColor:I

.field private mPressedOrFocusedBackground:Landroid/graphics/Bitmap;

.field private mPressedOutlineColor:I

.field private mPrevAlpha:I

.field private mStayPressed:Z

.field private final mTempCanvas:Landroid/graphics/Canvas;

.field private final mTempRect:Landroid/graphics/Rect;

.field private mTextVisible:Z

.field private mVisibleText:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mPrevAlpha:I

    new-instance v0, Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

    invoke-direct {v0}, Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;-><init>()V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mOutlineHelper:Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mTempCanvas:Landroid/graphics/Canvas;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mTempRect:Landroid/graphics/Rect;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mTextVisible:Z

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mPrevAlpha:I

    new-instance v0, Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

    invoke-direct {v0}, Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;-><init>()V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mOutlineHelper:Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mTempCanvas:Landroid/graphics/Canvas;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mTempRect:Landroid/graphics/Rect;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mTextVisible:Z

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->init()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mPrevAlpha:I

    new-instance v0, Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

    invoke-direct {v0}, Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;-><init>()V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mOutlineHelper:Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mTempCanvas:Landroid/graphics/Canvas;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mTempRect:Landroid/graphics/Rect;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mTextVisible:Z

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->init()V

    return-void
.end method

.method private createGlowingOutline(Landroid/graphics/Canvas;II)Landroid/graphics/Bitmap;
    .locals 5
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # I
    .param p3    # I

    sget v1, Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;->MAX_OUTER_BLUR_RADIUS:I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->getWidth()I

    move-result v2

    add-int/2addr v2, v1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->getHeight()I

    move-result v3

    add-int/2addr v3, v1

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    invoke-direct {p0, p1, v1}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->drawWithPadding(Landroid/graphics/Canvas;I)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mOutlineHelper:Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

    invoke-virtual {v2, v0, p1, p3, p2}, Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;->applyExtraThickExpensiveOutlineWithBlur(Landroid/graphics/Bitmap;Landroid/graphics/Canvas;II)V

    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    return-object v0
.end method

.method private drawWithPadding(Landroid/graphics/Canvas;I)V
    .locals 4
    .param p1    # Landroid/graphics/Canvas;
    .param p2    # I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->getDrawingRect(Landroid/graphics/Rect;)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->getExtendedPaddingTop()I

    move-result v1

    add-int/lit8 v1, v1, -0x3

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->getLayout()Landroid/text/Layout;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/text/Layout;->getLineTop(I)I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->getScrollX()I

    move-result v1

    neg-int v1, v1

    div-int/lit8 v2, p2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->getScrollY()I

    move-result v2

    neg-int v2, v2

    div-int/lit8 v3, p2, 0x2

    add-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    sget-object v1, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method private init()V
    .locals 6

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080003    # com.konka.avenger.R.color.bubble_dark_background

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    new-instance v2, Landroid/graphics/Paint;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setColor(I)V

    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x437f0000    # 255.0f

    div-float/2addr v2, v3

    iput v2, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mBubbleColorAlpha:F

    const v2, 0x7f080007    # com.konka.avenger.R.color.konka_orange

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mPressedGlowColor:I

    iput v2, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mPressedOutlineColor:I

    iput v2, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mFocusedGlowColor:I

    iput v2, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mFocusedOutlineColor:I

    const/high16 v2, 0x40800000    # 4.0f

    const/4 v3, 0x0

    const/high16 v4, 0x40000000    # 2.0f

    const/high16 v5, -0x23000000

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->setShadowLayer(FFFI)V

    return-void
.end method


# virtual methods
.method public applyFromShortcutInfo(Lcom/cyanogenmod/trebuchet/ShortcutInfo;Lcom/cyanogenmod/trebuchet/IconCache;)V
    .locals 3
    .param p1    # Lcom/cyanogenmod/trebuchet/ShortcutInfo;
    .param p2    # Lcom/cyanogenmod/trebuchet/IconCache;

    const/4 v2, 0x0

    invoke-virtual {p1, p2}, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->getIcon(Lcom/cyanogenmod/trebuchet/IconCache;)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v1, Lcom/cyanogenmod/trebuchet/FastBitmapDrawable;

    invoke-direct {v1, v0}, Lcom/cyanogenmod/trebuchet/FastBitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, v2, v1, v2, v2}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p1, Lcom/cyanogenmod/trebuchet/ShortcutInfo;->title:Ljava/lang/CharSequence;

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->setTag(Ljava/lang/Object;)V

    return-void
.end method

.method clearPressedOrFocusedBackground()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mPressedOrFocusedBackground:Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->setCellLayoutPressedOrFocusedIcon()V

    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1    # Landroid/graphics/Canvas;

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mBackground:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    iget v1, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mScrollX:I

    iget v2, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mScrollY:I

    iget-boolean v3, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mBackgroundSizeChanged:Z

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mRight:I

    iget v4, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mLeft:I

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mBottom:I

    iget v5, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mTop:I

    sub-int/2addr v4, v5

    invoke-virtual {v0, v6, v6, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iput-boolean v6, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mBackgroundSizeChanged:Z

    :cond_0
    or-int v3, v1, v2

    if-nez v3, :cond_2

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    const/high16 v4, 0x40800000    # 4.0f

    const/4 v5, 0x0

    const/high16 v6, 0x40000000    # 2.0f

    const/high16 v7, -0x23000000

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    invoke-super {p0, p1}, Landroid/widget/TextView;->draw(Landroid/graphics/Canvas;)V

    return-void

    :cond_2
    int-to-float v3, v1

    int-to-float v4, v2

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    neg-int v3, v1

    int-to-float v3, v3

    neg-int v4, v2

    int-to-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_0
.end method

.method protected drawableStateChanged()V
    .locals 7

    const/4 v3, 0x1

    const/4 v6, 0x0

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->isPressed()Z

    move-result v5

    if-eqz v5, :cond_2

    iget-boolean v3, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mDidInvalidateForPressedState:Z

    if-nez v3, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->setCellLayoutPressedOrFocusedIcon()V

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mBackground:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->getDrawableState()[I

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    :cond_1
    invoke-super {p0}, Landroid/widget/TextView;->drawableStateChanged()V

    return-void

    :cond_2
    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mPressedOrFocusedBackground:Landroid/graphics/Bitmap;

    if-nez v5, :cond_5

    move v0, v3

    :goto_1
    iget-boolean v5, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mStayPressed:Z

    if-nez v5, :cond_3

    iput-object v6, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mPressedOrFocusedBackground:Landroid/graphics/Bitmap;

    :cond_3
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->isFocused()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->getLayout()Landroid/text/Layout;

    move-result-object v5

    if-nez v5, :cond_6

    iput-object v6, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mPressedOrFocusedBackground:Landroid/graphics/Bitmap;

    :goto_2
    iput-boolean v4, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mStayPressed:Z

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->setCellLayoutPressedOrFocusedIcon()V

    :cond_4
    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mPressedOrFocusedBackground:Landroid/graphics/Bitmap;

    if-nez v5, :cond_7

    move v1, v3

    :goto_3
    if-nez v0, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->setCellLayoutPressedOrFocusedIcon()V

    goto :goto_0

    :cond_5
    move v0, v4

    goto :goto_1

    :cond_6
    iput-object v6, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mPressedOrFocusedBackground:Landroid/graphics/Bitmap;

    goto :goto_2

    :cond_7
    move v1, v4

    goto :goto_3
.end method

.method getPressedOrFocusedBackground()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mPressedOrFocusedBackground:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method getPressedOrFocusedBackgroundPadding()I
    .locals 1

    sget v0, Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;->MAX_OUTER_BLUR_RADIUS:I

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/TextView;->onAttachedToWindow()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mBackground:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mBackground:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    invoke-super {p0}, Landroid/widget/TextView;->onDetachedFromWindow()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mBackground:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mBackground:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    :cond_0
    return-void
.end method

.method protected onSetAlpha(I)Z
    .locals 3
    .param p1    # I

    iget v0, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mPrevAlpha:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mPrevAlpha:I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mPaint:Landroid/graphics/Paint;

    int-to-float v1, p1

    iget v2, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mBubbleColorAlpha:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    invoke-super {p0, p1}, Landroid/widget/TextView;->onSetAlpha(I)Z

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1    # Landroid/view/MotionEvent;

    invoke-super {p0, p1}, Landroid/widget/TextView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mPressedOrFocusedBackground:Landroid/graphics/Bitmap;

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mTempCanvas:Landroid/graphics/Canvas;

    iget v2, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mPressedGlowColor:I

    iget v3, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mPressedOutlineColor:I

    invoke-direct {p0, v1, v2, v3}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->createGlowingOutline(Landroid/graphics/Canvas;II)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mPressedOrFocusedBackground:Landroid/graphics/Bitmap;

    :cond_1
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->isPressed()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mDidInvalidateForPressedState:Z

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->setCellLayoutPressedOrFocusedIcon()V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mDidInvalidateForPressedState:Z

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->isPressed()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mPressedOrFocusedBackground:Landroid/graphics/Bitmap;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method setCellLayoutPressedOrFocusedIcon()V
    .locals 3

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    instance-of v2, v2, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/CellLayout;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mPressedOrFocusedBackground:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_1

    :goto_0
    invoke-virtual {v0, p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->setPressedOrFocusedIcon(Lcom/cyanogenmod/trebuchet/BubbleTextView;)V

    :cond_0
    return-void

    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method

.method protected setFrame(IIII)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget v0, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mLeft:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mRight:I

    if-ne v0, p3, :cond_0

    iget v0, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mTop:I

    if-ne v0, p2, :cond_0

    iget v0, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mBottom:I

    if-eq v0, p4, :cond_1

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mBackgroundSizeChanged:Z

    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->setFrame(IIII)Z

    move-result v0

    return v0
.end method

.method setStayPressed(Z)V
    .locals 1
    .param p1    # Z

    iput-boolean p1, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mStayPressed:Z

    if-nez p1, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mPressedOrFocusedBackground:Landroid/graphics/Bitmap;

    :cond_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->setCellLayoutPressedOrFocusedIcon()V

    return-void
.end method

.method public setTextVisible(Z)V
    .locals 1
    .param p1    # Z

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mTextVisible:Z

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mTextVisible:Z

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mVisibleText:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mVisibleText:Ljava/lang/CharSequence;

    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/BubbleTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1
    .param p1    # Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/BubbleTextView;->mBackground:Landroid/graphics/drawable/Drawable;

    if-eq p1, v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/TextView;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
