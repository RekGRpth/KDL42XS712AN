.class Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$4;
.super Ljava/lang/Object;
.source "SearchDropTargetBar.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->getAppsDialog()Lgreendroid/widget/PagedDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

.field private final synthetic val$adapter:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;


# direct methods
.method constructor <init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$4;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    iput-object p2, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$4;->val$adapter:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$4;->val$adapter:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->getSelectedApps()Ljava/util/HashSet;

    move-result-object v2

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$4;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->access$11(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)Lcom/cyanogenmod/trebuchet/Launcher;

    move-result-object v3

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Launcher;->getWorkspace()Lcom/cyanogenmod/trebuchet/Workspace;

    move-result-object v3

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Workspace;->getCurrentDropLayout()Lcom/cyanogenmod/trebuchet/CellLayout;

    move-result-object v1

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "[wjx]how many selectedapps?"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_0

    return-void

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/ApplicationInfo;

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "[wjx]how many?"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/util/HashSet;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->title:Ljava/lang/CharSequence;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$4;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;
    invoke-static {v4}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->access$11(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)Lcom/cyanogenmod/trebuchet/Launcher;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Lcom/cyanogenmod/trebuchet/Launcher;->addExternalItemToScreen(Lcom/cyanogenmod/trebuchet/ItemInfo;Lcom/cyanogenmod/trebuchet/CellLayout;)V

    goto :goto_0
.end method
