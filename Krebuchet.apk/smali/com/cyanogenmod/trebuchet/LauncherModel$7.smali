.class Lcom/cyanogenmod/trebuchet/LauncherModel$7;
.super Ljava/lang/Object;
.source "LauncherModel.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cyanogenmod/trebuchet/LauncherModel;->updateItemInDatabaseHelper(Landroid/content/Context;Landroid/content/ContentValues;Lcom/cyanogenmod/trebuchet/ItemInfo;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$callingFunction:Ljava/lang/String;

.field private final synthetic val$cr:Landroid/content/ContentResolver;

.field private final synthetic val$item:Lcom/cyanogenmod/trebuchet/ItemInfo;

.field private final synthetic val$itemId:J

.field private final synthetic val$uri:Landroid/net/Uri;

.field private final synthetic val$values:Landroid/content/ContentValues;


# direct methods
.method constructor <init>(Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;JLcom/cyanogenmod/trebuchet/ItemInfo;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$7;->val$cr:Landroid/content/ContentResolver;

    iput-object p2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$7;->val$uri:Landroid/net/Uri;

    iput-object p3, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$7;->val$values:Landroid/content/ContentValues;

    iput-wide p4, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$7;->val$itemId:J

    iput-object p6, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$7;->val$item:Lcom/cyanogenmod/trebuchet/ItemInfo;

    iput-object p7, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$7;->val$callingFunction:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/4 v5, 0x0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$7;->val$cr:Landroid/content/ContentResolver;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$7;->val$uri:Landroid/net/Uri;

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$7;->val$values:Landroid/content/ContentValues;

    invoke-virtual {v2, v3, v4, v5, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    sget-object v2, Lcom/cyanogenmod/trebuchet/LauncherModel;->sItemsIdMap:Ljava/util/HashMap;

    iget-wide v3, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$7;->val$itemId:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/ItemInfo;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$7;->val$item:Lcom/cyanogenmod/trebuchet/ItemInfo;

    if-eq v2, v0, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v2, "item: "

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$7;->val$item:Lcom/cyanogenmod/trebuchet/ItemInfo;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$7;->val$item:Lcom/cyanogenmod/trebuchet/ItemInfo;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/ItemInfo;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "modelItem: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/ItemInfo;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Error: ItemInfo passed to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/LauncherModel$7;->val$callingFunction:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " doesn\'t match original"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const-string v2, "null"

    goto :goto_0

    :cond_1
    const-string v2, "null"

    goto :goto_1

    :cond_2
    iget-wide v2, v0, Lcom/cyanogenmod/trebuchet/ItemInfo;->container:J

    const-wide/16 v4, -0x64

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    iget-wide v2, v0, Lcom/cyanogenmod/trebuchet/ItemInfo;->container:J

    const-wide/16 v4, -0x65

    cmp-long v2, v2, v4

    if-nez v2, :cond_5

    :cond_3
    sget-object v2, Lcom/cyanogenmod/trebuchet/LauncherModel;->sWorkspaceItems:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    sget-object v2, Lcom/cyanogenmod/trebuchet/LauncherModel;->sWorkspaceItems:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    :goto_2
    return-void

    :cond_5
    sget-object v2, Lcom/cyanogenmod/trebuchet/LauncherModel;->sWorkspaceItems:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_2
.end method
