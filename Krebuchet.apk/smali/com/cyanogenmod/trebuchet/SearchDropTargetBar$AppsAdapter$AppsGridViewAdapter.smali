.class Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter;
.super Landroid/widget/BaseAdapter;
.source "SearchDropTargetBar.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AppsGridViewAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/BaseAdapter;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field private mCount:I

.field private mHoverListener:Landroid/view/View$OnHoverListener;

.field private mStartIdx:I

.field final synthetic this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;


# direct methods
.method private constructor <init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;)V
    .locals 1

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter;->mStartIdx:I

    iput v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter;->mCount:I

    new-instance v0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter$1;

    invoke-direct {v0, p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter$1;-><init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter;)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter;->mHoverListener:Landroid/view/View$OnHoverListener;

    return-void
.end method

.method synthetic constructor <init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter;-><init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 2

    const/4 v0, 0x0

    iget v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter;->mCount:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter;->mCount:I

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->access$3(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;)Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    move-result-object v0

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mApps:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->access$2(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)Ljava/util/ArrayList;

    move-result-object v0

    iget v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter;->mStartIdx:I

    add-int/2addr v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter;->mStartIdx:I

    add-int/2addr v0, p1

    int-to-long v0, v0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    iget v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter;->mStartIdx:I

    add-int/2addr v3, p1

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;
    invoke-static {v4}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->access$3(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;)Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    move-result-object v4

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mApps:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->access$2(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v3, v4, :cond_0

    const/4 v3, 0x0

    :goto_0
    return-object v3

    :cond_0
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->access$3(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;)Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    move-result-object v3

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mApps:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->access$2(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)Ljava/util/ArrayList;

    move-result-object v3

    iget v4, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter;->mStartIdx:I

    add-int/2addr v4, p1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/ApplicationInfo;

    if-nez p2, :cond_1

    new-instance p2, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->access$0(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;)Landroid/content/Context;

    move-result-object v3

    invoke-direct {p2, v3}, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter;->mHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {p2, v3}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    new-instance v2, Landroid/widget/AbsListView$LayoutParams;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->access$3(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;)Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    move-result-object v3

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mCellWidth:I
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->access$3(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)I

    move-result v3

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;
    invoke-static {v4}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->access$3(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;)Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    move-result-object v4

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mCellHeight:I
    invoke-static {v4}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->access$4(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)I

    move-result v4

    invoke-direct {v2, v3, v4}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    move-object v3, p2

    check-cast v3, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;

    invoke-virtual {v3, v2}, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v3, 0x7f0d004c    # com.konka.avenger.R.id.paged_view_icon

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :cond_1
    move-object v3, p2

    check-cast v3, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->mHolographicOutlineHelper:Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;
    invoke-static {v4}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->access$1(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;)Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;->applyFromApplicationInfo(Lcom/cyanogenmod/trebuchet/ApplicationInfo;Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;)V

    move-object v3, p2

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    move-object v0, p2

    check-cast v0, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;->getApplicationInfo()Lcom/cyanogenmod/trebuchet/ApplicationInfo;

    move-result-object v1

    const-string v2, "SearchDropTargetBar"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onItemClick componame=("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v1, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->componentName:Landroid/content/ComponentName;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/PagedDialogViewIcon;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->mSelectedApps:Ljava/util/HashSet;
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->access$2(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;)Ljava/util/HashSet;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[wjx]onItemClick! selected:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->mSelectedApps:Ljava/util/HashSet;
    invoke-static {v4}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->access$2(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;)Ljava/util/HashSet;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/HashSet;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->mSelectedApps:Ljava/util/HashSet;
    invoke-static {v2}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;->access$2(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter;)Ljava/util/HashSet;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public setRange(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    iput p1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter;->mStartIdx:I

    iput p2, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$AppsAdapter$AppsGridViewAdapter;->mCount:I

    return-void
.end method
