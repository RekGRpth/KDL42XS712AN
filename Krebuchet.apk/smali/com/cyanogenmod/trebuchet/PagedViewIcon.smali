.class public Lcom/cyanogenmod/trebuchet/PagedViewIcon;
.super Landroid/widget/TextView;
.source "PagedViewIcon.java"

# interfaces
.implements Landroid/widget/Checkable;


# static fields
.field private static final TAG:Ljava/lang/String; = "PagedViewIcon"


# instance fields
.field private mAlpha:I

.field private mCheckedAlpha:F

.field private mCheckedAlphaAnimator:Landroid/animation/ObjectAnimator;

.field private mCheckedFadeInDuration:I

.field private mCheckedFadeOutDuration:I

.field private mCheckedOutline:Landroid/graphics/Bitmap;

.field private mHolographicAlpha:I

.field private mHolographicOutline:Landroid/graphics/Bitmap;

.field private mHolographicOutlineHelper:Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

.field mHolographicOutlineView:Lcom/cyanogenmod/trebuchet/HolographicPagedViewIcon;

.field private mIcon:Landroid/graphics/Bitmap;

.field private mIsChecked:Z

.field private final mPaint:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/cyanogenmod/trebuchet/PagedViewIcon;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/cyanogenmod/trebuchet/PagedViewIcon;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    const v3, 0x7f0b0003    # com.konka.avenger.R.integer.config_dragAppsCustomizeIconFadeAlpha

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mPaint:Landroid/graphics/Paint;

    const/16 v2, 0xff

    iput v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mAlpha:I

    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mCheckedAlpha:F

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x43800000    # 256.0f

    div-float/2addr v2, v3

    iput v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mCheckedAlpha:F

    const v2, 0x7f0b0001    # com.konka.avenger.R.integer.config_dragAppsCustomizeIconFadeInDuration

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mCheckedFadeInDuration:I

    const v2, 0x7f0b0002    # com.konka.avenger.R.integer.config_dragAppsCustomizeIconFadeOutDuration

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mCheckedFadeOutDuration:I

    :cond_0
    new-instance v2, Lcom/cyanogenmod/trebuchet/HolographicPagedViewIcon;

    invoke-direct {v2, p1, p0}, Lcom/cyanogenmod/trebuchet/HolographicPagedViewIcon;-><init>(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/PagedViewIcon;)V

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mHolographicOutlineView:Lcom/cyanogenmod/trebuchet/HolographicPagedViewIcon;

    return-void
.end method


# virtual methods
.method public applyFromApplicationInfo(Lcom/cyanogenmod/trebuchet/ApplicationInfo;Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;)V
    .locals 3
    .param p1    # Lcom/cyanogenmod/trebuchet/ApplicationInfo;
    .param p2    # Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

    const/4 v2, 0x0

    iput-object p2, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mHolographicOutlineHelper:Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

    iget-object v0, p1, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->iconBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mIcon:Landroid/graphics/Bitmap;

    new-instance v0, Lcom/cyanogenmod/trebuchet/FastBitmapDrawable;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mIcon:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Lcom/cyanogenmod/trebuchet/FastBitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, v2, v0, v2, v2}, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p1, Lcom/cyanogenmod/trebuchet/ApplicationInfo;->title:Ljava/lang/CharSequence;

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->setTag(Ljava/lang/Object;)V

    return-void
.end method

.method protected getHolographicOutline()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mHolographicOutline:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method protected getHolographicOutlineView()Lcom/cyanogenmod/trebuchet/HolographicPagedViewIcon;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mHolographicOutlineView:Lcom/cyanogenmod/trebuchet/HolographicPagedViewIcon;

    return-object v0
.end method

.method public invalidateCheckedImage()V
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mCheckedOutline:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mCheckedOutline:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mCheckedOutline:Landroid/graphics/Bitmap;

    :cond_0
    return-void
.end method

.method public isChecked()Z
    .locals 1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mIsChecked:Z

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1    # Landroid/graphics/Canvas;

    iget v5, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mAlpha:I

    if-lez v5, :cond_0

    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    :cond_0
    const/4 v4, 0x0

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mCheckedOutline:Landroid/graphics/Bitmap;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mPaint:Landroid/graphics/Paint;

    const/16 v6, 0xff

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mCheckedOutline:Landroid/graphics/Bitmap;

    :cond_1
    if-eqz v4, :cond_2

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->getScrollX()I

    move-result v3

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->getCompoundPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->getCompoundPaddingRight()I

    move-result v1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->getWidth()I

    move-result v5

    sub-int/2addr v5, v1

    sub-int v2, v5, v0

    add-int v5, v3, v0

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    sub-int v6, v2, v6

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    int-to-float v5, v5

    iget v6, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mPaddingTop:I

    int-to-float v6, v6

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, v5, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_2
    return-void
.end method

.method public setAlpha(F)V
    .locals 6
    .param p1    # F

    const/high16 v5, 0x437f0000    # 255.0f

    invoke-static {p1}, Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;->viewAlphaInterpolator(F)F

    move-result v3

    invoke-static {p1}, Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;->highlightAlphaInterpolator(F)F

    move-result v0

    mul-float v4, v3, v5

    float-to-int v2, v4

    mul-float v4, v0, v5

    float-to-int v1, v4

    iget v4, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mAlpha:I

    if-ne v4, v2, :cond_0

    iget v4, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mHolographicAlpha:I

    if-eq v4, v1, :cond_1

    :cond_0
    iput v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mAlpha:I

    iput v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mHolographicAlpha:I

    invoke-super {p0, v3}, Landroid/widget/TextView;->setAlpha(F)V

    :cond_1
    return-void
.end method

.method public setChecked(Z)V
    .locals 1
    .param p1    # Z

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->setChecked(ZZ)V

    return-void
.end method

.method setChecked(ZZ)V
    .locals 6
    .param p1    # Z
    .param p2    # Z

    iget-boolean v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mIsChecked:Z

    if-eq v2, p1, :cond_1

    iput-boolean p1, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mIsChecked:Z

    iget-boolean v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mIsChecked:Z

    if-eqz v2, :cond_2

    iget v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mCheckedAlpha:F

    iget v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mCheckedFadeInDuration:I

    :goto_0
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mCheckedAlphaAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mCheckedAlphaAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->cancel()V

    :cond_0
    if-eqz p2, :cond_3

    const-string v2, "alpha"

    const/4 v3, 0x2

    new-array v3, v3, [F

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->getAlpha()F

    move-result v5

    aput v5, v3, v4

    const/4 v4, 0x1

    aput v0, v3, v4

    invoke-static {p0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mCheckedAlphaAnimator:Landroid/animation/ObjectAnimator;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mCheckedAlphaAnimator:Landroid/animation/ObjectAnimator;

    int-to-long v3, v1

    invoke-virtual {v2, v3, v4}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mCheckedAlphaAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->start()V

    :goto_1
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->invalidate()V

    :cond_1
    return-void

    :cond_2
    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mCheckedFadeOutDuration:I

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->setAlpha(F)V

    goto :goto_1
.end method

.method public setHolographicOutline(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1    # Landroid/graphics/Bitmap;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mHolographicOutline:Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->getHolographicOutlineView()Lcom/cyanogenmod/trebuchet/HolographicPagedViewIcon;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/HolographicPagedViewIcon;->invalidate()V

    return-void
.end method

.method public toggle()V
    .locals 1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->mIsChecked:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/PagedViewIcon;->setChecked(Z)V

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
