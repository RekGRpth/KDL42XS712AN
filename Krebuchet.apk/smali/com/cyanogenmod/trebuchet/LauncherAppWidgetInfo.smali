.class Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;
.super Lcom/cyanogenmod/trebuchet/ItemInfo;
.source "LauncherAppWidgetInfo.java"


# static fields
.field static final NO_ID:I = -0x1


# instance fields
.field appWidgetId:I

.field hostView:Landroid/appwidget/AppWidgetHostView;

.field minHeight:I

.field minWidth:I

.field providerName:Landroid/content/ComponentName;


# direct methods
.method constructor <init>(I)V
    .locals 1
    .param p1    # I

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/ItemInfo;-><init>()V

    iput v0, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->appWidgetId:I

    iput v0, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->minWidth:I

    iput v0, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->minHeight:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->hostView:Landroid/appwidget/AppWidgetHostView;

    const/4 v0, 0x4

    iput v0, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->itemType:I

    iput p1, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->appWidgetId:I

    return-void
.end method

.method constructor <init>(Landroid/content/ComponentName;)V
    .locals 2
    .param p1    # Landroid/content/ComponentName;

    const/4 v1, -0x1

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/ItemInfo;-><init>()V

    iput v1, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->appWidgetId:I

    iput v1, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->minWidth:I

    iput v1, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->minHeight:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->hostView:Landroid/appwidget/AppWidgetHostView;

    const/4 v0, 0x4

    iput v0, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->itemType:I

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->providerName:Landroid/content/ComponentName;

    iput v1, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->spanX:I

    iput v1, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->spanY:I

    return-void
.end method


# virtual methods
.method onAddToDatabase(Landroid/content/ContentValues;)V
    .locals 2
    .param p1    # Landroid/content/ContentValues;

    invoke-super {p0, p1}, Lcom/cyanogenmod/trebuchet/ItemInfo;->onAddToDatabase(Landroid/content/ContentValues;)V

    const-string v0, "appWidgetId"

    iget v1, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->appWidgetId:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AppWidget(id="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->appWidgetId:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method unbind()V
    .locals 1

    invoke-super {p0}, Lcom/cyanogenmod/trebuchet/ItemInfo;->unbind()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;->hostView:Landroid/appwidget/AppWidgetHostView;

    return-void
.end method
