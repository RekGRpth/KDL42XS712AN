.class Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ShortcutsGridViewAdapter;
.super Lcom/cyanogenmod/trebuchet/VidgetAdapter$VidgetGridAdapter;
.source "SearchDropTargetBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ShortcutsGridViewAdapter"
.end annotation


# instance fields
.field private mHoverListener:Landroid/view/View$OnHoverListener;

.field final synthetic this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;


# direct methods
.method private constructor <init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;)V
    .locals 1

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ShortcutsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/VidgetAdapter$VidgetGridAdapter;-><init>(Lcom/cyanogenmod/trebuchet/VidgetAdapter;)V

    new-instance v0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ShortcutsGridViewAdapter$1;

    invoke-direct {v0, p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ShortcutsGridViewAdapter$1;-><init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ShortcutsGridViewAdapter;)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ShortcutsGridViewAdapter;->mHoverListener:Landroid/view/View$OnHoverListener;

    return-void
.end method

.method synthetic constructor <init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ShortcutsGridViewAdapter;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ShortcutsGridViewAdapter;-><init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;)V

    return-void
.end method


# virtual methods
.method public getItem(I)Ljava/lang/Object;
    .locals 2
    .param p1    # I

    iget v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ShortcutsGridViewAdapter;->mCount:I

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ShortcutsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;->access$0(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;)Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    move-result-object v0

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mShortcuts:Ljava/util/List;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->access$5(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)Ljava/util/List;

    move-result-object v0

    iget v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ShortcutsGridViewAdapter;->mStartIdx:I

    add-int/2addr v1, p1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v5, 0x0

    iget v4, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ShortcutsGridViewAdapter;->mStartIdx:I

    add-int/2addr v4, p1

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ShortcutsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;
    invoke-static {v6}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;->access$0(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;)Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    move-result-object v6

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mShortcuts:Ljava/util/List;
    invoke-static {v6}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->access$5(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-lt v4, v6, :cond_0

    move-object v4, v5

    :goto_0
    return-object v4

    :cond_0
    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ShortcutsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;
    invoke-static {v4}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;->access$0(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;)Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    move-result-object v4

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mShortcuts:Ljava/util/List;
    invoke-static {v4}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->access$5(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)Ljava/util/List;

    move-result-object v4

    iget v6, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ShortcutsGridViewAdapter;->mStartIdx:I

    add-int/2addr v6, p1

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    if-nez p2, :cond_1

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ShortcutsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;

    iget-object v4, v4, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v6, 0x7f03001e    # com.konka.avenger.R.layout.paged_dialog_widget_preview

    const/4 v7, 0x0

    invoke-virtual {v4, v6, v5, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    check-cast p2, Lcom/cyanogenmod/trebuchet/PagedViewWidget;

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ShortcutsGridViewAdapter;->mHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {p2, v4}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    new-instance v3, Landroid/widget/AbsListView$LayoutParams;

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ShortcutsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;

    iget v4, v4, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;->mVidgetHSpan:I

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ShortcutsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;

    iget v6, v6, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;->mVidgetCellWidth:I

    mul-int/2addr v4, v6

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ShortcutsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;

    iget v6, v6, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;->mVidgetVSpan:I

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ShortcutsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;

    iget v7, v7, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;->mVidgetCellHeight:I

    mul-int/2addr v6, v7

    invoke-direct {v3, v4, v6}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    move-object v4, p2

    check-cast v4, Lcom/cyanogenmod/trebuchet/PagedViewWidget;

    invoke-virtual {v4, v3}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ViewHolder;

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ShortcutsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;

    invoke-direct {v0, v4, v5}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ViewHolder;-><init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ViewHolder;)V

    new-instance v4, Lcom/cyanogenmod/trebuchet/PendingAddItemInfo;

    invoke-direct {v4}, Lcom/cyanogenmod/trebuchet/PendingAddItemInfo;-><init>()V

    iput-object v4, v0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ViewHolder;->mInfo:Lcom/cyanogenmod/trebuchet/PendingAddItemInfo;

    const v4, 0x7f0d0020    # com.konka.avenger.R.id.widget_name

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ViewHolder;->mNameView:Landroid/widget/TextView;

    const v4, 0x7f0d0021    # com.konka.avenger.R.id.widget_dims

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, v0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ViewHolder;->mDimsView:Landroid/widget/TextView;

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ViewHolder;

    iget-object v2, v0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ViewHolder;->mInfo:Lcom/cyanogenmod/trebuchet/PendingAddItemInfo;

    const/4 v4, 0x1

    iput v4, v2, Lcom/cyanogenmod/trebuchet/PendingAddItemInfo;->itemType:I

    new-instance v4, Landroid/content/ComponentName;

    iget-object v5, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v6, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v6, v6, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v4, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v4, v2, Lcom/cyanogenmod/trebuchet/PendingAddItemInfo;->componentName:Landroid/content/ComponentName;

    move-object v4, p2

    check-cast v4, Lcom/cyanogenmod/trebuchet/PagedViewWidget;

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ShortcutsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;

    iget-object v5, v5, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;->mPackageManager:Landroid/content/pm/PackageManager;

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ShortcutsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;

    iget-object v6, v6, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;->mHolographicOutlineHelper:Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;

    invoke-virtual {v4, v5, v1, v6}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->applyFromResolveInfo(Landroid/content/pm/PackageManager;Landroid/content/pm/ResolveInfo;Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;)V

    move-object v4, p2

    goto/16 :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ViewHolder;

    iget-object v2, v1, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ViewHolder;->mInfo:Lcom/cyanogenmod/trebuchet/PendingAddItemInfo;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ShortcutsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;

    iget-object v3, v3, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Launcher;->getWorkspace()Lcom/cyanogenmod/trebuchet/Workspace;

    move-result-object v3

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/Workspace;->getCurrentDropLayout()Lcom/cyanogenmod/trebuchet/CellLayout;

    move-result-object v0

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ShortcutsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;->access$0(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;)Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    move-result-object v3

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mShortcutsDialog:Lgreendroid/widget/PagedDialog;
    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->access$6(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)Lgreendroid/widget/PagedDialog;

    move-result-object v3

    invoke-virtual {v3}, Lgreendroid/widget/PagedDialog;->dismiss()V

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter$ShortcutsGridViewAdapter;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;

    iget-object v3, v3, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$ShortcutsAdapter;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v3, v2, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->addExternalItemToScreen(Lcom/cyanogenmod/trebuchet/ItemInfo;Lcom/cyanogenmod/trebuchet/CellLayout;)V

    return-void
.end method
