.class Lcom/cyanogenmod/trebuchet/CellLayout$7;
.super Landroid/animation/AnimatorListenerAdapter;
.source "CellLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cyanogenmod/trebuchet/CellLayout;->animateChildToPosition(Landroid/view/View;IIII)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field cancelled:Z

.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/CellLayout;

.field private final synthetic val$lp:Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;


# direct methods
.method constructor <init>(Lcom/cyanogenmod/trebuchet/CellLayout;Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;)V
    .locals 1

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/CellLayout$7;->this$0:Lcom/cyanogenmod/trebuchet/CellLayout;

    iput-object p2, p0, Lcom/cyanogenmod/trebuchet/CellLayout$7;->val$lp:Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout$7;->cancelled:Z

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1
    .param p1    # Landroid/animation/Animator;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout$7;->cancelled:Z

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1    # Landroid/animation/Animator;

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout$7;->cancelled:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout$7;->val$lp:Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->isLockedToGrid:Z

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout$7;->this$0:Lcom/cyanogenmod/trebuchet/CellLayout;

    # getter for: Lcom/cyanogenmod/trebuchet/CellLayout;->mReorderAnimators:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->access$4(Lcom/cyanogenmod/trebuchet/CellLayout;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout$7;->val$lp:Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/CellLayout$7;->this$0:Lcom/cyanogenmod/trebuchet/CellLayout;

    # getter for: Lcom/cyanogenmod/trebuchet/CellLayout;->mReorderAnimators:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/CellLayout;->access$4(Lcom/cyanogenmod/trebuchet/CellLayout;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/CellLayout$7;->val$lp:Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method
