.class public Lcom/cyanogenmod/trebuchet/FocusHelper;
.super Ljava/lang/Object;
.source "FocusHelper.java"


# static fields
.field private static final DEBUG:Z = true

.field private static final TAG:Ljava/lang/String; = "focushelper"

.field private static mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static findIndexOfIcon(Ljava/util/ArrayList;II)Landroid/view/View;
    .locals 4
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;II)",
            "Landroid/view/View;"
        }
    .end annotation

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int v1, p1, p2

    :goto_0
    if-ltz v1, :cond_0

    if-lt v1, v0, :cond_2

    :cond_0
    const/4 v2, 0x0

    :cond_1
    return-object v2

    :cond_2
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    instance-of v3, v2, Lcom/cyanogenmod/trebuchet/BubbleTextView;

    if-nez v3, :cond_1

    instance-of v3, v2, Lcom/cyanogenmod/trebuchet/FolderIcon;

    if-nez v3, :cond_1

    instance-of v3, v2, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;

    if-nez v3, :cond_1

    add-int/2addr v1, p2

    goto :goto_0
.end method

.method private static findTabHostParent(Landroid/view/View;)Landroid/widget/TabHost;
    .locals 2
    .param p0    # Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/widget/TabHost;

    if-eqz v1, :cond_1

    :cond_0
    check-cast v0, Landroid/widget/TabHost;

    return-object v0

    :cond_1
    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_0
.end method

.method private static getAppsCustomizePage(Landroid/view/ViewGroup;I)Landroid/view/ViewGroup;
    .locals 2
    .param p0    # Landroid/view/ViewGroup;
    .param p1    # I

    check-cast p0, Lcom/cyanogenmod/trebuchet/PagedView;

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/PagedView;->getPageAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    instance-of v1, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    :cond_0
    return-object v0
.end method

.method private static getCellLayoutChildrenForIndex(Landroid/view/ViewGroup;I)Lcom/cyanogenmod/trebuchet/CellLayoutChildren;
    .locals 2
    .param p0    # Landroid/view/ViewGroup;
    .param p1    # I

    invoke-virtual {p0, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    return-object v1
.end method

.method private static getCellLayoutChildrenSortedSpatially(Lcom/cyanogenmod/trebuchet/CellLayout;Landroid/view/ViewGroup;)Ljava/util/ArrayList;
    .locals 5
    .param p0    # Lcom/cyanogenmod/trebuchet/CellLayout;
    .param p1    # Landroid/view/ViewGroup;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/cyanogenmod/trebuchet/CellLayout;",
            "Landroid/view/ViewGroup;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getCountX()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v1, :cond_0

    new-instance v4, Lcom/cyanogenmod/trebuchet/FocusHelper$1;

    invoke-direct {v4, v0}, Lcom/cyanogenmod/trebuchet/FocusHelper$1;-><init>(I)V

    invoke-static {v3, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-object v3

    :cond_0
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private static getClosestIconOnLineH(Lcom/cyanogenmod/trebuchet/CellLayout;Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;
    .locals 27
    .param p0    # Lcom/cyanogenmod/trebuchet/CellLayout;
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/view/View;
    .param p3    # I

    invoke-static/range {p0 .. p1}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getCellLayoutChildrenSortedSpatially(Lcom/cyanogenmod/trebuchet/CellLayout;Landroid/view/ViewGroup;)Ljava/util/ArrayList;

    move-result-object v20

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v12

    check-cast v12, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getCountX()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getCountY()I

    move-result v3

    if-gez p3, :cond_0

    iget v6, v12, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellX:I

    :goto_0
    add-int v13, v6, p3

    if-ltz v13, :cond_b

    if-ge v13, v2, :cond_b

    const v4, 0x7f7fffff    # Float.MAX_VALUE

    const/4 v5, -0x1

    move-object/from16 v0, v20

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v9

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v10

    if-gez p3, :cond_1

    iget v0, v12, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellX:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v7, v0

    :goto_1
    iget v0, v12, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellY:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-double v0, v0

    move-wide/from16 v21, v0

    iget v0, v12, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellVSpan:I

    move/from16 v23, v0

    add-int/lit8 v23, v23, -0x1

    move/from16 v0, v23

    int-to-double v0, v0

    move-wide/from16 v23, v0

    const-wide/high16 v25, 0x4000000000000000L    # 2.0

    div-double v23, v23, v25

    add-double v21, v21, v23

    move-wide/from16 v0, v21

    double-to-float v8, v0

    const/4 v11, 0x0

    :goto_2
    if-lt v11, v10, :cond_2

    const/16 v21, -0x1

    move/from16 v0, v21

    if-le v5, v0, :cond_b

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Landroid/view/View;

    :goto_3
    return-object v21

    :cond_0
    iget v0, v12, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellX:I

    move/from16 v21, v0

    iget v0, v12, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellHSpan:I

    move/from16 v22, v0

    add-int v21, v21, v22

    add-int/lit8 v6, v21, -0x1

    goto :goto_0

    :cond_1
    iget v0, v12, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellX:I

    move/from16 v21, v0

    iget v0, v12, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellHSpan:I

    move/from16 v22, v0

    add-int v21, v21, v22

    add-int/lit8 v21, v21, -0x1

    move/from16 v0, v21

    int-to-float v7, v0

    goto :goto_1

    :cond_2
    if-ne v9, v11, :cond_4

    :cond_3
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    :cond_4
    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/view/View;

    invoke-virtual {v14}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v19

    check-cast v19, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;

    if-gez p3, :cond_8

    move-object/from16 v0, v19

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellX:I

    move/from16 v21, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellHSpan:I

    move/from16 v22, v0

    add-int v21, v21, v22

    add-int/lit8 v21, v21, -0x1

    move/from16 v0, v21

    if-ge v0, v6, :cond_7

    const/4 v15, 0x1

    :goto_4
    if-eqz v15, :cond_3

    instance-of v0, v14, Lcom/cyanogenmod/trebuchet/BubbleTextView;

    move/from16 v21, v0

    if-nez v21, :cond_5

    instance-of v0, v14, Lcom/cyanogenmod/trebuchet/FolderIcon;

    move/from16 v21, v0

    if-nez v21, :cond_5

    instance-of v0, v14, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;

    move/from16 v21, v0

    if-eqz v21, :cond_3

    :cond_5
    if-gez p3, :cond_a

    move-object/from16 v0, v19

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellHSpan:I

    move/from16 v21, v0

    add-int/lit8 v16, v21, -0x1

    :goto_5
    const/16 v17, 0x0

    :goto_6
    move-object/from16 v0, v19

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellVSpan:I

    move/from16 v21, v0

    move/from16 v0, v17

    move/from16 v1, v21

    if-ge v0, v1, :cond_3

    move-object/from16 v0, v19

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellX:I

    move/from16 v21, v0

    add-int v21, v21, v16

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    sub-float v21, v21, v7

    move/from16 v0, v21

    float-to-double v0, v0

    move-wide/from16 v21, v0

    const-wide/high16 v23, 0x4000000000000000L    # 2.0

    invoke-static/range {v21 .. v24}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v21

    move-object/from16 v0, v19

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellY:I

    move/from16 v23, v0

    add-int v23, v23, v17

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    sub-float v23, v23, v8

    move/from16 v0, v23

    float-to-double v0, v0

    move-wide/from16 v23, v0

    const-wide/high16 v25, 0x4000000000000000L    # 2.0

    invoke-static/range {v23 .. v26}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v23

    add-double v21, v21, v23

    invoke-static/range {v21 .. v22}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v21

    move-wide/from16 v0, v21

    double-to-float v0, v0

    move/from16 v18, v0

    cmpg-float v21, v18, v4

    if-gez v21, :cond_6

    move v5, v11

    move/from16 v4, v18

    :cond_6
    add-int/lit8 v17, v17, 0x1

    goto :goto_6

    :cond_7
    const/4 v15, 0x0

    goto :goto_4

    :cond_8
    move-object/from16 v0, v19

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellX:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-le v0, v6, :cond_9

    const/4 v15, 0x1

    goto/16 :goto_4

    :cond_9
    const/4 v15, 0x0

    goto/16 :goto_4

    :cond_a
    const/16 v16, 0x0

    goto :goto_5

    :cond_b
    const/16 v21, 0x0

    goto/16 :goto_3
.end method

.method private static getClosestIconOnLineV(Lcom/cyanogenmod/trebuchet/CellLayout;Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;
    .locals 27
    .param p0    # Lcom/cyanogenmod/trebuchet/CellLayout;
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/view/View;
    .param p3    # I

    invoke-static/range {p0 .. p1}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getCellLayoutChildrenSortedSpatially(Lcom/cyanogenmod/trebuchet/CellLayout;Landroid/view/ViewGroup;)Ljava/util/ArrayList;

    move-result-object v20

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    check-cast v11, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getCountX()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/CellLayout;->getCountY()I

    move-result v3

    if-gez p3, :cond_0

    iget v14, v11, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellY:I

    :goto_0
    add-int v12, v14, p3

    if-ltz v12, :cond_b

    if-ge v12, v3, :cond_b

    const v4, 0x7f7fffff    # Float.MAX_VALUE

    const/4 v5, -0x1

    move-object/from16 v0, v20

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v8

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v9

    iget v0, v11, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellX:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-double v0, v0

    move-wide/from16 v21, v0

    iget v0, v11, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellHSpan:I

    move/from16 v23, v0

    add-int/lit8 v23, v23, -0x1

    move/from16 v0, v23

    int-to-double v0, v0

    move-wide/from16 v23, v0

    const-wide/high16 v25, 0x4000000000000000L    # 2.0

    div-double v23, v23, v25

    add-double v21, v21, v23

    move-wide/from16 v0, v21

    double-to-float v6, v0

    if-gez p3, :cond_1

    iget v0, v11, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellY:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v7, v0

    :goto_1
    const/4 v10, 0x0

    :goto_2
    if-lt v10, v9, :cond_2

    const/16 v21, -0x1

    move/from16 v0, v21

    if-le v5, v0, :cond_b

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Landroid/view/View;

    :goto_3
    return-object v21

    :cond_0
    iget v0, v11, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellY:I

    move/from16 v21, v0

    iget v0, v11, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellVSpan:I

    move/from16 v22, v0

    add-int v21, v21, v22

    add-int/lit8 v14, v21, -0x1

    goto :goto_0

    :cond_1
    iget v0, v11, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellY:I

    move/from16 v21, v0

    iget v0, v11, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellVSpan:I

    move/from16 v22, v0

    add-int v21, v21, v22

    add-int/lit8 v21, v21, -0x1

    move/from16 v0, v21

    int-to-float v7, v0

    goto :goto_1

    :cond_2
    if-ne v8, v10, :cond_4

    :cond_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    :cond_4
    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/view/View;

    invoke-virtual {v13}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v19

    check-cast v19, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;

    if-gez p3, :cond_8

    move-object/from16 v0, v19

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellY:I

    move/from16 v21, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellVSpan:I

    move/from16 v22, v0

    add-int v21, v21, v22

    add-int/lit8 v21, v21, -0x1

    move/from16 v0, v21

    if-ge v0, v14, :cond_7

    const/4 v15, 0x1

    :goto_4
    if-eqz v15, :cond_3

    instance-of v0, v13, Lcom/cyanogenmod/trebuchet/BubbleTextView;

    move/from16 v21, v0

    if-nez v21, :cond_5

    instance-of v0, v13, Lcom/cyanogenmod/trebuchet/FolderIcon;

    move/from16 v21, v0

    if-nez v21, :cond_5

    instance-of v0, v13, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;

    move/from16 v21, v0

    if-eqz v21, :cond_3

    :cond_5
    if-gez p3, :cond_a

    move-object/from16 v0, v19

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellVSpan:I

    move/from16 v21, v0

    add-int/lit8 v17, v21, -0x1

    :goto_5
    const/16 v16, 0x0

    :goto_6
    move-object/from16 v0, v19

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellHSpan:I

    move/from16 v21, v0

    move/from16 v0, v16

    move/from16 v1, v21

    if-ge v0, v1, :cond_3

    move-object/from16 v0, v19

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellX:I

    move/from16 v21, v0

    add-int v21, v21, v16

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    sub-float v21, v21, v6

    move/from16 v0, v21

    float-to-double v0, v0

    move-wide/from16 v21, v0

    const-wide/high16 v23, 0x4000000000000000L    # 2.0

    invoke-static/range {v21 .. v24}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v21

    move-object/from16 v0, v19

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellY:I

    move/from16 v23, v0

    add-int v23, v23, v17

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    sub-float v23, v23, v7

    move/from16 v0, v23

    float-to-double v0, v0

    move-wide/from16 v23, v0

    const-wide/high16 v25, 0x4000000000000000L    # 2.0

    invoke-static/range {v23 .. v26}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v23

    add-double v21, v21, v23

    invoke-static/range {v21 .. v22}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v21

    move-wide/from16 v0, v21

    double-to-float v0, v0

    move/from16 v18, v0

    cmpg-float v21, v18, v4

    if-gez v21, :cond_6

    move v5, v10

    move/from16 v4, v18

    :cond_6
    add-int/lit8 v16, v16, 0x1

    goto :goto_6

    :cond_7
    const/4 v15, 0x0

    goto :goto_4

    :cond_8
    move-object/from16 v0, v19

    iget v0, v0, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->cellY:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-le v0, v14, :cond_9

    const/4 v15, 0x1

    goto/16 :goto_4

    :cond_9
    const/4 v15, 0x0

    goto/16 :goto_4

    :cond_a
    const/16 v17, 0x0

    goto :goto_5

    :cond_b
    const/16 v21, 0x0

    goto/16 :goto_3
.end method

.method private static getIconInDirection(Lcom/cyanogenmod/trebuchet/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;
    .locals 2
    .param p0    # Lcom/cyanogenmod/trebuchet/CellLayout;
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # I
    .param p3    # I

    invoke-static {p0, p1}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getCellLayoutChildrenSortedSpatially(Lcom/cyanogenmod/trebuchet/CellLayout;Landroid/view/ViewGroup;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0, p2, p3}, Lcom/cyanogenmod/trebuchet/FocusHelper;->findIndexOfIcon(Ljava/util/ArrayList;II)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method private static getIconInDirection(Lcom/cyanogenmod/trebuchet/CellLayout;Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;
    .locals 2
    .param p0    # Lcom/cyanogenmod/trebuchet/CellLayout;
    .param p1    # Landroid/view/ViewGroup;
    .param p2    # Landroid/view/View;
    .param p3    # I

    invoke-static {p0, p1}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getCellLayoutChildrenSortedSpatially(Lcom/cyanogenmod/trebuchet/CellLayout;Landroid/view/ViewGroup;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v0, v1, p3}, Lcom/cyanogenmod/trebuchet/FocusHelper;->findIndexOfIcon(Ljava/util/ArrayList;II)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method static handleAddGuideKeyEvent(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 12
    .param p0    # Landroid/view/View;
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v9, 0x1

    const/4 v11, -0x1

    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v10

    invoke-interface {v10}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildrenLayout()Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    move-result-object v6

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v8

    check-cast v8, Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v8, v2}, Lcom/cyanogenmod/trebuchet/Workspace;->indexOfChild(Landroid/view/View;)I

    move-result v5

    invoke-virtual {v8}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildCount()I

    move-result v4

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-eq v0, v9, :cond_0

    move v1, v9

    :goto_0
    const/4 v7, 0x0

    sparse-switch p1, :sswitch_data_0

    :goto_1
    return v7

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :sswitch_0
    if-eqz v1, :cond_2

    if-lez v5, :cond_5

    add-int/lit8 v10, v5, -0x1

    invoke-static {v8, v10}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getCellLayoutChildrenForIndex(Landroid/view/ViewGroup;I)Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    move-result-object v6

    const/4 v3, 0x0

    const/16 v10, 0x5c

    if-ne p1, v10, :cond_3

    invoke-static {v2, v6, v11, v9}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getIconInDirection(Lcom/cyanogenmod/trebuchet/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v3

    :cond_1
    :goto_2
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Landroid/view/View;->requestFocus()Z

    :cond_2
    :goto_3
    const/4 v7, 0x1

    goto :goto_1

    :cond_3
    const/16 v9, 0x15

    if-ne p1, v9, :cond_1

    invoke-virtual {v6}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->getChildCount()I

    move-result v9

    invoke-static {v2, v6, v9, v11}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getIconInDirection(Lcom/cyanogenmod/trebuchet/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v3

    goto :goto_2

    :cond_4
    add-int/lit8 v9, v5, -0x1

    invoke-virtual {v8, v9}, Lcom/cyanogenmod/trebuchet/Workspace;->snapToPage(I)V

    add-int/lit8 v9, v5, -0x1

    invoke-virtual {v8, v9}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/View;->requestFocus()Z

    goto :goto_3

    :cond_5
    invoke-static {v2, v6, v11, v9}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getIconInDirection(Lcom/cyanogenmod/trebuchet/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Landroid/view/View;->requestFocus()Z

    goto :goto_3

    :sswitch_1
    if-eqz v1, :cond_6

    add-int/lit8 v10, v4, -0x1

    if-ge v5, v10, :cond_8

    add-int/lit8 v10, v5, 0x1

    invoke-static {v8, v10}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getCellLayoutChildrenForIndex(Landroid/view/ViewGroup;I)Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    move-result-object v6

    invoke-static {v2, v6, v11, v9}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getIconInDirection(Lcom/cyanogenmod/trebuchet/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_7

    invoke-virtual {v3}, Landroid/view/View;->requestFocus()Z

    :cond_6
    :goto_4
    const/4 v7, 0x1

    goto :goto_1

    :cond_7
    add-int/lit8 v9, v5, 0x1

    invoke-virtual {v8, v9}, Lcom/cyanogenmod/trebuchet/Workspace;->snapToPage(I)V

    add-int/lit8 v9, v5, 0x1

    invoke-virtual {v8, v9}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/View;->requestFocus()Z

    goto :goto_4

    :cond_8
    invoke-virtual {v6}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->getChildCount()I

    move-result v9

    invoke-static {v2, v6, v9, v11}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getIconInDirection(Lcom/cyanogenmod/trebuchet/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {v3}, Landroid/view/View;->requestFocus()Z

    goto :goto_4

    :sswitch_data_0
    .sparse-switch
        0x15 -> :sswitch_0
        0x16 -> :sswitch_1
        0x5c -> :sswitch_0
        0x5d -> :sswitch_1
    .end sparse-switch
.end method

.method static handleAppsCustomizeKeyEvent(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 24
    .param p0    # Landroid/view/View;
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v22

    move-object/from16 v0, v22

    instance-of v0, v0, Lcom/cyanogenmod/trebuchet/PagedViewCellLayoutChildren;

    move/from16 v22, v0

    if-eqz v22, :cond_0

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v11

    check-cast v11, Landroid/view/ViewGroup;

    invoke-virtual {v11}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v17

    check-cast v17, Landroid/view/ViewGroup;

    move-object/from16 v22, v17

    check-cast v22, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;

    invoke-virtual/range {v22 .. v22}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->getCellCountX()I

    move-result v7

    move-object/from16 v22, v17

    check-cast v22, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;

    invoke-virtual/range {v22 .. v22}, Lcom/cyanogenmod/trebuchet/PagedViewCellLayout;->getCellCountY()I

    move-result v8

    :goto_0
    invoke-virtual/range {v17 .. v17}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v6

    check-cast v6, Lcom/cyanogenmod/trebuchet/PagedView;

    invoke-static {v6}, Lcom/cyanogenmod/trebuchet/FocusHelper;->findTabHostParent(Landroid/view/View;)Landroid/widget/TabHost;

    move-result-object v18

    move-object/from16 v0, p0

    invoke-virtual {v11, v0}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v10

    invoke-virtual {v11}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v12

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->indexOfChild(Landroid/view/View;)I

    move-result v22

    move/from16 v0, v22

    invoke-virtual {v6, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->indexToPage(I)I

    move-result v16

    invoke-virtual {v6}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v15

    rem-int v20, v10, v7

    div-int v21, v10, v7

    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    const/16 v22, 0x1

    move/from16 v0, v22

    if-eq v2, v0, :cond_1

    const/4 v9, 0x1

    :goto_1
    const/16 v19, 0x0

    sparse-switch p1, :sswitch_data_0

    :goto_2
    return v19

    :cond_0
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v17

    check-cast v17, Landroid/view/ViewGroup;

    move-object/from16 v11, v17

    move-object/from16 v22, v17

    check-cast v22, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;

    invoke-virtual/range {v22 .. v22}, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;->getCellCountX()I

    move-result v7

    move-object/from16 v22, v17

    check-cast v22, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;

    invoke-virtual/range {v22 .. v22}, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;->getCellCountY()I

    move-result v8

    goto :goto_0

    :cond_1
    const/4 v9, 0x0

    goto :goto_1

    :sswitch_0
    if-eqz v9, :cond_2

    if-lez v10, :cond_3

    if-lez v20, :cond_3

    add-int/lit8 v22, v10, -0x1

    move/from16 v0, v22

    invoke-virtual {v11, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->requestFocus()Z

    :cond_2
    :goto_3
    const/16 v19, 0x1

    goto :goto_2

    :cond_3
    if-lez v16, :cond_2

    add-int/lit8 v22, v16, -0x1

    move/from16 v0, v22

    invoke-static {v6, v0}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getAppsCustomizePage(Landroid/view/ViewGroup;I)Landroid/view/ViewGroup;

    move-result-object v13

    if-eqz v13, :cond_2

    add-int/lit8 v22, v16, -0x1

    move/from16 v0, v22

    invoke-virtual {v6, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->snapToPage(I)V

    invoke-virtual {v13}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v22

    add-int/lit8 v22, v22, -0x1

    move/from16 v0, v22

    invoke-virtual {v13, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    goto :goto_3

    :sswitch_1
    if-eqz v9, :cond_4

    add-int/lit8 v22, v12, -0x1

    move/from16 v0, v22

    if-ge v10, v0, :cond_5

    add-int/lit8 v22, v7, -0x1

    move/from16 v0, v20

    move/from16 v1, v22

    if-ge v0, v1, :cond_5

    add-int/lit8 v22, v10, 0x1

    move/from16 v0, v22

    invoke-virtual {v11, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->requestFocus()Z

    :cond_4
    :goto_4
    const/16 v19, 0x1

    goto :goto_2

    :cond_5
    add-int/lit8 v22, v15, -0x1

    move/from16 v0, v16

    move/from16 v1, v22

    if-ge v0, v1, :cond_4

    add-int/lit8 v22, v16, 0x1

    move/from16 v0, v22

    invoke-static {v6, v0}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getAppsCustomizePage(Landroid/view/ViewGroup;I)Landroid/view/ViewGroup;

    move-result-object v13

    if-eqz v13, :cond_4

    add-int/lit8 v22, v16, 0x1

    move/from16 v0, v22

    invoke-virtual {v6, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->snapToPage(I)V

    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v13, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    goto :goto_4

    :sswitch_2
    if-eqz v9, :cond_6

    if-lez v21, :cond_7

    add-int/lit8 v22, v21, -0x1

    mul-int v22, v22, v7

    add-int v14, v22, v20

    invoke-virtual {v11, v14}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->requestFocus()Z

    :cond_6
    :goto_5
    const/16 v19, 0x1

    goto/16 :goto_2

    :cond_7
    const v22, 0x7f0d0018    # com.konka.avenger.R.id.applist_latest

    move-object/from16 v0, v18

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->requestFocus()Z

    goto :goto_5

    :sswitch_3
    if-eqz v9, :cond_8

    add-int/lit8 v22, v8, -0x1

    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_8

    add-int/lit8 v22, v12, -0x1

    add-int/lit8 v23, v21, 0x1

    mul-int v23, v23, v7

    add-int v23, v23, v20

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->min(II)I

    move-result v14

    invoke-virtual {v11, v14}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->requestFocus()Z

    :cond_8
    const/16 v19, 0x1

    goto/16 :goto_2

    :sswitch_4
    if-eqz v9, :cond_9

    move-object v5, v6

    check-cast v5, Landroid/view/View$OnClickListener;

    move-object/from16 v0, p0

    invoke-interface {v5, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    :cond_9
    const/16 v19, 0x1

    goto/16 :goto_2

    :sswitch_5
    if-eqz v9, :cond_a

    if-lez v16, :cond_b

    add-int/lit8 v22, v16, -0x1

    move/from16 v0, v22

    invoke-static {v6, v0}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getAppsCustomizePage(Landroid/view/ViewGroup;I)Landroid/view/ViewGroup;

    move-result-object v13

    if-eqz v13, :cond_a

    add-int/lit8 v22, v16, -0x1

    move/from16 v0, v22

    invoke-virtual {v6, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->snapToPage(I)V

    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v13, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_a

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    :cond_a
    :goto_6
    const/16 v19, 0x1

    goto/16 :goto_2

    :cond_b
    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v11, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->requestFocus()Z

    goto :goto_6

    :sswitch_6
    if-eqz v9, :cond_c

    add-int/lit8 v22, v15, -0x1

    move/from16 v0, v16

    move/from16 v1, v22

    if-ge v0, v1, :cond_d

    add-int/lit8 v22, v16, 0x1

    move/from16 v0, v22

    invoke-static {v6, v0}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getAppsCustomizePage(Landroid/view/ViewGroup;I)Landroid/view/ViewGroup;

    move-result-object v13

    if-eqz v13, :cond_c

    add-int/lit8 v22, v16, 0x1

    move/from16 v0, v22

    invoke-virtual {v6, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->snapToPage(I)V

    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v13, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    if-eqz v4, :cond_c

    invoke-virtual {v4}, Landroid/view/View;->requestFocus()Z

    :cond_c
    :goto_7
    const/16 v19, 0x1

    goto/16 :goto_2

    :cond_d
    add-int/lit8 v22, v12, -0x1

    move/from16 v0, v22

    invoke-virtual {v11, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->requestFocus()Z

    goto :goto_7

    :sswitch_7
    if-eqz v9, :cond_e

    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v11, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->requestFocus()Z

    :cond_e
    const/16 v19, 0x1

    goto/16 :goto_2

    :sswitch_8
    if-eqz v9, :cond_f

    add-int/lit8 v22, v12, -0x1

    move/from16 v0, v22

    invoke-virtual {v11, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Landroid/view/View;->requestFocus()Z

    :cond_f
    const/16 v19, 0x1

    goto/16 :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_2
        0x14 -> :sswitch_3
        0x15 -> :sswitch_0
        0x16 -> :sswitch_1
        0x17 -> :sswitch_4
        0x42 -> :sswitch_4
        0x5c -> :sswitch_5
        0x5d -> :sswitch_6
        0x7a -> :sswitch_7
        0x7b -> :sswitch_8
    .end sparse-switch
.end method

.method static handleAppsCustomizeTabKeyEvent(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 7
    .param p0    # Landroid/view/View;
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v3, 0x1

    invoke-static {p0}, Lcom/cyanogenmod/trebuchet/FocusHelper;->findTabHostParent(Landroid/view/View;)Landroid/widget/TabHost;

    move-result-object v4

    const v6, 0x1020011    # android.R.id.tabcontent

    invoke-virtual {v4, v6}, Landroid/widget/TabHost;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    const v6, 0x7f0d0018    # com.konka.avenger.R.id.applist_latest

    invoke-virtual {v4, v6}, Landroid/widget/TabHost;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-eq v0, v3, :cond_1

    :goto_0
    const/4 v5, 0x0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    return v5

    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    :pswitch_1
    if-eqz v3, :cond_0

    const/4 v5, 0x1

    goto :goto_1

    :pswitch_2
    if-eqz v3, :cond_0

    const/4 v5, 0x1

    goto :goto_1

    :pswitch_3
    const/4 v5, 0x1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method static handleDragViewKeyEvent(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p0    # Landroid/view/View;
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v1, 0x1

    const-string v3, "focushelper"

    const-string v4, "###handleDragViewKeyEvent o"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-eq v0, v1, :cond_0

    :goto_0
    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_1
    return v2

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :pswitch_0
    if-eqz v1, :cond_1

    :cond_1
    const/4 v2, 0x1

    goto :goto_1

    :pswitch_1
    if-eqz v1, :cond_2

    :cond_2
    const/4 v2, 0x1

    goto :goto_1

    :pswitch_2
    if-eqz v1, :cond_3

    :cond_3
    const/4 v2, 0x1

    goto :goto_1

    :pswitch_3
    if-eqz v1, :cond_4

    :cond_4
    const/4 v2, 0x1

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static handleFolderKeyEvent(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 13
    .param p0    # Landroid/view/View;
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v12, -0x1

    const/4 v10, 0x1

    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v7

    check-cast v7, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    invoke-virtual {v7}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    check-cast v5, Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v11

    invoke-interface {v11}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v11

    invoke-interface {v11}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Lcom/cyanogenmod/trebuchet/Folder;

    iget-object v8, v3, Lcom/cyanogenmod/trebuchet/Folder;->mFolderName:Lcom/cyanogenmod/trebuchet/FolderEditText;

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-eq v0, v10, :cond_0

    move v4, v10

    :goto_0
    const/4 v9, 0x0

    sparse-switch p1, :sswitch_data_0

    :goto_1
    return v9

    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    :sswitch_0
    if-eqz v4, :cond_1

    invoke-static {v5, v7, p0, v12}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getClosestIconOnLineH(Lcom/cyanogenmod/trebuchet/CellLayout;Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {v6}, Landroid/view/View;->requestFocus()Z

    :cond_1
    const/4 v9, 0x1

    goto :goto_1

    :sswitch_1
    if-eqz v4, :cond_2

    invoke-static {v5, v7, p0, v10}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getClosestIconOnLineH(Lcom/cyanogenmod/trebuchet/CellLayout;Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-virtual {v6}, Landroid/view/View;->requestFocus()Z

    :cond_2
    :goto_2
    const/4 v9, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {v8}, Landroid/view/View;->requestFocus()Z

    goto :goto_2

    :sswitch_2
    if-eqz v4, :cond_4

    invoke-static {v5, v7, p0, v12}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getClosestIconOnLineV(Lcom/cyanogenmod/trebuchet/CellLayout;Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_4

    invoke-virtual {v6}, Landroid/view/View;->requestFocus()Z

    :cond_4
    const/4 v9, 0x1

    goto :goto_1

    :sswitch_3
    if-eqz v4, :cond_5

    invoke-static {v5, v7, p0, v10}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getClosestIconOnLineV(Lcom/cyanogenmod/trebuchet/CellLayout;Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_6

    invoke-virtual {v6}, Landroid/view/View;->requestFocus()Z

    :cond_5
    :goto_3
    const/4 v9, 0x1

    goto :goto_1

    :cond_6
    invoke-virtual {v8}, Landroid/view/View;->requestFocus()Z

    goto :goto_3

    :sswitch_4
    if-eqz v4, :cond_7

    invoke-static {v5, v7, v12, v10}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getIconInDirection(Lcom/cyanogenmod/trebuchet/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_7

    invoke-virtual {v6}, Landroid/view/View;->requestFocus()Z

    :cond_7
    const/4 v9, 0x1

    goto :goto_1

    :sswitch_5
    if-eqz v4, :cond_8

    invoke-virtual {v7}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->getChildCount()I

    move-result v10

    invoke-static {v5, v7, v10, v12}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getIconInDirection(Lcom/cyanogenmod/trebuchet/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_8

    invoke-virtual {v6}, Landroid/view/View;->requestFocus()Z

    :cond_8
    const/4 v9, 0x1

    goto :goto_1

    :sswitch_6
    if-eqz v4, :cond_9

    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v10

    invoke-interface {v10}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {p0}, Landroid/view/View;->getX()F

    move-result v10

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->getPaddingLeft()I

    move-result v11

    int-to-float v11, v11

    add-float/2addr v10, v11

    float-to-int v10, v10

    invoke-virtual {p0}, Landroid/view/View;->getY()F

    move-result v11

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->getPaddingTop()I

    move-result v12

    int-to-float v12, v12

    add-float/2addr v11, v12

    float-to-int v11, v11

    invoke-virtual {v2, v10, v11}, Lcom/cyanogenmod/trebuchet/CellLayout;->setTagToCellInfoForPoint(II)V

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/CellLayout;->getTag()Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    move-result-object v1

    const-string v11, "focushelper"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v10, "cellInfo"

    invoke-direct {v12, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez v1, :cond_a

    const-string v10, "=="

    :goto_4
    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v12, "NULL"

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v11, v10}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v1, :cond_9

    invoke-virtual {v3, v1}, Lcom/cyanogenmod/trebuchet/Folder;->popQuickActionMenu(Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;)V

    :cond_9
    const/4 v9, 0x1

    goto/16 :goto_1

    :cond_a
    const-string v10, "!="

    goto :goto_4

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_2
        0x14 -> :sswitch_3
        0x15 -> :sswitch_0
        0x16 -> :sswitch_1
        0x52 -> :sswitch_6
        0x7a -> :sswitch_4
        0x7b -> :sswitch_5
    .end sparse-switch
.end method

.method static handleHotseatButtonKeyEvent(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 14
    .param p0    # Landroid/view/View;
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v9

    check-cast v9, Landroid/view/ViewGroup;

    invoke-virtual {v9}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    const v12, 0x7f0d0033    # com.konka.avenger.R.id.workspace

    invoke-virtual {v5, v12}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual {v9, p0}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v2

    invoke-virtual {v9}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    invoke-virtual {v11}, Lcom/cyanogenmod/trebuchet/Workspace;->getCurrentPage()I

    move-result v8

    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v12, 0x1

    if-eq v0, v12, :cond_0

    const/4 v4, 0x1

    :goto_0
    const/4 v10, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_1
    return v10

    :cond_0
    const/4 v4, 0x0

    goto :goto_0

    :pswitch_0
    if-eqz v4, :cond_1

    if-lez v2, :cond_2

    add-int/lit8 v12, v2, -0x1

    invoke-virtual {v9, v12}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v12

    invoke-virtual {v12}, Landroid/view/View;->requestFocus()Z

    :cond_1
    :goto_2
    const/4 v10, 0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v12, v8, -0x1

    invoke-virtual {v11, v12}, Lcom/cyanogenmod/trebuchet/Workspace;->snapToPage(I)V

    goto :goto_2

    :pswitch_1
    if-eqz v4, :cond_3

    add-int/lit8 v12, v1, -0x1

    if-ge v2, v12, :cond_4

    add-int/lit8 v12, v2, 0x1

    invoke-virtual {v9, v12}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v12

    invoke-virtual {v12}, Landroid/view/View;->requestFocus()Z

    :cond_3
    :goto_3
    const/4 v10, 0x1

    goto :goto_1

    :cond_4
    add-int/lit8 v12, v8, 0x1

    invoke-virtual {v11, v12}, Lcom/cyanogenmod/trebuchet/Workspace;->snapToPage(I)V

    goto :goto_3

    :pswitch_2
    if-eqz v4, :cond_5

    invoke-virtual {v11, v8}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v6}, Lcom/cyanogenmod/trebuchet/CellLayout;->getChildrenLayout()Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    move-result-object v3

    const/4 v12, -0x1

    const/4 v13, 0x1

    invoke-static {v6, v3, v12, v13}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getIconInDirection(Lcom/cyanogenmod/trebuchet/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v7

    if-eqz v7, :cond_6

    invoke-virtual {v7}, Landroid/view/View;->requestFocus()Z

    :cond_5
    :goto_4
    const/4 v10, 0x1

    goto :goto_1

    :cond_6
    invoke-virtual {v11}, Lcom/cyanogenmod/trebuchet/Workspace;->requestFocus()Z

    goto :goto_4

    :pswitch_3
    const/4 v10, 0x1

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static handleIconKeyEvent(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 25
    .param p0    # Landroid/view/View;
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v15

    check-cast v15, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    invoke-virtual {v15}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->getParent()Landroid/view/ViewParent;

    move-result-object v11

    check-cast v11, Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual {v11}, Lcom/cyanogenmod/trebuchet/CellLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v18

    check-cast v18, Lcom/cyanogenmod/trebuchet/Workspace;

    invoke-virtual/range {v18 .. v18}, Lcom/cyanogenmod/trebuchet/Workspace;->getParent()Landroid/view/ViewParent;

    move-result-object v10

    check-cast v10, Landroid/view/ViewGroup;

    const v20, 0x7f0d0037    # com.konka.avenger.R.id.qsb_bar

    move/from16 v0, v20

    invoke-virtual {v10, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/view/ViewGroup;

    const v20, 0x7f0d0036    # com.konka.avenger.R.id.hotseat

    move/from16 v0, v20

    invoke-virtual {v10, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/view/ViewGroup;

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Lcom/cyanogenmod/trebuchet/Workspace;->indexOfChild(Landroid/view/View;)I

    move-result v14

    invoke-virtual/range {v18 .. v18}, Lcom/cyanogenmod/trebuchet/Workspace;->getChildCount()I

    move-result v13

    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    const/16 v20, 0x1

    move/from16 v0, v20

    if-eq v2, v0, :cond_1

    const/4 v7, 0x1

    :goto_0
    const/16 v17, 0x0

    sparse-switch p1, :sswitch_data_0

    :cond_0
    :goto_1
    move/from16 v20, v17

    :goto_2
    return v20

    :cond_1
    const/4 v7, 0x0

    goto :goto_0

    :sswitch_0
    if-eqz v7, :cond_2

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    check-cast v5, Lcom/cyanogenmod/trebuchet/CellLayout;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getX()F

    move-result v20

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->getPaddingLeft()I

    move-result v21

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    add-float v20, v20, v21

    move/from16 v0, v20

    float-to-double v0, v0

    move-wide/from16 v20, v0

    const-wide/high16 v22, 0x3fe0000000000000L    # 0.5

    add-double v20, v20, v22

    move-wide/from16 v0, v20

    double-to-int v0, v0

    move/from16 v20, v0

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getY()F

    move-result v21

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->getPaddingTop()I

    move-result v22

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    add-float v21, v21, v22

    move/from16 v0, v21

    float-to-double v0, v0

    move-wide/from16 v21, v0

    const-wide/high16 v23, 0x3fe0000000000000L    # 0.5

    add-double v21, v21, v23

    move-wide/from16 v0, v21

    double-to-int v0, v0

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Lcom/cyanogenmod/trebuchet/CellLayout;->setTagToCellInfoForPoint(II)V

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->getTag()Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;

    move-result-object v4

    const-string v21, "focushelper"

    new-instance v22, Ljava/lang/StringBuilder;

    const-string v20, "cellInfo"

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez v4, :cond_3

    const-string v20, "=="

    :goto_3
    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v22, "NULL"

    move-object/from16 v0, v20

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v4, :cond_2

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/CellLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v19

    check-cast v19, Lcom/cyanogenmod/trebuchet/Workspace;

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Lcom/cyanogenmod/trebuchet/Workspace;->popQuickActionMenu(Lcom/cyanogenmod/trebuchet/CellLayout$CellInfo;)V

    :cond_2
    const/16 v17, 0x1

    goto/16 :goto_1

    :cond_3
    const-string v20, "!="

    goto :goto_3

    :sswitch_1
    if-eqz v7, :cond_4

    const/16 v20, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v11, v15, v0, v1}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getClosestIconOnLineH(Lcom/cyanogenmod/trebuchet/CellLayout;Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v12

    if-eqz v12, :cond_5

    invoke-virtual {v12}, Landroid/view/View;->requestFocus()Z

    :cond_4
    :goto_4
    const/16 v17, 0x1

    goto/16 :goto_1

    :cond_5
    if-lez v14, :cond_4

    add-int/lit8 v20, v14, -0x1

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-static {v0, v1}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getCellLayoutChildrenForIndex(Landroid/view/ViewGroup;I)Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    move-result-object v15

    invoke-virtual {v15}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->getChildCount()I

    move-result v20

    const/16 v21, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v11, v15, v0, v1}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getIconInDirection(Lcom/cyanogenmod/trebuchet/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v12

    if-eqz v12, :cond_6

    invoke-virtual {v12}, Landroid/view/View;->requestFocus()Z

    goto :goto_4

    :cond_6
    add-int/lit8 v20, v14, -0x1

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->snapToPage(I)V

    add-int/lit8 v20, v14, -0x1

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->requestFocus()Z

    goto :goto_4

    :sswitch_2
    if-eqz v7, :cond_7

    const/16 v20, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v11, v15, v0, v1}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getClosestIconOnLineH(Lcom/cyanogenmod/trebuchet/CellLayout;Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v12

    if-eqz v12, :cond_8

    invoke-virtual {v12}, Landroid/view/View;->requestFocus()Z

    :cond_7
    :goto_5
    const/16 v17, 0x1

    goto/16 :goto_1

    :cond_8
    add-int/lit8 v20, v13, -0x1

    move/from16 v0, v20

    if-ge v14, v0, :cond_7

    add-int/lit8 v20, v14, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-static {v0, v1}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getCellLayoutChildrenForIndex(Landroid/view/ViewGroup;I)Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    move-result-object v15

    const/16 v20, -0x1

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v11, v15, v0, v1}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getIconInDirection(Lcom/cyanogenmod/trebuchet/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v12

    if-eqz v12, :cond_9

    invoke-virtual {v12}, Landroid/view/View;->requestFocus()Z

    goto :goto_5

    :cond_9
    add-int/lit8 v20, v14, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->snapToPage(I)V

    add-int/lit8 v20, v14, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->requestFocus()Z

    goto :goto_5

    :sswitch_3
    if-eqz v7, :cond_0

    const/16 v20, -0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v11, v15, v0, v1}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getClosestIconOnLineV(Lcom/cyanogenmod/trebuchet/CellLayout;Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v12

    if-eqz v12, :cond_a

    invoke-virtual {v12}, Landroid/view/View;->requestFocus()Z

    const/16 v17, 0x1

    goto/16 :goto_1

    :cond_a
    invoke-virtual/range {v16 .. v16}, Landroid/view/ViewGroup;->requestFocus()Z

    goto/16 :goto_1

    :sswitch_4
    if-eqz v7, :cond_0

    const/16 v20, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v11, v15, v0, v1}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getClosestIconOnLineV(Lcom/cyanogenmod/trebuchet/CellLayout;Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v12

    if-eqz v12, :cond_b

    invoke-virtual {v12}, Landroid/view/View;->requestFocus()Z

    const/16 v17, 0x1

    goto/16 :goto_1

    :cond_b
    if-eqz v8, :cond_0

    invoke-virtual {v8}, Landroid/view/ViewGroup;->requestFocus()Z

    goto/16 :goto_1

    :sswitch_5
    if-eqz v7, :cond_c

    if-lez v14, :cond_e

    add-int/lit8 v20, v14, -0x1

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-static {v0, v1}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getCellLayoutChildrenForIndex(Landroid/view/ViewGroup;I)Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    move-result-object v15

    const/16 v20, -0x1

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v11, v15, v0, v1}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getIconInDirection(Lcom/cyanogenmod/trebuchet/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v12

    if-eqz v12, :cond_d

    invoke-virtual {v12}, Landroid/view/View;->requestFocus()Z

    :cond_c
    :goto_6
    const/16 v17, 0x1

    goto/16 :goto_1

    :cond_d
    add-int/lit8 v20, v14, -0x1

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->snapToPage(I)V

    add-int/lit8 v20, v14, -0x1

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->requestFocus()Z

    goto :goto_6

    :cond_e
    const/16 v20, -0x1

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v11, v15, v0, v1}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getIconInDirection(Lcom/cyanogenmod/trebuchet/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v12

    if-eqz v12, :cond_c

    invoke-virtual {v12}, Landroid/view/View;->requestFocus()Z

    goto :goto_6

    :sswitch_6
    if-eqz v7, :cond_f

    add-int/lit8 v20, v13, -0x1

    move/from16 v0, v20

    if-ge v14, v0, :cond_11

    add-int/lit8 v20, v14, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-static {v0, v1}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getCellLayoutChildrenForIndex(Landroid/view/ViewGroup;I)Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    move-result-object v15

    const/16 v20, -0x1

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v11, v15, v0, v1}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getIconInDirection(Lcom/cyanogenmod/trebuchet/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v12

    if-eqz v12, :cond_10

    invoke-virtual {v12}, Landroid/view/View;->requestFocus()Z

    :cond_f
    :goto_7
    const/16 v17, 0x1

    goto/16 :goto_1

    :cond_10
    add-int/lit8 v20, v14, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->snapToPage(I)V

    add-int/lit8 v20, v14, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/Workspace;->getPageAt(I)Landroid/view/View;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->requestFocus()Z

    goto :goto_7

    :cond_11
    invoke-virtual {v15}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->getChildCount()I

    move-result v20

    const/16 v21, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v11, v15, v0, v1}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getIconInDirection(Lcom/cyanogenmod/trebuchet/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v12

    if-eqz v12, :cond_f

    invoke-virtual {v12}, Landroid/view/View;->requestFocus()Z

    goto :goto_7

    :sswitch_7
    if-eqz v7, :cond_12

    const/16 v20, -0x1

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v11, v15, v0, v1}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getIconInDirection(Lcom/cyanogenmod/trebuchet/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v12

    if-eqz v12, :cond_12

    invoke-virtual {v12}, Landroid/view/View;->requestFocus()Z

    :cond_12
    const/16 v17, 0x1

    goto/16 :goto_1

    :sswitch_8
    if-eqz v7, :cond_13

    invoke-virtual {v15}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->getChildCount()I

    move-result v20

    const/16 v21, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v11, v15, v0, v1}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getIconInDirection(Lcom/cyanogenmod/trebuchet/CellLayout;Landroid/view/ViewGroup;II)Landroid/view/View;

    move-result-object v12

    if-eqz v12, :cond_13

    invoke-virtual {v12}, Landroid/view/View;->requestFocus()Z

    :cond_13
    const/16 v17, 0x1

    goto/16 :goto_1

    :sswitch_9
    if-eqz v7, :cond_0

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v20

    move-object/from16 v0, v20

    instance-of v0, v0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;

    move/from16 v20, v0

    if-eqz v20, :cond_0

    const-string v20, "focushelper"

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "-----------------------key become click"

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v3, p0

    check-cast v3, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v20

    const-string v21, "AppWidget(id=2)"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_14

    new-instance v9, Landroid/content/Intent;

    invoke-direct {v9}, Landroid/content/Intent;-><init>()V

    new-instance v6, Landroid/content/ComponentName;

    const-string v20, "com.google.android.youtube"

    const-string v21, "com.google.android.youtube.app.honeycomb.Shell$HomeActivity"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-direct {v6, v0, v1}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v20

    invoke-interface/range {v20 .. v20}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v20

    check-cast v20, Lcom/cyanogenmod/trebuchet/Workspace;

    sput-object v20, Lcom/cyanogenmod/trebuchet/FocusHelper;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    sget-object v20, Lcom/cyanogenmod/trebuchet/FocusHelper;->mWorkspace:Lcom/cyanogenmod/trebuchet/Workspace;

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/Workspace;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Lcom/cyanogenmod/trebuchet/Launcher;->startActivity(Landroid/content/Intent;)V

    sget-object v20, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v21, "[wjx]yijing jin ru focushelper ==xiayibu"

    invoke-virtual/range {v20 .. v21}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/16 v20, 0x1

    goto/16 :goto_2

    :cond_14
    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->requestChildFocus()Z

    move-result v20

    if-nez v20, :cond_15

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->getChildCount()I

    move-result v20

    if-eqz v20, :cond_16

    const-string v20, "focushelper"

    const-string v21, "-----------------------0  click"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v3, v0}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->getChildAt(I)Landroid/view/View;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->callOnClick()Z

    :cond_15
    :goto_8
    const/16 v17, 0x1

    goto/16 :goto_1

    :cond_16
    const-string v20, "focushelper"

    const-string v21, "-----------------------1  click"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->callOnClick()Z

    goto :goto_8

    :sswitch_a
    if-eqz v7, :cond_0

    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v20

    move-object/from16 v0, v20

    instance-of v0, v0, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetInfo;

    move/from16 v20, v0

    if-eqz v20, :cond_0

    move-object/from16 v3, p0

    check-cast v3, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;->requestParentFocus()Z

    const/16 v17, 0x1

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_a
        0x13 -> :sswitch_3
        0x14 -> :sswitch_4
        0x15 -> :sswitch_1
        0x16 -> :sswitch_2
        0x42 -> :sswitch_9
        0x52 -> :sswitch_0
        0x5c -> :sswitch_5
        0x5d -> :sswitch_6
        0x7a -> :sswitch_7
        0x7b -> :sswitch_8
    .end sparse-switch
.end method

.method static handlePagedViewGridLayoutWidgetKeyEvent(Lcom/cyanogenmod/trebuchet/PagedViewWidget;ILandroid/view/KeyEvent;)Z
    .locals 23
    .param p0    # Lcom/cyanogenmod/trebuchet/PagedViewWidget;
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->getParent()Landroid/view/ViewParent;

    move-result-object v14

    check-cast v14, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;

    invoke-virtual {v14}, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v8

    check-cast v8, Lcom/cyanogenmod/trebuchet/PagedView;

    invoke-static {v8}, Lcom/cyanogenmod/trebuchet/FocusHelper;->findTabHostParent(Landroid/view/View;)Landroid/widget/TabHost;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-virtual {v14, v0}, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;->indexOfChild(Landroid/view/View;)I

    move-result v18

    invoke-virtual {v14}, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;->getChildCount()I

    move-result v17

    invoke-virtual {v8, v14}, Lcom/cyanogenmod/trebuchet/PagedView;->indexOfChild(Landroid/view/View;)I

    move-result v21

    move/from16 v0, v21

    invoke-virtual {v8, v0}, Lcom/cyanogenmod/trebuchet/PagedView;->indexToPage(I)I

    move-result v13

    invoke-virtual {v8}, Lcom/cyanogenmod/trebuchet/PagedView;->getChildCount()I

    move-result v12

    invoke-virtual {v14}, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;->getCellCountX()I

    move-result v4

    invoke-virtual {v14}, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;->getCellCountY()I

    move-result v5

    rem-int v19, v18, v4

    div-int v20, v18, v4

    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    const/16 v21, 0x1

    move/from16 v0, v21

    if-eq v2, v0, :cond_0

    const/4 v9, 0x1

    :goto_0
    const/4 v6, 0x0

    const/16 v16, 0x0

    sparse-switch p1, :sswitch_data_0

    :goto_1
    return v16

    :cond_0
    const/4 v9, 0x0

    goto :goto_0

    :sswitch_0
    if-eqz v9, :cond_1

    if-lez v18, :cond_2

    add-int/lit8 v21, v18, -0x1

    move/from16 v0, v21

    invoke-virtual {v14, v0}, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->requestFocus()Z

    :cond_1
    :goto_2
    const/16 v16, 0x1

    goto :goto_1

    :cond_2
    if-lez v13, :cond_1

    add-int/lit8 v21, v13, -0x1

    move/from16 v0, v21

    invoke-static {v8, v0}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getAppsCustomizePage(Landroid/view/ViewGroup;I)Landroid/view/ViewGroup;

    move-result-object v10

    if-eqz v10, :cond_1

    invoke-virtual {v10}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v21

    add-int/lit8 v21, v21, -0x1

    move/from16 v0, v21

    invoke-virtual {v10, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {v6}, Landroid/view/View;->requestFocus()Z

    goto :goto_2

    :sswitch_1
    if-eqz v9, :cond_3

    add-int/lit8 v21, v17, -0x1

    move/from16 v0, v18

    move/from16 v1, v21

    if-ge v0, v1, :cond_4

    add-int/lit8 v21, v18, 0x1

    move/from16 v0, v21

    invoke-virtual {v14, v0}, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->requestFocus()Z

    :cond_3
    :goto_3
    const/16 v16, 0x1

    goto :goto_1

    :cond_4
    add-int/lit8 v21, v12, -0x1

    move/from16 v0, v21

    if-ge v13, v0, :cond_3

    add-int/lit8 v21, v13, 0x1

    move/from16 v0, v21

    invoke-static {v8, v0}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getAppsCustomizePage(Landroid/view/ViewGroup;I)Landroid/view/ViewGroup;

    move-result-object v10

    if-eqz v10, :cond_3

    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v10, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-virtual {v6}, Landroid/view/View;->requestFocus()Z

    goto :goto_3

    :sswitch_2
    if-eqz v9, :cond_5

    if-lez v20, :cond_6

    add-int/lit8 v21, v20, -0x1

    mul-int v21, v21, v4

    add-int v11, v21, v19

    invoke-virtual {v14, v11}, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_5

    invoke-virtual {v6}, Landroid/view/View;->requestFocus()Z

    :cond_5
    :goto_4
    const/16 v16, 0x1

    goto :goto_1

    :cond_6
    const v21, 0x7f0d0018    # com.konka.avenger.R.id.applist_latest

    move/from16 v0, v21

    invoke-virtual {v15, v0}, Landroid/widget/TabHost;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->requestFocus()Z

    goto :goto_4

    :sswitch_3
    if-eqz v9, :cond_7

    add-int/lit8 v21, v5, -0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_7

    add-int/lit8 v21, v17, -0x1

    add-int/lit8 v22, v20, 0x1

    mul-int v22, v22, v4

    add-int v22, v22, v19

    invoke-static/range {v21 .. v22}, Ljava/lang/Math;->min(II)I

    move-result v11

    invoke-virtual {v14, v11}, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_7

    invoke-virtual {v6}, Landroid/view/View;->requestFocus()Z

    :cond_7
    const/16 v16, 0x1

    goto/16 :goto_1

    :sswitch_4
    if-eqz v9, :cond_8

    move-object v7, v8

    check-cast v7, Landroid/view/View$OnClickListener;

    move-object/from16 v0, p0

    invoke-interface {v7, v0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    :cond_8
    const/16 v16, 0x1

    goto/16 :goto_1

    :sswitch_5
    if-eqz v9, :cond_a

    if-lez v13, :cond_b

    add-int/lit8 v21, v13, -0x1

    move/from16 v0, v21

    invoke-static {v8, v0}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getAppsCustomizePage(Landroid/view/ViewGroup;I)Landroid/view/ViewGroup;

    move-result-object v10

    if-eqz v10, :cond_9

    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v10, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    :cond_9
    :goto_5
    if-eqz v6, :cond_a

    invoke-virtual {v6}, Landroid/view/View;->requestFocus()Z

    :cond_a
    const/16 v16, 0x1

    goto/16 :goto_1

    :cond_b
    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v14, v0}, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    goto :goto_5

    :sswitch_6
    if-eqz v9, :cond_d

    add-int/lit8 v21, v12, -0x1

    move/from16 v0, v21

    if-ge v13, v0, :cond_e

    add-int/lit8 v21, v13, 0x1

    move/from16 v0, v21

    invoke-static {v8, v0}, Lcom/cyanogenmod/trebuchet/FocusHelper;->getAppsCustomizePage(Landroid/view/ViewGroup;I)Landroid/view/ViewGroup;

    move-result-object v10

    if-eqz v10, :cond_c

    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v10, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    :cond_c
    :goto_6
    if-eqz v6, :cond_d

    invoke-virtual {v6}, Landroid/view/View;->requestFocus()Z

    :cond_d
    const/16 v16, 0x1

    goto/16 :goto_1

    :cond_e
    add-int/lit8 v21, v17, -0x1

    move/from16 v0, v21

    invoke-virtual {v14, v0}, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    goto :goto_6

    :sswitch_7
    if-eqz v9, :cond_f

    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v14, v0}, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_f

    invoke-virtual {v6}, Landroid/view/View;->requestFocus()Z

    :cond_f
    const/16 v16, 0x1

    goto/16 :goto_1

    :sswitch_8
    if-eqz v9, :cond_10

    add-int/lit8 v21, v17, -0x1

    move/from16 v0, v21

    invoke-virtual {v14, v0}, Lcom/cyanogenmod/trebuchet/PagedViewGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->requestFocus()Z

    :cond_10
    const/16 v16, 0x1

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_2
        0x14 -> :sswitch_3
        0x15 -> :sswitch_0
        0x16 -> :sswitch_1
        0x17 -> :sswitch_4
        0x42 -> :sswitch_4
        0x5c -> :sswitch_5
        0x5d -> :sswitch_6
        0x7a -> :sswitch_7
        0x7b -> :sswitch_8
    .end sparse-switch
.end method

.method static handleTabKeyEvent(Lcom/cyanogenmod/trebuchet/AccessibleTabView;ILandroid/view/KeyEvent;)Z
    .locals 10
    .param p0    # Lcom/cyanogenmod/trebuchet/AccessibleTabView;
    .param p1    # I
    .param p2    # Landroid/view/KeyEvent;

    const/4 v2, 0x1

    const/4 v7, 0x0

    invoke-static {}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->isScreenLarge()Z

    move-result v8

    if-nez v8, :cond_0

    :goto_0
    return v7

    :cond_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AccessibleTabView;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Lcom/cyanogenmod/trebuchet/FocusOnlyTabWidget;

    invoke-static {v3}, Lcom/cyanogenmod/trebuchet/FocusHelper;->findTabHostParent(Landroid/view/View;)Landroid/widget/TabHost;

    move-result-object v5

    const v8, 0x1020011    # android.R.id.tabcontent

    invoke-virtual {v5, v8}, Landroid/widget/TabHost;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/FocusOnlyTabWidget;->getTabCount()I

    move-result v4

    invoke-virtual {v3, p0}, Lcom/cyanogenmod/trebuchet/FocusOnlyTabWidget;->getChildTabIndex(Landroid/view/View;)I

    move-result v6

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-eq v0, v2, :cond_1

    :goto_1
    const/4 v7, 0x0

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v7, 0x1

    goto :goto_0

    :cond_1
    move v2, v7

    goto :goto_1

    :pswitch_1
    if-eqz v2, :cond_2

    if-lez v6, :cond_2

    add-int/lit8 v8, v6, -0x1

    invoke-virtual {v3, v8}, Lcom/cyanogenmod/trebuchet/FocusOnlyTabWidget;->getChildTabViewAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->requestFocus()Z

    :cond_2
    const/4 v7, 0x1

    goto :goto_0

    :pswitch_2
    if-eqz v2, :cond_3

    add-int/lit8 v8, v4, -0x1

    if-ge v6, v8, :cond_4

    add-int/lit8 v8, v6, 0x1

    invoke-virtual {v3, v8}, Lcom/cyanogenmod/trebuchet/FocusOnlyTabWidget;->getChildTabViewAt(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->requestFocus()Z

    :cond_3
    :goto_2
    const/4 v7, 0x1

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AccessibleTabView;->getNextFocusRightId()I

    move-result v8

    const/4 v9, -0x1

    if-eq v8, v9, :cond_3

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/AccessibleTabView;->getNextFocusRightId()I

    move-result v8

    invoke-virtual {v5, v8}, Landroid/widget/TabHost;->findViewById(I)Landroid/view/View;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/View;->requestFocus()Z

    goto :goto_2

    :pswitch_3
    if-eqz v2, :cond_5

    invoke-virtual {v1}, Landroid/view/ViewGroup;->requestFocus()Z

    :cond_5
    const/4 v7, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
