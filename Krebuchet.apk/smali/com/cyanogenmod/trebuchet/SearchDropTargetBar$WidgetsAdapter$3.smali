.class Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$3;
.super Ljava/lang/Object;
.source "SearchDropTargetBar.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;

.field private final synthetic val$layout:Lcom/cyanogenmod/trebuchet/PagedDialogGridView;

.field private final synthetic val$page:I


# direct methods
.method constructor <init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;Lcom/cyanogenmod/trebuchet/PagedDialogGridView;I)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$3;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;

    iput-object p2, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$3;->val$layout:Lcom/cyanogenmod/trebuchet/PagedDialogGridView;

    iput p3, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$3;->val$page:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$3;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;

    iget v3, v0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->mVidgetCellWidth:I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$3;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;

    iget v4, v0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->mVidgetCellHeight:I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$3;->val$layout:Lcom/cyanogenmod/trebuchet/PagedDialogGridView;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/PagedDialogGridView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$3;->val$layout:Lcom/cyanogenmod/trebuchet/PagedDialogGridView;

    invoke-virtual {v0, v5}, Lcom/cyanogenmod/trebuchet/PagedDialogGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Lcom/cyanogenmod/trebuchet/PagedViewWidget;

    invoke-virtual {v11}, Lcom/cyanogenmod/trebuchet/PagedViewWidget;->getPreviewSize()[I

    move-result-object v8

    aget v3, v8, v5

    const/4 v0, 0x1

    aget v4, v8, v0

    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/16 v9, 0x8

    iget v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$3;->val$page:I

    mul-int/lit8 v10, v0, 0x8

    move v7, v10

    :goto_0
    add-int/lit8 v0, v10, 0x8

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$3;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->access$0(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;)Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    move-result-object v1

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWidgets:Ljava/util/List;
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->access$7(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    if-lt v7, v0, :cond_1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$3;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;

    iget v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$3;->val$page:I

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$3;->val$layout:Lcom/cyanogenmod/trebuchet/PagedDialogGridView;

    invoke-virtual/range {v0 .. v6}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->prepareLoadWidgetPreviewsTask(ILjava/util/ArrayList;IIILcom/cyanogenmod/trebuchet/PagedDialogGridView;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$3;->this$1:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->access$0(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;)Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    move-result-object v0

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWidgets:Ljava/util/List;
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->access$7(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v7, v7, 0x1

    goto :goto_0
.end method
