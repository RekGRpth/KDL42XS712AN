.class Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;
.super Lcom/cyanogenmod/trebuchet/VidgetAdapter;
.source "SearchDropTargetBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WidgetsAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$ViewHolder;,
        Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$WidgetsGridViewAdapter;
    }
.end annotation


# instance fields
.field private mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private mOnKeyListener:Landroid/view/View$OnKeyListener;

.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;


# direct methods
.method public constructor <init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;Landroid/content/Context;Lcom/cyanogenmod/trebuchet/Launcher;IIII)V
    .locals 7
    .param p2    # Landroid/content/Context;
    .param p3    # Lcom/cyanogenmod/trebuchet/Launcher;
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    move v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/cyanogenmod/trebuchet/VidgetAdapter;-><init>(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/Launcher;IIII)V

    new-instance v0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$1;

    invoke-direct {v0, p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$1;-><init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->mOnKeyListener:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$2;

    invoke-direct {v0, p0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$2;-><init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    return-void
.end method

.method static synthetic access$0(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;)Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 2

    const/16 v0, 0x8

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWidgets:Ljava/util/List;
    invoke-static {v1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->access$7(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/lit8 v1, v1, -0x1

    div-int/lit8 v1, v1, 0x8

    return v1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    const/4 v9, 0x0

    const/4 v0, 0x0

    if-nez p2, :cond_1

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v8, 0x7f03001c    # com.konka.avenger.R.layout.paged_dialog_apps

    invoke-virtual {v7, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    new-instance v0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$WidgetsGridViewAdapter;

    invoke-direct {v0, p0, v9}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$WidgetsGridViewAdapter;-><init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$WidgetsGridViewAdapter;)V

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->mAdapters:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v2, p2

    check-cast v2, Landroid/widget/GridView;

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->mOnKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v2, v7}, Landroid/widget/GridView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    const/4 v7, 0x4

    invoke-virtual {v2, v7}, Landroid/widget/GridView;->setNumColumns(I)V

    const/4 v7, 0x0

    invoke-virtual {v2, v7}, Landroid/widget/GridView;->setChoiceMode(I)V

    invoke-virtual {v2, v0}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {v2, v0}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v7, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v2, v7}, Landroid/widget/GridView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    :goto_0
    move-object v3, p2

    check-cast v3, Lcom/cyanogenmod/trebuchet/PagedDialogGridView;

    move v5, p1

    invoke-virtual {v3}, Lcom/cyanogenmod/trebuchet/PagedDialogGridView;->isSetOnLayoutListener()Z

    move-result v7

    if-nez v7, :cond_0

    new-instance v7, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$3;

    invoke-direct {v7, p0, v3, v5}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$3;-><init>(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;Lcom/cyanogenmod/trebuchet/PagedDialogGridView;I)V

    invoke-virtual {v3, v7}, Lcom/cyanogenmod/trebuchet/PagedDialogGridView;->setOnLayoutListener(Ljava/lang/Runnable;)V

    :cond_0
    const/16 v4, 0x8

    mul-int/lit8 v6, p1, 0x8

    const/16 v7, 0x8

    iget-object v8, p0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter;->this$0:Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    # getter for: Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->mWidgets:Ljava/util/List;
    invoke-static {v8}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;->access$7(Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    sub-int/2addr v8, v6

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {v0, v6, v1}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$WidgetsGridViewAdapter;->setRange(II)V

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$WidgetsGridViewAdapter;->notifyDataSetChanged()V

    invoke-virtual {p2}, Landroid/view/View;->requestFocus()Z

    return-object p2

    :cond_1
    move-object v7, p2

    check-cast v7, Landroid/widget/GridView;

    invoke-virtual {v7}, Landroid/widget/GridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/SearchDropTargetBar$WidgetsAdapter$WidgetsGridViewAdapter;

    goto :goto_0
.end method
