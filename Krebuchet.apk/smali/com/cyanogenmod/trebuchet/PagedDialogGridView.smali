.class public Lcom/cyanogenmod/trebuchet/PagedDialogGridView;
.super Landroid/widget/GridView;
.source "PagedDialogGridView.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "PagedDialogGridView"


# instance fields
.field private mLastOnLayoutListener:Ljava/lang/Runnable;

.field private mOnLayoutListener:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/cyanogenmod/trebuchet/PagedDialogGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/cyanogenmod/trebuchet/PagedDialogGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/GridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/high16 v0, 0x60000

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/PagedDialogGridView;->setDescendantFocusability(I)V

    return-void
.end method


# virtual methods
.method public isSetOnLayoutListener()Z
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedDialogGridView;->mOnLayoutListener:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    const/4 v0, 0x0

    invoke-super {p0}, Landroid/widget/GridView;->onDetachedFromWindow()V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedDialogGridView;->mOnLayoutListener:Ljava/lang/Runnable;

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedDialogGridView;->mLastOnLayoutListener:Ljava/lang/Runnable;

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Landroid/widget/GridView;->onLayout(ZIIII)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedDialogGridView;->mOnLayoutListener:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedDialogGridView;->mOnLayoutListener:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedDialogGridView;->mOnLayoutListener:Ljava/lang/Runnable;

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedDialogGridView;->mLastOnLayoutListener:Ljava/lang/Runnable;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/PagedDialogGridView;->mOnLayoutListener:Ljava/lang/Runnable;

    :cond_0
    return-void
.end method

.method public setOnLayoutListener(Ljava/lang/Runnable;)V
    .locals 0
    .param p1    # Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/PagedDialogGridView;->mOnLayoutListener:Ljava/lang/Runnable;

    return-void
.end method
