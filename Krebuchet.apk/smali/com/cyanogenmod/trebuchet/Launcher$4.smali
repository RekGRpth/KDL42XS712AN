.class Lcom/cyanogenmod/trebuchet/Launcher$4;
.super Landroid/os/AsyncTask;
.source "Launcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cyanogenmod/trebuchet/Launcher;->checkForLocaleChange()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/Launcher;


# direct methods
.method constructor <init>(Lcom/cyanogenmod/trebuchet/Launcher;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Launcher$4;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;
    .locals 2
    .param p1    # [Ljava/lang/Void;

    new-instance v0, Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;-><init>(Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/Launcher$4;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # invokes: Lcom/cyanogenmod/trebuchet/Launcher;->readConfiguration(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;)V
    invoke-static {v1, v0}, Lcom/cyanogenmod/trebuchet/Launcher;->access$22(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;)V

    return-object v0
.end method

.method protected bridge varargs synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/Launcher$4;->doInBackground([Ljava/lang/Void;)Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;)V
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;

    invoke-static {p1}, Lcom/cyanogenmod/trebuchet/Launcher;->access$23(Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/Launcher$4;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    # invokes: Lcom/cyanogenmod/trebuchet/Launcher;->checkForLocaleChange()V
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->access$24(Lcom/cyanogenmod/trebuchet/Launcher;)V

    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;

    invoke-virtual {p0, p1}, Lcom/cyanogenmod/trebuchet/Launcher$4;->onPostExecute(Lcom/cyanogenmod/trebuchet/Launcher$LocaleConfiguration;)V

    return-void
.end method
