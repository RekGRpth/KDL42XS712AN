.class public Lcom/cyanogenmod/trebuchet/DragLayer;
.super Landroid/widget/FrameLayout;
.source "DragLayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cyanogenmod/trebuchet/DragLayer$HideHintRunnable;,
        Lcom/cyanogenmod/trebuchet/DragLayer$HintContent;,
        Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;,
        Lcom/cyanogenmod/trebuchet/DragLayer$ShowHintRunnable;
    }
.end annotation


# static fields
.field public static final DEFAULT_HINT_DELAY_MILLIS:J = 0x3e8L

.field public static final DEFAULT_HINT_DURATION_MILLIS:J = 0x3e8L

.field private static final RESHOWING_GAP_MILLIS:J = 0x1f4L


# instance fields
.field private mCubicEaseOutInterpolator:Landroid/animation/TimeInterpolator;

.field private mCurrentResizeFrame:Lcom/cyanogenmod/trebuchet/AppWidgetResizeFrame;

.field private mDragController:Lcom/cyanogenmod/trebuchet/DragController;

.field private mDropAnim:Landroid/animation/ValueAnimator;

.field private mDropView:Landroid/view/View;

.field private mDropViewAlpha:F

.field private mDropViewPos:[I

.field private mDropViewScale:F

.field private mFadeOutAnim:Landroid/animation/ValueAnimator;

.field private mHideHintRunnalbe:Lcom/cyanogenmod/trebuchet/DragLayer$HideHintRunnable;

.field private mHintAnchor:Landroid/view/View;

.field private mHintContentView:Landroid/widget/TextView;

.field private mHintDuration:J

.field private mHintHandler:Landroid/os/Handler;

.field private mHintPopup:Landroid/widget/PopupWindow;

.field private mHintText:Ljava/lang/String;

.field private mHitRect:Landroid/graphics/Rect;

.field private mHoverPointClosesFolder:Z

.field private mLastShowHintMillis:J

.field private mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

.field private mQsbIndex:I

.field private final mResizeFrames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/cyanogenmod/trebuchet/AppWidgetResizeFrame;",
            ">;"
        }
    .end annotation
.end field

.field private mShowHintRunnable:Lcom/cyanogenmod/trebuchet/DragLayer$ShowHintRunnable;

.field private mTmpXY:[I

.field private mWorkspaceIndex:I

.field private mXDown:I

.field private mYDown:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v6, 0x2

    const/4 v5, -0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-array v1, v6, [I

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mTmpXY:[I

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mResizeFrames:Ljava/util/ArrayList;

    iput-object v3, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDropAnim:Landroid/animation/ValueAnimator;

    iput-object v3, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mFadeOutAnim:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v2, 0x3fc00000    # 1.5f

    invoke-direct {v1, v2}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mCubicEaseOutInterpolator:Landroid/animation/TimeInterpolator;

    iput-object v3, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDropView:Landroid/view/View;

    new-array v1, v6, [I

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDropViewPos:[I

    iput-boolean v4, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHoverPointClosesFolder:Z

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHitRect:Landroid/graphics/Rect;

    iput v5, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mWorkspaceIndex:I

    iput v5, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mQsbIndex:I

    new-instance v1, Lcom/cyanogenmod/trebuchet/DragLayer$ShowHintRunnable;

    invoke-direct {v1, p0, v3}, Lcom/cyanogenmod/trebuchet/DragLayer$ShowHintRunnable;-><init>(Lcom/cyanogenmod/trebuchet/DragLayer;Lcom/cyanogenmod/trebuchet/DragLayer$ShowHintRunnable;)V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mShowHintRunnable:Lcom/cyanogenmod/trebuchet/DragLayer$ShowHintRunnable;

    new-instance v1, Lcom/cyanogenmod/trebuchet/DragLayer$HideHintRunnable;

    invoke-direct {v1, p0, v3}, Lcom/cyanogenmod/trebuchet/DragLayer$HideHintRunnable;-><init>(Lcom/cyanogenmod/trebuchet/DragLayer;Lcom/cyanogenmod/trebuchet/DragLayer$HideHintRunnable;)V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHideHintRunnalbe:Lcom/cyanogenmod/trebuchet/DragLayer$HideHintRunnable;

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mLastShowHintMillis:J

    invoke-virtual {p0, v4}, Lcom/cyanogenmod/trebuchet/DragLayer;->setMotionEventSplittingEnabled(Z)V

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/DragLayer;->setChildrenDrawingOrderEnabled(Z)V

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHintHandler:Landroid/os/Handler;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030012    # com.konka.avenger.R.layout.hint_popup

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHintContentView:Landroid/widget/TextView;

    new-instance v1, Landroid/widget/PopupWindow;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHintContentView:Landroid/widget/TextView;

    invoke-direct {v1, v2}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;)V

    iput-object v1, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHintPopup:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHintPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v4}, Landroid/widget/PopupWindow;->setTouchable(Z)V

    return-void
.end method

.method static synthetic access$0(Lcom/cyanogenmod/trebuchet/DragLayer;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHintContentView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$1(Lcom/cyanogenmod/trebuchet/DragLayer;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHintText:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$10(Lcom/cyanogenmod/trebuchet/DragLayer;)V
    .locals 0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/DragLayer;->fadeOutDragView()V

    return-void
.end method

.method static synthetic access$11(Lcom/cyanogenmod/trebuchet/DragLayer;Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDropView:Landroid/view/View;

    return-void
.end method

.method static synthetic access$12(Lcom/cyanogenmod/trebuchet/DragLayer;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDropView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$2(Lcom/cyanogenmod/trebuchet/DragLayer;)Landroid/widget/PopupWindow;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHintPopup:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic access$3(Lcom/cyanogenmod/trebuchet/DragLayer;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHintAnchor:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$4(Lcom/cyanogenmod/trebuchet/DragLayer;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHintHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$5(Lcom/cyanogenmod/trebuchet/DragLayer;)Lcom/cyanogenmod/trebuchet/DragLayer$HideHintRunnable;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHideHintRunnalbe:Lcom/cyanogenmod/trebuchet/DragLayer$HideHintRunnable;

    return-object v0
.end method

.method static synthetic access$6(Lcom/cyanogenmod/trebuchet/DragLayer;)J
    .locals 2

    iget-wide v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHintDuration:J

    return-wide v0
.end method

.method static synthetic access$7(Lcom/cyanogenmod/trebuchet/DragLayer;)[I
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDropViewPos:[I

    return-object v0
.end method

.method static synthetic access$8(Lcom/cyanogenmod/trebuchet/DragLayer;F)V
    .locals 0

    iput p1, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDropViewScale:F

    return-void
.end method

.method static synthetic access$9(Lcom/cyanogenmod/trebuchet/DragLayer;F)V
    .locals 0

    iput p1, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDropViewAlpha:F

    return-void
.end method

.method private animateViewIntoPosition(Landroid/view/View;IIIIFLjava/lang/Runnable;I)V
    .locals 13
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # F
    .param p7    # Ljava/lang/Runnable;
    .param p8    # I

    new-instance v4, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, p2

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int v3, v3, p3

    move/from16 v0, p3

    invoke-direct {v4, p2, v0, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v5, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int v2, v2, p4

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int v3, v3, p5

    move/from16 v0, p4

    move/from16 v1, p5

    invoke-direct {v5, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v12, 0x1

    move-object v2, p0

    move-object v3, p1

    move/from16 v7, p6

    move/from16 v8, p8

    move-object/from16 v11, p7

    invoke-virtual/range {v2 .. v12}, Lcom/cyanogenmod/trebuchet/DragLayer;->animateView(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;FFILandroid/view/animation/Interpolator;Landroid/view/animation/Interpolator;Ljava/lang/Runnable;Z)V

    return-void
.end method

.method private fadeOutDragView()V
    .locals 3

    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mFadeOutAnim:Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mFadeOutAnim:Landroid/animation/ValueAnimator;

    const-wide/16 v1, 0x96

    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mFadeOutAnim:Landroid/animation/ValueAnimator;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mFadeOutAnim:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mFadeOutAnim:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/cyanogenmod/trebuchet/DragLayer$4;

    invoke-direct {v1, p0}, Lcom/cyanogenmod/trebuchet/DragLayer$4;-><init>(Lcom/cyanogenmod/trebuchet/DragLayer;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mFadeOutAnim:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/cyanogenmod/trebuchet/DragLayer$5;

    invoke-direct {v1, p0}, Lcom/cyanogenmod/trebuchet/DragLayer$5;-><init>(Lcom/cyanogenmod/trebuchet/DragLayer;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mFadeOutAnim:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private handleTouchDown(Landroid/view/MotionEvent;Z)Z
    .locals 9
    .param p1    # Landroid/view/MotionEvent;
    .param p2    # Z

    const/4 v5, 0x1

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    float-to-int v3, v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v4, v6

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mResizeFrames:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-nez v7, :cond_1

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v6}, Lcom/cyanogenmod/trebuchet/Launcher;->getWorkspace()Lcom/cyanogenmod/trebuchet/Workspace;

    move-result-object v6

    invoke-virtual {v6}, Lcom/cyanogenmod/trebuchet/Workspace;->getOpenFolder()Lcom/cyanogenmod/trebuchet/Folder;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v6}, Lcom/cyanogenmod/trebuchet/Launcher;->isFolderClingVisible()Z

    move-result v6

    if-nez v6, :cond_3

    if-eqz p2, :cond_3

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Folder;->isEditingName()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-direct {p0, v1, p1}, Lcom/cyanogenmod/trebuchet/DragLayer;->isEventOverFolderTextRegion(Lcom/cyanogenmod/trebuchet/Folder;Landroid/view/MotionEvent;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Folder;->dismissEditingName()V

    :goto_0
    return v5

    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/AppWidgetResizeFrame;

    invoke-virtual {v0, v2}, Lcom/cyanogenmod/trebuchet/AppWidgetResizeFrame;->getHitRect(Landroid/graphics/Rect;)V

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/AppWidgetResizeFrame;->getLeft()I

    move-result v7

    sub-int v7, v3, v7

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/AppWidgetResizeFrame;->getTop()I

    move-result v8

    sub-int v8, v4, v8

    invoke-virtual {v0, v7, v8}, Lcom/cyanogenmod/trebuchet/AppWidgetResizeFrame;->beginResizeIfPointInRegion(II)Z

    move-result v7

    if-eqz v7, :cond_0

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mCurrentResizeFrame:Lcom/cyanogenmod/trebuchet/AppWidgetResizeFrame;

    iput v3, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mXDown:I

    iput v4, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mYDown:I

    invoke-virtual {p0, v5}, Lcom/cyanogenmod/trebuchet/DragLayer;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v1, v2}, Lcom/cyanogenmod/trebuchet/DragLayer;->getDescendantRectRelativeToSelf(Landroid/view/View;Landroid/graphics/Rect;)F

    invoke-direct {p0, v1, p1}, Lcom/cyanogenmod/trebuchet/DragLayer;->isEventOverFolder(Lcom/cyanogenmod/trebuchet/Folder;Landroid/view/MotionEvent;)Z

    move-result v6

    if-nez v6, :cond_3

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v6}, Lcom/cyanogenmod/trebuchet/Launcher;->closeFolder()V

    goto :goto_0

    :cond_3
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private isEventOverFolder(Lcom/cyanogenmod/trebuchet/Folder;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Lcom/cyanogenmod/trebuchet/Folder;
    .param p2    # Landroid/view/MotionEvent;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHitRect:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, v0}, Lcom/cyanogenmod/trebuchet/DragLayer;->getDescendantRectRelativeToSelf(Landroid/view/View;Landroid/graphics/Rect;)F

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHitRect:Landroid/graphics/Rect;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    return v0
.end method

.method private isEventOverFolderTextRegion(Lcom/cyanogenmod/trebuchet/Folder;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1    # Lcom/cyanogenmod/trebuchet/Folder;
    .param p2    # Landroid/view/MotionEvent;

    invoke-virtual {p1}, Lcom/cyanogenmod/trebuchet/Folder;->getEditTextRegion()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHitRect:Landroid/graphics/Rect;

    invoke-virtual {p0, v0, v1}, Lcom/cyanogenmod/trebuchet/DragLayer;->getDescendantRectRelativeToSelf(Landroid/view/View;Landroid/graphics/Rect;)F

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHitRect:Landroid/graphics/Rect;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    return v0
.end method

.method private sendTapOutsideFolderAccessibilityEvent(Z)V
    .locals 4
    .param p1    # Z

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz p1, :cond_1

    const v1, 0x7f0a0064    # com.konka.avenger.R.string.folder_tap_to_rename

    :goto_0
    const/16 v2, 0x8

    invoke-static {v2}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/DragLayer;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/accessibility/AccessibilityManager;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    :cond_0
    return-void

    :cond_1
    const v1, 0x7f0a0063    # com.konka.avenger.R.string.folder_tap_to_close

    goto :goto_0
.end method

.method private updateChildIndices()V
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->getWorkspace()Lcom/cyanogenmod/trebuchet/Workspace;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/DragLayer;->indexOfChild(Landroid/view/View;)I

    move-result v0

    iput v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mWorkspaceIndex:I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->getSearchBar()Lcom/cyanogenmod/trebuchet/SearchDropTargetBar;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/DragLayer;->indexOfChild(Landroid/view/View;)I

    move-result v0

    iput v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mQsbIndex:I

    :cond_0
    return-void
.end method


# virtual methods
.method public addResizeFrame(Lcom/cyanogenmod/trebuchet/ItemInfo;Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;Lcom/cyanogenmod/trebuchet/CellLayout;)V
    .locals 8
    .param p1    # Lcom/cyanogenmod/trebuchet/ItemInfo;
    .param p2    # Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;
    .param p3    # Lcom/cyanogenmod/trebuchet/CellLayout;

    const/4 v7, -0x1

    new-instance v0, Lcom/cyanogenmod/trebuchet/AppWidgetResizeFrame;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/DragLayer;->getContext()Landroid/content/Context;

    move-result-object v1

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/cyanogenmod/trebuchet/AppWidgetResizeFrame;-><init>(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/ItemInfo;Lcom/cyanogenmod/trebuchet/LauncherAppWidgetHostView;Lcom/cyanogenmod/trebuchet/CellLayout;Lcom/cyanogenmod/trebuchet/DragLayer;)V

    new-instance v6, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;

    invoke-direct {v6, v7, v7}, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;-><init>(II)V

    const/4 v1, 0x1

    iput-boolean v1, v6, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;->customPosition:Z

    invoke-virtual {p0, v0, v6}, Lcom/cyanogenmod/trebuchet/DragLayer;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mResizeFrames:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/cyanogenmod/trebuchet/AppWidgetResizeFrame;->snapToWidget(Z)V

    return-void
.end method

.method public animateView(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;FFILandroid/view/animation/Interpolator;Landroid/view/animation/Interpolator;Ljava/lang/Runnable;Z)V
    .locals 17
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/graphics/Rect;
    .param p3    # Landroid/graphics/Rect;
    .param p4    # F
    .param p5    # F
    .param p6    # I
    .param p7    # Landroid/view/animation/Interpolator;
    .param p8    # Landroid/view/animation/Interpolator;
    .param p9    # Ljava/lang/Runnable;
    .param p10    # Z

    move-object/from16 v0, p3

    iget v3, v0, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p2

    iget v4, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    int-to-double v3, v3

    const-wide/high16 v5, 0x4000000000000000L    # 2.0

    invoke-static {v3, v4, v5, v6}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v3

    move-object/from16 v0, p3

    iget v5, v0, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p2

    iget v6, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v5, v6

    int-to-double v5, v5

    const-wide/high16 v7, 0x4000000000000000L    # 2.0

    invoke-static {v5, v6, v7, v8}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v5

    add-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v3

    double-to-float v13, v3

    invoke-virtual/range {p0 .. p0}, Lcom/cyanogenmod/trebuchet/DragLayer;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v3, 0x7f0b001c    # com.konka.avenger.R.integer.config_dropAnimMaxDist

    invoke-virtual {v15, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    int-to-float v14, v3

    if-gez p6, :cond_0

    const v3, 0x7f0b001a    # com.konka.avenger.R.integer.config_dropAnimMaxDuration

    invoke-virtual {v15, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result p6

    cmpg-float v3, v13, v14

    if-gez v3, :cond_0

    move/from16 v0, p6

    int-to-float v3, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/DragLayer;->mCubicEaseOutInterpolator:Landroid/animation/TimeInterpolator;

    div-float v5, v13, v14

    invoke-interface {v4, v5}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v4

    mul-float/2addr v3, v4

    float-to-int v0, v3

    move/from16 p6, v0

    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDropAnim:Landroid/animation/ValueAnimator;

    if-eqz v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDropAnim:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->cancel()V

    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/DragLayer;->mFadeOutAnim:Landroid/animation/ValueAnimator;

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/DragLayer;->mFadeOutAnim:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->cancel()V

    :cond_2
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/cyanogenmod/trebuchet/DragLayer;->mDropView:Landroid/view/View;

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getAlpha()F

    move-result v12

    new-instance v3, Landroid/animation/ValueAnimator;

    invoke-direct {v3}, Landroid/animation/ValueAnimator;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDropAnim:Landroid/animation/ValueAnimator;

    if-eqz p8, :cond_3

    if-nez p7, :cond_4

    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDropAnim:Landroid/animation/ValueAnimator;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/cyanogenmod/trebuchet/DragLayer;->mCubicEaseOutInterpolator:Landroid/animation/TimeInterpolator;

    invoke-virtual {v3, v4}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDropAnim:Landroid/animation/ValueAnimator;

    move/from16 v0, p6

    int-to-long v4, v0

    invoke-virtual {v3, v4, v5}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDropAnim:Landroid/animation/ValueAnimator;

    const/4 v4, 0x2

    new-array v4, v4, [F

    fill-array-data v4, :array_0

    invoke-virtual {v3, v4}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDropAnim:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDropAnim:Landroid/animation/ValueAnimator;

    move-object/from16 v16, v0

    new-instance v3, Lcom/cyanogenmod/trebuchet/DragLayer$2;

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p8

    move-object/from16 v7, p7

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    move/from16 v10, p5

    move/from16 v11, p4

    invoke-direct/range {v3 .. v12}, Lcom/cyanogenmod/trebuchet/DragLayer$2;-><init>(Lcom/cyanogenmod/trebuchet/DragLayer;Landroid/view/View;Landroid/view/animation/Interpolator;Landroid/view/animation/Interpolator;Landroid/graphics/Rect;Landroid/graphics/Rect;FFF)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDropAnim:Landroid/animation/ValueAnimator;

    new-instance v4, Lcom/cyanogenmod/trebuchet/DragLayer$3;

    move-object/from16 v0, p0

    move-object/from16 v1, p9

    move/from16 v2, p10

    invoke-direct {v4, v0, v1, v2}, Lcom/cyanogenmod/trebuchet/DragLayer$3;-><init>(Lcom/cyanogenmod/trebuchet/DragLayer;Ljava/lang/Runnable;Z)V

    invoke-virtual {v3, v4}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDropAnim:Landroid/animation/ValueAnimator;

    invoke-virtual {v3}, Landroid/animation/ValueAnimator;->start()V

    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public animateViewIntoPosition(Lcom/cyanogenmod/trebuchet/DragView;Landroid/view/View;)V
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/DragView;
    .param p2    # Landroid/view/View;

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/cyanogenmod/trebuchet/DragLayer;->animateViewIntoPosition(Lcom/cyanogenmod/trebuchet/DragView;Landroid/view/View;Ljava/lang/Runnable;)V

    return-void
.end method

.method public animateViewIntoPosition(Lcom/cyanogenmod/trebuchet/DragView;Landroid/view/View;ILjava/lang/Runnable;)V
    .locals 17
    .param p1    # Lcom/cyanogenmod/trebuchet/DragView;
    .param p2    # Landroid/view/View;
    .param p3    # I
    .param p4    # Ljava/lang/Runnable;

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Lcom/cyanogenmod/trebuchet/CellLayoutChildren;->measureChild(Landroid/view/View;)V

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v14

    check-cast v14, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;

    new-instance v15, Landroid/graphics/Rect;

    invoke-direct {v15}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v15}, Lcom/cyanogenmod/trebuchet/DragLayer;->getViewRectRelativeToSelf(Landroid/view/View;Landroid/graphics/Rect;)V

    const/4 v3, 0x2

    new-array v12, v3, [I

    const/4 v3, 0x0

    iget v4, v14, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->x:I

    aput v4, v12, v3

    const/4 v3, 0x1

    iget v4, v14, Lcom/cyanogenmod/trebuchet/CellLayout$LayoutParams;->y:I

    aput v4, v12, v3

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v12}, Lcom/cyanogenmod/trebuchet/DragLayer;->getDescendantCoordRelativeToSelf(Landroid/view/View;[I)F

    move-result v9

    const/4 v3, 0x0

    aget v7, v12, v3

    const/4 v3, 0x1

    aget v8, v12, v3

    move-object/from16 v0, p2

    instance-of v3, v0, Landroid/widget/TextView;

    if-eqz v3, :cond_0

    move-object/from16 v16, p2

    check-cast v16, Landroid/widget/TextView;

    invoke-virtual/range {v16 .. v16}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v3

    const/4 v4, 0x1

    aget-object v13, v3, v4

    invoke-virtual/range {v16 .. v16}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v9

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    add-int/2addr v8, v3

    invoke-virtual/range {p1 .. p1}, Lcom/cyanogenmod/trebuchet/DragView;->getHeight()I

    move-result v3

    invoke-virtual {v13}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v9

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v8, v3

    invoke-virtual/range {p1 .. p1}, Lcom/cyanogenmod/trebuchet/DragView;->getMeasuredWidth()I

    move-result v3

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v9

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v7, v3

    :goto_0
    iget v5, v15, Landroid/graphics/Rect;->left:I

    iget v6, v15, Landroid/graphics/Rect;->top:I

    const/4 v3, 0x4

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    const/4 v3, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->setAlpha(F)V

    new-instance v10, Lcom/cyanogenmod/trebuchet/DragLayer$1;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p4

    invoke-direct {v10, v0, v1, v2}, Lcom/cyanogenmod/trebuchet/DragLayer$1;-><init>(Lcom/cyanogenmod/trebuchet/DragLayer;Landroid/view/View;Ljava/lang/Runnable;)V

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move/from16 v11, p3

    invoke-direct/range {v3 .. v11}, Lcom/cyanogenmod/trebuchet/DragLayer;->animateViewIntoPosition(Landroid/view/View;IIIIFLjava/lang/Runnable;I)V

    return-void

    :cond_0
    move-object/from16 v0, p2

    instance-of v3, v0, Lcom/cyanogenmod/trebuchet/FolderIcon;

    if-eqz v3, :cond_1

    sget v3, Lcom/cyanogenmod/trebuchet/HolographicOutlineHelper;->MAX_OUTER_BLUR_RADIUS:I

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v8, v3

    invoke-virtual/range {p1 .. p1}, Lcom/cyanogenmod/trebuchet/DragView;->getMeasuredWidth()I

    move-result v3

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v9

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v7, v3

    goto :goto_0

    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/cyanogenmod/trebuchet/DragView;->getHeight()I

    move-result v3

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v3, v9

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v8, v3

    invoke-virtual/range {p1 .. p1}, Lcom/cyanogenmod/trebuchet/DragView;->getMeasuredWidth()I

    move-result v3

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v3, v9

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v7, v3

    goto :goto_0
.end method

.method public animateViewIntoPosition(Lcom/cyanogenmod/trebuchet/DragView;Landroid/view/View;Ljava/lang/Runnable;)V
    .locals 1
    .param p1    # Lcom/cyanogenmod/trebuchet/DragView;
    .param p2    # Landroid/view/View;
    .param p3    # Ljava/lang/Runnable;

    const/4 v0, -0x1

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/cyanogenmod/trebuchet/DragLayer;->animateViewIntoPosition(Lcom/cyanogenmod/trebuchet/DragView;Landroid/view/View;ILjava/lang/Runnable;)V

    return-void
.end method

.method public animateViewIntoPosition(Lcom/cyanogenmod/trebuchet/DragView;[IFLjava/lang/Runnable;)V
    .locals 10
    .param p1    # Lcom/cyanogenmod/trebuchet/DragView;
    .param p2    # [I
    .param p3    # F
    .param p4    # Ljava/lang/Runnable;

    new-instance v9, Landroid/graphics/Rect;

    invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p0, p1, v9}, Lcom/cyanogenmod/trebuchet/DragLayer;->getViewRectRelativeToSelf(Landroid/view/View;Landroid/graphics/Rect;)V

    iget v2, v9, Landroid/graphics/Rect;->left:I

    iget v3, v9, Landroid/graphics/Rect;->top:I

    const/4 v0, 0x0

    aget v4, p2, v0

    const/4 v0, 0x1

    aget v5, p2, v0

    const/4 v8, -0x1

    move-object v0, p0

    move-object v1, p1

    move v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v8}, Lcom/cyanogenmod/trebuchet/DragLayer;->animateViewIntoPosition(Landroid/view/View;IIIIFLjava/lang/Runnable;I)V

    return-void
.end method

.method public clearAllResizeFrames()V
    .locals 3

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mResizeFrames:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mResizeFrames:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mResizeFrames:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/cyanogenmod/trebuchet/AppWidgetResizeFrame;

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/DragLayer;->removeView(Landroid/view/View;)V

    goto :goto_0
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 9
    .param p1    # Landroid/graphics/Canvas;

    const/4 v8, 0x1

    const/high16 v7, 0x40000000    # 2.0f

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDropView:Landroid/view/View;

    if-eqz v4, :cond_0

    invoke-virtual {p1, v8}, Landroid/graphics/Canvas;->save(I)I

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDropViewPos:[I

    const/4 v5, 0x0

    aget v4, v4, v5

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDropView:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getScrollX()I

    move-result v5

    sub-int v2, v4, v5

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDropViewPos:[I

    aget v4, v4, v8

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDropView:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getScrollY()I

    move-result v5

    sub-int v3, v4, v5

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDropView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDropView:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    int-to-float v4, v2

    int-to-float v5, v3

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    iget v4, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDropViewScale:F

    sub-float v4, v6, v4

    int-to-float v5, v1

    mul-float/2addr v4, v5

    div-float/2addr v4, v7

    iget v5, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDropViewScale:F

    sub-float v5, v6, v5

    int-to-float v6, v0

    mul-float/2addr v5, v6

    div-float/2addr v5, v7

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    iget v4, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDropViewScale:F

    iget v5, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDropViewScale:F

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->scale(FF)V

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDropView:Landroid/view/View;

    iget v5, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDropViewAlpha:F

    invoke-virtual {v4, v5}, Landroid/view/View;->setAlpha(F)V

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDropView:Landroid/view/View;

    invoke-virtual {v4, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_0
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # Landroid/view/KeyEvent;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDragController:Lcom/cyanogenmod/trebuchet/DragController;

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/DragController;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public dispatchUnhandledMove(Landroid/view/View;I)Z
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDragController:Lcom/cyanogenmod/trebuchet/DragController;

    invoke-virtual {v0, p1, p2}, Lcom/cyanogenmod/trebuchet/DragController;->dispatchUnhandledMove(Landroid/view/View;I)Z

    move-result v0

    return v0
.end method

.method protected getChildDrawingOrder(II)I
    .locals 2
    .param p1    # I
    .param p2    # I

    const/4 v1, -0x1

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/DragLayer;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/LauncherApplication;->isScreenLandscape(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->getChildDrawingOrder(II)I

    move-result p2

    :cond_0
    :goto_0
    return p2

    :cond_1
    iget v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mWorkspaceIndex:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mQsbIndex:I

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Launcher;->getWorkspace()Lcom/cyanogenmod/trebuchet/Workspace;

    move-result-object v0

    invoke-virtual {v0}, Lcom/cyanogenmod/trebuchet/Workspace;->isDrawingBackgroundGradient()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mQsbIndex:I

    if-ne p2, v0, :cond_2

    iget p2, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mWorkspaceIndex:I

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mWorkspaceIndex:I

    if-ne p2, v0, :cond_0

    iget p2, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mQsbIndex:I

    goto :goto_0
.end method

.method public getDescendantCoordRelativeToSelf(Landroid/view/View;[I)F
    .locals 9
    .param p1    # Landroid/view/View;
    .param p2    # [I

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v4, 0x2

    new-array v0, v4, [F

    aget v4, p2, v7

    int-to-float v4, v4

    aput v4, v0, v7

    aget v4, p2, v8

    int-to-float v4, v4

    aput v4, v0, v8

    invoke-virtual {p1}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/graphics/Matrix;->mapPoints([F)V

    invoke-virtual {p1}, Landroid/view/View;->getScaleX()F

    move-result v4

    mul-float/2addr v1, v4

    aget v4, v0, v7

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v4, v5

    aput v4, v0, v7

    aget v4, v0, v8

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v4, v5

    aput v4, v0, v8

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    :goto_0
    instance-of v4, v3, Landroid/view/View;

    if-eqz v4, :cond_0

    if-ne v3, p0, :cond_1

    :cond_0
    aget v4, v0, v7

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    aput v4, p2, v7

    aget v4, v0, v8

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    aput v4, p2, v8

    return v1

    :cond_1
    move-object v2, v3

    check-cast v2, Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/graphics/Matrix;->mapPoints([F)V

    invoke-virtual {v2}, Landroid/view/View;->getScaleX()F

    move-result v4

    mul-float/2addr v1, v4

    aget v4, v0, v7

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v5

    invoke-virtual {v2}, Landroid/view/View;->getScrollX()I

    move-result v6

    sub-int/2addr v5, v6

    int-to-float v5, v5

    add-float/2addr v4, v5

    aput v4, v0, v7

    aget v4, v0, v8

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v5

    invoke-virtual {v2}, Landroid/view/View;->getScrollY()I

    move-result v6

    sub-int/2addr v5, v6

    int-to-float v5, v5

    add-float/2addr v4, v5

    aput v4, v0, v8

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    goto :goto_0
.end method

.method public getDescendantRectRelativeToSelf(Landroid/view/View;Landroid/graphics/Rect;)F
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/graphics/Rect;

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mTmpXY:[I

    aput v4, v1, v4

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mTmpXY:[I

    aput v4, v1, v5

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mTmpXY:[I

    invoke-virtual {p0, p1, v1}, Lcom/cyanogenmod/trebuchet/DragLayer;->getDescendantCoordRelativeToSelf(Landroid/view/View;[I)F

    move-result v0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mTmpXY:[I

    aget v1, v1, v4

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mTmpXY:[I

    aget v2, v2, v5

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mTmpXY:[I

    aget v3, v3, v4

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mTmpXY:[I

    aget v4, v4, v5

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {p2, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    return v0
.end method

.method public getLocationInDragLayer(Landroid/view/View;[I)V
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # [I

    const/4 v1, 0x0

    aput v1, p2, v1

    const/4 v0, 0x1

    aput v1, p2, v0

    invoke-virtual {p0, p1, p2}, Lcom/cyanogenmod/trebuchet/DragLayer;->getDescendantCoordRelativeToSelf(Landroid/view/View;[I)F

    return-void
.end method

.method public getViewRectRelativeToSelf(Landroid/view/View;Landroid/graphics/Rect;)V
    .locals 10
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/graphics/Rect;

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x2

    new-array v1, v7, [I

    invoke-virtual {p0, v1}, Lcom/cyanogenmod/trebuchet/DragLayer;->getLocationInWindow([I)V

    aget v5, v1, v8

    aget v6, v1, v9

    invoke-virtual {p1, v1}, Landroid/view/View;->getLocationInWindow([I)V

    aget v3, v1, v8

    aget v4, v1, v9

    sub-int v0, v3, v5

    sub-int v2, v4, v6

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v7, v0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v8, v2

    invoke-virtual {p2, v0, v2, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    return-void
.end method

.method public hasResizeFrames()Z
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mResizeFrames:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hideHint(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHintHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHideHintRunnalbe:Lcom/cyanogenmod/trebuchet/DragLayer$HideHintRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHintAnchor:Landroid/view/View;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHintHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mShowHintRunnable:Lcom/cyanogenmod/trebuchet/DragLayer$ShowHintRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHintPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHintPopup:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    :cond_0
    return-void
.end method

.method public isWidgetBeingResized()Z
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mCurrentResizeFrame:Lcom/cyanogenmod/trebuchet/AppWidgetResizeFrame;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x0

    return v0
.end method

.method public onInterceptHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1    # Landroid/view/MotionEvent;

    const/4 v3, 0x0

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/Launcher;->getWorkspace()Lcom/cyanogenmod/trebuchet/Workspace;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/Launcher;->getWorkspace()Lcom/cyanogenmod/trebuchet/Workspace;

    move-result-object v5

    invoke-virtual {v5}, Lcom/cyanogenmod/trebuchet/Workspace;->getOpenFolder()Lcom/cyanogenmod/trebuchet/Folder;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :goto_1
    :pswitch_1
    invoke-direct {p0, v1, p1}, Lcom/cyanogenmod/trebuchet/DragLayer;->isEventOverFolder(Lcom/cyanogenmod/trebuchet/Folder;Landroid/view/MotionEvent;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-boolean v5, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHoverPointClosesFolder:Z

    if-nez v5, :cond_3

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Folder;->isEditingName()Z

    move-result v3

    invoke-direct {p0, v3}, Lcom/cyanogenmod/trebuchet/DragLayer;->sendTapOutsideFolderAccessibilityEvent(Z)V

    iput-boolean v4, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHoverPointClosesFolder:Z

    move v3, v4

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, v1, p1}, Lcom/cyanogenmod/trebuchet/DragLayer;->isEventOverFolder(Lcom/cyanogenmod/trebuchet/Folder;Landroid/view/MotionEvent;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/Folder;->isEditingName()Z

    move-result v3

    invoke-direct {p0, v3}, Lcom/cyanogenmod/trebuchet/DragLayer;->sendTapOutsideFolderAccessibilityEvent(Z)V

    iput-boolean v4, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHoverPointClosesFolder:Z

    move v3, v4

    goto :goto_0

    :cond_2
    iput-boolean v3, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHoverPointClosesFolder:Z

    goto :goto_1

    :cond_3
    if-eqz v2, :cond_4

    iput-boolean v3, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHoverPointClosesFolder:Z

    goto :goto_0

    :cond_4
    move v3, v4

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1    # Landroid/view/MotionEvent;

    const/4 v0, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, p1, v0}, Lcom/cyanogenmod/trebuchet/DragLayer;->handleTouchDown(Landroid/view/MotionEvent;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/DragLayer;->clearAllResizeFrames()V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDragController:Lcom/cyanogenmod/trebuchet/DragController;

    invoke-virtual {v0, p1}, Lcom/cyanogenmod/trebuchet/DragController;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 10
    .param p1    # Z
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/DragLayer;->getChildCount()I

    move-result v1

    const/4 v3, 0x0

    :goto_0
    if-lt v3, v1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0, v3}, Lcom/cyanogenmod/trebuchet/DragLayer;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout$LayoutParams;

    instance-of v5, v2, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;

    if-eqz v5, :cond_1

    move-object v4, v2

    check-cast v4, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;

    iget-boolean v5, v4, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;->customPosition:Z

    if-eqz v5, :cond_1

    iget v5, v4, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;->x:I

    iget v6, v4, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;->y:I

    iget v7, v4, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;->x:I

    iget v8, v4, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;->width:I

    add-int/2addr v7, v8

    iget v8, v4, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;->y:I

    iget v9, v4, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;->height:I

    add-int/2addr v8, v9

    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/view/View;->layout(IIII)V

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1    # Landroid/view/MotionEvent;

    const/4 v4, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    float-to-int v2, v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v3, v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    if-nez v5, :cond_1

    const/4 v5, 0x0

    invoke-direct {p0, p1, v5}, Lcom/cyanogenmod/trebuchet/DragLayer;->handleTouchDown(Landroid/view/MotionEvent;Z)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mCurrentResizeFrame:Lcom/cyanogenmod/trebuchet/AppWidgetResizeFrame;

    if-eqz v5, :cond_2

    const/4 v1, 0x1

    packed-switch v0, :pswitch_data_0

    :cond_2
    :goto_1
    if-nez v1, :cond_0

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDragController:Lcom/cyanogenmod/trebuchet/DragController;

    invoke-virtual {v4, p1}, Lcom/cyanogenmod/trebuchet/DragController;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v4

    goto :goto_0

    :pswitch_0
    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mCurrentResizeFrame:Lcom/cyanogenmod/trebuchet/AppWidgetResizeFrame;

    iget v6, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mXDown:I

    sub-int v6, v2, v6

    iget v7, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mYDown:I

    sub-int v7, v3, v7

    invoke-virtual {v5, v6, v7}, Lcom/cyanogenmod/trebuchet/AppWidgetResizeFrame;->visualizeResizeForDelta(II)V

    goto :goto_1

    :pswitch_1
    iget-object v5, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mCurrentResizeFrame:Lcom/cyanogenmod/trebuchet/AppWidgetResizeFrame;

    iget v6, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mXDown:I

    sub-int v6, v2, v6

    iget v7, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mYDown:I

    sub-int v7, v3, v7

    invoke-virtual {v5, v6, v7}, Lcom/cyanogenmod/trebuchet/AppWidgetResizeFrame;->commitResizeForDelta(II)V

    const/4 v5, 0x0

    iput-object v5, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mCurrentResizeFrame:Lcom/cyanogenmod/trebuchet/AppWidgetResizeFrame;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onViewAdded(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onViewAdded(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/DragLayer;->updateChildIndices()V

    return-void
.end method

.method protected onViewRemoved(Landroid/view/View;)V
    .locals 0
    .param p1    # Landroid/view/View;

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onViewRemoved(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/DragLayer;->updateChildIndices()V

    return-void
.end method

.method public setup(Lcom/cyanogenmod/trebuchet/Launcher;Lcom/cyanogenmod/trebuchet/DragController;)V
    .locals 0
    .param p1    # Lcom/cyanogenmod/trebuchet/Launcher;
    .param p2    # Lcom/cyanogenmod/trebuchet/DragController;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mLauncher:Lcom/cyanogenmod/trebuchet/Launcher;

    iput-object p2, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mDragController:Lcom/cyanogenmod/trebuchet/DragController;

    return-void
.end method

.method public showDelayedHint(Landroid/view/View;Ljava/lang/String;JJ)V
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # Ljava/lang/String;
    .param p3    # J
    .param p5    # J

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHintHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHideHintRunnalbe:Lcom/cyanogenmod/trebuchet/DragLayer$HideHintRunnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHintHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mShowHintRunnable:Lcom/cyanogenmod/trebuchet/DragLayer$ShowHintRunnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHintAnchor:Landroid/view/View;

    if-ne v2, p1, :cond_0

    iget-wide v2, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mLastShowHintMillis:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x1f4

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    const-string v2, "DragLayer"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "current("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") and last("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mLastShowHintMillis:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") too close, not showing"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iput-wide v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mLastShowHintMillis:J

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHintAnchor:Landroid/view/View;

    iput-object p2, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHintText:Ljava/lang/String;

    iput-wide p5, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHintDuration:J

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mHintHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/DragLayer;->mShowHintRunnable:Lcom/cyanogenmod/trebuchet/DragLayer$ShowHintRunnable;

    invoke-virtual {v2, v3, p3, p4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
