.class public Lcom/cyanogenmod/trebuchet/UserCenterMonitor;
.super Landroid/content/BroadcastReceiver;
.source "UserCenterMonitor.java"

# interfaces
.implements Lcom/cyanogenmod/trebuchet/ConnectivityService$IConnectivityMonitor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cyanogenmod/trebuchet/UserCenterMonitor$IUserCenterUpdateListener;,
        Lcom/cyanogenmod/trebuchet/UserCenterMonitor$QueryTask;
    }
.end annotation


# static fields
.field private static final ACTION_PASSPORT_MSG:Ljava/lang/String; = "com.konka.message.PASSPORT_MSG"

.field private static final ACTION_PASSPORT_SERVICE:Ljava/lang/String; = "com.konka.passport.service.USERINFO_SERVICE"

.field private static final ACTION_USERLOGIN:Ljava/lang/String; = "com.konka.passport.USERLOGIN"

.field private static final DEBUG:Z = true

.field private static final EXTRA_COUNT:Ljava/lang/String; = "count"

.field private static final EXTRA_HAS_MSG:Ljava/lang/String; = "hasMsg"

.field private static final EXTRA_STATUS:Ljava/lang/String; = "status"

.field private static final EXTRA_VALUE_LOGIN:Ljava/lang/String; = "login"

.field public static final KEY_USERCENTER_IS_LOGGEDIN:Ljava/lang/String; = "UserCenterMonitor.isLoggedIn"

.field public static final KEY_USERCENTER_MSG_COUNT:Ljava/lang/String; = "UserCenterMonitor.msgCount"

.field private static final TAG:Ljava/lang/String; = "wzd_usercenter"


# instance fields
.field private mContextRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentConnectivity:Landroid/os/Bundle;

.field private mIsLoggedIn:Z

.field private mListener:Lcom/cyanogenmod/trebuchet/UserCenterMonitor$IUserCenterUpdateListener;

.field private mMsgCount:I

.field private mServiceConn:Landroid/content/ServiceConnection;

.field private mUserInfo:Lcom/konka/passport/service/UserInfo;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/UserCenterMonitor$IUserCenterUpdateListener;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/cyanogenmod/trebuchet/UserCenterMonitor$IUserCenterUpdateListener;

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/UserCenterMonitor;->mIsLoggedIn:Z

    iput v0, p0, Lcom/cyanogenmod/trebuchet/UserCenterMonitor;->mMsgCount:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/UserCenterMonitor;->mCurrentConnectivity:Landroid/os/Bundle;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/UserCenterMonitor;->mContextRef:Ljava/lang/ref/WeakReference;

    iput-object p2, p0, Lcom/cyanogenmod/trebuchet/UserCenterMonitor;->mListener:Lcom/cyanogenmod/trebuchet/UserCenterMonitor$IUserCenterUpdateListener;

    new-instance v0, Lcom/cyanogenmod/trebuchet/UserCenterMonitor$1;

    invoke-direct {v0, p0}, Lcom/cyanogenmod/trebuchet/UserCenterMonitor$1;-><init>(Lcom/cyanogenmod/trebuchet/UserCenterMonitor;)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/UserCenterMonitor;->mServiceConn:Landroid/content/ServiceConnection;

    return-void
.end method

.method static synthetic access$0(Lcom/cyanogenmod/trebuchet/UserCenterMonitor;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/cyanogenmod/trebuchet/UserCenterMonitor;->mIsLoggedIn:Z

    return-void
.end method

.method static synthetic access$1(Lcom/cyanogenmod/trebuchet/UserCenterMonitor;)V
    .locals 0

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/UserCenterMonitor;->updateUserCenterState()V

    return-void
.end method

.method static synthetic access$2(Lcom/cyanogenmod/trebuchet/UserCenterMonitor;Lcom/konka/passport/service/UserInfo;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/UserCenterMonitor;->mUserInfo:Lcom/konka/passport/service/UserInfo;

    return-void
.end method

.method static synthetic access$3(Lcom/cyanogenmod/trebuchet/UserCenterMonitor;)Lcom/konka/passport/service/UserInfo;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/UserCenterMonitor;->mUserInfo:Lcom/konka/passport/service/UserInfo;

    return-object v0
.end method

.method private getCurrentConnectivity()Landroid/os/Bundle;
    .locals 3

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/UserCenterMonitor;->mCurrentConnectivity:Landroid/os/Bundle;

    const-string v1, "UserCenterMonitor.isLoggedIn"

    iget-boolean v2, p0, Lcom/cyanogenmod/trebuchet/UserCenterMonitor;->mIsLoggedIn:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/UserCenterMonitor;->mCurrentConnectivity:Landroid/os/Bundle;

    const-string v1, "UserCenterMonitor.msgCount"

    iget v2, p0, Lcom/cyanogenmod/trebuchet/UserCenterMonitor;->mMsgCount:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/UserCenterMonitor;->mCurrentConnectivity:Landroid/os/Bundle;

    return-object v0
.end method

.method private updateUserCenterState()V
    .locals 2

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/UserCenterMonitor;->mListener:Lcom/cyanogenmod/trebuchet/UserCenterMonitor$IUserCenterUpdateListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/UserCenterMonitor;->mListener:Lcom/cyanogenmod/trebuchet/UserCenterMonitor$IUserCenterUpdateListener;

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/UserCenterMonitor;->getCurrentConnectivity()Landroid/os/Bundle;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/cyanogenmod/trebuchet/UserCenterMonitor$IUserCenterUpdateListener;->onUpdateUserCenterConnectivity(Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v4, "com.konka.passport.USERLOGIN"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "status"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v4, "login"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    :goto_0
    iput-boolean v2, p0, Lcom/cyanogenmod/trebuchet/UserCenterMonitor;->mIsLoggedIn:Z

    iput v3, p0, Lcom/cyanogenmod/trebuchet/UserCenterMonitor;->mMsgCount:I

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/UserCenterMonitor;->updateUserCenterState()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v2, v3

    goto :goto_0

    :cond_2
    const-string v4, "com.konka.message.PASSPORT_MSG"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iput-boolean v2, p0, Lcom/cyanogenmod/trebuchet/UserCenterMonitor;->mIsLoggedIn:Z

    const-string v2, "count"

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/cyanogenmod/trebuchet/UserCenterMonitor;->mMsgCount:I

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/UserCenterMonitor;->updateUserCenterState()V

    goto :goto_1
.end method

.method public startMonitor()V
    .locals 5

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/UserCenterMonitor;->mContextRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "com.konka.passport.service.USERINFO_SERVICE"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/UserCenterMonitor;->mServiceConn:Landroid/content/ServiceConnection;

    const/4 v4, 0x1

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v3, "com.konka.passport.USERLOGIN"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "com.konka.message.PASSPORT_MSG"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {v0, p0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public stopMonitor()V
    .locals 4

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/UserCenterMonitor;->mContextRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "wzd_usercenter"

    const-string v3, "cathed exception:Unable to stop UserCenterMonitor"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
