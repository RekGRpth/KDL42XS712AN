.class Lcom/cyanogenmod/trebuchet/Launcher$25;
.super Landroid/animation/AnimatorListenerAdapter;
.source "Launcher.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cyanogenmod/trebuchet/Launcher;->dismissCling(Lcom/cyanogenmod/trebuchet/Cling;Ljava/lang/String;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/Launcher;

.field private final synthetic val$cling:Lcom/cyanogenmod/trebuchet/Cling;

.field private final synthetic val$flag:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/cyanogenmod/trebuchet/Launcher;Lcom/cyanogenmod/trebuchet/Cling;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/Launcher$25;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    iput-object p2, p0, Lcom/cyanogenmod/trebuchet/Launcher$25;->val$cling:Lcom/cyanogenmod/trebuchet/Cling;

    iput-object p3, p0, Lcom/cyanogenmod/trebuchet/Launcher$25;->val$flag:Ljava/lang/String;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 5
    .param p1    # Landroid/animation/Animator;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher$25;->val$cling:Lcom/cyanogenmod/trebuchet/Cling;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/cyanogenmod/trebuchet/Cling;->setVisibility(I)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher$25;->val$cling:Lcom/cyanogenmod/trebuchet/Cling;

    invoke-virtual {v2}, Lcom/cyanogenmod/trebuchet/Cling;->cleanup()V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher$25;->this$0:Lcom/cyanogenmod/trebuchet/Launcher;

    const-string v3, "com.cyanogenmod.trebuchet_preferences"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/cyanogenmod/trebuchet/Launcher;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/Launcher$25;->val$flag:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method
