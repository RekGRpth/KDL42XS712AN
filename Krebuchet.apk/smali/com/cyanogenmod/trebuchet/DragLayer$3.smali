.class Lcom/cyanogenmod/trebuchet/DragLayer$3;
.super Landroid/animation/AnimatorListenerAdapter;
.source "DragLayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/cyanogenmod/trebuchet/DragLayer;->animateView(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;FFILandroid/view/animation/Interpolator;Landroid/view/animation/Interpolator;Ljava/lang/Runnable;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/cyanogenmod/trebuchet/DragLayer;

.field private final synthetic val$fadeOut:Z

.field private final synthetic val$onCompleteRunnable:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/cyanogenmod/trebuchet/DragLayer;Ljava/lang/Runnable;Z)V
    .locals 0

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/DragLayer$3;->this$0:Lcom/cyanogenmod/trebuchet/DragLayer;

    iput-object p2, p0, Lcom/cyanogenmod/trebuchet/DragLayer$3;->val$onCompleteRunnable:Ljava/lang/Runnable;

    iput-boolean p3, p0, Lcom/cyanogenmod/trebuchet/DragLayer$3;->val$fadeOut:Z

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2
    .param p1    # Landroid/animation/Animator;

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer$3;->val$onCompleteRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer$3;->val$onCompleteRunnable:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    :cond_0
    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer$3;->val$fadeOut:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer$3;->this$0:Lcom/cyanogenmod/trebuchet/DragLayer;

    # invokes: Lcom/cyanogenmod/trebuchet/DragLayer;->fadeOutDragView()V
    invoke-static {v0}, Lcom/cyanogenmod/trebuchet/DragLayer;->access$10(Lcom/cyanogenmod/trebuchet/DragLayer;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragLayer$3;->this$0:Lcom/cyanogenmod/trebuchet/DragLayer;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/cyanogenmod/trebuchet/DragLayer;->access$11(Lcom/cyanogenmod/trebuchet/DragLayer;Landroid/view/View;)V

    goto :goto_0
.end method
