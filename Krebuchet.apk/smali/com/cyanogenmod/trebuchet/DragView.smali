.class public Lcom/cyanogenmod/trebuchet/DragView;
.super Landroid/view/View;
.source "DragView.java"


# instance fields
.field mAnim:Landroid/animation/ValueAnimator;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mDragLayer:Lcom/cyanogenmod/trebuchet/DragLayer;

.field private mDragRegion:Landroid/graphics/Rect;

.field private mDragVisualizeOffset:Landroid/graphics/Point;

.field private mHasDrawn:Z

.field private mLayoutParams:Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;

.field private mOffsetX:F

.field private mOffsetY:F

.field private mPaint:Landroid/graphics/Paint;

.field private mRegistrationX:I

.field private mRegistrationY:I


# direct methods
.method public constructor <init>(Lcom/cyanogenmod/trebuchet/Launcher;Landroid/graphics/Bitmap;IIIIII)V
    .locals 15
    .param p1    # Lcom/cyanogenmod/trebuchet/Launcher;
    .param p2    # Landroid/graphics/Bitmap;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # I

    invoke-direct/range {p0 .. p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/DragView;->mDragVisualizeOffset:Landroid/graphics/Point;

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/DragView;->mDragRegion:Landroid/graphics/Rect;

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/DragView;->mDragLayer:Lcom/cyanogenmod/trebuchet/DragLayer;

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/cyanogenmod/trebuchet/DragView;->mHasDrawn:Z

    const/4 v2, 0x0

    iput v2, p0, Lcom/cyanogenmod/trebuchet/DragView;->mOffsetX:F

    const/4 v2, 0x0

    iput v2, p0, Lcom/cyanogenmod/trebuchet/DragView;->mOffsetY:F

    invoke-virtual/range {p1 .. p1}, Lcom/cyanogenmod/trebuchet/Launcher;->getDragLayer()Lcom/cyanogenmod/trebuchet/DragLayer;

    move-result-object v2

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/DragView;->mDragLayer:Lcom/cyanogenmod/trebuchet/DragLayer;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/DragView;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v2, 0x7f0b0017    # com.konka.avenger.R.integer.config_dragViewExtraPixels

    invoke-virtual {v13, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    new-instance v7, Landroid/graphics/Matrix;

    invoke-direct {v7}, Landroid/graphics/Matrix;-><init>()V

    add-int v2, p7, v9

    div-int v2, v2, p7

    int-to-float v14, v2

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v14, v2

    if-eqz v2, :cond_0

    invoke-virtual {v7, v14, v14}, Landroid/graphics/Matrix;->setScale(FF)V

    :cond_0
    const v2, 0x7f0c0036    # com.konka.avenger.R.dimen.dragViewOffsetX

    invoke-virtual {v13, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    const v2, 0x7f0c0037    # com.konka.avenger.R.dimen.dragViewOffsetY

    invoke-virtual {v13, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v2

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/DragView;->mAnim:Landroid/animation/ValueAnimator;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/DragView;->mAnim:Landroid/animation/ValueAnimator;

    const-wide/16 v3, 0x6e

    invoke-virtual {v2, v3, v4}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/DragView;->mAnim:Landroid/animation/ValueAnimator;

    new-instance v3, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v4, 0x40200000    # 2.5f

    invoke-direct {v3, v4}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/DragView;->mAnim:Landroid/animation/ValueAnimator;

    new-instance v3, Lcom/cyanogenmod/trebuchet/DragView$1;

    invoke-direct {v3, p0, v11, v12}, Lcom/cyanogenmod/trebuchet/DragView$1;-><init>(Lcom/cyanogenmod/trebuchet/DragView;II)V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    const/4 v8, 0x1

    move-object/from16 v2, p2

    move/from16 v3, p5

    move/from16 v4, p6

    move/from16 v5, p7

    move/from16 v6, p8

    invoke-static/range {v2 .. v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/DragView;->mBitmap:Landroid/graphics/Bitmap;

    new-instance v2, Landroid/graphics/Rect;

    const/4 v3, 0x0

    const/4 v4, 0x0

    move/from16 v0, p7

    move/from16 v1, p8

    invoke-direct {v2, v3, v4, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p0, v2}, Lcom/cyanogenmod/trebuchet/DragView;->setDragRegion(Landroid/graphics/Rect;)V

    move/from16 v0, p3

    iput v0, p0, Lcom/cyanogenmod/trebuchet/DragView;->mRegistrationX:I

    move/from16 v0, p4

    iput v0, p0, Lcom/cyanogenmod/trebuchet/DragView;->mRegistrationY:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-virtual {p0, v10, v10}, Lcom/cyanogenmod/trebuchet/DragView;->measure(II)V

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method static synthetic access$0(Lcom/cyanogenmod/trebuchet/DragView;)F
    .locals 1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/DragView;->mOffsetX:F

    return v0
.end method

.method static synthetic access$1(Lcom/cyanogenmod/trebuchet/DragView;)F
    .locals 1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/DragView;->mOffsetY:F

    return v0
.end method

.method static synthetic access$2(Lcom/cyanogenmod/trebuchet/DragView;F)V
    .locals 0

    iput p1, p0, Lcom/cyanogenmod/trebuchet/DragView;->mOffsetX:F

    return-void
.end method

.method static synthetic access$3(Lcom/cyanogenmod/trebuchet/DragView;F)V
    .locals 0

    iput p1, p0, Lcom/cyanogenmod/trebuchet/DragView;->mOffsetY:F

    return-void
.end method

.method static synthetic access$4(Lcom/cyanogenmod/trebuchet/DragView;)Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragView;->mLayoutParams:Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;

    return-object v0
.end method

.method static synthetic access$5(Lcom/cyanogenmod/trebuchet/DragView;)Lcom/cyanogenmod/trebuchet/DragLayer;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragView;->mDragLayer:Lcom/cyanogenmod/trebuchet/DragLayer;

    return-object v0
.end method


# virtual methods
.method public getDragRegion()Landroid/graphics/Rect;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragView;->mDragRegion:Landroid/graphics/Rect;

    return-object v0
.end method

.method public getDragRegionHeight()I
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragView;->mDragRegion:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    return v0
.end method

.method public getDragRegionLeft()I
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragView;->mDragRegion:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    return v0
.end method

.method public getDragRegionTop()I
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragView;->mDragRegion:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    return v0
.end method

.method public getDragRegionWidth()I
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragView;->mDragRegion:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    return v0
.end method

.method public getDragVisualizeOffset()Landroid/graphics/Point;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragView;->mDragVisualizeOffset:Landroid/graphics/Point;

    return-object v0
.end method

.method getMotionPosition([I)[I
    .locals 4
    .param p1    # [I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragView;->mLayoutParams:Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;

    if-nez p1, :cond_0

    const/4 v1, 0x2

    new-array p1, v1, [I

    :cond_0
    const/4 v1, 0x0

    iget v2, v0, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;->x:I

    iget v3, p0, Lcom/cyanogenmod/trebuchet/DragView;->mRegistrationX:I

    add-int/2addr v2, v3

    iget v3, p0, Lcom/cyanogenmod/trebuchet/DragView;->mOffsetX:F

    float-to-int v3, v3

    sub-int/2addr v2, v3

    aput v2, p1, v1

    const/4 v1, 0x1

    iget v2, v0, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;->y:I

    iget v3, p0, Lcom/cyanogenmod/trebuchet/DragView;->mRegistrationY:I

    add-int/2addr v2, v3

    iget v3, p0, Lcom/cyanogenmod/trebuchet/DragView;->mOffsetY:F

    float-to-int v3, v3

    sub-int/2addr v2, v3

    aput v2, p1, v1

    return-object p1
.end method

.method public getOffsetY()F
    .locals 1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/DragView;->mOffsetY:F

    return v0
.end method

.method getPosition([I)[I
    .locals 3
    .param p1    # [I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragView;->mLayoutParams:Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;

    if-nez p1, :cond_0

    const/4 v1, 0x2

    new-array p1, v1, [I

    :cond_0
    const/4 v1, 0x0

    iget v2, v0, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;->x:I

    aput v2, p1, v1

    const/4 v1, 0x1

    iget v2, v0, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;->y:I

    aput v2, p1, v1

    return-object p1
.end method

.method public hasDrawn()Z
    .locals 1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/DragView;->mHasDrawn:Z

    return v0
.end method

.method move(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragView;->mLayoutParams:Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;

    iget v1, p0, Lcom/cyanogenmod/trebuchet/DragView;->mRegistrationX:I

    sub-int v1, p1, v1

    iget v2, p0, Lcom/cyanogenmod/trebuchet/DragView;->mOffsetX:F

    float-to-int v2, v2

    add-int/2addr v1, v2

    iput v1, v0, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;->x:I

    iget v1, p0, Lcom/cyanogenmod/trebuchet/DragView;->mRegistrationY:I

    sub-int v1, p2, v1

    iget v2, p0, Lcom/cyanogenmod/trebuchet/DragView;->mOffsetY:F

    float-to-int v2, v2

    add-int/2addr v1, v2

    iput v1, v0, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;->y:I

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/DragView;->mDragLayer:Lcom/cyanogenmod/trebuchet/DragLayer;

    invoke-virtual {v1}, Lcom/cyanogenmod/trebuchet/DragLayer;->requestLayout()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1    # Landroid/graphics/Canvas;

    const/4 v2, 0x0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/cyanogenmod/trebuchet/DragView;->mHasDrawn:Z

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragView;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/DragView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/DragView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/cyanogenmod/trebuchet/DragView;->setMeasuredDimension(II)V

    return-void
.end method

.method remove()V
    .locals 1

    new-instance v0, Lcom/cyanogenmod/trebuchet/DragView$2;

    invoke-direct {v0, p0}, Lcom/cyanogenmod/trebuchet/DragView$2;-><init>(Lcom/cyanogenmod/trebuchet/DragView;)V

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/DragView;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public setAlpha(F)V
    .locals 2
    .param p1    # F

    invoke-super {p0, p1}, Landroid/view/View;->setAlpha(F)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragView;->mPaint:Landroid/graphics/Paint;

    if-nez v0, :cond_0

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/DragView;->mPaint:Landroid/graphics/Paint;

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/DragView;->mPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v1, p1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/DragView;->invalidate()V

    return-void
.end method

.method public setDragRegion(Landroid/graphics/Rect;)V
    .locals 0
    .param p1    # Landroid/graphics/Rect;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/DragView;->mDragRegion:Landroid/graphics/Rect;

    return-void
.end method

.method public setDragVisualizeOffset(Landroid/graphics/Point;)V
    .locals 0
    .param p1    # Landroid/graphics/Point;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/DragView;->mDragVisualizeOffset:Landroid/graphics/Point;

    return-void
.end method

.method public setPaint(Landroid/graphics/Paint;)V
    .locals 0
    .param p1    # Landroid/graphics/Paint;

    iput-object p1, p0, Lcom/cyanogenmod/trebuchet/DragView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/cyanogenmod/trebuchet/DragView;->invalidate()V

    return-void
.end method

.method public show(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/DragView;->mDragLayer:Lcom/cyanogenmod/trebuchet/DragLayer;

    invoke-virtual {v1, p0}, Lcom/cyanogenmod/trebuchet/DragLayer;->addView(Landroid/view/View;)V

    new-instance v0, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;

    invoke-direct {v0, v2, v2}, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/DragView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iput v1, v0, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;->width:I

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/DragView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    iput v1, v0, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;->height:I

    iget v1, p0, Lcom/cyanogenmod/trebuchet/DragView;->mRegistrationX:I

    sub-int v1, p1, v1

    iput v1, v0, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;->x:I

    iget v1, p0, Lcom/cyanogenmod/trebuchet/DragView;->mRegistrationY:I

    sub-int v1, p2, v1

    iput v1, v0, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;->y:I

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;->customPosition:Z

    invoke-virtual {p0, v0}, Lcom/cyanogenmod/trebuchet/DragView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iput-object v0, p0, Lcom/cyanogenmod/trebuchet/DragView;->mLayoutParams:Lcom/cyanogenmod/trebuchet/DragLayer$LayoutParams;

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/DragView;->mAnim:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    return-void
.end method
