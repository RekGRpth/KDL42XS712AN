.class public Lcom/cyanogenmod/trebuchet/NetworkMonitor;
.super Landroid/content/BroadcastReceiver;
.source "NetworkMonitor.java"

# interfaces
.implements Lcom/cyanogenmod/trebuchet/ConnectivityService$IConnectivityMonitor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;,
        Lcom/cyanogenmod/trebuchet/NetworkMonitor$MonitorThreadHandler;,
        Lcom/cyanogenmod/trebuchet/NetworkMonitor$NetworkMonitorHandler;,
        Lcom/cyanogenmod/trebuchet/NetworkMonitor$WifiHandler;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field public static final ID_INTERFACE_ETHERNET:I = 0x0

.field public static final ID_INTERFACE_WIFI:I = 0x1

.field public static final ID_STATUS_CONNECTED:I = 0x2

.field public static final ID_STATUS_DISCONNECTED:I = 0x0

.field public static final ID_STATUS_UNREACHABLE:I = 0x1

.field private static final INET_CONDITION_THRESHOLD:I = 0x32

.field public static final KEY_NET_INTERFACE_ID:Ljava/lang/String; = "NetworkMonitor.interfaceId"

.field public static final KEY_NET_STATUS_ID:Ljava/lang/String; = "NetworkMonitor.Status"

.field public static final KEY_NET_WIFI_CONDICTION:Ljava/lang/String; = "NetworkMonitor.wifi.Condiction"

.field public static final KEY_NET_WIFI_LEVEL:Ljava/lang/String; = "NetworkMonitor.wifi.level"

.field private static final MSG_SET_NET_REACHABLE_STATUS:I = 0x3e9

.field private static final MSG_TEST_REACHABLE:I = 0x7d1

.field private static final REACHABLE_TEST_DELAY_MILLIS:I = 0x1388

.field private static final TAG:Ljava/lang/String; = "Krebuchet.NetworkMonitor"

.field private static final mWifiLevelCount:I = 0x5


# instance fields
.field private mActiveInterface:I

.field private mActiveStatus:I

.field private mContextRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private mCurrentConnectivity:Landroid/os/Bundle;

.field private mEthHWConnected:Z

.field private mEthReachable:Z

.field private mEthernetManager:Landroid/net/ethernet/EthernetManager;

.field private mInetCondition:I

.field private mListener:Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;

.field private mMonitorHandler:Lcom/cyanogenmod/trebuchet/NetworkMonitor$NetworkMonitorHandler;

.field private mMonitorThread:Landroid/os/HandlerThread;

.field private mMonitorThreadHandler:Lcom/cyanogenmod/trebuchet/NetworkMonitor$MonitorThreadHandler;

.field private mWifiActivity:I

.field private mWifiChannel:Lcom/android/internal/util/AsyncChannel;

.field private mWifiConnected:Z

.field private mWifiEnabled:Z

.field private mWifiLevel:I

.field private final mWifiManager:Landroid/net/wifi/WifiManager;

.field private mWifiRssi:I

.field private mWifiSsid:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    iput v3, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mActiveInterface:I

    iput v3, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mActiveStatus:I

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mCurrentConnectivity:Landroid/os/Bundle;

    iput v3, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mWifiActivity:I

    iput v3, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mInetCondition:I

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mContextRef:Ljava/lang/ref/WeakReference;

    iput-object p2, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mListener:Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;

    new-instance v2, Lcom/cyanogenmod/trebuchet/NetworkMonitor$NetworkMonitorHandler;

    invoke-direct {v2, p0, v4}, Lcom/cyanogenmod/trebuchet/NetworkMonitor$NetworkMonitorHandler;-><init>(Lcom/cyanogenmod/trebuchet/NetworkMonitor;Lcom/cyanogenmod/trebuchet/NetworkMonitor$NetworkMonitorHandler;)V

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mMonitorHandler:Lcom/cyanogenmod/trebuchet/NetworkMonitor$NetworkMonitorHandler;

    invoke-static {}, Landroid/net/ethernet/EthernetManager;->getInstance()Landroid/net/ethernet/EthernetManager;

    move-result-object v2

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mEthernetManager:Landroid/net/ethernet/EthernetManager;

    const-string v2, "wifi"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/WifiManager;

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mWifiManager:Landroid/net/wifi/WifiManager;

    new-instance v0, Lcom/cyanogenmod/trebuchet/NetworkMonitor$WifiHandler;

    invoke-direct {v0, p0, v4}, Lcom/cyanogenmod/trebuchet/NetworkMonitor$WifiHandler;-><init>(Lcom/cyanogenmod/trebuchet/NetworkMonitor;Lcom/cyanogenmod/trebuchet/NetworkMonitor$WifiHandler;)V

    new-instance v2, Lcom/android/internal/util/AsyncChannel;

    invoke-direct {v2}, Lcom/android/internal/util/AsyncChannel;-><init>()V

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mWifiChannel:Lcom/android/internal/util/AsyncChannel;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->getWifiServiceMessenger()Landroid/os/Messenger;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mWifiChannel:Lcom/android/internal/util/AsyncChannel;

    invoke-virtual {v2, p1, v0, v1}, Lcom/android/internal/util/AsyncChannel;->connect(Landroid/content/Context;Landroid/os/Handler;Landroid/os/Messenger;)V

    :cond_0
    return-void
.end method

.method static synthetic access$0(Lcom/cyanogenmod/trebuchet/NetworkMonitor;)Z
    .locals 1

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->testReachable()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1(Lcom/cyanogenmod/trebuchet/NetworkMonitor;)Lcom/cyanogenmod/trebuchet/NetworkMonitor$NetworkMonitorHandler;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mMonitorHandler:Lcom/cyanogenmod/trebuchet/NetworkMonitor$NetworkMonitorHandler;

    return-object v0
.end method

.method static synthetic access$10(Lcom/cyanogenmod/trebuchet/NetworkMonitor;)I
    .locals 1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mWifiActivity:I

    return v0
.end method

.method static synthetic access$11(Lcom/cyanogenmod/trebuchet/NetworkMonitor;I)V
    .locals 0

    iput p1, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mWifiActivity:I

    return-void
.end method

.method static synthetic access$2(Lcom/cyanogenmod/trebuchet/NetworkMonitor;)I
    .locals 1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mActiveInterface:I

    return v0
.end method

.method static synthetic access$3(Lcom/cyanogenmod/trebuchet/NetworkMonitor;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mEthHWConnected:Z

    return v0
.end method

.method static synthetic access$4(Lcom/cyanogenmod/trebuchet/NetworkMonitor;I)V
    .locals 0

    iput p1, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mActiveStatus:I

    return-void
.end method

.method static synthetic access$5(Lcom/cyanogenmod/trebuchet/NetworkMonitor;)I
    .locals 1

    iget v0, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mActiveStatus:I

    return v0
.end method

.method static synthetic access$6(Lcom/cyanogenmod/trebuchet/NetworkMonitor;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mEthReachable:Z

    return-void
.end method

.method static synthetic access$7(Lcom/cyanogenmod/trebuchet/NetworkMonitor;)Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mListener:Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;

    return-object v0
.end method

.method static synthetic access$8(Lcom/cyanogenmod/trebuchet/NetworkMonitor;)Landroid/os/Bundle;
    .locals 1

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->getCurrentConnectivityInfo()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$9(Lcom/cyanogenmod/trebuchet/NetworkMonitor;)Lcom/android/internal/util/AsyncChannel;
    .locals 1

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mWifiChannel:Lcom/android/internal/util/AsyncChannel;

    return-object v0
.end method

.method private getCurrentConnectivityInfo()Landroid/os/Bundle;
    .locals 3

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mCurrentConnectivity:Landroid/os/Bundle;

    const-string v1, "NetworkMonitor.interfaceId"

    iget v2, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mActiveInterface:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mCurrentConnectivity:Landroid/os/Bundle;

    const-string v1, "NetworkMonitor.Status"

    iget v2, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mActiveStatus:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget v0, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mActiveInterface:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mCurrentConnectivity:Landroid/os/Bundle;

    const-string v1, "NetworkMonitor.wifi.Condiction"

    iget v2, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mInetCondition:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mCurrentConnectivity:Landroid/os/Bundle;

    const-string v1, "NetworkMonitor.wifi.level"

    iget v2, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mWifiLevel:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    iget-object v0, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mCurrentConnectivity:Landroid/os/Bundle;

    return-object v0
.end method

.method private huntForSsid(Landroid/net/wifi/WifiInfo;)Ljava/lang/String;
    .locals 6
    .param p1    # Landroid/net/wifi/WifiInfo;

    invoke-virtual {p1}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v2

    :cond_0
    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->getConfiguredNetworks()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiConfiguration;

    iget v4, v0, Landroid/net/wifi/WifiConfiguration;->networkId:I

    invoke-virtual {p1}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    move-result v5

    if-ne v4, v5, :cond_1

    iget-object v2, v0, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    goto :goto_0
.end method

.method private testReachable()Z
    .locals 7

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mEthernetManager:Landroid/net/ethernet/EthernetManager;

    if-nez v6, :cond_1

    :cond_0
    :goto_0
    return v5

    :cond_1
    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mEthernetManager:Landroid/net/ethernet/EthernetManager;

    invoke-virtual {v6}, Landroid/net/ethernet/EthernetManager;->isConfigured()Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mEthernetManager:Landroid/net/ethernet/EthernetManager;

    invoke-virtual {v6}, Landroid/net/ethernet/EthernetManager;->getSavedConfig()Landroid/net/ethernet/EthernetDevInfo;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/ethernet/EthernetDevInfo;->getRouteAddr()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Landroid/net/ethernet/EthernetDevInfo;->getConnectMode()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    const-string v6, "manual"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-static {v4}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v2

    const/16 v6, 0x3e8

    invoke-virtual {v2, v6}, Ljava/net/InetAddress;->isReachable(I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    goto :goto_0

    :cond_2
    const/4 v5, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private updateActiveNetwork()V
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mWifiEnabled:Z

    if-eqz v0, :cond_1

    iput v1, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mActiveInterface:I

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mWifiConnected:Z

    if-eqz v0, :cond_0

    iput v3, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mActiveStatus:I

    :goto_0
    return-void

    :cond_0
    iput v1, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mActiveStatus:I

    goto :goto_0

    :cond_1
    iput v2, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mActiveInterface:I

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mEthHWConnected:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mEthReachable:Z

    if-eqz v0, :cond_2

    iput v3, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mActiveStatus:I

    goto :goto_0

    :cond_2
    iput v1, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mActiveStatus:I

    goto :goto_0

    :cond_3
    iput v2, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mActiveStatus:I

    goto :goto_0
.end method

.method private updateConnectivity(Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Intent;

    const/4 v2, 0x0

    const-string v3, "networkInfo"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/NetworkInfo;

    const-string v3, "inetCondition"

    invoke-virtual {p1, v3, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const/16 v3, 0x32

    if-le v0, v3, :cond_0

    const/4 v2, 0x1

    :cond_0
    iput v2, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mInetCondition:I

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->updateActiveNetwork()V

    return-void
.end method

.method private updateEthernetState(Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Intent;

    const/4 v3, 0x0

    const-string v2, "ETHERNET_state"

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    iput-boolean v3, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mEthHWConnected:Z

    :goto_0
    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->updateActiveNetwork()V

    return-void

    :pswitch_1
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mEthHWConnected:Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private updateWifiState(Landroid/content/Intent;)V
    .locals 9
    .param p1    # Landroid/content/Intent;

    const/4 v8, 0x0

    const/4 v4, 0x1

    const/16 v7, -0xc8

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v6, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const-string v6, "wifi_state"

    const/4 v7, 0x4

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    const/4 v7, 0x3

    if-ne v6, v7, :cond_1

    :goto_0
    iput-boolean v4, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mWifiEnabled:Z

    :cond_0
    :goto_1
    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->updateActiveNetwork()V

    return-void

    :cond_1
    move v4, v5

    goto :goto_0

    :cond_2
    const-string v6, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    const-string v6, "networkInfo"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/net/NetworkInfo;

    iget-boolean v3, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mWifiConnected:Z

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v6

    if-eqz v6, :cond_5

    :goto_2
    iput-boolean v4, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mWifiConnected:Z

    iget-boolean v4, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mWifiConnected:Z

    if-eqz v4, :cond_7

    if-nez v3, :cond_7

    const-string v4, "wifiInfo"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiInfo;

    if-nez v1, :cond_3

    iget-object v4, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mWifiManager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v1

    :cond_3
    if-eqz v1, :cond_6

    invoke-direct {p0, v1}, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->huntForSsid(Landroid/net/wifi/WifiInfo;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mWifiSsid:Ljava/lang/String;

    :cond_4
    :goto_3
    iput v5, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mWifiLevel:I

    iput v7, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mWifiRssi:I

    goto :goto_1

    :cond_5
    move v4, v5

    goto :goto_2

    :cond_6
    iput-object v8, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mWifiSsid:Ljava/lang/String;

    goto :goto_3

    :cond_7
    iget-boolean v4, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mWifiConnected:Z

    if-nez v4, :cond_4

    iput-object v8, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mWifiSsid:Ljava/lang/String;

    goto :goto_3

    :cond_8
    const-string v4, "android.net.wifi.RSSI_CHANGED"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-boolean v4, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mWifiConnected:Z

    if-eqz v4, :cond_0

    const-string v4, "newRssi"

    invoke-virtual {p1, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    iput v4, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mWifiRssi:I

    iget v4, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mWifiRssi:I

    const/4 v5, 0x5

    invoke-static {v4, v5}, Landroid/net/wifi/WifiManager;->calculateSignalLevel(II)I

    move-result v4

    iput v4, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mWifiLevel:I

    goto :goto_1
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/16 v2, 0x7d1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.net.ethernet.ETHERNET_STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0, p2}, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->updateEthernetState(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mMonitorThreadHandler:Lcom/cyanogenmod/trebuchet/NetworkMonitor$MonitorThreadHandler;

    invoke-virtual {v1, v2}, Lcom/cyanogenmod/trebuchet/NetworkMonitor$MonitorThreadHandler;->removeMessages(I)V

    iget-boolean v1, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mEthHWConnected:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mMonitorThreadHandler:Lcom/cyanogenmod/trebuchet/NetworkMonitor$MonitorThreadHandler;

    invoke-virtual {v1, v2}, Lcom/cyanogenmod/trebuchet/NetworkMonitor$MonitorThreadHandler;->sendEmptyMessage(I)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mListener:Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mListener:Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->getCurrentConnectivityInfo()Landroid/os/Bundle;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;->onUpdateNetworkConnectivity(Landroid/os/Bundle;)V

    goto :goto_0

    :cond_2
    const-string v1, "android.net.wifi.RSSI_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    invoke-direct {p0, p2}, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->updateWifiState(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mListener:Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mListener:Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->getCurrentConnectivityInfo()Landroid/os/Bundle;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;->onUpdateNetworkConnectivity(Landroid/os/Bundle;)V

    goto :goto_0

    :cond_4
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "android.net.conn.INET_CONDITION_ACTION"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_5
    invoke-direct {p0, p2}, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->updateConnectivity(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mListener:Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mListener:Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->getCurrentConnectivityInfo()Landroid/os/Bundle;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;->onUpdateNetworkConnectivity(Landroid/os/Bundle;)V

    goto :goto_0

    :cond_6
    const-string v1, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mListener:Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mListener:Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;

    invoke-direct {p0}, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->getCurrentConnectivityInfo()Landroid/os/Bundle;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/cyanogenmod/trebuchet/NetworkMonitor$INetworkUpdateListener;->onUpdateNetworkConnectivity(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public startMonitor()V
    .locals 5

    new-instance v2, Landroid/os/HandlerThread;

    const-string v3, "NetworkMonitor"

    const/16 v4, 0xa

    invoke-direct {v2, v3, v4}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mMonitorThread:Landroid/os/HandlerThread;

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mMonitorThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V

    new-instance v2, Lcom/cyanogenmod/trebuchet/NetworkMonitor$MonitorThreadHandler;

    iget-object v3, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mMonitorThread:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/cyanogenmod/trebuchet/NetworkMonitor$MonitorThreadHandler;-><init>(Lcom/cyanogenmod/trebuchet/NetworkMonitor;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mMonitorThreadHandler:Lcom/cyanogenmod/trebuchet/NetworkMonitor$MonitorThreadHandler;

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "android.net.ethernet.ETHERNET_STATE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.net.wifi.RSSI_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.net.conn.INET_CONDITION_ACTION"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mContextRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_0
    return-void
.end method

.method public stopMonitor()V
    .locals 4

    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mContextRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {v0, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/cyanogenmod/trebuchet/NetworkMonitor;->mMonitorThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->quit()Z

    return-void

    :catch_0
    move-exception v1

    const-string v2, "Krebuchet.NetworkMonitor"

    const-string v3, "cathed exception:Unable to stop NetworkMonitor"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
