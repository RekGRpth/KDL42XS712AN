.class public Landroid/net/ethernet/EthernetNative;
.super Ljava/lang/Object;
.source "EthernetNative.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "EthernetNative"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    :try_start_0
    const-string v1, "ethernet_jni"

    invoke-static {v1}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "EthernetNative"

    const-string v2, "Failed to load libethernet_jni.so while initializing android.net.ethernet; exception was "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native getInterfaceCnt()I
.end method

.method public static native getInterfaceName(I)Ljava/lang/String;
.end method

.method public static native getMacAddress()Ljava/lang/String;
.end method

.method public static native initEthernetNative()I
.end method

.method public static native isPppoeUp()I
.end method

.method public static native waitForEvent()Ljava/lang/String;
.end method
