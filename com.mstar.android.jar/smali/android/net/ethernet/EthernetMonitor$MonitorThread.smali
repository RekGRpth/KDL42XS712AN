.class Landroid/net/ethernet/EthernetMonitor$MonitorThread;
.super Ljava/lang/Thread;
.source "EthernetMonitor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/ethernet/EthernetMonitor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MonitorThread"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/net/ethernet/EthernetMonitor;


# direct methods
.method public constructor <init>(Landroid/net/ethernet/EthernetMonitor;)V
    .locals 1

    iput-object p1, p0, Landroid/net/ethernet/EthernetMonitor$MonitorThread;->this$0:Landroid/net/ethernet/EthernetMonitor;

    const-string v0, "EthMonitor"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method handleEvent(Ljava/lang/String;I)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # I

    packed-switch p2, :pswitch_data_0

    iget-object v0, p0, Landroid/net/ethernet/EthernetMonitor$MonitorThread;->this$0:Landroid/net/ethernet/EthernetMonitor;

    # getter for: Landroid/net/ethernet/EthernetMonitor;->mTracker:Landroid/net/ethernet/EthernetStateTracker;
    invoke-static {v0}, Landroid/net/ethernet/EthernetMonitor;->access$000(Landroid/net/ethernet/EthernetMonitor;)Landroid/net/ethernet/EthernetStateTracker;

    move-result-object v0

    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->FAILED:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v0, p1, v1}, Landroid/net/ethernet/EthernetStateTracker;->notifyStateChange(Ljava/lang/String;Landroid/net/NetworkInfo$DetailedState;)V

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Landroid/net/ethernet/EthernetMonitor$MonitorThread;->this$0:Landroid/net/ethernet/EthernetMonitor;

    # getter for: Landroid/net/ethernet/EthernetMonitor;->mTracker:Landroid/net/ethernet/EthernetStateTracker;
    invoke-static {v0}, Landroid/net/ethernet/EthernetMonitor;->access$000(Landroid/net/ethernet/EthernetMonitor;)Landroid/net/ethernet/EthernetStateTracker;

    move-result-object v0

    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v0, p1, v1}, Landroid/net/ethernet/EthernetStateTracker;->notifyStateChange(Ljava/lang/String;Landroid/net/NetworkInfo$DetailedState;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Landroid/net/ethernet/EthernetMonitor$MonitorThread;->this$0:Landroid/net/ethernet/EthernetMonitor;

    # getter for: Landroid/net/ethernet/EthernetMonitor;->mTracker:Landroid/net/ethernet/EthernetStateTracker;
    invoke-static {v0}, Landroid/net/ethernet/EthernetMonitor;->access$000(Landroid/net/ethernet/EthernetMonitor;)Landroid/net/ethernet/EthernetStateTracker;

    move-result-object v0

    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTED:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v0, p1, v1}, Landroid/net/ethernet/EthernetStateTracker;->notifyStateChange(Ljava/lang/String;Landroid/net/NetworkInfo$DetailedState;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Landroid/net/ethernet/EthernetMonitor$MonitorThread;->this$0:Landroid/net/ethernet/EthernetMonitor;

    # getter for: Landroid/net/ethernet/EthernetMonitor;->mTracker:Landroid/net/ethernet/EthernetStateTracker;
    invoke-static {v0}, Landroid/net/ethernet/EthernetMonitor;->access$000(Landroid/net/ethernet/EthernetMonitor;)Landroid/net/ethernet/EthernetStateTracker;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/ethernet/EthernetStateTracker;->notifyPhyConnected(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Landroid/net/ethernet/EthernetMonitor$MonitorThread;->this$0:Landroid/net/ethernet/EthernetMonitor;

    # getter for: Landroid/net/ethernet/EthernetMonitor;->mTracker:Landroid/net/ethernet/EthernetStateTracker;
    invoke-static {v0}, Landroid/net/ethernet/EthernetMonitor;->access$000(Landroid/net/ethernet/EthernetMonitor;)Landroid/net/ethernet/EthernetStateTracker;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/ethernet/EthernetStateTracker;->notifyAddressRemove(Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public run()V
    .locals 9

    const/4 v8, -0x1

    :cond_0
    const-string v5, "EthernetMonitor"

    const-string v6, "go poll events"

    invoke-static {v5, v6}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Landroid/net/ethernet/EthernetNative;->waitForEvent()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string v5, "EthernetMonitor"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "get event "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v3, v5, :cond_0

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-eq v4, v8, :cond_0

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v5, v4, 0x1

    add-int/2addr v3, v5

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    if-eq v4, v8, :cond_0

    add-int v5, v3, v4

    invoke-virtual {v2, v3, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v5, v4, 0x1

    add-int/2addr v3, v5

    const-string v5, "EthernetMonitor"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "dev: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " ev "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Slog;->v(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    const/4 v5, 0x3

    invoke-virtual {p0, v1, v5}, Landroid/net/ethernet/EthernetMonitor$MonitorThread;->handleEvent(Ljava/lang/String;I)V

    goto :goto_0

    :pswitch_2
    const/4 v5, 0x2

    invoke-virtual {p0, v1, v5}, Landroid/net/ethernet/EthernetMonitor$MonitorThread;->handleEvent(Ljava/lang/String;I)V

    goto :goto_0

    :pswitch_3
    const/4 v5, 0x1

    invoke-virtual {p0, v1, v5}, Landroid/net/ethernet/EthernetMonitor$MonitorThread;->handleEvent(Ljava/lang/String;I)V

    goto :goto_0

    :pswitch_4
    const/4 v5, 0x4

    invoke-virtual {p0, v1, v5}, Landroid/net/ethernet/EthernetMonitor$MonitorThread;->handleEvent(Ljava/lang/String;I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
