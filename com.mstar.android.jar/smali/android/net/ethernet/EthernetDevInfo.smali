.class public Landroid/net/ethernet/EthernetDevInfo;
.super Ljava/lang/Object;
.source "EthernetDevInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/net/ethernet/EthernetDevInfo;",
            ">;"
        }
    .end annotation
.end field

.field public static final ETHERNET_CONN_MODE_DHCP:Ljava/lang/String; = "dhcp"

.field public static final ETHERNET_CONN_MODE_MANUAL:Ljava/lang/String; = "manual"


# instance fields
.field private dev_name:Ljava/lang/String;

.field private dns:Ljava/lang/String;

.field private dns2:Ljava/lang/String;

.field private ipaddr:Ljava/lang/String;

.field private mode:Ljava/lang/String;

.field private netmask:Ljava/lang/String;

.field private proxy_host:Ljava/lang/String;

.field private proxy_on:I

.field private proxy_port:Ljava/lang/String;

.field private route:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/net/ethernet/EthernetDevInfo$1;

    invoke-direct {v0}, Landroid/net/ethernet/EthernetDevInfo$1;-><init>()V

    sput-object v0, Landroid/net/ethernet/EthernetDevInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Landroid/net/ethernet/EthernetDevInfo;->dev_name:Ljava/lang/String;

    iput-object v1, p0, Landroid/net/ethernet/EthernetDevInfo;->ipaddr:Ljava/lang/String;

    iput-object v1, p0, Landroid/net/ethernet/EthernetDevInfo;->netmask:Ljava/lang/String;

    iput-object v1, p0, Landroid/net/ethernet/EthernetDevInfo;->route:Ljava/lang/String;

    iput-object v1, p0, Landroid/net/ethernet/EthernetDevInfo;->dns:Ljava/lang/String;

    iput-object v1, p0, Landroid/net/ethernet/EthernetDevInfo;->dns2:Ljava/lang/String;

    const-string v0, "dhcp"

    iput-object v0, p0, Landroid/net/ethernet/EthernetDevInfo;->mode:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Landroid/net/ethernet/EthernetDevInfo;->proxy_on:I

    iput-object v1, p0, Landroid/net/ethernet/EthernetDevInfo;->proxy_host:Ljava/lang/String;

    iput-object v1, p0, Landroid/net/ethernet/EthernetDevInfo;->proxy_port:Ljava/lang/String;

    return-void
.end method

.method private loadFileAsString(Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v1, Ljava/lang/StringBuffer;

    const/16 v5, 0x3e8

    invoke-direct {v1, v5}, Ljava/lang/StringBuffer;-><init>(I)V

    new-instance v4, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/FileReader;

    invoke-direct {v5, p1}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v4, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    const/16 v5, 0x400

    new-array v0, v5, [C

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v4, v0}, Ljava/io/BufferedReader;->read([C)I

    move-result v2

    const/4 v5, -0x1

    if-eq v2, v5, :cond_0

    const/4 v5, 0x0

    invoke-static {v0, v5, v2}, Ljava/lang/String;->valueOf([CII)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :cond_0
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getConnectMode()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/net/ethernet/EthernetDevInfo;->mode:Ljava/lang/String;

    return-object v0
.end method

.method public getDns2Addr()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/net/ethernet/EthernetDevInfo;->dns2:Ljava/lang/String;

    return-object v0
.end method

.method public getDnsAddr()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/net/ethernet/EthernetDevInfo;->dns:Ljava/lang/String;

    return-object v0
.end method

.method public getIfName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/net/ethernet/EthernetDevInfo;->dev_name:Ljava/lang/String;

    return-object v0
.end method

.method public getIpAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/net/ethernet/EthernetDevInfo;->ipaddr:Ljava/lang/String;

    return-object v0
.end method

.method public getMacAddress()Ljava/lang/String;
    .locals 4

    :try_start_0
    const-string v1, "/sys/class/net/eth0/address"

    invoke-direct {p0, v1}, Landroid/net/ethernet/EthernetDevInfo;->loadFileAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/16 v3, 0x11

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getNetMask()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/net/ethernet/EthernetDevInfo;->netmask:Ljava/lang/String;

    return-object v0
.end method

.method public getProxyHost()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/net/ethernet/EthernetDevInfo;->proxy_host:Ljava/lang/String;

    return-object v0
.end method

.method public getProxyOn()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Landroid/net/ethernet/EthernetDevInfo;->proxy_on:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getProxyPort()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/net/ethernet/EthernetDevInfo;->proxy_port:Ljava/lang/String;

    return-object v0
.end method

.method public getRouteAddr()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/net/ethernet/EthernetDevInfo;->route:Ljava/lang/String;

    return-object v0
.end method

.method public setConnectMode(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_1

    const-string v0, "dhcp"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "manual"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iput-object p1, p0, Landroid/net/ethernet/EthernetDevInfo;->mode:Ljava/lang/String;

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDns2Addr(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    iput-object p1, p0, Landroid/net/ethernet/EthernetDevInfo;->dns2:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setDnsAddr(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    iput-object p1, p0, Landroid/net/ethernet/EthernetDevInfo;->dns:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setIfName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    iput-object p1, p0, Landroid/net/ethernet/EthernetDevInfo;->dev_name:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setIpAddress(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    iput-object p1, p0, Landroid/net/ethernet/EthernetDevInfo;->ipaddr:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setNetMask(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    iput-object p1, p0, Landroid/net/ethernet/EthernetDevInfo;->netmask:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setProxyHost(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    iput-object p1, p0, Landroid/net/ethernet/EthernetDevInfo;->proxy_host:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setProxyOn(Z)V
    .locals 1
    .param p1    # Z

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, p0, Landroid/net/ethernet/EthernetDevInfo;->proxy_on:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setProxyPort(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    iput-object p1, p0, Landroid/net/ethernet/EthernetDevInfo;->proxy_port:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public setRouteAddr(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    iput-object p1, p0, Landroid/net/ethernet/EthernetDevInfo;->route:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Landroid/net/ethernet/EthernetDevInfo;->dev_name:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/net/ethernet/EthernetDevInfo;->ipaddr:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/net/ethernet/EthernetDevInfo;->netmask:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/net/ethernet/EthernetDevInfo;->route:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/net/ethernet/EthernetDevInfo;->dns:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/net/ethernet/EthernetDevInfo;->dns2:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/net/ethernet/EthernetDevInfo;->mode:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Landroid/net/ethernet/EthernetDevInfo;->proxy_on:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Landroid/net/ethernet/EthernetDevInfo;->proxy_host:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/net/ethernet/EthernetDevInfo;->proxy_port:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
