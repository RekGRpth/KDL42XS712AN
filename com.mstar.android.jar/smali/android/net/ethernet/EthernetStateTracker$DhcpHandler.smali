.class Landroid/net/ethernet/EthernetStateTracker$DhcpHandler;
.super Landroid/os/Handler;
.source "EthernetStateTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/ethernet/EthernetStateTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DhcpHandler"
.end annotation


# instance fields
.field final synthetic this$0:Landroid/net/ethernet/EthernetStateTracker;


# direct methods
.method public constructor <init>(Landroid/net/ethernet/EthernetStateTracker;Landroid/os/Looper;Landroid/os/Handler;)V
    .locals 0
    .param p2    # Landroid/os/Looper;
    .param p3    # Landroid/os/Handler;

    iput-object p1, p0, Landroid/net/ethernet/EthernetStateTracker$DhcpHandler;->this$0:Landroid/net/ethernet/EthernetStateTracker;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    # setter for: Landroid/net/ethernet/EthernetStateTracker;->mTrackerTarget:Landroid/os/Handler;
    invoke-static {p1, p3}, Landroid/net/ethernet/EthernetStateTracker;->access$002(Landroid/net/ethernet/EthernetStateTracker;Landroid/os/Handler;)Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public PreDhcpStart()V
    .locals 3

    :goto_0
    const-string v1, "init.svc.bootanim"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "stopped"

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    const-wide/16 v1, 0x1

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1    # Landroid/os/Message;

    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Landroid/net/ethernet/EthernetStateTracker$DhcpHandler;->this$0:Landroid/net/ethernet/EthernetStateTracker;

    # getter for: Landroid/net/ethernet/EthernetStateTracker;->mDhcpTarget:Landroid/net/ethernet/EthernetStateTracker$DhcpHandler;
    invoke-static {v1}, Landroid/net/ethernet/EthernetStateTracker;->access$100(Landroid/net/ethernet/EthernetStateTracker;)Landroid/net/ethernet/EthernetStateTracker$DhcpHandler;

    move-result-object v2

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Landroid/net/ethernet/EthernetStateTracker$DhcpHandler;->this$0:Landroid/net/ethernet/EthernetStateTracker;

    # getter for: Landroid/net/ethernet/EthernetStateTracker;->mInterfaceStopped:Z
    invoke-static {v1}, Landroid/net/ethernet/EthernetStateTracker;->access$200(Landroid/net/ethernet/EthernetStateTracker;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "EthernetStateTracker"

    const-string v3, "DhcpHandler: DHCP request started"

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/net/ethernet/EthernetStateTracker$DhcpHandler;->PreDhcpStart()V

    iget-object v1, p0, Landroid/net/ethernet/EthernetStateTracker$DhcpHandler;->this$0:Landroid/net/ethernet/EthernetStateTracker;

    # getter for: Landroid/net/ethernet/EthernetStateTracker;->mInterfaceName:Ljava/lang/String;
    invoke-static {v1}, Landroid/net/ethernet/EthernetStateTracker;->access$300(Landroid/net/ethernet/EthernetStateTracker;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Landroid/net/ethernet/EthernetStateTracker$DhcpHandler;->this$0:Landroid/net/ethernet/EthernetStateTracker;

    # getter for: Landroid/net/ethernet/EthernetStateTracker;->mDhcpInfo:Landroid/net/DhcpInfoInternal;
    invoke-static {v3}, Landroid/net/ethernet/EthernetStateTracker;->access$400(Landroid/net/ethernet/EthernetStateTracker;)Landroid/net/DhcpInfoInternal;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/net/NetworkUtils;->runDhcp(Ljava/lang/String;Landroid/net/DhcpInfoInternal;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    const-string v1, "EthernetStateTracker"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DhcpHandler: DHCP request succeeded: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Landroid/net/ethernet/EthernetStateTracker$DhcpHandler;->this$0:Landroid/net/ethernet/EthernetStateTracker;

    # getter for: Landroid/net/ethernet/EthernetStateTracker;->mDhcpInfo:Landroid/net/DhcpInfoInternal;
    invoke-static {v4}, Landroid/net/ethernet/EthernetStateTracker;->access$400(Landroid/net/ethernet/EthernetStateTracker;)Landroid/net/DhcpInfoInternal;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/DhcpInfoInternal;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Landroid/net/ethernet/EthernetStateTracker$DhcpHandler;->this$0:Landroid/net/ethernet/EthernetStateTracker;

    iget-object v3, p0, Landroid/net/ethernet/EthernetStateTracker$DhcpHandler;->this$0:Landroid/net/ethernet/EthernetStateTracker;

    # getter for: Landroid/net/ethernet/EthernetStateTracker;->mDhcpInfo:Landroid/net/DhcpInfoInternal;
    invoke-static {v3}, Landroid/net/ethernet/EthernetStateTracker;->access$400(Landroid/net/ethernet/EthernetStateTracker;)Landroid/net/DhcpInfoInternal;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/net/ethernet/EthernetStateTracker;->PostLinkageConfig(Landroid/net/DhcpInfoInternal;)V

    :goto_1
    iget-object v1, p0, Landroid/net/ethernet/EthernetStateTracker$DhcpHandler;->this$0:Landroid/net/ethernet/EthernetStateTracker;

    iget-object v3, p0, Landroid/net/ethernet/EthernetStateTracker$DhcpHandler;->this$0:Landroid/net/ethernet/EthernetStateTracker;

    # getter for: Landroid/net/ethernet/EthernetStateTracker;->mDhcpInfo:Landroid/net/DhcpInfoInternal;
    invoke-static {v3}, Landroid/net/ethernet/EthernetStateTracker;->access$400(Landroid/net/ethernet/EthernetStateTracker;)Landroid/net/DhcpInfoInternal;

    move-result-object v3

    # invokes: Landroid/net/ethernet/EthernetStateTracker;->updateDhcpDevInfo(Landroid/net/DhcpInfoInternal;)V
    invoke-static {v1, v3}, Landroid/net/ethernet/EthernetStateTracker;->access$500(Landroid/net/ethernet/EthernetStateTracker;Landroid/net/DhcpInfoInternal;)V

    iget-object v1, p0, Landroid/net/ethernet/EthernetStateTracker$DhcpHandler;->this$0:Landroid/net/ethernet/EthernetStateTracker;

    # getter for: Landroid/net/ethernet/EthernetStateTracker;->mTrackerTarget:Landroid/os/Handler;
    invoke-static {v1}, Landroid/net/ethernet/EthernetStateTracker;->access$000(Landroid/net/ethernet/EthernetStateTracker;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :goto_2
    iget-object v1, p0, Landroid/net/ethernet/EthernetStateTracker$DhcpHandler;->this$0:Landroid/net/ethernet/EthernetStateTracker;

    const/4 v3, 0x0

    # setter for: Landroid/net/ethernet/EthernetStateTracker;->mStartingDhcp:Z
    invoke-static {v1, v3}, Landroid/net/ethernet/EthernetStateTracker;->access$602(Landroid/net/ethernet/EthernetStateTracker;Z)Z

    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_0
    const/4 v0, 0x2

    :try_start_1
    const-string v1, "EthernetStateTracker"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DhcpHandler: DHCP request failed: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Landroid/net/NetworkUtils;->getDhcpError()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_1
    iget-object v1, p0, Landroid/net/ethernet/EthernetStateTracker$DhcpHandler;->this$0:Landroid/net/ethernet/EthernetStateTracker;

    const/4 v3, 0x0

    # setter for: Landroid/net/ethernet/EthernetStateTracker;->mInterfaceStopped:Z
    invoke-static {v1, v3}, Landroid/net/ethernet/EthernetStateTracker;->access$202(Landroid/net/ethernet/EthernetStateTracker;Z)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
