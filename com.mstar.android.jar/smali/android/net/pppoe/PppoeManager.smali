.class public Landroid/net/pppoe/PppoeManager;
.super Ljava/lang/Object;
.source "PppoeManager.java"


# static fields
.field public static final MSG_PPPOE_AUTH_FAILED:I = 0x4

.field public static final MSG_PPPOE_CONNECT:I = 0x0

.field public static final MSG_PPPOE_CONNECTING:I = 0x2

.field public static final MSG_PPPOE_DISCONNECT:I = 0x1

.field public static final MSG_PPPOE_DISCONNECTING:I = 0x3

.field public static final MSG_PPPOE_FAILED:I = 0x6

.field public static final MSG_PPPOE_TIME_OUT:I = 0x5

.field private static final NET_INTERFACE:Ljava/lang/String; = "net_if"

.field private static final PASS_WORD:Ljava/lang/String; = "pass_word"

.field public static final PPPOE_STATE_ACTION:Ljava/lang/String; = "android.net.pppoe.PPPOE_STATE_ACTION"

.field public static final PPPOE_STATE_AUTHFAILED:Ljava/lang/String; = "authfailed"

.field public static final PPPOE_STATE_CONNECT:Ljava/lang/String; = "connect"

.field public static final PPPOE_STATE_CONNECTING:Ljava/lang/String; = "connecting"

.field public static final PPPOE_STATE_DISCONNECT:Ljava/lang/String; = "disconnect"

.field public static final PPPOE_STATE_DISCONNECTING:Ljava/lang/String; = "disconnecting"

.field public static final PPPOE_STATE_FAILED:Ljava/lang/String; = "failed"

.field public static final PPPOE_STATE_LINKTIMEOUT:Ljava/lang/String; = "linktimeout"

.field public static final PPPOE_STATE_STATUE:Ljava/lang/String; = "PppoeStatus"

.field private static final TAG:Ljava/lang/String; = "PppoeManager"

.field private static final USER_NAME:Ljava/lang/String; = "user_name"

.field static mInstance:Landroid/net/pppoe/PppoeManager;

.field static final mInstanceSync:Ljava/lang/Object;

.field private static mSocketThread:Ljava/lang/Thread;


# instance fields
.field private gpppoe_sta:Landroid/net/pppoe/PPPOE_STA;

.field private mIsDialing:Z

.field mService:Landroid/net/pppoe/IPppoeManager;

.field private netif:Ljava/lang/String;

.field private passwd:Ljava/lang/String;

.field private user:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    sput-object v1, Landroid/net/pppoe/PppoeManager;->mSocketThread:Ljava/lang/Thread;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Landroid/net/pppoe/PppoeManager;->mInstanceSync:Ljava/lang/Object;

    sput-object v1, Landroid/net/pppoe/PppoeManager;->mInstance:Landroid/net/pppoe/PppoeManager;

    return-void
.end method

.method private constructor <init>(Landroid/net/pppoe/IPppoeManager;)V
    .locals 2
    .param p1    # Landroid/net/pppoe/IPppoeManager;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/net/pppoe/PppoeManager;->mIsDialing:Z

    iput-object v1, p0, Landroid/net/pppoe/PppoeManager;->user:Ljava/lang/String;

    iput-object v1, p0, Landroid/net/pppoe/PppoeManager;->passwd:Ljava/lang/String;

    iput-object v1, p0, Landroid/net/pppoe/PppoeManager;->netif:Ljava/lang/String;

    sget-object v0, Landroid/net/pppoe/PPPOE_STA;->DISCONNECTED:Landroid/net/pppoe/PPPOE_STA;

    iput-object v0, p0, Landroid/net/pppoe/PppoeManager;->gpppoe_sta:Landroid/net/pppoe/PPPOE_STA;

    iput-object v1, p0, Landroid/net/pppoe/PppoeManager;->mService:Landroid/net/pppoe/IPppoeManager;

    iput-object p1, p0, Landroid/net/pppoe/PppoeManager;->mService:Landroid/net/pppoe/IPppoeManager;

    invoke-direct {p0}, Landroid/net/pppoe/PppoeManager;->init()V

    return-void
.end method

.method private PppoeGetInfo()V
    .locals 1

    invoke-virtual {p0}, Landroid/net/pppoe/PppoeManager;->PppoeGetUser()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/net/pppoe/PppoeManager;->user:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/net/pppoe/PppoeManager;->PppoeGetPW()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/net/pppoe/PppoeManager;->passwd:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/net/pppoe/PppoeManager;->PppoeGetInterface()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/net/pppoe/PppoeManager;->netif:Ljava/lang/String;

    return-void
.end method

.method private PppoeSetStatus(Landroid/net/pppoe/PPPOE_STA;)V
    .locals 0
    .param p1    # Landroid/net/pppoe/PPPOE_STA;

    iput-object p1, p0, Landroid/net/pppoe/PppoeManager;->gpppoe_sta:Landroid/net/pppoe/PPPOE_STA;

    return-void
.end method

.method static synthetic access$000(Landroid/net/pppoe/PppoeManager;Ljava/lang/String;Z)V
    .locals 0
    .param p0    # Landroid/net/pppoe/PppoeManager;
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    invoke-direct {p0, p1, p2}, Landroid/net/pppoe/PppoeManager;->setPppoeStatus(Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$100(Landroid/net/pppoe/PppoeManager;)V
    .locals 0
    .param p0    # Landroid/net/pppoe/PppoeManager;

    invoke-direct {p0}, Landroid/net/pppoe/PppoeManager;->updatePppoeStatus()V

    return-void
.end method

.method static synthetic access$202(Landroid/net/pppoe/PppoeManager;Z)Z
    .locals 0
    .param p0    # Landroid/net/pppoe/PppoeManager;
    .param p1    # Z

    iput-boolean p1, p0, Landroid/net/pppoe/PppoeManager;->mIsDialing:Z

    return p1
.end method

.method private connect()V
    .locals 2

    const-string v0, "ctl.start"

    const-string v1, "pppoe-start"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static getInstance()Landroid/net/pppoe/PppoeManager;
    .locals 4

    sget-object v1, Landroid/net/pppoe/PppoeManager;->mInstance:Landroid/net/pppoe/PppoeManager;

    if-nez v1, :cond_1

    sget-object v2, Landroid/net/pppoe/PppoeManager;->mInstanceSync:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    sget-object v1, Landroid/net/pppoe/PppoeManager;->mInstance:Landroid/net/pppoe/PppoeManager;

    if-nez v1, :cond_0

    const-string v1, "pppoe"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    new-instance v1, Landroid/net/pppoe/PppoeManager;

    invoke-static {v0}, Landroid/net/pppoe/IPppoeManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/pppoe/IPppoeManager;

    move-result-object v3

    invoke-direct {v1, v3}, Landroid/net/pppoe/PppoeManager;-><init>(Landroid/net/pppoe/IPppoeManager;)V

    sput-object v1, Landroid/net/pppoe/PppoeManager;->mInstance:Landroid/net/pppoe/PppoeManager;

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v1, Landroid/net/pppoe/PppoeManager;->mInstance:Landroid/net/pppoe/PppoeManager;

    return-object v1

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private init()V
    .locals 0

    invoke-direct {p0}, Landroid/net/pppoe/PppoeManager;->PppoeGetInfo()V

    invoke-direct {p0}, Landroid/net/pppoe/PppoeManager;->updatePppoeStatus()V

    return-void
.end method

.method private isSetupRunning()Z
    .locals 3

    const-string v1, "init.svc.pppoe-setup"

    const-string v2, ""

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "stopped"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private readPppInfoFromFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 13
    .param p1    # Ljava/lang/String;

    const/4 v7, 0x0

    if-nez p1, :cond_0

    move-object v8, v7

    :goto_0
    return-object v8

    :cond_0
    const/4 v6, -0x1

    const-string v10, "ifname"

    invoke-virtual {v10, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    const/4 v6, 0x1

    :cond_1
    :goto_1
    const/4 v10, -0x1

    if-eq v6, v10, :cond_b

    const-string v5, "/data/misc/ppp/ipaddr"

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    if-eqz v4, :cond_2

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_8

    :cond_2
    move-object v8, v7

    goto :goto_0

    :cond_3
    const-string v10, "ip"

    invoke-virtual {v10, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    const/4 v6, 0x2

    goto :goto_1

    :cond_4
    const-string v10, "route"

    invoke-virtual {v10, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    const/4 v6, 0x3

    goto :goto_1

    :cond_5
    const-string v10, "mask"

    invoke-virtual {v10, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    const/4 v6, 0x4

    goto :goto_1

    :cond_6
    const-string v10, "dns1"

    invoke-virtual {v10, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    const/4 v6, 0x5

    goto :goto_1

    :cond_7
    const-string v10, "dns2"

    invoke-virtual {v10, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v6, 0x6

    goto :goto_1

    :cond_8
    const/4 v0, 0x0

    const/4 v9, 0x0

    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v10, Ljava/io/FileReader;

    invoke-direct {v10, v4}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, 0x0

    :cond_9
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v9

    if-eqz v9, :cond_a

    add-int/lit8 v2, v2, 0x1

    if-ne v2, v6, :cond_9

    move-object v7, v9

    :cond_a
    if-eqz v1, :cond_b

    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    const/4 v0, 0x0

    :cond_b
    :goto_2
    move-object v8, v7

    goto :goto_0

    :catch_0
    move-exception v3

    const-string v10, "PppoeManager"

    const-string v11, "failure to close BufferedReader"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :catch_1
    move-exception v3

    :goto_3
    :try_start_3
    invoke-virtual {v3}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v0, :cond_b

    :try_start_4
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    const/4 v0, 0x0

    goto :goto_2

    :catch_2
    move-exception v3

    const-string v10, "PppoeManager"

    const-string v11, "failure to close BufferedReader"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :catch_3
    move-exception v3

    :goto_4
    :try_start_5
    invoke-virtual {v3}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eqz v0, :cond_b

    :try_start_6
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    const/4 v0, 0x0

    goto :goto_2

    :catch_4
    move-exception v3

    const-string v10, "PppoeManager"

    const-string v11, "failure to close BufferedReader"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :catchall_0
    move-exception v10

    :goto_5
    if-eqz v0, :cond_c

    :try_start_7
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    const/4 v0, 0x0

    :cond_c
    :goto_6
    throw v10

    :catch_5
    move-exception v3

    const-string v11, "PppoeManager"

    const-string v12, "failure to close BufferedReader"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    :catchall_1
    move-exception v10

    move-object v0, v1

    goto :goto_5

    :catch_6
    move-exception v3

    move-object v0, v1

    goto :goto_4

    :catch_7
    move-exception v3

    move-object v0, v1

    goto :goto_3
.end method

.method private readSettingInfoFromFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p1    # Ljava/lang/String;

    const/4 v6, 0x0

    if-nez p1, :cond_0

    move-object v8, v6

    :goto_0
    return-object v8

    :cond_0
    const-string v5, "/data/misc/ppp/pppoe.data"

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_1

    const/4 v8, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    const/4 v7, 0x0

    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v8, Ljava/io/FileReader;

    invoke-direct {v8, v4}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    :try_start_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_3

    const-string v8, "="

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    array-length v8, v3

    const/4 v9, 0x2

    if-ne v8, v9, :cond_2

    const/4 v8, 0x0

    aget-object v8, v3, v8

    invoke-virtual {p1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v8, 0x1

    aget-object v6, v3, v8
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :cond_3
    if-eqz v1, :cond_6

    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    const/4 v0, 0x0

    :cond_4
    :goto_1
    move-object v8, v6

    goto :goto_0

    :catch_0
    move-exception v2

    const-string v8, "PppoeManager"

    const-string v9, "failure to close BufferedReader"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    goto :goto_1

    :catch_1
    move-exception v2

    :goto_2
    :try_start_3
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v0, :cond_4

    :try_start_4
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    const/4 v0, 0x0

    goto :goto_1

    :catch_2
    move-exception v2

    const-string v8, "PppoeManager"

    const-string v9, "failure to close BufferedReader"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catch_3
    move-exception v2

    :goto_3
    :try_start_5
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eqz v0, :cond_4

    :try_start_6
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    const/4 v0, 0x0

    goto :goto_1

    :catch_4
    move-exception v2

    const-string v8, "PppoeManager"

    const-string v9, "failure to close BufferedReader"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catchall_0
    move-exception v8

    :goto_4
    if-eqz v0, :cond_5

    :try_start_7
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    const/4 v0, 0x0

    :cond_5
    :goto_5
    throw v8

    :catch_5
    move-exception v2

    const-string v9, "PppoeManager"

    const-string v10, "failure to close BufferedReader"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_5

    :catchall_1
    move-exception v8

    move-object v0, v1

    goto :goto_4

    :catch_6
    move-exception v2

    move-object v0, v1

    goto :goto_3

    :catch_7
    move-exception v2

    move-object v0, v1

    goto :goto_2

    :cond_6
    move-object v0, v1

    goto :goto_1
.end method

.method private setPppoeStatus(Ljava/lang/String;Z)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    :try_start_0
    iget-object v0, p0, Landroid/net/pppoe/PppoeManager;->mService:Landroid/net/pppoe/IPppoeManager;

    invoke-interface {v0, p1, p2}, Landroid/net/pppoe/IPppoeManager;->setPppoeStatus(Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private setup()V
    .locals 2

    const-string v0, "ctl.start"

    const-string v1, "pppoe-setup"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0}, Landroid/net/pppoe/PppoeManager;->isSetupRunning()Z

    move-result v0

    if-nez v0, :cond_0

    return-void
.end method

.method private stop()V
    .locals 2

    const-string v0, "ctl.start"

    const-string v1, "pppoe-stop"

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private updatePppoeStatus()V
    .locals 2

    invoke-virtual {p0}, Landroid/net/pppoe/PppoeManager;->getPppoeStatus()Ljava/lang/String;

    move-result-object v0

    const-string v1, "connect"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Landroid/net/pppoe/PPPOE_STA;->CONNECTED:Landroid/net/pppoe/PPPOE_STA;

    invoke-direct {p0, v0}, Landroid/net/pppoe/PppoeManager;->PppoeSetStatus(Landroid/net/pppoe/PPPOE_STA;)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Landroid/net/pppoe/PPPOE_STA;->DISCONNECTED:Landroid/net/pppoe/PPPOE_STA;

    invoke-direct {p0, v0}, Landroid/net/pppoe/PppoeManager;->PppoeSetStatus(Landroid/net/pppoe/PPPOE_STA;)V

    goto :goto_0
.end method

.method private declared-synchronized writeFile()V
    .locals 5

    monitor-enter p0

    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Ljava/io/FileWriter;

    const-string v3, "/data/misc/ppp/pppoe.data"

    invoke-direct {v2, v3}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "user_name="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Landroid/net/pppoe/PppoeManager;->user:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "pass_word="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Landroid/net/pppoe/PppoeManager;->passwd:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "net_if="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Landroid/net/pppoe/PppoeManager;->netif:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    if-eqz v2, :cond_2

    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileWriter;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    move-object v1, v2

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    move-object v1, v2

    goto :goto_0

    :catch_1
    move-exception v0

    :goto_1
    :try_start_4
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz v1, :cond_0

    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    :catch_2
    move-exception v0

    :try_start_6
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v3

    :goto_2
    monitor-exit p0

    throw v3

    :catchall_1
    move-exception v3

    :goto_3
    if-eqz v1, :cond_1

    :try_start_7
    invoke-virtual {v1}, Ljava/io/FileWriter;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_1
    :goto_4
    :try_start_8
    throw v3

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_4

    :catchall_2
    move-exception v3

    move-object v1, v2

    goto :goto_3

    :catch_4
    move-exception v0

    move-object v1, v2

    goto :goto_1

    :catchall_3
    move-exception v3

    move-object v1, v2

    goto :goto_2

    :cond_2
    move-object v1, v2

    goto :goto_0
.end method


# virtual methods
.method public PppoeDialup()Z
    .locals 3

    const/4 v0, 0x1

    iget-boolean v1, p0, Landroid/net/pppoe/PppoeManager;->mIsDialing:Z

    if-eqz v1, :cond_0

    const-string v0, "PppoeManager"

    const-string v1, "you can NOT dial up again when dialing"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput-boolean v0, p0, Landroid/net/pppoe/PppoeManager;->mIsDialing:Z

    sget-object v1, Landroid/net/pppoe/PPPOE_STA;->CONNECTING:Landroid/net/pppoe/PPPOE_STA;

    invoke-direct {p0, v1}, Landroid/net/pppoe/PppoeManager;->PppoeSetStatus(Landroid/net/pppoe/PPPOE_STA;)V

    const-string v1, "connecting"

    invoke-direct {p0, v1, v0}, Landroid/net/pppoe/PppoeManager;->setPppoeStatus(Ljava/lang/String;Z)V

    invoke-direct {p0}, Landroid/net/pppoe/PppoeManager;->connect()V

    new-instance v1, Landroid/net/pppoe/PppoeManager$2;

    invoke-direct {v1, p0}, Landroid/net/pppoe/PppoeManager$2;-><init>(Landroid/net/pppoe/PppoeManager;)V

    invoke-virtual {v1}, Landroid/net/pppoe/PppoeManager$2;->start()V

    const-string v1, "PppoeManager"

    const-string v2, "PppoeDialup"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public PppoeGetInterface()Ljava/lang/String;
    .locals 1

    const-string v0, "net_if"

    invoke-direct {p0, v0}, Landroid/net/pppoe/PppoeManager;->readSettingInfoFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public PppoeGetPW()Ljava/lang/String;
    .locals 1

    const-string v0, "pass_word"

    invoke-direct {p0, v0}, Landroid/net/pppoe/PppoeManager;->readSettingInfoFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public PppoeGetStatus()Landroid/net/pppoe/PPPOE_STA;
    .locals 2

    iget-object v0, p0, Landroid/net/pppoe/PppoeManager;->gpppoe_sta:Landroid/net/pppoe/PPPOE_STA;

    sget-object v1, Landroid/net/pppoe/PPPOE_STA;->CONNECTING:Landroid/net/pppoe/PPPOE_STA;

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Landroid/net/pppoe/PppoeManager;->updatePppoeStatus()V

    :cond_0
    iget-object v0, p0, Landroid/net/pppoe/PppoeManager;->gpppoe_sta:Landroid/net/pppoe/PPPOE_STA;

    return-object v0
.end method

.method public PppoeGetUser()Ljava/lang/String;
    .locals 1

    const-string v0, "user_name"

    invoke-direct {p0, v0}, Landroid/net/pppoe/PppoeManager;->readSettingInfoFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public PppoeHangUp()V
    .locals 2

    invoke-direct {p0}, Landroid/net/pppoe/PppoeManager;->stop()V

    sget-object v0, Landroid/net/pppoe/PPPOE_STA;->DISCONNECTED:Landroid/net/pppoe/PPPOE_STA;

    invoke-direct {p0, v0}, Landroid/net/pppoe/PppoeManager;->PppoeSetStatus(Landroid/net/pppoe/PPPOE_STA;)V

    const-string v0, "disconnect"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Landroid/net/pppoe/PppoeManager;->setPppoeStatus(Ljava/lang/String;Z)V

    const-string v0, "PppoeManager"

    const-string v1, "PppoeHangUp"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public PppoeMonitor()V
    .locals 0

    return-void
.end method

.method public PppoeSetInterface(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iput-object p1, p0, Landroid/net/pppoe/PppoeManager;->netif:Ljava/lang/String;

    invoke-direct {p0}, Landroid/net/pppoe/PppoeManager;->writeFile()V

    invoke-direct {p0}, Landroid/net/pppoe/PppoeManager;->setup()V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public PppoeSetPW(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iput-object p1, p0, Landroid/net/pppoe/PppoeManager;->passwd:Ljava/lang/String;

    invoke-direct {p0}, Landroid/net/pppoe/PppoeManager;->writeFile()V

    invoke-direct {p0}, Landroid/net/pppoe/PppoeManager;->setup()V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public PppoeSetUser(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    if-eqz p1, :cond_0

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iput-object p1, p0, Landroid/net/pppoe/PppoeManager;->user:Ljava/lang/String;

    invoke-direct {p0}, Landroid/net/pppoe/PppoeManager;->writeFile()V

    invoke-direct {p0}, Landroid/net/pppoe/PppoeManager;->setup()V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public connectPppoe(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const-string v0, "eth0"

    invoke-virtual {p0, p1, p2, v0}, Landroid/net/pppoe/PppoeManager;->connectPppoe(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public connectPppoe(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    const-string v1, "PppoeManager"

    const-string v2, "connectPppoe"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/net/pppoe/PppoeManager$1;

    const-string v2, "pppoe_dialup_thread"

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Landroid/net/pppoe/PppoeManager$1;-><init>(Landroid/net/pppoe/PppoeManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public disconnectPppoe()V
    .locals 2

    const-string v0, "PppoeManager"

    const-string v1, "disconnectPppoe"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Landroid/net/pppoe/PppoeManager;->PppoeHangUp()V

    return-void
.end method

.method public getDns1()Ljava/lang/String;
    .locals 1

    const-string v0, "dns1"

    invoke-direct {p0, v0}, Landroid/net/pppoe/PppoeManager;->readPppInfoFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDns2()Ljava/lang/String;
    .locals 1

    const-string v0, "dns2"

    invoke-direct {p0, v0}, Landroid/net/pppoe/PppoeManager;->readPppInfoFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getInterfaceName()Ljava/lang/String;
    .locals 1

    const-string v0, "ifname"

    invoke-direct {p0, v0}, Landroid/net/pppoe/PppoeManager;->readPppInfoFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIpaddr()Ljava/lang/String;
    .locals 1

    const-string v0, "ip"

    invoke-direct {p0, v0}, Landroid/net/pppoe/PppoeManager;->readPppInfoFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMask()Ljava/lang/String;
    .locals 1

    const-string v0, "mask"

    invoke-direct {p0, v0}, Landroid/net/pppoe/PppoeManager;->readPppInfoFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPppoeStatus()Ljava/lang/String;
    .locals 2

    :try_start_0
    iget-object v1, p0, Landroid/net/pppoe/PppoeManager;->mService:Landroid/net/pppoe/IPppoeManager;

    invoke-interface {v1}, Landroid/net/pppoe/IPppoeManager;->getPppoeStatus()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getRoute()Ljava/lang/String;
    .locals 1

    const-string v0, "route"

    invoke-direct {p0, v0}, Landroid/net/pppoe/PppoeManager;->readPppInfoFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isPppoeRunning()Z
    .locals 3

    const-string v1, "init.svc.pppoe-start"

    const-string v2, ""

    invoke-static {v1, v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "stopped"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method
