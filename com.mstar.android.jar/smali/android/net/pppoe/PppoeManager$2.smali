.class Landroid/net/pppoe/PppoeManager$2;
.super Ljava/lang/Thread;
.source "PppoeManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/net/pppoe/PppoeManager;->PppoeDialup()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/net/pppoe/PppoeManager;


# direct methods
.method constructor <init>(Landroid/net/pppoe/PppoeManager;)V
    .locals 0

    iput-object p1, p0, Landroid/net/pppoe/PppoeManager$2;->this$0:Landroid/net/pppoe/PppoeManager;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    const/4 v5, 0x1

    :cond_0
    const-wide/16 v2, 0x1f4

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v2, p0, Landroid/net/pppoe/PppoeManager$2;->this$0:Landroid/net/pppoe/PppoeManager;

    invoke-virtual {v2}, Landroid/net/pppoe/PppoeManager;->isPppoeRunning()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Landroid/net/pppoe/PppoeManager$2;->this$0:Landroid/net/pppoe/PppoeManager;

    invoke-virtual {v2}, Landroid/net/pppoe/PppoeManager;->getPppoeStatus()Ljava/lang/String;

    move-result-object v0

    const-string v2, "authfailed"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Landroid/net/pppoe/PppoeManager$2;->this$0:Landroid/net/pppoe/PppoeManager;

    const-string v3, "authfailed"

    # invokes: Landroid/net/pppoe/PppoeManager;->setPppoeStatus(Ljava/lang/String;Z)V
    invoke-static {v2, v3, v5}, Landroid/net/pppoe/PppoeManager;->access$000(Landroid/net/pppoe/PppoeManager;Ljava/lang/String;Z)V

    :cond_1
    :goto_1
    iget-object v2, p0, Landroid/net/pppoe/PppoeManager$2;->this$0:Landroid/net/pppoe/PppoeManager;

    # invokes: Landroid/net/pppoe/PppoeManager;->updatePppoeStatus()V
    invoke-static {v2}, Landroid/net/pppoe/PppoeManager;->access$100(Landroid/net/pppoe/PppoeManager;)V

    iget-object v2, p0, Landroid/net/pppoe/PppoeManager$2;->this$0:Landroid/net/pppoe/PppoeManager;

    const/4 v3, 0x0

    # setter for: Landroid/net/pppoe/PppoeManager;->mIsDialing:Z
    invoke-static {v2, v3}, Landroid/net/pppoe/PppoeManager;->access$202(Landroid/net/pppoe/PppoeManager;Z)Z

    return-void

    :catch_0
    move-exception v1

    const-string v2, "PppoeManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "InterruptedException: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const-string v2, "connect"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Landroid/net/pppoe/PppoeManager$2;->this$0:Landroid/net/pppoe/PppoeManager;

    const-string v3, "disconnect"

    # invokes: Landroid/net/pppoe/PppoeManager;->setPppoeStatus(Ljava/lang/String;Z)V
    invoke-static {v2, v3, v5}, Landroid/net/pppoe/PppoeManager;->access$000(Landroid/net/pppoe/PppoeManager;Ljava/lang/String;Z)V

    goto :goto_1
.end method
