.class Landroid/net/dlna/MediaServerControllerImpl;
.super Ljava/lang/Object;
.source "DLNAImpl.java"

# interfaces
.implements Landroid/net/dlna/MediaServerController;


# instance fields
.field private handle:I


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private synchronized native declared-synchronized DMSCCreateObject(Ljava/lang/String;Ljava/lang/String;)Landroid/net/dlna/ShareItem;
.end method

.method private synchronized native declared-synchronized DMSCTransferFile(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method private synchronized native declared-synchronized JNI_DMSC_Authenticate(Ljava/lang/String;)V
.end method

.method private synchronized native declared-synchronized JNI_DMSC_Browse(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/dlna/ShareObject;",
            ">;"
        }
    .end annotation
.end method

.method private synchronized native declared-synchronized JNI_DMSC_Browse_Ex(Ljava/lang/String;Landroid/net/dlna/BrowseFlag;Ljava/lang/String;IILjava/lang/String;)Landroid/net/dlna/BrowseResult;
.end method

.method private synchronized native declared-synchronized JNI_DMSC_GetDeviceInfo()Landroid/net/dlna/DeviceInfo;
.end method

.method private synchronized native declared-synchronized JNI_DMSC_GetProtocolInfo()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/dlna/ProtocolInfo;",
            ">;"
        }
    .end annotation
.end method


# virtual methods
.method public Authenticate(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Landroid/net/dlna/MediaServerControllerImpl;->JNI_DMSC_Authenticate(Ljava/lang/String;)V

    return-void
.end method

.method public Browse(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)Ljava/util/ArrayList;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # I
    .param p5    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/dlna/ShareObject;",
            ">;"
        }
    .end annotation

    invoke-direct/range {p0 .. p5}, Landroid/net/dlna/MediaServerControllerImpl;->JNI_DMSC_Browse(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public Browse_Ex(Ljava/lang/String;Landroid/net/dlna/BrowseFlag;Ljava/lang/String;IILjava/lang/String;)Landroid/net/dlna/BrowseResult;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Landroid/net/dlna/BrowseFlag;
    .param p3    # Ljava/lang/String;
    .param p4    # I
    .param p5    # I
    .param p6    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/dlna/ActionUnsupportedException;,
            Landroid/net/dlna/HostUnreachableException;,
            Landroid/net/dlna/MissingAuthenticationException;
        }
    .end annotation

    invoke-direct/range {p0 .. p6}, Landroid/net/dlna/MediaServerControllerImpl;->JNI_DMSC_Browse_Ex(Ljava/lang/String;Landroid/net/dlna/BrowseFlag;Ljava/lang/String;IILjava/lang/String;)Landroid/net/dlna/BrowseResult;

    move-result-object v0

    return-object v0
.end method

.method public CreateObject(Ljava/lang/String;Ljava/lang/String;)Landroid/net/dlna/ShareItem;
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Landroid/net/dlna/MediaServerControllerImpl;->DMSCCreateObject(Ljava/lang/String;Ljava/lang/String;)Landroid/net/dlna/ShareItem;

    move-result-object v0

    return-object v0
.end method

.method public GetDeviceInfo()Landroid/net/dlna/DeviceInfo;
    .locals 1

    invoke-direct {p0}, Landroid/net/dlna/MediaServerControllerImpl;->JNI_DMSC_GetDeviceInfo()Landroid/net/dlna/DeviceInfo;

    move-result-object v0

    return-object v0
.end method

.method public GetProtocolInfo()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/dlna/ProtocolInfo;",
            ">;"
        }
    .end annotation

    invoke-direct {p0}, Landroid/net/dlna/MediaServerControllerImpl;->JNI_DMSC_GetProtocolInfo()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public TransferFile(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Landroid/net/dlna/MediaServerControllerImpl;->DMSCTransferFile(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method
