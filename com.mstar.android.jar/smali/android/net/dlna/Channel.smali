.class public final enum Landroid/net/dlna/Channel;
.super Ljava/lang/Enum;
.source "Channel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/net/dlna/Channel;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/net/dlna/Channel;

.field public static final enum MASTER_CHANNEL:Landroid/net/dlna/Channel;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Landroid/net/dlna/Channel;

    const-string v1, "MASTER_CHANNEL"

    invoke-direct {v0, v1, v2}, Landroid/net/dlna/Channel;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/net/dlna/Channel;->MASTER_CHANNEL:Landroid/net/dlna/Channel;

    const/4 v0, 0x1

    new-array v0, v0, [Landroid/net/dlna/Channel;

    sget-object v1, Landroid/net/dlna/Channel;->MASTER_CHANNEL:Landroid/net/dlna/Channel;

    aput-object v1, v0, v2

    sput-object v0, Landroid/net/dlna/Channel;->$VALUES:[Landroid/net/dlna/Channel;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/net/dlna/Channel;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Landroid/net/dlna/Channel;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Landroid/net/dlna/Channel;

    return-object v0
.end method

.method public static values()[Landroid/net/dlna/Channel;
    .locals 1

    sget-object v0, Landroid/net/dlna/Channel;->$VALUES:[Landroid/net/dlna/Channel;

    invoke-virtual {v0}, [Landroid/net/dlna/Channel;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/net/dlna/Channel;

    return-object v0
.end method
