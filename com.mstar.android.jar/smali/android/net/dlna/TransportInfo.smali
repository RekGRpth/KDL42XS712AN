.class public Landroid/net/dlna/TransportInfo;
.super Ljava/lang/Object;
.source "TransportInfo.java"


# instance fields
.field current_speed:Ljava/lang/String;

.field current_transport_state:Landroid/net/dlna/TransportState;

.field current_transport_status:Landroid/net/dlna/TransportStatus;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/net/dlna/TransportState;Landroid/net/dlna/TransportStatus;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/net/dlna/TransportState;
    .param p2    # Landroid/net/dlna/TransportStatus;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/net/dlna/TransportInfo;->current_transport_state:Landroid/net/dlna/TransportState;

    iput-object p2, p0, Landroid/net/dlna/TransportInfo;->current_transport_status:Landroid/net/dlna/TransportStatus;

    iput-object p3, p0, Landroid/net/dlna/TransportInfo;->current_speed:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getCurrentSpeed()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/net/dlna/TransportInfo;->current_speed:Ljava/lang/String;

    return-object v0
.end method

.method public getCurrentTransportState()Landroid/net/dlna/TransportState;
    .locals 1

    iget-object v0, p0, Landroid/net/dlna/TransportInfo;->current_transport_state:Landroid/net/dlna/TransportState;

    return-object v0
.end method

.method public getCurrentTransportStatus()Landroid/net/dlna/TransportStatus;
    .locals 1

    iget-object v0, p0, Landroid/net/dlna/TransportInfo;->current_transport_status:Landroid/net/dlna/TransportStatus;

    return-object v0
.end method

.method public setCurrentSpeed(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Landroid/net/dlna/TransportInfo;->current_speed:Ljava/lang/String;

    return-void
.end method

.method public setCurrentTransportState(Landroid/net/dlna/TransportState;)V
    .locals 0
    .param p1    # Landroid/net/dlna/TransportState;

    iput-object p1, p0, Landroid/net/dlna/TransportInfo;->current_transport_state:Landroid/net/dlna/TransportState;

    return-void
.end method

.method public setCurrentTransportStatus(Landroid/net/dlna/TransportStatus;)V
    .locals 0
    .param p1    # Landroid/net/dlna/TransportStatus;

    iput-object p1, p0, Landroid/net/dlna/TransportInfo;->current_transport_status:Landroid/net/dlna/TransportStatus;

    return-void
.end method
