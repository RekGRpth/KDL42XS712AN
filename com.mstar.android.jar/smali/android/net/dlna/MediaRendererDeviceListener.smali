.class public interface abstract Landroid/net/dlna/MediaRendererDeviceListener;
.super Ljava/lang/Object;
.source "MediaRendererDeviceListener.java"


# virtual methods
.method public abstract OnGetDeviceCapabilities(I)Z
.end method

.method public abstract OnGetMediaInfo(I)Z
.end method

.method public abstract OnGetPositionInfo(I)Z
.end method

.method public abstract OnGetProtocolInfo()Z
.end method

.method public abstract OnGetTransportInfo(I)Z
.end method

.method public abstract OnGetTransportSettings(I)Z
.end method

.method public abstract OnGetVolume(I)Z
.end method

.method public abstract OnKeepAlive(I)Z
.end method

.method public abstract OnNext(I)Z
.end method

.method public abstract OnPause(I)Z
.end method

.method public abstract OnPlay(ILjava/lang/String;)Z
.end method

.method public abstract OnPrevious(I)Z
.end method

.method public abstract OnRotateClockwise(I)Z
.end method

.method public abstract OnRotateCounterClockwise(I)Z
.end method

.method public abstract OnSeek(ILandroid/net/dlna/SeekMode;Ljava/lang/String;)Z
.end method

.method public abstract OnSelectPreset(I)Z
.end method

.method public abstract OnSetAVTransportURI(ILjava/lang/String;Landroid/net/dlna/ShareObject;)Z
.end method

.method public abstract OnSetNextAVTransportURI(ILjava/lang/String;Landroid/net/dlna/ShareObject;)Z
.end method

.method public abstract OnSetVolume(ILandroid/net/dlna/VolumeInfo;)Z
.end method

.method public abstract OnStop(I)Z
.end method

.method public abstract OnZoomIn(I)Z
.end method

.method public abstract OnZoomOut(I)Z
.end method
