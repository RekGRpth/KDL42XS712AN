.class public interface abstract Landroid/net/dlna/MediaServerDevice;
.super Ljava/lang/Object;
.source "MediaServerDevice.java"


# virtual methods
.method public abstract AddMediaFile(Ljava/lang/String;Landroid/net/dlna/MediaMetaData;)V
.end method

.method public abstract Browse(Ljava/lang/String;)Landroid/net/dlna/ShareItem;
.end method

.method public abstract DisableUpload()I
.end method

.method public abstract EnableUpload()I
.end method

.method public abstract GetDeviceInfo()Landroid/net/dlna/DeviceInfo;
.end method

.method public abstract RemoveMediaFile(Ljava/lang/String;)V
.end method

.method public abstract SetDeviceInfo(Landroid/net/dlna/DeviceInfo;)V
.end method

.method public abstract SetListener(Landroid/net/dlna/MediaServerDeviceListener;)V
.end method

.method public abstract SetPassword(Ljava/lang/String;)V
.end method

.method public abstract SetProtocolInfo(Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/dlna/ProtocolInfo;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract SetUploadItemSaveDir(Ljava/lang/String;)I
.end method
