.class public Landroid/net/dlna/PositionInfo;
.super Ljava/lang/Object;
.source "PositionInfo.java"


# instance fields
.field private abs_count:I

.field private abs_time:Ljava/lang/String;

.field private rel_count:I

.field private rel_time:Ljava/lang/String;

.field private track:I

.field private track_duration:Ljava/lang/String;

.field private track_metadata:Ljava/lang/String;

.field private track_uri:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;
    .param p6    # Ljava/lang/String;
    .param p7    # I
    .param p8    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Landroid/net/dlna/PositionInfo;->track:I

    iput-object p2, p0, Landroid/net/dlna/PositionInfo;->track_duration:Ljava/lang/String;

    iput-object p3, p0, Landroid/net/dlna/PositionInfo;->track_metadata:Ljava/lang/String;

    iput-object p4, p0, Landroid/net/dlna/PositionInfo;->track_uri:Ljava/lang/String;

    iput-object p5, p0, Landroid/net/dlna/PositionInfo;->rel_time:Ljava/lang/String;

    iput-object p6, p0, Landroid/net/dlna/PositionInfo;->abs_time:Ljava/lang/String;

    iput p7, p0, Landroid/net/dlna/PositionInfo;->rel_count:I

    iput p8, p0, Landroid/net/dlna/PositionInfo;->abs_count:I

    return-void
.end method


# virtual methods
.method public getAbsCount()I
    .locals 1

    iget v0, p0, Landroid/net/dlna/PositionInfo;->abs_count:I

    return v0
.end method

.method public getAbsTime()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/net/dlna/PositionInfo;->abs_time:Ljava/lang/String;

    return-object v0
.end method

.method public getRelCount()I
    .locals 1

    iget v0, p0, Landroid/net/dlna/PositionInfo;->rel_count:I

    return v0
.end method

.method public getRelTime()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/net/dlna/PositionInfo;->rel_time:Ljava/lang/String;

    return-object v0
.end method

.method public getTrack()I
    .locals 1

    iget v0, p0, Landroid/net/dlna/PositionInfo;->track:I

    return v0
.end method

.method public getTrackDuration()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/net/dlna/PositionInfo;->track_duration:Ljava/lang/String;

    return-object v0
.end method

.method public getTrackMetadata()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/net/dlna/PositionInfo;->track_metadata:Ljava/lang/String;

    return-object v0
.end method

.method public getTrackURI()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/net/dlna/PositionInfo;->track_uri:Ljava/lang/String;

    return-object v0
.end method

.method public setAbsCount(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Landroid/net/dlna/PositionInfo;->abs_count:I

    return-void
.end method

.method public setAbsTime(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Landroid/net/dlna/PositionInfo;->abs_time:Ljava/lang/String;

    return-void
.end method

.method public setRelCount(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Landroid/net/dlna/PositionInfo;->rel_count:I

    return-void
.end method

.method public setRelTime(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Landroid/net/dlna/PositionInfo;->rel_time:Ljava/lang/String;

    return-void
.end method

.method public setTrack(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Landroid/net/dlna/PositionInfo;->track:I

    return-void
.end method

.method public setTrackDuration(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Landroid/net/dlna/PositionInfo;->track_duration:Ljava/lang/String;

    return-void
.end method

.method public setTrackMetadata(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Landroid/net/dlna/PositionInfo;->track_metadata:Ljava/lang/String;

    return-void
.end method

.method public setTrackURI(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Landroid/net/dlna/PositionInfo;->track_uri:Ljava/lang/String;

    return-void
.end method
