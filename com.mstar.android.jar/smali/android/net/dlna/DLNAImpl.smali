.class Landroid/net/dlna/DLNAImpl;
.super Ljava/lang/Object;
.source "DLNAImpl.java"

# interfaces
.implements Landroid/net/dlna/DLNA;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "dlnajni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    return-void
.end method

.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private synchronized native declared-synchronized JNI_DLNA_AsyncSearchDevice()V
.end method

.method private synchronized native declared-synchronized JNI_DLNA_Finalize()V
.end method

.method private synchronized native declared-synchronized JNI_DLNA_GetIP()Ljava/lang/String;
.end method

.method private synchronized native declared-synchronized JNI_DLNA_GetPort()I
.end method

.method private native JNI_DLNA_GetVersion()Ljava/lang/String;
.end method

.method private synchronized native declared-synchronized JNI_DLNA_Initialize(Ljava/lang/String;I)Z
.end method

.method private synchronized native declared-synchronized JNI_DLNA_SetDescriptionFile(Ljava/lang/String;)V
.end method

.method private synchronized native declared-synchronized JNI_DLNA_SetDeviceListener(Landroid/net/dlna/DeviceListener;)V
.end method


# virtual methods
.method public AsyncSearchDevice()V
    .locals 0

    invoke-direct {p0}, Landroid/net/dlna/DLNAImpl;->JNI_DLNA_AsyncSearchDevice()V

    return-void
.end method

.method public CreateMediaRendererDevice()Landroid/net/dlna/MediaRendererDevice;
    .locals 1

    new-instance v0, Landroid/net/dlna/MediaRendererDeviceImpl;

    invoke-direct {v0}, Landroid/net/dlna/MediaRendererDeviceImpl;-><init>()V

    return-object v0
.end method

.method public CreateMediaServerDevice()Landroid/net/dlna/MediaServerDevice;
    .locals 1

    new-instance v0, Landroid/net/dlna/MediaServerDeviceImpl;

    invoke-direct {v0}, Landroid/net/dlna/MediaServerDeviceImpl;-><init>()V

    return-object v0
.end method

.method public Finalize()V
    .locals 0

    invoke-direct {p0}, Landroid/net/dlna/DLNAImpl;->JNI_DLNA_Finalize()V

    return-void
.end method

.method public GetIP()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Landroid/net/dlna/DLNAImpl;->JNI_DLNA_GetIP()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public GetMediaRendererControllerList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/dlna/MediaRendererController;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public GetMediaServerControllerList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/dlna/MediaServerController;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public GetPort()I
    .locals 1

    invoke-direct {p0}, Landroid/net/dlna/DLNAImpl;->JNI_DLNA_GetPort()I

    move-result v0

    return v0
.end method

.method public GetVersion()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Landroid/net/dlna/DLNAImpl;->JNI_DLNA_GetVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public Initialize(Ljava/lang/String;I)Z
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Landroid/net/dlna/DLNAImpl;->JNI_DLNA_Initialize(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method public SetDescriptionFile(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Landroid/net/dlna/DLNAImpl;->JNI_DLNA_SetDescriptionFile(Ljava/lang/String;)V

    return-void
.end method

.method public SetDeviceListener(Landroid/net/dlna/DeviceListener;)V
    .locals 0
    .param p1    # Landroid/net/dlna/DeviceListener;

    invoke-direct {p0, p1}, Landroid/net/dlna/DLNAImpl;->JNI_DLNA_SetDeviceListener(Landroid/net/dlna/DeviceListener;)V

    return-void
.end method
