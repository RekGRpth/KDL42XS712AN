.class Landroid/net/dlna/MediaRendererControllerImpl;
.super Ljava/lang/Object;
.source "DLNAImpl.java"

# interfaces
.implements Landroid/net/dlna/MediaRendererController;


# instance fields
.field private handle:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Landroid/net/dlna/MediaRendererControllerImpl;->handle:I

    return-void
.end method

.method private synchronized native declared-synchronized JNI_DMRC_GetDeviceCapabilities(I)Landroid/net/dlna/DeviceCapabilities;
.end method

.method private synchronized native declared-synchronized JNI_DMRC_GetDeviceInfo()Landroid/net/dlna/DeviceInfo;
.end method

.method private synchronized native declared-synchronized JNI_DMRC_GetMediaInfo(I)Landroid/net/dlna/MediaInfo;
.end method

.method private synchronized native declared-synchronized JNI_DMRC_GetPositionInfo(I)Landroid/net/dlna/PositionInfo;
.end method

.method private synchronized native declared-synchronized JNI_DMRC_GetProtocolInfo()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/dlna/ProtocolInfo;",
            ">;"
        }
    .end annotation
.end method

.method private synchronized native declared-synchronized JNI_DMRC_GetTransportInfo(I)Landroid/net/dlna/TransportInfo;
.end method

.method private synchronized native declared-synchronized JNI_DMRC_GetTransportSettings(I)Landroid/net/dlna/TransportSettings;
.end method

.method private synchronized native declared-synchronized JNI_DMRC_GetVolume(ILandroid/net/dlna/Channel;)I
.end method

.method private synchronized native declared-synchronized JNI_DMRC_Next(I)V
.end method

.method private synchronized native declared-synchronized JNI_DMRC_Pause(I)V
.end method

.method private synchronized native declared-synchronized JNI_DMRC_Play(ILjava/lang/String;)V
.end method

.method private synchronized native declared-synchronized JNI_DMRC_Previous(I)V
.end method

.method private synchronized native declared-synchronized JNI_DMRC_RotateClockwise(I)V
.end method

.method private synchronized native declared-synchronized JNI_DMRC_RotateCounterClockwise(I)V
.end method

.method private synchronized native declared-synchronized JNI_DMRC_Seek(ILandroid/net/dlna/SeekMode;Ljava/lang/String;)V
.end method

.method private synchronized native declared-synchronized JNI_DMRC_SetAVTransportURI(ILjava/lang/String;Ljava/lang/String;)V
.end method

.method private synchronized native declared-synchronized JNI_DMRC_SetVolume(ILandroid/net/dlna/VolumeInfo;)V
.end method

.method private synchronized native declared-synchronized JNI_DMRC_Stop(I)V
.end method

.method private synchronized native declared-synchronized JNI_DMRC_ZoomIn(I)V
.end method

.method private synchronized native declared-synchronized JNI_DMRC_ZoomOut(I)V
.end method


# virtual methods
.method public GetDeviceCapabilities(I)Landroid/net/dlna/DeviceCapabilities;
    .locals 1
    .param p1    # I

    invoke-direct {p0, p1}, Landroid/net/dlna/MediaRendererControllerImpl;->JNI_DMRC_GetDeviceCapabilities(I)Landroid/net/dlna/DeviceCapabilities;

    move-result-object v0

    return-object v0
.end method

.method public GetDeviceInfo()Landroid/net/dlna/DeviceInfo;
    .locals 1

    invoke-direct {p0}, Landroid/net/dlna/MediaRendererControllerImpl;->JNI_DMRC_GetDeviceInfo()Landroid/net/dlna/DeviceInfo;

    move-result-object v0

    return-object v0
.end method

.method public GetMediaInfo(I)Landroid/net/dlna/MediaInfo;
    .locals 1
    .param p1    # I

    invoke-direct {p0, p1}, Landroid/net/dlna/MediaRendererControllerImpl;->JNI_DMRC_GetMediaInfo(I)Landroid/net/dlna/MediaInfo;

    move-result-object v0

    return-object v0
.end method

.method public GetPositionInfo(I)Landroid/net/dlna/PositionInfo;
    .locals 1
    .param p1    # I

    invoke-direct {p0, p1}, Landroid/net/dlna/MediaRendererControllerImpl;->JNI_DMRC_GetPositionInfo(I)Landroid/net/dlna/PositionInfo;

    move-result-object v0

    return-object v0
.end method

.method public GetProtocolInfo()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/dlna/ProtocolInfo;",
            ">;"
        }
    .end annotation

    invoke-direct {p0}, Landroid/net/dlna/MediaRendererControllerImpl;->JNI_DMRC_GetProtocolInfo()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public GetTransportInfo(I)Landroid/net/dlna/TransportInfo;
    .locals 1
    .param p1    # I

    invoke-direct {p0, p1}, Landroid/net/dlna/MediaRendererControllerImpl;->JNI_DMRC_GetTransportInfo(I)Landroid/net/dlna/TransportInfo;

    move-result-object v0

    return-object v0
.end method

.method public GetTransportSettings(I)Landroid/net/dlna/TransportSettings;
    .locals 1
    .param p1    # I

    invoke-direct {p0, p1}, Landroid/net/dlna/MediaRendererControllerImpl;->JNI_DMRC_GetTransportSettings(I)Landroid/net/dlna/TransportSettings;

    move-result-object v0

    return-object v0
.end method

.method public GetVolume(ILandroid/net/dlna/Channel;)I
    .locals 1
    .param p1    # I
    .param p2    # Landroid/net/dlna/Channel;

    invoke-direct {p0, p1, p2}, Landroid/net/dlna/MediaRendererControllerImpl;->JNI_DMRC_GetVolume(ILandroid/net/dlna/Channel;)I

    move-result v0

    return v0
.end method

.method public Next(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0, p1}, Landroid/net/dlna/MediaRendererControllerImpl;->JNI_DMRC_Next(I)V

    return-void
.end method

.method public Pause(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0, p1}, Landroid/net/dlna/MediaRendererControllerImpl;->JNI_DMRC_Pause(I)V

    return-void
.end method

.method public Play(ILjava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;

    invoke-direct {p0, p1, p2}, Landroid/net/dlna/MediaRendererControllerImpl;->JNI_DMRC_Play(ILjava/lang/String;)V

    return-void
.end method

.method public Previous(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0, p1}, Landroid/net/dlna/MediaRendererControllerImpl;->JNI_DMRC_Previous(I)V

    return-void
.end method

.method public RotateClockwise(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0, p1}, Landroid/net/dlna/MediaRendererControllerImpl;->JNI_DMRC_RotateClockwise(I)V

    return-void
.end method

.method public RotateCounterClockwise(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0, p1}, Landroid/net/dlna/MediaRendererControllerImpl;->JNI_DMRC_RotateCounterClockwise(I)V

    return-void
.end method

.method public Seek(ILandroid/net/dlna/SeekMode;Ljava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/net/dlna/SeekMode;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3}, Landroid/net/dlna/MediaRendererControllerImpl;->JNI_DMRC_Seek(ILandroid/net/dlna/SeekMode;Ljava/lang/String;)V

    return-void
.end method

.method public SetAVTransportURI(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3}, Landroid/net/dlna/MediaRendererControllerImpl;->JNI_DMRC_SetAVTransportURI(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public SetVolume(ILandroid/net/dlna/VolumeInfo;)V
    .locals 0
    .param p1    # I
    .param p2    # Landroid/net/dlna/VolumeInfo;

    invoke-direct {p0, p1, p2}, Landroid/net/dlna/MediaRendererControllerImpl;->JNI_DMRC_SetVolume(ILandroid/net/dlna/VolumeInfo;)V

    return-void
.end method

.method public Stop(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0, p1}, Landroid/net/dlna/MediaRendererControllerImpl;->JNI_DMRC_Stop(I)V

    return-void
.end method

.method public ZoomIn(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0, p1}, Landroid/net/dlna/MediaRendererControllerImpl;->JNI_DMRC_ZoomIn(I)V

    return-void
.end method

.method public ZoomOut(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0, p1}, Landroid/net/dlna/MediaRendererControllerImpl;->JNI_DMRC_ZoomOut(I)V

    return-void
.end method
