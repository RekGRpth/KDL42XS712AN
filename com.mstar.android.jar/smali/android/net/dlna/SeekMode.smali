.class public final enum Landroid/net/dlna/SeekMode;
.super Ljava/lang/Enum;
.source "SeekMode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/net/dlna/SeekMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/net/dlna/SeekMode;

.field public static final enum REL_TIME:Landroid/net/dlna/SeekMode;

.field public static final enum TRACK_NR:Landroid/net/dlna/SeekMode;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Landroid/net/dlna/SeekMode;

    const-string v1, "TRACK_NR"

    invoke-direct {v0, v1, v2}, Landroid/net/dlna/SeekMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/net/dlna/SeekMode;->TRACK_NR:Landroid/net/dlna/SeekMode;

    new-instance v0, Landroid/net/dlna/SeekMode;

    const-string v1, "REL_TIME"

    invoke-direct {v0, v1, v3}, Landroid/net/dlna/SeekMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/net/dlna/SeekMode;->REL_TIME:Landroid/net/dlna/SeekMode;

    const/4 v0, 0x2

    new-array v0, v0, [Landroid/net/dlna/SeekMode;

    sget-object v1, Landroid/net/dlna/SeekMode;->TRACK_NR:Landroid/net/dlna/SeekMode;

    aput-object v1, v0, v2

    sget-object v1, Landroid/net/dlna/SeekMode;->REL_TIME:Landroid/net/dlna/SeekMode;

    aput-object v1, v0, v3

    sput-object v0, Landroid/net/dlna/SeekMode;->$VALUES:[Landroid/net/dlna/SeekMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/net/dlna/SeekMode;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Landroid/net/dlna/SeekMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Landroid/net/dlna/SeekMode;

    return-object v0
.end method

.method public static values()[Landroid/net/dlna/SeekMode;
    .locals 1

    sget-object v0, Landroid/net/dlna/SeekMode;->$VALUES:[Landroid/net/dlna/SeekMode;

    invoke-virtual {v0}, [Landroid/net/dlna/SeekMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/net/dlna/SeekMode;

    return-object v0
.end method
