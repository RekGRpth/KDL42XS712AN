.class public interface abstract Landroid/net/dlna/DLNA;
.super Ljava/lang/Object;
.source "DLNA.java"


# virtual methods
.method public abstract AsyncSearchDevice()V
.end method

.method public abstract CreateMediaRendererDevice()Landroid/net/dlna/MediaRendererDevice;
.end method

.method public abstract CreateMediaServerDevice()Landroid/net/dlna/MediaServerDevice;
.end method

.method public abstract Finalize()V
.end method

.method public abstract GetIP()Ljava/lang/String;
.end method

.method public abstract GetMediaRendererControllerList()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/dlna/MediaRendererController;",
            ">;"
        }
    .end annotation
.end method

.method public abstract GetMediaServerControllerList()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/dlna/MediaServerController;",
            ">;"
        }
    .end annotation
.end method

.method public abstract GetPort()I
.end method

.method public abstract GetVersion()Ljava/lang/String;
.end method

.method public abstract Initialize(Ljava/lang/String;I)Z
.end method

.method public abstract SetDescriptionFile(Ljava/lang/String;)V
.end method

.method public abstract SetDeviceListener(Landroid/net/dlna/DeviceListener;)V
.end method
