.class public Landroid/net/dlna/BrowseResult;
.super Ljava/lang/Object;
.source "BrowseResult.java"


# instance fields
.field private share_objects:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/dlna/ShareObject;",
            ">;"
        }
    .end annotation
.end field

.field private total_matches:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;I)V
    .locals 0
    .param p2    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/dlna/ShareObject;",
            ">;I)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/net/dlna/BrowseResult;->share_objects:Ljava/util/ArrayList;

    iput p2, p0, Landroid/net/dlna/BrowseResult;->total_matches:I

    return-void
.end method


# virtual methods
.method public getShareObjects()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/dlna/ShareObject;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Landroid/net/dlna/BrowseResult;->share_objects:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getTotalMatches()I
    .locals 1

    iget v0, p0, Landroid/net/dlna/BrowseResult;->total_matches:I

    return v0
.end method

.method public setShareObjects(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/dlna/ShareObject;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Landroid/net/dlna/BrowseResult;->share_objects:Ljava/util/ArrayList;

    return-void
.end method

.method public setTotalMatches(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Landroid/net/dlna/BrowseResult;->total_matches:I

    return-void
.end method
