.class public interface abstract Landroid/net/dlna/MediaRendererDevice;
.super Ljava/lang/Object;
.source "MediaRendererDevice.java"


# virtual methods
.method public abstract GetDeviceInfo()Landroid/net/dlna/DeviceInfo;
.end method

.method public abstract SetDeviceCapabilities(ILandroid/net/dlna/DeviceCapabilities;)V
.end method

.method public abstract SetDeviceInfo(Landroid/net/dlna/DeviceInfo;)V
.end method

.method public abstract SetListener(Landroid/net/dlna/MediaRendererDeviceListener;)V
.end method

.method public abstract SetMediaInfo(ILandroid/net/dlna/MediaInfo;)V
.end method

.method public abstract SetPositionInfo(ILandroid/net/dlna/PositionInfo;)V
.end method

.method public abstract SetProtocolInfo(Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/dlna/ProtocolInfo;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract SetTransportInfo(ILandroid/net/dlna/TransportInfo;)V
.end method

.method public abstract SetTransportSettings(ILandroid/net/dlna/TransportSettings;)V
.end method

.method public abstract SetVolume(ILandroid/net/dlna/VolumeInfo;)V
.end method
