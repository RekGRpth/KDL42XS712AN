.class public interface abstract Landroid/net/dlna/MediaRendererController;
.super Ljava/lang/Object;
.source "MediaRendererController.java"


# virtual methods
.method public abstract GetDeviceCapabilities(I)Landroid/net/dlna/DeviceCapabilities;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/dlna/ActionUnsupportedException;,
            Landroid/net/dlna/HostUnreachableException;
        }
    .end annotation
.end method

.method public abstract GetDeviceInfo()Landroid/net/dlna/DeviceInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/dlna/ActionUnsupportedException;,
            Landroid/net/dlna/HostUnreachableException;
        }
    .end annotation
.end method

.method public abstract GetMediaInfo(I)Landroid/net/dlna/MediaInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/dlna/ActionUnsupportedException;,
            Landroid/net/dlna/HostUnreachableException;
        }
    .end annotation
.end method

.method public abstract GetPositionInfo(I)Landroid/net/dlna/PositionInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/dlna/ActionUnsupportedException;,
            Landroid/net/dlna/HostUnreachableException;
        }
    .end annotation
.end method

.method public abstract GetProtocolInfo()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/dlna/ProtocolInfo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/dlna/ActionUnsupportedException;,
            Landroid/net/dlna/HostUnreachableException;
        }
    .end annotation
.end method

.method public abstract GetTransportInfo(I)Landroid/net/dlna/TransportInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/dlna/ActionUnsupportedException;,
            Landroid/net/dlna/HostUnreachableException;
        }
    .end annotation
.end method

.method public abstract GetTransportSettings(I)Landroid/net/dlna/TransportSettings;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/dlna/ActionUnsupportedException;,
            Landroid/net/dlna/HostUnreachableException;
        }
    .end annotation
.end method

.method public abstract GetVolume(ILandroid/net/dlna/Channel;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/dlna/ActionUnsupportedException;,
            Landroid/net/dlna/HostUnreachableException;
        }
    .end annotation
.end method

.method public abstract Next(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/dlna/ActionUnsupportedException;,
            Landroid/net/dlna/HostUnreachableException;
        }
    .end annotation
.end method

.method public abstract Pause(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/dlna/ActionUnsupportedException;,
            Landroid/net/dlna/HostUnreachableException;
        }
    .end annotation
.end method

.method public abstract Play(ILjava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/dlna/ActionUnsupportedException;,
            Landroid/net/dlna/HostUnreachableException;
        }
    .end annotation
.end method

.method public abstract Previous(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/dlna/ActionUnsupportedException;,
            Landroid/net/dlna/HostUnreachableException;
        }
    .end annotation
.end method

.method public abstract RotateClockwise(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/dlna/ActionUnsupportedException;,
            Landroid/net/dlna/HostUnreachableException;
        }
    .end annotation
.end method

.method public abstract RotateCounterClockwise(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/dlna/ActionUnsupportedException;,
            Landroid/net/dlna/HostUnreachableException;
        }
    .end annotation
.end method

.method public abstract Seek(ILandroid/net/dlna/SeekMode;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/dlna/ActionUnsupportedException;,
            Landroid/net/dlna/HostUnreachableException;
        }
    .end annotation
.end method

.method public abstract SetAVTransportURI(ILjava/lang/String;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/dlna/ActionUnsupportedException;,
            Landroid/net/dlna/HostUnreachableException;,
            Landroid/net/dlna/ActionFailedException;
        }
    .end annotation
.end method

.method public abstract SetVolume(ILandroid/net/dlna/VolumeInfo;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/dlna/ActionUnsupportedException;,
            Landroid/net/dlna/HostUnreachableException;
        }
    .end annotation
.end method

.method public abstract Stop(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/dlna/ActionUnsupportedException;,
            Landroid/net/dlna/HostUnreachableException;
        }
    .end annotation
.end method

.method public abstract ZoomIn(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/dlna/ActionUnsupportedException;,
            Landroid/net/dlna/HostUnreachableException;
        }
    .end annotation
.end method

.method public abstract ZoomOut(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/dlna/ActionUnsupportedException;,
            Landroid/net/dlna/HostUnreachableException;
        }
    .end annotation
.end method
