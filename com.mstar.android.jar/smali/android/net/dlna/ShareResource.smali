.class public Landroid/net/dlna/ShareResource;
.super Ljava/lang/Object;
.source "ShareResource.java"


# instance fields
.field private protocol_info:Landroid/net/dlna/ProtocolInfo;

.field private size:J

.field private uri:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getProtocolInfo()Landroid/net/dlna/ProtocolInfo;
    .locals 1

    iget-object v0, p0, Landroid/net/dlna/ShareResource;->protocol_info:Landroid/net/dlna/ProtocolInfo;

    return-object v0
.end method

.method public getSize()J
    .locals 2

    iget-wide v0, p0, Landroid/net/dlna/ShareResource;->size:J

    return-wide v0
.end method

.method public getURI()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/net/dlna/ShareResource;->uri:Ljava/lang/String;

    return-object v0
.end method

.method public setProtocolInfo(Landroid/net/dlna/ProtocolInfo;)V
    .locals 0
    .param p1    # Landroid/net/dlna/ProtocolInfo;

    iput-object p1, p0, Landroid/net/dlna/ShareResource;->protocol_info:Landroid/net/dlna/ProtocolInfo;

    return-void
.end method

.method public setSize(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Landroid/net/dlna/ShareResource;->size:J

    return-void
.end method

.method public setURI(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Landroid/net/dlna/ShareResource;->uri:Ljava/lang/String;

    return-void
.end method
