.class public Landroid/net/dlna/VolumeInfo;
.super Ljava/lang/Object;
.source "VolumeInfo.java"


# instance fields
.field private channel:Landroid/net/dlna/Channel;

.field private current_volume:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/net/dlna/Channel;I)V
    .locals 0
    .param p1    # Landroid/net/dlna/Channel;
    .param p2    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/net/dlna/VolumeInfo;->channel:Landroid/net/dlna/Channel;

    iput p2, p0, Landroid/net/dlna/VolumeInfo;->current_volume:I

    return-void
.end method


# virtual methods
.method public getChannel()Landroid/net/dlna/Channel;
    .locals 1

    iget-object v0, p0, Landroid/net/dlna/VolumeInfo;->channel:Landroid/net/dlna/Channel;

    return-object v0
.end method

.method public getCurrentVolume()I
    .locals 1

    iget v0, p0, Landroid/net/dlna/VolumeInfo;->current_volume:I

    return v0
.end method

.method public setChannel(Landroid/net/dlna/Channel;)V
    .locals 0
    .param p1    # Landroid/net/dlna/Channel;

    iput-object p1, p0, Landroid/net/dlna/VolumeInfo;->channel:Landroid/net/dlna/Channel;

    return-void
.end method

.method public setCurrentVolume(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Landroid/net/dlna/VolumeInfo;->current_volume:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "VolumeInfo [ channel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/net/dlna/VolumeInfo;->channel:Landroid/net/dlna/Channel;

    invoke-virtual {v1}, Landroid/net/dlna/Channel;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " current_volume "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Landroid/net/dlna/VolumeInfo;->current_volume:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
