.class public Landroid/net/dlna/DLNAFactory;
.super Ljava/lang/Object;
.source "DLNAFactory.java"


# static fields
.field private static instance:Landroid/net/dlna/DLNA;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized CreateInstance()Landroid/net/dlna/DLNA;
    .locals 2

    const-class v1, Landroid/net/dlna/DLNAFactory;

    monitor-enter v1

    :try_start_0
    sget-object v0, Landroid/net/dlna/DLNAFactory;->instance:Landroid/net/dlna/DLNA;

    if-nez v0, :cond_0

    new-instance v0, Landroid/net/dlna/DLNAImpl;

    invoke-direct {v0}, Landroid/net/dlna/DLNAImpl;-><init>()V

    sput-object v0, Landroid/net/dlna/DLNAFactory;->instance:Landroid/net/dlna/DLNA;

    :cond_0
    sget-object v0, Landroid/net/dlna/DLNAFactory;->instance:Landroid/net/dlna/DLNA;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
