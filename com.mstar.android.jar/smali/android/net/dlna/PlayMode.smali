.class public final enum Landroid/net/dlna/PlayMode;
.super Ljava/lang/Enum;
.source "PlayMode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/net/dlna/PlayMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/net/dlna/PlayMode;

.field public static final enum NORMAL:Landroid/net/dlna/PlayMode;

.field public static final enum REPEAT_ALL:Landroid/net/dlna/PlayMode;

.field public static final enum SHUFFLE:Landroid/net/dlna/PlayMode;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Landroid/net/dlna/PlayMode;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v2}, Landroid/net/dlna/PlayMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/net/dlna/PlayMode;->NORMAL:Landroid/net/dlna/PlayMode;

    new-instance v0, Landroid/net/dlna/PlayMode;

    const-string v1, "SHUFFLE"

    invoke-direct {v0, v1, v3}, Landroid/net/dlna/PlayMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/net/dlna/PlayMode;->SHUFFLE:Landroid/net/dlna/PlayMode;

    new-instance v0, Landroid/net/dlna/PlayMode;

    const-string v1, "REPEAT_ALL"

    invoke-direct {v0, v1, v4}, Landroid/net/dlna/PlayMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/net/dlna/PlayMode;->REPEAT_ALL:Landroid/net/dlna/PlayMode;

    const/4 v0, 0x3

    new-array v0, v0, [Landroid/net/dlna/PlayMode;

    sget-object v1, Landroid/net/dlna/PlayMode;->NORMAL:Landroid/net/dlna/PlayMode;

    aput-object v1, v0, v2

    sget-object v1, Landroid/net/dlna/PlayMode;->SHUFFLE:Landroid/net/dlna/PlayMode;

    aput-object v1, v0, v3

    sget-object v1, Landroid/net/dlna/PlayMode;->REPEAT_ALL:Landroid/net/dlna/PlayMode;

    aput-object v1, v0, v4

    sput-object v0, Landroid/net/dlna/PlayMode;->$VALUES:[Landroid/net/dlna/PlayMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/net/dlna/PlayMode;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Landroid/net/dlna/PlayMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Landroid/net/dlna/PlayMode;

    return-object v0
.end method

.method public static values()[Landroid/net/dlna/PlayMode;
    .locals 1

    sget-object v0, Landroid/net/dlna/PlayMode;->$VALUES:[Landroid/net/dlna/PlayMode;

    invoke-virtual {v0}, [Landroid/net/dlna/PlayMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/net/dlna/PlayMode;

    return-object v0
.end method
