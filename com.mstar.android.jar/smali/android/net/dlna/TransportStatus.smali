.class public final enum Landroid/net/dlna/TransportStatus;
.super Ljava/lang/Enum;
.source "TransportStatus.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Landroid/net/dlna/TransportStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Landroid/net/dlna/TransportStatus;

.field public static final enum ERROR_OCCURRED:Landroid/net/dlna/TransportStatus;

.field public static final enum OK:Landroid/net/dlna/TransportStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Landroid/net/dlna/TransportStatus;

    const-string v1, "OK"

    invoke-direct {v0, v1, v2}, Landroid/net/dlna/TransportStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/net/dlna/TransportStatus;->OK:Landroid/net/dlna/TransportStatus;

    new-instance v0, Landroid/net/dlna/TransportStatus;

    const-string v1, "ERROR_OCCURRED"

    invoke-direct {v0, v1, v3}, Landroid/net/dlna/TransportStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Landroid/net/dlna/TransportStatus;->ERROR_OCCURRED:Landroid/net/dlna/TransportStatus;

    const/4 v0, 0x2

    new-array v0, v0, [Landroid/net/dlna/TransportStatus;

    sget-object v1, Landroid/net/dlna/TransportStatus;->OK:Landroid/net/dlna/TransportStatus;

    aput-object v1, v0, v2

    sget-object v1, Landroid/net/dlna/TransportStatus;->ERROR_OCCURRED:Landroid/net/dlna/TransportStatus;

    aput-object v1, v0, v3

    sput-object v0, Landroid/net/dlna/TransportStatus;->$VALUES:[Landroid/net/dlna/TransportStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Landroid/net/dlna/TransportStatus;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Landroid/net/dlna/TransportStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Landroid/net/dlna/TransportStatus;

    return-object v0
.end method

.method public static values()[Landroid/net/dlna/TransportStatus;
    .locals 1

    sget-object v0, Landroid/net/dlna/TransportStatus;->$VALUES:[Landroid/net/dlna/TransportStatus;

    invoke-virtual {v0}, [Landroid/net/dlna/TransportStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/net/dlna/TransportStatus;

    return-object v0
.end method
