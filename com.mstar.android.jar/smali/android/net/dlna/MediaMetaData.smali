.class public Landroid/net/dlna/MediaMetaData;
.super Ljava/lang/Object;
.source "MediaMetaData.java"


# instance fields
.field bitrate:I

.field bits_persample:I

.field color_depth:I

.field duration:J

.field mime_type:Ljava/lang/String;

.field nr_audio_channels:I

.field resolution:Ljava/lang/String;

.field sample_frequency:I

.field size:J


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(JJIIIILjava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p1    # J
    .param p3    # J
    .param p5    # I
    .param p6    # I
    .param p7    # I
    .param p8    # I
    .param p9    # Ljava/lang/String;
    .param p10    # I
    .param p11    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Landroid/net/dlna/MediaMetaData;->size:J

    iput-wide p3, p0, Landroid/net/dlna/MediaMetaData;->duration:J

    iput p5, p0, Landroid/net/dlna/MediaMetaData;->bitrate:I

    iput p6, p0, Landroid/net/dlna/MediaMetaData;->sample_frequency:I

    iput p7, p0, Landroid/net/dlna/MediaMetaData;->bits_persample:I

    iput p8, p0, Landroid/net/dlna/MediaMetaData;->nr_audio_channels:I

    iput-object p9, p0, Landroid/net/dlna/MediaMetaData;->resolution:Ljava/lang/String;

    iput p10, p0, Landroid/net/dlna/MediaMetaData;->color_depth:I

    iput-object p11, p0, Landroid/net/dlna/MediaMetaData;->mime_type:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getBitrate()I
    .locals 1

    iget v0, p0, Landroid/net/dlna/MediaMetaData;->bitrate:I

    return v0
.end method

.method public getBitsPersample()I
    .locals 1

    iget v0, p0, Landroid/net/dlna/MediaMetaData;->bits_persample:I

    return v0
.end method

.method public getColorDepth()I
    .locals 1

    iget v0, p0, Landroid/net/dlna/MediaMetaData;->color_depth:I

    return v0
.end method

.method public getDuration()J
    .locals 2

    iget-wide v0, p0, Landroid/net/dlna/MediaMetaData;->duration:J

    return-wide v0
.end method

.method public getMIMEType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/net/dlna/MediaMetaData;->mime_type:Ljava/lang/String;

    return-object v0
.end method

.method public getNrAudioChannels()I
    .locals 1

    iget v0, p0, Landroid/net/dlna/MediaMetaData;->nr_audio_channels:I

    return v0
.end method

.method public getResolution()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/net/dlna/MediaMetaData;->resolution:Ljava/lang/String;

    return-object v0
.end method

.method public getSampleFrequency()I
    .locals 1

    iget v0, p0, Landroid/net/dlna/MediaMetaData;->sample_frequency:I

    return v0
.end method

.method public getSize()J
    .locals 2

    iget-wide v0, p0, Landroid/net/dlna/MediaMetaData;->size:J

    return-wide v0
.end method

.method public setBitrate(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Landroid/net/dlna/MediaMetaData;->bitrate:I

    return-void
.end method

.method public setBitsPersample(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Landroid/net/dlna/MediaMetaData;->bits_persample:I

    return-void
.end method

.method public setColorDepth(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Landroid/net/dlna/MediaMetaData;->color_depth:I

    return-void
.end method

.method public setDuration(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Landroid/net/dlna/MediaMetaData;->duration:J

    return-void
.end method

.method public setMIMEType(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Landroid/net/dlna/MediaMetaData;->mime_type:Ljava/lang/String;

    return-void
.end method

.method public setNrAudioChannels(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Landroid/net/dlna/MediaMetaData;->nr_audio_channels:I

    return-void
.end method

.method public setResolution(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Landroid/net/dlna/MediaMetaData;->resolution:Ljava/lang/String;

    return-void
.end method

.method public setSampleFrequency(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Landroid/net/dlna/MediaMetaData;->sample_frequency:I

    return-void
.end method

.method public setSize(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Landroid/net/dlna/MediaMetaData;->size:J

    return-void
.end method
