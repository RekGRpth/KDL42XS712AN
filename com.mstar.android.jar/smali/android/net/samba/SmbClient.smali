.class public Landroid/net/samba/SmbClient;
.super Ljava/lang/Object;
.source "SmbClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/net/samba/SmbClient$scanHostByPing;
    }
.end annotation


# static fields
.field private static final MAX_THREAD_COUNT:I = 0x8

.field private static final bit0:I = 0x1

.field private static final bit1:I = 0x2

.field private static final bit10:I = 0x400

.field private static final bit11:I = 0x800

.field private static final bit12:I = 0x1000

.field private static final bit13:I = 0x2000

.field private static final bit14:I = 0x4000

.field private static final bit15:I = 0x8000

.field private static final bit2:I = 0x4

.field private static final bit3:I = 0x8

.field private static final bit4:I = 0x10

.field private static final bit5:I = 0x20

.field private static final bit6:I = 0x40

.field private static final bit7:I = 0x80

.field private static final bit8:I = 0x100

.field private static final bit9:I = 0x200

.field private static logEnable:Z = false

.field private static final smbclient_version:Ljava/lang/String; = "00.14"


# instance fields
.field private ScanCompleted:I

.field deviceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/samba/SmbDevice;",
            ">;"
        }
    .end annotation
.end field

.field private isScanning:Z

.field private onRecvMsglistener:Landroid/net/samba/OnRecvMsgListener;

.field private volatile stoprun:Z

.field private timeout:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Landroid/net/samba/SmbClient;->logEnable:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/net/samba/SmbClient;->deviceList:Ljava/util/ArrayList;

    const/16 v0, 0x1388

    iput v0, p0, Landroid/net/samba/SmbClient;->timeout:I

    iput-boolean v1, p0, Landroid/net/samba/SmbClient;->isScanning:Z

    iput-boolean v1, p0, Landroid/net/samba/SmbClient;->stoprun:Z

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/net/samba/SmbClient;->onRecvMsglistener:Landroid/net/samba/OnRecvMsgListener;

    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    sget-boolean v0, Landroid/net/samba/SmbClient;->logEnable:Z

    return v0
.end method

.method static synthetic access$100(Landroid/net/samba/SmbClient;)Z
    .locals 1
    .param p0    # Landroid/net/samba/SmbClient;

    iget-boolean v0, p0, Landroid/net/samba/SmbClient;->stoprun:Z

    return v0
.end method

.method static synthetic access$200(Landroid/net/samba/SmbClient;)I
    .locals 1
    .param p0    # Landroid/net/samba/SmbClient;

    iget v0, p0, Landroid/net/samba/SmbClient;->timeout:I

    return v0
.end method

.method static synthetic access$300(Landroid/net/samba/SmbClient;Ljava/lang/String;I)Z
    .locals 1
    .param p0    # Landroid/net/samba/SmbClient;
    .param p1    # Ljava/lang/String;
    .param p2    # I

    invoke-direct {p0, p1, p2}, Landroid/net/samba/SmbClient;->pingHost(Ljava/lang/String;I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Landroid/net/samba/SmbClient;)I
    .locals 1
    .param p0    # Landroid/net/samba/SmbClient;

    iget v0, p0, Landroid/net/samba/SmbClient;->ScanCompleted:I

    return v0
.end method

.method static synthetic access$476(Landroid/net/samba/SmbClient;I)I
    .locals 1
    .param p0    # Landroid/net/samba/SmbClient;
    .param p1    # I

    iget v0, p0, Landroid/net/samba/SmbClient;->ScanCompleted:I

    or-int/2addr v0, p1

    iput v0, p0, Landroid/net/samba/SmbClient;->ScanCompleted:I

    return v0
.end method

.method static synthetic access$502(Landroid/net/samba/SmbClient;Z)Z
    .locals 0
    .param p0    # Landroid/net/samba/SmbClient;
    .param p1    # Z

    iput-boolean p1, p0, Landroid/net/samba/SmbClient;->isScanning:Z

    return p1
.end method

.method private getLocalIP()Ljava/lang/String;
    .locals 9

    const/4 v5, 0x0

    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/net/NetworkInterface;

    invoke-virtual {v4}, Ljava/net/NetworkInterface;->getInetAddresses()Ljava/util/Enumeration;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/net/InetAddress;

    invoke-virtual {v3}, Ljava/net/InetAddress;->isLoopbackAddress()Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v3}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    goto :goto_0

    :catch_0
    move-exception v2

    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "WifiPreference IpAddress"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/net/SocketException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_2
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ip addr=:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-object v5
.end method

.method public static initSamba()V
    .locals 2

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "   smbclient_version : 00.14"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const-string v0, "jcifs.encoding"

    const-string v1, "GBK"

    invoke-static {v0, v1}, Ljcifs/Config;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "jcifs.smb.lmCompatibility"

    const-string v1, "0"

    invoke-static {v0, v1}, Ljcifs/Config;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "jcifs.smb.client.responseTimeout"

    const-string v1, "10000"

    invoke-static {v0, v1}, Ljcifs/Config;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "jcifs.netbios.retryTimeout"

    const-string v1, "10000"

    invoke-static {v0, v1}, Ljcifs/Config;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    const-string v0, "jcifs.smb.client.dfs.disabled"

    const-string v1, "true"

    invoke-static {v0, v1}, Ljcifs/Config;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    invoke-static {}, Ljcifs/Config;->registerSmbURLHandler()V

    return-void
.end method

.method private pingHost(Ljava/lang/String;I)Z
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # I

    const/4 v3, 0x0

    :try_start_0
    invoke-static {p1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/net/InetAddress;->isReachable(I)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ping  -w "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-boolean v4, Landroid/net/samba/SmbClient;->logEnable:Z

    if-eqz v4, :cond_0

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v4, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    return v3

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public SetPingTimeout(I)V
    .locals 1
    .param p1    # I

    const/16 v0, 0x64

    if-le p1, v0, :cond_0

    iput p1, p0, Landroid/net/samba/SmbClient;->timeout:I

    :cond_0
    return-void
.end method

.method public StopUpdate()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/net/samba/SmbClient;->stoprun:Z

    return-void
.end method

.method public getSmbDeviceList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/samba/SmbDevice;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Landroid/net/samba/SmbClient;->deviceList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public isUpdating()Z
    .locals 1

    iget-boolean v0, p0, Landroid/net/samba/SmbClient;->isScanning:Z

    return v0
.end method

.method public setOnRecvMsgListener(Landroid/net/samba/OnRecvMsgListener;)V
    .locals 0
    .param p1    # Landroid/net/samba/OnRecvMsgListener;

    iput-object p1, p0, Landroid/net/samba/SmbClient;->onRecvMsglistener:Landroid/net/samba/OnRecvMsgListener;

    return-void
.end method

.method public updateSmbDeviceList()V
    .locals 11

    const/4 v1, 0x0

    const/4 v10, 0x1

    iget-object v0, p0, Landroid/net/samba/SmbClient;->deviceList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-direct {p0}, Landroid/net/samba/SmbClient;->getLocalIP()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "no ip address!"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/net/samba/SmbClient;->onRecvMsglistener:Landroid/net/samba/OnRecvMsgListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/net/samba/SmbClient;->onRecvMsglistener:Landroid/net/samba/OnRecvMsgListener;

    invoke-interface {v0, v10}, Landroid/net/samba/OnRecvMsgListener;->onRecvMsgListener(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v0, "127.0.0.1"

    invoke-virtual {v2, v0}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Landroid/net/samba/SmbClient;->onRecvMsglistener:Landroid/net/samba/OnRecvMsgListener;

    if-eqz v0, :cond_2

    const-string v0, ":"

    invoke-virtual {v2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iput-boolean v1, p0, Landroid/net/samba/SmbClient;->isScanning:Z

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "no ip address or recv listener! "

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/net/samba/SmbClient;->onRecvMsglistener:Landroid/net/samba/OnRecvMsgListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/net/samba/SmbClient;->onRecvMsglistener:Landroid/net/samba/OnRecvMsgListener;

    invoke-interface {v0, v10}, Landroid/net/samba/OnRecvMsgListener;->onRecvMsgListener(I)V

    goto :goto_0

    :cond_3
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iput v1, p0, Landroid/net/samba/SmbClient;->ScanCompleted:I

    const/4 v7, 0x0

    :goto_1
    const/16 v0, 0x8

    if-ge v7, v0, :cond_0

    new-instance v9, Ljava/lang/Thread;

    new-instance v0, Landroid/net/samba/SmbClient$scanHostByPing;

    add-int/lit8 v3, v7, 0x1

    mul-int/lit16 v1, v7, 0x100

    div-int/lit8 v1, v1, 0x8

    add-int/lit8 v4, v1, 0x1

    mul-int/lit16 v1, v7, 0x100

    div-int/lit8 v1, v1, 0x8

    add-int/lit8 v5, v1, 0x20

    iget-object v6, p0, Landroid/net/samba/SmbClient;->onRecvMsglistener:Landroid/net/samba/OnRecvMsgListener;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Landroid/net/samba/SmbClient$scanHostByPing;-><init>(Landroid/net/samba/SmbClient;Ljava/lang/String;IIILandroid/net/samba/OnRecvMsgListener;)V

    invoke-direct {v9, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    iput-boolean v10, p0, Landroid/net/samba/SmbClient;->isScanning:Z

    :cond_4
    add-int/lit8 v7, v7, 0x1

    goto :goto_1
.end method
