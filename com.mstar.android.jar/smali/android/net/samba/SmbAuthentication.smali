.class public Landroid/net/samba/SmbAuthentication;
.super Ljava/lang/Object;
.source "SmbAuthentication.java"


# static fields
.field public static final ANONYMOUS:Landroid/net/samba/SmbAuthentication;


# instance fields
.field private passwd:Ljava/lang/String;

.field private username:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Landroid/net/samba/SmbAuthentication;->ANONYMOUS:Landroid/net/samba/SmbAuthentication;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    const-string v0, " "

    iput-object v0, p0, Landroid/net/samba/SmbAuthentication;->username:Ljava/lang/String;

    :goto_0
    if-nez p2, :cond_1

    const-string v0, ""

    iput-object v0, p0, Landroid/net/samba/SmbAuthentication;->passwd:Ljava/lang/String;

    :goto_1
    return-void

    :cond_0
    iput-object p1, p0, Landroid/net/samba/SmbAuthentication;->username:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iput-object p2, p0, Landroid/net/samba/SmbAuthentication;->passwd:Ljava/lang/String;

    goto :goto_1
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/net/samba/SmbAuthentication;->username:Ljava/lang/String;

    return-object v0
.end method

.method public getPassword()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/net/samba/SmbAuthentication;->passwd:Ljava/lang/String;

    return-object v0
.end method
