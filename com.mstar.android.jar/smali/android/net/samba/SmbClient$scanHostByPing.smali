.class Landroid/net/samba/SmbClient$scanHostByPing;
.super Ljava/lang/Object;
.source "SmbClient.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/net/samba/SmbClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "scanHostByPing"
.end annotation


# instance fields
.field callback:Landroid/net/samba/OnRecvMsgListener;

.field endIndex:I

.field startIndex:I

.field startIp:Ljava/lang/String;

.field final synthetic this$0:Landroid/net/samba/SmbClient;

.field threadid:I


# direct methods
.method constructor <init>(Landroid/net/samba/SmbClient;Ljava/lang/String;IIILandroid/net/samba/OnRecvMsgListener;)V
    .locals 1
    .param p2    # Ljava/lang/String;
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # Landroid/net/samba/OnRecvMsgListener;

    iput-object p1, p0, Landroid/net/samba/SmbClient$scanHostByPing;->this$0:Landroid/net/samba/SmbClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/net/samba/SmbClient$scanHostByPing;->startIp:Ljava/lang/String;

    iput-object p2, p0, Landroid/net/samba/SmbClient$scanHostByPing;->startIp:Ljava/lang/String;

    iput-object p6, p0, Landroid/net/samba/SmbClient$scanHostByPing;->callback:Landroid/net/samba/OnRecvMsgListener;

    iput p4, p0, Landroid/net/samba/SmbClient$scanHostByPing;->startIndex:I

    iput p5, p0, Landroid/net/samba/SmbClient$scanHostByPing;->endIndex:I

    iput p3, p0, Landroid/net/samba/SmbClient$scanHostByPing;->threadid:I

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    const/16 v13, 0xff

    const/4 v12, 0x2

    const/4 v11, 0x0

    const/4 v10, 0x1

    iget-object v7, p0, Landroid/net/samba/SmbClient$scanHostByPing;->startIp:Ljava/lang/String;

    if-nez v7, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v7, p0, Landroid/net/samba/SmbClient$scanHostByPing;->startIp:Ljava/lang/String;

    const/16 v8, 0x2e

    invoke-virtual {v7, v8}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    iget-object v7, p0, Landroid/net/samba/SmbClient$scanHostByPing;->startIp:Ljava/lang/String;

    add-int/lit8 v8, v2, 0x1

    invoke-virtual {v7, v11, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    iget-object v7, p0, Landroid/net/samba/SmbClient$scanHostByPing;->startIp:Ljava/lang/String;

    add-int/lit8 v8, v2, 0x1

    iget-object v9, p0, Landroid/net/samba/SmbClient$scanHostByPing;->startIp:Ljava/lang/String;

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    # getter for: Landroid/net/samba/SmbClient;->logEnable:Z
    invoke-static {}, Landroid/net/samba/SmbClient;->access$000()Z

    move-result v7

    if-eqz v7, :cond_2

    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "start ip : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " ip : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_2
    iget v3, p0, Landroid/net/samba/SmbClient$scanHostByPing;->startIndex:I

    :goto_1
    iget v7, p0, Landroid/net/samba/SmbClient$scanHostByPing;->endIndex:I

    if-gt v3, v7, :cond_3

    iget-object v7, p0, Landroid/net/samba/SmbClient$scanHostByPing;->this$0:Landroid/net/samba/SmbClient;

    # getter for: Landroid/net/samba/SmbClient;->stoprun:Z
    invoke-static {v7}, Landroid/net/samba/SmbClient;->access$100(Landroid/net/samba/SmbClient;)Z

    move-result v7

    if-eq v7, v10, :cond_3

    if-gt v3, v13, :cond_3

    if-ge v3, v10, :cond_4

    :cond_3
    iget v7, p0, Landroid/net/samba/SmbClient$scanHostByPing;->threadid:I

    packed-switch v7, :pswitch_data_0

    :goto_2
    iget-object v7, p0, Landroid/net/samba/SmbClient$scanHostByPing;->this$0:Landroid/net/samba/SmbClient;

    # getter for: Landroid/net/samba/SmbClient;->ScanCompleted:I
    invoke-static {v7}, Landroid/net/samba/SmbClient;->access$400(Landroid/net/samba/SmbClient;)I

    move-result v7

    and-int/lit16 v7, v7, 0xff

    if-ne v7, v13, :cond_0

    iget-object v7, p0, Landroid/net/samba/SmbClient$scanHostByPing;->this$0:Landroid/net/samba/SmbClient;

    # setter for: Landroid/net/samba/SmbClient;->isScanning:Z
    invoke-static {v7, v11}, Landroid/net/samba/SmbClient;->access$502(Landroid/net/samba/SmbClient;Z)Z

    iget-object v7, p0, Landroid/net/samba/SmbClient$scanHostByPing;->this$0:Landroid/net/samba/SmbClient;

    # getter for: Landroid/net/samba/SmbClient;->stoprun:Z
    invoke-static {v7}, Landroid/net/samba/SmbClient;->access$100(Landroid/net/samba/SmbClient;)Z

    move-result v7

    if-ne v7, v10, :cond_7

    iget-object v7, p0, Landroid/net/samba/SmbClient$scanHostByPing;->callback:Landroid/net/samba/OnRecvMsgListener;

    invoke-interface {v7, v10}, Landroid/net/samba/OnRecvMsgListener;->onRecvMsgListener(I)V

    goto/16 :goto_0

    :cond_4
    if-eq v0, v3, :cond_5

    if-ne v3, v10, :cond_6

    :cond_5
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_6
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Landroid/net/samba/SmbClient$scanHostByPing;->this$0:Landroid/net/samba/SmbClient;

    iget-object v8, p0, Landroid/net/samba/SmbClient$scanHostByPing;->this$0:Landroid/net/samba/SmbClient;

    # getter for: Landroid/net/samba/SmbClient;->timeout:I
    invoke-static {v8}, Landroid/net/samba/SmbClient;->access$200(Landroid/net/samba/SmbClient;)I

    move-result v8

    # invokes: Landroid/net/samba/SmbClient;->pingHost(Ljava/lang/String;I)Z
    invoke-static {v7, v6, v8}, Landroid/net/samba/SmbClient;->access$300(Landroid/net/samba/SmbClient;Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_5

    monitor-enter p0

    :try_start_0
    new-instance v5, Landroid/net/samba/SmbDevice;

    invoke-direct {v5, v6}, Landroid/net/samba/SmbDevice;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Landroid/net/samba/SmbClient$scanHostByPing;->this$0:Landroid/net/samba/SmbClient;

    iget-object v7, v7, Landroid/net/samba/SmbClient;->deviceList:Ljava/util/ArrayList;

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v7, p0, Landroid/net/samba/SmbClient$scanHostByPing;->callback:Landroid/net/samba/OnRecvMsgListener;

    const/4 v8, 0x3

    invoke-interface {v7, v8}, Landroid/net/samba/OnRecvMsgListener;->onRecvMsgListener(I)V

    monitor-exit p0

    goto :goto_3

    :catchall_0
    move-exception v7

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v7

    :pswitch_0
    iget-object v7, p0, Landroid/net/samba/SmbClient$scanHostByPing;->this$0:Landroid/net/samba/SmbClient;

    # |= operator for: Landroid/net/samba/SmbClient;->ScanCompleted:I
    invoke-static {v7, v10}, Landroid/net/samba/SmbClient;->access$476(Landroid/net/samba/SmbClient;I)I

    goto :goto_2

    :pswitch_1
    iget-object v7, p0, Landroid/net/samba/SmbClient$scanHostByPing;->this$0:Landroid/net/samba/SmbClient;

    # |= operator for: Landroid/net/samba/SmbClient;->ScanCompleted:I
    invoke-static {v7, v12}, Landroid/net/samba/SmbClient;->access$476(Landroid/net/samba/SmbClient;I)I

    goto :goto_2

    :pswitch_2
    iget-object v7, p0, Landroid/net/samba/SmbClient$scanHostByPing;->this$0:Landroid/net/samba/SmbClient;

    const/4 v8, 0x4

    # |= operator for: Landroid/net/samba/SmbClient;->ScanCompleted:I
    invoke-static {v7, v8}, Landroid/net/samba/SmbClient;->access$476(Landroid/net/samba/SmbClient;I)I

    goto :goto_2

    :pswitch_3
    iget-object v7, p0, Landroid/net/samba/SmbClient$scanHostByPing;->this$0:Landroid/net/samba/SmbClient;

    const/16 v8, 0x8

    # |= operator for: Landroid/net/samba/SmbClient;->ScanCompleted:I
    invoke-static {v7, v8}, Landroid/net/samba/SmbClient;->access$476(Landroid/net/samba/SmbClient;I)I

    goto :goto_2

    :pswitch_4
    iget-object v7, p0, Landroid/net/samba/SmbClient$scanHostByPing;->this$0:Landroid/net/samba/SmbClient;

    const/16 v8, 0x10

    # |= operator for: Landroid/net/samba/SmbClient;->ScanCompleted:I
    invoke-static {v7, v8}, Landroid/net/samba/SmbClient;->access$476(Landroid/net/samba/SmbClient;I)I

    goto :goto_2

    :pswitch_5
    iget-object v7, p0, Landroid/net/samba/SmbClient$scanHostByPing;->this$0:Landroid/net/samba/SmbClient;

    const/16 v8, 0x20

    # |= operator for: Landroid/net/samba/SmbClient;->ScanCompleted:I
    invoke-static {v7, v8}, Landroid/net/samba/SmbClient;->access$476(Landroid/net/samba/SmbClient;I)I

    goto/16 :goto_2

    :pswitch_6
    iget-object v7, p0, Landroid/net/samba/SmbClient$scanHostByPing;->this$0:Landroid/net/samba/SmbClient;

    const/16 v8, 0x40

    # |= operator for: Landroid/net/samba/SmbClient;->ScanCompleted:I
    invoke-static {v7, v8}, Landroid/net/samba/SmbClient;->access$476(Landroid/net/samba/SmbClient;I)I

    goto/16 :goto_2

    :pswitch_7
    iget-object v7, p0, Landroid/net/samba/SmbClient$scanHostByPing;->this$0:Landroid/net/samba/SmbClient;

    const/16 v8, 0x80

    # |= operator for: Landroid/net/samba/SmbClient;->ScanCompleted:I
    invoke-static {v7, v8}, Landroid/net/samba/SmbClient;->access$476(Landroid/net/samba/SmbClient;I)I

    goto/16 :goto_2

    :pswitch_8
    iget-object v7, p0, Landroid/net/samba/SmbClient$scanHostByPing;->this$0:Landroid/net/samba/SmbClient;

    const/16 v8, 0x100

    # |= operator for: Landroid/net/samba/SmbClient;->ScanCompleted:I
    invoke-static {v7, v8}, Landroid/net/samba/SmbClient;->access$476(Landroid/net/samba/SmbClient;I)I

    goto/16 :goto_2

    :pswitch_9
    iget-object v7, p0, Landroid/net/samba/SmbClient$scanHostByPing;->this$0:Landroid/net/samba/SmbClient;

    const/16 v8, 0x200

    # |= operator for: Landroid/net/samba/SmbClient;->ScanCompleted:I
    invoke-static {v7, v8}, Landroid/net/samba/SmbClient;->access$476(Landroid/net/samba/SmbClient;I)I

    goto/16 :goto_2

    :pswitch_a
    iget-object v7, p0, Landroid/net/samba/SmbClient$scanHostByPing;->this$0:Landroid/net/samba/SmbClient;

    const/16 v8, 0x400

    # |= operator for: Landroid/net/samba/SmbClient;->ScanCompleted:I
    invoke-static {v7, v8}, Landroid/net/samba/SmbClient;->access$476(Landroid/net/samba/SmbClient;I)I

    goto/16 :goto_2

    :pswitch_b
    iget-object v7, p0, Landroid/net/samba/SmbClient$scanHostByPing;->this$0:Landroid/net/samba/SmbClient;

    const/16 v8, 0x800

    # |= operator for: Landroid/net/samba/SmbClient;->ScanCompleted:I
    invoke-static {v7, v8}, Landroid/net/samba/SmbClient;->access$476(Landroid/net/samba/SmbClient;I)I

    goto/16 :goto_2

    :pswitch_c
    iget-object v7, p0, Landroid/net/samba/SmbClient$scanHostByPing;->this$0:Landroid/net/samba/SmbClient;

    const/16 v8, 0x1000

    # |= operator for: Landroid/net/samba/SmbClient;->ScanCompleted:I
    invoke-static {v7, v8}, Landroid/net/samba/SmbClient;->access$476(Landroid/net/samba/SmbClient;I)I

    goto/16 :goto_2

    :pswitch_d
    iget-object v7, p0, Landroid/net/samba/SmbClient$scanHostByPing;->this$0:Landroid/net/samba/SmbClient;

    const/16 v8, 0x2000

    # |= operator for: Landroid/net/samba/SmbClient;->ScanCompleted:I
    invoke-static {v7, v8}, Landroid/net/samba/SmbClient;->access$476(Landroid/net/samba/SmbClient;I)I

    goto/16 :goto_2

    :pswitch_e
    iget-object v7, p0, Landroid/net/samba/SmbClient$scanHostByPing;->this$0:Landroid/net/samba/SmbClient;

    const/16 v8, 0x4000

    # |= operator for: Landroid/net/samba/SmbClient;->ScanCompleted:I
    invoke-static {v7, v8}, Landroid/net/samba/SmbClient;->access$476(Landroid/net/samba/SmbClient;I)I

    goto/16 :goto_2

    :pswitch_f
    iget-object v7, p0, Landroid/net/samba/SmbClient$scanHostByPing;->this$0:Landroid/net/samba/SmbClient;

    const v8, 0x8000

    # |= operator for: Landroid/net/samba/SmbClient;->ScanCompleted:I
    invoke-static {v7, v8}, Landroid/net/samba/SmbClient;->access$476(Landroid/net/samba/SmbClient;I)I

    goto/16 :goto_2

    :cond_7
    iget-object v7, p0, Landroid/net/samba/SmbClient$scanHostByPing;->callback:Landroid/net/samba/OnRecvMsgListener;

    invoke-interface {v7, v12}, Landroid/net/samba/OnRecvMsgListener;->onRecvMsgListener(I)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method
