.class public Landroid/net/samba/SambaFile;
.super Ljava/lang/Object;
.source "SambaFile.java"


# instance fields
.field file:Ljcifs/smb/SmbFile;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/MalformedURLException;,
            Landroid/net/samba/SambaException;
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    new-instance v2, Landroid/net/samba/SambaException;

    const/4 v3, 0x2

    invoke-direct {v2, v3}, Landroid/net/samba/SambaException;-><init>(I)V

    throw v2

    :cond_0
    new-instance v2, Ljcifs/smb/SmbFile;

    invoke-direct {v2, p1}, Ljcifs/smb/SmbFile;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Landroid/net/samba/SambaFile;->file:Ljcifs/smb/SmbFile;

    :try_start_0
    iget-object v2, p0, Landroid/net/samba/SambaFile;->file:Ljcifs/smb/SmbFile;

    invoke-virtual {v2}, Ljcifs/smb/SmbFile;->getType()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    const/16 v2, 0x8

    if-eq v1, v2, :cond_2

    :cond_1
    new-instance v2, Landroid/net/samba/SambaException;

    const/4 v3, 0x3

    invoke-direct {v2, v3}, Landroid/net/samba/SambaException;-><init>(I)V

    throw v2
    :try_end_0
    .catch Ljcifs/smb/SmbException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    new-instance v2, Landroid/net/samba/SambaException;

    invoke-direct {v2, v0}, Landroid/net/samba/SambaException;-><init>(Ljcifs/smb/SmbException;)V

    throw v2

    :cond_2
    return-void
.end method


# virtual methods
.method public canRead()Z
    .locals 3

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Landroid/net/samba/SambaFile;->file:Ljcifs/smb/SmbFile;

    invoke-virtual {v2}, Ljcifs/smb/SmbFile;->canRead()Z
    :try_end_0
    .catch Ljcifs/smb/SmbException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljcifs/smb/SmbException;->printStackTrace()V

    goto :goto_0
.end method

.method public canWrite()Z
    .locals 3

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Landroid/net/samba/SambaFile;->file:Ljcifs/smb/SmbFile;

    invoke-virtual {v2}, Ljcifs/smb/SmbFile;->canWrite()Z
    :try_end_0
    .catch Ljcifs/smb/SmbException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljcifs/smb/SmbException;->printStackTrace()V

    goto :goto_0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/net/samba/SambaFile;->file:Ljcifs/smb/SmbFile;

    invoke-virtual {v0}, Ljcifs/smb/SmbFile;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/net/samba/SambaFile;->file:Ljcifs/smb/SmbFile;

    invoke-virtual {v0}, Ljcifs/smb/SmbFile;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isDirectory()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/samba/SambaException;
        }
    .end annotation

    :try_start_0
    iget-object v1, p0, Landroid/net/samba/SambaFile;->file:Ljcifs/smb/SmbFile;

    invoke-virtual {v1}, Ljcifs/smb/SmbFile;->isDirectory()Z
    :try_end_0
    .catch Ljcifs/smb/SmbException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    return v1

    :catch_0
    move-exception v0

    new-instance v1, Landroid/net/samba/SambaException;

    invoke-direct {v1, v0}, Landroid/net/samba/SambaException;-><init>(Ljcifs/smb/SmbException;)V

    throw v1
.end method

.method public isExists()Z
    .locals 2

    :try_start_0
    iget-object v1, p0, Landroid/net/samba/SambaFile;->file:Ljcifs/smb/SmbFile;

    invoke-virtual {v1}, Ljcifs/smb/SmbFile;->exists()Z
    :try_end_0
    .catch Ljcifs/smb/SmbException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljcifs/smb/SmbException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isFile()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/net/samba/SambaException;
        }
    .end annotation

    :try_start_0
    iget-object v1, p0, Landroid/net/samba/SambaFile;->file:Ljcifs/smb/SmbFile;

    invoke-virtual {v1}, Ljcifs/smb/SmbFile;->isFile()Z
    :try_end_0
    .catch Ljcifs/smb/SmbException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    return v1

    :catch_0
    move-exception v0

    new-instance v1, Landroid/net/samba/SambaException;

    invoke-direct {v1, v0}, Landroid/net/samba/SambaException;-><init>(Ljcifs/smb/SmbException;)V

    throw v1
.end method
