.class public Landroid/net/samba/SmbDevice;
.super Ljava/lang/Object;
.source "SmbDevice.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Landroid/net/samba/SmbDevice;",
            ">;"
        }
    .end annotation
.end field

.field private static final DEF_SMB_DEV_NAME:Ljava/lang/String; = "Default Smb Device"

.field public static final FLAG_WRITEABLE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "SmbDevice"

.field private static final localpath:Ljava/lang/String; = "/mnt/samba/"


# instance fields
.field private DEBUG:Z

.field private SmbURL:Ljava/lang/String;

.field private auth:Landroid/net/samba/SmbAuthentication;

.field folderList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/samba/SmbShareFolder;",
            ">;"
        }
    .end annotation
.end field

.field private ip:Ljava/lang/String;

.field private mHasPassWord:Z

.field private mMountedFlag:Z

.field private mName:Ljava/lang/String;

.field private mPassWord:Ljava/lang/String;

.field private mUser:Ljava/lang/String;

.field private mountPointByIp:Z

.field private onRecvMsg:Landroid/net/samba/OnRecvMsg;

.field root:Ljcifs/smb/SmbFile;

.field private stm:Lcom/mstar/android/storage/MStorageManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/net/samba/SmbDevice$1;

    invoke-direct {v0}, Landroid/net/samba/SmbDevice$1;-><init>()V

    sput-object v0, Landroid/net/samba/SmbDevice;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/net/samba/SmbDevice;->folderList:Ljava/util/ArrayList;

    iput-object v1, p0, Landroid/net/samba/SmbDevice;->ip:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Landroid/net/samba/SmbDevice;->mUser:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Landroid/net/samba/SmbDevice;->mPassWord:Ljava/lang/String;

    const-string v0, "Default Smb Device"

    iput-object v0, p0, Landroid/net/samba/SmbDevice;->mName:Ljava/lang/String;

    iput-object v1, p0, Landroid/net/samba/SmbDevice;->onRecvMsg:Landroid/net/samba/OnRecvMsg;

    iput-object v1, p0, Landroid/net/samba/SmbDevice;->SmbURL:Ljava/lang/String;

    iput-object v1, p0, Landroid/net/samba/SmbDevice;->auth:Landroid/net/samba/SmbAuthentication;

    iput-boolean v2, p0, Landroid/net/samba/SmbDevice;->mountPointByIp:Z

    iput-boolean v2, p0, Landroid/net/samba/SmbDevice;->mMountedFlag:Z

    iput-boolean v2, p0, Landroid/net/samba/SmbDevice;->mHasPassWord:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/net/samba/SmbDevice;->DEBUG:Z

    const-string v0, ""

    iput-object v0, p0, Landroid/net/samba/SmbDevice;->ip:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Landroid/net/samba/SmbDevice;->mUser:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Landroid/net/samba/SmbDevice;->mPassWord:Ljava/lang/String;

    const-string v0, "Default Smb Device"

    iput-object v0, p0, Landroid/net/samba/SmbDevice;->mName:Ljava/lang/String;

    const-string v0, "jcifs.encoding"

    const-string v1, "GBK"

    invoke-static {v0, v1}, Ljcifs/Config;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1    # Landroid/os/Parcel;

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/net/samba/SmbDevice;->folderList:Ljava/util/ArrayList;

    iput-object v1, p0, Landroid/net/samba/SmbDevice;->ip:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Landroid/net/samba/SmbDevice;->mUser:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Landroid/net/samba/SmbDevice;->mPassWord:Ljava/lang/String;

    const-string v0, "Default Smb Device"

    iput-object v0, p0, Landroid/net/samba/SmbDevice;->mName:Ljava/lang/String;

    iput-object v1, p0, Landroid/net/samba/SmbDevice;->onRecvMsg:Landroid/net/samba/OnRecvMsg;

    iput-object v1, p0, Landroid/net/samba/SmbDevice;->SmbURL:Ljava/lang/String;

    iput-object v1, p0, Landroid/net/samba/SmbDevice;->auth:Landroid/net/samba/SmbAuthentication;

    iput-boolean v2, p0, Landroid/net/samba/SmbDevice;->mountPointByIp:Z

    iput-boolean v2, p0, Landroid/net/samba/SmbDevice;->mMountedFlag:Z

    iput-boolean v2, p0, Landroid/net/samba/SmbDevice;->mHasPassWord:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/net/samba/SmbDevice;->DEBUG:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/net/samba/SmbDevice;->ip:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/net/samba/SmbDevice;->mUser:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/net/samba/SmbDevice;->mPassWord:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/net/samba/SmbDevice;->mName:Ljava/lang/String;

    new-instance v0, Landroid/net/samba/SmbAuthentication;

    iget-object v1, p0, Landroid/net/samba/SmbDevice;->mUser:Ljava/lang/String;

    iget-object v2, p0, Landroid/net/samba/SmbDevice;->mPassWord:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Landroid/net/samba/SmbAuthentication;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Landroid/net/samba/SmbDevice;->auth:Landroid/net/samba/SmbAuthentication;

    const-string v0, "jcifs.encoding"

    const-string v1, "GBK"

    invoke-static {v0, v1}, Ljcifs/Config;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/net/samba/SmbDevice;->folderList:Ljava/util/ArrayList;

    iput-object v1, p0, Landroid/net/samba/SmbDevice;->ip:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Landroid/net/samba/SmbDevice;->mUser:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Landroid/net/samba/SmbDevice;->mPassWord:Ljava/lang/String;

    const-string v0, "Default Smb Device"

    iput-object v0, p0, Landroid/net/samba/SmbDevice;->mName:Ljava/lang/String;

    iput-object v1, p0, Landroid/net/samba/SmbDevice;->onRecvMsg:Landroid/net/samba/OnRecvMsg;

    iput-object v1, p0, Landroid/net/samba/SmbDevice;->SmbURL:Ljava/lang/String;

    iput-object v1, p0, Landroid/net/samba/SmbDevice;->auth:Landroid/net/samba/SmbAuthentication;

    iput-boolean v2, p0, Landroid/net/samba/SmbDevice;->mountPointByIp:Z

    iput-boolean v2, p0, Landroid/net/samba/SmbDevice;->mMountedFlag:Z

    iput-boolean v2, p0, Landroid/net/samba/SmbDevice;->mHasPassWord:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/net/samba/SmbDevice;->DEBUG:Z

    iput-object p1, p0, Landroid/net/samba/SmbDevice;->ip:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Landroid/net/samba/SmbDevice;->mUser:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Landroid/net/samba/SmbDevice;->mPassWord:Ljava/lang/String;

    const-string v0, "Default Smb Device"

    iput-object v0, p0, Landroid/net/samba/SmbDevice;->mName:Ljava/lang/String;

    const-string v0, "jcifs.encoding"

    const-string v1, "GBK"

    invoke-static {v0, v1}, Ljcifs/Config;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    return-void
.end method

.method static convertToSambaFolder(Landroid/net/samba/SmbShareFolder;Ljcifs/smb/SmbFile;)V
    .locals 0
    .param p0    # Landroid/net/samba/SmbShareFolder;
    .param p1    # Ljcifs/smb/SmbFile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljcifs/smb/SmbException;
        }
    .end annotation

    iput-object p1, p0, Landroid/net/samba/SmbShareFolder;->file:Ljcifs/smb/SmbFile;

    return-void
.end method

.method private getServerName()Ljava/lang/String;
    .locals 6

    :try_start_0
    iget-object v5, p0, Landroid/net/samba/SmbDevice;->ip:Ljava/lang/String;

    invoke-static {v5}, Ljcifs/netbios/NbtAddress;->getByName(Ljava/lang/String;)Ljcifs/netbios/NbtAddress;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljcifs/netbios/NbtAddress;->isActive()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-static {v4}, Ljcifs/netbios/NbtAddress;->getAllByAddress(Ljcifs/netbios/NbtAddress;)[Ljcifs/netbios/NbtAddress;

    move-result-object v0

    const/4 v2, 0x0

    :goto_0
    array-length v5, v0

    if-ge v2, v5, :cond_1

    aget-object v3, v0, v2

    invoke-virtual {v3}, Ljcifs/netbios/NbtAddress;->isGroupAddress()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v3}, Ljcifs/netbios/NbtAddress;->getNameType()I

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v3}, Ljcifs/netbios/NbtAddress;->getHostName()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v3}, Ljcifs/netbios/NbtAddress;->getHostName()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    :goto_1
    return-object v5

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/net/UnknownHostException;->printStackTrace()V

    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method private isChinese(Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getAddress()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/net/samba/SmbDevice;->ip:Ljava/lang/String;

    return-object v0
.end method

.method public getAuth()Landroid/net/samba/SmbAuthentication;
    .locals 1

    iget-boolean v0, p0, Landroid/net/samba/SmbDevice;->mHasPassWord:Z

    if-nez v0, :cond_0

    sget-object v0, Landroid/net/samba/SmbAuthentication;->ANONYMOUS:Landroid/net/samba/SmbAuthentication;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Landroid/net/samba/SmbDevice;->auth:Landroid/net/samba/SmbAuthentication;

    goto :goto_0
.end method

.method public getHostName()Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/UnknownHostException;
        }
    .end annotation

    invoke-direct {p0}, Landroid/net/samba/SmbDevice;->getServerName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Landroid/net/samba/SmbDevice;->mName:Ljava/lang/String;

    iget-object v1, p0, Landroid/net/samba/SmbDevice;->mName:Ljava/lang/String;

    if-nez v1, :cond_0

    iget-object v1, p0, Landroid/net/samba/SmbDevice;->ip:Ljava/lang/String;

    invoke-static {v1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Landroid/net/samba/SmbDevice;->mName:Ljava/lang/String;

    :cond_0
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " hostName : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Landroid/net/samba/SmbDevice;->mName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v1, p0, Landroid/net/samba/SmbDevice;->mName:Ljava/lang/String;

    return-object v1
.end method

.method public getSharefolderList()Ljava/util/ArrayList;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/samba/SmbShareFolder;",
            ">;"
        }
    .end annotation

    const/4 v14, 0x0

    const/4 v1, 0x0

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "smb://"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, p0, Landroid/net/samba/SmbDevice;->ip:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    iput-object v12, p0, Landroid/net/samba/SmbDevice;->SmbURL:Ljava/lang/String;

    iget-object v12, p0, Landroid/net/samba/SmbDevice;->auth:Landroid/net/samba/SmbAuthentication;

    if-eqz v12, :cond_0

    iget-object v12, p0, Landroid/net/samba/SmbDevice;->auth:Landroid/net/samba/SmbAuthentication;

    invoke-virtual {v12}, Landroid/net/samba/SmbAuthentication;->getName()Ljava/lang/String;

    move-result-object v11

    iget-object v12, p0, Landroid/net/samba/SmbDevice;->auth:Landroid/net/samba/SmbAuthentication;

    invoke-virtual {v12}, Landroid/net/samba/SmbAuthentication;->getPassword()Ljava/lang/String;

    move-result-object v8

    const-string v12, "\\"

    invoke-virtual {v11, v12}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    if-lez v5, :cond_1

    iget-object v12, p0, Landroid/net/samba/SmbDevice;->auth:Landroid/net/samba/SmbAuthentication;

    invoke-virtual {v12}, Landroid/net/samba/SmbAuthentication;->getName()Ljava/lang/String;

    move-result-object v12

    add-int/lit8 v13, v5, 0x1

    invoke-virtual {v12, v13}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v11

    iget-object v12, p0, Landroid/net/samba/SmbDevice;->auth:Landroid/net/samba/SmbAuthentication;

    invoke-virtual {v12}, Landroid/net/samba/SmbAuthentication;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12, v14, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    new-instance v1, Ljcifs/smb/NtlmPasswordAuthentication;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ";"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ":"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v1, v12}, Ljcifs/smb/NtlmPasswordAuthentication;-><init>(Ljava/lang/String;)V

    :goto_0
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, " NtlmPasswordAuthentication = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :try_start_0
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, " SmbURL "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, p0, Landroid/net/samba/SmbDevice;->SmbURL:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v12, p0, Landroid/net/samba/SmbDevice;->SmbURL:Ljava/lang/String;

    if-nez v12, :cond_2

    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v13, " SmbURL == null"

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljcifs/smb/SmbException; {:try_start_0 .. :try_end_0} :catch_2

    const/4 v12, 0x0

    :goto_1
    return-object v12

    :cond_1
    const/4 v3, 0x0

    new-instance v1, Ljcifs/smb/NtlmPasswordAuthentication;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ":"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v1, v12}, Ljcifs/smb/NtlmPasswordAuthentication;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    :try_start_1
    iget-object v12, p0, Landroid/net/samba/SmbDevice;->auth:Landroid/net/samba/SmbAuthentication;

    if-nez v12, :cond_5

    new-instance v12, Ljcifs/smb/SmbFile;

    iget-object v13, p0, Landroid/net/samba/SmbDevice;->SmbURL:Ljava/lang/String;

    invoke-direct {v12, v13}, Ljcifs/smb/SmbFile;-><init>(Ljava/lang/String;)V

    iput-object v12, p0, Landroid/net/samba/SmbDevice;->root:Ljcifs/smb/SmbFile;

    :goto_2
    iget-object v12, p0, Landroid/net/samba/SmbDevice;->root:Ljcifs/smb/SmbFile;

    invoke-virtual {v12}, Ljcifs/smb/SmbFile;->listFiles()[Ljcifs/smb/SmbFile;

    move-result-object v0

    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "list1 number: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    array-length v14, v0

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v6, 0x0

    :goto_3
    array-length v12, v0

    if-ge v6, v12, :cond_9

    aget-object v12, v0, v6

    invoke-virtual {p0, v12}, Landroid/net/samba/SmbDevice;->isHost(Ljcifs/smb/SmbFile;)Z

    move-result v12

    if-eqz v12, :cond_3

    new-instance v10, Landroid/net/samba/SmbShareFolder;

    invoke-direct {v10, p0}, Landroid/net/samba/SmbShareFolder;-><init>(Landroid/net/samba/SmbDevice;)V

    aget-object v12, v0, v6

    invoke-static {v10, v12}, Landroid/net/samba/SmbDevice;->convertToSambaFolder(Landroid/net/samba/SmbShareFolder;Ljcifs/smb/SmbFile;)V

    iget-object v12, p0, Landroid/net/samba/SmbDevice;->folderList:Ljava/util/ArrayList;

    invoke-virtual {v12, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "get type :"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    aget-object v14, v0, v6

    invoke-virtual {v14}, Ljcifs/smb/SmbFile;->getType()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    aget-object v12, v0, v6

    invoke-virtual {v12}, Ljcifs/smb/SmbFile;->getType()I
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljcifs/smb/SmbException; {:try_start_1 .. :try_end_1} :catch_2

    move-result v12

    const/4 v13, 0x2

    if-ne v12, v13, :cond_8

    :try_start_2
    aget-object v12, v0, v6

    invoke-virtual {v12}, Ljcifs/smb/SmbFile;->listFiles()[Ljcifs/smb/SmbFile;

    move-result-object v2

    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "list2 number: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    array-length v14, v2

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v9, 0x0

    :goto_4
    array-length v12, v2

    if-ge v9, v12, :cond_8

    aget-object v12, v2, v9

    invoke-virtual {p0, v12}, Landroid/net/samba/SmbDevice;->isHost(Ljcifs/smb/SmbFile;)Z

    move-result v12

    if-eqz v12, :cond_4

    new-instance v10, Landroid/net/samba/SmbShareFolder;

    invoke-direct {v10, p0}, Landroid/net/samba/SmbShareFolder;-><init>(Landroid/net/samba/SmbDevice;)V

    aget-object v12, v2, v9

    invoke-static {v10, v12}, Landroid/net/samba/SmbDevice;->convertToSambaFolder(Landroid/net/samba/SmbShareFolder;Ljcifs/smb/SmbFile;)V

    iget-object v12, p0, Landroid/net/samba/SmbDevice;->folderList:Ljava/util/ArrayList;

    invoke-virtual {v12, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljcifs/smb/SmbException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/net/MalformedURLException; {:try_start_2 .. :try_end_2} :catch_0

    :cond_4
    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    :cond_5
    :try_start_3
    new-instance v12, Ljcifs/smb/SmbFile;

    iget-object v13, p0, Landroid/net/samba/SmbDevice;->SmbURL:Ljava/lang/String;

    invoke-direct {v12, v13, v1}, Ljcifs/smb/SmbFile;-><init>(Ljava/lang/String;Ljcifs/smb/NtlmPasswordAuthentication;)V

    iput-object v12, p0, Landroid/net/samba/SmbDevice;->root:Ljcifs/smb/SmbFile;
    :try_end_3
    .catch Ljava/net/MalformedURLException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljcifs/smb/SmbException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_2

    :catch_0
    move-exception v4

    invoke-virtual {v4}, Ljava/net/MalformedURLException;->printStackTrace()V

    :cond_6
    :goto_5
    iget-object v12, p0, Landroid/net/samba/SmbDevice;->folderList:Ljava/util/ArrayList;

    goto/16 :goto_1

    :catch_1
    move-exception v4

    :try_start_4
    invoke-virtual {v4}, Ljcifs/smb/SmbException;->getNtStatus()I

    move-result v7

    iget-object v12, p0, Landroid/net/samba/SmbDevice;->onRecvMsg:Landroid/net/samba/OnRecvMsg;

    if-eqz v12, :cond_7

    iget-object v12, p0, Landroid/net/samba/SmbDevice;->onRecvMsg:Landroid/net/samba/OnRecvMsg;

    invoke-interface {v12, v7}, Landroid/net/samba/OnRecvMsg;->onRecvMsg(I)V

    :cond_7
    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "workgroup :"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_8
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_3

    :cond_9
    iget-object v12, p0, Landroid/net/samba/SmbDevice;->folderList:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v12

    if-nez v12, :cond_b

    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "folderList : "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    iget-object v14, p0, Landroid/net/samba/SmbDevice;->folderList:Ljava/util/ArrayList;

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v12, p0, Landroid/net/samba/SmbDevice;->onRecvMsg:Landroid/net/samba/OnRecvMsg;

    if-eqz v12, :cond_6

    iget-object v12, p0, Landroid/net/samba/SmbDevice;->onRecvMsg:Landroid/net/samba/OnRecvMsg;

    const/4 v13, 0x0

    invoke-interface {v12, v13}, Landroid/net/samba/OnRecvMsg;->onRecvMsg(I)V
    :try_end_4
    .catch Ljava/net/MalformedURLException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljcifs/smb/SmbException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_5

    :catch_2
    move-exception v4

    invoke-virtual {v4}, Ljcifs/smb/SmbException;->getNtStatus()I

    move-result v7

    sget-object v12, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v12, v7}, Ljava/io/PrintStream;->println(I)V

    iget-object v12, p0, Landroid/net/samba/SmbDevice;->onRecvMsg:Landroid/net/samba/OnRecvMsg;

    if-eqz v12, :cond_a

    iget-object v12, p0, Landroid/net/samba/SmbDevice;->onRecvMsg:Landroid/net/samba/OnRecvMsg;

    invoke-interface {v12, v7}, Landroid/net/samba/OnRecvMsg;->onRecvMsg(I)V

    :cond_a
    invoke-virtual {v4}, Ljcifs/smb/SmbException;->printStackTrace()V

    goto :goto_5

    :cond_b
    :try_start_5
    iget-object v12, p0, Landroid/net/samba/SmbDevice;->onRecvMsg:Landroid/net/samba/OnRecvMsg;

    if-eqz v12, :cond_6

    iget-object v12, p0, Landroid/net/samba/SmbDevice;->onRecvMsg:Landroid/net/samba/OnRecvMsg;

    const v13, -0x3ffffddb    # -2.000131f

    invoke-interface {v12, v13}, Landroid/net/samba/OnRecvMsg;->onRecvMsg(I)V
    :try_end_5
    .catch Ljava/net/MalformedURLException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljcifs/smb/SmbException; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_5
.end method

.method public hasPassword()Z
    .locals 12

    const/4 v8, 0x1

    :try_start_0
    new-instance v4, Ljcifs/smb/SmbFile;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "smb://"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Landroid/net/samba/SmbDevice;->ip:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljcifs/smb/NtlmPasswordAuthentication;

    const/4 v11, 0x0

    invoke-direct {v10, v11}, Ljcifs/smb/NtlmPasswordAuthentication;-><init>(Ljava/lang/String;)V

    invoke-direct {v4, v9, v10}, Ljcifs/smb/SmbFile;-><init>(Ljava/lang/String;Ljcifs/smb/NtlmPasswordAuthentication;)V

    invoke-virtual {v4}, Ljcifs/smb/SmbFile;->listFiles()[Ljcifs/smb/SmbFile;

    move-result-object v7

    invoke-virtual {v4}, Ljcifs/smb/SmbFile;->exists()Z

    move-result v9

    if-eqz v9, :cond_1

    const-string v9, "SmbDevice"

    const-string v10, "hasPassword : smb server has no password!"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v7

    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_2

    aget-object v6, v0, v2

    invoke-virtual {v6}, Ljcifs/smb/SmbFile;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "$/"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const-string v9, "SmbDevice"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "hasPassword : Test shared folder: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v6}, Ljcifs/smb/SmbFile;->getPath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v5, Ljcifs/smb/SmbFile;

    invoke-virtual {v6}, Ljcifs/smb/SmbFile;->getPath()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljcifs/smb/NtlmPasswordAuthentication;

    const/4 v11, 0x0

    invoke-direct {v10, v11}, Ljcifs/smb/NtlmPasswordAuthentication;-><init>(Ljava/lang/String;)V

    invoke-direct {v5, v9, v10}, Ljcifs/smb/SmbFile;-><init>(Ljava/lang/String;Ljcifs/smb/NtlmPasswordAuthentication;)V

    invoke-virtual {v5}, Ljcifs/smb/SmbFile;->list()[Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    const-string v9, "SmbDevice"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, p0, Landroid/net/samba/SmbDevice;->mName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ": hasPassword : yes !"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_2
    return v8

    :cond_2
    const/4 v8, 0x0

    goto :goto_2
.end method

.method public isActive()Z
    .locals 3

    :try_start_0
    iget-object v2, p0, Landroid/net/samba/SmbDevice;->ip:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/net/samba/SmbDevice;->ip:Ljava/lang/String;

    invoke-static {v2}, Ljcifs/netbios/NbtAddress;->getByName(Ljava/lang/String;)Ljcifs/netbios/NbtAddress;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljcifs/netbios/NbtAddress;->isActive()Z
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/net/UnknownHostException;->printStackTrace()V

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isHost(Ljcifs/smb/SmbFile;)Z
    .locals 3
    .param p1    # Ljcifs/smb/SmbFile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljcifs/smb/SmbException;
        }
    .end annotation

    const/4 v1, 0x1

    invoke-virtual {p1}, Ljcifs/smb/SmbFile;->getType()I

    move-result v0

    invoke-virtual {p1}, Ljcifs/smb/SmbFile;->isHidden()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1

    :cond_2
    if-eq v0, v1, :cond_1

    const/16 v2, 0x8

    if-eq v0, v2, :cond_1

    const/4 v2, 0x4

    if-ne v0, v2, :cond_0

    goto :goto_0
.end method

.method public isMounted()Z
    .locals 1

    iget-boolean v0, p0, Landroid/net/samba/SmbDevice;->mMountedFlag:Z

    return v0
.end method

.method public localPath()Ljava/lang/String;
    .locals 2

    iget-boolean v0, p0, Landroid/net/samba/SmbDevice;->mountPointByIp:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/mnt/samba/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/net/samba/SmbDevice;->ip:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "/mnt/samba/"

    goto :goto_0
.end method

.method public mount(Landroid/net/samba/SmbAuthentication;I)I
    .locals 13
    .param p1    # Landroid/net/samba/SmbAuthentication;
    .param p2    # I

    const/4 v6, 0x1

    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    const/4 v6, 0x0

    :cond_0
    iget-object v0, p0, Landroid/net/samba/SmbDevice;->folderList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/net/samba/SmbShareFolder;

    invoke-virtual {v8}, Landroid/net/samba/SmbShareFolder;->getFileName()Ljava/lang/String;

    move-result-object v2

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " mount "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Landroid/net/samba/SmbDevice;->ip:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-direct {p0, v2}, Landroid/net/samba/SmbDevice;->isChinese(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v7

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " datas.length "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v3, v7

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v9, 0x0

    :goto_1
    array-length v0, v7

    if-ge v9, v0, :cond_1

    aget-char v0, v7, v9

    if-nez v0, :cond_2

    const/4 v0, 0x0

    invoke-static {v7, v0, v9}, Ljava/lang/String;->copyValueOf([CII)Ljava/lang/String;

    move-result-object v2

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " folderName "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_1
    const/4 v12, 0x1

    iget-object v0, p0, Landroid/net/samba/SmbDevice;->auth:Landroid/net/samba/SmbAuthentication;

    if-nez v0, :cond_3

    iget-object v0, p0, Landroid/net/samba/SmbDevice;->stm:Lcom/mstar/android/storage/MStorageManager;

    iget-object v1, p0, Landroid/net/samba/SmbDevice;->ip:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Landroid/net/samba/SmbDevice;->ip:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "/"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, " "

    const-string v5, ""

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/storage/MStorageManager;->mountSamba(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v12

    :goto_2
    if-nez v12, :cond_5

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, " mount jcifs fail! "

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    :cond_3
    iget-object v0, p0, Landroid/net/samba/SmbDevice;->auth:Landroid/net/samba/SmbAuthentication;

    invoke-virtual {v0}, Landroid/net/samba/SmbAuthentication;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v0, "\\"

    invoke-virtual {v4, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v11

    if-lez v11, :cond_4

    iget-object v0, p0, Landroid/net/samba/SmbDevice;->auth:Landroid/net/samba/SmbAuthentication;

    invoke-virtual {v0}, Landroid/net/samba/SmbAuthentication;->getName()Ljava/lang/String;

    move-result-object v0

    add-int/lit8 v1, v11, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    :cond_4
    iget-object v0, p0, Landroid/net/samba/SmbDevice;->stm:Lcom/mstar/android/storage/MStorageManager;

    iget-object v1, p0, Landroid/net/samba/SmbDevice;->ip:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Landroid/net/samba/SmbDevice;->ip:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "/"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/net/samba/SmbAuthentication;->getPassword()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/storage/MStorageManager;->mountSamba(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v12

    goto :goto_2

    :cond_5
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/net/samba/SmbDevice;->mMountedFlag:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/net/samba/SmbDevice;->mountPointByIp:Z

    goto/16 :goto_0

    :cond_6
    iget-boolean v0, p0, Landroid/net/samba/SmbDevice;->mMountedFlag:Z

    if-eqz v0, :cond_7

    const/16 v0, 0x10

    :goto_3
    return v0

    :cond_7
    const/16 v0, 0x11

    goto :goto_3
.end method

.method public mount(Landroid/net/samba/SmbAuthentication;)V
    .locals 13
    .param p1    # Landroid/net/samba/SmbAuthentication;

    const/4 v12, 0x0

    iget-object v0, p0, Landroid/net/samba/SmbDevice;->folderList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/net/samba/SmbShareFolder;

    invoke-virtual {v7}, Landroid/net/samba/SmbShareFolder;->getFileName()Ljava/lang/String;

    move-result-object v2

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " mount "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Landroid/net/samba/SmbDevice;->ip:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-direct {p0, v2}, Landroid/net/samba/SmbDevice;->isChinese(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v6

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " datas.length "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v3, v6

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v8, 0x0

    :goto_1
    array-length v0, v6

    if-ge v8, v0, :cond_0

    aget-char v0, v6, v8

    if-nez v0, :cond_1

    invoke-static {v6, v12, v8}, Ljava/lang/String;->copyValueOf([CII)Ljava/lang/String;

    move-result-object v2

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " folderName "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    const/4 v11, 0x1

    iget-object v0, p0, Landroid/net/samba/SmbDevice;->auth:Landroid/net/samba/SmbAuthentication;

    if-nez v0, :cond_2

    iget-object v0, p0, Landroid/net/samba/SmbDevice;->stm:Lcom/mstar/android/storage/MStorageManager;

    iget-object v1, p0, Landroid/net/samba/SmbDevice;->ip:Ljava/lang/String;

    const-string v4, " "

    const-string v5, ""

    move-object v3, v2

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/storage/MStorageManager;->mountSamba(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v11

    :goto_2
    if-nez v11, :cond_4

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, " mount jcifs fail! "

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Landroid/net/samba/SmbDevice;->auth:Landroid/net/samba/SmbAuthentication;

    invoke-virtual {v0}, Landroid/net/samba/SmbAuthentication;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v0, "\\"

    invoke-virtual {v4, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v10

    if-lez v10, :cond_3

    iget-object v0, p0, Landroid/net/samba/SmbDevice;->auth:Landroid/net/samba/SmbAuthentication;

    invoke-virtual {v0}, Landroid/net/samba/SmbAuthentication;->getName()Ljava/lang/String;

    move-result-object v0

    add-int/lit8 v1, v10, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    :cond_3
    iget-object v0, p0, Landroid/net/samba/SmbDevice;->stm:Lcom/mstar/android/storage/MStorageManager;

    iget-object v1, p0, Landroid/net/samba/SmbDevice;->ip:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/samba/SmbAuthentication;->getPassword()Ljava/lang/String;

    move-result-object v5

    move-object v3, v2

    invoke-virtual/range {v0 .. v5}, Lcom/mstar/android/storage/MStorageManager;->mountSamba(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v11

    goto :goto_2

    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/net/samba/SmbDevice;->mMountedFlag:Z

    iput-boolean v12, p0, Landroid/net/samba/SmbDevice;->mountPointByIp:Z

    goto/16 :goto_0

    :cond_5
    return-void
.end method

.method public remotePath()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "//"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/net/samba/SmbDevice;->ip:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setAddress(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Landroid/net/samba/SmbDevice;->ip:Ljava/lang/String;

    return-void
.end method

.method public setAuth(Landroid/net/samba/SmbAuthentication;)V
    .locals 1
    .param p1    # Landroid/net/samba/SmbAuthentication;

    iput-object p1, p0, Landroid/net/samba/SmbDevice;->auth:Landroid/net/samba/SmbAuthentication;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/net/samba/SmbAuthentication;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/net/samba/SmbDevice;->mUser:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/samba/SmbAuthentication;->getPassword()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/net/samba/SmbDevice;->mPassWord:Ljava/lang/String;

    iget-object v0, p0, Landroid/net/samba/SmbDevice;->mPassWord:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/net/samba/SmbDevice;->mHasPassWord:Z

    :cond_0
    return-void
.end method

.method public setHostName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Landroid/net/samba/SmbDevice;->mName:Ljava/lang/String;

    return-void
.end method

.method public setOnRecvMsg(Landroid/net/samba/OnRecvMsg;)V
    .locals 0
    .param p1    # Landroid/net/samba/OnRecvMsg;

    iput-object p1, p0, Landroid/net/samba/SmbDevice;->onRecvMsg:Landroid/net/samba/OnRecvMsg;

    return-void
.end method

.method public setStorageManager(Lcom/mstar/android/storage/MStorageManager;)V
    .locals 0
    .param p1    # Lcom/mstar/android/storage/MStorageManager;

    iput-object p1, p0, Landroid/net/samba/SmbDevice;->stm:Lcom/mstar/android/storage/MStorageManager;

    return-void
.end method

.method public testPassword(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 13
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const/4 v8, 0x0

    :try_start_0
    iget-object v9, p0, Landroid/net/samba/SmbDevice;->ip:Ljava/lang/String;

    if-nez v9, :cond_1

    iget-boolean v9, p0, Landroid/net/samba/SmbDevice;->DEBUG:Z

    if-eqz v9, :cond_0

    const-string v9, "SmbDevice"

    const-string v10, "testPassword --> Ip is null!"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return v8

    :cond_1
    new-instance v4, Ljcifs/smb/SmbFile;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "smb://"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Landroid/net/samba/SmbDevice;->ip:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljcifs/smb/NtlmPasswordAuthentication;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ":"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljcifs/smb/NtlmPasswordAuthentication;-><init>(Ljava/lang/String;)V

    invoke-direct {v4, v9, v10}, Ljcifs/smb/SmbFile;-><init>(Ljava/lang/String;Ljcifs/smb/NtlmPasswordAuthentication;)V

    invoke-virtual {v4}, Ljcifs/smb/SmbFile;->listFiles()[Ljcifs/smb/SmbFile;

    move-result-object v7

    invoke-virtual {v4}, Ljcifs/smb/SmbFile;->exists()Z

    move-result v9

    if-eqz v9, :cond_0

    move-object v0, v7

    array-length v3, v0

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_3

    aget-object v6, v0, v2

    invoke-virtual {v6}, Ljcifs/smb/SmbFile;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "$/"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    const-string v9, "SmbDevice"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "hasPassword : Test shared folder: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v6}, Ljcifs/smb/SmbFile;->getPath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v5, Ljcifs/smb/SmbFile;

    invoke-virtual {v6}, Ljcifs/smb/SmbFile;->getPath()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljcifs/smb/NtlmPasswordAuthentication;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ":"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljcifs/smb/NtlmPasswordAuthentication;-><init>(Ljava/lang/String;)V

    invoke-direct {v5, v9, v10}, Ljcifs/smb/SmbFile;-><init>(Ljava/lang/String;Ljcifs/smb/NtlmPasswordAuthentication;)V

    invoke-virtual {v5}, Ljcifs/smb/SmbFile;->list()[Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v1

    const-string v9, "SmbDevice"

    const-string v10, "testPassword : password is wrong!"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    :cond_3
    :try_start_1
    const-string v9, "SmbDevice"

    const-string v10, "testPassword : password is correct!"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    const/4 v8, 0x1

    goto/16 :goto_0
.end method

.method public unmount()I
    .locals 11

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v5, 0x0

    iget-object v6, p0, Landroid/net/samba/SmbDevice;->folderList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/samba/SmbShareFolder;

    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " umount "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Landroid/net/samba/SmbShareFolder;->getFileName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/net/samba/SmbShareFolder;->getFileName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Landroid/net/samba/SmbDevice;->isChinese(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " datas.length "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    array-length v8, v0

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v3, 0x0

    :goto_1
    array-length v6, v0

    if-ge v3, v6, :cond_0

    aget-char v6, v0, v3

    if-nez v6, :cond_1

    invoke-static {v0, v9, v3}, Ljava/lang/String;->copyValueOf([CII)Ljava/lang/String;

    move-result-object v2

    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " folderName "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    const/4 v5, 0x1

    iget-boolean v6, p0, Landroid/net/samba/SmbDevice;->mountPointByIp:Z

    if-eqz v6, :cond_2

    iget-object v6, p0, Landroid/net/samba/SmbDevice;->stm:Lcom/mstar/android/storage/MStorageManager;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Landroid/net/samba/SmbDevice;->ip:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v10}, Lcom/mstar/android/storage/MStorageManager;->unmountSamba(Ljava/lang/String;Z)Z

    move-result v5

    :goto_2
    if-nez v5, :cond_3

    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v7, " umount jcifs fail! "

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    iget-object v6, p0, Landroid/net/samba/SmbDevice;->stm:Lcom/mstar/android/storage/MStorageManager;

    invoke-virtual {v6, v2, v10}, Lcom/mstar/android/storage/MStorageManager;->unmountSamba(Ljava/lang/String;Z)Z

    move-result v5

    goto :goto_2

    :cond_3
    iput-boolean v9, p0, Landroid/net/samba/SmbDevice;->mMountedFlag:Z

    goto/16 :goto_0

    :cond_4
    if-eqz v5, :cond_5

    const/16 v6, 0x12

    :goto_3
    return v6

    :cond_5
    const/16 v6, 0x13

    goto :goto_3
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Landroid/net/samba/SmbDevice;->ip:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/net/samba/SmbDevice;->mUser:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/net/samba/SmbDevice;->mPassWord:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Landroid/net/samba/SmbDevice;->mName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
