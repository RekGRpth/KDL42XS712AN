.class public Ljcifs/netbios/Lmhosts;
.super Ljava/lang/Object;
.source "Lmhosts.java"


# static fields
.field private static final FILENAME:Ljava/lang/String;

.field private static final TAB:Ljava/util/Hashtable;

.field private static alt:I

.field private static lastModified:J

.field private static log:Ljcifs/util/LogStream;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string v0, "jcifs.netbios.lmhosts"

    invoke-static {v0}, Ljcifs/Config;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ljcifs/netbios/Lmhosts;->FILENAME:Ljava/lang/String;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    sput-object v0, Ljcifs/netbios/Lmhosts;->TAB:Ljava/util/Hashtable;

    const-wide/16 v0, 0x1

    sput-wide v0, Ljcifs/netbios/Lmhosts;->lastModified:J

    invoke-static {}, Ljcifs/util/LogStream;->getInstance()Ljcifs/util/LogStream;

    move-result-object v0

    sput-object v0, Ljcifs/netbios/Lmhosts;->log:Ljcifs/util/LogStream;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized getByName(Ljava/lang/String;)Ljcifs/netbios/NbtAddress;
    .locals 4
    .param p0    # Ljava/lang/String;

    const-class v1, Ljcifs/netbios/Lmhosts;

    monitor-enter v1

    :try_start_0
    new-instance v0, Ljcifs/netbios/Name;

    const/16 v2, 0x20

    const/4 v3, 0x0

    invoke-direct {v0, p0, v2, v3}, Ljcifs/netbios/Name;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    invoke-static {v0}, Ljcifs/netbios/Lmhosts;->getByName(Ljcifs/netbios/Name;)Ljcifs/netbios/NbtAddress;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static declared-synchronized getByName(Ljcifs/netbios/Name;)Ljcifs/netbios/NbtAddress;
    .locals 11
    .param p0    # Ljcifs/netbios/Name;

    const-class v8, Ljcifs/netbios/Lmhosts;

    monitor-enter v8

    const/4 v6, 0x0

    :try_start_0
    sget-object v7, Ljcifs/netbios/Lmhosts;->FILENAME:Ljava/lang/String;

    if-eqz v7, :cond_1

    new-instance v1, Ljava/io/File;

    sget-object v7, Ljcifs/netbios/Lmhosts;->FILENAME:Ljava/lang/String;

    invoke-direct {v1, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    sget-wide v9, Ljcifs/netbios/Lmhosts;->lastModified:J

    cmp-long v7, v4, v9

    if-lez v7, :cond_0

    sput-wide v4, Ljcifs/netbios/Lmhosts;->lastModified:J

    sget-object v7, Ljcifs/netbios/Lmhosts;->TAB:Ljava/util/Hashtable;

    invoke-virtual {v7}, Ljava/util/Hashtable;->clear()V

    const/4 v7, 0x0

    sput v7, Ljcifs/netbios/Lmhosts;->alt:I

    new-instance v7, Ljava/io/FileReader;

    invoke-direct {v7, v1}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    invoke-static {v7}, Ljcifs/netbios/Lmhosts;->populate(Ljava/io/Reader;)V

    :cond_0
    sget-object v7, Ljcifs/netbios/Lmhosts;->TAB:Ljava/util/Hashtable;

    invoke-virtual {v7, p0}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Ljcifs/netbios/NbtAddress;

    move-object v6, v0
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_0
    monitor-exit v8

    return-object v6

    :catch_0
    move-exception v2

    :try_start_1
    sget-object v7, Ljcifs/netbios/Lmhosts;->log:Ljcifs/util/LogStream;

    sget v7, Ljcifs/util/LogStream;->level:I

    const/4 v9, 0x1

    if-le v7, v9, :cond_1

    sget-object v7, Ljcifs/netbios/Lmhosts;->log:Ljcifs/util/LogStream;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "lmhosts file: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Ljcifs/netbios/Lmhosts;->FILENAME:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljcifs/util/LogStream;->println(Ljava/lang/String;)V

    sget-object v7, Ljcifs/netbios/Lmhosts;->log:Ljcifs/util/LogStream;

    invoke-virtual {v2, v7}, Ljava/io/FileNotFoundException;->printStackTrace(Ljava/io/PrintStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v7

    monitor-exit v8

    throw v7

    :catch_1
    move-exception v3

    :try_start_2
    sget-object v7, Ljcifs/netbios/Lmhosts;->log:Ljcifs/util/LogStream;

    sget v7, Ljcifs/util/LogStream;->level:I

    if-lez v7, :cond_1

    sget-object v7, Ljcifs/netbios/Lmhosts;->log:Ljcifs/util/LogStream;

    invoke-virtual {v3, v7}, Ljava/io/IOException;->printStackTrace(Ljava/io/PrintStream;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method static populate(Ljava/io/Reader;)V
    .locals 22
    .param p0    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v14, Ljava/io/BufferedReader;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    :cond_0
    :goto_0
    invoke-virtual {v14}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v20

    if-eqz v20, :cond_a

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_0

    const/4 v6, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    const/16 v7, 0x23

    if-ne v6, v7, :cond_5

    const-string v6, "#INCLUDE "

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    const/16 v6, 0x5c

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v20

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "smb:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const/16 v7, 0x5c

    const/16 v8, 0x2f

    move-object/from16 v0, v20

    invoke-virtual {v0, v7, v8}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    sget v6, Ljcifs/netbios/Lmhosts;->alt:I

    if-lez v6, :cond_2

    :try_start_0
    new-instance v6, Ljava/io/InputStreamReader;

    new-instance v7, Ljcifs/smb/SmbFileInputStream;

    move-object/from16 v0, v21

    invoke-direct {v7, v0}, Ljcifs/smb/SmbFileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v6, v7}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-static {v6}, Ljcifs/netbios/Lmhosts;->populate(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    sget v6, Ljcifs/netbios/Lmhosts;->alt:I

    add-int/lit8 v6, v6, -0x1

    sput v6, Ljcifs/netbios/Lmhosts;->alt:I

    :cond_1
    invoke-virtual {v14}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v20

    if-eqz v20, :cond_0

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v20

    const-string v6, "#END_ALTERNATE"

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    goto/16 :goto_0

    :catch_0
    move-exception v18

    sget-object v6, Ljcifs/netbios/Lmhosts;->log:Ljcifs/util/LogStream;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "lmhosts URL: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljcifs/util/LogStream;->println(Ljava/lang/String;)V

    sget-object v6, Ljcifs/netbios/Lmhosts;->log:Ljcifs/util/LogStream;

    move-object/from16 v0, v18

    invoke-virtual {v0, v6}, Ljava/io/IOException;->printStackTrace(Ljava/io/PrintStream;)V

    goto/16 :goto_0

    :cond_2
    new-instance v6, Ljava/io/InputStreamReader;

    new-instance v7, Ljcifs/smb/SmbFileInputStream;

    move-object/from16 v0, v21

    invoke-direct {v7, v0}, Ljcifs/smb/SmbFileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v6, v7}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-static {v6}, Ljcifs/netbios/Lmhosts;->populate(Ljava/io/Reader;)V

    goto/16 :goto_0

    :cond_3
    const-string v6, "#BEGIN_ALTERNATE"

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    sget v6, Ljcifs/netbios/Lmhosts;->alt:I

    add-int/lit8 v6, v6, 0x1

    sput v6, Ljcifs/netbios/Lmhosts;->alt:I

    goto/16 :goto_0

    :cond_4
    const-string v6, "#END_ALTERNATE"

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    sget v6, Ljcifs/netbios/Lmhosts;->alt:I

    if-lez v6, :cond_0

    sget v6, Ljcifs/netbios/Lmhosts;->alt:I

    add-int/lit8 v6, v6, -0x1

    sput v6, Ljcifs/netbios/Lmhosts;->alt:I

    new-instance v6, Ljava/io/IOException;

    const-string v7, "no lmhosts alternate includes loaded"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_5
    const/4 v6, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6}, Ljava/lang/Character;->isDigit(C)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->toCharArray()[C

    move-result-object v16

    const/16 v15, 0x2e

    const/16 v17, 0x0

    move/from16 v5, v17

    :goto_1
    move-object/from16 v0, v16

    array-length v6, v0

    move/from16 v0, v17

    if-ge v0, v6, :cond_7

    const/16 v6, 0x2e

    if-ne v15, v6, :cond_7

    const/4 v13, 0x0

    :goto_2
    move-object/from16 v0, v16

    array-length v6, v0

    move/from16 v0, v17

    if-ge v0, v6, :cond_6

    aget-char v15, v16, v17

    const/16 v6, 0x30

    if-lt v15, v6, :cond_6

    const/16 v6, 0x39

    if-gt v15, v6, :cond_6

    mul-int/lit8 v6, v13, 0xa

    add-int/2addr v6, v15

    add-int/lit8 v13, v6, -0x30

    add-int/lit8 v17, v17, 0x1

    goto :goto_2

    :cond_6
    shl-int/lit8 v6, v5, 0x8

    add-int v5, v6, v13

    add-int/lit8 v17, v17, 0x1

    goto :goto_1

    :cond_7
    :goto_3
    move-object/from16 v0, v16

    array-length v6, v0

    move/from16 v0, v17

    if-ge v0, v6, :cond_8

    aget-char v6, v16, v17

    invoke-static {v6}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v6

    if-eqz v6, :cond_8

    add-int/lit8 v17, v17, 0x1

    goto :goto_3

    :cond_8
    move/from16 v19, v17

    :goto_4
    move-object/from16 v0, v16

    array-length v6, v0

    move/from16 v0, v19

    if-ge v0, v6, :cond_9

    aget-char v6, v16, v19

    invoke-static {v6}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v6

    if-nez v6, :cond_9

    add-int/lit8 v19, v19, 0x1

    goto :goto_4

    :cond_9
    new-instance v4, Ljcifs/netbios/Name;

    move-object/from16 v0, v20

    move/from16 v1, v17

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x20

    const/4 v8, 0x0

    invoke-direct {v4, v6, v7, v8}, Ljcifs/netbios/Name;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    new-instance v3, Ljcifs/netbios/NbtAddress;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x1

    const/4 v11, 0x1

    sget-object v12, Ljcifs/netbios/NbtAddress;->UNKNOWN_MAC_ADDRESS:[B

    invoke-direct/range {v3 .. v12}, Ljcifs/netbios/NbtAddress;-><init>(Ljcifs/netbios/Name;IZIZZZZ[B)V

    sget-object v6, Ljcifs/netbios/Lmhosts;->TAB:Ljava/util/Hashtable;

    invoke-virtual {v6, v4, v3}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_a
    return-void
.end method
