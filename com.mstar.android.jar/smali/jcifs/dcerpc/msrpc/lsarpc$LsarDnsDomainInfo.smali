.class public Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;
.super Ljcifs/dcerpc/ndr/NdrObject;
.source "lsarpc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ljcifs/dcerpc/msrpc/lsarpc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LsarDnsDomainInfo"
.end annotation


# instance fields
.field public dns_domain:Ljcifs/dcerpc/rpc$unicode_string;

.field public dns_forest:Ljcifs/dcerpc/rpc$unicode_string;

.field public domain_guid:Ljcifs/dcerpc/rpc$uuid_t;

.field public name:Ljcifs/dcerpc/rpc$unicode_string;

.field public sid:Ljcifs/dcerpc/rpc$sid_t;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljcifs/dcerpc/ndr/NdrObject;-><init>()V

    return-void
.end method


# virtual methods
.method public decode(Ljcifs/dcerpc/ndr/NdrBuffer;)V
    .locals 20
    .param p1    # Ljcifs/dcerpc/ndr/NdrBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljcifs/dcerpc/ndr/NdrException;
        }
    .end annotation

    const/16 v18, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljcifs/dcerpc/ndr/NdrBuffer;->align(I)I

    const/16 v18, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljcifs/dcerpc/ndr/NdrBuffer;->align(I)I

    move-object/from16 v0, p0

    iget-object v0, v0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->name:Ljcifs/dcerpc/rpc$unicode_string;

    move-object/from16 v18, v0

    if-nez v18, :cond_0

    new-instance v18, Ljcifs/dcerpc/rpc$unicode_string;

    invoke-direct/range {v18 .. v18}, Ljcifs/dcerpc/rpc$unicode_string;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->name:Ljcifs/dcerpc/rpc$unicode_string;

    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->name:Ljcifs/dcerpc/rpc$unicode_string;

    move-object/from16 v18, v0

    invoke-virtual/range {p1 .. p1}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_short()I

    move-result v19

    move/from16 v0, v19

    int-to-short v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput-short v0, v1, Ljcifs/dcerpc/rpc$unicode_string;->length:S

    move-object/from16 v0, p0

    iget-object v0, v0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->name:Ljcifs/dcerpc/rpc$unicode_string;

    move-object/from16 v18, v0

    invoke-virtual/range {p1 .. p1}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_short()I

    move-result v19

    move/from16 v0, v19

    int-to-short v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput-short v0, v1, Ljcifs/dcerpc/rpc$unicode_string;->maximum_length:S

    invoke-virtual/range {p1 .. p1}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_long()I

    move-result v15

    const/16 v18, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljcifs/dcerpc/ndr/NdrBuffer;->align(I)I

    move-object/from16 v0, p0

    iget-object v0, v0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->dns_domain:Ljcifs/dcerpc/rpc$unicode_string;

    move-object/from16 v18, v0

    if-nez v18, :cond_1

    new-instance v18, Ljcifs/dcerpc/rpc$unicode_string;

    invoke-direct/range {v18 .. v18}, Ljcifs/dcerpc/rpc$unicode_string;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->dns_domain:Ljcifs/dcerpc/rpc$unicode_string;

    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->dns_domain:Ljcifs/dcerpc/rpc$unicode_string;

    move-object/from16 v18, v0

    invoke-virtual/range {p1 .. p1}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_short()I

    move-result v19

    move/from16 v0, v19

    int-to-short v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput-short v0, v1, Ljcifs/dcerpc/rpc$unicode_string;->length:S

    move-object/from16 v0, p0

    iget-object v0, v0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->dns_domain:Ljcifs/dcerpc/rpc$unicode_string;

    move-object/from16 v18, v0

    invoke-virtual/range {p1 .. p1}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_short()I

    move-result v19

    move/from16 v0, v19

    int-to-short v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput-short v0, v1, Ljcifs/dcerpc/rpc$unicode_string;->maximum_length:S

    invoke-virtual/range {p1 .. p1}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_long()I

    move-result v4

    const/16 v18, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljcifs/dcerpc/ndr/NdrBuffer;->align(I)I

    move-object/from16 v0, p0

    iget-object v0, v0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->dns_forest:Ljcifs/dcerpc/rpc$unicode_string;

    move-object/from16 v18, v0

    if-nez v18, :cond_2

    new-instance v18, Ljcifs/dcerpc/rpc$unicode_string;

    invoke-direct/range {v18 .. v18}, Ljcifs/dcerpc/rpc$unicode_string;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->dns_forest:Ljcifs/dcerpc/rpc$unicode_string;

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->dns_forest:Ljcifs/dcerpc/rpc$unicode_string;

    move-object/from16 v18, v0

    invoke-virtual/range {p1 .. p1}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_short()I

    move-result v19

    move/from16 v0, v19

    int-to-short v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput-short v0, v1, Ljcifs/dcerpc/rpc$unicode_string;->length:S

    move-object/from16 v0, p0

    iget-object v0, v0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->dns_forest:Ljcifs/dcerpc/rpc$unicode_string;

    move-object/from16 v18, v0

    invoke-virtual/range {p1 .. p1}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_short()I

    move-result v19

    move/from16 v0, v19

    int-to-short v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput-short v0, v1, Ljcifs/dcerpc/rpc$unicode_string;->maximum_length:S

    invoke-virtual/range {p1 .. p1}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_long()I

    move-result v8

    const/16 v18, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljcifs/dcerpc/ndr/NdrBuffer;->align(I)I

    move-object/from16 v0, p0

    iget-object v0, v0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->domain_guid:Ljcifs/dcerpc/rpc$uuid_t;

    move-object/from16 v18, v0

    if-nez v18, :cond_3

    new-instance v18, Ljcifs/dcerpc/rpc$uuid_t;

    invoke-direct/range {v18 .. v18}, Ljcifs/dcerpc/rpc$uuid_t;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->domain_guid:Ljcifs/dcerpc/rpc$uuid_t;

    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->domain_guid:Ljcifs/dcerpc/rpc$uuid_t;

    move-object/from16 v18, v0

    invoke-virtual/range {p1 .. p1}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_long()I

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Ljcifs/dcerpc/rpc$uuid_t;->time_low:I

    move-object/from16 v0, p0

    iget-object v0, v0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->domain_guid:Ljcifs/dcerpc/rpc$uuid_t;

    move-object/from16 v18, v0

    invoke-virtual/range {p1 .. p1}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_short()I

    move-result v19

    move/from16 v0, v19

    int-to-short v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput-short v0, v1, Ljcifs/dcerpc/rpc$uuid_t;->time_mid:S

    move-object/from16 v0, p0

    iget-object v0, v0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->domain_guid:Ljcifs/dcerpc/rpc$uuid_t;

    move-object/from16 v18, v0

    invoke-virtual/range {p1 .. p1}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_short()I

    move-result v19

    move/from16 v0, v19

    int-to-short v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput-short v0, v1, Ljcifs/dcerpc/rpc$uuid_t;->time_hi_and_version:S

    move-object/from16 v0, p0

    iget-object v0, v0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->domain_guid:Ljcifs/dcerpc/rpc$uuid_t;

    move-object/from16 v18, v0

    invoke-virtual/range {p1 .. p1}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_small()I

    move-result v19

    move/from16 v0, v19

    int-to-byte v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput-byte v0, v1, Ljcifs/dcerpc/rpc$uuid_t;->clock_seq_hi_and_reserved:B

    move-object/from16 v0, p0

    iget-object v0, v0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->domain_guid:Ljcifs/dcerpc/rpc$uuid_t;

    move-object/from16 v18, v0

    invoke-virtual/range {p1 .. p1}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_small()I

    move-result v19

    move/from16 v0, v19

    int-to-byte v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput-byte v0, v1, Ljcifs/dcerpc/rpc$uuid_t;->clock_seq_low:B

    const/4 v11, 0x6

    move-object/from16 v0, p1

    iget v10, v0, Ljcifs/dcerpc/ndr/NdrBuffer;->index:I

    const/16 v18, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljcifs/dcerpc/ndr/NdrBuffer;->advance(I)V

    invoke-virtual/range {p1 .. p1}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_long()I

    move-result v17

    if-eqz v15, :cond_7

    move-object/from16 v0, p1

    iget-object v0, v0, Ljcifs/dcerpc/ndr/NdrBuffer;->deferred:Ljcifs/dcerpc/ndr/NdrBuffer;

    move-object/from16 p1, v0

    invoke-virtual/range {p1 .. p1}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_long()I

    move-result v16

    invoke-virtual/range {p1 .. p1}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_long()I

    invoke-virtual/range {p1 .. p1}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_long()I

    move-result v14

    move-object/from16 v0, p1

    iget v13, v0, Ljcifs/dcerpc/ndr/NdrBuffer;->index:I

    mul-int/lit8 v18, v14, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljcifs/dcerpc/ndr/NdrBuffer;->advance(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->name:Ljcifs/dcerpc/rpc$unicode_string;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Ljcifs/dcerpc/rpc$unicode_string;->buffer:[S

    move-object/from16 v18, v0

    if-nez v18, :cond_6

    if-ltz v16, :cond_4

    const v18, 0xffff

    move/from16 v0, v16

    move/from16 v1, v18

    if-le v0, v1, :cond_5

    :cond_4
    new-instance v18, Ljcifs/dcerpc/ndr/NdrException;

    const-string v19, "invalid array conformance"

    invoke-direct/range {v18 .. v19}, Ljcifs/dcerpc/ndr/NdrException;-><init>(Ljava/lang/String;)V

    throw v18

    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->name:Ljcifs/dcerpc/rpc$unicode_string;

    move-object/from16 v18, v0

    move/from16 v0, v16

    new-array v0, v0, [S

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Ljcifs/dcerpc/rpc$unicode_string;->buffer:[S

    :cond_6
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Ljcifs/dcerpc/ndr/NdrBuffer;->derive(I)Ljcifs/dcerpc/ndr/NdrBuffer;

    move-result-object p1

    const/4 v12, 0x0

    :goto_0
    if-ge v12, v14, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->name:Ljcifs/dcerpc/rpc$unicode_string;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Ljcifs/dcerpc/rpc$unicode_string;->buffer:[S

    move-object/from16 v18, v0

    invoke-virtual/range {p1 .. p1}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_short()I

    move-result v19

    move/from16 v0, v19

    int-to-short v0, v0

    move/from16 v19, v0

    aput-short v19, v18, v12

    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    :cond_7
    if-eqz v4, :cond_b

    move-object/from16 v0, p1

    iget-object v0, v0, Ljcifs/dcerpc/ndr/NdrBuffer;->deferred:Ljcifs/dcerpc/ndr/NdrBuffer;

    move-object/from16 p1, v0

    invoke-virtual/range {p1 .. p1}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_long()I

    move-result v5

    invoke-virtual/range {p1 .. p1}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_long()I

    invoke-virtual/range {p1 .. p1}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_long()I

    move-result v3

    move-object/from16 v0, p1

    iget v2, v0, Ljcifs/dcerpc/ndr/NdrBuffer;->index:I

    mul-int/lit8 v18, v3, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljcifs/dcerpc/ndr/NdrBuffer;->advance(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->dns_domain:Ljcifs/dcerpc/rpc$unicode_string;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Ljcifs/dcerpc/rpc$unicode_string;->buffer:[S

    move-object/from16 v18, v0

    if-nez v18, :cond_a

    if-ltz v5, :cond_8

    const v18, 0xffff

    move/from16 v0, v18

    if-le v5, v0, :cond_9

    :cond_8
    new-instance v18, Ljcifs/dcerpc/ndr/NdrException;

    const-string v19, "invalid array conformance"

    invoke-direct/range {v18 .. v19}, Ljcifs/dcerpc/ndr/NdrException;-><init>(Ljava/lang/String;)V

    throw v18

    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->dns_domain:Ljcifs/dcerpc/rpc$unicode_string;

    move-object/from16 v18, v0

    new-array v0, v5, [S

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Ljcifs/dcerpc/rpc$unicode_string;->buffer:[S

    :cond_a
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Ljcifs/dcerpc/ndr/NdrBuffer;->derive(I)Ljcifs/dcerpc/ndr/NdrBuffer;

    move-result-object p1

    const/4 v12, 0x0

    :goto_1
    if-ge v12, v3, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->dns_domain:Ljcifs/dcerpc/rpc$unicode_string;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Ljcifs/dcerpc/rpc$unicode_string;->buffer:[S

    move-object/from16 v18, v0

    invoke-virtual/range {p1 .. p1}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_short()I

    move-result v19

    move/from16 v0, v19

    int-to-short v0, v0

    move/from16 v19, v0

    aput-short v19, v18, v12

    add-int/lit8 v12, v12, 0x1

    goto :goto_1

    :cond_b
    if-eqz v8, :cond_f

    move-object/from16 v0, p1

    iget-object v0, v0, Ljcifs/dcerpc/ndr/NdrBuffer;->deferred:Ljcifs/dcerpc/ndr/NdrBuffer;

    move-object/from16 p1, v0

    invoke-virtual/range {p1 .. p1}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_long()I

    move-result v9

    invoke-virtual/range {p1 .. p1}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_long()I

    invoke-virtual/range {p1 .. p1}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_long()I

    move-result v7

    move-object/from16 v0, p1

    iget v6, v0, Ljcifs/dcerpc/ndr/NdrBuffer;->index:I

    mul-int/lit8 v18, v7, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljcifs/dcerpc/ndr/NdrBuffer;->advance(I)V

    move-object/from16 v0, p0

    iget-object v0, v0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->dns_forest:Ljcifs/dcerpc/rpc$unicode_string;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Ljcifs/dcerpc/rpc$unicode_string;->buffer:[S

    move-object/from16 v18, v0

    if-nez v18, :cond_e

    if-ltz v9, :cond_c

    const v18, 0xffff

    move/from16 v0, v18

    if-le v9, v0, :cond_d

    :cond_c
    new-instance v18, Ljcifs/dcerpc/ndr/NdrException;

    const-string v19, "invalid array conformance"

    invoke-direct/range {v18 .. v19}, Ljcifs/dcerpc/ndr/NdrException;-><init>(Ljava/lang/String;)V

    throw v18

    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->dns_forest:Ljcifs/dcerpc/rpc$unicode_string;

    move-object/from16 v18, v0

    new-array v0, v9, [S

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Ljcifs/dcerpc/rpc$unicode_string;->buffer:[S

    :cond_e
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Ljcifs/dcerpc/ndr/NdrBuffer;->derive(I)Ljcifs/dcerpc/ndr/NdrBuffer;

    move-result-object p1

    const/4 v12, 0x0

    :goto_2
    if-ge v12, v7, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->dns_forest:Ljcifs/dcerpc/rpc$unicode_string;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Ljcifs/dcerpc/rpc$unicode_string;->buffer:[S

    move-object/from16 v18, v0

    invoke-virtual/range {p1 .. p1}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_short()I

    move-result v19

    move/from16 v0, v19

    int-to-short v0, v0

    move/from16 v19, v0

    aput-short v19, v18, v12

    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->domain_guid:Ljcifs/dcerpc/rpc$uuid_t;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Ljcifs/dcerpc/rpc$uuid_t;->node:[B

    move-object/from16 v18, v0

    if-nez v18, :cond_12

    if-ltz v11, :cond_10

    const v18, 0xffff

    move/from16 v0, v18

    if-le v11, v0, :cond_11

    :cond_10
    new-instance v18, Ljcifs/dcerpc/ndr/NdrException;

    const-string v19, "invalid array conformance"

    invoke-direct/range {v18 .. v19}, Ljcifs/dcerpc/ndr/NdrException;-><init>(Ljava/lang/String;)V

    throw v18

    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->domain_guid:Ljcifs/dcerpc/rpc$uuid_t;

    move-object/from16 v18, v0

    new-array v0, v11, [B

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    iput-object v0, v1, Ljcifs/dcerpc/rpc$uuid_t;->node:[B

    :cond_12
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Ljcifs/dcerpc/ndr/NdrBuffer;->derive(I)Ljcifs/dcerpc/ndr/NdrBuffer;

    move-result-object p1

    const/4 v12, 0x0

    :goto_3
    if-ge v12, v11, :cond_13

    move-object/from16 v0, p0

    iget-object v0, v0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->domain_guid:Ljcifs/dcerpc/rpc$uuid_t;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Ljcifs/dcerpc/rpc$uuid_t;->node:[B

    move-object/from16 v18, v0

    invoke-virtual/range {p1 .. p1}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_small()I

    move-result v19

    move/from16 v0, v19

    int-to-byte v0, v0

    move/from16 v19, v0

    aput-byte v19, v18, v12

    add-int/lit8 v12, v12, 0x1

    goto :goto_3

    :cond_13
    if-eqz v17, :cond_15

    move-object/from16 v0, p0

    iget-object v0, v0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->sid:Ljcifs/dcerpc/rpc$sid_t;

    move-object/from16 v18, v0

    if-nez v18, :cond_14

    new-instance v18, Ljcifs/dcerpc/rpc$sid_t;

    invoke-direct/range {v18 .. v18}, Ljcifs/dcerpc/rpc$sid_t;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->sid:Ljcifs/dcerpc/rpc$sid_t;

    :cond_14
    move-object/from16 v0, p1

    iget-object v0, v0, Ljcifs/dcerpc/ndr/NdrBuffer;->deferred:Ljcifs/dcerpc/ndr/NdrBuffer;

    move-object/from16 p1, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->sid:Ljcifs/dcerpc/rpc$sid_t;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljcifs/dcerpc/rpc$sid_t;->decode(Ljcifs/dcerpc/ndr/NdrBuffer;)V

    :cond_15
    return-void
.end method

.method public encode(Ljcifs/dcerpc/ndr/NdrBuffer;)V
    .locals 14
    .param p1    # Ljcifs/dcerpc/ndr/NdrBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljcifs/dcerpc/ndr/NdrException;
        }
    .end annotation

    const/4 v12, 0x4

    invoke-virtual {p1, v12}, Ljcifs/dcerpc/ndr/NdrBuffer;->align(I)I

    iget-object v12, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->name:Ljcifs/dcerpc/rpc$unicode_string;

    iget-short v12, v12, Ljcifs/dcerpc/rpc$unicode_string;->length:S

    invoke-virtual {p1, v12}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_short(I)V

    iget-object v12, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->name:Ljcifs/dcerpc/rpc$unicode_string;

    iget-short v12, v12, Ljcifs/dcerpc/rpc$unicode_string;->maximum_length:S

    invoke-virtual {p1, v12}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_short(I)V

    iget-object v12, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->name:Ljcifs/dcerpc/rpc$unicode_string;

    iget-object v12, v12, Ljcifs/dcerpc/rpc$unicode_string;->buffer:[S

    const/4 v13, 0x1

    invoke-virtual {p1, v12, v13}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_referent(Ljava/lang/Object;I)V

    iget-object v12, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->dns_domain:Ljcifs/dcerpc/rpc$unicode_string;

    iget-short v12, v12, Ljcifs/dcerpc/rpc$unicode_string;->length:S

    invoke-virtual {p1, v12}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_short(I)V

    iget-object v12, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->dns_domain:Ljcifs/dcerpc/rpc$unicode_string;

    iget-short v12, v12, Ljcifs/dcerpc/rpc$unicode_string;->maximum_length:S

    invoke-virtual {p1, v12}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_short(I)V

    iget-object v12, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->dns_domain:Ljcifs/dcerpc/rpc$unicode_string;

    iget-object v12, v12, Ljcifs/dcerpc/rpc$unicode_string;->buffer:[S

    const/4 v13, 0x1

    invoke-virtual {p1, v12, v13}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_referent(Ljava/lang/Object;I)V

    iget-object v12, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->dns_forest:Ljcifs/dcerpc/rpc$unicode_string;

    iget-short v12, v12, Ljcifs/dcerpc/rpc$unicode_string;->length:S

    invoke-virtual {p1, v12}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_short(I)V

    iget-object v12, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->dns_forest:Ljcifs/dcerpc/rpc$unicode_string;

    iget-short v12, v12, Ljcifs/dcerpc/rpc$unicode_string;->maximum_length:S

    invoke-virtual {p1, v12}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_short(I)V

    iget-object v12, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->dns_forest:Ljcifs/dcerpc/rpc$unicode_string;

    iget-object v12, v12, Ljcifs/dcerpc/rpc$unicode_string;->buffer:[S

    const/4 v13, 0x1

    invoke-virtual {p1, v12, v13}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_referent(Ljava/lang/Object;I)V

    iget-object v12, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->domain_guid:Ljcifs/dcerpc/rpc$uuid_t;

    iget v12, v12, Ljcifs/dcerpc/rpc$uuid_t;->time_low:I

    invoke-virtual {p1, v12}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_long(I)V

    iget-object v12, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->domain_guid:Ljcifs/dcerpc/rpc$uuid_t;

    iget-short v12, v12, Ljcifs/dcerpc/rpc$uuid_t;->time_mid:S

    invoke-virtual {p1, v12}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_short(I)V

    iget-object v12, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->domain_guid:Ljcifs/dcerpc/rpc$uuid_t;

    iget-short v12, v12, Ljcifs/dcerpc/rpc$uuid_t;->time_hi_and_version:S

    invoke-virtual {p1, v12}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_short(I)V

    iget-object v12, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->domain_guid:Ljcifs/dcerpc/rpc$uuid_t;

    iget-byte v12, v12, Ljcifs/dcerpc/rpc$uuid_t;->clock_seq_hi_and_reserved:B

    invoke-virtual {p1, v12}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_small(I)V

    iget-object v12, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->domain_guid:Ljcifs/dcerpc/rpc$uuid_t;

    iget-byte v12, v12, Ljcifs/dcerpc/rpc$uuid_t;->clock_seq_low:B

    invoke-virtual {p1, v12}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_small(I)V

    const/4 v7, 0x6

    iget v6, p1, Ljcifs/dcerpc/ndr/NdrBuffer;->index:I

    const/4 v12, 0x6

    invoke-virtual {p1, v12}, Ljcifs/dcerpc/ndr/NdrBuffer;->advance(I)V

    iget-object v12, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->sid:Ljcifs/dcerpc/rpc$sid_t;

    const/4 v13, 0x1

    invoke-virtual {p1, v12, v13}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_referent(Ljava/lang/Object;I)V

    iget-object v12, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->name:Ljcifs/dcerpc/rpc$unicode_string;

    iget-object v12, v12, Ljcifs/dcerpc/rpc$unicode_string;->buffer:[S

    if-eqz v12, :cond_0

    iget-object p1, p1, Ljcifs/dcerpc/ndr/NdrBuffer;->deferred:Ljcifs/dcerpc/ndr/NdrBuffer;

    iget-object v12, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->name:Ljcifs/dcerpc/rpc$unicode_string;

    iget-short v12, v12, Ljcifs/dcerpc/rpc$unicode_string;->length:S

    div-int/lit8 v10, v12, 0x2

    iget-object v12, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->name:Ljcifs/dcerpc/rpc$unicode_string;

    iget-short v12, v12, Ljcifs/dcerpc/rpc$unicode_string;->maximum_length:S

    div-int/lit8 v11, v12, 0x2

    invoke-virtual {p1, v11}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_long(I)V

    const/4 v12, 0x0

    invoke-virtual {p1, v12}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_long(I)V

    invoke-virtual {p1, v10}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_long(I)V

    iget v9, p1, Ljcifs/dcerpc/ndr/NdrBuffer;->index:I

    mul-int/lit8 v12, v10, 0x2

    invoke-virtual {p1, v12}, Ljcifs/dcerpc/ndr/NdrBuffer;->advance(I)V

    invoke-virtual {p1, v9}, Ljcifs/dcerpc/ndr/NdrBuffer;->derive(I)Ljcifs/dcerpc/ndr/NdrBuffer;

    move-result-object p1

    const/4 v8, 0x0

    :goto_0
    if-ge v8, v10, :cond_0

    iget-object v12, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->name:Ljcifs/dcerpc/rpc$unicode_string;

    iget-object v12, v12, Ljcifs/dcerpc/rpc$unicode_string;->buffer:[S

    aget-short v12, v12, v8

    invoke-virtual {p1, v12}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_short(I)V

    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_0
    iget-object v12, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->dns_domain:Ljcifs/dcerpc/rpc$unicode_string;

    iget-object v12, v12, Ljcifs/dcerpc/rpc$unicode_string;->buffer:[S

    if-eqz v12, :cond_1

    iget-object p1, p1, Ljcifs/dcerpc/ndr/NdrBuffer;->deferred:Ljcifs/dcerpc/ndr/NdrBuffer;

    iget-object v12, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->dns_domain:Ljcifs/dcerpc/rpc$unicode_string;

    iget-short v12, v12, Ljcifs/dcerpc/rpc$unicode_string;->length:S

    div-int/lit8 v1, v12, 0x2

    iget-object v12, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->dns_domain:Ljcifs/dcerpc/rpc$unicode_string;

    iget-short v12, v12, Ljcifs/dcerpc/rpc$unicode_string;->maximum_length:S

    div-int/lit8 v2, v12, 0x2

    invoke-virtual {p1, v2}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_long(I)V

    const/4 v12, 0x0

    invoke-virtual {p1, v12}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_long(I)V

    invoke-virtual {p1, v1}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_long(I)V

    iget v0, p1, Ljcifs/dcerpc/ndr/NdrBuffer;->index:I

    mul-int/lit8 v12, v1, 0x2

    invoke-virtual {p1, v12}, Ljcifs/dcerpc/ndr/NdrBuffer;->advance(I)V

    invoke-virtual {p1, v0}, Ljcifs/dcerpc/ndr/NdrBuffer;->derive(I)Ljcifs/dcerpc/ndr/NdrBuffer;

    move-result-object p1

    const/4 v8, 0x0

    :goto_1
    if-ge v8, v1, :cond_1

    iget-object v12, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->dns_domain:Ljcifs/dcerpc/rpc$unicode_string;

    iget-object v12, v12, Ljcifs/dcerpc/rpc$unicode_string;->buffer:[S

    aget-short v12, v12, v8

    invoke-virtual {p1, v12}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_short(I)V

    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_1
    iget-object v12, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->dns_forest:Ljcifs/dcerpc/rpc$unicode_string;

    iget-object v12, v12, Ljcifs/dcerpc/rpc$unicode_string;->buffer:[S

    if-eqz v12, :cond_2

    iget-object p1, p1, Ljcifs/dcerpc/ndr/NdrBuffer;->deferred:Ljcifs/dcerpc/ndr/NdrBuffer;

    iget-object v12, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->dns_forest:Ljcifs/dcerpc/rpc$unicode_string;

    iget-short v12, v12, Ljcifs/dcerpc/rpc$unicode_string;->length:S

    div-int/lit8 v4, v12, 0x2

    iget-object v12, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->dns_forest:Ljcifs/dcerpc/rpc$unicode_string;

    iget-short v12, v12, Ljcifs/dcerpc/rpc$unicode_string;->maximum_length:S

    div-int/lit8 v5, v12, 0x2

    invoke-virtual {p1, v5}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_long(I)V

    const/4 v12, 0x0

    invoke-virtual {p1, v12}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_long(I)V

    invoke-virtual {p1, v4}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_long(I)V

    iget v3, p1, Ljcifs/dcerpc/ndr/NdrBuffer;->index:I

    mul-int/lit8 v12, v4, 0x2

    invoke-virtual {p1, v12}, Ljcifs/dcerpc/ndr/NdrBuffer;->advance(I)V

    invoke-virtual {p1, v3}, Ljcifs/dcerpc/ndr/NdrBuffer;->derive(I)Ljcifs/dcerpc/ndr/NdrBuffer;

    move-result-object p1

    const/4 v8, 0x0

    :goto_2
    if-ge v8, v4, :cond_2

    iget-object v12, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->dns_forest:Ljcifs/dcerpc/rpc$unicode_string;

    iget-object v12, v12, Ljcifs/dcerpc/rpc$unicode_string;->buffer:[S

    aget-short v12, v12, v8

    invoke-virtual {p1, v12}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_short(I)V

    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    :cond_2
    invoke-virtual {p1, v6}, Ljcifs/dcerpc/ndr/NdrBuffer;->derive(I)Ljcifs/dcerpc/ndr/NdrBuffer;

    move-result-object p1

    const/4 v8, 0x0

    :goto_3
    if-ge v8, v7, :cond_3

    iget-object v12, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->domain_guid:Ljcifs/dcerpc/rpc$uuid_t;

    iget-object v12, v12, Ljcifs/dcerpc/rpc$uuid_t;->node:[B

    aget-byte v12, v12, v8

    invoke-virtual {p1, v12}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_small(I)V

    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    :cond_3
    iget-object v12, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->sid:Ljcifs/dcerpc/rpc$sid_t;

    if-eqz v12, :cond_4

    iget-object p1, p1, Ljcifs/dcerpc/ndr/NdrBuffer;->deferred:Ljcifs/dcerpc/ndr/NdrBuffer;

    iget-object v12, p0, Ljcifs/dcerpc/msrpc/lsarpc$LsarDnsDomainInfo;->sid:Ljcifs/dcerpc/rpc$sid_t;

    invoke-virtual {v12, p1}, Ljcifs/dcerpc/rpc$sid_t;->encode(Ljcifs/dcerpc/ndr/NdrBuffer;)V

    :cond_4
    return-void
.end method
