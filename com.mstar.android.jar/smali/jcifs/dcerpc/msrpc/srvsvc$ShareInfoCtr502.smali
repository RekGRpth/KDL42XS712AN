.class public Ljcifs/dcerpc/msrpc/srvsvc$ShareInfoCtr502;
.super Ljcifs/dcerpc/ndr/NdrObject;
.source "srvsvc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ljcifs/dcerpc/msrpc/srvsvc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ShareInfoCtr502"
.end annotation


# instance fields
.field public array:[Ljcifs/dcerpc/msrpc/srvsvc$ShareInfo502;

.field public count:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljcifs/dcerpc/ndr/NdrObject;-><init>()V

    return-void
.end method


# virtual methods
.method public decode(Ljcifs/dcerpc/ndr/NdrBuffer;)V
    .locals 6
    .param p1    # Ljcifs/dcerpc/ndr/NdrBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljcifs/dcerpc/ndr/NdrException;
        }
    .end annotation

    const/4 v4, 0x4

    invoke-virtual {p1, v4}, Ljcifs/dcerpc/ndr/NdrBuffer;->align(I)I

    invoke-virtual {p1}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_long()I

    move-result v4

    iput v4, p0, Ljcifs/dcerpc/msrpc/srvsvc$ShareInfoCtr502;->count:I

    invoke-virtual {p1}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_long()I

    move-result v1

    if-eqz v1, :cond_4

    iget-object p1, p1, Ljcifs/dcerpc/ndr/NdrBuffer;->deferred:Ljcifs/dcerpc/ndr/NdrBuffer;

    invoke-virtual {p1}, Ljcifs/dcerpc/ndr/NdrBuffer;->dec_ndr_long()I

    move-result v2

    iget v0, p1, Ljcifs/dcerpc/ndr/NdrBuffer;->index:I

    mul-int/lit8 v4, v2, 0x28

    invoke-virtual {p1, v4}, Ljcifs/dcerpc/ndr/NdrBuffer;->advance(I)V

    iget-object v4, p0, Ljcifs/dcerpc/msrpc/srvsvc$ShareInfoCtr502;->array:[Ljcifs/dcerpc/msrpc/srvsvc$ShareInfo502;

    if-nez v4, :cond_2

    if-ltz v2, :cond_0

    const v4, 0xffff

    if-le v2, v4, :cond_1

    :cond_0
    new-instance v4, Ljcifs/dcerpc/ndr/NdrException;

    const-string v5, "invalid array conformance"

    invoke-direct {v4, v5}, Ljcifs/dcerpc/ndr/NdrException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_1
    new-array v4, v2, [Ljcifs/dcerpc/msrpc/srvsvc$ShareInfo502;

    iput-object v4, p0, Ljcifs/dcerpc/msrpc/srvsvc$ShareInfoCtr502;->array:[Ljcifs/dcerpc/msrpc/srvsvc$ShareInfo502;

    :cond_2
    invoke-virtual {p1, v0}, Ljcifs/dcerpc/ndr/NdrBuffer;->derive(I)Ljcifs/dcerpc/ndr/NdrBuffer;

    move-result-object p1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_4

    iget-object v4, p0, Ljcifs/dcerpc/msrpc/srvsvc$ShareInfoCtr502;->array:[Ljcifs/dcerpc/msrpc/srvsvc$ShareInfo502;

    aget-object v4, v4, v3

    if-nez v4, :cond_3

    iget-object v4, p0, Ljcifs/dcerpc/msrpc/srvsvc$ShareInfoCtr502;->array:[Ljcifs/dcerpc/msrpc/srvsvc$ShareInfo502;

    new-instance v5, Ljcifs/dcerpc/msrpc/srvsvc$ShareInfo502;

    invoke-direct {v5}, Ljcifs/dcerpc/msrpc/srvsvc$ShareInfo502;-><init>()V

    aput-object v5, v4, v3

    :cond_3
    iget-object v4, p0, Ljcifs/dcerpc/msrpc/srvsvc$ShareInfoCtr502;->array:[Ljcifs/dcerpc/msrpc/srvsvc$ShareInfo502;

    aget-object v4, v4, v3

    invoke-virtual {v4, p1}, Ljcifs/dcerpc/msrpc/srvsvc$ShareInfo502;->decode(Ljcifs/dcerpc/ndr/NdrBuffer;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_4
    return-void
.end method

.method public encode(Ljcifs/dcerpc/ndr/NdrBuffer;)V
    .locals 5
    .param p1    # Ljcifs/dcerpc/ndr/NdrBuffer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljcifs/dcerpc/ndr/NdrException;
        }
    .end annotation

    const/4 v3, 0x4

    invoke-virtual {p1, v3}, Ljcifs/dcerpc/ndr/NdrBuffer;->align(I)I

    iget v3, p0, Ljcifs/dcerpc/msrpc/srvsvc$ShareInfoCtr502;->count:I

    invoke-virtual {p1, v3}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_long(I)V

    iget-object v3, p0, Ljcifs/dcerpc/msrpc/srvsvc$ShareInfoCtr502;->array:[Ljcifs/dcerpc/msrpc/srvsvc$ShareInfo502;

    const/4 v4, 0x1

    invoke-virtual {p1, v3, v4}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_referent(Ljava/lang/Object;I)V

    iget-object v3, p0, Ljcifs/dcerpc/msrpc/srvsvc$ShareInfoCtr502;->array:[Ljcifs/dcerpc/msrpc/srvsvc$ShareInfo502;

    if-eqz v3, :cond_0

    iget-object p1, p1, Ljcifs/dcerpc/ndr/NdrBuffer;->deferred:Ljcifs/dcerpc/ndr/NdrBuffer;

    iget v1, p0, Ljcifs/dcerpc/msrpc/srvsvc$ShareInfoCtr502;->count:I

    invoke-virtual {p1, v1}, Ljcifs/dcerpc/ndr/NdrBuffer;->enc_ndr_long(I)V

    iget v0, p1, Ljcifs/dcerpc/ndr/NdrBuffer;->index:I

    mul-int/lit8 v3, v1, 0x28

    invoke-virtual {p1, v3}, Ljcifs/dcerpc/ndr/NdrBuffer;->advance(I)V

    invoke-virtual {p1, v0}, Ljcifs/dcerpc/ndr/NdrBuffer;->derive(I)Ljcifs/dcerpc/ndr/NdrBuffer;

    move-result-object p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    iget-object v3, p0, Ljcifs/dcerpc/msrpc/srvsvc$ShareInfoCtr502;->array:[Ljcifs/dcerpc/msrpc/srvsvc$ShareInfo502;

    aget-object v3, v3, v2

    invoke-virtual {v3, p1}, Ljcifs/dcerpc/msrpc/srvsvc$ShareInfo502;->encode(Ljcifs/dcerpc/ndr/NdrBuffer;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
