.class public Ljcifs/ntlmssp/Type1Message;
.super Ljcifs/ntlmssp/NtlmMessage;
.source "Type1Message.java"


# static fields
.field private static final DEFAULT_DOMAIN:Ljava/lang/String;

.field private static final DEFAULT_FLAGS:I

.field private static final DEFAULT_WORKSTATION:Ljava/lang/String;


# instance fields
.field private suppliedDomain:Ljava/lang/String;

.field private suppliedWorkstation:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v1, 0x1

    const-string v2, "jcifs.smb.client.useUnicode"

    invoke-static {v2, v1}, Ljcifs/Config;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    or-int/lit16 v1, v1, 0x200

    sput v1, Ljcifs/ntlmssp/Type1Message;->DEFAULT_FLAGS:I

    const-string v1, "jcifs.smb.client.domain"

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljcifs/Config;->getProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Ljcifs/ntlmssp/Type1Message;->DEFAULT_DOMAIN:Ljava/lang/String;

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Ljcifs/netbios/NbtAddress;->getLocalHost()Ljcifs/netbios/NbtAddress;

    move-result-object v1

    invoke-virtual {v1}, Ljcifs/netbios/NbtAddress;->getHostName()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    sput-object v0, Ljcifs/ntlmssp/Type1Message;->DEFAULT_WORKSTATION:Ljava/lang/String;

    return-void

    :cond_0
    const/4 v1, 0x2

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 3

    invoke-static {}, Ljcifs/ntlmssp/Type1Message;->getDefaultFlags()I

    move-result v0

    invoke-static {}, Ljcifs/ntlmssp/Type1Message;->getDefaultDomain()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljcifs/ntlmssp/Type1Message;->getDefaultWorkstation()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Ljcifs/ntlmssp/Type1Message;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # I
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;

    invoke-direct {p0}, Ljcifs/ntlmssp/NtlmMessage;-><init>()V

    invoke-static {}, Ljcifs/ntlmssp/Type1Message;->getDefaultFlags()I

    move-result v0

    or-int/2addr v0, p1

    invoke-virtual {p0, v0}, Ljcifs/ntlmssp/Type1Message;->setFlags(I)V

    invoke-virtual {p0, p2}, Ljcifs/ntlmssp/Type1Message;->setSuppliedDomain(Ljava/lang/String;)V

    if-nez p3, :cond_0

    invoke-static {}, Ljcifs/ntlmssp/Type1Message;->getDefaultWorkstation()Ljava/lang/String;

    move-result-object p3

    :cond_0
    invoke-virtual {p0, p3}, Ljcifs/ntlmssp/Type1Message;->setSuppliedWorkstation(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>([B)V
    .locals 0
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-direct {p0}, Ljcifs/ntlmssp/NtlmMessage;-><init>()V

    invoke-direct {p0, p1}, Ljcifs/ntlmssp/Type1Message;->parse([B)V

    return-void
.end method

.method public static getDefaultDomain()Ljava/lang/String;
    .locals 1

    sget-object v0, Ljcifs/ntlmssp/Type1Message;->DEFAULT_DOMAIN:Ljava/lang/String;

    return-object v0
.end method

.method public static getDefaultFlags()I
    .locals 1

    sget v0, Ljcifs/ntlmssp/Type1Message;->DEFAULT_FLAGS:I

    return v0
.end method

.method public static getDefaultWorkstation()Ljava/lang/String;
    .locals 1

    sget-object v0, Ljcifs/ntlmssp/Type1Message;->DEFAULT_WORKSTATION:Ljava/lang/String;

    return-object v0
.end method

.method private parse([B)V
    .locals 9
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/16 v8, 0x8

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v8, :cond_1

    aget-byte v6, p1, v2

    sget-object v7, Ljcifs/ntlmssp/Type1Message;->NTLMSSP_SIGNATURE:[B

    aget-byte v7, v7, v2

    if-eq v6, v7, :cond_0

    new-instance v6, Ljava/io/IOException;

    const-string v7, "Not an NTLMSSP message."

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    invoke-static {p1, v8}, Ljcifs/ntlmssp/Type1Message;->readULong([BI)I

    move-result v6

    const/4 v7, 0x1

    if-eq v6, v7, :cond_2

    new-instance v6, Ljava/io/IOException;

    const-string v7, "Not a Type 1 message."

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    :cond_2
    const/16 v6, 0xc

    invoke-static {p1, v6}, Ljcifs/ntlmssp/Type1Message;->readULong([BI)I

    move-result v1

    const/4 v3, 0x0

    and-int/lit16 v6, v1, 0x1000

    if-eqz v6, :cond_3

    const/16 v6, 0x10

    invoke-static {p1, v6}, Ljcifs/ntlmssp/Type1Message;->readSecurityBuffer([BI)[B

    move-result-object v0

    new-instance v3, Ljava/lang/String;

    invoke-static {}, Ljcifs/ntlmssp/Type1Message;->getOEMEncoding()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v0, v6}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    :cond_3
    const/4 v4, 0x0

    and-int/lit16 v6, v1, 0x2000

    if-eqz v6, :cond_4

    const/16 v6, 0x18

    invoke-static {p1, v6}, Ljcifs/ntlmssp/Type1Message;->readSecurityBuffer([BI)[B

    move-result-object v5

    new-instance v4, Ljava/lang/String;

    invoke-static {}, Ljcifs/ntlmssp/Type1Message;->getOEMEncoding()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    :cond_4
    invoke-virtual {p0, v1}, Ljcifs/ntlmssp/Type1Message;->setFlags(I)V

    invoke-virtual {p0, v3}, Ljcifs/ntlmssp/Type1Message;->setSuppliedDomain(Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Ljcifs/ntlmssp/Type1Message;->setSuppliedWorkstation(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getSuppliedDomain()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljcifs/ntlmssp/Type1Message;->suppliedDomain:Ljava/lang/String;

    return-object v0
.end method

.method public getSuppliedWorkstation()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Ljcifs/ntlmssp/Type1Message;->suppliedWorkstation:Ljava/lang/String;

    return-object v0
.end method

.method public setSuppliedDomain(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Ljcifs/ntlmssp/Type1Message;->suppliedDomain:Ljava/lang/String;

    return-void
.end method

.method public setSuppliedWorkstation(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Ljcifs/ntlmssp/Type1Message;->suppliedWorkstation:Ljava/lang/String;

    return-void
.end method

.method public toByteArray()[B
    .locals 12

    const/16 v8, 0x10

    :try_start_0
    invoke-virtual {p0}, Ljcifs/ntlmssp/Type1Message;->getSuppliedDomain()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Ljcifs/ntlmssp/Type1Message;->getSuppliedWorkstation()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Ljcifs/ntlmssp/Type1Message;->getFlags()I

    move-result v2

    const/4 v3, 0x0

    const/4 v9, 0x0

    new-array v0, v9, [B

    if-eqz v4, :cond_2

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v9

    if-eqz v9, :cond_2

    const/4 v3, 0x1

    or-int/lit16 v2, v2, 0x1000

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v9

    invoke-static {}, Ljcifs/ntlmssp/Type1Message;->getOEMEncoding()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    :goto_0
    const/4 v9, 0x0

    new-array v7, v9, [B

    if-eqz v5, :cond_3

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v9

    if-eqz v9, :cond_3

    const/4 v3, 0x1

    or-int/lit16 v2, v2, 0x2000

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v9

    invoke-static {}, Ljcifs/ntlmssp/Type1Message;->getOEMEncoding()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v7

    :goto_1
    if-eqz v3, :cond_0

    array-length v8, v0

    add-int/lit8 v8, v8, 0x20

    array-length v9, v7

    add-int/2addr v8, v9

    :cond_0
    new-array v6, v8, [B

    sget-object v8, Ljcifs/ntlmssp/Type1Message;->NTLMSSP_SIGNATURE:[B

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/16 v11, 0x8

    invoke-static {v8, v9, v6, v10, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/16 v8, 0x8

    const/4 v9, 0x1

    invoke-static {v6, v8, v9}, Ljcifs/ntlmssp/Type1Message;->writeULong([BII)V

    const/16 v8, 0xc

    invoke-static {v6, v8, v2}, Ljcifs/ntlmssp/Type1Message;->writeULong([BII)V

    if-eqz v3, :cond_1

    const/16 v8, 0x10

    const/16 v9, 0x20

    invoke-static {v6, v8, v9, v0}, Ljcifs/ntlmssp/Type1Message;->writeSecurityBuffer([BII[B)V

    const/16 v8, 0x18

    array-length v9, v0

    add-int/lit8 v9, v9, 0x20

    invoke-static {v6, v8, v9, v7}, Ljcifs/ntlmssp/Type1Message;->writeSecurityBuffer([BII[B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    return-object v6

    :cond_2
    and-int/lit16 v2, v2, -0x1001

    goto :goto_0

    :cond_3
    and-int/lit16 v2, v2, -0x2001

    goto :goto_1

    :catch_0
    move-exception v1

    new-instance v8, Ljava/lang/IllegalStateException;

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v8
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    invoke-virtual {p0}, Ljcifs/ntlmssp/Type1Message;->getSuppliedDomain()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljcifs/ntlmssp/Type1Message;->getSuppliedWorkstation()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Type1Message[suppliedDomain="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez v0, :cond_0

    const-string v0, "null"

    :cond_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",suppliedWorkstation="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez v1, :cond_1

    const-string v1, "null"

    :cond_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ",flags=0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Ljcifs/ntlmssp/Type1Message;->getFlags()I

    move-result v3

    const/16 v4, 0x8

    invoke-static {v3, v4}, Ljcifs/util/Hexdump;->toHexString(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method
