.class public Ljcifs/smb/SID;
.super Ljcifs/dcerpc/rpc$sid_t;
.source "SID.java"


# static fields
.field public static CREATOR_OWNER:Ljcifs/smb/SID; = null

.field public static EVERYONE:Ljcifs/smb/SID; = null

.field public static final SID_FLAG_RESOLVE_SIDS:I = 0x1

.field public static final SID_TYPE_ALIAS:I = 0x4

.field public static final SID_TYPE_DELETED:I = 0x6

.field public static final SID_TYPE_DOMAIN:I = 0x3

.field public static final SID_TYPE_DOM_GRP:I = 0x2

.field public static final SID_TYPE_INVALID:I = 0x7

.field static final SID_TYPE_NAMES:[Ljava/lang/String;

.field public static final SID_TYPE_UNKNOWN:I = 0x8

.field public static final SID_TYPE_USER:I = 0x1

.field public static final SID_TYPE_USE_NONE:I = 0x0

.field public static final SID_TYPE_WKN_GRP:I = 0x5

.field public static SYSTEM:Ljcifs/smb/SID;

.field static sid_cache:Ljava/util/Map;


# instance fields
.field acctName:Ljava/lang/String;

.field domainName:Ljava/lang/String;

.field origin_auth:Ljcifs/smb/NtlmPasswordAuthentication;

.field origin_server:Ljava/lang/String;

.field type:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x0

    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "0"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "User"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "Domain group"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "Domain"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "Local group"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "Builtin group"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Deleted"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "Invalid"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "Unknown"

    aput-object v2, v0, v1

    sput-object v0, Ljcifs/smb/SID;->SID_TYPE_NAMES:[Ljava/lang/String;

    sput-object v3, Ljcifs/smb/SID;->EVERYONE:Ljcifs/smb/SID;

    sput-object v3, Ljcifs/smb/SID;->CREATOR_OWNER:Ljcifs/smb/SID;

    sput-object v3, Ljcifs/smb/SID;->SYSTEM:Ljcifs/smb/SID;

    :try_start_0
    new-instance v0, Ljcifs/smb/SID;

    const-string v1, "S-1-1-0"

    invoke-direct {v0, v1}, Ljcifs/smb/SID;-><init>(Ljava/lang/String;)V

    sput-object v0, Ljcifs/smb/SID;->EVERYONE:Ljcifs/smb/SID;

    new-instance v0, Ljcifs/smb/SID;

    const-string v1, "S-1-3-0"

    invoke-direct {v0, v1}, Ljcifs/smb/SID;-><init>(Ljava/lang/String;)V

    sput-object v0, Ljcifs/smb/SID;->CREATOR_OWNER:Ljcifs/smb/SID;

    new-instance v0, Ljcifs/smb/SID;

    const-string v1, "S-1-5-18"

    invoke-direct {v0, v1}, Ljcifs/smb/SID;-><init>(Ljava/lang/String;)V

    sput-object v0, Ljcifs/smb/SID;->SYSTEM:Ljcifs/smb/SID;
    :try_end_0
    .catch Ljcifs/smb/SmbException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Ljcifs/smb/SID;->sid_cache:Ljava/util/Map;

    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 10
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljcifs/smb/SmbException;
        }
    .end annotation

    const/4 v5, 0x0

    invoke-direct {p0}, Ljcifs/dcerpc/rpc$sid_t;-><init>()V

    iput-object v5, p0, Ljcifs/smb/SID;->domainName:Ljava/lang/String;

    iput-object v5, p0, Ljcifs/smb/SID;->acctName:Ljava/lang/String;

    iput-object v5, p0, Ljcifs/smb/SID;->origin_server:Ljava/lang/String;

    iput-object v5, p0, Ljcifs/smb/SID;->origin_auth:Ljcifs/smb/NtlmPasswordAuthentication;

    new-instance v3, Ljava/util/StringTokenizer;

    const-string v5, "-"

    invoke-direct {v3, p1, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v5

    const/4 v6, 0x3

    if-lt v5, v6, :cond_0

    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v5

    const-string v6, "S"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    :cond_0
    new-instance v5, Ljcifs/smb/SmbException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Bad textual SID format: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljcifs/smb/SmbException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_1
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Byte;->parseByte(Ljava/lang/String;)B

    move-result v5

    iput-byte v5, p0, Ljcifs/smb/SID;->revision:B

    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v4

    const-wide/16 v1, 0x0

    const-string v5, "0x"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x10

    invoke-static {v5, v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v1

    :goto_0
    const/4 v5, 0x6

    new-array v5, v5, [B

    iput-object v5, p0, Ljcifs/smb/SID;->identifier_authority:[B

    const/4 v0, 0x5

    :goto_1
    const-wide/16 v5, 0x0

    cmp-long v5, v1, v5

    if-lez v5, :cond_3

    iget-object v5, p0, Ljcifs/smb/SID;->identifier_authority:[B

    const-wide/16 v6, 0x100

    rem-long v6, v1, v6

    long-to-int v6, v6

    int-to-byte v6, v6

    aput-byte v6, v5, v0

    const/16 v5, 0x8

    shr-long/2addr v1, v5

    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_2
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    goto :goto_0

    :cond_3
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v5

    int-to-byte v5, v5

    iput-byte v5, p0, Ljcifs/smb/SID;->sub_authority_count:B

    iget-byte v5, p0, Ljcifs/smb/SID;->sub_authority_count:B

    if-lez v5, :cond_4

    iget-byte v5, p0, Ljcifs/smb/SID;->sub_authority_count:B

    new-array v5, v5, [I

    iput-object v5, p0, Ljcifs/smb/SID;->sub_authority:[I

    const/4 v0, 0x0

    :goto_2
    iget-byte v5, p0, Ljcifs/smb/SID;->sub_authority_count:B

    if-ge v0, v5, :cond_4

    iget-object v5, p0, Ljcifs/smb/SID;->sub_authority:[I

    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    const-wide v8, 0xffffffffL

    and-long/2addr v6, v8

    long-to-int v6, v6

    aput v6, v5, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    return-void
.end method

.method public constructor <init>(Ljcifs/dcerpc/rpc$sid_t;ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .param p1    # Ljcifs/dcerpc/rpc$sid_t;
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Z

    const/4 v1, 0x0

    invoke-direct {p0}, Ljcifs/dcerpc/rpc$sid_t;-><init>()V

    iput-object v1, p0, Ljcifs/smb/SID;->domainName:Ljava/lang/String;

    iput-object v1, p0, Ljcifs/smb/SID;->acctName:Ljava/lang/String;

    iput-object v1, p0, Ljcifs/smb/SID;->origin_server:Ljava/lang/String;

    iput-object v1, p0, Ljcifs/smb/SID;->origin_auth:Ljcifs/smb/NtlmPasswordAuthentication;

    iget-byte v1, p1, Ljcifs/dcerpc/rpc$sid_t;->revision:B

    iput-byte v1, p0, Ljcifs/smb/SID;->revision:B

    iget-byte v1, p1, Ljcifs/dcerpc/rpc$sid_t;->sub_authority_count:B

    iput-byte v1, p0, Ljcifs/smb/SID;->sub_authority_count:B

    iget-object v1, p1, Ljcifs/dcerpc/rpc$sid_t;->identifier_authority:[B

    iput-object v1, p0, Ljcifs/smb/SID;->identifier_authority:[B

    iget-object v1, p1, Ljcifs/dcerpc/rpc$sid_t;->sub_authority:[I

    iput-object v1, p0, Ljcifs/smb/SID;->sub_authority:[I

    iput p2, p0, Ljcifs/smb/SID;->type:I

    iput-object p3, p0, Ljcifs/smb/SID;->domainName:Ljava/lang/String;

    iput-object p4, p0, Ljcifs/smb/SID;->acctName:Ljava/lang/String;

    if-eqz p5, :cond_0

    iget-byte v1, p0, Ljcifs/smb/SID;->sub_authority_count:B

    add-int/lit8 v1, v1, -0x1

    int-to-byte v1, v1

    iput-byte v1, p0, Ljcifs/smb/SID;->sub_authority_count:B

    iget-byte v1, p0, Ljcifs/smb/SID;->sub_authority_count:B

    new-array v1, v1, [I

    iput-object v1, p0, Ljcifs/smb/SID;->sub_authority:[I

    const/4 v0, 0x0

    :goto_0
    iget-byte v1, p0, Ljcifs/smb/SID;->sub_authority_count:B

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Ljcifs/smb/SID;->sub_authority:[I

    iget-object v2, p1, Ljcifs/dcerpc/rpc$sid_t;->sub_authority:[I

    aget v2, v2, v0

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public constructor <init>(Ljcifs/smb/SID;I)V
    .locals 3
    .param p1    # Ljcifs/smb/SID;
    .param p2    # I

    const/4 v1, 0x0

    invoke-direct {p0}, Ljcifs/dcerpc/rpc$sid_t;-><init>()V

    iput-object v1, p0, Ljcifs/smb/SID;->domainName:Ljava/lang/String;

    iput-object v1, p0, Ljcifs/smb/SID;->acctName:Ljava/lang/String;

    iput-object v1, p0, Ljcifs/smb/SID;->origin_server:Ljava/lang/String;

    iput-object v1, p0, Ljcifs/smb/SID;->origin_auth:Ljcifs/smb/NtlmPasswordAuthentication;

    iget-byte v1, p1, Ljcifs/smb/SID;->revision:B

    iput-byte v1, p0, Ljcifs/smb/SID;->revision:B

    iget-object v1, p1, Ljcifs/smb/SID;->identifier_authority:[B

    iput-object v1, p0, Ljcifs/smb/SID;->identifier_authority:[B

    iget-byte v1, p1, Ljcifs/smb/SID;->sub_authority_count:B

    add-int/lit8 v1, v1, 0x1

    int-to-byte v1, v1

    iput-byte v1, p0, Ljcifs/smb/SID;->sub_authority_count:B

    iget-byte v1, p0, Ljcifs/smb/SID;->sub_authority_count:B

    new-array v1, v1, [I

    iput-object v1, p0, Ljcifs/smb/SID;->sub_authority:[I

    const/4 v0, 0x0

    :goto_0
    iget-byte v1, p1, Ljcifs/smb/SID;->sub_authority_count:B

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Ljcifs/smb/SID;->sub_authority:[I

    iget-object v2, p1, Ljcifs/smb/SID;->sub_authority:[I

    aget v2, v2, v0

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Ljcifs/smb/SID;->sub_authority:[I

    aput p2, v1, v0

    return-void
.end method

.method public constructor <init>([BI)V
    .locals 5
    .param p1    # [B
    .param p2    # I

    const/4 v4, 0x6

    const/4 v2, 0x0

    invoke-direct {p0}, Ljcifs/dcerpc/rpc$sid_t;-><init>()V

    iput-object v2, p0, Ljcifs/smb/SID;->domainName:Ljava/lang/String;

    iput-object v2, p0, Ljcifs/smb/SID;->acctName:Ljava/lang/String;

    iput-object v2, p0, Ljcifs/smb/SID;->origin_server:Ljava/lang/String;

    iput-object v2, p0, Ljcifs/smb/SID;->origin_auth:Ljcifs/smb/NtlmPasswordAuthentication;

    add-int/lit8 v1, p2, 0x1

    aget-byte v2, p1, p2

    iput-byte v2, p0, Ljcifs/smb/SID;->revision:B

    add-int/lit8 p2, v1, 0x1

    aget-byte v2, p1, v1

    iput-byte v2, p0, Ljcifs/smb/SID;->sub_authority_count:B

    new-array v2, v4, [B

    iput-object v2, p0, Ljcifs/smb/SID;->identifier_authority:[B

    iget-object v2, p0, Ljcifs/smb/SID;->identifier_authority:[B

    const/4 v3, 0x0

    invoke-static {p1, p2, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 p2, p2, 0x6

    iget-byte v2, p0, Ljcifs/smb/SID;->sub_authority_count:B

    const/16 v3, 0x64

    if-le v2, v3, :cond_0

    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Invalid SID sub_authority_count"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    iget-byte v2, p0, Ljcifs/smb/SID;->sub_authority_count:B

    new-array v2, v2, [I

    iput-object v2, p0, Ljcifs/smb/SID;->sub_authority:[I

    const/4 v0, 0x0

    :goto_0
    iget-byte v2, p0, Ljcifs/smb/SID;->sub_authority_count:B

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Ljcifs/smb/SID;->sub_authority:[I

    invoke-static {p1, p2}, Ljcifs/smb/ServerMessageBlock;->readInt4([BI)I

    move-result v3

    aput v3, v2, v0

    add-int/lit8 p2, p2, 0x4

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method static getGroupMemberSids0(Ljcifs/dcerpc/DcerpcHandle;Ljcifs/dcerpc/msrpc/SamrDomainHandle;Ljcifs/smb/SID;II)[Ljcifs/smb/SID;
    .locals 18
    .param p0    # Ljcifs/dcerpc/DcerpcHandle;
    .param p1    # Ljcifs/dcerpc/msrpc/SamrDomainHandle;
    .param p2    # Ljcifs/smb/SID;
    .param p3    # I
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v9, 0x0

    new-instance v16, Ljcifs/dcerpc/msrpc/lsarpc$LsarSidArray;

    invoke-direct/range {v16 .. v16}, Ljcifs/dcerpc/msrpc/lsarpc$LsarSidArray;-><init>()V

    const/4 v14, 0x0

    :try_start_0
    new-instance v10, Ljcifs/dcerpc/msrpc/SamrAliasHandle;

    const v3, 0x2000c

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p3

    invoke-direct {v10, v0, v1, v3, v2}, Ljcifs/dcerpc/msrpc/SamrAliasHandle;-><init>(Ljcifs/dcerpc/DcerpcHandle;Ljcifs/dcerpc/msrpc/SamrDomainHandle;II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    new-instance v15, Ljcifs/dcerpc/msrpc/MsrpcGetMembersInAlias;

    move-object/from16 v0, v16

    invoke-direct {v15, v10, v0}, Ljcifs/dcerpc/msrpc/MsrpcGetMembersInAlias;-><init>(Ljcifs/dcerpc/msrpc/SamrAliasHandle;Ljcifs/dcerpc/msrpc/lsarpc$LsarSidArray;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :try_start_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Ljcifs/dcerpc/DcerpcHandle;->sendrecv(Ljcifs/dcerpc/DcerpcMessage;)V

    iget v3, v15, Ljcifs/dcerpc/msrpc/MsrpcGetMembersInAlias;->retval:I

    if-eqz v3, :cond_1

    new-instance v3, Ljcifs/smb/SmbException;

    iget v4, v15, Ljcifs/dcerpc/msrpc/MsrpcGetMembersInAlias;->retval:I

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Ljcifs/smb/SmbException;-><init>(IZ)V

    throw v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v3

    move-object v14, v15

    move-object v9, v10

    :goto_0
    if-eqz v9, :cond_0

    invoke-virtual {v9}, Ljcifs/dcerpc/msrpc/SamrAliasHandle;->close()V

    :cond_0
    throw v3

    :cond_1
    :try_start_3
    iget-object v3, v15, Ljcifs/dcerpc/msrpc/MsrpcGetMembersInAlias;->sids:Ljcifs/dcerpc/msrpc/lsarpc$LsarSidArray;

    iget v3, v3, Ljcifs/dcerpc/msrpc/lsarpc$LsarSidArray;->num_sids:I

    new-array v0, v3, [Ljcifs/smb/SID;

    move-object/from16 v17, v0

    invoke-virtual/range {p0 .. p0}, Ljcifs/dcerpc/DcerpcHandle;->getServer()Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Ljcifs/dcerpc/DcerpcHandle;->getPrincipal()Ljava/security/Principal;

    move-result-object v12

    check-cast v12, Ljcifs/smb/NtlmPasswordAuthentication;

    const/4 v11, 0x0

    :goto_1
    move-object/from16 v0, v17

    array-length v3, v0

    if-ge v11, v3, :cond_2

    new-instance v3, Ljcifs/smb/SID;

    iget-object v4, v15, Ljcifs/dcerpc/msrpc/MsrpcGetMembersInAlias;->sids:Ljcifs/dcerpc/msrpc/lsarpc$LsarSidArray;

    iget-object v4, v4, Ljcifs/dcerpc/msrpc/lsarpc$LsarSidArray;->sids:[Ljcifs/dcerpc/msrpc/lsarpc$LsarSidPtr;

    aget-object v4, v4, v11

    iget-object v4, v4, Ljcifs/dcerpc/msrpc/lsarpc$LsarSidPtr;->sid:Ljcifs/dcerpc/rpc$sid_t;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct/range {v3 .. v8}, Ljcifs/smb/SID;-><init>(Ljcifs/dcerpc/rpc$sid_t;ILjava/lang/String;Ljava/lang/String;Z)V

    aput-object v3, v17, v11

    aget-object v3, v17, v11

    iput-object v13, v3, Ljcifs/smb/SID;->origin_server:Ljava/lang/String;

    aget-object v3, v17, v11

    iput-object v12, v3, Ljcifs/smb/SID;->origin_auth:Ljcifs/smb/NtlmPasswordAuthentication;

    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    :cond_2
    move-object/from16 v0, v17

    array-length v3, v0

    if-lez v3, :cond_3

    and-int/lit8 v3, p4, 0x1

    if-eqz v3, :cond_3

    move-object/from16 v0, v17

    invoke-static {v13, v12, v0}, Ljcifs/smb/SID;->resolveSids(Ljava/lang/String;Ljcifs/smb/NtlmPasswordAuthentication;[Ljcifs/smb/SID;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_3
    if-eqz v10, :cond_4

    invoke-virtual {v10}, Ljcifs/dcerpc/msrpc/SamrAliasHandle;->close()V

    :cond_4
    return-object v17

    :catchall_1
    move-exception v3

    goto :goto_0

    :catchall_2
    move-exception v3

    move-object v9, v10

    goto :goto_0
.end method

.method static getLocalGroupsMap(Ljava/lang/String;Ljcifs/smb/NtlmPasswordAuthentication;I)Ljava/util/Map;
    .locals 22
    .param p0    # Ljava/lang/String;
    .param p1    # Ljcifs/smb/NtlmPasswordAuthentication;
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    invoke-static/range {p0 .. p1}, Ljcifs/smb/SID;->getServerSid(Ljava/lang/String;Ljcifs/smb/NtlmPasswordAuthentication;)Ljcifs/smb/SID;

    move-result-object v5

    const/4 v10, 0x0

    const/4 v14, 0x0

    const/4 v3, 0x0

    new-instance v17, Ljcifs/dcerpc/msrpc/samr$SamrSamArray;

    invoke-direct/range {v17 .. v17}, Ljcifs/dcerpc/msrpc/samr$SamrSamArray;-><init>()V

    sget-object v19, Ljcifs/smb/SID;->sid_cache:Ljava/util/Map;

    monitor-enter v19

    :try_start_0
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "ncacn_np:"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v20, "[\\PIPE\\samr]"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Ljcifs/dcerpc/DcerpcHandle;->getHandle(Ljava/lang/String;Ljcifs/smb/NtlmPasswordAuthentication;)Ljcifs/dcerpc/DcerpcHandle;

    move-result-object v10

    new-instance v15, Ljcifs/dcerpc/msrpc/SamrPolicyHandle;

    const/high16 v18, 0x2000000

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-direct {v15, v10, v0, v1}, Ljcifs/dcerpc/msrpc/SamrPolicyHandle;-><init>(Ljcifs/dcerpc/DcerpcHandle;Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    :try_start_1
    new-instance v4, Ljcifs/dcerpc/msrpc/SamrDomainHandle;

    const/high16 v18, 0x2000000

    move/from16 v0, v18

    invoke-direct {v4, v10, v15, v0, v5}, Ljcifs/dcerpc/msrpc/SamrDomainHandle;-><init>(Ljcifs/dcerpc/DcerpcHandle;Ljcifs/dcerpc/msrpc/SamrPolicyHandle;ILjcifs/dcerpc/rpc$sid_t;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    :try_start_2
    new-instance v16, Ljcifs/dcerpc/msrpc/MsrpcEnumerateAliasesInDomain;

    const v18, 0xffff

    move-object/from16 v0, v16

    move/from16 v1, v18

    move-object/from16 v2, v17

    invoke-direct {v0, v4, v1, v2}, Ljcifs/dcerpc/msrpc/MsrpcEnumerateAliasesInDomain;-><init>(Ljcifs/dcerpc/msrpc/SamrDomainHandle;ILjcifs/dcerpc/msrpc/samr$SamrSamArray;)V

    move-object/from16 v0, v16

    invoke-virtual {v10, v0}, Ljcifs/dcerpc/DcerpcHandle;->sendrecv(Ljcifs/dcerpc/DcerpcMessage;)V

    move-object/from16 v0, v16

    iget v0, v0, Ljcifs/dcerpc/msrpc/MsrpcEnumerateAliasesInDomain;->retval:I

    move/from16 v18, v0

    if-eqz v18, :cond_3

    new-instance v18, Ljcifs/smb/SmbException;

    move-object/from16 v0, v16

    iget v0, v0, Ljcifs/dcerpc/msrpc/MsrpcEnumerateAliasesInDomain;->retval:I

    move/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Ljcifs/smb/SmbException;-><init>(IZ)V

    throw v18
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v18

    move-object v3, v4

    move-object v14, v15

    :goto_0
    if-eqz v10, :cond_2

    if-eqz v14, :cond_1

    if-eqz v3, :cond_0

    :try_start_3
    invoke-virtual {v3}, Ljcifs/dcerpc/msrpc/SamrDomainHandle;->close()V

    :cond_0
    invoke-virtual {v14}, Ljcifs/dcerpc/msrpc/SamrPolicyHandle;->close()V

    :cond_1
    invoke-virtual {v10}, Ljcifs/dcerpc/DcerpcHandle;->close()V

    :cond_2
    throw v18

    :catchall_1
    move-exception v18

    :goto_1
    monitor-exit v19
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v18

    :cond_3
    :try_start_4
    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    const/4 v6, 0x0

    :goto_2
    move-object/from16 v0, v16

    iget-object v0, v0, Ljcifs/dcerpc/msrpc/MsrpcEnumerateAliasesInDomain;->sam:Ljcifs/dcerpc/msrpc/samr$SamrSamArray;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Ljcifs/dcerpc/msrpc/samr$SamrSamArray;->count:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v6, v0, :cond_7

    move-object/from16 v0, v16

    iget-object v0, v0, Ljcifs/dcerpc/msrpc/MsrpcEnumerateAliasesInDomain;->sam:Ljcifs/dcerpc/msrpc/samr$SamrSamArray;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Ljcifs/dcerpc/msrpc/samr$SamrSamArray;->entries:[Ljcifs/dcerpc/msrpc/samr$SamrSamEntry;

    move-object/from16 v18, v0

    aget-object v7, v18, v6

    iget v0, v7, Ljcifs/dcerpc/msrpc/samr$SamrSamEntry;->idx:I

    move/from16 v18, v0

    move/from16 v0, v18

    move/from16 v1, p2

    invoke-static {v10, v4, v5, v0, v1}, Ljcifs/smb/SID;->getGroupMemberSids0(Ljcifs/dcerpc/DcerpcHandle;Ljcifs/dcerpc/msrpc/SamrDomainHandle;Ljcifs/smb/SID;II)[Ljcifs/smb/SID;

    move-result-object v12

    new-instance v8, Ljcifs/smb/SID;

    iget v0, v7, Ljcifs/dcerpc/msrpc/samr$SamrSamEntry;->idx:I

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-direct {v8, v5, v0}, Ljcifs/smb/SID;-><init>(Ljcifs/smb/SID;I)V

    const/16 v18, 0x4

    move/from16 v0, v18

    iput v0, v8, Ljcifs/smb/SID;->type:I

    invoke-virtual {v5}, Ljcifs/smb/SID;->getDomainName()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    iput-object v0, v8, Ljcifs/smb/SID;->domainName:Ljava/lang/String;

    new-instance v18, Ljcifs/dcerpc/UnicodeString;

    iget-object v0, v7, Ljcifs/dcerpc/msrpc/samr$SamrSamEntry;->name:Ljcifs/dcerpc/rpc$unicode_string;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v1, v2}, Ljcifs/dcerpc/UnicodeString;-><init>(Ljcifs/dcerpc/rpc$unicode_string;Z)V

    invoke-virtual/range {v18 .. v18}, Ljcifs/dcerpc/UnicodeString;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    iput-object v0, v8, Ljcifs/smb/SID;->acctName:Ljava/lang/String;

    const/4 v13, 0x0

    :goto_3
    array-length v0, v12

    move/from16 v18, v0

    move/from16 v0, v18

    if-ge v13, v0, :cond_6

    aget-object v18, v12, v13

    move-object/from16 v0, v18

    invoke-interface {v11, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/ArrayList;

    if-nez v9, :cond_4

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    aget-object v18, v12, v13

    move-object/from16 v0, v18

    invoke-interface {v11, v0, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_5

    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_5
    add-int/lit8 v13, v13, 0x1

    goto :goto_3

    :cond_6
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_2

    :cond_7
    if-eqz v10, :cond_a

    if-eqz v15, :cond_9

    if-eqz v4, :cond_8

    :try_start_5
    invoke-virtual {v4}, Ljcifs/dcerpc/msrpc/SamrDomainHandle;->close()V

    :cond_8
    invoke-virtual {v15}, Ljcifs/dcerpc/msrpc/SamrPolicyHandle;->close()V

    :cond_9
    invoke-virtual {v10}, Ljcifs/dcerpc/DcerpcHandle;->close()V

    :cond_a
    monitor-exit v19
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    return-object v11

    :catchall_2
    move-exception v18

    move-object v3, v4

    move-object v14, v15

    goto/16 :goto_1

    :catchall_3
    move-exception v18

    goto/16 :goto_0

    :catchall_4
    move-exception v18

    move-object v14, v15

    goto/16 :goto_0
.end method

.method public static getServerSid(Ljava/lang/String;Ljcifs/smb/NtlmPasswordAuthentication;)Ljcifs/smb/SID;
    .locals 12
    .param p0    # Ljava/lang/String;
    .param p1    # Ljcifs/smb/NtlmPasswordAuthentication;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v6, 0x0

    const/4 v8, 0x0

    new-instance v7, Ljcifs/dcerpc/msrpc/lsarpc$LsarDomainInfo;

    invoke-direct {v7}, Ljcifs/dcerpc/msrpc/lsarpc$LsarDomainInfo;-><init>()V

    sget-object v11, Ljcifs/smb/SID;->sid_cache:Ljava/util/Map;

    monitor-enter v11

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ncacn_np:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "[\\PIPE\\lsarpc]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Ljcifs/dcerpc/DcerpcHandle;->getHandle(Ljava/lang/String;Ljcifs/smb/NtlmPasswordAuthentication;)Ljcifs/dcerpc/DcerpcHandle;

    move-result-object v6

    new-instance v9, Ljcifs/dcerpc/msrpc/LsaPolicyHandle;

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {v9, v6, v0, v1}, Ljcifs/dcerpc/msrpc/LsaPolicyHandle;-><init>(Ljcifs/dcerpc/DcerpcHandle;Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    :try_start_1
    new-instance v10, Ljcifs/dcerpc/msrpc/MsrpcQueryInformationPolicy;

    const/4 v0, 0x5

    invoke-direct {v10, v9, v0, v7}, Ljcifs/dcerpc/msrpc/MsrpcQueryInformationPolicy;-><init>(Ljcifs/dcerpc/msrpc/LsaPolicyHandle;SLjcifs/dcerpc/ndr/NdrObject;)V

    invoke-virtual {v6, v10}, Ljcifs/dcerpc/DcerpcHandle;->sendrecv(Ljcifs/dcerpc/DcerpcMessage;)V

    iget v0, v10, Ljcifs/dcerpc/msrpc/MsrpcQueryInformationPolicy;->retval:I

    if-eqz v0, :cond_2

    new-instance v0, Ljcifs/smb/SmbException;

    iget v1, v10, Ljcifs/dcerpc/msrpc/MsrpcQueryInformationPolicy;->retval:I

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ljcifs/smb/SmbException;-><init>(IZ)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    move-object v8, v9

    :goto_0
    if-eqz v6, :cond_1

    if-eqz v8, :cond_0

    :try_start_2
    invoke-virtual {v8}, Ljcifs/dcerpc/msrpc/LsaPolicyHandle;->close()V

    :cond_0
    invoke-virtual {v6}, Ljcifs/dcerpc/DcerpcHandle;->close()V

    :cond_1
    throw v0

    :catchall_1
    move-exception v0

    :goto_1
    monitor-exit v11
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_2
    :try_start_3
    new-instance v0, Ljcifs/smb/SID;

    iget-object v1, v7, Ljcifs/dcerpc/msrpc/lsarpc$LsarDomainInfo;->sid:Ljcifs/dcerpc/rpc$sid_t;

    const/4 v2, 0x3

    new-instance v3, Ljcifs/dcerpc/UnicodeString;

    iget-object v4, v7, Ljcifs/dcerpc/msrpc/lsarpc$LsarDomainInfo;->name:Ljcifs/dcerpc/rpc$unicode_string;

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Ljcifs/dcerpc/UnicodeString;-><init>(Ljcifs/dcerpc/rpc$unicode_string;Z)V

    invoke-virtual {v3}, Ljcifs/dcerpc/UnicodeString;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Ljcifs/smb/SID;-><init>(Ljcifs/dcerpc/rpc$sid_t;ILjava/lang/String;Ljava/lang/String;Z)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-eqz v6, :cond_4

    if-eqz v9, :cond_3

    :try_start_4
    invoke-virtual {v9}, Ljcifs/dcerpc/msrpc/LsaPolicyHandle;->close()V

    :cond_3
    invoke-virtual {v6}, Ljcifs/dcerpc/DcerpcHandle;->close()V

    :cond_4
    monitor-exit v11
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    return-object v0

    :catchall_2
    move-exception v0

    move-object v8, v9

    goto :goto_1

    :catchall_3
    move-exception v0

    goto :goto_0
.end method

.method public static resolveSids(Ljava/lang/String;Ljcifs/smb/NtlmPasswordAuthentication;[Ljcifs/smb/SID;)V
    .locals 8
    .param p0    # Ljava/lang/String;
    .param p1    # Ljcifs/smb/NtlmPasswordAuthentication;
    .param p2    # [Ljcifs/smb/SID;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v1, Ljava/util/ArrayList;

    array-length v4, p2

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    sget-object v5, Ljcifs/smb/SID;->sid_cache:Ljava/util/Map;

    monitor-enter v5

    const/4 v2, 0x0

    :goto_0
    :try_start_0
    array-length v4, p2

    if-ge v2, v4, :cond_1

    sget-object v4, Ljcifs/smb/SID;->sid_cache:Ljava/util/Map;

    aget-object v6, p2, v2

    invoke-interface {v4, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljcifs/smb/SID;

    if-eqz v3, :cond_0

    aget-object v4, p2, v2

    iget v6, v3, Ljcifs/smb/SID;->type:I

    iput v6, v4, Ljcifs/smb/SID;->type:I

    aget-object v4, p2, v2

    iget-object v6, v3, Ljcifs/smb/SID;->domainName:Ljava/lang/String;

    iput-object v6, v4, Ljcifs/smb/SID;->domainName:Ljava/lang/String;

    aget-object v4, p2, v2

    iget-object v6, v3, Ljcifs/smb/SID;->acctName:Ljava/lang/String;

    iput-object v6, v4, Ljcifs/smb/SID;->acctName:Ljava/lang/String;

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    aget-object v4, p2, v2

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    :cond_1
    :try_start_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_2

    const/4 v4, 0x0

    new-array v4, v4, [Ljcifs/smb/SID;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljcifs/smb/SID;

    move-object v0, v4

    check-cast v0, [Ljcifs/smb/SID;

    move-object p2, v0

    invoke-static {p0, p1, p2}, Ljcifs/smb/SID;->resolveSids0(Ljava/lang/String;Ljcifs/smb/NtlmPasswordAuthentication;[Ljcifs/smb/SID;)V

    const/4 v2, 0x0

    :goto_2
    array-length v4, p2

    if-ge v2, v4, :cond_2

    sget-object v4, Ljcifs/smb/SID;->sid_cache:Ljava/util/Map;

    aget-object v6, p2, v2

    aget-object v7, p2, v2

    invoke-interface {v4, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_2
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public static resolveSids(Ljava/lang/String;Ljcifs/smb/NtlmPasswordAuthentication;[Ljcifs/smb/SID;II)V
    .locals 8
    .param p0    # Ljava/lang/String;
    .param p1    # Ljcifs/smb/NtlmPasswordAuthentication;
    .param p2    # [Ljcifs/smb/SID;
    .param p3    # I
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    new-instance v1, Ljava/util/ArrayList;

    array-length v4, p2

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    sget-object v5, Ljcifs/smb/SID;->sid_cache:Ljava/util/Map;

    monitor-enter v5

    const/4 v2, 0x0

    :goto_0
    if-ge v2, p4, :cond_1

    :try_start_0
    sget-object v4, Ljcifs/smb/SID;->sid_cache:Ljava/util/Map;

    add-int v6, p3, v2

    aget-object v6, p2, v6

    invoke-interface {v4, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljcifs/smb/SID;

    if-eqz v3, :cond_0

    add-int v4, p3, v2

    aget-object v4, p2, v4

    iget v6, v3, Ljcifs/smb/SID;->type:I

    iput v6, v4, Ljcifs/smb/SID;->type:I

    add-int v4, p3, v2

    aget-object v4, p2, v4

    iget-object v6, v3, Ljcifs/smb/SID;->domainName:Ljava/lang/String;

    iput-object v6, v4, Ljcifs/smb/SID;->domainName:Ljava/lang/String;

    add-int v4, p3, v2

    aget-object v4, p2, v4

    iget-object v6, v3, Ljcifs/smb/SID;->acctName:Ljava/lang/String;

    iput-object v6, v4, Ljcifs/smb/SID;->acctName:Ljava/lang/String;

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    add-int v4, p3, v2

    aget-object v4, p2, v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    :cond_1
    :try_start_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_2

    const/4 v4, 0x0

    new-array v4, v4, [Ljcifs/smb/SID;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljcifs/smb/SID;

    move-object v0, v4

    check-cast v0, [Ljcifs/smb/SID;

    move-object p2, v0

    invoke-static {p0, p1, p2}, Ljcifs/smb/SID;->resolveSids0(Ljava/lang/String;Ljcifs/smb/NtlmPasswordAuthentication;[Ljcifs/smb/SID;)V

    const/4 v2, 0x0

    :goto_2
    array-length v4, p2

    if-ge v2, v4, :cond_2

    sget-object v4, Ljcifs/smb/SID;->sid_cache:Ljava/util/Map;

    aget-object v6, p2, v2

    aget-object v7, p2, v2

    invoke-interface {v4, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_2
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method static resolveSids(Ljcifs/dcerpc/DcerpcHandle;Ljcifs/dcerpc/msrpc/LsaPolicyHandle;[Ljcifs/smb/SID;)V
    .locals 9
    .param p0    # Ljcifs/dcerpc/DcerpcHandle;
    .param p1    # Ljcifs/dcerpc/msrpc/LsaPolicyHandle;
    .param p2    # [Ljcifs/smb/SID;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v8, 0x0

    const/4 v7, 0x0

    new-instance v0, Ljcifs/dcerpc/msrpc/MsrpcLookupSids;

    invoke-direct {v0, p1, p2}, Ljcifs/dcerpc/msrpc/MsrpcLookupSids;-><init>(Ljcifs/dcerpc/msrpc/LsaPolicyHandle;[Ljcifs/smb/SID;)V

    invoke-virtual {p0, v0}, Ljcifs/dcerpc/DcerpcHandle;->sendrecv(Ljcifs/dcerpc/DcerpcMessage;)V

    iget v4, v0, Ljcifs/dcerpc/msrpc/MsrpcLookupSids;->retval:I

    sparse-switch v4, :sswitch_data_0

    new-instance v4, Ljcifs/smb/SmbException;

    iget v5, v0, Ljcifs/dcerpc/msrpc/MsrpcLookupSids;->retval:I

    invoke-direct {v4, v5, v7}, Ljcifs/smb/SmbException;-><init>(IZ)V

    throw v4

    :sswitch_0
    const/4 v1, 0x0

    :goto_0
    array-length v4, p2

    if-ge v1, v4, :cond_0

    aget-object v4, p2, v1

    iget-object v5, v0, Ljcifs/dcerpc/msrpc/MsrpcLookupSids;->names:Ljcifs/dcerpc/msrpc/lsarpc$LsarTransNameArray;

    iget-object v5, v5, Ljcifs/dcerpc/msrpc/lsarpc$LsarTransNameArray;->names:[Ljcifs/dcerpc/msrpc/lsarpc$LsarTranslatedName;

    aget-object v5, v5, v1

    iget-short v5, v5, Ljcifs/dcerpc/msrpc/lsarpc$LsarTranslatedName;->sid_type:S

    iput v5, v4, Ljcifs/smb/SID;->type:I

    aget-object v4, p2, v1

    iput-object v8, v4, Ljcifs/smb/SID;->domainName:Ljava/lang/String;

    aget-object v4, p2, v1

    iget v4, v4, Ljcifs/smb/SID;->type:I

    packed-switch v4, :pswitch_data_0

    :goto_1
    aget-object v4, p2, v1

    new-instance v5, Ljcifs/dcerpc/UnicodeString;

    iget-object v6, v0, Ljcifs/dcerpc/msrpc/MsrpcLookupSids;->names:Ljcifs/dcerpc/msrpc/lsarpc$LsarTransNameArray;

    iget-object v6, v6, Ljcifs/dcerpc/msrpc/lsarpc$LsarTransNameArray;->names:[Ljcifs/dcerpc/msrpc/lsarpc$LsarTranslatedName;

    aget-object v6, v6, v1

    iget-object v6, v6, Ljcifs/dcerpc/msrpc/lsarpc$LsarTranslatedName;->name:Ljcifs/dcerpc/rpc$unicode_string;

    invoke-direct {v5, v6, v7}, Ljcifs/dcerpc/UnicodeString;-><init>(Ljcifs/dcerpc/rpc$unicode_string;Z)V

    invoke-virtual {v5}, Ljcifs/dcerpc/UnicodeString;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Ljcifs/smb/SID;->acctName:Ljava/lang/String;

    aget-object v4, p2, v1

    iput-object v8, v4, Ljcifs/smb/SID;->origin_server:Ljava/lang/String;

    aget-object v4, p2, v1

    iput-object v8, v4, Ljcifs/smb/SID;->origin_auth:Ljcifs/smb/NtlmPasswordAuthentication;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :pswitch_0
    iget-object v4, v0, Ljcifs/dcerpc/msrpc/MsrpcLookupSids;->names:Ljcifs/dcerpc/msrpc/lsarpc$LsarTransNameArray;

    iget-object v4, v4, Ljcifs/dcerpc/msrpc/lsarpc$LsarTransNameArray;->names:[Ljcifs/dcerpc/msrpc/lsarpc$LsarTranslatedName;

    aget-object v4, v4, v1

    iget v2, v4, Ljcifs/dcerpc/msrpc/lsarpc$LsarTranslatedName;->sid_index:I

    iget-object v4, v0, Ljcifs/dcerpc/msrpc/MsrpcLookupSids;->domains:Ljcifs/dcerpc/msrpc/lsarpc$LsarRefDomainList;

    iget-object v4, v4, Ljcifs/dcerpc/msrpc/lsarpc$LsarRefDomainList;->domains:[Ljcifs/dcerpc/msrpc/lsarpc$LsarTrustInformation;

    aget-object v4, v4, v2

    iget-object v3, v4, Ljcifs/dcerpc/msrpc/lsarpc$LsarTrustInformation;->name:Ljcifs/dcerpc/rpc$unicode_string;

    aget-object v4, p2, v1

    new-instance v5, Ljcifs/dcerpc/UnicodeString;

    invoke-direct {v5, v3, v7}, Ljcifs/dcerpc/UnicodeString;-><init>(Ljcifs/dcerpc/rpc$unicode_string;Z)V

    invoke-virtual {v5}, Ljcifs/dcerpc/UnicodeString;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Ljcifs/smb/SID;->domainName:Ljava/lang/String;

    goto :goto_1

    :cond_0
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x3fffff8d -> :sswitch_0
        0x0 -> :sswitch_0
        0x107 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method static resolveSids0(Ljava/lang/String;Ljcifs/smb/NtlmPasswordAuthentication;[Ljcifs/smb/SID;)V
    .locals 8
    .param p0    # Ljava/lang/String;
    .param p1    # Ljcifs/smb/NtlmPasswordAuthentication;
    .param p2    # [Ljcifs/smb/SID;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x0

    const/4 v2, 0x0

    sget-object v6, Ljcifs/smb/SID;->sid_cache:Ljava/util/Map;

    monitor-enter v6

    :try_start_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "ncacn_np:"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "[\\PIPE\\lsarpc]"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, p1}, Ljcifs/dcerpc/DcerpcHandle;->getHandle(Ljava/lang/String;Ljcifs/smb/NtlmPasswordAuthentication;)Ljcifs/dcerpc/DcerpcHandle;

    move-result-object v1

    move-object v4, p0

    const/16 v5, 0x2e

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Ljava/lang/Character;->isDigit(C)Z

    move-result v5

    if-nez v5, :cond_0

    const/4 v5, 0x0

    invoke-virtual {v4, v5, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    :cond_0
    new-instance v3, Ljcifs/dcerpc/msrpc/LsaPolicyHandle;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "\\\\"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/16 v7, 0x800

    invoke-direct {v3, v1, v5, v7}, Ljcifs/dcerpc/msrpc/LsaPolicyHandle;-><init>(Ljcifs/dcerpc/DcerpcHandle;Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-static {v1, v3, p2}, Ljcifs/smb/SID;->resolveSids(Ljcifs/dcerpc/DcerpcHandle;Ljcifs/dcerpc/msrpc/LsaPolicyHandle;[Ljcifs/smb/SID;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    if-eqz v1, :cond_2

    if-eqz v3, :cond_1

    :try_start_2
    invoke-virtual {v3}, Ljcifs/dcerpc/msrpc/LsaPolicyHandle;->close()V

    :cond_1
    invoke-virtual {v1}, Ljcifs/dcerpc/DcerpcHandle;->close()V

    :cond_2
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    return-void

    :catchall_0
    move-exception v5

    :goto_0
    if-eqz v1, :cond_4

    if-eqz v2, :cond_3

    :try_start_3
    invoke-virtual {v2}, Ljcifs/dcerpc/msrpc/LsaPolicyHandle;->close()V

    :cond_3
    invoke-virtual {v1}, Ljcifs/dcerpc/DcerpcHandle;->close()V

    :cond_4
    throw v5

    :catchall_1
    move-exception v5

    :goto_1
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v5

    :catchall_2
    move-exception v5

    move-object v2, v3

    goto :goto_1

    :catchall_3
    move-exception v5

    move-object v2, v3

    goto :goto_0
.end method

.method public static toByteArray(Ljcifs/dcerpc/rpc$sid_t;)[B
    .locals 7
    .param p0    # Ljcifs/dcerpc/rpc$sid_t;

    iget-byte v4, p0, Ljcifs/dcerpc/rpc$sid_t;->sub_authority_count:B

    mul-int/lit8 v4, v4, 0x4

    add-int/lit8 v4, v4, 0x8

    new-array v2, v4, [B

    const/4 v0, 0x0

    add-int/lit8 v1, v0, 0x1

    iget-byte v4, p0, Ljcifs/dcerpc/rpc$sid_t;->revision:B

    aput-byte v4, v2, v0

    add-int/lit8 v0, v1, 0x1

    iget-byte v4, p0, Ljcifs/dcerpc/rpc$sid_t;->sub_authority_count:B

    aput-byte v4, v2, v1

    iget-object v4, p0, Ljcifs/dcerpc/rpc$sid_t;->identifier_authority:[B

    const/4 v5, 0x0

    const/4 v6, 0x6

    invoke-static {v4, v5, v2, v0, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/lit8 v0, v0, 0x6

    const/4 v3, 0x0

    :goto_0
    iget-byte v4, p0, Ljcifs/dcerpc/rpc$sid_t;->sub_authority_count:B

    if-ge v3, v4, :cond_0

    iget-object v4, p0, Ljcifs/dcerpc/rpc$sid_t;->sub_authority:[I

    aget v4, v4, v3

    invoke-static {v4, v2, v0}, Ljcifs/util/Encdec;->enc_uint32le(I[BI)I

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-object v2
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1    # Ljava/lang/Object;

    const/4 v3, 0x1

    const/4 v4, 0x0

    instance-of v5, p1, Ljcifs/smb/SID;

    if-eqz v5, :cond_0

    move-object v2, p1

    check-cast v2, Ljcifs/smb/SID;

    if-ne v2, p0, :cond_1

    move v4, v3

    :cond_0
    :goto_0
    return v4

    :cond_1
    iget-byte v5, v2, Ljcifs/smb/SID;->sub_authority_count:B

    iget-byte v6, p0, Ljcifs/smb/SID;->sub_authority_count:B

    if-ne v5, v6, :cond_0

    iget-byte v0, p0, Ljcifs/smb/SID;->sub_authority_count:B

    move v1, v0

    :goto_1
    add-int/lit8 v0, v1, -0x1

    if-lez v1, :cond_2

    iget-object v5, v2, Ljcifs/smb/SID;->sub_authority:[I

    aget v5, v5, v0

    iget-object v6, p0, Ljcifs/smb/SID;->sub_authority:[I

    aget v6, v6, v0

    if-ne v5, v6, :cond_0

    move v1, v0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_2
    const/4 v5, 0x6

    if-ge v0, v5, :cond_3

    iget-object v5, v2, Ljcifs/smb/SID;->identifier_authority:[B

    aget-byte v5, v5, v0

    iget-object v6, p0, Ljcifs/smb/SID;->identifier_authority:[B

    aget-byte v6, v6, v0

    if-ne v5, v6, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-byte v5, v2, Ljcifs/smb/SID;->revision:B

    iget-byte v6, p0, Ljcifs/smb/SID;->revision:B

    if-ne v5, v6, :cond_4

    :goto_3
    move v4, v3

    goto :goto_0

    :cond_4
    move v3, v4

    goto :goto_3
.end method

.method public getAccountName()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Ljcifs/smb/SID;->origin_server:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljcifs/smb/SID;->resolveWeak()V

    :cond_0
    iget v0, p0, Ljcifs/smb/SID;->type:I

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Ljcifs/smb/SID;->sub_authority:[I

    iget-byte v2, p0, Ljcifs/smb/SID;->sub_authority_count:B

    add-int/lit8 v2, v2, -0x1

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    iget v0, p0, Ljcifs/smb/SID;->type:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    const-string v0, ""

    goto :goto_0

    :cond_2
    iget-object v0, p0, Ljcifs/smb/SID;->acctName:Ljava/lang/String;

    goto :goto_0
.end method

.method public getDomainName()Ljava/lang/String;
    .locals 4

    iget-object v1, p0, Ljcifs/smb/SID;->origin_server:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Ljcifs/smb/SID;->resolveWeak()V

    :cond_0
    iget v1, p0, Ljcifs/smb/SID;->type:I

    const/16 v2, 0x8

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Ljcifs/smb/SID;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {p0}, Ljcifs/smb/SID;->getAccountName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_1
    iget-object v1, p0, Ljcifs/smb/SID;->domainName:Ljava/lang/String;

    goto :goto_0
.end method

.method public getDomainSid()Ljcifs/smb/SID;
    .locals 6

    const/4 v2, 0x3

    new-instance v0, Ljcifs/smb/SID;

    iget-object v3, p0, Ljcifs/smb/SID;->domainName:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {p0}, Ljcifs/smb/SID;->getType()I

    move-result v1

    if-eq v1, v2, :cond_0

    const/4 v5, 0x1

    :goto_0
    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ljcifs/smb/SID;-><init>(Ljcifs/dcerpc/rpc$sid_t;ILjava/lang/String;Ljava/lang/String;Z)V

    return-object v0

    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public getGroupMemberSids(Ljava/lang/String;Ljcifs/smb/NtlmPasswordAuthentication;I)[Ljcifs/smb/SID;
    .locals 9
    .param p1    # Ljava/lang/String;
    .param p2    # Ljcifs/smb/NtlmPasswordAuthentication;
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    iget v6, p0, Ljcifs/smb/SID;->type:I

    const/4 v7, 0x2

    if-eq v6, v7, :cond_0

    iget v6, p0, Ljcifs/smb/SID;->type:I

    const/4 v7, 0x4

    if-eq v6, v7, :cond_0

    const/4 v6, 0x0

    new-array v6, v6, [Ljcifs/smb/SID;

    :goto_0
    return-object v6

    :cond_0
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v0, 0x0

    invoke-virtual {p0}, Ljcifs/smb/SID;->getDomainSid()Ljcifs/smb/SID;

    move-result-object v2

    sget-object v7, Ljcifs/smb/SID;->sid_cache:Ljava/util/Map;

    monitor-enter v7

    :try_start_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ncacn_np:"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "[\\PIPE\\samr]"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, p2}, Ljcifs/dcerpc/DcerpcHandle;->getHandle(Ljava/lang/String;Ljcifs/smb/NtlmPasswordAuthentication;)Ljcifs/dcerpc/DcerpcHandle;

    move-result-object v3

    new-instance v5, Ljcifs/dcerpc/msrpc/SamrPolicyHandle;

    const/16 v6, 0x30

    invoke-direct {v5, v3, p1, v6}, Ljcifs/dcerpc/msrpc/SamrPolicyHandle;-><init>(Ljcifs/dcerpc/DcerpcHandle;Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    new-instance v1, Ljcifs/dcerpc/msrpc/SamrDomainHandle;

    const/16 v6, 0x200

    invoke-direct {v1, v3, v5, v6, v2}, Ljcifs/dcerpc/msrpc/SamrDomainHandle;-><init>(Ljcifs/dcerpc/DcerpcHandle;Ljcifs/dcerpc/msrpc/SamrPolicyHandle;ILjcifs/dcerpc/rpc$sid_t;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    :try_start_2
    invoke-virtual {p0}, Ljcifs/smb/SID;->getRid()I

    move-result v6

    invoke-static {v3, v1, v2, v6, p3}, Ljcifs/smb/SID;->getGroupMemberSids0(Ljcifs/dcerpc/DcerpcHandle;Ljcifs/dcerpc/msrpc/SamrDomainHandle;Ljcifs/smb/SID;II)[Ljcifs/smb/SID;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    move-result-object v6

    if-eqz v3, :cond_3

    if-eqz v5, :cond_2

    if-eqz v1, :cond_1

    :try_start_3
    invoke-virtual {v1}, Ljcifs/dcerpc/msrpc/SamrDomainHandle;->close()V

    :cond_1
    invoke-virtual {v5}, Ljcifs/dcerpc/msrpc/SamrPolicyHandle;->close()V

    :cond_2
    invoke-virtual {v3}, Ljcifs/dcerpc/DcerpcHandle;->close()V

    :cond_3
    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v6

    move-object v0, v1

    move-object v4, v5

    :goto_1
    :try_start_4
    monitor-exit v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v6

    :catchall_1
    move-exception v6

    :goto_2
    if-eqz v3, :cond_6

    if-eqz v4, :cond_5

    if-eqz v0, :cond_4

    :try_start_5
    invoke-virtual {v0}, Ljcifs/dcerpc/msrpc/SamrDomainHandle;->close()V

    :cond_4
    invoke-virtual {v4}, Ljcifs/dcerpc/msrpc/SamrPolicyHandle;->close()V

    :cond_5
    invoke-virtual {v3}, Ljcifs/dcerpc/DcerpcHandle;->close()V

    :cond_6
    throw v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :catchall_2
    move-exception v6

    goto :goto_1

    :catchall_3
    move-exception v6

    move-object v4, v5

    goto :goto_2

    :catchall_4
    move-exception v6

    move-object v0, v1

    move-object v4, v5

    goto :goto_2
.end method

.method public getRid()I
    .locals 2

    invoke-virtual {p0}, Ljcifs/smb/SID;->getType()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "This SID is a domain sid"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Ljcifs/smb/SID;->sub_authority:[I

    iget-byte v1, p0, Ljcifs/smb/SID;->sub_authority_count:B

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    return v0
.end method

.method public getType()I
    .locals 1

    iget-object v0, p0, Ljcifs/smb/SID;->origin_server:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljcifs/smb/SID;->resolveWeak()V

    :cond_0
    iget v0, p0, Ljcifs/smb/SID;->type:I

    return v0
.end method

.method public getTypeText()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Ljcifs/smb/SID;->origin_server:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljcifs/smb/SID;->resolveWeak()V

    :cond_0
    sget-object v0, Ljcifs/smb/SID;->SID_TYPE_NAMES:[Ljava/lang/String;

    iget v1, p0, Ljcifs/smb/SID;->type:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public hashCode()I
    .locals 4

    iget-object v2, p0, Ljcifs/smb/SID;->identifier_authority:[B

    const/4 v3, 0x5

    aget-byte v0, v2, v3

    const/4 v1, 0x0

    :goto_0
    iget-byte v2, p0, Ljcifs/smb/SID;->sub_authority_count:B

    if-ge v1, v2, :cond_0

    const v2, 0x1003f

    iget-object v3, p0, Ljcifs/smb/SID;->sub_authority:[I

    aget v3, v3, v1

    mul-int/2addr v2, v3

    add-int/2addr v0, v2

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return v0
.end method

.method public resolve(Ljava/lang/String;Ljcifs/smb/NtlmPasswordAuthentication;)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljcifs/smb/NtlmPasswordAuthentication;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v1, 0x1

    new-array v0, v1, [Ljcifs/smb/SID;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    invoke-static {p1, p2, v0}, Ljcifs/smb/SID;->resolveSids(Ljava/lang/String;Ljcifs/smb/NtlmPasswordAuthentication;[Ljcifs/smb/SID;)V

    return-void
.end method

.method resolveWeak()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Ljcifs/smb/SID;->origin_server:Ljava/lang/String;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Ljcifs/smb/SID;->origin_server:Ljava/lang/String;

    iget-object v1, p0, Ljcifs/smb/SID;->origin_auth:Ljcifs/smb/NtlmPasswordAuthentication;

    invoke-virtual {p0, v0, v1}, Ljcifs/smb/SID;->resolve(Ljava/lang/String;Ljcifs/smb/NtlmPasswordAuthentication;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-object v2, p0, Ljcifs/smb/SID;->origin_server:Ljava/lang/String;

    :goto_0
    iput-object v2, p0, Ljcifs/smb/SID;->origin_auth:Ljcifs/smb/NtlmPasswordAuthentication;

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    iput-object v2, p0, Ljcifs/smb/SID;->origin_server:Ljava/lang/String;

    iput-object v2, p0, Ljcifs/smb/SID;->origin_auth:Ljcifs/smb/NtlmPasswordAuthentication;

    throw v0

    :catch_0
    move-exception v0

    iput-object v2, p0, Ljcifs/smb/SID;->origin_server:Ljava/lang/String;

    goto :goto_0
.end method

.method public toDisplayString()Ljava/lang/String;
    .locals 3

    iget-object v1, p0, Ljcifs/smb/SID;->origin_server:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Ljcifs/smb/SID;->resolveWeak()V

    :cond_0
    iget-object v1, p0, Ljcifs/smb/SID;->domainName:Ljava/lang/String;

    if-eqz v1, :cond_5

    iget v1, p0, Ljcifs/smb/SID;->type:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    iget-object v0, p0, Ljcifs/smb/SID;->domainName:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_1
    iget v1, p0, Ljcifs/smb/SID;->type:I

    const/4 v2, 0x5

    if-eq v1, v2, :cond_2

    iget-object v1, p0, Ljcifs/smb/SID;->domainName:Ljava/lang/String;

    const-string v2, "BUILTIN"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_2
    iget v1, p0, Ljcifs/smb/SID;->type:I

    const/16 v2, 0x8

    if-ne v1, v2, :cond_3

    invoke-virtual {p0}, Ljcifs/smb/SID;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Ljcifs/smb/SID;->acctName:Ljava/lang/String;

    goto :goto_0

    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Ljcifs/smb/SID;->domainName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\\"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Ljcifs/smb/SID;->acctName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Ljcifs/smb/SID;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 11

    const/4 v10, 0x1

    const/4 v9, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "S-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-byte v7, p0, Ljcifs/smb/SID;->revision:B

    and-int/lit16 v7, v7, 0xff

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v6, p0, Ljcifs/smb/SID;->identifier_authority:[B

    aget-byte v6, v6, v9

    if-nez v6, :cond_0

    iget-object v6, p0, Ljcifs/smb/SID;->identifier_authority:[B

    aget-byte v6, v6, v10

    if-eqz v6, :cond_1

    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "0x"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Ljcifs/smb/SID;->identifier_authority:[B

    const/4 v8, 0x6

    invoke-static {v7, v9, v8}, Ljcifs/util/Hexdump;->toHexString([BII)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_0
    const/4 v0, 0x0

    :goto_1
    iget-byte v6, p0, Ljcifs/smb/SID;->sub_authority_count:B

    if-ge v0, v6, :cond_3

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "-"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Ljcifs/smb/SID;->sub_authority:[I

    aget v7, v7, v0

    int-to-long v7, v7

    const-wide v9, 0xffffffffL

    and-long/2addr v7, v9

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    const-wide/16 v4, 0x0

    const-wide/16 v1, 0x0

    const/4 v0, 0x5

    :goto_2
    if-le v0, v10, :cond_2

    iget-object v6, p0, Ljcifs/smb/SID;->identifier_authority:[B

    aget-byte v6, v6, v0

    int-to-long v6, v6

    const-wide/16 v8, 0xff

    and-long/2addr v6, v8

    long-to-int v8, v4

    shl-long/2addr v6, v8

    add-long/2addr v1, v6

    const-wide/16 v6, 0x8

    add-long/2addr v4, v6

    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    :cond_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_3
    return-object v3
.end method
