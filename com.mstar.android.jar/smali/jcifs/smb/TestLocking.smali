.class public Ljcifs/smb/TestLocking;
.super Ljava/lang/Object;
.source "TestLocking.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field delay:J

.field ltime:J

.field numComplete:I

.field numIter:I

.field numThreads:I

.field url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Ljcifs/smb/TestLocking;->numThreads:I

    iput v0, p0, Ljcifs/smb/TestLocking;->numIter:I

    const-wide/16 v0, 0x64

    iput-wide v0, p0, Ljcifs/smb/TestLocking;->delay:J

    const/4 v0, 0x0

    iput-object v0, p0, Ljcifs/smb/TestLocking;->url:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Ljcifs/smb/TestLocking;->numComplete:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljcifs/smb/TestLocking;->ltime:J

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 14
    .param p0    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    array-length v10, p0

    const/4 v11, 0x1

    if-ge v10, v11, :cond_0

    sget-object v10, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v11, "usage: TestLocking [-t <numThreads>] [-i <numIter>] [-d <delay>] url"

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v10, 0x1

    invoke-static {v10}, Ljava/lang/System;->exit(I)V

    :cond_0
    new-instance v7, Ljcifs/smb/TestLocking;

    invoke-direct {v7}, Ljcifs/smb/TestLocking;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    iput-wide v10, v7, Ljcifs/smb/TestLocking;->ltime:J

    const/4 v0, 0x0

    :goto_0
    array-length v10, p0

    if-ge v0, v10, :cond_4

    aget-object v10, p0, v0

    const-string v11, "-t"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    add-int/lit8 v0, v0, 0x1

    aget-object v10, p0, v0

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    iput v10, v7, Ljcifs/smb/TestLocking;->numThreads:I

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    aget-object v10, p0, v0

    const-string v11, "-i"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    add-int/lit8 v0, v0, 0x1

    aget-object v10, p0, v0

    invoke-static {v10}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    iput v10, v7, Ljcifs/smb/TestLocking;->numIter:I

    goto :goto_1

    :cond_2
    aget-object v10, p0, v0

    const-string v11, "-d"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    add-int/lit8 v0, v0, 0x1

    aget-object v10, p0, v0

    invoke-static {v10}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    iput-wide v10, v7, Ljcifs/smb/TestLocking;->delay:J

    goto :goto_1

    :cond_3
    aget-object v10, p0, v0

    iput-object v10, v7, Ljcifs/smb/TestLocking;->url:Ljava/lang/String;

    goto :goto_1

    :cond_4
    iget v10, v7, Ljcifs/smb/TestLocking;->numThreads:I

    new-array v8, v10, [Ljava/lang/Thread;

    const/4 v9, 0x0

    :goto_2
    iget v10, v7, Ljcifs/smb/TestLocking;->numThreads:I

    if-ge v9, v10, :cond_5

    new-instance v10, Ljava/lang/Thread;

    invoke-direct {v10, v7}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    aput-object v10, v8, v9

    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    aget-object v11, v8, v9

    invoke-virtual {v11}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    aget-object v10, v8, v9

    invoke-virtual {v10}, Ljava/lang/Thread;->start()V

    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    :cond_5
    :goto_3
    iget v10, v7, Ljcifs/smb/TestLocking;->numComplete:I

    iget v11, v7, Ljcifs/smb/TestLocking;->numThreads:I

    if-ge v10, v11, :cond_9

    :cond_6
    const-wide/16 v3, 0x2

    monitor-enter v7

    :try_start_0
    iget-wide v10, v7, Ljcifs/smb/TestLocking;->ltime:J

    iget-wide v12, v7, Ljcifs/smb/TestLocking;->delay:J

    add-long v5, v10, v12

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    cmp-long v10, v5, v1

    if-lez v10, :cond_7

    sub-long v3, v5, v1

    :cond_7
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const-wide/16 v10, 0x2

    cmp-long v10, v3, v10

    if-lez v10, :cond_8

    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "delay="

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_8
    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V

    const-wide/16 v10, 0x2

    cmp-long v10, v3, v10

    if-gtz v10, :cond_6

    monitor-enter v7

    :try_start_1
    invoke-virtual {v7}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v7

    goto :goto_3

    :catchall_0
    move-exception v10

    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v10

    :catchall_1
    move-exception v10

    :try_start_2
    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v10

    :cond_9
    const/4 v9, 0x0

    :goto_4
    iget v10, v7, Ljcifs/smb/TestLocking;->numThreads:I

    if-ge v9, v10, :cond_a

    aget-object v10, v8, v9

    invoke-virtual {v10}, Ljava/lang/Thread;->join()V

    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    aget-object v11, v8, v9

    invoke-virtual {v11}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    :cond_a
    sget-object v10, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v10}, Ljava/io/PrintStream;->println()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    :try_start_0
    new-instance v3, Ljcifs/smb/SmbFile;

    iget-object v9, p0, Ljcifs/smb/TestLocking;->url:Ljava/lang/String;

    invoke-direct {v3, v9}, Ljcifs/smb/SmbFile;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljcifs/smb/SmbFile;

    invoke-virtual {v3}, Ljcifs/smb/SmbFile;->getParent()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v1, v9}, Ljcifs/smb/SmbFile;-><init>(Ljava/lang/String;)V

    const/16 v9, 0x400

    new-array v0, v9, [B

    const/4 v4, 0x0

    :goto_0
    iget v9, p0, Ljcifs/smb/TestLocking;->numIter:I

    if-ge v4, v9, :cond_4

    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    iput-wide v9, p0, Ljcifs/smb/TestLocking;->ltime:J

    invoke-virtual {p0}, Ljava/lang/Object;->wait()V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v7

    const-wide v9, 0x3fd54fdf3b645a1dL    # 0.333

    cmpg-double v9, v7, v9

    if-gez v9, :cond_1

    invoke-virtual {v3}, Ljcifs/smb/SmbFile;->exists()Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :cond_0
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :catchall_0
    move-exception v9

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v9
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catch_0
    move-exception v2

    :try_start_5
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    iget v9, p0, Ljcifs/smb/TestLocking;->numComplete:I

    :goto_2
    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Ljcifs/smb/TestLocking;->numComplete:I

    return-void

    :cond_1
    const-wide v9, 0x3fe55810624dd2f2L    # 0.667

    cmpg-double v9, v7, v9

    if-gez v9, :cond_2

    :try_start_6
    invoke-virtual {v1}, Ljcifs/smb/SmbFile;->listFiles()[Ljcifs/smb/SmbFile;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_1

    :catch_1
    move-exception v6

    :try_start_7
    sget-object v9, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {v6}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v9

    iget v10, p0, Ljcifs/smb/TestLocking;->numComplete:I

    add-int/lit8 v10, v10, 0x1

    iput v10, p0, Ljcifs/smb/TestLocking;->numComplete:I

    throw v9

    :cond_2
    const-wide/high16 v9, 0x3ff0000000000000L    # 1.0

    cmpg-double v9, v7, v9

    if-gez v9, :cond_0

    :try_start_8
    invoke-virtual {v3}, Ljcifs/smb/SmbFile;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    :cond_3
    invoke-virtual {v5, v0}, Ljava/io/InputStream;->read([B)I

    move-result v9

    if-gtz v9, :cond_3

    invoke-virtual {v5}, Ljava/io/InputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_4
    iget v9, p0, Ljcifs/smb/TestLocking;->numComplete:I

    goto :goto_2
.end method
