.class public final enum Lcom/mstar/android/MDisplay$PanelMode;
.super Ljava/lang/Enum;
.source "MDisplay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/MDisplay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PanelMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/MDisplay$PanelMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/MDisplay$PanelMode;

.field public static final enum E_PANELMODE_4K1K_FP:Lcom/mstar/android/MDisplay$PanelMode;

.field public static final enum E_PANELMODE_4K2K_15HZ:Lcom/mstar/android/MDisplay$PanelMode;

.field public static final enum E_PANELMODE_NONE:Lcom/mstar/android/MDisplay$PanelMode;

.field public static final enum E_PANELMODE_NORMAL:Lcom/mstar/android/MDisplay$PanelMode;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/mstar/android/MDisplay$PanelMode;

    const-string v1, "E_PANELMODE_NONE"

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/MDisplay$PanelMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/MDisplay$PanelMode;->E_PANELMODE_NONE:Lcom/mstar/android/MDisplay$PanelMode;

    new-instance v0, Lcom/mstar/android/MDisplay$PanelMode;

    const-string v1, "E_PANELMODE_NORMAL"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/MDisplay$PanelMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/MDisplay$PanelMode;->E_PANELMODE_NORMAL:Lcom/mstar/android/MDisplay$PanelMode;

    new-instance v0, Lcom/mstar/android/MDisplay$PanelMode;

    const-string v1, "E_PANELMODE_4K1K_FP"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/MDisplay$PanelMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/MDisplay$PanelMode;->E_PANELMODE_4K1K_FP:Lcom/mstar/android/MDisplay$PanelMode;

    new-instance v0, Lcom/mstar/android/MDisplay$PanelMode;

    const-string v1, "E_PANELMODE_4K2K_15HZ"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/MDisplay$PanelMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/MDisplay$PanelMode;->E_PANELMODE_4K2K_15HZ:Lcom/mstar/android/MDisplay$PanelMode;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/mstar/android/MDisplay$PanelMode;

    sget-object v1, Lcom/mstar/android/MDisplay$PanelMode;->E_PANELMODE_NONE:Lcom/mstar/android/MDisplay$PanelMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mstar/android/MDisplay$PanelMode;->E_PANELMODE_NORMAL:Lcom/mstar/android/MDisplay$PanelMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/MDisplay$PanelMode;->E_PANELMODE_4K1K_FP:Lcom/mstar/android/MDisplay$PanelMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/MDisplay$PanelMode;->E_PANELMODE_4K2K_15HZ:Lcom/mstar/android/MDisplay$PanelMode;

    aput-object v1, v0, v5

    sput-object v0, Lcom/mstar/android/MDisplay$PanelMode;->$VALUES:[Lcom/mstar/android/MDisplay$PanelMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/MDisplay$PanelMode;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/MDisplay$PanelMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/MDisplay$PanelMode;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/MDisplay$PanelMode;
    .locals 1

    sget-object v0, Lcom/mstar/android/MDisplay$PanelMode;->$VALUES:[Lcom/mstar/android/MDisplay$PanelMode;

    invoke-virtual {v0}, [Lcom/mstar/android/MDisplay$PanelMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/MDisplay$PanelMode;

    return-object v0
.end method
