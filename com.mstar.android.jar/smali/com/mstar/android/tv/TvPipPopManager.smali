.class public Lcom/mstar/android/tv/TvPipPopManager;
.super Ljava/lang/Object;
.source "TvPipPopManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TvPipPopManager"

.field static mInstance:Lcom/mstar/android/tv/TvPipPopManager;

.field private static mService:Lcom/mstar/android/tv/ITvPipPop;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/mstar/android/tv/TvPipPopManager;->mInstance:Lcom/mstar/android/tv/TvPipPopManager;

    sput-object v0, Lcom/mstar/android/tv/TvPipPopManager;->mService:Lcom/mstar/android/tv/ITvPipPop;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/mstar/android/tv/TvPipPopManager;
    .locals 2

    sget-object v0, Lcom/mstar/android/tv/TvPipPopManager;->mInstance:Lcom/mstar/android/tv/TvPipPopManager;

    if-nez v0, :cond_1

    const-class v1, Lcom/mstar/android/tv/TvPipPopManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mstar/android/tv/TvPipPopManager;->mInstance:Lcom/mstar/android/tv/TvPipPopManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mstar/android/tv/TvPipPopManager;

    invoke-direct {v0}, Lcom/mstar/android/tv/TvPipPopManager;-><init>()V

    sput-object v0, Lcom/mstar/android/tv/TvPipPopManager;->mInstance:Lcom/mstar/android/tv/TvPipPopManager;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Lcom/mstar/android/tv/TvPipPopManager;->mInstance:Lcom/mstar/android/tv/TvPipPopManager;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static getService()Lcom/mstar/android/tv/ITvPipPop;
    .locals 1

    sget-object v0, Lcom/mstar/android/tv/TvPipPopManager;->mService:Lcom/mstar/android/tv/ITvPipPop;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/mstar/android/tv/TvPipPopManager;->mService:Lcom/mstar/android/tv/ITvPipPop;

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tv/TvManager;->getInstance()Lcom/mstar/android/tv/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvManager;->getTvPipPop()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v0

    sput-object v0, Lcom/mstar/android/tv/TvPipPopManager;->mService:Lcom/mstar/android/tv/ITvPipPop;

    sget-object v0, Lcom/mstar/android/tv/TvPipPopManager;->mService:Lcom/mstar/android/tv/ITvPipPop;

    goto :goto_0
.end method


# virtual methods
.method public checkPipSupport(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .param p2    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getService()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v2

    invoke-virtual {p2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v3

    invoke-interface {v1, v2, v3}, Lcom/mstar/android/tv/ITvPipPop;->checkPipSupport(II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public checkPipSupportOnSubSrc(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getService()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/mstar/android/tv/ITvPipPop;->checkPipSupportOnSubSrc(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public checkPopSupport(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .param p2    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getService()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v2

    invoke-virtual {p2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v3

    invoke-interface {v1, v2, v3}, Lcom/mstar/android/tv/ITvPipPop;->checkPopSupport(II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public checkTravelingModeSupport(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .param p2    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getService()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v2

    invoke-virtual {p2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v3

    invoke-interface {v1, v2, v3}, Lcom/mstar/android/tv/ITvPipPop;->checkTravelingModeSupport(II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public disable3dDualView()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getService()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvPipPop;->disable3dDualView()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public disablePip()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getService()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvPipPop;->disablePip()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public disablePop()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getService()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvPipPop;->disablePop()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public disableTravelingMode()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getService()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvPipPop;->disableTravelingMode()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public enable3dDualView(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/VideoWindowType;Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)Z
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .param p2    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .param p3    # Lcom/mstar/android/tvapi/common/vo/VideoWindowType;
    .param p4    # Lcom/mstar/android/tvapi/common/vo/VideoWindowType;

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getService()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v2

    invoke-virtual {p2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v3

    invoke-interface {v1, v2, v3, p3, p4}, Lcom/mstar/android/tv/ITvPipPop;->enable3dDualView(IILcom/mstar/android/tvapi/common/vo/VideoWindowType;Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public enablePipMM(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .param p2    # Lcom/mstar/android/tvapi/common/vo/VideoWindowType;

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getService()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->values()[Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    move-result-object v2

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v3

    invoke-interface {v1, v3, p2}, Lcom/mstar/android/tv/ITvPipPop;->enablePipMM(ILcom/mstar/android/tvapi/common/vo/VideoWindowType;)I

    move-result v3

    aget-object v2, v2, v3
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public enablePipTV(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .param p2    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .param p3    # Lcom/mstar/android/tvapi/common/vo/VideoWindowType;

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getService()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->values()[Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    move-result-object v2

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v3

    invoke-virtual {p2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v4

    invoke-interface {v1, v3, v4, p3}, Lcom/mstar/android/tv/ITvPipPop;->enablePipTV(IILcom/mstar/android/tvapi/common/vo/VideoWindowType;)I

    move-result v3

    aget-object v2, v2, v3
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public enablePopMM(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getService()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->values()[Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    move-result-object v2

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v3

    invoke-interface {v1, v3}, Lcom/mstar/android/tv/ITvPipPop;->enablePopMM(I)I

    move-result v3

    aget-object v2, v2, v3
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public enablePopTV(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .param p2    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getService()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->values()[Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    move-result-object v2

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v3

    invoke-virtual {p2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v4

    invoke-interface {v1, v3, v4}, Lcom/mstar/android/tv/ITvPipPop;->enablePopTV(II)I

    move-result v3

    aget-object v2, v2, v3
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public enableTravelingModeMM(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getService()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->values()[Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    move-result-object v2

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v3

    invoke-interface {v1, v3}, Lcom/mstar/android/tv/ITvPipPop;->enableTravelingModeMM(I)I

    move-result v3

    aget-object v2, v2, v3
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public enableTravelingModeTV(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .param p2    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getService()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;->values()[Lcom/mstar/android/tvapi/common/vo/EnumPipReturn;

    move-result-object v2

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v3

    invoke-virtual {p2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v4

    invoke-interface {v1, v3, v4}, Lcom/mstar/android/tv/ITvPipPop;->enableTravelingModeTV(II)I

    move-result v3

    aget-object v2, v2, v3
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getDtvRoute()I
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getService()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvPipPop;->getDtvRoute()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, -0x1

    goto :goto_0
.end method

.method public getIsDualViewOn()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getService()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvPipPop;->getIsDualViewOn()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getIsPipOn()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getService()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvPipPop;->getIsPipOn()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getIsPopOn()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getService()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvPipPop;->getIsPopOn()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getMainWindowSourceList()[I
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getService()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvPipPop;->getMainWindowSourceList()[I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getPipMode()Lcom/mstar/android/tvapi/common/vo/EnumPipModes;
    .locals 4

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getService()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumPipModes;->values()[Lcom/mstar/android/tvapi/common/vo/EnumPipModes;

    move-result-object v2

    invoke-interface {v1}, Lcom/mstar/android/tv/ITvPipPop;->getPipMode()I

    move-result v3

    aget-object v2, v2, v3
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getSubInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .locals 4

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getService()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    invoke-interface {v1}, Lcom/mstar/android/tv/ITvPipPop;->getSubInputSource()I

    move-result v3

    aget-object v2, v2, v3
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_NONE:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    goto :goto_0
.end method

.method public getSubWindowSourceList(Z)[I
    .locals 3
    .param p1    # Z

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getService()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvPipPop;->getSubWindowSourceList(Z)[I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isDualViewEnabled()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getService()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvPipPop;->isDualViewEnabled()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isPipEnabled()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getService()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvPipPop;->isPipEnabled()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isPipModeEnabled()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getService()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvPipPop;->isPipModeEnabled()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isPopEnabled()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getService()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvPipPop;->isPopEnabled()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setDtvRoute(I)Z
    .locals 3
    .param p1    # I

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getService()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvPipPop;->setDtvRoute(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setDualViewOnFlag(Z)Z
    .locals 3
    .param p1    # Z

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getService()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvPipPop;->setDualViewOnFlag(Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v2, 0x1

    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setPipDisplayFocusWindow(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)V
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getService()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->ordinal()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/mstar/android/tv/ITvPipPop;->setPipDisplayFocusWindow(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setPipOnFlag(Z)Z
    .locals 3
    .param p1    # Z

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getService()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvPipPop;->setPipOnFlag(Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public setPipSubwindow(Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)Z
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/VideoWindowType;

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getService()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvPipPop;->setPipSubwindow(Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setPopOnFlag(Z)Z
    .locals 3
    .param p1    # Z

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getService()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvPipPop;->setPopOnFlag(Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v2, 0x1

    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
