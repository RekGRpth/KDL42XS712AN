.class public Lcom/mstar/android/tv/TvChannelManager;
.super Ljava/lang/Object;
.source "TvChannelManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/tv/TvChannelManager$1;,
        Lcom/mstar/android/tv/TvChannelManager$Client3;,
        Lcom/mstar/android/tv/TvChannelManager$Client2;,
        Lcom/mstar/android/tv/TvChannelManager$Client1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "TvChannelManager"

.field static mInstance:Lcom/mstar/android/tv/TvChannelManager;

.field private static mService:Lcom/mstar/android/tv/ITvChannel;


# instance fields
.field private atvClient:Lcom/mstar/android/tv/TvChannelManager$Client2;

.field private atvListeners:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;",
            ">;"
        }
    .end annotation
.end field

.field private dtvClient:Lcom/mstar/android/tv/TvChannelManager$Client3;

.field private dtvListeners:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;",
            ">;"
        }
    .end annotation
.end field

.field private tvClient:Lcom/mstar/android/tv/TvChannelManager$Client1;

.field private tvListeners:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/mstar/android/tv/TvChannelManager;->mInstance:Lcom/mstar/android/tv/TvChannelManager;

    sput-object v0, Lcom/mstar/android/tv/TvChannelManager;->mService:Lcom/mstar/android/tv/ITvChannel;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mstar/android/tv/TvChannelManager;->tvListeners:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mstar/android/tv/TvChannelManager;->atvListeners:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mstar/android/tv/TvChannelManager;->dtvListeners:Ljava/util/HashMap;

    return-void
.end method

.method static synthetic access$000(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;
    .locals 1
    .param p0    # Lcom/mstar/android/tv/TvChannelManager;

    iget-object v0, p0, Lcom/mstar/android/tv/TvChannelManager;->tvListeners:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$100(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;
    .locals 1
    .param p0    # Lcom/mstar/android/tv/TvChannelManager;

    iget-object v0, p0, Lcom/mstar/android/tv/TvChannelManager;->atvListeners:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$200(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;
    .locals 1
    .param p0    # Lcom/mstar/android/tv/TvChannelManager;

    iget-object v0, p0, Lcom/mstar/android/tv/TvChannelManager;->dtvListeners:Ljava/util/HashMap;

    return-object v0
.end method

.method public static getInstance()Lcom/mstar/android/tv/TvChannelManager;
    .locals 2

    sget-object v0, Lcom/mstar/android/tv/TvChannelManager;->mInstance:Lcom/mstar/android/tv/TvChannelManager;

    if-nez v0, :cond_1

    const-class v1, Lcom/mstar/android/tv/TvChannelManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mstar/android/tv/TvChannelManager;->mInstance:Lcom/mstar/android/tv/TvChannelManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mstar/android/tv/TvChannelManager;

    invoke-direct {v0}, Lcom/mstar/android/tv/TvChannelManager;-><init>()V

    sput-object v0, Lcom/mstar/android/tv/TvChannelManager;->mInstance:Lcom/mstar/android/tv/TvChannelManager;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Lcom/mstar/android/tv/TvChannelManager;->mInstance:Lcom/mstar/android/tv/TvChannelManager;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static getService()Lcom/mstar/android/tv/ITvChannel;
    .locals 1

    sget-object v0, Lcom/mstar/android/tv/TvChannelManager;->mService:Lcom/mstar/android/tv/ITvChannel;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/mstar/android/tv/TvChannelManager;->mService:Lcom/mstar/android/tv/ITvChannel;

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tv/TvManager;->getInstance()Lcom/mstar/android/tv/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvManager;->getTvChannel()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v0

    sput-object v0, Lcom/mstar/android/tv/TvChannelManager;->mService:Lcom/mstar/android/tv/ITvChannel;

    sget-object v0, Lcom/mstar/android/tv/TvChannelManager;->mService:Lcom/mstar/android/tv/ITvChannel;

    goto :goto_0
.end method


# virtual methods
.method public addProgramToFavorite(Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;III)V
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const-string v2, "TvChannelManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "addProgramToFavorite(), paras favoriteId = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", programNo = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", programType = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", programId = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;->getValue()I

    move-result v2

    invoke-interface {v1, v2, p2, p3, p4}, Lcom/mstar/android/tv/ITvChannel;->addProgramToFavorite(IIII)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public changeToFirstService(Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;
    .param p2    # Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;

    const-string v2, "TvChannelManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "changeToFirstService(), paras enInputType = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", enServiceType = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceInputType;->ordinal()I

    move-result v2

    invoke-virtual {p2}, Lcom/mstar/android/tvapi/common/vo/EnumFirstServiceType;->ordinal()I

    move-result v3

    invoke-interface {v1, v2, v3}, Lcom/mstar/android/tv/ITvChannel;->changeToFirstService(II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public closeSubtitle()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvChannel;->closeSubtitle()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public deleteProgramFromFavorite(Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;III)V
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const-string v2, "TvChannelManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "deleteProgramFromFavorite(), paras favoriteId = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", programNo = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", programType = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", programId = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumFavoriteId;->getValue()I

    move-result v2

    invoke-interface {v1, v2, p2, p3, p4}, Lcom/mstar/android/tv/ITvChannel;->deleteProgramFromFavorite(IIII)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAtvCurrentFrequency()I
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvChannel;->getAtvCurrentFrequency()I

    move-result v1

    const-string v3, "TvChannelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAtvCurrentFrequency(), return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAtvProgramInfo(Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramInfo;I)I
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramInfo;
    .param p2    # I

    const-string v2, "TvChannelManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getAtvProgramInfo(), paras Cmd = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", u16Program = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramInfo;->ordinal()I

    move-result v2

    invoke-interface {v1, v2, p2}, Lcom/mstar/android/tv/ITvChannel;->getAtvProgramInfo(II)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, -0x1

    goto :goto_0
.end method

.method public getAtvSoundSystem()Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v2

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->values()[Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;

    move-result-object v3

    invoke-interface {v2}, Lcom/mstar/android/tv/ITvChannel;->getAtvSoundSystem()I

    move-result v4

    invoke-static {v4}, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->getOrdinalThroughValue(I)I

    move-result v4

    aget-object v1, v3, v4

    const-string v3, "TvChannelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAtvSoundSystem() , return EnumAtvSystemStandard "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAtvStationName(I)Ljava/lang/String;
    .locals 6
    .param p1    # I

    const-string v3, "TvChannelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAtvStationName(), paras programNo = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v2

    const/4 v1, 0x0

    :try_start_0
    invoke-interface {v2, p1}, Lcom/mstar/android/tv/ITvChannel;->getAtvStationName(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "TvChannelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAtvStationName(), return String "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAtvVideoSystem()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v2

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    move-result-object v3

    invoke-interface {v2}, Lcom/mstar/android/tv/ITvChannel;->getAtvVideoSystem()I

    move-result v4

    aget-object v1, v3, v4

    const-string v3, "TvChannelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAtvVideoSystem(), return EnumAvdVideoStandardType "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getChannelSwitchMode()Lcom/mstar/android/tvapi/common/vo/EnumChannelSwitchMode;
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v2

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumChannelSwitchMode;->values()[Lcom/mstar/android/tvapi/common/vo/EnumChannelSwitchMode;

    move-result-object v3

    invoke-interface {v2}, Lcom/mstar/android/tv/ITvChannel;->getChannelSwitchMode()I

    move-result v4

    aget-object v1, v3, v4

    const-string v3, "TvChannelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getChannelSwitchMode(), return EnumChannelSwitchMode "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getCurrentChannelNumber()I
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvChannel;->getCurrentChannelNumber()I

    move-result v1

    const-string v3, "TvChannelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getCurrentChannelNumber(), return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getCurrentLanguageIndex(Ljava/lang/String;)Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;
    .locals 6
    .param p1    # Ljava/lang/String;

    const-string v3, "TvChannelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getCurrentLanguageIndex(), paras languageCode = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v2

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumLanguage;

    move-result-object v3

    invoke-interface {v2, p1}, Lcom/mstar/android/tv/ITvChannel;->getCurrentLanguageIndex(Ljava/lang/String;)I

    move-result v4

    aget-object v1, v3, v4

    const-string v3, "TvChannelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getCurrentLanguageIndex(), return EnumLanguage "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getProgramAttribute(Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;III)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const-string v2, "TvChannelManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getProgramAttribute(), paras enpa = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", programNo = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", programType = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", programId = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;->ordinal()I

    move-result v2

    invoke-interface {v1, v2, p2, p3, p4}, Lcom/mstar/android/tv/ITvChannel;->getProgramAttribute(IIII)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getProgramCount(Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;)I
    .locals 6
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;

    const-string v3, "TvChannelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getProgramCount(),paras programCountType = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumProgramCountType;->ordinal()I

    move-result v3

    invoke-interface {v2, v3}, Lcom/mstar/android/tv/ITvChannel;->getProgramCount(I)I

    move-result v1

    const-string v3, "TvChannelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getProgramCount(), return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getProgramCtrl(Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;II)I
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;
    .param p2    # I
    .param p3    # I

    const-string v2, "TvChannelManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getProgramCtrl(), paras Cmd = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", u16Param2 = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", u16Param3 = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/atv/vo/EnumGetProgramCtrl;->ordinal()I

    move-result v2

    invoke-interface {v1, v2, p2, p3}, Lcom/mstar/android/tv/ITvChannel;->getProgramCtrl(III)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, -0x1

    goto :goto_0
.end method

.method public getProgramInfo(Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;Lcom/mstar/android/tvapi/common/vo/EnumProgramInfoType;)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;
    .locals 6
    .param p1    # Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;
    .param p2    # Lcom/mstar/android/tvapi/common/vo/EnumProgramInfoType;

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v2

    const-string v3, "TvChannelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getProgramInfo, paras ProgramInfoQueryCriteria is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;->queryIndex:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " EnumProgramInfoType is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p2}, Lcom/mstar/android/tvapi/common/vo/EnumProgramInfoType;->ordinal()I

    move-result v3

    invoke-interface {v2, p1, v3}, Lcom/mstar/android/tv/ITvChannel;->getProgramInfo(Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;I)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getProgramName(ILcom/mstar/android/tvapi/common/vo/EnumServiceType;I)Ljava/lang/String;
    .locals 6
    .param p1    # I
    .param p2    # Lcom/mstar/android/tvapi/common/vo/EnumServiceType;
    .param p3    # I

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v2

    const-string v3, "TvChannelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getProgramName, paras progNo is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " progType is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " programId is "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p2}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v3

    invoke-interface {v2, p1, v3, p3}, Lcom/mstar/android/tv/ITvChannel;->getProgramName(III)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getRfInfo(Lcom/mstar/android/tvapi/dtv/vo/RfInfo$EnumInfoType;I)Lcom/mstar/android/tvapi/dtv/vo/RfInfo;
    .locals 6
    .param p1    # Lcom/mstar/android/tvapi/dtv/vo/RfInfo$EnumInfoType;
    .param p2    # I

    const-string v3, "TvChannelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getRfInfo(), paras rfSignalInfoType = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", rfChNo = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v2

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/dtv/vo/RfInfo$EnumInfoType;->ordinal()I

    move-result v3

    invoke-interface {v2, v3, p2}, Lcom/mstar/android/tv/ITvChannel;->getRfInfo(II)Lcom/mstar/android/tvapi/dtv/vo/RfInfo;

    move-result-object v1

    const-string v3, "TvChannelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getRfInfo, return RfInfo frequency = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v1, Lcom/mstar/android/tvapi/dtv/vo/RfInfo;->frequency:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", isVHF = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, v1, Lcom/mstar/android/tvapi/dtv/vo/RfInfo;->isVHF:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", rfName = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v1, Lcom/mstar/android/tvapi/dtv/vo/RfInfo;->rfName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", rfPhyNum = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-short v5, v1, Lcom/mstar/android/tvapi/dtv/vo/RfInfo;->rfPhyNum:S

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v3, v1

    :goto_0
    return-object v3

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getSIFMtsMode()Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v2

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    move-result-object v3

    invoke-interface {v2}, Lcom/mstar/android/tv/ITvChannel;->getSIFMtsMode()I

    move-result v4

    aget-object v1, v3, v4

    const-string v3, "TvChannelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getSIFMtsMode() , return EnumAtvAudioModeType "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getSubtitleInfo()Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    const/4 v2, 0x0

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvChannel;->getSubtitleInfo()Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;

    move-result-object v2

    const-string v3, "TvChannelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getSubtitleInfo(), return DtvSubtitleInfo currentSubtitleIndex = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-short v5, v2, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->currentSubtitleIndex:S

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", subtitleServiceNumber = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-short v5, v2, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->subtitleServiceNumber:S

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", subtitleOn = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, v2, Lcom/mstar/android/tvapi/dtv/vo/DtvSubtitleInfo;->subtitleOn:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getSystemCountry()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v2

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    move-result-object v3

    invoke-interface {v2}, Lcom/mstar/android/tv/ITvChannel;->getSystemCountry()I

    move-result v4

    aget-object v1, v3, v4

    const-string v3, "TvChannelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getSystemCountry() , return EnumCountry "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getVideoStandard()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v2

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    move-result-object v3

    invoke-interface {v2}, Lcom/mstar/android/tv/ITvChannel;->getVideoStandard()I

    move-result v4

    aget-object v1, v3, v4

    const-string v3, "TvChannelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " getVideoStandard(), return EnumAvdVideoStandardType "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public isSignalStabled()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvChannel;->isSignalStabled()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public openSubtitle(I)Z
    .locals 5
    .param p1    # I

    const-string v2, "TvChannelManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "openSubtitle(), paras index = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvChannel;->openSubtitle(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public pauseAtvAutoTuning()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvChannel;->pauseAtvAutoTuning()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public pauseDtvScan()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvChannel;->pauseDtvScan()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public playDtvCurrentProgram()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvChannel;->playDtvCurrentProgram()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public programDown()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvChannel;->programDown()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public programUp()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvChannel;->programUp()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public registerOnAtvPlayerEventListener(Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/mstar/android/tv/TvChannelManager;->atvClient:Lcom/mstar/android/tv/TvChannelManager$Client2;

    if-nez v1, :cond_0

    new-instance v1, Lcom/mstar/android/tv/TvChannelManager$Client2;

    invoke-direct {v1, p0, v4}, Lcom/mstar/android/tv/TvChannelManager$Client2;-><init>(Lcom/mstar/android/tv/TvChannelManager;Lcom/mstar/android/tv/TvChannelManager$1;)V

    iput-object v1, p0, Lcom/mstar/android/tv/TvChannelManager;->atvClient:Lcom/mstar/android/tv/TvChannelManager$Client2;

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tv/TvManager;->getInstance()Lcom/mstar/android/tv/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tv/TvManager;->getTvCommon()Lcom/mstar/android/tv/ITvCommon;

    move-result-object v1

    const-string v2, "DeskAtvPlayerEventListener"

    iget-object v3, p0, Lcom/mstar/android/tv/TvChannelManager;->atvClient:Lcom/mstar/android/tv/TvChannelManager$Client2;

    invoke-interface {v1, v2, v3}, Lcom/mstar/android/tv/ITvCommon;->addClient(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/mstar/android/tv/TvChannelManager;->atvListeners:Ljava/util/HashMap;

    invoke-virtual {v1, v4, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public registerOnDtvPlayerEventListener(Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/mstar/android/tv/TvChannelManager;->dtvClient:Lcom/mstar/android/tv/TvChannelManager$Client3;

    if-nez v1, :cond_0

    new-instance v1, Lcom/mstar/android/tv/TvChannelManager$Client3;

    invoke-direct {v1, p0, v4}, Lcom/mstar/android/tv/TvChannelManager$Client3;-><init>(Lcom/mstar/android/tv/TvChannelManager;Lcom/mstar/android/tv/TvChannelManager$1;)V

    iput-object v1, p0, Lcom/mstar/android/tv/TvChannelManager;->dtvClient:Lcom/mstar/android/tv/TvChannelManager$Client3;

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tv/TvManager;->getInstance()Lcom/mstar/android/tv/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tv/TvManager;->getTvCommon()Lcom/mstar/android/tv/ITvCommon;

    move-result-object v1

    const-string v2, "DeskDtvPlayerEventListener"

    iget-object v3, p0, Lcom/mstar/android/tv/TvChannelManager;->dtvClient:Lcom/mstar/android/tv/TvChannelManager$Client3;

    invoke-interface {v1, v2, v3}, Lcom/mstar/android/tv/ITvCommon;->addClient(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/mstar/android/tv/TvChannelManager;->dtvListeners:Ljava/util/HashMap;

    invoke-virtual {v1, v4, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public registerOnTvPlayerEventListener(Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/mstar/android/tv/TvChannelManager;->tvClient:Lcom/mstar/android/tv/TvChannelManager$Client1;

    if-nez v1, :cond_0

    new-instance v1, Lcom/mstar/android/tv/TvChannelManager$Client1;

    invoke-direct {v1, p0, v4}, Lcom/mstar/android/tv/TvChannelManager$Client1;-><init>(Lcom/mstar/android/tv/TvChannelManager;Lcom/mstar/android/tv/TvChannelManager$1;)V

    iput-object v1, p0, Lcom/mstar/android/tv/TvChannelManager;->tvClient:Lcom/mstar/android/tv/TvChannelManager$Client1;

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tv/TvManager;->getInstance()Lcom/mstar/android/tv/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tv/TvManager;->getTvCommon()Lcom/mstar/android/tv/ITvCommon;

    move-result-object v1

    const-string v2, "DeskTvPlayerEventListener"

    iget-object v3, p0, Lcom/mstar/android/tv/TvChannelManager;->tvClient:Lcom/mstar/android/tv/TvChannelManager$Client1;

    invoke-interface {v1, v2, v3}, Lcom/mstar/android/tv/ITvCommon;->addClient(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/mstar/android/tv/TvChannelManager;->tvListeners:Ljava/util/HashMap;

    invoke-virtual {v1, v4, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public resumeAtvAutoTuning()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvChannel;->resumeAtvAutoTuning()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public resumeDtvScan()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvChannel;->resumeDtvScan()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setAtvChannel(I)I
    .locals 6
    .param p1    # I

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2, p1}, Lcom/mstar/android/tv/ITvChannel;->setAtvChannel(I)I

    move-result v1

    const-string v3, "TvChannelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setAtvChannel, return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAtvForceSoundSystem(Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;

    const-string v2, "TvChannelManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAtvForceSoundSystem(), paras eSoundSystem = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/AtvSystemStandard$EnumAtvSystemStandard;->ordinal()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/mstar/android/tv/ITvChannel;->setAtvForceSoundSystem(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setAtvForceVedioSystem(Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    const-string v2, "TvChannelManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAtvForceVedioSystem(), paras eVideoSystem = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/mstar/android/tv/ITvChannel;->setAtvForceVedioSystem(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setAtvProgramInfo(Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;II)I
    .locals 6
    .param p1    # Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;
    .param p2    # I
    .param p3    # I

    const-string v3, "TvChannelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setAtvProgramInfo(), paras Cmd = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Program = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Parma2 = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramInfo;->ordinal()I

    move-result v3

    invoke-interface {v2, v3, p2, p3}, Lcom/mstar/android/tv/ITvChannel;->setAtvProgramInfo(III)I

    move-result v1

    const-string v3, "TvChannelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setAtvProgramInfo(), return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setChannelChangeFreezeMode(Z)V
    .locals 5
    .param p1    # Z

    const-string v2, "TvChannelManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setChannelChangeFreezeMode(), paras freezeMode = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvChannel;->setChannelChangeFreezeMode(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setChannelSwitchMode(Lcom/mstar/android/tvapi/common/vo/EnumChannelSwitchMode;)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumChannelSwitchMode;

    const-string v2, "TvChannelManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setChannelSwitchMode(), paras eMode = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumChannelSwitchMode;->ordinal()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/mstar/android/tv/ITvChannel;->setChannelSwitchMode(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setDtvAntennaType(Lcom/mstar/android/tvapi/common/vo/EnumAntennaType;)V
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumAntennaType;

    const-string v2, "TvChannelManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setDtvAntennaType(), paras type = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumAntennaType;->ordinal()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/mstar/android/tv/ITvChannel;->setDtvAntennaType(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setDtvManualScanByFreq(I)Z
    .locals 5
    .param p1    # I

    const-string v2, "TvChannelManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setDtvManualScanByFreq(), paras FrequencyKHz = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvChannel;->setDtvManualScanByFreq(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setDtvManualScanByRF(I)Z
    .locals 5
    .param p1    # I

    const-string v2, "TvChannelManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setDtvManualScanByRF(), paras RFNum = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvChannel;->setDtvManualScanByRF(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setProgramAttribute(Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;III)V
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const-string v2, "TvChannelManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setProgramAttribute(), paras enpa = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", programNo = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", programType = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", programId = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumProgramAttribute;->ordinal()I

    move-result v2

    invoke-interface {v1, v2, p2, p3, p4}, Lcom/mstar/android/tv/ITvChannel;->setProgramAttribute(IIII)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setProgramCtrl(Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;II)I
    .locals 6
    .param p1    # Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;
    .param p2    # I
    .param p3    # I

    const-string v3, "TvChannelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setProgramCtrl(), paras Cmd = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", u16Param2 = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", u16Param3 = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/atv/vo/EnumSetProgramCtrl;->ordinal()I

    move-result v3

    invoke-interface {v2, v3, p2, p3}, Lcom/mstar/android/tv/ITvChannel;->setProgramCtrl(III)I

    move-result v1

    const-string v3, "TvChannelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setProgramCtrl(), return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setSystemCountry(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;)V
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;

    const-string v2, "TvChannelManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setSystemCountry(), paras memberCountry = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumCountry;->ordinal()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/mstar/android/tv/ITvChannel;->setSystemCountry(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public startAtvAutoTuning(III)Z
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const-string v2, "TvChannelManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startAtvAutoTuning(),  paras EventIntervalMs = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", FrequencyStart = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", FrequencyEnd = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1, p2, p3}, Lcom/mstar/android/tv/ITvChannel;->startAtvAutoTuning(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public startAtvManualTuning(IILcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;)Z
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;

    const-string v2, "TvChannelManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startAtvManualTuning(), paras EventIntervalMs = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Frequency = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", eMode = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    :try_start_0
    invoke-virtual {p3}, Lcom/mstar/android/tvapi/atv/vo/EnumAtvManualTuneMode;->ordinal()I

    move-result v2

    invoke-interface {v1, p1, p2, v2}, Lcom/mstar/android/tv/ITvChannel;->startAtvManualTuning(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public startDtvAutoScan()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvChannel;->startDtvAutoScan()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public startDtvFullScan()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvChannel;->startDtvFullScan()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public startDtvManualScan()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvChannel;->startDtvManualScan()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public stopAtvAutoTuning()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvChannel;->stopAtvAutoTuning()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public stopAtvManualTuning()V
    .locals 2

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvChannel;->stopAtvManualTuning()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public stopDtvScan()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvChannel;->stopDtvScan()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public switchAudioTrack(I)V
    .locals 5
    .param p1    # I

    const-string v2, "TvChannelManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "switchAudioTrack(), paras track = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvChannel;->switchAudioTrack(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public switchMSrvDtvRouteCmd(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvChannelManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "switchMSrvDtvRouteCmd(), paras dtvRoute = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvChannelManager;->getService()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvChannel;->switchMSrvDtvRouteCmd(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method
