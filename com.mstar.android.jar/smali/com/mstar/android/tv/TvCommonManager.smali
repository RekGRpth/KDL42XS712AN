.class public Lcom/mstar/android/tv/TvCommonManager;
.super Ljava/lang/Object;
.source "TvCommonManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/tv/TvCommonManager$1;,
        Lcom/mstar/android/tv/TvCommonManager$Client;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "TvCommonManager"

.field static mInstance:Lcom/mstar/android/tv/TvCommonManager;

.field private static mService:Lcom/mstar/android/tv/ITvCommon;


# instance fields
.field private tvClient:Lcom/mstar/android/tv/TvCommonManager$Client;

.field private tvListeners:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/mstar/android/tvapi/common/listener/OnTvEventListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/mstar/android/tv/TvCommonManager;->mInstance:Lcom/mstar/android/tv/TvCommonManager;

    sput-object v0, Lcom/mstar/android/tv/TvCommonManager;->mService:Lcom/mstar/android/tv/ITvCommon;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mstar/android/tv/TvCommonManager;->tvListeners:Ljava/util/HashMap;

    return-void
.end method

.method static synthetic access$000(Lcom/mstar/android/tv/TvCommonManager;)Ljava/util/HashMap;
    .locals 1
    .param p0    # Lcom/mstar/android/tv/TvCommonManager;

    iget-object v0, p0, Lcom/mstar/android/tv/TvCommonManager;->tvListeners:Ljava/util/HashMap;

    return-object v0
.end method

.method public static getInstance()Lcom/mstar/android/tv/TvCommonManager;
    .locals 2

    sget-object v0, Lcom/mstar/android/tv/TvCommonManager;->mInstance:Lcom/mstar/android/tv/TvCommonManager;

    if-nez v0, :cond_1

    const-class v1, Lcom/mstar/android/tv/TvCommonManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mstar/android/tv/TvCommonManager;->mInstance:Lcom/mstar/android/tv/TvCommonManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mstar/android/tv/TvCommonManager;

    invoke-direct {v0}, Lcom/mstar/android/tv/TvCommonManager;-><init>()V

    sput-object v0, Lcom/mstar/android/tv/TvCommonManager;->mInstance:Lcom/mstar/android/tv/TvCommonManager;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Lcom/mstar/android/tv/TvCommonManager;->mInstance:Lcom/mstar/android/tv/TvCommonManager;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static getService()Lcom/mstar/android/tv/ITvCommon;
    .locals 1

    sget-object v0, Lcom/mstar/android/tv/TvCommonManager;->mService:Lcom/mstar/android/tv/ITvCommon;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/mstar/android/tv/TvCommonManager;->mService:Lcom/mstar/android/tv/ITvCommon;

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tv/TvManager;->getInstance()Lcom/mstar/android/tv/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvManager;->getTvCommon()Lcom/mstar/android/tv/ITvCommon;

    move-result-object v0

    sput-object v0, Lcom/mstar/android/tv/TvCommonManager;->mService:Lcom/mstar/android/tv/ITvCommon;

    sget-object v0, Lcom/mstar/android/tv/TvCommonManager;->mService:Lcom/mstar/android/tv/ITvCommon;

    goto :goto_0
.end method


# virtual methods
.method public GetInputSourceStatus()[Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getService()Lcom/mstar/android/tv/ITvCommon;

    move-result-object v2

    const/4 v0, 0x0

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvCommon;->GetInputSourceStatus()[Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public OSD_Set3Dformat(I)V
    .locals 2
    .param p1    # I

    const-string v0, "mstar.desk-display-mode"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public closeSurfaceView()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getService()Lcom/mstar/android/tv/ITvCommon;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvCommon;->closeSurfaceView()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public enterSleepMode(ZZ)V
    .locals 5
    .param p1    # Z
    .param p2    # Z

    const-string v2, "TvCommonManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "enterSleepMode paras bMode = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", bNoSignalPwDn"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getService()Lcom/mstar/android/tv/ITvCommon;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1, p2}, Lcom/mstar/android/tv/ITvCommon;->enterSleepMode(ZZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAtvMtsMode()Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;
    .locals 4

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getService()Lcom/mstar/android/tv/ITvCommon;

    move-result-object v1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    move-result-object v2

    invoke-interface {v1}, Lcom/mstar/android/tv/ITvCommon;->getAtvMtsMode()I

    move-result v3

    aget-object v2, v2, v3
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getAtvSoundMode()Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;
    .locals 4

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getService()Lcom/mstar/android/tv/ITvCommon;

    move-result-object v1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    move-result-object v2

    invoke-interface {v1}, Lcom/mstar/android/tv/ITvCommon;->getAtvSoundMode()I

    move-result v3

    aget-object v2, v2, v3
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getService()Lcom/mstar/android/tv/ITvCommon;

    move-result-object v2

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    invoke-interface {v2}, Lcom/mstar/android/tv/ITvCommon;->getCurrentInputSource()I

    move-result v4

    aget-object v1, v3, v4

    const-string v3, "TvCommonManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getCurrentInputSource, return EnumInputSource "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getCurrentSubInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getService()Lcom/mstar/android/tv/ITvCommon;

    move-result-object v2

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    invoke-interface {v2}, Lcom/mstar/android/tv/ITvCommon;->getCurrentSubInputSource()I

    move-result v4

    aget-object v1, v3, v4

    const-string v3, "TvCommonManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getCurrentSubInputSource, return EnumInputSource "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getHPPortStatus()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getService()Lcom/mstar/android/tv/ITvCommon;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvCommon;->getHPPortStatus()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getPowerOnSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getService()Lcom/mstar/android/tv/ITvCommon;

    move-result-object v2

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    invoke-interface {v2}, Lcom/mstar/android/tv/ITvCommon;->getPowerOnSource()I

    move-result v4

    aget-object v1, v3, v4

    const-string v3, "TvCommonManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getPowerOnSource, return EnumInputSource "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getSourceList()[I
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getService()Lcom/mstar/android/tv/ITvCommon;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvCommon;->getSourceList()[I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public openSurfaceView(IIII)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getService()Lcom/mstar/android/tv/ITvCommon;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1, p2, p3, p4}, Lcom/mstar/android/tv/ITvCommon;->openSurfaceView(IIII)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public rebootSystem(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getService()Lcom/mstar/android/tv/ITvCommon;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvCommon;->rebootSystem(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public recoverySystem(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getService()Lcom/mstar/android/tv/ITvCommon;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvCommon;->recoverySystem(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public registerOnTvEventListener(Lcom/mstar/android/tvapi/common/listener/OnTvEventListener;)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/listener/OnTvEventListener;

    const/4 v4, 0x0

    iget-object v2, p0, Lcom/mstar/android/tv/TvCommonManager;->tvClient:Lcom/mstar/android/tv/TvCommonManager$Client;

    if-nez v2, :cond_0

    new-instance v2, Lcom/mstar/android/tv/TvCommonManager$Client;

    invoke-direct {v2, p0, v4}, Lcom/mstar/android/tv/TvCommonManager$Client;-><init>(Lcom/mstar/android/tv/TvCommonManager;Lcom/mstar/android/tv/TvCommonManager$1;)V

    iput-object v2, p0, Lcom/mstar/android/tv/TvCommonManager;->tvClient:Lcom/mstar/android/tv/TvCommonManager$Client;

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getService()Lcom/mstar/android/tv/ITvCommon;

    move-result-object v1

    const-string v2, "DeskTvEventListener"

    iget-object v3, p0, Lcom/mstar/android/tv/TvCommonManager;->tvClient:Lcom/mstar/android/tv/TvCommonManager$Client;

    invoke-interface {v1, v2, v3}, Lcom/mstar/android/tv/ITvCommon;->addClient(Ljava/lang/String;Landroid/os/IBinder;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/mstar/android/tv/TvCommonManager;->tvListeners:Ljava/util/HashMap;

    invoke-virtual {v2, v4, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v2, 0x1

    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAtvMtsMode(Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    .locals 4
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getService()Lcom/mstar/android/tv/ITvCommon;

    move-result-object v1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;->values()[Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    move-result-object v2

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumAtvAudioModeType;->ordinal()I

    move-result v3

    invoke-interface {v1, v3}, Lcom/mstar/android/tv/ITvCommon;->setAtvMtsMode(I)I

    move-result v3

    aget-object v2, v2, v3
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setHPPortStatus(Z)V
    .locals 2
    .param p1    # Z

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getService()Lcom/mstar/android/tv/ITvCommon;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvCommon;->setHPPortStatus(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    const-string v2, "TvCommonManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setInputSource, paras source = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getService()Lcom/mstar/android/tv/ITvCommon;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/mstar/android/tv/ITvCommon;->setInputSource(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setPowerOnSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    const-string v2, "TvCommonManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setPowerOnSource, paras eSource = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getService()Lcom/mstar/android/tv/ITvCommon;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/mstar/android/tv/ITvCommon;->setPowerOnSource(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setSurfaceView(IIII)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getService()Lcom/mstar/android/tv/ITvCommon;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1, p2, p3, p4}, Lcom/mstar/android/tv/ITvCommon;->setSurfaceView(IIII)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public standbySystem(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getService()Lcom/mstar/android/tv/ITvCommon;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvCommon;->standbySystem(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
