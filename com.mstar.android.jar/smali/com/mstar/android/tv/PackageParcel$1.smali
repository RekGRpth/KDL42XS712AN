.class final Lcom/mstar/android/tv/PackageParcel$1;
.super Ljava/lang/Object;
.source "PackageParcel.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tv/PackageParcel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/mstar/android/tv/PackageParcel;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/mstar/android/tv/PackageParcel;
    .locals 6
    .param p1    # Landroid/os/Parcel;

    new-instance v0, Lcom/mstar/android/tv/PackageParcel;

    invoke-direct {v0}, Lcom/mstar/android/tv/PackageParcel;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "tempSize ==="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    new-array v1, v2, [B

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readByteArray([B)V

    invoke-virtual {v0, v1}, Lcom/mstar/android/tv/PackageParcel;->setPackageArray([B)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-virtual {p0, p1}, Lcom/mstar/android/tv/PackageParcel$1;->createFromParcel(Landroid/os/Parcel;)Lcom/mstar/android/tv/PackageParcel;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/mstar/android/tv/PackageParcel;
    .locals 1
    .param p1    # I

    new-array v0, p1, [Lcom/mstar/android/tv/PackageParcel;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-virtual {p0, p1}, Lcom/mstar/android/tv/PackageParcel$1;->newArray(I)[Lcom/mstar/android/tv/PackageParcel;

    move-result-object v0

    return-object v0
.end method
