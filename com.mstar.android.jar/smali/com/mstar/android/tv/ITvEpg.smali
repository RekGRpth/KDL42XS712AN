.class public interface abstract Lcom/mstar/android/tv/ITvEpg;
.super Ljava/lang/Object;
.source "ITvEpg.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/tv/ITvEpg$Stub;
    }
.end annotation


# virtual methods
.method public abstract addingEpgPriority(Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscProgramInfo;I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract beginToGetEventInformation(IIII)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract endToGetEventInformation()V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getEventCount(IIIIJ)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getEventExtendInfoByTime(IIIIJ)Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getEventInfoByTime(IIIIJ)Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getFirstEventInformation(J)Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract getNextEventInformation()Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscEpgEventInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract resetEPGProgPriority()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
