.class public Lcom/mstar/android/tv/TvS3DManager;
.super Ljava/lang/Object;
.source "TvS3DManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TvS3DManager"

.field static mInstance:Lcom/mstar/android/tv/TvS3DManager;

.field private static mService:Lcom/mstar/android/tv/ITvS3D;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/mstar/android/tv/TvS3DManager;->mInstance:Lcom/mstar/android/tv/TvS3DManager;

    sput-object v0, Lcom/mstar/android/tv/TvS3DManager;->mService:Lcom/mstar/android/tv/ITvS3D;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/mstar/android/tv/TvS3DManager;
    .locals 2

    sget-object v0, Lcom/mstar/android/tv/TvS3DManager;->mInstance:Lcom/mstar/android/tv/TvS3DManager;

    if-nez v0, :cond_1

    const-class v1, Lcom/mstar/android/tv/TvS3DManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mstar/android/tv/TvS3DManager;->mInstance:Lcom/mstar/android/tv/TvS3DManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mstar/android/tv/TvS3DManager;

    invoke-direct {v0}, Lcom/mstar/android/tv/TvS3DManager;-><init>()V

    sput-object v0, Lcom/mstar/android/tv/TvS3DManager;->mInstance:Lcom/mstar/android/tv/TvS3DManager;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Lcom/mstar/android/tv/TvS3DManager;->mInstance:Lcom/mstar/android/tv/TvS3DManager;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static getService()Lcom/mstar/android/tv/ITvS3D;
    .locals 1

    sget-object v0, Lcom/mstar/android/tv/TvS3DManager;->mService:Lcom/mstar/android/tv/ITvS3D;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/mstar/android/tv/TvS3DManager;->mService:Lcom/mstar/android/tv/ITvS3D;

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tv/TvManager;->getInstance()Lcom/mstar/android/tv/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvManager;->getTvS3D()Lcom/mstar/android/tv/ITvS3D;

    move-result-object v0

    sput-object v0, Lcom/mstar/android/tv/TvS3DManager;->mService:Lcom/mstar/android/tv/ITvS3D;

    sget-object v0, Lcom/mstar/android/tv/TvS3DManager;->mService:Lcom/mstar/android/tv/ITvS3D;

    goto :goto_0
.end method


# virtual methods
.method public get3DDepthMode()I
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvS3DManager;->getService()Lcom/mstar/android/tv/ITvS3D;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvS3D;->get3DDepthMode()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    const-string v3, "TvS3DManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "get3DDepthMode(), return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public get3DOffsetMode()I
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvS3DManager;->getService()Lcom/mstar/android/tv/ITvS3D;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvS3D;->get3DOffsetMode()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    const-string v3, "TvS3DManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "get3DOffsetMode(), return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public get3DOutputAspectMode()Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DOutputAspect;
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvS3DManager;->getService()Lcom/mstar/android/tv/ITvS3D;

    move-result-object v2

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DOutputAspect;->values()[Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DOutputAspect;

    move-result-object v3

    invoke-interface {v2}, Lcom/mstar/android/tv/ITvS3D;->get3DOutputAspectMode()I

    move-result v4

    aget-object v1, v3, v4
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v3, "TvS3DManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "get3DOutputAspectMode(), return EnumThreeDVideo3DOutputAspect "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAutoStartMode()Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvS3DManager;->getService()Lcom/mstar/android/tv/ITvS3D;

    move-result-object v2

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;->values()[Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;

    move-result-object v3

    invoke-interface {v2}, Lcom/mstar/android/tv/ITvS3D;->getAutoStartMode()I

    move-result v4

    aget-object v1, v3, v4
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v3, "TvS3DManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAutoStartMode(), return EnumThreeDVideoAutoStart "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getDisplay3DTo2DMode()Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvS3DManager;->getService()Lcom/mstar/android/tv/ITvS3D;

    move-result-object v2

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;->values()[Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    move-result-object v3

    invoke-interface {v2}, Lcom/mstar/android/tv/ITvS3D;->getDisplay3DTo2DMode()I

    move-result v4

    aget-object v1, v3, v4
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v3, "TvS3DManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getDisplay3DTo2DMode(), return EnumThreeDVideo3DTo2D "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getDisplayFormat()Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvS3DManager;->getService()Lcom/mstar/android/tv/ITvS3D;

    move-result-object v2

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->values()[Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    move-result-object v3

    invoke-interface {v2}, Lcom/mstar/android/tv/ITvS3D;->getDisplayFormat()I

    move-result v4

    aget-object v1, v3, v4
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v3, "TvS3DManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getDisplayFormat(), return EnumThreeDVideoDisplayFormat "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getLrViewSwitch()Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoLrViewSwitch;
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvS3DManager;->getService()Lcom/mstar/android/tv/ITvS3D;

    move-result-object v2

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoLrViewSwitch;->values()[Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoLrViewSwitch;

    move-result-object v3

    invoke-interface {v2}, Lcom/mstar/android/tv/ITvS3D;->getLrViewSwitch()I

    move-result v4

    aget-object v1, v3, v4
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v3, "TvS3DManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " getLrViewSwitch(), return EnumThreeDVideoLrViewSwitch "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getSelfAdaptiveDetect()Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoSelfAdaptiveDetect;
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvS3DManager;->getService()Lcom/mstar/android/tv/ITvS3D;

    move-result-object v2

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoSelfAdaptiveDetect;->values()[Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoSelfAdaptiveDetect;

    move-result-object v3

    invoke-interface {v2}, Lcom/mstar/android/tv/ITvS3D;->getSelfAdaptiveDetect()I

    move-result v4

    aget-object v1, v3, v4
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v3, "TvS3DManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getSelfAdaptiveDetect(), return EnumThreeDVideoSelfAdaptiveDetect "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public set3DDepthMode(I)Z
    .locals 5
    .param p1    # I

    const-string v2, "TvS3DManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "set3DDepthMode(i), paras mode3DDepth = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvS3DManager;->getService()Lcom/mstar/android/tv/ITvS3D;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvS3D;->set3DDepthMode(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public set3DOffsetMode(I)Z
    .locals 5
    .param p1    # I

    const-string v2, "TvS3DManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "set3DOffsetMode(), paras mode3DOffset = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvS3DManager;->getService()Lcom/mstar/android/tv/ITvS3D;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvS3D;->set3DOffsetMode(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public set3DOutputAspectMode(Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DOutputAspect;)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DOutputAspect;

    const-string v2, "TvS3DManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "set3DOutputAspectMode(), paras outputAspectMode = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvS3DManager;->getService()Lcom/mstar/android/tv/ITvS3D;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DOutputAspect;->ordinal()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/mstar/android/tv/ITvS3D;->set3DOutputAspectMode(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public set3DTo2D(Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;

    const-string v2, "TvS3DManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "set3DTo2D(), paras displayMode = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvS3DManager;->getService()Lcom/mstar/android/tv/ITvS3D;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideo3DTo2D;->ordinal()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/mstar/android/tv/ITvS3D;->set3DTo2D(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setAutoStartMode(Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;

    const-string v2, "TvS3DManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAutoStartMode(), paras autoStartMode = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvS3DManager;->getService()Lcom/mstar/android/tv/ITvS3D;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoAutoStart;->ordinal()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/mstar/android/tv/ITvS3D;->setAutoStartMode(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setDisplayFormat(Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;

    const-string v2, "TvS3DManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setDisplayFormat(), paras displayFormat = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvS3DManager;->getService()Lcom/mstar/android/tv/ITvS3D;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoDisplayFormat;->ordinal()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/mstar/android/tv/ITvS3D;->setDisplayFormat(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setDisplayFormatForUI(I)V
    .locals 2
    .param p1    # I

    invoke-static {}, Lcom/mstar/android/tv/TvS3DManager;->getService()Lcom/mstar/android/tv/ITvS3D;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvS3D;->setDisplayFormatForUI(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setLrViewSwitch(Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoLrViewSwitch;)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoLrViewSwitch;

    const-string v2, "TvS3DManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setLrViewSwitch(), paras LrViewSwitchMode = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvS3DManager;->getService()Lcom/mstar/android/tv/ITvS3D;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoLrViewSwitch;->ordinal()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/mstar/android/tv/ITvS3D;->setLrViewSwitch(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setSelfAdaptiveDetect(Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoSelfAdaptiveDetect;)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoSelfAdaptiveDetect;

    const-string v2, "TvS3DManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setSelfAdaptiveDetect(), paras selfAdaptiveDetect = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvS3DManager;->getService()Lcom/mstar/android/tv/ITvS3D;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumThreeDVideoSelfAdaptiveDetect;->ordinal()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/mstar/android/tv/ITvS3D;->setSelfAdaptiveDetect(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method
