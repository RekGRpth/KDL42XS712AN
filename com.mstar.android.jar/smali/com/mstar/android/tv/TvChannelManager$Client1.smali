.class Lcom/mstar/android/tv/TvChannelManager$Client1;
.super Lcom/mstar/android/tv/ITvPlayerEventClient$Stub;
.source "TvChannelManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tv/TvChannelManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Client1"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mstar/android/tv/TvChannelManager;


# direct methods
.method private constructor <init>(Lcom/mstar/android/tv/TvChannelManager;)V
    .locals 0

    iput-object p1, p0, Lcom/mstar/android/tv/TvChannelManager$Client1;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    invoke-direct {p0}, Lcom/mstar/android/tv/ITvPlayerEventClient$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mstar/android/tv/TvChannelManager;Lcom/mstar/android/tv/TvChannelManager$1;)V
    .locals 0
    .param p1    # Lcom/mstar/android/tv/TvChannelManager;
    .param p2    # Lcom/mstar/android/tv/TvChannelManager$1;

    invoke-direct {p0, p1}, Lcom/mstar/android/tv/TvChannelManager$Client1;-><init>(Lcom/mstar/android/tv/TvChannelManager;)V

    return-void
.end method


# virtual methods
.method public onHbbtvUiEvent(ILcom/mstar/android/tvapi/common/vo/HbbtvEventInfo;)Z
    .locals 3
    .param p1    # I
    .param p2    # Lcom/mstar/android/tvapi/common/vo/HbbtvEventInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client1;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->tvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$000(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    invoke-interface {v1, p1, p2}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onHbbtvUiEvent(ILcom/mstar/android/tvapi/common/vo/HbbtvEventInfo;)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onPopupDialog(III)Z
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client1;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->tvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$000(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    invoke-interface {v1, p1, p2, p3}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onPopupDialog(III)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onPvrNotifyAlwaysTimeShiftProgramNotReady(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client1;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->tvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$000(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onPvrNotifyAlwaysTimeShiftProgramNotReady(I)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onPvrNotifyAlwaysTimeShiftProgramReady(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client1;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->tvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$000(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onPvrNotifyAlwaysTimeShiftProgramReady(I)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onPvrNotifyCiPlusProtection(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client1;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->tvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$000(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onPvrNotifyCiPlusProtection(I)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onPvrNotifyCiPlusRetentionLimitUpdate(II)Z
    .locals 3
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client1;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->tvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$000(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    invoke-interface {v1, p1, p2}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onPvrNotifyCiPlusRetentionLimitUpdate(II)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onPvrNotifyOverRun(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client1;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->tvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$000(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onPvrNotifyOverRun(I)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onPvrNotifyParentalControl(II)Z
    .locals 3
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client1;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->tvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$000(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    invoke-interface {v1, p1, p2}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onPvrNotifyParentalControl(II)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onPvrNotifyPlaybackBegin(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client1;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->tvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$000(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onPvrNotifyPlaybackBegin(I)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onPvrNotifyPlaybackSpeedChange(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client1;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->tvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$000(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onPvrNotifyPlaybackSpeedChange(I)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onPvrNotifyPlaybackStop(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client1;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->tvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$000(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onPvrNotifyPlaybackStop(I)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onPvrNotifyPlaybackTime(II)Z
    .locals 3
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client1;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->tvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$000(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    invoke-interface {v1, p1, p2}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onPvrNotifyPlaybackTime(II)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onPvrNotifyRecordSize(II)Z
    .locals 3
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client1;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->tvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$000(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    invoke-interface {v1, p1, p2}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onPvrNotifyRecordSize(II)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onPvrNotifyRecordStop(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client1;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->tvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$000(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onPvrNotifyRecordStop(I)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onPvrNotifyRecordTime(II)Z
    .locals 3
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client1;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->tvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$000(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    invoke-interface {v1, p1, p2}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onPvrNotifyRecordTime(II)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onPvrNotifyTimeShiftOverwritesAfter(II)Z
    .locals 3
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client1;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->tvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$000(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    invoke-interface {v1, p1, p2}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onPvrNotifyTimeShiftOverwritesAfter(II)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onPvrNotifyTimeShiftOverwritesBefore(II)Z
    .locals 3
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client1;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->tvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$000(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    invoke-interface {v1, p1, p2}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onPvrNotifyTimeShiftOverwritesBefore(II)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onPvrNotifyUsbRemoved(II)Z
    .locals 3
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client1;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->tvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$000(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    invoke-interface {v1, p1, p2}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onPvrNotifyUsbRemoved(II)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onScreenSaverMode(II)Z
    .locals 3
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client1;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->tvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$000(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    invoke-interface {v1, p1, p2}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onScreenSaverMode(II)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onSignalLock(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client1;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->tvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$000(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onSignalLock(I)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onSignalUnLock(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client1;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->tvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$000(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onSignalUnLock(I)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onTvProgramInfoReady(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client1;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->tvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$000(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;->onTvProgramInfoReady(I)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method
