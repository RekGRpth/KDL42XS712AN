.class Lcom/mstar/android/tv/TvChannelManager$Client3;
.super Lcom/mstar/android/tv/IDtvPlayerEventClient$Stub;
.source "TvChannelManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tv/TvChannelManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Client3"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mstar/android/tv/TvChannelManager;


# direct methods
.method private constructor <init>(Lcom/mstar/android/tv/TvChannelManager;)V
    .locals 0

    iput-object p1, p0, Lcom/mstar/android/tv/TvChannelManager$Client3;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    invoke-direct {p0}, Lcom/mstar/android/tv/IDtvPlayerEventClient$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mstar/android/tv/TvChannelManager;Lcom/mstar/android/tv/TvChannelManager$1;)V
    .locals 0
    .param p1    # Lcom/mstar/android/tv/TvChannelManager;
    .param p2    # Lcom/mstar/android/tv/TvChannelManager$1;

    invoke-direct {p0, p1}, Lcom/mstar/android/tv/TvChannelManager$Client3;-><init>(Lcom/mstar/android/tv/TvChannelManager;)V

    return-void
.end method


# virtual methods
.method public onAudioModeChange(IZ)Z
    .locals 3
    .param p1    # I
    .param p2    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client3;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->dtvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$200(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    invoke-interface {v1, p1, p2}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onAudioModeChange(IZ)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onChangeTtxStatus(IZ)Z
    .locals 3
    .param p1    # I
    .param p2    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client3;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->dtvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$200(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    invoke-interface {v1, p1, p2}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onChangeTtxStatus(IZ)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onCiLoadCredentialFail(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client3;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->dtvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$200(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onCiLoadCredentialFail(I)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onDtvAutoTuningScanInfo(ILcom/mstar/android/tvapi/dtv/vo/DtvEventScan;)Z
    .locals 3
    .param p1    # I
    .param p2    # Lcom/mstar/android/tvapi/dtv/vo/DtvEventScan;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client3;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->dtvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$200(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    invoke-interface {v1, p1, p2}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onDtvAutoTuningScanInfo(ILcom/mstar/android/tvapi/dtv/vo/DtvEventScan;)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onDtvAutoUpdateScan(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client3;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->dtvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$200(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onDtvAutoUpdateScan(I)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onDtvChannelNameReady(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client3;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->dtvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$200(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onDtvChannelNameReady(I)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onDtvPriComponentMissing(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client3;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->dtvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$200(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onDtvPriComponentMissing(I)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onDtvProgramInfoReady(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client3;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->dtvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$200(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onDtvProgramInfoReady(I)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onEpgTimerSimulcast(II)Z
    .locals 3
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client3;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->dtvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$200(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    invoke-interface {v1, p1, p2}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onEpgTimerSimulcast(II)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onGingaStatusMode(IZ)Z
    .locals 3
    .param p1    # I
    .param p2    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client3;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->dtvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$200(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    invoke-interface {v1, p1, p2}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onGingaStatusMode(IZ)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onHbbtvStatusMode(IZ)Z
    .locals 3
    .param p1    # I
    .param p2    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client3;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->dtvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$200(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    invoke-interface {v1, p1, p2}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onHbbtvStatusMode(IZ)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onMheg5EventHandler(II)Z
    .locals 3
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client3;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->dtvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$200(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    invoke-interface {v1, p1, p2}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onMheg5EventHandler(II)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onMheg5ReturnKey(II)Z
    .locals 3
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client3;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->dtvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$200(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    invoke-interface {v1, p1, p2}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onMheg5ReturnKey(II)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onMheg5StatusMode(II)Z
    .locals 3
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client3;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->dtvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$200(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    invoke-interface {v1, p1, p2}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onMheg5StatusMode(II)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onOadDownload(II)Z
    .locals 3
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client3;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->dtvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$200(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    invoke-interface {v1, p1, p2}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onOadDownload(II)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onOadHandler(III)Z
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client3;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->dtvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$200(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    invoke-interface {v1, p1, p2, p3}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onOadHandler(III)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onOadTimeout(II)Z
    .locals 3
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client3;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->dtvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$200(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    invoke-interface {v1, p1, p2}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onOadTimeout(II)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onPopupScanDialogFrequencyChange(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client3;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->dtvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$200(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onPopupScanDialogFrequencyChange(I)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onPopupScanDialogLossSignal(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client3;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->dtvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$200(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onPopupScanDialogLossSignal(I)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onPopupScanDialogNewMultiplex(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client3;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->dtvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$200(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onPopupScanDialogNewMultiplex(I)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onRctPresence(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client3;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->dtvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$200(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onRctPresence(I)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onSignalLock(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client3;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->dtvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$200(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onSignalLock(I)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onSignalUnLock(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client3;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->dtvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$200(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onSignalUnLock(I)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onTsChange(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client3;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->dtvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$200(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/dtv/listener/OnDtvPlayerEventListener;->onTsChange(I)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method
