.class public Lcom/mstar/android/tv/PackageParcel;
.super Ljava/lang/Object;
.source "PackageParcel.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/android/tv/PackageParcel;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_PACKAGE_ARRAY_SIZE:I = 0x20


# instance fields
.field private packageArray:[B

.field private pkgSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/android/tv/PackageParcel$1;

    invoke-direct {v0}, Lcom/mstar/android/tv/PackageParcel$1;-><init>()V

    sput-object v0, Lcom/mstar/android/tv/PackageParcel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getPackageArray()[B
    .locals 1

    iget-object v0, p0, Lcom/mstar/android/tv/PackageParcel;->packageArray:[B

    return-object v0
.end method

.method public setPackageArray([B)V
    .locals 4
    .param p1    # [B

    const/16 v3, 0x20

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "packageArray.length "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    array-length v0, p1

    if-lez v0, :cond_0

    iput-object p1, p0, Lcom/mstar/android/tv/PackageParcel;->packageArray:[B

    array-length v0, p1

    iput v0, p0, Lcom/mstar/android/tv/PackageParcel;->pkgSize:I

    :goto_0
    return-void

    :cond_0
    new-array v0, v3, [B

    iput-object v0, p0, Lcom/mstar/android/tv/PackageParcel;->packageArray:[B

    iput v3, p0, Lcom/mstar/android/tv/PackageParcel;->pkgSize:I

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget-object v0, p0, Lcom/mstar/android/tv/PackageParcel;->packageArray:[B

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "this.packageArray.length "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mstar/android/tv/PackageParcel;->packageArray:[B

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget v0, p0, Lcom/mstar/android/tv/PackageParcel;->pkgSize:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/android/tv/PackageParcel;->packageArray:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "write oackagearray ok"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    return-void
.end method
