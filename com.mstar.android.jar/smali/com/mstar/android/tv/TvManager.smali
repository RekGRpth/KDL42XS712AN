.class public Lcom/mstar/android/tv/TvManager;
.super Ljava/lang/Object;
.source "TvManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TvManager"

.field static mInstance:Lcom/mstar/android/tv/TvManager;


# instance fields
.field mService:Lcom/mstar/android/tv/ITvService;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/mstar/android/tv/TvManager;->mInstance:Lcom/mstar/android/tv/TvManager;

    return-void
.end method

.method private constructor <init>(Lcom/mstar/android/tv/ITvService;)V
    .locals 1
    .param p1    # Lcom/mstar/android/tv/ITvService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mstar/android/tv/TvManager;->mService:Lcom/mstar/android/tv/ITvService;

    iput-object p1, p0, Lcom/mstar/android/tv/TvManager;->mService:Lcom/mstar/android/tv/ITvService;

    return-void
.end method

.method public static getInstance()Lcom/mstar/android/tv/TvManager;
    .locals 4

    sget-object v1, Lcom/mstar/android/tv/TvManager;->mInstance:Lcom/mstar/android/tv/TvManager;

    if-nez v1, :cond_1

    const-class v2, Lcom/mstar/android/tv/TvManager;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/mstar/android/tv/TvManager;->mInstance:Lcom/mstar/android/tv/TvManager;

    if-nez v1, :cond_0

    const-string v1, "tv"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    new-instance v1, Lcom/mstar/android/tv/TvManager;

    invoke-static {v0}, Lcom/mstar/android/tv/ITvService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mstar/android/tv/ITvService;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/mstar/android/tv/TvManager;-><init>(Lcom/mstar/android/tv/ITvService;)V

    sput-object v1, Lcom/mstar/android/tv/TvManager;->mInstance:Lcom/mstar/android/tv/TvManager;

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v1, Lcom/mstar/android/tv/TvManager;->mInstance:Lcom/mstar/android/tv/TvManager;

    return-object v1

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method


# virtual methods
.method public getTvAtscChannel()Lcom/mstar/android/tv/ITvAtscChannel;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/android/tv/TvManager;->mService:Lcom/mstar/android/tv/ITvService;

    invoke-interface {v1}, Lcom/mstar/android/tv/ITvService;->getTvAtscChannel()Lcom/mstar/android/tv/ITvAtscChannel;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getTvAudio()Lcom/mstar/android/tv/ITvAudio;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/android/tv/TvManager;->mService:Lcom/mstar/android/tv/ITvService;

    invoke-interface {v1}, Lcom/mstar/android/tv/ITvService;->getTvAudio()Lcom/mstar/android/tv/ITvAudio;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getTvCc()Lcom/mstar/android/tv/ITvCc;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/android/tv/TvManager;->mService:Lcom/mstar/android/tv/ITvService;

    invoke-interface {v1}, Lcom/mstar/android/tv/ITvService;->getTvCc()Lcom/mstar/android/tv/ITvCc;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getTvCec()Lcom/mstar/android/tv/ITvCec;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/android/tv/TvManager;->mService:Lcom/mstar/android/tv/ITvService;

    invoke-interface {v1}, Lcom/mstar/android/tv/ITvService;->getTvCec()Lcom/mstar/android/tv/ITvCec;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getTvChannel()Lcom/mstar/android/tv/ITvChannel;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/android/tv/TvManager;->mService:Lcom/mstar/android/tv/ITvService;

    invoke-interface {v1}, Lcom/mstar/android/tv/ITvService;->getTvChannel()Lcom/mstar/android/tv/ITvChannel;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getTvCommon()Lcom/mstar/android/tv/ITvCommon;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/android/tv/TvManager;->mService:Lcom/mstar/android/tv/ITvService;

    invoke-interface {v1}, Lcom/mstar/android/tv/ITvService;->getTvCommon()Lcom/mstar/android/tv/ITvCommon;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getTvEpg()Lcom/mstar/android/tv/ITvEpg;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/android/tv/TvManager;->mService:Lcom/mstar/android/tv/ITvService;

    invoke-interface {v1}, Lcom/mstar/android/tv/ITvService;->getTvEpg()Lcom/mstar/android/tv/ITvEpg;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getTvFactory()Lcom/mstar/android/tv/ITvFactory;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/android/tv/TvManager;->mService:Lcom/mstar/android/tv/ITvService;

    invoke-interface {v1}, Lcom/mstar/android/tv/ITvService;->getTvFactory()Lcom/mstar/android/tv/ITvFactory;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getTvPicture()Lcom/mstar/android/tv/ITvPicture;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/android/tv/TvManager;->mService:Lcom/mstar/android/tv/ITvService;

    invoke-interface {v1}, Lcom/mstar/android/tv/ITvService;->getTvPicture()Lcom/mstar/android/tv/ITvPicture;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getTvPipPop()Lcom/mstar/android/tv/ITvPipPop;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/android/tv/TvManager;->mService:Lcom/mstar/android/tv/ITvService;

    invoke-interface {v1}, Lcom/mstar/android/tv/ITvService;->getTvPipPop()Lcom/mstar/android/tv/ITvPipPop;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getTvS3D()Lcom/mstar/android/tv/ITvS3D;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/android/tv/TvManager;->mService:Lcom/mstar/android/tv/ITvService;

    invoke-interface {v1}, Lcom/mstar/android/tv/ITvService;->getTvS3D()Lcom/mstar/android/tv/ITvS3D;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getTvTimer()Lcom/mstar/android/tv/ITvTimer;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/android/tv/TvManager;->mService:Lcom/mstar/android/tv/ITvService;

    invoke-interface {v1}, Lcom/mstar/android/tv/ITvService;->getTvTimer()Lcom/mstar/android/tv/ITvTimer;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method
