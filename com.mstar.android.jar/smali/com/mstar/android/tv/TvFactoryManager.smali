.class public Lcom/mstar/android/tv/TvFactoryManager;
.super Ljava/lang/Object;
.source "TvFactoryManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TvFactoryManager"

.field static mInstance:Lcom/mstar/android/tv/TvFactoryManager;

.field private static mService:Lcom/mstar/android/tv/ITvFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/mstar/android/tv/TvFactoryManager;->mInstance:Lcom/mstar/android/tv/TvFactoryManager;

    sput-object v0, Lcom/mstar/android/tv/TvFactoryManager;->mService:Lcom/mstar/android/tv/ITvFactory;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/mstar/android/tv/TvFactoryManager;
    .locals 2

    sget-object v0, Lcom/mstar/android/tv/TvFactoryManager;->mInstance:Lcom/mstar/android/tv/TvFactoryManager;

    if-nez v0, :cond_1

    const-class v1, Lcom/mstar/android/tv/TvFactoryManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mstar/android/tv/TvFactoryManager;->mInstance:Lcom/mstar/android/tv/TvFactoryManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mstar/android/tv/TvFactoryManager;

    invoke-direct {v0}, Lcom/mstar/android/tv/TvFactoryManager;-><init>()V

    sput-object v0, Lcom/mstar/android/tv/TvFactoryManager;->mInstance:Lcom/mstar/android/tv/TvFactoryManager;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Lcom/mstar/android/tv/TvFactoryManager;->mInstance:Lcom/mstar/android/tv/TvFactoryManager;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static getService()Lcom/mstar/android/tv/ITvFactory;
    .locals 1

    sget-object v0, Lcom/mstar/android/tv/TvFactoryManager;->mService:Lcom/mstar/android/tv/ITvFactory;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/mstar/android/tv/TvFactoryManager;->mService:Lcom/mstar/android/tv/ITvFactory;

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tv/TvManager;->getInstance()Lcom/mstar/android/tv/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvManager;->getTvFactory()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v0

    sput-object v0, Lcom/mstar/android/tv/TvFactoryManager;->mService:Lcom/mstar/android/tv/ITvFactory;

    sget-object v0, Lcom/mstar/android/tv/TvFactoryManager;->mService:Lcom/mstar/android/tv/ITvFactory;

    goto :goto_0
.end method


# virtual methods
.method public changeWbParameterWhenSourceChange()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvFactory;->changeWbParameterWhenSourceChange()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public enableUartDebug()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvFactory;->enableUartDebug()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public execAutoAdc()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvFactory;->execAutoAdc()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v2, 0x0

    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public execSetInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "execSetInputSource, paras inputSource = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/mstar/android/tv/ITvFactory;->execSetInputSource(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v2, 0x0

    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public get3DSelfAdaptiveLevel()I
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->get3DSelfAdaptiveLevel()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "get3DSelfAdaptiveLevel, return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAdcBlueGain()I
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getAdcBlueGain()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAdcBlueGain, return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAdcBlueOffset()I
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getAdcBlueOffset()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAdcBlueOffset, return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAdcGreenGain()I
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getAdcGreenGain()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAdcGreenGain, return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAdcGreenOffset()I
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getAdcGreenOffset()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAdcGreenOffset, return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAdcIndex()Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;->values()[Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    move-result-object v3

    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getAdcIndex()I

    move-result v4

    aget-object v1, v3, v4

    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAdcIndex, return EnumAdcSetIndexType "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAdcPhase()I
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getAdcPhase()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAdcPhase, return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAdcRedGain()I
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getAdcRedGain()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAdcRedGain, return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAdcRedOffset()I
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getAdcRedOffset()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAdcRedOffset, return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAefc43()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getAefc43()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAefc43, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAefc44()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getAefc44()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAefc44, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAefc66Bit76()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getAefc66Bit76()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAefc66Bit76, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAefc6EBit3210()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getAefc6EBit3210()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAefc6EBit3210, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAefc6EBit7654()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getAefc6EBit7654()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAefc6EBit7654, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAefcA0()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getAefcA0()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAefcA0, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAefcA1()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getAefcA1()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAefcA1, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAefcCb()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getAefcCb()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getVifVgaMaximum, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAefcCfBit2Atv()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getAefcCb()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getVifVgaMaximum, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAefcCfBit2Av()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getAefcCb()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getVifVgaMaximum, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAefcD4()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getAefcD4()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAefcD4, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAefcD5Bit2()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getAefcD5Bit2()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAefcD5Bit2, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAefcD7HighBound()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getAefcD7HighBound()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAefcD7HighBound, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAefcD7LowBound()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getAefcD7LowBound()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAefcD7LowBound, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAefcD8Bit3210()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getAefcD8Bit3210()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAefcD8Bit3210, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAefcD9Bit0()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getAefcD9Bit0()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAefcD9Bit0, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAudioDspVersion()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getAudioDspVersion()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAudioDspVersion, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAudioHiDevMode()I
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getAudioHiDevMode()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAudioHiDevMode, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAudioNrThreshold()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getAudioNrThreshold()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAudioNrThreshold, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAudioPrescale()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getAudioPrescale()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAudioPrescale, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getAudioSifThreshold()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getAudioSifThreshold()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getAudioSifThreshold, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getBoardType()Ljava/lang/String;
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, 0x0

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getBoardType()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getBoardType, return String "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getChinaDescramblerBox()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getChinaDescramblerBox()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getChinaDescramblerBox, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getCompileTime()Ljava/lang/String;
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, 0x0

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getCompileTime()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getCompileTime, return String "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getCurveType()Lcom/mstar/android/tvapi/common/vo/EnumNlaSetIndex;
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumNlaSetIndex;->values()[Lcom/mstar/android/tvapi/common/vo/EnumNlaSetIndex;

    move-result-object v3

    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getCurveType()I

    move-result v4

    aget-object v1, v3, v4
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getTestPattern"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getDelayReduce()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getDelayReduce()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getDelayReduce, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getDtvAvAbnormalDelay()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvFactory;->getDtvAvAbnormalDelay()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getFactoryPreSetFeature()I
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getFactoryPreSetFeature()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getFactoryPreSetFeature, return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getGainDistributionThreshold()I
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getVifCrThreshold()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getVifCrThreshold, return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getLvdsModulation()I
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getLvdsModulation()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getLvdsModulation, return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getLvdsPercentage()I
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getLvdsPercentage()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getLvdsPercentage, return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getLvdsenable()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvFactory;->getLvdsenable()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getMiuEnable()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvFactory;->getMiuEnable()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getMiuModulation()I
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getMiuModulation()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getMiuModulation, return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getMiuPercentage()I
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getMiuPercentage()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getMiuPercentage, return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getOsdV0Nonlinear()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getOsdV0Nonlinear()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getOsdV0Nonlinear, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getOsdV100Nonlinear()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getOsdV100Nonlinear()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getOsdV100Nonlinear, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getOsdV25Nonlinear()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getOsdV25Nonlinear()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getOsdV25Nonlinear, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getOsdV50Nonlinear()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getOsdV50Nonlinear()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getOsdV50Nonlinear, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getOsdV75Nonlinear()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getOsdV75Nonlinear()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getOsdV75Nonlinear, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getOverScanHPosition()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getOverScanHPosition()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getOverScanHPosition, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getOverScanHSize()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getOverScanHSize()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getOverScanHSize, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getOverScanSourceType()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v3

    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getOverScanSourceType()I

    move-result v4

    aget-object v1, v3, v4
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getOverScanSourceType, return EnumInputSource "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getOverScanVPosition()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getOverScanVPosition()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getOverScanVPosition, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getOverScanVSize()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getOverScanVSize()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getOverScanVSize, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getPanelSwing()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getPanelSwing()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getPanelSwing, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getPanelType()Ljava/lang/String;
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, 0x0

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getPanelType()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getPanelType, return String "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getPeqFoCoarse(I)I
    .locals 6
    .param p1    # I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2, p1}, Lcom/mstar/android/tv/ITvFactory;->getPeqFoCoarse(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getPeqFoCoarse, return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getPeqFoFine(I)I
    .locals 6
    .param p1    # I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2, p1}, Lcom/mstar/android/tv/ITvFactory;->getPeqFoFine(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getPeqFoFine, return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getPeqGain(I)I
    .locals 6
    .param p1    # I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2, p1}, Lcom/mstar/android/tv/ITvFactory;->getPeqGain(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getPeqGain, return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getPeqQ(I)I
    .locals 6
    .param p1    # I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2, p1}, Lcom/mstar/android/tv/ITvFactory;->getPeqQ(I)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getPeqQ, return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getPowerOnMode()Lcom/mstar/android/tvapi/factory/vo/EnumAcOnPowerOnMode;
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/factory/vo/EnumAcOnPowerOnMode;->values()[Lcom/mstar/android/tvapi/factory/vo/EnumAcOnPowerOnMode;

    move-result-object v3

    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getPowerOnMode()I

    move-result v4

    aget-object v1, v3, v4
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getOverScanSourceType, return EnumInputSource "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getSoftWareVersion()Ljava/lang/String;
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, 0x0

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getSoftWareVersion()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getSoftWareVersion, return String "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getTestPattern()Lcom/mstar/android/tvapi/factory/vo/EnumScreenMute;
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/factory/vo/EnumScreenMute;->values()[Lcom/mstar/android/tvapi/factory/vo/EnumScreenMute;

    move-result-object v3

    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getTestPattern()I

    move-result v4

    aget-object v1, v3, v4
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getTestPattern, return EnumScreenMute "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getUartOnOff()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvFactory;->getUartOnOff()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getVdDspVersion()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getVdDspVersion()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getVdDspVersion, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getVifAgcRef()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getVifVersion()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getVifAgcRef, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getVifAsiaSignalOption()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvFactory;->getVifAsiaSignalOption()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getVifClampGainOvNegative()I
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getVifClampGainOvNegative()I

    move-result v1

    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getVifClampGainOvNegative, return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getVifCrKi()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getVifCrKi()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getVifCrKi, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getVifCrKp()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getVifCrKp()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getVifCrKp, return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getVifCrKpKiAdjust()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvFactory;->getVifCrKpKiAdjust()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getVifCrThreshold()I
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getVifCrThreshold()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getVifCrThreshold, return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getVifOverModulation()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvFactory;->getVifOverModulation()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getVifTop()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getVifTop()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getVifTop, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getVifVersion()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getVifVersion()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getVifVersion, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getVifVgaMaximum()I
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getVifVgaMaximum()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getVifVgaMaximum, return int "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getWatchDogMode()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvFactory;->getWatchDogMode()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getWbBlueGain()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getWbBlueGain()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getWbBlueGain, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getWbBlueOffset()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getWbBlueOffset()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getWbBlueOffset, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getWbGreenGain()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getWbGreenGain()I

    move-result v3

    int-to-short v1, v3

    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getWbGreenGain, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getWbGreenOffset()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getWbGreenOffset()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getWbGreenOffset, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getWbRedGain()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getWbRedGain()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getWbRedGain, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getWbRedOffset()S
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v2

    const/4 v1, -0x1

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvFactory;->getWbRedOffset()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    int-to-short v1, v3

    :goto_0
    const-string v3, "TvFactoryManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getWbRedOffset, return short "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public restoreToDefault()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvFactory;->restoreToDefault()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public set3DSelfAdaptiveLevel(I)Z
    .locals 5
    .param p1    # I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "set3DSelfAdaptiveLevel, paras threeDSelfAdaptiveLevel = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->set3DSelfAdaptiveLevel(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setAdcBlueGain(I)Z
    .locals 5
    .param p1    # I

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAdcBlueGain, paras blueGain = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setAdcBlueGain(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v2, 0x0

    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAdcBlueOffset(I)Z
    .locals 5
    .param p1    # I

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAdcBlueOffset, paras blueOffset = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setAdcBlueOffset(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v2, 0x0

    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAdcGreenGain(I)Z
    .locals 5
    .param p1    # I

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAdcGreenGain, paras greenGain = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setAdcGreenGain(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v2, 0x0

    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAdcGreenOffset(I)Z
    .locals 5
    .param p1    # I

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAdcGreenOffset, paras greenOffset = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setAdcGreenOffset(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v2, 0x0

    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAdcIndex(Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAdcIndex, paras eIdx = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;->ordinal()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/mstar/android/tv/ITvFactory;->setAdcIndex(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setAdcPhase(I)Z
    .locals 5
    .param p1    # I

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAdcPhase, return int "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setAdcPhase(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v2, 0x0

    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAdcRedGain(I)Z
    .locals 5
    .param p1    # I

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAdcRedGain, paras redGain = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setAdcRedGain(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v2, 0x0

    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAdcRedOffset(I)Z
    .locals 5
    .param p1    # I

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAdcRedOffset, paras redOffset = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setAdcRedOffset(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v2, 0x0

    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAefc43(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAefc43, paras aefc43 = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setAefc43(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setAefc44(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAefc44, paras aefc44 = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setAefc44(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setAefc66Bit76(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAefc66Bit76, paras aefc66Bit76 = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setAefc66Bit76(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setAefc6EBit3210(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAefc6EBit3210, paras aefc6EBit3210 = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setAefc6EBit3210(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setAefc6EBit7654(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAefc6EBit7654, paras aefc6EBit7654 = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setAefc6EBit7654(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setAefcA0(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAefcA0, paras aefcA0 = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setAefcA0(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setAefcA1(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAefcA1, paras aefcA1 = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setAefcA1(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setAefcCB(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAefcCB, paras aefcCB = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setAefcCB(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setAefcCfBit2Atv(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAefcCfBit2Atv, paras aefcCfBit2Atv = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setAefcCfBit2Atv(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setAefcCfBit2Av(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAefcCfBit2Av, paras aefcCfBit2Av = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setAefcCfBit2Av(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setAefcD4(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAefcD4, paras aefcD4 = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setAefcD4(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setAefcD5Bit2(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAefcD5Bit2, paras aefcD5Bit2 = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setAefcD5Bit2(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setAefcD7HighBound(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAefcD7HighBound, paras aefcD7HighBound = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setAefcD7HighBound(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setAefcD7LowBound(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAefcD7LowBound, paras aefcD7LowBound = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setAefcD7LowBound(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setAefcD8Bit3210(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAefcD8Bit3210, paras aefcD8Bit3210 = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setAefcD8Bit3210(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setAefcD9Bit0(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAefcD9Bit0, paras aefcD9Bit0 = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setAefcD9Bit0(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setAudioDspVersion(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAudioDspVersion, paras aduioDspVersion = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setAudioDspVersion(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setAudioHiDevMode(I)Z
    .locals 5
    .param p1    # I

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAudioHiDevMode, paras audioHiDevMode = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setAudioHiDevMode(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setAudioNrThreshold(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAudioNrThreshold, paras audioNrThreshold = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setAudioNrThreshold(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setAudioPrescale(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAudioPrescale, paras audioPrescaleVal = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setAudioPrescale(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setAudioSifThreshold(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setAudioSifThreshold, paras audioSifThreshold = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setAudioSifThreshold(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setChinaDescramblerBox(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setChinaDescramblerBox, paras chinaDescramblerBox = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setChinaDescramblerBox(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setCurveType(Lcom/mstar/android/tvapi/common/vo/EnumNlaSetIndex;)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumNlaSetIndex;

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setCurveType, paras curveTypeIndex = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumNlaSetIndex;->ordinal()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/mstar/android/tv/ITvFactory;->setCurveType(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setDelayReduce(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setDelayReduce, paras delayReduce = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setDelayReduce(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setDtvAvAbnormalDelay(Z)Z
    .locals 5
    .param p1    # Z

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setDtvAvAbnormalDelay, paras isEnable = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setDtvAvAbnormalDelay(Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setEnvironment(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setEnvironment, paras name = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1, p2}, Lcom/mstar/android/tv/ITvFactory;->setEnvironment(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setFactoryPreSetFeature(I)Z
    .locals 5
    .param p1    # I

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setFactoryPreSetFeature, paras factoryPreset = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setFactoryPreSetFeature(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setGainDistributionThreshold(I)Z
    .locals 5
    .param p1    # I

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setGainDistributionThreshold, paras gainDistributionThreshold = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setGainDistributionThreshold(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setLvdsEnable(Z)Z
    .locals 5
    .param p1    # Z

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setLvdsEnable, paras lvdsSscEnable = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setLvdsEnable(Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setLvdsModulation(I)Z
    .locals 5
    .param p1    # I

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setLvdsModulation, paras lvdsSscSpan = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setLvdsModulation(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setLvdsPercentage(I)Z
    .locals 5
    .param p1    # I

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setLvdsPercentage, paras lvdsSscStep = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setLvdsPercentage(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setMiuEnable(Z)Z
    .locals 5
    .param p1    # Z

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setMiuEnable, paras miuSscEnable = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setMiuEnable(Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setMiuModulation(I)Z
    .locals 5
    .param p1    # I

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setMiuModulation, paras miuSscSpan = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setMiuModulation(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setMiuPercentage(I)Z
    .locals 5
    .param p1    # I

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setMiuPercentage, paras miuSscStep = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setMiuPercentage(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setOsdV0Nonlinear(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setOsdV0Nonlinear, paras nonlinearVal = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setOsdV0Nonlinear(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setOsdV100Nonlinear(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setOsdV100Nonlinear, paras nonlinearVal = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setOsdV100Nonlinear(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setOsdV25Nonlinear(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setOsdV25Nonlinear, paras nonlinearVal = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setOsdV25Nonlinear(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setOsdV50Nonlinear(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setOsdV50Nonlinear, paras nonlinearVal = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setOsdV50Nonlinear(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setOsdV75Nonlinear(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setOsdV75Nonlinear, paras nonlinearVal = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setOsdV75Nonlinear(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setOverScanHPosition(S)Z
    .locals 5
    .param p1    # S

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setOverScanHPosition, paras hPosition = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setOverScanHPosition(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setOverScanHSize(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setOverScanHSize, paras hSize = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setOverScanHSize(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setOverScanSourceType(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setOverScanSourceType, paras SourceType = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/mstar/android/tv/ITvFactory;->setOverScanSourceType(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setOverScanVPosition(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setOverScanVPosition, paras vPosition = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setOverScanVPosition(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setOverScanVSize(S)Z
    .locals 5
    .param p1    # S

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setOverScanVSize, paras vSize = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setOverScanVSize(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setPanelSwing(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setPanelSwing, paras panleSwingVal = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setPanelSwing(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setPeqFoCoarse(II)Z
    .locals 5
    .param p1    # I
    .param p2    # I

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setPeqFoCoarse, paras coarseVal = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1, p2}, Lcom/mstar/android/tv/ITvFactory;->setPeqFoCoarse(II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setPeqFoFine(II)Z
    .locals 5
    .param p1    # I
    .param p2    # I

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setPeqFoFine, paras fineVal = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1, p2}, Lcom/mstar/android/tv/ITvFactory;->setPeqFoFine(II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setPeqGain(II)Z
    .locals 5
    .param p1    # I
    .param p2    # I

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setPeqGain, paras gainVal = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1, p2}, Lcom/mstar/android/tv/ITvFactory;->setPeqGain(II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setPeqQ(II)Z
    .locals 5
    .param p1    # I
    .param p2    # I

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setPeqQ, paras QValue = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1, p2}, Lcom/mstar/android/tv/ITvFactory;->setPeqQ(II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setPowerOnMode(Lcom/mstar/android/tvapi/factory/vo/EnumAcOnPowerOnMode;)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/factory/vo/EnumAcOnPowerOnMode;

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setPowerOnMode, paras factoryPowerMode = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/factory/vo/EnumAcOnPowerOnMode;->ordinal()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/mstar/android/tv/ITvFactory;->setPowerOnMode(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setTestPattern(Lcom/mstar/android/tvapi/factory/vo/EnumScreenMute;)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/factory/vo/EnumScreenMute;

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setTestPattern, paras testPatternMode = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/factory/vo/EnumScreenMute;->ordinal()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/mstar/android/tv/ITvFactory;->setTestPattern(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setUartOnOff(Z)Z
    .locals 5
    .param p1    # Z

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setUartOnOff, paras isEnable = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setUartOnOff(Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setVdDspVersion(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setVdDspVersion, paras vdDspVersion = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setVdDspVersion(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setVifAgcRef(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setVifAgcRef, paras vifAgcRef = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setVifAgcRef(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setVifAsiaSignalOption(Z)Z
    .locals 5
    .param p1    # Z

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setVifAsiaSignalOption, paras vifAsiaSignalOption = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setVifAsiaSignalOption(Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setVifClampGainOvNegative(I)Z
    .locals 5
    .param p1    # I

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setVifClampGainOvNegative, paras vifClampGainOvNegative = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setVifClampGainOvNegative(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setVifCrKi(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setVifCrKi, paras vifCrKi = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setVifCrKi(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setVifCrKp(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setVifCrKp, paras vifCrKp = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setVifCrKp(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setVifCrKpKiAdjust(Z)Z
    .locals 5
    .param p1    # Z

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setVifCrKpKiAdjust, paras vifCrKpKiAdjust = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setVifCrKpKiAdjust(Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setVifCrThreshold(I)Z
    .locals 5
    .param p1    # I

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setVifCrThreshold, paras vifCrThreshold = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setVifCrThreshold(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setVifOverModulation(Z)Z
    .locals 5
    .param p1    # Z

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setVifOverModulation, paras vifOverModulation = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setVifOverModulation(Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setVifTop(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setVifTop, paras vifTop = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setVifTop(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setVifVersion(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setVifVersion, paras vifVersion = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setVifVersion(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setVifVgaMaximum(I)Z
    .locals 5
    .param p1    # I

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setVifVgaMaximum, paras vifVgaMaximum = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setVifVgaMaximum(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setWatchDogMode(Z)Z
    .locals 5
    .param p1    # Z

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setWatchDogMode, paras isEnable = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setWatchDogMode(Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setWbBlueGain(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setWbBlueGain, paras blueGain = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setWbBlueGain(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setWbBlueOffset(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setWbBlueOffset, paras blueOffset = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setWbBlueOffset(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setWbGreenGain(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setWbGreenGain, paras greenGain = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setWbGreenGain(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setWbGreenOffset(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setWbGreenOffset, paras greenOffset = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setWbGreenOffset(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setWbRedGain(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setWbRedGain, paras redGain = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setWbRedGain(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v2, 0x0

    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setWbRedOffset(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvFactoryManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setWbRedOffset, paras redOffset = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvFactoryManager;->getService()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvFactory;->setWbRedOffset(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method
