.class public Lcom/mstar/android/tv/TvAtscChannelManager;
.super Ljava/lang/Object;
.source "TvAtscChannelManager.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TvAtscChannelManager"

.field static mInstance:Lcom/mstar/android/tv/TvAtscChannelManager;

.field private static mService:Lcom/mstar/android/tv/ITvAtscChannel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/mstar/android/tv/TvAtscChannelManager;->mInstance:Lcom/mstar/android/tv/TvAtscChannelManager;

    sput-object v0, Lcom/mstar/android/tv/TvAtscChannelManager;->mService:Lcom/mstar/android/tv/ITvAtscChannel;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/mstar/android/tv/TvAtscChannelManager;
    .locals 2

    sget-object v0, Lcom/mstar/android/tv/TvAtscChannelManager;->mInstance:Lcom/mstar/android/tv/TvAtscChannelManager;

    if-nez v0, :cond_1

    const-class v1, Lcom/mstar/android/tv/TvAtscChannelManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/mstar/android/tv/TvAtscChannelManager;->mInstance:Lcom/mstar/android/tv/TvAtscChannelManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/mstar/android/tv/TvAtscChannelManager;

    invoke-direct {v0}, Lcom/mstar/android/tv/TvAtscChannelManager;-><init>()V

    sput-object v0, Lcom/mstar/android/tv/TvAtscChannelManager;->mInstance:Lcom/mstar/android/tv/TvAtscChannelManager;

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v0, Lcom/mstar/android/tv/TvAtscChannelManager;->mInstance:Lcom/mstar/android/tv/TvAtscChannelManager;

    return-object v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static getService()Lcom/mstar/android/tv/ITvAtscChannel;
    .locals 1

    sget-object v0, Lcom/mstar/android/tv/TvAtscChannelManager;->mService:Lcom/mstar/android/tv/ITvAtscChannel;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/mstar/android/tv/TvAtscChannelManager;->mService:Lcom/mstar/android/tv/ITvAtscChannel;

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tv/TvManager;->getInstance()Lcom/mstar/android/tv/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tv/TvManager;->getTvAtscChannel()Lcom/mstar/android/tv/ITvAtscChannel;

    move-result-object v0

    sput-object v0, Lcom/mstar/android/tv/TvAtscChannelManager;->mService:Lcom/mstar/android/tv/ITvAtscChannel;

    sget-object v0, Lcom/mstar/android/tv/TvAtscChannelManager;->mService:Lcom/mstar/android/tv/ITvAtscChannel;

    goto :goto_0
.end method


# virtual methods
.method public final changeDtvToManualFirstService(I)Z
    .locals 5
    .param p1    # I

    const-string v2, "TvAtscChannelManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "changeDtvToManualFirstService, paras rfCh = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvAtscChannelManager;->getService()Lcom/mstar/android/tv/ITvAtscChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvAtscChannel;->changeDtvToManualFirstService(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public changeProgramList()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvAtscChannelManager;->getService()Lcom/mstar/android/tv/ITvAtscChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvAtscChannel;->changeProgramList()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public deleteAllMainList()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvAtscChannelManager;->getService()Lcom/mstar/android/tv/ITvAtscChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvAtscChannel;->deleteAllMainList()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public deleteAtvMainList()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvAtscChannelManager;->getService()Lcom/mstar/android/tv/ITvAtscChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvAtscChannel;->deleteAtvMainList()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public deleteChannelInformationByRf(S)Z
    .locals 5
    .param p1    # S

    const-string v2, "TvAtscChannelManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "deleteChannelInformationByRf, paras rfCh = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvAtscChannelManager;->getService()Lcom/mstar/android/tv/ITvAtscChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvAtscChannel;->deleteChannelInformationByRf(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public deleteDtvMainList()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvAtscChannelManager;->getService()Lcom/mstar/android/tv/ITvAtscChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvAtscChannel;->deleteDtvMainList()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getCanadaEngRatingLock()Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;
    .locals 6

    const/4 v1, 0x0

    invoke-static {}, Lcom/mstar/android/tv/TvAtscChannelManager;->getService()Lcom/mstar/android/tv/ITvAtscChannel;

    move-result-object v2

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;->values()[Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

    move-result-object v3

    invoke-interface {v2}, Lcom/mstar/android/tv/ITvAtscChannel;->getCanadaEngRatingLock()I

    move-result v4

    aget-object v1, v3, v4
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v3, "TvAtscChannelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getCanadaEngRatingLock, return EnumCanadaEngRatingType "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getCanadaFreRatingLock()Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaFreRatingType;
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvAtscChannelManager;->getService()Lcom/mstar/android/tv/ITvAtscChannel;

    move-result-object v2

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaFreRatingType;->values()[Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaFreRatingType;

    move-result-object v3

    invoke-interface {v2}, Lcom/mstar/android/tv/ITvAtscChannel;->getCanadaFreRatingLock()I

    move-result v4

    aget-object v1, v3, v4
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string v3, "TvAtscChannelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getCanadaFreRatingLock, return EnumCanadaFreRatingType "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getCurrentChannelInformation()Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscMainListChannelInformation;
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvAtscChannelManager;->getService()Lcom/mstar/android/tv/ITvAtscChannel;

    move-result-object v1

    const/4 v2, 0x0

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvAtscChannel;->getCurrentChannelInformation()Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscMainListChannelInformation;

    move-result-object v2

    const-string v3, "TvAtscChannelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getCurrentChannelInformation, return AtscMainListChannelInformation id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscMainListChannelInformation;->id:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", majorNumber = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscMainListChannelInformation;->majorNumber:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", minorNumbe = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscMainListChannelInformation;->minorNumber:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", progId = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscMainListChannelInformation;->progId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", rfCh = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-short v5, v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscMainListChannelInformation;->rfCh:S

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getCurrentProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvAtscChannelManager;->getService()Lcom/mstar/android/tv/ITvAtscChannel;

    move-result-object v1

    const/4 v2, 0x0

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvAtscChannel;->getCurrentProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v2

    const-string v3, "TvAtscChannelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getCurrentProgramInfo, return ProgramInfo antennaType = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->antennaType:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", favorite = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-short v5, v2, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->favorite:S

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", frequency = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->frequency:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", majorNum = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-short v5, v2, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->majorNum:S

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", minorNum = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-short v5, v2, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->minorNum:S

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", number = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->number:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", progId = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->progId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", queryIndex = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->queryIndex:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", screenMuteStatus = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->screenMuteStatus:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", serviceId = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", serviceName = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v2, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", serviceType = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-short v5, v2, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", transportStreamId = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->transportStreamId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getCurrentRatingInformation()Ljava/lang/String;
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvAtscChannelManager;->getService()Lcom/mstar/android/tv/ITvAtscChannel;

    move-result-object v2

    const/4 v1, 0x0

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/android/tv/ITvAtscChannel;->getCurrentRatingInformation()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    const-string v3, "TvAtscChannelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getCurrentRatingInformation, return Sting "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getCurrentVChipBlockStatus()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvAtscChannelManager;->getService()Lcom/mstar/android/tv/ITvAtscChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvAtscChannel;->getCurrentVChipBlockStatus()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getProgramInfo(I)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;
    .locals 5
    .param p1    # I

    const-string v2, "TvAtscChannelManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getProgramInfo, paras queryIndex = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvAtscChannelManager;->getService()Lcom/mstar/android/tv/ITvAtscChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvAtscChannel;->getProgramInfo(I)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    return-object v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getRRTInformation()Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation;
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvAtscChannelManager;->getService()Lcom/mstar/android/tv/ITvAtscChannel;

    move-result-object v1

    const/4 v2, 0x0

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvAtscChannel;->getRRTInformation()Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    const-string v3, "TvAtscChannelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getRRTInformation, return Region5RatingInformation dimensionNo = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-short v5, v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation;->dimensionNo:S

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getUsaMpaaRatingLock()Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaMpaaRatingType;
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvAtscChannelManager;->getService()Lcom/mstar/android/tv/ITvAtscChannel;

    move-result-object v1

    const/4 v2, 0x0

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvAtscChannel;->getUsaMpaaRatingLock()Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaMpaaRatingType;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    const-string v3, "TvAtscChannelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getUsaMpaaRatingLock, return UsaMpaaRatingType enUaMpaaRatingType = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaMpaaRatingType;->enUaMpaaRatingType:Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaMpaaRatingType$EnumUsaMpaaRatingType;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", isNr = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, v2, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaMpaaRatingType;->isNr:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getUsaTvRatingLock()Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;
    .locals 6

    invoke-static {}, Lcom/mstar/android/tv/TvAtscChannelManager;->getService()Lcom/mstar/android/tv/ITvAtscChannel;

    move-result-object v1

    const/4 v2, 0x0

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvAtscChannel;->getUsaTvRatingLock()Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    :goto_0
    const-string v3, "TvAtscChannelManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "return UsaTvRatingInformation "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-object v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public getVChipInputSourceBlockStatus(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    const-string v2, "TvAtscChannelManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getVChipInputSourceBlockStatus, paras enInputSource = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvAtscChannelManager;->getService()Lcom/mstar/android/tv/ITvAtscChannel;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/mstar/android/tv/ITvAtscChannel;->getVChipInputSourceBlockStatus(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public programSel(II)Z
    .locals 5
    .param p1    # I
    .param p2    # I

    const-string v2, "TvAtscChannelManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "programSel, paras majorNumber = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", minorNumber = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvAtscChannelManager;->getService()Lcom/mstar/android/tv/ITvAtscChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1, p2}, Lcom/mstar/android/tv/ITvAtscChannel;->programSel(II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public resetRRTSetting()Z
    .locals 3

    invoke-static {}, Lcom/mstar/android/tv/TvAtscChannelManager;->getService()Lcom/mstar/android/tv/ITvAtscChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1}, Lcom/mstar/android/tv/ITvAtscChannel;->resetRRTSetting()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setCanadaEngGuideline(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tv/TvAtscChannelManager;->getService()Lcom/mstar/android/tv/ITvAtscChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvAtscChannel;->setCanadaEngGuideline(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setCanadaEngRatingLock(Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

    const-string v2, "TvAtscChannelManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setCanadaEngRatingLock, paras enRatingType = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvAtscChannelManager;->getService()Lcom/mstar/android/tv/ITvAtscChannel;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;->ordinal()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/mstar/android/tv/ITvAtscChannel;->setCanadaEngRatingLock(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setCanadaFreGuideline(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tv/TvAtscChannelManager;->getService()Lcom/mstar/android/tv/ITvAtscChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvAtscChannel;->setCanadaFreGuideline(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setCanadaFreRatingLock(Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaFreRatingType;)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaFreRatingType;

    const-string v2, "TvAtscChannelManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setCanadaFreRatingLock, paras enRatingType = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvAtscChannelManager;->getService()Lcom/mstar/android/tv/ITvAtscChannel;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaFreRatingType;->ordinal()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/mstar/android/tv/ITvAtscChannel;->setCanadaFreRatingLock(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setDtvAntennaType(Lcom/mstar/android/tvapi/common/vo/EnumAntennaType;)V
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumAntennaType;

    const-string v2, "TvAtscChannelManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setDtvAntennaType(), paras type = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvAtscChannelManager;->getService()Lcom/mstar/android/tv/ITvAtscChannel;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/EnumAntennaType;->ordinal()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/mstar/android/tv/ITvAtscChannel;->setDtvAntennaType(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setDynamicGuideline(III)Z
    .locals 3
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tv/TvAtscChannelManager;->getService()Lcom/mstar/android/tv/ITvAtscChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1, p2, p3}, Lcom/mstar/android/tv/ITvAtscChannel;->setDynamicGuideline(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final setProgramName(IILjava/lang/String;)V
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/lang/String;

    const-string v2, "TvAtscChannelManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setProgramName, paras majorNumber = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", minorNumber = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", programName = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvAtscChannelManager;->getService()Lcom/mstar/android/tv/ITvAtscChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1, p2, p3}, Lcom/mstar/android/tv/ITvAtscChannel;->setProgramName(IILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setUsaMpaaGuideline(IZ)Z
    .locals 3
    .param p1    # I
    .param p2    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tv/TvAtscChannelManager;->getService()Lcom/mstar/android/tv/ITvAtscChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1, p2}, Lcom/mstar/android/tv/ITvAtscChannel;->setUsaMpaaGuideline(IZ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setUsaMpaaRatingLock(Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaMpaaRatingType;)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaMpaaRatingType;

    const-string v2, "TvAtscChannelManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setUsaMpaaRatingLock, paras usaMpaaRatingType = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvAtscChannelManager;->getService()Lcom/mstar/android/tv/ITvAtscChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvAtscChannel;->setUsaMpaaRatingLock(Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaMpaaRatingType;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setUsaTvGuideline(II)Z
    .locals 3
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-static {}, Lcom/mstar/android/tv/TvAtscChannelManager;->getService()Lcom/mstar/android/tv/ITvAtscChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1, p2}, Lcom/mstar/android/tv/ITvAtscChannel;->setUsaTvGuideline(II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setUsaTvRatingLock(Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;

    const-string v2, "TvAtscChannelManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setUsaTvRatingLock, paras usaTvRatingInfo = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvAtscChannelManager;->getService()Lcom/mstar/android/tv/ITvAtscChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1}, Lcom/mstar/android/tv/ITvAtscChannel;->setUsaTvRatingLock(Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setVChipGuideline(SSSS)Z
    .locals 5
    .param p1    # S
    .param p2    # S
    .param p3    # S
    .param p4    # S

    const-string v2, "TvAtscChannelManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setVChipGuideline, paras ratingType = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", para1 = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", para2 = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", para3 = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvAtscChannelManager;->getService()Lcom/mstar/android/tv/ITvAtscChannel;

    move-result-object v1

    :try_start_0
    invoke-interface {v1, p1, p2, p3, p4}, Lcom/mstar/android/tv/ITvAtscChannel;->setVChipGuideline(IIII)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setVChipInputSourceBlockStatus(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Z)V
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    .param p2    # Z

    const-string v2, "TvAtscChannelManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setVChipInputSourceBlockStatus, paras enInputSource = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", enable = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tv/TvAtscChannelManager;->getService()Lcom/mstar/android/tv/ITvAtscChannel;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v2

    invoke-interface {v1, v2, p2}, Lcom/mstar/android/tv/ITvAtscChannel;->setVChipInputSourceBlockStatus(IZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
