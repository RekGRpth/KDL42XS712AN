.class Lcom/mstar/android/tv/TvChannelManager$Client2;
.super Lcom/mstar/android/tv/IAtvPlayerEventClient$Stub;
.source "TvChannelManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/tv/TvChannelManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Client2"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mstar/android/tv/TvChannelManager;


# direct methods
.method private constructor <init>(Lcom/mstar/android/tv/TvChannelManager;)V
    .locals 0

    iput-object p1, p0, Lcom/mstar/android/tv/TvChannelManager$Client2;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    invoke-direct {p0}, Lcom/mstar/android/tv/IAtvPlayerEventClient$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/mstar/android/tv/TvChannelManager;Lcom/mstar/android/tv/TvChannelManager$1;)V
    .locals 0
    .param p1    # Lcom/mstar/android/tv/TvChannelManager;
    .param p2    # Lcom/mstar/android/tv/TvChannelManager$1;

    invoke-direct {p0, p1}, Lcom/mstar/android/tv/TvChannelManager$Client2;-><init>(Lcom/mstar/android/tv/TvChannelManager;)V

    return-void
.end method


# virtual methods
.method public onAtvAutoTuningScanInfo(ILcom/mstar/android/tvapi/atv/vo/AtvEventScan;)Z
    .locals 3
    .param p1    # I
    .param p2    # Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client2;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->atvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$100(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;

    invoke-interface {v1, p1, p2}, Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;->onAtvAutoTuningScanInfo(ILcom/mstar/android/tvapi/atv/vo/AtvEventScan;)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onAtvManualTuningScanInfo(ILcom/mstar/android/tvapi/atv/vo/AtvEventScan;)Z
    .locals 3
    .param p1    # I
    .param p2    # Lcom/mstar/android/tvapi/atv/vo/AtvEventScan;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client2;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->atvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$100(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;

    invoke-interface {v1, p1, p2}, Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;->onAtvManualTuningScanInfo(ILcom/mstar/android/tvapi/atv/vo/AtvEventScan;)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onAtvProgramInfoReady(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client2;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->atvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$100(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;->onAtvProgramInfoReady(I)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onSignalLock(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client2;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->atvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$100(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;->onSignalLock(I)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method

.method public onSignalUnLock(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/tv/TvChannelManager$Client2;->this$0:Lcom/mstar/android/tv/TvChannelManager;

    # getter for: Lcom/mstar/android/tv/TvChannelManager;->atvListeners:Ljava/util/HashMap;
    invoke-static {v2}, Lcom/mstar/android/tv/TvChannelManager;->access$100(Lcom/mstar/android/tv/TvChannelManager;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;

    invoke-interface {v1, p1}, Lcom/mstar/android/tvapi/atv/listener/OnAtvPlayerEventListener;->onSignalUnLock(I)Z

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    return v2
.end method
