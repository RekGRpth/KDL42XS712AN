.class public Lcom/mstar/android/sip/SipManager;
.super Ljava/lang/Object;
.source "SipManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/sip/SipManager$ListenerRelay;
    }
.end annotation


# static fields
.field public static final ACTION_SIP_ADD_PHONE:Ljava/lang/String; = "com.android.phone.SIP_ADD_PHONE"

.field public static final ACTION_SIP_INCOMING_CALL:Ljava/lang/String; = "com.android.phone.SIP_INCOMING_CALL"

.field public static final ACTION_SIP_REMOVE_PHONE:Ljava/lang/String; = "com.android.phone.SIP_REMOVE_PHONE"

.field public static final ACTION_SIP_SERVICE_UP:Ljava/lang/String; = "android.net.sip.SIP_SERVICE_UP"

.field public static final EXTRA_CALL_ID:Ljava/lang/String; = "android:sipCallID"

.field public static final EXTRA_LOCAL_URI:Ljava/lang/String; = "android:localSipUri"

.field public static final EXTRA_OFFER_SD:Ljava/lang/String; = "android:sipOfferSD"

.field public static final INCOMING_CALL_RESULT_CODE:I = 0x65

.field private static final TAG:Ljava/lang/String; = "SipManager"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mSipService:Lcom/mstar/android/sip/ISipService;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/mstar/android/sip/SipManager;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/mstar/android/sip/SipManager;->createSipService()V

    return-void
.end method

.method public static createIncomingCallBroadcast(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p0    # Ljava/lang/String;
    .param p1    # Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "android:sipCallID"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android:sipOfferSD"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-object v0
.end method

.method private static createRelay(Lcom/mstar/android/sip/SipRegistrationListener;Ljava/lang/String;)Lcom/mstar/android/sip/ISipSessionListener;
    .locals 1
    .param p0    # Lcom/mstar/android/sip/SipRegistrationListener;
    .param p1    # Ljava/lang/String;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/mstar/android/sip/SipManager$ListenerRelay;

    invoke-direct {v0, p0, p1}, Lcom/mstar/android/sip/SipManager$ListenerRelay;-><init>(Lcom/mstar/android/sip/SipRegistrationListener;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private createSipService()V
    .locals 2

    const-string v1, "sip"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/mstar/android/sip/ISipService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mstar/android/sip/ISipService;

    move-result-object v1

    iput-object v1, p0, Lcom/mstar/android/sip/SipManager;->mSipService:Lcom/mstar/android/sip/ISipService;

    return-void
.end method

.method public static getCallId(Landroid/content/Intent;)Ljava/lang/String;
    .locals 1
    .param p0    # Landroid/content/Intent;

    const-string v0, "android:sipCallID"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getOfferSessionDescription(Landroid/content/Intent;)Ljava/lang/String;
    .locals 1
    .param p0    # Landroid/content/Intent;

    const-string v0, "android:sipOfferSD"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static isApiSupported(Landroid/content/Context;)Z
    .locals 2
    .param p0    # Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.software.sip"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isIncomingCallIntent(Landroid/content/Intent;)Z
    .locals 3
    .param p0    # Landroid/content/Intent;

    const/4 v2, 0x0

    if-nez p0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-static {p0}, Lcom/mstar/android/sip/SipManager;->getCallId(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Lcom/mstar/android/sip/SipManager;->getOfferSessionDescription(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static isSipWifiOnly(Landroid/content/Context;)Z
    .locals 2
    .param p0    # Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1110032    # android.R.bool.config_sip_wifi_only

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public static isVoipSupported(Landroid/content/Context;)Z
    .locals 2
    .param p0    # Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.software.sip.voip"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/mstar/android/sip/SipManager;->isApiSupported(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static newInstance(Landroid/content/Context;)Lcom/mstar/android/sip/SipManager;
    .locals 1
    .param p0    # Landroid/content/Context;

    invoke-static {p0}, Lcom/mstar/android/sip/SipManager;->isApiSupported(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/mstar/android/sip/SipManager;

    invoke-direct {v0, p0}, Lcom/mstar/android/sip/SipManager;-><init>(Landroid/content/Context;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public close(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/sip/SipException;
        }
    .end annotation

    :try_start_0
    iget-object v1, p0, Lcom/mstar/android/sip/SipManager;->mSipService:Lcom/mstar/android/sip/ISipService;

    invoke-interface {v1, p1}, Lcom/mstar/android/sip/ISipService;->close(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/mstar/android/sip/SipException;

    const-string v2, "close()"

    invoke-direct {v1, v2, v0}, Lcom/mstar/android/sip/SipException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public createSipSession(Lcom/mstar/android/sip/SipProfile;Lcom/mstar/android/sip/SipSession$Listener;)Lcom/mstar/android/sip/SipSession;
    .locals 4
    .param p1    # Lcom/mstar/android/sip/SipProfile;
    .param p2    # Lcom/mstar/android/sip/SipSession$Listener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/sip/SipException;
        }
    .end annotation

    :try_start_0
    iget-object v2, p0, Lcom/mstar/android/sip/SipManager;->mSipService:Lcom/mstar/android/sip/ISipService;

    const/4 v3, 0x0

    invoke-interface {v2, p1, v3}, Lcom/mstar/android/sip/ISipService;->createSession(Lcom/mstar/android/sip/SipProfile;Lcom/mstar/android/sip/ISipSessionListener;)Lcom/mstar/android/sip/ISipSession;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v2, Lcom/mstar/android/sip/SipException;

    const-string v3, "Failed to create SipSession; network unavailable?"

    invoke-direct {v2, v3}, Lcom/mstar/android/sip/SipException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    new-instance v2, Lcom/mstar/android/sip/SipException;

    const-string v3, "createSipSession()"

    invoke-direct {v2, v3, v0}, Lcom/mstar/android/sip/SipException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :cond_0
    :try_start_1
    new-instance v2, Lcom/mstar/android/sip/SipSession;

    invoke-direct {v2, v1, p2}, Lcom/mstar/android/sip/SipSession;-><init>(Lcom/mstar/android/sip/ISipSession;Lcom/mstar/android/sip/SipSession$Listener;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    return-object v2
.end method

.method public getListOfProfiles()[Lcom/mstar/android/sip/SipProfile;
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/android/sip/SipManager;->mSipService:Lcom/mstar/android/sip/ISipService;

    invoke-interface {v1}, Lcom/mstar/android/sip/ISipService;->getListOfProfiles()[Lcom/mstar/android/sip/SipProfile;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/mstar/android/sip/SipProfile;

    goto :goto_0
.end method

.method public getSessionFor(Landroid/content/Intent;)Lcom/mstar/android/sip/SipSession;
    .locals 5
    .param p1    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/sip/SipException;
        }
    .end annotation

    :try_start_0
    invoke-static {p1}, Lcom/mstar/android/sip/SipManager;->getCallId(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/mstar/android/sip/SipManager;->mSipService:Lcom/mstar/android/sip/ISipService;

    invoke-interface {v3, v0}, Lcom/mstar/android/sip/ISipService;->getPendingSession(Ljava/lang/String;)Lcom/mstar/android/sip/ISipSession;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v3, 0x0

    :goto_0
    return-object v3

    :cond_0
    new-instance v3, Lcom/mstar/android/sip/SipSession;

    invoke-direct {v3, v2}, Lcom/mstar/android/sip/SipSession;-><init>(Lcom/mstar/android/sip/ISipSession;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v3, Lcom/mstar/android/sip/SipException;

    const-string v4, "getSessionFor()"

    invoke-direct {v3, v4, v1}, Lcom/mstar/android/sip/SipException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
.end method

.method public isOpened(Ljava/lang/String;)Z
    .locals 3
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/sip/SipException;
        }
    .end annotation

    :try_start_0
    iget-object v1, p0, Lcom/mstar/android/sip/SipManager;->mSipService:Lcom/mstar/android/sip/ISipService;

    invoke-interface {v1, p1}, Lcom/mstar/android/sip/ISipService;->isOpened(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    return v1

    :catch_0
    move-exception v0

    new-instance v1, Lcom/mstar/android/sip/SipException;

    const-string v2, "isOpened()"

    invoke-direct {v1, v2, v0}, Lcom/mstar/android/sip/SipException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public isRegistered(Ljava/lang/String;)Z
    .locals 3
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/sip/SipException;
        }
    .end annotation

    :try_start_0
    iget-object v1, p0, Lcom/mstar/android/sip/SipManager;->mSipService:Lcom/mstar/android/sip/ISipService;

    invoke-interface {v1, p1}, Lcom/mstar/android/sip/ISipService;->isRegistered(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    return v1

    :catch_0
    move-exception v0

    new-instance v1, Lcom/mstar/android/sip/SipException;

    const-string v2, "isRegistered()"

    invoke-direct {v1, v2, v0}, Lcom/mstar/android/sip/SipException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public makeAudioCall(Lcom/mstar/android/sip/SipProfile;Lcom/mstar/android/sip/SipProfile;Lcom/mstar/android/sip/SipAudioCall$Listener;I)Lcom/mstar/android/sip/SipAudioCall;
    .locals 4
    .param p1    # Lcom/mstar/android/sip/SipProfile;
    .param p2    # Lcom/mstar/android/sip/SipProfile;
    .param p3    # Lcom/mstar/android/sip/SipAudioCall$Listener;
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/sip/SipException;
        }
    .end annotation

    iget-object v2, p0, Lcom/mstar/android/sip/SipManager;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/mstar/android/sip/SipManager;->isVoipSupported(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Lcom/mstar/android/sip/SipException;

    const-string v3, "VOIP API is not supported"

    invoke-direct {v2, v3}, Lcom/mstar/android/sip/SipException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    new-instance v0, Lcom/mstar/android/sip/SipAudioCall;

    iget-object v2, p0, Lcom/mstar/android/sip/SipManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2, p1}, Lcom/mstar/android/sip/SipAudioCall;-><init>(Landroid/content/Context;Lcom/mstar/android/sip/SipProfile;)V

    invoke-virtual {v0, p3}, Lcom/mstar/android/sip/SipAudioCall;->setListener(Lcom/mstar/android/sip/SipAudioCall$Listener;)V

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v2}, Lcom/mstar/android/sip/SipManager;->createSipSession(Lcom/mstar/android/sip/SipProfile;Lcom/mstar/android/sip/SipSession$Listener;)Lcom/mstar/android/sip/SipSession;

    move-result-object v1

    invoke-virtual {v0, p2, v1, p4}, Lcom/mstar/android/sip/SipAudioCall;->makeCall(Lcom/mstar/android/sip/SipProfile;Lcom/mstar/android/sip/SipSession;I)V

    return-object v0
.end method

.method public makeAudioCall(Ljava/lang/String;Ljava/lang/String;Lcom/mstar/android/sip/SipAudioCall$Listener;I)Lcom/mstar/android/sip/SipAudioCall;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Lcom/mstar/android/sip/SipAudioCall$Listener;
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/sip/SipException;
        }
    .end annotation

    iget-object v1, p0, Lcom/mstar/android/sip/SipManager;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/mstar/android/sip/SipManager;->isVoipSupported(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/mstar/android/sip/SipException;

    const-string v2, "VOIP API is not supported"

    invoke-direct {v1, v2}, Lcom/mstar/android/sip/SipException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    :try_start_0
    new-instance v1, Lcom/mstar/android/sip/SipProfile$Builder;

    invoke-direct {v1, p1}, Lcom/mstar/android/sip/SipProfile$Builder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/mstar/android/sip/SipProfile$Builder;->build()Lcom/mstar/android/sip/SipProfile;

    move-result-object v1

    new-instance v2, Lcom/mstar/android/sip/SipProfile$Builder;

    invoke-direct {v2, p2}, Lcom/mstar/android/sip/SipProfile$Builder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/mstar/android/sip/SipProfile$Builder;->build()Lcom/mstar/android/sip/SipProfile;

    move-result-object v2

    invoke-virtual {p0, v1, v2, p3, p4}, Lcom/mstar/android/sip/SipManager;->makeAudioCall(Lcom/mstar/android/sip/SipProfile;Lcom/mstar/android/sip/SipProfile;Lcom/mstar/android/sip/SipAudioCall$Listener;I)Lcom/mstar/android/sip/SipAudioCall;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    :catch_0
    move-exception v0

    new-instance v1, Lcom/mstar/android/sip/SipException;

    const-string v2, "build SipProfile"

    invoke-direct {v1, v2, v0}, Lcom/mstar/android/sip/SipException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public open(Lcom/mstar/android/sip/SipProfile;)V
    .locals 3
    .param p1    # Lcom/mstar/android/sip/SipProfile;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/sip/SipException;
        }
    .end annotation

    :try_start_0
    iget-object v1, p0, Lcom/mstar/android/sip/SipManager;->mSipService:Lcom/mstar/android/sip/ISipService;

    invoke-interface {v1, p1}, Lcom/mstar/android/sip/ISipService;->open(Lcom/mstar/android/sip/SipProfile;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/mstar/android/sip/SipException;

    const-string v2, "open()"

    invoke-direct {v1, v2, v0}, Lcom/mstar/android/sip/SipException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public open(Lcom/mstar/android/sip/SipProfile;Landroid/app/PendingIntent;Lcom/mstar/android/sip/SipRegistrationListener;)V
    .locals 3
    .param p1    # Lcom/mstar/android/sip/SipProfile;
    .param p2    # Landroid/app/PendingIntent;
    .param p3    # Lcom/mstar/android/sip/SipRegistrationListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/sip/SipException;
        }
    .end annotation

    if-nez p2, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "incomingCallPendingIntent cannot be null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/mstar/android/sip/SipManager;->mSipService:Lcom/mstar/android/sip/ISipService;

    invoke-virtual {p1}, Lcom/mstar/android/sip/SipProfile;->getUriString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p3, v2}, Lcom/mstar/android/sip/SipManager;->createRelay(Lcom/mstar/android/sip/SipRegistrationListener;Ljava/lang/String;)Lcom/mstar/android/sip/ISipSessionListener;

    move-result-object v2

    invoke-interface {v1, p1, p2, v2}, Lcom/mstar/android/sip/ISipService;->open3(Lcom/mstar/android/sip/SipProfile;Landroid/app/PendingIntent;Lcom/mstar/android/sip/ISipSessionListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/mstar/android/sip/SipException;

    const-string v2, "open()"

    invoke-direct {v1, v2, v0}, Lcom/mstar/android/sip/SipException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public register(Lcom/mstar/android/sip/SipProfile;ILcom/mstar/android/sip/SipRegistrationListener;)V
    .locals 4
    .param p1    # Lcom/mstar/android/sip/SipProfile;
    .param p2    # I
    .param p3    # Lcom/mstar/android/sip/SipRegistrationListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/sip/SipException;
        }
    .end annotation

    :try_start_0
    iget-object v2, p0, Lcom/mstar/android/sip/SipManager;->mSipService:Lcom/mstar/android/sip/ISipService;

    invoke-virtual {p1}, Lcom/mstar/android/sip/SipProfile;->getUriString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p3, v3}, Lcom/mstar/android/sip/SipManager;->createRelay(Lcom/mstar/android/sip/SipRegistrationListener;Ljava/lang/String;)Lcom/mstar/android/sip/ISipSessionListener;

    move-result-object v3

    invoke-interface {v2, p1, v3}, Lcom/mstar/android/sip/ISipService;->createSession(Lcom/mstar/android/sip/SipProfile;Lcom/mstar/android/sip/ISipSessionListener;)Lcom/mstar/android/sip/ISipSession;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v2, Lcom/mstar/android/sip/SipException;

    const-string v3, "SipService.createSession() returns null"

    invoke-direct {v2, v3}, Lcom/mstar/android/sip/SipException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    new-instance v2, Lcom/mstar/android/sip/SipException;

    const-string v3, "register()"

    invoke-direct {v2, v3, v0}, Lcom/mstar/android/sip/SipException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :cond_0
    :try_start_1
    invoke-interface {v1, p2}, Lcom/mstar/android/sip/ISipSession;->register(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    return-void
.end method

.method public setRegistrationListener(Ljava/lang/String;Lcom/mstar/android/sip/SipRegistrationListener;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/mstar/android/sip/SipRegistrationListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/sip/SipException;
        }
    .end annotation

    :try_start_0
    iget-object v1, p0, Lcom/mstar/android/sip/SipManager;->mSipService:Lcom/mstar/android/sip/ISipService;

    invoke-static {p2, p1}, Lcom/mstar/android/sip/SipManager;->createRelay(Lcom/mstar/android/sip/SipRegistrationListener;Ljava/lang/String;)Lcom/mstar/android/sip/ISipSessionListener;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Lcom/mstar/android/sip/ISipService;->setRegistrationListener(Ljava/lang/String;Lcom/mstar/android/sip/ISipSessionListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/mstar/android/sip/SipException;

    const-string v2, "setRegistrationListener()"

    invoke-direct {v1, v2, v0}, Lcom/mstar/android/sip/SipException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public takeAudioCall(Landroid/content/Intent;Lcom/mstar/android/sip/SipAudioCall$Listener;)Lcom/mstar/android/sip/SipAudioCall;
    .locals 7
    .param p1    # Landroid/content/Intent;
    .param p2    # Lcom/mstar/android/sip/SipAudioCall$Listener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/sip/SipException;
        }
    .end annotation

    if-nez p1, :cond_0

    new-instance v5, Lcom/mstar/android/sip/SipException;

    const-string v6, "Cannot retrieve session with null intent"

    invoke-direct {v5, v6}, Lcom/mstar/android/sip/SipException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_0
    invoke-static {p1}, Lcom/mstar/android/sip/SipManager;->getCallId(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v5, Lcom/mstar/android/sip/SipException;

    const-string v6, "Call ID missing in incoming call intent"

    invoke-direct {v5, v6}, Lcom/mstar/android/sip/SipException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_1
    invoke-static {p1}, Lcom/mstar/android/sip/SipManager;->getOfferSessionDescription(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    new-instance v5, Lcom/mstar/android/sip/SipException;

    const-string v6, "Session description missing in incoming call intent"

    invoke-direct {v5, v6}, Lcom/mstar/android/sip/SipException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_2
    :try_start_0
    iget-object v5, p0, Lcom/mstar/android/sip/SipManager;->mSipService:Lcom/mstar/android/sip/ISipService;

    invoke-interface {v5, v1}, Lcom/mstar/android/sip/ISipService;->getPendingSession(Ljava/lang/String;)Lcom/mstar/android/sip/ISipSession;

    move-result-object v3

    if-nez v3, :cond_3

    new-instance v5, Lcom/mstar/android/sip/SipException;

    const-string v6, "No pending session for the call"

    invoke-direct {v5, v6}, Lcom/mstar/android/sip/SipException;-><init>(Ljava/lang/String;)V

    throw v5
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v4

    new-instance v5, Lcom/mstar/android/sip/SipException;

    const-string v6, "takeAudioCall()"

    invoke-direct {v5, v6, v4}, Lcom/mstar/android/sip/SipException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v5

    :cond_3
    :try_start_1
    new-instance v0, Lcom/mstar/android/sip/SipAudioCall;

    iget-object v5, p0, Lcom/mstar/android/sip/SipManager;->mContext:Landroid/content/Context;

    invoke-interface {v3}, Lcom/mstar/android/sip/ISipSession;->getLocalProfile()Lcom/mstar/android/sip/SipProfile;

    move-result-object v6

    invoke-direct {v0, v5, v6}, Lcom/mstar/android/sip/SipAudioCall;-><init>(Landroid/content/Context;Lcom/mstar/android/sip/SipProfile;)V

    new-instance v5, Lcom/mstar/android/sip/SipSession;

    invoke-direct {v5, v3}, Lcom/mstar/android/sip/SipSession;-><init>(Lcom/mstar/android/sip/ISipSession;)V

    invoke-virtual {v0, v5, v2}, Lcom/mstar/android/sip/SipAudioCall;->attachCall(Lcom/mstar/android/sip/SipSession;Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Lcom/mstar/android/sip/SipAudioCall;->setListener(Lcom/mstar/android/sip/SipAudioCall$Listener;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    return-object v0
.end method

.method public unregister(Lcom/mstar/android/sip/SipProfile;Lcom/mstar/android/sip/SipRegistrationListener;)V
    .locals 4
    .param p1    # Lcom/mstar/android/sip/SipProfile;
    .param p2    # Lcom/mstar/android/sip/SipRegistrationListener;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/mstar/android/sip/SipException;
        }
    .end annotation

    :try_start_0
    iget-object v2, p0, Lcom/mstar/android/sip/SipManager;->mSipService:Lcom/mstar/android/sip/ISipService;

    invoke-virtual {p1}, Lcom/mstar/android/sip/SipProfile;->getUriString()Ljava/lang/String;

    move-result-object v3

    invoke-static {p2, v3}, Lcom/mstar/android/sip/SipManager;->createRelay(Lcom/mstar/android/sip/SipRegistrationListener;Ljava/lang/String;)Lcom/mstar/android/sip/ISipSessionListener;

    move-result-object v3

    invoke-interface {v2, p1, v3}, Lcom/mstar/android/sip/ISipService;->createSession(Lcom/mstar/android/sip/SipProfile;Lcom/mstar/android/sip/ISipSessionListener;)Lcom/mstar/android/sip/ISipSession;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v2, Lcom/mstar/android/sip/SipException;

    const-string v3, "SipService.createSession() returns null"

    invoke-direct {v2, v3}, Lcom/mstar/android/sip/SipException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    new-instance v2, Lcom/mstar/android/sip/SipException;

    const-string v3, "unregister()"

    invoke-direct {v2, v3, v0}, Lcom/mstar/android/sip/SipException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :cond_0
    :try_start_1
    invoke-interface {v1}, Lcom/mstar/android/sip/ISipSession;->unregister()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    return-void
.end method
