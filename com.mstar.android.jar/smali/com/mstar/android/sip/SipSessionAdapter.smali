.class public Lcom/mstar/android/sip/SipSessionAdapter;
.super Lcom/mstar/android/sip/ISipSessionListener$Stub;
.source "SipSessionAdapter.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/mstar/android/sip/ISipSessionListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallBusy(Lcom/mstar/android/sip/ISipSession;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/ISipSession;

    return-void
.end method

.method public onCallChangeFailed(Lcom/mstar/android/sip/ISipSession;ILjava/lang/String;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/ISipSession;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public onCallEnded(Lcom/mstar/android/sip/ISipSession;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/ISipSession;

    return-void
.end method

.method public onCallEstablished(Lcom/mstar/android/sip/ISipSession;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/ISipSession;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onCallTransferring(Lcom/mstar/android/sip/ISipSession;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/ISipSession;
    .param p2    # Ljava/lang/String;

    return-void
.end method

.method public onCalling(Lcom/mstar/android/sip/ISipSession;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/ISipSession;

    return-void
.end method

.method public onError(Lcom/mstar/android/sip/ISipSession;ILjava/lang/String;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/ISipSession;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public onRegistering(Lcom/mstar/android/sip/ISipSession;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/ISipSession;

    return-void
.end method

.method public onRegistrationDone(Lcom/mstar/android/sip/ISipSession;I)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/ISipSession;
    .param p2    # I

    return-void
.end method

.method public onRegistrationFailed(Lcom/mstar/android/sip/ISipSession;ILjava/lang/String;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/ISipSession;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public onRegistrationTimeout(Lcom/mstar/android/sip/ISipSession;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/ISipSession;

    return-void
.end method

.method public onRinging(Lcom/mstar/android/sip/ISipSession;Lcom/mstar/android/sip/SipProfile;Ljava/lang/String;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/ISipSession;
    .param p2    # Lcom/mstar/android/sip/SipProfile;
    .param p3    # Ljava/lang/String;

    return-void
.end method

.method public onRingingBack(Lcom/mstar/android/sip/ISipSession;)V
    .locals 0
    .param p1    # Lcom/mstar/android/sip/ISipSession;

    return-void
.end method
