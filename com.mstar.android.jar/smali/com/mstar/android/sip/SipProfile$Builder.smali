.class public Lcom/mstar/android/sip/SipProfile$Builder;
.super Ljava/lang/Object;
.source "SipProfile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/sip/SipProfile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private mAddressFactory:Ljavax/sip/address/AddressFactory;

.field private mDisplayName:Ljava/lang/String;

.field private mProfile:Lcom/mstar/android/sip/SipProfile;

.field private mProxyAddress:Ljava/lang/String;

.field private mUri:Ljavax/sip/address/SipURI;


# direct methods
.method public constructor <init>(Lcom/mstar/android/sip/SipProfile;)V
    .locals 3
    .param p1    # Lcom/mstar/android/sip/SipProfile;

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Lcom/mstar/android/sip/SipProfile;

    invoke-direct {v1, v2}, Lcom/mstar/android/sip/SipProfile;-><init>(Lcom/mstar/android/sip/SipProfile$1;)V

    iput-object v1, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mProfile:Lcom/mstar/android/sip/SipProfile;

    :try_start_0
    invoke-static {}, Ljavax/sip/SipFactory;->getInstance()Ljavax/sip/SipFactory;

    move-result-object v1

    invoke-virtual {v1}, Ljavax/sip/SipFactory;->createAddressFactory()Ljavax/sip/address/AddressFactory;

    move-result-object v1

    iput-object v1, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mAddressFactory:Ljavax/sip/address/AddressFactory;
    :try_end_0
    .catch Ljavax/sip/PeerUnavailableException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_0
    :try_start_1
    # invokes: Ljava/lang/Object;->clone()Ljava/lang/Object;
    invoke-static {p1}, Lcom/mstar/android/sip/SipProfile;->access$200(Lcom/mstar/android/sip/SipProfile;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/mstar/android/sip/SipProfile;

    iput-object v1, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mProfile:Lcom/mstar/android/sip/SipProfile;
    :try_end_1
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_1 .. :try_end_1} :catch_1

    iget-object v1, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mProfile:Lcom/mstar/android/sip/SipProfile;

    # setter for: Lcom/mstar/android/sip/SipProfile;->mAddress:Ljavax/sip/address/Address;
    invoke-static {v1, v2}, Lcom/mstar/android/sip/SipProfile;->access$302(Lcom/mstar/android/sip/SipProfile;Ljavax/sip/address/Address;)Ljavax/sip/address/Address;

    invoke-virtual {p1}, Lcom/mstar/android/sip/SipProfile;->getUri()Ljavax/sip/address/SipURI;

    move-result-object v1

    iput-object v1, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mUri:Ljavax/sip/address/SipURI;

    iget-object v1, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mUri:Ljavax/sip/address/SipURI;

    invoke-virtual {p1}, Lcom/mstar/android/sip/SipProfile;->getPassword()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljavax/sip/address/SipURI;->setUserPassword(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/mstar/android/sip/SipProfile;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mDisplayName:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/mstar/android/sip/SipProfile;->getProxyAddress()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mProxyAddress:Ljava/lang/String;

    iget-object v1, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mProfile:Lcom/mstar/android/sip/SipProfile;

    invoke-virtual {p1}, Lcom/mstar/android/sip/SipProfile;->getPort()I

    move-result v2

    # setter for: Lcom/mstar/android/sip/SipProfile;->mPort:I
    invoke-static {v1, v2}, Lcom/mstar/android/sip/SipProfile;->access$402(Lcom/mstar/android/sip/SipProfile;I)I

    return-void

    :catch_1
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "should not occur"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v2, Lcom/mstar/android/sip/SipProfile;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/mstar/android/sip/SipProfile;-><init>(Lcom/mstar/android/sip/SipProfile$1;)V

    iput-object v2, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mProfile:Lcom/mstar/android/sip/SipProfile;

    :try_start_0
    invoke-static {}, Ljavax/sip/SipFactory;->getInstance()Ljavax/sip/SipFactory;

    move-result-object v2

    invoke-virtual {v2}, Ljavax/sip/SipFactory;->createAddressFactory()Ljavax/sip/address/AddressFactory;

    move-result-object v2

    iput-object v2, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mAddressFactory:Ljavax/sip/address/AddressFactory;
    :try_end_0
    .catch Ljavax/sip/PeerUnavailableException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez p1, :cond_0

    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "uriString cannot be null"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    :cond_0
    iget-object v2, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mAddressFactory:Ljavax/sip/address/AddressFactory;

    invoke-direct {p0, p1}, Lcom/mstar/android/sip/SipProfile$Builder;->fix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljavax/sip/address/AddressFactory;->createURI(Ljava/lang/String;)Ljavax/sip/address/URI;

    move-result-object v1

    instance-of v2, v1, Ljavax/sip/address/SipURI;

    if-eqz v2, :cond_1

    check-cast v1, Ljavax/sip/address/SipURI;

    iput-object v1, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mUri:Ljavax/sip/address/SipURI;

    iget-object v2, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mProfile:Lcom/mstar/android/sip/SipProfile;

    iget-object v3, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mUri:Ljavax/sip/address/SipURI;

    invoke-interface {v3}, Ljavax/sip/address/SipURI;->getHost()Ljava/lang/String;

    move-result-object v3

    # setter for: Lcom/mstar/android/sip/SipProfile;->mDomain:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/mstar/android/sip/SipProfile;->access$502(Lcom/mstar/android/sip/SipProfile;Ljava/lang/String;)Ljava/lang/String;

    return-void

    :cond_1
    new-instance v2, Ljava/text/ParseException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is not a SIP URI"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v2
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Lcom/mstar/android/sip/SipProfile;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/mstar/android/sip/SipProfile;-><init>(Lcom/mstar/android/sip/SipProfile$1;)V

    iput-object v1, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mProfile:Lcom/mstar/android/sip/SipProfile;

    :try_start_0
    invoke-static {}, Ljavax/sip/SipFactory;->getInstance()Ljavax/sip/SipFactory;

    move-result-object v1

    invoke-virtual {v1}, Ljavax/sip/SipFactory;->createAddressFactory()Ljavax/sip/address/AddressFactory;

    move-result-object v1

    iput-object v1, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mAddressFactory:Ljavax/sip/address/AddressFactory;
    :try_end_0
    .catch Ljavax/sip/PeerUnavailableException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "username and serverDomain cannot be null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_1
    iget-object v1, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mAddressFactory:Ljavax/sip/address/AddressFactory;

    invoke-interface {v1, p1, p2}, Ljavax/sip/address/AddressFactory;->createSipURI(Ljava/lang/String;Ljava/lang/String;)Ljavax/sip/address/SipURI;

    move-result-object v1

    iput-object v1, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mUri:Ljavax/sip/address/SipURI;

    iget-object v1, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mProfile:Lcom/mstar/android/sip/SipProfile;

    # setter for: Lcom/mstar/android/sip/SipProfile;->mDomain:Ljava/lang/String;
    invoke-static {v1, p2}, Lcom/mstar/android/sip/SipProfile;->access$502(Lcom/mstar/android/sip/SipProfile;Ljava/lang/String;)Ljava/lang/String;

    return-void
.end method

.method private fix(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sip:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-object p1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sip:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method


# virtual methods
.method public build()Lcom/mstar/android/sip/SipProfile;
    .locals 6

    iget-object v2, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mProfile:Lcom/mstar/android/sip/SipProfile;

    iget-object v3, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mUri:Ljavax/sip/address/SipURI;

    invoke-interface {v3}, Ljavax/sip/address/SipURI;->getUserPassword()Ljava/lang/String;

    move-result-object v3

    # setter for: Lcom/mstar/android/sip/SipProfile;->mPassword:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/mstar/android/sip/SipProfile;->access$1102(Lcom/mstar/android/sip/SipProfile;Ljava/lang/String;)Ljava/lang/String;

    iget-object v2, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mUri:Ljavax/sip/address/SipURI;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljavax/sip/address/SipURI;->setUserPassword(Ljava/lang/String;)V

    :try_start_0
    iget-object v2, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mProxyAddress:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mAddressFactory:Ljavax/sip/address/AddressFactory;

    iget-object v3, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mProxyAddress:Ljava/lang/String;

    invoke-direct {p0, v3}, Lcom/mstar/android/sip/SipProfile$Builder;->fix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljavax/sip/address/AddressFactory;->createURI(Ljava/lang/String;)Ljavax/sip/address/URI;

    move-result-object v1

    check-cast v1, Ljavax/sip/address/SipURI;

    iget-object v2, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mProfile:Lcom/mstar/android/sip/SipProfile;

    invoke-interface {v1}, Ljavax/sip/address/SipURI;->getHost()Ljava/lang/String;

    move-result-object v3

    # setter for: Lcom/mstar/android/sip/SipProfile;->mProxyAddress:Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/mstar/android/sip/SipProfile;->access$1202(Lcom/mstar/android/sip/SipProfile;Ljava/lang/String;)Ljava/lang/String;

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mProfile:Lcom/mstar/android/sip/SipProfile;

    iget-object v3, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mAddressFactory:Ljavax/sip/address/AddressFactory;

    iget-object v4, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mDisplayName:Ljava/lang/String;

    iget-object v5, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mUri:Ljavax/sip/address/SipURI;

    invoke-interface {v3, v4, v5}, Ljavax/sip/address/AddressFactory;->createAddress(Ljava/lang/String;Ljavax/sip/address/URI;)Ljavax/sip/address/Address;

    move-result-object v3

    # setter for: Lcom/mstar/android/sip/SipProfile;->mAddress:Ljavax/sip/address/Address;
    invoke-static {v2, v3}, Lcom/mstar/android/sip/SipProfile;->access$302(Lcom/mstar/android/sip/SipProfile;Ljavax/sip/address/Address;)Ljavax/sip/address/Address;
    :try_end_0
    .catch Ljavax/sip/InvalidArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_1

    iget-object v2, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mProfile:Lcom/mstar/android/sip/SipProfile;

    return-object v2

    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mProfile:Lcom/mstar/android/sip/SipProfile;

    # getter for: Lcom/mstar/android/sip/SipProfile;->mProtocol:Ljava/lang/String;
    invoke-static {v2}, Lcom/mstar/android/sip/SipProfile;->access$800(Lcom/mstar/android/sip/SipProfile;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "UDP"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mUri:Ljavax/sip/address/SipURI;

    iget-object v3, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mProfile:Lcom/mstar/android/sip/SipProfile;

    # getter for: Lcom/mstar/android/sip/SipProfile;->mProtocol:Ljava/lang/String;
    invoke-static {v3}, Lcom/mstar/android/sip/SipProfile;->access$800(Lcom/mstar/android/sip/SipProfile;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljavax/sip/address/SipURI;->setTransportParam(Ljava/lang/String;)V

    :cond_2
    iget-object v2, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mProfile:Lcom/mstar/android/sip/SipProfile;

    # getter for: Lcom/mstar/android/sip/SipProfile;->mPort:I
    invoke-static {v2}, Lcom/mstar/android/sip/SipProfile;->access$400(Lcom/mstar/android/sip/SipProfile;)I

    move-result v2

    const/16 v3, 0x13c4

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mUri:Ljavax/sip/address/SipURI;

    iget-object v3, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mProfile:Lcom/mstar/android/sip/SipProfile;

    # getter for: Lcom/mstar/android/sip/SipProfile;->mPort:I
    invoke-static {v3}, Lcom/mstar/android/sip/SipProfile;->access$400(Lcom/mstar/android/sip/SipProfile;)I

    move-result v3

    invoke-interface {v2, v3}, Ljavax/sip/address/SipURI;->setPort(I)V
    :try_end_1
    .catch Ljavax/sip/InvalidArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    :catch_1
    move-exception v0

    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public setAuthUserName(Ljava/lang/String;)Lcom/mstar/android/sip/SipProfile$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mProfile:Lcom/mstar/android/sip/SipProfile;

    # setter for: Lcom/mstar/android/sip/SipProfile;->mAuthUserName:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/mstar/android/sip/SipProfile;->access$602(Lcom/mstar/android/sip/SipProfile;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setAutoRegistration(Z)Lcom/mstar/android/sip/SipProfile$Builder;
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mProfile:Lcom/mstar/android/sip/SipProfile;

    # setter for: Lcom/mstar/android/sip/SipProfile;->mAutoRegistration:Z
    invoke-static {v0, p1}, Lcom/mstar/android/sip/SipProfile;->access$1002(Lcom/mstar/android/sip/SipProfile;Z)Z

    return-object p0
.end method

.method public setDisplayName(Ljava/lang/String;)Lcom/mstar/android/sip/SipProfile$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mDisplayName:Ljava/lang/String;

    return-object p0
.end method

.method public setOutboundProxy(Ljava/lang/String;)Lcom/mstar/android/sip/SipProfile$Builder;
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mProxyAddress:Ljava/lang/String;

    return-object p0
.end method

.method public setPassword(Ljava/lang/String;)Lcom/mstar/android/sip/SipProfile$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mUri:Ljavax/sip/address/SipURI;

    invoke-interface {v0, p1}, Ljavax/sip/address/SipURI;->setUserPassword(Ljava/lang/String;)V

    return-object p0
.end method

.method public setPort(I)Lcom/mstar/android/sip/SipProfile$Builder;
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    const v0, 0xffff

    if-gt p1, v0, :cond_0

    const/16 v0, 0x3e8

    if-ge p1, v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "incorrect port arugment: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mProfile:Lcom/mstar/android/sip/SipProfile;

    # setter for: Lcom/mstar/android/sip/SipProfile;->mPort:I
    invoke-static {v0, p1}, Lcom/mstar/android/sip/SipProfile;->access$402(Lcom/mstar/android/sip/SipProfile;I)I

    return-object p0
.end method

.method public setProfileName(Ljava/lang/String;)Lcom/mstar/android/sip/SipProfile$Builder;
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mProfile:Lcom/mstar/android/sip/SipProfile;

    # setter for: Lcom/mstar/android/sip/SipProfile;->mProfileName:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/mstar/android/sip/SipProfile;->access$702(Lcom/mstar/android/sip/SipProfile;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setProtocol(Ljava/lang/String;)Lcom/mstar/android/sip/SipProfile$Builder;
    .locals 3
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "protocol cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p1

    const-string v0, "UDP"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "TCP"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unsupported protocol: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mProfile:Lcom/mstar/android/sip/SipProfile;

    # setter for: Lcom/mstar/android/sip/SipProfile;->mProtocol:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/mstar/android/sip/SipProfile;->access$802(Lcom/mstar/android/sip/SipProfile;Ljava/lang/String;)Ljava/lang/String;

    return-object p0
.end method

.method public setSendKeepAlive(Z)Lcom/mstar/android/sip/SipProfile$Builder;
    .locals 1
    .param p1    # Z

    iget-object v0, p0, Lcom/mstar/android/sip/SipProfile$Builder;->mProfile:Lcom/mstar/android/sip/SipProfile;

    # setter for: Lcom/mstar/android/sip/SipProfile;->mSendKeepAlive:Z
    invoke-static {v0, p1}, Lcom/mstar/android/sip/SipProfile;->access$902(Lcom/mstar/android/sip/SipProfile;Z)Z

    return-object p0
.end method
