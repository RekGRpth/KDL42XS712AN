.class Lcom/mstar/android/sip/SipSession$1;
.super Lcom/mstar/android/sip/ISipSessionListener$Stub;
.source "SipSession.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mstar/android/sip/SipSession;->createListener()Lcom/mstar/android/sip/ISipSessionListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/mstar/android/sip/SipSession;


# direct methods
.method constructor <init>(Lcom/mstar/android/sip/SipSession;)V
    .locals 0

    iput-object p1, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    invoke-direct {p0}, Lcom/mstar/android/sip/ISipSessionListener$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallBusy(Lcom/mstar/android/sip/ISipSession;)V
    .locals 2
    .param p1    # Lcom/mstar/android/sip/ISipSession;

    iget-object v0, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    # getter for: Lcom/mstar/android/sip/SipSession;->mListener:Lcom/mstar/android/sip/SipSession$Listener;
    invoke-static {v0}, Lcom/mstar/android/sip/SipSession;->access$000(Lcom/mstar/android/sip/SipSession;)Lcom/mstar/android/sip/SipSession$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    # getter for: Lcom/mstar/android/sip/SipSession;->mListener:Lcom/mstar/android/sip/SipSession$Listener;
    invoke-static {v0}, Lcom/mstar/android/sip/SipSession;->access$000(Lcom/mstar/android/sip/SipSession;)Lcom/mstar/android/sip/SipSession$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    invoke-virtual {v0, v1}, Lcom/mstar/android/sip/SipSession$Listener;->onCallBusy(Lcom/mstar/android/sip/SipSession;)V

    :cond_0
    return-void
.end method

.method public onCallChangeFailed(Lcom/mstar/android/sip/ISipSession;ILjava/lang/String;)V
    .locals 2
    .param p1    # Lcom/mstar/android/sip/ISipSession;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    # getter for: Lcom/mstar/android/sip/SipSession;->mListener:Lcom/mstar/android/sip/SipSession$Listener;
    invoke-static {v0}, Lcom/mstar/android/sip/SipSession;->access$000(Lcom/mstar/android/sip/SipSession;)Lcom/mstar/android/sip/SipSession$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    # getter for: Lcom/mstar/android/sip/SipSession;->mListener:Lcom/mstar/android/sip/SipSession$Listener;
    invoke-static {v0}, Lcom/mstar/android/sip/SipSession;->access$000(Lcom/mstar/android/sip/SipSession;)Lcom/mstar/android/sip/SipSession$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    invoke-virtual {v0, v1, p2, p3}, Lcom/mstar/android/sip/SipSession$Listener;->onCallChangeFailed(Lcom/mstar/android/sip/SipSession;ILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onCallEnded(Lcom/mstar/android/sip/ISipSession;)V
    .locals 2
    .param p1    # Lcom/mstar/android/sip/ISipSession;

    iget-object v0, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    # getter for: Lcom/mstar/android/sip/SipSession;->mListener:Lcom/mstar/android/sip/SipSession$Listener;
    invoke-static {v0}, Lcom/mstar/android/sip/SipSession;->access$000(Lcom/mstar/android/sip/SipSession;)Lcom/mstar/android/sip/SipSession$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    # getter for: Lcom/mstar/android/sip/SipSession;->mListener:Lcom/mstar/android/sip/SipSession$Listener;
    invoke-static {v0}, Lcom/mstar/android/sip/SipSession;->access$000(Lcom/mstar/android/sip/SipSession;)Lcom/mstar/android/sip/SipSession$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    invoke-virtual {v0, v1}, Lcom/mstar/android/sip/SipSession$Listener;->onCallEnded(Lcom/mstar/android/sip/SipSession;)V

    :cond_0
    return-void
.end method

.method public onCallEstablished(Lcom/mstar/android/sip/ISipSession;Ljava/lang/String;)V
    .locals 2
    .param p1    # Lcom/mstar/android/sip/ISipSession;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    # getter for: Lcom/mstar/android/sip/SipSession;->mListener:Lcom/mstar/android/sip/SipSession$Listener;
    invoke-static {v0}, Lcom/mstar/android/sip/SipSession;->access$000(Lcom/mstar/android/sip/SipSession;)Lcom/mstar/android/sip/SipSession$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    # getter for: Lcom/mstar/android/sip/SipSession;->mListener:Lcom/mstar/android/sip/SipSession$Listener;
    invoke-static {v0}, Lcom/mstar/android/sip/SipSession;->access$000(Lcom/mstar/android/sip/SipSession;)Lcom/mstar/android/sip/SipSession$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    invoke-virtual {v0, v1, p2}, Lcom/mstar/android/sip/SipSession$Listener;->onCallEstablished(Lcom/mstar/android/sip/SipSession;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onCallTransferring(Lcom/mstar/android/sip/ISipSession;Ljava/lang/String;)V
    .locals 3
    .param p1    # Lcom/mstar/android/sip/ISipSession;
    .param p2    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    # getter for: Lcom/mstar/android/sip/SipSession;->mListener:Lcom/mstar/android/sip/SipSession$Listener;
    invoke-static {v0}, Lcom/mstar/android/sip/SipSession;->access$000(Lcom/mstar/android/sip/SipSession;)Lcom/mstar/android/sip/SipSession$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    # getter for: Lcom/mstar/android/sip/SipSession;->mListener:Lcom/mstar/android/sip/SipSession$Listener;
    invoke-static {v0}, Lcom/mstar/android/sip/SipSession;->access$000(Lcom/mstar/android/sip/SipSession;)Lcom/mstar/android/sip/SipSession$Listener;

    move-result-object v0

    new-instance v1, Lcom/mstar/android/sip/SipSession;

    iget-object v2, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    # getter for: Lcom/mstar/android/sip/SipSession;->mListener:Lcom/mstar/android/sip/SipSession$Listener;
    invoke-static {v2}, Lcom/mstar/android/sip/SipSession;->access$000(Lcom/mstar/android/sip/SipSession;)Lcom/mstar/android/sip/SipSession$Listener;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Lcom/mstar/android/sip/SipSession;-><init>(Lcom/mstar/android/sip/ISipSession;Lcom/mstar/android/sip/SipSession$Listener;)V

    invoke-virtual {v0, v1, p2}, Lcom/mstar/android/sip/SipSession$Listener;->onCallTransferring(Lcom/mstar/android/sip/SipSession;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onCalling(Lcom/mstar/android/sip/ISipSession;)V
    .locals 2
    .param p1    # Lcom/mstar/android/sip/ISipSession;

    iget-object v0, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    # getter for: Lcom/mstar/android/sip/SipSession;->mListener:Lcom/mstar/android/sip/SipSession$Listener;
    invoke-static {v0}, Lcom/mstar/android/sip/SipSession;->access$000(Lcom/mstar/android/sip/SipSession;)Lcom/mstar/android/sip/SipSession$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    # getter for: Lcom/mstar/android/sip/SipSession;->mListener:Lcom/mstar/android/sip/SipSession$Listener;
    invoke-static {v0}, Lcom/mstar/android/sip/SipSession;->access$000(Lcom/mstar/android/sip/SipSession;)Lcom/mstar/android/sip/SipSession$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    invoke-virtual {v0, v1}, Lcom/mstar/android/sip/SipSession$Listener;->onCalling(Lcom/mstar/android/sip/SipSession;)V

    :cond_0
    return-void
.end method

.method public onError(Lcom/mstar/android/sip/ISipSession;ILjava/lang/String;)V
    .locals 2
    .param p1    # Lcom/mstar/android/sip/ISipSession;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    # getter for: Lcom/mstar/android/sip/SipSession;->mListener:Lcom/mstar/android/sip/SipSession$Listener;
    invoke-static {v0}, Lcom/mstar/android/sip/SipSession;->access$000(Lcom/mstar/android/sip/SipSession;)Lcom/mstar/android/sip/SipSession$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    # getter for: Lcom/mstar/android/sip/SipSession;->mListener:Lcom/mstar/android/sip/SipSession$Listener;
    invoke-static {v0}, Lcom/mstar/android/sip/SipSession;->access$000(Lcom/mstar/android/sip/SipSession;)Lcom/mstar/android/sip/SipSession$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    invoke-virtual {v0, v1, p2, p3}, Lcom/mstar/android/sip/SipSession$Listener;->onError(Lcom/mstar/android/sip/SipSession;ILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onRegistering(Lcom/mstar/android/sip/ISipSession;)V
    .locals 2
    .param p1    # Lcom/mstar/android/sip/ISipSession;

    iget-object v0, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    # getter for: Lcom/mstar/android/sip/SipSession;->mListener:Lcom/mstar/android/sip/SipSession$Listener;
    invoke-static {v0}, Lcom/mstar/android/sip/SipSession;->access$000(Lcom/mstar/android/sip/SipSession;)Lcom/mstar/android/sip/SipSession$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    # getter for: Lcom/mstar/android/sip/SipSession;->mListener:Lcom/mstar/android/sip/SipSession$Listener;
    invoke-static {v0}, Lcom/mstar/android/sip/SipSession;->access$000(Lcom/mstar/android/sip/SipSession;)Lcom/mstar/android/sip/SipSession$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    invoke-virtual {v0, v1}, Lcom/mstar/android/sip/SipSession$Listener;->onRegistering(Lcom/mstar/android/sip/SipSession;)V

    :cond_0
    return-void
.end method

.method public onRegistrationDone(Lcom/mstar/android/sip/ISipSession;I)V
    .locals 2
    .param p1    # Lcom/mstar/android/sip/ISipSession;
    .param p2    # I

    iget-object v0, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    # getter for: Lcom/mstar/android/sip/SipSession;->mListener:Lcom/mstar/android/sip/SipSession$Listener;
    invoke-static {v0}, Lcom/mstar/android/sip/SipSession;->access$000(Lcom/mstar/android/sip/SipSession;)Lcom/mstar/android/sip/SipSession$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    # getter for: Lcom/mstar/android/sip/SipSession;->mListener:Lcom/mstar/android/sip/SipSession$Listener;
    invoke-static {v0}, Lcom/mstar/android/sip/SipSession;->access$000(Lcom/mstar/android/sip/SipSession;)Lcom/mstar/android/sip/SipSession$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    invoke-virtual {v0, v1, p2}, Lcom/mstar/android/sip/SipSession$Listener;->onRegistrationDone(Lcom/mstar/android/sip/SipSession;I)V

    :cond_0
    return-void
.end method

.method public onRegistrationFailed(Lcom/mstar/android/sip/ISipSession;ILjava/lang/String;)V
    .locals 2
    .param p1    # Lcom/mstar/android/sip/ISipSession;
    .param p2    # I
    .param p3    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    # getter for: Lcom/mstar/android/sip/SipSession;->mListener:Lcom/mstar/android/sip/SipSession$Listener;
    invoke-static {v0}, Lcom/mstar/android/sip/SipSession;->access$000(Lcom/mstar/android/sip/SipSession;)Lcom/mstar/android/sip/SipSession$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    # getter for: Lcom/mstar/android/sip/SipSession;->mListener:Lcom/mstar/android/sip/SipSession$Listener;
    invoke-static {v0}, Lcom/mstar/android/sip/SipSession;->access$000(Lcom/mstar/android/sip/SipSession;)Lcom/mstar/android/sip/SipSession$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    invoke-virtual {v0, v1, p2, p3}, Lcom/mstar/android/sip/SipSession$Listener;->onRegistrationFailed(Lcom/mstar/android/sip/SipSession;ILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onRegistrationTimeout(Lcom/mstar/android/sip/ISipSession;)V
    .locals 2
    .param p1    # Lcom/mstar/android/sip/ISipSession;

    iget-object v0, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    # getter for: Lcom/mstar/android/sip/SipSession;->mListener:Lcom/mstar/android/sip/SipSession$Listener;
    invoke-static {v0}, Lcom/mstar/android/sip/SipSession;->access$000(Lcom/mstar/android/sip/SipSession;)Lcom/mstar/android/sip/SipSession$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    # getter for: Lcom/mstar/android/sip/SipSession;->mListener:Lcom/mstar/android/sip/SipSession$Listener;
    invoke-static {v0}, Lcom/mstar/android/sip/SipSession;->access$000(Lcom/mstar/android/sip/SipSession;)Lcom/mstar/android/sip/SipSession$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    invoke-virtual {v0, v1}, Lcom/mstar/android/sip/SipSession$Listener;->onRegistrationTimeout(Lcom/mstar/android/sip/SipSession;)V

    :cond_0
    return-void
.end method

.method public onRinging(Lcom/mstar/android/sip/ISipSession;Lcom/mstar/android/sip/SipProfile;Ljava/lang/String;)V
    .locals 2
    .param p1    # Lcom/mstar/android/sip/ISipSession;
    .param p2    # Lcom/mstar/android/sip/SipProfile;
    .param p3    # Ljava/lang/String;

    iget-object v0, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    # getter for: Lcom/mstar/android/sip/SipSession;->mListener:Lcom/mstar/android/sip/SipSession$Listener;
    invoke-static {v0}, Lcom/mstar/android/sip/SipSession;->access$000(Lcom/mstar/android/sip/SipSession;)Lcom/mstar/android/sip/SipSession$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    # getter for: Lcom/mstar/android/sip/SipSession;->mListener:Lcom/mstar/android/sip/SipSession$Listener;
    invoke-static {v0}, Lcom/mstar/android/sip/SipSession;->access$000(Lcom/mstar/android/sip/SipSession;)Lcom/mstar/android/sip/SipSession$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    invoke-virtual {v0, v1, p2, p3}, Lcom/mstar/android/sip/SipSession$Listener;->onRinging(Lcom/mstar/android/sip/SipSession;Lcom/mstar/android/sip/SipProfile;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onRingingBack(Lcom/mstar/android/sip/ISipSession;)V
    .locals 2
    .param p1    # Lcom/mstar/android/sip/ISipSession;

    iget-object v0, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    # getter for: Lcom/mstar/android/sip/SipSession;->mListener:Lcom/mstar/android/sip/SipSession$Listener;
    invoke-static {v0}, Lcom/mstar/android/sip/SipSession;->access$000(Lcom/mstar/android/sip/SipSession;)Lcom/mstar/android/sip/SipSession$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    # getter for: Lcom/mstar/android/sip/SipSession;->mListener:Lcom/mstar/android/sip/SipSession$Listener;
    invoke-static {v0}, Lcom/mstar/android/sip/SipSession;->access$000(Lcom/mstar/android/sip/SipSession;)Lcom/mstar/android/sip/SipSession$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/mstar/android/sip/SipSession$1;->this$0:Lcom/mstar/android/sip/SipSession;

    invoke-virtual {v0, v1}, Lcom/mstar/android/sip/SipSession$Listener;->onRingingBack(Lcom/mstar/android/sip/SipSession;)V

    :cond_0
    return-void
.end method
