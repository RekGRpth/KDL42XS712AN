.class Lcom/mstar/android/storage/MStorageManager$ISOActionListener;
.super Landroid/os/storage/IISOActionListener$Stub;
.source "MStorageManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/storage/MStorageManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ISOActionListener"
.end annotation


# instance fields
.field private mListeners:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/mstar/android/storage/MStorageManager$ISOListenerDelegate;",
            ">;"
        }
    .end annotation
.end field

.field private nonces:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/mstar/android/storage/OnISOEvnetListener;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/mstar/android/storage/MStorageManager;


# direct methods
.method private constructor <init>(Lcom/mstar/android/storage/MStorageManager;)V
    .locals 1

    iput-object p1, p0, Lcom/mstar/android/storage/MStorageManager$ISOActionListener;->this$0:Lcom/mstar/android/storage/MStorageManager;

    invoke-direct {p0}, Landroid/os/storage/IISOActionListener$Stub;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mstar/android/storage/MStorageManager$ISOActionListener;->mListeners:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/mstar/android/storage/MStorageManager$ISOActionListener;->nonces:Ljava/util/HashMap;

    return-void
.end method

.method synthetic constructor <init>(Lcom/mstar/android/storage/MStorageManager;Lcom/mstar/android/storage/MStorageManager$1;)V
    .locals 0
    .param p1    # Lcom/mstar/android/storage/MStorageManager;
    .param p2    # Lcom/mstar/android/storage/MStorageManager$1;

    invoke-direct {p0, p1}, Lcom/mstar/android/storage/MStorageManager$ISOActionListener;-><init>(Lcom/mstar/android/storage/MStorageManager;)V

    return-void
.end method


# virtual methods
.method public addListener(Lcom/mstar/android/storage/OnISOEvnetListener;)Ljava/lang/Integer;
    .locals 6
    .param p1    # Lcom/mstar/android/storage/OnISOEvnetListener;

    new-instance v1, Lcom/mstar/android/storage/MStorageManager$ISOListenerDelegate;

    iget-object v3, p0, Lcom/mstar/android/storage/MStorageManager$ISOActionListener;->this$0:Lcom/mstar/android/storage/MStorageManager;

    invoke-direct {v1, v3, p1}, Lcom/mstar/android/storage/MStorageManager$ISOListenerDelegate;-><init>(Lcom/mstar/android/storage/MStorageManager;Lcom/mstar/android/storage/OnISOEvnetListener;)V

    new-instance v2, Ljava/lang/Integer;

    # getter for: Lcom/mstar/android/storage/MStorageManager$ISOListenerDelegate;->nonce:I
    invoke-static {v1}, Lcom/mstar/android/storage/MStorageManager$ISOListenerDelegate;->access$200(Lcom/mstar/android/storage/MStorageManager$ISOListenerDelegate;)I

    move-result v3

    invoke-direct {v2, v3}, Ljava/lang/Integer;-><init>(I)V

    iget-object v4, p0, Lcom/mstar/android/storage/MStorageManager$ISOActionListener;->mListeners:Ljava/util/HashMap;

    monitor-enter v4

    :try_start_0
    iget-object v5, p0, Lcom/mstar/android/storage/MStorageManager$ISOActionListener;->nonces:Ljava/util/HashMap;

    monitor-enter v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v3, p0, Lcom/mstar/android/storage/MStorageManager$ISOActionListener;->mListeners:Ljava/util/HashMap;

    invoke-virtual {v3, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, p0, Lcom/mstar/android/storage/MStorageManager$ISOActionListener;->nonces:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    const/4 v3, 0x1

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v3, p0, Lcom/mstar/android/storage/MStorageManager$ISOActionListener;->nonces:Ljava/util/HashMap;

    invoke-virtual {v3, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return-object v2

    :catchall_0
    move-exception v3

    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v3

    :catchall_1
    move-exception v3

    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v3
.end method

.method public onISOEvent(Ljava/lang/String;II)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I

    iget-object v2, p0, Lcom/mstar/android/storage/MStorageManager$ISOActionListener;->mListeners:Ljava/util/HashMap;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/android/storage/MStorageManager$ISOActionListener;->mListeners:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/storage/MStorageManager$ISOListenerDelegate;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/mstar/android/storage/MStorageManager$ISOListenerDelegate;->getListener()Lcom/mstar/android/storage/OnISOEvnetListener;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p0, v1, v3}, Lcom/mstar/android/storage/MStorageManager$ISOActionListener;->removeListener(Lcom/mstar/android/storage/OnISOEvnetListener;Ljava/lang/Integer;)V

    monitor-exit v2

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0, p1, p3}, Lcom/mstar/android/storage/MStorageManager$ISOListenerDelegate;->sendISOStateChanged(Ljava/lang/String;I)V

    :cond_1
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public removeListener(Lcom/mstar/android/storage/OnISOEvnetListener;)V
    .locals 6
    .param p1    # Lcom/mstar/android/storage/OnISOEvnetListener;

    iget-object v3, p0, Lcom/mstar/android/storage/MStorageManager$ISOActionListener;->mListeners:Ljava/util/HashMap;

    monitor-enter v3

    :try_start_0
    iget-object v4, p0, Lcom/mstar/android/storage/MStorageManager$ISOActionListener;->nonces:Ljava/util/HashMap;

    monitor-enter v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v2, p0, Lcom/mstar/android/storage/MStorageManager$ISOActionListener;->nonces:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lcom/mstar/android/storage/MStorageManager$ISOActionListener;->mListeners:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return-void

    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v2

    :catchall_1
    move-exception v2

    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v2
.end method

.method public removeListener(Lcom/mstar/android/storage/OnISOEvnetListener;Ljava/lang/Integer;)V
    .locals 4
    .param p1    # Lcom/mstar/android/storage/OnISOEvnetListener;
    .param p2    # Ljava/lang/Integer;

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p0, Lcom/mstar/android/storage/MStorageManager$ISOActionListener;->mListeners:Ljava/util/HashMap;

    monitor-enter v2

    :try_start_0
    iget-object v3, p0, Lcom/mstar/android/storage/MStorageManager$ISOActionListener;->nonces:Ljava/util/HashMap;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v1, p0, Lcom/mstar/android/storage/MStorageManager$ISOActionListener;->nonces:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/mstar/android/storage/MStorageManager$ISOActionListener;->nonces:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    iget-object v1, p0, Lcom/mstar/android/storage/MStorageManager$ISOActionListener;->mListeners:Ljava/util/HashMap;

    invoke-virtual {v1, p2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1

    :catchall_1
    move-exception v1

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method
