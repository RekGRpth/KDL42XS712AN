.class public Lcom/mstar/android/storage/MStorageManager;
.super Ljava/lang/Object;
.source "MStorageManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/storage/MStorageManager$1;,
        Lcom/mstar/android/storage/MStorageManager$ISOActionListener;,
        Lcom/mstar/android/storage/MStorageManager$ISOListenerDelegate;,
        Lcom/mstar/android/storage/MStorageManager$ISOStateChangedStorageEvent;,
        Lcom/mstar/android/storage/MStorageManager$StorageEvent;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MStorageManager"

.field static mInstance:Lcom/mstar/android/storage/MStorageManager;

.field static final mInstanceSync:Ljava/lang/Object;

.field private static mMStorageManager:Lcom/mstar/android/storage/MStorageManager;


# instance fields
.field private mISOActionListener:Lcom/mstar/android/storage/MStorageManager$ISOActionListener;

.field private final mNextNonce:Ljava/util/concurrent/atomic/AtomicInteger;

.field mService:Landroid/os/storage/IMountService;

.field mTgtLooper:Landroid/os/Looper;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/mstar/android/storage/MStorageManager;->mInstanceSync:Ljava/lang/Object;

    sput-object v1, Lcom/mstar/android/storage/MStorageManager;->mInstance:Lcom/mstar/android/storage/MStorageManager;

    sput-object v1, Lcom/mstar/android/storage/MStorageManager;->mMStorageManager:Lcom/mstar/android/storage/MStorageManager;

    return-void
.end method

.method private constructor <init>(Landroid/os/storage/IMountService;Landroid/os/Looper;)V
    .locals 2
    .param p1    # Landroid/os/storage/IMountService;
    .param p2    # Landroid/os/Looper;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/mstar/android/storage/MStorageManager;->mService:Landroid/os/storage/IMountService;

    new-instance v0, Lcom/mstar/android/storage/MStorageManager$ISOActionListener;

    invoke-direct {v0, p0, v1}, Lcom/mstar/android/storage/MStorageManager$ISOActionListener;-><init>(Lcom/mstar/android/storage/MStorageManager;Lcom/mstar/android/storage/MStorageManager$1;)V

    iput-object v0, p0, Lcom/mstar/android/storage/MStorageManager;->mISOActionListener:Lcom/mstar/android/storage/MStorageManager$ISOActionListener;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/mstar/android/storage/MStorageManager;->mNextNonce:Ljava/util/concurrent/atomic/AtomicInteger;

    iput-object p1, p0, Lcom/mstar/android/storage/MStorageManager;->mService:Landroid/os/storage/IMountService;

    iput-object p2, p0, Lcom/mstar/android/storage/MStorageManager;->mTgtLooper:Landroid/os/Looper;

    return-void
.end method

.method static synthetic access$100(Lcom/mstar/android/storage/MStorageManager;)I
    .locals 1
    .param p0    # Lcom/mstar/android/storage/MStorageManager;

    invoke-direct {p0}, Lcom/mstar/android/storage/MStorageManager;->getNextNonce()I

    move-result v0

    return v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/mstar/android/storage/MStorageManager;
    .locals 5
    .param p0    # Landroid/content/Context;

    sget-object v1, Lcom/mstar/android/storage/MStorageManager;->mInstance:Lcom/mstar/android/storage/MStorageManager;

    if-nez v1, :cond_1

    sget-object v2, Lcom/mstar/android/storage/MStorageManager;->mInstanceSync:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/mstar/android/storage/MStorageManager;->mInstance:Lcom/mstar/android/storage/MStorageManager;

    if-nez v1, :cond_0

    const-string v1, "mount"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    new-instance v1, Lcom/mstar/android/storage/MStorageManager;

    invoke-static {v0}, Landroid/os/storage/IMountService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/storage/IMountService;

    move-result-object v3

    invoke-virtual {p0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Lcom/mstar/android/storage/MStorageManager;-><init>(Landroid/os/storage/IMountService;Landroid/os/Looper;)V

    sput-object v1, Lcom/mstar/android/storage/MStorageManager;->mInstance:Lcom/mstar/android/storage/MStorageManager;

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v1, Lcom/mstar/android/storage/MStorageManager;->mInstance:Lcom/mstar/android/storage/MStorageManager;

    return-object v1

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method private getNextNonce()I
    .locals 1

    iget-object v0, p0, Lcom/mstar/android/storage/MStorageManager;->mNextNonce:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    return v0
.end method


# virtual methods
.method public formatVolume(Ljava/lang/String;)Z
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/mstar/android/storage/MStorageManager;->mService:Landroid/os/storage/IMountService;

    invoke-interface {v3, p1}, Landroid/os/storage/IMountService;->formatVolume(Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_0

    const/4 v2, 0x1

    :cond_0
    :goto_0
    return v2

    :catch_0
    move-exception v0

    const-string v3, "MStorageManager"

    const-string v4, "Failed to format volume"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getISOFileMountPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "filename cannot be null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/mstar/android/storage/MStorageManager;->mService:Landroid/os/storage/IMountService;

    invoke-interface {v1, p1}, Landroid/os/storage/IMountService;->getISOFileMountPath(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const-string v1, "MStorageManager"

    const-string v2, "Failed to get the mount path of the ISO file"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMountedISOFileList()[Ljava/lang/String;
    .locals 3

    :try_start_0
    iget-object v1, p0, Lcom/mstar/android/storage/MStorageManager;->mService:Landroid/os/storage/IMountService;

    invoke-interface {v1}, Landroid/os/storage/IMountService;->getMountedISOFileList()[Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const-string v1, "MStorageManager"

    const-string v2, "Failed to get the mounted ISO files"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getVolumeLabel(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/android/storage/MStorageManager;->mService:Landroid/os/storage/IMountService;

    invoke-interface {v1, p1}, Landroid/os/storage/IMountService;->getVolumeLabel(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const-string v1, "MStorageManager"

    const-string v2, "Failed to get volume label"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getVolumeList()[Landroid/os/storage/StorageVolume;
    .locals 7

    const/4 v6, 0x0

    iget-object v5, p0, Lcom/mstar/android/storage/MStorageManager;->mService:Landroid/os/storage/IMountService;

    if-nez v5, :cond_1

    new-array v4, v6, [Landroid/os/storage/StorageVolume;

    :cond_0
    :goto_0
    return-object v4

    :cond_1
    :try_start_0
    iget-object v5, p0, Lcom/mstar/android/storage/MStorageManager;->mService:Landroid/os/storage/IMountService;

    invoke-interface {v5}, Landroid/os/storage/IMountService;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v3

    if-nez v3, :cond_2

    const/4 v5, 0x0

    new-array v4, v5, [Landroid/os/storage/StorageVolume;

    goto :goto_0

    :cond_2
    array-length v2, v3

    new-array v4, v2, [Landroid/os/storage/StorageVolume;

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_0

    aget-object v5, v3, v1

    check-cast v5, Landroid/os/storage/StorageVolume;

    aput-object v5, v4, v1
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v5, "MStorageManager"

    const-string v6, "Failed to get volume list"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v4, 0x0

    goto :goto_0
.end method

.method public getVolumePaths()[Ljava/lang/String;
    .locals 5

    invoke-virtual {p0}, Lcom/mstar/android/storage/MStorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    move-result-object v3

    if-nez v3, :cond_1

    const/4 v2, 0x0

    :cond_0
    return-object v2

    :cond_1
    array-length v0, v3

    new-array v2, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v4, v3, v1

    invoke-virtual {v4}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public getVolumeState(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;

    iget-object v1, p0, Lcom/mstar/android/storage/MStorageManager;->mService:Landroid/os/storage/IMountService;

    if-nez v1, :cond_0

    const-string v1, "removed"

    :goto_0
    return-object v1

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/mstar/android/storage/MStorageManager;->mService:Landroid/os/storage/IMountService;

    invoke-interface {v1, p1}, Landroid/os/storage/IMountService;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "MStorageManager"

    const-string v2, "Failed to get volume state"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getVolumeUUID(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;

    :try_start_0
    iget-object v1, p0, Lcom/mstar/android/storage/MStorageManager;->mService:Landroid/os/storage/IMountService;

    invoke-interface {v1, p1}, Landroid/os/storage/IMountService;->getVolumeUUID(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const-string v1, "MStorageManager"

    const-string v2, "Failed to get volume uuid"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isISOFileMounted(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/mstar/android/storage/MStorageManager;->getISOFileMountPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public mountISO(Ljava/lang/String;Lcom/mstar/android/storage/OnISOEvnetListener;)Z
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/mstar/android/storage/OnISOEvnetListener;

    if-nez p1, :cond_0

    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "filename cannot be null"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/mstar/android/storage/MStorageManager;->mISOActionListener:Lcom/mstar/android/storage/MStorageManager$ISOActionListener;

    invoke-virtual {v3, p2}, Lcom/mstar/android/storage/MStorageManager$ISOActionListener;->addListener(Lcom/mstar/android/storage/OnISOEvnetListener;)Ljava/lang/Integer;

    move-result-object v1

    iget-object v3, p0, Lcom/mstar/android/storage/MStorageManager;->mService:Landroid/os/storage/IMountService;

    iget-object v4, p0, Lcom/mstar/android/storage/MStorageManager;->mISOActionListener:Lcom/mstar/android/storage/MStorageManager$ISOActionListener;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-interface {v3, p1, v4, v5}, Landroid/os/storage/IMountService;->mountISO(Ljava/lang/String;Landroid/os/storage/IISOActionListener;I)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v3, p0, Lcom/mstar/android/storage/MStorageManager;->mISOActionListener:Lcom/mstar/android/storage/MStorageManager$ISOActionListener;

    invoke-virtual {v3, p2, v1}, Lcom/mstar/android/storage/MStorageManager$ISOActionListener;->removeListener(Lcom/mstar/android/storage/OnISOEvnetListener;Ljava/lang/Integer;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return v2

    :catch_0
    move-exception v0

    const-string v3, "MStorageManager"

    const-string v4, "Failed to mount ISO"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public mountSamba(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 10
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Ljava/lang/String;
    .param p5    # Ljava/lang/String;

    const/4 v9, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/mstar/android/storage/MStorageManager;->mService:Landroid/os/storage/IMountService;

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v7}, Landroid/os/storage/IMountService;->mountSamba(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v8

    const-string v0, "MStorageManager"

    const-string v1, "Failed to mount Samba"

    invoke-static {v0, v1, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v9

    goto :goto_0
.end method

.method public mountVolume(Ljava/lang/String;)Z
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/mstar/android/storage/MStorageManager;->mService:Landroid/os/storage/IMountService;

    invoke-interface {v3, p1}, Landroid/os/storage/IMountService;->mountVolume(Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_0

    const/4 v2, 0x1

    :cond_0
    :goto_0
    return v2

    :catch_0
    move-exception v0

    const-string v3, "MStorageManager"

    const-string v4, "Failed to mount volume"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public removeOnISOEvnetListener(Lcom/mstar/android/storage/OnISOEvnetListener;)V
    .locals 1
    .param p1    # Lcom/mstar/android/storage/OnISOEvnetListener;

    iget-object v0, p0, Lcom/mstar/android/storage/MStorageManager;->mISOActionListener:Lcom/mstar/android/storage/MStorageManager$ISOActionListener;

    invoke-virtual {v0, p1}, Lcom/mstar/android/storage/MStorageManager$ISOActionListener;->removeListener(Lcom/mstar/android/storage/OnISOEvnetListener;)V

    return-void
.end method

.method public unmountISO(Ljava/lang/String;Z)Z
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "filename cannot be null"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/mstar/android/storage/MStorageManager;->mService:Landroid/os/storage/IMountService;

    invoke-interface {v1, p1, p2}, Landroid/os/storage/IMountService;->unmountISO(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const-string v1, "MStorageManager"

    const-string v2, "Failed to unmount ISO"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public unmountSamba(Ljava/lang/String;Z)Z
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Z

    :try_start_0
    iget-object v1, p0, Lcom/mstar/android/storage/MStorageManager;->mService:Landroid/os/storage/IMountService;

    invoke-interface {v1, p1, p2}, Landroid/os/storage/IMountService;->unmountSamba(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const-string v1, "MStorageManager"

    const-string v2, "Failed to unmount Samba"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public unmountVolume(Ljava/lang/String;ZZ)V
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # Z
    .param p3    # Z

    :try_start_0
    iget-object v1, p0, Lcom/mstar/android/storage/MStorageManager;->mService:Landroid/os/storage/IMountService;

    invoke-interface {v1, p1, p2, p3}, Landroid/os/storage/IMountService;->unmountVolume(Ljava/lang/String;ZZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "MStorageManager"

    const-string v2, "Failed to unmount volume"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
