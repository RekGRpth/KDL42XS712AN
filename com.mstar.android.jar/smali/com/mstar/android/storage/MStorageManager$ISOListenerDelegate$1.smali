.class Lcom/mstar/android/storage/MStorageManager$ISOListenerDelegate$1;
.super Landroid/os/Handler;
.source "MStorageManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/mstar/android/storage/MStorageManager$ISOListenerDelegate;-><init>(Lcom/mstar/android/storage/MStorageManager;Lcom/mstar/android/storage/OnISOEvnetListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/mstar/android/storage/MStorageManager$ISOListenerDelegate;

.field final synthetic val$this$0:Lcom/mstar/android/storage/MStorageManager;


# direct methods
.method constructor <init>(Lcom/mstar/android/storage/MStorageManager$ISOListenerDelegate;Landroid/os/Looper;Lcom/mstar/android/storage/MStorageManager;)V
    .locals 0
    .param p2    # Landroid/os/Looper;

    iput-object p1, p0, Lcom/mstar/android/storage/MStorageManager$ISOListenerDelegate$1;->this$1:Lcom/mstar/android/storage/MStorageManager$ISOListenerDelegate;

    iput-object p3, p0, Lcom/mstar/android/storage/MStorageManager$ISOListenerDelegate$1;->val$this$0:Lcom/mstar/android/storage/MStorageManager;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1    # Landroid/os/Message;

    iget-object v3, p0, Lcom/mstar/android/storage/MStorageManager$ISOListenerDelegate$1;->this$1:Lcom/mstar/android/storage/MStorageManager$ISOListenerDelegate;

    invoke-virtual {v3}, Lcom/mstar/android/storage/MStorageManager$ISOListenerDelegate;->getListener()Lcom/mstar/android/storage/OnISOEvnetListener;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/mstar/android/storage/MStorageManager$StorageEvent;

    iget v3, p1, Landroid/os/Message;->what:I

    const/4 v4, 0x4

    if-ne v3, v4, :cond_1

    move-object v1, v0

    check-cast v1, Lcom/mstar/android/storage/MStorageManager$ISOStateChangedStorageEvent;

    iget-object v3, v1, Lcom/mstar/android/storage/MStorageManager$ISOStateChangedStorageEvent;->path:Ljava/lang/String;

    iget v4, v1, Lcom/mstar/android/storage/MStorageManager$ISOStateChangedStorageEvent;->state:I

    invoke-virtual {v2, v3, v4}, Lcom/mstar/android/storage/OnISOEvnetListener;->onISOStateChange(Ljava/lang/String;I)V

    goto :goto_0

    :cond_1
    const-string v3, "MStorageManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unsupported event "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Landroid/os/Message;->what:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
