.class Lcom/mstar/android/storage/MStorageManager$StorageEvent;
.super Ljava/lang/Object;
.source "MStorageManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/storage/MStorageManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "StorageEvent"
.end annotation


# static fields
.field static final EVENT_ISO_STATE_CHANGED:I = 0x4


# instance fields
.field private mMessage:Landroid/os/Message;

.field final synthetic this$0:Lcom/mstar/android/storage/MStorageManager;


# direct methods
.method public constructor <init>(Lcom/mstar/android/storage/MStorageManager;I)V
    .locals 1
    .param p2    # I

    iput-object p1, p0, Lcom/mstar/android/storage/MStorageManager$StorageEvent;->this$0:Lcom/mstar/android/storage/MStorageManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    iput-object v0, p0, Lcom/mstar/android/storage/MStorageManager$StorageEvent;->mMessage:Landroid/os/Message;

    iget-object v0, p0, Lcom/mstar/android/storage/MStorageManager$StorageEvent;->mMessage:Landroid/os/Message;

    iput p2, v0, Landroid/os/Message;->what:I

    iget-object v0, p0, Lcom/mstar/android/storage/MStorageManager$StorageEvent;->mMessage:Landroid/os/Message;

    iput-object p0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public getMessage()Landroid/os/Message;
    .locals 1

    iget-object v0, p0, Lcom/mstar/android/storage/MStorageManager$StorageEvent;->mMessage:Landroid/os/Message;

    return-object v0
.end method
