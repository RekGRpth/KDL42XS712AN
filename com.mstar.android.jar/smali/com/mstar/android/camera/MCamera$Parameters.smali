.class public Lcom/mstar/android/camera/MCamera$Parameters;
.super Ljava/lang/Object;
.source "MCamera.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/camera/MCamera;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Parameters"
.end annotation


# static fields
.field public static final EN_TRAVELING_CALLBACK_NO_SIGNAL:I = 0x0

.field public static final EN_TRAVELING_CALLBACK_VIDEO_INTERLACE:I = 0x2

.field public static final EN_TRAVELING_CALLBACK_VIDEO_PROGRESSIVE:I = 0x1

.field public static final E_DWIN_CAPTURE_IP:I = 0x3

.field public static final E_DWIN_CAPTURE_OP_ALL:I = 0x1

.field public static final E_DWIN_CAPTURE_OP_VIDEO:I = 0x2

.field public static final E_TRAVELING_2ND_VIDEO_AUTO:I = 0x3

.field public static final E_TRAVELING_2ND_VIDEO_PROGRESSIVE_AUTO:I = 0x4

.field public static final E_TRAVELING_2ND_VIDEO_PROGRESSIVE_PQ:I = 0x5

.field public static final E_TRAVELING_ALL_VIDEO:I = 0x2

.field public static final E_TRAVELING_ALL_VIDEO_WITH_OSD:I = 0x1

.field public static final E_TRAVELING_MEM_FORMAT_ABGR8888:I = 0x2

.field public static final E_TRAVELING_MEM_FORMAT_ARGB8888:I = 0x1

.field public static final E_TRAVELING_MEM_FORMAT_YC420:I = 0x3

.field public static final E_TRAVELING_MEM_FORMAT_YUV422_YUYV:I = 0x0

.field public static final E_TRAVELING_RES_1280_720:I = 0xc

.field public static final E_TRAVELING_RES_176_144:I = 0x7

.field public static final E_TRAVELING_RES_320_176:I = 0xb

.field public static final E_TRAVELING_RES_320_240:I = 0x1

.field public static final E_TRAVELING_RES_480_320:I = 0x5

.field public static final E_TRAVELING_RES_640_368:I = 0xa

.field public static final E_TRAVELING_RES_640_480:I = 0x3

.field public static final E_TRAVELING_RES_720_400:I = 0x9

.field public static final E_TRAVELING_RES_720_480:I = 0x2

.field public static final E_TRAVELING_RES_720_576:I = 0x0

.field public static final E_TRAVELING_SPEED_FAST:I = 0x2

.field public static final E_TRAVELING_SPEED_MIDDLE:I = 0x1

.field public static final E_TRAVELING_SPEED_SLOW:I = 0x0

.field public static final KEY_DWIN_CAPTURE_MODE:Ljava/lang/String; = "dwincapture-mode"

.field public static final KEY_MAIN_INPUT_SOURCE:Ljava/lang/String; = "main-input-source"

.field public static final KEY_SUB_INPUT_SOURCE:Ljava/lang/String; = "sub-input-source"

.field public static final KEY_TRAVELING_MEM_FORMAT:Ljava/lang/String; = "traveling-mem-format"

.field public static final KEY_TRAVELING_MODE:Ljava/lang/String; = "traveling-mode"

.field public static final KEY_TRAVELING_RES:Ljava/lang/String; = "traveling-res"

.field public static final KEY_TRAVELING_SPEED:Ljava/lang/String; = "traveling-speed"

.field public static final MAPI_INPUT_SOURCE_ATV:I = 0x1

.field public static final MAPI_INPUT_SOURCE_CVBS:I = 0x2

.field public static final MAPI_INPUT_SOURCE_CVBS2:I = 0x3

.field public static final MAPI_INPUT_SOURCE_CVBS3:I = 0x4

.field public static final MAPI_INPUT_SOURCE_CVBS4:I = 0x5

.field public static final MAPI_INPUT_SOURCE_CVBS5:I = 0x6

.field public static final MAPI_INPUT_SOURCE_CVBS6:I = 0x7

.field public static final MAPI_INPUT_SOURCE_CVBS7:I = 0x8

.field public static final MAPI_INPUT_SOURCE_CVBS8:I = 0x9

.field public static final MAPI_INPUT_SOURCE_CVBS_MAX:I = 0xa

.field public static final MAPI_INPUT_SOURCE_DTV:I = 0x1c

.field public static final MAPI_INPUT_SOURCE_DTV2:I = 0x25

.field public static final MAPI_INPUT_SOURCE_DVI:I = 0x1d

.field public static final MAPI_INPUT_SOURCE_DVI2:I = 0x1e

.field public static final MAPI_INPUT_SOURCE_DVI3:I = 0x1f

.field public static final MAPI_INPUT_SOURCE_DVI4:I = 0x20

.field public static final MAPI_INPUT_SOURCE_DVI_MAX:I = 0x21

.field public static final MAPI_INPUT_SOURCE_HDMI:I = 0x17

.field public static final MAPI_INPUT_SOURCE_HDMI2:I = 0x18

.field public static final MAPI_INPUT_SOURCE_HDMI3:I = 0x19

.field public static final MAPI_INPUT_SOURCE_HDMI4:I = 0x1a

.field public static final MAPI_INPUT_SOURCE_HDMI_MAX:I = 0x1b

.field public static final MAPI_INPUT_SOURCE_JPEG:I = 0x24

.field public static final MAPI_INPUT_SOURCE_KTV:I = 0x23

.field public static final MAPI_INPUT_SOURCE_SCART:I = 0x14

.field public static final MAPI_INPUT_SOURCE_SCART2:I = 0x15

.field public static final MAPI_INPUT_SOURCE_SCART_MAX:I = 0x16

.field public static final MAPI_INPUT_SOURCE_STORAGE:I = 0x22

.field public static final MAPI_INPUT_SOURCE_STORAGE2:I = 0x26

.field public static final MAPI_INPUT_SOURCE_SVIDEO:I = 0xb

.field public static final MAPI_INPUT_SOURCE_SVIDEO2:I = 0xc

.field public static final MAPI_INPUT_SOURCE_SVIDEO3:I = 0xd

.field public static final MAPI_INPUT_SOURCE_SVIDEO4:I = 0xe

.field public static final MAPI_INPUT_SOURCE_SVIDEO_MAX:I = 0xf

.field public static final MAPI_INPUT_SOURCE_VGA:I = 0x0

.field public static final MAPI_INPUT_SOURCE_YPBPR:I = 0x10

.field public static final MAPI_INPUT_SOURCE_YPBPR2:I = 0x11

.field public static final MAPI_INPUT_SOURCE_YPBPR3:I = 0x12

.field public static final MAPI_INPUT_SOURCE_YPBPR_MAX:I = 0x13


# instance fields
.field final synthetic this$0:Lcom/mstar/android/camera/MCamera;


# direct methods
.method public constructor <init>(Lcom/mstar/android/camera/MCamera;)V
    .locals 0

    iput-object p1, p0, Lcom/mstar/android/camera/MCamera$Parameters;->this$0:Lcom/mstar/android/camera/MCamera;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
