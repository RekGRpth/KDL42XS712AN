.class public Lcom/mstar/android/wifi/MWifiManager;
.super Ljava/lang/Object;
.source "MWifiManager.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "MWifiManager"

.field public static final WIFI_DEVICE_ADDED_ACTION:Ljava/lang/String; = "com.mstar.android.wifi.device.added"

.field public static final WIFI_DEVICE_REMOVED_ACTION:Ljava/lang/String; = "com.mstar.android.wifi.device.removed"

.field static mInstance:Lcom/mstar/android/wifi/MWifiManager;

.field static final mInstanceSync:Ljava/lang/Object;


# instance fields
.field mService:Landroid/net/wifi/IWifiManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/mstar/android/wifi/MWifiManager;->mInstanceSync:Ljava/lang/Object;

    const/4 v0, 0x0

    sput-object v0, Lcom/mstar/android/wifi/MWifiManager;->mInstance:Lcom/mstar/android/wifi/MWifiManager;

    return-void
.end method

.method private constructor <init>(Landroid/net/wifi/IWifiManager;)V
    .locals 1
    .param p1    # Landroid/net/wifi/IWifiManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mstar/android/wifi/MWifiManager;->mService:Landroid/net/wifi/IWifiManager;

    iput-object p1, p0, Lcom/mstar/android/wifi/MWifiManager;->mService:Landroid/net/wifi/IWifiManager;

    return-void
.end method

.method public static getInstance()Lcom/mstar/android/wifi/MWifiManager;
    .locals 4

    sget-object v1, Lcom/mstar/android/wifi/MWifiManager;->mInstance:Lcom/mstar/android/wifi/MWifiManager;

    if-nez v1, :cond_1

    sget-object v2, Lcom/mstar/android/wifi/MWifiManager;->mInstanceSync:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    sget-object v1, Lcom/mstar/android/wifi/MWifiManager;->mInstance:Lcom/mstar/android/wifi/MWifiManager;

    if-nez v1, :cond_0

    const-string v1, "wifi"

    invoke-static {v1}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    new-instance v1, Lcom/mstar/android/wifi/MWifiManager;

    invoke-static {v0}, Landroid/net/wifi/IWifiManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/wifi/IWifiManager;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/mstar/android/wifi/MWifiManager;-><init>(Landroid/net/wifi/IWifiManager;)V

    sput-object v1, Lcom/mstar/android/wifi/MWifiManager;->mInstance:Lcom/mstar/android/wifi/MWifiManager;

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    sget-object v1, Lcom/mstar/android/wifi/MWifiManager;->mInstance:Lcom/mstar/android/wifi/MWifiManager;

    return-object v1

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method


# virtual methods
.method public isWifiDeviceExist()Z
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/android/wifi/MWifiManager;->mService:Landroid/net/wifi/IWifiManager;

    invoke-interface {v1}, Landroid/net/wifi/IWifiManager;->isWifiDeviceExist()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isWifiDeviceSupportP2p()Z
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/android/wifi/MWifiManager;->mService:Landroid/net/wifi/IWifiManager;

    invoke-interface {v1}, Landroid/net/wifi/IWifiManager;->isWifiDeviceSupportP2p()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isWifiDeviceSupportSoftap()Z
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/android/wifi/MWifiManager;->mService:Landroid/net/wifi/IWifiManager;

    invoke-interface {v1}, Landroid/net/wifi/IWifiManager;->isWifiDeviceSupportSoftap()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isWifiDeviceSupportWps()Z
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/android/wifi/MWifiManager;->mService:Landroid/net/wifi/IWifiManager;

    invoke-interface {v1}, Landroid/net/wifi/IWifiManager;->isWifiDeviceSupportWps()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public numOfWifiDeviceExist()I
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/android/wifi/MWifiManager;->mService:Landroid/net/wifi/IWifiManager;

    invoke-interface {v1}, Landroid/net/wifi/IWifiManager;->numOfWifiDeviceExist()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0
.end method
