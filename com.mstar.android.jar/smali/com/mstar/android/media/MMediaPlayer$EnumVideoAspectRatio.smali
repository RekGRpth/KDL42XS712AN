.class public final enum Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;
.super Ljava/lang/Enum;
.source "MMediaPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/media/MMediaPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumVideoAspectRatio"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

.field public static final enum E_VIDEO_ASPECT_RATIO_16X9:Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

.field public static final enum E_VIDEO_ASPECT_RATIO_16X9_COMBIND:Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

.field public static final enum E_VIDEO_ASPECT_RATIO_16X9_PAN_SCAN:Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

.field public static final enum E_VIDEO_ASPECT_RATIO_16X9_PILLARBOX:Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

.field public static final enum E_VIDEO_ASPECT_RATIO_4X3:Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

.field public static final enum E_VIDEO_ASPECT_RATIO_4X3_COMBIND:Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

.field public static final enum E_VIDEO_ASPECT_RATIO_4X3_LETTER_BOX:Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

.field public static final enum E_VIDEO_ASPECT_RATIO_4X3_PAN_SCAN:Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

.field public static final enum E_VIDEO_ASPECT_RATIO_AUTO:Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    const-string v1, "E_VIDEO_ASPECT_RATIO_AUTO"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;->E_VIDEO_ASPECT_RATIO_AUTO:Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    const-string v1, "E_VIDEO_ASPECT_RATIO_4X3"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;->E_VIDEO_ASPECT_RATIO_4X3:Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    const-string v1, "E_VIDEO_ASPECT_RATIO_16X9"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;->E_VIDEO_ASPECT_RATIO_16X9:Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    const-string v1, "E_VIDEO_ASPECT_RATIO_16X9_PILLARBOX"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;->E_VIDEO_ASPECT_RATIO_16X9_PILLARBOX:Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    const-string v1, "E_VIDEO_ASPECT_RATIO_4X3_PAN_SCAN"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;->E_VIDEO_ASPECT_RATIO_4X3_PAN_SCAN:Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    const-string v1, "E_VIDEO_ASPECT_RATIO_4X3_LETTER_BOX"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;->E_VIDEO_ASPECT_RATIO_4X3_LETTER_BOX:Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    const-string v1, "E_VIDEO_ASPECT_RATIO_16X9_PAN_SCAN"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;->E_VIDEO_ASPECT_RATIO_16X9_PAN_SCAN:Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    const-string v1, "E_VIDEO_ASPECT_RATIO_4X3_COMBIND"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;->E_VIDEO_ASPECT_RATIO_4X3_COMBIND:Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    const-string v1, "E_VIDEO_ASPECT_RATIO_16X9_COMBIND"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;->E_VIDEO_ASPECT_RATIO_16X9_COMBIND:Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;->E_VIDEO_ASPECT_RATIO_AUTO:Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;->E_VIDEO_ASPECT_RATIO_4X3:Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;->E_VIDEO_ASPECT_RATIO_16X9:Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;->E_VIDEO_ASPECT_RATIO_16X9_PILLARBOX:Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;->E_VIDEO_ASPECT_RATIO_4X3_PAN_SCAN:Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;->E_VIDEO_ASPECT_RATIO_4X3_LETTER_BOX:Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;->E_VIDEO_ASPECT_RATIO_16X9_PAN_SCAN:Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;->E_VIDEO_ASPECT_RATIO_4X3_COMBIND:Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;->E_VIDEO_ASPECT_RATIO_16X9_COMBIND:Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;->$VALUES:[Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;
    .locals 1

    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;->$VALUES:[Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    invoke-virtual {v0}, [Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    return-object v0
.end method
