.class public Lcom/mstar/android/media/DivxDrmInfo;
.super Ljava/lang/Object;
.source "DivxDrmInfo.java"


# instance fields
.field private mU16CheckSum:I

.field private mU32DrmRentalCode:I

.field private mU32DrmRentalFile:I

.field private mU32DrmRentalLimit:I

.field private mU32DrmRentalUseCount:I

.field private mU8DrmAuthorization:I

.field private mU8DrmFileFormat:I

.field private mU8DrmRentalStatus:I


# direct methods
.method public constructor <init>(Landroid/media/Metadata;)V
    .locals 7
    .param p1    # Landroid/media/Metadata;

    const/16 v6, 0x34

    const/16 v5, 0x33

    const/16 v4, 0x32

    const/16 v2, 0x31

    const/4 v3, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "JAVA, Enter DivxDrmInfo constructure"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Landroid/media/Metadata;->has(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v2}, Landroid/media/Metadata;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/mstar/android/media/DivxDrmInfo;->mU32DrmRentalCode:I

    :goto_0
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mU32DrmRentalCode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mstar/android/media/DivxDrmInfo;->mU32DrmRentalCode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {p1, v4}, Landroid/media/Metadata;->has(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, v4}, Landroid/media/Metadata;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/mstar/android/media/DivxDrmInfo;->mU32DrmRentalFile:I

    :goto_1
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mU32DrmRentalFile : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mstar/android/media/DivxDrmInfo;->mU32DrmRentalFile:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {p1, v5}, Landroid/media/Metadata;->has(I)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1, v5}, Landroid/media/Metadata;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/mstar/android/media/DivxDrmInfo;->mU32DrmRentalLimit:I

    :goto_2
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mU32DrmRentalLimit : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mstar/android/media/DivxDrmInfo;->mU32DrmRentalLimit:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {p1, v6}, Landroid/media/Metadata;->has(I)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1, v6}, Landroid/media/Metadata;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/mstar/android/media/DivxDrmInfo;->mU32DrmRentalUseCount:I

    :goto_3
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mU32DrmRentalUseCount : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mstar/android/media/DivxDrmInfo;->mU32DrmRentalUseCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/16 v0, 0x35

    invoke-virtual {p1, v0}, Landroid/media/Metadata;->has(I)Z

    move-result v0

    if-eqz v0, :cond_4

    const/16 v0, 0x35

    invoke-virtual {p1, v0}, Landroid/media/Metadata;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/mstar/android/media/DivxDrmInfo;->mU16CheckSum:I

    :goto_4
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mU16CheckSum : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mstar/android/media/DivxDrmInfo;->mU16CheckSum:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/16 v0, 0x36

    invoke-virtual {p1, v0}, Landroid/media/Metadata;->has(I)Z

    move-result v0

    if-eqz v0, :cond_5

    const/16 v0, 0x36

    invoke-virtual {p1, v0}, Landroid/media/Metadata;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/mstar/android/media/DivxDrmInfo;->mU8DrmRentalStatus:I

    :goto_5
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mU8DrmRentalStatus : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mstar/android/media/DivxDrmInfo;->mU8DrmRentalStatus:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Landroid/media/Metadata;->has(I)Z

    move-result v0

    if-eqz v0, :cond_6

    const/16 v0, 0x37

    invoke-virtual {p1, v0}, Landroid/media/Metadata;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/mstar/android/media/DivxDrmInfo;->mU8DrmFileFormat:I

    :goto_6
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mU8DrmFileFormat : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mstar/android/media/DivxDrmInfo;->mU8DrmFileFormat:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/16 v0, 0x38

    invoke-virtual {p1, v0}, Landroid/media/Metadata;->has(I)Z

    move-result v0

    if-eqz v0, :cond_7

    const/16 v0, 0x38

    invoke-virtual {p1, v0}, Landroid/media/Metadata;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/mstar/android/media/DivxDrmInfo;->mU8DrmAuthorization:I

    :goto_7
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mU8DrmAuthorization : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mstar/android/media/DivxDrmInfo;->mU8DrmAuthorization:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "JAVA, Exit DivxDrmInfo constructure"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void

    :cond_0
    iput v3, p0, Lcom/mstar/android/media/DivxDrmInfo;->mU32DrmRentalCode:I

    goto/16 :goto_0

    :cond_1
    iput v3, p0, Lcom/mstar/android/media/DivxDrmInfo;->mU32DrmRentalFile:I

    goto/16 :goto_1

    :cond_2
    iput v3, p0, Lcom/mstar/android/media/DivxDrmInfo;->mU32DrmRentalLimit:I

    goto/16 :goto_2

    :cond_3
    iput v3, p0, Lcom/mstar/android/media/DivxDrmInfo;->mU32DrmRentalUseCount:I

    goto/16 :goto_3

    :cond_4
    iput v3, p0, Lcom/mstar/android/media/DivxDrmInfo;->mU16CheckSum:I

    goto/16 :goto_4

    :cond_5
    iput v3, p0, Lcom/mstar/android/media/DivxDrmInfo;->mU8DrmRentalStatus:I

    goto/16 :goto_5

    :cond_6
    iput v3, p0, Lcom/mstar/android/media/DivxDrmInfo;->mU8DrmFileFormat:I

    goto :goto_6

    :cond_7
    iput v3, p0, Lcom/mstar/android/media/DivxDrmInfo;->mU8DrmAuthorization:I

    goto :goto_7
.end method


# virtual methods
.method public GetDrmAuthorization()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/media/DivxDrmInfo;->mU8DrmAuthorization:I

    return v0
.end method

.method public GetDrmCheckSum()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/media/DivxDrmInfo;->mU16CheckSum:I

    return v0
.end method

.method public GetDrmFileFormat()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/media/DivxDrmInfo;->mU8DrmFileFormat:I

    return v0
.end method

.method public GetDrmRentalCode()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/media/DivxDrmInfo;->mU32DrmRentalCode:I

    return v0
.end method

.method public GetDrmRentalFile()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/media/DivxDrmInfo;->mU32DrmRentalFile:I

    return v0
.end method

.method public GetDrmRentalLimit()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/media/DivxDrmInfo;->mU32DrmRentalLimit:I

    return v0
.end method

.method public GetDrmRentalStatus()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/media/DivxDrmInfo;->mU8DrmRentalStatus:I

    return v0
.end method

.method public GetDrmRentalUseCount()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/media/DivxDrmInfo;->mU32DrmRentalUseCount:I

    return v0
.end method
