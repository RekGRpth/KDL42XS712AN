.class public final enum Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;
.super Ljava/lang/Enum;
.source "MMediaPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/media/MMediaPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EN_DATASOURCE_MEDIA_FORMAT_TYPE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

.field public static final enum E_DATASOURCE_MEDIA_FORMAT_TYPE_ASF:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

.field public static final enum E_DATASOURCE_MEDIA_FORMAT_TYPE_AVI:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

.field public static final enum E_DATASOURCE_MEDIA_FORMAT_TYPE_ESDATA:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

.field public static final enum E_DATASOURCE_MEDIA_FORMAT_TYPE_FLV:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

.field public static final enum E_DATASOURCE_MEDIA_FORMAT_TYPE_MAX:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

.field public static final enum E_DATASOURCE_MEDIA_FORMAT_TYPE_MKV:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

.field public static final enum E_DATASOURCE_MEDIA_FORMAT_TYPE_MP4:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

.field public static final enum E_DATASOURCE_MEDIA_FORMAT_TYPE_MPG:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

.field public static final enum E_DATASOURCE_MEDIA_FORMAT_TYPE_RM:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

.field public static final enum E_DATASOURCE_MEDIA_FORMAT_TYPE_TS:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

.field public static final enum E_DATASOURCE_MEDIA_FORMAT_TYPE_UNKNOWN:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    const-string v1, "E_DATASOURCE_MEDIA_FORMAT_TYPE_UNKNOWN"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->E_DATASOURCE_MEDIA_FORMAT_TYPE_UNKNOWN:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    const-string v1, "E_DATASOURCE_MEDIA_FORMAT_TYPE_AVI"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->E_DATASOURCE_MEDIA_FORMAT_TYPE_AVI:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    const-string v1, "E_DATASOURCE_MEDIA_FORMAT_TYPE_MP4"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->E_DATASOURCE_MEDIA_FORMAT_TYPE_MP4:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    const-string v1, "E_DATASOURCE_MEDIA_FORMAT_TYPE_MKV"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->E_DATASOURCE_MEDIA_FORMAT_TYPE_MKV:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    const-string v1, "E_DATASOURCE_MEDIA_FORMAT_TYPE_ASF"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->E_DATASOURCE_MEDIA_FORMAT_TYPE_ASF:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    const-string v1, "E_DATASOURCE_MEDIA_FORMAT_TYPE_RM"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->E_DATASOURCE_MEDIA_FORMAT_TYPE_RM:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    const-string v1, "E_DATASOURCE_MEDIA_FORMAT_TYPE_TS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->E_DATASOURCE_MEDIA_FORMAT_TYPE_TS:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    const-string v1, "E_DATASOURCE_MEDIA_FORMAT_TYPE_MPG"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->E_DATASOURCE_MEDIA_FORMAT_TYPE_MPG:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    const-string v1, "E_DATASOURCE_MEDIA_FORMAT_TYPE_FLV"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->E_DATASOURCE_MEDIA_FORMAT_TYPE_FLV:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    const-string v1, "E_DATASOURCE_MEDIA_FORMAT_TYPE_ESDATA"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->E_DATASOURCE_MEDIA_FORMAT_TYPE_ESDATA:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    const-string v1, "E_DATASOURCE_MEDIA_FORMAT_TYPE_MAX"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->E_DATASOURCE_MEDIA_FORMAT_TYPE_MAX:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    const/16 v0, 0xb

    new-array v0, v0, [Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->E_DATASOURCE_MEDIA_FORMAT_TYPE_UNKNOWN:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->E_DATASOURCE_MEDIA_FORMAT_TYPE_AVI:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->E_DATASOURCE_MEDIA_FORMAT_TYPE_MP4:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->E_DATASOURCE_MEDIA_FORMAT_TYPE_MKV:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->E_DATASOURCE_MEDIA_FORMAT_TYPE_ASF:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->E_DATASOURCE_MEDIA_FORMAT_TYPE_RM:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->E_DATASOURCE_MEDIA_FORMAT_TYPE_TS:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->E_DATASOURCE_MEDIA_FORMAT_TYPE_MPG:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->E_DATASOURCE_MEDIA_FORMAT_TYPE_FLV:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->E_DATASOURCE_MEDIA_FORMAT_TYPE_ESDATA:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->E_DATASOURCE_MEDIA_FORMAT_TYPE_MAX:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->$VALUES:[Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;
    .locals 1

    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->$VALUES:[Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    invoke-virtual {v0}, [Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    return-object v0
.end method
