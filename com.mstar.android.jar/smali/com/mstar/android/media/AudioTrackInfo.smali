.class public Lcom/mstar/android/media/AudioTrackInfo;
.super Ljava/lang/Object;
.source "AudioTrackInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/media/AudioTrackInfo$1;,
        Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "AudioTrackInfo"


# instance fields
.field private mAlbum:Ljava/lang/String;

.field private mArtist:Ljava/lang/String;

.field private mBiteRate:I

.field private mCodecID:I

.field private mCurrentPlayTime:I

.field private mLanguageType:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

.field private mSampleRate:I

.field private mTitle:Ljava/lang/String;

.field private mTotalPlayTime:I

.field private mYear:I


# direct methods
.method public constructor <init>(ZLandroid/media/Metadata;[Ljava/lang/String;)V
    .locals 12
    .param p1    # Z
    .param p2    # Landroid/media/Metadata;
    .param p3    # [Ljava/lang/String;

    const/16 v8, 0x14

    const/16 v7, 0xe

    const/4 v11, 0x0

    const/4 v10, 0x0

    const/4 v9, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_9

    invoke-virtual {p2, v8}, Landroid/media/Metadata;->has(I)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {p2, v8}, Landroid/media/Metadata;->getInt(I)I

    move-result v6

    iput v6, p0, Lcom/mstar/android/media/AudioTrackInfo;->mBiteRate:I

    :goto_0
    const/16 v6, 0x21

    invoke-virtual {p2, v6}, Landroid/media/Metadata;->has(I)Z

    move-result v6

    if-eqz v6, :cond_7

    const/16 v6, 0x21

    invoke-virtual {p2, v6}, Landroid/media/Metadata;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/net/IDN;->toUnicode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1

    const-string v6, "AudioTrackInfo"

    const-string v7, "The year information is null."

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput v10, p0, Lcom/mstar/android/media/AudioTrackInfo;->mYear:I

    :goto_1
    const/16 v6, 0x17

    invoke-virtual {p2, v6}, Landroid/media/Metadata;->has(I)Z

    move-result v6

    if-eqz v6, :cond_8

    const/16 v6, 0x17

    invoke-virtual {p2, v6}, Landroid/media/Metadata;->getInt(I)I

    move-result v6

    iput v6, p0, Lcom/mstar/android/media/AudioTrackInfo;->mSampleRate:I

    :goto_2
    aget-object v6, p3, v10

    iput-object v6, p0, Lcom/mstar/android/media/AudioTrackInfo;->mTitle:Ljava/lang/String;

    const/4 v6, 0x1

    aget-object v6, p3, v6

    iput-object v6, p0, Lcom/mstar/android/media/AudioTrackInfo;->mAlbum:Ljava/lang/String;

    const/4 v6, 0x2

    aget-object v6, p3, v6

    iput-object v6, p0, Lcom/mstar/android/media/AudioTrackInfo;->mArtist:Ljava/lang/String;

    iput v9, p0, Lcom/mstar/android/media/AudioTrackInfo;->mTotalPlayTime:I

    iput v9, p0, Lcom/mstar/android/media/AudioTrackInfo;->mCurrentPlayTime:I

    :goto_3
    const/16 v6, 0x3e

    invoke-virtual {p2, v6}, Landroid/media/Metadata;->has(I)Z

    move-result v6

    if-eqz v6, :cond_c

    const/16 v6, 0x3e

    invoke-virtual {p2, v6}, Landroid/media/Metadata;->getInt(I)I

    move-result v6

    iput v6, p0, Lcom/mstar/android/media/AudioTrackInfo;->mCodecID:I

    :goto_4
    const/16 v6, 0x3d

    invoke-virtual {p2, v6}, Landroid/media/Metadata;->has(I)Z

    move-result v6

    if-eqz v6, :cond_d

    invoke-static {}, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->values()[Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    move-result-object v6

    const/16 v7, 0x3d

    invoke-virtual {p2, v7}, Landroid/media/Metadata;->getInt(I)I

    move-result v7

    aget-object v6, v6, v7

    iput-object v6, p0, Lcom/mstar/android/media/AudioTrackInfo;->mLanguageType:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    :goto_5
    return-void

    :cond_0
    iput v9, p0, Lcom/mstar/android/media/AudioTrackInfo;->mBiteRate:I

    goto :goto_0

    :cond_1
    const-string v1, ""

    const/4 v2, 0x0

    :goto_6
    if-ge v2, v3, :cond_5

    invoke-virtual {v5, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v6, 0x30

    if-lt v0, v6, :cond_3

    const/16 v6, 0x39

    if-gt v0, v6, :cond_3

    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_2
    :goto_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    :cond_3
    add-int/lit8 v6, v2, 0x2

    if-ge v6, v3, :cond_4

    add-int/lit8 v6, v2, 0x2

    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    const/16 v7, 0x30

    if-le v6, v7, :cond_4

    add-int/lit8 v6, v2, 0x2

    invoke-virtual {v5, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    const/16 v7, 0x39

    if-lt v6, v7, :cond_2

    :cond_4
    const-string v6, "0"

    invoke-virtual {v1, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_7

    :cond_5
    const/16 v4, 0x8

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v6

    if-le v4, v6, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    :cond_6
    invoke-virtual {v1, v10, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, p0, Lcom/mstar/android/media/AudioTrackInfo;->mYear:I

    const-string v6, "AudioTrackInfo"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mYear:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/mstar/android/media/AudioTrackInfo;->mYear:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_7
    iput v10, p0, Lcom/mstar/android/media/AudioTrackInfo;->mYear:I

    goto/16 :goto_1

    :cond_8
    iput v9, p0, Lcom/mstar/android/media/AudioTrackInfo;->mSampleRate:I

    goto/16 :goto_2

    :cond_9
    invoke-virtual {p2, v7}, Landroid/media/Metadata;->has(I)Z

    move-result v6

    if-eqz v6, :cond_a

    invoke-virtual {p2, v7}, Landroid/media/Metadata;->getInt(I)I

    move-result v6

    iput v6, p0, Lcom/mstar/android/media/AudioTrackInfo;->mTotalPlayTime:I

    :goto_8
    const/16 v6, 0x22

    invoke-virtual {p2, v6}, Landroid/media/Metadata;->has(I)Z

    move-result v6

    if-eqz v6, :cond_b

    const/16 v6, 0x22

    invoke-virtual {p2, v6}, Landroid/media/Metadata;->getInt(I)I

    move-result v6

    iput v6, p0, Lcom/mstar/android/media/AudioTrackInfo;->mCurrentPlayTime:I

    :goto_9
    iput v9, p0, Lcom/mstar/android/media/AudioTrackInfo;->mBiteRate:I

    iput v10, p0, Lcom/mstar/android/media/AudioTrackInfo;->mYear:I

    iput v9, p0, Lcom/mstar/android/media/AudioTrackInfo;->mSampleRate:I

    iput-object v11, p0, Lcom/mstar/android/media/AudioTrackInfo;->mTitle:Ljava/lang/String;

    iput-object v11, p0, Lcom/mstar/android/media/AudioTrackInfo;->mAlbum:Ljava/lang/String;

    iput-object v11, p0, Lcom/mstar/android/media/AudioTrackInfo;->mArtist:Ljava/lang/String;

    goto/16 :goto_3

    :cond_a
    iput v9, p0, Lcom/mstar/android/media/AudioTrackInfo;->mTotalPlayTime:I

    goto :goto_8

    :cond_b
    iput v9, p0, Lcom/mstar/android/media/AudioTrackInfo;->mCurrentPlayTime:I

    goto :goto_9

    :cond_c
    iput v9, p0, Lcom/mstar/android/media/AudioTrackInfo;->mCodecID:I

    goto/16 :goto_4

    :cond_d
    iput-object v11, p0, Lcom/mstar/android/media/AudioTrackInfo;->mLanguageType:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    goto/16 :goto_5
.end method


# virtual methods
.method public getAlbum()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/mstar/android/media/AudioTrackInfo;->mAlbum:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "AudioTrackInfo"

    const-string v1, "Only audio type can get Album and local audio couldn\'t get infomation with the function."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/mstar/android/media/AudioTrackInfo;->mAlbum:Ljava/lang/String;

    return-object v0
.end method

.method public getArtist()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/mstar/android/media/AudioTrackInfo;->mArtist:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "AudioTrackInfo"

    const-string v1, "Only audio type can get Artist and local auido couldn\'t get infomation with the function."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/mstar/android/media/AudioTrackInfo;->mArtist:Ljava/lang/String;

    return-object v0
.end method

.method public getAudioLanguageType()Ljava/lang/String;
    .locals 3

    iget-object v1, p0, Lcom/mstar/android/media/AudioTrackInfo;->mLanguageType:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    sget-object v1, Lcom/mstar/android/media/AudioTrackInfo$1;->$SwitchMap$com$mstar$android$media$AudioTrackInfo$MEDIA_AUDIO_LANGUAGE:[I

    iget-object v2, p0, Lcom/mstar/android/media/AudioTrackInfo;->mLanguageType:Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;

    invoke-virtual {v2}, Lcom/mstar/android/media/AudioTrackInfo$MEDIA_AUDIO_LANGUAGE;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    const-string v0, "undefined"

    goto :goto_0

    :pswitch_0
    const-string v0, "German"

    goto :goto_0

    :pswitch_1
    const-string v0, "English"

    goto :goto_0

    :pswitch_2
    const-string v0, "Spanish"

    goto :goto_0

    :pswitch_3
    const-string v0, "Greek"

    goto :goto_0

    :pswitch_4
    const-string v0, "French"

    goto :goto_0

    :pswitch_5
    const-string v0, "Croatian"

    goto :goto_0

    :pswitch_6
    const-string v0, "Italian"

    goto :goto_0

    :pswitch_7
    const-string v0, "Dutch"

    goto :goto_0

    :pswitch_8
    const-string v0, "Polish"

    goto :goto_0

    :pswitch_9
    const-string v0, "Portuguese"

    goto :goto_0

    :pswitch_a
    const-string v0, "Russian"

    goto :goto_0

    :pswitch_b
    const-string v0, "Romanian"

    goto :goto_0

    :pswitch_c
    const-string v0, "Swedish"

    goto :goto_0

    :pswitch_d
    const-string v0, "Arabic"

    goto :goto_0

    :pswitch_e
    const-string v0, "Chinese"

    goto :goto_0

    :pswitch_f
    const-string v0, "Japanese"

    goto :goto_0

    :pswitch_10
    const-string v0, "Korean"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method public getBiteRate()I
    .locals 2

    const/4 v0, -0x1

    iget v1, p0, Lcom/mstar/android/media/AudioTrackInfo;->mBiteRate:I

    if-ne v0, v1, :cond_0

    const-string v0, "AudioTrackInfo"

    const-string v1, "Only audio type can get BiteRate and local audio couldn\'t get infomation with the function."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v0, p0, Lcom/mstar/android/media/AudioTrackInfo;->mBiteRate:I

    return v0
.end method

.method public getCodecID()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/media/AudioTrackInfo;->mCodecID:I

    return v0
.end method

.method public getCurrentPlayTime()I
    .locals 2

    const/4 v0, -0x1

    iget v1, p0, Lcom/mstar/android/media/AudioTrackInfo;->mCurrentPlayTime:I

    if-ne v0, v1, :cond_0

    const-string v0, "AudioTrackInfo"

    const-string v1, "Only vedio type can get CurrentPlayTime."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v0, p0, Lcom/mstar/android/media/AudioTrackInfo;->mCurrentPlayTime:I

    return v0
.end method

.method public getSampleRate()I
    .locals 2

    const/4 v0, -0x1

    iget v1, p0, Lcom/mstar/android/media/AudioTrackInfo;->mSampleRate:I

    if-ne v0, v1, :cond_0

    const-string v0, "AudioTrackInfo"

    const-string v1, "Only audio type can get SampleRate and local audio couldn\'t get infomation with the function."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v0, p0, Lcom/mstar/android/media/AudioTrackInfo;->mSampleRate:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/mstar/android/media/AudioTrackInfo;->mTitle:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, "AudioTrackInfo"

    const-string v1, "Only audio type can get Title and local audio couldn\'t get infomation with the function."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/mstar/android/media/AudioTrackInfo;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getTotalPlayTime()I
    .locals 2

    const/4 v0, -0x1

    iget v1, p0, Lcom/mstar/android/media/AudioTrackInfo;->mTotalPlayTime:I

    if-ne v0, v1, :cond_0

    const-string v0, "AudioTrackInfo"

    const-string v1, "Only vedio type can get TotalPlayTime."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v0, p0, Lcom/mstar/android/media/AudioTrackInfo;->mTotalPlayTime:I

    return v0
.end method

.method public getYear()I
    .locals 2

    iget v0, p0, Lcom/mstar/android/media/AudioTrackInfo;->mYear:I

    if-nez v0, :cond_0

    const-string v0, "AudioTrackInfo"

    const-string v1, "Only audio type can get Year and local audio couldn\'t get infomation with the function."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v0, p0, Lcom/mstar/android/media/AudioTrackInfo;->mYear:I

    return v0
.end method
