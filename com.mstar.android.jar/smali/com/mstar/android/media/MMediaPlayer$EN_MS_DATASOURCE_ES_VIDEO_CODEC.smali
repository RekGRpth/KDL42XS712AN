.class public final enum Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;
.super Ljava/lang/Enum;
.source "MMediaPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/media/MMediaPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EN_MS_DATASOURCE_ES_VIDEO_CODEC"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

.field public static final enum E_DATASOURCE_ES_VIDEO_CODEC_AVS:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

.field public static final enum E_DATASOURCE_ES_VIDEO_CODEC_DIVX:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

.field public static final enum E_DATASOURCE_ES_VIDEO_CODEC_DIVX3:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

.field public static final enum E_DATASOURCE_ES_VIDEO_CODEC_DIVX4:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

.field public static final enum E_DATASOURCE_ES_VIDEO_CODEC_FLV:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

.field public static final enum E_DATASOURCE_ES_VIDEO_CODEC_FOURCCEX:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

.field public static final enum E_DATASOURCE_ES_VIDEO_CODEC_H263:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

.field public static final enum E_DATASOURCE_ES_VIDEO_CODEC_H264:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

.field public static final enum E_DATASOURCE_ES_VIDEO_CODEC_MJPEG:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

.field public static final enum E_DATASOURCE_ES_VIDEO_CODEC_MPEG1VIDEO:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

.field public static final enum E_DATASOURCE_ES_VIDEO_CODEC_MPEG2VIDEO:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

.field public static final enum E_DATASOURCE_ES_VIDEO_CODEC_MPEG4:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

.field public static final enum E_DATASOURCE_ES_VIDEO_CODEC_RV30:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

.field public static final enum E_DATASOURCE_ES_VIDEO_CODEC_RV40:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

.field public static final enum E_DATASOURCE_ES_VIDEO_CODEC_TS:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

.field public static final enum E_DATASOURCE_ES_VIDEO_CODEC_UNKNOW:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

.field public static final enum E_DATASOURCE_ES_VIDEO_CODEC_VC1:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

.field public static final enum E_DATASOURCE_ES_VIDEO_CODEC_WMV3:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    const-string v1, "E_DATASOURCE_ES_VIDEO_CODEC_UNKNOW"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_UNKNOW:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    const-string v1, "E_DATASOURCE_ES_VIDEO_CODEC_MPEG1VIDEO"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_MPEG1VIDEO:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    const-string v1, "E_DATASOURCE_ES_VIDEO_CODEC_MPEG2VIDEO"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_MPEG2VIDEO:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    const-string v1, "E_DATASOURCE_ES_VIDEO_CODEC_MPEG4"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_MPEG4:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    const-string v1, "E_DATASOURCE_ES_VIDEO_CODEC_H263"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_H263:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    const-string v1, "E_DATASOURCE_ES_VIDEO_CODEC_DIVX3"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_DIVX3:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    const-string v1, "E_DATASOURCE_ES_VIDEO_CODEC_DIVX4"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_DIVX4:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    const-string v1, "E_DATASOURCE_ES_VIDEO_CODEC_DIVX"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_DIVX:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    const-string v1, "E_DATASOURCE_ES_VIDEO_CODEC_H264"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_H264:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    const-string v1, "E_DATASOURCE_ES_VIDEO_CODEC_AVS"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_AVS:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    const-string v1, "E_DATASOURCE_ES_VIDEO_CODEC_RV30"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_RV30:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    const-string v1, "E_DATASOURCE_ES_VIDEO_CODEC_RV40"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_RV40:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    const-string v1, "E_DATASOURCE_ES_VIDEO_CODEC_MJPEG"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_MJPEG:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    const-string v1, "E_DATASOURCE_ES_VIDEO_CODEC_VC1"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_VC1:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    const-string v1, "E_DATASOURCE_ES_VIDEO_CODEC_WMV3"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_WMV3:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    const-string v1, "E_DATASOURCE_ES_VIDEO_CODEC_FLV"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_FLV:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    const-string v1, "E_DATASOURCE_ES_VIDEO_CODEC_FOURCCEX"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_FOURCCEX:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    const-string v1, "E_DATASOURCE_ES_VIDEO_CODEC_TS"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_TS:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    const/16 v0, 0x12

    new-array v0, v0, [Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_UNKNOW:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_MPEG1VIDEO:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_MPEG2VIDEO:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_MPEG4:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_H263:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_DIVX3:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_DIVX4:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_DIVX:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_H264:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_AVS:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_RV30:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_RV40:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_MJPEG:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_VC1:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_WMV3:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_FLV:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_FOURCCEX:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->E_DATASOURCE_ES_VIDEO_CODEC_TS:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->$VALUES:[Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;
    .locals 1

    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->$VALUES:[Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    invoke-virtual {v0}, [Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    return-object v0
.end method
