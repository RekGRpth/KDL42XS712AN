.class public final enum Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;
.super Ljava/lang/Enum;
.source "MMediaPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/media/MMediaPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EnumPlayerSeamlessMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;

.field public static final enum E_PLAYER_SEAMLESS_FREEZ:Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;

.field public static final enum E_PLAYER_SEAMLESS_NONE:Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;

.field public static final enum E_PLAYER_SEAMLESS_SMOTH:Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;

    const-string v1, "E_PLAYER_SEAMLESS_NONE"

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;->E_PLAYER_SEAMLESS_NONE:Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;

    const-string v1, "E_PLAYER_SEAMLESS_FREEZ"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;->E_PLAYER_SEAMLESS_FREEZ:Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;

    const-string v1, "E_PLAYER_SEAMLESS_SMOTH"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;->E_PLAYER_SEAMLESS_SMOTH:Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;->E_PLAYER_SEAMLESS_NONE:Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;->E_PLAYER_SEAMLESS_FREEZ:Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;->E_PLAYER_SEAMLESS_SMOTH:Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;

    aput-object v1, v0, v4

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;->$VALUES:[Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;
    .locals 1

    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;->$VALUES:[Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;

    invoke-virtual {v0}, [Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;

    return-object v0
.end method
