.class public final enum Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;
.super Ljava/lang/Enum;
.source "MMediaPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/media/MMediaPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EN_MS_DATASOURCE_ES_AUDIO_CODEC"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

.field public static final enum E_DATASOURCE_ES_AUDIO_CODEC_AAC:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

.field public static final enum E_DATASOURCE_ES_AUDIO_CODEC_AC3:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

.field public static final enum E_DATASOURCE_ES_AUDIO_CODEC_AC3_PLUS:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

.field public static final enum E_DATASOURCE_ES_AUDIO_CODEC_ADPCM:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

.field public static final enum E_DATASOURCE_ES_AUDIO_CODEC_AMR_NB:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

.field public static final enum E_DATASOURCE_ES_AUDIO_CODEC_AMR_WB:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

.field public static final enum E_DATASOURCE_ES_AUDIO_CODEC_COOK:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

.field public static final enum E_DATASOURCE_ES_AUDIO_CODEC_DTS:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

.field public static final enum E_DATASOURCE_ES_AUDIO_CODEC_FLAC:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

.field public static final enum E_DATASOURCE_ES_AUDIO_CODEC_MP3:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

.field public static final enum E_DATASOURCE_ES_AUDIO_CODEC_MPEG:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

.field public static final enum E_DATASOURCE_ES_AUDIO_CODEC_PCM:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

.field public static final enum E_DATASOURCE_ES_AUDIO_CODEC_RAAC:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

.field public static final enum E_DATASOURCE_ES_AUDIO_CODEC_UNKNOW:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

.field public static final enum E_DATASOURCE_ES_AUDIO_CODEC_VORBIS:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

.field public static final enum E_DATASOURCE_ES_AUDIO_CODEC_WMA:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    const-string v1, "E_DATASOURCE_ES_AUDIO_CODEC_UNKNOW"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_UNKNOW:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    const-string v1, "E_DATASOURCE_ES_AUDIO_CODEC_WMA"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_WMA:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    const-string v1, "E_DATASOURCE_ES_AUDIO_CODEC_DTS"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_DTS:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    const-string v1, "E_DATASOURCE_ES_AUDIO_CODEC_MP3"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_MP3:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    const-string v1, "E_DATASOURCE_ES_AUDIO_CODEC_MPEG"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_MPEG:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    const-string v1, "E_DATASOURCE_ES_AUDIO_CODEC_AC3"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_AC3:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    const-string v1, "E_DATASOURCE_ES_AUDIO_CODEC_AC3_PLUS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_AC3_PLUS:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    const-string v1, "E_DATASOURCE_ES_AUDIO_CODEC_AAC"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_AAC:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    const-string v1, "E_DATASOURCE_ES_AUDIO_CODEC_PCM"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_PCM:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    const-string v1, "E_DATASOURCE_ES_AUDIO_CODEC_ADPCM"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_ADPCM:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    const-string v1, "E_DATASOURCE_ES_AUDIO_CODEC_RAAC"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_RAAC:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    const-string v1, "E_DATASOURCE_ES_AUDIO_CODEC_COOK"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_COOK:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    const-string v1, "E_DATASOURCE_ES_AUDIO_CODEC_FLAC"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_FLAC:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    const-string v1, "E_DATASOURCE_ES_AUDIO_CODEC_VORBIS"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_VORBIS:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    const-string v1, "E_DATASOURCE_ES_AUDIO_CODEC_AMR_NB"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_AMR_NB:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    const-string v1, "E_DATASOURCE_ES_AUDIO_CODEC_AMR_WB"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_AMR_WB:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    const/16 v0, 0x10

    new-array v0, v0, [Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_UNKNOW:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_WMA:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_DTS:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_MP3:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_MPEG:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_AC3:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_AC3_PLUS:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_AAC:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_PCM:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_ADPCM:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_RAAC:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_COOK:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_FLAC:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_VORBIS:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_AMR_NB:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->E_DATASOURCE_ES_AUDIO_CODEC_AMR_WB:Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->$VALUES:[Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;
    .locals 1

    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->$VALUES:[Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    invoke-virtual {v0}, [Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    return-object v0
.end method
