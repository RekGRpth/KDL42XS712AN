.class public Lcom/mstar/android/media/MMediaPlayer;
.super Landroid/media/MediaPlayer;
.source "MMediaPlayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/media/MMediaPlayer$1;,
        Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;,
        Lcom/mstar/android/media/MMediaPlayer$EN_MOVIE_THUMBNAIL_FORMAT;,
        Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;,
        Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;,
        Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;,
        Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;,
        Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_CONTENT_TYPE;,
        Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_PLAYER_TYPE;,
        Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;
    }
.end annotation


# static fields
.field private static final DATASOURCE_AP_ANDROID_STREAMING:I = 0x7

.field private static final DATASOURCE_AP_ANDROID_USB:I = 0x6

.field private static final DATASOURCE_AP_DLNA:I = 0x2

.field private static final DATASOURCE_AP_HBBTV:I = 0x3

.field private static final DATASOURCE_AP_NETFLIX:I = 0x1

.field private static final DATASOURCE_AP_NORMAL:I = 0x0

.field private static final DATASOURCE_AP_WEBBROWSER:I = 0x4

.field private static final DATASOURCE_AP_WMDRM10:I = 0x5

.field private static final DATASOURCE_CONTENT_TYPE_ES:I = 0x3

.field private static final DATASOURCE_CONTENT_TYPE_MASS_STORAGE:I = 0x0

.field private static final DATASOURCE_CONTENT_TYPE_NETWORK_STREAM_WITHOUT_SEEK:I = 0x2

.field private static final DATASOURCE_CONTENT_TYPE_NETWORK_STREAM_WITH_SEEK:I = 0x1

.field private static final DATASOURCE_ES_AUDIO_CODEC_AAC:I = 0x6

.field private static final DATASOURCE_ES_AUDIO_CODEC_AC3:I = 0x4

.field private static final DATASOURCE_ES_AUDIO_CODEC_AC3_PLUS:I = 0x5

.field private static final DATASOURCE_ES_AUDIO_CODEC_ADPCM:I = 0x8

.field private static final DATASOURCE_ES_AUDIO_CODEC_AMR_NB:I = 0xd

.field private static final DATASOURCE_ES_AUDIO_CODEC_AMR_WB:I = 0xe

.field private static final DATASOURCE_ES_AUDIO_CODEC_COOK:I = 0xa

.field private static final DATASOURCE_ES_AUDIO_CODEC_DTS:I = 0x1

.field private static final DATASOURCE_ES_AUDIO_CODEC_FLAC:I = 0xb

.field private static final DATASOURCE_ES_AUDIO_CODEC_MP3:I = 0x2

.field private static final DATASOURCE_ES_AUDIO_CODEC_MPEG:I = 0x3

.field private static final DATASOURCE_ES_AUDIO_CODEC_PCM:I = 0x7

.field private static final DATASOURCE_ES_AUDIO_CODEC_RAAC:I = 0x9

.field private static final DATASOURCE_ES_AUDIO_CODEC_UNKNOW:I = -0x1

.field private static final DATASOURCE_ES_AUDIO_CODEC_VORBIS:I = 0xc

.field private static final DATASOURCE_ES_AUDIO_CODEC_WMA:I = 0x0

.field private static final DATASOURCE_ES_VIDEO_CODEC_AVS:I = 0x8

.field private static final DATASOURCE_ES_VIDEO_CODEC_DIVX:I = 0x6

.field private static final DATASOURCE_ES_VIDEO_CODEC_DIVX3:I = 0x4

.field private static final DATASOURCE_ES_VIDEO_CODEC_DIVX4:I = 0x5

.field private static final DATASOURCE_ES_VIDEO_CODEC_FLV:I = 0xe

.field private static final DATASOURCE_ES_VIDEO_CODEC_FOURCCEX:I = 0xf

.field private static final DATASOURCE_ES_VIDEO_CODEC_H263:I = 0x3

.field private static final DATASOURCE_ES_VIDEO_CODEC_H264:I = 0x7

.field private static final DATASOURCE_ES_VIDEO_CODEC_MJPEG:I = 0xb

.field private static final DATASOURCE_ES_VIDEO_CODEC_MPEG1VIDEO:I = 0x0

.field private static final DATASOURCE_ES_VIDEO_CODEC_MPEG2VIDEO:I = 0x1

.field private static final DATASOURCE_ES_VIDEO_CODEC_MPEG4:I = 0x2

.field private static final DATASOURCE_ES_VIDEO_CODEC_RV30:I = 0x9

.field private static final DATASOURCE_ES_VIDEO_CODEC_RV40:I = 0xa

.field private static final DATASOURCE_ES_VIDEO_CODEC_TS:I = 0x10

.field private static final DATASOURCE_ES_VIDEO_CODEC_UNKNOW:I = -0x1

.field private static final DATASOURCE_ES_VIDEO_CODEC_VC1:I = 0xc

.field private static final DATASOURCE_ES_VIDEO_CODEC_WMV3:I = 0xd

.field private static final DATASOURCE_MEDIA_FORMAT_TYPE_ASF:I = 0x3

.field private static final DATASOURCE_MEDIA_FORMAT_TYPE_AVI:I = 0x0

.field private static final DATASOURCE_MEDIA_FORMAT_TYPE_ESDATA:I = 0x8

.field private static final DATASOURCE_MEDIA_FORMAT_TYPE_FLV:I = 0x7

.field private static final DATASOURCE_MEDIA_FORMAT_TYPE_MAX:I = 0x9

.field private static final DATASOURCE_MEDIA_FORMAT_TYPE_MKV:I = 0x2

.field private static final DATASOURCE_MEDIA_FORMAT_TYPE_MP4:I = 0x1

.field private static final DATASOURCE_MEDIA_FORMAT_TYPE_MPG:I = 0x6

.field private static final DATASOURCE_MEDIA_FORMAT_TYPE_RM:I = 0x4

.field private static final DATASOURCE_MEDIA_FORMAT_TYPE_TS:I = 0x5

.field private static final DATASOURCE_MEDIA_FORMAT_TYPE_UNKNOWN:I = -0x1

.field private static final IMEDIA_PLAYER:Ljava/lang/String; = "android.media.IMediaPlayer"

.field private static final INVOKE_ID_ADD_EXTERNAL_SOURCE:I = 0x2

.field private static final INVOKE_ID_ADD_EXTERNAL_SOURCE_FD:I = 0x3

.field private static final INVOKE_ID_DESELECT_TRACK:I = 0x5

.field private static final INVOKE_ID_GET_TRACK_INFO:I = 0x1

.field private static final INVOKE_ID_SELECT_TRACK:I = 0x4

.field private static final INVOKE_ID_SET_VIDEO_SCALE_MODE:I = 0x6

.field private static final KEY_PARAMETER_ACCEPT_NEXT_SEAMLESS:I = 0x7da

.field private static final KEY_PARAMETER_AVCODEC_CHANGED:I = 0x7d8

.field private static final KEY_PARAMETER_CHANGE_PROGRAM:I = 0x7d5

.field private static final KEY_PARAMETER_DATASOURCE_SWITCH:I = 0x7d2

.field private static final KEY_PARAMETER_DEMUX_RESET:I = 0x7d0

.field private static final KEY_PARAMETER_GET_ALL_SUBTITLE_TRACK_INFO:I = 0x7dd

.field private static final KEY_PARAMETER_GET_SUBTITLE_TRACK_INFO:I = 0x7dc

.field private static final KEY_PARAMETER_MEDIA_CREATE_THUMBNAIL_MODE:I = 0x7d6

.field private static final KEY_PARAMETER_PAYLOAD_SHOT:I = 0x7d3

.field private static final KEY_PARAMETER_SET_SEAMLESS_MODE:I = 0x7d1

.field private static final KEY_PARAMETER_SET_SUBTITLE_INDEX:I = 0x7db

.field private static final KEY_PARAMETER_SET_TS_INFO:I = 0x7d9

.field private static final KEY_PARAMETER_SWITCH_TO_PUSH_DATA_MODE:I = 0x7d4

.field private static final KEY_PARAMETER_VIDEO_ONLY_MODE:I = 0x7d7

.field public static final MEDIA_INFO_AUDIO_UNSUPPORT:I = 0x3ea

.field public static final MEDIA_INFO_NETWORK_CACHE_PERCENT:I = 0x3e9

.field public static final MEDIA_INFO_SUBTITLE_UPDATA:I = 0x3e8

.field public static final MEDIA_INFO_VIDEO_UNSUPPORT:I = 0x3eb

.field public static final MEDIA_TRACK_TYPE_TIMEDBITMAP:I = 0x4

.field private static final MS_DATASOURCE_PLAYER_MOVIE:I = 0x1

.field private static final MS_DATASOURCE_PLAYER_MUSIC:I = 0x2

.field private static final MS_DATASOURCE_PLAYER_UNKNOW:I = 0x0

.field private static final TAG:Ljava/lang/String; = "MMediaPlayer"

.field private static final VIDEO_ASPECT_RATIO_16X9:I = 0x2

.field private static final VIDEO_ASPECT_RATIO_16X9_COMBIND:I = 0x8

.field private static final VIDEO_ASPECT_RATIO_16X9_PAN_SCAN:I = 0x6

.field private static final VIDEO_ASPECT_RATIO_16X9_PILLARBOX:I = 0x3

.field private static final VIDEO_ASPECT_RATIO_4X3:I = 0x1

.field private static final VIDEO_ASPECT_RATIO_4X3_COMBIND:I = 0x7

.field private static final VIDEO_ASPECT_RATIO_4X3_LETTER_BOX:I = 0x5

.field private static final VIDEO_ASPECT_RATIO_4X3_PAN_SCAN:I = 0x4

.field private static final VIDEO_ASPECT_RATIO_AUTO:I


# instance fields
.field private mDataSourceAppType:I

.field private mDataSourceAudioCodec:I

.field private mDataSourceContentType:I

.field private mDataSourceMediaFormat:I

.field private mDataSourcePlayerType:I

.field private mDataSourceVideoCodec:I

.field private mNaticeSubtitleSurfaceTexture:I

.field private mSubtitleSurfaceHolder:Landroid/view/SurfaceHolder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "mmedia_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/media/MMediaPlayer;->native_init()V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/media/MediaPlayer;-><init>()V

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourcePlayerType:I

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceContentType:I

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceVideoCodec:I

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceAudioCodec:I

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceMediaFormat:I

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceAppType:I

    return-void
.end method

.method private native _setDataSource(Ljava/io/InputStream;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation
.end method

.method private native _setSubtitleSurface(Landroid/view/Surface;)V
.end method

.method public static create(Ljava/io/InputStream;Landroid/view/SurfaceHolder;)Lcom/mstar/android/media/MMediaPlayer;
    .locals 4
    .param p0    # Ljava/io/InputStream;
    .param p1    # Landroid/view/SurfaceHolder;

    :try_start_0
    new-instance v1, Lcom/mstar/android/media/MMediaPlayer;

    invoke-direct {v1}, Lcom/mstar/android/media/MMediaPlayer;-><init>()V

    invoke-virtual {v1, p0}, Lcom/mstar/android/media/MMediaPlayer;->setDataSource(Ljava/io/InputStream;)V

    if-eqz p1, :cond_0

    invoke-virtual {v1, p1}, Lcom/mstar/android/media/MMediaPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    :cond_0
    invoke-virtual {v1}, Lcom/mstar/android/media/MMediaPlayer;->prepare()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    const-string v2, "MMediaPlayer"

    const-string v3, "create failed:"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_1
    const/4 v1, 0x0

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v2, "MMediaPlayer"

    const-string v3, "create failed:"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :catch_2
    move-exception v0

    const-string v2, "MMediaPlayer"

    const-string v3, "create failed:"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private static final native native_init()V
.end method

.method private updateSubtitleSurfaceScreenOn()V
    .locals 2

    iget-object v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mSubtitleSurfaceHolder:Landroid/view/SurfaceHolder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mSubtitleSurfaceHolder:Landroid/view/SurfaceHolder;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setKeepScreenOn(Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method public DataSourceSwitch(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    const/16 v0, 0x7d2

    invoke-virtual {p0, v0, p1}, Lcom/mstar/android/media/MMediaPlayer;->setParameter(ILjava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public DemuxReset()Z
    .locals 2

    const/16 v0, 0x7d0

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/mstar/android/media/MMediaPlayer;->setParameter(II)Z

    move-result v0

    return v0
.end method

.method public SetSeamlessMode(Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;)Z
    .locals 3
    .param p1    # Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;

    const/4 v0, -0x1

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EnumPlayerSeamlessMode:[I

    invoke-virtual {p1}, Lcom/mstar/android/media/MMediaPlayer$EnumPlayerSeamlessMode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    const/4 v0, -0x1

    :goto_0
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v1, 0x0

    :goto_1
    return v1

    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    :cond_0
    const/16 v1, 0x7d1

    invoke-virtual {p0, v1, v0}, Lcom/mstar/android/media/MMediaPlayer;->setParameter(II)Z

    move-result v1

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public addTimedTextSource(Ljava/io/FileDescriptor;JJLjava/lang/String;)V
    .locals 3
    .param p1    # Ljava/io/FileDescriptor;
    .param p2    # J
    .param p4    # J
    .param p6    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    :try_start_0
    const-string v2, "android.media.IMediaPlayer"

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeFileDescriptor(Ljava/io/FileDescriptor;)V

    invoke-virtual {v1, p2, p3}, Landroid/os/Parcel;->writeLong(J)V

    invoke-virtual {v1, p4, p5}, Landroid/os/Parcel;->writeLong(J)V

    invoke-virtual {v1, p6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    invoke-virtual {p0, v1, v0}, Lcom/mstar/android/media/MMediaPlayer;->invoke(Landroid/os/Parcel;Landroid/os/Parcel;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    return-void

    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v2
.end method

.method public addTimedTextSource(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v2}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/mstar/android/media/MMediaPlayer;->addTimedTextSource(Ljava/io/FileDescriptor;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    return-void

    :cond_0
    new-instance v3, Ljava/io/IOException;

    invoke-direct {v3, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public captureVideoThumbnail(Ljava/lang/String;II)Landroid/graphics/Bitmap;
    .locals 4
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    new-instance v1, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v1}, Landroid/media/MediaMetadataRetriever;-><init>()V

    :try_start_0
    invoke-virtual {v1, p1}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    const-wide/16 v2, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/media/MediaMetadataRetriever;->getFrameAtTime(J)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :try_start_1
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_4

    :goto_0
    if-nez v0, :cond_0

    const/4 v2, 0x0

    :goto_1
    return-object v2

    :catch_0
    move-exception v2

    :try_start_2
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_1
    move-exception v2

    goto :goto_0

    :catch_2
    move-exception v2

    :try_start_3
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_0

    :catch_3
    move-exception v2

    goto :goto_0

    :catchall_0
    move-exception v2

    :try_start_4
    invoke-virtual {v1}, Landroid/media/MediaMetadataRetriever;->release()V
    :try_end_4
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_5

    :goto_2
    throw v2

    :cond_0
    const/4 v2, 0x1

    invoke-static {v0, p2, p3, v2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v2, v0

    goto :goto_1

    :catch_4
    move-exception v2

    goto :goto_0

    :catch_5
    move-exception v3

    goto :goto_2
.end method

.method public captureVideoThumbnailRelease()V
    .locals 2

    const-string v0, "MMediaPlayer"

    const-string v1, "captureVideoThumbnailRelease"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/mstar/android/media/MMediaPlayer;->release()V

    return-void
.end method

.method public native divx_GetAutochapter()I
.end method

.method public native divx_GetAutochapterTime(I)I
.end method

.method public native divx_GetChapter()I
.end method

.method public native divx_GetChapterTime(I)I
.end method

.method public divx_GetDrmInfo()Lcom/mstar/android/media/DivxDrmInfo;
    .locals 4

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Lcom/mstar/android/media/MMediaPlayer;->getMetadata(ZZ)Landroid/media/Metadata;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/mstar/android/media/DivxDrmInfo;

    invoke-direct {v1, v0}, Lcom/mstar/android/media/DivxDrmInfo;-><init>(Landroid/media/Metadata;)V

    :goto_0
    return-object v1

    :cond_0
    const-string v2, "MMediaPlayer"

    const-string v3, "divx_GetDrmInfo getMetadata return null!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public native divx_GetEdition()I
.end method

.method public divx_GetResumePlay(Lcom/mstar/android/media/DivxResumeInfo;)V
    .locals 8
    .param p1    # Lcom/mstar/android/media/DivxResumeInfo;

    const/16 v7, 0x30

    const/16 v6, 0x2f

    const/16 v5, 0x2e

    const/16 v4, 0x2d

    const/16 v3, 0x2c

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer;->getMetadata(ZZ)Landroid/media/Metadata;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0, v3}, Landroid/media/Metadata;->has(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v3}, Landroid/media/Metadata;->getLong(I)J

    move-result-wide v1

    invoke-virtual {p1, v1, v2}, Lcom/mstar/android/media/DivxResumeInfo;->setFilePos(J)V

    :cond_0
    invoke-virtual {v0, v4}, Landroid/media/Metadata;->has(I)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0, v4}, Landroid/media/Metadata;->getInt(I)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/mstar/android/media/DivxResumeInfo;->setPTS(I)V

    :cond_1
    invoke-virtual {v0, v5}, Landroid/media/Metadata;->has(I)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0, v5}, Landroid/media/Metadata;->getInt(I)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/mstar/android/media/DivxResumeInfo;->setResumeMKV(I)V

    :cond_2
    invoke-virtual {v0, v6}, Landroid/media/Metadata;->has(I)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0, v6}, Landroid/media/Metadata;->getInt(I)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/mstar/android/media/DivxResumeInfo;->setTitle(I)V

    :cond_3
    invoke-virtual {v0, v7}, Landroid/media/Metadata;->has(I)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0, v7}, Landroid/media/Metadata;->getInt(I)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/mstar/android/media/DivxResumeInfo;->setEdition(I)V

    :cond_4
    :goto_0
    return-void

    :cond_5
    const-string v1, "MMediaPlayer"

    const-string v2, "divx_GetResumePlay getMetadata return null!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public native divx_GetTitle()I
.end method

.method public native divx_SetAutochapter(I)V
.end method

.method public native divx_SetChapter(I)V
.end method

.method public native divx_SetEdition(I)V
.end method

.method public divx_SetResumePlay(Lcom/mstar/android/media/DivxResumeInfo;)I
    .locals 4
    .param p1    # Lcom/mstar/android/media/DivxResumeInfo;

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    invoke-virtual {p1}, Lcom/mstar/android/media/DivxResumeInfo;->getFilePos()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    invoke-virtual {p1}, Lcom/mstar/android/media/DivxResumeInfo;->getPTS()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p1}, Lcom/mstar/android/media/DivxResumeInfo;->getResumeMKV()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p1}, Lcom/mstar/android/media/DivxResumeInfo;->getTitle()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p1}, Lcom/mstar/android/media/DivxResumeInfo;->getEdition()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p0, v0}, Lcom/mstar/android/media/MMediaPlayer;->native_divx_SetResumePlay(Landroid/os/Parcel;)I

    move-result v1

    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    return v1
.end method

.method public native divx_SetTitle(I)V
.end method

.method public getAllSubtitleTrackInfo()Lcom/mstar/android/media/SubtitleTrackInfo;
    .locals 3

    const/16 v2, 0x7dd

    invoke-virtual {p0, v2}, Lcom/mstar/android/media/MMediaPlayer;->getParcelParameter(I)Landroid/os/Parcel;

    move-result-object v1

    new-instance v0, Lcom/mstar/android/media/SubtitleTrackInfo;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/SubtitleTrackInfo;-><init>(Landroid/os/Parcel;I)V

    return-object v0
.end method

.method public getAudioCodecType()Ljava/lang/String;
    .locals 5

    const/16 v4, 0x1a

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Lcom/mstar/android/media/MMediaPlayer;->getMetadata(ZZ)Landroid/media/Metadata;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v4}, Landroid/media/Metadata;->has(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v4}, Landroid/media/Metadata;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const-string v2, "MMediaPlayer"

    const-string v3, "getAudioCodecType getMetadata return null!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getAudioTrackInfo(Z)Lcom/mstar/android/media/AudioTrackInfo;
    .locals 7
    .param p1    # Z

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0, v4, v5}, Lcom/mstar/android/media/MMediaPlayer;->getMetadata(ZZ)Landroid/media/Metadata;

    move-result-object v0

    new-array v3, v6, [Ljava/lang/String;

    if-ne p1, v5, :cond_0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v6, :cond_0

    invoke-virtual {p0, v1}, Lcom/mstar/android/media/MMediaPlayer;->getAudioTrackStringData(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    if-eqz v0, :cond_1

    new-instance v2, Lcom/mstar/android/media/AudioTrackInfo;

    invoke-direct {v2, p1, v0, v3}, Lcom/mstar/android/media/AudioTrackInfo;-><init>(ZLandroid/media/Metadata;[Ljava/lang/String;)V

    :goto_1
    return-object v2

    :cond_1
    const-string v4, "MMediaPlayer"

    const-string v5, "getAudioTrackInfo getMetadata return null!!"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public native getAudioTrackStringData(I)Ljava/lang/String;
.end method

.method public native getPlayMode()I
.end method

.method public native getSubtitleData()Ljava/lang/String;
.end method

.method public getSubtitleTrackInfo(I)Lcom/mstar/android/media/SubtitleTrackInfo;
    .locals 3
    .param p1    # I

    const/16 v2, 0x7db

    invoke-virtual {p0, v2, p1}, Lcom/mstar/android/media/MMediaPlayer;->setParameter(II)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v2, 0x7dc

    invoke-virtual {p0, v2}, Lcom/mstar/android/media/MMediaPlayer;->getParcelParameter(I)Landroid/os/Parcel;

    move-result-object v1

    new-instance v0, Lcom/mstar/android/media/SubtitleTrackInfo;

    invoke-direct {v0, v1}, Lcom/mstar/android/media/SubtitleTrackInfo;-><init>(Landroid/os/Parcel;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getVideoInfo()Lcom/mstar/android/media/VideoCodecInfo;
    .locals 4

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Lcom/mstar/android/media/MMediaPlayer;->getMetadata(ZZ)Landroid/media/Metadata;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/mstar/android/media/VideoCodecInfo;

    invoke-direct {v1, v0}, Lcom/mstar/android/media/VideoCodecInfo;-><init>(Landroid/media/Metadata;)V

    :goto_0
    return-object v1

    :cond_0
    const-string v2, "MMediaPlayer"

    const-string v3, "getVideoInfo getMetadata return null!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public native native_captureMovieThumbnail(Landroid/os/Parcel;Landroid/os/Parcel;)Landroid/graphics/Bitmap;
.end method

.method public native native_divx_SetReplayFlag(Z)V
.end method

.method public native native_divx_SetResumePlay(Landroid/os/Parcel;)I
.end method

.method public native native_setExternalDataSourceInfo(Landroid/os/Parcel;)V
.end method

.method public native native_setVideoDisplayAspectRatio(I)V
.end method

.method public native offSubtitleTrack()V
.end method

.method public native onSubtitleTrack()V
.end method

.method public native setAudioTrack(I)V
.end method

.method public setDataSource(Ljava/io/InputStream;)V
    .locals 3
    .param p1    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/IllegalArgumentException;,
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    const/4 v2, 0x0

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    iget v1, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourcePlayerType:I

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget v1, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceContentType:I

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget v1, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceVideoCodec:I

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget v1, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceAudioCodec:I

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget v1, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceMediaFormat:I

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget v1, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceAppType:I

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p0, v0}, Lcom/mstar/android/media/MMediaPlayer;->native_setExternalDataSourceInfo(Landroid/os/Parcel;)V

    invoke-direct {p0, p1}, Lcom/mstar/android/media/MMediaPlayer;->_setDataSource(Ljava/io/InputStream;)V

    iput v2, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourcePlayerType:I

    iput v2, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceContentType:I

    iput v2, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceVideoCodec:I

    iput v2, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceAudioCodec:I

    iput v2, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceMediaFormat:I

    iput v2, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceAppType:I

    return-void
.end method

.method public setExternalDataSourceAppType(Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;)Z
    .locals 3
    .param p1    # Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    const/4 v2, 0x1

    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_DATASOURCE_APP_TYPE:[I

    invoke-virtual {p1}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceAppType:I

    :goto_0
    return v2

    :pswitch_0
    iput v2, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceAppType:I

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x2

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceAppType:I

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x3

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceAppType:I

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x4

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceAppType:I

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x5

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceAppType:I

    goto :goto_0

    :pswitch_5
    const/4 v0, 0x6

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceAppType:I

    goto :goto_0

    :pswitch_6
    const/4 v0, 0x7

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceAppType:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public setExternalDataSourceAudioCodec(Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;)Z
    .locals 3
    .param p1    # Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;

    const/4 v2, 0x1

    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC:[I

    invoke-virtual {p1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_AUDIO_CODEC;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, -0x1

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceAudioCodec:I

    :goto_0
    return v2

    :pswitch_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceAudioCodec:I

    goto :goto_0

    :pswitch_1
    iput v2, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceAudioCodec:I

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceAudioCodec:I

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x3

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceAudioCodec:I

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x4

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceAudioCodec:I

    goto :goto_0

    :pswitch_5
    const/4 v0, 0x5

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceAudioCodec:I

    goto :goto_0

    :pswitch_6
    const/4 v0, 0x6

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceAudioCodec:I

    goto :goto_0

    :pswitch_7
    const/4 v0, 0x7

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceAudioCodec:I

    goto :goto_0

    :pswitch_8
    const/16 v0, 0x8

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceAudioCodec:I

    goto :goto_0

    :pswitch_9
    const/16 v0, 0x9

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceAudioCodec:I

    goto :goto_0

    :pswitch_a
    const/16 v0, 0xa

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceAudioCodec:I

    goto :goto_0

    :pswitch_b
    const/16 v0, 0xb

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceAudioCodec:I

    goto :goto_0

    :pswitch_c
    const/16 v0, 0xc

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceAudioCodec:I

    goto :goto_0

    :pswitch_d
    const/16 v0, 0xd

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceAudioCodec:I

    goto :goto_0

    :pswitch_e
    const/16 v0, 0xe

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceAudioCodec:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method public setExternalDataSourceContentType(Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_CONTENT_TYPE;)Z
    .locals 3
    .param p1    # Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_CONTENT_TYPE;

    const/4 v2, 0x1

    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_CONTENT_TYPE:[I

    invoke-virtual {p1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_CONTENT_TYPE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceContentType:I

    :goto_0
    return v2

    :pswitch_0
    iput v2, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceContentType:I

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x2

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceContentType:I

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x3

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceContentType:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setExternalDataSourceMediaFormat(Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;)Z
    .locals 3
    .param p1    # Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;

    const/4 v2, 0x1

    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE:[I

    invoke-virtual {p1}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_MEDIA_FORMAT_TYPE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, -0x1

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceMediaFormat:I

    :goto_0
    return v2

    :pswitch_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceMediaFormat:I

    goto :goto_0

    :pswitch_1
    iput v2, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceMediaFormat:I

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceMediaFormat:I

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x3

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceMediaFormat:I

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x4

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceMediaFormat:I

    goto :goto_0

    :pswitch_5
    const/4 v0, 0x5

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceMediaFormat:I

    goto :goto_0

    :pswitch_6
    const/4 v0, 0x6

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceMediaFormat:I

    goto :goto_0

    :pswitch_7
    const/4 v0, 0x7

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceMediaFormat:I

    goto :goto_0

    :pswitch_8
    const/16 v0, 0x8

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceMediaFormat:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public setExternalDataSourcePlayerType(Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_PLAYER_TYPE;)Z
    .locals 3
    .param p1    # Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_PLAYER_TYPE;

    const/4 v2, 0x1

    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_PLAYER_TYPE:[I

    invoke-virtual {p1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_PLAYER_TYPE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourcePlayerType:I

    :goto_0
    return v2

    :pswitch_0
    iput v2, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourcePlayerType:I

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x2

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourcePlayerType:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setExternalDataSourceVideoCodec(Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;)Z
    .locals 3
    .param p1    # Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;

    const/4 v2, 0x1

    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC:[I

    invoke-virtual {p1}, Lcom/mstar/android/media/MMediaPlayer$EN_MS_DATASOURCE_ES_VIDEO_CODEC;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, -0x1

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceVideoCodec:I

    :goto_0
    return v2

    :pswitch_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceVideoCodec:I

    goto :goto_0

    :pswitch_1
    iput v2, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceVideoCodec:I

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceVideoCodec:I

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x3

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceVideoCodec:I

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x4

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceVideoCodec:I

    goto :goto_0

    :pswitch_5
    const/4 v0, 0x5

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceVideoCodec:I

    goto :goto_0

    :pswitch_6
    const/4 v0, 0x6

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceVideoCodec:I

    goto :goto_0

    :pswitch_7
    const/4 v0, 0x7

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceVideoCodec:I

    goto :goto_0

    :pswitch_8
    const/16 v0, 0x8

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceVideoCodec:I

    goto :goto_0

    :pswitch_9
    const/16 v0, 0x9

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceVideoCodec:I

    goto :goto_0

    :pswitch_a
    const/16 v0, 0xa

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceVideoCodec:I

    goto :goto_0

    :pswitch_b
    const/16 v0, 0xb

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceVideoCodec:I

    goto :goto_0

    :pswitch_c
    const/16 v0, 0xc

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceVideoCodec:I

    goto :goto_0

    :pswitch_d
    const/16 v0, 0xd

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceVideoCodec:I

    goto :goto_0

    :pswitch_e
    const/16 v0, 0xe

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceVideoCodec:I

    goto :goto_0

    :pswitch_f
    const/16 v0, 0xf

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceVideoCodec:I

    goto :goto_0

    :pswitch_10
    const/16 v0, 0x10

    iput v0, p0, Lcom/mstar/android/media/MMediaPlayer;->mDataSourceVideoCodec:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method public native setPlayMode(I)Z
.end method

.method public native setSubtitleDataSource(Ljava/lang/String;)V
.end method

.method public setSubtitleDisplay(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1    # Landroid/view/SurfaceHolder;

    iput-object p1, p0, Lcom/mstar/android/media/MMediaPlayer;->mSubtitleSurfaceHolder:Landroid/view/SurfaceHolder;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    :goto_0
    invoke-direct {p0, v0}, Lcom/mstar/android/media/MMediaPlayer;->_setSubtitleSurface(Landroid/view/Surface;)V

    invoke-direct {p0}, Lcom/mstar/android/media/MMediaPlayer;->updateSubtitleSurfaceScreenOn()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public native setSubtitleSync(I)I
.end method

.method public native setSubtitleTrack(I)V
.end method

.method public setVideoDisplayAspectRatio(Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;)Z
    .locals 5
    .param p1    # Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v0, "MMediaPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "aspect Ratio:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$1;->$SwitchMap$com$mstar$android$media$MMediaPlayer$EnumVideoAspectRatio:[I

    invoke-virtual {p1}, Lcom/mstar/android/media/MMediaPlayer$EnumVideoAspectRatio;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p0, v3}, Lcom/mstar/android/media/MMediaPlayer;->native_setVideoDisplayAspectRatio(I)V

    :goto_0
    return v4

    :pswitch_0
    invoke-virtual {p0, v3}, Lcom/mstar/android/media/MMediaPlayer;->native_setVideoDisplayAspectRatio(I)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, v4}, Lcom/mstar/android/media/MMediaPlayer;->native_setVideoDisplayAspectRatio(I)V

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/mstar/android/media/MMediaPlayer;->native_setVideoDisplayAspectRatio(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
