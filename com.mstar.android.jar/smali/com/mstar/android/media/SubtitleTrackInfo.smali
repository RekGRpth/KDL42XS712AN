.class public Lcom/mstar/android/media/SubtitleTrackInfo;
.super Ljava/lang/Object;
.source "SubtitleTrackInfo.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/android/media/SubtitleTrackInfo$1;,
        Lcom/mstar/android/media/SubtitleTrackInfo$SUBTITLE_LANGUAGE_TYPE;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SubtitleTrackInfo"


# instance fields
.field private final INFO_NUM:I

.field private final MAX_SUBTITLE_TRACK:I

.field private mDataPos:I

.field private mIsGetAllInfo:Z

.field private mParcel:Landroid/os/Parcel;

.field private mSubtitleLanguageType:Lcom/mstar/android/media/SubtitleTrackInfo$SUBTITLE_LANGUAGE_TYPE;

.field private mSubtitleNum:I


# direct methods
.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x14

    iput v0, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->MAX_SUBTITLE_TRACK:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->INFO_NUM:I

    iput-object p1, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    iget-object v0, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    if-nez v0, :cond_0

    const-string v0, "SubtitleTrackInfo"

    const-string v1, "The parcel getted from below layer is null OR you have called SubtitleTrackInfo\'s funtion with a wrong way!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mIsGetAllInfo:Z

    iget-object v0, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    invoke-virtual {v0}, Landroid/os/Parcel;->dataPosition()I

    move-result v0

    iput v0, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mDataPos:I

    const-string v0, "SubtitleTrackInfo"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mDataPos = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mDataPos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;I)V
    .locals 5
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    const/16 v4, 0x14

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v4, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->MAX_SUBTITLE_TRACK:I

    const/4 v1, 0x3

    iput v1, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->INFO_NUM:I

    iput-object p1, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    iget-object v1, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    if-nez v1, :cond_1

    const-string v1, "SubtitleTrackInfo"

    const-string v2, "The parcel getted from below layer is null OR you have called SubtitleTrackInfo\'s funtion with a wrong way!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    const-string v1, "SubtitleTrackInfo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TOTOAL INFO NUM = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    invoke-virtual {v1}, Landroid/os/Parcel;->dataPosition()I

    move-result v1

    iput v1, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mDataPos:I

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mIsGetAllInfo:Z

    add-int/lit8 v1, v0, -0x2

    div-int/lit8 v1, v1, 0x3

    iput v1, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mSubtitleNum:I

    iget v1, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mSubtitleNum:I

    if-le v1, v4, :cond_2

    const-string v1, "SubtitleTrackInfo"

    const-string v2, "Subtitle\'s number overflow! "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput v4, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mSubtitleNum:I

    :cond_2
    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    const-string v1, "SubtitleTrackInfo"

    const-string v2, "The video is NOT playing through MstarPlayer or MstPlayer,so all the subtitle trakc infomations are invalid!!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public getAllImageSubtitleCount()I
    .locals 3

    iget-object v1, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    if-nez v1, :cond_0

    const-string v1, "SubtitleTrackInfo"

    const-string v2, "The parcel in SubtitleTrackInfo object is null!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, -0x2

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    iget v2, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mDataPos:I

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->setDataPosition(I)V

    const/4 v0, 0x0

    :goto_1
    iget v1, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mSubtitleNum:I

    mul-int/lit8 v1, v1, 0x3

    add-int/lit8 v1, v1, 0x1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    goto :goto_0
.end method

.method public getAllInternalSubtitleCount()I
    .locals 3

    iget-object v1, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    if-nez v1, :cond_0

    const-string v1, "SubtitleTrackInfo"

    const-string v2, "The parcel in SubtitleTrackInfo object is null!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, -0x2

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    iget v2, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mDataPos:I

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->setDataPosition(I)V

    const/4 v0, 0x0

    :goto_1
    iget v1, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mSubtitleNum:I

    mul-int/lit8 v1, v1, 0x3

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    goto :goto_0
.end method

.method public getAllSubtitleCount()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mSubtitleNum:I

    return v0
.end method

.method public getSubtitleCodeType()I
    .locals 2

    iget-object v0, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    if-nez v0, :cond_0

    const-string v0, "SubtitleTrackInfo"

    const-string v1, "The parcel in SubtitleTrackInfo object is null!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, -0x2

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    iget v1, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mDataPos:I

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    iget-object v0, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    invoke-virtual {v0}, Landroid/os/Parcel;->readInt()I

    iget-object v0, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    invoke-virtual {v0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    goto :goto_0
.end method

.method public getSubtitleCodeType([I)V
    .locals 4
    .param p1    # [I

    iget-object v2, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    if-nez v2, :cond_1

    const-string v2, "SubtitleTrackInfo"

    const-string v3, "The parcel in SubtitleTrackInfo object is null!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    iget v3, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mDataPos:I

    invoke-virtual {v2, v3}, Landroid/os/Parcel;->setDataPosition(I)V

    const/4 v1, 0x0

    :goto_1
    iget v2, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mSubtitleNum:I

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    :try_start_0
    iget-object v2, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    aput v2, p1, v1
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v2, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v2, "SubtitleTrackInfo"

    const-string v3, "Make sure the allocation of array\'s space based on the result of function getAllSubtitleCount()!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "SubtitleTrackInfo"

    const-string v3, "getSubtitleLanguageType:"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getSubtitleLanguageType(Z)Ljava/lang/String;
    .locals 3
    .param p1    # Z

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    if-nez v1, :cond_1

    const-string v1, "SubtitleTrackInfo"

    const-string v2, "The parcel in SubtitleTrackInfo object is null!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-boolean v1, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mIsGetAllInfo:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    iget v2, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mDataPos:I

    invoke-virtual {v1, v2}, Landroid/os/Parcel;->setDataPosition(I)V

    iget-object v1, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    iget-object v1, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I

    invoke-static {}, Lcom/mstar/android/media/SubtitleTrackInfo$SUBTITLE_LANGUAGE_TYPE;->values()[Lcom/mstar/android/media/SubtitleTrackInfo$SUBTITLE_LANGUAGE_TYPE;

    move-result-object v1

    iget-object v2, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mSubtitleLanguageType:Lcom/mstar/android/media/SubtitleTrackInfo$SUBTITLE_LANGUAGE_TYPE;

    :cond_2
    iget-object v1, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mSubtitleLanguageType:Lcom/mstar/android/media/SubtitleTrackInfo$SUBTITLE_LANGUAGE_TYPE;

    if-eqz v1, :cond_0

    if-nez p1, :cond_3

    sget-object v1, Lcom/mstar/android/media/SubtitleTrackInfo$1;->$SwitchMap$com$mstar$android$media$SubtitleTrackInfo$SUBTITLE_LANGUAGE_TYPE:[I

    iget-object v2, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mSubtitleLanguageType:Lcom/mstar/android/media/SubtitleTrackInfo$SUBTITLE_LANGUAGE_TYPE;

    invoke-virtual {v2}, Lcom/mstar/android/media/SubtitleTrackInfo$SUBTITLE_LANGUAGE_TYPE;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    const-string v0, "undefined"

    goto :goto_0

    :pswitch_0
    const-string v0, "German"

    goto :goto_0

    :pswitch_1
    const-string v0, "English"

    goto :goto_0

    :pswitch_2
    const-string v0, "Spanish"

    goto :goto_0

    :pswitch_3
    const-string v0, "Greek"

    goto :goto_0

    :pswitch_4
    const-string v0, "French"

    goto :goto_0

    :pswitch_5
    const-string v0, "Croatian"

    goto :goto_0

    :pswitch_6
    const-string v0, "Italian"

    goto :goto_0

    :pswitch_7
    const-string v0, "Dutch"

    goto :goto_0

    :pswitch_8
    const-string v0, "Polish"

    goto :goto_0

    :pswitch_9
    const-string v0, "Portuguese"

    goto :goto_0

    :pswitch_a
    const-string v0, "Russian"

    goto :goto_0

    :pswitch_b
    const-string v0, "Romanian"

    goto :goto_0

    :pswitch_c
    const-string v0, "Swedish"

    goto :goto_0

    :pswitch_d
    const-string v0, "Arabic"

    goto :goto_0

    :pswitch_e
    const-string v0, "Chinese"

    goto :goto_0

    :pswitch_f
    const-string v0, "Japanese"

    goto :goto_0

    :pswitch_10
    const-string v0, "Korean"

    goto :goto_0

    :cond_3
    const-string v0, ""

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method public getSubtitleLanguageType([Ljava/lang/String;Z)V
    .locals 4
    .param p1    # [Ljava/lang/String;
    .param p2    # Z

    iget-object v2, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    if-nez v2, :cond_1

    const-string v2, "SubtitleTrackInfo"

    const-string v3, "The parcel in SubtitleTrackInfo object is null!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    iget v3, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mDataPos:I

    invoke-virtual {v2, v3}, Landroid/os/Parcel;->setDataPosition(I)V

    const/4 v1, 0x0

    :goto_1
    iget v2, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mSubtitleNum:I

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    iget-object v2, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    invoke-static {}, Lcom/mstar/android/media/SubtitleTrackInfo$SUBTITLE_LANGUAGE_TYPE;->values()[Lcom/mstar/android/media/SubtitleTrackInfo$SUBTITLE_LANGUAGE_TYPE;

    move-result-object v2

    iget-object v3, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    invoke-virtual {v3}, Landroid/os/Parcel;->readInt()I

    move-result v3

    aget-object v2, v2, v3

    iput-object v2, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mSubtitleLanguageType:Lcom/mstar/android/media/SubtitleTrackInfo$SUBTITLE_LANGUAGE_TYPE;

    :try_start_0
    invoke-virtual {p0, p2}, Lcom/mstar/android/media/SubtitleTrackInfo;->getSubtitleLanguageType(Z)Ljava/lang/String;

    move-result-object v2

    aput-object v2, p1, v1
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v2, "SubtitleTrackInfo"

    const-string v3, "Make sure the allocation of array\'s space based on the result of function getAllSubtitleCount()!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "SubtitleTrackInfo"

    const-string v3, "getSubtitleLanguageType:"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public getSubtitleType()I
    .locals 2

    iget-object v0, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    if-nez v0, :cond_0

    const-string v0, "SubtitleTrackInfo"

    const-string v1, "The parcel in SubtitleTrackInfo object is null!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, -0x2

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    iget v1, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mDataPos:I

    invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    iget-object v0, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    invoke-virtual {v0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    goto :goto_0
.end method

.method public getSubtitleType([I)V
    .locals 4
    .param p1    # [I

    iget-object v2, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    if-nez v2, :cond_1

    const-string v2, "SubtitleTrackInfo"

    const-string v3, "The parcel in SubtitleTrackInfo object is null!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    iget v3, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mDataPos:I

    invoke-virtual {v2, v3}, Landroid/os/Parcel;->setDataPosition(I)V

    const/4 v1, 0x0

    :goto_1
    iget v2, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mSubtitleNum:I

    if-ge v1, v2, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    aput v2, p1, v1
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v2, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    iget-object v2, p0, Lcom/mstar/android/media/SubtitleTrackInfo;->mParcel:Landroid/os/Parcel;

    invoke-virtual {v2}, Landroid/os/Parcel;->readInt()I

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v2, "SubtitleTrackInfo"

    const-string v3, "Make sure the allocation of array\'s space based on the result of function getAllSubtitleCount()!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "SubtitleTrackInfo"

    const-string v3, "getSubtitleLanguageType:"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
