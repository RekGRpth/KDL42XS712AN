.class public final enum Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;
.super Ljava/lang/Enum;
.source "MMediaPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/android/media/MMediaPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EN_DATASOURCE_APP_TYPE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

.field public static final enum E_DATASOURCE_AP_ANDROID_STREAMING:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

.field public static final enum E_DATASOURCE_AP_ANDROID_USB:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

.field public static final enum E_DATASOURCE_AP_DLNA:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

.field public static final enum E_DATASOURCE_AP_HBBTV:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

.field public static final enum E_DATASOURCE_AP_NETFLIX:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

.field public static final enum E_DATASOURCE_AP_NORMAL:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

.field public static final enum E_DATASOURCE_AP_WEBBROWSER:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

.field public static final enum E_DATASOURCE_AP_WMDRM10:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    const-string v1, "E_DATASOURCE_AP_NORMAL"

    invoke-direct {v0, v1, v3}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;->E_DATASOURCE_AP_NORMAL:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    const-string v1, "E_DATASOURCE_AP_NETFLIX"

    invoke-direct {v0, v1, v4}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;->E_DATASOURCE_AP_NETFLIX:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    const-string v1, "E_DATASOURCE_AP_DLNA"

    invoke-direct {v0, v1, v5}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;->E_DATASOURCE_AP_DLNA:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    const-string v1, "E_DATASOURCE_AP_HBBTV"

    invoke-direct {v0, v1, v6}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;->E_DATASOURCE_AP_HBBTV:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    const-string v1, "E_DATASOURCE_AP_WEBBROWSER"

    invoke-direct {v0, v1, v7}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;->E_DATASOURCE_AP_WEBBROWSER:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    const-string v1, "E_DATASOURCE_AP_WMDRM10"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;->E_DATASOURCE_AP_WMDRM10:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    const-string v1, "E_DATASOURCE_AP_ANDROID_USB"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;->E_DATASOURCE_AP_ANDROID_USB:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    new-instance v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    const-string v1, "E_DATASOURCE_AP_ANDROID_STREAMING"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;->E_DATASOURCE_AP_ANDROID_STREAMING:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;->E_DATASOURCE_AP_NORMAL:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;->E_DATASOURCE_AP_NETFLIX:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;->E_DATASOURCE_AP_DLNA:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;->E_DATASOURCE_AP_HBBTV:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;->E_DATASOURCE_AP_WEBBROWSER:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;->E_DATASOURCE_AP_WMDRM10:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;->E_DATASOURCE_AP_ANDROID_USB:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;->E_DATASOURCE_AP_ANDROID_STREAMING:Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;->$VALUES:[Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    return-object v0
.end method

.method public static values()[Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;
    .locals 1

    sget-object v0, Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;->$VALUES:[Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    invoke-virtual {v0}, [Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/mstar/android/media/MMediaPlayer$EN_DATASOURCE_APP_TYPE;

    return-object v0
.end method
