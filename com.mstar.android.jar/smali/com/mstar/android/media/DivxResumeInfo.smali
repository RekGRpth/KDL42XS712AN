.class public Lcom/mstar/android/media/DivxResumeInfo;
.super Ljava/lang/Object;
.source "DivxResumeInfo.java"


# static fields
.field private static final LOGD:Z = false

.field private static final TAG:Ljava/lang/String; = "DivxResumeInfo"


# instance fields
.field private mSName:Ljava/lang/String;

.field private mSPath:Ljava/lang/String;

.field private mU32PTS:I

.field private mU64FilePos:J

.field private mU8Edition:I

.field private mU8ResumeMKV:I

.field private mU8Title:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/media/Metadata;)V
    .locals 2
    .param p1    # Landroid/media/Metadata;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x2c

    invoke-virtual {p1, v0}, Landroid/media/Metadata;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/mstar/android/media/DivxResumeInfo;->mU64FilePos:J

    const/16 v0, 0x2d

    invoke-virtual {p1, v0}, Landroid/media/Metadata;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/mstar/android/media/DivxResumeInfo;->mU32PTS:I

    const/16 v0, 0x2e

    invoke-virtual {p1, v0}, Landroid/media/Metadata;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/mstar/android/media/DivxResumeInfo;->mU8ResumeMKV:I

    const/16 v0, 0x2f

    invoke-virtual {p1, v0}, Landroid/media/Metadata;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/mstar/android/media/DivxResumeInfo;->mU8Title:I

    const/16 v0, 0x30

    invoke-virtual {p1, v0}, Landroid/media/Metadata;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/mstar/android/media/DivxResumeInfo;->mU8Edition:I

    return-void
.end method


# virtual methods
.method public getEdition()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/media/DivxResumeInfo;->mU8Edition:I

    return v0
.end method

.method public getFilePos()J
    .locals 2

    iget-wide v0, p0, Lcom/mstar/android/media/DivxResumeInfo;->mU64FilePos:J

    return-wide v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mstar/android/media/DivxResumeInfo;->mSName:Ljava/lang/String;

    return-object v0
.end method

.method public getPTS()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/media/DivxResumeInfo;->mU32PTS:I

    return v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/mstar/android/media/DivxResumeInfo;->mSPath:Ljava/lang/String;

    return-object v0
.end method

.method public getResumeMKV()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/media/DivxResumeInfo;->mU8ResumeMKV:I

    return v0
.end method

.method public getTitle()I
    .locals 1

    iget v0, p0, Lcom/mstar/android/media/DivxResumeInfo;->mU8Title:I

    return v0
.end method

.method public setEdition(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mstar/android/media/DivxResumeInfo;->mU8Edition:I

    return-void
.end method

.method public setFilePos(J)V
    .locals 0
    .param p1    # J

    iput-wide p1, p0, Lcom/mstar/android/media/DivxResumeInfo;->mU64FilePos:J

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mstar/android/media/DivxResumeInfo;->mSName:Ljava/lang/String;

    return-void
.end method

.method public setPTS(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mstar/android/media/DivxResumeInfo;->mU32PTS:I

    return-void
.end method

.method public setPath(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/mstar/android/media/DivxResumeInfo;->mSPath:Ljava/lang/String;

    return-void
.end method

.method public setResumeMKV(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mstar/android/media/DivxResumeInfo;->mU8ResumeMKV:I

    return-void
.end method

.method public setTitle(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/mstar/android/media/DivxResumeInfo;->mU8Title:I

    return-void
.end method
